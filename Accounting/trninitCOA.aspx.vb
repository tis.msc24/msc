'Created By Widi On 06 August 2014
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trninitCOA
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Dim sSql As String = ""
    Private cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Public Function GetIDCB() As String
        Return Eval("cmpcode") & "," & Eval("crdgloid")
    End Function

    Private Function IsFilterValid()
        Dim sError As String = ""
        If dd_branch.SelectedValue = "" Then
            sError &= "- Please select OUTLET field!<BR>"
        End If
        Dim sErr As String = ""
        If gldate.Text = "" Then
            sError &= "- Please fill INIT DATE field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, CompnyName & "- WARNING", 2, "modalMsgBox")
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvTblDtl.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function GetDropDownListValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvTblDtl.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                End If
            Next
        End If
        Return sReturn
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = GVtrnjurnal.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Private Function IsInputValid() As Boolean
        Try
            Dim sMsg As String = ""
            Dim sErr As String = ""
            Dim offDate As String = ""

            Dim CutOffDate As Date
            sSql = "SELECT genother2 FROM QL_mstgen WHERE gengroup ='CUTOFFDATE' AND genother3='" & Session("CompnyCode") & "'"
            If Not IsDate(toDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & "- WARNING", 2, "modalMsgBox")
                Exit Function
            Else
                CutOffDate = CDate(toDate(GetStrData(sSql)))
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & "- WARNING", 2, "modalMsgBox")
                Exit Function
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "- WARNING", 1, "modalMsgBox")
            Return False
            Exit Function
        End Try
        Return True
    End Function

#End Region

#Region "Procedures"

    Private Sub InitAllDDL()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
            FillDDL(dd_branchCari, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_branch, sSql)
                FillDDL(dd_branchCari, sSql)
            Else
                FillDDL(dd_branch, sSql)
                FillDDL(dd_branchCari, sSql)
                'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
                'ddlFromcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dd_branch, sSql)
            FillDDL(dd_branchCari, sSql)
        End If

        'If Session("branch_id") = "01" Then
        '    sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
        'Else
        '    sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and gencode='" & Session("branch_id") & "'"
        'End If
        'FillDDL(dd_branch, sSql)
        'FillDDL(dd_branchCari, sSql)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssclass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub fillTextBox(ByVal vjurnaloid As String, ByVal voutlet As String)
        Try
            sSql = "SELECT  c.crdgloid,c.crdgldate,c.acctgoid,a.acctgcode+'-'+a.acctgdesc AS acctgdesc,c.amtopen,c.glflag,c.gldesc,c.createuser,c.upduser,c.updtime, c.branch_code " & _
            "FROM QL_crdgl c INNER JOIN QL_mstacctg a ON a.acctgoid=c.acctgoid " & _
            "WHERE c.cmpcode='MSC' AND c.crdgloid<0 AND c.crdgloid=" & vjurnaloid
            Dim dtData As DataTable = cKon.ambiltabel(sSql, "QL_crdgl")
            If dtData.Rows.Count > 0 Then
                crdgloid.Text = dtData.Rows(0)("crdgloid").ToString
                gldate.Text = Format(CDate(dtData.Rows(0)("crdgldate").ToString), "dd/MM/yyyy")

                Session("acctgoid") = dtData.Rows(0)("acctgoid").ToString
                Session("acctgdesc") = dtData.Rows(0)("acctgdesc").ToString
                Session("glamt") = ToMaskEdit(Math.Abs(ToDouble(dtData.Rows(0)("amtopen").ToString)), 2)
                Session("gldbcr") = IIf(ToDouble(dtData.Rows(0)("amtopen").ToString) < 0, "C", "D")
                dd_branch.SelectedValue = dtData.Rows(0)("branch_code").ToString
                crdglflag.Text = dtData.Rows(0)("glflag").ToString
                crdglnote.Text = dtData.Rows(0)("acctgdesc").ToString
                create.Text = "Created By <B>" & dtData.Rows(0)("createuser").ToString & "</B> On <B>" & dtData.Rows(0)("updtime").ToString & "</B> "
                update.Text = "; Last Updated By <B>" & dtData.Rows(0)("upduser").ToString & "</B> On <B>" & dtData.Rows(0)("updtime").ToString & "</B> "

             
                If crdglflag.Text = "OPEN" Then
                    imbSave.Visible = False
                    imbDelete.Visible = False
                    imbPost.Visible = False
                    gldate.Enabled = False
                    imbDate.Visible = False
                    crdglnote.Enabled = False
                End If

            Else
                showMessage("Gagal Tampilkan Data Saldo Awal GL !!", CompnyName & "- ERROR", 1, "modalMsgBox")
                Exit Sub
            End If
            dd_branch.Enabled = False
            dd_branch.CssClass = "inpTextDisabled"
        Catch ex As Exception
            conn.Close()
            showMessage(ex.Message, CompnyName & "- ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Public Sub binddata()
        'Dim CutOffStaingAwal As String
        'Dim CutOffstringAkhir As String
        Dim CutOffDateAwal As Date
        Dim CutOffDateAkhir As Date
        Try
            If IsDate(toDate(txtPeriode1.Text)) = False Then
                showMessage("Format tgl periode 1 salah !!", CompnyName & "- WARNING", 2, "modalMsgBox")
                Exit Sub
            End If
            If IsDate(toDate(txtPeriode2.Text)) = False Then
                showMessage("Format tgl periode 2 salah !!", CompnyName & "- WARNING", 2, "modalMsgBox")
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Format tgl periode salah !!", CompnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        End Try
        Dim branchcode As String = dd_branchCari.SelectedValue
        CutOffDateAwal = "#" & toDate(txtPeriode1.Text) & " 7:27:15PM#"
        CutOffDateAwal = FormatDateTime(CutOffDateAwal, DateFormat.ShortDate)

        CutOffDateAkhir = "#" & toDate(txtPeriode2.Text) & " 7:27:15PM#"
        CutOffDateAkhir = FormatDateTime(CutOffDateAkhir, DateFormat.ShortDate)
        sSql = "SELECT c.crdgloid, c.crdgldate, c.periodacctg, a.acctgcode + ' - ' + a.acctgdesc AS account, ABS(c.amtopen) AS amtopen, c.glflag, c.cmpcode, g.gendesc as outlet, 'dbcr' =CASE WHEN c.amtopen < 0 THEN 'CREDIT' ELSE 'DEBET' END, 0 AS selected FROM QL_crdgl c INNER JOIN QL_mstacctg a ON a.acctgoid=c.acctgoid INNER JOIN QL_mstgen g ON c.branch_code=g.gencode AND g.gengroup = 'CABANG' WHERE c.branch_code='" & branchcode & "' AND c.crdgloid<0 AND glflag<>'DELETE' "

        If cbBlmPosting.Checked Then
            sSql += "AND c.glflag<>'OPEN' "
        End If
        sSql += "ORDER BY c.crdgldate DESC"

        Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_crdgl")
        Session("TblGL") = objTable
        GVtrnjurnal.DataSource = Session("TblGL")
        GVtrnjurnal.DataBind()
        UnabledCheckBox()
    End Sub

    Private Sub UpdateDetailData()
        If Not Session("TblDtl") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                For C1 As Integer = 0 To gvTblDtl.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvTblDtl.Rows(C1)
                    dtView.RowFilter = "seq=" & row.Cells(0).Text
                    If dtView.Count > 0 Then
                        dtView(0)("acctgdbcr") = GetDropDownListValue(C1, 3)
                        dtView(0)("amount") = ToDouble(GetTextBoxValue(C1, 4))
                    End If
                    dtView.RowFilter = ""
                Next
                Session("TblDtl") = dtView.ToTable
            End If
        End If
    End Sub

    Public Sub UnabledCheckBox()

        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblGL")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (GVtrnjurnal.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("glflag".ToString)) = "OPEN" Or Trim(objRow(C1)("glflag".ToString) = "DELETE") Then
                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = GVtrnjurnal.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub CheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblGL")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("glflag").ToString) <> "OPEN" And Trim(objRow(C1)("glflag").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = GVtrnjurnal.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblGL")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("glflag").ToString) <> "OPEN" And Trim(objRow(C1)("glflag").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = GVtrnjurnal.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub UpdateCheckedPost()
        'Untuk mendapat cek box value multi select
        Dim dv As DataView = Session("TblGL").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To GVtrnjurnal.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = GVtrnjurnal.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        Dim a As String() = sOid.Split(",")
                        dv.RowFilter = "crdgloid=" & a(1) & " AND cmpcode='" & a(0) & "'"
                        If cbcheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Accounting\trninitCOA.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        Session("outlet") = Request.QueryString("cmpcode")

        Page.Title = CompnyName & " - COA Inittial Balance"
        imbDelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan menghapus data ini ?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan mem-posting data ini ?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            txtPeriode1.Text = Format(Now, "01/MM/yyyy")
            txtPeriode2.Text = Format(Now, "dd/MM/yyyy")
            InitAllDDL()
            binddata()

            Dim CutOffDate As String = ""
            'sSql = "SELECT top 1 crdgldate FROM QL_crdgl WHERE glflag = 'OPEN' AND branch_code = '" & Session("branch_id") & "' ORDER BY crdgldate ASC "
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
            If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & "- WARNING", 2, "modalMsgBox")
                Exit Sub
            Else
                CutOffDate = GetStrData(sSql)
            End If

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                fillTextBox(Session("oid"), "MSC")
                TabContainer1.ActiveTabIndex = 1
            Else
                gldate.Text = CutOffDate
                crdglflag.Text = "In Process"
                txtPostDate.Text = "1/1/1900"
                TabContainer1.ActiveTabIndex = 0
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
                update.Text = ""
                imbSave.Visible = False
                imbPost.Visible = True
                imbCancel.Visible = True
                imbDelete.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnShowData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowData.Click
        If IsFilterValid() Then
            Dim sPeriod As String = GetPeriodAcctg(CDate(toDate(gldate.Text)))

            If Session("oid") Is Nothing Or Session("oid") = "" Then
                sSql = "SELECT 0 seq,acctgoid,acctgcode,acctgdesc, acctgdbcr, 0.0 amount FROM QL_mstacctg W WHERE acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND a.cmpcode=W.cmpcode) AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ORDER BY acctgcode"
                Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstacctg")
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    dtTbl.Rows.Item(C1)("seq") = C1 + 1
                Next
                Session("TblDtl") = dtTbl
            Else
                sSql = "SELECT 0 seq,acctgoid,acctgcode,acctgdesc, acctgdbcr, 0.0 amount FROM QL_mstacctg W WHERE acctgoid='" & Session("acctgoid") & "' AND acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND a.cmpcode=W.cmpcode) AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ORDER BY acctgcode"
                Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstacctg")
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    dtTbl.Rows.Item(C1)("seq") = C1 + 1
                    dtTbl.Rows.Item(C1)("amount") = ToMaskEdit(ToDouble(Session("glamt")), 3)
                    dtTbl.Rows.Item(C1)("acctgdbcr") = Session("gldbcr")
                Next
                Session("TblDtl") = dtTbl
            End If

            gvTblDtl.DataSource = Session("TblDtl")
            gvTblDtl.DataBind()
            gvTblDtl.Visible = True
        End If
    End Sub

    Protected Sub gvTblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub cbPeriode_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtPeriode1.Enabled = sender.Checked : txtPeriode2.Enabled = sender.Checked
        btnPeriode1.Visible = sender.Checked : btnPeriode2.Visible = sender.Checked
        If sender.Checked Then
            txtPeriode1.CssClass = "inpText" : txtPeriode2.CssClass = "inpText"
        Else
            txtPeriode1.CssClass = "inpTextDisabled" : txtPeriode2.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub GVtrnjurnal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVtrnjurnal.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub GVtrnjurnal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVtrnjurnal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(6).Text = Format(CDate(e.Row.Cells(6).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub GVtrnjurnal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVtrnjurnal.SelectedIndexChanged
        Session("oid") = GVtrnjurnal.SelectedDataKey(0).ToString
        Response.Redirect("~/accounting/trninitCOA.aspx?mstoid=" & Session("oid"))
    End Sub

#End Region

    Protected Sub imbSave_Click(sender As Object, e As ImageClickEventArgs) Handles imbSave.Click
        UpdateDetailData()
        '==============================
        'Cek Peroide Bulanan Dari Crdgl
        '==============================
        'If ToDouble(amount.Text) > 0 Then
        'CEK PERIODE AKTIF BULANAN
        sSql = "SELECT DISTINCT ISNULL(periodacctg,'') FROM QL_crdgl Where glflag <> 'CLOSE'"
        If GetStrData(sSql) <> "" Then
            sSql = "select distinct left(isnull(periodacctg,''),4)+'-'+substring(isnull(periodacctg,''),6,2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctg(toDate(gldate.Text)) < GetStrData(sSql) Then
                showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
      
        'End If
        'Dim CutOffDate As Date
        'Dim cod As String
        'sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND genother2 = (SELECT genoid FROM QL_mstgen WHERE gengroup = 'Cabang' AND gencode = '" & dd_branch.SelectedValue & "')"
        'If IsDBNull(cKon.ambilscalar(sSql)) Then
        '    showMessage("- Silakan atur tanggal Cut Off sebelum menggunakan form ini", CompnyName & "- WARNING", 2, "modalMsgBox")

        '    CutOffDate = "1/1/1900"
        '    Exit Sub
        'Else
        '    cod = cKon.ambilscalar(sSql)
        '    'cod = Replace(cod, "/", "-") #08/29/2008 7:27:15PM#
        '    cod = toDate(cod)
        '    CutOffDate = "#" & cod & " 7:27:15PM#"
        '    CutOffDate = FormatDateTime(CutOffDate, DateFormat.ShortDate)
        '    If Not IsDate(CutOffDate) Then
        '        showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & "- WARNING", 2, "modalMsgBox")
        '        Exit Sub
        '    End If
        'End If

        If Session("TblDtl") Is Nothing Then
            showMessage("Please view COA data first!", CompnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        End If
        Dim objTable As DataTable = Session("TblDtl")
        If objTable.Rows.Count <= 0 Then
            showMessage("Please view COA data first!", CompnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        End If

        Dim cRate As New ClassRate()
        cRate.SetRateValue(CInt(1), toDate(gldate.Text))

        Dim objView As DataView = objTable.DefaultView
        objView.RowFilter = "amount > 0"
        If objView.Count <= 0 Then
            objView.RowFilter = ""
            showMessage("Please fill Init Amount for some COA to be initiated!", CompnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        End If
        Dim periodacctg As String = GetDateToPeriodAcctg(toDate(gldate.Text))
        Dim glmstoid As Integer = CInt(GetStrData("SELECT ((CASE WHEN MIN(glmstoid) IS NULL THEN 0 WHEN MIN(glmstoid)>=1 THEN 0 ELSE MIN(glmstoid) END) - 1) AS Oid FROM QL_trnglmst"))
        Dim gldtloid As Integer = CInt(GetStrData("SELECT ((CASE WHEN MIN(gldtloid) IS NULL THEN 0 WHEN MIN(gldtloid)>=1 THEN 0 ELSE MIN(gldtloid) END) - 1) AS Oid FROM QL_trngldtl"))
        Dim crdgloid As Integer = CInt(GetStrData("SELECT ((CASE WHEN MIN(crdgloid) IS NULL THEN 0 WHEN MIN(crdgloid)>=1 THEN 0 ELSE MIN(crdgloid) END) - 1) AS Oid FROM QL_crdgl"))

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "INSERT INTO QL_trnglmst (cmpcode,branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type) VALUES ('" & CompnyCode & "','" & dd_branch.SelectedValue & "', '" & glmstoid & "', '" & CDate(toDate(gldate.Text)) & "', '" & periodacctg & "', 'COA Initial Balance', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            For C1 As Integer = 0 To objView.Count - 1
                sSql = "INSERT into QL_trngldtl (cmpcode,branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & CompnyCode & "','" & dd_branch.SelectedValue & "', " & gldtloid & ", " & C1 + 1 & ", " & glmstoid & ", " & objView(C1).Item("acctgoid") & ", '" & objView(C1).Item("acctgdbcr").ToString & "', " & ToDouble(objView(C1).Item("amount").ToString) & ", 'COA Initial Balance', 'COA Initial Balance', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objView(C1).Item("amount").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objView(C1).Item("amount").ToString) * cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                gldtloid -= 1
                ' Insert QL_crdgl
                sSql = "INSERT INTO QL_crdgl (cmpcode,branch_code, crdgloid, periodacctg, crdgldate, acctgoid, amtopen,amtopenidr, amtdebit, amtcredit, amtbalance,amtbalanceidr, amtopenusd, amtdebitusd, amtcreditusd, amtbalanceusd, postdate, glflag,gldesc, createuser, upduser, updtime) VALUES ('" & CompnyCode & "','" & dd_branch.SelectedValue & "', " & crdgloid & ", '" & periodacctg & "', '" & CDate(toDate(gldate.Text)) & "', " & objView(C1).Item("acctgoid") & ", " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyIDRValue & "," & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyIDRValue & ", 0, 0, " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyIDRValue & ", " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyIDRValue & ", " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyUSDValue & ", 0, 0, " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyUSDValue & ", CURRENT_TIMESTAMP, 'Open', 'COA Initial Balance', '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                crdgloid -= 1
            Next
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, CompnyName & "- ERROR", 1, "modalMsgBox")
            conn.Close()
            objView.RowFilter = ""
            Exit Sub
        End Try
        objView.RowFilter = ""
        Response.Redirect("~/accounting/trninitCOA.aspx?awal=true")
    End Sub

    Protected Sub imbCancel_Click(sender As Object, e As ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("~/accounting/trninitCOA.aspx?awal=true")
    End Sub

    Protected Sub imbPost_Click(sender As Object, e As ImageClickEventArgs) Handles imbPost.Click
        crdglflag.Text = "OPEN"
        imbSave_Click(sender, e)
    End Sub

    Protected Sub imbDelete_Click(sender As Object, e As ImageClickEventArgs) Handles imbDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_crdgl SET glflag='DELETE' WHERE branch_code='" & dd_branch.SelectedValue & "' AND crdgloid=" & crdgloid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        End Try
        Response.Redirect("trninitCOA.aspx?awal=true")
    End Sub

    Protected Sub imbFindBoM_Click(sender As Object, e As ImageClickEventArgs) Handles imbFindBoM.Click
        GVtrnjurnal.PageIndex = 0
        binddata()
    End Sub

    Protected Sub imbAllBoM_Click(sender As Object, e As ImageClickEventArgs) Handles imbAllBoM.Click
        cbBlmPosting.Checked = False
        txtPeriode1.Text = Format(Now, "01/MM/yyyy")
        txtPeriode2.Text = Format(Now, "dd/MM/yyyy")
        binddata()
    End Sub

    Protected Sub dd_branch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dd_branch.SelectedIndexChanged

        Dim cod As String = ""
        If IsPostBack Then
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND genother2 = (SELECT genoid FROM QL_mstgen WHERE gengroup = 'Cabang' AND gencode = '" & dd_branch.SelectedValue & "')"
            cod = cKon.ambilscalar(sSql)
            If cod = "" Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & "- WARNING", 2, "modalMsgBox")
                gldate.Text = Format(Now, "dd/MM/yyyy")

                Exit Sub
            Else
                gldate.Text = cod
            End If
        End If


    End Sub

    Protected Sub btnCheckAll_Click(sender As Object, e As ImageClickEventArgs) Handles btnCheckAll.Click
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(sender As Object, e As ImageClickEventArgs) Handles btnUncheckAll.Click
        UncheckAll()
    End Sub

    Protected Sub btnPosting_Click(sender As Object, e As ImageClickEventArgs) Handles btnPosting.Click
        Dim sErrorku As String = ""
        If Not Session("TblGL") Is Nothing Then
            Dim dt As DataTable = Session("TblGL")
            If dt.Rows.Count > 0 Then
                UpdateCheckedPost()
                Dim dv As DataView = dt.DefaultView

                dv.RowFilter = "selected=1"
                If dv.Count = 0 Then
                    sErrorku &= "Please select COA Balance first!"
                End If
                dv.RowFilter = ""
                dv.RowFilter = "selected=1 AND glflag='OPEN'"
                If dv.Count > 0 Then
                    sErrorku &= "COA Balance have been POSTED before!"
                End If
                dv.RowFilter = ""

                If sErrorku <> "" Then

                    showMessage(sErrorku, CompnyName & "- WARNING", 2, "modalMsgBox")
                    dv.RowFilter = ""
                    Exit Sub
                End If

                Dim parsingOID As Integer = 0
                Dim sCmpcode As String = ""
                Dim dtAwal As DataTable = Session("TblGL")
                Dim dvAwal As DataView = dtAwal.DefaultView
                Dim iSeq As Integer = dvAwal.Count + 1
                dvAwal.AllowEdit = True
                dvAwal.AllowNew = True
                dv.RowFilter = "selected=1 AND glflag='In Process'"
                'poststatx.Text = "SPECIAL"

                Dim objTrans As SqlClient.SqlTransaction
                Dim objCmd As New SqlClient.SqlCommand
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                objCmd.Connection = conn
                objCmd.Transaction = objTrans

                Try
                    Dim seq As Integer = 0
                    For C1 As Integer = 0 To dv.Count - 1
                        parsingOID = dv(C1)("crdgloid").ToString
                        sCmpcode = dv(C1)("cmpcode").ToString

                        ' UPDATE
                        crdglflag.Text = "OPEN"
                        Dim postDate As String = IIf(crdglflag.Text = "OPEN", "CURRENT_TIMESTAMP", "'01/01/1900'")

                        sSql = "UPDATE QL_crdgl SET postdate=" & postDate & ",crdglgroup='MONTHLY',glflag='" & crdglflag.Text & "', upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE branch_code ='" & dd_branch.SelectedValue & "' AND crdgloid=" & parsingOID
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        If Not (Session("errmsg") Is Nothing Or Session("errmsg") = "") Then
                            sErrorku &= "Can't POST Cash Bank No " & dv(C1)("cashbankno").ToString & " with reason:<BR>" & Session("errmsg")
                        End If
                        seq = C1
                    Next

                    objTrans.Commit()
                    conn.Close()

                    If seq = dv.Count - 1 Then
                        showMessage("Data Telah di Posting <BR> ", CompnyName & "- INFORMATION", 2, "modalMsgBox")
                        Response.Redirect("trninitCOA.aspx?awal=true")
                    End If

                Catch ex As Exception
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.Message, CompnyName & "- INFORMATION", 2, "modalMsgBox")
                    Exit Sub
                End Try
            End If
        End If
    End Sub

    Protected Sub btnMsgBoxOK_Click(sender As Object, e As ImageClickEventArgs) Handles btnMsgBoxOK.Click

    End Sub

    Protected Sub dd_branchCari_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dd_branchCari.SelectedIndexChanged
        GVtrnjurnal.PageIndex = 0
        binddata()
    End Sub

    Protected Sub crdglnote_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If crdglnote.MaxLength >= "200" Then
            showMessage("Max 200 kata !! <BR> ", CompnyName & "- INFORMATION", 2, "modalMsgBox")
        End If
    End Sub
End Class
