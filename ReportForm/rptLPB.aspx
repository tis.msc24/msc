<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptLPB.aspx.vb" Inherits="ReportForm_frmpostat" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table width="100%">
        <tr>
            <td align="center">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="center" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: REPORT PDO :."></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: transparent" valign="center">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD id="TD7" align=left runat="server" Visible="false">Type</TD><TD id="TD9" runat="server" Visible="false">:</TD><TD id="TD8" align=left runat="server" Visible="false"><asp:DropDownList id="ddlsumdetail" runat="server" Width="79px" CssClass="inpText" __designer:wfdid="w62"><asp:ListItem Value="Detail">Detail</asp:ListItem>
<asp:ListItem>Summary</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Periode</TD><TD>:</TD><TD align=left><asp:TextBox id="date1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w63"></asp:TextBox> <asp:ImageButton id="search1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w64"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w65"></asp:Label> <asp:TextBox id="date2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w66"></asp:TextBox> <asp:ImageButton id="search2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w67"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(dd/mm/yyyy)" __designer:wfdid="w68"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label5" runat="server" Width="42px" Text="Cabang" __designer:wfdid="w69"></asp:Label></TD><TD><asp:Label id="Label6" runat="server" Text=":" __designer:wfdid="w70"></asp:Label></TD><TD align=left><asp:DropDownList id="BranchDDL" runat="server" Width="157px" CssClass="inpText" __designer:wfdid="w71" OnSelectedIndexChanged="dView_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Text="PIC" __designer:wfdid="w72"></asp:Label></TD><TD><asp:Label id="Label8" runat="server" Text=": " __designer:wfdid="w73"></asp:Label></TD><TD align=left><asp:DropDownList id="pic" runat="server" Width="157px" CssClass="inpText" __designer:wfdid="w74"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Supplier</TD><TD>:</TD><TD align=left><asp:TextBox id="custcode" runat="server" Width="178px" CssClass="inpTextDisabled" __designer:wfdid="w75" Enabled="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w76"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w77"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="No. PO" __designer:wfdid="w78"></asp:Label></TD><TD><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w79"></asp:Label></TD><TD align=left><asp:TextBox id="dono" runat="server" Width="178px" CssClass="inpTextDisabled" __designer:wfdid="w80" Enabled="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindDO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w81"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseDO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w82"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="LblPDO" runat="server" Width="52px" Text="No. PDO" __designer:wfdid="w83"></asp:Label></TD><TD><asp:Label id="lblPDO6" runat="server" Text=":" __designer:wfdid="w84"></asp:Label></TD><TD align=left><asp:TextBox id="sono" runat="server" Width="178px" CssClass="inpTextDisabled" __designer:wfdid="w85" Enabled="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w86"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w87"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label9" runat="server" Width="88px" Text="Katalog" __designer:wfdid="w88"></asp:Label></TD><TD><asp:Label id="Label10" runat="server" Text=":" __designer:wfdid="w89"></asp:Label></TD><TD align=left><asp:TextBox id="itemcode" runat="server" Width="178px" CssClass="inpTextDisabled" __designer:wfdid="w90" Enabled="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w91"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w92"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="BtnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w93"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnViewPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w94"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnViewExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w95"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w96"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w97" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w98"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w99" TargetControlID="Date1" PopupButtonID="search1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w100" TargetControlID="Date2" PopupButtonID="search2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w101" TargetControlID="date1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w102" TargetControlID="date2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w103" AutoDataBind="true"></CR:CrystalReportViewer> 
</ContentTemplate>
                    <Triggers>
<asp:PostBackTrigger ControlID="BtnViewPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="BtnViewExcel"></asp:PostBackTrigger>
</Triggers>
                </asp:UpdatePanel><asp:UpdatePanel id="upListCust" runat="server"><contenttemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="DDLFilterListCust" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="custname">Supplier</asp:ListItem>
<asp:ListItem Value="Custcode">Kode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListCust" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="98%" ForeColor="#333333" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="custoid,custcode,custname" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                    <asp:CheckBox ID="cbLMCust" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>' />
                                                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListCust" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListCust" PopupDragHandleControlID="lblListCust">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="upListDO" runat="server"><contenttemplate>
<asp:Panel id="PanelListDO" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListDO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Nota PO"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListDO" runat="server" Width="100%" DefaultButton="btnFindListDO"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListDO" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="doitemno">No. SI</asp:ListItem>
<asp:ListItem Value="doitemmstoid">No. Draft</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListDO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListDO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListDO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllDO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneDO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedDO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListDO" runat="server" Width="99%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                        <asp:CheckBox ID="cbLMDO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("doitemmstoid") %>' />
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="doitemmstoid" HeaderText="No. Draft">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemno" HeaderText="No. PO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Supplier" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                &nbsp;
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListDO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListDO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDO" runat="server" TargetControlID="btnHiddenListDO" PopupDragHandleControlID="lblListDO" PopupControlID="PanelListDO" BackgroundCssClass="modalBackground">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListDO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="upListSO" runat="server"><contenttemplate>
<asp:Panel id="PanelListSO" runat="server" Width="1000px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of PDO"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="Label241" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="trnjualno">No. PDO</asp:ListItem>
<asp:ListItem Value="soitemno">No. PO</asp:ListItem>
<asp:ListItem Enabled="False" Value="soitemmstoid">No. Draft</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" ForeColor="#333333" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                        <asp:CheckBox ID="cbLMSO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("soitemmstoid") %>' />
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="soitemmstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemno" HeaderText="No. PDO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodate" HeaderText="Tgl. PDO">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                &nbsp;
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListSO" BackgroundCssClass="modalBackground">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="upListMat" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="itemlongdesc">Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3>Jenis Barang : <asp:DropDownList id="JenisBarangDDL" runat="server" CssClass="inpText" Font-Size="Small">
                                                                            <asp:ListItem>ALL</asp:ListItem>
                                                                            <asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
                                                                            <asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
                                                                            <asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
                                                                            <asp:ListItem>ASSET</asp:ListItem>
                                                                        </asp:DropDownList></TD></TR></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" Width="96%" ForeColor="#333333" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemcode" AllowPaging="True">
                                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                            <Columns>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Strikeout="False" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="itemlongdesc" HeaderText="Katalog">
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="unit" HeaderText="Unit">
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="JenisNya" HeaderText="Jenis">
                                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                                HorizontalAlign="Right" />
                                                            <EmptyDataTemplate>
                                                                &nbsp;
                                                            </EmptyDataTemplate>
                                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                        </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" Drag="True">
                                        </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" DropShadow="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel></th>
        </tr>
    </table>
            </td>
        </tr>
    </table>
</asp:Content>

