<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnReturAP.aspx.vb" Inherits="Accounting_trnReturAP" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Koreksi Pelunasan Hutang"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                    <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel3" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 73px" align=left>Filter</TD><TD align=left colSpan=6><asp:DropDownList id="ddlFilter" runat="server" Width="110px" CssClass="inpText">
<asp:ListItem Value="cashbankno">Pay AP No.</asp:ListItem>
<asp:ListItem Value="s.suppname">Supplier</asp:ListItem>
<asp:ListItem Value="p.Payrefno">Payrefno</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 73px" align=left>Periode</TD><TD align=left colSpan=6><asp:TextBox id="txtPeriode1" runat="server" Width="80px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="X-Small" Text="to"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="80px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 73px" align=left>Status</TD><TD align=left colSpan=6><asp:DropDownList id="postinge" runat="server" Width="110px" CssClass="inpText">
<asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value=" ">In Process</asp:ListItem>
<asp:ListItem Value="POST">POST</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="ddlpaytype" runat="server" Width="55px" CssClass="inpText" Visible="False">
<asp:ListItem Text="CASH" Value="K"></asp:ListItem>
<asp:ListItem Text="NONCASH" Value="BB"></asp:ListItem>
<asp:ListItem Value="G">GIRO</asp:ListItem>
<asp:ListItem Value="D">DP</asp:ListItem>
<asp:ListItem Value="C">CREDIT CARD</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 73px" align=left></TD><TD align=left colSpan=6><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:DropDownList id="orderby" runat="server" Width="105px" CssClass="inpText" Visible="False"><asp:ListItem Value="cashbankno desc">Pay AP No(Z-A)</asp:ListItem>
<asp:ListItem Value="cashbankno">Pay AP No(A-Z)</asp:ListItem>
<asp:ListItem Value="cashbankdate desc">Pay Date(Z-A)</asp:ListItem>
<asp:ListItem Value="cashbankdate">Pay Date(A-Z)</asp:ListItem>
<asp:ListItem Value="suppname desc">Supplier(Z-A)</asp:ListItem>
<asp:ListItem Value="suppname">Supplier(A-Z)</asp:ListItem>
</asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 73px" align=left></TD><TD align=left colSpan=6><ajaxToolkit:CalendarExtender id="ce4" runat="server" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy" TargetControlID="txtperiode1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce5" runat="server" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy" TargetControlID="txtperiode2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee3" runat="server" TargetControlID="txtPeriode2" CultureName="id-ID" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee4" runat="server" TargetControlID="txtPeriode1" CultureName="id-ID" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=7><asp:GridView id="GVmstPAYAP" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" EnableModelValidation="True" OnRowCommand="gridCommand" OnPageIndexChanging="GVmstPAYAP_PageIndexChanging" OnSelectedIndexChanged="GVmstPAYAP_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankno" GridLines="None">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="No" SortExpression="cashbankno"><ItemTemplate>
<asp:HyperLink ID="HyperLink1" runat="server" 
            NavigateUrl='<%# Eval("cashbankoid", "trnReturAP.aspx?oid={0}") %>' 
            Text='<%# Eval("cashbankno") %>'></asp:HyperLink>                                                                   
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>

<ItemStyle ForeColor="Navy" HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppliername" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left" Width="150px" Wrap="True" />
</asp:BoundField>
<asp:BoundField DataField="date" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Apno" HeaderText="Nomer AP">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Amount">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right" 
        Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Catatan">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField ButtonType="Image" SelectImageUrl="~/Images/print.gif" 
        ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Red" HorizontalAlign="Right"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Data tidak ditemukan !!</asp:Label>     
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" HorizontalAlign="Center"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"></TR></TBODY></TABLE></asp:Panel> <asp:Label id="Label13" runat="server" Font-Bold="True" Text="Grand Total : Rp." Visible="False"></asp:Label> <asp:Label id="lblgrandtotal" runat="server" Font-Bold="True" Visible="False">0.00</asp:Label> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="GVmstPAYAP"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span style="font-size: 9pt"><strong> Daftar Pelunasan Hutang :.</strong></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>Form Pelunasan Hutang </strong><span style="font-size: 9pt">
                                <strong>:.</strong></span></span>&nbsp;</HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><asp:Label id="Label1" runat="server" Font-Bold="True" ForeColor="Black" Text="Informasi :" Font-Underline="True"></asp:Label><BR /><TABLE width="100%"><TBODY><TR><TD class="Label" align=left><asp:Label id="cashbankoid" runat="server" Width="50px" Visible="False"></asp:Label></TD><TD style="HEIGHT: 10px" class="Label" align=left>&nbsp;<asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Data" Visible="False"></asp:Label> <asp:Label id="CutofDate" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; HEIGHT: 10px" class="Label" align=left><asp:Label id="lblPOST" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3>&nbsp;<asp:Label id="cashbankno" runat="server" Visible="False"></asp:Label> <asp:Label id="lblcabang" runat="server"></asp:Label> </TD></TR><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left><asp:DropDownList style="HEIGHT: 17px" id="ddlcabang" runat="server" Width="101px" CssClass="inpText" AutoPostBack="True" Enabled="true">
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="NoBukti" runat="server" Width="54px" Text="No. Bukti"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="defcbno" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False" MaxLength="20"></asp:TextBox></TD><TD></TD><TD></TD></TR><TR><TD class="Label" align=left>Supplier</TD><TD class="Label" align=left><asp:TextBox id="suppnames" runat="server" Width="170px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w6"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton> <asp:Label id="Label9" runat="server" Font-Bold="False" Visible="False" __designer:wfdid="w8">0</asp:Label></TD><TD class="Label" align=left>No. AP</TD><TD class="Label" align=left><asp:TextBox id="ApNo" runat="server" Width="123px" CssClass="inpText" MaxLength="20" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="SearchApNo" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseApNo" runat="server" ImageUrl="~/Images/erase.bmp" Height="16px" __designer:wfdid="w5"></asp:ImageButton> <asp:Label id="RetcbOidAp" runat="server" Text="0" Visible="False"></asp:Label></TD><TD></TD><TD></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:GridView id="GvRetAp" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="branch_code,cashbankoid,cashbankno,cashbankamount,cashbankgroup,cashbanktype,SuppName,PayAmt,cashbankacctgoid,trndpapoid" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Transaksi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanktype" HeaderText="Type">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="PayAmt" HeaderText="Amount">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankacctgoid" HeaderText="cashbankacctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label10" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD></TD><TD></TD></TR><TR><TD class="Label" align=left>Tipe Payment&nbsp;</TD><TD class="Label" align=left><asp:DropDownList id="payflag" runat="server" Width="101px" CssClass="inpText" AutoPostBack="True">
                            <asp:ListItem>CASH</asp:ListItem>
<asp:ListItem Value="NONCASH">NON CASH</asp:ListItem>
<asp:ListItem>VOUCHER</asp:ListItem>
<asp:ListItem>GIRO</asp:ListItem>
<asp:ListItem>DP</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left>Tanggal <asp:Label id="Label25" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="PaymentDate" runat="server" Width="75px" CssClass="inpTextDisabled" Enabled="False" OnTextChanged="PaymentDate_TextChanged"></asp:TextBox> <asp:ImageButton id="btnPayDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" Visible="False" Enabled="False" BorderColor="White"></asp:ImageButton> <asp:DropDownList id="CurrencyOid" runat="server" CssClass="inpTextDisabled" Visible="False" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD><asp:Label id="lblRealisasi" runat="server" Width="83px" Text="No. Realisasi" Visible="False"></asp:Label> <asp:Label id="AmtReal" runat="server" Text="AmtReal" Visible="False"></asp:Label></TD><TD><asp:TextBox id="RealNo" runat="server" Width="123px" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="sRealBtn" onclick="sRealBtn_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseV" onclick="EraseV_Click" runat="server" ImageUrl="~/Images/erase.bmp" Height="16px" Visible="False"></asp:ImageButton> <asp:Label id="RealOid" runat="server" Font-Bold="False" Visible="False">0</asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD colSpan=2><asp:GridView id="RealGv" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="realisasioid,realisasino,realisasiamt,realisasistatus,voucherno,voucheroid,voucherdesc,realisasitype,trnbelipono" GridLines="None" OnPageIndexChanging="RealGv_PageIndexChanging" OnRowDataBound="RealGv_RowDataBound" OnSelectedIndexChanged="RealGv_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="voucheroid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="realisasino" HeaderText="No. Realisasi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasiamt" HeaderText="Amount">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasitype" HeaderText="Type">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label10" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblpayduedate" runat="server" Width="70px" Text="Jth. Tempo " Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" Visible="False" BorderColor="White"></asp:ImageButton></TD><TD class="Label" align=left>COA <asp:Label id="lblPayType" runat="server" Text="Cash"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="cashbankacctgoid" runat="server" Width="304px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="cashbankacctgoid_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left>Total Payment</TD><TD class="Label" align=left><asp:TextBox id="amtbelinettodtl" runat="server" Width="111px" CssClass="inpTextDisabled" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lbldpno" runat="server" Text="DP No" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="trndpapoid" runat="server" Width="196px" CssClass="inpTextDisabled" Visible="False" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="trndpapoid_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lblpayrefno" runat="server" Width="46px" Text="Ref No." Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="payrefno" runat="server" Width="150px" CssClass="inpText" Visible="False" MaxLength="15"></asp:TextBox> <asp:TextBox id="code" runat="server" Width="99px" CssClass="inpTextDisabled" Visible="False" Enabled="False" MaxLength="30"></asp:TextBox> <asp:ImageButton id="creditsearch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False"></asp:ImageButton> <asp:ImageButton id="CREDITCLEAR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lbldpbalance" runat="server" Width="67px" Text="DP Balance" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="dpbalance" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False">0.00</asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Catatan</TD><TD class="Label" align=left colSpan=5><asp:TextBox id="cashbanknote" runat="server" Width="450px" CssClass="inpText" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:MaskedEditExtender id="meeCurrRate" runat="server" ErrorTooltipEnabled="True" InputDirection="RightToLeft" MaskType="Number" Mask="999,999,999.99" TargetControlID="currencyRate"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" MaskType="Date" Mask="99/99/9999" TargetControlID="PaymentDate" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce1" runat="server" TargetControlID="PaymentDate" PopupButtonID="btnPayDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce3" runat="server" TargetControlID="payduedate" PopupButtonID="btnDueDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" MaskType="Date" Mask="99/99/9999" TargetControlID="payduedate" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <asp:Label id="lblBankName" runat="server" Text="Bank Name" Visible="False"></asp:Label> <asp:Label id="lblnotice" runat="server" Font-Size="X-Small"></asp:Label> <asp:Label id="trnsuppoid" runat="server" Font-Size="X-Small" Visible="False"></asp:Label> <asp:TextBox id="currencyRate" runat="server" Width="100px" CssClass="inpText" Visible="False" AutoPostBack="True" OnTextChanged="currencyRate_TextChanged"></asp:TextBox>&nbsp; <asp:TextBox id="NetPayment" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" ReadOnly="True">0.0000</asp:TextBox> <asp:DropDownList id="ddlBankName" runat="server" Width="151px" CssClass="inpText" Visible="False"></asp:DropDownList> <asp:TextBox id="TotalCost" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" ReadOnly="True">0.0000</asp:TextBox></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD id="TD33" align=left runat="server" visible="false"><asp:Label id="Label3" runat="server" Font-Bold="True" ForeColor="Black" Text="Detail :" Font-Underline="True"></asp:Label> <asp:Label id="I_U2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" id="TD22" align=left runat="server" visible="false"><asp:Label id="trnbelimstoid" runat="server" Font-Size="X-Small" Font-Bold="False" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" id="TD55" align=left runat="server" visible="false">&nbsp;<asp:CheckBox id="CBTax" runat="server" Width="72px" Font-Size="X-Small" Visible="False" AutoPostBack="True" OnCheckedChanged="CBTax_CheckedChanged"></asp:CheckBox></TD><TD id="TD41" align=left runat="server" visible="false"><asp:Label id="Payseq" runat="server" Visible="False"></asp:Label> <asp:Label id="acctgoid" runat="server" Font-Bold="False" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: small; WIDTH: 131px" id="TD26" align=left runat="server" visible="true"><asp:Label id="invCurrOid" runat="server" Font-Bold="False"></asp:Label></TD><TD id="TD4" align=left colSpan=2 runat="server" visible="false"><asp:Label id="invCurrCode" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="TD37" align=left runat="server" visible="false">Nota No <asp:Label id="Label26" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" id="TD48" align=left runat="server" visible="false"><asp:TextBox id="trnbelino" runat="server" Width="123px" CssClass="inpTextDisabled" Enabled="False" MaxLength="20"></asp:TextBox> <asp:ImageButton id="btnSearchPurchasing" onclick="btnSearchPurchasing_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:Label id="OidPO" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" id="TD31" align=left runat="server" visible="false">Akun Hutang</TD><TD id="TD20" align=left runat="server" visible="false"><asp:TextBox id="APAcc" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD style="FONT-SIZE: small; WIDTH: 131px" id="TD38" align=left runat="server" visible="false"></TD><TD id="TD42" align=left colSpan=2 runat="server" visible="false"></TD></TR><TR><TD id="TD18" align=left runat="server" visible="false">Supplier</TD><TD id="TD29" align=left runat="server" visible="false"><asp:TextBox id="suppname" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False" MaxLength="20"></asp:TextBox></TD><TD id="TD6" align=left runat="server" visible="false">Invoice Currency</TD><TD id="TD52" align=left runat="server" visible="false"><asp:TextBox id="invCurrDesc" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD id="TD35" align=left runat="server" visible="false"></TD><TD id="TD46" align=left colSpan=2 runat="server" visible="false">Invoice Rate <asp:TextBox id="invCurrRate" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False">0</asp:TextBox></TD></TR><TR><TD id="TD15" align=left runat="server" visible="false">Inv.Amt Trans</TD><TD id="TD30" align=left runat="server" visible="false"><asp:TextBox id="amttrans" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False" MaxLength="100">0</asp:TextBox></TD><TD id="TD53" align=left runat="server" visible="false">Paid Amt</TD><TD id="TD3" align=left runat="server" visible="false"><asp:TextBox id="amtpaid" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False" MaxLength="100">0</asp:TextBox></TD><TD id="TD12" align=left runat="server" visible="false">Total retur</TD><TD id="TD16" align=left colSpan=2 runat="server" visible="false"><asp:TextBox id="amtretur" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False" MaxLength="100">0</asp:TextBox></TD></TR><TR><TD id="TD25" align=left runat="server" visible="false">A/P Balance</TD><TD id="TD24" align=left runat="server" visible="false"><asp:TextBox id="APAmt" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False" MaxLength="30">0</asp:TextBox></TD><TD id="TD28" align=left runat="server" visible="false">Total Bayar <asp:Label id="Label8" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD id="TD54" align=left runat="server" visible="false"><asp:TextBox id="amtpayment" runat="server" Width="121px" CssClass="inpText" AutoPostBack="True" MaxLength="10" OnTextChanged="amtpayment_TextChanged">0</asp:TextBox></TD><TD id="TD51" align=left runat="server" visible="false">Sub Total</TD><TD id="TD13" align=left colSpan=2 runat="server" visible="false"><asp:TextBox id="totalpayment" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False" MaxLength="30">0</asp:TextBox></TD></TR><TR><TD id="TD43" align=left runat="server" visible="false">Detail&nbsp;Note</TD><TD id="TD45" align=left colSpan=3 runat="server" visible="false"><asp:TextBox id="txtNote" runat="server" Width="503px" CssClass="inpText"></asp:TextBox></TD><TD id="TD39" align=left runat="server" visible="false"><asp:Label id="RefTypeTbl" runat="server" Visible="False"></asp:Label></TD><TD id="TD11" align=left colSpan=2 runat="server" visible="false"></TD></TR><TR><TD id="TD5" align=left runat="server" visible="false"><asp:CheckBox id="chkOther" runat="server" Width="136px" Text="Selisih Pembayaran" AutoPostBack="True" OnCheckedChanged="chkOther_CheckedChanged"></asp:CheckBox></TD><TD id="TD8" align=left runat="server" visible="false"><asp:DropDownList id="DDLOtherType" runat="server" Width="155px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="DDLOtherType_SelectedIndexChanged">
                    <asp:ListItem Value="+">Kelebihan Bayar</asp:ListItem>
<asp:ListItem Value="-">Kurang Bayar</asp:ListItem>
</asp:DropDownList></TD><TD id="TD2" align=left runat="server" visible="false">Selisih&nbsp;</TD><TD id="TD1" align=left runat="server" visible="false"><asp:TextBox id="otheramt" runat="server" Width="125px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False" OnTextChanged="otheramt_TextChanged"></asp:TextBox></TD><TD id="TD19" align=left runat="server" visible="false"><asp:Label id="lblTaxPct" runat="server" Text="Tax (%)" Visible="False"></asp:Label></TD><TD id="TD47" align=left colSpan=2 runat="server" visible="false"><asp:TextBox id="TaxAmount" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False" MaxLength="30" ReadOnly="True">0.00000</asp:TextBox> <asp:TextBox id="txtPaymentNo" runat="server" Width="25px" CssClass="inpText" Visible="False" MaxLength="10" ReadOnly="True"></asp:TextBox> </TD></TR><TR><TD id="TD44" align=left runat="server" visible="false"><asp:CheckBox id="cbDP" runat="server" Text="DP" Visible="False" AutoPostBack="True" OnCheckedChanged="DP_CheckedChanged"></asp:CheckBox></TD><TD id="TD50" align=left runat="server" visible="false"><asp:DropDownList id="ddlDPNo" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="ddlDPNo_SelectedIndexChanged"></asp:DropDownList></TD><TD id="TD49" align=left runat="server" visible="false"><asp:Label id="lblDPAmount" runat="server" Text="DP Amount" Visible="False"></asp:Label></TD><TD id="TD21" align=left runat="server" visible="false"><asp:TextBox id="DPAmt" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True">0.00</asp:TextBox> <asp:Label id="lblTaxAcc" runat="server" Text="Tax Account" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: small; WIDTH: 131px" id="TD9" align=left runat="server" visible="false"><asp:Label id="lblAmtTax" runat="server" Text="Tax Amount" Visible="False"></asp:Label></TD><TD id="TD34" align=left colSpan=2 runat="server" visible="false"><asp:TextBox id="trnTaxPct" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False" Enabled="False" MaxLength="20">0.00</asp:TextBox></TD></TR><TR><TD id="TD17" align=left colSpan=7 runat="server" visible="false"><asp:Label id="Label14" runat="server" Font-Bold="True" Text="Detail Selisih Bayar :" Font-Underline="True"></asp:Label><asp:DropDownList id="TaxAccount" runat="server" Width="100px" CssClass="inpText" Visible="False" OnSelectedIndexChanged="cashbankacctgoid_SelectedIndexChanged"></asp:DropDownList> <ajaxToolkit:FilteredTextBoxExtender id="ftx_payamt" runat="server" TargetControlID="amtpayment" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbdpPayment" runat="server" TargetControlID="DPAmt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteDtlselisih" runat="server" TargetControlID="amtdtlselisih" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD id="TD27" align=left runat="server" visible="false">Akun selisih bayar</TD><TD id="TD23" align=left colSpan=3 runat="server" visible="false"><asp:DropDownList id="otherAcctgoid" runat="server" Width="408px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD id="TD40" align=left runat="server" visible="false">Total selisih <asp:Label id="Label18" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD id="TD36" align=left colSpan=2 runat="server" visible="false"><asp:TextBox id="amtdtlselisih" runat="server" Width="125px" CssClass="inpText" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD id="TD14" vAlign=top align=left runat="server" visible="false">Note<asp:Label id="stateDtlSls" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Selisih" Visible="False"></asp:Label></TD><TD id="TD10" align=left colSpan=3 runat="server" visible="false"><asp:TextBox id="dtlnoteselisih" runat="server" Width="503px" CssClass="inpText"></asp:TextBox></TD><TD id="TD7" vAlign=top align=left colSpan=3 runat="server" visible="false"><asp:LinkButton id="lkbAddDtlSlisih" onclick="lkbAddDtlSlisih_Click" runat="server" Font-Bold="True" Visible="False">[Tambah Detail Selisih]</asp:LinkButton> <asp:LinkButton id="lkbClearDtlSlisih" onclick="lkbClearDtlSlisih_Click" runat="server" Font-Bold="True" Visible="False">[Batal]</asp:LinkButton> </TD></TR><TR><TD id="TD32" vAlign=top align=left colSpan=7 runat="server" visible="false"><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 150px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 117%"><asp:GridView id="gvDtlSelisih" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="selisihseq" GridLines="None" OnSelectedIndexChanged="gvDtlSelisih_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="White" HorizontalAlign="Center" 
        Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="selisihseq" HeaderText="No">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Center" 
        Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun selisih bayar">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="529px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtlselisih" HeaderText="Total">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right" 
        Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dtlnoteselisih" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl=" " DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Center" 
        Width="25px"></HeaderStyle>

<ItemStyle Font-Bold="True" Font-Size="Medium" ForeColor="Red" 
        HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle CssClass="gvhdr" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" 
        Text="Data selisih bayar tidak ditemukan !!">
</asp:Label>                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD id="TD56" vAlign=top align=right colSpan=13 runat="server" visible="false"><asp:ImageButton id="ibtn" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" AlternateText="Add to List"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=right colSpan=7><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 225px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 70%"><asp:GridView id="GVDtlPayAP" runat="server" Width="100%" Height="17px" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="payseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="101px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="COA">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="177px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invCurrCode" HeaderText="Inv. Currency" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invCurrRate" HeaderText="Inv. Rate" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Amt DN/CN" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtretur" HeaderText="Total Kembali" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Pembayaran">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paynote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Overline="False" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="RealOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="payres1" HeaderText="payres1" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data detail tidak ditemukan !!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="Label21" runat="server" Font-Bold="True" Text="Grand Total :  "></asp:Label>&nbsp;<asp:Label id="amtbelinettodtl4" runat="server" Font-Bold="True">0.0000</asp:Label> </FIELDSET> </TD></TR><TR><TD vAlign=top align=left colSpan=7>Last Updated By <asp:Label id="updUser" runat="server" Font-Bold="True"></asp:Label>&nbsp;On <asp:Label id="updTime" runat="server" Font-Bold="True"></asp:Label> </TD></TR><TR><TD vAlign=top align=left colSpan=7><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton style="WIDTH: 60px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnPosting2" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" AlternateText="Posting"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton style="WIDTH: 60px" id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" Visible="False"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=center colSpan=7><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w3" AssociatedUpdatePanelID="UpdatePanel11"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w5"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><asp:UpdatePanel id="UpdatePanel4s" runat="server"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Supplier"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLSuppID" runat="server" CssClass="inpText">
                                                                                    <asp:ListItem Value="suppCODE">Kode</asp:ListItem>
                                                                                    <asp:ListItem Selected="True" Value="suppNAME">Name</asp:ListItem>
                                                                                </asp:DropDownList> <asp:TextBox id="txtFindSuppID" runat="server" Width="121px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibtnSuppID" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAlls" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset7"><DIV id="Div5"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 91%"><asp:GridView id="gvSupplier" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID,Code,Name" GridLines="None" EmptyDataText="No data in database." AllowSorting="True">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="ID" HeaderText="suppOID" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="CODE" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Name" HeaderText="Nama">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="lblstatusdatasupp" runat="server" ForeColor="Red" Text="No Suppplier Data !" Visible="False"></asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSupp" onclick="CloseSupp_Click" runat="server" CausesValidation="False" Font-Bold="False">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender3s" runat="server" TargetControlID="hiddenbtn2s" PopupDragHandleControlID="lblSuppdata" PopupControlID="Panel1" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2s" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel6" runat="server"><ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPosting2" PopupDragHandleControlID="lblPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel7" runat="server"><ContentTemplate>
<asp:Panel id="Panel2" runat="server" Width="99%" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblPurcdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Akun Pembayaran(Nota)"></asp:Label></TD></TR><TR><TD align=center colSpan=3><asp:DropDownList id="FilterTxt" runat="server" CssClass="inpText"><asp:ListItem Value="trnbelino">No. Nota</asp:ListItem>
<asp:ListItem Value="trnbelinote">No. SJ (Note)</asp:ListItem>
</asp:DropDownList>&nbsp;: <asp:TextBox id="txtInputNotaBeli" runat="server" CssClass="inpText"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton id="imbFindInv" onclick="imbFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" AlternateText="Find"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbViewAllInv" onclick="imbViewAllInv_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" onclick="btnClearSupp_Click1" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" Visible="False"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=center colSpan=3><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 260px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset8"><DIV id="Div7"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 99%; HEIGHT: 87%"><asp:GridView id="gvPurchasing" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelino,suppname,trnbelidate,amttrans,amtpaid,acctgoid,trntaxpct,currencyoid,currencyrate,currencycode,currencydesc,amtretur,payduedate,reftype,AmtVo,trnbelipono" GridLines="None" OnSelectedIndexChanged="gvPurchasing_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="Nota. No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PreFixSupp" HeaderText="ID Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="AmtVo" HeaderText="Voucher">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelinote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Total bayar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtretur" HeaderText="Total retur">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="95px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimstoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Tidak ada data hutang !!</asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="ClosePurc" onclick="ClosePurc_Click" runat="server" CausesValidation="False" Font-Bold="True">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" TargetControlID="hiddenbtnpur" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel2" PopupDragHandleControlID="lblPurcdata"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtnpur" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel4" runat="server"><ContentTemplate>
<ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" TargetControlID="hiddenbtn2" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel1" PopupDragHandleControlID="lblSuppdata"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </asp:View> &nbsp;</asp:MultiView>&nbsp; 
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>


                        </ContentTemplate>

                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp;
</asp:Content>

