Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports ClassProcedure
Imports Koneksi
Imports System.Globalization
Imports System.IO
Imports System.Drawing

Partial Class Transaction_trnSalesReturn
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As ReportDocument
    Dim crate As ClassRate
#End Region

#Region "Procedure"
    Private Function setTabelSN() As DataTable
        Dim dtSN As New DataTable
        dtSN.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSN.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSN.Columns.Add("SN", Type.GetType("System.String"))
        Return dtSN
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub BindAllData(ByVal sWhere As String)

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sWhere &= " AND a.createuser='" & Session("UserID") & "'"
        End If

        If FCabangNya.SelectedValue <> "ALL" Then
            sWhere &= " AND a.branch_code='" & FCabangNya.SelectedValue & "'"
        End If
        sSql = "Select a.branch_code,a.trnjualreturmstoid, a.trnjualreturno,a.refnotaretur,a.orderno, a.trnjualdate, b.custname, a.trnjualstatus,cb.gendesc From QL_trnjualreturmst a inner join ql_mstcust b on a.cmpcode = b.cmpcode and a.trncustoid = b.custoid Inner Join QL_mstgen cb ON cb.cmpcode =a.cmpcode AND a.branch_code=cb.gencode AND cb.gengroup='CABANG' Where a.cmpcode = '" & CompnyCode & "'" & sWhere & " order by a.trnjualreturno"
        ViewState("salesretur") = Nothing
        ViewState("salesretur") = cKon.ambiltabel(sSql, "salesretur")
        GVTrn.DataSource = ViewState("salesretur")
        GVTrn.DataBind()
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FCabangNya, sSql)
            Else
                FillDDL(FCabangNya, sSql)
                FCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                FCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(FCabangNya, sSql)
            FCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            FCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub DDLBranch()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlFromcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlFromcabang, sSql)
            Else
                FillDDL(ddlFromcabang, sSql)
                'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
                'ddlFromcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(ddlFromcabang, sSql)
            'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
            'ddlFromcabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub initallddl()
        sSql = "SELECT a.genoid,a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' and a.genother6 IN ('UMUM','ECOMMERCE') AND b.gengroup = 'WAREHOUSE' AND a.cmpcode = '" & CompnyCode & "' Where a.genoid like '%" & itemloc.Text & "%' AND a.genother2 IN (Select genoid FROM QL_mstgen Where genoid=a.genother2 AND gencode='" & ddlFromcabang.SelectedValue & "') ORDER BY a.genoid"
        FillDDL(fromlocation, sSql)
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If
        fromlocation.SelectedIndex = 0
    End Sub

    Private Sub printinvoiceservices(ByVal name As String)
        'Try
        '    rpt = New ReportDocument
        '    rpt.Load(Server.MapPath("~\Report\Invoiceservices.rpt"))
        '    'cProc.SetDBLogonForReport(rpt)
        '    rpt.SetParameterValue("sWhere", " AND reqoid = " & Session("oid") & "")
        '    Response.Buffer = False
        '    Response.ClearHeaders()
        '    Response.ClearContent()
        '    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "INVSVC_" & name)
        '    rpt.Close() : rpt.Dispose()
        'Catch ex As Exception
        '    showMessage(ex.ToString, 2)
        'End Try
    End Sub

    Private Sub filltextbox(ByVal branch As String, ByVal id As Integer)
        Dim oid As Integer = 0 : ddlFromcabang.Enabled = False
        If Integer.TryParse(id, oid) = False Then
            Response.Redirect("trnSalesReturn.aspx?awal=true")
        Else
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "Select a.trnjualmstoid, a.branch_code, a.orderno, a.trnjualreturno, a.refSJjualno, a.refnotaretur,a.typeretur, a.trnjualdate, a.trncustoid, a.trncustname, a.trnpaytype, a.trnjualref, a.trnamtjual, a.trntaxpct, a.trnamttax, a.trnamtjualnetto, a.trnjualnote, a.trnjualstatus, b.itemloc, (select Case trnjualdate When '1/1/1900' then '1/1/2018' Else trnjualdate End from QL_trnjualmst j Where a.trnjualmstoid=j.trnjualmstoid AND j.branch_code=a.branch_code) jualdate, a.currencyoid, a.upduser, a.updtime, a.typeSO, a.salesoid spgoid, a.amtnettso, a.createtime from QL_trnjualreturmst a inner join QL_trnjualreturdtl b on a.branch_code=b.branch_code and a.trnjualreturmstoid=b.trnjualreturmstoid Where a.trnjualreturmstoid = " & oid & " And a.cmpcode = '" & CompnyCode & "' and a.branch_code='" & branch & "'"
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    DDLBranch()
                    ddlFromcabang.SelectedValue = xreader("branch_code").ToString
                    noreturn.Text = xreader("trnjualreturno")
                    returndate.Text = Format(xreader("trnjualdate"), "dd/MM/yyyy")
                    paytype.Text = xreader("trnpaytype")
                    initallddl()
                    'fromlocation.SelectedValue = xreader("itemloc")
                    trdate.Text = Format(xreader("jualdate"), "dd/MM/yyyy")
                    invoiceno.Text = xreader("refnotaretur").ToString
                    sjno.Text = xreader("refSJjualno")
                    sono.Text = xreader("orderno").ToString
                    JualMstOid.Text = xreader("trnjualmstoid")
                    lblcurr.Text = xreader("currencyoid")
                    typeSO.Text = xreader("typeSO").ToString
                    branch_code.Text = xreader("branch_code").ToString
                    If lblcurr.Text = "1" Then
                        txtcurr.Text = "Rupiah"
                    Else
                        txtcurr.Text = "D0llar"
                    End If
                    spgoid.Text = Integer.Parse(xreader("spgoid"))
                    custname.Text = xreader("trncustname").ToString
                    custoid.Text = Integer.Parse(xreader("trncustoid"))
                    noref.Text = xreader("trnjualref").ToString
                    grossreturn.Text = ToMaskEdit(xreader("trnamtjual"), 3)
                    taxpct.Text = ToMaskEdit(xreader("trntaxpct"), 3)
                    taxamount.Text = xreader("trnamttax")
                    totalamount.Text = ToMaskEdit(xreader("trnamtjualnetto"), 3)
                    status.Text = xreader("trnjualstatus")
                    note.Text = xreader("trnjualnote")
                    upduser.Text = xreader("upduser")
                    updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt")
                    DDLtype.SelectedValue = xreader("typeretur")
                    NettSO.Text = ToMaskEdit(xreader("amtnettso"), 3)
                    createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                End While
            End If
            xreader.Close()
            conn.Close()

            sSql = "Select b.branch_code, a.trnjualdtlseq seq, c.itemcode, c.itemdesc itemname, a.trnjualdtlqty qty, d.gendesc unit, a.trnjualdtlprice price, a.trnjualnote note, c.itemoid, a.amtjualnetto netamt, a.trnjualdtlunitoid unitoid, a.unitSeq unitseq, a.trnjualdtloid, a.trnjualdtlqty maxqty, (select y.gendesc + ' - ' + x.gendesc from ql_mstgen x inner join ql_mstgen y on x.cmpcode = y.cmpcode and x.genother1 = y.genoid and x.gengroup = 'location' and x.genother6 IN ('UMUM','ECOMMERCE') and y.gengroup = 'WAREHOUSE' and x.cmpcode = a.cmpcode and x.genoid = a.itemloc) location, a.itemloc locationoid, a.trnjualdtldiscqty promodisc, a.amtjualdisc promodiscamt, a.qtyso, ISNULL(so.promooid,0) promooid, ISNULL(p.promtype,'-') PromoType from QL_trnjualreturdtl a inner join QL_trnjualreturmst b on a.cmpcode = b.cmpcode and a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b.branch_code inner join QL_mstItem c on a.cmpcode = c.cmpcode and a.itemoid = c.itemoid inner join QL_mstgen d on a.cmpcode = d.cmpcode and a.trnjualdtlunitoid = d.genoid INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid=b.trnjualmstoid AND b.branch_code=jm.branch_code LEFT JOIN QL_trnordermst so ON so.ordermstoid=jm.ordermstoid AND so.branch_code=jm.branch_code LEFT JOIN QL_mstpromo p ON promoid=so.promooid Where a.cmpcode = '" & CompnyCode & "' And a.trnjualreturmstoid = " & oid & " AND b.branch_code='" & branch_code.Text & "'"
            ViewState("dtlsalesreturn") = cKon.ambiltabel(sSql, "dtlsalesreturn")
            GVDtl.DataSource = ViewState("dtlsalesreturn")
            GVDtl.DataBind() : calculateheader()

            If status.Text.ToUpper = "APPROVED" Then
                ibcancel.Visible = True : ibsave.Visible = False
                ibdelete.Visible = False : ibapproval.Visible = False
                ibposting.Visible = False : btnPrint.Visible = True
            ElseIf status.Text.ToUpper = "REJECTED" Or status.Text.ToUpper = "IN APPROVAL" Then
                ibcancel.Visible = True : ibsave.Visible = False
                ibdelete.Visible = False : ibapproval.Visible = False
                ibposting.Visible = False : btnPrint.Visible = False
            Else
                ibcancel.Visible = True : ibsave.Visible = True
                ibdelete.Visible = True : ibapproval.Visible = True
                ibposting.Visible = False : btnPrint.Visible = False
            End If

            If typeSO.Text = "Konsinyasi" Then
                GVDtl.Columns(4).Visible = True
                LblQtySO.Visible = True : Label3.Visible = True
                QtySO.Visible = True : LblNettSO.Visible = True
                Label4.Visible = True : NettSO.Visible = True
            Else
                GVDtl.Columns(4).Visible = False
                LblQtySO.Visible = False : Label3.Visible = False
                QtySO.Visible = False : LblNettSO.Visible = False
                Label4.Visible = False : NettSO.Visible = False
            End If

        End If
    End Sub

    Private Sub inittax()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "select genother1 from ql_mstgen where gengroup = 'tax' and cmpcode = '" & CompnyCode & "'"
        xCmd.CommandText = sSql
        taxpct.Text = ToDouble(xCmd.ExecuteScalar)

        conn.Close()
    End Sub

    Private Sub BindCust()
        Try 
            sSql = "select distinct b.trncustoid, a.custcode, b.trncustname From QL_mstcust a inner join QL_trnjualmst b on a.cmpcode=b.cmpcode and a.custoid = b.trncustoid where b.trncustname like '%" & Tchar(custname.Text) & "%' AND b.branch_code='" & ddlFromcabang.SelectedValue & "'"
            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " AND b.createuser='" & Session("UserID") & "'"
            End If
            sSql &= "Order By custcode"
            Session("Cust") = cKon.ambiltabel(sSql, "Ql_mstcust")
            gvCustomer.DataSource = Session("Cust")
            gvCustomer.DataBind() : gvCustomer.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindItem()
        Try
            If Integer.Parse(JualMstOid.Text) < 0 Then
                sSql = "Select top 100 c.itemoid, c.satuan1, 0 unitseq, c.itemdesc, c.itemcode, c.Merk, 0.00 trnjualdtlqty, 0.00 Sisaqty, d.gendesc unit, 0 trnjualdtloid, case c.pricelist when 0 then 1 else c.pricelist end trnjualdtlprice,0.00 promodisc ,0.00 promodiscamt, 0 itemloc, 0.00 QtyRet, 0 promooid, '-' PromType From ql_mstitem c inner join QL_mstgen d on c.cmpcode=d.cmpcode and c.satuan1 = d.genoid AND d.gengroup='ITEMUNIT' Where (c.itemdesc LIKE '%" & Tchar(itemname.Text) & "%' OR c.itemcode LIKE '%" & Tchar(itemname.Text) & "%' OR Merk LIKE '%" & Tchar(itemname.Text) & "%')"
            Else
                sSql = "Select a.itemoid, a.trnjualdtlunitoid satuan1, a.unitseq, c.itemdesc, c.itemcode, c.Merk,a.trnjualdtlqty, (a.trnjualdtlqty - ISNULL(QtyRet,0.00)) Sisaqty, d.gendesc unit, a.trnjualdtloid, case a.trnjualdtlprice when 0.00 then 1 else a.trnjualdtlprice end trnjualdtlprice, a.trnjualdtldiscqty promodisc, CEILING((a.amtjualdisc/a.trnjualdtlqty)*(a.trnjualdtlqty - a.trnjualdtlqty_retur)) promodiscamt, a.itemloc, ISNULL(QtyRet,0.00) QtyRet, so.promooid, ISNULL(p.promtype,'-') PromType From ql_trnjualdtl a inner join ql_mstitem c on a.cmpcode = c.cmpcode and a.itemoid = c.itemoid Inner join QL_mstgen d on a.cmpcode = d.cmpcode and a.trnjualdtlunitoid = d.genoid AND d.gengroup='ITEMUNIT' Left Join (Select rm.trnjualmstoid, rm.branch_code, Isnull(SUM(rd.trnjualdtlqty),0.00) QtyRet, rd.trnjualdtloid, rd.itemoid From QL_trnjualreturdtl rd Inner JOin QL_trnjualreturmst rm ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rd.branch_code=rm.branch_code Where rm.trnjualstatus<>'Rejected' Group BY rm.trnjualmstoid, rm.branch_code, rd.trnjualdtloid, rd.itemoid) re ON re.trnjualmstoid=a.trnjualmstoid AND re.branch_code=a.branch_code AND re.trnjualdtloid=a.trnjualdtloid AND re.itemoid=a.itemoid Inner Join QL_trnjualmst jm ON jm.trnjualmstoid=a.trnjualmstoid AND a.branch_code=jm.branch_code Inner Join QL_trnordermst so ON jm.ordermstoid=so.ordermstoid AND so.branch_code=jm.branch_code AND jm.trnjualmstoid=a.trnjualmstoid AND a.branch_code=jm.branch_code LEFT JOIN QL_mstpromo p ON promoid=so.promooid Where a.trnjualmstoid ='" & JualMstOid.Text & "'"
            End If

            Dim Dr As DataTable = cKon.ambiltabel(sSql, "itemsalesreturn")
            GVItem.DataSource = Dr
            GVItem.DataBind() : GVItem.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindDataSI()
        If custoid.Text = "" Then
            showMessage("Please choose Customer!", 2)
            Exit Sub
        End If

        Dim lvl As String = "", sWh As String = ""
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            lvl &= " AND a.upduser='" & Session("UserID") & "'"
        End If

        If DDLtype.SelectedValue = 4 Then
            sWh = " AND ISNULL(a.trnamtjualnettoidr,0.00)=ISNULL(a.accumpaymentidr,0.00)"
        Else
            sWh = "AND ISNULL(a.trnamtjualnettoidr,0.00)<>ISNULL(a.accumpaymentidr,0.00)"
        End If

        Try
            sSql = "Select a.trnjualmstoid, a.branch_code, a.trnjualno, a.orderno, a.trncustoid, a.trnsjjualno, a.currencyoid, a.trnjualdate, c.custoid, c.custname, a.trnpaytype, a.trnamttax, a.trncustname, (Select DISTINCT so.typeSO From QL_trnordermst so Where so.orderno=a.orderno AND so.branch_code=a.branch_code AND so.trncustoid=a.trncustoid AND so.ordermstoid=a.ordermstoid) typeSO, a.spgoid, a.trnamtjualidr, Isnull(QtyRet,0.00) QtyRet, (Select SUM(jd.trnjualdtlqty) from QL_trnjualdtl jd Where jd.trnjualmstoid=a.trnjualmstoid) trnjualdtlqty From ql_trnjualmst a inner join QL_mstcust c on a.cmpcode = c.cmpcode and a.trncustoid = c.custoid Left Join (Select rm.trnjualmstoid,rm.branch_code,ISnull(SUM(rd.trnjualdtlqty),0.00) QtyRet from QL_trnjualreturdtl rd Inner JOin QL_trnjualreturmst rm ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rd.branch_code=rm.branch_code Group BY rm.trnjualmstoid,rm.branch_code) re ON re.trnjualmstoid=a.trnjualmstoid AND re.branch_code=a.branch_code Where a.cmpcode = '" & CompnyCode & "' AND a.trnjualmstoid>0 and a.trnjualno like '%" & Tchar(invoiceno.Text) & "%' and c.custoid = '" & custoid.Text & "' and a.trnjualstatus in ('POST') AND a.branch_code='" & ddlFromcabang.SelectedValue & "' " & lvl & " " & sWh & _
                  " UNION ALL " & _
                  "Select a.trnjualmstoid, a.branch_code, a.trnjualno, a.orderno, a.trncustoid, a.trnsjjualno, a.currencyoid, Case a.trnjualdate When '1/1/1900' then '01/01/2018' Else trnjualdate End trnjualdate, c.custoid, c.custname, a.trnpaytype, a.trnamttax, a.trncustname, 'User' typeSO, a.spgoid, a.trnamtjualidr-ISNULL((Select SUM(rm.trnamtjualnettoidr) From QL_trnjualreturmst rm Where rm.trnjualmstoid=a.trnjualmstoid AND a.branch_code=rm.branch_code AND rm.trnjualstatus='APPROVED'),0.00) trnamtjualidr, 0.00 QtyRet, 0.00 trnjualdtlqty From QL_trnjualmst a Inner Join QL_mstcust c ON c.custoid=a.trncustoid and a.branch_code=c.branch_code Where a.cmpcode = '" & CompnyCode & "' AND a.trnjualmstoid<0 and a.trnjualno like '%" & Tchar(invoiceno.Text) & "%' And c.custoid = '" & custoid.Text & "' and a.trnjualstatus in ('POST') AND a.branch_code='" & ddlFromcabang.SelectedValue & "' " & sWh & " ORDER BY trnjualno"
            ViewState("trnjualmst") = cKon.ambiltabel(sSql, "trsalesreturn")
            FillGV(GVTr, sSql, "trnjualmst")
            GVTr.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub GVTr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVTr.SelectedIndexChanged
        Try
            invoiceno.Text = GVTr.SelectedDataKey("trnjualno").ToString
            sono.Text = GVTr.SelectedDataKey("orderno").ToString
            sjno.Text = GVTr.SelectedDataKey("trnsjjualno").ToString
            trdate.Text = Format(GVTr.SelectedDataKey("trnjualdate"), "dd/MM/yyyy")
            custname.Text = GVTr.SelectedDataKey("custname").ToString
            taxamount.Text = GVTr.SelectedDataKey.Item("trnamttax")
            JualMstOid.Text = GVTr.SelectedDataKey.Item("trnjualmstoid")
            custoid.Text = GVTr.SelectedDataKey.Item("trncustoid")
            paytype.Text = GVTr.SelectedDataKey.Item("trnpaytype")
            lblcurr.Text = GVTr.SelectedDataKey.Item("currencyoid")
            typeSO.Text = GVTr.SelectedDataKey.Item("typeSO")
            branch_code.Text = GVTr.SelectedDataKey.Item("branch_code")
            spgoid.Text = Integer.Parse(GVTr.SelectedDataKey.Item("spgoid"))

            If Integer.Parse(JualMstOid.Text) < 0 Then
                NettSO.Text = ToMaskEdit(GVTr.SelectedDataKey("trnamtjualidr"), 3)
            Else
                NettSO.Text = ToMaskEdit(0, 3)
            End If

            If lblcurr.Text = 1 Then
                txtcurr.Text = "Rupiah"
            Else
                txtcurr.Text = "Dollar"
            End If

            If GVTr.SelectedDataKey("ordertaxamt") <> 0 Then
                inittax()
            Else
                taxpct.Text = "0.00"
            End If
            GVTr.Visible = False

            If typeSO.Text = "Konsinyasi" Then
                LblQtySO.Visible = True : Label3.Visible = True
                QtySO.Visible = True : LblNettSO.Visible = True
                Label4.Visible = True : NettSO.Visible = True
            Else
                LblQtySO.Visible = False : Label3.Visible = False
                QtySO.Visible = False : LblNettSO.Visible = False
                Label4.Visible = False : NettSO.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lbldate.Text = Format(GetServerTime(), "yyyy-MM-dd")
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        'Session.Timeout = 60

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Session("ApprovalLimit") = appLimit
            Response.Redirect("~\Transaction\trnSalesReturn.aspx")
        End If

        Page.Title = CompnyName & " - Sales Return"

        Session("sCmpcode") = Request.QueryString("branch_code")
        Session("oid") = Request.QueryString("oid")

        ibapproval.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin mengirimkan permintaan approval pada data ini?');")
        ibdelete.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin menghapus data ini?');")
        ibposting.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin melakukan posting data ini?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
            tbperiodstart.Text = Format(GetServerTime(), "01/MM/yyyy")
            tbperiodend.Text = Format(Date.Now, "dd/MM/yyyy")
            noreturn.Text = GenerateID("QL_trnjualreturmst", CompnyCode)
            InitDDLBranch() : DDLBranch() : initallddl()
            bindalldata(" And convert(date,a.trnjualdate,103) between '" & New Date(Date.Now.Year, Date.Now.Month, 1) & "' and '" & Date.Now & "'")

            If Session("oid") = "" Or Session("oid") Is Nothing Then
                TabContainer1.ActiveTabIndex = 0
                masterstate.Text = "new"
                returndate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                lblcreate.Text = "Create On"
            Else
                TabContainer1.ActiveTabIndex = 1
                masterstate.Text = "edit"
                filltextbox(Session("sCmpcode"), Session("oid"))
                lblcreate.Text = "Last Update By"
            End If
        End If
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("trnSalesReturn.aspx?awal=true")
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        Dim date1, date2 As New Date
        Dim sWhere As String = ""
        If tbperiodstart.Text.Trim = "" Or tbperiodend.Text.Trim = "" Then
            showMessage("All period cannot empty!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodstart.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("First period is not valid!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodend.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("second period is not valid!", 2)
            Exit Sub
        End If
        If CDate(toDate(tbperiodstart.Text)) > CDate(toDate(tbperiodend.Text)) Then
            showMessage("Last periode must be greather than First Periode!", 2)
            Exit Sub
        End If

        If CbPeriode.Checked = True Then
            sWhere &= " And convert(date,a.trnjualdate,103) between '" & date1 & "' and '" & date2 & "'"
        Else
            sWhere &= " And " & ddlfilter.SelectedValue.ToString & " Like '%" & Tchar(tbfilter.Text) & "%' " & IIf(ddlfilterstatus.SelectedValue.ToString <> "All", " and a.trnjualstatus = '" & ddlfilterstatus.SelectedValue.ToString & "'", "") & ""
        End If
        bindalldata(sWhere)
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        tbfilter.Text = ""
        'FCabangNya.SelectedValue = "ALL"
        ddlfilterstatus.SelectedIndex = 0
        CbPeriode.Checked = False
        bindalldata("")
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub ibapproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapproval.Click
        Dim sMsgApproval As String = ""

        sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnjualreturmst' and branch_code Like '%" & ddlFromcabang.SelectedValue & "%' order by approvallevel"
        Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
        If dtData2.Rows.Count > 0 Then
            Session("TblApproval") = dtData2
        Else
            sMsgApproval += "- Setting Approval Sales Return untuk Cabang ini belum ada, silahkan hubungi admin dahulu.<BR>"
        End If

        If sMsgApproval <> "" Then
            showMessage(sMsgApproval, 2)
            Exit Sub
        End If

        status.Text = "In Approval"
        ibsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub ibposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposting.Click
        status.Text = "POST"
        'If DDLtype.SelectedValue = 1 Then
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "Select ((Select SUM(dt.amtjualnetto) From QL_trnjualdtl dt Where trnjualmstoid=jm.trnjualmstoid And jm.branch_code=dt.branch_code) - accumpayment - abs(amtretur+" & ToDouble(totalamount.Text) & ")) hutang from ql_trnjualmst jm where cmpcode='" & CompnyCode & "' and trnjualmstoid=(select trnjualmstoid from ql_trnjualmst Where trnjualno='" & invoiceno.Text & "')"
        xCmd.CommandText = sSql : amtpiutang.Text = xCmd.ExecuteScalar
        conn.Close() : returamount.Text = totalamount.Text

        If ToDouble(amtpiutang.Text) > ToDouble(returamount.Text) Then
            amtpiutang.Text = ToDouble(returamount.Text) + (ToDouble(returpiutangamount.Text))
        ElseIf ToDouble(amtpiutang.Text) = ToDouble(returamount.Text) Then
            amtpiutang.Text = ToDouble(returamount.Text)
        ElseIf ToDouble(amtpiutang.Text) < 0 Then
            returpiutangamount.Text = (ToDouble(amtpiutang.Text) * -1)
            If returpiutangamount.Text < 0 Then
                returpiutangamount.Text = 0
            ElseIf ToDouble(returamount.Text) - ToDouble(returpiutangamount.Text) < 0 Then
                returpiutangamount.Text = ToDouble(returamount.Text)
            End If
            amtpiutang.Text = 0
        End If

        amtppn.Text = ToMaskEdit(ToDouble(taxamount.Text), 2)
        amtpotpenjualan.Text = amtdiscdtl.Text
        returamount.Text = ToDouble(returamount.Text) - ToDouble(amtppn.Text)
        If ToDouble(returpiutangamount.Text) = 0 Then
            amtpiutang.Text = (ToDouble(returamount.Text) + ToDouble(amtppn.Text) + (ToDouble(costamount.Text)))
        ElseIf ToDouble(returpiutangamount.Text) > 0 Then
            amtpiutang.Text = (ToDouble(returamount.Text) - ToDouble(returpiutangamount.Text) + ToDouble(amtppn.Text) + (ToDouble(costamount.Text)))
        End If
        returamount.Text = ToDouble(returamount.Text) + ToDouble(amtpotpenjualan.Text)
        ibsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub ibsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsave.Click
        Dim sMsg As String = ""
        If CDate(toDate(returndate.Text)) < CDate(toDate(trdate.Text)) Then
            sMsg &= "- Maaf, Tanggal retur tidak boleh kurang dari tanggal SI..!!<br>"
        End If

        Dim hppne As Decimal = 0.0
        If grossreturn.Text = "" Then sMsg &= "- Maaf, Amount Retur masih kosong..!!<br>"

        If ToDouble(grossreturn.Text) = 0.0 Then sMsg &= "- Maaf, Amount Retur masih kosong..!!<br>"

        If returndate.Text = "" Then sMsg &= "- Maaf, tanggal retur kosong..!!<br>"

        Dim returdate As New Date
        If Date.TryParseExact(returndate.Text, "dd/MM/yyyy", Nothing, Nothing, returdate) = False Then
            sMsg &= "- Maaf, Tanggal tidak valid..!!<br>"
        End If

        If returdate < New Date(1900, 1, 1) Then sMsg &= "- Maaf, tanggal kurang dari '1/1/1900'..!!<br>"
        If returdate > New Date(2079, 6, 6) Then sMsg &= "- Maaf, tanggal tidak boleh melebihi '6/6/2079'..!!<br>"
  
        If Not ViewState("dtlsalesreturn") Is Nothing Then
            Dim dtab As DataTable = ViewState("dtlsalesreturn")
            If dtab.Rows.Count <= 0 Then
                sMsg &= "- Maaf, Silahkan input detail Sales Return.<br>"
            End If
        Else
            sMsg &= "- Maaf, Silahkan input detail Sales Return.<br>"
        End If

        If Integer.Parse(JualMstOid.Text) < 0 Then
            If ToDouble(totalamount.Text) > ToDouble(NettSO.Text) Then
                sMsg &= "- Maaf Sekali, amount retur yang anda input " & ToMaskEdit(totalamount.Text, 4) & " melebihi amount SI " & ToMaskEdit(NettSO.Text, 4) & "!!<br>"
            End If
        End If

        If Integer.Parse(JualMstOid.Text) < 0 Then
            sSql = "Select Isnull(SUM(trnamtjualnettoidr),0.00)-Isnull(SUM(accumpaymentidr),0.00) From QL_trnjualmst Where trnjualmstoid=" & Integer.Parse(JualMstOid.Text) & " AND branch_code='" & ddlFromcabang.SelectedValue & "'"
            If DDLtype.SelectedValue = 4 And GetScalar(sSql) <> 0 Then
                sMsg &= "- Maaf, Anda tidak bisa pilih type retur " & DDLtype.SelectedItem.Text & " jika nota SI " & invoiceno.Text & " belum lunas..!!<br>"
            ElseIf DDLtype.SelectedValue = 1 And GetScalar(sSql) = 0 Then
                sMsg &= "- Maaf, Anda tidak bisa pilih type retur " & DDLtype.SelectedItem.Text & " jika nota SI " & invoiceno.Text & " sudah lunas..!!<br>"
            End If
        Else
            sSql = "Select Isnull(SUM(amttransidr),0.00)-Isnull(SUM(amtbayaridr),0.00) From QL_conar Where reftype NOT IN ('QL_TRNINVOICE','QL_trnbiayaeksmst')AND refoid=" & Integer.Parse(JualMstOid.Text) & " AND trnarstatus='POST' Group by refoid"
            If DDLtype.SelectedValue = 4 And GetScalar(sSql) <> 0 Then
                sMsg &= "- Maaf, Anda tidak bisa pilih type retur " & DDLtype.SelectedItem.Text & " jika nota SI " & invoiceno.Text & " belum lunas..!!<br>"
            ElseIf DDLtype.SelectedValue = 1 And GetScalar(sSql) = 0 Then
                sMsg &= "- Maaf, Anda tidak bisa pilih type retur " & DDLtype.SelectedItem.Text & " jika nota SI " & invoiceno.Text & " sudah lunas..!!<br>"
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trnjualreturmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "' AND branch_code='" & ddlFromcabang.SelectedValue & "'")) > 0 Then
                sMsg &= "- Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "select trnjualstatus from QL_trnjualreturmst Where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & " and branch_code='" & ddlFromcabang.SelectedValue & "'"
            Dim stat As String = GetStrData(sSql)
            If stat.ToLower = "post" Then
                sMsg &= "- Maaf, data sudah posting..!!<br />"
                status.Text = "In Process"
            ElseIf stat.ToLower = "in approval" Or stat.ToLower = "approved" Then
                sMsg &= "- Maaf, status transaksi sudah '" & stat & "', tekan Cancel dan cek kembali status transaksi ini..!!<br />"
                status.Text = "In Process"
            ElseIf stat Is Nothing Or stat = "" Then
                sMsg &= "- Maaf, Sales return tidak ada..!!<br>" 
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2) : status.Text = "In Process"
            Exit Sub
        End If

        ' Rate ini tidak dipakai
        Dim cRate As New ClassRate()
        cRate.SetRateValue(CInt(lblcurr.Text), toDate(trdate.Text))
        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            Dim period As String = GetDateToPeriodAcctg(returdate)
            Dim alastpaydate As New Date
            sSql = "Select genoid From ql_mstgen Where gengroup = 'PAYTYPE' and gendesc = 'cash' And cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = Integer.Parse(paytype.Text) Then
                alastpaydate = returdate
            Else
                alastpaydate = CDate("1/1/1900")
            End If
            '---- Generated ID tabel -----
            sSql = "Select lastoid From ql_mstoid where tablename = 'QL_trnjualreturmst' and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : Dim returmstoid As Integer = xCmd.ExecuteScalar + 1

            sSql = "Select lastoid From ql_mstoid Where tablename = 'QL_trnjualreturdtl' and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : Dim returdtloid As Integer = xCmd.ExecuteScalar

            sSql = "Select lastoid From ql_mstoid Where tablename = 'QL_Approval' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : Dim AppOid As Integer = xCmd.ExecuteScalar + 1

            Dim OidSO, OidDtlSO As Integer
            sSql = "Select lastoid From ql_mstoid where tablename = 'QL_trnorderdtl' and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : OidDtlSO = xCmd.ExecuteScalar + 1

            sSql = "Select lastoid From ql_mstoid where tablename = 'QL_trnordermst' and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : OidSO = xCmd.ExecuteScalar + 1
            '------------------------------

            If masterstate.Text = "new" Then
                If Not status.Text = "POST" Then
                    noreturn.Text = (returmstoid).ToString
                End If

                sSql = "insert into QL_trnjualreturmst (cmpcode,branch_code, trnjualreturmstoid, orderno, periodacctg, trnjualtype, trnjualreturno, refSJjualno, refnotaretur, trnjualdate, trnjualref, trncustoid, trncustname, trnpaytype, trnamtjual,trnamtjualidr,trnamtjualusd, trntaxpct, trnamttax,trnamttaxidr,trnamttaxusd, trndisctype, trndiscamt, trnamtjualnetto, trnamtjualnettoidr, trnamtjualnettousd, trnjualnote, trnjualstatus, trnjualres1, amtdischdr, amtdiscdtl, accumpayment, lastpaymentdate, userpay, createuser, upduser, updtime, refNotaRevisi,trnorderstatus, ekspedisioid, amtdischdr2, trndisctype2, trndiscamt2, postacctg, amtretur_piutang, amtpayretur_piutang, finalappovaldatetime, finalapprovaluser, finalapprovalcode, canceluser, canceltime, cancelnote,currencyoid,typeretur,typeSO,trnjualmstoid,salesoid,amtnettso,createtime) Values " & _
                "('" & CompnyCode & "','" & ddlFromcabang.SelectedValue & "', " & returmstoid & ", '" & sono.Text & "', '" & GetDateToPeriodAcctg(returdate) & "', 'GROSIR', '" & noreturn.Text & "', '" & sjno.Text & "', '" & invoiceno.Text & "', '" & returdate & "', '" & Tchar(noref.Text) & "', " & Integer.Parse(custoid.Text) & ", '" & Tchar(custname.Text) & "', " & Integer.Parse(paytype.Text) & ", " & ToDouble(grossreturn.Text) & ", " & ToDouble(grossreturn.Text) & ", " & ToDouble(grossreturn.Text) & ", " & ToDouble(taxpct.Text) & ", " & ToDouble(taxamount.Text) & ", " & ToDouble(taxamount.Text) & ", " & ToDouble(taxamount.Text) & ", 'AMT', 0.0, " & ToDouble(totalamount.Text) & ", " & ToDouble(totalamount.Text) & ", " & ToDouble(totalamount.Text) & ", '" & Tchar(note.Text) & "', '" & status.Text & "', '', 0.0, 0.0, 0.0, '" & alastpaydate & "', 0.0, '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', isnull((select trnorderstatus from QL_trnordermst Where orderno = '" & sono.Text & "' and cmpcode = '" & CompnyCode & "' AND ordermstoid=(SELECT ordermstoid FROM QL_trnjualmst WHere trnjualmstoid=" & Integer.Parse(JualMstOid.Text) & " AND branch_code='" & ddlFromcabang.SelectedValue & "')), ''), 0, 0.0, 'AMT', 0.0, '', 0.0, 0.0, '1/1/1900', '', '', '', '1/1/1900', '','" & lblcurr.Text & "','" & DDLtype.SelectedValue & "','" & typeSO.Text & "'," & Integer.Parse(JualMstOid.Text) & "," & Integer.Parse(spgoid.Text) & "," & ToDouble(NettSO.Text) & ",'" & CDate(toDate(createtime.Text)) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & returmstoid & " where tablename = 'QL_trnjualreturmst' And cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() 
            Else
                '================= Proses Edit data ===============
                '--------------------------------------------------
                returmstoid = Session("oid")
                sSql = "Update QL_trnjualreturmst set orderno = '" & sono.Text & "', periodacctg = '" & GetDateToPeriodAcctg(returdate) & "', trnjualreturno = '" & noreturn.Text & "', refSJjualno = '" & sjno.Text & "', refnotaretur = '" & invoiceno.Text & "', trnjualdate = '" & returdate & "', trnjualref = '" & Tchar(noref.Text) & "', trncustoid = " & Integer.Parse(custoid.Text) & ", trncustname = '" & Tchar(custname.Text) & "', trnpaytype = " & Integer.Parse(paytype.Text) & ", trnamtjual = " & ToDouble(grossreturn.Text) & ",trnamtjualidr = " & ToDouble(grossreturn.Text) & ",trnamtjualusd = " & ToDouble(grossreturn.Text) & ", trntaxpct = " & ToDouble(taxpct.Text) & ", trnamttax = " & ToDouble(taxamount.Text) & ", trnamttaxidr = " & ToDouble(taxamount.Text) & ", trnamttaxusd = " & ToDouble(taxamount.Text) & ", trnamtjualnetto = " & ToDouble(totalamount.Text) & ", trnamtjualnettoidr = " & ToDouble(totalamount.Text) & ", trnamtjualnettousd = " & ToDouble(totalamount.Text) & ", trnjualnote = '" & Tchar(note.Text) & "', trnjualstatus = '" & status.Text & "', lastpaymentdate = '" & alastpaydate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP,currencyoid='" & lblcurr.Text & "',typeretur='" & DDLtype.SelectedValue & "',typeSO='" & typeSO.Text & "',trnjualmstoid=" & Integer.Parse(JualMstOid.Text) & ",salesoid=" & Integer.Parse(spgoid.Text) & ",amtnettso=" & ToDouble(NettSO.Text) & " Where trnjualreturmstoid = " & returmstoid & " and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If status.Text = "In Approval" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus,branch_code) VALUES " & _
                            "('" & CompnyCode & "'," & AppOid + c1 & ", '" & "SR" & ddlFromcabang.SelectedValue & "_" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_trnjualreturmst', '" & Session("oid") & "','In Approval','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1', '" & objTable.Rows(c1).Item("approvalstatus") & "','" & ddlFromcabang.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next

                        sSql = "UPDATE ql_mstoid set lastoid = " & AppOid + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                sSql = "Delete From QL_trnjualreturdtl Where trnjualreturmstoid = " & Session("oid") & " and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If
            '----- End Edit Data -----

            Dim qty_to As Double = 0.0
            If Not ViewState("dtlsalesreturn") Is Nothing Then
                Dim dtab As DataTable = ViewState("dtlsalesreturn")
                If status.Text = "In Approval" Then
                    If typeSO.Text = "Konsinyasi" Then
                        sSql = "INSERT INTO [QL_trnordermst] ([cmpcode],[ordermstoid],[to_branch],[branch_code],[orderno],[trnordertype],[trnorderdate],[trnorderref],[trncustoid],[trncustname],[trnpaytype],[trnordernote],[salesoid],[updtime],[upduser],[createuser],[trnorderstatus],[amtjualnetto],[amtjualnettoidr],[amtjualnettousd],[ekspedisioid],[spgoid],[finalappovaldatetime],[finalapprovaluser],[finalapprovalcode],[canceluser],[canceltime],[cancelnote],[delivdate],[consigneeoid],[trntaxpct],[periodacctg],[promooid],[createtime],[SOBO],[flagbottomprice],[otorisasiuser],[digit],[currencyoid],[rateoid],[rate2oid],[timeofpaymentSO],[ordertaxamt],[ordertaxtype],[revisenote],[flagTax],[typeExpedisi],[typeSO],[bundling],[amtdisc1],[amtdisc2],trnjualreturmstoid)" & _
                            " VALUES ('" & CompnyCode & "'," & Integer.Parse(OidSO) & "," & ddlFromcabang.SelectedValue & "," & ddlFromcabang.SelectedValue & ",'" & OidSO & "','GROSIR','" & GetServerTime() & "','" & Tchar(noref.Text) & "'," & Integer.Parse(custoid.Text) & ",'" & Tchar(custname.Text) & "'," & Integer.Parse(paytype.Text) & ",'" & Tchar(note.Text) & "'," & Integer.Parse(spgoid.Text) & ",CURRENT_TIMESTAMP,'" & Session("UserID") & "','" & Session("UserID") & "','In Process'," & ToDouble(NettSO.Text) & "," & ToDouble(NettSO.Text) & "," & ToDouble(NettSO.Text) & ",0," & Integer.Parse(spgoid.Text) & ",'1/1/1900','','','','1/1/1900','',CURRENT_TIMESTAMP," & Integer.Parse(returmstoid) & "," & ToDouble(taxpct.Text) & ",'" & GetDateToPeriodAcctg(returdate) & "',0,CURRENT_TIMESTAMP,0,'N','',4,1,0,0.0000," & Integer.Parse(paytype.Text) & "," & ToDouble(taxamount.Text) & ",'NONTAX','',0,'Darat','Dealer',0,0.0000," & ToDouble(promodiscamt.Text) & "," & Integer.Parse(returmstoid) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "update ql_mstoid set lastoid = " & Integer.Parse(OidSO) & " Where tablename = 'QL_trnordermst' And cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    End If
                End If
                'dtab.Rows(i).Item("promodisc")
                Dim discamt As Double = 0
                For i As Integer = 0 To dtab.Rows.Count - 1
                    returdtloid = returdtloid + 1
                    If Integer.Parse(dtab.Rows(i).Item("promooid")) <> 0 Then
                        discamt = ToDouble(dtab.Rows(i).Item("promodisc")) / ToDouble(dtab.Rows(i).Item("qty"))
                    Else
                        discamt = ToDouble(dtab.Rows(i).Item("promodisc"))
                    End If
                    sSql = "insert into QL_trnjualreturdtl (cmpcode, branch_code, trnjualreturdtloid, trnjualreturmstoid, trnjualdtlseq, itemloc, itemoid, trnjualdtlqty, trnjualdtlunitoid, unitSeq, trnjualdtlprice, trnjualdtlpriceidr, trnjualdtlpriceusd, trnjualdtlpriceunitoid, trnjualdtldisctype, trnjualdtldiscqty, trnjualflag, trnjualnote, trnjualstatus, amtjualdisc, amtjualnetto ,amtjualnettoidr, amtjualnettousd, createuser, upduser, updtime, trnjualdtldisctype2, trnjualdtldiscqty2, amtjualdisc2, trnjualdtloid, qtyso) Values" & _
                        " ('" & CompnyCode & "','" & ddlFromcabang.SelectedValue & "', " & Integer.Parse(returdtloid) & ", " & Integer.Parse(returmstoid) & ", " & dtab.Rows(i).Item("seq") & ", " & dtab.Rows(i).Item("locationoid") & ", " & dtab.Rows(i).Item("itemoid") & ", " & dtab.Rows(i).Item("qty") & ", " & dtab.Rows(i).Item("unitoid") & ", " & dtab.Rows(i).Item("unitseq") & ", " & dtab.Rows(i).Item("price") & ", " & dtab.Rows(i).Item("price") & ", " & dtab.Rows(i).Item("price") & ", " & dtab.Rows(i).Item("price") & ", 'AMT', " & ToDouble(discamt) & ", '" & status.Text & "', '" & Tchar(dtab.Rows(i).Item("note")) & "', '" & status.Text & "', " & dtab.Rows(i).Item("promodiscamt") & ", " & dtab.Rows(i).Item("netamt") & ", " & ToDouble(dtab.Rows(i).Item("netamt")) & ", " & dtab.Rows(i).Item("netamt") & ",'" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'AMT', 0, 0, " & Integer.Parse(dtab.Rows(i).Item("trnjualdtloid")) & "," & ToDouble(dtab.Rows(i).Item("QtySO")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If status.Text = "In Approval" Then
                        If typeSO.Text = "Konsinyasi" Then
                            sSql = "INSERT INTO [QL_trnorderdtl] ([cmpcode],[trnorderdtloid],[branch_code],[to_branch],[trnordermstoid],[trnorderdtlseq],[itemoid],[trnorderdtlqty],[trnorderdtlunitoid],[unitseq],[trnorderdtlnote],[createuser],[upduser],[updtime],[currencyoid],[varprice],[trnorderprice],[trnorderpriceidr],[trnorderpriceusd],[trnamountdtl],[trnamountdtlidr],[trnamountdtlusd],[trnorderdtlstatus],[orderdelivqty],[trndiskonpromo],[trndiskonpromoidr],[trndiskonpromousd],[trnamountbruto],[trnamountbrutoidr],[trnamountbrutousd],[promooid],[flagbottompricedtl],[bottomprice],[amountbottomprice],[statusDelivery],[tempAmtUsd],[amtdtldisc1],[amtdtldisc2],[jenisprice],[flagpromo],[typebarang],trnjualreturdtloid,trnjualreturmstoid)" & _
                            " VALUES ('" & CompnyCode & "'," & Integer.Parse(OidDtlSO) & ",'" & ddlFromcabang.SelectedValue & "','" & ddlFromcabang.SelectedValue & "'," & Integer.Parse(OidSO) & "," & dtab.Rows(i).Item("seq") & "," & Integer.Parse(dtab.Rows(i).Item("itemoid")) & "," & ToDouble(dtab.Rows(i).Item("qtyso")) & "," & Integer.Parse(dtab.Rows(i).Item("unitoid")) & "," & dtab.Rows(i).Item("seq") & ",'" & Tchar(dtab.Rows(i).Item("note").ToString) & "','" & Session("UserID") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,1,0.0000," & ToDouble(dtab.Rows(i).Item("price")) & "," & ToDouble(dtab.Rows(i).Item("price")) & "," & ToDouble(dtab.Rows(i).Item("price")) & "," & (ToDouble(dtab.Rows(i).Item("price")) * ToDouble(dtab.Rows(i).Item("qtyso"))) - ToDouble(dtab.Rows(i).Item("promodiscamt")) & "," & (ToDouble(dtab.Rows(i).Item("price")) * ToDouble(dtab.Rows(i).Item("qtyso"))) - ToDouble(dtab.Rows(i).Item("promodiscamt")) & "," & (ToDouble(dtab.Rows(i).Item("price")) * ToDouble(dtab.Rows(i).Item("qtyso"))) - ToDouble(dtab.Rows(i).Item("promodiscamt")) & ",'',0.0000,0.0000,0.0000,0.0000," & ToDouble(dtab.Rows(i).Item("price")) * ToDouble(dtab.Rows(i).Item("qtyso")) & "," & ToDouble(dtab.Rows(i).Item("price")) * ToDouble(dtab.Rows(i).Item("qtyso")) & "," & ToDouble(dtab.Rows(i).Item("price")) * ToDouble(dtab.Rows(i).Item("qtyso")) & ",0,'',0.00,0.00,'',0.00,0.00," & ToDouble(dtab.Rows(i).Item("promodiscamt")) & ",'NORMAL','N',''," & Integer.Parse(returdtloid) & "," & Integer.Parse(returmstoid) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            OidDtlSO += 1
                        End If
                    End If
                Next

                sSql = "Update ql_mstoid set lastoid = " & OidDtlSO - 1 & " Where tablename = 'QL_trnorderdtl' and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update ql_mstoid set lastoid = " & returdtloid & " where tablename = 'QL_trnjualreturdtl' and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            otrans.Commit() : conn.Close()
        Catch ex As Exception
            otrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, 2)
            status.Text = "In Process" : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnSalesReturn.aspx?awal=true")
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click
        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            sSql = "select trnjualstatus from QL_trnjualreturmst WHERE trnjualreturmstoid = " & Integer.Parse(Session("oid")) & " and branch_code = '" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "post" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return has been posted!<br />It can't be deleted!", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Data retur sudah dihapus...!!", 2)
                Exit Sub
            End If

            sSql = "delete from QL_trnjualreturmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & " and branch_code = '" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnjualreturdtl where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & " and branch_code='" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            otrans.Commit()
            conn.Close()
            Response.Redirect("trnSalesReturn.aspx?awal=true")
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString & "<br />" & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnsearchtr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "select a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid, e.custname,d.trntaxpct, d.trnpaytype from ql_trnTTSmst a inner join ql_TrnTTSDtl b on a.TTSoid = b.ttsoid inner join ql_mstitem c on b.refoid = c.itemoid inner join QL_trnjualmst d on a.cmpcode = d.cmpcode and a.trnsjjualno = d.trnsjjualno inner join QL_mstcust e on a.cmpcode = e.cmpcode and a.custoid = e.custoid where a.cmpcode = '" & CompnyCode & "' and a.TTSstatus = 'post' and  group by a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid,d.trntaxpct, e.custname, d.trnpaytype having (sum(b.qty)-(select isnull(sum(m.trnjualdtlqty), 0) from QL_trnjualreturdtl m inner join QL_trnjualreturmst n on m.cmpcode = n.cmpcode and m.trnjualreturmstoid = n.trnjualreturmstoid inner join ql_mstitem o on m.cmpcode = o.cmpcode and m.itemoid = o.itemoid where n.ttsoid = a.TTSoid)) > 0" & _
        " union all " & _
        "select a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid, e.custname,d.trntaxpct, d.trnpaytype from ql_trnTTSmst a inner join  ql_TrnTTSDtl b on a.TTSoid = b.ttsoid inner join ql_mstitem c on b.refoid = c.itemoid inner join QL_trnjualdtl dd on a.cmpcode = dd.cmpcode and a.trnsjjualno = dd.trnsjjualno and b.refoid = dd.itemoid  inner join QL_trnjualmst d on a.cmpcode = d.cmpcode and dd.trnjualmstoid  = d.trnjualmstoid  inner join QL_mstcust e on a.cmpcode = e.cmpcode and a.custoid = e.custoid where a.cmpcode = '" & CompnyCode & "' and a.TTSstatus = 'post' and d.trnjualstatus = 'POST'  group by a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid,d.trntaxpct, e.custname, d.trnpaytype having (sum(b.qty)-(select isnull(sum(m.trnjualdtlqty), 0) from QL_trnjualreturdtl m inner join QL_trnjualreturmst n on m.cmpcode = n.cmpcode and m.trnjualreturmstoid = n.trnjualreturmstoid inner join ql_mstitem o on m.cmpcode = o.cmpcode and m.itemoid = o.itemoid where n.ttsoid = a.TTSoid)) > 0"

        ViewState("trsalesreturn") = cKon.ambiltabel(sSql, "trsalesreturn")
        GVTr.DataSource = ViewState("trsalesreturn")
        GVTr.DataBind()

        GVTr.Visible = True
        GVTr.PageIndex = 0
    End Sub

    Protected Sub GVTr_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTr.PageIndexChanging
        GVTr.PageIndex = e.NewPageIndex
        BindDataSI()
    End Sub

    Protected Sub btnerasetr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        trdate.Text = "" : paytype.Text = ""
        custname.Text = "" : custoid.Text = ""
        invoiceno.Text = "" : sjno.Text = ""
        sono.Text = "" : taxpct.Text = "0.00"

        If GVTr.Visible = True Then
            ViewState("trsalesreturn") = Nothing
            GVTr.DataSource = Nothing
            GVTr.DataBind()
            GVTr.Visible = False
        End If

        itemname.Text = "" : itemcode.Text = ""
        itemoid.Text = "" : qty.Text = "0.00"
        unit.Text = "" : satuan1.Text = ""
        priceperunit.Text = "0.00"   
        totalamountdtl.Text = "0.00"
        netamount.Text = "0.00" : notedtl.Text = ""
        labelseq.Text = "" : detailstate.Text = "new"

        If GVItem.Visible = True Then
            ViewState("itemsalesreturn") = Nothing
            GVItem.DataSource = Nothing
            GVItem.DataBind()
            GVItem.Visible = False
        End If

        GVDtl.DataSource = Nothing
        GVDtl.DataBind()
        ViewState("dtlsalesreturn") = Nothing
    End Sub

    Protected Sub btnsearchinvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataSI() : GVTr.Visible = True
    End Sub

    Protected Sub btnsearchitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearchitem.Click
        If JualMstOid.Text <> "" Then
            BindItem()
        Else
            showMessage("- Maaf, Tolong Pilih katalog.nya dulu..!! (-_-)", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btneraseitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btneraseitem.Click
        itemname.Text = "" : itemcode.Text = ""
        itemoid.Text = "" : qty.Text = "0.00"
        maxqty.Text = "0.00" : unit.Text = ""
        satuan1.Text = "" : unitseq.Text = ""
        trnjualdtloid.Text = "" : promodisc.Text = "0.00"
        promodiscamt.Text = "0.00"

        If GVItem.Visible = True Then
            ViewState("itemsalesreturn") = Nothing
            GVItem.DataSource = Nothing
            GVItem.DataBind()
            GVItem.Visible = False
        End If
    End Sub

    Protected Sub GVItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItem.PageIndexChanging
        GVItem.PageIndex = e.NewPageIndex
        BindItem()
    End Sub

    Private Function checkotherretur(ByVal sino As String, ByVal iItemOid As Integer, ByVal cmpcode As String) As Boolean
        sSql = "select COUNT (-1) from ql_trnjualreturmst a inner join QL_trnjualreturdtl b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b. branch_code Where a.trnjualstatus ='In process' and a.cmpcode = '" & cmpcode & "' and a.refnotaretur ='" & sino & "' and  b.itemoid='" & iItemOid & "' AND a.branch_code='" & ddlFromcabang.SelectedValue & "'"

        If masterstate.Text = "edit" Then
            sSql &= " AND a.trnjualreturmstoid <>" & Session("oid")
        ElseIf masterstate.Text = "new" Then
            sSql &= " AND a.trnjualreturmstoid <> '' "
        End If
        Return cKon.ambilscalar(sSql) > 0
    End Function

    Protected Sub btnaddtolist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddtolist.Click
        Dim eSmsg As String = "" 

        If itemname.Text = "" Or itemoid.Text = "" Then
            eSmsg &= "- Maaf, Anda belum memilih katalog...!!<br>"
        End If

        If ToDouble(qty.Text) <= 0 Then
            eSmsg &= "- Maaf, Quantity yang anda input masih nol..!!<br>"
        End If

        If Integer.Parse(JualMstOid.Text) > 0 Then
            If ToDouble(qty.Text) > ToDouble(maxqty.Text) Then
                sSql = "Select re.trnjualreturno, re.trnjualstatus, c.itemdesc, a.trnjualdtlqty, ISNULL(QtyRet,0.00) QtyRet, a.itemoid From ql_trnjualdtl a inner join ql_mstitem c on a.cmpcode = c.cmpcode and a.itemoid = c.itemoid Left Join (Select rm.trnjualreturno, rm.trnjualmstoid, rm.branch_code, Isnull(SUM(rd.trnjualdtlqty),0.00) QtyRet, rd.trnjualdtloid, rd.itemoid, rm.trnjualstatus From QL_trnjualreturdtl rd Inner JOin QL_trnjualreturmst rm ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rd.branch_code=rm.branch_code Group BY rm.trnjualmstoid, rm.branch_code, rd.trnjualdtloid, rd.itemoid, rm.trnjualreturno, rm.trnjualstatus) re ON re.trnjualmstoid=a.trnjualmstoid AND re.branch_code=a.branch_code AND re.trnjualdtloid=a.trnjualdtloid AND re.itemoid=a.itemoid Where a.trnjualmstoid =" & Integer.Parse(JualMstOid.Text) & " AND a.itemoid=" & Integer.Parse(itemoid.Text) & " AND re.trnjualstatus<>'Rejected'"
                Dim dt As DataTable = cKon.ambiltabel2(sSql, "Validasi")
                If dt.Rows.Count > 0 Then
                    eSmsg &= "- Maaf, Quantity " & itemname.Text & " yang anda input melebihi Qty Sisa..!!<br>"
                    For i As Integer = 0 To dt.Rows.Count - 1
                        eSmsg &= "- Nomer Retur : " & dt.Rows(i)("trnjualreturno") & " => Qty SI : " & ToMaskEdit(dt.Rows(i)("trnjualdtlqty"), 3) & " => Qty Retur : " & ToMaskEdit(dt.Rows(i)("QtyRet"), 3) & " => Status : " & dt.Rows(i)("trnjualstatus") & "<br>"
                    Next
                End If
            End If
        End If

        If typeSO.Text = "Konsinyasi" Then
            If ToDouble(QtySO.Text) > ToDouble(maxqty.Text) Then
                eSmsg &= "- Maaf, Quantity SO yang anda input melebihi batas maximum..!!<br>"
            End If

            If ToDouble(QtySO.Text) <= 0 Then
                eSmsg &= "- Maaf, No. SI " & invoiceno.Text & " adalah nota type konsinyasi, silahkan input Quantity SO..!!<br>"
            End If
        End If

        If detailstate.Text = "new" Then
            labelseq.Text = GVDtl.Rows.Count + 1
        End If

        Dim dtab As New DataTable
        If ViewState("dtlsalesreturn") Is Nothing Then
            dtab.Columns.Add("seq", Type.GetType("System.Int32"))
            dtab.Columns.Add("itemcode", Type.GetType("System.String"))
            dtab.Columns.Add("itemname", Type.GetType("System.String"))
            dtab.Columns.Add("qty", Type.GetType("System.Double"))
            dtab.Columns.Add("qtyso", Type.GetType("System.Double"))
            dtab.Columns.Add("unit", Type.GetType("System.String"))
            dtab.Columns.Add("price", Type.GetType("System.Double"))
            dtab.Columns.Add("note", Type.GetType("System.String"))
            dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("netamt", Type.GetType("System.Double"))
            dtab.Columns.Add("unitoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("unitseq", Type.GetType("System.Int32"))
            dtab.Columns.Add("trnjualdtloid", Type.GetType("System.Int32"))
            dtab.Columns.Add("maxqty", Type.GetType("System.Double"))
            dtab.Columns.Add("location", Type.GetType("System.String"))
            dtab.Columns.Add("locationoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("promodisc", Type.GetType("System.Double"))
            dtab.Columns.Add("promodiscamt", Type.GetType("System.Double"))
            dtab.Columns.Add("itemloc", Type.GetType("System.Double"))
            dtab.Columns.Add("branch_code", Type.GetType("System.String"))
            dtab.Columns.Add("promooid", Type.GetType("System.Int32"))
            ViewState("dtlsalesreturn") = dtab
        Else
            dtab = ViewState("dtlsalesreturn")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid=" & Integer.Parse(itemoid.Text) & " AND seq <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                eSmsg &= "- Maaf, Barang sudah di add to list..!!<br>"
            End If
        End If

        If eSmsg <> "" Then
            showMessage(eSmsg, 2)
            Exit Sub
        End If
 
        If detailstate.Text = "new" Then
            labelseq.Text = GVDtl.Rows.Count + 1
            Dim drow As DataRow = dtab.NewRow
            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemcode") = itemcode.Text
            drow("itemname") = itemname.Text
            drow("qty") = ToDouble(qty.Text)
            drow("qtyso") = ToDouble(QtySO.Text)
            drow("unit") = unit.Text
            drow("price") = ToDouble(priceperunit.Text)
            drow("note") = notedtl.Text
            drow("itemoid") = Integer.Parse(itemoid.Text)
            drow("netamt") = ToDouble(totalamountdtl.Text)
            drow("unitoid") = Integer.Parse(satuan1.Text)
            drow("maxqty") = ToDouble(maxqty.Text)
            drow("unitseq") = Integer.Parse(unitseq.Text)
            drow("trnjualdtloid") = Integer.Parse(trnjualdtloid.Text)
            drow("location") = fromlocation.SelectedItem
            drow("locationoid") = fromlocation.SelectedValue
            drow("promodisc") = ToDouble(promodisc.Text)
            drow("promodiscamt") = ToDouble(promodiscamt.Text)
            drow("branch_code") = ddlFromcabang.SelectedValue
            drow("promooid") = promooid.Text
            dtab.Rows.Add(drow)
        Else
            Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "")
            If drow.Length > 0 Then
                drow(0).BeginEdit()
                drow(0)("itemcode") = itemcode.Text
                drow(0)("itemname") = itemname.Text
                drow(0)("qty") = ToDouble(qty.Text)
                drow(0)("qtyso") = ToDouble(QtySO.Text)
                drow(0)("unit") = unit.Text
                drow(0)("price") = ToDouble(priceperunit.Text)
                drow(0)("note") = notedtl.Text
                drow(0)("itemoid") = Integer.Parse(itemoid.Text)
                drow(0)("netamt") = ToDouble(totalamountdtl.Text)
                drow(0)("unitoid") = Integer.Parse(satuan1.Text)
                drow(0)("unitseq") = Integer.Parse(unitseq.Text)
                drow(0)("maxqty") = ToDouble(maxqty.Text)
                drow(0)("trnjualdtloid") = Integer.Parse(trnjualdtloid.Text)
                drow(0)("location") = fromlocation.SelectedItem.ToString
                drow(0)("locationoid") = fromlocation.SelectedValue
                drow(0)("promodisc") = ToDouble(promodisc.Text)
                drow(0)("promodiscamt") = ToDouble(promodisc.Text)
                drow(0)("branch_code") = ToDouble(promodisc.Text)
                drow(0)("promooid") = promooid.Text
                drow(0).EndEdit()
            Else
                showMessage("Sorry, missing something!", 2)
                Exit Sub
            End If
        End If
        dtab.AcceptChanges()
        dtab.Select("", "seq")

        GVDtl.DataSource = dtab : GVDtl.DataBind()
        ViewState("dtlsalesreturn") = dtab

        ClearNya()
        detailstate.Text = "new"
        GVDtl.SelectedIndex = -1
        GVDtl.Columns(10).Visible = True

        If typeSO.Text = "Konsinyasi" Then
            GVDtl.Columns(4).Visible = True
        Else
            GVDtl.Columns(4).Visible = False
        End If
        calculateheader() : HitungNettSO()
        'End If
    End Sub

    Private Sub ClearNya()
        itemname.Text = "" : itemoid.Text = ""
        itemcode.Text = "" : qty.Text = "0.00"
        maxqty.Text = "0.00" : unit.Text = ""
        unitseq.Text = "" : satuan1.Text = ""
        priceperunit.Text = "0.00"
        totalamountdtl.Text = "0.00"
        netamount.Text = "0.00" : notedtl.Text = ""
        promodisc.Text = "0.00" : promodiscamt.Text = "0.00"
        labelseq.Text = "" : promooid.Text = 0
    End Sub

    Private Sub HitungNettSO()
        Dim AmtNettSO As Double
        Dim ds As DataTable = ViewState("dtlsalesreturn")
        If Not ViewState("dtlsalesreturn") Is Nothing Then
            For v1 As Int32 = 0 To ds.Rows.Count - 1
                AmtNettSO += ToDouble(ds.Rows(v1)("price") * ds.Rows(v1)("qtyso")) - ToDouble(ds.Rows(v1)("promodiscamt"))
            Next
            If Integer.Parse(JualMstOid.Text) > 0 Then
                NettSO.Text = ToMaskEdit(ToDouble(AmtNettSO), 4)
            End If
        End If
    End Sub

    Protected Sub btnclear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnclear.Click
        itemname.Text = "" : itemcode.Text = ""
        itemoid.Text = "" : qty.Text = "0.00"
        maxqty.Text = "0.00" : unit.Text = ""
        unitseq.Text = "" : satuan1.Text = ""
        trnjualdtloid.Text = "" : promodisc.Text = "0.00"
        promodiscamt.Text = "0.00" : priceperunit.Text = "0.00"
        totalamountdtl.Text = "0.00" : netamount.Text = "0.00"
        notedtl.Text = "" : labelseq.Text = ""
        detailstate.Text = "new"
        GVDtl.SelectedIndex = -1
        GVDtl.Columns(10).Visible = True
    End Sub

    Protected Sub GVDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl.RowDeleting
        If Not ViewState("dtlsalesreturn") Is Nothing Then
            Dim dtab As DataTable = ViewState("dtlsalesreturn")
            Dim seq As Integer = Integer.Parse(GVDtl.Rows(e.RowIndex).Cells(1).Text)
            Dim drow() As DataRow = dtab.Select("seq = " & seq & "")
            drow(0).Delete()
            dtab.Select(Nothing)
            dtab.AcceptChanges()

            Dim arow As DataRow = Nothing
            For i As Integer = 0 To dtab.Rows.Count - 1
                If dtab.Rows(i).Item("seq") > seq Then
                    arow = dtab.Rows(i)
                    arow.BeginEdit()
                    arow("seq") = arow("seq") - 1
                    arow.EndEdit()
                End If
            Next
            dtab.AcceptChanges()
            If dtab.Rows.Count = 0 Then
                grossreturn.Text = "0.00"
                totalamount.Text = "0.00"
            End If
            ViewState("dtlsalesreturn") = dtab
            GVDtl.DataSource = dtab
            GVDtl.DataBind()
        End If
    End Sub

    Protected Sub GVDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl.SelectedIndexChanged
        DDLBranch()
        ddlFromcabang.SelectedValue = GVDtl.SelectedDataKey("branch_code").ToString
        labelseq.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(1).Text
        detailstate.Text = "edit"
        itemname.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(2).Text
        itemcode.Text = GVDtl.SelectedDataKey("itemcode")
        itemoid.Text = GVDtl.SelectedDataKey("itemoid")
        qty.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(3).Text
        maxqty.Text = GetStrData("select (a.trnjualdtlqty - a.trnjualdtlqty_retur) qty from QL_trnjualdtl a inner join ql_trnjualmst b on a.trnjualmstoid = b.trnjualmstoid and a.branch_code = b.branch_code where a.itemoid = '" & GVDtl.SelectedDataKey("itemoid") & "' and b.trnjualno='" & invoiceno.Text & "'")
        qty.Enabled = False
        qty.CssClass = "inpTextDisabled"
        unit.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(4).Text
        unitseq.Text = GVDtl.SelectedDataKey("unitseq")
        satuan1.Text = GVDtl.SelectedDataKey("unitoid")
        initallddl()
        itemloc.Text = GVDtl.SelectedDataKey("locationoid")
        fromlocation.SelectedValue = GVDtl.SelectedDataKey("locationoid")
        notedtl.Text = HttpUtility.HtmlDecode(GVDtl.Rows(GVDtl.SelectedIndex).Cells(8).Text)
        trnjualdtloid.Text = GVDtl.SelectedDataKey("trnjualdtloid")
        promodisc.Text = Format(GVDtl.SelectedDataKey("promodisc"), "#,##0.0000")
        promodiscamt.Text = Format(GVDtl.SelectedDataKey("promodiscamt"), "#,##0.0000")
        priceperunit.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(5).Text
        totalamountdtl.Text = Format(GVDtl.SelectedDataKey("netamt"), "#,##0.0000")
        GVDtl.Columns(10).Visible = False
    End Sub

    Protected Sub discheader_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles discheader.TextChanged
        calculateheader()
    End Sub

    Private Sub calculateheader()
        If GVDtl.Rows.Count > 0 Then
            Dim agrossreturn As Double = 0.0 : Dim ataxamount As Double = 0.0
            Dim adiscamount As Double = 0.0 : Dim atotaldischeader As Double = 0.0
            Dim atotalamount As Double = 0.0
            Dim drow As DataRow = Nothing
            Dim dtab As New DataTable

            If Not ViewState("dtlsalesreturn") Is Nothing Then
                dtab = ViewState("dtlsalesreturn")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    drow = dtab.Rows(i)
                    agrossreturn = agrossreturn + drow.Item("netamt")
                    adiscamount = adiscamount + drow.Item("promodiscamt")
                Next
            Else
                showMessage("Missing something '>_<'", 2)
                Exit Sub
            End If
            grossreturn.Text = Format(agrossreturn, "#,##0.00")
            totalamount.Text = Format(agrossreturn + (agrossreturn * (ToDouble(taxpct.Text) / 100)), "#,##0.00")
            taxamount.Text = Format(agrossreturn * (ToDouble(taxpct.Text) / 100), "#,##0.00")
            amtdiscdtl.Text = adiscamount

        Else
            grossreturn.Text = "0.00" : taxamount.Text = "0.00"
            discamount.Text = "0.00"
            totaldischeader.Text = IIf(discheadertype.SelectedValue = "AMT", Format(ToDouble(discheader.Text), "#,##0.00"), Format((ToDouble(grossreturn.Text) - ToDouble(discamount.Text)) * ToDouble(discheader.Text), "#,##0.00"))
            totalamount.Text = "0.00" : amtdiscdtl.Text = "0.00"
        End If
    End Sub

    Protected Sub discheadertype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles discheadertype.SelectedIndexChanged
        calculateheader()
    End Sub
    ' sono.Text
    Private Sub calculatedetail()
        'promodiscamt.Text = ToMaskEdit(ToDouble(qty.Text * promodisc.Text), 3)
        Dim price As Decimal = ToMaskEdit(priceperunit.Text, 3)
        Dim qtyc As Decimal = ToMaskEdit(qty.Text, 3)
        Dim pro As Decimal = ToMaskEdit(promodisc.Text, 3)
        Dim hf As Decimal = ToMaskEdit(price, 3) * ToMaskEdit(qty.Text, 3)
        Dim PromoOid As Integer = GetScalar("select promooid from QL_trnordermst Where orderno='" & sono.Text & "'")
        If PromoOid = 0 Then
            totalamountdtl.Text = ToMaskEdit((qty.Text * priceperunit.Text) - (promodisc.Text * qty.Text), 3)
            promodiscamt.Text = ToMaskEdit(promodisc.Text * qty.Text, 3)
        Else
            totalamountdtl.Text = ToMaskEdit((qty.Text * priceperunit.Text) - (promodiscamt.Text), 3)
        End If
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        calculatedetail()
    End Sub

    Protected Sub priceperunit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles priceperunit.TextChanged
        'ToMaskEdit(priceperunit.Text, 3)
        'calculatedetail()
    End Sub

    Protected Sub GVTrn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTrn.PageIndexChanging
        If Not ViewState("salesretur") Is Nothing Then
            GVTrn.DataSource = ViewState("salesretur")
            GVTrn.PageIndex = e.NewPageIndex
            GVTrn.DataBind()
        End If
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lb As LinkButton = TryCast(sender, LinkButton)
            Dim str As String() = lb.ToolTip.Split(",")
            Dim cBranch As String = sender.CommandName
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\printsalesreturn.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", " where a.trnjualreturmstoid = " & Integer.Parse(str(0)) & " and a.branch_code='" & cBranch & "'")
            rpt.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Sales_Retur_" & str(1))
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Try
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\printsalesreturn.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", " where a.trnjualreturmstoid = " & Session("oid") & " and a.branch_code='" & Session("branch_id") & "'")
            rpt.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Sales_Return_" & noreturn.Text)
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btneraseinvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        invoiceno.Text = "" : trdate.Text = ""
        paytype.Text = "" : sjno.Text = ""
        sono.Text = "" : JualMstOid.Text = ""
        custoid.Text = "" : custname.Text = ""
        GVTr.Visible = False
        typeSO.Text = "" : branch_code.Text = ""
    End Sub

    Protected Sub btncustomer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindCust()
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custname.Text = gvCustomer.Rows(gvCustomer.SelectedIndex).Cells(2).Text
        custoid.Text = gvCustomer.SelectedDataKey.Item("trncustoid")
        gvCustomer.Visible = False
        BindDataSI()
    End Sub

    Protected Sub btnerasercustomer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custoid.Text = "" : custname.Text = ""
        gvCustomer.Visible = False : GVTr.Visible = False
    End Sub

    Protected Sub ibdelete_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim otrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            sSql = "select trnjualstatus from QL_trnjualreturmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "post" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return has been posted!<br />It can't be deleted!", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return data cannot be found!", 2)
                Exit Sub
            End If

            sSql = "delete from QL_trnjualreturmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnjualreturdtl where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If Not ViewState("dtlsalesreturn") Is Nothing Then
                Dim dtab As DataTable = ViewState("dtlsalesreturn")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    'returdtloid = returdtloid + 1
                    sSql = "update QL_trnjualdtl set trnjualdtlqty_retur = trnjualdtlqty_retur - " & dtab.Rows(i).Item("qty") & ", trnjualdtlqty_retur_unit3 = trnjualdtlqty_retur_unit3 - (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & ") where trnjualdtloid = " & dtab.Rows(i).Item("trnjualdtloid") & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                Next
            End If
            otrans.Commit()
            conn.Close()

            Response.Redirect("trnSalesReturn.aspx?awal=true")
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub GVSN_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim iIndex As Int16 = e.RowIndex
        Dim objTableSN As DataTable = Session("TblSN")
        objTableSN.Rows.RemoveAt(iIndex)
        'resequence sjDtlsequence 
        For C1 As Int16 = 0 To objTableSN.Rows.Count - 1
            Dim dr As DataRow = objTableSN.Rows(C1)
            dr.BeginEdit()
            dr("SNseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblSN") = objTableSN
        GVSN.Visible = True
        GVSN.DataSource = objTableSN
        GVSN.DataBind()
        ModalPopupSN.Show()
        TextSN.Text = ""
        TextSN.Focus()
    End Sub

    Protected Sub EnterSN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EnterSN.Click
        TextSN.Focus()
        sSql = "select COUNT(a.sn)  FROM QL_mstitemDtl a  INNER JOIN QL_Mst_SN b ON a.sn = b.id_sn  WHERE b.status_in_out ='out' and trnjualmstoid = (select c.trnsjjualno from ql_trnsjjualmst c inner join QL_trnjualmst b on c.trnsjjualno = b.trnsjjualno WHERE trnjualno = '" & invoiceno.Text & "') AND a.sn = '" & TextSN.Text & "' and a.itemoid = '" & KodeItem.Text & "' "
        Dim SN As Object = cKon.ambilscalar(sSql)

        If Not SN = 0 Then
            If Session("TblSN") Is Nothing Then
                Dim dtlTableSN As DataTable = setTabelSN()
                Session("TblSN") = dtlTableSN
            End If

            Dim objTableSN As DataTable
            objTableSN = Session("TblSN")
            Dim dv As DataView = objTableSN.DefaultView

            'Cek apa sudah ada item yang sama dalam Tabel Detail
            If NewSN.Text = "New SN" Then
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "'"
            Else
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "' AND SNseq <>" & SNseq.Text
            End If

            If dv.Count > 0 Then
                LabelPesanSn.Text = "Serial Number Sudah Entry"
                dv.RowFilter = "" : ModalPopupSN.Show()
                TextSN.Text = "" : TextSN.Focus()
                Exit Sub
            End If

            dv.RowFilter = ""
            'insert/update to list data
            Dim objRow As DataRow
            If NewSN.Text = "New SN" Then
                objRow = objTableSN.NewRow()
                objRow("SNseq") = objTableSN.Rows.Count + 1
            Else
                Dim selrow As DataRow() = objTableSN.Select("SNseq=" & SNseq.Text)
                objRow = selrow(0)
                objRow.BeginEdit()
            End If

            objRow("itemoid") = KodeItem.Text
            objRow("SN") = TextSN.Text

            If NewSN.Text = "New SN" Then
                objTableSN.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblSN") = objTableSN
            GVSN.Visible = True
            GVSN.DataSource = Nothing
            GVSN.DataSource = objTableSN
            GVSN.DataBind()
            LabelPesanSn.Text = ""
            SNseq.Text = objTableSN.Rows.Count + 1
            GVSN.SelectedIndex = -1
            TextSN.Focus()
        Else
            LabelPesanSn.Text = "Serial Number Unknow"
        End If
        ModalPopupSN.Show()
        TextSN.Text = ""
        TextSN.Focus()
    End Sub

    Protected Sub lkbPilihItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPilihItem.Click
        If Not Session("TblSN") Is Nothing Then
            Dim objTableSN As DataTable
            objTableSN = Session("TblSN")
            Dim dvjum As DataView = objTableSN.DefaultView
            dvjum.RowFilter = "itemoid= '" & KodeItem.Text & "'"
            If dvjum.Count = TextQty.Text Then
                dvjum.RowFilter = ""
                Dim dtab As New DataTable
                If ViewState("dtlsalesreturn") Is Nothing Then
                    dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemcode", Type.GetType("System.String"))
                    dtab.Columns.Add("itemname", Type.GetType("System.String"))
                    dtab.Columns.Add("qty", Type.GetType("System.Double"))
                    dtab.Columns.Add("unit", Type.GetType("System.String"))
                    dtab.Columns.Add("price", Type.GetType("System.Double"))
                    dtab.Columns.Add("note", Type.GetType("System.String"))
                    dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("netamt", Type.GetType("System.Double"))
                    dtab.Columns.Add("unitoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("unitseq", Type.GetType("System.Int32"))
                    dtab.Columns.Add("trnjualdtloid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("maxqty", Type.GetType("System.Double"))
                    dtab.Columns.Add("location", Type.GetType("System.String"))
                    dtab.Columns.Add("locationoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("promodisc", Type.GetType("System.Double"))
                    dtab.Columns.Add("promodiscamt", Type.GetType("System.Double"))
                    dtab.Columns.Add("itemloc", Type.GetType("System.Double"))
                    ViewState("dtlsalesreturn") = dtab
                Else
                    dtab = ViewState("dtlsalesreturn")
                End If
                If detailstate.Text = "new" Then
                    labelseq.Text = GVDtl.Rows.Count + 1
                    Dim drow As DataRow = dtab.NewRow
                    drow("seq") = Integer.Parse(labelseq.Text)
                    drow("itemcode") = itemcode.Text
                    drow("itemname") = itemname.Text
                    drow("qty") = ToDouble(qty.Text)
                    drow("unit") = unit.Text
                    drow("price") = ToDouble(priceperunit.Text)
                    drow("note") = notedtl.Text
                    drow("itemoid") = Integer.Parse(itemoid.Text)
                    drow("netamt") = ToDouble(totalamountdtl.Text)
                    drow("unitoid") = Integer.Parse(satuan1.Text)
                    drow("maxqty") = ToDouble(maxqty.Text)
                    drow("unitseq") = Integer.Parse(unitseq.Text)
                    drow("trnjualdtloid") = Integer.Parse(trnjualdtloid.Text)
                    drow("location") = fromlocation.SelectedItem.ToString
                    drow("locationoid") = fromlocation.SelectedValue
                    drow("promodisc") = ToDouble(promodisc.Text)
                    drow("promodiscamt") = ToDouble(promodiscamt.Text)
                    drow("itemloc") = ToDouble(itemloc.Text)
                    dtab.Rows.Add(drow)
                Else
                    Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "")
                    If drow.Length > 0 Then
                        drow(0).BeginEdit()
                        drow(0)("itemcode") = itemcode.Text
                        drow(0)("itemname") = itemname.Text
                        drow(0)("qty") = ToDouble(qty.Text)
                        drow(0)("unit") = unit.Text
                        drow(0)("price") = ToDouble(priceperunit.Text)
                        drow(0)("note") = notedtl.Text
                        drow(0)("itemoid") = Integer.Parse(itemoid.Text)
                        drow(0)("netamt") = ToDouble(totalamountdtl.Text)
                        drow(0)("unitoid") = Integer.Parse(satuan1.Text)
                        drow(0)("unitseq") = Integer.Parse(unitseq.Text)
                        drow(0)("maxqty") = ToDouble(maxqty.Text)
                        drow(0)("trnjualdtloid") = Integer.Parse(trnjualdtloid.Text)
                        drow(0)("location") = fromlocation.SelectedItem.ToString
                        drow(0)("locationoid") = fromlocation.SelectedValue
                        drow(0)("promodisc") = ToDouble(promodisc.Text)
                        drow(0)("promodiscamt") = ToDouble(promodiscamt.Text)
                        'drow(0)("itemloc") = ToDouble(itemloc.Text)
                        drow(0).EndEdit()
                    Else
                        showMessage("Sorry, missing something!", 2)
                        Exit Sub
                    End If
                End If
                dtab.AcceptChanges()
                dtab.Select("", "seq")

                GVDtl.DataSource = dtab
                GVDtl.DataBind()
                ViewState("dtlsalesreturn") = dtab

                itemname.Text = "" : itemoid.Text = ""
                itemcode.Text = "" :   qty.Text = "0.00"
                maxqty.Text = "0.00" : unit.Text = ""
                unitseq.Text = "" : satuan1.Text = ""
                priceperunit.Text = "0.00" : totalamountdtl.Text = "0.00"
                netamount.Text = "0.00" : notedtl.Text = ""
                promodisc.Text = "0.00" : promodiscamt.Text = "0.00"
                labelseq.Text = "" : detailstate.Text = "new"
                GVDtl.SelectedIndex = -1
                GVDtl.Columns(10).Visible = True
                calculateheader()
            Else
                LabelPesanSn.Text = "Jumlah Item Tidak Sesuai Dengan jumlah Serial Number"
                ModalPopupSN.Show()
                TextSN.Text = ""
                TextSN.Focus()
            End If
        Else
            LabelPesanSn.Text = "Mohon Masukan Kode Serial Number"
            ModalPopupSN.Show()
            TextSN.Text = ""
            TextSN.Focus()
        End If
    End Sub

    Protected Sub lkbCloseSN_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbCloseSN.Click
        '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@
        If Not Session("TblSN") Is Nothing Then
            Dim objTableSN As DataTable = Session("TblSN")
            Dim dvSN As DataView = objTableSN.DefaultView
            dvSN.RowFilter = "itemoid = " & GVDtl.DataKeyNames(0)
            For kdsn As Integer = 0 To dvSN.Count - 1
                objTableSN.Rows.RemoveAt(0)
            Next
        End If
        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCustomer.DataSource = Session("Cust")
        gvCustomer.PageIndex = e.NewPageIndex
        gvCustomer.DataBind()
    End Sub

    Protected Sub GVTrn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVTrn.SelectedIndexChanged
        Response.Redirect("~\Transaction\trnSalesReturn.aspx?branch_code=" & GVTrn.SelectedDataKey("branch_code").ToString & "&oid=" & GVTrn.SelectedDataKey("trnjualreturmstoid").ToString & "")
    End Sub

    Protected Sub ddlFromcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFromcabang.SelectedIndexChanged
        initallddl()
    End Sub

    Protected Sub tbperiodstart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbperiodstart.TextChanged
        CbPeriode.Checked = True
    End Sub

    Protected Sub GVItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItem.SelectedIndexChanged
        itemname.Text = GVItem.SelectedDataKey("itemdesc").ToString
        itemcode.Text = GVItem.SelectedDataKey("itemcode").ToString
        itemoid.Text = GVItem.SelectedDataKey("itemoid")
        qty.Enabled = True : qty.CssClass = "inpText"
        maxqty.Text = ToMaskEdit(ToDouble(GVItem.SelectedDataKey("Sisaqty")), 3)
        qty.Text = ToMaskEdit(ToDouble(GVItem.SelectedDataKey("Sisaqty")), 3)
        priceperunit.Text = ToMaskEdit(ToDouble(GVItem.SelectedDataKey("trnjualdtlprice")), 3)
        unit.Text = GVItem.SelectedDataKey("unit").ToString
        satuan1.Text = GVItem.SelectedDataKey("satuan1")
        unitseq.Text = GVItem.SelectedDataKey("unitseq")
        trnjualdtloid.Text = Integer.Parse(GVItem.SelectedDataKey("trnjualdtloid"))
        If Integer.Parse(JualMstOid.Text) < 0 Then
            itemloc.Text = fromlocation.SelectedValue
            priceperunit.Visible = True
            priceperunit.Enabled = True
            priceperunit.CssClass = "inpText"
        Else
            itemloc.Text = GVItem.SelectedDataKey("itemloc")
            fromlocation.SelectedValue = GVItem.SelectedDataKey("itemloc")
            priceperunit.Visible = True
            priceperunit.Enabled = False
        End If
        'initallddl()
        promooid.Text = Integer.Parse(GVItem.SelectedDataKey("promooid"))
        If promooid.Text <> 0 Then
            promodisc.Text = ToMaskEdit(GVItem.SelectedDataKey("promodiscamt"), 3)
            promodiscamt.Text = ToMaskEdit(promodisc.Text, 3)
        Else
            promodisc.Text = ToMaskEdit(GVItem.SelectedDataKey("promodisc"), 3)
            promodiscamt.Text = ToMaskEdit(qty.Text * promodisc.Text, 3)
        End If
        'totalamountdtl.Text = ToMaskEdit((qty.Text * priceperunit.Text) - (qty.Text * promodisc.Text), 3)
        GVItem.Visible = False
        calculatedetail()
    End Sub
#End Region
End Class