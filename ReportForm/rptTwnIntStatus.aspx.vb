Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptTWintStatus
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim fCabang As String = ""
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal message As String, ByVal sCaption As String, ByVal iType As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption
        Validasi.Text = message
        panelMsg.Visible = True
        btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("Maaf, Format tanggal salah..!!", CompnyName & "- Warning", 2)
            Exit Sub
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Maaf, periode 2 lebih besar dari periode 1..!!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        Try
            Dim StatusOs As String = ""
            Dim sWhere As String = " AND twm.Trfwhservicedate BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & " 23:59:59'"

            If ddlcabang.SelectedValue = "Pengirim" Then
                If dCabangNya.SelectedValue <> "ALL" Then
                    sWhere &= "And twm.FromBranch='" & dCabangNya.SelectedValue & "'"
                End If
            Else
                If dCabangNya.SelectedValue <> "ALL" Then
                    sWhere &= "And twm.ToBranch='" & dCabangNya.SelectedValue & "'"
                End If
            End If

            If KembaliOid.Text <> "" Then
                sWhere &= "AND tcm.trntcserviceoid = " & Integer.Parse(KembaliOid.Text) & ""
            End If

            If oid.Text <> "" Then
                sWhere &= "AND twm.Trfwhserviceoid = " & Integer.Parse(oid.Text) & ""
            End If

            If ReqOid.Text <> "" Then
                sWhere &= "AND twd.reqoid = " & Integer.Parse(ReqOid.Text) & ""
            End If

            If itemoid.Text <> "" Then
                sWhere &= "AND i.itemoid = " & Integer.Parse(itemoid.Text) & ""
            End If

            If statusDelivery.SelectedValue <> "ALL" Then
                If statusDelivery.SelectedValue = "COMPLETE" Then
                    StatusOs &= " And upper(StatusOs)='COMPLETE'"
                Else
                    StatusOs &= " And upper(StatusOs) <> 'COMPLETE'"
                End If
            End If

            vReport = New ReportDocument
            If tipe = "excel" Then
                vReport.Load(Server.MapPath("~\Report\rptTWIntstatusExl.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptTWIntstatus.rpt"))
            End If

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("StatusOs", StatusOs)
            vReport.SetParameterValue("Startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Status_TW_Internal" & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Status_TW_Internal" & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "- Warning", 2)
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Public Sub BindDataListItem()
        If dCabangNya.SelectedValue <> "ALL" Then
            fCabang = " AND twm.FromBranch='" & dCabangNya.SelectedValue & "'"
        End If
        sSql = "Select i.itemoid,i.itemcode,i.itemdesc,Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag From QL_mstitem i Where (itemdesc like '%" & Tchar(itemname.Text) & "%' OR i.itemcode LIKE '%" & Tchar(itemname.Text) & "%' OR i.merk like '%" & Tchar(itemname.Text) & "%') AND i.itemoid IN (Select twd.itemoid from ql_trfwhservicedtl twd Inner Join ql_trfwhservicemst twm ON twd.trfwhserviceoid=twm.Trfwhserviceoid Where twm.TrfwhserviceNo Like '%" & Tchar(nota.Text.Trim) & "%' AND Trfwhservicetype='INTERNAL'" & fCabang & ") AND i.itemflag='AKTIF'"
        FillGV(gvItem, sSql, "ql_mstitem")
        gvItem.Visible = True
    End Sub

    Public Sub bindDataListTcS()
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("Maaf, Format tanggal salah..!!", CompnyName & "- Warning", 2)
            Exit Sub
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Maaf, periode 2 lebih besar dari periode 1..!!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        sSql = "Select trntcserviceoid,trntcserviceno,Convert(Char(20),trntcservicedate,103) trntcservicedate,ca.gendesc CabangAsal,ct.gendesc,FromBranch,ToBranch From ql_trntcservicemst tcm Inner join QL_mstgen ca ON ca.gencode=tcm.FromBranch AND ca.gengroup='CABANG' Inner join QL_mstgen ct ON ct.gencode=tcm.FromBranch AND ct.gengroup='CABANG' Where tcm.trntcserviceno LIKE '%" & Tchar(NoKembali.Text) & "%' AND tcm.Trfwhserviceno LIKE '%" & Tchar(nota.Text) & "%' AND tcm.trntcservicedate Between '" & CDate(toDate(dateAwal.Text.Trim)) & "' AND '" & CDate(toDate(dateAkhir.Text.Trim)) & "' AND tcm.trntcservicestatus='POST'"

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= "AND tcm.branch_code='" & dCabangNya.SelectedValue & "'"
        End If

        If ddlcabang.SelectedValue = "Pengirim" Then
            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= "And tcm.FromBranch='" & dCabangNya.SelectedValue & "'"
            End If
        Else
            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= "And tcm.ToBranch='" & dCabangNya.SelectedValue & "'"
            End If
        End If

        sSql &= "Order by trntcserviceoid Desc"
        FillGV(Gvkem, sSql, "ql_trntcservicemst")
        Gvkem.Visible = True
    End Sub

    Public Sub bindDataListTWS()
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("Maaf, Format tanggal salah..!!", CompnyName & "- Warning", 2)
            Exit Sub
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Maaf, periode 2 lebih besar dari periode 1..!!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        sSql = "Select * from (Select twm.trfwhserviceoid,br.gendesc Cabang,twm.TrfwhserviceNo,Convert(Char(20),twm.Trfwhservicedate,103) Trfwhservicedate,twm.Trfwhservicestatus,twm.FromMtrLocOid,twm.ToMtrLocOId,br.gendesc +' - '+loc.gendesc Gudang,ct.gendesc CabangTuju,FromBranch,ToBranch From ql_trfwhservicemst twm Inner Join QL_mstgen br ON br.gencode=twm.FromBranch AND br.gengroup='CABANG' Inner Join QL_mstgen loc ON loc.genoid=twm.FromMtrLocOid AND loc.gengroup='LOCATION' Inner Join QL_mstgen ct ON ct.gencode=twm.ToBranch AND ct.gengroup='CABANG' Where Trfwhservicetype='INTERNAL' AND (twm.TrfwhserviceNo Like '%" & Tchar(nota.Text) & "%') AND twm.Trfwhservicedate Between '" & CDate(toDate(dateAwal.Text.Trim)) & "' AND '" & CDate(toDate(dateAkhir.Text.Trim)) & "') tw Where Trfwhservicestatus='APPROVED'"

        If ddlcabang.SelectedValue = "Pengirim" Then
            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= "And FromBranch='" & dCabangNya.SelectedValue & "'"
            End If
        Else
            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= "And ToBranch='" & dCabangNya.SelectedValue & "'"
            End If
        End If

        sSql &= " Order By Trfwhservicedate"
        FillGV(GVNota, sSql, "ql_trfwhservicemst")
        GVNota.Visible = True
    End Sub

    Private Sub BindReqTTS()
        Try
            sSql = "Select cb.gendesc,rq.branch_code,reqoid,reqcode,Convert(Char(20),rq.reqdate,103) ReqDate,c.custname From QL_TRNREQUEST rq Inner Join QL_mstgen cb ON cb.gencode=rq.branch_code AND cb.gengroup='CABANG' Inner Join QL_mstcust c ON c.custoid=rq.reqcustoid Where rq.cmpcode='" & CompnyCode & "' AND (rq.reqcode LIKE '%" & TcharNoTrim(ReqCode.Text) & "%' OR c.custcode LIKE '%" & TcharNoTrim(ReqCode.Text) & "%' OR c.custname LIKE '%" & TcharNoTrim(ReqCode.Text) & "%') AND rq.reqoid IN (Select reqoid from ql_trfwhservicedtl twd Inner Join ql_trfwhservicemst twm On twm.Trfwhserviceoid=twd.trfwhserviceoid Where twm.Trfwhservicetype='INTERNAL' AND twm.Trfwhservicedate Between '" & CDate(toDate(dateAwal.Text.Trim)) & "' AND '" & CDate(toDate(dateAkhir.Text.Trim)) & "'"
            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= " AND twm.FromBranch='" & dCabangNya.SelectedValue & "'"
            End If

            If oid.Text <> "" Then
                sSql &= " AND twm.trfwhserviceoid like '%" & Integer.Parse(oid.Text) & "%'"
            End If

            sSql &= " Group by reqoid) Order By rq.reqdate"
            FillGV(GVReq, sSql, "QL_TRNREQUEST")
            GVReq.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & "- Warning", 2)
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Event"
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item("itemdesc")
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptTwnIntStatus.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Servis Supplier Status"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            fDDLBranch()
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptTwnIntStatus.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showPrint("excel")
    End Sub

    Protected Sub btnSearchNota_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListTWS()
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        GVNota.PageIndex = e.NewPageIndex
        bindDataListTWS()
    End Sub

    Protected Sub GVNota_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVNota.SelectedIndexChanged
        crvMutasiStock.ReportSource = Nothing
        nota.Text = GVNota.SelectedDataKey.Item("TrfwhserviceNo")
        oid.Text = GVNota.SelectedDataKey.Item("trfwhserviceoid")
        'suppname.Text = GVNota.SelectedDataKey.Item("suppname")
        'suppoid.Text = GVNota.SelectedDataKey.Item("suppoid")
        GVNota.Visible = False
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nota.Text = "" : oid.Text = ""
        GVNota.Visible = False
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        showPrint("pdf")
    End Sub

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        btnValidasi.Visible = False
        panelMsg.Visible = False
    End Sub

    Protected Sub btnSearchKem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKem.Click
        bindDataListTcS()
    End Sub

    Protected Sub Gvkem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Gvkem.PageIndexChanging
        Gvkem.PageIndex = e.NewPageIndex
        bindDataListTcS()
    End Sub

    Protected Sub Gvkem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Gvkem.SelectedIndexChanged
        NoKembali.Text = Gvkem.SelectedDataKey.Item("trntcserviceno")
        KembaliOid.Text = Integer.Parse(Gvkem.SelectedDataKey.Item("trntcserviceoid"))
        Gvkem.Visible = False
    End Sub

    Protected Sub EraseKem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EraseKem.Click
        NoKembali.Text = "" : KembaliOid.Text = ""
        Gvkem.Visible = False
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        BindDataListItem()
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showPrint("")
    End Sub

    Protected Sub ddlcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcabang.SelectedIndexChanged
        nota.Text = "" : oid.Text = ""
        GVNota.Visible = False
    End Sub

    Protected Sub BtnSearchReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSearchReq.Click
        BindReqTTS()
    End Sub

    Protected Sub BtnEraseReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnEraseReq.Click
        ReqCode.Text = "" : ReqOid.Text = ""
        GVReq.Visible = False
    End Sub

    Protected Sub GVReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVReq.PageIndexChanging
        GVReq.PageIndex = e.NewPageIndex
        BindReqTTS()
    End Sub

    Protected Sub GVReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVReq.SelectedIndexChanged
        Try
            ReqCode.Text = GVReq.SelectedDataKey.Item("reqcode")
            ReqOid.Text = GVReq.SelectedDataKey("reqoid")
            GVReq.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & "- Warning", 2)
            Exit Sub
        End Try
    End Sub
#End Region
End Class
