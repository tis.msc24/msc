Imports System.Data
Imports System.Data.SqlClient
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure

Partial Class Transaction_trnAssignFakturPajak
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim sSql As String = ""
    Dim CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cmd As New SqlCommand("", conn)
    Dim cKon As New Koneksi
    Dim cFungsi As New ClassFunction
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedures"
    Private Sub ChangeAllSelection(ByVal gvTarget As GridView, ByVal iSelectValue As Integer)
        If Not Session("DataSI") Is Nothing Then
            Dim dtab As DataTable = Session("DataSI")

            If dtab.Rows.Count > 0 Then
                For i As Integer = 0 To dtab.Rows.Count - 1
                    dtab.Rows(i)("selected") = iSelectValue
                Next
                dtab.AcceptChanges()
            End If
            gvTarget.DataSource = dtab
            gvTarget.DataBind()
            Session("DataSI") = dtab
        End If
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("DataSI") Is Nothing Then
            Dim dtab As DataTable = Session("DataSI")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvSI.Rows.Count - 1
                    cb = gvSI.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "trnjualmstoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("DataSI") = dtab
        End If
    End Sub

    Private Sub showMessage(ByVal isi As String, ByVal judul As String)
        isiPesan.Text = isi
        captionPesan.Text = judul
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, True)
    End Sub

    Private Sub ClearSI()
        gvSI.DataSource = Nothing
        gvSI.DataBind()
        Session("DataSI") = Nothing
        gvSI.Visible = False
        btnAddSI.Visible = False
        btnCancelSI.Visible = False
        lblKodefaktur.Visible = False
        DDLKodeFaktur.Visible = False
    End Sub
#End Region

#Region "Functions"
    Public Function GetSelected() As Boolean
        Return (Eval("selected") = 1)
    End Function
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        ' jangan lupa cek hak akses
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xSetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xSetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xSetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xSetRole
            Response.Redirect("~/Transaction/trnAssignFakturPajak.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        btnPostingFaktur.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk POST data ini?');")
        Page.Title = CompnyName & " - Assign No Faktur"

        If Not Page.IsPostBack Then
            'gvBarcodeList.DataSource = Nothing
            'gvBarcodeList.DataBind()
            'tglAwal.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
            'tglAkhir.Text = Format(CDate(DateTime.Now.ToShortDateString()), "dd/MM/yyyy")
            tglAwal2.Text = Format(CDate(DateTime.Now.ToShortDateString()), "01/MM/yyyy")
            tglAkhir2.Text = Format(CDate(DateTime.Now.ToShortDateString()), "dd/MM/yyyy")
            'btnFind_Click(sender, e)
            'ddlFilter.SelectedIndex = 0
            FillDDL(DDLKodeFaktur, "select gencode a,gencode b FROM QL_mstgen WHERE gengroup='KODE_FAKTUR' ORDER BY gencode")
        End If
        TabContainer1.ActiveTabIndex = 1
        TabContainer1.Tabs(0).Enabled = False
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, False)
    End Sub

    Protected Sub btnFindSI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindSI.Click
        Dim sMsg As String = ""
        Try
            Dim dat1 As Date = CDate(toDate(tglAwal2.Text))
            Dim dat2 As Date = CDate(toDate(tglAkhir2.Text))
        Catch ex As Exception
            sMsg = "- Format filter tanggal salah.<BR>"
        End Try
        If CDate(toDate(tglAwal2.Text)) > CDate(toDate(tglAkhir2.Text)) Then
            sMsg = "- Tanggal Start Harus Lebih kecil dari pada Tanggal Akhir.<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING")
            Exit Sub
        End If

        sSql = "SELECT 0 AS selected,jm.branch_code,g.genother1 AS branch," & _
            "jm.trnjualmstoid,jm.trnjualno,jm.trnjualdate,c.custname,jm.trnamtjualnettoidr " & _
            "FROM ql_trnjualmst jm INNER JOIN QL_mstcust c on jm.trncustoid=c.custoid " & _
            "INNER JOIN QL_trnordermst om ON om.branch_code=jm.branch_code AND om.orderno=jm.orderno " & _
            "INNER JOIN QL_mstgen g ON g.gencode=jm.branch_code AND g.gengroup='CABANG' " & _
            "WHERE jm.trnjualstatus='POST' AND trnjualno like '" & DDLNoSI.SelectedValue & "' AND trnjualno LIKE '%" & Tchar(txtFilter2.Text) & "%' " & _
            "AND ISNULL(jm.trnjualref,'')='' AND jm.trnjualdate BETWEEN '" & CDate(toDate(tglAwal2.Text)) & " 0:0' AND '" & CDate(toDate(tglAkhir2.Text)) & " 23:59' " & _
            "ORDER BY jm.trnjualno"
        Dim dtSI As DataTable = GetDataTable(sSql, "ql_trnjualmst")
        gvSI.DataSource = dtSI
        gvSI.DataBind()
        Session("DataSI") = dtSI
        gvSI.Visible = True
        btnAddSI.Visible = True
        btnCancelSI.Visible = True
        lblKodefaktur.Visible = True
        DDLKodeFaktur.Visible = True
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click
        ChangeAllSelection(gvSI, 1)
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectNone.Click
        ChangeAllSelection(gvSI, 0)
    End Sub

    Protected Sub gvSI_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSI.PageIndexChanging
        UpdateCheckedGV()
        gvSI.PageIndex = e.NewPageIndex
        Dim dtItem As DataTable = Session("DataSI")
        gvSI.DataSource = dtItem
        gvSI.DataBind()
    End Sub

    Protected Sub gvSI_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSI.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy hh:mm")
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub btnAddSI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSI.Click
        UpdateCheckedGV()

        sSql = "SELECT jm.branch_code,g.genother1 AS branch," & _
            "jm.trnjualmstoid,jm.trnjualno,jm.trnjualdate,c.custname,jm.trnamtjualnettoidr,jm.trnjualref,'' kode_faktur,'' dtlfakturnofaktur " & _
            "FROM ql_trnjualmst jm INNER JOIN QL_mstcust c on jm.trncustoid=c.custoid " & _
            "INNER JOIN QL_trnordermst om ON om.branch_code=jm.branch_code AND om.orderno=jm.orderno " & _
            "INNER JOIN QL_mstgen g ON g.gencode=jm.branch_code AND g.gengroup='CABANG' " & _
            "WHERE jm.trnjualmstoid=0 " & _
            "ORDER BY jm.trnjualno"
        Dim dtFaktur As DataTable = GetDataTable(sSql, "ql_trnjualmst")
        dtFaktur.Rows.Clear()

        Dim dtSI As New DataTable : Dim dvSI As DataView : Dim iNotaCount As Integer = 0
        If Not Session("DataSI") Is Nothing Then
            dtSI = Session("DataSI")
            dvSI = dtSI.DefaultView
            dvSI.RowFilter = "selected=1"
            iNotaCount = dvSI.Count
        End If

        Dim sMsg As String = ""
        Dim dtNoFaktur As New DataTable
        If iNotaCount > 0 Then
            sSql = "SELECT TOP " & iNotaCount & " dtlfakturnofaktur from QL_mstfakturpajakdtl d " & _
                "INNER JOIN QL_mstfakturpajak m ON m.mstfakturoid=d.mstfakturoid " & _
                "where d.dtlfakturstatus='OPEN' AND m.mstfakturstatus='ACTIVE' " & _
                "and dtlfakturnofaktur not in (select right(trnjualref,15) from QL_trnjualmst) " & _
                "order by m.mstfakturoid,dtlfakturoid"
            dtNoFaktur = GetDataTable(sSql, "ql_trnjualmst")
            If DDLKodeFaktur.Items.Count < 1 Then
                sMsg &= "- Tidak ada Kode Faktur yang tersedia. Silakan isi Master General Group 'KODE_FAKTUR' dahulu.<BR>"
            End If
            If dtNoFaktur.Rows.Count < 1 Then
                sMsg &= "- Tidak ada Master Nomor Faktur Pajak yang tersedia.<BR>"
            Else
                If dtNoFaktur.Rows.Count <> iNotaCount Then
                    sMsg &= "- Jumlah Master Nomor Faktur Pajak yang tersedia tidak mencukupi. (" & iNotaCount & "/" & dtNoFaktur.Rows.Count & ").<BR>"
                End If
            End If
        Else
            sMsg &= "- Tidak ada Sales Invoice yang dipilih.<BR>"
        End If
        
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING")
            Exit Sub
        End If

        For C1 As Integer = 0 To iNotaCount - 1
            Dim oRow As DataRow = dtFaktur.NewRow
            oRow("branch_code") = dvSI(C1)("branch_code").ToString
            oRow("branch") = dvSI(C1)("branch").ToString
            oRow("trnjualmstoid") = dvSI(C1)("trnjualmstoid").ToString
            oRow("trnjualno") = dvSI(C1)("trnjualno").ToString
            oRow("trnjualdate") = CDate(dvSI(C1)("trnjualdate").ToString)
            oRow("custname") = dvSI(C1)("custname").ToString
            oRow("trnamtjualnettoidr") = ToDouble(dvSI(C1)("trnamtjualnettoidr").ToString)
            oRow("trnjualref") = DDLKodeFaktur.SelectedValue.ToString & "." & dtNoFaktur.Rows(C1)("dtlfakturnofaktur").ToString
            oRow("kode_faktur") = DDLKodeFaktur.SelectedValue.ToString
            oRow("dtlfakturnofaktur") = dtNoFaktur.Rows(C1)("dtlfakturnofaktur").ToString
            dtFaktur.Rows.Add(oRow)
        Next
        dvSI.RowFilter = ""

        Session("Faktur") = dtFaktur
        gvFaktur.DataSource = dtFaktur
        gvFaktur.DataBind()
        ClearSI()
    End Sub

    Protected Sub btnCancelSI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSI.Click
        ClearSI()
    End Sub

    Protected Sub gvFaktur_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFaktur.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy hh:mm")
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~/Transaction/trnAssignFakturPajak.aspx?awal=true")
    End Sub

    Protected Sub btnPostingFaktur_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPostingFaktur.Click
        Dim sMsg As String = ""
        If Session("Faktur") Is Nothing Then
            sMsg &= "- Tidak ada data yang akan di-POSTING."
        Else
            Dim objTableCek As DataTable = Session("Faktur")
            If objTableCek.Rows.Count <= 0 Then
                sMsg &= "- Tidak ada data yang akan di-POSTING."
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - Warning")
            Exit Sub
        End If

        Dim objTable As DataTable = Session("Faktur")

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        cmd.Transaction = objTrans
        Try
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                sSql = "UPDATE QL_mstfakturpajakdtl set dtlfakturstatus='CLOSE',dtlfakturreftype='SI',dtlfakturrefno='" & objTable.Rows(C1)("trnjualno").ToString & "'," & _
                    "upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP " & _
                    "where dtlfakturnofaktur = '" & objTable.Rows(C1)("dtlfakturnofaktur").ToString & "'"
                cmd.CommandText = sSql
                cmd.ExecuteNonQuery()

                sSql = "UPDATE ql_trnjualmst set trnjualref='" & objTable.Rows(C1)("trnjualref").ToString & "'," & _
                    "upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP " & _
                    "where branch_code='" & objTable.Rows(C1)("branch_code").ToString & "' AND trnjualmstoid = '" & objTable.Rows(C1)("trnjualmstoid").ToString & "'"
                cmd.CommandText = sSql
                cmd.ExecuteNonQuery()
            Next

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - Error")
            Exit Sub
        End Try
        Response.Redirect("~/Transaction/trnAssignFakturPajak.aspx?awal=true")
    End Sub
#End Region

#Region "Unused"
    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlFilter.SelectedValue <> "spmno" Then
            tglAwal.Enabled = True
            tglAwal.CssClass = "inpText"
            tglAkhir.Enabled = True
            tglAkhir.CssClass = "inpText"
        Else
            tglAwal.Enabled = False
            tglAwal.CssClass = "inpTextDisabled"
            tglAkhir.Enabled = False
            tglAkhir.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        tglAwal.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        tglAkhir.Text = Format(Now, "dd/MM/yyyy")
        ddlFilter.SelectedIndex = 0
        txtFilter.Text = ""

        Dim sQuery As String = ""
        sQuery &= " AND " & ddlFilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%' AND dateclosed BETWEEN '" & CDate(toDate(tglAwal.Text)) & " 00:00:00' AND '" & CDate(toDate(tglAkhir.Text)) & " 23:59:59' "
        sQuery &= " AND m.reqstatus in ('SPKCLOSE','In') "
        bindData(sQuery, gvClosedBPM)
    End Sub

    Protected Sub gvClosedBPM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvClosedBPM.PageIndexChanging
        gvClosedBPM.PageIndex = e.NewPageIndex
        bindData("", gvClosedBPM)
    End Sub

    Protected Sub gvClosedBPM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvClosedBPM_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvClosedBPM.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")

            Dim objGrid As GridView = CType(e.Row.Cells(4).FindControl("gvMR2"), GridView)
            sSql = "select m.spkno " & _
                   "from ql_trnrequest mst " & _
                   "inner join QL_trnspkResultItem dtl on mst.reqoid=dtl.iodtloid " & _
                   "inner join QL_trnspkmainprod m on m.spkmainoid=dtl.spkmainoid " & _
                   "where mst.reqcode='" & e.Row.Cells(0).Text & "' "
            Dim ods As New SqlDataAdapter(sSql, conn)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)
            objGrid.DataSource = objTablee.DefaultView
            objGrid.DataBind()
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sQuery As String = ""
        Try
            Dim dat1 As Date = CDate(toDate(tglAwal.Text))
            Dim dat2 As Date = CDate(toDate(tglAkhir.Text))
        Catch ex As Exception
            showMessage("Filter tanggal salah!!", CompnyName & " - Warning") : Exit Sub
        End Try
        If CDate(toDate(tglAwal.Text)) > CDate(toDate(tglAkhir.Text)) Then
            showMessage("Tanggal Start Harus Lebih kecil dari pada Tanggal Akhir !!", CompnyName & " - Warning") : Exit Sub
        End If

        sQuery &= " AND " & ddlFilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%' AND dateclosed BETWEEN '" & CDate(toDate(tglAwal.Text)) & " 00:00:00' AND '" & CDate(toDate(tglAkhir.Text)) & " 23:59:59' "

        sQuery &= " AND m.reqstatus in ('SPKCLOSE','In') and userclosed <> '' "
        bindData(sQuery, gvClosedBPM)
    End Sub

    Private Sub bindData(ByVal moreQuery As String, ByVal gvTarget As GridView)
        sSql = "SELECT distinct m.reqoid spmoid, m.reqdate spmdate,noteclosed spmnoteclosed,dateclosed spmdateclosed,userclosed spmuserclosed," & _
                 " m.reqcode spmno, '' spmnote, m.reqstatus spmstatus,barcode machine,m.updtime " & _
                 " FROM ql_trnrequest m "

        sSql = sSql & " WHERE m.cmpcode LIKE '%" & CompnyCode & "%' " & _
        moreQuery & " " & _
        "ORDER BY m.updtime desc"
        FillGV(gvTarget, sSql, "ql_trnspmmst")
        gvTarget.Visible = True
    End Sub
#End Region

End Class
