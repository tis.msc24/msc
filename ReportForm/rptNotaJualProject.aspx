<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptNotaJualProject.aspx.vb" Inherits="rptNotaJual" title="Toko Ali - Laporan Nota Penjualan" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Sales Invoice/Nota Jual Project"></asp:Label></th>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 979px" align=center>&nbsp;&nbsp;&nbsp;&nbsp; <TABLE><TBODY><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right><asp:Label id="Label2" runat="server" Text="Tipe Laporan :" __designer:wfdid="w81"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="type" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w82" AutoPostBack="True"><asp:ListItem Value="rptnotajualSum.rpt">Summary</asp:ListItem>
<asp:ListItem Value="rptnotajualDetail.rpt">Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="tdPeriod1" align=right runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode : " __designer:wfdid="w83"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w84"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w85"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w86"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w87"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w88">(dd/MM/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w89" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w90" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w91" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w92" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="TD1" align=right runat="server" Visible="true">No Nota :</TD><TD id="TD2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="nota" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w93"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" onclick="btnSearchNota_Click1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w94"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w95"></asp:ImageButton>&nbsp;<asp:Label id="oid" runat="server" __designer:wfdid="w96" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="tdperiod3" align=right runat="server" Visible="true"></TD><TD id="tdperiod4" align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="GVNota" runat="server" Width="600px" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w1" BorderColor="#DEDFDE" OnSelectedIndexChanged="GVNota_SelectedIndexChanged1" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="trnjualmstoid,trnjualno,trncustname,trnjualstatus,trncustoid" PageSize="5" UseAccessibleHeader="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="No Nota"></asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Date"></asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer"></asp:BoundField>
<asp:BoundField DataField="trnjualstatus" HeaderText="Status"></asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right runat="server" Visible="true">Note Nota :</TD><TD align=left runat="server" Visible="true"><asp:TextBox id="note" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox></TD><TD align=right runat="server" Visible="true"></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="TD6" align=right runat="server" Visible="false">Lokasi :</TD><TD id="TD5" align=left runat="server" Visible="false"><asp:DropDownList id="ddlLocation" runat="server" Width="236px" CssClass="inpText" __designer:wfdid="w98" AutoPostBack="True"><asp:ListItem>ALL LOCATION</asp:ListItem>
</asp:DropDownList></TD><TD id="TD7" align=right runat="server" Visible="false"></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right><asp:Label id="Label3" runat="server" Text="Item / Barang :" __designer:wfdid="w99"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="itemname" runat="server" Width="238px" CssClass="inpText" __designer:wfdid="w100"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click1" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="17px" __designer:wfdid="w101"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w102"></asp:ImageButton>&nbsp;*just for detail<asp:Label id="itemoid" runat="server" __designer:wfdid="w349" Visible="False"></asp:Label>&nbsp;</TD></TR><TR><TD style="WIDTH: 164px; HEIGHT: 1px" align=right></TD><TD align=left colSpan=3><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w2" BorderColor="#DEDFDE" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3" PageSize="12" UseAccessibleHeader="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4"><Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Width="50px" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Width="50px" ForeColor="Navy" HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang"></asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang"></asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga"></asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga"></asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga"></asp:BoundField>
</Columns>

<RowStyle BackColor="#F7F7DE"></RowStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<PagerStyle HorizontalAlign="Right"></PagerStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Customer :</TD><TD align=left colSpan=3><asp:TextBox id="custname" runat="server" Width="238px" CssClass="inpText" __designer:wfdid="w105"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCustomer" onclick="btnSearchCustomer_click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w106"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseCustomer" onclick="btnEraseCustomer_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w107"></asp:ImageButton>&nbsp;<asp:Label id="custoid" runat="server" __designer:wfdid="w354" Visible="False">custoid</asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD align=left colSpan=3><asp:GridView id="gvCust" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w3" BorderColor="#DEDFDE" OnSelectedIndexChanged="gvCust_SelectedIndexChanged1" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="custcode,custoid,custname" PageSize="12" UseAccessibleHeader="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Kode Customer"></asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama Customer"></asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address"></asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Status :</TD><TD align=left><asp:DropDownList id="status" runat="server" Width="114px" CssClass="inpText" __designer:wfdid="w4"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList></TD><TD align=right><asp:DropDownList id="FilterDDLGrup" runat="server" Width="236px" CssClass="inpText" __designer:wfdid="w110" Visible="False"></asp:DropDownList></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="WIDTH: 126px" align=left>&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList id="period" runat="server" Width="135px" CssClass="inpText" __designer:wfdid="w111" Visible="False"></asp:DropDownList> </TD><TD align=right><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="102px" CssClass="inpText" __designer:wfdid="w112" Visible="False"></asp:DropDownList>&nbsp;</TD><TD align=left></TD></TR></TBODY></TABLE><asp:Label id="labelEx" runat="server" Width="98px" __designer:wfdid="w113"></asp:Label></TD></TR><TR><TD style="WIDTH: 979px" align=center><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w114"></asp:Label></TD></TR><TR><TD style="WIDTH: 979px; HEIGHT: 32px" align=center><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w115"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w116"></asp:ImageButton> <asp:ImageButton id="ibpdf" onclick="BTNpRINT_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w117"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w118"></asp:ImageButton><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w119"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Sedang proses....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w120"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD style="WIDTH: 979px" align=center><TABLE><TBODY><TR><TD style="HEIGHT: 59px" align=left><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" Width="350px" Height="50px" __designer:wfdid="w121" ShowAllPageIds="True" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibpdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</asp:Content>

