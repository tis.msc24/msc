<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="AdjHpp.aspx.vb" Inherits="AdjHpp" title="" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" colspan="2" valign="middle">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Navy"
                    Text=".: ADJUSTMENT HPP" Font-Size="X-Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Adj. Hpp :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlList" runat="server" Width="100%" __designer:wfdid="w230" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 10%" align=left><asp:Label id="Label4x" runat="server" Text="Filter" __designer:wfdid="w231"></asp:Label></TD><TD style="WIDTH: 2%" align=center>:</TD><TD align=left colSpan=2><asp:DropDownList id="FilterDDLMst" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w232"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterTextMst" runat="server" Width="156px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w233"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 10%" align=left>Jenis</TD><TD style="WIDTH: 2%" align=center>:</TD><TD align=left colSpan=2><asp:DropDownList id="JenisDDL" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w234"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w235"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w236"></asp:ImageButton> <asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w237" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4><asp:GridView id="gvList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w226" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" AllowPaging="True" AllowSorting="True" DataKeyNames="itemcode">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Kode"><ItemTemplate>
<asp:LinkButton id="lkbSelect" onclick="lkbSelect_Click" runat="server" Font-Size="8pt" Text='<%# Eval("itemcode") %>' __designer:wfdid="w67" ToolTip="<%# GetTransID() %>" CommandName=''></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="hpp" HeaderText="Hpp">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="No data found."></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" align=left colSpan=4><asp:Label id="lblViewInfo" runat="server" Font-Size="8pt" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w239"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="imgForm" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Adj. Hpp :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left colSpan=6><asp:Button id="BtnAddItem" runat="server" CssClass="green" Font-Size="8pt" Font-Bold="True" Text="Add Katalog" __designer:wfdid="w187"></asp:Button>&nbsp;</TD></TR><TR><TD class="Label" align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 300px; BACKGROUND-COLOR: beige"><asp:GridView id="gvFindCrd" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w188" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode ">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="hpp" HeaderText="Last HPP">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="NewHpp" HeaderText="New Hpp">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="keterangan" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SaldoAkhir" HeaderText="Stok Akhir">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="COLOR: #585858" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w189"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w190"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" align=left colSpan=6>Last Updated By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w191">-</asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w192">-</asp:Label></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="imbSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w193"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w194"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w195" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w196" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w197"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <asp:UpdatePanel id="upListMat" runat="server" __designer:dtid="281474976710683" __designer:wfdid="w198"><ContentTemplate __designer:dtid="281474976710684">
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" __designer:wfdid="w199" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Katalog" __designer:wfdid="w200"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" __designer:wfdid="w201" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblFilterInListMat" runat="server" Font-Size="8pt" Text="Filter :" __designer:wfdid="w202"></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w203"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Enabled="False">Hpp</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w204"></asp:TextBox></TD></TR><TR><TD align=center colSpan=3>Jenis : <asp:DropDownList id="ddlType" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w205"></asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w206"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w207"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w208"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w209"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w210"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 875px; HEIGHT: 300px; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w211" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbSelectMat" runat="server" __designer:wfdid="w157" Checked='<%# eval("CheckValue") %>' ToolTip='<%# eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="hpp" HeaderText="HPP">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="New Hpp"><ItemTemplate>
<asp:TextBox id="pExpedisi" runat="server" Width="98px" CssClass="inpText" Text='<%# eval("NewHpp") %>' __designer:wfdid="w266" MaxLength="18" OnTextChanged="pExpedisi_TextChanged"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTBpExpedisi" runat="server" __designer:wfdid="w267" ValidChars="1234567890." TargetControlID="pExpedisi"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
                                            <asp:TextBox ID="tbMatNote" runat="server" CssClass="inpText" MaxLength="100" Text='<%# eval("keterangan") %>'
                                                Width="150px"></asp:TextBox>
                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="SaldoAkhir" HeaderText="Stok Akhir">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddListMat" runat="server" Font-Bold="True" __designer:wfdid="w212">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListMat" runat="server" Font-Bold="True" __designer:wfdid="w213">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" __designer:wfdid="w214" Drag="True" PopupDragHandleControlID="lblListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat" TargetControlID="btnHideListMat">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" __designer:wfdid="w215" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE><DIV>&nbsp;</DIV>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbSave"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> &nbsp;
</ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

