'Developt By Unknown
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnNotaHR
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim sql1 As String = ""
    Dim cProc As New ClassProcedure
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Private ws As DataTable
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
    Dim cRate As New ClassRate
    Dim cKon As New Koneksi
    Dim dtotal As Double
#End Region

#Region "Prosedure"
    Protected Sub ClearDtlVoucher()
        trnbelino.Text = "" : trnbelioid.Text = "" : suppnota.Text = ""
        AmtPI.Text = "0.00" : amtuse.Text = "0.00" : amtbalance.Text = "0.00" : AmtVdtl.Text = "0.00"
        addcostacctgoid.SelectedIndex = 0 : addcostamt.Text = ""
        cashbanknote.Text = ""
        I_Udtl.Text = "New Detail" : labelseq.Text = (GVvOucherDtl.Rows.Count + 1).ToString
        GVvOucherDtl.Visible = False
    End Sub

    Protected Sub HitungDtlAmt()
        Dim iCountrow As Integer = GVvOucherDtl.Rows.Count
        Dim iGtotal As Decimal = 0 : Dim SisaTotal As Decimal = 0
        Dim dt_temp As New DataTable : dt_temp = Session("QL_trnnotahrdtl")
        For i As Integer = 0 To dt_temp.Rows.Count - 1
            iGtotal += ToDouble(dt_temp.Rows(i)("amtvoucher").ToString)
            SisaTotal += ToDouble(dt_temp.Rows(i)("addcostamt").ToString)
            dt_temp.Rows(i).BeginEdit()
            dt_temp.Rows(i).Item(0) = i + 1
            dt_temp.Rows(i).EndEdit()
        Next
        Session("QL_trnnotahrdtl") = dt_temp
        GVvOucherDtl.DataSource = dt_temp
        GVvOucherDtl.DataBind()
        lblTampung.Text = ToMaskEdit(ToDouble(iGtotal), 4)
        SisaAmtDtl.Text = ToMaskEdit(ToDouble(SisaTotal), 4)
    End Sub

    Public Overloads Shared Function CheckYearIsValid(ByVal sdate As Date) As Boolean
        Dim min As Integer = 2000 : Dim max As Integer = 2072
        CheckYearIsValid = False
        If sdate.Year < min Then
            CheckYearIsValid = True
        End If
        If sdate.Year > max Then
            CheckYearIsValid = True
        End If
        Return CheckYearIsValid
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindPI()
        ' 2018/08/21 - Update  Multi supplier dalam 1 Nota HR
        Try
            sSql = "Select * from (Select trnbelimstoid, trnbelino, CONVERT(VARCHAR(20), trnbelidate, 103) AS trnbelidate, (select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.trnbelidtldisctype2 = 'VCR' ) AS Voucher, ISNULL((select SUM(trnnotahrdtlamtidr) from QL_trnnotahrdtl nd where nd.trnbelimstoid = bm.trnbelimstoid),0) AS usevoucher, (select SUM(bd.amtdisc2) from QL_trnbelidtl bd Where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.trnbelidtldisctype2 = 'VCR' ) - ISNULL((select SUM(trnnotahrdtlamtidr) + SUM(selisihamtidr) From QL_trnnotahrdtl nd where nd.trnbelimstoid = bm.trnbelimstoid),0) AS sisavoucher, s.suppname from QL_trnbelimst bm INNER JOIN QL_mstsupp s ON s.suppoid=bm.trnsuppoid where bm.trnbelimstoid > 0 and trnbelimstoid in (Select trnbelimstoid from QL_trnbelidtl Where (amtdisc2 > 0 or amtdisc2idr > 0)) And bm.trnbelino like '%" & trnbelino.Text & "%') nt where sisavoucher > 0"
            Dim dtVo As DataTable = GetDataTable(sSql, "QL_trnbelimst")
            Session("tblPI") = dtVo : GvVoucher.DataSource = dtVo
            GvVoucher.DataBind() : GvVoucher.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindSupplier()
        ' 20180821 - Update multisupplier jadi supplier tidak dibatasi
        Try
            sSql = "SELECT suppoid, suppcode, suppname FROM QL_mstsupp WHERE suppflag='ACTIVE' AND (suppcode LIKE '%" & TcharNoTrim(suppname.Text) & "%' OR suppname LIKE '%" & TcharNoTrim(suppname.Text) & "%') ORDER BY suppname"
            Dim dtPo As DataTable = GetDataTable(sSql, "QL_mstsupp")
            Session("tblSupp") = dtPo : gvPO.DataSource = dtPo
            gvPO.DataBind() : gvPO.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Public Sub binddata(ByVal sType As String)
        Try
            sSql = "Select 0 Selected, nm.cmpcode, nm.trnnotahroid, nm.trnnotahrno, CONVERT(Char(10),nm.trnnotahrdate,103) AS trnnotahrdate, trnnotahrstatus, ISNULL(s.suppoid,0) suppoid, ISNULL(s.suppname,'-') suppname, trnnotahramtnettoidr realisasiamt, selisihhramtidr AS selisihamt, trnnotahrstatus FROM QL_trnnotahrmst nm LEFT JOIN QL_mstsupp s ON s.suppoid=nm.suppoid WHERE nm.trnnotahroid>0 " & sType & " ORDER BY trnnotahroid DESC"
            Dim objTable As DataTable = GetDataTable(sSql, "QL_trnnotahrmst")
            Session("tbldata") = objTable : gvmstcost.DataSource = objTable
            gvmstcost.DataBind() : UnabledCheckBox()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Public Sub bindLastSearched()
        If (Not Session("SearchReceipt") Is Nothing) Or (Not Session("SearchReceipt") = "") Then
            Dim mySqlConn As New SqlConnection(ConnStr)
            Dim sqlSelect As String = Session("SearchReceipt")
            Dim objTable As DataTable = ClassFunction.GetDataTable(sqlSelect, "Receipt")
            Session("tbldata") = objTable : gvmstcost.DataSource = objTable
            gvmstcost.DataBind() : UnabledCheckBox()
            mySqlConn.Close()
        End If
    End Sub

    Private Sub BindCOA(ByVal sFilter As String)
        'FillGVAcctg(GvAccCost, "VAR_RECEIPT1", "MSC", sFilter)
        'btnHidden.Visible = True : Panel1.Visible = True
        'GvAccCost.Visible = True : Mpe1.Show()
    End Sub

    Public Sub filltextbox(ByVal sCmpcode As String, ByVal vpayid As String)
        Try
            sSql = "Select nm.cmpcode, nm.trnnotahroid, nm.trnnotahrno, nm.trnnotahrdate, nm.trnnotahrstatus, ISNULL(s.suppoid,0) suppoid, ISNULL(s.suppname,'-') suppname, nm.trnnotahramtnettoidr, nm.selisihhramtidr, nm.trnnotahrnote, nm.upduser, nm.updtime FROM QL_trnnotahrmst nm LEFT JOIN QL_mstsupp s ON s.suppoid = nm.suppoid Where nm.trnnotahroid>0 AND nm.trnnotahroid = " & vpayid & ""

            If objConn.State = ConnectionState.Closed Then
                objConn.Open()
            End If
            xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    notaHROid.Text = Trim(xreader("trnnotahroid"))
                    RealisasiNo.Text = xreader("trnnotahrno").ToString
                    Realisasidate.Text = Format(xreader("trnnotahrdate"), "dd/MM/yyyy")
                    NoteHdr.Text = xreader("trnnotahrnote").ToString
                    suppname.Text = xreader("suppname").ToString
                    suppname.Enabled = False : suppname.CssClass = "inpTextDisabled"
                    RealisasiNo.Enabled = False : RealisasiNo.CssClass = "inpTextDisabled"
                    btnCariSupplier.Visible = False : EraseSuppbtn.Visible = False
                    lblPOST.Text = xreader("trnnotahrstatus").ToString
                    supplierOid.Text = xreader("suppoid")
                    upduser.Text = "Last Update By <B>" & Trim(xreader("upduser")) & "</B> On <B>" & Trim(xreader("updtime")) & "</B> "
                End While
            End If
            objConn.Close()
            sSql = "Select nd.trnnotahrdtlseq AS seqdtl, nd.trnbelimstoid, bm.trnbelino, nd.trnnotahrdtlamtidr AS amtvoucher, nd.selisihamtidr AS amtselisih, nd.trnnotahrdtlnote, nd.trnnotahrdtlres1 AS acctgoid, nd.trnnotahrdtlres2 AS flagselisih,(select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = nd.trnbelimstoid and bd.trnbelidtldisctype2 = 'VCR') AS totalvoucher,s.suppname,nd.addcostacctgoid,nd.addcostamt From QL_trnnotahrdtl nd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid = nd.trnbelimstoid INNER JOIN QL_mstsupp s ON s.suppoid=bm.trnsuppoid Where nd.trnnotahroid=" & vpayid & ""
            Dim dtab As DataTable = cKon.ambiltabel(sSql, "QL_trnnotahrdtl")
            GVvOucherDtl.DataSource = dtab : GVvOucherDtl.DataBind()
            Session("QL_trnnotahrdtl") = dtab
            HitungDtlAmt()

            If lblPOST.Text <> "In Process" Then
                lblcashbankno.Visible = True : drafno.Visible = False
                RealisasiNo.Visible = True : notaHROid.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GenerateNoRealisasi()
        If i_u2.Text = "New Data" Then
            Dim iCurID As Integer = 0
            Dim Cabang As String = GetStrData("select substring (gendesc,1,3) from ql_mstgen Where gencode='" & Session("branch_id") & "' AND gengroup='CABANG'")
            Session("vNoCashBank") = "HR/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT MAX(replace(trnnotahrno,'" & Session("vNoCashBank") & "','')) trnnotahrno FROM ql_trnnotahrmst WHERE trnnotahrno LIKE '" & Session("vNoCashBank") & "%' AND cmpcode='" & CompnyCode & "'"

            If Not IsDBNull(GetScalar(sSql)) Then
                iCurID = GetScalar(sSql) + 1
            Else
                iCurID = 1
            End If
            Session("sNo") = GenNumberString(Session("vNoCashBank"), "", iCurID, 4)
        End If
    End Sub

    Public Sub CheckAll()
        Dim objdt As DataTable = Session("tbldata")
        If objdt Is Nothing Then
            showMessage("- No Found Data Click Button Find Or View All..!!<br>", 2)
            Exit Sub
        End If
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For C1 As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(C1)("cashbankstatus").ToString.ToUpper) <> "POST" Then
                    objRow(C1)("selected") = 1
                    objTable.AcceptChanges()
                End If
            Next
        End If
        Session("tbldata") = objTable
        gvmstcost.DataSource = objTable
        gvmstcost.DataBind()
    End Sub

    Public Sub UnabledCheckBox()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (gvmstcost.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("cashbankstatus").ToString) = "Post" Or Trim(objRow(C1)("cashbankstatus").ToString) = "DELETE" Then

                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Dim objdt As DataTable = Session("tbldata")
        If objdt Is Nothing Then
            showMessage("- Not Found Data Click Button Find Or View All..!!<br>", 2)
            Exit Sub
        End If
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For C1 As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(C1)("cashbankstatus").ToString) <> "POST" Then
                    objRow(C1)("selected") = 0
                    objTable.AcceptChanges()
                End If
            Next
        End If
        Session("tbldata") = objTable
        gvmstcost.DataSource = objTable
        gvmstcost.DataBind()
    End Sub

    Private Sub UpdateCheckedPost()
        'Untuk mendapat cek box value multi select
        Dim dv As DataView = Session("tbldata").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvmstcost.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        Dim a As String() = sOid.Split(",")
                        dv.RowFilter = "cashbankoid=" & a(1) & " AND cmpcode='" & a(0) & "'"
                        If cbcheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub ddlCOACb(ByVal cVar As String)
        FillDDLAcctg(DdlAkun, cVar, Session("branch_id"))
    End Sub

    Private Sub InitAddCost()
        FillDDLAcctg(addcostacctgoid, "VAR_SELISIH_HR+", Session("branch_id"))
        If addcostacctgoid.Items.Count = 0 Then
            showMessage("Silahkan seting akun Biaya Tambahan untuk VAR_SELISIH_HR+ di master interface !!", 2)
            Exit Sub
        End If
    End Sub

    Private Sub InitOtherAcctg(ByVal sType As String)
        If sType = "+" Then
            FillDDLAcctg(ddlSelisihCOA, "VAR_SELISIH_HR+", Session("branch_id"))
            If ddlSelisihCOA.Items.Count = 0 Then
                showMessage("Silahkan seting VAR_SELISIH_HR+ di master interface !!", 2)
                Exit Sub
            End If
        ElseIf sType = "-" Then
            FillDDLAcctg(ddlSelisihCOA, "VAR_SELISIH_HR-", Session("branch_id"))
            If ddlSelisihCOA.Items.Count = 0 Then
                showMessage("Silahkan seting VAR_SELISIH_HR- di master interface !!", 2)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub CalculateTotalPayment()
        AmtVdtl.Text = ToMaskEdit(ToDouble(AmtVdtl.Text), 4)
        
        If ToDouble(AmtVdtl.Text) > ToDouble(amtbalance.Text) Then ' Kurang
            cbSelisih.Enabled = False
            cbSelisih.Checked = True : ddlSelisih.SelectedValue = "+"
            SetOtherAccount(True)
            ddlSelisih_SelectedIndexChanged(Nothing, Nothing)
            selisih.Text = ToMaskEdit(ToDouble(AmtVdtl.Text) - ToDouble(amtbalance.Text), 4)
            totalselisih.Text = selisih.Text
        ElseIf ToDouble(AmtVdtl.Text) < ToDouble(amtbalance.Text) Then ' Lebih
            cbSelisih.Enabled = True
            cbSelisih.Checked = False : ddlSelisih.SelectedValue = "-"
            ddlSelisih_SelectedIndexChanged(Nothing, Nothing)
            SetOtherAccount(True)
            selisih.Text = ToMaskEdit(ToDouble(amtbalance.Text) - ToDouble(AmtVdtl.Text), 4)
            totalselisih.Text = selisih.Text
        Else
            cbSelisih.Checked = False
            cbSelisih.Enabled = False
            SetOtherAccount(False)
            selisih.Text = ToMaskEdit(0, 4)
            totalselisih.Text = selisih.Text
        End If

        CalculateSelisih()

        addcostamt.Text = ToMaskEdit(ToDouble(addcostamt.Text), 4)
    End Sub

    Private Sub SetOtherAccount(ByVal bState As Boolean)
        ddlSelisihCOA.Enabled = bState
        If bState Then
            ddlSelisihCOA.CssClass = "inpText"
        Else
            ddlSelisihCOA.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub CalculateSelisih()
        ' Calculate total Detail Selisih
        If Not Session("DtlSelisih") Is Nothing Then
            Dim objCek As DataTable = Session("DtlSelisih")
            Dim dTotalSelisih As Double = Math.Abs(ToDouble(objCek.Compute("SUM(amtdtlselisih)", "").ToString))
            If dTotalSelisih < 0 Then dTotalSelisih *= -1
            selisih.Text = ToMaskEdit(dTotalSelisih, 4)
        End If
    End Sub

#End Region

#Region "Function"

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Public Function GetEnabled() As Boolean
        Return Eval("trnnotahrstatus").ToString.ToUpper = "In Process"
    End Function

    Public Function GetCheckedOid() As String
        Return Eval("cmpcode") & "," & Eval("trnnotahroid")
    End Function

    Public Function GetPrintHdr() As String
        Return Eval("cmpcode") & "," & Eval("trnnotahroid")
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode = '" & sFilterCode & "' AND acctgflag='A'"
        'If Not bParentAllowed Then
        '    sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
        'End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("~\Transaction\trnNotaHR.aspx")
        End If
        Session("sCmpcode") = Request.QueryString("cmpcode")
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Delete this data?');")
        BtnSendAp.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Post this data?');")
        Page.Title = CompnyName & " - Nota Piutang Gantung"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", 2)
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        binddata("")
        If Not IsPostBack Then
            payflag_SelectedIndexChanged(Nothing, Nothing)
            txtPeriode1.Text = Format(GetServerTime.AddDays(-2), "dd/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            notaHROid.Text = GenerateID("ql_trnnotahrmst", CompnyCode)
            InitAddCost()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u2.Text = "Update Data" : BtnSendAp.Visible = True
                filltextbox(Session("sCmpcode"), Session("oid"))
                lblIO.Text = "U P D A T E"
                UnabledCheckBox()
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 1
                payflag.Enabled = False : payflag.CssClass = "inpTextDisabled"
                'payflag_SelectedIndexChanged(sender, e)
            Else
                i_u2.Text = "New Data" : BtnSendAp.Visible = False
                Realisasidate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                GenerateNoRealisasi()
                RealisasiNo.Text = Session("sNo")
                lblPOST.Text = "In Process" : btnDelete.Visible = False
                upduser.Text = "-" : updtime.Text = "-"
                TabContainer1.ActiveTabIndex = 0
            End If

        End If
        If lblPOST.Text = "Post" Then
            btnshowCOA.Visible = True : btnPrint.Visible = False
            BtnSendAp.Visible = False : btnDelete.Visible = False
            btnsave.Visible = False
        Else
            btnPrint.Visible = False : btnshowCOA.Visible = False
        End If
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sVar As String = ""
        Dim dVal As Boolean = False
        If payflag.SelectedValue = "BANK" Then
            sVar = "VAR_BANK" : dVal = True
            LblAkun.Visible = True : DdlAkun.Visible = True
        ElseIf payflag.SelectedValue = "CASH" Then
            sVar = "VAR_CASH" : dVal = True
            LblAkun.Visible = True : DdlAkun.Visible = True
        Else
            LblAkun.Visible = False : DdlAkun.Visible = False
        End If
        ddlCOACb(sVar)
    End Sub

    Protected Sub BtnCari_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindCOA("")
    End Sub

    Protected Sub GVDtlCost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("dtlTable") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("dtlTable")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing : gvmstcost.PageIndex = 0
        Dim sType As String = "" : Dim sMsg As String = ""
        Dim tgle As Date = CDate(toDate(txtPeriode1.Text))
        'Dim tgle2 As Date = CDate(toDate(txtPeriode2.Text))
        Try
            tgle = CDate(toDate(txtPeriode2.Text))
        Catch ex As Exception
            sMsg &= "- Maaf, Format tanggal salah !!<br>"
            Exit Sub
        End Try

        If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
            sMsg &= "- Periode akhir harus >= periode awal !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        If statuse.SelectedValue <> "ALL" Then
            sType &= "AND trnnotahrstatus='" & statuse.SelectedValue & "'"
        End If
        If cbPeriod.Checked = True Then
            sType &= " AND trnnotahrdate Between '" & (toDate(txtPeriode1.Text)) & "' AND '" & (toDate(txtPeriode2.Text)) & "' "
        End If
        sType &= " AND " & DDlfilter.SelectedValue & " LIKE '%" & Tchar(no.Text.Trim) & "%'"
        binddata(sType) : Session("SearchExpense") = sql_temp
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing
        txtPeriode1.Text = Format(GetServerTime(), "01/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        gvmstcost.PageIndex = 0 : statuse.SelectedIndex = 0
        cbPeriod.Checked = False
        no.Text = "" : binddata("")
    End Sub

    Protected Sub gvmstcost_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvmstcost.PageIndexChanging
        gvmstcost.PageIndex = e.NewPageIndex
        Dim sType As String = "" : Dim sMsg As String = ""

        If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
            sMsg &= "- Periode akhir harus >= periode awal !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        If statuse.SelectedValue <> "ALL" Then
            sType &= "AND trnnotahrstatus='" & statuse.SelectedValue & "'"
        End If
        If cbPeriod.Checked = True Then
            sType &= " AND trnnotahrdate Between '" & (toDate(txtPeriode1.Text)) & "' AND '" & (toDate(txtPeriode2.Text)) & "' "
        End If
        binddata(sType)
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UncheckAll()
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        Dim sMsg As String = "" : Dim dateStat1 As Boolean
        Dim dt1 As Date : Dim NoReal As String = ""

        If Session("oid") = Nothing Or Session("oid") = "" Then
            GenerateNoRealisasi()
        Else
            NoReal = RealisasiNo.Text
        End If

        If supplierOid.Text = "" Then
            showMessage("- Pilih Supplier terlebih dahulu!<BR>", 2)
            Exit Sub
        End If

        If i_u2.Text = "New Data" Then
            dt1 = CDate(toDate(Realisasidate.Text))
        Else
            dt1 = CDate(toDate(Realisasidate.Text))
        End If
        Try
            dateStat1 = True
        Catch ex As Exception
            dateStat1 = False
            sMsg &= "- Format Tanggal salah !!<BR>"
        End Try

        Try
            If NoteHdr.Text.Length >= 100 Then
                sMsg &= "- Note harus <= 100 Character...!!<BR>"
            End If
        Catch ex As Exception
        End Try

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            lblPOST.Text = "In Process"
            Exit Sub
        End If
        '==============================
        'Cek Peroide Bulanan Dari Crdgl
        '==============================

        sSql = "select distinct left(isnull(periodacctg,''),4)+'-'+substring(isnull(periodacctg,''),6,2) FROM QL_crdgl Where glflag='OPEN'"
        If GetPeriodAcctg(CDate(toDate(Realisasidate.Text))) < GetStrData(sSql) Then
            showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>", 2)
            Exit Sub
        End If

        Dim cRate As New ClassRate()
        Dim iConAROid As Int32 = GenerateID("QL_CONHR", CompnyCode)
        Dim vCMst As Integer = GenerateID("QL_trncashbankmst", CompnyCode)
        Dim vIDgl As Integer = GenerateID("QL_cashbankgl", CompnyCode)
        Dim vIDMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim vIDDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
        Dim iSeq As Int16 = 1
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(toDate(Realisasidate.Text)))
        Dim Closing As String = "", realisasi As String = "", gantungPiutang As String = "", dAcctg As String = "", cAcctg As String = "", varselisih As String = "", iHRAcctgoid, iHRSAcctgoid As Integer

        Dim sVarErr As String = ""
        If Not IsInterfaceExists("VAR_HR", CompnyCode) Then
            sVarErr = "VAR_HR"
        Else
            iHRAcctgoid = GetAcctgOID(GetVarInterface("VAR_HR", Session("branch_id")), CompnyCode)
            If iHRAcctgoid = 0 Then
                sVarErr = "VAR_HR"
            End If
        End If

        If Not IsInterfaceExists("VAR_HRS", CompnyCode) Then
            sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_HRS"
        Else
            iHRSAcctgoid = GetAcctgOID(GetVarInterface("VAR_HRS", Session("branch_id")), CompnyCode)
            If iHRSAcctgoid = 0 Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_HRS"
            End If
        End If

        If sVarErr <> "" Then
            showMessage(GetInterfaceWarning(sVarErr, "approved"), 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If

        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If Session("oid") = Nothing Or Session("oid") = "" Then
                notaHROid.Text = GenerateID("QL_trnnotahrmst", CompnyCode)

                sSql = "INSERT INTO QL_trnnotahrmst (cmpcode, trnnotahroid, branch_code, trnnotahrno, trnnotahrdate, suppoid, trnnotahrdpp, trnnotahrtaxtype, trnnotahrtaxpct, trnnotahrtaxamt, trnnotahramtnetto, trnnotahramtnettoidr, trnnotahramtnettousd, hracctgoid, selisihhramt, selisihhramtidr, selisihhramtusd, selisihhracctgoid, trnnotahracumamt, trnnotahracumamtidr, trnnotahracumamtusd, trnnotahrreturamt, trnnotahrreturamtidr, trnnotahrreturamtusd, trnnotahrnote, trnnotahrstatus, trnnotahrres1, trnnotahrres2, crtuser, crttime, upduser, updtime)" & _
                " VALUES ('" & CompnyCode & "'," & notaHROid.Text & ", '" & Session("branch_id") & "', '" & notaHROid.Text & "','" & CDate(toDate(Realisasidate.Text)) & "', " & supplierOid.Text & ", " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", 'NON TAX' ,10, 0, " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", 0, " & iHRAcctgoid & ", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '" & Tchar(NoteHdr.Text) & "','" & lblPOST.Text.Trim & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & notaHROid.Text & " WHERE tablename='QL_trnnotahrmst' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                sSql = "UPDATE QL_trnnotahrmst Set suppoid = " & supplierOid.Text & ", trnnotahrdpp = " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", trnnotahramtnetto = " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", trnnotahramtnettoidr = " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", selisihhramt = 0, selisihhramtidr = 0, trnnotahrstatus='" & lblPOST.Text.Trim & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, trnnotahrnote='" & Tchar(NoteHdr.Text) & "' Where trnnotahroid=" & notaHROid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            '--Insert Detail Realisasi
            If Not Session("QL_trnnotahrdtl") Is Nothing Then
                Dim objTable As DataTable
                Dim objRow() As DataRow

                objTable = Session("QL_trnnotahrdtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                sSql = "Delete QL_trnnotahrdtl Where trnnotahroid=" & notaHROid.Text & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Session("vIDCost") = GenerateID("ql_trnnotahrdtl", "" & CompnyCode & "")
                Dim dateStr As String = "'" & toDate(Realisasidate.Text) & "'"
                For C1 As Int16 = 0 To objRow.Length - 1
                    Dim sSelisih As Integer
                    If Session("oid") = Nothing Or Session("oid") = "" Then
                        sSelisih = IIf(objRow(C1)("flagselisih") = "Lebih dari Nota", ToDouble(objRow(C1)("amtselisih")) * -1, ToDouble(objRow(C1)("amtselisih")))
                    Else
                        sSelisih = IIf(objRow(C1)("flagselisih") = "Lebih dari Nota", ToDouble(objRow(C1)("amtselisih")), ToDouble(objRow(C1)("amtselisih")))
                    End If
                    sSql = "INSERT INTO QL_trnnotahrdtl (cmpcode, trnnotahrdtloid, trnnotahroid, trnnotahrdtlseq, trnbelimstoid, trnnotahrdtlamt, trnnotahrdtlamtidr, trnnotahrdtlamtusd, selisihamt, selisihamtidr, selisihamtusd, trnnotahrdtlnote, crtuser, crttime, upduser, updtime, trnnotahrdtlres1, trnnotahrdtlres2, addcostacctgoid, addcostamt)" & _
                    " VALUES ('" & CompnyCode & "'," & Session("vIDCost") & "," & notaHROid.Text & ",'" & Tchar(objRow(C1)("seqdtl")) & "'," & objRow(C1)("trnbelimstoid") & "," & ToDouble(objRow(C1)("amtvoucher")) & "," & ToDouble(objRow(C1)("amtvoucher")) & ", 0, " & sSelisih & ", " & sSelisih & ", 0, '" & Tchar(objRow(C1)("trnnotahrdtlnote")) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & objRow(C1)("acctgoid") & "', '" & objRow(C1)("flagselisih") & "'," & objRow(C1)("addcostacctgoid").ToString & "," & ToDouble(objRow(C1)("addcostamt").ToString) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    ' GL Detail
                    Session("vIDCost") += 1
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & (Session("vIDCost") - 1) & " WHERE tablename='QL_trnnotahrdtl'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If lblPOST.Text = "Post" Then
                Dim objTable As DataTable
                Dim objRow() As DataRow

                objTable = Session("QL_trnnotahrdtl")
                Dim dv As DataView = objTable.DefaultView
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                GenerateNoRealisasi()
                Dim dd As String = Format(CDate(toDate(Realisasidate.Text)), "MM/dd/yyyy")
                Dim mm As String = Format(GetServerTime(), "MM/dd/yyyy")
                Dim sNo As String = ""
                'If dd <= mm Then
                '    sNo = RealisasiNo.Text
                'Else
                sNo = Session("sNo")
                'End If

                Dim sDate As String = "(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))"
                sSql = "INSERT INTO QL_conhr (cmpcode, conhroid, branch_code, conhrtype, conhrflag, periodacctg, reftype, refoid, refacctgoid, refdate, refamt, refamtidr, refamtusd, payreftype, payrefoid, payacctgoid, payrefdate, payduedate, payrefno, payrefamt, payrefamtidr, payrefamtusd, conhrstatus, conhrnote, conhrres1, conhrres2, crtuser, crttime, upduser, updtime) VALUES ('" & CompnyCode & "', " & iConAROid & ", '" & Session("branch_id") & "', 'HR', '', '" & sPeriod & "', 'QL_trnnotahrmst', " & notaHROid.Text & ", " & iHRAcctgoid & ", " & sDate & ", " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", '', 0, 0, '01/01/1900', '" & CDate(toDate(Realisasidate.Text)) & "', '', 0, 0, 0, 'Post', '" & Tchar(NoteHdr.Text) & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iConAROid & " WHERE tablename='QL_CONHR' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, branch_code, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type) VALUES ('" & CompnyCode & "', " & vIDMst & ", '" & Session("branch_id") & "', " & sDate & ", '" & sPeriod & "', 'HR|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'NOTAHR')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                ' Insert GL DTL                   
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother2, glpostdate, createuser, createtime) VALUES ('" & CompnyCode & "', " & vIDDtl & ", '" & Session("branch_id") & "', " & iSeq & ", " & vIDMst & ", " & iHRAcctgoid & ", 'D', " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", '" & sNo & "', 'NOTA PIUTANG GANTUNG - " & sNo & " (" & Tchar(NoteHdr.Text) & ")', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", " & ToDouble(lblTampung.Text) + ToDouble(SisaAmtDtl.Text) & ", '" & notaHROid.Text & "', '', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                vIDDtl += 1
                iSeq += 1
                For C1 As Int16 = 0 To objRow.Length - 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother2, glpostdate, createuser, createtime) VALUES ('" & CompnyCode & "', " & vIDDtl & ", '" & Session("branch_id") & "', " & iSeq & ", " & vIDMst & ", " & iHRSAcctgoid & ", 'C', " & ToDouble(objRow(C1)("amtvoucher")) + ToDouble(objRow(C1)("amtselisih")) & ", '" & sNo & "', 'NOTA PIUTANG GANTUNG - " & sNo & " (PI=" & objRow(C1)("trnbelino").ToString & ")" & Tchar(objRow(C1)("trnnotahrdtlnote")) & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objRow(C1)("amtvoucher")) + ToDouble(objRow(C1)("amtselisih")) & ", " & ToDouble(objRow(C1)("amtvoucher")) + ToDouble(objRow(C1)("amtselisih")) & ", '" & ToDouble(objRow(C1)("trnbelimstoid")) & "', '', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    vIDDtl += 1
                    iSeq += 1
                    If ToDouble(objRow(C1)("addcostamt").ToString) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother2, glpostdate, createuser, createtime) VALUES ('" & CompnyCode & "', " & vIDDtl & ", '" & Session("branch_id") & "', " & iSeq & ", " & vIDMst & ", " & objRow(C1)("addcostacctgoid").ToString & ", 'C', " & ToDouble(objRow(C1)("addcostamt").ToString) & ", '" & sNo & "', 'NOTA PIUTANG GANTUNG - " & sNo & " (PI=" & objRow(C1)("trnbelino").ToString & ")" & Tchar(objRow(C1)("trnnotahrdtlnote")) & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objRow(C1)("addcostamt").ToString) & ", " & ToDouble(objRow(C1)("addcostamt").ToString) & ", '" & ToDouble(objRow(C1)("trnbelimstoid")) & "', '', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        vIDDtl += 1
                        iSeq += 1
                    End If
                Next
                dv.RowFilter = "amtselisih > 0"
                For v1 As Integer = 0 To dv.Count - 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother2, glpostdate, createuser, createtime) VALUES ('" & CompnyCode & "', " & vIDDtl & ", '" & Session("branch_id") & "', " & iSeq & ", " & vIDMst & ", " & dv.Item(v1)("acctgoid") & ", 'D', " & ToDouble(dv.Item(v1)("amtselisih")) & ", '" & sNo & "', '" & Tchar(dv.Item(v1)("trnnotahrdtlnote")) & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dv.Item(v1)("amtselisih")) & ", " & ToDouble(dv.Item(v1)("amtselisih")) & ", '" & ToDouble(dv.Item(v1)("trnbelimstoid")) & "', '', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    vIDDtl += 1
                    iSeq += 1
                Next
                dv.RowFilter = ""
                dv.RowFilter = "amtselisih < 0"
                For v1 As Integer = 0 To dv.Count - 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother2, glpostdate, createuser, createtime) VALUES ('" & CompnyCode & "', " & vIDDtl & ", '" & Session("branch_id") & "', " & iSeq & ", " & vIDMst & ", " & dv.Item(v1)("acctgoid") & ", 'C', " & ToDouble(dv.Item(v1)("amtselisih")) * -1 & ", '" & sNo & "', '" & Tchar(dv.Item(v1)("trnnotahrdtlnote")) & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dv.Item(v1)("amtselisih")) * -1 & ", " & ToDouble(dv.Item(v1)("amtselisih")) * -1 & ", '" & ToDouble(dv.Item(v1)("trnbelimstoid")) & "', '', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    vIDDtl += 1
                    iSeq += 1
                Next
                dv.RowFilter = ""
                sSql = "UPDATE QL_mstoid SET lastoid=" & vIDMst & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & vIDDtl - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnnotahrmst set trnnotahrstatus = 'Post', trnnotahrdate = " & sDate & ", upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP, trnnotahrno = '" & sNo & "' where trnnotahroid = " & notaHROid.Text & ""
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : objConn.Close()
        Catch ex As Exception
            objTrans.Rollback() : objConn.Close()
            lblPOST.Text = "In Process" : showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try

        Session("oid") = Nothing
        If lblPOST.Text = "Post" Then
            Session("SavedInfo") = "Data Telah di Posting <BR>"
        Else
            Response.Redirect("trnNotaHR.aspx?awal=true")
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Session("oid") = Nothing : Session("dtlTable") = Nothing
            Response.Redirect("~\Transaction\trnNotaHR.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If

        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM ql_trnnotahrmst Where trnnotahroid=" & notaHROid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trnnotahrdtl Where trnnotahroid=" & notaHROid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : objConn.Close()
        Catch ex As Exception
            objTrans.Rollback() : objConn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("oid") = Nothing
        Response.Redirect("~\Transaction\trnNotaHR.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~/Transaction/trnNotaHR.aspx?awal=true")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PrintReport(CompnyCode, notaHROid.Text)
    End Sub

    Protected Sub gvmstcost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvmstcost.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub 

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showTableCOA(RealisasiNo.Text, Session("branch_id"), gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True
        mpePosting2.Show()
    End Sub

    Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing : Session("dtlTable") = Nothing
        Response.Redirect("trnNotaHR.aspx?awal=true")
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub gridCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If (e.CommandName = "ConfirmPrint") Then
            Response.Redirect("~/ReportForm/ql_printreport.aspx?no=" & Trim(e.CommandArgument.ToString()) & "&printtype=1")
        ElseIf (e.CommandName = "PrintVoucher") Then
            Response.Redirect("~/ReportForm/ql_printreport.aspx?no=" & Trim(e.CommandArgument.ToString()) & "&printtype=1")
        End If
    End Sub

    Protected Sub btnViewLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing
        If Session("SearchExpense") Is Nothing = False Then
            bindLastSearched()
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False
        btnHidePosting2.Visible = False
        mpePosting2.Hide()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(NewMaskEdit(e.Row.Cells(4).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(NewMaskEdit(e.Row.Cells(3).Text), 3)
        End If
    End Sub 

    Protected Sub PrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sOid = sender.tooltip
        Dim a As String() = sOid.split(",")
        sSql = "select cashbankstatus from QL_trncashbankmst where cmpcode='" & a(0) & "' AND cashbankoid='" & a(1) & "'"
        If GetStrData(sSql) = "POST" Then
            PrintReport(a(0), a(1))
        Else
            showMessage("Status In Proses Tidak bisa di Print, Pilih Transaksi lain !!! ", 2)
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal cmpCode As String, ByVal cbOid As Integer)
        'untuk print 
        Dim type As String = payflag.SelectedValue
        Dim cbNo As String = "RECEIPT-" & GetStrData(sSql)
        Dim sWhere As String = "AND cashbankgroup='RECEIPT'"

        If type <> "" Then
            Report.Load(Server.MapPath(folderReport & "printBKK.rpt"))
            'Report.SetParameterValue("sWhere", sWhere)
            Report.SetParameterValue("cmpCode", cmpCode)
            Report.SetParameterValue("cbOid", cbOid)
            Report.SetParameterValue("companyname", "MULTI SARANA COMPUTER")

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, Report)

            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, cbNo)
            Report.Close() : Report.Dispose() : Session("sNo") = Nothing
        End If

    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindCOA("")
    End Sub

    Protected Sub ViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindCOA("")
    End Sub

    Protected Sub btncarisupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCariSupplier.Click
        BindSupplier()
    End Sub

    Protected Sub gvPO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPO.PageIndex = e.NewPageIndex
        BindSupplier() : gvPO.Visible = True
    End Sub

    Protected Sub gvPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        supplierOid.Text = gvPO.SelectedDataKey("suppoid").ToString()
        suppname.Text = gvPO.SelectedDataKey("suppname").ToString()
        gvPO.Visible = False
    End Sub

    Protected Sub EraseSuppbtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EraseSuppbtn.Click
        supplierOid.Text = "" : suppname.Text = "" : AmtPI.Text = "0.00"
        : gvPO.Visible = False : GvVoucher.Visible = False
        trnbelino.Text = "" : trnbelioid.Text = ""
        amtbalance.Text = ""
        AmtVdtl.Text = "" : AmtPI.Text = ""
    End Sub

    Protected Sub GvCoa_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
        End If
    End Sub

    Protected Sub BtnSendAp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSendAp.Click
        lblPOST.Text = "Post" : btnsave_Click(sender, e)
    End Sub

    Protected Sub btnCariPI_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCariPI.Click
        BindPI()
    End Sub

    Protected Sub EraseVc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trnbelioid.Text = "" : trnbelino.Text = "" : GvVoucher.Visible = False
    End Sub

    Protected Sub GvVoucher_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 3)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 3)
        End If
    End Sub

    Protected Sub GvVoucher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        trnbelino.Text = GvVoucher.SelectedDataKey("trnbelino").ToString().Trim
        trnbelioid.Text = GvVoucher.SelectedDataKey("trnbelimstoid")
        AmtVdtl.Text = ToMaskEdit(GvVoucher.SelectedDataKey("sisavoucher"), 3)
        amtuse.Text = ToMaskEdit(GvVoucher.SelectedDataKey("usevoucher"), 3)
        amtbalance.Text = ToMaskEdit(GvVoucher.SelectedDataKey("sisavoucher"), 3)
        AmtPI.Text = ToMaskEdit(GvVoucher.SelectedDataKey("voucher"), 3)
        suppnota.Text = GvVoucher.SelectedDataKey("suppname").ToString()
        GvVoucher.Visible = False
    End Sub

    Protected Sub GvVoucher_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvVoucher.PageIndex = e.NewPageIndex
        BindPI() : GvVoucher.Visible = True
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If trnbelioid.Text.Trim = "" Or trnbelino.Text.Trim = "" Then
            showMessage("Maaf, PI belum dipilih!", 2)
            Exit Sub
        End If
        If Double.Parse(AmtVdtl.Text.Trim) = 0.0 Then
            showMessage("Maaf, Amount Voucher tidak boleh 0!", 2)
            Exit Sub
        End If
        If addcostacctgoid.Items.Count < 1 Then
            showMessage("Silahkan seting akun Biaya Tambahan untuk VAR_SELISIH_HR+ di master interface !!", 2)
            Exit Sub
        End If
        suppname.Enabled = False
        btnCariSupplier.Visible = False : EraseSuppbtn.Visible = False
        '--Create Table Detail--
        '-----------------------
        Dim dtab As DataTable
        If I_Udtl.Text = "New Detail" Then
            If Session("QL_trnnotahrdtl") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seqdtl", Type.GetType("System.Int32"))
                dtab.Columns.Add("trnbelimstoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("trnbelino", Type.GetType("System.String"))
                dtab.Columns.Add("totalvoucher", Type.GetType("System.Double"))
                dtab.Columns.Add("amtvoucher", Type.GetType("System.Double"))
                dtab.Columns.Add("amtselisih", Type.GetType("System.Double"))
                dtab.Columns.Add("trnnotahrdtlnote", Type.GetType("System.String"))
                dtab.Columns.Add("flagselisih", Type.GetType("System.String"))
                dtab.Columns.Add("acctgoid", Type.GetType("System.String"))
                dtab.Columns.Add("suppname", Type.GetType("System.String"))
                dtab.Columns.Add("addcostacctgoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("addcostamt", Type.GetType("System.Double"))
                Session("QL_trnnotahrdtl") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("QL_trnnotahrdtl")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("QL_trnnotahrdtl")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("trnbelimstoid = " & Integer.Parse(trnbelioid.Text) & " AND seqdtl <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Maaf, PI tidak bisa ditambahkan ke dalam list ! PI ini Sudah ada di dalam list !", 2)
                Exit Sub
            End If
        End If
        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_Udtl.Text = "New Detail" Then
            drow = dtab.NewRow
            drow("seqdtl") = Integer.Parse(labelseq.Text)
            drow("trnbelimstoid") = Integer.Parse(trnbelioid.Text)
            drow("trnbelino") = trnbelino.Text.Trim
            drow("totalvoucher") = ToDouble(AmtPI.Text)
            drow("amtvoucher") = ToDouble(AmtVdtl.Text)
            drow("trnnotahrdtlnote") = cashbanknote.Text.Trim
            drow("suppname") = suppnota.Text.Trim
            If cbSelisih.Checked Then
                If ddlSelisih.SelectedValue = "+" Then
                    drow("flagselisih") = "Lebih dari Nota"
                    drow("acctgoid") = ddlSelisihCOA.SelectedValue
                Else
                    drow("flagselisih") = "Kurang dari Nota"
                    drow("acctgoid") = ddlSelisihCOA.SelectedValue
                End If
                drow("amtselisih") = ToDouble(totalselisih.Text)
            Else
                drow("flagselisih") = ""
                drow("amtselisih") = 0
                drow("acctgoid") = ""
                drow("addcostacctgoid") = addcostacctgoid.SelectedValue
                drow("addcostamt") = ToDouble(addcostamt.Text)
            End If
            dtab.Rows.Add(drow) : dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seqdtl = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0) : drowedit(0).BeginEdit()
            drow("trnbelimstoid") = Integer.Parse(trnbelioid.Text)
            drow("trnbelino") = trnbelino.Text.Trim
            drow("totalvoucher") = ToDouble(AmtPI.Text)
            drow("amtvoucher") = ToDouble(AmtVdtl.Text)
            drow("trnnotahrdtlnote") = cashbanknote.Text.Trim
            drow("addcostacctgoid") = addcostacctgoid.SelectedValue
            drow("addcostamt") = ToDouble(addcostamt.Text)
            If cbSelisih.Checked Then
                If ddlSelisih.SelectedValue = "+" Then
                    drow("flagselisih") = "Lebih dr Nota"
                    drow("acctgoid") = ddlSelisihCOA.SelectedValue
                Else
                    drow("amtselisih") = ToDouble(totalselisih.Text)
                    drow("flagselisih") = "Kurang dr Nota"
                End If
                drow("amtselisih") = ToDouble(totalselisih.Text)
            Else
                drow("flagselisih") = ""
                drow("amtselisih") = 0
                drow("acctgoid") = ""
                drow("suppname") = suppnota.Text.Trim
            End If
            drowedit(0).EndEdit() : dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If
        GVvOucherDtl.DataSource = dtab : GVvOucherDtl.DataBind()
        Session("QL_trnnotahrdtl") = dtab
        HitungDtlAmt()
        ClearDtlVoucher() 
        cbSelisih.Checked = False
        GVvOucherDtl.SelectedIndex = -1
        GVvOucherDtl.DataSource = Session("QL_trnnotahrdtl")
        GVvOucherDtl.DataBind() : GVvOucherDtl.Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("QL_trnnotahrdtl") Is Nothing Then
            dtab = Session("QL_trnnotahrdtl")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seqdtl = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seqdtl") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GVvOucherDtl.DataSource = dtab
        GVvOucherDtl.DataBind()
        Session("QL_trnnotahrdtl") = dtab
        If dtab.Rows.Count = 0 Then
            suppname.Enabled = True
            btnCariSupplier.Visible = True : EraseSuppbtn.Visible = True
        End If
        HitungDtlAmt()
    End Sub

    Protected Sub GVvOucherDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        I_Udtl.Text = "edit"
        trnbelioid.Text = GVvOucherDtl.SelectedDataKey("voucheroid").ToString
        trnbelino.Text = GVvOucherDtl.SelectedDataKey("voucherdesc").ToString
        suppnota.Text = GVvOucherDtl.SelectedDataKey("suppname").ToString
        AmtVdtl.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtrealdtl")), 3)
        cashbanknote.Text = GVvOucherDtl.SelectedDataKey("notedtl").ToString
        labelseq.Text = GVvOucherDtl.SelectedDataKey("seqdtl").ToString

        Dim transdate As Date = Date.ParseExact(Realisasidate.Text, "dd/MM/yyyy", Nothing)
        If GVvOucherDtl.Visible = True Then
            GVvOucherDtl.DataSource = Nothing
            GVvOucherDtl.DataBind()
            GVvOucherDtl.Visible = False
        End If
        'GVvOucherDtl.Columns(7).Visible = False
    End Sub

    Protected Sub AmtVdtl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CalculateTotalPayment()
    End Sub

    Protected Sub GVvOucherDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 3)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 3)
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlVoucher()
    End Sub

    Protected Sub ErasePI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ErasePI.Click
        trnbelino.Text = "" : trnbelioid.Text = "" : suppnota.Text = ""
        AmtVdtl.Text = "" : AmtPI.Text = ""
        cashbanknote.Text = "" : amtbalance.Text = ""
        selisih.Text = "" : totalselisih.Text = ""
        cbSelisih.Checked = False
    End Sub

    Protected Sub cbSelisih_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSelisih.CheckedChanged
        SetOtherAccount(cbSelisih.Checked)
        InitOtherAcctg(ddlSelisih.SelectedValue)
    End Sub

    Protected Sub ddlSelisih_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelisih.SelectedIndexChanged
        InitOtherAcctg(ddlSelisih.SelectedValue)
    End Sub
#End Region

    Protected Sub addcostamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addcostamt.TextChanged
        CalculateTotalPayment()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnNotaHR.aspx?awal=true")
            End If
        End If
    End Sub

End Class
