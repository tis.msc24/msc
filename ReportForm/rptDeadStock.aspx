<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptDeadStock.aspx.vb" Inherits="ReportForm_rptDeadStock" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" width="100%">
        <tr>
            <td colspan="3" rowspan="3" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
                    <tr>
                      <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Navy" Text=".: Report Dead Stock" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                    <tr>
                        <th align="center" style="background-color: transparent" valign="center" id="TH1">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE><TBODY><TR><TD align=left colSpan=4>Period</TD><TD align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:TextBox id="bulan" runat="server" Width="55px" CssClass="inpText" __designer:wfdid="w1">1</asp:TextBox>&nbsp;Bulan</TD></TR><TR><TD align=left colSpan=4><asp:Label id="Label3" runat="server" Width="59px" Text="Katalog" __designer:wfdid="w42"></asp:Label></TD><TD align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:TextBox id="ItemName" runat="server" Width="255px" Height="37px" CssClass="inpTextDisabled" __designer:dtid="562949953421371" __designer:wfdid="w47" Enabled="False" Rows="2" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" Width="27px" ImageUrl="~/Images/search2.gif" ImageAlign="Top" Height="27px" __designer:wfdid="w48"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="27px" __designer:wfdid="w49"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w58"></asp:ImageButton> <asp:ImageButton id="BTNpRINT" onclick="BTNpRINT_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w52"></asp:ImageButton> <asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w53"></asp:ImageButton> <asp:ImageButton id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w54"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w56" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w57"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <asp:DropDownList id="period" runat="server" Width="135px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w55" AutoPostBack="True" Visible="False"><asp:ListItem Value="1">1 Bulan</asp:ListItem>
<asp:ListItem Value="2">2 Bulan</asp:ListItem>
<asp:ListItem Value="3">3 Bulan</asp:ListItem>
<asp:ListItem Value="6">6 Bulan</asp:ListItem>
<asp:ListItem Value="9">9 Bulan</asp:ListItem>
<asp:ListItem Value="12">12 Bulan</asp:ListItem>
</asp:DropDownList><asp:Label id="Label2" runat="server" Font-Size="8pt" ForeColor="Red" __designer:wfdid="w51"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crv" runat="server" __designer:wfdid="w2" HasExportButton="False" HasDrillUpButton="False" HasToggleGroupTreeButton="False" HasSearchButton="False" DisplayGroupTree="False" AutoDataBind="true" HasZoomFactorList="False" HasViewList="False" HasPrintButton="False" HasCrystalLogo="False"></CR:CrystalReportViewer> 
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="BTNpRINT"></asp:PostBackTrigger>
</triggers>
</asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Katalog</asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText" Font-Size="8pt"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=3><asp:ImageButton id="btnALLSelectItem" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w1"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneItem" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedItem" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 325px"><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" UseAccessibleHeader="False" PageSize="100" GridLines="None" AutoGenerateColumns="False" DataKeyNames="itemcode,itemdesc,itemoid,satuan3" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Katalog">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Satuan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="SaddleBrown" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" PopupControlID="pnlListMat" Drag="True" TargetControlID="btnHideListMat">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
                            <asp:UpdatePanel id="upPopUpMsg" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" TargetControlID="bePopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>

