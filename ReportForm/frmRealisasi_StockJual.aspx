<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmRealisasi_StockJual.aspx.vb" Inherits="ReportForm_frmRealisasi_Penjualan" Title="Untitled Page" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" style="text-align: left; height: 23px;" valign="center">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Report Realisasi Stock Jual :."></asp:Label></th>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=center colSpan=1><BR /><TABLE width=580><TBODY><TR><TD align=right>Report Type</TD><TD style="TEXT-ALIGN: left" align=left>:</TD><TD style="TEXT-ALIGN: left" align=left><asp:DropDownList id="ddlType" runat="server" Width="130px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False"><asp:ListItem Value="Detail">Detail</asp:ListItem>
<asp:ListItem Value="Summary">Summary</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=right><asp:DropDownList id="ddlOption1" runat="server" Width="96px" CssClass="inpText" AutoPostBack="True">
                                                <asp:ListItem Value="pomst.trnorderdate">SO Date</asp:ListItem>
                                                <asp:ListItem Value="sjmst.trnsjjualdate">SDO Date</asp:ListItem>
                                                <asp:ListItem Value="retmst.trnjualdate">Return Date</asp:ListItem>
                                            </asp:DropDownList></TD><TD style="TEXT-ALIGN: left" align=left>:</TD><TD style="TEXT-ALIGN: left" align=left><asp:TextBox id="podate1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibPOdate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label2" runat="server" Text="to"></asp:Label> <asp:TextBox id="podate2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibPODate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" TargetControlID="podate1" PopupButtonID="ibPOdate1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender4" runat="server" TargetControlID="podate2" PopupButtonID="ibPODate2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="podate1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="podate2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD id="TDSupplier" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvCustomer" runat="server" Width="98%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="custoid,custcode,custname" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged">
                                                <RowStyle BackColor="#F7F7DE" Font-Size="X-Small" />
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <ItemStyle Font-Size="XX-Small" ForeColor="Red" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="custcode" HeaderText="Code" />
                                                    <asp:BoundField DataField="custname" HeaderText="Nama" />
                                                </Columns>
                                                <FooterStyle BackColor="#F25407" />
                                                <PagerStyle BackColor="Silver" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="lblstatusdataCust" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                                </EmptyDataTemplate>
                                                <SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView> <asp:TextBox id="custName" runat="server" Width="187px" CssClass="inpText" __designer:wfdid="w1"></asp:TextBox><asp:ImageButton id="ibSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w2"></asp:ImageButton><asp:ImageButton id="ibDelSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton><asp:Label id="custoid" runat="server" Visible="False" __designer:wfdid="w4"></asp:Label></TD></TR><TR><TD align=right>Taxable</TD><TD style="TEXT-ALIGN: left" align=left>:</TD><TD style="TEXT-ALIGN: left" align=left><asp:DropDownList id="ddlTax" runat="server" Width="130px" CssClass="inpText" AutoPostBack="True">
                                                <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:DropDownList></TD></TR><TR><TD align=right>Group</TD><TD style="TEXT-ALIGN: left" align=left>:</TD><TD style="TEXT-ALIGN: left" align=left><asp:DropDownList id="FilterDDLGrup" runat="server" Width="405px" CssClass="inpText">
                                            </asp:DropDownList></TD></TR><TR><TD align=right>Sub Group</TD><TD style="TEXT-ALIGN: left" align=left>:</TD><TD style="TEXT-ALIGN: left" align=left><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="405px" CssClass="inpText">
                                            </asp:DropDownList></TD></TR><TR><TD align=right>Barang</TD><TD align=left>:</TD><TD align=left colSpan=1><asp:TextBox id="material" runat="server" Width="187px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibSearchMat" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="ibClearMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="matoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="tdMaterial" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvItem" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3,merk" BorderColor="#DEDFDE" PageSize="12" UseAccessibleHeader="False">
                                                <RowStyle BackColor="#F7F7DE" />
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <HeaderStyle Width="50px" />
                                                        <ItemStyle ForeColor="Red" HorizontalAlign="Center" Width="50px" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="merk" HeaderText="Merk">
                                                        <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False" />
                                                    <asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False" />
                                                    <asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False" />
                                                    <asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False" />
                                                    <asp:BoundField DataField="satuan3" HeaderText="Sat Std" Visible="False" />
                                                    <asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False" />
                                                </Columns>
                                                <PagerStyle BackColor="Silver" Font-Bold="True" HorizontalAlign="Right" ForeColor="White" />
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                </EmptyDataTemplate>
                                                <SelectedRowStyle BackColor="Yellow" />
                                                <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 21px" align=right>SO No.</TD><TD style="HEIGHT: 21px" align=left>:</TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="orderno" runat="server" Width="187px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchPO" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelPO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="ordermstoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="tdPO" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvSO" runat="server" Width="450px" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="ordermstoid,orderno,trnorderdate" BorderColor="#DEDFDE" UseAccessibleHeader="False">
                                                <RowStyle BackColor="#F7F7DE" />
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <HeaderStyle Font-Size="X-Small" Width="50px" />
                                                        <ItemStyle Font-Size="XX-Small" ForeColor="Red" HorizontalAlign="Center" Width="50px" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="orderno" HeaderText="SO No.">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="trnorderdate" HeaderText="SO Date">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <PagerStyle BackColor="Silver" Font-Bold="True" HorizontalAlign="Right" />
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                </EmptyDataTemplate>
                                                <SelectedRowStyle BackColor="Yellow" />
                                                <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView> </TD></TR><TR><TD align=right>SDO No.</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="trnsjjualno" runat="server" Width="187px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchLPB" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelLPB" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="trnsjjualmstoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="tdLPB" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvSDO" runat="server" Width="98%" BackColor="White" AllowPaging="True" AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyNames="trnsjjualmstoid,trnsjjualno,trnsjjualdate" BorderColor="#DEDFDE">
                                                <RowStyle BackColor="#F7F7DE"></RowStyle>
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px"></HeaderStyle>

                                                        <ItemStyle HorizontalAlign="Center" Width="30px" ForeColor="Red"></ItemStyle>
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="trnsjjualno" HeaderText="SDO No">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>

                                                        <ItemStyle Width="100px" HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="trnsjjualdate" HeaderText="SDO Date">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>

                                                        <ItemStyle Width="100px" HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                </Columns>

                                                <FooterStyle BackColor="#CCCC99"></FooterStyle>

                                                <PagerStyle HorizontalAlign="Right" BackColor="Silver" ForeColor="White" Font-Bold="True"></PagerStyle>
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                                </EmptyDataTemplate>

                                                <SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White"></HeaderStyle>

                                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            </asp:GridView> </TD></TR><TR><TD id="TD1" align=right runat="server" visible="false">Sales Return No</TD><TD id="TD2" align=left runat="server" visible="false">:</TD><TD id="TD3" align=left runat="server" visible="false"><asp:TextBox id="trnjualreturno" runat="server" Width="187px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchPurchaseReturn" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelPurchaseReturn" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="trnjualreturmstoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="tdPurchaseReturn" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvSalesReturn" runat="server" Width="98%" BackColor="White" AllowPaging="True" AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyNames="trnjualreturmstoid,trnjualreturno,trnjualdate" BorderColor="#DEDFDE">
                                                <RowStyle BackColor="#F7F7DE"></RowStyle>
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px"></HeaderStyle>

                                                        <ItemStyle HorizontalAlign="Center" Width="30px" ForeColor="Red"></ItemStyle>
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="trnjualreturno" HeaderText="Retur No">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>

                                                        <ItemStyle Width="100px" HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="trnjualdate" HeaderText="Retur Date">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>

                                                        <ItemStyle Width="100px" HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                </Columns>

                                                <FooterStyle BackColor="#CCCC99"></FooterStyle>

                                                <PagerStyle HorizontalAlign="Right" BackColor="Silver" ForeColor="White" Font-Bold="True"></PagerStyle>
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                                </EmptyDataTemplate>

                                                <SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White"></HeaderStyle>

                                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            </asp:GridView> </TD></TR><TR><TD align=right>SO Status</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="postatus" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True">
                                                <asp:ListItem>ALL</asp:ListItem>
                                                <asp:ListItem>In Process</asp:ListItem>
                                                <asp:ListItem>In Approval</asp:ListItem>
                                                <asp:ListItem>Approved</asp:ListItem>
                                                <asp:ListItem>Closed</asp:ListItem>
                                            </asp:DropDownList></TD></TR><TR><TD align=right>SDO Status</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlSDOStatus" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True">
                                                <asp:ListItem>ALL</asp:ListItem>
                                                <asp:ListItem>In Process</asp:ListItem>
                                                <asp:ListItem>In Approval</asp:ListItem>
                                                <asp:ListItem>Approved</asp:ListItem>
                                                <asp:ListItem>Closed</asp:ListItem>
                                            </asp:DropDownList></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=1></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=1><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ToPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbExport" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 97px; TEXT-ALIGN: center" vAlign=top><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w1">
                                <ProgressTemplate>
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:Image><br />
                                    Loading report ... 
                                </ProgressTemplate>
                            </asp:UpdateProgress> <CR:CrystalReportViewer id="crv_penjualan" runat="server" __designer:wfdid="w14" AutoDataBind="true"></CR:CrystalReportViewer> &nbsp;<BR />&nbsp;<asp:UpdatePanel id="upMsgbox" runat="server" __designer:wfdid="w3">
                                <ContentTemplate>
                                    <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK" __designer:wfdid="w4">
                                        <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                                                        <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow" __designer:wfdid="w5">
                                                            <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black" __designer:wfdid="w6"></asp:Label></asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                                                </tr>
                                                <tr>
                                                    <td style="WIDTH: 46px" valign="top" align="center">
                                                        <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w7"></asp:Image>
                                                    </td>
                                                    <td valign="top" align="left">
                                                        <asp:Label ID="lblMessage" runat="server" ForeColor="Black" __designer:wfdid="w8"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                                        <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w9"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                                        <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w10"></asp:ImageButton></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" DropShadow="True" Drag="True" BackgroundCssClass="modalBackground" __designer:wfdid="w11">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False" __designer:wfdid="w12"></asp:Button>
                                </ContentTemplate>
                            </asp:UpdatePanel> </TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=1></TD></TR></TBODY></TABLE>
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
