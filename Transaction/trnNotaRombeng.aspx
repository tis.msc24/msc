<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnNotaRombeng.aspx.vb" Inherits="NotaRombeng" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server"> 

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Nota Rombeng"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel2" runat="server" __designer:wfdid="w25" DefaultButton="btnSearch"><TABLE style="WIDTH: 700px"><TBODY><TR><TD align=left>Periode</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w26"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="X-Small" Text="to" __designer:wfdid="w28"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w29"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w30"></asp:ImageButton> <asp:CheckBox id="cbPeriode" runat="server" Font-Size="X-Small" Text="Periode" __designer:wfdid="w31" Visible="False" Checked="True"></asp:CheckBox>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Cabang</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="dd_cabang" runat="server" CssClass="inpText" __designer:wfdid="w32"></asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Nomer</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="no" runat="server" Width="143px" CssClass="inpText" __designer:wfdid="w33"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="statuse" runat="server" Width="98px" CssClass="inpText" __designer:wfdid="w34"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value=" ">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewLast" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4 Visible="false"><STRONG><SPAN style="COLOR: #ff0033">Pemberitahuan : * Data transaksi dengan status in procces akan otomatis closed jika melewati tanggal sekarang, mohon untuk segera di posting</SPAN></STRONG></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><asp:GridView id="gvmstcost" runat="server" Width="950px" ForeColor="#333333" __designer:wfdid="w38" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankno,cashbankoid,cashbankstatus,branch_code" AllowPaging="True" GridLines="None" PageSize="8">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Bold="True" ForeColor="DarkRed"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Transaksi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="BtnPrintGV" onclick="BtnPrintGV_Click" runat="server" ImageUrl="~/Images/print.gif" __designer:wfdid="w45" ToolTip='<%# Eval("cashbankno") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data tidak ditemukan !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w39" Enabled="True" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy" TargetControlID="txtperiode1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w40" Enabled="True" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy" TargetControlID="txtperiode2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w41" TargetControlID="txtPeriode1" CultureName="id-ID" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w42" TargetControlID="txtPeriode2" CultureName="id-ID" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE></asp:Panel><asp:Label id="Label13" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Expense : Rp. " __designer:wfdid="w43" Visible="False"></asp:Label> <asp:Label id="lblgrandtotal" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w44" Visible="False"></asp:Label> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="gvmstcost"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="FONT-SIZE: 9pt">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image>
                                List Of Nota Rombeng </span></strong>&nbsp; <strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="Label10" runat="server" Width="101px" Font-Size="Small" Font-Bold="True" Text="Information :" Font-Underline="True" __designer:wfdid="w121"></asp:Label></TD><TD align=left><asp:Label id="lblIO" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="" Visible="False" __designer:wfdid="w122"></asp:Label></TD><TD align=left><asp:Label id="CutofDate" runat="server" Visible="False" __designer:wfdid="w123"></asp:Label></TD><TD align=left><asp:ImageButton id="imbCBDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w124"></asp:ImageButton> <asp:DropDownList id="cashbankacctgoid" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w125"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label6" runat="server" Font-Size="X-Small" Text="Cabang" __designer:wfdid="w126"></asp:Label></TD><TD align=left><asp:DropDownList id="deptoid" runat="server" Width="150px" CssClass="inpText" Font-Bold="False" AutoPostBack="True" OnSelectedIndexChanged="deptoid_SelectedIndexChanged" __designer:wfdid="w127"></asp:DropDownList><asp:Label id="cashbankoid" runat="server" Visible="False" __designer:wfdid="w128"></asp:Label><asp:Label id="trnragsmstoid" runat="server" Visible="False" __designer:wfdid="w129"></asp:Label></TD><TD align=left><asp:Label id="Label12" runat="server" Font-Size="X-Small" Text="Tanggal" __designer:wfdid="w130"></asp:Label> <asp:Label id="Label22" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w131"></asp:Label></TD><TD align=left><asp:TextBox id="cashbankdate" runat="server" Width="60px" CssClass="inpTextDisabled" AutoPostBack="True" __designer:wfdid="w132" OnTextChanged="cashbankdate_TextChanged" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label23" runat="server" ForeColor="Red" __designer:wfdid="w133">(dd/MM/yyyy)</asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label24" runat="server" Font-Size="X-Small" Text="No. Nota" __designer:wfdid="w134"></asp:Label></TD><TD align=left><asp:TextBox id="CashBankNo" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w135" Enabled="False"></asp:TextBox></TD><TD align=left><asp:Label id="Label9" runat="server" Font-Size="X-Small" Text="Pembayaran" __designer:wfdid="w136"></asp:Label></TD><TD align=left><asp:DropDownList id="payflag" runat="server" Width="105px" CssClass="inpText" Font-Bold="False" AutoPostBack="True" OnSelectedIndexChanged="payflag_SelectedIndexChanged" __designer:wfdid="w137"><asp:ListItem Text="CASH" Value="CASH"></asp:ListItem>
<asp:ListItem Text="NON CASH" Value="NON CASH"></asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label16" runat="server" Width="101px" Font-Size="X-Small" Text="Cash/Akun Bank" __designer:wfdid="w138"></asp:Label></TD><TD align=left><asp:TextBox id="txtCoa" runat="server" Width="315px" CssClass="inpText " __designer:wfdid="w139"></asp:TextBox> <asp:ImageButton id="btnCariCoa" onclick="BtnCariCoa_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w140"></asp:ImageButton> <asp:ImageButton id="ImageButton2" onclick="btnErase_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w141"></asp:ImageButton> <asp:Label id="lblCoa" runat="server" Text="0" Visible="False" __designer:wfdid="w142"></asp:Label></TD><TD align=left><asp:Label id="Label17" runat="server" Width="53px" Font-Size="X-Small" Text="No. Rek" Visible="False" __designer:wfdid="w143"></asp:Label> <asp:Label id="Label8" runat="server" Width="1px" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w144"></asp:Label></TD><TD align=left><asp:TextBox id="payrefno" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w145" MaxLength="30"></asp:TextBox></TD></TR><TR><TD align=left></TD><TD align=left><asp:GridView id="GvCoa" runat="server" Width="406px" ForeColor="#333333" OnSelectedIndexChanged="GvCoa_SelectedIndexChanged" __designer:wfdid="w146" OnPageIndexChanging="GvCoa_PageIndexChanging" PageSize="5" GridLines="None" AllowPaging="True" DataKeyNames="acctgoid,acctgcode,acctgdesc" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun COA">
<HeaderStyle CssClass="gvhdr" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD><TD align=left></TD><TD align=left></TD></TR><TR><TD vAlign=top align=left><asp:Label id="Label4" runat="server" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w147"></asp:Label></TD><TD align=left><asp:TextBox id="cashbanknote" runat="server" Width="314px" Height="49px" CssClass="inpText" __designer:wfdid="w148" MaxLength="95" TextMode="MultiLine"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label11" runat="server" Width="61px" Font-Size="Small" Font-Bold="True" Text="Detail :" Font-Underline="True" __designer:wfdid="w149"></asp:Label></TD><TD align=left colSpan=3><asp:Label id="trnragsdtlseq" runat="server" Visible="False" __designer:wfdid="w150"></asp:Label><asp:Label id="cashbankgloid" runat="server" Visible="False" __designer:wfdid="w151"></asp:Label><asp:Label id="createtime" runat="server" Font-Bold="True" Visible="False" __designer:wfdid="w152"></asp:Label><asp:Label id="MtrLocoid" runat="server" Visible="False" __designer:wfdid="w153"></asp:Label><asp:Label id="trnragsdtloid" runat="server" Visible="False" __designer:wfdid="w154"></asp:Label><asp:Label id="i_u2" runat="server" Font-Bold="True" ForeColor="Red" Text="New Detail" Visible="False" __designer:wfdid="w155"></asp:Label><asp:Label id="lblPOST" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w156"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="LblBarang" runat="server" Width="47px" Font-Size="X-Small" Text="Katalog" __designer:wfdid="w83"></asp:Label> <asp:Label id="Label19" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w158"></asp:Label></TD><TD align=left><asp:TextBox id="itemdesc" runat="server" Width="315px" CssClass="inpText " __designer:wfdid="w193"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnCari" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w160"></asp:ImageButton> <asp:ImageButton id="BtnEraseItem" onclick="btnErase_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w161"></asp:ImageButton> <asp:Label id="Itemoid" runat="server" Visible="False" __designer:wfdid="w162"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left></TD><TD align=left colSpan=3><asp:GridView id="GVItem" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w163" PageSize="5" GridLines="None" AllowPaging="True" DataKeyNames="refoid,itemcode,itemdesc,mtrlocoid,Gudang,SaldoNya" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle CssClass="gvhdr" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SaldoNya" HeaderText="Qty Stok">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtrlocoid" HeaderText="mtrlocoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="refoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                    <asp:Label ID="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!"></asp:Label>
                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="Label1" runat="server" Font-Size="X-Small" Text="Quantity" __designer:wfdid="w164"></asp:Label> <asp:Label id="Label7" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w165"></asp:Label></TD><TD align=left><asp:TextBox id="QtyNya" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w166" MaxLength="20">0.00</asp:TextBox> <asp:Label id="MaxQty" runat="server" __designer:wfdid="w167">0.00</asp:Label> <STRONG>Max Qty</STRONG></TD><TD align=left><asp:Label id="Label14" runat="server" Font-Size="X-Small" Text="Price Jual" __designer:wfdid="w168"></asp:Label> <asp:Label id="Label21" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w169"></asp:Label></TD><TD align=left><asp:TextBox id="ItemPrice" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w170" MaxLength="20">0.00</asp:TextBox> <asp:Label id="lblCurr" runat="server" Font-Bold="True" Text="IDR" Visible="False" __designer:wfdid="w171"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lblItemDesc" runat="server" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w172"></asp:Label></TD><TD align=left><asp:TextBox id="trnragsdtlnote" runat="server" Width="328px" CssClass="inpText" __designer:wfdid="w173" MaxLength="150"></asp:TextBox></TD><TD align=right>&nbsp;<asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w174"></asp:ImageButton></TD><TD align=left><asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w175"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 950px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtlCost" runat="server" Width="100%" ForeColor="#333333" OnSelectedIndexChanged="GVDtlCost_SelectedIndexChanged" __designer:wfdid="w176" PageSize="8" GridLines="None" DataKeyNames="trnragsdtlseq" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="Blue" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnragsdtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnragsdtlprice" HeaderText="Price Jual">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnragsdtlqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnragsdtlnetto" HeaderText="Total Amt">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnragsdtlnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="mtrlocoid" HeaderText="mtrlocoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail biaya"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><asp:Label id="lblTotExpense" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Receipt : " __designer:wfdid="w177"></asp:Label> <asp:Label id="TotalNetto" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w178"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><SPAN style="FONT-SIZE: 10pt; COLOR: #696969">Last update On </SPAN><asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w179"></asp:Label> By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w180"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton style="HEIGHT: 23px" id="btnsave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:wfdid="w181"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" CausesValidation="False" __designer:wfdid="w182"></asp:ImageButton> <asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w183"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:wfdid="w184"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" Visible="False" __designer:wfdid="w185"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" runat="server" Width="66px" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="24px" Visible="False" __designer:wfdid="w186"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w187" AssociatedUpdatePanelID="UpdatePanel5" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w188"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="ce7" runat="server" __designer:wfdid="w189" TargetControlID="cashbankdate" Format="dd/MM/yyyy" PopupButtonID="imbCBDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meedate" runat="server" __designer:wfdid="w190" TargetControlID="cashbankdate" Mask="99/99/9999" MaskType="Date" InputDirection="RightToLeft" ClearMaskOnLostFocus="true" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEQtyNya" runat="server" __designer:wfdid="w191" TargetControlID="QtyNya" ValidChars="1234567890,.">
</ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FteItemPrice" runat="server" __designer:wfdid="w192" TargetControlID="ItemPrice" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>

                        </ContentTemplate>
                        <HeaderTemplate>
                            <span style="font-size: 9pt">
                                <strong><span>
                                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                    Form Nota Rombeng </span></strong></span> <strong><span style="font-size: 9pt">:.</span></strong>


                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Nama COA">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
