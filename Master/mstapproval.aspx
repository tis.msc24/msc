<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstapproval.aspx.vb" Inherits="Master_mstapproval" title="Approval Structure" %>
<%--
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
   <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" colspan="2" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="Navy" Text=".: Data Approval"></asp:Label>
            </th>
        </tr>
    </table>
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
<asp:UpdatePanel id="UpdatePanelSearch" runat="server"><contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch" __designer:wfdid="w183"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=5><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w190"><asp:ListItem Value="gendesc">Table Name</asp:ListItem>
<asp:ListItem Value="username">Approval User</asp:ListItem>
<asp:ListItem Value="approvalstatus">Approval Status</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w191" MaxLength="30"></asp:TextBox>&nbsp;&nbsp; </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbSorting" runat="server" Text="Sort By" __designer:wfdid="w10"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=5><asp:DropDownList id="DDLSorting" runat="server" CssClass="inpText" __designer:wfdid="w8"><asp:ListItem Value="tablename">Table Name</asp:ListItem>
<asp:ListItem Value="approvaluser">Approval User</asp:ListItem>
<asp:ListItem Value="approvalstatus">Approval Status</asp:ListItem>
</asp:DropDownList>&nbsp;- <asp:DropDownList id="DDLSorting2" runat="server" CssClass="inpText" __designer:wfdid="w9"><asp:ListItem>ASC</asp:ListItem>
<asp:ListItem>DESC</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD align=left colSpan=5><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton><asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton><asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=7>&nbsp;<FIELDSET style="WIDTH: 60%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 300px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 85%; BACKGROUND-COLOR: beige"><asp:GridView id="GVmstapproval" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w196" EmptyDataText="No data in database." AutoGenerateColumns="False" GridLines="None" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="approvalsetoid" DataNavigateUrlFormatString="~\master\mstapproval.aspx?oid={0}" DataTextField="tablename" HeaderText="Table Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="30%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="approvaluser" HeaderText="Approval User">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="approvalstatus" HeaderText="Approval Status">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="lblmsg" runat="server" CssClass="Important" Text="No Data found !!" Visible="True"></asp:Label>
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp;</DIV></FIELDSET></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
</asp:UpdatePanel> 
</ContentTemplate>
            <HeaderTemplate>
<img align="absMiddle" alt="" src="../Images/corner.gif" />
                <span style="font-size: 9pt"><strong>.: List of Approval</strong></span>&nbsp; 
</HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE id="Table2" cellSpacing=2 cellPadding=4 width="100%" align=center border=0><TBODY><TR style="COLOR: #000099"><TD style="WIDTH: 100px; TEXT-ALIGN: left" class="Label">Table Name </TD><TD style="COLOR: #000099; TEXT-ALIGN: left" colSpan=2><asp:DropDownList id="tablename" runat="server" Width="225px" CssClass="inpText" __designer:wfdid="w129"></asp:DropDownList></TD></TR><TR style="COLOR: #000099"><TD style="WIDTH: 100px; TEXT-ALIGN: left" class="Label">Approval Type</TD><TD style="TEXT-ALIGN: left" colSpan=2><asp:DropDownList id="approvaltype" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w130" AutoPostBack="True"><asp:ListItem>FINAL</asp:ListItem>
<asp:ListItem Enabled="False">PROCESS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 100px; TEXT-ALIGN: left" class="Label">Approval Level</TD><TD style="TEXT-ALIGN: left" colSpan=2><asp:DropDownList id="approvallevel" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Size="X-Small" __designer:wfdid="w131" AutoPostBack="True" Enabled="False"><asp:ListItem>7</asp:ListItem>
<asp:ListItem>6</asp:ListItem>
<asp:ListItem>5</asp:ListItem>
<asp:ListItem>4</asp:ListItem>
<asp:ListItem>3</asp:ListItem>
<asp:ListItem>2</asp:ListItem>
<asp:ListItem>1</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 100px; TEXT-ALIGN: left" class="Label">Approval User</TD><TD style="TEXT-ALIGN: left" colSpan=2><asp:DropDownList id="approvaluser" runat="server" Width="242px" CssClass="inpText" __designer:wfdid="w132"></asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 100px; TEXT-ALIGN: left" class="Label">Approval Status</TD><TD style="TEXT-ALIGN: left" colSpan=2><asp:TextBox id="approvalstatus" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w134" MaxLength="10" size="20" ReadOnly="True"></asp:TextBox> <asp:TextBox id="approvalsetoid" runat="server" Width="25px" CssClass="inpText" __designer:wfdid="w139" MaxLength="10" Enabled="False" size="20" Visible="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 100px; TEXT-ALIGN: left" class="Label">Cabang<ajaxToolkit:ListSearchExtender id="LSEUser" runat="server" __designer:wfdid="w133" PromptCssClass="Important" PromptText="Type for searching .." TargetControlID="approvaluser"></ajaxToolkit:ListSearchExtender></TD><TD style="TEXT-ALIGN: left" colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 61%; HEIGHT: 143px; BACKGROUND-COLOR: beige"><asp:CheckBoxList id="cbCabang" runat="server" Width="528px" BackColor="#FFFFC0" __designer:wfdid="w155" CellPadding="5" BorderColor="Black" RepeatDirection="Horizontal" RepeatColumns="4">
</asp:CheckBoxList></DIV></TD></TR><TR><TD style="COLOR: #585858; TEXT-ALIGN: left" colSpan=3><asp:Panel id="PanelUpdate" runat="server" Width="1005px" __designer:wfdid="w136">Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="[Updtime]" __designer:wfdid="w137"></asp:Label> by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="[upduser]" __designer:wfdid="w138"></asp:Label></asp:Panel> </TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w140"></asp:ImageButton>&nbsp; <asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w141"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w142"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w151" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w154"></asp:Image><BR />LOADING....</SPAN><BR /></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</contenttemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanelValidasi" runat="server"><contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" __designer:wfdid="w144" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: center" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w145"></asp:Label></TD></TR><TR><TD><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px" __designer:wfdid="w146"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" CssClass="Important" __designer:wfdid="w147"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w148"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" __designer:wfdid="w149" TargetControlID="ButtonExtendervalidasi" DropShadow="True" PopupControlID="PanelValidasi" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" __designer:wfdid="w150" Visible="False"></asp:Button> 
</contenttemplate>
</asp:UpdatePanel> 
</ContentTemplate>
            <HeaderTemplate>
<img id="Img2" align="absMiddle" alt="" src="../Images/corner.gif" />
                <span style="font-size: 9pt"><strong>.: Form Approval</strong></span> 
</HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

