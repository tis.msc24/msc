<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptRealisasiVoucher.aspx.vb" Inherits="ReportForm_frmpostat" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias">
        <tr>
            <th align="center" class="header" valign="center" style="width: 975px; text-align: center;">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".:  REPORT REALISASI VOUCHER :."></asp:Label></th>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD align=left>Type</TD><TD>:</TD><TD align=left><asp:DropDownList id="ddlsumdetail" runat="server" Width="79px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w83" OnSelectedIndexChanged="ddlsumdetail_SelectedIndexChanged"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem Value="Detail">Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label11" runat="server" Text="Periode" __designer:wfdid="w109"></asp:Label></TD><TD><asp:Label id="Label12" runat="server" Text=":" __designer:wfdid="w109"></asp:Label></TD><TD align=left><asp:TextBox id="date1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w84"></asp:TextBox> <asp:ImageButton id="search1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w85"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w86"></asp:Label> <asp:TextBox id="date2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w87"></asp:TextBox> <asp:ImageButton id="search2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w88"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(dd/mm/yyyy)" __designer:wfdid="w89"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label5" runat="server" Width="42px" Text="Cabang" __designer:wfdid="w109" Visible="False"></asp:Label></TD><TD><asp:Label id="Label6" runat="server" Text=":" __designer:wfdid="w109" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="BranchDDL" runat="server" Width="157px" CssClass="inpText" __designer:wfdid="w112" OnSelectedIndexChanged="dView_SelectedIndexChanged" Visible="False"></asp:DropDownList></TD></TR><TR><TD id="TD4" align=left runat="server" Visible="false">Taxable</TD><TD id="TD6" runat="server" Visible="false">:</TD><TD id="TD5" align=left runat="server" Visible="false"><asp:DropDownList id="taxable" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w123" OnSelectedIndexChanged="detailstatus_SelectedIndexChanged">
                                            <asp:ListItem Value="NO">No</asp:ListItem>
                                            <asp:ListItem Value="YES">Yes</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="No. PO" __designer:wfdid="w109"></asp:Label></TD><TD><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w109"></asp:Label></TD><TD align=left><asp:TextBox id="trnPono" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w110"></asp:TextBox> <asp:ImageButton id="ImgPono" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w111"></asp:ImageButton> <asp:ImageButton id="ImgErasePono" onclick="ImgErasePono_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w112"></asp:ImageButton> <asp:Label id="PoMstoid" runat="server" __designer:wfdid="w93" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD></TD><TD align=left><asp:GridView id="poNoGV" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w94" OnSelectedIndexChanged="poNoGV_SelectedIndexChanged" Visible="False" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnbelimstoid,trnbelipono" AllowPaging="True" OnPageIndexChanging="poNoGV_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelipono" HeaderText="Nomer PO">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="poDate" HeaderText="Tanggal PO">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp. Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="Label13" runat="server" Text="Supplier" __designer:wfdid="w109"></asp:Label></TD><TD><asp:Label id="Label14" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD align=left><asp:TextBox id="txtSupplier" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="imgSupplier" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w4" OnClick="imgSupplier_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="eraseSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w5" OnClick="eraseSupplier_Click"></asp:ImageButton>&nbsp;<asp:Label id="IdSupplier" runat="server" __designer:wfdid="w93" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD></TD><TD align=left><asp:GridView id="gvSupplier" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w94" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged" Visible="False" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="suppoid,suppname" AllowPaging="True" OnPageIndexChanging="gvSupplier_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="suppname" HeaderText="Nama Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Alamat Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Text="Kode Voucher" __designer:wfdid="w109"></asp:Label></TD><TD><asp:Label id="Label8" runat="server" Text=":" __designer:wfdid="w109"></asp:Label></TD><TD align=left><asp:TextBox id="txtkdVcr" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgKdvcr" onclick="ImgKdvcr_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:ImageButton id="imgerasekdvcr" onclick="imgerasekdvcr_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:Label id="kdVoucher" runat="server" __designer:wfdid="w93" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD></TD><TD align=left><asp:GridView id="kdVcrGV" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w7" OnSelectedIndexChanged="kdVcrGV_SelectedIndexChanged" Visible="False" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="voucherno,voucherdesc" AllowPaging="True" OnPageIndexChanging="kdVcrGV_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="voucherno" HeaderText="Kode Voucher">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="voucherdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="Label9" runat="server" Text="Kode Realisasi" __designer:wfdid="w109" Visible="False"></asp:Label></TD><TD><asp:Label id="Label10" runat="server" Text=":" __designer:wfdid="w167" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="txtKdRealisasi" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w168" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgKdRealisasi" onclick="ImgKdRealisasi_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w169" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseKdRealisasi" onclick="EraseKdRealisasi_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w170" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="KdRealisasi" runat="server" __designer:wfdid="w93" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD></TD><TD align=left><asp:GridView id="KdRealisasiGV" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w7" OnSelectedIndexChanged="KdRealisasiGV_SelectedIndexChanged" Visible="False" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="realisasioid,realisasino" AllowPaging="True" OnPageIndexChanging="KdRealisasiGV_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="realisasino" HeaderText="Kode Realisasi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasidate" HeaderText="Tanggal Realisasi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasioid" HeaderText="realisasioid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD2" align=left runat="server" Visible="false"></TD><TD id="Td1" runat="server" Visible="false"></TD><TD id="Td3" align=left runat="server" Visible="false"><asp:DropDownList id="dView" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w96" OnSelectedIndexChanged="dView_SelectedIndexChanged" Visible="False">
                                            <asp:ListItem>Local</asp:ListItem>
                                            <asp:ListItem Value="Trading">Import</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w97" TargetControlID="Date1" PopupButtonID="search1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w98" TargetControlID="Date2" PopupButtonID="search2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w99" TargetControlID="date1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w100" TargetControlID="date2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD colSpan=3><asp:ImageButton id="ImageButton1" onclick="ImageButton1_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w101"></asp:ImageButton>&nbsp;<asp:ImageButton id="ToPDF" onclick="ToPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w102"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbExport" onclick="imbExport_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w103"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w104"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 120px" align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w105"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle" __designer:wfdid="w106"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="labelmsg" runat="server" ForeColor="Red" __designer:wfdid="w107"></asp:Label></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=center colSpan=3><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w108" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
                        <asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

