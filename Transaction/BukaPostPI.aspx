<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="BukaPostPI.aspx.vb" Inherits="BukaPostPI" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
    <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0"
        width="100%" onclick="return tableutama_onclick()" class="tabelhias">
        <tr>
            <th class="header" colspan="3" style="vertical-align: middle; text-align: left; height: 25px;" valign="top" align="left">
                <asp:Label ID="Label111" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Unpost Purchase Invoice"></asp:Label>
                </Th>                    
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
<asp:Panel id="Panel2" runat="server" __designer:wfdid="w35" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small" align=left>Filter :</TD><TD align=left colSpan=2><asp:DropDownList id="ddlFilter" runat="server" CssClass="inpText" __designer:wfdid="w33"><asp:ListItem Value="trnbelino">Invoice No</asp:ListItem>
<asp:ListItem Value="trnbelipono">PO No</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w34" MaxLength="30"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w35" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Periode :</TD><TD align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w36"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton>&nbsp;<asp:Label id="Label169" runat="server" Text="to" __designer:wfdid="w38"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w39"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton>&nbsp;<asp:Label id="Label179" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w41"></asp:Label> <ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w43" TargetControlID="FilterPeriod1" Format="dd/MM/yyyy" PopupButtonID="imbPeriod1"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left>Status :</TD><TD align=left colSpan=2><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w44"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w46"></asp:ImageButton> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton> <asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w48" Visible="False" Checked="True"></asp:CheckBox><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w49" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left><asp:Label id="Company" runat="server" CssClass="Important" __designer:wfdid="w53" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="InvoiceID" runat="server" CssClass="Important" __designer:wfdid="w54"></asp:Label></TD><TD style="WIDTH: 3px" align=left><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w55" TargetControlID="FilterPeriod2" Format="dd/MM/yyyy" PopupButtonID="imbPeriod2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left colSpan=3><asp:GridView id="tbldata" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w1" PageSize="7" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelino,trntaxtype" OnSelectedIndexChanged="tbldata_SelectedIndexChanged" GridLines="None" OnPageIndexChanging="tbldata_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trnbelimstoid" DataNavigateUrlFormatString="~\transaction\BukaPostPI.aspx?oid={0}" DataTextField="trnbelino" HeaderText="Invoice No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnbelipono" HeaderText="PO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Invoice Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="supplier" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelistatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!" __designer:wfdid="w57"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="tbldata"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                <strong> <span style="font-size: 9pt">
                                List Of Purchase
                            Invoice</span></strong> <strong><span style="font-size: 9pt">:.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel7" runat="server" RenderMode="Inline">
                                <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w61" ActiveViewIndex="0"><asp:View id="View1" runat="server" __designer:wfdid="w62"><asp:Label id="Label1" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information" __designer:wfdid="w63"></asp:Label> <asp:Label id="Label2" runat="server" Font-Size="Small" ForeColor="#585858" Text="|" __designer:wfdid="w64"></asp:Label> <asp:LinkButton id="lkbMore0" onclick="lkbMore0_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w65">More Information</asp:LinkButton><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="i_u" runat="server" Font-Size="X-Small" ForeColor="Red" Text="N E W" __designer:wfdid="w66" Visible="False"></asp:Label><asp:Label id="oid" runat="server" Font-Size="X-Small" __designer:wfdid="w67" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="trnbelimstoid" runat="server" __designer:wfdid="w68" Visible="False"></asp:Label><asp:Label id="dbpino" runat="server" __designer:wfdid="w69" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="periodacctg" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:wfdid="w70" Visible="False"></asp:Label></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w71" TargetControlID="trnbelidate" Format="dd/MM/yyyy" PopupButtonID="ImageButton1"></ajaxToolkit:CalendarExtender></TD><TD style="FONT-SIZE: x-small" align=left><asp:DropDownList id="typeTax" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w72" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w73" TargetControlID="currencyRate" Mask="999,999,999.99" MaskType="Number" ErrorTooltipEnabled="True" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice No</TD><TD align=left><asp:TextBox id="trnbelino" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w74" MaxLength="30" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Invoice Date <asp:Label id="Label19" runat="server" Width="1px" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w75"></asp:Label></TD><TD align=left><asp:TextBox style="TEXT-ALIGN: justify" id="trnbelidate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w76" AutoPostBack="True" MaxLength="10" OnTextChanged="trnbelidate_TextChanged"></asp:TextBox> <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w77"></asp:ImageButton> <asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w78"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>PO Date</TD><TD align=left><asp:TextBox id="podate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w79" MaxLength="30" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>PO No. <asp:Label id="Label4" runat="server" Width="1px" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w80"></asp:Label></TD><TD align=left><asp:TextBox id="trnbelipono" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w81" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchPO" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w82"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearPO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w83"></asp:ImageButton> <asp:Label id="prefixoid" runat="server" Text="Label" __designer:wfdid="w84" Visible="False"></asp:Label>&nbsp;<asp:Label id="mCabang" runat="server" __designer:wfdid="w85" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>Supplier</TD><TD align=left><asp:TextBox id="suppliername" runat="server" Width="175px" CssClass="inpTextDisabled" __designer:wfdid="w86" MaxLength="100" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" id="TD2" align=left runat="server" Visible="false">Identifer No</TD><TD id="TD1" align=left runat="server" Visible="false"><asp:TextBox id="identifierno" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w87" MaxLength="30" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD align=left colSpan=3><asp:GridView id="gvListPO" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w88" Visible="False" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelipono,trnbelipodate,trnsuppoid,suppname,curroid,currate,trnbelidisctype,trnbelidisc,digit,trnpaytype,prefixoid,branch_code" OnSelectedIndexChanged="gvListPO_SelectedIndexChanged" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelimstoid" HeaderText="trnbelimstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipono" HeaderText="PO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipodate" HeaderText="PO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsuppoid" HeaderText="trnsuppoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lkbDetail" onclick="lkbDetail_Click" runat="server" __designer:wfdid="w6" ToolTip='<%# eval("trnbelipono") %>'>Detail</asp:LinkButton>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD vAlign=top align=left colSpan=3><asp:GridView id="ItemGv" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w89" Visible="False" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="NoSJ" HeaderText="No. PDO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Quantity" SortExpression="qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Payment Term</TD><TD align=left><asp:DropDownList id="TRNPAYTYPE" runat="server" Width="128px" CssClass="inpTextDisabled" __designer:wfdid="w90" AppendDataBoundItems="True" Enabled="False"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Currency</TD><TD align=left><asp:DropDownList id="CurrencyOid" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w91" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Currency Rate</TD><TD align=left><asp:TextBox id="currencyRate" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w92" AutoPostBack="True" ReadOnly="True" OnTextChanged="currencyRate_TextChanged">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice Note</TD><TD align=left><asp:TextBox id="trnbelinote" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w93" MaxLength="30"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Status</TD><TD align=left><asp:TextBox id="posting" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w94" ReadOnly="True">In Process</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left colSpan=2><asp:Label id="Label25" runat="server" Text="Ref. No" __designer:wfdid="w95" Visible="False"></asp:Label>&nbsp;<asp:TextBox id="trnbeliref" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w96" Visible="False" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Purchase Amount</TD><TD align=left><asp:TextBox id="amtbeli" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w97" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Total Detail Disc.</TD><TD align=left><asp:TextBox id="amtdiscdtl" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w98" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Net. Detail Amount</TD><TD align=left><asp:TextBox id="netdetailamt" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w99" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left>Disc. Header Type</TD><TD style="HEIGHT: 21px" align=left><asp:DropDownList id="trnbelidisctype" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w100" AutoPostBack="True"><asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
<asp:ListItem Value="PCT">PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left><asp:Label id="lbl1" runat="server" Font-Size="X-Small" Text="Disc. Header (Rp.)" __designer:wfdid="w101"></asp:Label></TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="trnbelidisc" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w102" AutoPostBack="True" MaxLength="12">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left>Disc. Header Amount</TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="amtdischdr" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w103" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Tax Type</TD><TD align=left><asp:DropDownList id="Tax" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w104" OnSelectedIndexChanged="Tax_SelectedIndexChanged" Enabled="False"><asp:ListItem Value="1">NON TAX</asp:ListItem>
<asp:ListItem Value="0">TAX</asp:ListItem>
</asp:DropDownList> <asp:Label id="TypeTaxlabel" runat="server" __designer:wfdid="w105" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label33" runat="server" Text="Voucher Amount" __designer:wfdid="w106"></asp:Label></TD><TD align=left><asp:TextBox id="voucherAmt" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w107" AutoPostBack="True" MaxLength="4">0</asp:TextBox> <asp:Label id="trnsuppoid" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w108" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="TaxAmmount" runat="server" Text="Tax Amount" __designer:wfdid="w109"></asp:Label></TD><TD align=left><asp:TextBox id="amttax" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w110" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice + (Tax)</TD><TD align=left><asp:TextBox id="amtbelinetto1" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w111" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label34" runat="server" Text="Ekpedisi" __designer:wfdid="w112"></asp:Label></TD><TD align=left><asp:TextBox id="ekspedisiAmt" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w113" ReadOnly="True" Enabled="False">0.00</asp:TextBox> <asp:Label id="trnbelitype" runat="server" Font-Size="X-Small" __designer:wfdid="w114" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>Total Invoice</TD><TD align=left><asp:TextBox id="totalinvoice" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w115" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w116" TargetControlID="trnbelidate" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="True" UserDateFormat="MonthDayYear" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" CultureName="en-US"></ajaxToolkit:MaskedEditExtender></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee50" runat="server" __designer:wfdid="w117" TargetControlID="trnbelidisc" Mask="999,999,999.99" MaskType="Number" ErrorTooltipEnabled="True" InputDirection="RightToLeft" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server" __designer:wfdid="w118"><asp:LinkButton id="lkbInfo1" onclick="lkbInfo1_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w119">Information</asp:LinkButton> <asp:Label id="Label7" runat="server" Font-Size="Small" ForeColor="#585858" Text="|" __designer:wfdid="w120"></asp:Label> <asp:Label id="Label3" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="More Information" __designer:wfdid="w121"></asp:Label><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="trnbelidtlunit" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w122"></asp:Label></TD><TD align=left><asp:Label id="iRow" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" __designer:wfdid="w123" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><ajaxToolkit:MaskedEditExtender id="mee44" runat="server" __designer:wfdid="w124" TargetControlID="trnbelidtlqty" Mask="999,999,999.99" MaskType="Number" ErrorTooltipEnabled="True" InputDirection="RightToLeft" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" CultureName="en-US" AcceptNegative="Left"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><asp:TextBox id="txtamount" runat="server" CssClass="inpText" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w125" Visible="False" MaxLength="30" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><ajaxToolkit:MaskedEditExtender id="mee4" runat="server" __designer:wfdid="w126" TargetControlID="trnbelidtldisc" Mask="999,999,999.99" MaskType="Number" ErrorTooltipEnabled="True" InputDirection="RightToLeft" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" CultureName="en-US"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><asp:Label id="sjoid" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" Font-Bold="True" __designer:wfdid="w127" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:TextBox id="Location" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w128" Visible="False" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 192px" align=left><ajaxToolkit:MaskedEditExtender id="mee424" runat="server" __designer:wfdid="w129" TargetControlID="trnbelidtldisc2" Mask="999,999,999.99" MaskType="Number" ErrorTooltipEnabled="True" InputDirection="RightToLeft" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" CultureName="en-US"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Delivery&nbsp;No&nbsp;<asp:Label id="Label5" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w130"></asp:Label></TD><TD align=left><asp:TextBox id="noteSJ" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w131" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSJ" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w132"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSj" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w133"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblmtrref" runat="server" Text="Item" __designer:wfdid="w134"></asp:Label></TD><TD align=left><asp:TextBox id="Material" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w135" MaxLength="100" ReadOnly="True"></asp:TextBox><asp:ImageButton id="btnSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w136" Visible="False"></asp:ImageButton><asp:ImageButton id="btnClearSupp" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w137" Visible="False"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left>Quantity</TD><TD align=left><asp:TextBox id="trnbelidtlqty" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w138" AutoPostBack="True" MaxLength="10" ReadOnly="True">0.00</asp:TextBox> <asp:Label id="unit" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w139"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>Price <asp:Label id="Label23" runat="server" Width="1px" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w140"></asp:Label></TD><TD style="WIDTH: 192px" align=left><asp:TextBox id="trnbelidtlprice" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w141" AutoPostBack="True" MaxLength="30" Enabled="False" ValidationGroup="MKE">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=4><asp:GridView id="gvListSJ" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w142" Visible="False" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnsjbelioid,trnsjbelino,trnsjbelinote " OnSelectedIndexChanged="gvListSJ_SelectedIndexChanged" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjbelioid" HeaderText="trnsjbelioid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelino" HeaderText="Delivery No" SortExpression="trnsjbelino">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjreceivedate" HeaderText="Recvd. Date" SortExpression="trnsjreceivedate">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelinote" HeaderText="Note" SortExpression="trnsjbelinote">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lkbDetailSJ" onclick="lkbDetailSJ_Click" runat="server" __designer:wfdid="w7" ToolTip='<%# eval("trnsjbelioid") %>'>Detail</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD vAlign=top align=left colSpan=4><asp:GridView id="gvListSJDetail" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w143" Visible="False" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnsjbelidtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description" SortExpression="matlongdesc">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Quantity" SortExpression="qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Unit" SortExpression="gendesc">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Total Amount</TD><TD align=left><asp:TextBox id="amtbrutto" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w144" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Disc. Type 1</TD><TD align=left><asp:DropDownList id="trnbelidtldisctype" runat="server" Width="105px" CssClass="inpTextDisabled" __designer:wfdid="w145" AutoPostBack="True" Enabled="False"><asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
<asp:ListItem Value="PCT">PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lbl2" runat="server" Font-Size="X-Small" Text="Discount (Rp)" __designer:wfdid="w146"></asp:Label>&nbsp;1</TD><TD align=left><asp:TextBox id="trnbelidtldisc" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w147" AutoPostBack="True" MaxLength="12" Enabled="False">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Disc. Amount 1</TD><TD style="WIDTH: 192px" align=left><asp:TextBox id="amtdisc" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w148" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Total Disc</TD><TD align=left><asp:TextBox id="totdiscdtl" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w149" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Disc. Type 2</TD><TD align=left><asp:DropDownList id="trnbelidtldisctype2" runat="server" Width="105px" CssClass="inpTextDisabled" __designer:wfdid="w150" AutoPostBack="True" OnSelectedIndexChanged="trnbelidtldisctype2_SelectedIndexChanged" Enabled="False"><asp:ListItem Value="VCR">VOUCHER</asp:ListItem>
<asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
<asp:ListItem Value="PCT">PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lbl3" runat="server" Font-Size="X-Small" Text="Discount (Rp)" __designer:wfdid="w151"></asp:Label>&nbsp;2</TD><TD align=left><asp:TextBox id="trnbelidtldisc2" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w152" AutoPostBack="True" MaxLength="12" OnTextChanged="trnbelidtldisc2_TextChanged" Enabled="False">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD style="WIDTH: 192px" align=left><asp:TextBox id="amtdisc2" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w153" Visible="False" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Net.&nbsp;Amount</TD><TD align=left><asp:TextBox id="amtbelinetto2" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w154" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Note</TD><TD align=left><asp:TextBox id="trnbelidtlnote" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w155" MaxLength="30"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="noSj" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:wfdid="w156" Visible="False"></asp:Label></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w157" TargetControlID="trnbelidtlprice" Mask="999,999,999.99" MaskType="Number" ErrorTooltipEnabled="True" InputDirection="RightToLeft" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" CultureName="en-US"></ajaxToolkit:MaskedEditExtender></TD><TD style="FONT-SIZE: x-small" align=left colSpan=2><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w158" Visible="False" AlternateText="Update"></asp:ImageButton> <asp:ImageButton id="btnCancelDtl" onclick="btnCancelDtl_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w159" Visible="False" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=8><FIELDSET style="WIDTH: 100%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 150px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="tbldtl2" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w160" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelidtlseq" OnSelectedIndexChanged="tbldtl2_SelectedIndexChanged" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle Font-Size="X-Small" ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelidtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sjno" HeaderText="No. PDO">
<FooterStyle Width="75px"></FooterStyle>

<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="material" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidtlqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidtlprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdisc2" HeaderText="Voucher">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdisc" HeaderText="Discount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbelinetto" HeaderText="Total Amt.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelinote" HeaderText="No. SJ">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sjrefoid" Visible="False">
<HeaderStyle ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="sjoid" Visible="False">
<HeaderStyle ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label24" runat="server" CssClass="Important" Text="No detail Invoice !!" __designer:wfdid="w10"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="lblAmtjualnetto" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Invoice Detail = " __designer:wfdid="w161"></asp:Label> <asp:Label id="txtAmountNetto" runat="server" Font-Size="Small" Font-Bold="True" Text="0.00" __designer:wfdid="w162"></asp:Label></FIELDSET></TD></TR></TBODY></TABLE></asp:View>&nbsp; </asp:MultiView><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left>Last Update By <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w163"></asp:Label>&nbsp;On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w164"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSaveMstr" onclick="btnSaveMstr_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w165" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCancelMstr" onclick="btnCancelMstr_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w166"></asp:ImageButton> <asp:ImageButton id="btnDeleteMstr" onclick="btnDeleteMstr_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w167"></asp:ImageButton> <asp:ImageButton id="BtnPosting2" onclick="BtnPosting2_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w168" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:wfdid="w169" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnUnPost" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" __designer:wfdid="w207" OnClick="BtnUnPost_Click"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w170" Visible="False"></asp:ImageButton> </TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD vAlign=top align=left></TD><TD vAlign=top align=left></TD></TR><TR><TD vAlign=top align=left><asp:UpdatePanel id="upPreview" runat="server" __designer:dtid="562949953421348" __designer:wfdid="w172"><ContentTemplate __designer:dtid="562949953421349">
<asp:Panel id="pnlPreview" runat="server" Width="750px" CssClass="modalBox" __designer:wfdid="w173" Visible="False"><TABLE width="100%" align=center><TBODY><TR><TD align=center><asp:Label id="lblCaptPreview" runat="server" Font-Size="Medium" Font-Bold="True" Text="Preview Jurnal" __designer:wfdid="w174"></asp:Label></TD></TR><TR><TD><asp:Panel id="pnlScrollPreview" runat="server" Width="100%" Height="200px" __designer:wfdid="w175" ScrollBars="Vertical"><asp:GridView id="gvPreview" runat="server" Width="97%" ForeColor="#333333" __designer:wfdid="w176" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="code" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="desc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="credit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No Data Found !!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> </TD></TR><TR><TD align=center><asp:LinkButton id="lkbPostPreview" onclick="lkbPostPreview_Click" runat="server" Font-Bold="True" __designer:wfdid="w177">[ POST ]</asp:LinkButton> <asp:LinkButton id="lkbClosePreview" onclick="lkbClosePreview_Click" runat="server" Font-Bold="True" __designer:wfdid="w178">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><asp:Button id="beHidePreview" runat="server" __designer:wfdid="w179" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpePreview" runat="server" __designer:wfdid="w180" TargetControlID="beHidePreview" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptPreview" PopupControlID="pnlPreview" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel8" runat="server" __designer:dtid="844424930131998" __designer:wfdid="w181"><ContentTemplate __designer:dtid="844424930131999">
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" __designer:wfdid="w182" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting" __designer:wfdid="w183"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" __designer:wfdid="w184" AutoGenerateColumns="False" CellPadding="4" GridLines="None" OnRowDataBound="gvakun_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 18px" align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w185">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" __designer:wfdid="w186" TargetControlID="btnHidePosting2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" __designer:wfdid="w187" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel></TD><TD vAlign=top align=left><asp:UpdatePanel id="UpdatePanel5" runat="server" __designer:wfdid="w188"><ContentTemplate>
<asp:Panel id="pnlPosting" runat="server" Width="400px" CssClass="modalBox" __designer:wfdid="w189" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="Invoice Posting" __designer:wfdid="w190"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblpaymentype" runat="server" Text="Payment Type" __designer:wfdid="w191"></asp:Label></TD><TD align=left><asp:DropDownList id="paymentype" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w192" AutoPostBack="True" OnSelectedIndexChanged="paymentype_SelectedIndexChanged"><asp:ListItem>CASH</asp:ListItem>
<asp:ListItem>NONCASH</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label26" runat="server" Width="90px" Text="Reference No" __designer:wfdid="w193" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="referensino" runat="server" CssClass="inpText" __designer:wfdid="w194" Visible="False"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblcashbankacctg" runat="server" Text="Cash Account" __designer:wfdid="w195"></asp:Label></TD><TD align=left><asp:DropDownList id="cashbankacctg" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w196"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblapaccount" runat="server" Text="AP Account" __designer:wfdid="w197" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="apaccount" runat="server" Width="275px" CssClass="inpText" __designer:wfdid="w198" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblpurchasingaccount" runat="server" Text="Purchasing Account" __designer:wfdid="w199"></asp:Label></TD><TD align=left><asp:DropDownList id="purchasingaccount" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w200"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left><asp:Label id="Label21" runat="server" Text="Cash Account For Cost" __designer:wfdid="w201" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><asp:DropDownList id="CashForCost" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w202" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2>&nbsp;<asp:LinkButton id="lkbPosting" onclick="lkbPosting_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w203">[ POST ]</asp:LinkButton> <asp:LinkButton id="lkbCancel" onclick="lkbCancel_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w204">[ CANCEL ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting" runat="server" __designer:wfdid="w205" TargetControlID="btnHidePosting" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting" PopupControlID="pnlPosting" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting" runat="server" __designer:wfdid="w206" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR><TR><TD vAlign=top align=left></TD><TD style="WIDTH: 87px" vAlign=top align=left></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                <strong><span style="font-size: 9pt">
                                Form Unpost PI </span></strong> <strong><span style="font-size: 9pt">:.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" __designer:wfdid="w7" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w9"></asp:Image> </TD><TD class="Label" vAlign=middle align=left><asp:Label id="lblMessage" runat="server" ForeColor="Red" __designer:wfdid="w10"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" __designer:wfdid="w11" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w12"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" __designer:wfdid="w13" TargetControlID="beMsgBox" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" CausesValidation="False" __designer:wfdid="w14" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
        
</asp:Content>
