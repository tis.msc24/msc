<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptTRetur.aspx.vb" Inherits="rptTRetur"%>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Transfer Gudang Retur"></asp:Label></th>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 979px" align=center>&nbsp;&nbsp;&nbsp;&nbsp; <TABLE><TBODY><TR><TD style="WIDTH: 35%; font-size: small; vertical-align: top; white-space: nowrap; text-align: right;" align=right></TD><TD align=left colSpan=3></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 35%; font-size: small; white-space: nowrap; text-align: right;" id="tdPeriod1" align=right runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode : "></asp:Label></TD><TD style="VERTICAL-ALIGN: top" id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="62px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="62px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" PopupButtonID="imageButton1" TargetControlID="dateAwal" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" PopupButtonID="imageButton2" TargetControlID="dateAkhir" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 35%; font-size: small; white-space: nowrap; text-align: right;" align=right runat="server" Visible="true">No&nbsp;Transfer Gudang Retur&nbsp;:</TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="nota" runat="server" Width="236px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" onclick="btnSearchNota_Click1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:Label id="oid" runat="server" Visible="False"></asp:Label><BR /><asp:GridView id="GVNota" runat="server" Width="34%" Font-Size="X-Small" ForeColor="#333333" BorderColor="#DEDFDE" CellPadding="4" BorderWidth="1px" BorderStyle="Solid" UseAccessibleHeader="False" PageSize="5" DataKeyNames="trntrftoreturoid,trntrftoreturno" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="GVNota_SelectedIndexChanged1">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trntrftoreturno" HeaderText="No. Transfer Gudang Retur">
<HeaderStyle Wrap="False"></HeaderStyle>
</asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 35%; font-size: small; vertical-align: top; white-space: nowrap; text-align: right;" align=right><asp:Label id="lblAsal" runat="server" Width="88px" Text="Lokasi Asal :"></asp:Label></TD><TD style="WIDTH: 126px" align=left><asp:DropDownList id="ddlLocation" runat="server" Width="236px" CssClass="inpText"><asp:ListItem>ALL LOCATION</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 35%; font-size: small; vertical-align: top; white-space: nowrap; text-align: right;" align=right><asp:Label id="lblTujuan" runat="server" Text="Lokasi Tujuan :" Visible="False"></asp:Label></TD><TD style="WIDTH: 286px" align=left><asp:DropDownList id="ddlLocation1" runat="server" Width="236px" CssClass="inpText" Visible="False"><asp:ListItem>ALL LOCATION</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 35%; font-size: small; white-space: nowrap; text-align: right;" align=right><asp:Label id="Label3" runat="server" Width="96px" Text="Item / Barang :"></asp:Label>&nbsp;</TD><TD style="HEIGHT: 1px" align=left colSpan=3><asp:TextBox id="itemname" runat="server" Width="236px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" Visible="False"></asp:Label>&nbsp;&nbsp;<BR /><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" BorderColor="#DEDFDE" CellPadding="4" BorderWidth="1px" BorderStyle="Solid" UseAccessibleHeader="False" PageSize="12" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3" AutoGenerateColumns="False" AllowPaging="True"><Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Width="50px" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Width="50px" ForeColor="Navy" HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang"></asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang"></asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga"></asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga"></asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga"></asp:BoundField>
</Columns>

<RowStyle BackColor="#F7F7DE"></RowStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<PagerStyle HorizontalAlign="Right"></PagerStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 35%; font-size: small; vertical-align: top; white-space: nowrap; text-align: right;" align=right></TD><TD style="HEIGHT: 1px" align=left colSpan=3>&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD style="WIDTH: 35%; font-size: small; vertical-align: top; white-space: nowrap; text-align: right;" align=right></TD><TD style="WIDTH: 126px" align=left></TD><TD style="WIDTH: 246px" align=right><asp:DropDownList id="FilterDDLGrup" runat="server" Width="236px" CssClass="inpText" Visible="False"></asp:DropDownList></TD><TD style="WIDTH: 286px" align=left></TD></TR><TR><TD style="WIDTH: 35%; font-size: small; vertical-align: top; white-space: nowrap; text-align: right;" align=right></TD><TD style="WIDTH: 126px" align=left>&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList id="period" runat="server" Width="135px" CssClass="inpText" Visible="False"></asp:DropDownList> </TD><TD style="WIDTH: 246px" align=right><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="102px" CssClass="inpText" Visible="False"></asp:DropDownList>&nbsp;</TD><TD style="WIDTH: 286px" align=left></TD></TR></TBODY></TABLE><asp:Label id="labelEx" runat="server" Width="98px"></asp:Label></TD></TR><TR><TD style="WIDTH: 979px"></TD></TR><TR><TD style="WIDTH: 979px; HEIGHT: 32px" align=center><asp:ImageButton id="btnreport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Sedang proses....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD style="WIDTH: 979px" align=center><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="WIDTH: 979px; HEIGHT: 90px" align=center></TD></TR><TR><TD style="WIDTH: 979px" align=center><TABLE><TBODY><TR><TD align=left><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE> <asp:UpdatePanel id="UpdatePanel2" runat="server"><ContentTemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width=250><TBODY><TR><TD style="WIDTH: 294px; HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=250><TBODY><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" OnClick="btnErrOK_Click"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelErrMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</asp:Content>

