'Developt By Shutup_M
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnVoucher
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim sql1 As String = ""
    Dim cProc As New ClassProcedure
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Private ws As DataTable
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
    Dim cRate As New ClassRate
    Dim cKon As New Koneksi
    Dim dtotal As Double
#End Region

#Region "Prosedure"
    Protected Sub ClearDtlVoucher()
        txtVoucher.Text = "" : lblCoa.Text = ""
        cashbanknote.Text = "" : AmtVdtl.Text = "0.00"
        no.Text = "" : AmtSisaDtl.Text = "0.00" : vDtlAmt.Text = "0.00"
        I_Udtl.Text = "New Detail" : labelseq.Text = "1"
        lblTampung.Text = "0.00" : selisihamount.Text = "0.00"
    End Sub

    Private Sub HitungVoucher()
        '--Hitung Sisa Amount Header
        selisihamount.Text = ToMaskEdit(realisasiamt.Text - AmtRealHdr.Text, 4)
        Dim ttVhdr As Double = ToDouble(realisasiamt.Text) - ToDouble(AmtRealHdr.Text)
        If ToDouble(ttVhdr) < 0 Then
            selisihamount.Text = ToMaskEdit(AmtRealHdr.Text - realisasiamt.Text, 4)
        ElseIf ToDouble(ttVhdr) > 0 Then
            selisihamount.Text = ToMaskEdit(realisasiamt.Text - AmtRealHdr.Text, 4)
        End If
    End Sub

    Protected Sub HitungDtlAmt()
        Dim iCountrow As Integer = GVvOucherDtl.Rows.Count
        Dim iGtotal, SisaTotal, iReal As Double
        Dim dt_temp As New DataTable : dt_temp = Session("QL_trnrealisasidtl")
        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(GVvOucherDtl.Rows(i).Cells(4).Text)
            iReal += ToDouble(GVvOucherDtl.Rows(i).Cells(5).Text)
            SisaTotal += ToDouble(GVvOucherDtl.Rows(i).Cells(6).Text)
            dt_temp.Rows(i).BeginEdit()
            dt_temp.Rows(i).Item(0) = i + 1
            dt_temp.Rows(i).EndEdit()
        Next
        Session("QL_trnrealisasidtl") = dt_temp
        GVvOucherDtl.DataSource = dt_temp
        GVvOucherDtl.DataBind()
        lblTampung.Text = ToMaskEdit(ToDouble(iGtotal), 4)
        SisaAmtDtl.Text = ToMaskEdit(ToDouble(SisaTotal), 4)
        AmtRealHdr.Text = ToMaskEdit(ToDouble(iReal), 4)
        HitungVoucher()
    End Sub

    Public Overloads Shared Function CheckYearIsValid(ByVal sdate As Date) As Boolean
        Dim min As Integer = 2000 : Dim max As Integer = 2072
        CheckYearIsValid = False
        If sdate.Year < min Then
            CheckYearIsValid = True
        End If
        If sdate.Year > max Then
            CheckYearIsValid = True
        End If
        Return CheckYearIsValid
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindAkun()
        sSql = "Select v.voucheroid,voucherno,voucherdesc,ISNULL(amtrealdtl,0.00) amtrealdtl,amtvoucheridr amtAsliV,ISNULL(amtvoucheridr,0.00)-ISNULL(amtrealdtl,0.00) amtvoucheridr,ISNULL(rd.amtsisadtl,0.00) amtsisadtl,amtdtltemp From QL_voucherdtl v LEFT JOIN QL_trnrealisasidtl rd ON rd.voucheroid=v.voucheroid Where trnbelino='" & Tchar(NoPoTxt.Text.Trim) & "' AND (voucherno LIKE '%" & Tchar(txtVoucher.Text.Trim) & "%' OR voucherdesc LIKE '%" & Tchar(txtVoucher.Text.Trim) & "%') And (ISNULL(amtvoucheridr,0.00)-ISNULL(amtrealdtl,0.00)) > 0.00"
        Dim dtVo As DataTable = GetDataTable(sSql, "QL_VoucherDtl")
        Session("tblPO") = dtVo : GvVoucher.DataSource = dtVo
        GvVoucher.DataBind() : GvVoucher.Visible = True
    End Sub

    Private Sub BindPO()
        sSql = "SELECT * FROM (Select po.trnbelimstoid,po.trnbelipono AS NoPo ,(Select Isnull(SUM(amtdisc2idr),0.00) from QL_podtl Where trnbelimstoid=po.trnbelimstoid And po.flagVcr=1 AND ISNULL(amtdisc2idr,0.00) <> 0)-ISNULL((Select SUM(Case  When (rd.amtvoucherdtl-rd.amtrealdtl) < 0 Then rd.amtvoucherdtl else rd.amtrealdtl end) from QL_trnrealisasidtl rd INNER JOIN QL_trnrealisasi rm ON rm.realisasioid=rd.trnrealisasioid Where rm.pomstoid=po.trnbelimstoid),0.00) VoucherPI,ISNULL((SELECT SUM(amtdisc2idr) From QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code Where bm.trnbelipono=po.trnbelipono HAving SUM(amtdisc2idr) <> 0),0.00) VoucherPI1,'' NoPI,suppname,(Select SUM(sjd.qty) From ql_trnsjbelidtl sjd Inner Join ql_trnsjbelimst sjm ON sjd.trnsjbelioid=sjm.trnsjbelioid INNER JOIN QL_podtl pdt ON pdt.trnbelidtloid=sjd.trnbelidtloid Where sjm.trnbelipono=po.trnbelipono AND sjm.trnsjbelistatus='INVOICED') qtysj,(Select SUM(pd.trnbelidtlqty) From QL_podtl pd Where pd.trnbelimstoid=po.trnbelimstoid AND trnbelidtlstatus='completed' AND pd.branch_code=po.branch_code) qtypo,ISNULL((Select SUM(r.realisasiamt) from QL_trnrealisasi r Where r.pomstoid=po.trnbelimstoid),0.00) AmtReal From QL_pomst po INNER JOIN QL_mstsupp sp ON sp.suppoid=po.trnsuppoid Where flagVcr=1 AND po.trnbelipono IN (SELECT trnbelipono FROM QL_trnbelimst bm Where trnbelipono=po.trnbelipono AND po.branch_code=bm.branch_code AND trnbelistatus = 'POST') AND (po.trnbelipono LIKE '%" & Tchar(NoPoTxt.Text.Trim) & "%' OR sp.suppname LIKE '%" & Tchar(NoPoTxt.Text.Trim) & "%') AND po.trnbelimstoid IN (Select Distinct trnbelimstoid From QL_podtl pod Where pod.trnbelimstoid=po.trnbelimstoid And pod.trnbelidtlstatus='completed') AND (select COUNT(*) from ql_trnsjbelimst f where f.trnbelipono=po.trnbelipono And trnsjbelistatus<>'INVOICED' AND po.branch_code=f.branch_code)=0 ) AS RV Where VoucherPI > 0 AND (qtypo-qtysj)=0 AND VoucherPI<>0.00"
        
        Dim dtPo As DataTable = GetDataTable(sSql, "QL_vPo")
        Session("tblPO") = dtPo : gvPO.DataSource = dtPo
        gvPO.DataBind() : gvPO.Visible = True
    End Sub

    Public Sub binddata(ByVal sType As String)
        sSql = "Select 0 Selected,r.cmpcode,realisasioid,realisasino,CONVERT(Char(10),realisasidate,103) realisasidate,realisasistatus,realisasitype,po.trnbelimstoid pomstoid,amttemp realisasiamt,po.trnbelipono,0 voucherdesc,Realisasinote,0 voucheroid FROM QL_trnrealisasi r INNER JOIN QL_pomst po ON r.pomstoid=po.trnbelimstoid " & sType & " ORDER BY realisasioid DESC"
        Dim objTable As DataTable = GetDataTable(sSql, "QL_trnrealvo")
        Session("tbldata") = objTable : gvmstcost.DataSource = objTable
        gvmstcost.DataBind() : UnabledCheckBox()
    End Sub

    Public Sub bindLastSearched()
        If (Not Session("SearchReceipt") Is Nothing) Or (Not Session("SearchReceipt") = "") Then
            Dim mySqlConn As New SqlConnection(ConnStr)
            Dim sqlSelect As String = Session("SearchReceipt")

            Dim objTable As DataTable = ClassFunction.GetDataTable(sqlSelect, "Receipt")
            Session("tbldata") = objTable : gvmstcost.DataSource = objTable
            gvmstcost.DataBind() : UnabledCheckBox()
            mySqlConn.Close()
        End If

    End Sub

    Private Sub BindCOA(ByVal sFilter As String)
        'FillGVAcctg(GvAccCost, "VAR_RECEIPT1", "MSC", sFilter)
        'btnHidden.Visible = True : Panel1.Visible = True
        'GvAccCost.Visible = True : Mpe1.Show()
    End Sub

    Public Sub filltextbox(ByVal sCmpcode As String, ByVal vpayid As String)
        sSql = "Select po.branch_code,r.cmpcode,realisasioid,realisasino,realisasidate,realisasistatus,realisasitype,r.pomstoid trnbelimstoid,realisasiamt,po.trnbelipono trnbelino,ISNULL(r.amtvoucherpi,0.00) amtvoucherpi,Realisasinote,r.upduser,r.updtime,r.selisihamt,r.acctgoidreal,ISNULL(amtsisa,0.00) amtsisa,amttemp,amttempsisa FROM QL_trnrealisasi r INNER JOIN QL_pomst po ON r.pomstoid=po.trnbelimstoid Where realisasioid=" & vpayid & ""

        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                RealisasiOid.Text = Trim(xreader("realisasioid"))
                payflag.SelectedValue = Trim(xreader("realisasitype"))
                RealisasiNo.Text = xreader("realisasino").ToString
                Realisasidate.Text = Format(xreader("realisasidate"), "dd/MM/yyyy")
                cashbanknote.Text = xreader("Realisasinote").ToString
                NoPoTxt.Text = xreader("trnbelino").ToString
                selisihamount.Text = ToMaskEdit(xreader("amtsisa"), 4)
                realisasiamt.Text = ToMaskEdit(ToDouble(xreader("amtvoucherpi")), 4)
                lblTampung.Text = ToMaskEdit(ToDouble(xreader("amttemp")), 4)
                AmtRealHdr.Text = ToMaskEdit(ToDouble(xreader("amttemp")), 4)
                NoPoTxt.Enabled = False : NoPoTxt.CssClass = "inpTextDisabled"
                RealisasiNo.Enabled = False : RealisasiNo.CssClass = "inpTextDisabled"
                btnCariPO.Visible = False : ErasePObtn.Visible = False
                lblPOST.Text = xreader("realisasistatus").ToString
                OidPO.Text = xreader("trnbelimstoid")
                SisaAmtDtl.Text = ToMaskEdit(ToDouble(xreader("amttempsisa")), 4)
                upduser.Text = "Last Update By <B>" & Trim(xreader("upduser")) & "</B> On <B>" & Trim(xreader("updtime")) & "</B> "

                If Trim(xreader("realisasistatus").ToString.ToUpper) = "IN APPROVAL" Then
                    btnshowCOA.Visible = True : BtnSendAp.Visible = False
                    btnsave.Visible = False : btnDelete.Visible = False
                    lblPOST.Text = Trim(xreader("realisasistatus")).Trim.ToString
                    lblcashbankno.Visible = True : drafno.Visible = False
                Else
                    If Trim(xreader("realisasistatus")).ToUpper = "IN PROCESS" Then
                        cabang.Text = Trim(xreader("branch_code"))
                    End If
                    btnshowCOA.Visible = False
                End If
                RealisasiOid.Visible = False
                If Trim(xreader("realisasistatus").ToString.ToUpper) = "APPROVED" Then
                    GVvOucherDtl.Columns(7).Visible = True
                    GVvOucherDtl.Columns(8).Visible = True
                Else
                    GVvOucherDtl.Columns(7).Visible = False
                    GVvOucherDtl.Columns(8).Visible = False
                End If
            End While
        End If
        objConn.Close()
        sSql = "Select rd.seqdtl,rd.voucheroid,vd.voucherdesc,rd.amtvoucherdtl,rd.amtrealdtl,rd.amtsisadtl,rd.notedtl,amtdtltemp,amtdtltempsisa From QL_trnrealisasidtl rd INNER JOIN QL_voucherdtl vd ON vd.voucheroid=rd.voucheroid Where rd.trnrealisasioid=" & vpayid & ""
        Dim dtab As DataTable = cKon.ambiltabel(sSql, "detailtw")
        GVvOucherDtl.DataSource = dtab : GVvOucherDtl.DataBind()
        Session("QL_trnrealisasidtl") = dtab
    End Sub

    Private Sub GenerateNoRealisasi()
        If i_u2.Text = "New Data" Then
            Dim iCurID As Integer = 0
            Dim Cabang As String = GetStrData("select substring (gendesc,1,3) from ql_mstgen Where gencode='" & Session("branch_id") & "' AND gengroup='CABANG'")
            Session("vNoCashBank") = "RV/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT MAX(replace(realisasino,'" & Session("vNoCashBank") & "','')) realisasino FROM QL_trnrealisasi WHERE realisasino LIKE '" & Session("vNoCashBank") & "%' AND cmpcode='" & CompnyCode & "'"

            If Not IsDBNull(GetScalar(sSql)) Then
                iCurID = GetScalar(sSql) + 1
            Else
                iCurID = 1
            End If
            Session("sNo") = GenNumberString(Session("vNoCashBank"), "", iCurID, 4)
        End If
    End Sub

    Public Sub CheckAll()
        Dim objdt As DataTable = Session("tbldata")
        If objdt Is Nothing Then
            showMessage("- No Found Data Click Button Find Or View All..!!<br>", 2)
            Exit Sub
        End If
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For C1 As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(C1)("cashbankstatus").ToString.ToUpper) <> "POST" Then
                    objRow(C1)("selected") = 1
                    objTable.AcceptChanges()
                End If
            Next
        End If
        Session("tbldata") = objTable
        gvmstcost.DataSource = objTable
        gvmstcost.DataBind()
    End Sub

    Public Sub UnabledCheckBox()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (gvmstcost.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("cashbankstatus").ToString) = "POST" Or Trim(objRow(C1)("cashbankstatus").ToString) = "DELETE" Then

                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Dim objdt As DataTable = Session("tbldata")
        If objdt Is Nothing Then
            showMessage("- Not Found Data Click Button Find Or View All..!!<br>", 2)
            Exit Sub
        End If
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For C1 As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(C1)("cashbankstatus").ToString) <> "POST" Then
                    objRow(C1)("selected") = 0
                    objTable.AcceptChanges()
                End If
            Next
        End If
        Session("tbldata") = objTable
        gvmstcost.DataSource = objTable
        gvmstcost.DataBind()
    End Sub

    Private Sub UpdateCheckedPost()
        'Untuk mendapat cek box value multi select
        Dim dv As DataView = Session("tbldata").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvmstcost.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        Dim a As String() = sOid.Split(",")
                        dv.RowFilter = "cashbankoid=" & a(1) & " AND cmpcode='" & a(0) & "'"
                        If cbcheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnVoucher.aspx?awal=true")
            End If
        End If
    End Sub

    Private Sub ddlCOACb(ByVal cVar As String)
        FillDDLAcctg(DdlAkun, cVar, Session("branch_id"))
    End Sub

#End Region

#Region "Function"

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Public Function GetEnabled() As Boolean
        Return Eval("realisasistatus").ToString.ToUpper = "IN PROCESS"
    End Function

    Public Function GetCheckedOid() As String
        Return Eval("cmpcode") & "," & Eval("realisasioid")
    End Function

    Public Function GetPrintHdr() As String
        Return Eval("cmpcode") & "," & Eval("realisasioid")
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode = '" & sFilterCode & "' AND acctgflag='A'"
        'If Not bParentAllowed Then
        '    sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
        'End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("~\Accounting\trnVoucher.aspx")
        End If
        Session("sCmpcode") = Request.QueryString("cmpcode")
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        BtnSendAp.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Send this data?');")
        Page.Title = CompnyName & " - Pencariran Voucher"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName)
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        binddata("")
        If Not IsPostBack Then
            payflag_SelectedIndexChanged(Nothing, Nothing)
            txtPeriode1.Text = Format(GetServerTime.AddDays(-2), "dd/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            RealisasiOid.Text = GenerateID("QL_trnrealisasi", CompnyCode)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u2.Text = "Update Data" : BtnSendAp.Visible = True
                filltextbox(Session("sCmpcode"), Session("oid"))
                lblIO.Text = "U P D A T E"
                UnabledCheckBox()
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 1
                payflag.Enabled = False : payflag.CssClass = "inpTextDisabled"
                'payflag_SelectedIndexChanged(sender, e)
            Else
                i_u2.Text = "New Data" : BtnSendAp.Visible = False
                Realisasidate.Text = Format(GetServerTime, "dd/MM/yyyy")
                GenerateNoRealisasi()
                RealisasiNo.Text = Session("sNo")
                lblPOST.Text = "IN PROCESS" : btnDelete.Visible = False
                upduser.Text = "-" : updtime.Text = "-"
                TabContainer1.ActiveTabIndex = 0
            End If

        End If
        If lblPOST.Text = "APPROVED" Then
            btnshowCOA.Visible = True : btnPrint.Visible = False
            BtnSendAp.Visible = False : btnDelete.Visible = False
            btnsave.Visible = False
        Else
            btnPrint.Visible = False : btnshowCOA.Visible = False
        End If
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sVar As String = ""
        Dim dVal As Boolean = False
        If payflag.SelectedValue = "BANK" Then
            sVar = "VAR_BANK" : dVal = True
            LblAkun.Visible = True : DdlAkun.Visible = True
        ElseIf payflag.SelectedValue = "CASH" Then
            sVar = "VAR_CASH" : dVal = True
            LblAkun.Visible = True : DdlAkun.Visible = True
        Else
            LblAkun.Visible = False : DdlAkun.Visible = False
        End If
        ddlCOACb(sVar)
    End Sub

    Protected Sub BtnCari_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindCOA("")
    End Sub

    Protected Sub GVDtlCost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("dtlTable") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("dtlTable")
                Dim dv As DataView = objTable.DefaultView
                realisasiamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("cashbankglamt").ToString), 4)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing : gvmstcost.PageIndex = 0
        Dim sType As String = "" : Dim sMsg As String = ""
        Dim tgle As Date = CDate(toDate(txtPeriode1.Text))
        'Dim tgle2 As Date = CDate(toDate(txtPeriode2.Text))
        Try
            tgle = CDate(toDate(txtPeriode2.Text))
            'tgle2 = CDate(toDate(txtPeriode2.Text))
        Catch ex As Exception
            sMsg &= "- Maaf, Format tanggal salah !!<br>"
            Exit Sub
        End Try

        If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
            sMsg &= "- Periode akhir harus >= periode awal !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        If statuse.SelectedValue <> "ALL" Then
            sType &= "AND realisasistatus='" & statuse.SelectedValue & "'"
        End If
        If payType.SelectedValue <> "ALL" Then
            sType &= "AND realisasitype ='" & payType.SelectedValue & "'"
        End If
        sType &= " AND realisasidate Between '" & (toDate(txtPeriode1.Text)) & "' AND '" & (toDate(txtPeriode2.Text)) & "' AND " & DDlfilter.SelectedValue & " LIKE '%" & Tchar(no.Text.Trim) & "%'"
        binddata(sType) : Session("SearchExpense") = sql_temp
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing
        txtPeriode1.Text = Format(GetServerTime, "dd/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
        gvmstcost.PageIndex = 0 : statuse.SelectedIndex = 0
        no.Text = "" : binddata("") 
    End Sub

    Protected Sub gvmstcost_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvmstcost.PageIndexChanging
        gvmstcost.PageIndex = e.NewPageIndex
        Dim sType As String = "" : Dim sMsg As String = ""
        'Try
        '    Dim tgle As Date = CDate(toDate(txtPeriode1.Text))
        '    tgle = CDate(toDate(txtPeriode1.Text))
        '    Dim tgle2 As Date = CDate(toDate(txtPeriode1.Text))
        '    tgle2 = CDate(toDate(txtPeriode2.Text))
        'Catch ex As Exception
        '    showMessage("Invalid format date !!", 2)
        '    Exit Sub
        'End Try

        If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
            sMsg &= "- Periode akhir harus >= periode awal !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        If statuse.SelectedValue <> "ALL" Then
            sType &= "AND realisasistatus='" & statuse.SelectedValue & "'"
        End If
        If payType.SelectedValue <> "ALL" Then
            sType &= "AND realisasitype ='" & payType.SelectedValue & "'"
        End If
        sType &= " AND realisasidate Between '" & (toDate(txtPeriode1.Text)) & "' AND '" & (toDate(txtPeriode2.Text)) & "' AND " & DDlfilter.SelectedValue & " LIKE '%" & Tchar(no.Text.Trim) & "%'"
        binddata(sType)
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UncheckAll()
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim sMsg As String = "" : Dim dateStat1 As Boolean
        Dim dt1 As Date : Dim NoReal As String = ""

        If Session("oid") = Nothing Or Session("oid") = "" Then
            GenerateNoRealisasi()
        Else
            NoReal = RealisasiNo.Text
        End If

        If NoPoTxt.Text = "" Then
            showMessage("- Belum memilih PI..!!<BR>", 2)
            Exit Sub
        End If

        If i_u2.Text = "New Data" Then
            dt1 = CDate(toDate(Realisasidate.Text))
        Else
            dt1 = CDate(toDate(Realisasidate.Text))
        End If

        'If Session("oid") = Nothing Or Session("oid") = "" Then
        '    If cKon.ambilscalar("select COUNT (trnbelimstoid) from ql_trnrealisasi where trnbelimstoid = " & OidPO.Text & " AND realisasistatus='IN PROCESS'") > 0 Then
        '        sMsg &= "- Ada Nomor PI Yang Masih IN PROCESS Pada Transaksi Realisasi Voucher, Posting Terlebih Dahulu!<BR>"
        '    End If
        'End If

        Try
            dateStat1 = True
        Catch ex As Exception
            dateStat1 = False
            sMsg &= "- Format Tanggal salah !!<BR>"
        End Try

        Try
            If cashbanknote.Text.Length >= 100 Then
                sMsg &= "- Note harus <= 100 Character...!!<BR>"
            End If
        Catch ex As Exception
        End Try

        'If CDate(Realisasidate.Text) <= CDate(CutofDate.Text) Then
        '    sMsg &= "- Tanggal Tidak boleh <= CutoffDate (" & CutofDate.Text & ") !! <BR> "
        'End If
        'Dim sDate As String = dt1
        'Dim aDate As Date = Format(GetServerTime(), "MM/dd/yyyy")
        'If dt1 < Format(GetServerTime(), "MM/dd/yyyy") Then
        '    sMsg &= "- Tanggal realisasi (" & Realisasidate.Text & ") Tidak boleh <= tanggal (" & Format(GetServerTime(), "dd/MM/yyyy") & ") !! <BR> "
        'End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            lblPOST.Text = "IN PROCESS"
            Exit Sub
        End If
        '==============================
        'Cek Peroide Bulanan Dari Crdgl
        '==============================
 
        sSql = "select distinct left(isnull(periodacctg,''),4)+'-'+substring(isnull(periodacctg,''),6,2) FROM QL_crdgl Where glflag='OPEN'"
        If GetPeriodAcctg(CDate(toDate(Realisasidate.Text))) < GetStrData(sSql) Then
            showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>", 2)
            Exit Sub
        End If

        Dim cRate As New ClassRate()
        Dim vCMst As Integer = GenerateID("QL_trncashbankmst", CompnyCode)
        Dim vIDgl As Integer = GenerateID("QL_cashbankgl", CompnyCode)
        Dim vIDMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim vIDDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
        'Dim amtdt As Double
        Dim Closing As String = "" : Dim realisasi As String = ""
        Dim gantungPiutang As String = "" : Dim dAcctg As String = ""
        Dim cAcctg As String = "" : Dim varselisih As String = ""
        Dim RealAcctgOid, vAcctoid, cash, bank, AcctgLabaOid As Integer

        If cKon.ambilscalar("select COUNT(trnbelimstoid) from ql_trnrealisasi where pomstoid = " & OidPO.Text & "") > 0 Then
            sSql = "SELECT bm.trnbelimstoid,trnbelidtloid,trnbelipono,selisih amtdisc2idr,ISNULL((bd.amtdisc2idr-realisasiamt),0.00) AS amtrealisasi,ISNULL(realisasiamt,0.00) As AmtMurni,selisih FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelino='" & NoPoTxt.Text.Trim & "' ORDER BY trnbelidtloid ASC"
        Else
            sSql = "SELECT bm.trnbelimstoid,trnbelidtloid,trnbelipono,Case When trnbelidtlres1 ='In Process' then bd.amtdisc2idr else ISNULL(bd.amtdisc2idr - realisasiamt,0.00) end amtdisc2idr,ISNULL((bd.amtdisc2idr-realisasiamt),0.00) AS amtrealisasi,ISNULL(realisasiamt,0.00) As AmtMurni,selisih FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelino='" & NoPoTxt.Text.Trim & "' ORDER BY trnbelidtloid ASC"
        End If

        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnbelidtl")
        If objTbl.Rows.Count = 0 Then
            If lblPOST.Text = "POST" Then
            Else
                'If Session("oid") = Nothing Or Session("oid") = "" Then
                '    showMessage("Tidak Ada Detil PI <br>", 2)
                '    Exit Sub
                'End If
            End If
            sSql = "SELECT realisasiamt FROM QL_trnbelidtl dtl INNER JOIN QL_trnbelimst mst ON mst.trnbelimstoid = dtl.trnbelimstoid WHERE mst.trnbelino='" & NoPoTxt.Text & "'"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "realisasiamt")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = " realisasiamt > 0"
            If dv.Count > 0 Then
                For C2 As Integer = 0 To dv.Count - 1
                    dtotal += dtotal + dv.Item(C2).Item("realisasiamt").ToString
                Next
            End If
            dv.RowFilter = ""
        End If

        'AMBIL VAR VAR_REALISASI_VOUCHER
        realisasi = GetVarInterface("VAR_REALISASI_VOUCHER", Session("branch_id"))
        If realisasi = "0" Or realisasi Is Nothing Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_REALISASI_VOUCHER'!", 2)
            Exit Sub
        End If
        RealAcctgOid = GetAccountOid(realisasi, False)

        'AMBIL VAR VAR_VOUCHER
        gantungPiutang = GetVarInterface("VAR_VOUCHER", Session("branch_id"))
        If gantungPiutang = "0" Or gantungPiutang Is Nothing Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_VOUCHER' !!", 2)
            Exit Sub
        End If
        vAcctoid = GetAccountOid(gantungPiutang, False)

        'AMBIL VAR VAR_PENDAPATAN_VOUCHER
        varselisih = GetVarInterface("VAR_PENDAPATAN_VOUCHER", Session("branch_id"))
        If varselisih = "0" Or varselisih Is Nothing Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_PENDAPATAN_VOUCHER'!", 2)
            Exit Sub
        End If
        AcctgLabaOid = GetAccountOid(varselisih, False)
        '------KONDISI TYPE REALISASI---------
        '-------------------------------------
        If payflag.SelectedValue = "CASH" Then
            cash = DdlAkun.SelectedValue 'varbank = DdlAkun.SelectedValue
            If cash <= 0 Then
                showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_CASH' !!", 2)
                Exit Sub
            End If
        ElseIf payflag.SelectedValue = "BANK" Then
            bank = DdlAkun.SelectedValue 'varcash = DdlAkun.SelectedValue
            If bank <= 0 Then
                showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_BANK' !!", 2)
                Exit Sub
            End If
        Else
            bank = 0 : cash = 0
        End If

        If payflag.SelectedValue = "CLOSED" Then
            dAcctg = RealAcctgOid : cAcctg = vAcctoid
        ElseIf payflag.SelectedValue = "CASH" Then
            dAcctg = DdlAkun.SelectedValue : cAcctg = vAcctoid
        ElseIf payflag.SelectedValue = "BANK" Then
            dAcctg = DdlAkun.SelectedValue : cAcctg = vAcctoid
        Else
            dAcctg = RealAcctgOid : cAcctg = vAcctoid
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If

        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            '===== UPdate tabel beli detail ======
            Session("TblDtl") = objTbl
            Dim Tb As DataView = objTbl.DefaultView
            Dim amts, cekamt, sisarealisasi, selisihrealisasi As Double
            Dim realselisih As Double = 0.0 : Dim StatusPI As String = ""

            For c1 As Int16 = 0 To objTbl.Rows.Count - 1
                sisarealisasi = ToDouble(realisasiamt.Text) - ToDouble(dtotal)
                If sisarealisasi > 0 Then 
                    cekamt = ToDouble(sisarealisasi) - (ToDouble(objTbl.Rows(c1).Item("amtdisc2idr")))
                    If cekamt >= 0 Then
                        amts = ToDouble(objTbl.Rows(c1).Item("amtdisc2idr"))
                    Else
                        amts = sisarealisasi
                        'If i_u2.Text = "New Data" Then
                        '    If ToDouble(objTbl.Rows(c1).Item("AmtMurni")) > 0 Then
                        '        amts = ToDouble(realisasiamt.Text) + ToDouble(objTbl.Rows(c1).Item("AmtMurni"))
                        '    Else
                        '        amts = ToDouble(realisasiamt.Text) - mTotal
                        '    End If
                        'Else
                        '    If ToDouble(objTbl.Rows(c1).Item("amtrealisasi")) = 0 Then
                        '        amts = ToDouble(objTbl.Rows(c1).Item("amtdisc2idr"))
                        '    Else
                        '        amts = ToDouble(realisasiamt.Text) - mTotal
                        '    End If
                        'End If
                    End If

                    dtotal += ToDouble(objTbl.Rows(c1).Item("amtdisc2idr"))
                    Session("dtotal") = dtotal

                    'sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1 = '" & lblPOST.Text.Trim & "',realisasiamt =realisasiamt + " & amts & ",realisasioid=" & RealisasiOid.Text & ",selisih = " & ToDouble(objTbl.Rows(c1).Item("amtdisc2idr")) - amts & "  WHERE trnbelimstoid=" & objTbl.Rows(c1).Item("trnbelimstoid") & " AND trnbelidtloid=" & objTbl.Rows(c1).Item("trnbelidtloid") & " And branch_code='" & Session("branch_id") & "'"
                    'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    'sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1 = 'IN PROCESS',realisasiamt =0 ,realisasioid=0 WHERE trnbelimstoid=" & objTbl.Rows(c1).Item("trnbelimstoid") & " AND trnbelidtloid=" & objTbl.Rows(c1).Item("trnbelidtloid") & " And branch_code='" & Session("branch_id") & "'"
                    'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                
            Next 

            'sSql = "SELECT bm.trnbelimstoid FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelipono='" & Tchar(NoPoTxt.Text.Trim) & "' AND (ISNULL((bd.amtdisc2idr-realisasiamt),0.00)) <> 0 and realisasiamt <> 0 ORDER BY trnbelidtloid ASC"
            'xCmd.CommandText = sSql : mstoid = xCmd.ExecuteScalar()

            'sSql = "SELECT trnbelidtloid FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelipono='" & Tchar(NoPoTxt.Text.Trim) & "' AND (ISNULL((bd.amtdisc2idr-realisasiamt),0.00)) <> 0 and realisasiamt <> 0 ORDER BY trnbelidtloid ASC"
            'xCmd.CommandText = sSql : dtloid = xCmd.ExecuteScalar()

            'sSql = "SELECT ISNULL((bd.amtdisc2idr-realisasiamt),0.00) AS amtrealisasi FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelipono='" & Tchar(NoPoTxt.Text.Trim) & "' AND (ISNULL((bd.amtdisc2idr-realisasiamt),0.00)) <> 0 and realisasiamt <> 0 ORDER BY trnbelidtloid ASC"
            'xCmd.CommandText = sSql : realselisih = xCmd.ExecuteScalar()

            'sSql = "UPDATE QL_trnbelidtl SET selisih=" & ToDouble(realselisih) & " WHERE trnbelimstoid=" & mstoid & " AND trnbelidtloid=" & dtloid & " And branch_code='" & Session("branch_id") & "'"

            'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'KELEBIHAN AMOUNT REALISASI TERHADAP PIUTANG GANTUNG
            If selisihrealisasi = ToDouble(realisasiamt.Text) - ToDouble(dtotal) < 0 Then
                selisihrealisasi = 0
            End If

            'If selisihrealisasi > 0 Then
            '    varselisih = GetVarInterface("VAR_SELISIH_REALISASI_VOUCHER", "MSC")
            '    selisihrealisasiAcctgOid = GetAccountOid(selisihrealisasi, False)
            '    If varselisih Is Nothing Then
            '        showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_SELISIH_REALISASI_VOUCHER' !!", 2)
            '        Exit Sub
            '    End If
            'End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO QL_trnrealisasi (cmpcode,realisasioid,realisasino,realisasidate,realisasistatus,realisasiamt,pomstoid,trnbelimstoid,trnbelidtloid,acctgoid,upduser,updtime,Realisasinote,selisihamt,acctgoidreal,amtvoucherpi,amtsisa,realisasitype,amttemp,amttempsisa)" & _
                " VALUES ('MSC'," & RealisasiOid.Text & ",'" & RealisasiNo.Text & "','" & CDate(toDate(Realisasidate.Text)) & "','" & lblPOST.Text.Trim & "'," & ToDouble(AmtRealHdr.Text) & "," & OidPO.Text & ",0,0,0,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Tchar(cashbanknote.Text) & "'," & ToDouble(selisihamount.Text) & ",0," & ToDouble(realisasiamt.Text) & "," & ToDouble(selisihamount.Text) & ",''," & ToDouble(AmtRealHdr.Text) & "," & ToDouble(SisaAmtDtl.Text) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & RealisasiOid.Text & " WHERE tablename='QL_trnrealisasi' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                sSql = "UPDATE QL_trnrealisasi Set realisasidate='" & CDate(toDate(Realisasidate.Text)) & "', realisasistatus='" & lblPOST.Text.Trim & "', realisasiamt=" & ToDouble(AmtRealHdr.Text) & ", pomstoid=" & OidPO.Text & ", upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, Realisasinote='" & Tchar(cashbanknote.Text.Trim) & "', selisihamt=" & ToDouble(selisihamount.Text) & ", amtsisa=" & ToDouble(selisihamount.Text) & ", amttemp=" & ToDouble(AmtRealHdr.Text) & ", amttempsisa=" & ToDouble(selisihamount.Text) & ", amtvoucherpi=" & ToDouble(realisasiamt.Text) & " Where realisasioid=" & RealisasiOid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            '--Insert Detail Realisasi
            If Not Session("QL_trnrealisasidtl") Is Nothing Then
                Dim objTable As DataTable
                Dim objRow() As DataRow

                objTable = Session("QL_trnrealisasidtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

              sSql = "Delete QL_trnrealisasidtl Where trnrealisasioid=" & RealisasiOid.Text & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Session("vIDCost") = GenerateID("QL_trnrealisasidtl", "MSC")
                Dim dateStr As String = "'" & toDate(Realisasidate.Text) & "'"
                
                For C1 As Int16 = 0 To objRow.Length - 1
                    sSql = "INSERT INTO QL_trnrealisasidtl (cmpcode,trnrealisasidtloid,trnrealisasioid,seqdtl,voucheroid,amtrealdtl,amtvoucherdtl,amtsisadtl,dtlstatus,updtime,upduser,notedtl,amtdtltemp,amtdtltempsisa)" & _
                    " VALUES ('MSC'," & Session("vIDCost") & "," & RealisasiOid.Text & ",'" & Tchar(objRow(C1)("seqdtl")) & "'," & objRow(C1)("voucheroid") & "," & ToDouble(objRow(C1)("amtrealdtl")) & "," & ToDouble(objRow(C1)("amtvoucherdtl")) & "," & ToDouble(objRow(C1)("amtsisadtl")) & ",'" & lblPOST.Text & "',current_timestamp,'" & Session("UserID") & "','" & Tchar(objRow(C1)("notedtl")) & "'," & ToDouble(objRow(C1)("amtrealdtl")) & "," & ToDouble(objRow(C1)("amtsisadtl")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    ' GL Detail
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & (Session("vIDCost") + objRow.Length) & " WHERE tablename='QL_trnrealisasidtl'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If 
 
            objTrans.Commit() : objConn.Close()
        Catch ex As Exception
            objTrans.Rollback() : objConn.Close()
            lblPOST.Text = "IN PROCESS" : showMessage(ex.ToString, 1)
            Exit Sub
        End Try

        Session("oid") = Nothing
        If lblPOST.Text = "APPROVED" Then
            Session("SavedInfo") = "Data Telah di Posting <BR>"
        Else
            Response.Redirect("trnVoucher.aspx?awal=true")
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Session("oid") = Nothing : Session("dtlTable") = Nothing
            Response.Redirect("~\Accounting\trnVoucher.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'sSql = "SELECT bm.trnbelimstoid,trnbelidtloid,trnbelipono,bd.amtdisc2idr,bd.amtdisc2idr-realisasiamt amtrealisasi FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE ISNULL(bd.trnbelidtlres1,'')='IN PROCESS' AND trnbelipono='" & NoPoTxt.Text.Trim & "' ORDER BY trnbelidtloid ASC"
        'Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnbelidtl")
        'sSql = "Select COUNT (*) FROM QL_trnrealisasi Where pomstoid=" & OidPO.Text & " AND realisasioid <> " & RealisasiOid.Text & ""
        'Dim CekParsial As Double = ToDouble(GetStrData(sSql))

        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If

        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            'sSql = "DELETE FROM QL_cashbankgl WHERE cmpcode='" & CompnyCode & "' AND cashbankoid IN (Select cashbankoid FROM QL_trncashbankmst Where cashbankno='" & Trim(RealisasiNo.Text) & "')"
            'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'sSql = "DELETE QL_trncashbankmst WHERE cmpcode='" & CompnyCode & "' AND cashbankno = '" & Trim(RealisasiNo.Text) & "'"
            'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'For c1 As Int16 = 0 To objTbl.Rows.Count - 1
            '    If CekParsial > 0 And c1 = 0 Then
            '        Dim resAmt As Double = ToDouble(objTbl.Rows(c1).Item("amtdisc2idr")) - ToDouble(realisasiamt.Text)
            '        sSql = "UPDATE QL_trnbelidtl SET realisasiamt=" & resAmt & " WHERE trnbelimstoid=" & objTbl.Rows(c1).Item("trnbelimstoid") & " AND trnbelidtloid=" & objTbl.Rows(c1).Item("trnbelidtloid") & " And branch_code='" & Session("branch_id") & "' AND trnbelidtlres1='IN PROCESS'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    Else
            '        sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1 = 'In Process',realisasiamt=0.00 WHERE trnbelimstoid=" & objTbl.Rows(c1).Item("trnbelimstoid") & " AND trnbelidtloid=" & objTbl.Rows(c1).Item("trnbelidtloid") & " And branch_code='" & Session("branch_id") & "' AND trnbelidtlres1='IN PROCESS'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    End If

            '    sSql = "UPDATE QL_trnbelidtl SET selisih= " & objTbl.Rows(c1).Item("amtdisc2idr") & ",trnbelidtlres1 = 'In Process' WHERE trnbelimstoid=" & objTbl.Rows(c1).Item("trnbelimstoid") & " AND branch_code='" & Session("branch_id") & "'"
            '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'Next
            sSql = "DELETE FROM QL_trnrealisasi Where realisasioid=" & RealisasiOid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trnrealisasidtl Where trnrealisasioid=" & RealisasiOid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : objConn.Close()
        Catch ex As Exception
            objTrans.Rollback() : objConn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("oid") = Nothing
        Response.Redirect("~\Accounting\trnVoucher.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~/accounting/trnVoucher.aspx?awal=true")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PrintReport(CompnyCode, RealisasiOid.Text)
    End Sub

    Protected Sub gvmstcost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvmstcost.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showTableCOA(RealisasiNo.Text, Session("branch_id"), gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True
        mpePosting2.Show()
    End Sub

    Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing : Session("dtlTable") = Nothing
        Response.Redirect("trnVoucher.aspx?awal=true")
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub gridCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If (e.CommandName = "ConfirmPrint") Then
            Response.Redirect("~/ReportForm/ql_printreport.aspx?no=" & Trim(e.CommandArgument.ToString()) & "&printtype=1")
        ElseIf (e.CommandName = "PrintVoucher") Then
            Response.Redirect("~/ReportForm/ql_printreport.aspx?no=" & Trim(e.CommandArgument.ToString()) & "&printtype=1")
        End If
    End Sub

    Protected Sub btnViewLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing
        If Session("SearchExpense") Is Nothing = False Then
            bindLastSearched()
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False
        btnHidePosting2.Visible = False
        mpePosting2.Hide()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
            e.Row.Cells(3).Text = NewMaskEdit(e.Row.Cells(3).Text)
        End If
    End Sub

    Protected Sub realisasiamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles realisasiamt.TextChanged
        realisasiamt.Text = ToMaskEdit(realisasiamt.Text, 4)
        HitungVoucher()
    End Sub

    Protected Sub pic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub PrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sOid = sender.tooltip
        Dim a As String() = sOid.split(",")
        sSql = "select cashbankstatus from QL_trncashbankmst where cmpcode='" & a(0) & "' AND cashbankoid='" & a(1) & "'"
        If GetStrData(sSql) = "POST" Then
            PrintReport(a(0), a(1))
        Else
            showMessage("Status In Proses Tidak bisa di Print, Pilih Transaksi lain !!! ", 2)
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal cmpCode As String, ByVal cbOid As Integer)
        'untuk print
        'sSql = "select cashbankno from QL_trncashbankmst WHERE cmpcode='" & cmpCode & "' AND cashbankoid='" & cbOid & "' and branch_code='" & Session("branch_id") & "'"
        Dim type As String = payflag.SelectedValue
        Dim cbNo As String = "RECEIPT-" & GetStrData(sSql)
        Dim sWhere As String = "AND cashbankgroup='RECEIPT'"

        If type <> "" Then
            Report.Load(Server.MapPath(folderReport & "printBKK.rpt"))
            'Report.SetParameterValue("sWhere", sWhere)
            Report.SetParameterValue("cmpCode", cmpCode)
            Report.SetParameterValue("cbOid", cbOid)
            Report.SetParameterValue("companyname", "MULTI SARANA COMPUTER")

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, Report)

            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, cbNo)
            Report.Close() : Report.Dispose() : Session("sNo") = Nothing
        End If

    End Sub

    Protected Sub gvmstcost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvmstcost.SelectedIndexChanged
        Response.Redirect("trnVoucher.aspx?cmpcode=" & gvmstcost.SelectedDataKey("cmpcode").ToString & "&oid=" & gvmstcost.SelectedDataKey("realisasioid").ToString & "")
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindCOA("")
    End Sub

    Protected Sub ViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindCOA("")
    End Sub

    Protected Sub btnCariPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindPO()
    End Sub

    Protected Sub gvPO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPO.PageIndex = e.NewPageIndex
        BindPO() : gvPO.Visible = True
    End Sub

    Protected Sub gvPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        OidPO.Text = gvPO.SelectedDataKey("trnbelimstoid").ToString()
        NoPoTxt.Text = gvPO.SelectedDataKey("NoPo").ToString()
        realisasiamt.Text = ToMaskEdit(gvPO.SelectedDataKey("VoucherPI").ToString(), 4)
        lblTampung.Text = ToMaskEdit(gvPO.SelectedDataKey("VoucherPI").ToString(), 4)
        AmtPI.Text = ToMaskEdit(gvPO.SelectedDataKey("VoucherPI1").ToString(), 4)
        gvPO.Visible = False : HitungVoucher()
    End Sub

    Protected Sub ErasePObtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        OidPO.Text = "" : NoPoTxt.Text = "" : AmtPI.Text = "0.00"
        realisasiamt.Text = "0.00" : gvPO.Visible = False
    End Sub

    Protected Sub gvPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
        End If
    End Sub

    Protected Sub GvCoa_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)
        End If
    End Sub

    Protected Sub BtnSendAp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSendAp.Click

        lblPOST.Text = "IN APPROVAL"
         If checkApproval("ql_trnrealisasi", "In Approval", Session("oid"), "New", "FINAL", Session("branch_id")) > 0 Then
            showMessage("Purchase Order ini sudah di send for Approval", 2)
            Exit Sub
        End If

        '-------------------------------------------------------------------------------------
        If lblPOST.Text.ToUpper = "IN APPROVAL" Then
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnrealisasi' Order By approvallevel"
            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                showMessage("Tidak ada Approval User untuk Form Realisasi, Silahkan contact yang berwenang untuk setting user approval", 2)
                Exit Sub
            End If

            Dim objTrans As SqlClient.SqlTransaction
            If objConn.State = ConnectionState.Closed Then
                objConn.Open()
            End If
            objTrans = objConn.BeginTransaction()
            xCmd.Transaction = objTrans
            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            Try
                If Session("oid") <> Nothing Or Session("oid") <> "" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus,branch_code) VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", '" & "RV" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_trnrealisasi', '" & Session("oid") & "','In Approval','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1', '" & objTable.Rows(c1).Item("approvalstatus") & "','" & Session("branch_id") & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Else
                    showMessage("Silahkan Simpan Data terlebih dahulu.", 2)
                    Exit Sub
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As OleDb.OleDbException
                objTrans.Rollback()
                xCmd.Connection.Close()
                showMessage(ex.ToString, 1)
                Exit Sub
            End Try
            '---------------------------------------------------------------------------
        ElseIf lblPOST.Text = "Rejected" Then
            lblPOST.Text = "In Process"
        End If

        btnsave_Click(sender, e)
    End Sub

    Protected Sub btnCariCoa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If OidPO.Text <> "0" Then
            BindAkun()
        Else
            showMessage("Kolom No. PO belum di isi..", 2)
        End If
    End Sub

    Protected Sub EraseVc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblCoa.Text = "" : txtVoucher.Text = "" : GvVoucher.Visible = False
    End Sub

    Protected Sub GvVoucher_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)
        End If
    End Sub

    Protected Sub GvVoucher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtVoucher.Text = GvVoucher.SelectedDataKey("voucherdesc").ToString().Trim
        lblCoa.Text = GvVoucher.SelectedDataKey("voucheroid")
        vDtlAmt.Text = ToMaskEdit(GvVoucher.SelectedDataKey("amtvoucheridr"), 4)
        AmtVdtl.Text = ToMaskEdit(GvVoucher.SelectedDataKey("amtvoucheridr"), 4)
        GvVoucher.Visible = False
    End Sub

    Protected Sub GvVoucher_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvVoucher.PageIndex = e.NewPageIndex
        BindAkun() : GvVoucher.Visible = True
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If lblCoa.Text.Trim = "" Or txtVoucher.Text.Trim = "" Then
            showMessage("Maaf, Voucher detail belum dipilih!", 2)
            Exit Sub
        End If
        If Double.Parse(AmtVdtl.Text.Trim) = 0.0 Then
            showMessage("Maaf, Amount Voucher tidak boleh 0!", 2)
            Exit Sub
        End If
        NoPoTxt.Enabled = False
        btnCariPO.Visible = False : ErasePObtn.Visible = False
        '--Create Table Detail--
        '-----------------------
        Dim dtab As DataTable
        If I_Udtl.Text = "New Detail" Then
            If Session("QL_trnrealisasidtl") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seqdtl", Type.GetType("System.Int32"))
                dtab.Columns.Add("voucheroid", Type.GetType("System.Int32"))
                dtab.Columns.Add("voucherdesc", Type.GetType("System.String"))
                dtab.Columns.Add("amtrealdtl", Type.GetType("System.Double"))
                dtab.Columns.Add("amtvoucherdtl", Type.GetType("System.Double"))
                dtab.Columns.Add("amtsisadtl", Type.GetType("System.Double"))
                dtab.Columns.Add("amtdtltemp", Type.GetType("System.Double"))
                dtab.Columns.Add("amtdtltempsisa", Type.GetType("System.Double"))
                dtab.Columns.Add("notedtl", Type.GetType("System.String"))
                Session("QL_trnrealisasidtl") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("QL_trnrealisasidtl")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("QL_trnrealisasidtl")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("voucheroid = " & Integer.Parse(lblCoa.Text) & " AND seqdtl <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Maaf, Voucher tidak bisa ditambahkan ke dalam list ! Voucher ini Sudah ada di dalam list !", 2)
                Exit Sub
            End If
        End If
        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_Udtl.Text = "New Detail" Then
            drow = dtab.NewRow
            drow("seqdtl") = Integer.Parse(labelseq.Text)
            drow("voucheroid") = Integer.Parse(lblCoa.Text)
            drow("voucherdesc") = txtVoucher.Text.Trim
            drow("amtrealdtl") = ToDouble(AmtVdtl.Text)
            drow("amtdtltemp") = ToDouble(AmtVdtl.Text)
            drow("amtvoucherdtl") = ToDouble(vDtlAmt.Text)
            'If ToDouble(drow("amtrealdtl")) > ToDouble(drow("amtvoucherdtl")) Then
            '    AmtSisaDtl.Text = ToDouble(drow("amtrealdtl")) - ToDouble(drow("amtvoucherdtl"))
            'ElseIf ToDouble(drow("amtrealdtl")) < ToDouble(drow("amtvoucherdtl")) Then
            '    AmtSisaDtl.Text = ToDouble(drow("amtvoucherdtl")) - ToDouble(drow("amtrealdtl"))
            'Else
            AmtSisaDtl.Text = ToDouble(drow("amtvoucherdtl")) - ToDouble(drow("amtrealdtl"))
            'End If
            drow("amtsisadtl") = ToDouble(drow("amtvoucherdtl")) - ToDouble(drow("amtrealdtl"))
            drow("amtdtltempsisa") = ToDouble(drow("amtvoucherdtl")) - ToDouble(drow("amtrealdtl"))
            drow("notedtl") = cashbanknote.Text.Trim
            dtab.Rows.Add(drow) : dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seqdtl = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0) : drowedit(0).BeginEdit()

            drow("voucheroid") = Integer.Parse(lblCoa.Text)
            drow("voucherdesc") = txtVoucher.Text.Trim
            drow("amtrealdtl") = ToDouble(AmtVdtl.Text)
            drow("amtdtltemp") = ToDouble(AmtVdtl.Text)
            drow("amtvoucherdtl") = ToDouble(vDtlAmt.Text)
            'If ToDouble(drow("amtrealdtl")) > ToDouble(drow("amtvoucherdtl")) Then
            '    AmtSisaDtl.Text = ToDouble(drow("amtrealdtl")) - ToDouble(drow("amtvoucherdtl"))
            'ElseIf ToDouble(drow("amtrealdtl")) < ToDouble(drow("amtvoucherdtl")) Then
            '    AmtSisaDtl.Text = ToDouble(drow("amtvoucherdtl")) - ToDouble(drow("amtrealdtl"))
            'Else
            AmtSisaDtl.Text = ToDouble(drow("amtvoucherdtl")) - ToDouble(drow("amtrealdtl"))
            'End If
            drow("amtsisadtl") = ToDouble(drow("amtvoucherdtl")) - ToDouble(drow("amtrealdtl"))
            drow("amtdtltempsisa") = ToDouble(drow("amtvoucherdtl")) - ToDouble(drow("amtrealdtl"))
            drow("notedtl") = cashbanknote.Text.Trim
            drowedit(0).EndEdit() : dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If
        GVvOucherDtl.DataSource = dtab : GVvOucherDtl.DataBind()
        Session("QL_trnrealisasidtl") = dtab
        GVvOucherDtl.Columns(7).Visible = False
        GVvOucherDtl.Columns(8).Visible = False
        HitungVoucher() : HitungDtlAmt()
        labelseq.Text = (GVvOucherDtl.Rows.Count + 1).ToString
        txtVoucher.Text = "" : AmtVdtl.Text = "0.00"
        vDtlAmt.Text = "0.00" : cashbanknote.Text = ""
        lblCoa.Text = "" : AmtSisaDtl.Text = "0.00"
        GVvOucherDtl.SelectedIndex = -1
        GVvOucherDtl.DataSource = Session("QL_trnrealisasidtl")
        GVvOucherDtl.DataBind() : GVvOucherDtl.Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("QL_trnrealisasidtl") Is Nothing Then
            dtab = Session("QL_trnrealisasidtl")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seqdtl = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seqdtl") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GVvOucherDtl.DataSource = dtab
        GVvOucherDtl.DataBind()
        Session("QL_trnrealisasidtl") = dtab
        If dtab.Rows.Count = 0 Then
            NoPoTxt.Enabled = True
            btnCariPO.Visible = True : ErasePObtn.Visible = True
        End If
        HitungDtlAmt()
    End Sub

    Protected Sub GVvOucherDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        I_Udtl.Text = "edit"
        lblCoa.Text = GVvOucherDtl.SelectedDataKey("voucheroid")
        txtVoucher.Text = GVvOucherDtl.SelectedDataKey("voucherdesc").ToString
        AmtVdtl.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtrealdtl")), 4)
        vDtlAmt.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtvoucherdtl")), 4)
        AmtSisaDtl.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtsisadtl")), 4)
        cashbanknote.Text = GVvOucherDtl.SelectedDataKey("notedtl").ToString 
        labelseq.Text = GVvOucherDtl.SelectedDataKey("seqdtl").ToString

        Dim transdate As Date = Date.ParseExact(Realisasidate.Text, "dd/MM/yyyy", Nothing)
        If GVvOucherDtl.Visible = True Then
            GVvOucherDtl.DataSource = Nothing
            GVvOucherDtl.DataBind()
            GVvOucherDtl.Visible = False
        End If
        'GVvOucherDtl.Columns(7).Visible = False
    End Sub

    Protected Sub AmtVdtl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        AmtVdtl.Text = ToMaskEdit(AmtVdtl.Text, 4)
        HitungVoucher()
    End Sub

    Protected Sub GVvOucherDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 4)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
            If lblPOST.Text.ToUpper = "APPROVED" Then
                e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 4)
                e.Row.Cells(8).Text = ToMaskEdit(e.Row.Cells(8).Text, 4)
            End If
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlVoucher()
    End Sub
 
    Protected Sub btnClear_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        labelseq.Text = "0" : lblTampung.Text = "0.00"
        txtVoucher.Text = "" : AmtVdtl.Text = "0.00"
        vDtlAmt.Text = "0.00" : cashbanknote.Text = ""
        lblCoa.Text = "" : AmtSisaDtl.Text = "0.00"
        selisihamount.Text = "0.00"
        GVvOucherDtl.Visible = False
    End Sub
#End Region
End Class
