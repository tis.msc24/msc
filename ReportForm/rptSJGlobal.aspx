<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSJGlobal.aspx.vb" Inherits="ReportForm_frmpostat" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="text-align: left;">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".:  LAPORAN SURAT JALAN"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" class="header" style="background-color: transparent; text-align: center"
                valign="center">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD align=left>Periode</TD><TD>:</TD><TD align=left><asp:TextBox id="date1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox> <asp:ImageButton id="search1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w4"></asp:Label> <asp:TextBox id="date2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w5"></asp:TextBox> <asp:ImageButton id="search2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(dd/mm/yyyy)" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label5" runat="server" Width="42px" Text="Cabang" __designer:wfdid="w109"></asp:Label></TD><TD><asp:Label id="Label6" runat="server" Text=":" __designer:wfdid="w109"></asp:Label></TD><TD align=left><asp:DropDownList id="BranchDDL" runat="server" Width="157px" CssClass="inpText" __designer:wfdid="w10" OnSelectedIndexChanged="dView_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD align=left>No. SJ</TD><TD><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w11"></asp:Label></TD><TD align=left><asp:TextBox id="trnPono" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w12"></asp:TextBox> <asp:ImageButton id="ImgPono" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w13"></asp:ImageButton> <asp:ImageButton id="ImgErasePono" onclick="ImgErasePono_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w14"></asp:ImageButton> <asp:Label id="SJoid" runat="server" __designer:wfdid="w15" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD></TD><TD align=left><asp:GridView id="poNoGV" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w16" Visible="False" OnSelectedIndexChanged="poNoGV_SelectedIndexChanged" OnPageIndexChanging="poNoGV_PageIndexChanging" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="sjglobalmstoid,sjglobalno" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="sjglobalno" HeaderText="No. Surat Jalan">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="tglSj" HeaderText="Tanggal SJ">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="LblPDO" runat="server" Width="52px" Text="Item" __designer:wfdid="w17"></asp:Label></TD><TD><asp:Label id="lblPDO6" runat="server" Text=":" __designer:wfdid="w18"></asp:Label></TD><TD align=left><asp:TextBox id="Itemdesc" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w19"></asp:TextBox> <asp:ImageButton id="imgSjno" onclick="imgSjno_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w20"></asp:ImageButton> <asp:ImageButton id="EraseSjNo" onclick="EraseSjNo_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w21"></asp:ImageButton> <asp:Label id="Itemoid" runat="server" __designer:wfdid="w22" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD></TD><TD align=left><asp:GridView id="SjNoGV" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w23" Visible="False" OnSelectedIndexChanged="SjNoGV_SelectedIndexChanged" OnPageIndexChanging="SjNoGV_PageIndexChanging" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemoid,itemdesc" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="Itemcode" HeaderText="Itemcode">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD2" align=left runat="server" Visible="false"></TD><TD id="Td1" runat="server" Visible="false"></TD><TD id="Td3" align=left runat="server" Visible="false"><asp:DropDownList id="dView" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w24" Visible="False" OnSelectedIndexChanged="dView_SelectedIndexChanged" AutoPostBack="True">
                                            <asp:ListItem>Local</asp:ListItem>
                                            <asp:ListItem Value="Trading">Import</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w25" TargetControlID="Date1" PopupButtonID="search1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w26" TargetControlID="Date2" PopupButtonID="search2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w27" TargetControlID="date1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w28" TargetControlID="date2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD colSpan=3><asp:ImageButton id="ImageButton1" onclick="ImageButton1_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton>&nbsp;<asp:ImageButton id="ToPDF" onclick="ToPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w30"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbExport" onclick="imbExport_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w32"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w33"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="labelmsg" runat="server" ForeColor="Red" __designer:wfdid="w35"></asp:Label></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=center colSpan=3><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w36" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
                        <asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

