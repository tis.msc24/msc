Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class ReportForm_rptOutstandingPO
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Public Sub initddl()

        initGroup()
        initSubGroup()

    End Sub
    Public Sub initGroup()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("ALL GROUP", "ALL GROUP"))
        FilterDDLGrup.SelectedValue = "ALL GROUP"
    End Sub
    Public Sub initSubGroup()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GROUP", "ALL SUB GROUP"))
        FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP"
    End Sub
    Public Sub showPrint(ByVal tipe As String)
        lblkonfirmasi.Text = ""
        Session("diprint") = "False"

        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Try
            Dim namaPDF As String = ""
            
            Dim swhere As String = " where convert(char(10),pom.trnbelipodate,121) between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "'"
            swhere &= IIf(ToDouble(itemoid.Text) > 0, " and  pom.trnbelimstoid in (select d.trnbelimstoid from ql_podtl d where d.cmpcode='" & CompnyCode & "' and d.itemoid=" & ToDouble(itemoid.Text) & ")", " ")
            swhere &= IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", "  ", "  and  i.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
            swhere &= IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", "  ", "  and  i.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")
            swhere &= IIf(oid.Text.Trim = "", " ", " and pom.trnbelimstoid = '" & oid.Text & "' ")
            swhere &= IIf(suppoid.Text.Trim = "", " ", " and supp.suppoid = " & suppoid.Text & " ")
            swhere &= IIf(itemoid.Text.Trim = "", " ", " and i.itemoid = " & itemoid.Text & " ")

            If taxable.SelectedValue <> "ALL" Then
                If taxable.SelectedValue = "YES" Then
                    swhere &= " and pom.trntaxpct>0"
                Else
                    swhere &= " and pom.trntaxpct=0"
                End If
            End If

            vReport = New ReportDocument
            If Type.SelectedValue = "Summary" Then
                vReport.Load(Server.MapPath("~\Report\rptOutstandingPOSum.rpt"))
                namaPDF = "OUTSTANDING_PO_SUM_"
            Else
                vReport.Load(Server.MapPath("~\Report\rptOutstandingPODetail.rpt"))
                namaPDF = "OUTSTANDING_PO_DETAIL_"
            End If

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.SetParameterValue("sWhere", swhere)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("filterPO", IIf(oid.Text.Trim = "", "ALL", nota.Text))
            vReport.SetParameterValue("filterGroup", IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", "ALL GROUP", FilterDDLGrup.SelectedItem.Text))
            vReport.SetParameterValue("filterSubGroup", IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", "ALL SUB GROUP", FilterDDLSubGrup.SelectedItem.Text))
            vReport.SetParameterValue("filterItem", IIf(itemoid.Text.Trim = "", "ALL", itemname.Text))
            vReport.SetParameterValue("filterMerk", IIf(merk.Text.Trim = "", "ALL", merk.Text))
            vReport.SetParameterValue("filterSupplier", IIf(suppoid.Text.Trim = "", "ALL", suppname.Text))
            vReport.SetParameterValue("filterTaxable", IIf(taxable.SelectedValue = "ALL", "ALL", taxable.SelectedValue))
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            If tipe = "" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Public Sub BindDataListItem()
        sSql = "select ql_mstitem.itemcode, ql_mstitem.itemdesc, " & _
        "ql_mstitem.itempriceunit1,ql_mstitem.itempriceunit2," & _
        "ql_mstitem.itempriceunit3,ql_mstitem.itemoid, g.gendesc satuan1," & _
        "g2.gendesc satuan2, g3.gendesc satuan3,konversi1_2, " & _
        "konversi2_3,ql_mstitem.merk from ql_mstitem inner join ql_mstgen g on g.genoid=satuan1 " & _
        "and itemflag='AKTIF' inner join ql_mstgen g2 on g2.genoid=satuan2  " & _
        "inner join ql_mstgen g3 on g3.genoid=satuan3  " & _
        "where itemdesc like '%" & Tchar(itemname.Text) & "%' or ql_mstitem.itemcode like '%" & Tchar(itemname.Text) & "%' or ql_mstitem.merk like '%" & Tchar(itemname.Text) & "%'"
        FillGV(gvItem, sSql, "ql_mstitem")
        gvItem.Visible = True
    End Sub

    Public Sub bindDataListSupplier()
        sSql = "select suppoid, suppcode, suppname, supptype from ql_mstsupp where suppflag='Active' and suppname like '%" & Tchar(suppname.Text) & "%' or suppcode like '%" & Tchar(suppname.Text) & "%'"
        FillGV(gvSupp, sSql, "ql_mstsupp")
        gvSupp.Visible = True
    End Sub

    Public Sub bindDataListPO()
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Periode 1!"
            Exit Sub
        End If

        sSql = "select pom.trnbelimstoid, pom.trnbelipono, sup.suppoid, sup.suppname from ql_pomst pom inner join ql_mstsupp sup on sup.suppoid=pom.trnsuppoid where pom.trnbelitype = 'GROSIR' and pom.trnbelistatus in ('Approved') and trnbelipodate between '" & CDate(toDate(dateAwal.Text)) & "' and '" & CDate(toDate(dateAkhir.Text)) & "' and trnbelipono like '%" & Tchar(nota.Text.Trim) & "%'"
        FillGV(GVNota, sSql, "ql_pomst")
        GVNota.Visible = True
    End Sub
#End Region

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        merk.Text = gvItem.SelectedDataKey.Item("merk")
        gvItem.DataSource = Nothing
        gvItem.DataBind()
        gvItem.Visible = False
        Session("gvItem") = Nothing
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        merk.Text = ""
        gvItem.DataSource = Nothing
        gvItem.DataBind()
        gvItem.Visible = False
        Session("gvItem") = Nothing
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataListItem()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        'If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Response.Redirect("~\ReportForm\rptOutstandingPO.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Outstanding PO"

        If type.SelectedValue = "Summary" Then
        End If

        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrint("")
            End If
        Else
            initddl()
            dateAwal.Text = Format(New Date(Now.Year, Now.Month, 1), "dd/MM/yyyy")
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        Session("diprint") = "True"
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptOutstandingPO.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showPrint("excel")
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListSupplier()
    End Sub

    Protected Sub btnSearchNota_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListPO()
    End Sub

    Protected Sub btnEraseSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppname.Text = ""
        suppoid.Text = ""
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        suppname.Text = gvSupp.SelectedDataKey.Item("suppname")
        suppoid.Text = gvSupp.SelectedDataKey.Item("suppoid")
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupp.PageIndexChanging
        gvSupp.PageIndex = e.NewPageIndex
        bindDataListSupplier()
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        GVNota.PageIndex = e.NewPageIndex
        bindDataListPO()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nota.Text = ""
        oid.Text = ""
        gvNota.Visible = False
    End Sub

    Protected Sub GVNota_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        nota.Text = GVNota.SelectedDataKey.Item("trnbelipono")
        oid.Text = GVNota.SelectedDataKey.Item("trnbelimstoid")
        suppname.Text = GVNota.SelectedDataKey.Item("suppname")
        suppoid.Text = GVNota.SelectedDataKey.Item("suppoid")
        GVNota.Visible = False
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        Session("diprint") = "False"
        showPrint("pdf")
    End Sub

End Class
