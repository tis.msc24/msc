Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports ClassProcedure
Imports Koneksi
Imports System.Globalization
Imports System.IO
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Windows.Forms

Partial Class Transaction_trnfinalsvc
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As ReportDocument
#End Region

#Region "Function"
    Public Function GetStatus() As Boolean
        If Eval("TypeFinal") = "REPLACE" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function CrtDtlBarang() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqmstoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("ItemOid", Type.GetType("System.Int32"))
        dtab.Columns.Add("snno", Type.GetType("System.String"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("reqqty", Type.GetType("System.Decimal"))
        dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
        dtab.Columns.Add("Unit", Type.GetType("System.String"))
        dtab.Columns.Add("Kelengkapan", Type.GetType("System.String"))
        dtab.Columns.Add("reqdtljob", Type.GetType("System.String"))
        dtab.Columns.Add("typegaransi", Type.GetType("System.String"))
        dtab.Columns.Add("PARTOID", Type.GetType("System.Int32"))
        dtab.Columns.Add("partdescshort", Type.GetType("System.String"))
        dtab.Columns.Add("PartUnitOid", Type.GetType("System.Int32"))
        dtab.Columns.Add("partsqty", Type.GetType("System.Decimal"))
        dtab.Columns.Add("PartsUnit", Type.GetType("System.String"))
        dtab.Columns.Add("mtrloc", Type.GetType("System.Int32"))
        dtab.Columns.Add("spartprice", Type.GetType("System.String"))
        dtab.Columns.Add("totalprice", Type.GetType("System.Decimal"))
        dtab.Columns.Add("flagbarang", Type.GetType("System.String"))
        Session("itemdetail") = dtab
        Return dtab
    End Function

    Private Function CrtDtlRusak() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemrusakoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemreqoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itempartoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("trnfinaloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("itemtitipan", Type.GetType("System.String"))
        dtab.Columns.Add("qtyrusak", Type.GetType("System.Decimal"))
        dtab.Columns.Add("qtyreq", Type.GetType("System.Decimal"))
        dtab.Columns.Add("qtypart", Type.GetType("System.Decimal"))
        dtab.Columns.Add("locoidrusak", Type.GetType("System.Int32"))
        dtab.Columns.Add("trnfinalpartoid", Type.GetType("System.Int32"))
        Session("itemrusak") = dtab
        Return dtab
    End Function

    Private Function checkifexist(ByVal oid As Integer) As Boolean
        Dim result As Boolean = False

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT COUNT(mstreqoid) FROM QL_TRNFINAL WHERE mstreqoid = " & oid & ""
            xCmd.CommandText = sSql
            Dim res As Integer = xCmd.ExecuteScalar
            If res > 0 Then
                result = True
            End If

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            result = False
        End Try

        Return result
    End Function

    Private Function saverecord() As String
        Dim result As String = ""
        Dim serviceno As String = tbservicesno.Text
        Dim orderoid As Integer = 0
        If Integer.TryParse(lblreqoid.Text, orderoid) Then
            orderoid = orderoid
        Else
            result = "- Maaf, Nomor service tidak valid!"
            Return result
        End If
        'If Not DateTime.TryParseExact(sfindate, "dd/MM/yyyy HH:mm", Nothing, DateTimeStyles.None, findate) Then
        '    result = "Waktu finish dari service tidak valid!"
        '    Return result
        'End If
        Dim status As String = lblfinstatus.Text
        Dim itservoid As Integer = 0

        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans
        Try
            sSql = "SELECT COUNT(finoid) from ql_trnfinal WHERE mstreqoid = " & orderoid & ""
            xCmd.CommandText = sSql
            Dim res As Integer = xCmd.ExecuteScalar
            If res > 0 Then
                result = "Service dengan nomor " & serviceno & " telah tercatat sebelumnya..!!"
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                Return result
            End If

            Dim Finoid As Integer = GenerateID("QL_TRNFINAL", CompnyCode)
            sSql = "INSERT INTO QL_TRNFINAL ([CMPCODE],[BRANCH_CODE],[FINOID],[finaldate],[MSTREQOID],[custoid],[priceservis],[amtnettservis],[FINITEMSTATUS],[mtrlocoid],[SJOID],[FINTIME],[FINSTATUS],[RECEIVETIME],[CREATEUSER],[CREATETIME],[UPDUSER],[UPDTIME],trnfinalno,cabangasal)" & _
            " VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Finoid & ",'" & CDate(toDate(tglFinal.Text)) & "'," & Integer.Parse(lblreqoid.Text) & "," & Integer.Parse(custoid.Text) & "," & ToDouble(BiayaSrv.Text) & "," & ToDouble(NettNya.Text) & ",'" & Tchar(lblfinstatus.Text) & "'," & matLoc.SelectedValue & ",0,CURRENT_TIMESTAMP,'" & Tchar(lblfinstatus.Text) & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Tchar(TrnFinalNo.Text) & "','" & CabangAsal.Text & "')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_mstoid SET lastoid = " & Finoid & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNFINAL'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If Not Session("itemdetail") Is Nothing Then
                Dim dtab As DataTable = Session("itemdetail")

                If dtab.Rows.Count > 0 Then
                    sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid WHERE tablename = 'QL_TRNFINALSPART' AND cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    Dim spartoid As Integer = xCmd.ExecuteScalar

                    For i As Integer = 0 To dtab.Rows.Count - 1
                        spartoid = spartoid + 1
                        sSql = "INSERT INTO QL_TRNFINALSPART ([cmpcode],[branch_code],[trnifinaldtloid],[trnfinaloid],[reqoid],[reqdtloid],[itemreqoid],[itemqty],[unitoid],[typebarang],[itempartoid],[itempartqty],[itempartunitoid],[itemlocoid],[itempartprice],[itemtotalprice],[seq],[itempartstatusdtl],[upduser],[updtime])" & _
                        " VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(spartoid) & "," & Integer.Parse(Finoid) & "," & Integer.Parse(lblreqoid.Text) & "," & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & "," & Integer.Parse(dtab.Rows(i).Item("itemoid")) & "," & ToDouble(dtab.Rows(i).Item("reqqty")) & "," & Integer.Parse(dtab.Rows(i).Item("satuan1")) & ",'" & dtab.Rows(i).Item("typegaransi").ToString & "'," & Integer.Parse(dtab.Rows(i).Item("PARTOID")) & "," & ToDouble(dtab.Rows(i).Item("partsqty")) & "," & Integer.Parse(dtab.Rows(i).Item("PartUnitOid")) & "," & Integer.Parse(matLoc.SelectedValue) & "," & ToDouble(dtab.Rows(i).Item("spartprice")) & "," & ToDouble(dtab.Rows(i).Item("totalprice")) & "," & dtab.Rows(i).Item("seq") & ",'','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid = " & spartoid & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNFINALSPART'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            otrans.Commit()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            result = ex.ToString
        End Try
        Return result
    End Function

    Private Function updaterecord() As String
        Dim result As String = ""
        Dim serviceno As String = tbservicesno.Text
        Dim reqoid As Integer = 0
        If Integer.TryParse(lblreqoid.Text, reqoid) Then
            reqoid = reqoid
        Else
            result = "Nomor service tidak valid!"
            Return result
        End If
        Dim orderoid As Integer = 0
        If Integer.TryParse(lblcreqoid.Text, orderoid) Then
            orderoid = orderoid
        Else
            result = "Nomor service tidak valid!"
            Return result
        End If

        Dim status As String = lblfinstatus.Text
        Dim itservoid As Integer = 0

        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans
        Try

            otrans.Commit()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            result = ex.ToString
        End Try
        Return result
    End Function

    Private Function postingrecord() As String
        Dim result As String = ""
        Dim orderoid As Integer = 0
        If Integer.TryParse(lblreqoid.Text, orderoid) Then
            orderoid = orderoid
        Else
            result = "Nomor service tidak valid!"
            Return result
        End If

        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans
        Try

            sSql = "UPDATE ql_trnfinal SET receivetime = '" & DateTime.Now & "', finstatus = 'Final', upduser = '" & lblcurrentuser.Text & "', updtime = '" & DateTime.Now & "' WHERE mstreqoid = " & orderoid & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_trnrequest SET reqstatus = 'Final', upduser = '" & lblcurrentuser.Text & "', updtime = '" & DateTime.Now & "' WHERE reqoid = " & orderoid & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            otrans.Commit()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            result = ex.ToString
        End Try

        Return result
    End Function

    Private Function deleterecord() As String
        Dim result As String = ""

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "DELETE FROM ql_trnfinal WHERE finoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trnfinalspart WHERE trnfinaloid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() 

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            result = ex.ToString
        End Try

        Return result
    End Function
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub GeneratedID()
        If i_u.Text = "new" Then
            FinOid.Text = GenerateID("QL_TRNFINAL", CompnyCode)
            TrnFinalNo.Text = GenerateID("QL_TRNFINAL", CompnyCode)
        End If
    End Sub

    Private Sub GeneratedNo()
        Dim reqcode As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim code As String = "" : Dim iCurID As Integer = 0
            Dim sCode As String = ""
            Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & DdlCabang.SelectedValue & "' AND gengroup='CABANG'")
            code = "FIN/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

            sSql = "SELECT ISNULL(MAX(ABS(replace(trnfinalno,'" & code & "',''))),0) trnfinalno FROM QL_TRNFINAL WHERE trnfinalno LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            If Not IsDBNull(xCmd.ExecuteScalar) Then
                iCurID = xCmd.ExecuteScalar + 1
            Else
                iCurID = 1
            End If
            code = GenNumberString(code, "", iCurID, 4)
            TrnFinalNo.Text = code

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub HitungNet()
        If Not Session("itemdetail") Is Nothing Then
            Dim dtp As DataTable = Session("itemdetail")
            Dim Amt As Double = 0
            For c1 As Int32 = 0 To dtp.Rows.Count - 1
                Amt += ToDouble(dtp.Rows(c1)("totalprice"))
            Next
            TotalPriceDtl.Text = ToMaskEdit(ToDouble(Amt), 3)
            NettNya.Text = ToMaskEdit(ToDouble(Amt) + ToDouble(BiayaSrv.Text), 3)
        End If
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangnya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangnya, sSql)
            Else
                FillDDL(dCabangnya, sSql)
                dCabangnya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangnya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangnya, sSql)
            dCabangnya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangnya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DdlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DdlCabang, sSql)
            Else
                FillDDL(DdlCabang, sSql)
                DdlCabang.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DdlCabang, sSql)
            DdlCabang.SelectedValue = "10"
        End If
    End Sub

    Private Sub clearform()
        lblcurrentuser.Text = Session("UserID")
        ddlfilter.SelectedIndex = 0 : cbperiod.Checked = False
        tbperiodstart.Text = Date.Now.ToString("dd/MM/yyyy")
        tbperiodend.Text = Date.Now.ToString("dd/MM/yyyy")
        cbstatus.Checked = False : ddlfilterstatus.SelectedIndex = 0
        tbservicesno.Text = "" : lblreqoid.Text = ""
        lblcreqoid.Text = ""

        TypeBarang.Text = "" : lblfinitemname.Text = ""
        lblfinstatus.Text = "In Process" : phone.Text = ""

        gvrequest.DataSource = Nothing : gvrequest.DataBind()
        ibdelete.Visible = False : ibposting.Visible = True
        'ibsave.Visible = False
        ibsearchsvc.Visible = True : ibdelsvc.Visible = True
        ddlpopfilter.SelectedIndex = 0 : tbpopfilter.Text = ""

        tbsparepartjob.Text = "" : lblspartstate.Text = "new"
        tbsparepart.Text = "" : merkspr.Text = ""
        tbsparepartqty.Text = 0
        tbsparepartprice.Text = ToMaskEdit(0, 3)
        ibsearchspart.Visible = False : ibclearspart.Visible = True

        lblcreate.Text = "Create On <span style='font-weight: bold;'>" & Date.Now.ToString("dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & Session("UserID") & "</span>"
    End Sub

    Private Sub bindDataFinal()
        Try
            sSql = "Select fm.FINOID,fm.BRANCH_CODE,cbp.gendesc CabangInp,fm.trnfinalno,Convert(VarChar(20),fm.finaldate,103) FinalDate,rqm.reqcode,cba.gencode codeAs,cba.gendesc DescCbAs,fm.cabangasal,fm.FINSTATUS,cu.custoid,cu.custname,cu.custcode,fm.typetts From QL_TRNFINAL fm Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=fm.MSTREQOID AND fm.cabangasal=rqm.branch_code Inner Join QL_mstcust cu ON cu.custoid=rqm.reqcustoid AND rqm.branch_code=cu.branch_code Inner Join QL_mstgen cbp ON cbp.gencode=fm.BRANCH_CODE AND cbp.gengroup='Cabang' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal AND cba.gengroup='Cabang' Where " & ddlfilter.SelectedValue & " LIKE '%" & Tchar(tbfilter.Text) & "%'"

            If cbperiod.Checked = True Then
                sSql &= " AND fm.finaldate Between '" & CDate(toDate(tbperiodstart.Text)) & "' AND '" & CDate(toDate(tbperiodend.Text)) & "'"
            End If

            If cbstatus.Checked = True Then
                sSql &= " AND FINSTATUS='" & ddlfilterstatus.SelectedValue & "'"
            End If

            If dCabangnya.SelectedValue <> "ALL" Then
                sSql &= " And fm.BRANCH_CODE='" & dCabangnya.SelectedValue & "'"
            End If
            sSql &= " Order By fm.updtime Desc"
            Session("datafinal") = Nothing
            Session("datafinal") = cKon.ambiltabel(sSql, "datafinal")
            gvdata.DataSource = Session("datafinal")
            gvdata.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & " <br / " & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindItemRusak()
        Try
            If reqdtloid.Text <> "" Then
                sSql = "Select itemoid, itemcode, itemdesc, Merk from QL_mstitem Where itemflag='AKTIF' AND (itemdesc LIKE '%" & Tchar(ItemRusak.Text) & "%' OR itemcode LIKE '%" & Tchar(ItemRusak.Text) & "%' Or Merk LIKE '%" & Tchar(ItemRusak.Text) & "%') Order By itemdesc"
                Dim it As DataTable = cKon.ambiltabel(sSql, "BindItemRusak")
                GvItemRusak.DataSource = it : GvItemRusak.DataBind()
                GvItemRusak.Visible = True
            Else
                showMessage("Maaf, Silahkan klik tombol tambah yang ada pada kolom detail...!!", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try 
    End Sub

    Private Sub fillTextForm(ByVal OidFinal As Integer)
        lblcurrentuser.Text = Session("UserID")
        DdlCabang.Enabled = False
        Try
            Dim oid As Integer = Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "Select fm.FINOID, fm.BRANCH_CODE, cbp.gendesc DescCbInpt, fm.trnfinalno, fm.finaldate, rqm.reqcode, cba.gencode CdCabangAs, cba.gendesc CabangAsal, fm.FINSTATUS, cu.custoid, cu.custname, rqm.reqoid, fm.mtrlocoid, cu.phone1, fm.trnfinalnote, fm.priceservis, fm.amtnettservis, fm.FINTIME, fm.receivetime, fm.UPDTIME, fm.UPDUSER, fm.CREATETIME, fm.CREATEUSER, fm.locoidrusak, fm.locoidtitipan, typetts, flagfinal From QL_TRNFINAL fm Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=fm.MSTREQOID AND fm.cabangasal=rqm.branch_code Inner Join QL_mstcust cu ON cu.custoid=rqm.reqcustoid AND rqm.branch_code=cu.branch_code Inner Join QL_mstgen cbp ON cbp.gencode=fm.BRANCH_CODE AND cbp.gengroup='Cabang' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal AND cbp.gengroup='Cabang' Where fm.FINOID=" & OidFinal
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                'rowstate = 1
                While xreader.Read
                    Dim Sql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang' AND gencode='" & xreader("BRANCH_CODE").ToString & "'"
                    FillDDL(DdlCabang, Sql)
                    DdlCabang.SelectedValue = xreader("BRANCH_CODE").ToString
                    CabangAsal.Text = xreader("CdCabangAs").ToString
                    lblreqoid.Text = Integer.Parse(xreader("reqoid"))
                    lblcreqoid.Text = Integer.Parse(xreader("reqoid"))
                    FinOid.Text = Integer.Parse(xreader("FINOID"))
                    TrnFinalNo.Text = xreader("trnfinalno").ToString
                    custoid.Text = Integer.Parse(xreader("custoid"))
                    tbservicesno.Text = xreader("reqcode").ToString
                    DDLTypeTTS.SelectedValue = xreader("typetts").ToString
                    InitLoc()
                    TypeTTS.Text = xreader("flagfinal").ToString
                    matLoc.SelectedValue = Integer.Parse(xreader("mtrlocoid"))
                    OidLocRusak.Text = Integer.Parse(xreader("locoidrusak"))
                    OidLocTitipan.Text = Integer.Parse(xreader("locoidtitipan"))
                    tbsparepartjob.Text = xreader("trnfinalnote").ToString
                    BiayaSrv.Text = ToMaskEdit(ToDouble(xreader("priceservis")), 3)
                    lblfincust.Text = xreader("custname").ToString
                    phone.Text = xreader("phone1").ToString
                    tglFinal.Text = Format(xreader("finaldate"), "dd/MM/yyyy").ToString
                    lblfinstatus.Text = xreader("FINSTATUS").ToString
                    createtime.Text = Format(xreader("CREATETIME"), "dd/MM/yyyy HH:mm:ss tt")

                    If Not IsDBNull(xreader("upduser")) Or xreader("upduser").ToString <> "" Then
                        lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("upduser").ToString & "</span>"
                    Else
                        lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("createtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("createuser").ToString & "</span>"
                    End If
                End While
            End If

            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

            If DDLTypeTTS.SelectedValue = "REPLACE" Then
                gvrequest.Columns(13).HeaderText = "Barang Replace"
            Else
                gvrequest.Columns(13).HeaderText = "Barang Spare Parts"
            End If

            sSql = "Select fd.seq, fd.trnfinaloid finoid, fd.reqoid reqmstoid, fd.reqdtloid, rqd.itemoid, rqd.snno, rqd.itemdesc, rqd.itemoid itemreqoid, fd.itemqty reqqty, fd.itemqty, fd.unitoid satuan1, ur.gendesc Unit, rqd.kelengkapan, rqd.reqdtljob, rqd.typegaransi, fd.itempartoid PARTOID, Isnull(i.itemdesc,'') partdescshort, fd.itempartunitoid PartUnitOid, Case sp.gendesc When 'AWL' then '' else sp.gendesc End PartsUnit, fd.itemlocoid mtrloc, fd.itempartqty partsqty, fd.itempartprice spartprice, fd.itemtotalprice totalprice From QL_TRNFINALSPART fd Inner Join QL_TRNREQUESTDTL rqd ON rqd.reqdtloid=fd.reqdtloid AND fd.reqoid=rqd.reqmstoid AND rqd.itemoid=fd.itemreqoid Left Join ql_mstitem i ON i.itemoid=fd.itempartoid Inner Join QL_mstgen ur ON ur.genoid=fd.unitoid AND ur.gengroup='ITEMUNIT' Left Join QL_mstgen sp ON sp.genoid=fd.itempartunitoid AND ur.gengroup='ITEMUNIT' Where fd.trnfinaloid=" & OidFinal & " Order By fd.seq"
            Dim dtab As DataTable = cKon.ambiltabel(sSql, "sPartDtl")
            Session("itemdetail") = dtab : gvrequest.DataSource = dtab
            gvrequest.DataBind() : HitungNet()

            Dim dv As DataView = dtab.DefaultView
            dv.RowFilter = "itemreqoid<>PARTOID"
            '-- KONDISI JIKA TYPE SERVICE ADA SPAREPART --
            If DDLTypeTTS.SelectedValue = "SERVICE" Then
                sSql = "Select rs.trnfinalpartoid, rs.trnfinaloid, rs.reqoid, rs.reqdtloid, rs.itemrusakoid, rs.itemreqoid, rs.itempartoid, rs.qtyrusak, rs.qtyreq, rs.qtypart, seq, rs.locoidrusak, i.itemdesc, rd.itemdesc ItemTitipan From QL_trnfinaldtl rs Inner Join QL_mstitem i ON i.itemoid=rs.itemrusakoid INNER JOIN QL_TRNREQUESTDTL rd ON rd.reqdtloid=rs.reqdtloid Where trnfinaloid=" & OidFinal & " Order By seq"
                Dim dtb As DataTable = cKon.ambiltabel2(sSql, "itemrusak")
                Session("itemrusak") = dtb : GvRusak.DataSource = dtb
                GvRusak.DataBind()

                If dtb.Rows.Count > 0 Then
                    DDLTypeTTS.Enabled = False
                    If lblfinstatus.Text = "POST" Or lblfinstatus.Text = "APPROVED" Then
                        ibposting.Visible = False : BtnSendApp.Visible = False
                        ibdelete.Visible = False : ibsave.Visible = False
                    ElseIf lblfinstatus.Text = "Rejected" Or lblfinstatus.Text = "IN APPROVAL" Then
                        ibposting.Visible = False : BtnSendApp.Visible = False
                        ibdelete.Visible = False : ibsave.Visible = False
                    Else
                        ibposting.Visible = False : BtnSendApp.Visible = True
                        ibdelete.Visible = True
                    End If
                Else
                    If lblfinstatus.Text = "POST" Or lblfinstatus.Text = "APPROVED" Then
                        ibposting.Visible = False : BtnSendApp.Visible = False
                        ibdelete.Visible = False : ibsave.Visible = False
                    ElseIf lblfinstatus.Text = "Rejected" Or lblfinstatus.Text = "IN APPROVAL" Then
                        ibposting.Visible = False : BtnSendApp.Visible = False
                        ibdelete.Visible = False : ibsave.Visible = False
                    Else
                        ibposting.Visible = True : BtnSendApp.Visible = False
                        ibdelete.Visible = True
                    End If
                End If
            Else
                If lblfinstatus.Text = "POST" Or lblfinstatus.Text = "APPROVED" Or lblfinstatus.Text = "Rejected" Then
                    ibposting.Visible = False : BtnSendApp.Visible = False
                    ibdelete.Visible = False : ibsave.Visible = False
                ElseIf lblfinstatus.Text = "Rejected" Or lblfinstatus.Text = "IN APPROVAL" Then
                    ibposting.Visible = False : BtnSendApp.Visible = False
                    ibdelete.Visible = False : ibsave.Visible = False
                Else
                    If dv.Count > 0 Then
                        ibposting.Visible = False : BtnSendApp.Visible = True
                    Else
                        ibposting.Visible = True : BtnSendApp.Visible = False
                    End If
                    ibdelete.Visible = True
                End If
            End If
            '-- AKHIR KONDISI JIKA TYPE SERVICE ADA SPAREPART --
            DDLTypeTTS.Enabled = False 

        Catch ex As Exception
            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString & "<br />" & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub bindorderdata()
        Try
            sSql = " SELECT rm.branch_code, rm.reqoid, Convert(CHAR(20),rm.reqdate,103) reqdate, rm.reqcode, c.custname, c.custoid, c.phone1, rm.branch_code CabangAsal, rm.reqservicecat Cate, Case rm.reqservicecat When 'N' Then 'Service' When 'Y' Then 'Saldo Awal' When 'M' then 'Mutasi' Else 'Service' End sType, SUM(rd.reqqty) ReqQty, ISNULL((SELECT SUM(itemqty) FROM QL_TRNFINALSPART sp INNER JOIN QL_TRNFINAL sf ON sf.FINOID=sp.trnfinaloid AND sp.reqoid=rm.reqoid WHERE FINSTATUS NOT IN ('Rejected') AND (sf.cabangasal=rm.branch_code AND sf.branch_code='" & DdlCabang.SelectedValue & "')),0.00) QtyFinal FROM QL_TRNREQUEST rm INNER JOIN QL_TRNREQUESTDTL rd ON rd.reqmstoid=rm.reqoid INNER JOIN QL_mstcust c ON c.custoid=rm.reqcustoid AND c.branch_code=rm.branch_code INNER JOIN QL_conmtr con ON con.conrefoid=rd.reqdtloid Inner Join QL_mstgen g ON g.genoid=con.mtrlocoid AND gengroup='LOCATION' AND genother5='TITIPAN' AND con.branch_code='" & DdlCabang.SelectedValue & "' AND rd.reqdtloid=con.conrefoid WHERE rm.cmpcode='" & CompnyCode & "' AND " & ddlpopfilter.SelectedValue & " LIKE '%" & TcharNoTrim(tbpopfilter.Text) & "%' AND reqservicecat NOT IN ('Y','M') AND reqstatus IN ('POST','APPROVED') GROUP BY rm.reqoid, rm.reqcode, c.custname, c.custoid, c.phone1, rm.branch_code, reqservicecat, rm.reqdate HAVING(ISNULL(SUM(con.qtyIn)-SUM(con.qtyout),0.00) > 0.00) ORDER BY rm.reqdate DESC"
            Session("dataorderpop") = Nothing
            Session("dataorderpop") = cKon.ambiltabel(sSql, "dataorderpop")
            gvpoporder.DataSource = Session("dataorderpop")
            gvpoporder.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & " - " & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub printinvoiceservices(ByVal name As String)
        Try
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\Invoiceservices.rpt"))
            'cProc.SetDBLogonForReport(rpt)
            rpt.SetParameterValue("sWhere", " AND reqoid = " & Session("oid") & "")
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "INVSVC_" & name)
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Private Sub printinvoicesparepart(ByVal name As String)
        Try
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\Invoicesparepart.rpt"))
            'cProc.SetDBLogonForReport(rpt)
            rpt.SetParameterValue("sWhere", " AND reqoid = " & Session("oid") & "")
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "INVPART_" & name)
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Private Sub createlist()
        Dim dtable As New DataTable
        dtable.Columns.Add("seqbarang", Type.GetType("System.Int32"))
        dtable.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtable.Columns.Add("reqitemoid", Type.GetType("System.Int32"))
        dtable.Columns.Add("spartoid", Type.GetType("System.Int32"))
        dtable.Columns.Add("spartmoid", Type.GetType("System.Int32"))
        dtable.Columns.Add("partdescshort", Type.GetType("System.String"))
        dtable.Columns.Add("merk", Type.GetType("System.String"))
        dtable.Columns.Add("spartqty", Type.GetType("System.Decimal"))
        dtable.Columns.Add("spartprice", Type.GetType("System.Decimal"))
        dtable.Columns.Add("totalprice", Type.GetType("System.Decimal"))
        dtable.Columns.Add("mtrloc", Type.GetType("System.Int32"))
        Session("datadetail") = dtable
    End Sub

    Private Sub createdel()
        Dim objTable As New DataTable
        objTable.Columns.Add("spartoid", Type.GetType("System.Int32"))
        Session("delete") = objTable
    End Sub

    Private Sub InitLoc()
        sSql = "SELECT a.genoid,ISNULL((SELECT gendesc FROM QL_mstgen g WHERE g.genoid=a.genother1 AND g.gengroup = 'WAREHOUSE'),'GUDANG PERJALANAN') +' - '+a.gendesc AS gendesc,* From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' /*AND a.gencode <> 'EXPEDISI'*/ AND c.gencode='" & DdlCabang.SelectedValue & "' AND a.genother6='UMUM'"
        FillDDL(matLoc, sSql)
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "PartOid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("QtyOs") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(4).Controls)), 3)
                            dv(0)("spartprice") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(5).Controls)), 3)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If


        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "PartOid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("QtyOs") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(4).Controls)), 3)
                            dv(0)("spartprice") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(5).Controls)), 3)
                        Else
                            dv(0)("checkvalue") = "False"
                            dv(0)("QtyOs") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(4).Controls)), 3)
                            dv(0)("spartprice") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(5).Controls)), 3)
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub Bindpartdata(ByVal ItemFilter As String)
        If DDLTypeTTS.SelectedValue = "SERVICE" Then
            lblCaptItem.Text = "List Spareparts"
            LabelNya.Visible = True
        Else
            lblCaptItem.Text = "List Katalog Replace"
            LabelNya.Visible = False
        End If

        sSql = "Select *," & ItemOid.Text & " itemoid,'" & DDLTypeTTS.SelectedValue & "' TypeFinal from (Select 'False' Checkvalue,i.itemcode partcode, i.itemdesc partdescshort,i.itemoid PartOid, 'BUAH' satuan3, i.merk brand,i.pricelist spartprice,0.00 partsqty,0.00 QtyOs,Isnull((Select SUM(qtyIn)-SUM(qtyOut) from QL_conmtr con Where con.refoid=i.itemoid And con.branch_code='" & DdlCabang.SelectedValue & "' AND con.periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' AND con.mtrlocoid=" & matLoc.SelectedValue & "),0.00) saldoAkhir,i.satuan1 PartUnitOid,g.gendesc partsUnit,'PARTS' FlagBarang From ql_mstitem i inner join ql_mstgen g on g.genoid=satuan1 and g.gengroup='ITEMUNIT' And itemflag='AKTIF' AND stockflag='T' " & ItemFilter & ") dt"

        Dim dta As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        If dta.Rows.Count > 0 Then
            Session("TblListMat") = dta
            Session("TblListMatView") = Session("TblListMat")
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.SelectedIndex = -1
            GVItemList.Visible = True
        Else
            btnHideItem.Visible = False : panelItem.Visible = False
            showMessage("- Maaf, data yang anda maksud tidak ada stok..!!", 2)
            Exit Sub
        End If
    End Sub

    Private Sub ClearDetail()
        SeqBarang.Text = "1"
        If Session("datarequest") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("datarequest")
            SeqBarang.Text = objTable.Rows.Count + 1
            GvBarang.SelectedIndex = -1
        End If
        I_Text2.Text = "New Detail"

        ItemOid.Text = "" : SerialNo.Text = ""
        lblfinitemname.Text = "" : TypeBarang.Text = ""
        Kelengkapan.Text = "" : Kerusakan.Text = ""
        Qty.Text = "" : DdlUnit.SelectedIndex = 0
        reqdtloid.Text = ""
    End Sub

    Private Sub ClearSparts()
        merkspr.Text = "" : tbsparepartqty.Text = ""
        tbsparepartprice.Text = "" : TotPrice.Text = 0
        Stock.Visible = False : ss.Visible = False
        saldoakhir.Text = "" : tbsparepart.Text = ""

        lblfinitemname.Text = "" : TypeBarang.Text = ""
        Kelengkapan.Text = "" : Kerusakan.Text = ""
        Qty.Text = "0.00"
    End Sub

    Private Sub UnitDDL()
        sSql = "Select genoid,gendesc from QL_mstgen Where gengroup='ITEMUNIT'"
        FillDDL(DdlUnit, sSql)
    End Sub

    Private Sub BindBarang()
        Try
            If lblreqoid.Text = "" Then
                showMessage("- Maaf, Anda belum memilih nomer Tanda Terima Sementara (TTS)...!!", 2)
                Exit Sub
            End If
            sSql = "Select rq.reqdtloid, Rq.reqmstoid, i.itemoid, i.itemcode, i.itemdesc, Rq.reqqty, con.branch_code ToBranch, Rq.snno, rq.kelengkapan, rq.reqdtljob, rq.typegaransi, i.satuan1, 'BUAH' gendesc, con.mtrlocoid OidLoc, 'RUSAK' FlagBarang, 'SERVICE' TypeFinal, ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00) QtySts, ISNULL(SUM(qtyIn),0.00) - ISNULL(SUM(qtyOut),0.00) QtyTtp, ISNULL((QtyFinal),0.00) QtyFinal, (ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00)/* - ISNULL((QtyFinal),0.00)*/) osqty From QL_TRNREQUESTDTL Rq Inner Join QL_mstitem i ON i.itemoid=rq.itemoid Inner Join QL_conmtr con ON con.conrefoid=Rq.reqdtloid AND con.branch_code='" & DdlCabang.SelectedValue & "' AND rq.itemoid=con.refoid AND con.Formname NOT IN ('QL_TRNFINALSPART') Left Join (Select reqdtloid, reqoid, itemreqoid, sum(itemqty) QtyFinal From QL_TRNFINALSPART fs INNER JOIN QL_TRNFINAL fm ON fm.FINOID=fs.trnfinaloid Where (fs.branch_code='" & DdlCabang.SelectedValue & "' OR fm.cabangasal='" & DdlCabang.SelectedValue & "') AND trnfinaloid NOT IN (Select FINOID from QL_TRNFINAL f Where f.FINSTATUS='REJECTED') Group by reqdtloid, reqoid, itemreqoid) fs ON fs.reqoid=rq.reqmstoid AND con.conrefoid=fs.reqdtloid Where con.mtrlocoid IN (SELECT a.genoid From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.genother5='TITIPAN' AND c.gencode=con.branch_code) AND con.branch_code='" & DdlCabang.SelectedValue & "' AND Rq.reqmstoid=" & Integer.Parse(lblreqoid.Text) & " And (i.itemcode Like '%" & TcharNoTrim(lblfinitemname.Text) & "%' Or i.Itemdesc Like '%" & TcharNoTrim(lblfinitemname.Text) & "%') Group by rq.reqdtloid, Rq.reqmstoid, i.itemoid, i.itemcode, i.itemdesc, Rq.reqqty, con.branch_code, Rq.snno, rq.kelengkapan, rq.reqdtljob, rq.typegaransi, i.satuan1, con.mtrlocoid, ISNULL((QtyFinal),0.00)  Having (ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00)/*- ISNULL((QtyFinal),0.00)*/)>0.00"
            Dim dtb As DataTable = cKon.ambiltabel(sSql, "QL_TRNREQUESTDTL")
            GvBarang.DataSource = dtb : Session("GvBarang") = dtb
            GvBarang.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub HitungTotal()
        TotPrice.Text = ToMaskEdit(ToDouble(tbsparepartqty.Text) * ToDouble(tbsparepartprice.Text), 3)
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        'Session.Timeout = 60

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            'Session.Timeout = 60
            Response.Redirect("trnfinalsvc.aspx")
        End If

        Page.Title = CompnyName & " - Final Services"
        Session("oid") = Request.QueryString("oid")

        ibposting.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin memposting data ini?');")
        ibdelete.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin menghapus data ini?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            fDDLBranch()
            Session("delete") = Nothing : Session("datadetail") = Nothing
            bindDataFinal() : InitCabang() : InitLoc() : UnitDDL()
            DdlCabang_SelectedIndexChanged(Nothing, Nothing)

            If Session("oid") = "" Or Session("oid") Is Nothing Then
                lbltitle.Text = "New Final Services"
                clearform() : TabContainer1.ActiveTabIndex = 0
                tglFinal.Text = Format(GetServerTime, "dd/MM/yyyy")
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                GeneratedID() : ibposting.Visible = False
            Else
                lbltitle.Text = "Update Final Services"
                fillTextForm(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            End If
        End If
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("~\Transaction\trnfinalsvc.aspx?awal=true")
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        bindDataFinal()
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        bindDataFinal()
        ddlfilter.SelectedIndex = 0 : tbfilter.Text = ""
        tbperiodstart.Text = Date.Now.ToString("dd/MM/yyyy")
        tbperiodend.Text = Date.Now.ToString("dd/MM/yyyy")
        cbstatus.Checked = False : cbperiod.Checked = False
        ddlfilterstatus.SelectedIndex = 0
        lblerror.Text = ""
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
        If sValidasi.Text <> "" Then
            UpdateCheckedMat()
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.Visible = True
            sValidasi.Text = "" : mpeItem.Show()
        End If
    End Sub

    Protected Sub ibposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposting.Click
        lblfinstatus.Text = "POST" : ibsave_Click(sender, e)
    End Sub

    Protected Sub ibsearchsvc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsearchsvc.Click
        bindorderdata() : cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, True)
    End Sub

    Protected Sub ibpopcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopcancel.Click
        ddlpopfilter.SelectedIndex = 0
        tbpopfilter.Text = ""
        cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, False)
        lblerror.Text = ""
    End Sub

    Protected Sub ibdelsvc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelsvc.Click
        clearform()
    End Sub

    Protected Sub gvpoporder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvpoporder.SelectedIndexChanged
        lblreqoid.Text = Integer.Parse(gvpoporder.SelectedDataKey.Item("reqoid"))
        tbservicesno.Text = gvpoporder.SelectedDataKey.Item("reqcode").ToString
        lblfincust.Text = gvpoporder.SelectedDataKey.Item("custname").ToString
        custoid.Text = gvpoporder.SelectedDataKey.Item("custoid").ToString
        phone.Text = gvpoporder.SelectedDataKey.Item("phone1").ToString
        CabangAsal.Text = gvpoporder.SelectedDataKey.Item("CabangAsal").ToString
        TypeTTS.Text = gvpoporder.SelectedDataKey.Item("Cate").ToString
        sSql = "Select gd.genoid from QL_mstgen gd Where gengroup='location' AND genother6='TITIPAN' And genother2 IN (Select cb.genoid from QL_mstgen cb Where cb.genoid=gd.genother2 AND cb.gengroup='CABANG' AND cb.gencode='" & DdlCabang.SelectedValue & "')"
        OidLocTitipan.Text = GetScalar(sSql)

        cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, False)
        lblerror.Text = "" : lblspartstate.Text = "new" : tbsparepart.Text = ""
        merkspr.Text = "" : tbsparepartqty.Text = 0
        tbsparepartprice.Text = ToMaskEdit(0, 3)
    End Sub

    Protected Sub ibpopfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopfind.Click
        bindorderdata() : mpepopgv.Show()
    End Sub

    Protected Sub gvdata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvdata.PageIndexChanging
        gvdata.PageIndex = e.NewPageIndex
        bindDataFinal()
        'gvdata.DataSource = Session("datafinal")
        'gvdata.DataBind()
    End Sub

    Protected Sub ibpopviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopviewall.Click
        ddlpopfilter.SelectedIndex = 0
        tbpopfilter.Text = ""
        bindorderdata() : mpepopgv.Show()
    End Sub

    Protected Sub ibsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsave.Click
        Dim result As String = "", serviceno As String = tbservicesno.Text
        Dim TotalHpp As Double = 0, ItemParts As Double = 0, hpp As Double = 0, SaldoAkhire As Double = 0.0
        Dim dtab As DataTable = Session("itemdetail")
        Dim dTav As DataView = dtab.DefaultView
        Dim AppOid As Integer = GenerateID("QL_Approval", CompnyCode)
        Dim ItemoidRusak As Integer = GenerateID("QL_trnfinaldtl", CompnyCode)
        Dim spartoid As Integer = 0

        If DDLTypeTTS.SelectedValue = "SERVICE" Then
            lblCaptItem.Text = "Barang Spareparts"
        Else
            lblCaptItem.Text = "Barang Replace"
        End If

        If Not Session("itemdetail") Is Nothing Then
            Dim dtd As DataTable = Session("itemdetail")
            If ToDouble(dtd.Rows.Count) = 0 Then
                result &= "- Maaf, anda belum input data detail barang..!!<br>"
            End If
        Else
            If gvrequest.Visible = False Then
                result &= "- Maaf, anda belum input data detail barang..!!<br>"
            End If
        End If

        If ToDouble(lblreqoid.Text) = 0 Then
            result &= "- Maaf, Silahkan pilih nomor tanda terima dahulu..!!<br>"
        End If

        If DDLTypeTTS.SelectedValue = "REPLACE" Then
            If Session("itemdetail") Is Nothing Then
                result &= "- Maaf, Silahkan pilih barang replace..!!<br>"
            End If
        End If

        sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & CompnyCode & "' AND periodacctg = '" & GetPeriodAcctg(GetServerTime()) & "' AND closingdate = '1/1/1900'"
        If GetScalar(sSql) = 0 Then
            result &= "Tanggal Ini Tidak Dalam Open Stock !!<br>"
            lblfinstatus.Text = "In Process"
        End If

        If Session("oid") <> "" Or Not Session("oid") Is Nothing Then
            If Not Session("itemdetail") Is Nothing Then
                If TypeTTS.Text <> "Y" Then
                    For j As Integer = 0 To dtab.Rows.Count - 1
                        sSql = "select saldoakhir From (Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) saldoAkhir From QL_conmtr con INNER JOIN ql_mstitem m ON m.itemoid=con.refoid Where refoid=" & Integer.Parse(dtab.Rows(j).Item("PARTOID")) & " AND mtrlocoid=" & matLoc.SelectedValue & " AND con.mtrlocoid IN (SELECT genoid FROM QL_mstgen WHERE genother6='UMUM' and gengroup='LOCATION') AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND branch_code='" & DdlCabang.SelectedValue & "' Group By mtrlocoid, periodacctg, branch_code, m.stockflag, m.itemoid) tblitem"
                        If Integer.Parse(dtab.Rows(j).Item("PARTOID")) <> 0 Then
                            SaldoAkhire = GetScalar(sSql)
                            If SaldoAkhire = Nothing Or SaldoAkhire = 0 Then
                                result &= "- Maaf, Stok akhir untuk barang '" & dtab.Rows(j).Item("partdescshort") & "' tidak memenuhi..!!<br>"
                                lblfinstatus.Text = "In Process"
                            End If
                        End If
                    Next
                End If
            End If
        End If

        If lblfinstatus.Text = "IN APPROVAL" Then
            sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNFINAL' And branch_code LIKE '%" & DdlCabang.SelectedValue & "%' order by approvallevel"
            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                result &= "- Maaf, User untuk Cabang " & DdlCabang.SelectedItem.Text & " belum disetting, Silahkan hubungi admin..!!<br>"
            End If
        ElseIf lblfinstatus.Text = "Rejected" Then
            lblfinstatus.Text = "IN PROCESS"
        End If

        If checkApproval("QL_TRNFINAL", "QL_TRNFINAL", Session("oid"), "New", "FINAL", DdlCabang.SelectedValue) > 0 Then
            result &= "- Maaf, data ini sudah send Approval..!!<br>"
            lblfinstatus.Text = "IN APPROVAL"
        End If

        '-- Validasi untuk antisipasi dobel klik --
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_TRNFINAL WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                result &= "Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT FINSTATUS FROM QL_TRNFINAL WHERE FINOID = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & CompnyCode & "'"
            Dim srest As String = GetStrData(sSql)

            If srest Is Nothing Or srest = "" Then
                result &= "Maaf, Data tidak ditemukan..!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
            Else
                If srest.ToLower = "post" Then
                    result &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
                End If
            End If
        End If
        '-- End Validasi untuk antisipasi dobel klik --

        If result <> "" Then
            lblfinstatus.Text = "in process"
            showMessage(result, 2)
            Exit Sub
        End If

        'GENERATED ULANG NO TRANSAKSI ANTISIPASI JIKA ADA USER INPUT BERSAMAN
        If Session("oid") = "" Or Session("oid") Is Nothing Then
            FinOid.Text = GenerateID("QL_TRNFINAL", CompnyCode)
            TrnFinalNo.Text = GenerateID("QL_TRNFINAL", CompnyCode)
        Else
            FinOid.Text = Session("oid")
            TrnFinalNo.Text = Session("oid")
        End If

        If lblfinstatus.Text = "POST" Then
            GeneratedNo()
        End If

        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            If Session("oid") = "" Or Session("oid") Is Nothing Then
                sSql = "INSERT INTO QL_TRNFINAL ([CMPCODE], [BRANCH_CODE], [FINOID], [finaldate], [MSTREQOID], [custoid], [priceservis], [amtnettservis], [FINITEMSTATUS], [mtrlocoid], [SJOID], [FINTIME], [FINSTATUS], [RECEIVETIME], [CREATEUSER], [CREATETIME], [UPDUSER], [UPDTIME], [trnfinalno], [cabangasal], [locoidtitipan], [locoidrusak], [typetts], [flagfinal])" & _
                " VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & Integer.Parse(FinOid.Text) & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & Integer.Parse(lblreqoid.Text) & ", " & Integer.Parse(custoid.Text) & ", " & ToDouble(BiayaSrv.Text) & ", " & ToDouble(NettNya.Text) & ", '" & Tchar(lblfinstatus.Text) & "', " & matLoc.SelectedValue & ", 0, CURRENT_TIMESTAMP, '" & Tchar(lblfinstatus.Text) & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', '" & CDate(toDate(createtime.Text)) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Tchar(TrnFinalNo.Text) & "', '" & CabangAsal.Text & "', " & Integer.Parse(OidLocTitipan.Text) & ", " & Integer.Parse(OidLocRusak.Text) & ", '" & DDLTypeTTS.SelectedValue & "', '" & TypeTTS.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(FinOid.Text) & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNFINAL'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("itemdetail") Is Nothing Then
                    If dtab.Rows.Count > 0 Then
                        sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid WHERE tablename = 'QL_TRNFINALSPART' AND cmpcode = '" & CompnyCode & "'"
                        xCmd.CommandText = sSql : spartoid = xCmd.ExecuteScalar

                        For i As Integer = 0 To dtab.Rows.Count - 1
                            spartoid = spartoid + 1
                            sSql = "INSERT INTO QL_TRNFINALSPART ([cmpcode], [branch_code], [trnifinaldtloid], [trnfinaloid], [reqoid], [reqdtloid], [itemreqoid], [itemqty], [unitoid], [typebarang], [itempartoid], [itempartqty], [itempartunitoid], [itemlocoid], [itempartprice], [itemtotalprice], [seq], [itempartstatusdtl], [upduser], [updtime], [locoidtitipan], [locoidrusak])" & _
                            " VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & Integer.Parse(spartoid) & ", " & Integer.Parse(FinOid.Text) & ", " & Integer.Parse(lblreqoid.Text) & ", " & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & ", " & Integer.Parse(dtab.Rows(i).Item("itemoid")) & ", " & ToDouble(dtab.Rows(i).Item("reqqty")) & ", " & Integer.Parse(dtab.Rows(i).Item("satuan1")) & ", '" & dtab.Rows(i).Item("typegaransi").ToString & "', " & Integer.Parse(dtab.Rows(i).Item("PARTOID")) & ", " & ToDouble(dtab.Rows(i).Item("partsqty")) & ", " & Integer.Parse(dtab.Rows(i).Item("PartUnitOid")) & ", " & Integer.Parse(matLoc.SelectedValue) & ", " & ToDouble(dtab.Rows(i).Item("spartprice")) & ", " & ToDouble(dtab.Rows(i).Item("totalprice")) & ", " & dtab.Rows(i).Item("seq") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Integer.Parse(OidLocTitipan.Text) & ", " & Integer.Parse(OidLocRusak.Text) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next

                        sSql = "UPDATE QL_mstoid SET lastoid = " & spartoid & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNFINALSPART'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                '-- Input detail barang rusak --
                If Not Session("itemrusak") Is Nothing Then
                    dTav.RowFilter = "PARTOID>0"
                    Dim dtb As DataTable = Session("itemrusak")
                    If dTav.Count > 0 And dtb.Rows.Count <= 0 Then
                        otrans.Rollback() : conn.Close() : lblfinstatus.Text = "In Process"
                        showMessage("- Maaf, Silahkan Input Detail Barang Rusak", 2)
                        Exit Sub
                    End If

                    For i As Integer = 0 To dtb.Rows.Count - 1
                        spartoid = spartoid + 1
                        sSql = "Select hpp From ql_mstitem Where itemoid=" & Integer.Parse(dtb.Rows(i).Item("itemrusakoid")) & ""
                        xCmd.CommandText = sSql : Dim LastHpp As Double = xCmd.ExecuteScalar

                        sSql = "INSERT INTO [QL_trnfinaldtl] ([cmpcode], [branch_code], [trnfinalpartoid], [trnfinaldtloid], [trnfinaloid], [reqoid],[reqdtloid], [itemrusakoid], [itemreqoid], [itempartoid], [qtyrusak], [qtyreq], [qtypart], [hpp], [updtime], [upduser], [seq], [locoidrusak], [typefinal]) " & _
                        "VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & ItemoidRusak & ", " & Integer.Parse(spartoid) & ", " & Integer.Parse(FinOid.Text) & ", " & Integer.Parse(lblreqoid.Text) & ", " & Integer.Parse(dtb.Rows(i).Item("reqdtloid")) & ", " & Integer.Parse(dtb.Rows(i).Item("itemrusakoid")) & ", " & Integer.Parse(dtb.Rows(i).Item("itemreqoid")) & ", " & Integer.Parse(dtb.Rows(i).Item("itempartoid")) & ", " & ToDouble(dtb.Rows(i).Item("qtyrusak")) & ", " & ToDouble(dtb.Rows(i).Item("qtyreq")) & ", " & ToDouble(dtb.Rows(i).Item("qtypart")) & ", " & ToDouble(LastHpp) & ", CURRENT_TIMESTAMP, '" & Session("UserID") & "', " & Integer.Parse(dtb.Rows(i).Item("seq")) & ", " & Integer.Parse(dtb.Rows(i).Item("locoidrusak")) & ", 'EXTERNAL')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        ItemoidRusak += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid = " & ItemoidRusak & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_trnfinaldtl'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

            Else ' Proses Edit

                sSql = "UPDATE QL_TRNFINAL SET flagfinal='" & TypeTTS.Text & "', [finaldate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), [MSTREQOID] = " & lblreqoid.Text & ", [custoid] = " & custoid.Text & ", [priceservis] = " & ToDouble(BiayaSrv.Text) & ", [amtnettservis] = " & ToDouble(NettNya.Text) & ", [FINITEMSTATUS] = '" & lblfinstatus.Text & "', [mtrlocoid] = " & Integer.Parse(matLoc.SelectedValue) & ", [FINSTATUS] ='" & lblfinstatus.Text & "', [UPDUSER] = '" & Session("UserID") & "', [UPDTIME]=Current_timestamp, [trnfinalno] = '" & Tchar(TrnFinalNo.Text) & "', [cabangasal] = '" & CabangAsal.Text & "', [trnfinalnote] = '" & Tchar(tbsparepartjob.Text) & "', locoidtitipan=" & Integer.Parse(OidLocTitipan.Text) & ", locoidrusak=" & Integer.Parse(OidLocRusak.Text) & " WHERE [FINOID]=" & Session("oid") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "DELETE FROM ql_trnfinalspart WHERE trnfinaloid=" & Session("oid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("itemdetail") Is Nothing Then
                    If dtab.Rows.Count > 0 Then
                        sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid WHERE tablename = 'QL_TRNFINALSPART' AND cmpcode = '" & CompnyCode & "'"
                        xCmd.CommandText = sSql : spartoid = xCmd.ExecuteScalar

                        For i As Integer = 0 To dtab.Rows.Count - 1
                            spartoid = spartoid + 1
                            sSql = "INSERT INTO QL_TRNFINALSPART ([cmpcode], [branch_code], [trnifinaldtloid], [trnfinaloid], [reqoid], [reqdtloid], [itemreqoid], [itemqty], [unitoid], [typebarang], [itempartoid], [itempartqty], [itempartunitoid], [itemlocoid], [itempartprice], [itemtotalprice], [seq], [itempartstatusdtl], [upduser], [updtime], locoidtitipan, locoidrusak)" & _
                            " VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & Integer.Parse(spartoid) & ", " & Integer.Parse(Session("oid")) & ", " & Integer.Parse(lblreqoid.Text) & ", " & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & ", " & Integer.Parse(dtab.Rows(i).Item("itemoid")) & ", " & ToDouble(dtab.Rows(i).Item("reqqty")) & ", " & Integer.Parse(dtab.Rows(i).Item("satuan1")) & ", '" & dtab.Rows(i).Item("typegaransi").ToString & "', " & Integer.Parse(dtab.Rows(i).Item("PARTOID")) & ", " & ToDouble(dtab.Rows(i).Item("partsqty")) & ", " & Integer.Parse(dtab.Rows(i).Item("PartUnitOid")) & ", " & Integer.Parse(matLoc.SelectedValue) & ", " & ToDouble(dtab.Rows(i).Item("spartprice")) & ", " & ToDouble(dtab.Rows(i).Item("totalprice")) & ", " & dtab.Rows(i).Item("seq") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Integer.Parse(OidLocTitipan.Text) & ", " & Integer.Parse(OidLocRusak.Text) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ItemoidRusak += 1
                        Next

                        sSql = "UPDATE QL_mstoid SET lastoid = " & spartoid & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNFINALSPART'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                If DDLTypeTTS.SelectedValue = "SERVICE" Then
                    '-- Input detail barang rusak --
                    sSql = "DELETE FROM QL_trnfinaldtl WHERE trnfinaloid=" & Session("oid")
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    If Not Session("itemrusak") Is Nothing Then
                        dTav.RowFilter = "PARTOID>0"
                        Dim dtb As DataTable = Session("itemrusak")
                        If dTav.Count > 0 And dtb.Rows.Count <= 0 Then
                            otrans.Rollback() : conn.Close() : lblfinstatus.Text = "In Process"
                            showMessage("- Maaf, Silahkan Input Detail Barang Rusak", 2)
                            Exit Sub
                        End If

                        For i As Integer = 0 To dtb.Rows.Count - 1
                            spartoid = spartoid + 1

                            sSql = "Select hpp From ql_mstitem Where itemoid=" & Integer.Parse(dtb.Rows(i).Item("itemrusakoid")) & ""
                            xCmd.CommandText = sSql : Dim LastHpp As Double = xCmd.ExecuteScalar

                            sSql = "INSERT INTO [QL_trnfinaldtl] ([cmpcode], [branch_code], [trnfinalpartoid], [trnfinaldtloid], [trnfinaloid], [reqoid], [reqdtloid], [itemrusakoid], [itemreqoid], [itempartoid], [qtyrusak], [qtyreq], [qtypart], [hpp], [updtime], [upduser], [seq], [locoidrusak]) " & _
                            "VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & ItemoidRusak & ", " & Integer.Parse(spartoid) & ", " & Integer.Parse(FinOid.Text) & ", " & Integer.Parse(lblreqoid.Text) & ", " & Integer.Parse(dtb.Rows(i).Item("reqdtloid")) & ", " & Integer.Parse(dtb.Rows(i).Item("itemrusakoid")) & ", " & Integer.Parse(dtb.Rows(i).Item("itemreqoid")) & ", " & Integer.Parse(dtb.Rows(i).Item("itempartoid")) & ", " & ToDouble(dtb.Rows(i).Item("qtyrusak")) & ", " & ToDouble(dtb.Rows(i).Item("qtyreq")) & ", " & ToDouble(dtb.Rows(i).Item("qtypart")) & ", " & ToDouble(LastHpp) & ", CURRENT_TIMESTAMP, '" & Session("UserID") & "', " & Integer.Parse(dtb.Rows(i).Item("seq")) & ", " & Integer.Parse(dtb.Rows(i).Item("locoidrusak")) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ItemoidRusak += 1
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid = " & ItemoidRusak & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_trnfinaldtl'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                If lblfinstatus.Text = "POST" Then
                    'Generate crdmtr ID
                    sSql = "SELECT ISNULL(lastoid,0)+1 FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : Dim conmtroid As Int32 = xCmd.ExecuteScalar
                    ' Input proses pembeNtukan stok
                    If Not Session("itemdetail") Is Nothing Then
                        For t As Integer = 0 To dtab.Rows.Count - 1
                            '------------------------------ 
                            'jika type final adalah replace
                            If DDLTypeTTS.SelectedValue = "REPLACE" Then
                                '---------------------------------------------
                                'mulai proses input stok keluar gudang titipan 
                                '    sSql = "select hpp from ql_mstitem where itemoid=" & Integer.Parse(dtab.Rows(t).Item("itemreqoid")) & ""
                                '    xCmd.CommandText = sSql : Dim HppRusak As Double = xCmd.ExecuteScalar

                                '    sSql = "INSERT INTO QL_conmtr (cmpcode, branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, conrefoid) " & _
                                '"VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & conmtroid & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & TrnFinalNo.Text & "', '" & FinOid.Text & "', 'QL_TRNFINALSPART', " & dtab.Rows(t).Item("itemreqoid") & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & Integer.Parse(OidLocTitipan.Text) & ", 0, " & ToDouble(dtab.Rows(t).Item("itemqty")) & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(dtab.Rows(t).Item("itemqty")) * ToDouble(HppRusak) & ", '" & Tchar(tbservicesno.Text) & "-" & TrnFinalNo.Text & "', " & ToDouble(HppRusak) & ", " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                                '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                '    conmtroid += 1
                                '    'End proses input stok keluar gudang titipan
                                '    '-------------------------------------------

                                '    '-------------------------------------------------
                                '    'mulai proses input stok keluar gudang barang bagus 
                                '    sSql = "select hpp from ql_mstitem where itemoid=" & Integer.Parse(dtab.Rows(t).Item("PARTOID")) & ""
                                '    xCmd.CommandText = sSql : Dim HppPengganti As Double = xCmd.ExecuteScalar

                                '    sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, conrefoid) " & _
                                '"VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & conmtroid & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & TrnFinalNo.Text & "', '" & FinOid.Text & "', 'QL_TRNFINALSPART', " & dtab.Rows(t).Item("PARTOID") & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & Integer.Parse(matLoc.SelectedValue) & ", 0, " & ToDouble(dtab.Rows(t).Item("partsqty")) & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(HppPengganti) * ToDouble(dtab.Rows(t).Item("partsqty")) & ", '" & Tchar(tbservicesno.Text) & "-" & TrnFinalNo.Text & "', " & ToDouble(HppPengganti) & ", " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                                '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                '    conmtroid += 1

                                '    'mulai proses input stok masuk gudang rusak
                                '    '-------------------------------------------
                                '    sSql = "INSERT INTO QL_conmtr (cmpcode, branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, conrefoid) " & _
                                '"VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & conmtroid & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & TrnFinalNo.Text & "', '" & Integer.Parse(FinOid.Text) & "', 'QL_TRNFINALSPART', " & dtab.Rows(t).Item("itemreqoid") & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & Integer.Parse(OidLocRusak.Text) & ", " & ToDouble(dtab.Rows(t).Item("itemqty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(dtab.Rows(t).Item("itemqty")) * ToDouble(HppRusak) & ", '" & Tchar(tbservicesno.Text) & "-" & TrnFinalNo.Text & "', " & ToDouble(HppRusak) & ", " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                                '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                '    conmtroid += 1
                                '    'End proses input stok masuk gudang rusak
                                '    '---------------------------------------- 

                                '    If TypeTTS.Text <> "Y" Then
                                '        'mulai proses input stok masuk gudang titipan dari barang replace
                                '        '----------------------------------------------------------------
                                '        If Integer.Parse(dtab.Rows(t).Item("PARTOID")) <> 0 Then
                                '            sSql = "INSERT INTO QL_conmtr (cmpcode, branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, conrefoid) " & _
                                '            "VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & conmtroid & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & TrnFinalNo.Text & "', '" & Integer.Parse(FinOid.Text) & "', 'QL_TRNFINALSPART', " & Integer.Parse(dtab.Rows(t).Item("PARTOID")) & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & Integer.Parse(OidLocTitipan.Text) & ", " & ToDouble(dtab.Rows(t).Item("partsqty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & ToDouble(dtab.Rows(t).Item("spartprice")) * ToDouble(dtab.Rows(t).Item("partsqty")) & ", " & ToDouble(dtab.Rows(t).Item("itemqty")) * ToDouble(HppRusak) & ", '" & Tchar(tbservicesno.Text) & "-" & TrnFinalNo.Text & "', " & ToDouble(HppRusak) & ", " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                                '            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                '            conmtroid += 1
                                '        End If
                                '    End If
                                'end jika type final replace
                                '---------------------------   
                            Else
                                '---------------------------------------------
                                'mulai proses input stok keluar gudang titipan 
                                sSql = "select hpp from ql_mstitem where itemoid=" & Integer.Parse(dtab.Rows(t).Item("itemreqoid")) & ""
                                xCmd.CommandText = sSql : Dim HppRusak As Double = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_conmtr (cmpcode, branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, conrefoid) " & _
                            "VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & conmtroid & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & TrnFinalNo.Text & "', '" & FinOid.Text & "', 'QL_TRNFINALSPART', " & dtab.Rows(t).Item("itemreqoid") & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & Integer.Parse(OidLocTitipan.Text) & ", 0, " & ToDouble(dtab.Rows(t).Item("itemqty")) & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(dtab.Rows(t).Item("itemqty")) * ToDouble(HppRusak) & ", '" & Tchar(tbservicesno.Text) & "-" & TrnFinalNo.Text & "', " & ToDouble(HppRusak) & ", " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                conmtroid += 1

                                sSql = "INSERT INTO QL_conmtr (cmpcode, branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, conrefoid) " & _
                            "VALUES ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & conmtroid & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & TrnFinalNo.Text & "', '" & FinOid.Text & "', 'QL_TRNFINALSPART', " & dtab.Rows(t).Item("itemreqoid") & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & Integer.Parse(OidLocTitipan.Text) & ",  " & ToDouble(dtab.Rows(t).Item("itemqty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(dtab.Rows(t).Item("itemqty")) * ToDouble(HppRusak) & ", '" & Tchar(tbservicesno.Text) & "-" & TrnFinalNo.Text & "', " & ToDouble(HppRusak) & ", " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                conmtroid += 1
                            End If
                            '-- update tgl 01 feb 2021
                            'end jika type final service
                            '---------------------------   
                        Next

                        sSql = "Update QL_mstoid set lastoid = " & conmtroid + 1 & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        If TypeTTS.Text <> "Y" Then
                            '---- COA PERSEDIAAN BARANG DAGANGAN ----
                            Dim iMatAccount As String = GetVarInterface("VAR_GUDANG", DdlCabang.SelectedValue)
                            If iMatAccount = "?" Or iMatAccount = "0" Or iMatAccount = "" Then
                                result &= "- Maaf,interface 'VAR_GUDANG' belum di setting silahkan hubungi admin untuk seting interface !!"
                            End If

                            sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iMatAccount & "'"
                            xCmd.CommandText = sSql : Dim COA_gudang As Integer = xCmd.ExecuteScalar
                            If COA_gudang = 0 Or COA_gudang = Nothing Then
                                result &= "Maaf, Akun COA untuk VAR_GUDANG tidak ditemukan..!!<br>"
                            End If

                            '---- COA PERSEDIAAN SEMENTARA ----
                            Dim iAkunSementara As String = GetVarInterface("VAR_SERVIS", DdlCabang.SelectedValue)

                            If iAkunSementara = "?" Or iAkunSementara = "0" Or iAkunSementara = "" Then
                                result &= "- Maaf,interface 'VAR_SERVIS' belum di setting silahkan hubungi admin untuk seting interface !!<br>"
                            End If

                            sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iAkunSementara & "'"
                            xCmd.CommandText = sSql : Dim COA_TAMPUNG As Integer = xCmd.ExecuteScalar
                            If COA_TAMPUNG = 0 Or COA_TAMPUNG = Nothing Then
                                result &= "Maaf, Akun COA untuk VAR_SERVIS tidak ditemukan..!!<br>"
                            End If

                            If result <> "" Then
                                otrans.Rollback() : conn.Close()
                                lblfinstatus.Text = "In Process"
                                showMessage(result, 2)
                                Exit Sub
                            End If

                            Dim iglmst As Int64 = GenerateID("QL_trnglmst", CompnyCode)
                            Dim igldtl As Int64 = GenerateID("QL_trngldtl", CompnyCode)

                            For T1 As Integer = 0 To dtab.Rows.Count - 1
                                sSql = "Select HPP from QL_mstitem Where itemoid=" & dtab.Rows(T1).Item("PARTOID") & ""
                                xCmd.CommandText = sSql : hpp = xCmd.ExecuteScalar
                                TotalHpp += ToDouble(hpp) * dtab.Rows(T1).Item("partsqty")
                            Next

                            '--- Jika Final Type Replace ---
                            If DDLTypeTTS.SelectedValue = "REPLACE" Then
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime) VALUES" & _
                                " ('" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & Integer.Parse(iglmst) & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', 'TRNFINAL(" & TrnFinalNo.Text & ")', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(iglmst) & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                '---- Jurnal Pesediaan barang dagangan --------
                                '   COA                      |     D   |   C
                                'Persediaan dalam perjalanan | 450.000 |
                                'Persediaan barang dagangan  |         | 450.000        
                                '-----------------------------------------------
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                                " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & ", " & 1 & ", " & Integer.Parse(iglmst) & ", " & Integer.Parse(COA_TAMPUNG) & ", 'D', " & ToDouble(TotalHpp) & ", " & ToDouble(TotalHpp) & ", " & 0 & ", '" & TrnFinalNo.Text & "', 'TRNFINAL(" & TrnFinalNo.Text & ")', '" & FinOid.Text & "', '', 'POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DdlCabang.SelectedValue & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                igldtl = igldtl + 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                                "('" & CompnyCode & "', " & Integer.Parse(igldtl) & ", " & 2 & ", " & Integer.Parse(iglmst) & ", " & Integer.Parse(COA_gudang) & ", 'C', " & ToDouble(TotalHpp) & ", " & ToDouble(TotalHpp) & ", " & ToDouble(TotalHpp) & ", '" & TrnFinalNo.Text & "', 'TRNFINAL(" & TrnFinalNo.Text & ")', '" & FinOid.Text & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DdlCabang.SelectedValue & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                hpp = 0 : TotalHpp = 0
                                For T1 As Integer = 0 To dtab.Rows.Count - 1
                                    sSql = "Select HPP from QL_mstitem Where itemoid=" & dtab.Rows(T1).Item("itemreqoid") & ""
                                    xCmd.CommandText = sSql : hpp = xCmd.ExecuteScalar
                                    TotalHpp += ToDouble(hpp) * dtab.Rows(T1).Item("itemqty")
                                Next

                                '---- COA Persediaan Barang rusak ----
                                Dim COABarangRusak As String = GetVarInterface("VAR_STOK_RUSAK", DdlCabang.SelectedValue)
                                If COABarangRusak = "?" Or COABarangRusak = "0" Or COABarangRusak = "" Then
                                    result &= "- Maaf,interface 'VAR_STOK_RUSAK' belum di setting silahkan hubungi admin untuk seting interface !!"
                                End If

                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & COABarangRusak & "'"
                                xCmd.CommandText = sSql : Dim OidStokRusak As Integer = xCmd.ExecuteScalar
                                If OidStokRusak = 0 Or OidStokRusak = Nothing Then
                                    result &= "Maaf sekali, Akun COA untuk VAR_STOK_RUSAK tidak ditemukan..!!<br>"
                                End If

                                '---- COA Biaya Pemakaian Barang ----
                                Dim COABiayaRusak As String = GetVarInterface("VAR_BIAYA_RUSAK", DdlCabang.SelectedValue)
                                If COABiayaRusak = "?" Or COABiayaRusak = "0" Or COABiayaRusak = "" Then
                                    result &= "- Maaf,interface 'VAR_BIAYA_RUSAK' belum di setting silahkan hubungi admin untuk seting interface !!"
                                End If

                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iMatAccount & "'"
                                xCmd.CommandText = sSql : Dim OidBiayaRusak As Integer = xCmd.ExecuteScalar
                                If OidBiayaRusak = 0 Or OidBiayaRusak = Nothing Then
                                    result &= "Maaf, Akun COA untuk VAR_BIAYA_RUSAK tidak ditemukan..!!<br>"
                                End If

                                If result <> "" Then
                                    otrans.Rollback() : conn.Close()
                                    lblfinstatus.Text = "In Process"
                                    showMessage(result, 2)
                                    Exit Sub
                                End If

                                '---- Jurnal Pesediaan barang dagangan -------
                                '       COA                |    D    |   C
                                'Persediaan Barang rusak   | 450.000 |
                                'Biaya Pemakaian persediaan|         | 450.000        
                                '---------------------------------------------
                                igldtl = igldtl + 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                               " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & ", " & 3 & ", " & Integer.Parse(iglmst) & ", " & Integer.Parse(OidStokRusak) & ", 'D', " & ToDouble(TotalHpp) & ", " & ToDouble(TotalHpp) & ", " & 0 & ", '" & TrnFinalNo.Text & "', 'TRNFINAL(" & TrnFinalNo.Text & ")', '" & FinOid.Text & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DdlCabang.SelectedValue & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                igldtl = igldtl + 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                                "('" & CompnyCode & "', " & Integer.Parse(igldtl) & ", " & 4 & ", " & Integer.Parse(iglmst) & ", " & Integer.Parse(OidBiayaRusak) & ", 'C', " & ToDouble(TotalHpp) & ", " & ToDouble(TotalHpp) & ", " & ToDouble(TotalHpp) & ", '" & TrnFinalNo.Text & "', 'TRNFINAL(" & TrnFinalNo.Text & ")', '" & FinOid.Text & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DdlCabang.SelectedValue & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                sSql = "Update QL_mstoid set lastoid = " & igldtl & " Where tablename = 'QL_trngldtl' And cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            End If
                            '------ Jika Final Type Replace -------
                        End If
                    End If
                End If
            End If '-- End Post --

            If lblfinstatus.Text = "IN APPROVAL" Then
                If Session("oid") <> Nothing Or Session("oid") <> "" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL (cmpcode, approvaloid, branch_code, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus) VALUES" & _
                            " ('" & CompnyCode & "', " & AppOid & ", '" & DdlCabang.SelectedValue & "', '" & "FIN" & AppOid & "_" & AppOid & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'QL_TRNFINAL', '" & Session("oid") & "', 'IN APPROVAL', '0', '" & objTable.Rows(c1).Item("approvaluser") & "', '1/1/1900', '" & objTable.Rows(c1).Item("approvaltype") & "', '1', '" & objTable.Rows(c1).Item("approvalstatus") & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            AppOid += 1
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & AppOid - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Else
                    otrans.Rollback() : conn.Close()
                    lblfinstatus.Text = "In Process"
                    showMessage("- Silahkan Input Data", 2)
                    Exit Sub
                End If
            End If
            otrans.Commit() : conn.Close()

        Catch ex As Exception
            otrans.Rollback() : conn.Close()
            lblfinstatus.Text = "In Process"
            showMessage(ex.ToString & "; " & sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnfinalsvc.aspx?awal=true")
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click
        Dim result As String = ""
        result = deleterecord()
        If result = "" Then
            Response.Redirect("~\Transaction\trnfinalsvc.aspx?awal=true")
        Else
            showMessage(result, 2)
        End If
    End Sub

    Protected Sub ibclearspart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibclearspart.Click
        tbsparepart.Text = ""
        merkspr.Text = "" : tbsparepartqty.Text = ""
        tbsparepartprice.Text = ""
        lblspartstate.Text = "new" : tbsparepartqty.Text = 0
        tbsparepartprice.Text = ToMaskEdit(0, 3)
        Stock.Visible = False : ss.Visible = False : saldoakhir.Text = ""
    End Sub

    Protected Sub ibsearchspart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsearchspart.Click
        'bindpartdata()
        'cProc.SetModalPopUpExtender(bepoppartgv, panpoppartgv, mpepoppartgv, True)
    End Sub

    Protected Sub ibpoppartfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoppartfind.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = ddlpoppartfilter.SelectedValue & " LIKE '%" & Tchar(tbpoppartfilter.Text) & "%'"

                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind() : dv.RowFilter = ""
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
            End If
        Else
            showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
        End If
    End Sub

    Protected Sub ibpoppartviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoppartviewall.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                ddlpoppartfilter.SelectedIndex = -1
                tbpoppartfilter.Text = ""
                Session("TblListMatView") = Session("TblListMat")
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind()
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
            End If
        Else
            showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
        End If
    End Sub

    Protected Sub DdlCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitLoc()
    End Sub

    Protected Sub ImbEbarang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImbEbarang.Click
        ItemOid.Text = "" : SerialNo.Text = ""
        lblfinitemname.Text = "" : TypeBarang.Text = ""
        Kelengkapan.Text = "" : Kerusakan.Text = ""
        Qty.Text = "" : reqdtloid.Text = ""
        OidLocTitipan.Text = "" : FlagBarang.Text = ""
        GvBarang.Visible = False
    End Sub

    Protected Sub GvBarang_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvBarang.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub GvBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvBarang.SelectedIndexChanged
        Try
            Dim jMsg As String = ""
            ItemOid.Text = Integer.Parse(GvBarang.SelectedDataKey.Item("itemoid"))
            SerialNo.Text = GvBarang.SelectedDataKey.Item("snno").ToString
            lblfinitemname.Text = GvBarang.SelectedDataKey.Item("itemdesc").ToString
            TypeBarang.Text = GvBarang.SelectedDataKey.Item("typegaransi").ToString
            Kelengkapan.Text = GvBarang.SelectedDataKey.Item("kelengkapan").ToString
            Kerusakan.Text = GvBarang.SelectedDataKey.Item("reqdtljob").ToString
            Qty.Text = ToMaskEdit(ToDouble(GetTextValue(GvBarang.SelectedRow.Cells(5).Controls)), 3)
            DdlUnit.SelectedValue = Integer.Parse(GvBarang.SelectedDataKey.Item("satuan1"))
            reqdtloid.Text = Integer.Parse(GvBarang.SelectedDataKey.Item("reqdtloid"))
            FlagBarang.Text = GvBarang.SelectedDataKey.Item("FlagBarang").ToString

            sSql = "Select gd.genoid from QL_mstgen gd Where gengroup='location' AND genother6='RUSAK' And genother2 IN (Select cb.genoid from QL_mstgen cb Where cb.genoid=gd.genother2 AND cb.gengroup='CABANG' AND cb.gencode='" & DdlCabang.SelectedValue & "')"

            Dim LocOidRusak As Integer = GetScalar(sSql)
            If LocOidRusak = 0 Then
                jMsg &= "- Maaf, Gudang Barang rusak untuk cabang Anda belum ada, Silahkan hubungi admin agar di input gudang baru pada form master general..!!!<br>"
            Else
                OidLocRusak.Text = LocOidRusak
            End If

            If ToDouble(Qty.Text) = 0 Then
                jMsg &= "- Maaf, Kolom Qty penerimaan belum anda isi..!!<br>"
            End If

            If ToDouble(Qty.Text) > ToDouble(GvBarang.SelectedDataKey.Item("QtySts")) Then
                jMsg &= "- Maaf, Qty yang anda input melebihi qty TTS..!!<br>"
            End If

            If jMsg <> "" Then
                Qty.Text = "" : showMessage(jMsg, 2)
                Exit Sub
            End If

            GvBarang.Visible = False
            If TypeTTS.Text <> "M" Then
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
                If DDLTypeTTS.SelectedValue = "REPLACE" Then
                    Bindpartdata("AND i.itemoid=" & ItemOid.Text & "")
                    CBGantiBarang.Visible = True
                Else
                    Bindpartdata("") : CBGantiBarang.Checked = False
                    CBGantiBarang.Visible = False
                End If
                BtnAddToListBarang.Visible = False
            Else
                BtnAddToListBarang.Visible = True
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub BtnAddToListBarang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAddToListBarang.Click
        Dim SaldoAwal As String = GetStrData("SELECT reqservicecat FROM QL_TRNREQUEST WHERE reqoid = " & Integer.Parse(lblreqoid.Text) & "")
        Dim sErr As String = "" : Dim dtab As DataTable

        If Session("itemdetail") Is Nothing Then
            dtab = CrtDtlBarang() : Session("itemdetail") = dtab
            SeqBarang.Text = 0 + 1
        Else
            dtab = Session("itemdetail")
            SeqBarang.Text = dtab.Rows.Count + 1
        End If

        If sValidasi.Text <> "" Then
            showMessage(sValidasi.Text, 2)
            Exit Sub
        End If

        Dim dtView As DataView = dtab.DefaultView
        If I_Text2.Text = "New Detail" Then
            dtView.RowFilter = "itemoid= '" & ItemOid.Text & "' AND reqdtloid=" & reqdtloid.Text
        Else
            dtView.RowFilter = "itemoid= '" & ItemOid.Text & "' AND reqdtloid=" & reqdtloid.Text & " AND seq<>" & SeqBarang.Text
        End If

        If dtView.Count > 0 Then
            If ItemOid.Text <> "" Then
                sValidasi.Text &= "Maaf, " & lblfinitemname.Text & " Sudah ditambahkan, cek pada tabel..!!!<BR>"
            End If
            dtView.RowFilter = ""
            Exit Sub
        End If
        dtView.RowFilter = ""

        If sValidasi.Text <> "" Then
            showMessage(sValidasi.Text, 2)
            Exit Sub
        End If

        Dim objRow As DataRow
        If I_Text2.Text = "New Detail" Then
            objRow = dtab.NewRow()
            objRow("seq") = dtab.Rows.Count + 1
        Else
            Dim selrow As DataRow() = dtab.Select("seq=" & SeqBarang.Text)
            objRow = selrow(0)
            objRow.BeginEdit()
        End If

        Dim drow As DataRow : Dim drowedit() As DataRow
        If I_Text2.Text = "New Detail" Then
            drow = dtab.NewRow
            drow("seq") = SeqBarang.Text
            drow("reqmstoid") = Integer.Parse(lblreqoid.Text)
            drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
            drow("itemoid") = Integer.Parse(ItemOid.Text)
            drow("snno") = SerialNo.Text
            drow("itemdesc") = lblfinitemname.Text
            drow("reqqty") = ToMaskEdit(ToDouble(Qty.Text), 3)
            drow("satuan1") = DdlUnit.SelectedValue
            drow("Unit") = DdlUnit.SelectedItem.Text
            drow("Kelengkapan") = Kelengkapan.Text
            drow("reqdtljob") = Kelengkapan.Text
            drow("typegaransi") = TypeBarang.Text
            drow("PARTOID") = 0
            drow("partdescshort") = ""
            drow("PartUnitOid") = 0
            drow("partsqty") = ToMaskEdit(0, 3)
            drow("PartsUnit") = ""
            drow("mtrloc") = 0
            drow("spartprice") = ToMaskEdit(0, 3)
            drow("totalprice") = ToMaskEdit(0, 3)
            dtab.Rows.Add(drow) : dtab.AcceptChanges()
        End If
        drow.EndEdit() : SeqBarang.Text += 1
        dtView.RowFilter = ""

        DDLTypeTTS.Enabled = False
        BtnAddToListBarang.Visible = False
        btnHideItem.Visible = False : panelItem.Visible = False
        mpeItem.Show() : gvrequest.DataSource = dtab
        gvrequest.DataBind() : Session("itemdetail") = dtab
        HitungNet() : ClearSparts()

        If SaldoAwal = "M" Then
            BtnItemRusak.Enabled = False
        End If

        If sValidasi.Text <> "" Then
            showMessage(sValidasi.Text, 2)
            Exit Sub
        End If
    End Sub

    Protected Sub BtnParts_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Bindpartdata("")
    End Sub

    Protected Sub tbsparepartqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbsparepartqty.TextChanged
        tbsparepartqty.Text = ToMaskEdit(tbsparepartqty.Text, 3)
        HitungTotal()
    End Sub

    Protected Sub tbsparepartprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbsparepartprice.TextChanged
        tbsparepartprice.Text = ToMaskEdit(tbsparepartprice.Text, 3)
        HitungTotal()
    End Sub

    Protected Sub BiayaSrv_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BiayaSrv.TextChanged
        BiayaSrv.Text = ToMaskEdit(BiayaSrv.Text, 3)
        HitungNet()
    End Sub

    Protected Sub lkbCloseItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseItem.Click
        btnHideItem.Visible = False : panelItem.Visible = False
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        Try
            UpdateCheckedMat()
            Dim saldoawal As String = GetStrData("Select reqservicecat From QL_TRNREQUEST Where reqoid = " & Integer.Parse(lblreqoid.Text) & "")
            'If DDLTypeTTS.SelectedValue = "REPLACE" Then
            sSql = "Select ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00) - ISNULL((Select SUM(itemqty) QtyFinal From QL_TRNFINALSPART fs INNER JOIN QL_TRNFINAL fm ON fm.FINOID=fs.trnfinaloid Where trnfinaloid NOT IN (Select FINOID from QL_TRNFINAL f Where f.FINSTATUS='REJECTED') AND fs.reqoid=Rq.reqmstoid And reqdtloid=con.conrefoid AND fs.branch_code='" & DdlCabang.SelectedValue & "'),0.00) QtyFinal From QL_TRNREQUESTDTL Rq inner Join QL_mstitem i ON i.itemoid=rq.itemoid Inner Join QL_conmtr con ON con.conrefoid=Rq.reqdtloid AND rq.itemoid=con.refoid AND con.Formname NOT IN ('QL_TRNFINALSPART') Where Rq.reqmstoid=" & Integer.Parse(lblreqoid.Text) & " And con.conrefoid=" & Integer.Parse(reqdtloid.Text) & " Group BY con.conrefoid, Rq.reqmstoid Having(ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyout),0.00))-ISNULL((Select SUM(itemqty) QtyFinal From QL_TRNFINALSPART fs INNER JOIN QL_TRNFINAL fm ON fm.FINOID=fs.trnfinaloid Where trnfinaloid NOT IN (Select FINOID from QL_TRNFINAL f Where f.FINSTATUS='REJECTED') AND fs.reqoid=Rq.reqmstoid And reqdtloid=con.conrefoid AND fs.branch_code='" & DdlCabang.SelectedValue & "'),0.00)>0.00"
            'Else
            '    sSql = "Select ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00)-ISNULL(QtyFinal,0.00) QtyTtp From QL_TRNREQUESTDTL Rq Inner Join QL_mstitem i ON i.itemoid=rq.itemoid Inner Join QL_conmtr con ON con.conrefoid=Rq.reqdtloid AND con.branch_code='" & DdlCabang.SelectedValue & "' AND rq.itemoid=con.refoid AND con.Formname NOT IN ('QL_TRNFINALSPART') Left Join (Select reqdtloid, reqoid, itemreqoid, sum(itemqty) QtyFinal From QL_TRNFINALSPART fs INNER JOIN QL_TRNFINAL fm ON fm.FINOID=fs.trnfinaloid Where (fs.branch_code='" & DdlCabang.SelectedValue & "' OR fm.cabangasal='" & DdlCabang.SelectedValue & "') AND trnfinaloid NOT IN (Select FINOID from QL_TRNFINAL f Where f.FINSTATUS='REJECTED') Group by reqdtloid, reqoid, itemreqoid) fs ON fs.reqoid=rq.reqmstoid AND con.conrefoid=fs.reqdtloid Where con.mtrlocoid IN (SELECT a.genoid From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.genother5='TITIPAN' AND c.gencode=con.branch_code) AND con.branch_code='" & DdlCabang.SelectedValue & "' AND Rq.reqmstoid=" & Integer.Parse(lblreqoid.Text) & " And rq.reqdtloid=" & Integer.Parse(reqdtloid.Text) & " Group by rq.reqdtloid, Rq.reqmstoid, i.itemoid, i.itemcode, i.itemdesc, Rq.reqqty, con.branch_code, Rq.snno, rq.kelengkapan, rq.reqdtljob, rq.typegaransi, i.satuan1, con.mtrlocoid, ISNULL((QtyFinal),0.00) Having(ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyout),0.00))-ISNULL(QtyFinal,0.00)>0.00"
            'End If

            Dim QtySisa As Double = GetScalar(sSql)
            If DDLTypeTTS.SelectedValue = "REPLACE" Then
                If ToDouble(Qty.Text) > ToDouble(QtySisa) Then
                    sValidasi.Text &= "- Maaf, Qty " & lblfinitemname.Text & " melebihi qty penerimaan " & ToMaskEdit(QtySisa, 3) & "..!!!<BR>"
                End If
            End If

            sSql = "SELECT a.genoid From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.genother5='RUSAK' AND c.gencode='" & DdlCabang.SelectedValue & "'"
            Dim LocRusak As Integer = GetScalar(sSql)
            If Session("TblListMat") IsNot Nothing Then
                Dim dt As DataTable = Session("TblListMat")
                If dt.Rows.Count > 0 Then
                    Dim dv As DataView = dt.DefaultView
                    dv.RowFilter = "checkvalue='True' AND itemoid=" & Integer.Parse(ItemOid.Text)

                    If dv.Count > 0 Then
                        Dim sErr As String = ""

                        If DDLTypeTTS.SelectedValue = "REPLACE" Then
                            If ToDouble(dv.Count) > 1 Then
                                sValidasi.Text &= "- Maaf, replace harus satu jenis barang..!!<br>"
                            End If
                        End If

                        For C1 As Integer = 0 To dv.Count - 1
                            If DDLTypeTTS.SelectedValue = "REPLACE" Then
                                If ToDouble(dv(C1)("QtyOs")) <> ToDouble(Qty.Text) Then
                                    sValidasi.Text &= "- Maaf, Quantity replace " & ToMaskEdit(dv(C1)("QtyOs"), 3) & " harus sama dengan qty penerimaan " & ToMaskEdit(Qty.Text, 3) & "..!!<br>"
                                End If
                            End If

                            If ToDouble(dv(C1)("QtyOs")) <= 0.0 Then
                                sValidasi.Text &= "- Maaf, " & dv(C1)("partdescshort") & " belum di input qty..!!<br />"
                            End If

                            If saldoawal <> "Y" Then
                                If ToDouble(dv(C1)("QtyOs")) > ToDouble(dv(C1)("saldoakhir")) Then
                                    sValidasi.Text &= "- Maaf, " & dv(C1)("partdescshort") & " melebihi qty Stok akhir..!!<br />"
                                End If
                            End If
                        Next

                        Dim dtab As DataTable
                        If Session("itemdetail") Is Nothing Then
                            dtab = CrtDtlBarang() : Session("itemdetail") = dtab
                            SeqBarang.Text = 0 + 1
                        Else
                            dtab = Session("itemdetail")
                            SeqBarang.Text = dtab.Rows.Count + 1
                        End If

                        Dim dtView As DataView = dtab.DefaultView
                        If I_Text2.Text = "New Detail" Then
                            dtView.RowFilter = "itemoid= '" & Integer.Parse(ItemOid.Text) & "' AND reqdtloid=" & Integer.Parse(reqdtloid.Text) & ""
                        Else
                            dtView.RowFilter = "itemoid= '" & Integer.Parse(ItemOid.Text) & "' AND seq<>" & SeqBarang.Text
                        End If

                        If dtView.Count > 0 Then
                            If ItemOid.Text <> "" Then
                                sValidasi.Text &= "Maaf, " & lblfinitemname.Text & " Sudah ditambahkan, cek pada tabel..!!!<BR>"
                            End If
                            dtView.RowFilter = ""
                        End If
                        dtView.RowFilter = ""

                        If sValidasi.Text <> "" Then
                            showMessage(sValidasi.Text, 2)
                            Exit Sub
                        End If

                        Dim objRow As DataRow
                        If I_Text2.Text = "New Detail" Then
                            objRow = dtab.NewRow()
                            objRow("seq") = dtab.Rows.Count + 1
                        Else
                            Dim selrow As DataRow() = dtab.Select("seq=" & SeqBarang.Text)
                            objRow = selrow(0)
                            objRow.BeginEdit()
                        End If

                        If DDLTypeTTS.SelectedValue = "REPLACE" Then
                            gvrequest.Columns(13).HeaderText = "Barang Replace"
                        Else
                            gvrequest.Columns(13).HeaderText = "Barang Spare Parts"
                        End If

                        For C1 As Integer = 0 To dv.Count - 1
                            dtView.RowFilter = "PARTOID=" & Integer.Parse(dv(C1)("PARTOID")) & ""
                            Dim drow As DataRow : Dim drowedit() As DataRow
                            If I_Text2.Text = "New Detail" Then
                                drow = dtab.NewRow
                                drow("seq") = SeqBarang.Text
                                drow("reqmstoid") = Integer.Parse(lblreqoid.Text)
                                drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                                drow("itemoid") = Integer.Parse(ItemOid.Text)
                                drow("snno") = SerialNo.Text
                                drow("itemdesc") = lblfinitemname.Text
                                drow("reqqty") = ToMaskEdit(ToDouble(Qty.Text), 3)
                                drow("satuan1") = DdlUnit.SelectedValue
                                drow("Unit") = DdlUnit.SelectedItem.Text
                                drow("Kelengkapan") = Kelengkapan.Text
                                drow("reqdtljob") = Kelengkapan.Text
                                drow("typegaransi") = TypeBarang.Text
                                drow("PARTOID") = Integer.Parse(dv(C1)("PARTOID"))
                                drow("partdescshort") = dv(C1)("partdescshort").ToString
                                drow("PartUnitOid") = Integer.Parse(dv(C1)("PartUnitOid"))
                                drow("partsqty") = ToMaskEdit(ToDouble(dv(C1)("QtyOs")), 3)
                                drow("PartsUnit") = dv(C1)("PartsUnit").ToString
                                drow("mtrloc") = matLoc.SelectedValue

                                If DDLTypeTTS.SelectedValue = "REPLACE" Then
                                    drow("spartprice") = ToMaskEdit(0.0, 3)
                                    drow("totalprice") = ToMaskEdit(0.0, 3)
                                Else
                                    drow("spartprice") = ToMaskEdit(ToDouble(dv(C1)("spartprice")), 3)
                                    drow("totalprice") = ToMaskEdit(ToDouble(dv(C1)("QtyOs")) * ToDouble(dv(C1)("spartprice")), 3)
                                End If
                                dtab.Rows.Add(drow) : dtab.AcceptChanges()
                            Else
                                drowedit = dtab.Select(" PARTOID=" & Integer.Parse(dv(C1)("PARTOID")) & "", "")
                                drow = drowedit(0)
                                drowedit(0).BeginEdit()
                                drow("seq") = SeqBarang.Text
                                drow("reqmstoid") = Integer.Parse(lblreqoid.Text)
                                drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                                drow("itemoid") = Integer.Parse(ItemOid.Text)
                                drow("itemdesc") = lblfinitemname.Text
                                drow("reqqty") = ToMaskEdit(ToDouble(Qty.Text), 3)
                                drow("satuan1") = DdlUnit.SelectedValue
                                drow("Unit") = DdlUnit.SelectedItem.Text
                                drow("Kelengkapan") = Kelengkapan.Text
                                drow("reqdtljob") = Kelengkapan.Text
                                drow("typegaransi") = TypeBarang.Text

                                drow("PARTOID") = Integer.Parse(dv(C1)("PARTOID"))
                                drow("partdescshort") = dv(C1)("partdescshort").ToString
                                drow("PartUnitOid") = Integer.Parse(dv(C1)("PartUnitOid"))
                                drow("partsqty") = ToMaskEdit(ToDouble(dv(C1)("QtyOs")), 3)
                                drow("PartsUnit") = dv(C1)("PartsUnit").ToString
                                drow("mtrloc") = matLoc.SelectedValue

                                If DDLTypeTTS.SelectedValue = "REPLACE" Then
                                    drow("spartprice") = ToMaskEdit(0.0, 3)
                                    drow("totalprice") = ToMaskEdit(0.0, 3)
                                Else
                                    drow("spartprice") = ToMaskEdit(ToDouble(dv(C1)("spartprice")), 3)
                                    drow("totalprice") = ToMaskEdit(ToDouble(dv(C1)("QtyOs")) * ToDouble(dv(C1)("spartprice")), 3)
                                End If

                                drowedit(0).EndEdit()
                                dtab.Select(Nothing, Nothing)
                                dtab.AcceptChanges()
                            End If
                            dtView.RowFilter = "" : SeqBarang.Text += 1
                        Next
                        HitungNet() : dv.RowFilter = ""

                        If DDLTypeTTS.SelectedValue = "SERVICE" Then
                            Dim dl As DataView = dt.DefaultView
                            dl.RowFilter = "checkvalue='True' AND itemoid=" & Integer.Parse(ItemOid.Text)
                            '---- Add to list barang rusak ----
                            Dim dtb As DataTable
                            If Session("itemrusak") Is Nothing Then
                                dtb = CrtDtlRusak() : Session("itemrusak") = dtb
                                SeqBarang.Text = 0 + 1
                            Else
                                dtb = Session("itemrusak")
                                SeqBarang.Text = dtb.Rows.Count + 1
                            End If

                            Dim dvr As DataView = dtb.DefaultView
                            If I_Text2.Text = "New Detail" Then
                                dvr.RowFilter = "itemreqoid= '" & Integer.Parse(ItemOid.Text) & "'"
                            Else
                                dvr.RowFilter = "itemreqoid= '" & Integer.Parse(ItemOid.Text) & "' AND seq<>" & SeqBarang.Text
                            End If

                            For C1 As Integer = 0 To dl.Count - 1
                                dvr.RowFilter = "itempartoid=" & Integer.Parse(dl(C1)("PARTOID")) & ""
                                Dim dr As DataRow : Dim rowedit() As DataRow
                                If I_Text2.Text = "New Detail" Then
                                    dr = dtb.NewRow
                                    dr("seq") = SeqBarang.Text
                                    dr("reqoid") = Integer.Parse(lblreqoid.Text)
                                    dr("reqdtloid") = Integer.Parse(reqdtloid.Text)
                                    dr("trnfinaloid") = Integer.Parse(FinOid.Text)
                                    dr("itemreqoid") = Integer.Parse(ItemOid.Text)
                                    dr("qtyreq") = ToMaskEdit(ToDouble(Qty.Text), 3)
                                    dr("itemrusakoid") = Integer.Parse(dl(C1)("PARTOID"))
                                    dr("itemtitipan") = lblfinitemname.Text
                                    dr("itemdesc") = dl(C1)("partdescshort").ToString
                                    dr("qtyrusak") = ToMaskEdit(dl(C1)("QtyOs"), 3)
                                    dr("itempartoid") = Integer.Parse(dl(C1)("PARTOID"))
                                    dr("qtypart") = ToMaskEdit(dl(C1)("QtyOs"), 3)
                                    dr("locoidrusak") = Integer.Parse(LocRusak)
                                    dr("trnfinalpartoid") = Integer.Parse(0)
                                    dtb.Rows.Add(dr) : dtb.AcceptChanges()
                                Else
                                    rowedit = dtb.Select("itempartoid=" & Integer.Parse(dl(C1)("PARTOID")) & "", "")
                                    dr = rowedit(0)
                                    rowedit(0).BeginEdit()

                                    dr("seq") = SeqBarang.Text
                                    dr("reqoid") = Integer.Parse(lblreqoid.Text)
                                    dr("reqdtloid") = Integer.Parse(reqdtloid.Text)
                                    dr("trnfinaloid") = Integer.Parse(FinOid.Text)
                                    dr("itemreqoid") = Integer.Parse(ItemOid.Text)
                                    dr("qtyreq") = ToMaskEdit(ToDouble(Qty.Text), 3)
                                    dr("itemrusakoid") = Integer.Parse(dl(C1)("PARTOID"))
                                    dr("itemtitipan") = lblfinitemname.Text
                                    dr("itemdesc") = dl(C1)("partdescshort").ToString
                                    dr("qtyrusak") = ToMaskEdit(dv(C1)("QtyOs"), 3)
                                    dr("itempartoid") = Integer.Parse(dl(C1)("PARTOID"))
                                    dr("qtypart") = ToMaskEdit(dl(C1)("QtyOs"), 3)
                                    dr("locoidrusak") = Integer.Parse(LocRusak)
                                    dr("trnfinalpartoid") = Integer.Parse(0)

                                    rowedit(0).EndEdit()
                                    dtb.Select(Nothing, Nothing)
                                    dtb.AcceptChanges()
                                End If
                                dr.EndEdit() : SeqBarang.Text += 1
                                dvr.RowFilter = ""
                            Next
                            GvRusak.DataSource = dtb
                            GvRusak.DataBind() : Session("itemrusak") = dtb

                            dl.RowFilter = ""
                            If dtb.Rows.Count > 0 Then
                                AddToListRusak.Visible = True
                            Else
                                AddToListRusak.Visible = False
                            End If
                        End If

                        If DDLTypeTTS.SelectedValue = "REPLACE" Then
                            GvRusak.Columns(0).Visible = False
                            GvRusak.Columns(7).Visible = False
                        Else
                            GvRusak.Columns(0).Visible = True
                            GvRusak.Columns(7).Visible = True
                        End If

                        gvrequest.DataSource = dtab
                        gvrequest.DataBind() : Session("itemdetail") = dtab

                        DDLTypeTTS.Enabled = False
                        btnHideItem.Visible = False : panelItem.Visible = False
                        mpeItem.Show() : ClearSparts()

                    Else
                        If TypeTTS.Text <> "M" Then
                            If DDLTypeTTS.SelectedValue <> "SERVICE" Then
                                sValidasi.Text &= "- Maaf, anda belum pilih barang..!!<br>"
                            Else
                                BtnAddToListBarang_Click(Nothing, Nothing)
                            End If
                        Else
                            BtnAddToListBarang_Click(Nothing, Nothing)
                        End If
                    End If
                End If
            Else
                sValidasi.Text &= "- Maaf, Silahkan klik add to list dulu..!!<br>"
            End If

            If sValidasi.Text <> "" Then
                showMessage(sValidasi.Text, 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        GVItemList.PageIndex = e.NewPageIndex
        'If UpdateCheckedMat() Then
        UpdateCheckedMat()

        Dim dtTbl As DataTable = Session("TblListMat")
        Dim dtView As DataView = dtTbl.DefaultView
        Dim sFilter As String = ddlpoppartfilter.SelectedValue & " LIKE '%" & Tchar(tbpoppartfilter.Text) & "%'"
        dtView.RowFilter = sFilter
        If dtView.Count > 0 Then
            Session("TblListMatView") = dtView.ToTable
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : dtView.RowFilter = ""
            mpeItem.Show()
        Else
            dtView.RowFilter = ""
            showMessage("Maaf, Data Barang tidak ditemukan..!!", 2)
        End If
        'Else
        'mpeItem.Show()
        'End If
    End Sub

    Protected Sub gvrequest_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvrequest.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(15).Text = ToMaskEdit(ToDouble(e.Row.Cells(15).Text), 3)
            e.Row.Cells(18).Text = ToMaskEdit(ToDouble(e.Row.Cells(18).Text), 3)
            e.Row.Cells(19).Text = ToMaskEdit(ToDouble(e.Row.Cells(19).Text), 3)
        End If
    End Sub

    Protected Sub gvrequest_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvrequest.RowDeleting
        Try
            Dim iIndex As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("itemrusak")
            objTable.Rows.RemoveAt(iIndex)
            'resequence
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C1)
                dr.BeginEdit()
                dr("seq") = C1 + 1
                dr.EndEdit()
            Next

            Session("itemrusak") = objTable
            GvRusak.DataSource = objTable
            GvRusak.DataBind()
            Session("sequence") = objTable.Rows.Count + 1
        Catch ex As Exception
            showMessage(ex.ToString, 2) : Exit Sub
        End Try
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Error Program..!!", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(0).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        gvrequest.DataSource = dtab
        gvrequest.DataBind()
        Session("itemdetail") = dtab
        HitungNet()
    End Sub

    Protected Sub BtnSbarang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSbarang.Click
        BindBarang() : GvBarang.Visible = True
        CBGantiBarang.Checked = False
    End Sub

    Protected Sub GVItemList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
        End If
    End Sub

    Protected Sub BtnSendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSendApp.Click
        lblfinstatus.Text = "IN APPROVAL" : ibsave_Click(sender, e)
    End Sub

    Protected Sub GvRusak_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvRusak.SelectedIndexChanged
        Try
            Dim dt As DataTable = Session("itemrusak")
            Dim dv As DataView = dt.DefaultView
            ItemOidRusak.Text = GvRusak.SelectedDataKey.Item("itemrusakoid")
            dv.RowFilter = "itemrusakoid=" & ItemOidRusak.Text
            If dv.Count > 0 Then
                FinOid.Text = dv(0).Item("trnfinaloid")
                trnfinalpartoid.Text = dv(0).Item("trnfinalpartoid")
                lblreqoid.Text = dv(0).Item("reqoid")
                reqdtloid.Text = dv(0).Item("reqdtloid")
                itempartoid.Text = dv(0).Item("itempartoid")
                ItemOid.Text = dv(0).Item("itemreqoid")
                QtyRusak.Text = ToMaskEdit(dv(0).Item("qtyrusak"), 3)
                qtyreq.Text = ToMaskEdit(dv(0).Item("qtyreq"), 3)
                qtypart.Text = ToMaskEdit(dv(0).Item("qtypart"), 3)
                SeqBarang.Text = dv(0).Item("seq")
                OidLocRusak.Text = dv(0).Item("locoidrusak")
                ItemRusak.Text = dv(0).Item("itemdesc")
                AddToListRusak.Visible = True
            End If
            I_Text3.Text = "Edit"
            dv.RowFilter = ""
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 2)
            Exit Sub
        End Try
        
    End Sub

    Protected Sub BtnItemRusak_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnItemRusak.Click
        BindItemRusak()
    End Sub

    Protected Sub GvItemRusak_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvItemRusak.PageIndexChanging
        GvItemRusak.PageIndex = e.NewPageIndex
        BindItemRusak()
    End Sub

    Protected Sub GvItemRusak_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvItemRusak.SelectedIndexChanged
        ItemOidRusak.Text = GvItemRusak.SelectedDataKey.Item("itemoid")
        ItemRusak.Text = GvItemRusak.SelectedDataKey.Item("itemdesc")
        GvItemRusak.Visible = False
    End Sub

    Protected Sub EraseBtnRusak_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EraseBtnRusak.Click
        ItemRusak.Text = "" : QtyRusak.Text = "0.00"
        GvItemRusak.Visible = False
    End Sub

    Protected Sub AddToListRusak_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles AddToListRusak.Click
        Try
            Dim sErr As String = "", dtab As DataTable, dtView As DataView
            If Session("itemrusak") Is Nothing Then
                dtab = CrtDtlRusak() : Session("itemrusak") = dtab
                SeqBarang.Text = 0 + 1
            Else
                dtab = Session("itemrusak") 
            End If

            dtView = dtab.DefaultView
            If ItemOidRusak.Text = "" Then
                sErr &= "Maaf, tolong pilih katalog dulu..!!!<BR>"
            End If

            If I_Text3.Text = "NEW" Then
                dtView.RowFilter = "itemrusakoid= '" & ItemOidRusak.Text & "'"
            Else
                dtView.RowFilter = "itemrusakoid= '" & ItemOidRusak.Text & "' AND seq<>" & SeqBarang.Text
            End If

            If dtView.Count > 0 Then
                If ItemOidRusak.Text <> "" Then
                    sErr &= "Maaf, " & ItemRusak.Text & " Sudah ditambahkan, cek pada tabel..!!!<BR>"
                End If
                dtView.RowFilter = ""
                Exit Sub
            End If
            dtView.RowFilter = ""

            If sErr <> "" Then
                showMessage(sErr, 2)
                Exit Sub
            End If

            Dim drow As DataRow
            Dim drowedit() As DataRow

            If I_Text3.Text = "NEW" Then
                drow = dtab.NewRow
                drow("seq") = dtab.Rows.Count + 1
                drow("reqoid") = Integer.Parse(lblreqoid.Text)
                drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                drow("trnfinaloid") = Integer.Parse(FinOid.Text)
                drow("itemreqoid") = Integer.Parse(ItemOid.Text)
                drow("qtyreq") = ToMaskEdit(ToDouble(Qty.Text), 3)
                drow("itemrusakoid") = Integer.Parse(ItemOidRusak.Text)
                drow("itemdesc") = ItemRusak.Text.ToString
                drow("qtyrusak") = ToMaskEdit(QtyRusak.Text, 3)
                drow("itempartoid") = Integer.Parse(0)
                drow("qtypart") = ToMaskEdit(QtyRusak.Text, 3)
                drow("locoidrusak") = Integer.Parse(OidLocRusak.Text)
                drow("trnfinalpartoid") = Integer.Parse(0)
                dtab.Rows.Add(drow) : dtab.AcceptChanges()
            Else
                drowedit = dtab.Select("seq =" & SeqBarang.Text & "", "")
                drow = drowedit(0) : drowedit(0).BeginEdit()
                drow("reqoid") = Integer.Parse(lblreqoid.Text)
                drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                drow("trnfinaloid") = Integer.Parse(FinOid.Text)
                drow("itemreqoid") = Integer.Parse(ItemOid.Text)
                drow("qtyreq") = ToMaskEdit(ToDouble(Qty.Text), 3)
                drow("itemrusakoid") = Integer.Parse(ItemOidRusak.Text)
                drow("itemdesc") = ItemRusak.Text.ToString
                drow("qtyrusak") = ToMaskEdit(QtyRusak.Text, 3)
                drow("itempartoid") = Integer.Parse(0)
                drow("qtypart") = ToMaskEdit(QtyRusak.Text, 3)
                drow("locoidrusak") = Integer.Parse(OidLocRusak.Text)
                drow("trnfinalpartoid") = Integer.Parse(0)

                drowedit(0).EndEdit()
                dtab.Select(Nothing, Nothing)
                dtab.AcceptChanges()
            End If

            GvRusak.DataSource = dtab
            GvRusak.DataBind() : Session("itemrusak") = dtab

            SeqBarang.Text = 1
            AddToListRusak.Visible = False
            ItemRusak.Text = "" : QtyRusak.Text = "0.00"
            ItemOidRusak.Text = ""
            I_Text3.Text = "NEW"
            If sValidasi.Text <> "" Then
                showMessage(sValidasi.Text, 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub lbdelrusak_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow
        Dim Seq As String = sender.tooltip
        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("itemrusak") Is Nothing Then
            dtab = Session("itemrusak")
        Else
            showMessage("Error Program..!!", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(Seq) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GvRusak.DataSource = dtab
        GvRusak.DataBind()
        Session("itemrusak") = dtab
    End Sub

    Protected Sub CBGantiBarang_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CBGantiBarang.CheckedChanged
        If CBGantiBarang.Checked = True Then
            Bindpartdata("")
        Else
            Bindpartdata("AND i.itemoid=" & ItemOid.Text & "")
        End If
        panelItem.Visible = True : btnHideItem.Visible = True
        mpeItem.Show()
    End Sub

    Protected Sub getdetailspart(ByVal sender As Object, ByVal e As EventArgs)
        lblspartstate.Text = "edit"
        Dim lbtn As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lbtn.NamingContainer, GridViewRow)
        tbsparepart.Text = ""
        merkspr.Text = ""
        matLoc.SelectedValue = ""
        tbsparepartqty.Text = ""
        tbsparepartprice.Text = ""
        Stock.Visible = True : ss.Visible = True
        saldoakhir.Visible = True : saldoakhir.Text = ""
    End Sub

    Protected Sub deletespart(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbtn As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lbtn.NamingContainer, GridViewRow)
        If Session("delete") Is Nothing Then
            createdel()
        End If
        Dim spartoid As Int32 = 0
        Dim dtab As DataTable = Session("delete")
        Dim drow As DataRow = dtab.NewRow
        drow("spartoid") = spartoid : dtab.Rows.Add(drow)
        Session("delete") = dtab : Dim otab As DataTable = Session("datadetail")
        otab.Rows.RemoveAt(gvr.RowIndex)
        Session("datadetail") = otab

        tbsparepart.Text = ""
        merkspr.Text = "" : tbsparepartqty.Text = ""
        tbsparepartprice.Text = ""
        ibsearchspart.Visible = False : ibclearspart.Visible = True
        lblspartstate.Text = "new" : tbsparepartqty.Text = 0
        tbsparepartprice.Text = ToMaskEdit(0, 3)

        Stock.Visible = False : ss.Visible = False : saldoakhir.Text = ""
    End Sub

#End Region

    Protected Sub LinkTambah_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            reqdtloid.Text = sender.ToolTip
            ItemOid.Text = sender.CommandName
            Dim dt As DataTable = Session("itemdetail")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "reqdtloid= " & reqdtloid.Text & " AND itemoid=" & ItemOid.Text
            If dv.Count > 0 Then
                FinOid.Text = dv(0).Item("finoid")
                lblreqoid.Text = dv(0).Item("reqmstoid")
                ItemOid.Text = dv(0).Item("itemoid")
                qtyreq.Text = ToMaskEdit(dv(0).Item("reqqty"), 3)
                Qty.Text = ToMaskEdit(dv(0).Item("reqqty"), 3)
                AddToListRusak.Visible = True
            End If
            dv.RowFilter = "" : I_Text3.Text = "NEW"
            BindItemRusak()
        Catch ex As Exception
            showMessage(ex.ToString & "<br />", 1)
            Exit Sub
        End Try
       
    End Sub
End Class