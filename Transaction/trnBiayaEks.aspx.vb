﻿Imports System.Data.SqlClient
Imports System.Data
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnBiayaEks
    Inherits System.Web.UI.Page

#Region "Variables"
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim dtStaticItem As DataTable
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Private cKon As New Koneksi
    Private cProc As New ClassProcedure
#End Region

#Region "Procedures"
    Private Sub BindShowCOA(ByVal noRef As String, ByVal CodeCabang As String, ByVal Obj As GridView)
        sSql = "Select a.acctgcode acctgcode, a.acctgdesc, case d.gldbcr when 'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit', glnote from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid where noref ='" & noRef & "' and d.branch_code='" & CodeCabang.Trim & "' and substring(glother1, 19,1) = '0' order by d.gldtloid desc, a.acctgcode"
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "ql_mstacctg")
        Session("MSTcust") = objTable : Obj.DataSource = objTable
        Obj.DataBind()
    End Sub

    Private Sub PrintReport(ByVal Branch As String, ByVal Oid As String)
        Dim sWhere As String = " Where bm.trnbiayaeksoid=" & Oid & " AND bm.branch_code='" & Branch & "'"

        report.Load(Server.MapPath(folderReport & "rptNotaExp.rpt"))
        'End If

        report.SetParameterValue("sWhere", sWhere)
        report.SetParameterValue("companyname", "MULTI SARANA COMPUTER")

        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)
        'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "")
        report.Close() : report.Dispose() : Session("no") = Nothing
    End Sub

    Public Function GetStatus() As Boolean
        If Eval("SisaAmt") = "0.00" Then
            Return False
        ElseIf Eval("approvalstatus") = "In Approval" Or Eval("approvalstatus") = "APPROVED" Then
            Return False
        ElseIf Eval("Status") = "In Process" Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function GetBayar() As Boolean
        If Eval("approvalstatus") = "In Approval" Or Eval("approvalstatus") = "APPROVED" Then
            Return False
        ElseIf Eval("bayaridr") > 0 Then
            Return True
        End If
    End Function

    Public Function GetRetur() As Boolean
        If Eval("approvalstatus") = "APPROVED" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub HitungAmt()
        Dim dtlNya As DataTable = Session("tbldtl")
        Dim amt As Double = 0
        For C1 As Int16 = 0 To dtlNya.Rows.Count - 1
            amt += ToDouble(dtlNya.Rows(C1)("amttrans"))
        Next
        amtekspedisi.Text = ToMaskEdit(ToDouble(amt), 4)
    End Sub

    Private Sub Fill_payflag()
        initDDLcashbank("") ': GenerateReqCode()
    End Sub

    Public Sub initDDLcashbank(ByVal cashbank As String)
        If lblPOST.Text = "POST" Then
            btnSave.Visible = False
        Else
            btnSave.Visible = True
        End If

        If TypeNotaDDL.SelectedValue.ToUpper = "NON EXPEDISI" Then
            cashbank = "VAR_NON_EXP"
        Else
            cashbank = "VAR_NOTA"
        End If

        FillDDLAcctg(cashbankacctgoid, cashbank, ddlcabang.SelectedValue)
        If cashbankacctgoid.Items.Count < 1 Then
            showMessage("Maaf, account COA untuk VAR_NOTA belum di setting silahkan hubungi admin accounting", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 D.curratestoIDRbeli from QL_mstcurrhist D INNER JOIN ql_mstcurr m on M.CMPCODE=D.cmpcode AND m.currencyoid=d.curroid and  m.cmpcode='" & cmpcode & "' and currencyoid=" & iOid & " order by d.currdate desc"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        conn.Close()
    End Sub

    Public Function InvoiceRate(ByVal curroid As Integer) As Double
        Dim curRate As Double
        If Session("paymentOid") > 0 Then
            sSql = "select isnull((case when c.currencyoid=1 then rate2usdvalue else rate2idrvalue end),0) rateValue  from ql_trnjualmst a INNER JOIN ql_trnordermst b on a.orderno = b.orderno left join ql_mstrate2 c on b.rate2oid = c.rate2oid Where a.trnjualmstoid=" & Session("paymentOid") & ""
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            curRate = xCmd.ExecuteScalar
            Return curRate
            conn.Close()
        Else
            curRate = DailyRate()
            Return curRate
        End If
    End Function

    Public Function DailyRate() As Single
        Dim curRate As Single
        sSql = "select top 1 rate2idrvalue from ql_mstrate2 where currencyoid=2 and rate2month = MONTH(GETDATE()) and rate2year = year(getdate()) order by rate2oid desc"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curRate = xCmd.ExecuteScalar
        Return curRate
        conn.Close()

    End Function

    Function WeeklyRate() As Single
        Dim curRate As Single
        sSql = "select top 1 rateidrvalue from ql_mstrate where currencyoid=2 and  convert(varchar,getdate(),101) between  convert(varchar,ratedate,101) and convert(varchar,ratetodate,101) order by rateoid desc"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curRate = ToMaskEdit(xCmd.ExecuteScalar, 2)
        Return curRate
        conn.Close()

    End Function

    Private Sub GenerateReqCode()
        Dim reqcode As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim code As String = ""
            Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & ddlcabang.SelectedValue & "' AND gengroup='CABANG'")
            code = "NT/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnbiayaeksno,4) AS INT)),0) FROM ql_trnbiayaeksmst WHERE trnbiayaeksno LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            reqcode = code & sequence
            biayaeksno.Text = reqcode

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub UncheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("status").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmstEkp.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub gridCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If (e.CommandName = "ConfirmPrint") Then
            Response.Redirect("~/reportform/ql_printBK.aspx?paymentoid=" & Trim(e.CommandArgument.ToString()) & "&printtype=8")
        End If
    End Sub

    Public Sub DataBindEks()
        Try
            If IsDate(toDate(txtPeriode1.Text)) = False Or IsDate(toDate(txtPeriode1.Text)) = False Then
                showMessage("Periode awal salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If IsDate(toDate(txtPeriode2.Text)) = False Or IsDate(toDate(txtPeriode2.Text)) = False Then
                showMessage("Periode akhir salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            sSql = "Select bm.branch_code, trnbiayaeksoid, trnbiayaeksno, ISNULL(cb.cashbankno,'') cashbankno, trnbiayaeksdate, cu.custname, bm.[status], approvalstatus, bm.trnbiayaeksnote, ISNULL(typenota,'') typenota, ISNULL(tf.SisaAmt,0.00) SisaAmt, amtekspedisi AS amtnt, Isnull(bayaridr,0.00) bayaridr, Isnull(cnnt,0.00) cnnt, Isnull(returnt,0.00) returnt, bm.reasonretur From ql_trnbiayaeksmst bm LEFT JOIN QL_trncashbankmst cb ON cb.cashbankoid=(CASE typenota When 'NON EXPEDISI' then 0 else bm.cashbankoid end) And cb.branch_code=bm.branch_code INNER JOIN QL_mstcust cu ON cu.custoid=bm.custoid AND cu.branch_code=bm.branch_code LEFT JOIN (Select branch_code,refoid,SUM(beliidr)-(SUM(bayaridr)+SUM(returnt)+SUM(cnnt)) SisaAmt, SUM(bayaridr) bayaridr, SUM(cnnt) cnnt, SUM(returnt) returnt From ( SELECT con.refoid,con.branch_code, con.custoid, amttransidr beliidr,0.00 bayaridr, 0.00 AS cnnt, 0.00 AS returnt FROM QL_conar con WHERE con.reftype = 'ql_trnbiayaeksmst' AND con.trnartype in ('EXP') Union ALL SELECT con.refoid, con.branch_code, con.custoid, 0.00 beliidr, amtbayaridr bayaridr, 0.00 AS cnnt, 0.00 AS returnt FROM QL_conar con WHERE con.reftype = 'QL_trnpayar' AND con.trnartype in ('PAYAREXP') Union ALL SELECT con.refoid, con.branch_code, con.custoid, 0.00 beliidr, 0.00 bayaridr, amtbayaridr AS cnnt, 0.00 AS returnt FROM QL_conar con WHERE con.reftype = 'ql_creditnote' AND con.trnartype in ('CNNT') Union ALL SELECT con.refoid, con.branch_code, con.custoid, 0.00 beliidr, 0.00 bayaridr, 0.00 AS cnnt, amtbayaridr AS returnt FROM QL_conar con WHERE con.reftype = 'QL_trnpayar' AND con.trnartype in ('RETUREXP')) bm Group by branch_code, custoid, refoid) tf ON tf.refoid=bm.trnbiayaeksoid AND tf.branch_code=bm.branch_code Where " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%' AND trnbiayaeksdate Between '" & CDate(toDate(txtPeriode1.Text)) & "' AND '" & CDate(toDate(txtPeriode2.Text)) & "'"

            If DDLTypeNota.SelectedValue <> "ALL" Then
                sSql &= " AND typenota='" & DDLTypeNota.SelectedValue & "'"
            End If

            If postinge.SelectedValue <> "ALL" Then
                If postinge.SelectedValue = "Approved" Or postinge.SelectedValue = "Rejected" Then
                    sSql &= " AND approvalstatus='" & postinge.SelectedValue & "'"
                Else
                    sSql &= " AND status='" & postinge.SelectedValue & "'"
                End If
            End If

            If fCabang.SelectedValue <> "ALL" Then
                sSql &= " AND bm.branch_code='" & fCabang.SelectedValue & "'"
            End If
            sSql &= " Order By trnbiayaeksdate Desc,branch_code asc"
            Dim objTable As DataTable = cKon.ambiltabel(sSql, "ql_trnbiayaeksmst")
            Session("tbldata") = objTable
            GVmstEkp.DataSource = objTable
            GVmstEkp.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & "<BR><BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Private Sub BindCust(ByVal cmpcode As String, ByVal branch_code As String)
        Dim DimNya As String = ""
        If TypeNotaDDL.SelectedValue.ToUpper = "EXPEDISI" Then
            DimNya = "And custoid in ( select m.trncustoid from ql_trnjualmst m Where m.ekspedisioid Not In (0,3255) and trnjualstatus in ('Approved','Post') And flagexpedisi <> 'POST' And m.ekspedisioid Not In (0,3255))"
        End If

        sSql = "SELECT custOID AS ID,custCODE AS Code, custNAME AS Name,(Select gendesc FROM QL_mstgen cb Where cb.gencode=c.branch_code AND cb.gengroup='CABANG') Cabang FROM QL_MSTcust AS c WHERE CMPCODE = '" & cmpcode & "' and branch_code = '" & ddlcabang.SelectedValue & "' AND " & DDLSuppID.SelectedValue & " LIKE '%" & Tchar(txtFindSuppID.Text) & "%' " & DimNya & " ORDER BY custCODE"
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_MSTcust")
        Session("MSTcust") = objTable : gvSupplier.DataSource = objTable
        gvSupplier.DataBind()
    End Sub

    Protected Sub bindDataSI(ByVal cmpcode As String)
        Try
            sSql = "SELECT * From (" & _
                    " SELECT Distinct top 150 b.branch_code,b.trnjualmstoid trnbelimstoid, b.trncustoid trnsuppoid, s.custname suppname, b.trnjualno trnbelino,trnjualdate trnbelidate,case b.trnjualref when '-' then '' when '' then '' else b.trnjualref end trnjualref, dateadd(day,abs(g.genother1), trnjualdate)jt, b.trnamtjualnettoidr amttrans,c.acctgoid, b.trnjualnote,'EXPEDISI' TypeNya,(Select sj.noExpedisi from ql_trnsjjualmst sj Where sj.trnsjjualmstoid=b.trnsjjualmstoid AND sj.branch_code=b.branch_code) NoExpedisi From ql_trnjualmst b INNER JOIN QL_mstcurr cr On cr.currencyoid=b.currencyoid INNER JOIN ql_mstcust s On b.trncustoid=s.custoid AND b.trncustoid=" & trnsuppoid.Text & " AND b.cmpcode=s.cmpcode INNER JOIN ql_mstgen g On g.genoid=(Case b.trnpaytype When 0 then 34 else b.trnpaytype End) AND g.gengroup = 'PAYTYPE' INNER JOIN ql_conar c On b.trnjualmstoid = c.refoid AND c.reftype='QL_TRNJUALMST' Where b.trnjualno LIKE '%" & Tchar(NoNota.Text.Trim) & "%' AND trnjualstatus IN ('Approved','Post') AND b.cmpcode='" & cmpcode & "' AND b.trnjualno NOT LIKE '%R' AND b.trnjualno NOT IN (SELECT v.refnotarevisi From QL_trnjualmst v Where b.trnjualmstoid=v.trnjualmstoid AND b.branch_code=v.branch_code) AND b.ekspedisioid not in (0,3255) AND b.trnjualmstoid NOT IN (Select be.trnjualmstoid From ql_trnbiayaeksdtl be INNER JOIN ql_trnbiayaeksmst bm ON bm.trnbiayaeksoid=be.trnbiayaeksoid Where be.trnjualmstoid=b.trnjualmstoid AND be.branch_code=b.branch_code AND bm.status IN ('POST','In Process'))" & _
                        " UNION ALL " & _
                        "Select top 150 '" & ddlcabang.SelectedValue & "' branch_code,b.genoid trnbelimstoid, " & trnsuppoid.Text & " trnsuppoid, '" & suppnames.Text & "' suppname, b.gencode trnbelino,getdate() trnbelidate,b.gendesc trnjualref, getdate() jt, 0.0 amttrans,0 acctgoid, ''trnjualnote ,'NON EXPEDISI' TypeNya,'' NoExpedisi From ql_mstgen b Where gengroup='NOTA' AND (b.gencode LIKE '%" & Tchar(NoNota.Text.Trim) & "%' OR gendesc LIKE '%" & Tchar(NoNota.Text.Trim) & "%')" & _
                        ") dt Where dt.amttrans >= 0 And branch_code='" & ddlcabang.SelectedValue & "' AND TypeNya='" & TypeNotaDDL.SelectedValue & "' Order By trnbelimstoid"
            FillGV(gvPurchasing, sSql, "ql_trnbelimst")
            cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, True)
        Catch ex As Exception
            showMessage(ex.ToString & "<br >" & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Protected Sub ClearDtlAP()
        I_U2.Text = "New Detail" : trnbelimstoid.Text = ""
        trnbelino.Text = "" : Payseq.Text = 1 : amttrans.Text = 0
        noteDtl.Text = ""
    End Sub

    Protected Sub ClearDtlAP(ByVal bState As Boolean)
        trnbelino.Text = "" : trnbelimstoid.Text = ""
        noteDtl.Text = "" : Payseq.Text = 1
        I_U2.Text = "New Detail"
        TypeNotaDDL.Enabled = True
    End Sub

    Public Sub CheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("cashbankstatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmstEkp.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub FillTextBoxRetur(ByVal Branch_code As String, ByVal vPayid As Integer)
        Try
            sSql = "Select bm.branch_code,(Select cg.gendesc from QL_mstgen cg Where cg.gencode=bm.branch_code AND cg.gengroup='CABANG') DescCabang,ISNULL(cb.cashbankoid,0) cashbankoid,bm.custoid,trnbiayaeksoid,trnbiayaeksno,ISNULL(cb.cashbankno,'-') cashbankno,trnbiayaeksdate,cu.custname,ac.acctgoid,bm.status,bm.amtekspedisi,bm.trnbiayaeksnote,bm.updtime,bm.upduser,ISNULL(bm.cashbankdtloid,0) cashbankdtloid,ISNULL(typenota,'') typenota,cabangasal,('(' + ac.acctgcode + ') ' + ac.acctgdesc) CoaBiayaDesc,bm.reasonretur From ql_trnbiayaeksmst bm LEFT JOIN QL_trncashbankmst cb ON cb.cashbankoid=(CASE typenota When 'NON EXPEDISI' then 0 else bm.cashbankoid end) And cb.branch_code=bm.cabangasal INNER JOIN QL_mstcust cu ON cu.custoid=bm.custoid AND cu.branch_code=bm.branch_code Inner Join QL_mstacctg ac ON bm.acctgoid=ac.acctgoid Where trnbiayaeksoid =" & vPayid & " AND bm.branch_code='" & Branch_code & "'"
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    LblCabang.Text = xreader("DescCabang").ToString
                    LblExpOid.Text = Integer.Parse(xreader("trnbiayaeksoid"))
                    LblCbOid.Text = Integer.Parse(xreader("cashbankoid"))
                    LblNoCb.Text = Trim(xreader("cashbankno").ToString)
                    LblCustOid.Text = Integer.Parse(xreader("custoid"))
                    LblCustName.Text = xreader("custname").ToString
                    LblNoBiaya.Text = Trim(xreader("trnbiayaeksno").ToString)
                    LblCOA.Text = xreader("CoaBiayaDesc").ToString
                    LblCoaOId.Text = Integer.Parse(xreader("acctgoid"))
                    LblCodeCabang.Text = Trim(xreader("branch_code").ToString)
                    LblAmount.Text = ToMaskEdit(xreader("amtekspedisi"), 3)
                    LblTanggal.Text = Format(xreader("trnbiayaeksdate"), "dd/MM/yyy")
                    LblStatus.Text = Trim(xreader("status").ToString)
                    LblCbDtlOid.Text = Integer.Parse(xreader("cashbankdtloid"))
                    LblCabangAsal.Text = Trim(xreader("cabangasal").ToString)
                    LblTypeNota.Text = xreader("typenota").ToString
                    ReasonRetur.Text = xreader("reasonretur").ToString
                End While
            End If
            conn.Close()
            If LblTypeNota.Text = "NON EXPEDISI" Then
                NoBankLbl.Visible = False : LblNoCb.Visible = False
            Else
                NoBankLbl.Visible = True : LblNoCb.Visible = True
            End If
            '---- Bindata Detail ----         
            sSql = "Select trnbiayaeksdtloid,bd.trnbiayaeksoid,trnjualmstoid trnjualoid,payseq,trnjualno,notedtl,bd.amtekspedisi amttrans,bm.typenota,Case bm.typenota When 'EXPEDISI' Then bd.trnjualno Else (Select dc.gendesc from QL_mstgen dc Where dc.genoid=bd.trnjualmstoid AND gengroup='NOTA') End DescNya From ql_trnbiayaeksdtl bd Inner Join ql_trnbiayaeksmst bm ON bm.trnbiayaeksoid=bd.trnbiayaeksoid Where bm.trnbiayaeksoid='" & vPayid & "' AND bm.branch_code='" & Branch_code & "'"
            Dim dtlTable As DataTable = cKon.ambiltabel(sSql, "ql_trnbiayaeksdtl")
            Session("tbldtl") = dtlTable : GVDtlRet.DataSource = dtlTable
            If TypeNotaDDL.SelectedValue.ToUpper = "EXPEDISI" Then
                GVDtlRet.Columns(4).Visible = False
            Else
                GVDtlRet.Columns(4).Visible = True
            End If
            GVDtlRet.DataBind() : GVDtlRet.Visible = True
            '==== End Bindata Detail =====

            '---- Binddata Show COA ----
            sSql = "Select ROW_NUMBER() OVER (ORDER BY a.acctgcode ASC) AS seq,a.acctgcode,a.acctgoid, d.gldbcr,a.acctgdesc, case d.gldbcr when 'D' then SUM(c.amttrans)-SUM(amtbayar) else 0 end 'Debet', case d.gldbcr when 'C' then SUM(c.amttrans)-SUM(amtbayar) else 0 end 'Kredit', glnote, SUM(c.amttrans)-SUM(amtbayar) From ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid INNER JOIN ql_trnbiayaeksmst bm ON bm.trnbiayaeksno = d.noref INNER JOIN QL_conar c ON c.refoid = bm.trnbiayaeksoid and trnartype IN ('EXP', 'PAYAREXP', 'CNNT') Where noref ='" & LblNoBiaya.Text & "' and d.cmpcode='" & cmpcode & "' GROUP BY a.acctgcode, a.acctgoid, a.acctgdesc, d.gldbcr, d.glamt, d.glnote, d.glmstoid order by d.glmstoid"

            Dim DtCoa As DataTable = cKon.ambiltabel2(sSql, "ql_mstacctg")
            Session("DtCoa") = DtCoa : gvPreview.DataSource = DtCoa
            gvPreview.DataBind() : gvPreview.Visible = True
            '==== End Bindata Show COA =====

        Catch ex As Exception
            showMessage(ex.ToString & "<BR><BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
    End Sub

    Public Sub FillTextBox(ByVal Branch_code As String, ByVal vPayid As Integer)
        Try
            sSql = "Select bm.branch_code,ISNULL(cb.cashbankoid,0) cashbankoid,bm.custoid,trnbiayaeksoid,trnbiayaeksno,ISNULL(cb.cashbankno,'Tanpa No. Bank') cashbankno,trnbiayaeksdate,cu.custname,bm.acctgoid,bm.status,bm.amtekspedisi,bm.trnbiayaeksnote,bm.updtime,bm.upduser,ISNULL(bm.cashbankdtloid,0) cashbankdtloid,ISNULL(typenota,'') typenota,cabangasal From ql_trnbiayaeksmst bm LEFT JOIN QL_trncashbankmst cb ON cb.cashbankoid=(CASE typenota When 'NON EXPEDISI' then 0 else bm.cashbankoid end) And cb.branch_code=bm.cabangasal INNER JOIN QL_mstcust cu ON cu.custoid=bm.custoid AND cu.branch_code=bm.branch_code Where trnbiayaeksoid=" & vPayid & " AND bm.branch_code='" & Branch_code & "'"
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    expOid.Text = Trim(xreader("trnbiayaeksoid"))
                    cashbankoid.Text = Trim(xreader("cashbankoid"))
                    CashBankNo.Text = Trim(xreader("cashbankno").ToString)
                    trnsuppoid.Text = xreader("custoid")
                    suppnames.Text = xreader("custname").ToString
                    biayaeksno.Text = Trim(xreader("trnbiayaeksno").ToString)
                    sSql = "Select acctgoid,('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc from QL_mstacctg a Where acctgoid=" & xreader("acctgoid") & ""
                    FillDDL(cashbankacctgoid, sSql)
                    cashbankacctgoid.SelectedValue = xreader("acctgoid")
                    ddlcabang.SelectedValue = Trim(xreader("branch_code").ToString)
                    NoteExp.Text = Trim(xreader("trnbiayaeksnote").ToString)
                    amtekspedisi.Text = ToMaskEdit(xreader("amtekspedisi"), 3)
                    PaymentDate.Text = Format(xreader("trnbiayaeksdate"), "dd/MM/yyy")
                    txtStatus.Text = Trim(xreader("status").ToString)
                    BankDtlOid.Text = xreader("cashbankdtloid")
                    updUser.Text = xreader("upduser").ToString
                    updTime.Text = xreader("updtime")
                    lblPOST.Text = Trim(xreader("status").ToString)
                    CabangDr.Text = Trim(xreader("cabangasal").ToString)
                    TypeNotaDDL.SelectedValue = xreader("typenota").ToString
                End While
            End If
            conn.Close()

            If TypeNotaDDL.SelectedValue.ToUpper = "NON EXPEDISI" Then
                LblNoBank.Visible = False : amttrans.Visible = True
                amttrans.Enabled = True : amttrans.CssClass = "inpText"
                CashBankNo.Visible = False : BankBtnCari.Visible = False
                LblTotalNota.Visible = True : EraseNoBank.Visible = False
            Else
                If Integer.Parse(cashbankoid.Text) <= 0 Then
                    LblNoBank.Visible = False : amttrans.Visible = True
                    amttrans.Enabled = True : amttrans.CssClass = "inpText"
                    CashBankNo.Visible = False : BankBtnCari.Visible = False
                    LblTotalNota.Visible = True : EraseNoBank.Visible = False
                Else
                    LblNoBank.Visible = True : EraseNoBank.Visible = True
                    CashBankNo.Visible = True : BankBtnCari.Visible = False
                End If
            End If
            TypeNotaDDL.Enabled = False
        Catch ex As Exception
            showMessage(ex.ToString & "<BR><BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try

        sSql = "Select trnbiayaeksdtloid,trnbiayaeksoid,trnjualmstoid trnjualoid,payseq,trnjualno,notedtl,amtekspedisi amttrans from ql_trnbiayaeksdtl Where trnbiayaeksoid='" & vPayid & "' AND branch_code='" & Branch_code & "'"
        Dim dtlTable As DataTable = cKon.ambiltabel(sSql, "ql_trnbiayaeksdtl")
        Session("tbldtl") = dtlTable : GVDtlEkp.DataSource = dtlTable

        If TypeNotaDDL.SelectedValue.ToUpper = "EXPEDISI" Then
            If Integer.Parse(cashbankoid.Text) <= 0 Then
                GVDtlEkp.Columns(3).Visible = True
            Else
                GVDtlEkp.Columns(3).Visible = False
            End If
        Else
            GVDtlEkp.Columns(3).Visible = True
        End If
        GVDtlEkp.DataBind() : GVDtlEkp.Visible = True
        TypeNotaDDL.Enabled = False
    End Sub

    Private Sub initCbg()
        sSql = "Select gencode,gendesc from QL_mstgen Where gengroup='CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlcabang, sSql)
            ddlcabang.Enabled = True : ddlcabang.CssClass = "inpText"
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlcabang, sSql)
                ddlcabang.Enabled = True : ddlcabang.CssClass = "inpText"
            Else
                FillDDL(ddlcabang, sSql)
                ddlcabang.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlcabang, sSql)
            ddlcabang.SelectedValue = "10"
        End If

    End Sub

    Private Sub iniCbg1()
        sSql = "Select gencode,gendesc from QL_mstgen Where gengroup='CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCabang, sSql)
            Else
                FillDDL(fCabang, sSql)
                fCabang.Items.Add(New ListItem("ALL", "ALL"))
                fCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fCabang, sSql)
            fCabang.Items.Add(New ListItem("ALL", "ALL"))
            fCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub BindDataBank()
        Dim sSqli As String = ""
        Try
            If trnsuppoid.Text = "" Then
                showMessage("Maaf, Pilih Customer dulu dengan klik icon Loop pada kolom customer!", CompnyName & " - INFORMASI", 2, "modalMsgBoxOK")
                Exit Sub
            End If

            sSql = "Select genother4 from QL_mstgen Where gengroup='CABANG' And gencode='" & ddlcabang.SelectedValue & "'"

            If GetStrData(sSql) = "YA" Then
                sSqli = "Select '" & ddlcabang.SelectedValue & "' branch_code,0 cashbankgloid,0 cashbankoid," & cashbankacctgoid.SelectedValue & " acctgoid,0.00 cashbankglamtidr,(Select acctgcode from QL_mstacctg Where acctgoid=" & cashbankacctgoid.SelectedValue & ") acctgcode,'" & cashbankacctgoid.SelectedItem.Text & "' acctgdesc,'NO EXPENSE' cashbankno,'' flagexpedisi,'" & ddlcabang.SelectedItem.Text & "' Cabang,'" & ddlcabang.SelectedItem.Text & "' CabangDr,1 NomerNya UNION ALL"
            End If

            sSql = "Select * from (" & sSqli & " SELECT cgl.branch_code,cashbankgloid,cashbankoid,a.acctgoid,cashbankglamtidr,acctgcode,acctgdesc,(SELECT Distinct cashbankno FROM QL_trncashbankmst cbm Where cgl.cashbankoid=cbm.cashbankoid AND cbm.branch_code=cgl.branch_code) cashbankno,ISNULL((SELECT Distinct flagexpedisi FROM QL_trncashbankmst cbm Where cgl.cashbankoid=cbm.cashbankoid AND cbm.branch_code=cgl.branch_code),'') flagexpedisi,(Select DISTINCT gendesc from QL_mstgen gb Where gb.gencode=a.branch_code AND gengroup='CABANG') Cabang,(Select DISTINCT gencode from QL_mstgen gb Where gb.gencode=a.branch_code AND gengroup='CABANG') CabangDr,2 NomerNya FROM QL_cashbankgl cgl INNER JOIN QL_mstacctg a ON a.acctgoid=cgl.acctgoid WHERE cgl.acctgoid=" & cashbankacctgoid.SelectedValue & " AND a.branch_code='" & ddlcabang.SelectedValue & "' AND cgl.cashbankgloid NOT IN (Select cashbankdtloid from ql_trnbiayaeksmst be Where be.cashbankdtloid=cgl.cashbankgloid AND cgl.branch_code=be.branch_code AND be.status IN ('POST','In Process')) " & _
            ") cb Where cashbankno LIKE '%" & Tchar(TxtCbNo.Text.Trim) & "%' Order By NomerNya"

            Dim tbldt As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankmst")
            gvSupplierX.DataSource = tbldt
            gvSupplierX.DataBind()
            If tbldt.Rows.Count > 0 Then
                cProc.SetModalPopUpExtender(hiddenbtn2sX, Panel1X, ModalPopupExtender3sX, True)
            Else
                TxtCbNo.Text = ""
                cProc.SetModalPopUpExtender(hiddenbtn2sX, Panel1X, ModalPopupExtender3sX, True)
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br >" & sSql, CompnyName & " - INFORMASI", 2, "modalMsgBoxOK")
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Functions"
    Private Function SetTabelDetailSlisih() As DataTable
        Dim nuDT As New DataTable
        nuDT.Columns.Add("selisihseq", Type.GetType("System.Int32"))
        nuDT.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("acctgdesc", Type.GetType("System.String"))
        nuDT.Columns.Add("amtdtlselisih", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("dtlnoteselisih", Type.GetType("System.String"))
        Return nuDT
    End Function

    Private Function SetTabelDetail() As DataTable
        Dim nuDT As New DataTable
        nuDT.Columns.Add("payseq", Type.GetType("System.Int32"))
        nuDT.Columns.Add("trnjualoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("trnjualno", Type.GetType("System.String"))
        'nuDT.Columns.Add("amttrans", Type.GetType("System.Double"))
        nuDT.Columns.Add("notedtl", Type.GetType("System.String"))
        'nuDT.Columns.Add("amtekspedisi", Type.GetType("System.Double"))
        Return nuDT
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = GVmstEkp.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchAP")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchAP") = sqlSearch
            Response.Redirect(Page.AppRelativeVirtualPath) '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Biaya Ekspedisi"
        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk HAPUS data ini ?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk POST data ini ?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        I_U.Text = "new"
        '=============================================================
        If Not IsPostBack Then
            iniCbg1()
            ddlcabang_SelectedIndexChanged(Nothing, Nothing)
            biayaeksno.Text = GenerateID("ql_trnbiayaeksmst", cmpcode)
            txtPeriode1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            initCbg() : DataBindEks()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                I_U.Text = "update" ': I_U2.Text = "Update"
                PaymentDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TxtLunas.ActiveTabIndex = 1
                btnSearchSupp.Visible = False : PaymentDate.Enabled = False
                PaymentDate.CssClass = "inpTextDisabled" : btnPayDate.Visible = False
                Fill_payflag()
                FillTextBox(Session("branch_code"), Session("oid"))
                If lblPOST.Text = "POST" Then
                    btnPosting.Visible = False : btnSave.Visible = False
                    btnDelete.Visible = False : ibtn.Visible = False
                Else
                    btnPosting.Visible = True : btnSave.Visible = True
                    btnDelete.Visible = True
                End If
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                txtStatus.Text = "In Process"
                I_U.Text = "new" ': I_U2.Text = "New Detail"
                Session("mstoid2") = GenerateID("QL_trncashbankmst", cmpcode)
                cashbankoid.Text = Session("mstoid2")
                expOid.Text = GenerateID("QL_trnbiayaeksmst", cmpcode)
                btnDelete.Visible = False : btnPrint.Visible = False : lblPOST.Text = ""
                Payseq.Text = 1 : Session("PaySeq") = Payseq.Text
                PaymentDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                updUser.Text = Session("UserID") : updTime.Text = GetServerTime()
                TxtLunas.ActiveTabIndex = 0
                Fill_payflag() ' : GenerateReqCode()
                btnSearchSupp.Visible = True : trnbelimstoid.Visible = False
            End If
        End If

        If lblPOST.Text = "POST" Or lblPOST.Text = "CLOSED" Then
            btnshowCOA.Visible = True : btnPrint.Visible = True
            TypeNotaDDL.Enabled = False : btnPosting.Visible = False
            btnDelete.Visible = False : btnSave.Visible = False
        ElseIf lblPOST.Text = "APPROVED" Then
            btnshowCOA.Visible = True : btnPrint.Visible = True
            TypeNotaDDL.Enabled = False : btnPosting.Visible = False
            btnDelete.Visible = False : btnSave.Visible = False
        ElseIf lblPOST.Text = "Revised" Then
            btnshowCOA.Visible = True : btnPrint.Visible = True
            TypeNotaDDL.Enabled = False : btnPosting.Visible = False
            btnDelete.Visible = False : btnSave.Visible = False
        ElseIf lblPOST.Text = "In Approval" Then
            btnshowCOA.Visible = True : btnPrint.Visible = True
            TypeNotaDDL.Enabled = False : btnPosting.Visible = False
            btnDelete.Visible = False : btnSave.Visible = False
        Else
            btnPrint.Visible = False : btnshowCOA.Visible = False
            TypeNotaDDL.Enabled = True : btnPosting.Visible = True
            btnDelete.Visible = True : btnSave.Visible = True
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Protected Sub btnSearchPurchasing_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPurchasing.Click
        If trnsuppoid.Text.Trim = "" Then
            showMessage("Maaf, Pilih Customer dulu dengan klik icon Loop pada kolom Customer..!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        NoNota.Text = "" : bindDataSI(cmpcode)
    End Sub

    Protected Sub gvPurchasing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        trnbelimstoid.Text = gvPurchasing.SelectedDataKey.Item("trnbelimstoid").ToString()
        trnbelino.Text = gvPurchasing.SelectedDataKey.Item("trnbelino").ToString()
        'amttrans.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amttrans").ToString), 2)
        cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, False)
        cProc.DisposeGridView(gvPurchasing)
    End Sub

    Protected Sub ClosePurc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hiddenbtnpur.Visible = False : Panel2.Visible = False : ModalPopupExtender2.Hide()
        gvPurchasing.DataSource = Nothing : gvPurchasing.DataBind()
    End Sub

    Protected Sub GVDtlEkp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVDtlEkp.RowCommand

    End Sub

    Protected Sub GVDtlEkp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlEkp.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            'e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
        End If
    End Sub

    Protected Sub GVDtlEkp_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlEkp.RowDeleting
        Dim dtlTable As DataTable = Session("tbldtl")
        If lblPOST.Text <> "POST" Then
            Dim idx As Integer = e.RowIndex
            'Dim objTable As DataTable = Session("tbldtl")
            ' CEK apakah induk transaksi (utang) ato bukan
            Dim dvTemp As DataView = dtlTable.DefaultView
            dvTemp.RowFilter = "payseq=" & idx + 1
            ' Get Id Induk 
            Dim idInvoice As Integer = dtlTable.Rows(e.RowIndex).Item("trnjualoid")
            dvTemp.RowFilter = ""

            ' DELETE data bila ada
            For C1 As Integer = dtlTable.Rows.Count - 1 To 0 Step -1
                If dtlTable.Rows(C1).Item("trnjualoid") = idInvoice Then
                    dtlTable.Rows.RemoveAt(C1)
                End If
            Next

            'resequence Detial 
            For C2 As Int16 = 0 To dtlTable.Rows.Count - 1
                Dim dr As DataRow = dtlTable.Rows(C2)
                dr.BeginEdit() : dr("payseq") = C2 + 1 : dr.EndEdit()
            Next

            Session("tbldtl") = dtlTable
            GVDtlEkp.DataSource = Session("tbldtl")
            GVDtlEkp.DataBind()
            Payseq.Text = dtlTable.Rows.Count + 1
            Session("PaySeq") = Payseq.Text
        Else
            e.Cancel = True 'Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Cannot be Deleted!!"))
        End If
    End Sub

    Protected Sub GVDtlEkp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtlEkp.SelectedIndexChanged
        Session("index") = GVDtlEkp.SelectedIndex
        If Session("tbldtl") Is Nothing = False Then
            Payseq.Text = GVDtlEkp.SelectedDataKey("payseq").ToString().Trim
            I_U2.Text = "update"
            'lblUpdNo.Text = GVDtlEkp.SelectedIndex.ToString
            Dim objTable As DataTable
            objTable = Session("tbldtl")
            Dim edRow() As DataRow = objTable.Select("payseq=" & Payseq.Text)
            If edRow.Length > 0 Then
                trnbelino.Text = edRow(0).Item("trnjualno").ToString
                trnbelimstoid.Text = Integer.Parse(edRow(0).Item("trnjualoid"))
                If TypeNotaDDL.SelectedValue.ToUpper = "EXPEDISI" Then
                    If Integer.Parse(cashbankoid.Text) <= 0 Then
                        amttrans.Text = ToMaskEdit(ToDouble(edRow(0).Item("amttrans")), 3)
                    Else
                        amttrans.Text = 0.0
                    End If
                Else
                    amttrans.Text = ToMaskEdit(ToDouble(edRow(0).Item("amttrans")), 3)
                End If
                noteDtl.Text = edRow(0).Item("notedtl").ToString
            Else
                showMessage("tidak dapat load detail !!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            End If
            I_U2.Text = "update"
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim fTgl As String = "AND cashbankdate>='" & CDate(toDate(txtPeriode1.Text)) & "' and cashbankdate<='" & CDate(toDate(txtPeriode2.Text)) & "'"
        GVmstEkp.PageIndex = 0 : DataBindEks()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        txtPeriode1.Text = Format(GetServerTime.AddDays(-1), "01/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
        postinge.SelectedIndex = 0 : ddlFilter.SelectedIndex = 0
        If Session("UserLevel") = 2 Then
            If Session("branch_id") = "10" Then
                fCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            fCabang.SelectedValue = "ALL"
        End If
        DDLTypeNota.SelectedValue = "ALL"
        FilterText.Text = "" : DataBindEks()
    End Sub

    Protected Sub btnClearPurchase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP(True)
    End Sub

    Protected Sub LBPost_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnPosting_Click(sender, e)
    End Sub

    Protected Sub GVmstEkp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstEkp.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 4)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 4)
            e.Row.Cells(8).Text = ToMaskEdit(e.Row.Cells(8).Text, 4)
        End If
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
        If lblState.Text = "INV" Then
            lblState.Text = "" : ModalPopupExtender2.Show()
        End If
    End Sub

    Protected Sub imbViewAllInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewAllInv.Click
        NoNota.Text = ""
        bindDataSI(cmpcode)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("oid") = Nothing : Session("tbldtl") = Nothing
        Response.Redirect("~\Transaction\trnBiayaEks.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim xcmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        xcmd.Connection = objConn
        xcmd.Transaction = objTrans
        Try

            sSql = "Delete from ql_trnbiayaeksmst Where cmpcode='" & cmpcode & "' AND trnbiayaeksoid=" & expOid.Text
            xcmd.CommandText = sSql : xcmd.ExecuteNonQuery()

            sSql = "Delete from ql_trnbiayaeksdtl Where cmpcode='" & cmpcode & "' AND trnbiayaeksoid=" & expOid.Text
            xcmd.CommandText = sSql : xcmd.ExecuteNonQuery()
            objTrans.Commit()
            xcmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xcmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
        Session("oid") = Nothing : Session("tbldtl") = Nothing
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtStatus.Text = "POST" : btnSave_Click(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = "" : Dim mVar As String = ""
        Dim VArNya As String = "" : Dim LblExp As String = ""

        'If TypeNotaDDL.SelectedValue = "EXPEDISI" Then
        '    If CashBankNo.Text.Trim = "" Then
        '        sMsg &= "Maaf, anda belum memilih no bank, silahkan pilih nomer bank, <br>" & _
        '       "jika data bank tidak ada silahkan input biaya expedisi di form expense atau hubungi kasir untuk input biaya expedisi<br/ >"
        '    End If
        '    If suppnames.Text.Trim = "" Then
        '        sMsg &= "Maaf, anda belum memilih customer..!!<br/ >"
        '    End If
        'Else
        '    BankDtlOid.Text = 0 : cashbankoid.Text = 0
        '    CabangDr.Text = 0
        'End If

        If ToDouble(amtekspedisi.Text) <= 0 Then
            sMsg &= "Total transaksi Nota Ekspedisi harus lebih besar dari 0 !!<br/ >"
        End If

        If Session("tbldtl") Is Nothing Then
            sMsg &= "Maaf, Anda belum menigisi data detail..!!<br/ >"
        End If

        If txtStatus.Text = "POST" Then
            If TypeNotaDDL.SelectedValue.ToUpper = "NON EXPEDISI" Then
                VArNya = "VAR_PIUTANG_LAIN" ': LblExp = "NON EXP"
            Else
                VArNya = "VAR_PIUTANG_EXP" ': LblExp = "EXP"
            End If

            sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & VArNya & "' AND interfaceres1='" & ddlcabang.SelectedValue & "'"
            mVar = GetStrData(sSql)

            If mVar = "" Or mVar = "0" Then
                sMsg &= "Maaf, Interface " & VArNya & " belum di setting, Silahkan hubungi admin accounting untuk seting interface..!!<br />"
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM ql_trnbiayaeksmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf, Data sudah ter input, Klik tombol cancel dan mohon untuk cek data pada tab List form..!!<br>"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "Select [status] from ql_trnbiayaeksmst Where trnbiayaeksoid=" & Integer.Parse(Session("oid")) & ""

            Dim srest As String = GetStrData(sSql)
            If srest Is Nothing Or srest = "" Then
                sMsg &= "Maaf, Data tidak ditemukan..!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
            Else
                If srest.ToLower = "post" Then
                    sMsg &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
                End If
            End If
        End If

        If sMsg <> "" Then
            txtStatus.Text = "In Process"
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        If Session("new") = True Then
            Session("oid") = Nothing
        End If

        Dim reqOid As Integer

        If Session("oid") = Nothing Or Session("oid") = "" Then
            expOid.Text = GenerateID("ql_trnbiayaeksmst", cmpcode)
        Else
            expOid.Text = Session("oid")
        End If

        If txtStatus.Text = "POST" Then
            '-- generated Ulang Nomer Transaksi --
            GenerateReqCode()
            '-------------------------------------
        Else
            biayaeksno.Text = expOid.Text
        End If

        Dim Dtloid As Integer = 0 'GenerateID("ql_trnbiayaeksdtl", cmpcode)
        Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", cmpcode)
        Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", cmpcode)
        Dim PeriodAcctg As String = GetPeriodAcctg(Format(GetServerTime(), "MM/dd/yyy"))
        Dim iSeq As Integer = 1

        If TypeNotaDDL.SelectedValue.ToUpper <> "EXPEDISI" Then
            BankDtlOid.Text = 0 : cashbankoid.Text = 0
            CabangDr.Text = 0
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If Session("oid") = Nothing Or Session("oid") = "" Then

                sSql = "SELECT Isnull(MAX(trnbiayaeksoid),0)+1 From ql_trnbiayaeksmst Where cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : expOid.Text = xCmd.ExecuteScalar()

                sSql = "INSERT into QL_trnbiayaeksmst (cmpcode,trnbiayaeksoid,branch_code,trnbiayaeksdate,trnbiayaeksno,custoid,trnbiayaeksnote,status,amtekspedisi,createuser,createtime,upduser,updtime,cashbankoid,cashbankdtloid,acctgoid,typenota,cabangasal) " & _
                   " VALUES ('" & cmpcode & "'," & expOid.Text & ",'" & ddlcabang.SelectedValue & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & Tchar(biayaeksno.Text) & "'," & trnsuppoid.Text & ",'" & Tchar(NoteExp.Text) & "','" & txtStatus.Text & "','" & ToDouble(amtekspedisi.Text) & "','" & Session("UserID") & "','" & CDate(toDate(createtime.Text)) & "','" & Session("UserID") & "',current_timestamp," & cashbankoid.Text & "," & BankDtlOid.Text & "," & cashbankacctgoid.SelectedValue & ",'" & TypeNotaDDL.SelectedValue.ToUpper & "','" & CabangDr.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid dari QL_mstoid table QL_trncashbankmst
                sSql = "update QL_mstoid set lastoid=" & expOid.Text & " Where tablename ='ql_trnbiayaeksmst' And cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else

                sSql = "update ql_trnbiayaeksmst set branch_code='" & ddlcabang.SelectedValue & "',status='" & txtStatus.Text & "', custoid=" & trnsuppoid.Text & ", updtime=current_timestamp, trnbiayaeksnote='" & Tchar(NoteExp.Text) & "',amtekspedisi=" & ToDouble(amtekspedisi.Text) & ",cashbankoid=" & cashbankoid.Text & ",cashbankdtloid=" & BankDtlOid.Text & ",typenota='" & TypeNotaDDL.SelectedValue.ToUpper & "',cabangasal='" & CabangDr.Text & "',acctgoid=" & cashbankacctgoid.SelectedValue & ",trnbiayaeksno='" & Tchar(biayaeksno.Text) & "', trnbiayaeksdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Where trnbiayaeksoid='" & Integer.Parse(Session("oid")) & "' And cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If Not Session("tbldtl") Is Nothing Then
                Dim objTable As DataTable ': trnbiayaeksdtloid.Text = oid
                objTable = Session("tbldtl")
                If objTable.Rows.Count <> 0 Then
                    sSql = "delete From ql_trnbiayaeksdtl Where trnbiayaeksoid=" & Integer.Parse(expOid.Text) & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                For C1 As Integer = 0 To objTable.Rows.Count - 1
                    sSql = "select ISNULL(lastoid,0) From QL_mstoid Where tablename = 'ql_trnbiayaeksdtl' and cmpcode='" & cmpcode & "'"
                    xCmd.CommandText = sSql : Dtloid = xCmd.ExecuteScalar()

                    sSql = "insert into ql_trnbiayaeksdtl (cmpcode, branch_code, trnbiayaeksdtloid, trnbiayaeksoid, amtekspedisi, trnjualno,trnjualmstoid,payseq,notedtl) values" & _
                    " ('" & cmpcode & "', '" & ddlcabang.SelectedValue & "', " & Dtloid + 1 & ", " & expOid.Text & "," & ToDouble(objTable.Rows(C1).Item("amttrans")) & ",'" & objTable.Rows(C1).Item("trnjualno") & "'," & objTable.Rows(C1).Item("trnjualoid") & "," & objTable.Rows(C1).Item("payseq") & ",'" & Tchar(objTable.Rows(C1).Item("notedtl")) & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "update QL_mstoid set lastoid=" & Dtloid + 1 & " Where tablename='ql_trnbiayaeksdtl' and cmpcode='" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            If txtStatus.Text.ToUpper = "POST" Then
                '--------- Pengkondisian ------------
                '====================================
                If TypeNotaDDL.SelectedValue.ToUpper = "EXPEDISI" Then
                    sSql = "UPdate ql_trncashbankmst Set flagexpedisi='POST' Where cashbankoid=" & cashbankoid.Text & " AND branch_code='" & CabangDr.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    Dim objTbl As DataTable = Session("tbldtl")
                    For C1 As Integer = 0 To objTbl.Rows.Count - 1
                        sSql = "Update ql_trnjualmst Set flagexpedisi='POST' Where trnjualmstoid=" & objTbl.Rows(C1).Item("trnjualoid") & " AND branch_code='" & ddlcabang.SelectedValue & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                End If

                '=========== Insert Auto Jurnal ===========
                '------------------------------------------
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid,branch_code, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type) " & _
                    " VALUES ('" & cmpcode & "', " & iGlMstOid & ",'" & ddlcabang.SelectedValue & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & PeriodAcctg & "','NT|No=" & Tchar(biayaeksno.Text) & "', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'NT')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' Insert Into GL Dtl 
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid,branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1,glpostdate) VALUES " & _
                " ('" & cmpcode & "', " & iGlDtlOid & ",'" & ddlcabang.SelectedValue & "', " & iSeq & ", " & iGlMstOid & ", " & cashbankacctgoid.SelectedValue & ", 'C', " & ToDouble(amtekspedisi.Text) & ", '" & Tchar(biayaeksno.Text) & "', '" & Tchar(NoteExp.Text) & "', 'POST', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(amtekspedisi.Text) & ", " & ToDouble(amtekspedisi.Text) & ", 'ql_trnbiayaeksmst " & reqOid & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' Uang Muka

                sSql = "Select acctgoid from ql_mstacctg Where acctgcode='" & mVar & "'"
                xCmd.CommandText = sSql : Dim acctgoid As Integer = xCmd.ExecuteScalar()
                iGlDtlOid += 1 : iSeq += 1

                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid,branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1,glpostdate) VALUES " & _
                " ('" & cmpcode & "', " & iGlDtlOid & ",'" & ddlcabang.SelectedValue & "', " & iSeq & ", " & iGlMstOid & ", " & acctgoid & ", 'D', " & ToDouble(amtekspedisi.Text) & ", '" & biayaeksno.Text & "', '" & Tchar(NoteExp.Text) & "', 'POST', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(amtekspedisi.Text) & ", " & ToDouble(amtekspedisi.Text) & ", 'ql_trnbiayaeksmst " & reqOid & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' =============== QL_conar ===============

                sSql = "Select ISNULL(MAX(conaroid),0)+1 From QL_conar Where cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : Dim ConAROid As Integer = xCmd.ExecuteScalar()

                sSql = "INSERT INTO QL_conar (cmpcode,branch_code,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnarflag,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amttransidr,amttransusd,amtbayar,trnarnote, trnarres1,upduser,updtime) VALUES " & _
                "('" & cmpcode & "','" & ddlcabang.SelectedValue & "'," & ConAROid & ",'QL_trnbiayaeksmst'," & expOid.Text & ",0," & trnsuppoid.Text & ",'" & acctgoid & "','POST','','EXP',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & PeriodAcctg & "',0,'1/1/1900','',0,'" & CDate(toDate(PaymentDate.Text)) & "'," & ToDouble(amtekspedisi.Text) & "," & ToDouble(amtekspedisi.Text) & ",0,0,'NOTA(NO=" & biayaeksno.Text & ")','','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & ConAROid & " Where tablename ='QL_conar' and cmpcode = '" & cmpcode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close() : txtStatus.Text = "In Process"
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 2, "modalMsgBox")
            objTrans.Dispose()
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnBiayaEks.aspx?awal=true")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport(ddlcabang.SelectedValue, Session("oid"))
    End Sub

    Protected Sub GVmstEkp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnBiayaEks.aspx?branch_code=" & GVmstEkp.SelectedDataKey("branch_code").ToString & "&oid=" & GVmstEkp.SelectedDataKey("trnbiayaeksoid").ToString & "")
    End Sub

    Private Sub GenerateDefaultNo(ByVal sFlag As String, ByVal iAcctgOid As Integer, ByVal dDate As Date)
        ' BKK/10FKBE0001
        Dim sCBType As String
        Select Case sFlag
            Case "CASH" : sCBType = "BKM"
            Case "BANK" : sCBType = "BBM"
            Case "GIRO" : sCBType = "BGM"
            Case "CREDIT CARD" : sCBType = "BCM"
            Case "DP" : sCBType = "BDM"
            Case Else : sCBType = "BLM"
        End Select
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & ddlcabang.SelectedValue & "' and gengroup='CABANG'")
        Dim sNo As String = sCBType & "/" & cabang & "/" & Format(dDate, "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1  FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%' and branch_code='" & ddlcabang.SelectedValue & "'"
        biayaeksno.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), 4)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        BindCust(cmpcode, ddlcabang.SelectedValue)
        hiddenbtn2.Visible = True : Panel1.Visible = True
        MPE3.Show()
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        trnsuppoid.Text = "" : ClearDtlAP()
        Session("tbldtl") = Nothing : GVDtlEkp.DataSource = Nothing
        GVDtlEkp.DataBind()
    End Sub

    Protected Sub ibtnSuppID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnSuppID.Click
        MPE3.Show()
        BindCust(cmpcode, ddlcabang.SelectedValue)
    End Sub

    Protected Sub imbViewAlls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewAlls.Click
        txtFindSuppID.Text = ""
        BindCust(cmpcode, ddlcabang.SelectedValue)
        MPE3.Show()
    End Sub

    Protected Sub CloseSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CloseSupp.Click
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, MPE3, False)
        cProc.DisposeGridView(gvSupplier)
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        ClearDtlAP()
        Session("tbldtl") = Nothing : GVDtlEkp.DataSource = Nothing
        GVDtlEkp.DataBind()
        trnsuppoid.Text = gvSupplier.SelectedDataKey.Item("ID")
        suppnames.Text = gvSupplier.SelectedDataKey.Item("Name").ToString
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, MPE3, False)
        cProc.DisposeGridView(gvSupplier)
        Fill_payflag()
    End Sub

    Private Sub FillDPAccount(ByVal iDPOid As Integer)
        sSql = "SELECT trndpaRacctgoid acctgoid FROM QL_trndpaR WHERE cmpcode='" & cmpcode & "' AND trndpaRoid='" & iDPOid & "'"
        cashbankacctgoid.SelectedValue = CStr(cKon.ambilscalar(sSql))
    End Sub

    Private Sub FillDPBalance(ByVal iDPOid As Integer)
        sSql = "SELECT ISNULL(dp.trndpaRamt-dp.trndpaRacumamt,0) "
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= "+ISNULL((SELECT SUM(pap.payamt) FROM QL_trnpayaR pap " & _
                "WHERE pap.paybankoid=dp.trndpaRoid AND pap.cashbankoid='" & Session("oid") & "'),0) "
        End If
        sSql &= "FROM QL_trndpaR dp WHERE cmpcode='" & cmpcode & "' AND trndpaRoid='" & iDPOid & "'"
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        BindShowCOA(biayaeksno.Text, ddlcabang.SelectedValue, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = NewMaskEdit(e.Row.Cells(3).Text)
            e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
        End If
    End Sub

    Protected Sub GVmstEkp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVmstEkp.PageIndex = e.NewPageIndex
        DataBindEks()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False
        mpeMsgbox.Hide()
        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\transaction\trnBiayaEks.aspx?awal=true")
        ElseIf lblMessage.Text = "- Maaf, Kolom reason mohon untuk di isi..!!<br />" Then
            cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
        End If
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, False)
    End Sub

    Protected Sub ibtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtn.Click
        Dim sMsg As String = ""
        TypeNotaDDL.Enabled = False
        If trnbelimstoid.Text = "" Or ToDouble(trnbelimstoid.Text) = 0 Then
            sMsg &= "- Pilih No Nota dahulu!!<BR>"
        End If

        If TypeNotaDDL.SelectedValue.ToUpper = "EXPEDISI" Then
            If Integer.Parse(cashbankoid.Text) <= 0 Then
                If amttrans.Text = 0 Or amttrans.Text = "" Then
                    sMsg &= "- Maaf, Amt detail ekpedisi tidak boleh nol. Silahkan input amount ekspedisi..!!<BR>"
                End If
            Else
                If amtekspedisi.Text = 0 Or amtekspedisi.Text = "" Then
                    sMsg &= "- Maaf, Amt Ekpedisi tidak boleh nol. Silahkan input amount ekspedisi..!!<BR>"
                End If
            End If
        Else
            If amttrans.Text = 0 Or amttrans.Text = "" Then
                sMsg &= "- Maaf, Amount tidak boleh nol. Silahkan input amount ekspedisi..!!<BR>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        If Session("tbldtl") Is Nothing Then
            Dim nuDT As New DataTable
            nuDT.Columns.Add("payseq", Type.GetType("System.Int32"))
            nuDT.Columns.Add("trnjualoid", Type.GetType("System.Int32"))
            nuDT.Columns.Add("trnjualno", Type.GetType("System.String"))
            nuDT.Columns.Add("notedtl", Type.GetType("System.String"))
            nuDT.Columns.Add("amttrans", Type.GetType("System.Decimal"))
            Session("tbldtl") = nuDT
        End If

        Dim objTable As DataTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        If I_U2.Text = "New Detail" Then
            dv.RowFilter = "trnjualoid='" & Trim(trnbelimstoid.Text) & "' AND trnjualno='" & trnbelino.Text & "'"
        Else
            dv.RowFilter = "trnjualoid='" & Trim(trnbelimstoid.Text) & "' AND payseq <> " & Payseq.Text
        End If
        If dv.Count > 0 Then
            showMessage("Data sudah ditambahkan sebelumnya !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            dv.RowFilter = ""
            GVDtlEkp.DataSource = Session("tbldtl")
            GVDtlEkp.DataBind()
            Exit Sub
        End If
        dv.RowFilter = ""
        ' DELETE data bila ada
        For C1 As Integer = objTable.Rows.Count - 1 To 0 Step -1
            If objTable.Rows(C1).Item("trnjualoid") = trnbelimstoid.Text Then
                objTable.Rows.RemoveAt(C1)
            End If
        Next

        Dim oRow As DataRow
        oRow = objTable.NewRow
        oRow("payseq") = objTable.Rows.Count + 1
        oRow("trnjualoid") = trnbelimstoid.Text
        oRow("trnjualno") = trnbelino.Text
        oRow("amttrans") = amttrans.Text
        oRow("notedtl") = Tchar(noteDtl.Text) 
        objTable.Rows.Add(oRow)

        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            objTable.Rows(C2)("payseq") = C2 + 1
        Next
        Session("trnjualoid") = trnbelimstoid.Text
        Session("tbldtl") = objTable

        If TypeNotaDDL.SelectedValue.ToUpper = "EXPEDISI" Then
            If Integer.Parse(cashbankoid.Text) <= 0 Then
                GVDtlEkp.Columns(3).Visible = True
            Else
                GVDtlEkp.Columns(3).Visible = False
            End If
        Else
            GVDtlEkp.Columns(3).Visible = True
        End If
        GVDtlEkp.DataSource = Session("tbldtl")
        GVDtlEkp.DataBind()

        If TypeNotaDDL.SelectedValue.ToUpper = "NON EXPEDISI" Then
            HitungAmt()
        Else
            If Integer.Parse(cashbankoid.Text) <= 0 Then
                HitungAmt()
            End If
        End If

        GVDtlEkp.Visible = True
        ClearDtlAP() : GVDtlEkp.SelectedIndex = -1
    End Sub

    Protected Sub gvSupplierX_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvSupplierX.SelectedIndexChanged
        cashbankoid.Text = Integer.Parse(gvSupplierX.SelectedDataKey("cashbankoid"))
        BankDtlOid.Text = Integer.Parse(gvSupplierX.SelectedDataKey("cashbankgloid"))
        CashBankNo.Text = gvSupplierX.SelectedDataKey("cashbankno").ToString
        amtekspedisi.Text = ToMaskEdit(gvSupplierX.SelectedDataKey("cashbankglamtidr").ToString, 3)
        CabangDr.Text = gvSupplierX.SelectedDataKey("branch_code").ToString

        If Integer.Parse(cashbankoid.Text) <= 0 Then
            LblNoBank.Visible = True : CashBankNo.Visible = True
            BankBtnCari.Visible = True : amttrans.Visible = True
            LblTotalNota.Visible = True : amttrans.Enabled = True
            amttrans.CssClass = "inpText"
        Else
            LblNoBank.Visible = True : CashBankNo.Visible = True
            BankBtnCari.Visible = True : amttrans.Visible = False
            LblTotalNota.Visible = False
        End If
        cProc.SetModalPopUpExtender(hiddenbtn2sX, Panel1X, ModalPopupExtender3sX, False)
    End Sub

    Protected Sub CloseSuppX_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseSuppX.Click
        cProc.SetModalPopUpExtender(hiddenbtn2sX, Panel1X, ModalPopupExtender3sX, False)
    End Sub

    Protected Sub gvSupplierX_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSupplierX.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
        End If
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvSupplier.PageIndex = e.NewPageIndex
        BindCust(cmpcode, ddlcabang.SelectedValue)
        hiddenbtn2.Visible = True : Panel1.Visible = True
        MPE3.Show()
    End Sub

    Protected Sub ddlcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Fill_payflag()
    End Sub

    Protected Sub iBtBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtBank.Click
        BindDataBank()
    End Sub

    Protected Sub BankBtnCari_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BankBtnCari.Click
        BindDataBank()
    End Sub

    Protected Sub gvSupplierX_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvSupplierX.PageIndex = e.NewPageIndex
        BindDataBank()
    End Sub

    Protected Sub TypeNotaDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim var_nota As String = ""
        If TypeNotaDDL.SelectedValue.ToUpper = "NON EXPEDISI" Then
            LblNoBank.Visible = False : CashBankNo.Visible = False
            BankBtnCari.Visible = False : amttrans.Visible = True
            LblTotalNota.Visible = True : amttrans.Enabled = True
            amttrans.CssClass = "inpText" : EraseNoBank.Visible = False
        Else
            LblNoBank.Visible = True : CashBankNo.Visible = True
            BankBtnCari.Visible = True : amttrans.Visible = False
            LblTotalNota.Visible = False : EraseNoBank.Visible = True
        End If
        initDDLcashbank(TypeNotaDDL.SelectedValue)

        trnbelimstoid.Text = 0 : trnbelino.Text = ""
        cashbankoid.Text = 0 : BankDtlOid.Text = 0
        CashBankNo.Text = "" : amtekspedisi.Text = ToMaskEdit(0.0, 3)
    End Sub

    Protected Sub ViewAllBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ViewAllBank.Click
        BindDataBank()
    End Sub

    Protected Sub imbFindInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataSI(cmpcode)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDtlAP(True)
    End Sub

    Protected Sub BtnRetur_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LblValids.Text = ""
        Dim OidBiaya As Integer = sender.ToolTip
        Dim cBang As String = sender.CommandArgument
        Try
            FillTextBoxRetur(cBang, OidBiaya)
            sSql = "Select (Select trnbiayaeksno from ql_trnbiayaeksmst sb Where sb.trnbiayaeksoid=bm.refoid) NomerNT,SUM(beliidr) AS beliidr, SUM(beliidr)-Isnull(SUM(bayaridr),0.00) SisaAmt, SUM(bayaridr) bayaridr From (SELECT con.refoid,con.branch_code, con.custoid, amttransidr beliidr, 0.00 bayaridr FROM QL_conar con WHERE con.reftype = 'ql_trnbiayaeksmst' AND con.trnartype in ('EXP') Union ALL SELECT con.refoid,con.branch_code, con.custoid, 0.00 beliidr, amtbayaridr bayaridr FROM QL_conar con WHERE con.reftype = 'QL_trnpayar' AND con.trnartype in ('PAYAREXP','CNNT') Union ALL SELECT con.refoid,con.branch_code, con.custoid,0.00 beliidr, amtbayaridr bayaridr FROM QL_conar con WHERE con.reftype = 'QL_trnpayar' AND con.trnartype in ('RETUREXP')) bm Where refoid=" & OidBiaya & " Group by branch_code,custoid,refoid"
            Dim ObjNya As DataTable = cKon.ambiltabel(sSql, "QL_CekData")
            cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
        Catch ex As Exception
            showMessage(ex.ToString & "<BR><BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Protected Sub lkbClosePreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbClosePreview.Click
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, False)
    End Sub

    Protected Sub gvPreview_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPreview.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
        End If
    End Sub

    Protected Sub lkbPostPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPostPreview.Click
        Dim sMsg As String = ""

        If ReasonRetur.Text.Trim = "" Then
            sMsg &= "- Maaf, Kolom reason mohon untuk di isi..!!<br />"
        End If

        sSql = "SELECT count(*) from QL_approvalstructure WHERE tablename = 'ql_trnbiayaeksmst' AND approvaltype = 'FINAL' AND Branch_code like '%" & LblCodeCabang.Text & "%'"
        Dim XApproval As Integer = GetScalar(sSql)
        If XApproval = 0 Then
            sMsg &= "- Maaf, Approval User tidak ditemukan, silahkan hubungi administrator..!!<br />"
        End If

        If checkApproval("ql_trnbiayaeksmst", "In Approval", LblExpOid.Text, "New", "FINAL", LblCodeCabang.Text) > 0 Then
            sMsg &= "- Maaf, Data ini sudah di send for Approval..!!<br/>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trnbiayaeksmst' And branch_code Like '%" & LblCodeCabang.Text & "%' order by approvallevel"
        Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")

        If dtData2.Rows.Count > 0 Then
            Session("TblApproval") = dtData2
        Else
            showMessage("- Maaf, Approval User tidak ditemukan, silahkan hubungi administrator..!!<br />", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "update ql_trnbiayaeksmst set approvalstatus='In Approval', updtime=current_timestamp,reasonretur='" & Tchar(ReasonRetur.Text) & "' ,upduser='" & Session("UserID") & "',returdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Where trnbiayaeksoid='" & Integer.Parse(LblExpOid.Text) & "' And cmpcode='" & cmpcode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Session("AppOid") = GenerateID("QL_Approval", cmpcode)

            If Not Session("TblApproval") Is Nothing Then
                Dim objTable As DataTable : objTable = Session("TblApproval")
                For c1 As Int16 = 0 To objTable.Rows.Count - 1
                    sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus,branch_code) " & _
                        " VALUES ('" & cmpcode & "'," & Session("AppOid") + c1 & ",'" & "NT" & LblExpOid.Text & "_" & Session("AppOid") + c1 & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','ql_trnbiayaeksmst','" & LblExpOid.Text & "','In Approval','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1','" & objTable.Rows(c1).Item("approvalstatus") & "','" & LblCodeCabang.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next

                sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + _
                     objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : xCmd.Connection.Close()

        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 2, "ModalMsgBox")
            objTrans.Dispose()
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnBiayaEks.aspx?awal=true")
    End Sub
#End Region

    Protected Sub EraseNoBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EraseNoBank.Click
        cashbankoid.Text = 0 : BankDtlOid.Text = 0
        CashBankNo.Text = "" : amtekspedisi.Text = ToMaskEdit(0.0, 3)
    End Sub

    Protected Sub EraseCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EraseCust.Click
        ClearDtlAP()
        Session("tbldtl") = Nothing
        GVDtlEkp.DataSource = Nothing
        GVDtlEkp.DataBind()
        trnsuppoid.Text = 0 : suppnames.Text = ""
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, MPE3, False)
        cProc.DisposeGridView(gvSupplier)
        Fill_payflag()
        cashbankoid.Text = 0 : BankDtlOid.Text = 0
        CashBankNo.Text = "" : amtekspedisi.Text = ToMaskEdit(0.0, 3)
    End Sub

    Protected Sub BtnPrintList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            PrintReport(sender.CommandName, sender.ToolTip)
        Catch ex As Exception
            showMessage(ex.ToString & "<BR><BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit sub
        End Try
    End Sub
End Class