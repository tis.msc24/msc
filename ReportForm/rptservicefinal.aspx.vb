
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Globalization

Partial Class ReportForm_rptservicefinal
    Inherits System.Web.UI.Page


#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As ReportDocument
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub clearform()
        ddlfilter.SelectedIndex = 0
        tbperiodstart.Text = Date.Now.ToString("01" & "/MM/yyyy")
        tbperiodend.Text = Date.Now.ToString("dd/MM/yyyy")
        ddlfilterstatus.SelectedIndex = 0
        crv.ReportSource = Nothing
    End Sub

    Private Sub showreport(ByVal sTipe As String)
        Try
            Dim sMsg As String = "" : Dim sWhere As String = "Where fm.CMPCODE='MSC'"
            If DDLcabang.SelectedValue <> "ALL" Then
                sWhere &= " AND fm.branch_code='" & DDLcabang.SelectedValue & "'"
            End If

            sWhere &= " AND " & ddlfilter.SelectedValue.ToString & " LIKE '%" & Tchar(tbfilter.Text.Trim) & "%'"
            If tbperiodstart.Text.Replace("/", "").Trim = "" Or tbperiodend.Text.Replace("/", "").Trim = "" Then
                sMsg &= "Periode awal atau akhir tidak boleh kosong..!!<br>"
            End If

            Dim datestart As String = tbperiodstart.Text.Trim
            Dim dateend As String = tbperiodend.Text.Trim
            Dim sdate, edate As New Date

            If Date.TryParseExact(datestart, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, sdate) And Date.TryParseExact(dateend, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, edate) Then
                Dim syear As Integer = Integer.Parse(sdate.Date.Year)
                Dim eyear As Integer = Integer.Parse(edate.Date.Year)

                If (syear < 1900 Or syear > 9999) Or (eyear < 1900 Or eyear > 9999) Then
                    sMsg &= "Tahun periode harus lebih besar dari 1900 dan lebih kecil dari 9999..!!<br>"
                Else
                    If edate >= sdate Then
                        sWhere &= " AND CONVERT(date, fm.finaldate, 103) BETWEEN '" & sdate & " 00:00:00' AND '" & edate & " 23:59:59'"
                    Else
                        sMsg &= "Periode akhir harus lebih besar dari periode awal..!!<br>"
                    End If
                End If
            Else
                sMsg &= "Periode tidak valid..!!<br>"
            End If

            If cbstatus.SelectedValue = "Status" Then
                If ddlfilterstatus.SelectedValue <> "All" Then
                    sWhere &= " AND LOWER(fm.FINSTATUS) = '" & ddlfilterstatus.SelectedValue & "'"
                End If
                sWhere &= " and fm.FINSTATUS <> 'CLOSED'"
            Else
                sWhere &= " and fm.FINSTATUS = 'CLOSED'"
            End If

            If DDLGaransi.SelectedValue <> "ALL" Then
                sWhere &= " AND typegaransi='" & DDLGaransi.SelectedValue & "'"
            End If

            If sMsg <> "" Then
                showMessage(sMsg, 2)
                Exit Sub
            End If
            rpt = New ReportDocument
            If sTipe = "Exl" Then
                rpt.Load(Server.MapPath("~\Report\rptservicefinalreportExl.rpt"))
            Else
                rpt.Load(Server.MapPath("~\Report\rptservicefinalreport.rpt"))
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            setDBLogonForReport(crConnInfo, rpt)

            rpt.SetParameterValue("sWhere", sWhere)
            rpt.SetParameterValue("reportName", rpt.FileName.Substring(rpt.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            rpt.SetParameterValue("dPeriode", tbperiodstart.Text & " - " & tbperiodend.Text)
            crv.DisplayGroupTree = False

            If sTipe = "Exl" Then
                rpt.Load(Server.MapPath("~\Report\rptservicefinalreportExl.rpt"))
            ElseIf sTipe = "" Then
                rpt.Load(Server.MapPath("~\Report\rptservicefinalreport.rpt"))
            End If

            If sTipe = "" Then
                crv.DisplayGroupTree = False
                crv.ReportSource = rpt
            ElseIf sTipe = "Pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "FINAL" & Format(GetServerTime(), "dd_MM_yy"))
                rpt.Close() : rpt.Dispose()
            ElseIf sTipe = "Exl" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "FINAL" & Format(GetServerTime(), "dd_MM_yy"))
                rpt.Close() : rpt.Dispose()
            End If
 
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub Cabangddl()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLcabang, sSql)
            Else
                FillDDL(DDLcabang, sSql)
                DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
                DDLcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLcabang, sSql)
            DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
            DDLcabang.SelectedValue = "ALL"
        End If
        'Dim sCabang As String = ""
        'If kacabCek("ReportForm/rptservicefinal.aspx", Session("UserID")) = False Then
        '    If Session("branch_id") <> "01" Then
        '        sCabang &= " AND gencode='" & Session("branch_id") & "'"
        '    Else
        '        sCabang = ""
        '    End If
        'End If
        'sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='MSC' And g.gengroup='CABANG' " & sCabang & ""
        'FillDDL(DDLcabang, sSql)
    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        Session.Timeout = 60

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Reportform\rptservicefinal.aspx")
        End If

        'If checkPagePermission("~\Transaction\rptservicefinal.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If

        Page.Title = CompnyName & " - Report Servis Final"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not Page.IsPostBack Then
            clearform()
            Session("report") = Nothing
            Cabangddl()
        End If

    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("~\Reportform\rptservicefinal.aspx?awal=true")
    End Sub

    Protected Sub ibreport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibreport.Click
        showreport("")
    End Sub

    Protected Sub ibtopdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtopdf.Click
        showreport("Pdf")
    End Sub

    Protected Sub ibtoexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtoexcel.Click
        showreport("Exl")
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub crv_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crv.Navigate
        crv.ReportSource = Session("report")
    End Sub

    Protected Sub Page_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        Try
            If Not rpt Is Nothing Then
                If rpt.IsLoaded Then
                    rpt.Dispose()
                    rpt.Close()
                End If
            End If
        Catch ex As Exception
            rpt.Dispose()
            rpt.Close()
        End Try
    End Sub
#End Region
End Class
