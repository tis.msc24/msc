<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnfinalInternal.aspx.vb" Inherits="TrnFinalIntern" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="100%">
        <tr>
            <td style="height: 100%">
    <table width="100%">
        <tr>
            <th align="left" class="header" style="width: 100%" valign="center">
            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Servis Final Internal" Width="267px"></asp:Label>
            </th>
        </tr>
    </table>
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px; vertical-align: text-top;">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; List Of Servis Final Internal </span><strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="dCabangnya" runat="server" Width="128px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD>Filter</TD><TD>:</TD><TD><asp:DropDownList id="ddlfilter" runat="server" Width="128px" CssClass="inpText"><asp:ListItem Value="trnfinalintno">No. Final</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="reqcode">No. TTS</asp:ListItem>
</asp:DropDownList>&nbsp;<SPAN style="COLOR: #ff0000"><asp:TextBox id="tbfilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;</SPAN></TD></TR><TR><TD><asp:CheckBox id="cbperiod" runat="server" Text="Periode"></asp:CheckBox></TD><TD>:</TD><TD><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="64px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;to&nbsp;<asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="64px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;<SPAN style="FONT-SIZE: 11px; COLOR: red"><SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN></SPAN></TD></TR><TR><TD><asp:CheckBox id="cbstatus" runat="server" Text="Status"></asp:CheckBox></TD><TD>:</TD><TD><asp:DropDownList id="ddlfilterstatus" runat="server" Width="120px" CssClass="inpText"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem Value="POST">Post</asp:ListItem>
<asp:ListItem>Invoiced</asp:ListItem>
<asp:ListItem Enabled="False" Value="finish">Finish</asp:ListItem>
<asp:ListItem Enabled="False" Value="final">Final</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="ibfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton> <asp:ImageButton id="ibviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton> <asp:ImageButton id="ibprintdaftar" runat="server" ImageUrl="~/Images/print.png" ImageAlign="Top" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; HEIGHT: 10px" colSpan=3><asp:GridView style="WIDTH: 100%" id="gvdata" runat="server" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="finoid" DataNavigateUrlFormatString="~/Transaction/trnfinalInternal.aspx?oid={0}" DataTextField="trnfinalno" HeaderText="No. Final">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="180px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="180px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="finaldate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl Final">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CabangInp" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqcode" HeaderText="No. TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="finstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data not found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:MaskedEditExtender id="MEE3" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="tbperiodstart"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEE4" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="tbperiodend"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CE1" runat="server" TargetControlID="tbperiodstart" Format="dd/MM/yyyy" PopupButtonID="ibperiodstart"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CE2" runat="server" TargetControlID="tbperiodend" Format="dd/MM/yyyy" PopupButtonID="ibperiodend"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px">
                             <img align="absMiddle" alt="" src="../Images/corner.gif"/>&nbsp;Form Servis Final Internal</span>&nbsp;<strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: middle" colSpan=6><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label><asp:Label id="lbltitle" runat="server" Font-Size="10px" ForeColor="Red" Font-Underline="True" Visible="False"></asp:Label><asp:Label id="i_u" runat="server" ForeColor="Red" Visible="False">new</asp:Label></TD></TR><TR><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DdlCabang" runat="server" Width="165px" CssClass="inpText" OnSelectedIndexChanged="DdlCabang_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList> <asp:Label id="CabangAsal" runat="server" Visible="False"></asp:Label></TD><TD id="TD10" Visible="false"></TD><TD id="TD12" Visible="false"></TD><TD id="TD11" Visible="false"><asp:DropDownList id="DDLTypeTTS" runat="server" Height="20px" CssClass="inpText" Visible="False" AutoPostBack="True"><asp:ListItem>SERVICE</asp:ListItem>
<asp:ListItem>REPLACE</asp:ListItem>
<asp:ListItem Enabled="False" Value="SA">SALDO AWAL</asp:ListItem>
</asp:DropDownList><asp:Label id="TypeTTS" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD>No. Final</TD><TD>:</TD><TD><asp:TextBox id="TrnFinalNo" runat="server" Width="165px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:Label id="FinOid" runat="server" Visible="False"></asp:Label></TD><TD Visible="false">Tanggal</TD><TD Visible="false">:</TD><TD Visible="false"><asp:TextBox id="tglFinal" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:Label id="createtime" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD>No. Tanda Terima</TD><TD>:</TD><TD><asp:TextBox id="tbservicesno" runat="server" Width="165px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibsearchsvc" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibdelsvc" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblreqoid" runat="server" Visible="False"></asp:Label></TD><TD>Customer</TD><TD>:</TD><TD><asp:TextBox id="lblfincust" runat="server" Width="224px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD>Telepon</TD><TD>:</TD><TD><asp:TextBox id="phone" runat="server" Width="165px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:Label id="ItemOid" runat="server" Visible="False"></asp:Label></TD><TD>Keterangan</TD><TD>:</TD><TD align=left><asp:TextBox id="tbsparepartjob" runat="server" Width="165px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD colSpan=3><asp:Label id="Label700" runat="server" Width="219px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang yang di service" Font-Underline="True"></asp:Label><asp:Label id="reqdtloid" runat="server" Visible="False"></asp:Label><asp:Label id="SeqBarang" runat="server" Visible="False"></asp:Label><asp:Label id="I_Text2" runat="server" Font-Bold="True" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label></TD><TD><asp:Label id="SerialNo" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD><asp:TextBox id="lblfinstatus" runat="server" CssClass="inpTextDisabled" Visible="False" Enabled="False">In Process</asp:TextBox> <asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD>Katalog Rusak</TD><TD>:</TD><TD><asp:TextBox id="lblfinitemname" runat="server" Width="295px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnSbarang" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImbEbarang" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> </TD><TD>Type Barang</TD><TD>:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="TypeBarang" runat="server" Width="102px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:Label id="FlagBarang" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top" colSpan=4><asp:GridView style="WIDTH: 99%" id="GvBarang" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="reqmstoid,reqdtloid,itemoid,itemcode,itemdesc,reqqty,snno,kelengkapan,reqdtljob,typegaransi,OidLoc,FlagBarang" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Font-Underline="False" ForeColor="SaddleBrown" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="snno" HeaderText="Serial No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqqty" HeaderText="Qty TTS">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typegaransi" HeaderText="Kerusakan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kelengkapan" HeaderText="kelengkapan" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="OidLoc" HeaderText="OidLoc" Visible="False"></asp:BoundField>
<asp:BoundField DataField="FlagBarang" HeaderText="FlagBarang" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD>Gudang</TD><TD>:</TD><TD><asp:DropDownList id="DDLGudangRusak" runat="server" Height="20px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD>Keterangan</TD><TD>:</TD><TD><asp:TextBox id="Kerusakan" runat="server" Width="222px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD>Quantity</TD><TD>:</TD><TD><asp:TextBox id="Qty" runat="server" Width="59px" CssClass="inpText">0.00</asp:TextBox></TD><TD colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnAddToListBarang" runat="server" ImageUrl="~/Images/addtolist.png"></asp:ImageButton> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvrequest" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="reqdtloid" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemOid" HeaderText="ItemOid" Visible="False">
<HeaderStyle HorizontalAlign="Left" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="snno" HeaderText="Serial No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl1" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="mtrloc" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Sparepart"><ItemTemplate>
<asp:Button id="BtnDtlSparepart" onclick="BtnDtlSparepart_Click" runat="server" CssClass="btn orange" Text="Sparepart" ToolTip='<%# Eval("reqdtloid") %>' CommandName='<%# Eval("ItemOid") %>'></asp:Button> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Rombeng/Rusak"><ItemTemplate>
<asp:Button id="BtnRombeng" onclick="BtnRombeng_Click" runat="server" CssClass="btn green" Text="Rusak/Rombeng" ToolTip='<%# Eval("reqdtloid") %>' CommandName='<%# eval("itemoid") %>'></asp:Button> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Barang Bagus"><ItemTemplate>
<asp:Button id="BtnDtlBagus" runat="server" CssClass="btn orange" Text="Barang Bagus" ToolTip='<%# Eval("reqdtloid") %>' OnClick="BtnDtlBagus_Click" CommandName='<%# Eval("ItemOid") %>'></asp:Button>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="DelRusak" onclick="DelRusak_Click" runat="server" ToolTip='<%# Eval("reqdtloid") %>'><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="VERTICAL-ALIGN: top"><STRONG><SPAN style="TEXT-DECORATION: underline"><asp:Label id="Label6" runat="server" Width="143px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Spareparts" Font-Underline="True"></asp:Label> <asp:Label id="lblspartstate" runat="server" Font-Bold="True" ForeColor="Red" Visible="False">new</asp:Label></SPAN></STRONG></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top"><asp:Label id="PartsOid" runat="server" Visible="False"></asp:Label><asp:Label id="SeqParts" runat="server" Visible="False"></asp:Label><asp:Label id="ItemCode" runat="server" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD><asp:DropDownList id="matLoc" runat="server" Height="20px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GvParts" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="seq,finoid,itemcode,itemdesc,qty,notedtl1,mtrlocoid,reqoid,reqdtloid,hpp,itemrusakoid,PartOid" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl1" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="DelPart" onclick="DelPart_Click" runat="server" ToolTip='<%# Eval("seq") %>'><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="hpp" HeaderText="HPP" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD colSpan=3><asp:Label id="Label3" runat="server" Width="355px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang Rusak Atau Rombeng hasil service" Font-Underline="True"></asp:Label> <asp:DropDownList id="DDLRombeng" runat="server" CssClass="inpTextDisabled" Visible="False"></asp:DropDownList></TD><TD id="Td1" runat="server"></TD><TD id="Td2" runat="server"></TD><TD id="Td3" runat="server"></TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GvRombeng" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="seq" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl3" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="DeleteRombeng" onclick="DeleteRombeng_Click" runat="server" ToolTip='<%# Eval("seq") %>'><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD colSpan=3><asp:Label id="Label4" runat="server" Width="248px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang Bagus hasil service" Font-Underline="True"></asp:Label><asp:Label id="TxtBagus" runat="server" Font-Bold="True" ForeColor="Red" Visible="False">new</asp:Label> <asp:DropDownList id="LocoidBagusDDL" runat="server" Height="20px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD><TD runat="server"></TD><TD runat="server"></TD><TD runat="server"></TD></TR><TR><TD id="TD19" colSpan=6 runat="server" Visible="true"><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GvBagus" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="seq,itembagusoid,itemcode,itemdesc,qty,mtrlocoid,hpp,notedtl4,reqdtloid" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl4" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="DelBagus" runat="server" ToolTip='<%# Eval("Seq") %>' OnClick="DelBagus_Click"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD colSpan=6><asp:Label id="NettNya" runat="server" Font-Size="Medium" Font-Bold="True" Text="0.00" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibsave" runat="server" ImageUrl="~/Images/Save.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" runat="server" ImageUrl="~/Images/Delete.png" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnSendApp" runat="server" ImageUrl="~/Images/sendapproval.png" CausesValidation="False" Visible="False"></asp:ImageButton></TD></TR><TR><TD colSpan=6><asp:Label id="lblcreate" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress>&nbsp; </TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="uppopupmsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepopupmsg" runat="server" TargetControlID="bepopupmsg" DropShadow="True" PopupControlID="pnlpopupmsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblcaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bepopupmsg" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderStyle="None" BorderColor="Transparent"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="uppupgv" runat="server">
        <contenttemplate>
<asp:Panel id="panpopgv" runat="server" CssClass="modalBox" Visible="False" DefaultButton="ibpopfind"><DIV style="OVERFLOW: auto"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="Label480" runat="server" Width="220px" Font-Size="15pt" Font-Bold="True" Text="List tanda terima"></asp:Label></TD></TR><TR><TD align=center colSpan=3>Filter:<asp:DropDownList id="ddlpopfilter" runat="server" Width="112px" CssClass="inpText"><asp:ListItem Value="rq.reqcode">No. TTS</asp:ListItem>
<asp:ListItem Value="c.custname">Customer</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="tbpopfilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibpopfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton> <asp:ImageButton id="ibpopviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 99%; HEIGHT: 168px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvpoporder" runat="server" ForeColor="#333333" DataKeyNames="reqoid,reqcode,reqdate,custname,phone1,custoid ,Cate ,CabangAsal" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Font-Underline="False" ForeColor="SaddleBrown" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="reqcode" HeaderText="No. TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl. TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sType" HeaderText="Type TTS">
<HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="reqoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" HeaderText="custoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="Cabang Asal" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Cate" HeaderText="Cate" Visible="False"></asp:BoundField>
<asp:BoundField DataField="LocOidTitipan" HeaderText="LocOidTitipan" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" ForeColor="Red">Data not found!</asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=3><asp:Label id="lblerror" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=3><asp:ImageButton id="ibpopcancel" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton></TD></TR></TBODY></TABLE></DIV></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepopgv" runat="server" Drag="True" PopupDragHandleControlID="ibpopcancel" BackgroundCssClass="modalBackground" PopupControlID="panpopgv" DropShadow="True" TargetControlID="bepopgv"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bepopgv" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderColor="Transparent" BorderStyle="None"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel8" runat="server">
        <contenttemplate>
<asp:Panel id="panelItem" runat="server" Width="80%" CssClass="modalBox" Visible="False" DefaultButton="ibpoppartfind"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptItem" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Spareparts"></asp:Label></TD></TR><TR><TD align=center>Filter : <asp:DropDownList id="ddlpoppartfilter" runat="server" Width="112px" CssClass="inpText"><asp:ListItem Value="partdescshort">Deskripsi</asp:ListItem>
<asp:ListItem Value="partcode">Spart Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="tbpoppartfilter" runat="server" Width="160px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibpoppartfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibpoppartviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView id="GVItemList" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" Visible="False" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" AllowPaging="True" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("PARTOID") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="partcode" HeaderText="Parts Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="partdescshort" HeaderText="Spare Parts">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="brand" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Parts Qty"><ItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("QtyOs") %>' MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" ValidChars="1234567890.," TargetControlID="tbQty">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Harga" Visible="False"><ItemTemplate>
<asp:TextBox id="PriceParts" runat="server" Width="100px" CssClass="inpText" Text='<%# eval("spartprice") %>' Enabled="<%# GetStatus() %>" MaxLength="15"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftEPrice" runat="server" TargetControlID="PriceParts" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="partsUnit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Stock Akhir">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="5px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="UnitOid" HeaderText="UnitOid" Visible="False">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label91" runat="server" 
                                                                        Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:LinkButton id="LabelNya" runat="server" ForeColor="Red" Visible="False">* Tetap klik add to list dengan/tanpa spareparts</asp:LinkButton></TD></TR><TR><TD align=center><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseItem" runat="server">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"><asp:Label id="sValidasi" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" TargetControlID="btnHideItem" PopupDragHandleControlID="lblCaptItem" BackgroundCssClass="modalBackground" PopupControlID="panelItem"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideItem" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Katalog"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblFilterInListMat" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemdesc">katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" align=center>Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectMat" runat="server" ToolTip='<%# eval("itemrombengoid") %>' Checked='<%# eval("CheckValue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbMatQty" runat="server" Width="104px" CssClass="inpText" Text='<%# eval("qty") %>' MaxLength="18"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty" runat="server" TargetControlID="tbMatQty" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Keterangan"><ItemTemplate>
<asp:TextBox id="noterusak" runat="server" Width="150px" CssClass="inpText" ToolTip='<%# Eval("notedtl3") %>'></asp:TextBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Gudang"><ItemTemplate>
<asp:DropDownList id="DDLGudangRombeng" runat="server" Height="20px" CssClass="inpText" ToolTip='<%# eval("mtrlocoid") %>'></asp:DropDownList>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupDragHandleControlID="lblListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListSO" runat="server">
        <contenttemplate>
<asp:Panel id="PanelListSO" runat="server" Width="1000px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Large" Font-Bold="True" Text="Daftar Katalog"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMSO" runat="server" ToolTip='<%# eval("itembagusoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbMatQty1" runat="server" Width="104px" CssClass="inpText" Text='<%# eval("qty") %>' MaxLength="18"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty1" runat="server" TargetControlID="tbMatQty1" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Keterangan"><ItemTemplate>
<asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" BackgroundCssClass="modalBackground" PopupControlID="PanelListSO">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

