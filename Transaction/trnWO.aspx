<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnWO.aspx.vb" Inherits="Transaction_WorkOrder" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Surat Perintah Kerja" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of SPK :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="lblFilter" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" Width="105px" CssClass="inpText"><asp:ListItem Value="wom.womstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="wom.wono">SPK Leburan No.</asp:ListItem>
<asp:ListItem Value="som.sono">SO No.</asp:ListItem>
<asp:ListItem Value="i.matrawcode">FG Code</asp:ListItem>
<asp:ListItem Value="i.matrawlongdesc">FG Description</asp:ListItem>
<asp:ListItem Value="wom.upduser">Posted By</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="250px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" Width="80px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="80px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblLabelPeriod" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbBusUnit" runat="server" Text="Business Unit"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLBusUnit" runat="server" Width="360px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w1"></asp:CheckBox> <asp:CheckBox id="cbQtyKIK" runat="server" Text="Wgt SPK" Visible="False"></asp:CheckBox></TD><TD style="HEIGHT: 22px" class="Label" align=center>:</TD><TD style="HEIGHT: 22px" class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w2"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Revised</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
<asp:ListItem>Cancel</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="FilterDDLQtyKIK" runat="server" Width="50px" CssClass="inpText" Visible="False"><asp:ListItem>&gt;</asp:ListItem>
<asp:ListItem>&gt;=</asp:ListItem>
<asp:ListItem>=</asp:ListItem>
<asp:ListItem>&lt;&gt;</asp:ListItem>
<asp:ListItem>&lt;</asp:ListItem>
<asp:ListItem>&lt;=</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextQtyKIK" runat="server" Width="100px" CssClass="inpText" Visible="False" MaxLength="16"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbKIKQty" runat="server" TargetControlID="FilterTextQtyKIK" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbWOInProcess" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" Width="100%" ForeColor="#333333" AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="womstoid" DataNavigateUrlFormatString="~\Transaction\trnWO.aspx?oid={0}" DataTextField="womstoid" HeaderText="Draft No." SortExpression="womstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="wodate" HeaderText="Date" SortExpression="wodate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wono" HeaderText="SPK No." SortExpression="wono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemno" HeaderText="SO No." SortExpression="soitemno" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="FG Code" SortExpression="itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="FG Description" SortExpression="itemlongdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1qty" HeaderText="Wgt (gr)" SortExpression="wodtl1qty" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="womststatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="postuser" HeaderText="Posted By" SortExpression="postuser">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("womstoid") %>' Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label166" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="SPK Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="PeriodAcctg" runat="server" Visible="False"></asp:Label> <asp:Label id="cmpcode" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" Width="305px" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="typeorder" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="typespk" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wono" runat="server" Width="260px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="womstoid" runat="server"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="PIC"></asp:Label>&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLPIC" runat="server" Width="253px" CssClass="inpText" __designer:wfdid="w1" AutoPostBack="True"></asp:DropDownList> <asp:TextBox id="wodate" runat="server" Width="80px" CssClass="inpText" ToolTip="MM/dd/yyyy" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPRDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label59" runat="server" Text="Rencana Produksi"></asp:Label> <asp:Label id="Label60" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 22px" class="Label" align=center>:</TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:TextBox id="startProd" runat="server" Width="80px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbstartprod" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label61" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label57" runat="server" Text="Rencana Selesai"></asp:Label> <asp:Label id="Label58" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="HEIGHT: 22px" class="Label" align=center>:</TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:TextBox id="finishProd" runat="server" Width="80px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbfinishprod" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label62" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label28" runat="server" Text="Jenis Spk" __designer:wfdid="w86"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 22px" class="Label" align=center>:</TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:DropDownList id="ddlspk" runat="server" Width="183px" CssClass="inpText" __designer:wfdid="w87"><asp:ListItem Value="RK">RAKITAN</asp:ListItem>
<asp:ListItem Value="P">PROMO</asp:ListItem>
</asp:DropDownList></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label96" runat="server" Text="Untuk Cabang" __designer:wfdid="w1"></asp:Label></TD><TD style="HEIGHT: 22px" class="Label" align=center>:</TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:DropDownList id="ddltobranch" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w2" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 36px" class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 36px" class="Label" align=center>:</TD><TD style="HEIGHT: 36px" class="Label" align=left><asp:TextBox id="womstnote" runat="server" Width="300px" Height="29px" CssClass="inpText" MaxLength="100" TextMode="MultiLine"></asp:TextBox></TD><TD style="HEIGHT: 36px" class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status"></asp:Label></TD><TD style="HEIGHT: 36px" class="Label" align=center>:</TD><TD style="HEIGHT: 36px" class="Label" align=left><asp:TextBox id="womststatus" runat="server" Width="100px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Revise Note" __designer:wfdid="w1" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center><asp:Label id="Label27" runat="server" Width="1px" Text=":" __designer:wfdid="w1" Visible="False"></asp:Label></TD><TD class="Label" align=left rowSpan=3><asp:TextBox id="txtrevise" runat="server" Width="300px" Height="29px" CssClass="inpText" __designer:wfdid="w2" Visible="False" MaxLength="100" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label4" runat="server" Text="Doc. Ref. No." Visible="False"></asp:Label> <asp:TextBox id="wodocrefno" runat="server" Width="157px" CssClass="inpText" Visible="False" MaxLength="50"></asp:TextBox> <BR /><asp:Image id="imgMat" runat="server" Width="130px" ImageUrl="~/Images/PictureMaterial/none.png" Height="80px" Visible="False"></asp:Image></TD><TD class="Label" align=left><asp:Label id="PictureLoc" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left" class="Label" align=left rowSpan=3><asp:RadioButton id="customer" runat="server" Text="Customer Order" Visible="False" AutoPostBack="True" OnCheckedChanged="customer_CheckedChanged" GroupName="rb" Checked="True"></asp:RadioButton>&nbsp;<asp:RadioButton id="stock" runat="server" Text="Stock" Visible="False" AutoPostBack="True" OnCheckedChanged="stock_CheckedChanged" GroupName="rb"></asp:RadioButton><BR /><asp:RadioButton id="Stamping" runat="server" Text="Stamping" Visible="False" GroupName="rb1" Checked="True"></asp:RadioButton>&nbsp;<asp:RadioButton id="Casting" runat="server" Text="Casting" Visible="False" GroupName="rb1"></asp:RadioButton>&nbsp;<asp:RadioButton id="all" runat="server" Text="Cast + Stamp" Visible="False" GroupName="rb1"></asp:RadioButton></TD></TR><TR><TD class="Label" align=left></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="Label52" runat="server" Text="Type Order" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD></TR><TR><TD class="Label" align=left></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="Label56" runat="server" Text="Type SPK" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDate" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbPRDate" TargetControlID="wodate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="wodate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cdatestart" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbstartprod" TargetControlID="startProd"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meestartprod" runat="server" TargetControlID="startProd" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cdatefinish" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbfinishprod" TargetControlID="finishProd"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meefinishprod" runat="server" TargetControlID="finishProd" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label173" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="SPK Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:MultiView id="MultiView1" runat="server"><asp:View id="View1" runat="server"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label2" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Detail Barang" Font-Underline="False"></asp:Label>&nbsp;<asp:Label id="Label12" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:LinkButton id="lkbDetProc1" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Process</asp:LinkButton><asp:Label id="Label13" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Visible="False" Font-Underline="False"></asp:Label><asp:LinkButton id="lbkdetWip" onclick="lbkdetWip_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue" Visible="False">Detail Wip</asp:LinkButton>&nbsp;<asp:Label id="Label87" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:LinkButton id="lkbDetMat1" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Material</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 14%; HEIGHT: 26px" class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%; HEIGHT: 26px" class="Label" align=center></TD><TD style="WIDTH: 35%; HEIGHT: 26px" class="Label" align=left><asp:Label id="wodtl1seq" runat="server" Visible="False">1</asp:Label> <asp:Label id="wodtl1oid" runat="server" Visible="False"></asp:Label> <asp:Label id="soitemmstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="soitemdtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="lbspkoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%; HEIGHT: 26px" class="Label" align=left><asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 26px" class="Label" align=center></TD><TD style="WIDTH: 307px; HEIGHT: 26px" class="Label" align=left><asp:Label id="itemcode" runat="server" Visible="False"></asp:Label> <asp:Label id="bomoid" runat="server" Visible="False"></asp:Label> <asp:Label id="groupoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label36" runat="server" Text="Output Item"></asp:Label><asp:Label id="Label37" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="itemshortdesc" runat="server" Width="220px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label67" runat="server" Text="Kode Baru"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center>:</TD><TD style="WIDTH: 307px" class="Label" align=left><asp:TextBox id="kodebarudtl" runat="server" Width="259px" CssClass="inpTextDisabled"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label152" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl1note" runat="server" Width="300px" CssClass="inpText" MaxLength="500"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label70" runat="server" Text="Qty" __designer:wfdid="w5"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center>:</TD><TD style="WIDTH: 307px" class="Label" align=left><asp:TextBox id="qtyprod" runat="server" Width="98px" CssClass="inpText" __designer:wfdid="w7" AutoPostBack="True" OnTextChanged="qtyprod_TextChanged"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label63" runat="server" Text="Type Emas" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="tipeEmas" runat="server" Width="109px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label64" runat="server" Text="Kadar Emas" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD style="WIDTH: 307px" class="Label" align=left><asp:TextBox id="kadarEmas" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox>&nbsp;</TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:FilteredTextBoxExtender id="ftbdiameter" runat="server" __designer:wfdid="w1" TargetControlID="diameter" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD class="Label" align=left></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD style="WIDTH: 307px" class="Label" align=left>&nbsp; <ajaxToolkit:FilteredTextBoxExtender id="ftbprod" runat="server" __designer:wfdid="w3" TargetControlID="qtyprod" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList1" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear1" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label68" runat="server" Text="Kode Lama" __designer:wfdid="w4" Visible="False"></asp:Label> </TD><TD style="HEIGHT: 22px" class="Label" align=center></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:TextBox id="kodelamadtl" runat="server" Width="166px" CssClass="inpTextDisabled" __designer:wfdid="w6" Visible="False"></asp:TextBox>&nbsp;&nbsp;<asp:DropDownList id="wodtl1unitoid1" runat="server" Width="75px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label65" runat="server" Text="Berat Per Unit" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 22px" class="Label" align=center></TD><TD style="WIDTH: 307px; HEIGHT: 22px" class="Label" align=left><asp:TextBox id="berat" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False"></asp:TextBox>&nbsp; <asp:DropDownList id="wodtl1unitoid2" runat="server" Width="75px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left colSpan=6><asp:GridView id="GVDtl1" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w10" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" DataKeyNames="wodtl1seq">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wono" HeaderText="SPK No." Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemno" HeaderText="SO No." Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="FG Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemshortdesc" HeaderText="FG Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyproduksi" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratlilin" HeaderText="Berat Lilin (gr)" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratUnit" HeaderText="Berat/Unit (gr)" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kadaremas" HeaderText="Kadar Emas (%)" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typeemasdesc" HeaderText="Type Emas" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1qty" HeaderText="Berat Total" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1unit" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1area" HeaderText="Area" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1note" HeaderText="Detail Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label66" runat="server" Text="Diameter" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="diameter" runat="server" Width="100px" CssClass="inpText" Visible="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label69" runat="server" Text="Berat Lilin" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD style="WIDTH: 307px" class="Label" align=left><asp:TextBox id="beratlilinFG" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="Label29" runat="server" Text="Division" Visible="False"></asp:Label> <asp:Label id="Label40" runat="server" Text="Rounding Qty" Visible="False"></asp:Label> <asp:Label id="Label38" runat="server" Text="SO Qty" Visible="False"></asp:Label> <asp:TextBox id="groupdesc" runat="server" Width="52px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox><asp:TextBox id="soitemqty" runat="server" Width="31px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox><asp:TextBox id="itemlimitqty" runat="server" Width="29px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Area" Visible="False"></asp:Label> <asp:Label id="Label54" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD style="WIDTH: 307px" class="Label" align=left><asp:TextBox id="wodtl1area" runat="server" Width="100px" CssClass="inpText" Visible="False" MaxLength="16"></asp:TextBox> <asp:Label id="Label55" runat="server" CssClass="Important" Text="M2" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label45" runat="server" Text="SPK Item No." Visible="False"></asp:Label>&nbsp;<asp:Label id="Label81" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="spkno" runat="server" Width="260px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="findspk" onclick="findspk_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="clearspk" onclick="clearspk_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="lblmaxqty" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD style="WIDTH: 307px" class="Label" align=left><ajaxToolkit:FilteredTextBoxExtender id="ftbArea" runat="server" __designer:wfdid="w4" TargetControlID="wodtl1area" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="wodtl1totalmatamt" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1dlcamt" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1ohdamt" runat="server" Visible="False"></asp:Label> <asp:Label id="curroid_ohd" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1ohdamtidr" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1marginamt" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1precostamt" runat="server" Visible="False"></asp:Label> <asp:Label id="curroid_precost" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1precostamtidr" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupduser_precost" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupdtime_precost" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupduser_bom" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupdtime_bom" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupduser_dlc" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupdtime_dlc" runat="server" Visible="False"></asp:Label> </TD></TR></TBODY></TABLE></asp:View><asp:View id="View2" runat="server"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="lkbDetItem1" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Barang</asp:LinkButton>&nbsp;<asp:Label id="Label24" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:Label id="Label32" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Detail Process" Font-Underline="False"></asp:Label> <asp:Label id="Label88" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Visible="False" Font-Underline="False"></asp:Label> <asp:LinkButton id="lbkDetWip1" onclick="lbkDetWip1_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue" Visible="False">Detail Wip</asp:LinkButton>&nbsp;<asp:Label id="Label31" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label> <asp:LinkButton id="lkbDetMat2" onclick="lkbDetMat_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Material</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u4" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="wodtl2seq" runat="server" Visible="False">1</asp:Label> <asp:Label id="wodtl2oid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl1oid" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl2desc" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label33" runat="server" Text="Sequence" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="wodtl2procseq" runat="server" Width="56px" CssClass="inpText" Visible="False" AutoPostBack="True" OnTextChanged="wodtl2procseq_TextChanged"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label34" runat="server" Text="Process Type" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="wodtl2type" runat="server" Width="105px" CssClass="inpTextDisabled" Visible="False" OnSelectedIndexChanged="wodtl2type_SelectedIndexChanged" AutoPostBack="True" Enabled="False"><asp:ListItem Value="FG">FG</asp:ListItem>
<asp:ListItem>WIP</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label46" runat="server" Text="From Process"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptoid" runat="server" Width="305px" CssClass="inpText" OnSelectedIndexChanged="deptoid_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lbltodept" runat="server" Text="To Process"></asp:Label></TD><TD class="Label" align=center><asp:Label id="septtodept" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="todeptoid" runat="server" Width="305px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 14px" class="Label" align=left><asp:Label id="lbltukang" runat="server" Text="Operator 1"></asp:Label></TD><TD style="HEIGHT: 14px" class="Label" align=center>:</TD><TD style="HEIGHT: 14px" class="Label" align=left><asp:DropDownList id="DDLop1" runat="server" Width="160px" CssClass="inpText"></asp:DropDownList> <asp:Label id="op1oid" runat="server"></asp:Label></TD><TD style="HEIGHT: 14px" class="Label" align=left><asp:Label id="Label71" runat="server" Text="Operator 2"></asp:Label></TD><TD style="HEIGHT: 14px" class="Label" align=center>:</TD><TD style="HEIGHT: 14px" class="Label" align=left><asp:DropDownList id="DDLop2" runat="server" Width="184px" CssClass="inpText"></asp:DropDownList> <asp:Label id="op2oid" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label72" runat="server" Text="Quality Control"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLqa" runat="server" Width="160px" CssClass="inpText"></asp:DropDownList> <asp:Label id="qaoid" runat="server"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label85" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="noteprocess" runat="server" Width="298px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:DropDownList id="tukang" runat="server" Width="64px" CssClass="inpText" Visible="False"></asp:DropDownList></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="datestart" runat="server" Width="72px" CssClass="inpText" ToolTip="MM/dd/yyyy" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDateStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:TextBox id="TimeStart" runat="server" Width="55px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp;<asp:Label id="Label43" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="processlimitqty" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="DateFinish" runat="server" Width="72px" CssClass="inpText" ToolTip="MM/dd/yyyy" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbdatefinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:TextBox id="TimeFinish" runat="server" Width="55px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp;<asp:Label id="Label50" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbDtl2Qty" runat="server" TargetControlID="wodtl2qty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDateStart" TargetControlID="datestart"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="datestart" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" TargetControlID="TimeStart" MaskType="Time" Mask="99:99:99"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" PopupButtonID="imbdatefinish" TargetControlID="datefinish"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="DateFinish" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="TimeFinish" MaskType="Time" Mask="99:99:99"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="berat" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbberatoutput" runat="server" TargetControlID="beratoutput" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender>&nbsp; </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList2" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear2" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 155px" class="Label" align=left colSpan=6><asp:GridView id="GVDtl2" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" DataKeyNames="wodtl2seq">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wodtl2procseq" HeaderText="Seq">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2type" HeaderText="Process Type" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="From Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="todeptname" HeaderText="To Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2qty" HeaderText="Qty" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2unit" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datestart" HeaderText="Start Date" Visible="False"></asp:BoundField>
<asp:BoundField DataField="datefinish" HeaderText="Finish Date" Visible="False"></asp:BoundField>
<asp:BoundField DataField="tukang" HeaderText="Craftsman" Visible="False">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomWIPdesc" HeaderText="Output" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="bomkadarWip" HeaderText="Kadar" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratmaxoutput" HeaderText="Berat" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="operator1" HeaderText="OP1">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="operator2" HeaderText="OP2">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="quality" HeaderText="QC">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:View><asp:View id="View3" runat="server"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="lkbDetItem2" onclick="lkbDetItem_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Barang</asp:LinkButton>&nbsp;<asp:Label id="Label23" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:LinkButton id="lkbDetProc2" onclick="lkbDetProc_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Process</asp:LinkButton><asp:Label id="Label89" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Visible="False" Font-Underline="False"></asp:Label><asp:LinkButton id="lbkdetWip3" onclick="lbkdetWip3_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue" Visible="False">Detail Wip</asp:LinkButton>&nbsp;<asp:Label id="Label19" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:Label id="Label18" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Detail Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlMatDtl5" runat="server" Width="100%" BorderWidth="1px" BorderStyle="Outset" BorderColor="Silver"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label41" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Detail Material Level 5" Visible="False" Font-Underline="False"></asp:Label> <asp:Label id="Label15" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Visible="False" Font-Underline="False"></asp:Label> <asp:LinkButton id="lbLevel03" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue" Visible="False">Detail Material Level 2</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u3" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="wodtl3seq" runat="server" Visible="False">1</asp:Label> <asp:Label id="wodtl3oid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl2oid" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl3refoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="wodtl3refcode" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl3qtyref" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label39" runat="server" Text="Process"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLProcess" runat="server" Width="305px" CssClass="inpText"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Material"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl3refshortdesc" runat="server" Width="183px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="findmat2" onclick="findmat2_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="clearmat2" onclick="clearmat2_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Note" __designer:wfdid="w4"></asp:Label> <asp:Label id="Label20" runat="server" Text="Mat. Type" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl3note" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w5" MaxLength="100"></asp:TextBox> <asp:DropDownList id="wodtl3reftype" runat="server" Width="105px" CssClass="inpText" Visible="False"><asp:ListItem Value="Emas 24K">EMAS 24K</asp:ListItem>
<asp:ListItem Value="Bahan Baku">BAHAN BAKU</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label42" runat="server" Text="Qty Pakai" __designer:wfdid="w1"></asp:Label> <asp:Label id="Label83" runat="server" Text="Toleransi Pakai" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="qtypakai" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox>&nbsp;<asp:DropDownList id="wodtl3unitoid" runat="server" Width="105px" CssClass="inpTextDisabled" __designer:wfdid="w3" Visible="False" Enabled="False"></asp:DropDownList> <asp:TextBox id="tolerancedtl3" runat="server" Width="100px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp;</TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="Label84" runat="server" Text="Jenis Emas" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="typeEmasdtl2" runat="server" Width="105px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6><asp:ImageButton id="btnlist3" onclick="btnlist3_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w6" Visible="False"></asp:ImageButton><asp:ImageButton id="btnClear3" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label95" runat="server" Text="Berat Pakai" Visible="False"></asp:Label></TD><TD style="HEIGHT: 22px" class="Label" align=center></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:TextBox id="qtypakaibiji" runat="server" Width="100px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp; <asp:DropDownList id="wodtl3unitbijioid" runat="server" Width="105px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label25" runat="server" Text="Kadar Emas" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 22px" class="Label" align=center></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:TextBox id="kadaremasdtl2" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox>&nbsp;</TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left colSpan=6><asp:GridView id="GVDtl3" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w8" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" DataKeyNames="wodtl3seq" OnRowDeleting="GVDtl3_RowDeleting">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wodtl2desc" HeaderText="Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3reftype" HeaderText="Mat. Type" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3refcode" HeaderText="Mat. Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3refshortdesc" HeaderText="Mat. Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3qty" HeaderText="Wgt (gr)" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3qtybiji" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtybom" HeaderText="Qty BOM" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tolerance" HeaderText="Toleransi (%)" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3unit" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="20px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 21px" class="Label" align=left></TD><TD style="HEIGHT: 21px" class="Label" align=center></TD><TD style="HEIGHT: 21px" class="Label" align=left></TD><TD style="HEIGHT: 21px" class="Label" align=left><asp:Label id="Label35" runat="server" Text="Qty BOM" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 21px" class="Label" align=center></TD><TD style="HEIGHT: 21px" class="Label" align=left><asp:TextBox id="QtyBOM" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="Label30" runat="server" Text="Stock Gudang" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px" class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="stockMat3" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left></TD><TD style="HEIGHT: 22px" class="Label" align=center></TD><TD style="HEIGHT: 22px" class="Label" align=left></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label44" runat="server" Text="Satuan Material" Visible="False"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 22px" class="Label" align=center></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:TextBox id="wodtl3qty" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox> <asp:Label id="weightMat" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlMatDtl3" runat="server" Width="100%" BorderWidth="1px" BorderStyle="Outset" BorderColor="Silver"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="lbLevel05" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Material Level 5</asp:LinkButton> <asp:Label id="Label17" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label> <asp:Label id="Label16" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Detail Material Level 2" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u5" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="wodtl4seq" runat="server" Visible="False">1</asp:Label> <asp:Label id="wodtl4oid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl3oid" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl4refoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="wodtl4refcode" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl4qtyref" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label47" runat="server" Text="Process"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLProcess2" runat="server" Width="305px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label48" runat="server" Text="Mat. Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="wodtl4reftype" runat="server" Width="105px" CssClass="inpTextDisabled" Enabled="False"><asp:ListItem>Raw</asp:ListItem>
<asp:ListItem>General</asp:ListItem>
<asp:ListItem>Spare Part</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label49" runat="server" Text="Material"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl4refshortdesc" runat="server" Width="300px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label51" runat="server" Text="Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl4qty" runat="server" Width="100px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;- <asp:DropDownList id="wodtl4unitoid" runat="server" Width="105px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label53" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl4note" runat="server" Width="300px" CssClass="inpText" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnClear4" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="GVDtl4" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" DataKeyNames="wodtl4seq">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wodtl2desc" HeaderText="Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4reftype" HeaderText="Mat. Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4refcode" HeaderText="Mat. Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4refshortdesc" HeaderText="Mat. Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View4" runat="server"><TABLE width="100%"><TBODY><TR id="Tr1" visible="False"><TD class="Label" align=left colSpan=6><asp:LinkButton id="lbkfg4" onclick="lbkfg4_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Barang</asp:LinkButton>&nbsp;<asp:Label id="Label90" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:LinkButton id="lbkprocess4" onclick="lbkprocess4_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Process</asp:LinkButton>&nbsp;<asp:Label id="Label92" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Visible="False" Font-Underline="False"></asp:Label>&nbsp;<asp:Label id="Label91" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Detail Wip" Visible="False" Font-Underline="False"></asp:Label>&nbsp;<asp:Label id="Label93" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:LinkButton id="lbkdetmat4" onclick="lkbDetMat_Click" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="SteelBlue">Detail Material</asp:LinkButton></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label94" runat="server" Text="Process"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLProcessWip" runat="server" Width="269px" CssClass="inpText"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label74" runat="server" Text="WIP"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="output" runat="server" Width="233px" CssClass="inpTextDisabled"></asp:TextBox>&nbsp;<asp:ImageButton id="imbfindWIP" onclick="imbfindWIP_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbclearWip" onclick="imbclearWip_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label75" runat="server" Text="Kode WIP"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="kodebarudtl1" runat="server" Width="274px" CssClass="inpTextDisabled"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label76" runat="server" Text="Jenis Emas"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="typeEmasdtl1" runat="server" Width="105px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label77" runat="server" Text="Kadar Emas"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="kadaremasdtl" runat="server" Width="50px" CssClass="inpTextDisabled"></asp:TextBox>&nbsp;%</TD></TR><TR><TD class="Label" align=left><asp:Label id="Label80" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl2note" runat="server" Width="263px" CssClass="inpText" MaxLength="300"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label119" runat="server" Text="Toleransi susut" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="tolerance" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label78" runat="server" Text="Plan Berat/Unit" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="beratoutput" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox> <asp:DropDownList id="bomdtl2unitoid" runat="server" Width="105px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label79" runat="server" Text="Plan Qty" Visible="False"></asp:Label> </TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="wodtl2qty" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False" MaxLength="16"></asp:TextBox>&nbsp; <asp:DropDownList id="wodtl2unitoid" runat="server" Width="105px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 18px" class="Label" align=left><asp:Label id="Label82" runat="server" Text="Satuan Item" Visible="False"></asp:Label></TD><TD style="HEIGHT: 18px" class="Label" align=center></TD><TD style="HEIGHT: 18px" class="Label" align=left></TD><TD style="HEIGHT: 18px" class="Label" align=left><asp:Label id="Label73" runat="server" Text="Kode Lama Output" Visible="False"></asp:Label></TD><TD style="HEIGHT: 18px" class="Label" align=center></TD><TD style="HEIGHT: 18px" class="Label" align=left><asp:TextBox id="kodelamadtl1" runat="server" Width="274px" CssClass="inpTextDisabled" Visible="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6>&nbsp;<asp:Label id="wipseq" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="i_uwip" runat="server" Visible="False"></asp:Label> <asp:Label id="wipoid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl3seq" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl1seq" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl3reftype" runat="server" Visible="False"></asp:Label> <asp:Label id="itempictureloc" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl1desc" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="addtoListWip" onclick="addtoListWip_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="clearWip" onclick="clearWip_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD style="HEIGHT: 155px" class="Label" align=left colSpan=6><asp:GridView id="GvDtlWip" runat="server" Width="100%" Height="115px" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" OnSelectedIndexChanged="GvDtlWip_SelectedIndexChanged" DataKeyNames="bomdtl3seq" OnRowDataBound="GvDtlWip_RowDataBound" OnRowDeleting="GvDtlWip_RowDeleting">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="bomdtl3seq" HeaderText="Seq">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl1desc" HeaderText="Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3desc" HeaderText="WIP">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty (Biji)" Visible="False"><ItemTemplate>
<asp:TextBox ID="qty" runat="server" CssClass="inpTextDisabled" 
            Enabled="False" OnTextChanged="qty_TextChanged" 
            Text='<%# bind("bomdtl3qty") %>' Width="51px"></asp:TextBox><br /><ajaxToolkit:FilteredTextBoxExtender 
            ID="ftbQty" runat="server" TargetControlID="qty" 
            ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Berat/Unit (gr)" Visible="False"><ItemTemplate>
<asp:TextBox ID="beratperunit" runat="server" 
            CssClass="inpTextDisabled" Enabled="False" 
            OnTextChanged="beratperunit_TextChanged" Text='<%# bind("bomdtl3wgt") %>' 
            Width="50px"></asp:TextBox><br /><ajaxToolkit:FilteredTextBoxExtender 
            ID="ftbberatGV" runat="server" 
            TargetControlID="beratperunit" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="bomdtl3kadaremas" HeaderText="Kadar Emas(%)">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Tolerance (%)" Visible="False"><ItemTemplate>
<asp:TextBox ID="tolerance" runat="server" 
            CssClass="inpTextDisabled" Enabled="False" 
            OnTextChanged="tolerance_TextChanged" Text='<%# bind("bomdtl3tolerance") %>' 
            Width="50px"></asp:TextBox><br /><ajaxToolkit:FilteredTextBoxExtender 
            ID="ftbtoleranceGV" runat="server" 
            TargetControlID="tolerance" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="operator1" HeaderText="OP1" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="operator2" HeaderText="OP2" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="quality" HeaderText="QC" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Picture"><ItemTemplate>
<asp:Image ID="Image3" runat="server" 
            ImageUrl='<%# Eval("itempictureloc") %>'></asp:Image> <br /><asp:LinkButton 
            ID="lbkpict" runat="server" 
            CommandArgument='<%# eval("bomdtl3refoid") %>' ForeColor="Blue" 
            onclick="lbkpict_Click">View</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="tukang" HeaderText="Craftsman" Visible="False">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datestart" HeaderText="Start Date" Visible="False">
<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3unit" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="bomdtl3qty" HeaderText="qty" Visible="False"></asp:BoundField>
<asp:BoundField DataField="bomdtl3wgt" HeaderText="Berat/Unit" Visible="False"></asp:BoundField>
<asp:BoundField DataField="bomdtl3tolerance" HeaderText="tolerance" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:View></asp:MultiView></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnApproval" onclick="btnApproval_Click" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                            <ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image ID="Image1" runat="server" 
                                                ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />Please Wait .....</SPAN>
                                                <BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form SPK :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListWIP" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListWIP" runat="server" Width="700px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListWIP" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of WIP" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListWIP" runat="server" Width="100%" DefaultButton="btnFindListWIP"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center>Filter : <asp:DropDownList id="FilterDDLListWIP" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemcode">Code</asp:ListItem>
<asp:ListItem Value="itemlongdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListWIP" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListWIP" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListWIP" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center><asp:CheckBox id="cbCat01ListWIP" runat="server" Text="Cat. 1 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01ListWIP" runat="server" CssClass="inpText" Width="150px" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02ListWIP" runat="server" Text="Cat. 2 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02ListWIP" runat="server" CssClass="inpText" Width="150px" Visible="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center><asp:CheckBox id="cbCat03ListWIP" runat="server" Text="Cat. 3 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03ListWIP" runat="server" CssClass="inpText" Width="150px" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04ListWIP" runat="server" Text="Cat. 4 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04ListWIP" runat="server" CssClass="inpText" Width="150px" Visible="False"></asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListWIP" runat="server" Width="99%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" OnPageIndexChanging="gvListWIP_PageIndexChanging" OnSelectedIndexChanged="gvListWIP_SelectedIndexChanged" DataKeyNames="itemoid,itemcode,itemlongdesc,kadarcontoh,typeemas">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListWIP" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListWIP" runat="server" TargetControlID="btnHideListWIP" Drag="True" PopupControlID="pnlListWIP" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListWIP"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListWIP" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpListSPK" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListKIK" runat="server" Width="550px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListKIK" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Surat Perintah Kerja" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFindListKIK" runat="server" Width="100%" CssClass="Label" DefaultButton="btnFindListKIK">Filter : <asp:DropDownList id="FilterDDLListKIK" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="wom.wono">SPK No.</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListKIK" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListKIK" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" OnClick="btnFindListKIK_Click"></asp:ImageButton> <asp:ImageButton id="btnAllListKIK" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListKIK" runat="server" Width="99%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" OnPageIndexChanging="gvListKIK_PageIndexChanging" OnSelectedIndexChanged="gvListKIK_SelectedIndexChanged" DataKeyNames="womstoid,wono,wodate">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wono" HeaderText="SPK No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodate" HeaderText="SPK Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                            <asp:Label ID="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="lkbCloseListKIK" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListKIK" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListKIK" runat="server" TargetControlID="btnHideListKIK" PopupControlID="pnlListKIK" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListKIK">
            </ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListSO" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSO" runat="server" CssClass="modalBox" Width="700px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Sales Order" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO">Filter : <asp:DropDownList id="FilterDDLListSO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="som.sono">SO No.</asp:ListItem>
<asp:ListItem Value="cust.custname">Customer</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSO" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" ForeColor="#333333" Width="99%" DataKeyNames="soitemmstoid,soitemno,groupoid,groupdesc" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" OnRowDataBound="gvListSO_RowDataBound" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="soitemno" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemdate" HeaderText="SO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemtype" HeaderText="SO Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderkadaremas" HeaderText="Kadar (%)">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHideListSO" Drag="True" PopupControlID="pnlListSO" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListSO"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSO" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListItem" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListItem" runat="server" Width="700px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListItem" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Output Item" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListItem" runat="server" Width="100%" DefaultButton="btnFindListItem">Filter : <asp:DropDownList id="FilterDDLListItem" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemcode">Code</asp:ListItem>
<asp:ListItem Value="itemdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListItem" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListItem" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListItem" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListItem" runat="server" Width="99%" ForeColor="#333333" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="soitemdtloid,itemoid,itemcode,itemlongdesc,soitemunitoid,soitemqty,bomoid,itemlimitqty,itemres2,precosttotalmatamt,precostdlcamt,precostoverheadamt,precostmarginamt,precostsalesprice,precostoverheadamtidr,precostsalespriceidr,curroid_overhead,curroid_salesprice,lastupduser_bom,lastupdtime_bom,itemoldcode,beratlilin,kadar,typeEmas,beratUnit,berattotal,qtyproduksi" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merek">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty (biji)" Visible="False"><ItemTemplate>
<asp:TextBox id="tbwodtl1qty" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("qtyproduksi") %>' __designer:wfdid="w2" MaxLength="16"></asp:TextBox>&nbsp;<ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w3" TargetControlID="tbwodtl1qty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListItem" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListItem" runat="server" TargetControlID="btnHideListItem" Drag="True" PopupControlID="pnlListItem" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblTitleListItem"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListItem" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat2" runat="server" Width="900px" CssClass="modalBox" Visible="False" DefaultButton="btnFindListMat2"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 896px" class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat2" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Level 5" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 896px" class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat2" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="matrefcode">Code</asp:ListItem>
<asp:ListItem Value="matrefshortdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat2" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat2" onclick="btnFindListMat2_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat2" onclick="btnAllListMat2_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 896px" class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01ListMat" runat="server" Text="Cat. 1 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01ListMat" runat="server" Width="200px" CssClass="inpText" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="FilterDDLCat01ListMat_SelectedIndexChanged"></asp:DropDownList> <asp:CheckBox id="cbCat02ListMat" runat="server" Text="Cat. 2 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02ListMat" runat="server" Width="200px" CssClass="inpText" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="FilterDDLCat02ListMat_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 896px" class="Label" align=center colSpan=3><asp:CheckBox id="cbCat03ListMat" runat="server" Text="Cat. 3 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03ListMat" runat="server" Width="200px" CssClass="inpText" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="FilterDDLCat03ListMat_SelectedIndexChanged"></asp:DropDownList> <asp:CheckBox id="cbCat04ListMat" runat="server" Text="Cat. 4 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04ListMat" runat="server" Width="200px" CssClass="inpText" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 896px" class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD style="WIDTH: 896px" class="Label" align=center colSpan=3><asp:GridView id="gvListMat2" runat="server" Width="99%" ForeColor="#333333" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvListMat2_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbListMat2" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("matrefoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matrefcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrefshortdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Wgt (gr)" Visible="False"><ItemTemplate>
<asp:TextBox id="tbBOMQty" runat="server" CssClass="inpText" Text='<%# eval("bomdtl3qty") %>' Width="50px" MaxLength="16"></asp:TextBox>&nbsp;<ajaxToolkit:FilteredTextBoxExtender id="ftbQtyGV" runat="server" TargetControlID="tbBOMQty" ValidChars="1234567890,.">
</ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbBOMQtybiji" runat="server" CssClass="inpText" Text='<%# eval("bomdtl3qtybiji") %>' Width="50px" MaxLength="16"></asp:TextBox>&nbsp;<ajaxToolkit:FilteredTextBoxExtender id="ftbQtybijiGV" runat="server" TargetControlID="tbBOMQtybiji" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="matrefunit" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Tolerance" Visible="False"><ItemTemplate>
<asp:TextBox id="toleranceMat3" runat="server" CssClass="inpText" Text='<%# eval("tolerance") %>' Width="41px"></asp:TextBox> <asp:Label id="Label86" runat="server" Text="%"></asp:Label><BR /><ajaxToolkit:FilteredTextBoxExtender id="ftbtolerancemast3" runat="server" TargetControlID="toleranceMat3" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbBOMNote" runat="server" CssClass="inpText" Text='<%# eval("bomdtl2note") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="roundqtydtl2" HeaderText="Round Qty" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="Black"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 896px; HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToList" onclick="lkbAddToList_Click" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseListMat2" onclick="lkbCloseListMat2_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat2" runat="server" TargetControlID="btnHideListMat2" Drag="True" PopupControlID="pnlListMat2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblTitleListMat2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat2" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" Width="334px" Visible="False" Height="319px" DefaultButton="btnMsgBoxOK"><TABLE cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="COLOR: black; BACKGROUND-COLOR: gold; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaptionpict" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image11" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD vAlign=top align=left colSpan=2><asp:Image id="Image3" runat="server" Width="300px" Visible="False" Height="200px"></asp:Image></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" Drag="True" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" CausesValidation="False" Width="1px" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

