﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTargetItem.aspx.vb" Inherits="Transaction_trnTargetItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <%--<script type="text/javascript">
    function s()
    {
    try {
        var t = document.getElementById("<%=gvTblData.ClientID%>");
        var t2 = t.cloneNode(true)
        for(i = t2.rows.length -1;i > 0;i--)
        t2.deleteRow(i)
        t.deleteRow(0)
        divgvTblData.appendChild(t2)
        }
    catch(errorInfo) {}
    }
    window.onload = s
function tbRight_onclick() {

}

  </script>--%>

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%" onclick="return tbRight_onclick()">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" oreColor="Maroon" Text=".: TARGET ITEM"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">



                        
<ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
<asp:Panel style="MARGIN-TOP: 4px" id="pnlFind" runat="server" DefaultButton="btnfind" __designer:wfdid="w24"><TABLE><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddlfCabang" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w2"></asp:DropDownList></TD><TD align=left></TD></TR><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w3"><asp:ListItem Value="trntargetno">No</asp:ListItem>
<asp:ListItem Value="trntargetnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFindSJ" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox>&nbsp;</TD><TD align=left><ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w27" PopupButtonID="imbPeriod1" Format="dd/MM/yyyy" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w35" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                        </ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w3"></asp:CheckBox></TD><TD style="HEIGHT: 36px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w28"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton>&nbsp;<asp:Label id="Label16" runat="server" Text="to" __designer:wfdid="w30"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w31"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w32"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w33"></asp:Label>&nbsp;&nbsp; </TD><TD align=left><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w36" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w37"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w38"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem Value="POST">Post</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton style="HEIGHT: 23px" id="btnfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton></TD><TD align=left><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w41" PopupButtonID="imbPeriod2" Format="dd/MM/yyyy" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> </TD></TR><TR><TD align=left colSpan=6><asp:GridView id="gvTblData" runat="server" Width="800px" ForeColor="#333333" __designer:wfdid="w42" PageSize="8" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trntargetoid" DataNavigateUrlFormatString="~\transaction\trntargetitem.aspx?oid={0}" DataTextField="trntargetno" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trntargetdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl Target">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntargetstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntargetnote" HeaderText="Note" HtmlEncode="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" __designer:wfdid="w12" ToolTip='<%# Eval("trntargetoid") %>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" Text='<%# Eval("trntargetoid")%>' __designer:wfdid="w11"></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                            <asp:Label ID="Label3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" BorderStyle="None" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; <BR />
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="gvTblData"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

                        
</ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> List of Target Item</span></strong>&nbsp;<strong><span
                                style="font-size: 9pt">:.</span></strong>
</HeaderTemplate>



                    
</ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">



                        
<ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" EnableTheming="True" __designer:wfdid="w67"><asp:View id="View1" runat="server" __designer:wfdid="w68"><asp:Label id="Label4" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Header" __designer:wfdid="w69"></asp:Label> &nbsp;&nbsp;<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 108px" align=left><asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New" __designer:wfdid="w70"></asp:Label> <asp:Label id="oid" runat="server" Font-Size="X-Small" __designer:wfdid="w71"></asp:Label> </TD><TD style="WIDTH: 2px" align=left></TD><TD align=left colSpan=3>&nbsp;<ajaxToolkit:MaskedEditExtender id="meesend" runat="server" __designer:wfdid="w72" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" TargetControlID="trnBerlakudate"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="WIDTH: 108px" align=left>Cabang</TD><TD style="WIDTH: 2px" align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="DDLPengirim" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w82" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left>No.&nbsp;Target </TD><TD style="WIDTH: 2px" class="Label" align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trnsjjualno" runat="server" Width="187px" CssClass="inpTextDisabled" __designer:wfdid="w78" MaxLength="20" Enabled="False" ReadOnly="True"></asp:TextBox>&nbsp;<asp:Label id="targetOid" runat="server" Font-Size="X-Small" __designer:wfdid="w90" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left>Tanggal&nbsp; </TD><TD style="WIDTH: 2px" class="Label" align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trnBerlakudate" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w1"></asp:TextBox> <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton> <asp:Label id="Label11" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w74"></asp:Label></TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left>Status</TD><TD style="WIDTH: 2px" class="Label" align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="posting" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w86" ReadOnly="True">In Process</asp:TextBox></TD></TR><TR><TD style="WIDTH: 108px" class="Label" vAlign=top align=left>Note</TD><TD style="WIDTH: 2px" class="Label" vAlign=top align=left>:</TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="NoteMst" runat="server" Width="200px" Height="38px" CssClass="inpText" __designer:wfdid="w85" MaxLength="500" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 108px" class="Label" vAlign=top align=left></TD><TD style="WIDTH: 2px" class="Label" vAlign=top align=left></TD><TD vAlign=top align=left colSpan=3>&nbsp;&nbsp; <ajaxToolkit:CalendarExtender id="cesend" runat="server" __designer:wfdid="w73" TargetControlID="trnBerlakudate" PopupButtonID="ImageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><asp:TextBox id="SJdate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w87" Enabled="False" Visible="False"></asp:TextBox><asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w88" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 108px" class="Label" vAlign=top align=left><asp:LinkButton id="LinkButton10" runat="server" CssClass="submenu" Font-Size="Small" Font-Bold="True" ForeColor="Black" __designer:wfdid="w89">Detail</asp:LinkButton> </TD><TD style="WIDTH: 2px" class="Label" vAlign=top align=left></TD><TD align=left colSpan=3>&nbsp;</TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left><asp:Label id="I_u2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" __designer:wfdid="w91"></asp:Label></TD><TD style="WIDTH: 2px" class="Label" align=left></TD><TD align=left colSpan=3><asp:Label id="seqglobal" runat="server" Font-Size="X-Small" __designer:wfdid="w90" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left>Barang <asp:Label id="Label6" runat="server" CssClass="Important" Text="*" __designer:wfdid="w92"></asp:Label> </TD><TD style="WIDTH: 2px" class="Label" align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="itemname" runat="server" Width="219px" Height="16px" CssClass="inpText" __designer:wfdid="w93"></asp:TextBox> <asp:ImageButton id="imbFindItem" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w94"></asp:ImageButton> <asp:ImageButton id="imbClearItem" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w95"></asp:ImageButton> <asp:Label id="itemoid" runat="server" Font-Size="X-Small" __designer:wfdid="w90" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left>&nbsp;</TD><TD style="WIDTH: 2px" class="Label" align=left></TD><TD align=left colSpan=3><asp:GridView id="gvReference" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w99" PageSize="8" GridLines="None" DataKeyNames="itemoid,itemcode,itemdesc,Merk" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="10px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label66" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left>Qty</TD><TD style="WIDTH: 2px" class="Label" align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="QtyTarget" runat="server" Width="72px" CssClass="inpText" __designer:wfdid="w97" AutoPostBack="True" MaxLength="9"></asp:TextBox> </TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left>Amount</TD><TD style="WIDTH: 2px" class="Label" align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="amtTarget" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w48" AutoPostBack="True" MaxLength="12"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 108px" class="Label" vAlign=top align=left>Start&nbsp;Tgl Target</TD><TD style="WIDTH: 2px" class="Label" vAlign=top align=left>:</TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="trntargetdate" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton3" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:ImageButton> <asp:Label id="Label5" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w51"></asp:Label></TD></TR><TR><TD style="WIDTH: 108px" class="Label" vAlign=top align=left>End Tgl&nbsp;Target</TD><TD style="WIDTH: 2px" class="Label" vAlign=top align=left>:</TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="trntargetenddate" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w3" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton4" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD style="WIDTH: 108px" id="TD1" class="Label" vAlign=top align=left runat="server" Visible="false">Periode Target</TD><TD style="WIDTH: 2px" id="TD2" class="Label" vAlign=top align=left runat="server" Visible="false">:</TD><TD id="TD3" vAlign=top align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="periodTarget" runat="server" Width="136px" CssClass="inpTextDisabled" __designer:wfdid="w52" AutoPostBack="True" MaxLength="4"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 108px" class="Label" vAlign=top align=left>Note</TD><TD style="WIDTH: 2px" class="Label" vAlign=top align=left>:</TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="txtLenght" runat="server" Width="221px" Height="31px" CssClass="inpText" __designer:wfdid="w101" AutoPostBack="True" MaxLength="100" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left>&nbsp;</TD><TD style="WIDTH: 2px" class="Label" align=left></TD><TD align=right colSpan=3><asp:ImageButton id="btnAddtolist" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w102"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w103"></asp:ImageButton> </TD><TD style="WIDTH: 11px" align=left></TD><TD align=left></TD></TR><TR><TD style="WIDTH: 108px" class="Label" align=left></TD><TD style="WIDTH: 2px" class="Label" align=left></TD><TD align=left colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="ftQtyTarget" runat="server" __designer:wfdid="w117" TargetControlID="QtyTarget" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftamtTarget" runat="server" __designer:wfdid="w3" TargetControlID="amtTarget" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:CalendarExtender id="cextglTarget" runat="server" __designer:wfdid="w1" TargetControlID="trntargetdate" PopupButtonID="ImageButton3" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cextargetEndDate" runat="server" __designer:wfdid="w6" TargetControlID="trntargetenddate" PopupButtonID="ImageButton4" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left colSpan=5><asp:GridView id="tbldtl" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w104" GridLines="None" DataKeyNames="seqglobal,itemoid,itemname,qty,tgltarget,tglendtarget,amount,notedtl" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seqglobal" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemname" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tgltarget" HeaderText="Start Tgl target">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="30px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tglendtarget" HeaderText="End Tgl Target">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="LinkButton9" runat="server" __designer:wfdid="w2" ToolTip='<%# Eval("seqglobal") %>' CommandName='<%# Eval("qty") %>' CommandArgument='<%# Eval("itemname") %>'>Detail</asp:LinkButton> 
</ItemTemplate>
</asp:TemplateField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="5%"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lbltext" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Create" __designer:wfdid="w105"></asp:Label> &nbsp;By <asp:Label id="createuser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="createuser" __designer:wfdid="w106"></asp:Label> &nbsp;On <asp:Label id="createtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="createtime" __designer:wfdid="w107"></asp:Label> <BR /><asp:Label id="lbltext0" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Last update" __designer:wfdid="w108"></asp:Label> &nbsp; By <asp:Label id="updUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Upduser" __designer:wfdid="w109"></asp:Label> &nbsp;On <asp:Label id="updTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Updtime" __designer:wfdid="w110"></asp:Label> </TD><TD style="WIDTH: 11px" align=left>&nbsp;</TD><TD style="WIDTH: 94px" align=left>&nbsp;</TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w111"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w112"></asp:ImageButton> <asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w113"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w114"></asp:ImageButton> <asp:ImageButton id="imbSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" __designer:wfdid="w115" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrintSJ" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w116" Visible="False"></asp:ImageButton> </TD><TD style="WIDTH: 11px" align=left>&nbsp;</TD><TD style="WIDTH: 94px" align=left>&nbsp;</TD></TR></TBODY></TABLE></asp:View> </asp:MultiView> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnPrintSJ"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

                        
</ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Target Item</span></strong>&nbsp;<strong><span
                                style="font-size: 9pt">:.</span></strong>
</HeaderTemplate>



                    
</ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Visible="False" CssClass="modalMsgBox" DefaultButton="btnMsgBoxOK">
                <table>
                    <tr>
                        <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" CssClass="Important"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Visible="False" CausesValidation="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

