<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptAdjusmentStok.aspx.vb" Inherits="rptAdjusmentStok" Title="Toko Ali - Adjusment Stok" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Adjusment Stok"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE><TBODY><TR><TD id="tdPeriod1" vAlign=top align=left runat="server" visible="true"><asp:Label id="Label6" runat="server" Text="Periode " __designer:wfdid="w5"></asp:Label></TD><TD id="Td1" vAlign=top align=left runat="server" visible="true">:</TD><TD id="tdperiod2" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="dateAwal" runat="server" Width="62px" CssClass="inpText" __designer:wfdid="w6"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton>&nbsp;- <asp:TextBox id="dateAkhir" runat="server" Width="62px" CssClass="inpText" __designer:wfdid="w8" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w10">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w11" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w12" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w13" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w14" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD id="Td2" vAlign=top align=left runat="server" visible="true">PIC</TD><TD id="Td3" vAlign=top align=left runat="server" visible="true">:</TD><TD id="Td4" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="ddlSPG" runat="server" Width="183px" CssClass="inpText" __designer:wfdid="w15"></asp:DropDownList></TD></TR><TR><TD id="Td5" vAlign=top align=left runat="server" visible="true">Cabang</TD><TD id="Td6" vAlign=top align=left runat="server" visible="true">:</TD><TD id="Td7" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="dd_branch" runat="server" Width="184px" CssClass="inpText" __designer:wfdid="w16" AutoPostBack="true"></asp:DropDownList></TD></TR><TR><TD id="Td8" vAlign=top align=left runat="server" visible="true">Lokasi&nbsp;</TD><TD id="Td9" vAlign=top align=left runat="server" visible="true">:</TD><TD id="Td10" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="itemloc" runat="server" Width="184px" CssClass="inpText" __designer:wfdid="w17"></asp:DropDownList></TD></TR><TR><TD vAlign=top align=left>Nama Barang</TD><TD vAlign=top align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="itemname" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w18"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchItem" onclick="imbSearchItem_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w19"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseItem" onclick="imbEraseItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w20"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" __designer:wfdid="w21" Visible="False"></asp:Label> </TD></TR><TR><TD vAlign=top align=right></TD><TD vAlign=top align=right></TD><TD align=left colSpan=3><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w22" PageSize="8" GridLines="None" OnPageIndexChanging="gvItem_PageIndexChanging" CellPadding="4" UseAccessibleHeader="False" DataKeyNames="itemcode,itemoid,itemdesc,merk" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="gvItem_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Maroon" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Item">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Item">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD vAlign=top align=left>Merk</TD><TD vAlign=top align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="Merk" runat="server" Width="141px" CssClass="inpText" __designer:wfdid="w23"></asp:TextBox></TD></TR><TR><TD vAlign=top align=left>Keterangan</TD><TD vAlign=top align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="noteAdj" runat="server" Width="260px" CssClass="inpText" __designer:wfdid="w24"></asp:TextBox></TD></TR><TR><TD align=center colSpan=5><asp:Label id="labelEx" runat="server" Width="98px" __designer:wfdid="w25"></asp:Label></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnreport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> <asp:ImageButton id="ibpdf" onclick="ibpdf_Click1" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w30" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w31"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD align=center colSpan=5><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w32"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" __designer:wfdid="w33" ShowAllPageIds="True" AutoDataBind="true"></CR:CrystalReportViewer> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="ibpdf"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel2" runat="server"><contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w31" Visible="False"><TABLE width=250><TBODY><TR><TD style="WIDTH: 294px; HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w32"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=250><TBODY><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png" __designer:wfdid="w33"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red" __designer:wfdid="w34"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD><asp:ImageButton id="btnErrOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" __designer:wfdid="w35"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" __designer:wfdid="w36" TargetControlID="btnExtender" Drag="True" DropShadow="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" __designer:wfdid="w37" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

