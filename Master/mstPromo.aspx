<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstPromo.aspx.vb" Inherits="Master_mstPromo" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Master Promo"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div style="width: 100%; text-align: left">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" class="Label">
                                                Filter</td>
<td align="left" class="Label">
:</td>
<td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="promeventname">Promo Name</asp:ListItem>
                                                    <asp:ListItem Value="promeventcode">Code</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="Label">
                                                <asp:Label ID="Label18" runat="server" Text="Type Promo"></asp:Label>
                                            </td>
                                            <td align="left" class="Label">
                                                :</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="promotypeddl" runat="server" CssClass="inpText" Width="126px">
                                                    <asp:ListItem>ALL</asp:ListItem>
                                                    <asp:ListItem>Buy x Get y</asp:ListItem>
                                                    <asp:ListItem>Buy nX get nY</asp:ListItem>
                                                    <asp:ListItem>BUNDLING</asp:ListItem>
                                                    <asp:ListItem>Disc % (Item Only)</asp:ListItem>
                                                    <asp:ListItem Enabled="False">Buy 1 Get 1</asp:ListItem>
                                                    <asp:ListItem Value="DISCAMT">Disc. Amount (Rp.)</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="height: 10px;">
                                                Status</td>
<td align="left" style="height: 10px">
:</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                                    <asp:ListItem Value="INACTIVE">INACTIVE</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsBottom" />
                                                <asp:ImageButton ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsBottom" /></td>
                                        </tr>
<tr>
<td align="left" colspan="6" style="height: 10px">
                                        <asp:GridView ID="GVMst" runat="server" CellPadding="4" AutoGenerateColumns="False"
                                            Width="100%" ForeColor="#333333" GridLines="None" AllowPaging="True" PageSize="8">
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" Font-Bold="True" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="promoid" DataNavigateUrlFormatString="mstPromo.aspx?oid={0}"
                                                    DataTextField="promeventcode" HeaderText="Kode">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Font-Bold="True" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" CssClass="gvhdr" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="promeventname" HeaderText="Promo">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" CssClass="gvhdr" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promtype" HeaderText="Type">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promstartdate" HeaderText="Start Date">
                                                    <ItemStyle Font-Size="X-Small" Width="100px" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="X-Small" Width="100px" CssClass="gvhdr" HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promfinishdate" HeaderText="End Date">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promstarttime" HeaderText="Start Time">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promfinishtime" HeaderText="End Time">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" CssClass="gvhdr" />
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
</td>
</tr>
                                    </table>
                                </asp:Panel>
&nbsp;
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List Master Promo :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:Label id="Label48" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Master Promo" Font-Underline="False"></asp:Label> <asp:Label id="lblamp" runat="server" Visible="False"></asp:Label> <asp:Label id="promoid" runat="server" Visible="False"></asp:Label> <TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 90px" class="Label" align=left>Code&nbsp;<asp:Label id="Label28" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="promeventcode" runat="server" CssClass="inpTextDisabled" MaxLength="20"></asp:TextBox> <asp:Button id="btnAMP" onclick="btnSales_Click" runat="server" CssClass="inpText" Text="Get Data AMP" BorderWidth="1px" BorderColor="Maroon"></asp:Button></TD><TD class="Label" align=left>Promo Name&nbsp;<asp:Label id="Label55" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="promeventname" runat="server" Width="200px" CssClass="inpText" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Start Date&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="promstartdate" runat="server" Width="75px" CssClass="inpText" MaxLength="6"></asp:TextBox>&nbsp;<asp:ImageButton id="ibstartdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle">
                                                        </asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label>&nbsp; </TD><TD class="Label" align=left>End Date&nbsp;<asp:Label id="Label57" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="promfinishdate" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibfinishdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle">
                                                        </asp:ImageButton>&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label>&nbsp; </TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Start Time&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="promstarttime" runat="server" Width="75px" CssClass="inpText" MaxLength="5"></asp:TextBox> </TD><TD class="Label" align=left>End Time&nbsp;<asp:Label id="Label6" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="promfinishtime" runat="server" Width="58px" CssClass="inpText" MaxLength="5"></asp:TextBox> </TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Type</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlPromtype" runat="server" CssClass="inpText" AutoPostBack="True"><asp:ListItem>Buy x Get y</asp:ListItem>
<asp:ListItem>Buy nX get nY</asp:ListItem>
<asp:ListItem>BUNDLING</asp:ListItem>
<asp:ListItem>Disc % (Item Only)</asp:ListItem>
<asp:ListItem Enabled="False">Buy 1 Get 1</asp:ListItem>
<asp:ListItem Value="DISCAMT">Disc. Amount (Rp.)</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=3><asp:DropDownList id="membercardrole" runat="server" CssClass="inpText" Visible="False"><asp:ListItem>None</asp:ListItem>
<asp:ListItem>Plus</asp:ListItem>
<asp:ListItem>Not Define</asp:ListItem>
<asp:ListItem>Choose</asp:ListItem>
</asp:DropDownList><asp:Label id="status" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=5 rowSpan=1><asp:TextBox id="note" runat="server" Width="400px" CssClass="inpText" MaxLength="150"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Day</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=5 rowSpan=1><asp:CheckBoxList id="cbDay" runat="server" ForeColor="White" BackColor="#FFFFC0" BorderColor="Black" CellPadding="5" RepeatColumns="4" RepeatDirection="Horizontal"><asp:ListItem Selected="True" Value="1">Sunday</asp:ListItem>
<asp:ListItem Selected="True" Value="2">Monday</asp:ListItem>
<asp:ListItem Selected="True" Value="3">Tuesday</asp:ListItem>
<asp:ListItem Selected="True" Value="4">Wednesday</asp:ListItem>
<asp:ListItem Selected="True" Value="5">Thursday</asp:ListItem>
<asp:ListItem Selected="True" Value="6">Friday</asp:ListItem>
<asp:ListItem Selected="True" Value="7">Saturday</asp:ListItem>
</asp:CheckBoxList></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=5 rowSpan=1></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=5 rowSpan=1><DIV style="OVERFLOW-Y: scroll; WIDTH: 61%; HEIGHT: 143px; BACKGROUND-COLOR: beige"><asp:CheckBoxList id="cbCabang" runat="server" Width="528px" BackColor="#FFFFC0" BorderColor="Black" CellPadding="5" RepeatColumns="4" RepeatDirection="Horizontal">
</asp:CheckBoxList></DIV></TD></TR></TBODY></TABLE><asp:Label id="Label14" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Item Utama" Font-Underline="True"></asp:Label><BR /><TABLE id="tblMenu" width="100%" runat="server" visible="false"><TBODY><TR><TD class="Label" colSpan=10><asp:Label id="I_u2" runat="server" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label> <asp:Label id="lblmenuygada" runat="server"></asp:Label> <asp:Label id="TypeBarang" runat="server" Visible="False"></asp:Label> <asp:Label id="seq" runat="server" Visible="False"></asp:Label> <asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Katalog&nbsp;Utama <asp:Label id="Label11" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="itemdesc1" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:ImageButton id="imbFindMenu1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearMenu1" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label17" runat="server">Price Katalog</asp:Label>&nbsp;</TD><TD class="Label" align=left><asp:TextBox id="amountdisc1" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True">0.00</asp:TextBox></TD><TD class="Label" align=left colSpan=6>&nbsp;</TD></TR><TR><TD class="Label" align=left>Qty Utama</TD><TD class="Label" align=left><asp:TextBox id="Qty1" runat="server" Width="40px" CssClass="inpText" AutoPostBack="True">1.00</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label16" runat="server" Width="74px" Visible="False">Price Promo</asp:Label>
    <asp:Label id="Label20" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="Subsidi" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True">0.00</asp:TextBox></TD><TD class="Label" align=left colSpan=6></TD></TR><TR><TD id="TD4" class="Label" align=left runat="server" Visible="true"><asp:Label id="DiscNyaLbl" runat="server" Visible="False">Disc/Unit</asp:Label></TD><TD id="TD8" class="Label" align=left runat="server" Visible="true"><asp:TextBox id="DiscNya" runat="server" Width="80px" CssClass="inpText" Visible="False" AutoPostBack="True">0.00</asp:TextBox> </TD><TD id="TD2" class="Label" align=left runat="server" Visible="true"></TD><TD id="TD5" class="Label" align=left runat="server" Visible="true"></TD><TD class="Label" align=left colSpan=6 Visible="true"><asp:Button id="BtnAdd" runat="server" CssClass="btn green" Font-Bold="True" ForeColor="White" Text="Add To List"></asp:Button></TD></TR><TR><TD id="Td3" class="Label" align=left colSpan=10 runat="server" Visible="true"><asp:Label id="Label8" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Item Bonus" Font-Underline="True"></asp:Label> <asp:Label id="itemoid2" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="LblItem2" runat="server">katalog Bonus</asp:Label>&nbsp;<asp:Label id="Label13" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="itemdesc2" runat="server" Width="200px" CssClass="inpText" MaxLength="6" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMenu2" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearMenu2" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;</TD><TD class="Label" align=left><asp:Label id="LblPriceBrg2" runat="server" Width="73px">Price Katalog</asp:Label> <asp:Label id="Label1" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="AmtDisc2" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True">0</asp:TextBox></TD><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left><asp:Label id="LblPricePorm2" runat="server" Width="74px" Visible="False">Price Promo</asp:Label></TD><TD class="Label" align=left><asp:TextBox id="Subsidi2" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="LblQty2" runat="server" Width="60px">Qty Bonus</asp:Label></TD><TD class="Label" align=left><asp:TextBox id="Qty2" runat="server" Width="40px" CssClass="inpText" AutoPostBack="True"></asp:TextBox> </TD><TD class="Label" align=left colSpan=6><asp:ImageButton id="BtnAddList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" AlternateText="Add to List"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=10><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 178px; BACKGROUND-COLOR: beige"><asp:GridView id="gvPromoDtl" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" GridLines="None" AutoGenerateColumns="False" DataKeyNames="itemoid" EmptyDataText="No Data">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typebarang" HeaderText="Type Katalog" SortExpression="typebarang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty1" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amountdisc" HeaderText="Price katalog">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Subsidi" HeaderText="Price Promo">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="discnya" HeaderText="Discount">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalamt" HeaderText="Total Amt">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalcashback" HeaderText="Total Cashback">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Bold="True" Width="2px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="qty2" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemdesc2" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data Detail katalog belum di input..!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD class="Label" align=left colSpan=10><asp:Label id="Label10" runat="server" Font-Size="Large" Font-Bold="True" Text="Total Amount : Rp. "></asp:Label> <asp:Label id="totKotor" runat="server" Font-Size="Large" Font-Bold="True" Text="0.00"></asp:Label>&nbsp;<asp:Label id="Label19" runat="server" Font-Size="Large" Font-Bold="True" Text=",-"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=10>Last Update On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858"></asp:Label> By <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858"></asp:Label> </TD></TR><TR><TD class="Label" align=left colSpan=10><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" CausesValidation="False" CommandName="Update"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CommandName="Cancel"></asp:ImageButton> <asp:ImageButton id="BtnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" CommandName="Delete"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=10><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />LOADING....</SPAN><BR /></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="ceStartDate" runat="server" TargetControlID="promstartdate" Format="dd/MM/yyyy" PopupButtonID="ibstartdate"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ceFinishDate" runat="server" TargetControlID="promfinishdate" Format="dd/MM/yyyy" PopupButtonID="ibfinishdate"></ajaxToolkit:CalendarExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEQty1" runat="server" TargetControlID="Qty1" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FETqty2" runat="server" TargetControlID="Qty2" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTESubsidi" runat="server" TargetControlID="Subsidi" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEDiscNya" runat="server" TargetControlID="DiscNya" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTESubsidi2" runat="server" TargetControlID="Subsidi2" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="MEpromstartdate" runat="server" Enabled="true" TargetControlID="promstartdate" MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MEpromstarttime" runat="server" Enabled="true" TargetControlID="promstarttime" MaskType="Time" Mask="99:99:99" CultureName="en-US" AcceptAMPM="True" UserTimeFormat="TwentyFourHour"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEAmtDisc2" runat="server" TargetControlID="AmtDisc2" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="MEpromfinishdate" runat="server" Enabled="true" TargetControlID="promfinishdate" MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MEpromfinishtime" runat="server" TargetControlID="promfinishtime" MaskType="Time" Mask="99:99:99" AcceptAMPM="True" UserTimeFormat="TwentyFourHour"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEamountdisc1" runat="server" TargetControlID="amountdisc1" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEpergroupdisc" runat="server" TargetControlID="pergroupdisc" FilterType="Numbers">
                                                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEpersubgroupdisc" runat="server" TargetControlID="persubgroupdisc" FilterType="Numbers">
                                                    </ajaxToolkit:FilteredTextBoxExtender>&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD class="Label" align=left colSpan=10><TABLE style="WIDTH: 100%" id="tblGroup" runat="server"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label9" runat="server" Text="Per : " font-size="X-Small"></asp:Label> <asp:DropDownList id="ddlInsertType" runat="server" CssClass="inpText" AutoPostBack="true"><asp:ListItem>Item</asp:ListItem>
<asp:ListItem Value="GROUP">Group Item</asp:ListItem>
<asp:ListItem Value="SUBGROUP">Sub Group Item</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Per&nbsp;Group</TD><TD class="Label" align=left><asp:DropDownList id="ddlGroup" runat="server" CssClass="inpText">
                                                    </asp:DropDownList></TD><TD class="Label" align=left width=100>Discount</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="pergroupdisc" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> (%)</TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" id="tblSubGroup" runat="server" visible="false"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Width="105px" Text="Per Sub Group"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="ddlSubGroup" runat="server" CssClass="inpText">
                                                    </asp:DropDownList></TD><TD class="Label" align=left width=100>Discount</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="persubgroupdisc" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> (%)</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
<asp:Panel id="PanelUser" runat="server" Width="600px" CssClass="modalBox" Visible="False" DefaultButton="btnSearchItem"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="lblListUser" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Katalog"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center">Item&nbsp;:<asp:DropDownList id="ddlFkatalog" runat="server" CssClass="inpText"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="filterMenuItem" runat="server" Width="237px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle">
                                                                    </asp:ImageButton> <asp:ImageButton id="btnListItem" onclick="btnListItem_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblstate" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD><asp:GridView id="gvMenu" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" DataKeyNames="itemoid,itemdesc,pricelist" AutoGenerateColumns="False" GridLines="None" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="lblmsg3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="lbCloseMenu" runat="server">[Close]</asp:LinkButton></TD></TR><TR><TD style="TEXT-ALIGN: center">&nbsp;</TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MpeUser" runat="server" TargetControlID="BtnUser" PopupControlID="PanelUser" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListUser" Drag="True">
                                                </ajaxToolkit:ModalPopupExtender> <asp:Button id="BtnUser" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
                                        </asp:UpdatePanel><asp:UpdatePanel id="UpdatePanelPerson" runat="server"><contenttemplate>
<asp:Panel id="PanelPerson" runat="server" Width="700px" CssClass="modalBox" Visible="False" DefaultButton="btnFindPerson"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="LabePerson" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of AMP"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label" align=left>Filter : <asp:DropDownList id="DDLFilterPerson" runat="server" CssClass="inpText"><asp:ListItem Value="personname">Person Name</asp:ListItem>
<asp:ListItem Value="nip">N.I.P</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterPerson" runat="server" Width="184px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindPerson" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllPerson" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=left><asp:GridView id="GVPerson" runat="server" Width="100%" Height="31px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" CellPadding="4" EmptyDataText="No data in database." DataKeyNames="nip,personname,personoid,divisi" AutoGenerateColumns="False" GridLines="None" PageSize="8" AllowPaging="True" OnSelectedIndexChanged="GVPerson_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="Red" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="nip" HeaderText="NIP">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="AMP Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="40%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divisi" HeaderText="Jabatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="No Person on List"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="LinkButton4" onclick="LinkButton4_Click" runat="server">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="ButtonPerson" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="MPEPerson" runat="server" TargetControlID="ButtonPerson" Drag="True" PopupDragHandleControlID="LabelPerson" BackgroundCssClass="modalBackground" PopupControlID="PanelPerson">
                </ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Master Promo :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD vAlign=top><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="PanelValidasi" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False">
            </asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
