<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstWilayah.aspx.vb" Inherits="Master_mstWilayah" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">

<%--<script type="text/javascript">
    function s()
    {
    try {
        var t = document.getElementById("<%=GVmst.ClientID%>");
        var t2 = t.cloneNode(true)
        for(i = t2.rows.length -1;i > 0;i--)
        t2.deleteRow(i)
        t.deleteRow(0)
        divTblData.appendChild(t2)
        }
    catch(errorInfo) {}
    }
    window.onload = s
  </script>--%>
 <table id="tableutama" align="center" border="1" cellpadding="3" cellspacing="0"
        width="100%" class="tabelhias">
        <tr style="font-size: 8pt; color: #000099">
            <th class="header" colspan="3" valign="middle">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Data Region"></asp:Label><br />
                <asp:SqlDataSource ID="SDSMatType" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                    SelectCommand="SELECT genoid, gendesc FROM QL_mstgen WHERE (gengroup LIKE 'MATSUBCATEGORY%')" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="Label" style="font-size: 11px; font-weight: normal;"> Filter : </td>
                                        <td align="left" colspan="5">
                                        <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                            <asp:ListItem Value="regioncode">Region Code</asp:ListItem>
                                            <asp:ListItem Value="regionname">Region Name</asp:ListItem>
                                            <asp:ListItem Value="regiondesc">Description</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="150px" CssClass="inpText"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" />
                                        <asp:ImageButton
                                            ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" CausesValidation="False" />
                                            <asp:ImageButton ID="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="Label" colspan="6">
                            <fieldset  id="field1" style="width: 99%; text-align: left; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"  ><div id='divTblData'>
 <asp:GridView id="GVmst" runat="server" Width="100%" AllowPaging="True" EmptyDataText="No data in database." AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
<Columns>
    <asp:HyperLinkField DataNavigateUrlFields="regionoid" DataNavigateUrlFormatString="~\master\MstWilayah.aspx?Oid={0}"
        DataTextField="regioncode" HeaderText="Kode Region">
        <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:HyperLinkField>
    <asp:BoundField DataField="regionname" HeaderText="Name Region">
        <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
    <asp:BoundField DataField="regiondesc" HeaderText="Description">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
</Columns>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" Font-Bold="True"></SelectedRowStyle>

<PagerStyle ForeColor="#333333" HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True"></PagerStyle>

<HeaderStyle BackColor="#990000" ForeColor="White" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>
                                &nbsp;</div>
    </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>&nbsp; &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate><img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>
                            :: List of Region</strong></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate><img align="absMiddle" alt="" src="../Images/corner.gif" id="Img2" />
                            <span style="font-size: 9pt"><strong>:: Form Region</strong></span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 968px; HEIGHT: 80px"><TBODY><TR><TD style="WIDTH: 77px" class="Label" align=left>Code Region</TD><TD style="WIDTH: 10px" class="Label" align=left>:</TD><TD style="WIDTH: 187px" class="Label" align=left><asp:TextBox id="regioncode" runat="server" Width="81px" CssClass="inpTextDisabled" Text='<%# Bind("ROLENAME", "{0}") %>' __designer:wfdid="w632" MaxLength="30" size="20" ReadOnly="True"></asp:TextBox> <asp:Label id="i_u" runat="server" ForeColor="Red" __designer:wfdid="w633"></asp:Label></TD><TD class="Label" align=left></TD><TD style="WIDTH: 100px" class="Label" align=left>Nama Region&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w634"></asp:Label></TD><TD style="WIDTH: 4px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="regionname" runat="server" Width="149px" CssClass="inpText" Text='<%# Bind("ROLENAME", "{0}") %>' __designer:wfdid="w635" MaxLength="30" size="20"></asp:TextBox></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 77px; HEIGHT: 21px" class="Label" align=left>Deskripsi&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w636"></asp:Label></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 10px; HEIGHT: 21px" class="Label" align=left>:</TD><TD style="WIDTH: 187px; HEIGHT: 21px" class="Label" align=left><asp:TextBox id="regiondesc" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w637" MaxLength="50"></asp:TextBox></TD><TD style="HEIGHT: 21px" class="Label" align=left></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 100px; HEIGHT: 21px" class="Label" align=left>Tarif <asp:Label id="Label8" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w638"></asp:Label></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 4px; HEIGHT: 21px" class="Label" align=left>:</TD><TD style="HEIGHT: 21px" class="Label" align=left><asp:TextBox id="txtTarif" runat="server" CssClass="inpText" __designer:wfdid="w639" MaxLength="14"></asp:TextBox></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 77px" class="Label" align=left></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 10px" class="Label" align=left></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 187px" class="Label" align=left></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 3px" class="Label" align=left></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 100px" class="Label" align=left></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 4px" class="Label" align=left></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeTarif" runat="server" __designer:wfdid="w640" TargetControlID="txtTarif" InputDirection="RightToLeft" Mask="9,999,999.99" MaskType="Number"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left colSpan=5><asp:Label id="Label4" runat="server" Font-Size="13px" Font-Bold="True" Text="Region Detail :" __designer:wfdid="w641" Font-Underline="True"></asp:Label>&nbsp; <asp:DropDownList id="dllarea" runat="server" CssClass="inpText" __designer:wfdid="w642"></asp:DropDownList> <asp:ImageButton id="imbAdd" onclick="imbAdd_Click" runat="server" ImageUrl="~/Images/Add.png" ImageAlign="AbsMiddle" __designer:wfdid="w643"></asp:ImageButton></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px; WIDTH: 4px" class="Label" align=left colSpan=1></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left colSpan=7><asp:GridView style="BACKGROUND-COLOR: transparent" id="GVRole" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w644" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="30px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="30px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="areacode" HeaderText="Area Code">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="areadesc" HeaderText="Area Name">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="800px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="800px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Delete" Visible="False"><HeaderTemplate>
&nbsp;
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbDeleteShipping" runat="server" __designer:wfdid="w3"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:CommandField DeleteText="X" EditText="edit" ShowDeleteButton="True">
<ControlStyle BorderColor="White" ForeColor="Red" Width="10px"></ControlStyle>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblNodataShipping" runat="server" CssClass="Important" Text="Data Not Found  !!" __designer:wfdid="w4"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left colSpan=7><asp:Label id="I_U5" runat="server" CssClass="Important" Text="New Detail" __designer:wfdid="w652" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left colSpan=7><asp:Label id="lblUpd" runat="server" Text="Create" __designer:wfdid="w656"></asp:Label>&nbsp;by&nbsp;<asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w657"></asp:Label>&nbsp;on&nbsp;&nbsp;<asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w658"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left colSpan=7><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w649" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w650" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w651" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp; &nbsp;&nbsp;
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKValidasi_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

