<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="uploadinit.aspx.vb" Inherits="Master_uploadinit" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: UPLOAD INIT STOCK" CssClass="Title" Font-Italic="False" Font-Size="21px" ForeColor="Maroon"></asp:Label><asp:SqlDataSource ID="SDSList"
                    runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>"></asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=left colSpan=2>Upload Excel : <asp:FileUpload id="fuXls" runat="server" CssClass="inpText" __designer:wfdid="w15"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnXls" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton><asp:Button id="PrintExl" runat="server" CssClass="green" Font-Bold="True" ForeColor="White" Text="Download Excelnya" __designer:wfdid="w17"></asp:Button> <asp:Button id="BtnListGdg" runat="server" CssClass="green" Font-Bold="True" ForeColor="White" Text="List Gudang" __designer:wfdid="w99"></asp:Button>&nbsp; <asp:LinkButton id="LinkClose" runat="server" __designer:wfdid="w103" Visible="False" OnClick="LinkClose_Click">Close Lis Gudang</asp:LinkButton></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="GvGudang" runat="server" Width="50%" ForeColor="#333333" __designer:wfdid="w7" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="genoid" HeaderText="ID Gudang">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="ID Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="lblEmptyDetail" runat="server" CssClass="Important" Text="Data tidak ditemukan !!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" align=left colSpan=2>&nbsp;<asp:Label id="stockadjno" runat="server" __designer:wfdid="w19" Visible="False"></asp:Label> <asp:Label id="resfield1" runat="server" __designer:wfdid="w20" Visible="False"></asp:Label> <asp:Label id="lblError" runat="server" __designer:wfdid="w18" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 178px"><asp:GridView id="gvDtl" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w7" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="nomor" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="ID Item">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Item Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="ID Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="hpp" HeaderText="HPP">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldo" HeaderText="Saldo Awal ">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Font-Strikeout="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" ShowDeleteButton="True" ButtonType="Image" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="lblEmptyDetail" runat="server" CssClass="Important" Text="Data tidak ditemukan !!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD class="Label" align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w22" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w23"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=2>By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w38"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w39"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:ImageButton id="btnSave" onclick="btnSave_Click1" runat="server" ImageUrl="~/Images/Posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w24" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w25" AlternateText="Cancel"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=2><asp:Label id="Label6" runat="server" Text="Put Modal PopUp here" __designer:wfdid="w28" Visible="False"></asp:Label></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnXls"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="PrintExl"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Upload Adjusment :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" __designer:wfdid="w29" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow" __designer:wfdid="w30"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black" __designer:wfdid="w31"></asp:Label></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w32"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black" __designer:wfdid="w33"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" __designer:wfdid="w34" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w35"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" __designer:wfdid="w36" DropShadow="True" PopupControlID="PanelMsgBox" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="beMsgBox">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" __designer:wfdid="w37" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</td>
        </tr>
    </table>
</asp:Content>

