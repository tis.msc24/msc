Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports ClassProcedure
Imports Koneksi
Imports System.Globalization
Imports System.IO
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Windows.Forms

'==================================
'Last Update Programmer by Shutup_M
'==================================

Partial Class Transaction_trnReturPI
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As ReportDocument
#End Region

#Region "Procedure"
    Private Sub BindSupp()
        sSql = "Select Distinct * from (SELECT DISTINCT s.suppoid , s.suppcode , s.suppname , s.suppaddr FROM QL_trnsjbelimst a INNER JOIN QL_pomst P ON A.trnbelipono = P.trnbelipono inner join QL_mstsupp s on a.cmpcode=s.cmpcode AND P.trnsuppoid=s.suppoid INNER JOIN QL_trnbelimst b ON b.trnbelipono = a.trnbelipono where b.trnbelistatus = 'POST' and (s.suppcode like '%" & Tchar(suppname.Text.Trim) & "%' or s.suppname like '%" & Tchar(suppname.Text.Trim) & "%') AND a.branch_code='" & CabangNya.SelectedValue & "' UNION ALL Select s.suppoid , s.suppcode , s.suppname , s.suppaddr from QL_mstsupp s Inner Join QL_trnbelimst b ON b.trnsuppoid=s.suppoid AND trnbelimstoid<0 and (s.suppcode like '%" & Tchar(suppname.Text.Trim) & "%' or s.suppname like '%" & Tchar(suppname.Text.Trim) & "%') AND b.branch_code='" & CabangNya.SelectedValue & "') sp"
        ViewState("trpurchasereturn") = cKon.ambiltabel(sSql, "trpurchasereturn")
        GVTr.DataSource = ViewState("trpurchasereturn")
        GVTr.DataBind() : GVTr.PageIndex = 0
    End Sub

    Private Sub binddataTR()
        sSql = "select a.trnTrfToReturoid, a.trnTrfToReturNo, a.trnsjbelino, a.trnbelipono, b.trntaxpct , a.trntrfdate, a.flag, b.trnpaytype, c.suppoid, c.suppname, (select distinct op.trnbelino from QL_trnbelidtl qa inner join QL_trnbelimst op on qa.cmpcode = op.cmpcode and qa.trnbelimstoid = op.trnbelimstoid where qa.trnsjbelino = a.trnsjbelino) trnbelino from ql_trnTrfToReturmst a inner join QL_pomst b on a.cmpcode = b.cmpcode and a.trnbelipono = b.trnbelipono inner join QL_mstsupp c on b.cmpcode = c.cmpcode and b.trnsuppoid = c.suppoid where a.cmpcode = '" & CompnyCode & "' and a.flag <> 'service' and a.status = 'post' and a.trnTrfToReturNo like '%" & Tchar(notr.Text) & "%' and ((select sum(v.qty_to) from ql_trnTrfToReturDtl v inner join ql_trnTrfToReturmst w on v.cmpcode = w.cmpcode and v.trnTrfToReturoid = w.trnTrfToReturoid inner join QL_mstItem x on v.cmpcode = x.cmpcode and v.refoid = x.itemoid and v.refname = 'ql_mstitem' where w.cmpcode = a.cmpcode and w.trnTrfToReturNo = a.trnTrfToReturNo)-(select isnull(sum(case m.unitseq when 1 then (cast(o.konversi1_2 as decimal(18,2))*cast(o.konversi2_3 as decimal(18,2))*m.trnbelireturdtlqty) when 2 then (cast(o.konversi2_3 as decimal(18,2))*m.trnbelireturdtlqty) when 3 then (m.trnbelireturdtlqty) end),0) from QL_trnbeliReturdtl m inner join QL_trnbeliReturmst n on m.cmpcode = n.cmpcode and m.trnbeliReturmstoid = n.trnbeliReturmstoid and n.trnbelitype = 'grosir' inner join QL_mstItem o on m.cmpcode = o.cmpcode and m.itemoid = o.itemoid where n.cmpcode = a.cmpcode and n.trnTrfToReturNo = a.trnTrfToReturNo)-(select isnull(sum(ab.qty_to), 0) from ql_trnTrfFromReturDtl ab inner join ql_trnTrfFromReturmst bc on ab.cmpcode = bc.cmpcode and ab.trnTrfFromReturoid = bc.trnTrfFromReturoid where bc.cmpcode = a.cmpcode and bc.trnTrfToReturNo = a.trnTrfToReturNo and bc.status='POST')) > 0"

        ViewState("trpurchasereturn") = cKon.ambiltabel(sSql, "trpurchasereturn")
        GVTr.DataSource = ViewState("trpurchasereturn")
        GVTr.DataBind()
        'GVTr.Visible = True
        GVTr.PageIndex = 0
    End Sub

    Private Sub DDLBranch()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(CabangNya, sSql)
                'CabangNya.Items.Add(New ListItem("ALL", "ALL"))
                'CabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
            'CabangNya.Items.Add(New ListItem("ALL", "ALL"))
            'CabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DdlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DdlCabang, sSql)
            Else
                FillDDL(DdlCabang, sSql)
                DdlCabang.Items.Add(New ListItem("ALL", "ALL"))
                DdlCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DdlCabang, sSql)
            DdlCabang.Items.Add(New ListItem("ALL", "ALL"))
            DdlCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub Bindalldata(ByVal sWhere As String)
        sSql = "select a.trnbeliReturmstoid, a.trnbelireturno, a.trnbelidate, a.trnTrfToReturNo, a.trnbelidate, a.trnsuppoid suppoid,c.suppname, (Select d.trnpaytype from QL_pomst d Where a.trnbelipono = d.trnbelipono AND c.suppoid=a.trnsuppoid) trnpaytype, a.refnotaretur, a.amtbeli, a.trntaxpct, a.trnamttax, a.amtdischdr, a.trnbelidisctype, a.trnbelidisc, a.amtbelinetto, a.trnbelistatus, a.trnbelinote,a.trnbelino,a.branch_code,ISNULL((Select sm.frombranch from ql_trnsjbelimst sm Where sm.trnsjbelino=a.trnsjbelino AND a.branch_code=sm.frombranch),branch_code) frombranch,a.trnsjbelino From QL_trnbeliReturmst a inner join QL_mstsupp c on c.suppoid=a.trnsuppoid Where a.cmpcode = 'MSC' and a.trnbelitype = 'GROSIR' " & sWhere & ""

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND a.upduser='" & Session("UserID") & "'"
        End If

        If DdlCabang.SelectedValue <> "ALL" Then
            sSql &= " AND a.branch_code='" & DdlCabang.SelectedValue & "'"
        End If

        sSql &= " order by a.trnbelireturno"
        ViewState("purchaseretur") = Nothing
        ViewState("purchaseretur") = cKon.ambiltabel(sSql, "purchaseretur")
        GVTrn.DataSource = ViewState("purchaseretur")
        GVTrn.DataBind()
    End Sub

    Private Sub InvBindData()
        cariIv.Visible = True : Dim top As String = ""
        Dim uCabang As String = "" : Dim StatusNya As String = ""
        If TypeNya.SelectedValue.ToUpper = "NORMAL" Then
            StatusNya = " AND StatusNya='Belum'"
        Else
            StatusNya = " AND StatusNya='LUNAS'"
        End If

        If suppoid.Text = "" Then
            showMessage("Maaf, Pilih Supplier terlebih dahulu!", 2)
            Exit Sub
        End If

        If invoiceno.Text.Trim.ToUpper = "" Then
            top = "TOP 200"
        End If
        sSql = "Select " & top & " * from (Select bm.trnbelimstoid, bm.trnbelino, bm.trnbelipono, p.trnpaytype, p.trntaxpct, bd.trnsjbelioid, bd.trnsjbelino, ISNULL(SUM(trnbelidtlqty),0.00) QtyItem, ISNULL((Select SUM(trnbelireturdtlqty) From QL_trnbeliReturdtl rd Inner Join QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid AND rm.trnsjbelino=bd.trnsjbelino AND rm.trnbelistatus IN ('Approved','POST') AND rd.trnbelireturdtlflag='Approved'),0.00) QtyRetur, ISNULL(SUM(trnbelidtlqty),0.00)-ISNULL((Select SUM(trnbelireturdtlqty) From QL_trnbeliReturdtl rd Inner Join QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid AND rm.trnsjbelino=bd.trnsjbelino Where rd.trnbelireturdtlflag='Approved' AND rm.trnbelipono=bm.trnbelipono AND rm.trnbelino=bm.trnbelino AND rm.trnbelistatus IN ('Approved','POST') AND rd.trnbelireturdtlflag='Approved'),0.00) QtyRtr, bm.branch_code, bd.to_branch_code ToBranch, bm.amtbelinetto, bm.accumpaymentidr, Case When (bm.amtbelinetto-bm.accumpaymentidr)=0 Then 'Lunas' Else 'Belum' End StatusNya, bm.trnbelistatus StatusPI, bm.trnsuppoid, bm.amtbelinetto-ISNULL((Select SUM(rm.amtbelinetto) from QL_trnbeliReturmst rm Where rm.trnbelimstoid=bm.trnbelimstoid AND rm.branch_code=bm.branch_code),0.0000) AmtSisa From QL_trnbelimst bm Inner Join QL_pomst p ON p.trnbelipono=bm.trnbelipono Inner Join QL_trnbelidtl bd ON bd.trnbelimstoid=bm.trnbelimstoid AND bm.branch_code=bd.branch_code Where bm.trnbelistatus='POST' Group BY bm.trnbelimstoid, bm.trnbelino, bm.trnbelipono, p.trnpaytype,p.trntaxpct, bd.trnsjbelioid, bd.trnsjbelino, p.trnbelimstoid,bm.branch_code, bd.to_branch_code, bm.amtbelinetto, bm.accumpaymentidr, bm.trnbelistatus, bm.trnsuppoid) rt Where QtyRtr > 0.00 " & StatusNya & " AND ToBranch='" & CabangNya.SelectedValue & "' AND trnbelino LIKE '%" & Tchar(invoiceno.Text) & "%' AND trnsuppoid=" & Tchar(suppoid.Text) & " UNION ALL Select * from (Select bm.trnbelimstoid, bm.trnbelino, bm.trnbelipono, 34 trnpaytype, 0.00 trntaxpct, 0 trnsjbelioid, '' trnsjbelino, 0.00 QtyItem, ISNULL((Select SUM(trnbelireturdtlqty) from QL_trnbeliReturdtl rd Inner Join QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid AND rm.trnbelistatus='Approved' AND rd.trnbelireturdtlflag='Approved' AND rm.trnbelimstoid=bm.trnbelimstoid AND bm.branch_code=rm.branch_code),0.00) QtyRetur, ISNULL((Select SUM(trnbelireturdtlqty) from QL_trnbeliReturdtl rd Inner Join QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid AND rm.trnbelistatus='Approved' AND rd.trnbelireturdtlflag='Approved' AND rm.trnbelimstoid=bm.trnbelimstoid AND bm.branch_code=rm.branch_code),0.00) QtyRtr, bm.branch_code, bm.branch_code ToBranch, bm.amtbelinetto, bm.accumpaymentidr, Case When (bm.amtbelinetto-bm.accumpaymentidr)=0 Then 'Lunas' Else 'Belum' End StatusNya, bm.trnbelistatus StatusPI, bm.trnsuppoid, bm.amtbelinetto-ISNULL((Select SUM(rm.amtbelinetto) from QL_trnbeliReturmst rm Where rm.trnbelimstoid=bm.trnbelimstoid AND rm.branch_code=bm.branch_code AND rm.trnbelistatus='APPROVED'),0.00) AmtSisa from QL_trnbelimst bm Where trnbelimstoid<0 AND bm.amtbelinetto-ISNULL((Select SUM(rm.amtbelinetto) from QL_trnbeliReturmst rm Where rm.trnbelimstoid=bm.trnbelimstoid AND rm.branch_code=bm.branch_code),0.00)>0.00) rt Where ToBranch='" & CabangNya.SelectedValue & "' AND trnbelino LIKE '%" & Tchar(invoiceno.Text) & "%' AND trnsuppoid=" & Tchar(suppoid.Text) & " " & StatusNya & ""
        ViewState("trpurchasereturnInv") = cKon.ambiltabel(sSql, "trpurchasereturnInv")
        GVInv.DataSource = ViewState("trpurchasereturnInv")
        GVInv.DataBind() : GVInv.Visible = True
    End Sub

    Private Sub initallddl()

        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & CabangNya.SelectedValue & "' and gengroup = 'cabang') AND a.genother6='UMUM'"

        FillDDL(fromlocation, sSql)
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If
        fromlocation.SelectedIndex = 0
    End Sub

    Private Sub printinvoiceservices(ByVal name As String)
        Try
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\Invoiceservices.rpt"))
            'cProc.SetDBLogonForReport(rpt)
            rpt.SetParameterValue("sWhere", " AND reqoid = " & Session("oid") & "")
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "INVSVC_" & name)
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Private Sub filltextbox(ByVal CabangRet As String, ByVal id As Integer)
        Dim oid As Integer = 0 : TypeNya.Enabled = False
        CabangNya.Enabled = False
        If Integer.TryParse(id, oid) = False Then
            Response.Redirect("trnPurchaseReturn.aspx?awal=true")
        Else
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "Select a.branch_code, a.trnbeliReturmstoid, a.trnbelireturno, a.trntaxpct, a.trnbelidate, a.trnbelipono, a.trnbelino, c.suppoid, c.suppname, a.trnpaytype, a.refnotaretur, a.amtbeli,a.amtvoucher,a.amtekspedisi, a.trnamttax, a.trnbelidisctype, a.trnbelidisc, a.amtdischdr, a.trnbelistatus, a.trnbelinote,Isnull((Select SUM(trnbelidtlqty) from QL_trnbelidtl bd Inner Join QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid And bm.branch_code=bd.branch_code),0.00) qtyPo, a.trnbelimstoid, a.typeret, Isnull((Select sm.trnsjbelioid from ql_trnsjbelimst sm Where sm.trnsjbelino=a.trnsjbelino AND sm.frombranch=a.branch_code),0) trnsjbelioid, a.trnsjbelino, a.branch_code CabangRetur, amtbelinett, a.createtime, a.updtime From QL_trnbeliReturmst a inner join QL_mstsupp c on a.cmpcode = c.cmpcode and a.trnsuppoid = c.suppoid Where a.trnbeliReturmstoid = " & oid & " And a.cmpcode = '" & CompnyCode & "' AND a.branch_code='" & CabangRet & "'"
            xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    ReturOid.Text = Integer.Parse(xreader("trnbeliReturmstoid"))
                    CabangNya.SelectedValue = xreader("CabangRetur").ToString
                    mCabang.Text = xreader("branch_code").ToString
                    noreturn.Text = xreader("trnbelireturno").ToString
                    returndate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                    paytype.Text = xreader("trnpaytype")
                    trnopo.Text = xreader("trnbelipono").ToString
                    LbIDSJ.Text = Integer.Parse(xreader("trnsjbelioid"))
                    trnosj.Text = xreader("trnsjbelino").ToString
                    troid.Text = Integer.Parse(xreader("trnbelimstoid"))
                    invoiceno.Text = xreader("trnbelino").ToString
                    suppname.Text = xreader("suppname").ToString
                    suppoid.Text = Integer.Parse(xreader("suppoid"))
                    noref.Text = xreader("refnotaretur").ToString
                    grossreturn.Text = ToMaskEdit(xreader("amtbeli"), 3)
                    taxpct.Text = ToMaskEdit(xreader("trntaxpct"), 3)
                    taxamount.Text = ToMaskEdit(xreader("trnamttax"), 3)
                    totaldischeader.Text = ToMaskEdit(xreader("amtdischdr"), 3)
                    discheadertype.SelectedValue = xreader("trnbelidisctype")
                    discheader.Text = ToMaskEdit(xreader("trnbelidisc"), 3)
                    voucher.Text = ToMaskEdit(xreader("amtvoucher"), 3)
                    ekspedisi.Text = ToMaskEdit(xreader("amtekspedisi"), 3)
                    status.Text = xreader("trnbelistatus").ToString
                    note.Text = xreader("trnbelinote").ToString
                    qtyPo.Text = ToMaskEdit(xreader("qtyPo"), 3)
                    temptotalamount.Text = GetStrData("select (amtbeli-amtdiscdtl) temptotal from ql_trnbelimst Where trnbelimstoid = " & Integer.Parse(troid.Text) & "")
                    UpdTime.Text = xreader("updtime")
                    tempdisc.Text = GetStrData("select amtdischdr from ql_trnbelimst Where trnbelimstoid = " & Integer.Parse(troid.Text) & "")
                    TypeNya.SelectedValue = xreader("typeret")
                    AmtSisa.Text = ToMaskEdit(ToDouble(xreader("amtbelinett")), 3)
                    createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss tt")
                End While
            End If
            xreader.Close()
            conn.Close()
            CabangNya_SelectedIndexChanged(Nothing, Nothing)

            sSql = "Select a.trnbelireturdtlseq seq, c.itemcode, c.itemdesc itemname, a.trnbelireturdtlqty qty, 'BUAH' unit, a.trnbelireturdtlprice price, a.trnbelireturdtlnote note, a.itemoid, a.itemloc, a.trnbelireturdtldisctype disc1type, a.trnbelireturdtldisctype2 disc2type, a.trnbelireturdtldisctype3 disc3type, a.trnbelireturdtlqtydisc disc1, a.trnbelireturdtlqtydisc2 disc2, a.trnbelireturdtlqtydisc3 disc3, a.amtdisc disc1amt, a.amtdisc2 disc2amt, a.amtdisc3 disc3amt, (a.trnbelireturdtlqty*a.trnbelireturdtlprice) totalamt, (a.amtdisc ) totaldisc, ((a.trnbelireturdtlqty*a.trnbelireturdtlprice)-(a.amtdisc )) netamt, a.amtekspedisi, a.trnbelireturdtlunitoid unitoid, a.unitseq, a.trnbelidtloid, CASE c.has_sn WHEN 1 THEN 'Y' ELSE 'N' END has_SN from QL_trnbeliReturdtl a inner join QL_trnbeliReturmst b on a.cmpcode = b.cmpcode and a.trnbeliReturmstoid = b.trnbeliReturmstoid inner join QL_mstitem c on a.cmpcode = c.cmpcode and a.itemoid = c.itemoid Where a.trnbeliReturmstoid = " & oid & " and a.cmpcode = '" & CompnyCode & "' "
            ViewState("dtlpurchasereturn") = cKon.ambiltabel(sSql, "dtlpurchasereturn")
            GVDtl.DataSource = ViewState("dtlpurchasereturn")
            GVDtl.DataBind() : calculateheader()

            If status.Text = "Approved" Or status.Text = "In Approval" Then
                ibcancel.Visible = True : ibsave.Visible = False
                ibdelete.Visible = False : ibapproval.Visible = False
                btnPrint.Visible = True
            Else
                ibcancel.Visible = True : ibsave.Visible = True
                ibdelete.Visible = True : ibapproval.Visible = True
                btnPrint.Visible = False
            End If
        End If
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        Session.Timeout = 60

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Session("ApprovalLimit") = appLimit
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("trnPurchaseReturn.aspx")
        End If

        Page.Title = CompnyName & " - Purchase Return"
        Session("sCmpcode") = Request.QueryString("branch_code")
        Session("oid") = Request.QueryString("oid")

        ibapproval.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin mengirimkan permintaan approval pada data ini?');")
        ibdelete.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin menghapus data ini?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            InitDDLBranch()
            createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
            tbperiodstart.Text = Format(GetServerTime(), "dd/MM/yyyy")
            tbperiodend.Text = Format(GetServerTime(), "dd/MM/yyyy")
            DDLBranch() : initallddl() : TypeNya.Enabled = True
            bindalldata("") : binddataTR() : BindSupp()
            If Session("oid") = "" Or Session("oid") Is Nothing Then
                ReturOid.Text = GenerateID("QL_trnbeliReturmst", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
                masterstate.Text = "new"
                returndate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                noreturn.Text = GenerateID("QL_trnbeliReturmst", CompnyCode)
            Else
                TabContainer1.ActiveTabIndex = 1
                masterstate.Text = "edit"
                filltextbox(Session("sCmpcode"), Session("oid"))
            End If
        End If
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("trnPurchaseReturn.aspx?awal=true")
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        Dim date1, date2 As New Date : Dim sMs As String = "" : Dim sWhere As String = ""
        If tbperiodstart.Text.Trim = "" Or tbperiodend.Text.Trim = "" Then
            sMs &= "- Maaf, Semua periode harus diisi..!!<br>" 
        End If

        If Date.TryParseExact(tbperiodstart.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            sMs &= "- Maaf, Periode awal tidak valid..!!<br>"
        End If

        If Date.TryParseExact(tbperiodend.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            sMs &= "- Maaf, Periode akhir tidak valid..!!<br>" 
        End If

        If sMs <> "" Then
            showMessage(sMs, 2)
            Exit Sub
        End If

        If CbPeriode.Checked = True Then
            sWhere &= " And convert(date,a.trnbelidate,103) between '" & date1 & "' And '" & date2 & "'"
        End If
        sWhere &= " And " & ddlfilter.SelectedValue.ToString & " Like '%" & Tchar(tbfilter.Text) & "%' " & IIf(ddlfilterstatus.SelectedValue.ToString <> "All", " And a.trnbelistatus = '" & ddlfilterstatus.SelectedValue.ToString & "'", "") & ""
        bindalldata(sWhere)
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        CbPeriode.Checked = False
        tbperiodstart.Text = Format(GetServerTime(), "dd/MM/yyyy")
        tbperiodend.Text = Format(GetServerTime(), "dd/MM/yyyy")

        Dim date1, date2 As New Date
        If tbperiodstart.Text.Trim = "" Or tbperiodend.Text.Trim = "" Then
            showMessage("Semua periode harus diisi!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodstart.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Periode awal tidak valid!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodend.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Periode akhir tidak valid!", 2)
            Exit Sub
        End If

        ddlfilter.SelectedIndex = 0 : tbfilter.Text = ""
        tbperiodstart.Text = Format(GetServerTime(), "dd/MM/yyyy")
        tbperiodend.Text = Format(GetServerTime(), "dd/MM/yyyy")
        ddlfilterstatus.SelectedIndex = 0
        bindalldata("")
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub ibsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsave.Click
        Dim objTableSN As DataTable : Dim sMs As String = ""
        If returndate.Text = "" Then
            sMs &= "Maaf, Tanggal retur tidak boleh kosong..!!<br>"
            status.Text = "In Process" 
        End If

        Dim returdate As New Date
        If Date.TryParseExact(returndate.Text, "dd/MM/yyyy", Nothing, Nothing, returdate) = False Then
            sMs &= "Maaf, Tanggal retur tidak valid..!!<br>"
            status.Text = "In Process" 
        End If

        returndate.Text = Format(GetServerTime(), "dd/MM/yyyy")

        If returdate < New Date(1900, 1, 1) Then
            sMs &= "- Maaf, Tanggal retur tidak boleh kurang dari '1/1/1900'..!!<br>"
            status.Text = "In Process"
        End If

        If returdate > New Date(2079, 6, 6) Then
            sMs &= "- Maaf, Tanggal retur tidak boleh lebih dari '6/6/2079'..!!<br>"
            status.Text = "In Process" 
        End If

        If GVDtl.Rows.Count <= 0 Then
            sMs &= "- Maaf, Detail item pada purchase retur tidak boleh kosong..!!<br>"
            status.Text = "In Process" 
        End If

        Dim dcek As DataTable = ViewState("dtlpurchasereturn")
        For i As Integer = 0 To dcek.Rows.Count - 1
            sSql = "SELECT Isnull(SUM(qtyIn)-SUM(qtyOut),0.0000) sa From QL_conmtr Where refoid=" & dcek.Rows(i).Item("itemoid") & " and periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' and mtrlocoid=" & dcek.Rows(i).Item("itemloc") & " gROUP BY refoid,periodacctg,branch_code,mtrlocoid"
            Dim stokAkhir As Double = GetScalar(sSql)
            If ToDouble(qty.Text) > ToDouble(stokAkhir) Then
                sMs &= "- Maaf, qty " & itemname.Text & " melebihi stok akhir..!!<br>" 
            End If
            'Exit For
        Next

        If Not Session("oid") Is Nothing Or Session("oid") <> "" Then
            If Session("TblSN") Is Nothing Then
                sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid, s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trnreturbelimstoid = '" & Integer.Parse(Session("oid")) & "'"
                objTableSN = cKon.ambiltabel(sSql, "TbSN")
            End If
        End If

        sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnbelireturmst' and branch_code Like '%" & mCabang.Text & "%' order by approvallevel"
        Dim dtab2 As DataTable = cKon.ambiltabel2(sSql, "QL_approvalstructure")

        If sMs <> "" Then
            showMessage(sMs, 2)
            Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trnbeliReturmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                showMessage("Maaf, Data yang anda input sudah di proses, klik tombol ok lalu klik tombol cancel dan periksa data pada list form..!!!", 3)
                status.Text = "In Process"
                Exit Sub
            End If
        End If

        ' VALIDASI 
        If Session("oid") = Nothing Or Session("oid") = "" Then
            sSql = "SELECT COUNT(*) FROM QL_trnbeliReturmst WHERE trnbeliReturmstoid=" & ReturOid.Text
            If CheckDataExists(sSql) Then
                ReturOid.Text = GenerateID("QL_trnbeliReturmst", CompnyCode)
            End If
        End If

        Dim otrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            Dim period As String = GetDateToPeriodAcctg(returdate).Trim
            Dim alastpaydate As New Date
            sSql = "select genoid from ql_mstgen where gengroup = 'PAYTYPE' and gendesc = 'cash' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = Integer.Parse(paytype.Text) Then
                alastpaydate = returdate
            Else
                alastpaydate = CDate("1/1/1900")
            End If

            'sSql = "select lastoid from ql_mstoid where tablename = 'QL_trnbeliReturmst' and cmpcode = '" & CompnyCode & "'"
            'xCmd.CommandText = sSql
            'Dim returmstoid As Integer = xCmd.ExecuteScalar + 1

            sSql = "select lastoid from ql_mstoid where tablename = 'QL_trnbeliReturdtl' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim returdtloid As Integer = xCmd.ExecuteScalar

            If Session("oid") = Nothing Or Session("oid") = "" Then

                sSql = "insert into QL_trnbeliReturmst (cmpcode, trnbeliReturmstoid, periodacctg, trnbelitype, trnbelireturno, trnbelidate, trnbelipono, trnsjbelino, trnbelino, trnsuppoid, trnpaytype, trnbelidisc, trnbelidisctype, trnbelinote, trnbelistatus, createuser, upduser, updtime, amtdischdr, amtbeli, amtbelinetto, accumpayment, lastpaydate, amtdiscdtl,amtvoucher,amtekspedisi, refnotaretur, PIC, trntaxpct, trnamttax, trnbelidisctype2, trnbelidisc2, amtdischdr2, trnbelidisctype3, trnbelidisc3, amtdischdr3, postacctg, user_postacctg, updtime_postacctg, amtretur_hutang, amtpayretur_hutang, amt_qtyclose, finalappovaldatetime, finalapprovaluser, finalapprovalcode, canceluser, canceltime, cancelnote, curroid, currate, trnTrfToReturNo, branch_code,typeret,trnbelimstoid,amtbelinett,createtime) values " & _
                "('" & CompnyCode & "', " & Integer.Parse(ReturOid.Text) & ", '" & GetDateToPeriodAcctg(CDate(toDate(returndate.Text))) & "', 'GROSIR', '" & noreturn.Text & "', '" & CDate(toDate(returndate.Text)) & "', '" & trnopo.Text & "', '" & trnosj.Text & "', '" & invoiceno.Text & "', " & Integer.Parse(suppoid.Text) & ", " & Integer.Parse(paytype.Text) & ", " & ToDouble(discheader.Text) & ", '" & discheadertype.SelectedValue.ToString & "', '" & Tchar(note.Text) & "', '" & status.Text & "', '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(totaldischeader.Text) & ", " & ToDouble(totalamount.Text) & ", " & ToDouble(totalamount.Text) & ", " & ToDouble(totalamount.Text) & ", '" & alastpaydate & "', " & ToDouble(discamount.Text) & "," & ToDouble(voucher.Text) & "," & ToDouble(ekspedisi.Text) & ", '" & Tchar(noref.Text) & "', 0, " & ToDouble(taxpct.Text) & ", " & ToDouble(taxamount.Text) & ", 'AMT', 0, 0, 'AMT', 0, 0, '', '', '1/1/1900', 0, 0, 0, '1/1/1900', '', '', '', '1/1/1900','', 1, 1,'" & trnodp.Text & "','" & CabangNya.SelectedValue & "','" & TypeNya.SelectedValue & "'," & Integer.Parse(troid.Text) & "," & ToDouble(AmtSisa.Text) & ",'" & CDate(toDate(createtime.Text)) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & Integer.Parse(ReturOid.Text) & " where tablename = 'QL_trnbeliReturmst' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '@@@@@@@@@@@@ update QL_Mst_SN  Ql_Mstitemdtl ketika save @@@@@@@@@@@@@@@@@@@@@@@@@@@
                'If Not Session("TblSN") Is Nothing Then
                '    objTableSN = Session("TblSN")
                '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                '        xCmd.CommandText = sSql
                '        Dim itemcodesn As String = xCmd.ExecuteScalar()

                '        sSql = "update QL_Mst_SN set status_approval = '" & status.Text & "',trnreturbelimstoid = '" & returmstoid & "', tglreturbeli = '" & returdate & "' where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = '" & returdate & "', status_item  = '" & status.Text & "' where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '    Next
                'End If
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            Else
                ReturOid.Text = Session("oid")
                sSql = "select trnbelistatus from QL_trnbeliReturmst where trnbeliReturmstoid = " & Integer.Parse(Session("oid")) & ""
                xCmd.CommandText = sSql
                Dim stat As String = xCmd.ExecuteScalar
                If stat.ToLower = "approved" Then 
                    sMs &= "- Maaf, Purchase return tidak bisa diubah karena telah berstatus 'Approved'..!!<br>"
                ElseIf stat.ToLower = "in approval" Then
                    sMs &= "- Maaf, Purchase return tidak bisa diubah karena sedang berstatus 'In Approval'..!!<br>"
                ElseIf stat Is Nothing Or stat = "" Then
                    sMs &= "- Maaf, Purchase return tidak ditemukan..!!<br>"
                End If

                If sMs <> "" Then
                    otrans.Rollback() : xCmd.Connection.Close()
                    showMessage(sMs, 2) : status.Text = "In Process"
                    Exit Sub
                End If

                sSql = "update QL_trnbeliReturmst set periodacctg = '" & GetDateToPeriodAcctg(CDate(toDate(returndate.Text))) & "', trnbelitype = 'GROSIR', trnbelireturno = '" & noreturn.Text & "', trnbelidate = '" & CDate(toDate(returndate.Text)) & "', trnbelipono = '" & trnopo.Text & "', trnsjbelino = '" & trnosj.Text & "', trnbelino = '" & invoiceno.Text & "', trnsuppoid = " & Integer.Parse(suppoid.Text) & ", trnpaytype = " & Integer.Parse(paytype.Text) & ", trnbelidisc = " & ToDouble(discheader.Text) & ", trnbelidisctype = '" & discheadertype.SelectedValue.ToString & "', trnbelinote = '" & Tchar(note.Text) & "', trnbelistatus = '" & status.Text & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP, amtdischdr = " & ToDouble(totaldischeader.Text) & ", amtbeli = " & ToDouble(totalamount.Text) & ", amtbelinetto = " & ToDouble(totalamount.Text) & ", accumpayment = " & ToDouble(totalamount.Text) & ", lastpaydate = '" & alastpaydate & "', amtdiscdtl = " & ToDouble(discamount.Text) & ", amtvoucher=" & ToDouble(voucher.Text) & ",amtekspedisi=" & ToDouble(ekspedisi.Text) & ", refnotaretur = '" & Tchar(noref.Text) & "', PIC = 0, trntaxpct = " & ToDouble(taxpct.Text) & ", trnamttax = " & ToDouble(taxamount.Text) & ", trnbelidisctype2 = 'AMT', trnbelidisc2 = 0, amtdischdr2 = 0, trnbelidisctype3 = 'AMT', trnbelidisc3 = 0, amtdischdr3 = 0, postacctg = '', user_postacctg = '', updtime_postacctg = '1/1/1900', amtretur_hutang = 0, amtpayretur_hutang = 0, amt_qtyclose = 0, finalappovaldatetime = '1/1/1900', finalapprovaluser = '', finalapprovalcode = '', canceluser = '', canceltime = '1/1/1900', cancelnote = '', curroid = 1, currate = 1, trnTrfToReturNo = '" & trnodp.Text & "',trnbelimstoid=" & troid.Text & ",branch_code='" & CabangNya.SelectedValue & "',amtbelinett=" & ToDouble(AmtSisa.Text) & " Where trnbeliReturmstoid = " & Session("oid") & " and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "delete from ql_trnbelireturdtl where trnbeliReturmstoid = " & Session("oid") & " and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '@@@@@@@@@@@@ update QL_Mst_SN Ql_Mstitemdtl ketika Edit @@@@@@@@@@@@@@@@@@@@@@@@@@@
                'If Session("TblSN") Is Nothing Then
                '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                '        Dim itemcodesn As String = cKon.ambilscalar(sSql)

                '        sSql = "update QL_Mst_SN set status_approval = 'POST',trnreturbelimstoid = NULL, tglreturbeli = NULL  where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '        sSql = "update QL_mstitemDtl set  status_item= 'POST' where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '    Next
                'Else
                '    objTableSN = Session("TblSN")
                '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                '        Dim itemcodesn As String = cKon.ambilscalar(sSql)

                '        sSql = "update QL_Mst_SN set status_approval = 'POST',trnreturbelimstoid = NULL, tglreturbeli = NULL  where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '        sSql = "update QL_mstitemDtl set status_item= 'POST' where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '    Next
                'End If

                'If Not Session("TblSN") Is Nothing Then
                '    objTableSN = Session("TblSN")
                '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                '        xCmd.CommandText = sSql
                '        Dim itemcodesn As String = xCmd.ExecuteScalar()

                '        sSql = "update QL_Mst_SN set status_approval = '" & status.Text & "',trnreturbelimstoid = '" & Integer.Parse(ReturOid.Text) & "', tglreturbeli = '" & returdate & "' where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = '" & returdate & "', status_item  = '" & status.Text & "' where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '    Next
                'Else

                '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                '        xCmd.CommandText = sSql : Dim itemcodesn As String = xCmd.ExecuteScalar()

                '        sSql = "update QL_Mst_SN set status_approval = '" & status.Text & "',trnreturbelimstoid = '" & Integer.Parse(ReturOid.Text) & "', tglreturbeli = '" & returdate & "' where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = '" & returdate & "', status_item  = '" & status.Text & "' where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '    Next
                'End If
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            End If

            Dim qty_to As Double = 0.0

            If Not ViewState("dtlpurchasereturn") Is Nothing Then
                Dim dtab As DataTable = ViewState("dtlpurchasereturn")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    returdtloid = returdtloid + 1
                    sSql = "insert into QL_trnbeliReturdtl (cmpcode, trnbeliReturdtloid, trnbeliReturmstoid, trnbelireturdtlseq, itemloc, itemoid, trnbelireturdtlqty, trnbelireturdtlunitoid, trnbelireturdtlprice, unitseq, trnbelireturdtldisctype, trnbelireturdtlqtydisc, trnbelireturdtlflag, trnbelireturdtlnote, trnbelireturdtlstatus, amtdisc, amtbelinetto, createuser, updtime, upduser, trnbelireturdtldisctype2, amtdisc2, trnbelireturdtlqtydisc2, trnbelireturdtldisctype3, amtdisc3, trnbelireturdtlqtydisc3, trnbelidtloid, hpp_new, hpp_old,amtekspedisi) values ('" & CompnyCode & "', " & returdtloid & ", " & Integer.Parse(ReturOid.Text) & ", " & dtab.Rows(i).Item("seq") & ", " & dtab.Rows(i).Item("itemloc") & ", " & dtab.Rows(i).Item("itemoid") & ", " & dtab.Rows(i).Item("qty") & ", " & dtab.Rows(i).Item("unitoid") & ", " & dtab.Rows(i).Item("price") & ", " & dtab.Rows(i).Item("unitseq") & ", '" & dtab.Rows(i).Item("disc1type") & "', " & dtab.Rows(i).Item("disc1") & ", '" & status.Text & "', '" & Tchar(dtab.Rows(i).Item("note")) & "', '" & status.Text & "', " & dtab.Rows(i).Item("disc1amt") & ", " & dtab.Rows(i).Item("netamt") & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', '" & dtab.Rows(i).Item("disc2type") & "', " & dtab.Rows(i).Item("disc2amt") & ", " & dtab.Rows(i).Item("disc2") & ", '" & dtab.Rows(i).Item("disc3type") & "', " & dtab.Rows(i).Item("disc3amt") & ", " & dtab.Rows(i).Item("disc3") & ", " & dtab.Rows(i).Item("trnbelidtloid") & ", 0, 0," & ToDouble(dtab.Rows(i).Item("amtekspedisi")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next

                sSql = "update ql_mstoid set lastoid = " & returdtloid & " where tablename = 'QL_trnbeliReturdtl' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                otrans.Rollback() : xCmd.Connection.Close()
                showMessage("ERROR PROGRAM SEGERA HUBUNGI ADMIN TERKAIT...!!", 1)
                status.Text = "In Process"
                Exit Sub
            End If

            If status.Text = "In Approval" Then
                sSql = "select lastoid from ql_mstoid where tablename = 'ql_approval' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                Dim approvaloid As Integer = xCmd.ExecuteScalar

                If dtab2.Rows.Count > 0 Then
                    For i As Integer = 0 To dtab2.Rows.Count - 1
                        approvaloid = approvaloid + 1
                        sSql = "insert into ql_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus,branch_code) VALUES ('" & CompnyCode & "', " & approvaloid & ", '" & "PR" & Session("oid") & "_" & approvaloid & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'QL_trnbelireturmst', '" & Session("oid") & "', 'In Approval', '0', '" & dtab2.Rows(i).Item("approvaluser") & "', '1/1/1900', '" & dtab2.Rows(i).Item("approvaltype") & "', '1', '" & dtab2.Rows(i).Item("approvalstatus") & "','" & CabangNya.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "update ql_mstoid set lastoid = " & approvaloid & " where tablename = 'ql_approval' and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    otrans.Rollback()
                    xCmd.Connection.Close()
                    showMessage("Maaf, User approved Retur beli belum disetting..!!", 2)
                    status.Text = "In Process"
                    Exit Sub
                End If
            End If

            otrans.Commit() : conn.Close()
        Catch ex As Exception
            otrans.Rollback() : conn.Close()
            showMessage(ex.ToString, 2)
            status.Text = "In Process"
            Exit Sub
        End Try
        Response.Redirect("trnPurchaseReturn.aspx?awal=true")
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click

        Dim otrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            sSql = "select trnbelistatus from QL_trnbeliReturmst where trnbeliReturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : Dim stat As String = xCmd.ExecuteScalar

            If stat.ToLower = "approved" Then
                showMessage("Purchase return tidak bisa dihapus karena telah berstatus 'Approved'!", 2)
                otrans.Rollback()
                conn.Close()
                Exit Sub
            ElseIf stat.ToLower = "in approval" Then
                showMessage("Purchase return tidak bisa dihapus karena sedang berstatus 'In Approval'!", 2)
                otrans.Rollback()
                conn.Close()
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                showMessage("Purchase return tidak ditemukan!", 2)
                otrans.Rollback()
                conn.Close()
                Exit Sub
            End If

            sSql = "delete from QL_trnbeliReturmst where trnbeliReturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnbeliReturdtl where trnbeliReturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_approval where tablename = 'QL_trnbelireturmst' and oid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'If Session("TblSN") = Nothing Then
            '    '@@@@@@@@@@@@@@@@@@@@ update QL_Mst_SN  Ql_Mstitemdtl ketika Edit @@@@@@@@@@@@@@@@
            '    sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trnreturbelimstoid = '" & Integer.Parse(Session("oid")) & "'"

            '    Dim objTableSN As DataTable = cKon.ambiltabel(sSql, "TbSN")
            '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
            '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
            '        Dim itemcodesn As String = cKon.ambilscalar(sSql)

            '        sSql = "update QL_Mst_SN set status_approval = 'POST',trnreturbelimstoid = NULL, tglreturbeli = NULL  where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '        sSql = "update QL_mstitemDtl set  status_item= 'POST'  where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    Next
            'Else
            '    Dim objTableSN As DataTable
            '    objTableSN = Session("TblSN")
            '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
            '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
            '        Dim itemcodesn As String = cKon.ambilscalar(sSql)

            '        sSql = "update QL_Mst_SN set status_approval = 'POST',trnreturbelimstoid = NULL, tglreturbeli = NULL  where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '        sSql = "update QL_mstitemDtl set  status_item= 'POST'  where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    Next
            'End If

            otrans.Commit()
            conn.Close()
            'Response.Redirect("trnPurchaseReturn.aspx?awal=true")
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
            status.Text = "In Process"
            Exit Sub
        End Try
        Response.Redirect("trnPurchaseReturn.aspx?awal=true")
    End Sub

    Protected Sub btnsearchtr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearchtr.Click
        binddataTR()
    End Sub

    Protected Sub btnerasetr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnerasetr.Click
        notr.Text = "" : troid.Text = ""
        trnodp.Text = "" : trnosj.Text = ""
        trnopo.Text = "" : trdate.Text = ""
        invoiceno.Text = "" : paytype.Text = ""
        suppname.Text = "" : suppoid.Text = ""
        btnsearchtr.Enabled = True
        grossreturn.Text = "0.00" : taxamount.Text = "0.00"
        taxpct.Text = 0 : totaldischeader.Text = "0.00"
        discheadertype.SelectedIndex = 0 : discheader.Text = "0.00"
        discamount.Text = "0.00" : totalamount.Text = "0.00"

        If GVTr.Visible = True Then
            ViewState("trpurchasereturn") = Nothing
            GVTr.DataSource = Nothing
            GVTr.DataBind()
            GVTr.Visible = False
        End If

        itemname.Text = "" : itemcode.Text = ""
        itemoid.Text = "" : gudangreturqty.Text = "0.00"
        poprice.Text = "0.00" : qty.Text = "0.00"
        Unit.Text = "" : satuan1.Text = ""
        priceperunit.Text = "0.00" : disc1type.SelectedIndex = 0
        disc2type.SelectedIndex = 0 : disc3type.SelectedIndex = 0
        disc1.Text = "0.00" : disc2.Text = "0.00"
        disc3.Text = "0.00" : discamount1.Text = "0.00"
        discamount2.Text = "0.00" : discamount3.Text = "0.00"
        totalamountdtl.Text = "0.00" : totaldisc.Text = "0.00"
        netamount.Text = "0.00" : notedtl.Text = ""
        labelseq.Text = "" : detailstate.Text = "new"

        If GVItem.Visible = True Then
            ViewState("itempurchasereturn") = Nothing
            GVItem.DataSource = Nothing
            GVItem.DataBind()
            GVItem.Visible = False
        End If

        GVDtl.DataSource = Nothing
        GVDtl.DataBind()
        ViewState("dtlpurchasereturn") = Nothing
    End Sub

    Protected Sub GVTr_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTr.PageIndexChanging
        GVTr.DataSource = ViewState("trpurchasereturn")
        GVTr.PageIndex = e.NewPageIndex
        GVTr.DataBind()
    End Sub

    Protected Sub lbviewinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbviewinfo.Click

    End Sub

    Protected Sub GVTr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVTr.SelectedIndexChanged
        suppoid.Text = GVTr.SelectedDataKey("suppoid")
        suppname.Text = GVTr.Rows(GVTr.SelectedIndex).Cells(2).Text
        ViewState("trpurchasereturn") = Nothing
        GVTr.DataSource = Nothing
        GVTr.DataBind()
        GVTr.Visible = False
    End Sub

    Protected Sub btnsearchitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearchitem.Click
        If troid.Text = "" Then
            showMessage("Pilih nomor PI terlebih dahulu!", 2)
            Exit Sub
        End If

        If trnodp.Text <> notr.Text Then
            showMessage("Pilih nomor PI terlebih dahulu!", 2)
            Exit Sub
        End If
        If Integer.Parse(troid.Text) > 0 Then
            ' Old Query, bermasalah ketika ada 1 Nota 2 Barang sama (Barang biasa dan barang bonus)
            'sSql = "select * from (SELECT b.trnsjbelidtlseq, b.refoid,i.itemcode,i.itemdesc, i.merk, b.unitoid satuan1, g1.gendesc unit,(select x.trnbelidtlprice from QL_trnbelidtl x Where trnsjbelino=sj.trnsjbelino and x.itemoid = b.refoid) poprice,(select x.trnbelidtldisctype from QL_trnbelidtl x Where trnsjbelino=sj.trnsjbelino and x.itemoid = b.refoid) disc1type,(select x.trnbelidtlqtydisc from QL_trnbelidtl x  Where x.trnsjbelino=sj.trnsjbelino and x.itemoid = b.refoid) disc1,(select x.trnbelidtldisctype2 from QL_trnbelidtl x Where trnsjbelino=sj.trnsjbelino and x.itemoid = b.refoid) disc2type,(select x.trnbelidtlqtydisc2/x.trnbelidtlqty from QL_trnbelidtl x Where trnsjbelino=sj.trnsjbelino and x.itemoid = b.refoid) disc2,(select x.ekspedisiidr/x.trnbelidtlqty from ql_podtl x inner join ql_pomst y on x.cmpcode = y.cmpcode and x.trnbelimstoid = y.trnbelimstoid where x.itemoid = b.refoid AND y.trnbelipono=a.trnbelipono AND x.bonus=b.bonus) ekspedisi,round((b.qty), 2) qty,(select ivd.trnbelidtloid from QL_trnbelidtl ivd inner join QL_trnbelimst op on ivd.cmpcode = op.cmpcode and ivd.trnbelimstoid = op.trnbelimstoid where ivd.trnsjbelino = sj.trnsjbelino and ivd.itemoid = b.refoid and op.trnbelistatus = 'POST') trnbelidtloid, CASE i.has_SN WHEN 1 THEN 'Y' ELSE 'N' END has_SN,round((b.qty), 2)-ISNULL((Select sum(rd.trnbelireturdtlqty) From QL_trnbeliReturdtl rd INNER JOIN QL_trnbelidtl dt ON dt.trnbelidtloid=rd.trnbelidtloid AND rd.itemoid=i.itemoid AND dt.trnsjbelino=sj.trnsjbelino AND dt.branch_code=a.branch_code AND a.trnbelimstoid=dt.trnbelimstoid INNER JOIN QL_trnbeliReturmst rm ON rd.trnbeliReturmstoid=rm.trnbeliReturmstoid AND rd.trnbelireturdtlflag='Approved'),0.00) as qtyrtr, b.mtrlocoid , g3.gendesc +' - '+g2.gendesc location,a.branch_code,sj.frombranch,ISNULL(co.SaldoAkhir,0.0000) SaldoAkhir FROM QL_trnbelimst a INNER JOIN ql_trnsjbelimst sj ON sj.trnbelipono = a.trnbelipono /*AND sj.frombranch=a.branch_code*/ INNER JOIN ql_trnsjbelidtl b ON sj.trnsjbelioid = b.trnsjbelioid /*AND sj.frombranch=b.frombranch*/ INNER JOIN QL_mstItem i ON b.refoid = i.itemoid INNER JOIN QL_mstgen g1 ON g1.genoid = b.unitoid AND g1.gengroup='Itemunit' INNER JOIN QL_mstgen g2 ON g2.genoid = b.mtrlocoid AND g2.gengroup ='LOCATION' INNER JOIN QL_mstgen g3 ON g3.genoid = g2.genother1 and g3.gengroup='WAREHOUSE' LEFT JOIN (Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.0000) SaldoAkhir,mtrlocoid ConLoc,refoid,branch_code From QL_conmtr con Where periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' Group BY mtrlocoid,refoid,periodacctg,branch_code) co ON co.refoid=b.refoid AND co.branch_code=sj.branch_code AND co.ConLoc=" & fromlocation.SelectedValue & " Where a.cmpcode = 'MSC' and a.trnbelimstoid ='" & Tchar(troid.Text) & "' and sj.trnsjbelioid = '" & Tchar(LbIDSJ.Text) & "' AND sj.branch_code='" & CabangNya.SelectedValue & "' And (i.itemcode like '%" & Tchar(itemname.Text.Trim) & "%' or i.itemdesc like '%" & Tchar(itemname.Text.Trim) & "%' or i.Merk like '%" & Tchar(itemname.Text.Trim) & "%')) dt Where qtyrtr > 0"
            ' New Query, hanya solusi sementara ==> qty ikut dijoinkan, ke depan harus diperbaiki link menggunakan ID detail per record barang
            sSql = "SELECT sj.trnsjbelino , b.refoid itemoid, b.trnsjbelidtlseq, b.refoid, i.itemcode, i.itemdesc, i.merk," & _
                " b.unitoid satuan1, g1.gendesc unit, ad.trnbelidtlprice poprice, ad.trnbelidtldisctype disc1type," & _
                " ad.trnbelidtlqtydisc disc1, ad.trnbelidtldisctype2 disc2type, (ad.trnbelidtlqtydisc2/ad.trnbelidtlqty) disc2," & _
                " ISNULL((SELECT a1.ekspedisiidr/a1.trnbelidtlqty from ql_podtl a1 inner join ql_pomst a2 on a1.cmpcode=a2.cmpcode " & _
                " and a1.trnbelimstoid=a2.trnbelimstoid Where a1.itemoid=b.refoid AND a2.trnbelipono=a.trnbelipono " & _
                " AND a1.bonus=b.bonus),0.0) ekspedisi,round(ROUND((b.qty), 2)-ISNULL((Select sum(rd.trnbelireturdtlqty) From QL_trnbeliReturdtl rd WHERE ad.trnbelidtloid=rd.trnbelidtloid AND rd.trnbelireturdtlflag='Approved'),0.00), 2) qty, ad.trnbelidtloid, " & _
                " CASE i.has_SN WHEN 1 THEN 'Y' ELSE 'N' END has_SN, ROUND((b.qty), 2)-ISNULL((Select sum(rd.trnbelireturdtlqty) From QL_trnbeliReturdtl rd " & _
                "WHERE rd.trnbelireturdtlflag='Approved' AND ad.trnbelidtloid=rd.trnbelidtloid),0.00) as qtyrtr," & _
                "b.mtrlocoid, g3.gendesc +' - '+g2.gendesc location, a.branch_code, sj.frombranch," & _
                "ISNULL(co.SaldoAkhir,0.00) SaldoAkhir " & _
                "FROM QL_trnbelimst a INNER JOIN QL_trnbelidtl ad ON a.trnbelimstoid=ad.trnbelimstoid " & _
                "INNER JOIN ql_trnsjbelidtl b ON ad.trnsjbelioid = b.trnsjbelioid and ad.itemoid = b.refoid And ad.trnbelidtlqty = b.qty " & _
                "INNER JOIN ql_trnsjbelimst sj ON sj.trnsjbelioid = ad.trnsjbelioid " & _
                "INNER JOIN QL_mstItem i ON b.refoid = i.itemoid " & _
                "INNER JOIN QL_mstgen g1 ON g1.genoid = b.unitoid AND g1.gengroup='Itemunit' " & _
                "INNER JOIN QL_mstgen g2 ON g2.genoid = b.mtrlocoid AND g2.gengroup ='LOCATION' " & _
                "INNER JOIN QL_mstgen g3 ON g3.genoid = g2.genother1 and g3.gengroup='WAREHOUSE' " & _
                "LEFT JOIN (Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) SaldoAkhir, mtrlocoid ConLoc, refoid, branch_code " & _
                "From QL_conmtr con Where periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' Group BY mtrlocoid,refoid,periodacctg,branch_code " & _
                ") co ON co.refoid=b.refoid AND co.branch_code=sj.branch_code AND co.ConLoc=" & fromlocation.SelectedValue & " " & _
                "Where a.cmpcode = '" & CompnyCode & "' and a.trnbelimstoid=" & Tchar(troid.Text) & " AND sj.branch_code='" & CabangNya.SelectedValue & "' " & _
                "And (i.itemcode like '%" & Tchar(itemname.Text.Trim) & "%' or i.itemdesc like '%" & TcharNoTrim(itemname.Text.Trim) & "%') " & _
                "and ROUND((b.qty), 2)-ISNULL((Select sum(rd.trnbelireturdtlqty) From QL_trnbeliReturdtl rd " & _
                "WHERE rd.trnbelireturdtlflag='Approved' AND ad.trnbelidtloid=rd.trnbelidtloid),0.00) > 0"
        Else
            sSql = "SELECT TOP 200 * FROM (SELECT 0 trnsjbelidtlseq, i.itemoid refoid,i.itemcode,i.itemdesc, i.merk,i.satuan1, g1.gendesc unit, i.lastPricebuyUnit1 poprice, 'AMT' disc1type, 0.00 disc1, 'VCR' disc2type, 0.00 disc2, 0.00 ekspedisi, 0.00 qty,0 trnbelidtloid, CASE i.has_SN WHEN 1 THEN 'Y' ELSE 'N' END has_SN,0.0000 as qtyrtr, Isnull(mtrlocoid," & fromlocation.SelectedValue & ") mtrlocoid,ISnull(WhDesc+' - '+LocDesc,'" & CabangNya.SelectedValue & "') location,ISnull(branch_code,'" & CabangNya.SelectedValue & "') branch_code, ISNULL(branch_code,'" & CabangNya.SelectedValue & "') frombranch, Isnull(SaldoAkhir ,0.00) SaldoAkhir FROM QL_mstItem i INNER JOIN QL_mstgen g1 ON g1.genoid =i.satuan1 AND g1.gengroup='Itemunit' LEFT JOIN (" & _
            "Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) SaldoAkhir, mtrlocoid, refoid, g2.gendesc LocDesc, g3.gendesc WhDesc, con.branch_code From QL_conmtr con INNER JOIN QL_mstgen g2 ON g2.genoid = con.mtrlocoid AND g2.gengroup ='LOCATION' INNER JOIN QL_mstgen g3 ON g3.genoid = g2.genother1 and g3.gengroup='WAREHOUSE' Where periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' AND con.branch_code='" & CabangNya.SelectedValue & "' AND con.mtrlocoid=" & fromlocation.SelectedValue & " Group BY mtrlocoid, refoid, periodacctg, g2.gendesc, g3.gendesc, con.branch_code) co ON co.refoid=i.itemoid Where (i.itemcode like '%" & Tchar(itemname.Text.Trim) & "%' or i.itemdesc like '%" & TcharNoTrim(itemname.Text.Trim) & "%' or i.Merk like '%" & Tchar(itemname.Text.Trim) & "%')) OT"
        End If
        ViewState("itempurchasereturn") = cKon.ambiltabel(sSql, "itempurchasereturn")
        Session("BindItem") = ViewState("itempurchasereturn")
        GVItem.DataSource = ViewState("itempurchasereturn")
        GVItem.DataBind() : GVItem.Visible = True
        GVItem.PageIndex = 0
    End Sub

    Protected Sub btneraseitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btneraseitem.Click
        itemname.Text = "" : itemcode.Text = "" : itemoid.Text = ""
        gudangreturqty.Text = "0.00" : poprice.Text = "0.00"
        qty.Text = "0.00" : Unit.Text = "" : unitseq.Text = ""
        satuan1.Text = "" : priceperunit.Text = "0.00"
        discamount1.Text = "0.00" : totalamountdtl.Text = "0.00"
        netamount.Text = "0.00" : texthasSN.Text = ""
        If GVItem.Visible = True Then
            ViewState("itempurchasereturn") = Nothing
            GVItem.DataSource = Nothing
            GVItem.DataBind()
            GVItem.Visible = False
        End If
    End Sub

    Protected Sub GVItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItem.PageIndexChanging
        GVItem.DataSource = ViewState("itempurchasereturn")
        GVItem.PageIndex = e.NewPageIndex
        GVItem.DataBind()
    End Sub

    Protected Sub GVItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItem.SelectedIndexChanged
        itemname.Text = GVItem.SelectedDataKey("itemdesc").ToString
        itemcode.Text = GVItem.SelectedDataKey("itemcode").ToString
        itemoid.Text = GVItem.SelectedDataKey("refoid") 
        priceperunit.Text = ToMaskEdit(GVItem.SelectedDataKey("poprice"), 3)
        qty.Text = ToMaskEdit(ToDouble(GVItem.SelectedDataKey("qty")), 3)
        Unit.Text = GVItem.Rows(GVItem.SelectedIndex).Cells(5).Text
        unitseq.Text = GVItem.SelectedDataKey("trnsjbelidtlseq")
        satuan1.Text = GVItem.SelectedDataKey("satuan1")
        disc1type.SelectedValue = GVItem.SelectedDataKey("disc1type")
        disc1.Text = ToMaskEdit(GVItem.SelectedDataKey("disc1"), 3)
        disc2type.SelectedValue = GVItem.SelectedDataKey("disc2type")
        disc2.Text = ToMaskEdit(GVItem.SelectedDataKey("disc2"), 3)
        ekspedisidtl.Text = ToMaskEdit(GVItem.SelectedDataKey("ekspedisi"), 3)
        trnbelidtloid.Text = GVItem.SelectedDataKey("trnbelidtloid")
        texthasSN.Text = GVItem.SelectedDataKey("has_SN")
        itemLoc.Text = GVItem.SelectedDataKey("mtrlocoid")
        ViewState("itempurchasereturn") = Nothing
        GVItem.DataSource = Nothing : GVItem.DataBind()
        GVItem.Visible = False : calculatedetail()
    End Sub

    Protected Sub btnaddtolist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddtolist.Click
        TypeNya.Enabled = False : CabangNya.Enabled = False : Dim sMs As String = ""
        btnsuppliersearch.Enabled = False
        If itemname.Text = "" Or itemoid.Text = "" Then
            sMs &= "Maaf, Pilih item terlebih dulu..!!<br>" 
        End If

        If ToDouble(troid.Text) < 0 Then
        Else
            If ToDouble(priceperunit.Text) = 0 Then
                sMs &= "Price per unit tidak boleh 0..!!<br>"
            End If
        End If

        If Integer.Parse(troid.Text) > 0 Then
            If Not Session("BindItem") Is Nothing Then
                Dim obj As DataTable = Session("BindItem")
                Dim dtv As DataView = obj.DefaultView
                dtv.RowFilter = "itemoid=" & itemoid.Text & " AND trnbelidtloid=" & trnbelidtloid.Text & ""
                For C1 As Integer = 0 To dtv.Count - 1
                    If ToDouble(qty.Text) > ToDouble(dtv(C1).Item("qty")) Then
                        sMs &= "Maaf, qty " & itemname.Text & " melebihi Qty PI " & ToMaskEdit(dtv(C1).Item("qty"), 3) & "..!!<br>"
                    End If
                    Exit For
                Next
            End If
        End If

        sSql = "SELECT Isnull(SUM(qtyIn)-SUM(qtyOut),0.00) sA fROM QL_conmtr wHERE refoid=" & itemoid.Text & " and periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' and mtrlocoid=" & itemLoc.Text & " gROUP BY refoid,periodacctg,branch_code,mtrlocoid"
        Dim stokAkhir As Double = GetScalar(sSql)

        If ToDouble(qty.Text) > ToDouble(stokAkhir) Then
            sMs &= "Maaf, qty " & itemname.Text & " melebihi stok akhir<br>" 
        End If

        If masterstate.Text = "new" Then
            sSql = "SELECT COUNT(*) FROM QL_trnbeliReturmst br Inner Join QL_trnbeliReturdtl bd ON br.trnbeliReturmstoid=bd.trnbeliReturmstoid WHERE trnbelino = '" & TcharNoTrim(invoiceno.Text) & "' and trnbelistatus IN ('In Approval','In Process') AND bd.itemoid=" & itemoid.Text & ""
            Dim proses As Integer = cKon.ambilscalar(sSql)
            If proses <> 0 Then
                sMs &= "Maaf, No. Invoice " & invoiceno.Text & " sudah pernah di input retur dengan status retur masih In Process / In Approval<br>"
            End If
        End If

        If Integer.Parse(troid.Text) > 0 Then
            ToDouble(priceperunit.Text)
        End If

        If Integer.Parse(troid.Text) < 0 Then
            If ToDouble(totalamount.Text) > ToDouble(AmtSisa.Text) Then
                sMs &= "- Maaf, Amount Retur " & ToMaskEdit(totalamount.Text, 3) & " Melebihi Amount PI " & ToMaskEdit(AmtSisa.Text, 3) & ""
            End If
        End If

        If sMs <> "" Then
            showMessage(sMs, 2)
            Exit Sub
        End If

        If detailstate.Text = "new" Then
            labelseq.Text = GVDtl.Rows.Count + 1
        End If

        Dim dtab As New DataTable
        If ViewState("dtlpurchasereturn") Is Nothing Then
            dtab.Columns.Add("seq", Type.GetType("System.Int32"))
            dtab.Columns.Add("itemcode", Type.GetType("System.String"))
            dtab.Columns.Add("itemname", Type.GetType("System.String"))
            dtab.Columns.Add("itemloc", Type.GetType("System.String"))
            dtab.Columns.Add("qty", Type.GetType("System.Double"))
            dtab.Columns.Add("unit", Type.GetType("System.String"))
            dtab.Columns.Add("price", Type.GetType("System.Double"))
            dtab.Columns.Add("note", Type.GetType("System.String"))
            dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("amtekspedisi", Type.GetType("System.Double")) 
            dtab.Columns.Add("disc1type", Type.GetType("System.String"))
            dtab.Columns.Add("disc2type", Type.GetType("System.String"))
            dtab.Columns.Add("disc3type", Type.GetType("System.String"))
            dtab.Columns.Add("disc1", Type.GetType("System.Double"))
            dtab.Columns.Add("disc2", Type.GetType("System.Double"))
            dtab.Columns.Add("disc3", Type.GetType("System.Double"))
            dtab.Columns.Add("disc1amt", Type.GetType("System.Double"))
            dtab.Columns.Add("disc2amt", Type.GetType("System.Double"))
            dtab.Columns.Add("disc3amt", Type.GetType("System.Double"))
            dtab.Columns.Add("totalamt", Type.GetType("System.Double"))
            dtab.Columns.Add("totaldisc", Type.GetType("System.Double"))
            dtab.Columns.Add("netamt", Type.GetType("System.Double"))
            dtab.Columns.Add("unitoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("unitseq", Type.GetType("System.Int32"))
            dtab.Columns.Add("trnbelidtloid", Type.GetType("System.Int32"))
            dtab.Columns.Add("has_sn", Type.GetType("System.String"))
            ViewState("dtlpurchasereturn") = dtab
        Else
            dtab = ViewState("dtlpurchasereturn")
        End If

        If texthasSN.Text = "Y" Then
            If detailstate.Text = "new" Then
                TextItem.Text = itemname.Text
                KodeItem.Text = itemoid.Text
                TextQty.Text = ToDouble(qty.Text)
                TextSatuan.Text = Unit.Text
                GVSN.DataSource = Nothing
                GVSN.DataBind()
                cProc.SetModalPopUpExtender(btnHideSN, pnlInvnSN, ModalPopupSN, True)
                TextSN.Text = ""
                LabelPesanSn.Text = ""
                cProc.SetFocusToControl(Me.Page, TextSN)
            Else
                If status.Text = "In Process" Or status.Text = "In Approval" Or status.Text = "Approved" Then

                    If Session("TblSN") Is Nothing Then
                        pnlInvnSN.Visible = True
                        btnHideSN.Visible = True
                        sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid, s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trnreturbelimstoid = '" & noreturn.Text & "' AND i.itemoid = '" & itemoid.Text & "' "
                        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
                        Dim objDs As New DataSet
                        mySqlDA.Fill(objDs, "dataSN")
                        GVSN.DataSource = Nothing
                        GVSN.DataSource = objDs.Tables("dataSN")
                        GVSN.DataBind()
                        Session("TblSN") = objDs.Tables("dataSN")
                        cProc.SetModalPopUpExtender(btnHideSN, pnlInvnSN, ModalPopupSN, True)
                        TextItem.Text = itemname.Text
                        KodeItem.Text = itemoid.Text
                        TextQty.Text = ToDouble(qty.Text)
                        TextSatuan.Text = Unit.Text
                        cProc.SetFocusToControl(Me.Page, TextSN)
                    Else
                        Dim objTableSN As DataTable
                        objTableSN = Session("TblSN")
                        Dim dvSN As DataView = objTableSN.DefaultView
                        Dim itemid As String = itemoid.Text
                        dvSN.RowFilter = "itemoid = " & itemid
                        cProc.SetModalPopUpExtender(btnHideSN, pnlInvnSN, ModalPopupSN, True)
                        TextItem.Text = itemname.Text
                        KodeItem.Text = itemoid.Text
                        TextQty.Text = ToDouble(qty.Text)
                        TextSatuan.Text = Unit.Text
                        GVSN.DataSource = Nothing
                        GVSN.DataSource = dvSN
                        GVSN.DataBind()
                        cProc.SetFocusToControl(Me.Page, TextSN)
                    End If
                End If
            End If

        Else
            If dtab.Rows.Count > 0 Then
                Dim drowc() As DataRow = dtab.Select("trnbelidtloid = " & Integer.Parse(trnbelidtloid.Text) & " AND seq <> " & Integer.Parse(labelseq.Text) & " AND itemoid=" & Integer.Parse(itemoid.Text) & "")
                If drowc.Length > 0 Then
                    showMessage("Item sudah pernah ditambahkan ke dalam list !", 2)
                    Exit Sub
                End If
            End If

            If detailstate.Text = "new" Then
                labelseq.Text = GVDtl.Rows.Count + 1
                Dim drow As DataRow = dtab.NewRow
                drow("seq") = Integer.Parse(labelseq.Text)
                drow("itemcode") = itemcode.Text
                drow("itemname") = itemname.Text
                drow("itemloc") = itemLoc.Text
                drow("qty") = ToDouble(qty.Text)
                drow("unit") = Unit.Text
                drow("price") = ToMaskEdit(ToDouble(priceperunit.Text), 3)
                drow("note") = notedtl.Text
                drow("itemoid") = Integer.Parse(itemoid.Text)
                drow("amtekspedisi") = ToMaskEdit(ToDouble(ekspedisidtl.Text), 3)
                drow("disc1type") = disc1type.SelectedValue.ToString
                drow("disc2type") = disc2type.SelectedValue.ToString
                drow("disc3type") = disc3type.SelectedValue.ToString
                drow("disc1") = ToMaskEdit(ToDouble(disc1.Text), 3)
                drow("disc2") = ToMaskEdit(ToDouble(disc2.Text), 3)
                drow("disc3") = ToMaskEdit(ToDouble(disc3.Text), 3)
                drow("disc1amt") = ToMaskEdit(ToDouble(discamount1.Text), 3)
                drow("disc2amt") = ToMaskEdit(ToDouble(discamount2.Text), 3)
                drow("disc3amt") = ToMaskEdit(ToDouble(discamount3.Text), 3)
                drow("totalamt") = ToMaskEdit(ToDouble(totalamountdtl.Text), 3)
                drow("totaldisc") = ToMaskEdit(ToDouble(totaldisc.Text), 3)
                drow("netamt") = ToMaskEdit(ToDouble(netamount.Text), 3)
                drow("unitoid") = Integer.Parse(satuan1.Text)
                drow("unitseq") = Integer.Parse(unitseq.Text)
                drow("trnbelidtloid") = trnbelidtloid.Text
                drow("has_sn") = texthasSN.Text
                dtab.Rows.Add(drow)
            Else
                Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "")
                If drow.Length > 0 Then
                    drow(0).BeginEdit()
                    drow(0)("itemcode") = itemcode.Text
                    drow(0)("itemname") = itemname.Text
                    drow(0)("itemLoc") = itemLoc.Text
                    drow(0)("qty") = ToDouble(qty.Text)
                    drow(0)("unit") = Unit.Text
                    drow(0)("price") = ToMaskEdit(ToDouble(priceperunit.Text), 3)
                    drow(0)("note") = notedtl.Text
                    drow(0)("itemoid") = Integer.Parse(itemoid.Text)
                    drow(0)("amtekspedisi") = ToMaskEdit(ToDouble(ekspedisidtl.Text), 3)
                    drow(0)("disc1type") = disc1type.SelectedValue.ToString
                    drow(0)("disc2type") = disc2type.SelectedValue.ToString
                    drow(0)("disc3type") = disc3type.SelectedValue.ToString
                    drow(0)("disc1") = ToMaskEdit(ToDouble(disc1.Text), 3)
                    drow(0)("disc2") = ToMaskEdit(ToDouble(disc2.Text), 3)
                    drow(0)("disc3") = ToMaskEdit(ToDouble(disc3.Text), 3)
                    drow(0)("disc1amt") = ToMaskEdit(ToDouble(discamount1.Text), 3)
                    drow(0)("disc2amt") = ToMaskEdit(ToDouble(discamount2.Text), 3)
                    drow(0)("disc3amt") = ToMaskEdit(ToDouble(discamount3.Text), 3)
                    drow(0)("totalamt") = ToMaskEdit(ToDouble(totalamountdtl.Text), 3)
                    drow(0)("totaldisc") = ToMaskEdit(ToDouble(totaldisc.Text), 3)
                    drow(0)("netamt") = ToMaskEdit(ToDouble(netamount.Text), 3)
                    drow(0)("unitoid") = Integer.Parse(satuan1.Text)
                    drow(0)("unitseq") = Integer.Parse(unitseq.Text)
                    drow(0)("trnbelidtloid") = trnbelidtloid.Text
                    drow(0)("has_sn") = texthasSN.Text
                    drow(0).EndEdit()
                Else
                    showMessage("ERROR PROGRAM SEGERA HUBUNGI ADMIN TERKAIT", 1)
                    Exit Sub
                End If
            End If
            dtab.AcceptChanges()
            dtab.Select("", "seq")

            GVDtl.DataSource = dtab : GVDtl.DataBind()
            ViewState("dtlpurchasereturn") = dtab

            itemname.Text = "" : itemoid.Text = ""
            itemcode.Text = "" : gudangreturqty.Text = "0.00"
            poprice.Text = "0.00" : qty.Text = "0.00"
            Unit.Text = "" : unitseq.Text = ""
            satuan1.Text = "" : trnbelidtloid.Text = ""
            priceperunit.Text = "0.00" : disc1type.SelectedIndex = 0
            disc2type.SelectedIndex = 0 : disc3type.SelectedIndex = 0
            disc1.Text = "0.00" : disc2.Text = "0.00"
            disc3.Text = "0.00" : ekspedisidtl.Text = "0.00"
            discamount1.Text = "0.00" : discamount2.Text = "0.00"
            discamount3.Text = "0.00" : totalamountdtl.Text = "0.00"
            totaldisc.Text = "0.00" : netamount.Text = "0.00"
            notedtl.Text = "" : texthasSN.Text = ""
            labelseq.Text = "" : detailstate.Text = "new"

            GVDtl.SelectedIndex = -1
            GVDtl.Columns(8).Visible = True
            calculateheader()
        End If

    End Sub

    Protected Sub btnclear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnclear.Click
        CabangNya.Enabled = True : btnsuppliersearch.Enabled = True
        itemname.Text = "" : itemcode.Text = ""
        itemoid.Text = "" : gudangreturqty.Text = "0.00"
        poprice.Text = "0.00" : qty.Text = "0.00"
        Unit.Text = "" : unitseq.Text = ""
        satuan1.Text = "" : TypeNya.Enabled = True

        priceperunit.Text = "0.00" : disc1type.SelectedIndex = 0
        disc2type.SelectedIndex = 0 : disc3type.SelectedIndex = 0
        disc1.Text = "0.00" : disc2.Text = "0.00"
        disc3.Text = "0.00" : discamount1.Text = "0.00"
        discamount2.Text = "0.00" : discamount3.Text = "0.00"
        totalamountdtl.Text = "0.00" : totaldisc.Text = "0.00"
        netamount.Text = "0.00" : notedtl.Text = ""

        labelseq.Text = "" : detailstate.Text = "new"

        GVDtl.SelectedIndex = -1
        GVDtl.Columns(8).Visible = True
    End Sub

    Protected Sub GVDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl.RowDeleting
        If Not ViewState("dtlpurchasereturn") Is Nothing Then

            Dim iIndex As Int16 = e.RowIndex
            Dim objTable As DataTable = ViewState("dtlpurchasereturn")
            '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@
            If Not Session("TblSN") Is Nothing Then
                Dim objTableSN As DataTable = Session("TblSN")
                Dim dvSN As DataView = objTableSN.DefaultView
                Dim itemid As String = objTable.Rows(iIndex).Item("itemoid")
                dvSN.RowFilter = "itemoid = " & itemid
                If Not dvSN.Count = 0 Then
                    For kdsn As Integer = 0 To dvSN.Count - 1
                        objTableSN.Rows.RemoveAt(0)
                    Next
                End If
                objTableSN.AcceptChanges()
            End If

            Dim dtab As DataTable = ViewState("dtlpurchasereturn")
            Dim seq As Integer = Integer.Parse(GVDtl.Rows(e.RowIndex).Cells(1).Text)
            Dim drow() As DataRow = dtab.Select("seq = " & seq & "")
            drow(0).Delete()
            dtab.Select(Nothing)
            dtab.AcceptChanges()

            Dim arow As DataRow = Nothing
            For i As Integer = 0 To dtab.Rows.Count - 1
                If dtab.Rows(i).Item("seq") > seq Then
                    arow = dtab.Rows(i)
                    arow.BeginEdit()
                    arow("seq") = arow("seq") - 1
                    arow.EndEdit()
                End If
            Next
            dtab.AcceptChanges()

            ViewState("dtlpurchasereturn") = dtab
            GVDtl.DataSource = dtab
            GVDtl.DataBind()
        End If
    End Sub

    Protected Sub GVDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl.SelectedIndexChanged
        labelseq.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(1).Text
        detailstate.Text = "edit"

        itemname.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(3).Text
        itemcode.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(2).Text
        itemoid.Text = GVDtl.SelectedDataKey("itemoid")
        itemLoc.Text = GVDtl.SelectedDataKey("itemloc")
        'gudangreturqty.Text = Format(GVDtl.SelectedDataKey("gudangqty"), "#,##0.00")
        'poprice.Text = Format(GVDtl.SelectedDataKey("poprice"), "#,##0.00")
        qty.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(4).Text
        Unit.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(5).Text
        unitseq.Text = GVDtl.SelectedDataKey("unitseq")
        satuan1.Text = GVDtl.SelectedDataKey("unitoid")
        trnbelidtloid.Text = GVDtl.SelectedDataKey("trnbelidtloid")
        priceperunit.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(6).Text
        disc1type.SelectedValue = GVDtl.SelectedDataKey("disc1type")
        disc2type.SelectedValue = GVDtl.SelectedDataKey("disc2type")
        disc3type.SelectedValue = GVDtl.SelectedDataKey("disc3type")
        disc1.Text = ToMaskEdit(GVDtl.SelectedDataKey("disc1"), 3)
        disc2.Text = ToMaskEdit(GVDtl.SelectedDataKey("disc2"), 3)
        disc3.Text = ToMaskEdit(GVDtl.SelectedDataKey("disc3"), 3)
        ekspedisidtl.Text = ToMaskEdit(GVDtl.SelectedDataKey("amtekspedisi"), 3)
        discamount1.Text = ToMaskEdit(GVDtl.SelectedDataKey("disc1amt"), 3)
        discamount2.Text = ToMaskEdit(GVDtl.SelectedDataKey("disc2amt"), 3)
        discamount3.Text = ToMaskEdit(GVDtl.SelectedDataKey("disc3amt"), 3)
        totalamountdtl.Text = ToMaskEdit(GVDtl.SelectedDataKey("totalamt"), 3)
        totaldisc.Text = ToMaskEdit(GVDtl.SelectedDataKey("totaldisc"), 3)
        netamount.Text = ToMaskEdit(GVDtl.SelectedDataKey("netamt"), 3)
        notedtl.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(7).Text.Replace("&nbsp;", "")
        texthasSN.Text = GVDtl.SelectedDataKey("has_sn")
        GVDtl.Columns(8).Visible = False
    End Sub

    Protected Sub discheader_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles discheader.TextChanged
        calculateheader()
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        qty.Text = ToMaskEdit(ToDouble(qty.Text), 3)
        calculatedetail()
    End Sub

    Protected Sub priceperunit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles priceperunit.TextChanged
        calculatedetail()
    End Sub

    Protected Sub disc1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc1.TextChanged
        calculatedetail()
    End Sub

    Protected Sub disc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc2.TextChanged
        calculatedetail()
    End Sub

    Protected Sub disc3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc3.TextChanged
        calculatedetail()
    End Sub

    Private Sub calculateheader()
        Dim qtyitem As Integer = 0
        If GVDtl.Rows.Count > 0 Then
            Dim agrossreturn As Double = 0.0 : Dim ataxamount As Double = 0.0
            Dim adiscamount As Double = 0.0 : Dim atotaldischeader As Double = 0.0
            Dim atotalamount As Double = 0.0 : Dim aReturndtlNet As Double = 0.0
            Dim avoucher As Double = 0.0 : Dim aekspedisi As Double = 0.0

            Dim drow As DataRow = Nothing
            Dim dtab As New DataTable

            If Not ViewState("dtlpurchasereturn") Is Nothing Then
                dtab = ViewState("dtlpurchasereturn")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    drow = dtab.Rows(i)
                    agrossreturn = agrossreturn + drow.Item("totalamt")
                    adiscamount = adiscamount + drow.Item("totaldisc")
                    avoucher = avoucher + drow.Item("disc2amt")
                    aekspedisi = aekspedisi + drow.Item("amtekspedisi")
                    aReturndtlNet = aReturndtlNet + drow.Item("netamt")
                    qtyitem = qtyitem + drow.Item("qty")
                Next
            Else
                showMessage("Missing something '>_<'", 1)
                Exit Sub
            End If

            voucher.Text = ToMaskEdit(avoucher, 3)
            ekspedisi.Text = ToMaskEdit(aekspedisi, 3)
            discamount.Text = ToMaskEdit(adiscamount, 3)

            If discheadertype.SelectedValue = "AMT" Then
                discheader.Text = ToMaskEdit(ToDouble(tempdisc.Text) / ToDouble(qtyPo.Text), 3)
            End If
 
            taxamount.Text = ToMaskEdit(0, 3)
            totalamount.Text = ToMaskEdit(agrossreturn - adiscamount - ToDouble(totaldischeader.Text), 3)
            grossreturn.Text = ToMaskEdit(totalamount.Text, 3)
        Else
            grossreturn.Text = "0.00" : taxamount.Text = "0.00" : discamount.Text = "0.00" 
            totalamount.Text = "0.00"
        End If
    End Sub

    Private Sub calculatedetail()
        discamount1.Text = IIf(disc1type.SelectedValue = "AMT", ToMaskEdit(ToDouble(disc1.Text * qty.Text), 3), ToMaskEdit(((ToDouble(disc1.Text) / 100) * ToDouble(priceperunit.Text)) * ToDouble(qty.Text), 3))

        discamount2.Text = IIf(disc2type.SelectedValue = "VCR", ToMaskEdit(ToDouble(disc2.Text) * ToDouble(qty.Text), 3), ToMaskEdit((ToDouble(disc2.Text) / 100) * ((ToDouble(priceperunit.Text) * ToDouble(qty.Text)) - ToDouble(discamount1.Text)), 3))
        discamount3.Text = IIf(disc3type.SelectedValue = "AMT", ToMaskEdit(ToDouble(disc3.Text) * ToDouble(qty.Text), 3), ToMaskEdit((ToDouble(disc3.Text) / 100) * ((ToDouble(priceperunit.Text) * ToDouble(qty.Text)) - (ToDouble(discamount1.Text)) - (ToDouble(discamount2.Text))), 3))

        totalamountdtl.Text = ToMaskEdit(ToDouble(qty.Text * priceperunit.Text), 3)
        totaldisc.Text = ToMaskEdit(ToDouble(discamount1.Text), 3)
        netamount.Text = ToMaskEdit(ToDouble(totalamountdtl.Text - totaldisc.Text), 3)
    End Sub

    Protected Sub discheadertype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles discheadertype.SelectedIndexChanged
        calculateheader()
    End Sub

    Protected Sub disc1type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc1type.SelectedIndexChanged
        calculatedetail()
    End Sub

    Protected Sub disc2type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc2type.SelectedIndexChanged
        calculatedetail()
    End Sub

    Protected Sub disc3type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc3type.SelectedIndexChanged
        calculatedetail()
    End Sub

    Protected Sub ibapproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapproval.Click
        status.Text = "In Approval"
        ibsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lb As LinkButton = TryCast(sender, LinkButton)
            Dim str As String() = lb.ToolTip.Split(",")

            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\printpurchasereturn.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", " where a.trnbeliReturmstoid = " & Integer.Parse(str(0)) & "")
            rpt.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "PR_" & str(1))
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Try
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\printpurchasereturn.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", " where a.trnbeliReturmstoid = " & Session("oid") & "")
            rpt.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "PR_" & noreturn.Text)
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Protected Sub GVTrn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTrn.PageIndexChanging
        If Not ViewState("purchaseretur") Is Nothing Then
            'GVTrn.DataSource = ViewState("purchaseretur")
            Dim date1, date2 As New Date
            If tbperiodstart.Text.Trim = "" Or tbperiodend.Text.Trim = "" Then
                showMessage("Semua periode harus diisi!", 2)
                Exit Sub
            End If
            If Date.TryParseExact(tbperiodstart.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
                showMessage("Periode awal tidak valid!", 2)
                Exit Sub
            End If
            If Date.TryParseExact(tbperiodend.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
                showMessage("Periode akhir tidak valid!", 2)
                Exit Sub
            End If
            Dim sWhere As String = ""

            If CbPeriode.Checked = True Then
                sWhere &= " And convert(date,a.trnbelidate,103) between '" & date1 & "' And '" & date2 & "'"
            End If
            sWhere &= " And " & ddlfilter.SelectedValue.ToString & " Like '%" & Tchar(tbfilter.Text) & "%' " & IIf(ddlfilterstatus.SelectedValue.ToString <> "All", " And a.trnbelistatus = '" & ddlfilterstatus.SelectedValue.ToString & "'", "") & ""

            GVTrn.PageIndex = e.NewPageIndex
            bindalldata(sWhere)
            'GVTrn.DataBind()
        End If
    End Sub

    Protected Sub btnInvoicesearch_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnInvoicesearch.Click
        InvBindData()
    End Sub

    Protected Sub btnsuppliersearch_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnsuppliersearch.Click
        BindSupp()
        GVTr.Visible = True
    End Sub

    Protected Sub btnerasesupplier_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnerasesupplier.Click
        suppname.Text = "" : suppoid.Text = ""
        notr.Text = "" : troid.Text = "" : CabangNya.Enabled = True
        trnodp.Text = "" : trnosj.Text = "" : trnopo.Text = ""
        trdate.Text = "" : invoiceno.Text = "" : paytype.Text = ""
        LbIDSJ.Text = "" : qtyPo.Text = 0 : mCabang.Text = ""
        grossreturn.Text = "0.00" : taxamount.Text = "0.00"
        taxpct.Text = 0 : totaldischeader.Text = "0.00"
        discheadertype.SelectedIndex = 0
        discheader.Text = "0.00" : discamount.Text = "0.00"
        totalamount.Text = "0.00"

        'If GVTr.Visible = True Then
        ViewState("trpurchasereturn") = Nothing
        GVTr.DataSource = Nothing
        GVTr.DataBind()
        GVTr.Visible = False
        'End If

        itemname.Text = "" : itemcode.Text = ""
        itemoid.Text = "" : gudangreturqty.Text = "0.00"
        poprice.Text = "0.00" : qty.Text = "0.00"
        Unit.Text = "" : satuan1.Text = ""
        priceperunit.Text = "0.00"
        disc1type.SelectedIndex = 0
        disc2type.SelectedIndex = 0
        disc3type.SelectedIndex = 0
        disc1.Text = "0.00" : disc2.Text = "0.00"
        disc3.Text = "0.00" : discamount1.Text = "0.00"
        discamount2.Text = "0.00" : discamount3.Text = "0.00"
        totalamountdtl.Text = "0.00" : totaldisc.Text = "0.00"
        netamount.Text = "0.00" : notedtl.Text = ""
        labelseq.Text = "" : detailstate.Text = "new"

        ViewState("trpurchasereturnInv") = Nothing
        GVInv.DataSource = Nothing
        GVInv.DataBind()
        GVInv.Visible = False

        'If GVItem.Visible = True Then
        ViewState("itempurchasereturn") = Nothing
        GVItem.DataSource = Nothing
        GVItem.DataBind()
        GVItem.Visible = False
        'End If

        GVDtl.DataSource = Nothing
        GVDtl.DataBind()
        ViewState("dtlpurchasereturn") = Nothing
    End Sub

    Protected Sub GVInv_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GVInv.SelectedIndexChanged
        troid.Text = GVInv.SelectedDataKey("trnbelimstoid")
        trnosj.Text = GVInv.SelectedDataKey("trnsjbelino")
        trnopo.Text = GVInv.SelectedDataKey("trnbelipono")
        invoiceno.Text = GVInv.SelectedDataKey("trnbelino")
        paytype.Text = GVInv.SelectedDataKey("trnpaytype")
        taxpct.Text = GVInv.SelectedDataKey("trntaxpct")
        LbIDSJ.Text = GVInv.SelectedDataKey("trnsjbelioid")
        qtyPo.Text = GVInv.SelectedDataKey("qtyitem")
        mCabang.Text = GVInv.SelectedDataKey("ToBranch")
        AmtSisa.Text = GVInv.SelectedDataKey("AmtSisa")
        discheadertype.SelectedValue = GetStrData("select trnbelidisctype from ql_trnbelimst where trnbelino = '" & invoiceno.Text & "' ")
        discheader.Text = GetStrData("select trnbelidisc from ql_trnbelimst where trnbelino = '" & invoiceno.Text & "' ")
        totaldischeader.Text = GetStrData("select trnbelidisc from ql_trnbelimst where trnbelino = '" & invoiceno.Text & "' ")
        temptotalamount.Text = GetStrData("select (amtbeli-amtdiscdtl) temptotal from ql_trnbelimst where trnbelino = '" & invoiceno.Text & "' ")

        tempdisc.Text = GetStrData("select amtdischdr  from ql_trnbelimst where trnbelino = '" & invoiceno.Text & "' ")
        cariIv.Visible = False
    End Sub

    Protected Sub btneraseInvoice_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btneraseInvoice.Click
        notr.Text = "" : troid.Text = "" : trnodp.Text = ""
        trnosj.Text = "" : trnopo.Text = "" : trdate.Text = ""
        invoiceno.Text = "" : paytype.Text = "" : LbIDSJ.Text = ""
        qtyPo.Text = 0

        grossreturn.Text = "0.00" : taxamount.Text = "0.00" : taxpct.Text = 0
        totaldischeader.Text = "0.00" : discheadertype.SelectedIndex = 0
        discheader.Text = "0.00" : discamount.Text = "0.00"
        totalamount.Text = "0.00"

        'If GVTr.Visible = True Then
        ViewState("trpurchasereturnInv") = Nothing
        GVInv.DataSource = Nothing
        GVInv.DataBind()
        GVInv.Visible = False
        'End If

        itemname.Text = "" : itemcode.Text = ""
        itemoid.Text = "" : gudangreturqty.Text = "0.00"
        poprice.Text = "0.00" : qty.Text = "0.00"
        Unit.Text = "" : satuan1.Text = "" : priceperunit.Text = "0.00"
        disc1type.SelectedIndex = 0
        disc2type.SelectedIndex = 0
        disc3type.SelectedIndex = 0
        disc1.Text = "0.00" : disc2.Text = "0.00" : disc3.Text = "0.00"
        discamount1.Text = "0.00" : discamount2.Text = "0.00"
        discamount3.Text = "0.00" : totalamountdtl.Text = "0.00"
        totaldisc.Text = "0.00" : netamount.Text = "0.00"
        notedtl.Text = "" : labelseq.Text = "" : detailstate.Text = "new"

        If GVItem.Visible = True Then
            ViewState("itempurchasereturn") = Nothing
            GVItem.DataSource = Nothing
            GVItem.DataBind()
            GVItem.Visible = False
        End If

        GVDtl.DataSource = Nothing
        GVDtl.DataBind()
        ViewState("dtlpurchasereturn") = Nothing
        cariIv.Visible = False
    End Sub

    Private Function setTabelSN() As DataTable
        Dim dtSN As New DataTable
        dtSN.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSN.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSN.Columns.Add("SN", Type.GetType("System.String"))
        Return dtSN
    End Function

    Protected Sub EnterSN_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles EnterSN.Click
        sSql = "select COUNT(a.sn) FROM QL_mstitemDtl a  WHERE a.last_trans_type not in( 'Jual') AND a.sn = '" & TextSN.Text.Trim & "' and a.itemoid = " & KodeItem.Text & " AND branch_code = '" & Session("branch_id") & "' AND status_item not in ('In Process' , 'In Approval', 'Approved') AND locoid = " & itemLoc.Text & " and status_in_out = 'In'"
        Dim SN As Object = cKon.ambilscalar(sSql)
        If Not SN = 0 Then
            If Session("TblSN") Is Nothing Then
                Dim dtlTableSN As DataTable = setTabelSN()
                Session("TblSN") = dtlTableSN
            End If

            Dim objTableSN As DataTable
            Dim objTableJUmSN As DataTable
            objTableSN = Session("TblSN")
            Dim dv As DataView = objTableSN.DefaultView

            objTableJUmSN = Session("TblSN")
            Dim jumDvSN As DataView = objTableJUmSN.DefaultView

            If NewSN.Text = "New SN" Then
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "'"
                'dvjum.RowFilter = "itemoid= '" & KodeItem.Text & "'"
            Else
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "' AND SNseq <>" & SNseq.Text
                'dvjum.RowFilter = "itemoid= '" & KodeItem.Text & "' AND SNseq <>" & SNseq.Text
            End If

            If dv.Count > 0 Then
                LabelPesanSn.Text = "Serial Number Sudah Entry"
                dv.RowFilter = ""
                cProc.SetModalPopUpExtender(btnHideSN, pnlInvnSN, ModalPopupSN, True)
                TextSN.Text = ""
                cProc.SetFocusToControl(Me.Page, TextSN)
                Exit Sub
            End If

            jumDvSN.RowFilter = "itemoid= '" & KodeItem.Text & "'"
            If jumDvSN.Count >= TextQty.Text Then
                LabelPesanSn.Text = "Jumlah Item Tidak Sesuai Dengan jumlah Serial Number"
                dv.RowFilter = ""
                cProc.SetModalPopUpExtender(btnHideSN, pnlInvnSN, ModalPopupSN, True)
                TextSN.Text = ""
                cProc.SetFocusToControl(Me.Page, TextSN)
                Exit Sub
            End If

            dv.RowFilter = ""
            'insert/update to list data
            Dim objRow As DataRow
            If NewSN.Text = "New SN" Then
                objRow = objTableSN.NewRow()
                objRow("SNseq") = objTableSN.Rows.Count + 1
            Else
                Dim selrow As DataRow() = objTableSN.Select("SNseq=" & SNseq.Text)
                objRow = selrow(0)
                objRow.BeginEdit()
            End If

            objRow("itemoid") = KodeItem.Text
            objRow("SN") = TextSN.Text

            If NewSN.Text = "New SN" Then
                objTableSN.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If


            Session("TblSN") = objTableSN
            GVSN.Visible = True
            GVSN.DataSource = Nothing
            Dim dvSN As DataView = objTableSN.DefaultView

            dvSN.RowFilter = "itemoid= '" & KodeItem.Text & "'"
            GVSN.DataSource = dvSN
            GVSN.DataBind()
            LabelPesanSn.Text = ""
            SNseq.Text = objTableSN.Rows.Count + 1
            GVSN.SelectedIndex = -1
            TextSN.Focus()
        Else
            LabelPesanSn.Text = "Serial Number Tidak Dikenal"
            TextSN.Text = ""
            cProc.SetFocusToControl(Me.Page, TextSN)
        End If
        cProc.SetModalPopUpExtender(btnHideSN, pnlInvnSN, ModalPopupSN, True)
        TextSN.Text = ""
        cProc.SetFocusToControl(Me.Page, TextSN)
    End Sub

    Protected Sub lkbCloseSN_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbCloseSN.Click

        Dim Objdtl As DataTable = ViewState("dtlpurchasereturn")
        '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@
        If Not Session("TblSN") Is Nothing Then
            Dim objTableSN As DataTable = Session("TblSN")
            Dim dvSN As DataView = objTableSN.DefaultView
            Dim itemid As String = itemoid.Text
            dvSN.RowFilter = "itemoid = " & itemid

            Dim dvdtl As DataView = Objdtl.DefaultView
            dvdtl.RowFilter = "itemoid = " & itemid
            If dvdtl.Count = 0 Then
                If Not dvSN.Count = 0 Then
                    For kdsn As Integer = 0 To dvSN.Count - 1
                        objTableSN.Rows.RemoveAt(0)
                    Next
                End If
                objTableSN.AcceptChanges()
            End If
        End If
        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        itemname.Text = "" : itemoid.Text = "" : itemcode.Text = ""
        itemLoc.Text = "" : texthasSN.Text = "" : gudangreturqty.Text = "0.00"
        poprice.Text = "0.00" : qty.Text = "0.00" : Unit.Text = ""
        unitseq.Text = "" : satuan1.Text = "" : trnbelidtloid.Text = ""
        priceperunit.Text = "0.00" : disc1type.SelectedIndex = 0
        disc2type.SelectedIndex = 0
        disc3type.SelectedIndex = 0
        disc1.Text = "0.00" : disc2.Text = "0.00" : disc3.Text = "0.00"
        discamount1.Text = "0.00" : discamount2.Text = "0.00"
        discamount3.Text = "0.00" : totalamountdtl.Text = "0.00"
        totaldisc.Text = "0.00" : netamount.Text = "0.00"
        notedtl.Text = "" : labelseq.Text = ""
        detailstate.Text = "new"
        GVDtl.SelectedIndex = -1
        GVDtl.Columns(8).Visible = True
        'calculateheader()

        GVDtl.SelectedIndex = -1
        btnaddtolist.Visible = True
    End Sub

    Protected Sub lkbPilihItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbPilihItem.Click
        If Not Session("TblSN") Is Nothing Then
            Dim objTableSN As DataTable
            objTableSN = Session("TblSN")
            Dim dvjum As DataView = objTableSN.DefaultView
            dvjum.RowFilter = "itemoid = '" & KodeItem.Text & "'"

            If dvjum.Count = TextQty.Text Then
                dvjum.RowFilter = ""
                Dim dtab As DataTable
                dtab = ViewState("dtlpurchasereturn")
                Dim dv As DataView = dtab.DefaultView
                'Cek apa sudah ada item yang sama dalam Tabel Detail

                If dtab.Rows.Count > 0 Then
                    Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(itemoid.Text) & " AND seq <> " & Integer.Parse(labelseq.Text) & "")
                    If drowc.Length > 0 Then
                        showMessage("Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list !", 2)
                        Exit Sub
                    End If
                End If

                If detailstate.Text = "new" Then
                    labelseq.Text = GVDtl.Rows.Count + 1
                    Dim drow As DataRow = dtab.NewRow
                    drow("seq") = Integer.Parse(labelseq.Text)
                    drow("itemcode") = itemcode.Text
                    drow("itemname") = itemname.Text
                    drow("itemloc") = itemLoc.Text
                    drow("qty") = ToDouble(qty.Text)
                    drow("unit") = Unit.Text
                    drow("price") = ToDouble(priceperunit.Text)
                    drow("note") = notedtl.Text
                    drow("itemoid") = Integer.Parse(itemoid.Text)
                    'drow("gudangqty") = ToDouble(gudangreturqty.Text)
                    'drow("poprice") = ToDouble(poprice.Text)
                    drow("disc1type") = disc1type.SelectedValue.ToString
                    drow("disc2type") = disc2type.SelectedValue.ToString
                    drow("disc3type") = disc3type.SelectedValue.ToString
                    drow("disc1") = ToDouble(disc1.Text)
                    drow("disc2") = ToDouble(disc2.Text)
                    drow("disc3") = ToDouble(disc3.Text)
                    drow("disc1amt") = ToDouble(discamount1.Text)
                    drow("disc2amt") = ToDouble(discamount2.Text)
                    drow("disc3amt") = ToDouble(discamount3.Text)
                    drow("totalamt") = ToDouble(totalamountdtl.Text)
                    drow("totaldisc") = ToDouble(totaldisc.Text)
                    drow("netamt") = ToDouble(netamount.Text)
                    drow("unitoid") = Integer.Parse(satuan1.Text)
                    drow("unitseq") = Integer.Parse(unitseq.Text)
                    drow("trnbelidtloid") = trnbelidtloid.Text
                    drow("has_sn") = texthasSN.Text
                    dtab.Rows.Add(drow)
                Else
                    Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "")
                    If drow.Length > 0 Then
                        drow(0).BeginEdit()
                        drow(0)("itemcode") = itemcode.Text
                        drow(0)("itemname") = itemname.Text
                        drow(0)("itemloc") = itemLoc.Text
                        drow(0)("qty") = ToDouble(qty.Text)
                        drow(0)("unit") = Unit.Text
                        drow(0)("price") = ToDouble(priceperunit.Text)
                        drow(0)("note") = notedtl.Text
                        drow(0)("itemoid") = Integer.Parse(itemoid.Text)
                        'drow(0)("gudangqty") = ToDouble(gudangreturqty.Text)
                        'drow(0)("poprice") = ToDouble(poprice.Text)
                        drow(0)("disc1type") = disc1type.SelectedValue.ToString
                        drow(0)("disc2type") = disc2type.SelectedValue.ToString
                        drow(0)("disc3type") = disc3type.SelectedValue.ToString
                        drow(0)("disc1") = ToDouble(disc1.Text)
                        drow(0)("disc2") = ToDouble(disc2.Text)
                        drow(0)("disc3") = ToDouble(disc3.Text)
                        drow(0)("disc1amt") = ToDouble(discamount1.Text)
                        drow(0)("disc2amt") = ToDouble(discamount2.Text)
                        drow(0)("disc3amt") = ToDouble(discamount3.Text)
                        drow(0)("totalamt") = ToDouble(totalamountdtl.Text)
                        drow(0)("totaldisc") = ToDouble(totaldisc.Text)
                        drow(0)("netamt") = ToDouble(netamount.Text)
                        drow(0)("unitoid") = Integer.Parse(satuan1.Text)
                        drow(0)("unitseq") = Integer.Parse(unitseq.Text)
                        drow(0)("trnbelidtloid") = trnbelidtloid.Text
                        drow(0)("has_sn") = texthasSN.Text
                        drow(0).EndEdit()
                    Else
                        showMessage("Sorry, missing something!", 2)
                        Exit Sub
                    End If
                End If

                dtab.AcceptChanges()
                dtab.Select("", "seq")
                GVDtl.DataSource = dtab
                GVDtl.DataBind()
                ViewState("dtlpurchasereturn") = dtab

                itemname.Text = "" : itemoid.Text = "" : itemcode.Text = ""
                itemLoc.Text = "" : gudangreturqty.Text = "0.00"
                poprice.Text = "0.00" : qty.Text = "0.00"
                Unit.Text = "" : unitseq.Text = "" : satuan1.Text = ""
                trnbelidtloid.Text = "" : priceperunit.Text = "0.00"
                disc1type.SelectedIndex = 0
                disc2type.SelectedIndex = 0
                disc3type.SelectedIndex = 0
                disc1.Text = "0.00" : disc2.Text = "0.00" : disc3.Text = "0.00"
                discamount1.Text = "0.00" : discamount2.Text = "0.00"
                discamount3.Text = "0.00" : totalamountdtl.Text = "0.00"
                totaldisc.Text = "0.00" : netamount.Text = "0.00"
                notedtl.Text = "" : labelseq.Text = ""
                texthasSN.Text = "" : detailstate.Text = "new"
                GVDtl.SelectedIndex = -1
                GVDtl.Columns(8).Visible = True
                calculateheader()

                GVDtl.SelectedIndex = -1
                btnaddtolist.Visible = True
            Else
                LabelPesanSn.Text = "Jumlah Item Tidak Sesuai Dengan jumlah Serial Number"
                cProc.SetModalPopUpExtender(btnHideSN, pnlInvnSN, ModalPopupSN, True)
                TextSN.Text = ""
                cProc.SetFocusToControl(Me.Page, TextSN)
            End If

        Else
            LabelPesanSn.Text = "Mohon Masukan Kode Serial Number"
            cProc.SetModalPopUpExtender(btnHideSN, pnlInvnSN, ModalPopupSN, True)
            TextSN.Text = ""
            cProc.SetFocusToControl(Me.Page, TextSN)
        End If
    End Sub

    Protected Sub GVSN_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles GVSN.RowDeleting
        Dim iIndex As Int16 = e.RowIndex
        Dim objTableSN As DataTable = Session("TblSN")
        objTableSN.Rows.RemoveAt(iIndex)

        'resequence sjDtlsequence 
        For C1 As Int16 = 0 To objTableSN.Rows.Count - 1
            Dim dr As DataRow = objTableSN.Rows(C1)
            dr.BeginEdit()
            dr("SNseq") = C1 + 1
            dr.EndEdit()
        Next

        Session("TblSN") = objTableSN
        GVSN.Visible = True
        GVSN.DataSource = objTableSN
        GVSN.DataBind()
        ModalPopupSN.Show()
        LabelPesanSn.Text = ""
        TextSN.Text = ""
        cProc.SetFocusToControl(Me.Page, TextSN)
    End Sub

    Protected Sub GVInv_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'GVInv.DataSource = ViewState("trpurchasereturnInv")
        GVInv.PageIndex = e.NewPageIndex
        InvBindData()
        'GVInv.DataBind()
    End Sub
#End Region

    Protected Sub TypeNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TypeNya.SelectedIndexChanged
        InvBindData()
    End Sub

    Protected Sub CabangNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CabangNya.SelectedIndexChanged
        initallddl()
    End Sub

    Protected Sub GVTrn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVTrn.SelectedIndexChanged
        Response.Redirect("~\Transaction\trnPurchaseReturn.aspx?branch_code=" & GVTrn.SelectedDataKey("branch_code").ToString & "&oid=" & GVTrn.SelectedDataKey("trnbeliReturmstoid") & "")
    End Sub

    Protected Sub tbperiodstart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbperiodstart.TextChanged
        CbPeriode.Checked = True
    End Sub
End Class
