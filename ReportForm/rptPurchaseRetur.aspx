<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPurchaseRetur.aspx.vb" Inherits="ReportForm_rptPurchaseRetur" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
        Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
    <%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
        Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    <table align="left" border="0" width="100%">
        <tr>
            <td colspan="3" align="center">
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Navy"
                                Text=".: Laporan Retur Pembelian" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                    <tr>
                        <th align="center" style="background-color: transparent" valign="center">
                            <table width="100%">
                                <tr>
                                    <td align="center" colspan="3">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
<TABLE><TBODY><TR><TD align=left>Cabang</TD><TD align=left><asp:DropDownList id="dCabangNya" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w129"></asp:DropDownList></TD></TR><TR><TD align=left>Periode </TD><TD align=left><asp:TextBox id="range1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w130"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w131"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w132"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w133"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Font-Size="8pt" Text="(dd/MM/yyyy)" __designer:wfdid="w134"></asp:Label></TD></TR><TR><TD align=left>Type </TD><TD align=left><asp:DropDownList id="ddltype" runat="server" Width="110px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w135" AutoPostBack="True"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label5" runat="server" Width="62px" Text="Supplier" __designer:wfdid="w237"></asp:Label></TD><TD align=left><asp:TextBox id="custcode" runat="server" Width="180px" CssClass="inpText" __designer:wfdid="w238" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w239"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w240"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label8" runat="server" Width="49px" Text="No. PI" __designer:wfdid="w136"></asp:Label></TD><TD align=left><asp:TextBox id="dono" runat="server" Width="180px" CssClass="inpText" __designer:wfdid="w137" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindDO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w138"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseDO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w139"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Width="49px" Text="No. PR" __designer:wfdid="w59"></asp:Label></TD><TD align=left><asp:TextBox id="sono" runat="server" Width="180px" CssClass="inpText" __designer:wfdid="w192" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w193"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w194"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Width="14px" Text="Katalog" __designer:wfdid="w65" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="itemcode" runat="server" Width="180px" CssClass="inpText" __designer:wfdid="w218" TextMode="MultiLine" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w219" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w220" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label4" runat="server" Width="33px" Text="PIC" __designer:wfdid="w161" Visible="False"></asp:Label> </TD><TD align=left><asp:DropDownList id="sales" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w162" AutoPostBack="True" Visible="False"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=2><asp:ImageButton id="btnshowprint" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w163"></asp:ImageButton> <asp:ImageButton id="btnpdf" onclick="BTNpRINT_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w164"></asp:ImageButton> <asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w165"></asp:ImageButton> <asp:ImageButton id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w166"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w167" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w168"></asp:Image><BR __designer:dtid="844424930132047" /><SPAN style="FONT-SIZE: 12pt">Sedang proses....</SPAN><BR /></SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w170" Format="dd/MM/yyyy" PopupButtonID="ImageButton2" TargetControlID="range2">
                                        </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w171" Format="dd/MM/yyyy" PopupButtonID="ImageButton1" TargetControlID="range1">
                                        </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w172" TargetControlID="range1" CultureName="id-ID" ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date">
                                        </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w173" TargetControlID="range2" CultureName="id-ID" ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date">
                                        </ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:dtid="281474976710669" __designer:wfdid="w174" AutoDataBind="true"></CR:CrystalReportViewer> 
</ContentTemplate>
                    <Triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
</Triggers>
                </asp:UpdatePanel></td>
                                </tr>
                            </table>
                            <asp:UpdatePanel id="upListDO" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelListDO" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListDO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Nota PI"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListDO" runat="server" Width="100%" DefaultButton="btnFindListDO"><asp:Label id="Label24" runat="server" Font-Size="8pt" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListDO" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="doitemno">No. PI</asp:ListItem>
<asp:ListItem Value="doitemmstoid">No. Draft</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListDO" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"></asp:TextBox> <asp:ImageButton id="btnFindListDO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListDO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllDO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneDO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedDO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListDO" runat="server" Width="98%" ForeColor="#333333" GridLines="None" PageSize="8" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                        <asp:CheckBox ID="cbLMDO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("doitemmstoid") %>' />
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="doitemmstoid" HeaderText="No. Draft">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemno" HeaderText="No. SI">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                &nbsp;
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListDO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListDO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDO" runat="server" TargetControlID="btnHiddenListDO" PopupDragHandleControlID="lblListDO" PopupControlID="PanelListDO" BackgroundCssClass="modalBackground">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListDO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListSO" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelListSO" runat="server" Width="1000px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Purchase Return"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="Label241" runat="server" Font-Size="8pt" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="trnbelino">No. PI</asp:ListItem>
<asp:ListItem Value="soitemno">No. PR</asp:ListItem>
<asp:ListItem Enabled="False" Value="soitemmstoid">No. Draft</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="100%" ForeColor="#333333" GridLines="None" PageSize="8" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMSO" runat="server" ToolTip='<%# eval("soitemmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="soitemmstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemno" HeaderText="No. Retur">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodate" HeaderText="Tgl. Retur">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                &nbsp;
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListSO" BackgroundCssClass="modalBackground">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListMat" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><SPAN style="FONT-SIZE: 8pt">Filter</SPAN> : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="itemlongdesc">Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText" Font-Size="8pt"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><SPAN style="FONT-SIZE: 8pt">Jenis</SPAN> <SPAN style="FONT-SIZE: 8pt">Barang</SPAN> : <asp:DropDownList id="JenisBarangDDL" runat="server" CssClass="inpText" Font-Size="8pt"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText" Font-Size="8pt"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server" Font-Size="8pt">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" Width="100%" ForeColor="#333333" GridLines="None" PageSize="8" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemcode">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                        <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                &nbsp;
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" Drag="True">
                                        </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListCust" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust"><SPAN style="FONT-SIZE: 8pt">Filter</SPAN> : <asp:DropDownList id="DDLFilterListCust" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="custname">Supplier</asp:ListItem>
<asp:ListItem Value="Custcode">Kode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListCust" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="100%" ForeColor="#333333" GridLines="None" PageSize="8" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="custoid,custcode,custname">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                    <asp:CheckBox ID="cbLMCust" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>' />
                                                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListCust" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" PopupDragHandleControlID="lblListCust" PopupControlID="pnlListCust" BackgroundCssClass="modalBackground" Drag="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upPopUpMsg" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </th>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
</asp:Content>

