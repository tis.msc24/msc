'Prgmr:ifan | LastUpdt:10.10.10
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine


Partial Class trndpar_payother
    Inherits System.Web.UI.Page

#Region "Variables"
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ql_Conn"))
    Dim objCmd As New SqlCommand("", conn)
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function MonthToAlphabet(ByVal iMonth As Integer) As String
        Select Case iMonth
            Case 1 : Return "A"
            Case 2 : Return "B"
            Case 3 : Return "C"
            Case 4 : Return "D"
            Case 5 : Return "E"
            Case 6 : Return "F"
            Case 7 : Return "G"
            Case 8 : Return "H"
            Case 9 : Return "I"
            Case 10 : Return "J"
            Case 11 : Return "K"
            Case 12 : Return "L"
            Case Else : Return "?"
        End Select
    End Function

    Private Function seperate(ByVal cabangin As String) As String
        Dim init As String = "'" & cabangin & "'"
        init = init.Replace(",", "','").Replace(" ", "")
        Return init
    End Function

#End Region

#Region "Procedures"
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal no As String, ByVal type As String)
        'untuk print
        If type <> "" Then
            If type = "BBK" Then
                report.Load(Server.MapPath(folderReport & "printDPAR_ReturBBK.rpt"))
            ElseIf type = "BKK" Then
                report.Load(Server.MapPath(folderReport & "printDPAR_ReturBKK.rpt"))
            ElseIf type = "BGK" Then
                report.Load(Server.MapPath(folderReport & "printDPAR_ReturBGK.rpt"))
            End If

            report.SetParameterValue("cmpcode", cmpcode)
            report.SetParameterValue("no", no)
            report.SetParameterValue("companyname", CompnyName)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no)
            report.Close() : report.Dispose() : Session("no") = Nothing
        End If
    End Sub

    Private Sub FillTextBox(ByVal iOid As Integer)
        sSql = "SELECT ar.trndparoid,ar.cashbankoid,ar.trndparno,ar.trndpardate,ar.custoid custoid,c.custname suppname,ar.payreftype,ar.cashbankacctgoid,ar.payduedate,ar.payrefno,ar.trndparacctgoid,ar.currencyoid,ar.currencyrate,ar.trndparamt,ar.trndparnote,ar.trndparstatus,ar.upduser,ar.updtime, AR.trndparoid_ref , (select d.trndparno  from ql_trndpar d Where d.cmpcode='" & cmpcode & "' and d.trndparoid=ar.trndparoid_ref  )dpno, trndparacumamt FROM QL_trndpar_back ar INNER JOIN QL_mstcust c ON c.cmpcode=ar.cmpcode AND c.custoid=ar.custoid WHERE ar.cmpcode='" & cmpcode & "' AND ar.trndparoid=" & iOid

        Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_trndpar")
        If dtMst.Rows.Count < 1 Then
            showMessage("Gagal membuka data Retur-Uang Muka Penjualan !!<BR>Info :<BR>" & sSql, 1)
            Exit Sub
        Else
            FillDDL(CabangNya, "select gencode,gendesc from QL_mstgen where gengroup='CABANG'")
            CabangNya.SelectedValue = GetStrData(" select gencode from QL_trndpar INNER JOIN QL_mstgen on gencode=branch_code where trndparoid = '" & dtMst.Rows(0)("trndparoid_ref").ToString & "'")
            trndparoid.Text = dtMst.Rows(0)("trndparoid").ToString
            cashbankoid.Text = dtMst.Rows(0)("cashbankoid").ToString
            trndparno.Text = dtMst.Rows(0)("trndparno").ToString
            trndpardate.Text = Format(CDate(dtMst.Rows(0)("trndpardate").ToString), "dd/MM/yyyy")
            custoid.Text = dtMst.Rows(0)("custoid").ToString
            custname.Text = dtMst.Rows(0)("suppname").ToString
            payreftype.SelectedValue = dtMst.Rows(0)("payreftype").ToString
            currentpayreftype.Text = dtMst.Rows(0)("payreftype").ToString
            InitDDLCashBank(payreftype.SelectedValue)
            cashbankacctgoid.SelectedValue = dtMst.Rows(0)("cashbankacctgoid").ToString
            currentcbacctgoid.Text = dtMst.Rows(0)("cashbankacctgoid").ToString

            If payreftype.SelectedValue <> "CASH" Then
                payduedate.Text = Format(CDate(dtMst.Rows(0)("payduedate").ToString), "dd/MM/yyyy")
                payrefno.Text = dtMst.Rows(0)("payrefno").ToString
            End If
            initAcctgoid()
            trndparacctgoid.SelectedValue = dtMst.Rows(0)("trndparacctgoid").ToString
            currencyoid.SelectedValue = dtMst.Rows(0)("currencyoid").ToString
            currencyrate.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("currencyrate").ToString), 4)
            PAYAMT.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndparamt").ToString), 4)
            trndparamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndparacumamt").ToString), 4)
            trndparnote.Text = dtMst.Rows(0)("trndparnote").ToString
            trndparstatus.Text = dtMst.Rows(0)("trndparstatus").ToString
            upduser.Text = dtMst.Rows(0)("upduser").ToString
            updtime.Text = dtMst.Rows(0)("updtime").ToString
            dpno.Items.Clear()
            dpno.Items.Add(dtMst.Rows(0)("dpno"))
            dpno.Items(0).Value = dtMst.Rows(0)("trndparoid_ref")
            If trndparstatus.Text <> "POST" Then
                btnSave.Visible = True : btnDelete.Visible = True : btnPosting.Visible = True
            Else
                CabangNya.Enabled = False : btnSave.Visible = False : btnDelete.Visible = False : btnPosting.Visible = False
            End If
            trndpardate.Enabled = False
            imbDPARDate.Visible = False
            trndpardate.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(CabangNya, sSql) 
                CabangNya.SelectedValue = Session("branch_id")
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql) 
            CabangNya.SelectedValue = Session("branch_id")
        End If
    End Sub

    Private Sub InitfDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fddlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fddlCabang, sSql)
            Else
                FillDDL(fddlCabang, sSql)
                fddlCabang.Items.Add(New ListItem("ALL", "ALL"))
                fddlCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fddlCabang, sSql)
            fddlCabang.Items.Add(New ListItem("ALL", "ALL"))
            fddlCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub showTableCOA(ByVal noRef As String, ByVal cmpcode As String, ByVal Obj As GridView)
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim ssql As String = "select a.acctgcode acctgcode, a.acctgdesc, case d.gldbcr when 'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit', glnote from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid where noref ='" & noRef & "'  order by d.gldbcr desc, a.acctgcode"
        'and d.branch_code='" & cmpcode & "'
        Dim xadap As New SqlDataAdapter(ssql, conn)
        Dim otable As New DataTable
        xadap.Fill(otable)
        If otable.Rows.Count = 0 Then
            ssql = "select a.acctgcode acctgcode , a.acctgdesc, case d.gldbcr when 'D' then d.glamt else 0 end 'Debet',  case d.gldbcr when  'C' then d.glamt else 0 end 'Kredit' , glnote  from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid   where glnote like '%" & noRef & "'   order by d.gldbcr desc, a.acctgcode"
            ' and d.branch_code='" & cmpcode & "'
            otable.Rows.Clear()
            Dim xadap2 As New SqlDataAdapter(ssql, conn)
            xadap2.Fill(otable)
        End If
        Obj.DataSource = otable
        Obj.DataBind()

    End Sub

    Private Sub BindData(ByVal sPLus As String)
        sSql = "SELECT ar.cashbankoid,ar.trndparoid,ar.trndparno,ar.trndpardate,c.custname suppname,ar.trndparamt,ar.trndparstatus, ar.trndparnote, trndparflag, (select d.trndparno from ql_trndpar d where d.trndparoid=ar.trndparoid_ref  )dpno, (select gendesc  from ql_trndpar d INNER JOIN QL_mstgen on gencode=branch_code where d.trndparoid=ar.trndparoid_ref and gengroup = 'CABANG') as branch_code FROM QL_trndpar_back ar INNER JOIN QL_mstcust c ON c.cmpcode=ar.cmpcode AND c.custoid=ar.custoid WHERE ar.cmpcode='" & cmpcode & "' " & sPLus & " and  ar.trndpardate  between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "' ORDER BY trndparno"
        FillGV(gvMst, sSql, "QL_trndpar")
    End Sub

    Private Sub BindCustomer(ByVal sPlus As String)
        sSql = "SELECT c.custoid,c.custcode, c.custname FROM QL_mstcust c INNER JOIN QL_TRNdpar p on p.custoid=c.custoid and p.cmpcode=C.cmpcode WHERE p.branch_code = '" & CabangNya.SelectedValue & "' AND c.cmpcode='" & cmpcode & "' " & sPlus & " GROUP BY c.custoid, c.custcode, c.custname having SUM(p.trndparamt)-SUM(p.trndparacumamt) > 0 ORDER BY c.custname"
        FillGV(gvCust, sSql, "QL_mstcreditcard")
    End Sub

    Private Sub SetControlCashBank(ByVal bState As String)
        lblDueDate.Visible = bState : lblNeedDue.Visible = bState
        payduedate.Visible = bState : imbDueDate.Visible = bState : lblDate.Visible = bState
        lblRefNo.Visible = bState 'lblNeedRef.Visible = bState : 
        payrefno.Visible = bState
    End Sub

    Private Sub InitDDLCashBank(ByVal sCashbank As String)
        cashbankacctgoid.Items.Clear()
        If Trim(sCashbank) = "CASH" Then
            Dim varCash As String = GetVarInterface("VAR_CASH", CabangNya.SelectedValue)
            If varCash = "" Then
                showMessage("Silakan buat Perkiraan Kas di Data Perkiraan (VAR_CASH) !!", 2)
            Else
                sSql = "SELECT a1.acctgoid,a1.acctgcode+'-'+a1.acctgdesc FROM QL_mstacctg a1 WHERE a1.acctgcode IN (" & seperate(varCash) & ")   "
                sSql &= "  AND a1.acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=a1.cmpcode ) ORDER BY a1.acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If
        ElseIf Trim(sCashbank) = "GIRO" Then
            Dim varCREDITCARD As String = GetVarInterface("VAR_GIRO_HUTANG", CabangNya.SelectedValue)
            If varCREDITCARD = "" Then
                showMessage("Silakan buat Perkiraan Giro Piutang di Data Perkiraan !!", 2)
            Else
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode IN (" & seperate(varCREDITCARD) & ")  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode ) ORDER BY acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If
        ElseIf Trim(sCashbank) = "NON CASH" Then
            Dim varBank As String = GetVarInterface("VAR_BANK", CabangNya.SelectedValue)
            If varBank = "" Then
                showMessage("Silakan buat Perkiraan Bank di Data Perkiraan !!", 2)
            Else
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode IN (" & seperate(varBank) & ")  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode ) ORDER BY acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If

        ElseIf Trim(sCashbank) = "OTHER" Then
            Dim varBank As String = GetVarInterface("VAR_BANK", CabangNya.SelectedValue)
            Dim varCash As String = GetVarInterface("VAR_CASH", CabangNya.SelectedValue)
            If varBank = "" Then
                showMessage("Silakan buat Perkiraan Bank di Data Perkiraan !!", 2)
            Else
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE   (acctgcode NOT IN (" & seperate(varBank) & ")  AND  acctgcode NOT IN (" & seperate(varCash) & "))   AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode ) ORDER BY acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If
        End If
        If sCashbank = "NON CASH" Then
            SetControlCashBank(True)
        Else
            SetControlCashBank(False)
        End If
        creditsearch.Visible = False
        CREDITCLEAR.Visible = False
        code.Visible = False : code.Text = ""
        payrefno.Text = "" : payrefno.Enabled = True
        payrefno.CssClass = "inpText"
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 isnull(curratestoIDRbeli,1) from QL_mstcurrhist where cmpcode='" & cmpcode & "' and curroid=" & iOid & " order by currdate desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyrate.Text = ToMaskEdit(objCmd.ExecuteScalar, 4)
        conn.Close()
    End Sub

    Private Sub InitAllDDL()
        FillDDL(currencyoid, "select 1,0 ")
        If currencyoid.Items.Count > 0 Then
            FillCurrencyRate(currencyoid.SelectedItem.Value)
        Else
            currencyrate.Text = ToMaskEdit(1, 4)
        End If
        InitDDLCashBank(payreftype.SelectedValue)
    End Sub

    Private Sub initAcctgoid()
        ' dpar
        Dim vardpar As String = GetVarInterface("VAR_DPAR", CabangNya.SelectedValue)
        If vardpar = "" Then
            showMessage("Buat Perkiraan untuk Uang Muka Penjualan (VAR_DPAR) !!", 2)
        Else
            sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & vardpar & "%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
            FillDDL(trndparacctgoid, sSql)
        End If
    End Sub

    Private Sub GenerateNo()
        Dim reqcode As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim code As String = ""
            Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")
            code = "DPR/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trndparno,1) AS INT)),0) FROM ql_trndpar_back WHERE trndparno LIKE '" & code & "%'"
            objCmd.CommandText = sSql
            Dim sequence As String = Format((objCmd.ExecuteScalar + 1), "0000")
            reqcode = code & sequence
            trndparno.Text = reqcode

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        '' THROW TO NOT AUTHORIZED PAGE IF NO ROLE EXIST
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId

            Session("wesposting") = "false"
            Response.Redirect("~\accounting\trndpar_payother.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & "- Down Payment AR - (Return/Pay Other)" ' CHANGE FORM NAME HERE
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk HAPUS data ini?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk POSTING data ini?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not Page.IsPostBack Then
            InitDDLBranch() : InitfDDLBranch()
            InitAllDDL() : initAcctgoid()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid")) ' FOR UPDATE DATE
                TabContainer1.ActiveTabIndex = 1
                payreftype.Enabled = False
                payreftype.CssClass = "inpTextDisabled"
                imbSearchCust.Visible = False
                imbClearCust.Visible = False
            Else
                trndpardate.Text = Format(GetServerTime, "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime, "dd/MM/yyyy")
                InitDDLCashBank(payreftype.SelectedValue)
                btnDelete.Visible = False
                trndparstatus.Text = "In Process"
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0
                trndparoid.Text = GenerateID("QL_trndpar_back", cmpcode)
                trndparno.Text = GenerateID("QL_trndpar_back", cmpcode)
            End If
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            Dim sCabang As String = ""
            If fddlCabang.SelectedValue <> "ALL" Then
                sCabang = " AND (select d.branch_code from ql_trndpar d where d.trndparoid=ar.trndparoid_ref) = '" & fddlCabang.SelectedValue & "'"
            End If
            BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(filtertext.Text) & "%' " & sCabang & " ")
        End If

        If trndparstatus.Text = "POST" Then
            btnshowCOA.Visible = True
        Else
            btnshowCOA.Visible = False
        End If
    End Sub

    Protected Sub imbOKValidasi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub payreftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLCashBank(payreftype.SelectedValue)
    End Sub

    Protected Sub currencyoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillCurrencyRate(currencyoid.SelectedValue)
    End Sub

    Protected Sub imbSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindCustomer("  and  " & suppFilter1.SelectedValue & " like '%" & Tchar(filtertext.Text) & "%'")
        cProc.SetModalPopUpExtender(btnHideCust, pnlCust, mpeCust, True)
    End Sub

    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custoid.Text = "" : custname.Text = ""
        dpno.Items.Clear()
        trndparamt.Text = ""
        PAYAMT.Text = ""
    End Sub

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custoid.Text = gvCust.SelectedDataKey("custoid").ToString
        custname.Text = gvCust.SelectedDataKey("custname").ToString
        cProc.SetModalPopUpExtender(btnHideCust, pnlCust, mpeCust, False)
        cProc.DisposeGridView(gvCust)
        sSql = "Select d.trndparoid, d.trndparno from QL_trndpar d where d.custoid=" & custoid.Text & "  and d.trndparamt-d.trndparacumamt>0 and d.cmpcode='" & cmpcode & "' and trndparstatus='POST' AND branch_code ='" & CabangNya.SelectedValue & "'"
        FillDDL(dpno, sSql)
        FillAmountDP() 
    End Sub

    Protected Sub lkbCloseCust_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideCust, pnlCust, mpeCust, False)
        cProc.DisposeGridView(gvCust)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~\accounting\trndpar_payother.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trndparstatus.Text = "POST" : btnSave_Click(sender, e)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans

        Try
            sSql = "DELETE FROM QL_trndpar_back WHERE cmpcode='" & cmpcode & "' AND trndparoid=" & trndparoid.Text
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_cashbankgl WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, 1) : Exit Sub
        End Try
        Response.Redirect("~\accounting\trndpar_payother.aspx?awal=true")
    End Sub

    Protected Sub gvMst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & gvMst.SelectedDataKey.Item("cashbankoid").ToString & ""
        Dim sqlku As String = "SELECT cashbankno FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & gvMst.SelectedDataKey.Item("cashbankoid").ToString & ""
        PrintReport(cKon.ambilscalar(sqlku), cKon.ambilscalar(sSql))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        If trndparstatus.Text = "POST" Then
            If Session("wesposting") = "true" Then
                showMessage("This data has been posted !!  Press cancel or check data in list information", 2) 'Please select a PO Number
                btnPosting.Visible = True
                trndparstatus.Text = "In Process" : Exit Sub
            Else
                Session("wesposting") = "true"
            End If
        End If

        Dim sMsg As String = ""
        If Not IsDate(toDate(trndpardate.Text)) Then
            sMsg &= "- Format Tanggal Salah !!<BR>"
        End If

        If custoid.Text = "" Then
            sMsg &= "- Silahkan Pilih Customer !!<BR>"
        End If

        If cashbankacctgoid.Items.Count < 1 Then
            sMsg &= "- Silahkan Pilih cash, bank atau other COA  !!<BR>"
        End If

        If trndparacctgoid.Items.Count < 1 Then
            sMsg &= "- Silahkan Pilih DP account !!<BR>"
        End If

        If payduedate.Visible Then
            If Not IsDate(toDate(payduedate.Text)) Then
                sMsg &= "- Format Due Date Salah !!<BR>"
            End If
        End If

        If payduedate.Visible And (IsDate(toDate(payduedate.Text))) And (IsDate(toDate(trndpardate.Text))) Then
            If CDate(toDate(payduedate.Text)) < CDate(toDate(trndpardate.Text)) Then
                sMsg &= "- Tanggal jatuh tempo tidak boleh kurang dari tanggal transaksi !!<BR>"
            End If
        End If
        If currencyoid.Items.Count < 1 Then
            sMsg &= "- Buat data Mata Uang di data Currency !!<BR>"
        End If
        If ToDouble(currencyrate.Text) <= 0 Then
            currencyrate.Text = ToMaskEdit(1, 4)
        End If
        If ToDouble(trndparamt.Text) <= 0 Then
            sMsg &= "- DP Amount Harus > 0 !!<BR>"
        End If
        If ToDouble(PAYAMT.Text) <= 0 Then
            sMsg &= "- Pay Amount Harus > 0 !!<BR>"
        End If
        If ToDouble(PAYAMT.Text) <> ToDouble(trndparamt.Text) Then
            sMsg &= "- Pay Amt Harus Sama Dengan DP Amt !!<BR>"
        End If
        If trndparnote.Text.Trim.Length > 200 Then
            sMsg &= "- Note maksimal 200 karakter !!<BR>"
        End If

        'CEK APAKAH GIRO SUDAH DI CLEARING BLOM?
        If dpno.Items.Count > 0 Then
            If dpno.SelectedItem.Text.Trim.IndexOf("DP.AR.G") >= 0 Then
                sSql = "select COUNT(-1) from QL_TRNDPAR A INNER JOIN QL_GiroPaymentDtl d ON A.CMPCODE=d.cmpcode  and d.NoGiro=a.payrefno  and a.trndparoid=" & dpno.SelectedValue & " inner join QL_GiroPaymentMst m on m.GiroPaymentMstOid=d.GiroPaymentMstOid and m.cmpcode=d.cmpcode and m.GiroPaymentStatus='POST' AND paymentref_table IN ('QL_CASHBANKMST-PAYAR','QL_TRNDPAR')"
                If GetStrData(sSql) = 0 Then
                    sSql = "select COUNT(-1) from QL_TRNDPAR A INNER JOIN QL_GiroPaymentDtl d ON A.CMPCODE=d.cmpcode  and d.NoGiro=(select girono + ' / ' + NoRekening  from QL_GIRODTL where girodtloid = (select girodtloid  from QL_trncashbankmst where cashbankno = a.payrefno)) and a.trndparoid=" & dpno.SelectedValue & " inner join QL_GiroPaymentMst m on m.GiroPaymentMstOid=d.GiroPaymentMstOid and m.cmpcode=d.cmpcode and m.GiroPaymentStatus='POST'  AND paymentref_table IN ('QL_CASHBANKMST-PAYAR','QL_TRNDPAR')"
                    If GetStrData(sSql) = 0 Then 'cek hj Again tuk DP Giro dari BGM lebih
                        sMsg &= "- That's Giro must be clearing !!<BR>"
                    End If
                End If

                'cek validasi data
                sSql = "select d.trndparamt-d.trndparacumamt from QL_trndpar d where d.trndparoid=" & dpno.SelectedValue & "  and d.cmpcode='" & cmpcode & "'"
                If ToDouble(trndparamt.Text) <> ToDouble(GetStrData(sSql)) Then
                    sMsg &= "- DP amount isn't valid !!<BR>"
                End If
            End If
        Else
            sMsg &= "- Silahkan Pilih DP No !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Session("wesposting") = "false"
            trndparstatus.Text = "In Process" : Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            GenerateNo()
        End If

        Session("CBMst") = GenerateID("QL_trncashbankmst", cmpcode)
        Session("DPAR") = GenerateID("QL_trndpar_back", cmpcode)
        Session("GLMst") = GenerateID("QL_trnglmst", cmpcode)
        Session("GLDtl") = GenerateID("QL_trngldtl", cmpcode)
        Dim iCbdtloid As Int64 = GenerateID("QL_CASHBANKGL", cmpcode)

        Session("GLSeq") = 1
        currencyrate.Text = 1
        ' temp var utk gl-trans connection
        Dim iCBMst, iTransOid As Integer

        sSql = "select count(-1) from QL_trndpar_back where trnDPARno='" & trndparno.Text & "' and trnDPARstatus='POST' and  cmpcode='" & cmpcode & "'"
        If cKon.ambilscalar(sSql) > 0 Then
            showMessage("Data ini sudah di Posting !, Tekan batal untuk lihat Data di List Information", 2)
            trndparstatus.Text = "In Process" : Session("wesposting") = "false" : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                ' QL_trndpar
                Dim due As String = "" : Dim refno As String = ""
                If payreftype.SelectedValue = "CASH" Then
                    due = "'1/1/1900'" : refno = ""
                Else
                    due = "'" & CDate(toDate(payduedate.Text)) & "'" : refno = Tchar(payrefno.Text)
                End If

                sSql = "INSERT INTO QL_trndpar_back (cmpcode,trndparoid,trndparno,trndpardate,custoid,cashbankoid,trndparacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate,trndparamt,taxtype,taxoid,taxpct,taxamt,trndparnote,trndparflag,trndparacumamt,trndparstatus, createuser,createtime,upduser,updtime, trndparoid_ref) VALUES ('" & cmpcode & "'," & Session("DPAR") & ",'" & trndparno.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))," & custoid.Text & "," & Session("CBMst") & "," & trndparacctgoid.SelectedValue & ",'" & payreftype.SelectedValue & "'," & cashbankacctgoid.SelectedValue & "," & due & ",'" & refno & "'," & currencyoid.SelectedValue & "," & ToDouble(currencyrate.Text) & "," & ToDouble(PayAmt.Text) & ",'',0,0,0,'" & Tchar(trndparnote.Text) & "','CLOSE'," & ToDouble(trndparamt.Text) & ",'" & trndparstatus.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP, " & dpno.SelectedValue & ")"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                iTransOid = Session("DPAR")

                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("DPAR") & " WHERE tablename='QL_trndpar_back' AND cmpcode='" & cmpcode & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                'udp[ate ke dp asline untuk cashbacknya
                sSql = "UPDATE QL_trndpar SET trndparacumamt=trndparacumamt+" & ToDouble(PayAmt.Text) & ", trndparacumamt_BACK=" & ToDouble(PayAmt.Text) & ", TRNdparFLAG= (CASE WHEN trndparacumamt+" & ToDouble(PayAmt.Text) & ">=TRNDPARAMT THEN 'CLOSE' ELSE  'OPEN' end  ) WHERE trndparoid=" & dpno.SelectedValue & " AND cmpcode='" & cmpcode & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                ' QL_trncashbankmst
                Dim sCBType As String = "" : Dim sCBCode As String = ""
                Dim sNo As String = "" : Dim iCurID As Integer = 0
                Dim sAcctgCode As String = ""

                If payreftype.SelectedValue = "CASH" Then
                    sCBType = "BKK"
                ElseIf payreftype.SelectedValue = "NON CASH" Then
                    sCBType = "BBK"
                ElseIf payreftype.SelectedValue = "GIRO" Then
                    sCBType = "BGK"  
                End If

                If sCBType <> "O" Then
                    ' Cashbank No
                    Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")
                    sNo = sCBType & "/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                    sSql = "SELECT MAX(ABS(replace(cashbankno,'" & sNo & "',''))) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%'"
                    objCmd.CommandText = sSql
                    If Not IsDBNull(objCmd.ExecuteScalar) Then
                        iCurID = objCmd.ExecuteScalar + 1
                    Else
                        iCurID = 1
                    End If
                    Session("sCBNo") = GenNumberString(sNo, "", iCurID, 4)
                    cashbankoid.Text = Session("CBMst")
                    sSql = "INSERT INTO QL_trncashbankmst (branch_code,cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,createuser,createtime,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, pic_refname) VALUES ('" & CabangNya.SelectedValue & "','" & cmpcode & "','" & Session("CBMst") & "','" & Session("sCBNo") & "','" & trndparstatus.Text & "','" & sCBType & "','R_DPAR'," & cashbankacctgoid.SelectedValue & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'RETUR UM. PENJUALAN|NO=" & trndparno.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP," & currencyoid.SelectedValue & "," & ToDouble(currencyrate.Text) & ", " & custoid.Text & ", 'QL_MSTCUST')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    iCBMst = Session("CBMst")

                    sSql = "UPDATE QL_mstoid SET lastoid=" & Session("CBMst") & " WHERE tablename='QL_trncashbankmst' AND cmpcode='" & cmpcode & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
            Else

                Dim due As String = "" : Dim refno As String = ""
                If payreftype.SelectedValue = "CASH" Then
                    due = "'1/1/1900'" : refno = ""
                Else
                    due = "'" & CDate(toDate(payduedate.Text)) & "'" : refno = Tchar(payrefno.Text)
                End If

                sSql = "UPDATE QL_trndpar_BACK SET trndparno='" & trndparno.Text & "',trndpardate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),custoid=" & custoid.Text & ",cashbankoid=" & cashbankoid.Text & "," & "trndparacctgoid=" & trndparacctgoid.SelectedValue & ",payreftype='" & payreftype.SelectedValue & "',cashbankacctgoid=" & cashbankacctgoid.SelectedValue & ",payduedate=" & due & ",payrefno='" & refno & "',currencyoid=" & currencyoid.SelectedValue & ",currencyrate=" & ToDouble(currencyrate.Text) & "," & "trndparamt=" & ToDouble(PayAmt.Text) & ",trndparnote='" & Tchar(trndparnote.Text) & "',trndparflag='CLOSE'," & "trndparacumamt=" & ToDouble(trndparamt.Text) & ",trndparstatus='" & trndparstatus.Text & "',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP " & "WHERE cmpcode='" & cmpcode & "' AND trndparoid=" & trndparoid.Text
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                iTransOid = trndparoid.Text

                Dim bUpdPayType As Boolean = False
                Dim sCBType As String = "" : Dim sCBCode As String = ""
                Dim sNo As String = "" : Dim iCurID As Integer = 0

                If payreftype.SelectedValue <> currentpayreftype.Text Or cashbankacctgoid.SelectedValue <> currentcbacctgoid.Text Then
                    ' jika ada perubahan pada payment type
                    If payreftype.SelectedValue = "CASH" Then
                        sCBType = "BKK"
                    ElseIf payreftype.SelectedValue = "NON CASH" Then
                        sCBType = "BBK"
                    ElseIf payreftype.SelectedValue = "GIRO" Then
                        sCBType = "BGK"
                    Else
                        sCBType = "O"
                    End If
                    ' Cashbank No
                    Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")
                    sNo = sCBType & "/" & Cabang & "/" & Format(GetServerTime(), "yy/MM") & "/"
                    sSql = "SELECT MAX(ABS(replace(cashbankno,'" & sNo & "',''))) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%'"
                    objCmd.CommandText = sSql
                    If Not IsDBNull(objCmd.ExecuteScalar) Then
                        iCurID = objCmd.ExecuteScalar + 1
                    Else
                        iCurID = 1
                    End If
                    Session("sCBNo") = GenNumberString(sNo, "", iCurID, 3)
                    bUpdPayType = True
                End If

                If sCBType <> "O" Then
                    sSql = "UPDATE QL_trncashbankmst SET branch_code='" & CabangNya.SelectedValue & "',cashbankacctgoid=" & cashbankacctgoid.SelectedValue & ",cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),cashbankstatus='" & trndparstatus.Text & "',upduser='" & Session("UserID") & "',updtime=current_timestamp,cashbanknote='RETUR UM. PENJUALAN|NO=" & trndparno.Text & "', pic=" & custoid.Text & ", pic_refname='QL_MSTCUST',cashbankcurroid=" & currencyoid.SelectedValue & ",cashbankcurrate=" & ToDouble(currencyrate.Text)
                    If bUpdPayType Then
                        sSql &= ",cashbankno='" & Session("sCBNo") & "',cashbanktype='" & sCBType & "'"
                    End If
                    sSql &= " WHERE cmpcode='" & cmpcode & "' and cashbankoid=" & cashbankoid.Text
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    iCBMst = cashbankoid.Text
                End If
            End If

            If payreftype.SelectedValue <> "OTHER" Then
                'insert to cashbank gl
                sSql = "INSERT INTO QL_cashbankgl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglnote,cashbankglstatus,cashbankglres1,duedate,refno,createuser,createtime,upduser,updtime,branch_code) VALUES ('" & cmpcode & "'," & iCbdtloid & "," & cashbankoid.Text & "," & trndparacctgoid.SelectedValue & "," & ToDouble(PayAmt.Text) & ",'R_DPAR|No=" & trndparno.Text & " ~ " & Tchar(trndparnote.Text) & "','" & trndparstatus.Text & "','" & trndparno.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & Tchar(payrefno.Text.Trim) & "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iCbdtloid & " WHERE tablename='QL_CASHBANKGL' AND cmpcode='" & cmpcode & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If

            ' IF POST
            If trndparstatus.Text = "POST" Then
                ' QL_trnglmst
                sSql = "INSERT INTO QL_trnglmst (branch_code,cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type) VALUES ('" & CabangNya.SelectedValue & "','" & cmpcode & "'," & Session("GLMst") & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg(GetServerTime()) & "','RETUR UM. PENJUALAN|NO=" & trndparno.Text & "','" & trndparstatus.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'DPARR')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                ' QL_trngldtl - DEBET (UM PENJUALAN)
                sSql = "INSERT INTO QL_trngldtl (branch_code,cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt, glamtidr, glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CabangNya.SelectedValue & "','" & cmpcode & "'," & Session("GLDtl") & "," & Session("GLSeq") & "," & Session("GLMst") & "," & trndparacctgoid.SelectedValue & ",'D'," & ToDouble(PayAmt.Text) & "," & ToDouble(PayAmt.Text) & ", 0,'" & trndparno.Text & "','DP A/R Retur | Supp. " & Tchar(custname.Text) & " | No. DP. " & dpno.SelectedItem.Text & " | Note." & Tchar(trndparnote.Text) & "','QL_trndpar_back " & iCBMst & "','" & iTransOid & "','" & trndparstatus.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                Session("GLDtl") += 1 : Session("GLSeq") += 1

                ' QL_trngldtl - CREDIT (KAS/BANK)
                sSql = "INSERT INTO QL_trngldtl (branch_code,cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt, glamtidr, glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CabangNya.SelectedValue & "','" & cmpcode & "'," & Session("GLDtl") & "," & Session("GLSeq") & "," & Session("GLMst") & _
                    "," & cashbankacctgoid.SelectedValue & ",'C'," & ToDouble(PayAmt.Text) & ", " & ToDouble(PayAmt.Text) & ", 0,'" & trndparno.Text & "','DP A/R Retur | Supp. " & Tchar(custname.Text) & " | No. DP. " & dpno.SelectedItem.Text & " | Note." & Tchar(trndparnote.Text) & "','QL_trndpar_back " & iCBMst & "','" & iTransOid & "','" & trndparstatus.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                Session("GLDtl") += 1 : Session("GLSeq") += 1

                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("GLMst") & " WHERE cmpcode='" & cmpcode & "' AND tablename='QL_trnglmst'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("GLDtl") - 1 & " WHERE cmpcode='" & cmpcode & "' AND tablename='QL_trngldtl'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : trndparstatus.Text = "In Process"
            showMessage(ex.ToString & "<br />" & sSql, 1) : Session("wesposting") = "false" : Exit Sub
        End Try
        Response.Redirect("~\accounting\trndpar_payother.aspx?awal=true")
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvMst.PageIndex = 0
        Dim sPlus As String = ""
        sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterTextsupp.Text) & "%'"
        If fddlCabang.SelectedValue <> "ALL" Then
            sPlus &= " AND (select d.branch_code from ql_trndpar d where d.trndparoid=ar.trndparoid_ref) = '" & fddlCabang.SelectedValue & "'"
        End If
        BindData(sPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDL.SelectedIndex = 0 : FilterTextsupp.Text = ""
        Dim sPlus As String = ""
        If fddlCabang.SelectedValue <> "ALL" Then
            sPlus = " AND (select d.branch_code from ql_trndpar d where d.trndparoid=ar.trndparoid_ref) = '" & fddlCabang.SelectedValue & "'"
        End If
        BindData(sPlus)
    End Sub

    Private Sub FillAmountDP()
        trndparamt.Text = 0
        If dpno.Items.Count > 0 Then
            sSql = "select d.trndparamt-d.trndparacumamt from QL_trndpar d where d.trndparoid=" & dpno.SelectedValue & " And d.cmpcode='" & cmpcode & "'"
            trndparamt.Text = ToMaskEdit(ToDouble(GetStrData(sSql)), 3)
            PayAmt.Text = ToMaskEdit(ToDouble(GetStrData(sSql)), 3)
        End If
    End Sub

    Protected Sub trndparamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndparamt.TextChanged
        trndparamt.Text = ToMaskEdit(ToDouble(trndparamt.Text.Trim), 3)
    End Sub

    Protected Sub trndpardate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndpardate.TextChanged

        GenerateNo()

    End Sub

    Protected Sub imbsearchFilter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbsearchFilter.Click
        BindCustomer("")
        cProc.SetModalPopUpExtender(btnHideCust, pnlCust, mpeCust, True)
    End Sub

    Protected Sub CREDITCLEAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CREDITCLEAR.Click
        payrefno.Text = ""
        code.Text = ""
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA(trndparno.Text, cmpcode, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(NewMaskEdit(e.Row.Cells(4).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(NewMaskEdit(e.Row.Cells(3).Text), 4)

        End If
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvMst.PageIndex = e.NewPageIndex
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterTextsupp.Text) & "%' ")
    End Sub

    Protected Sub dpno_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpno.SelectedIndexChanged
        FillAmountDP()
    End Sub

    Protected Sub CabangNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitAllDDL()
        initAcctgoid()
        imbClearCust_Click(Nothing, Nothing)
        GenerateNo()
    End Sub
#End Region

    Protected Sub PayAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PayAmt.TextChanged
        If ToDouble(PayAmt.Text) = 0 Then
            PayAmt.Text = 0
        Else
            PayAmt.Text = ToMaskEdit(ToDouble(PayAmt.Text), 3)
        End If
    End Sub
End Class
