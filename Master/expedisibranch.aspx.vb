﻿Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Partial Class Master_expedisibranch
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    'Belum tersedia :)
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        If Session("branch_id") = "01" Or Session("UserID").ToString.ToUpper = "ADMIN" Then
            sSql = "SELECT row_number() over(order by branchcode) as nmr,id_expedisi,i.itemdesc,branchcode,panjang,lebar,tinggi, gendesc,price,a.expedisi_type FROM mstexpedisibranch a inner join QL_mstgen b on branchcode = gencode inner join ql_mstitem i ON i.itemoid=a.itemoid where gengroup='cabang'"
            Dim telo As String = String.Empty
            If cbBranch.Checked = True Then
                If FilterDDL.SelectedValue <> "" Then
                    sSql &= " AND branchcode=" & FilterDDL.SelectedValue
                Else
                    cbBranch.Checked = False
                End If
            End If
            telo = sSql
        Else
            sSql = "SELECT row_number() over(order by branchcode) as nmr,i.itemdesc,id_expedisi,branchcode,panjang,lebar,tinggi, gendesc,price,a.expedisi_type FROM mstexpedisibranch a inner join QL_mstgen b on branchcode = gencode inner join ql_mstitem i ON i.itemoid=a.itemoid where gengroup='cabang' and a.branchcode='" & Session("branch_id") & "'"
        End If

        If dd_jalur.SelectedValue <> "All" Then
            sSql &= " and expedisi_type='" & dd_jalur.SelectedValue & "'"
        End If

        'sSql &= " ORDER BY a.branchcode"

        Dim dt As DataTable = cKon.ambiltabel(sSql, "mstexpedisibranch")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("nmr") = C1 + 1
            'a = dt.Rows(C1)("nmr")
            dt.Rows(C1)("id_expedisi") = (dt.Rows(C1)("id_expedisi"))
            dt.Rows(C1)("branchcode") = (dt.Rows(C1)("branchcode"))
            dt.Rows(C1)("gendesc") = dt.Rows(C1)("gendesc")
            dt.Rows(C1)("price") = ToMaskEdit(dt.Rows(C1)("price"), 3)
        Next
        Session("TblMst") = dt
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
   
        Try
            sSql = "SELECT i.itemoid,i.itemdesc,branchcode,gendesc,panjang,lebar,tinggi,price,expedisi_type,a.beratbarang,a.beratvolume FROM mstexpedisibranch a inner join QL_mstgen on branchcode = gencode inner join ql_mstitem i ON i.itemoid=a.itemoid Where gengroup='cabang' and id_expedisi=" & Request.QueryString("oid")
            Dim testsql As String = String.Empty
            testsql = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            'btnDelete.Visible = False : btnSave.Visible = False
            btnDelete.Visible = True
            While xreader.Read
                itemoid.Text = xreader("itemoid").ToString
                itemdesc.Text = xreader("itemdesc").ToString
                branchid.SelectedValue = xreader("branchcode").ToString
                txt_panjang.Text = ToDouble(xreader("panjang"))
                txt_lebar.Text = ToDouble(xreader("lebar"))
                txt_tinggi.Text = ToDouble(xreader("tinggi"))
                dd_type.SelectedValue = ToDouble(xreader("expedisi_type"))
                priceid.Text = ToDouble(xreader("price"))
                BeratBarang.Text = ToDouble(xreader("beratbarang"))
                BeratVolume.Text = ToDouble(xreader("beratvolume"))
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub BindItem()
        sSql = "Select itemoid,itemcode,itemdesc,dimensi_p,dimensi_l,dimensi_t,beratbarang,beratvolume from ql_mstitem i Where (itemdesc LIKE '%" & Tchar(itemdesc.Text) & "%' OR itemcode LIKE '%" & Tchar(itemdesc.Text) & "%')"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "ItemSearch")
        Session("gvItemSearch") = dt
        gvItemSearch.DataSource = Session("gvItemSearch")
        gvItemSearch.DataBind()
    End Sub

    Private Sub InitDDLCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(branchid, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(branchid, sSql)
            Else
                FillDDL(branchid, sSql) 
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(branchid, sSql) 
        End If
    End Sub

    Private Sub InitAllDDL()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FilterDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FilterDDL, sSql)
            Else
                FillDDL(FilterDDL, sSql)
                FilterDDL.Items.Add(New ListItem("ALL", "ALL"))
                FilterDDL.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(FilterDDL, sSql)
            FilterDDL.Items.Add(New ListItem("ALL", "ALL"))
            FilterDDL.SelectedValue = "ALL"
        End If

    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Session("branch") = branch
            Session("branch_id") = branch_id 
            Response.Redirect("~\Master\expedisibranch.aspx")
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        Page.Title = CompnyName & " - Harga Expedisi Branch"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
            BindMstData("")
        Else
            I_U.Text = "Update Data"
            branchid.Enabled = False
            dd_type.Enabled = False
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        'btnSave.Attributes.Add("OnClick", "javascript:return confirm('Your data will not be able to be edited or deleted anymore. Are you sure to continue?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL() : InitDDLCabang()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
                txt_panjang.Enabled = False
                txt_panjang.CssClass = "inpTextDisabled"
                txt_lebar.Enabled = False
                txt_lebar.CssClass = "inpTextDisabled"
                txt_tinggi.Enabled = False
                txt_tinggi.CssClass = "inpTextDisabled"
            Else
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData("")
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        cbBranch.Checked = False
        FilterDDL.SelectedIndex = -1
        BindMstData("")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sCode As String = branchid.SelectedValue
        Dim cmd As New SqlCommand
        Dim aa As String = String.Empty
        Dim bb As String = String.Empty
        Dim HitVolume As Decimal
        Dim HitExpedisi As Decimal
       
        If priceid.Text = "" Or txt_panjang.Text = "" Or txt_tinggi.Text = "" Or txt_lebar.Text = "" Then
            showMessage("Masukkan Panjang, Lebar, Tinggi dan Harga ", 2)
            Exit Sub
        Else
            Try
                conn.Open()
                cmd.Connection = conn
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    Session("MstOid") = GenerateIDbranch("mstexpedisibranch", "10")
                    Try
                        HitVolume = txt_panjang.Text * txt_lebar.Text * txt_tinggi.Text
                        HitExpedisi = priceid.Text / HitVolume
                        sSql = "INSERT INTO mstexpedisibranch (panjang,lebar,tinggi,branchcode, price,expedisi_type,itemoid,beratbarang,beratvolume,crtuser,crttime,upduser,updtime) VALUES(" & txt_panjang.Text & ", " & txt_lebar.Text & ", " & txt_tinggi.Text & ",'" & branchid.SelectedValue & "', " & priceid.Text & ",'" & dd_type.SelectedValue & "'," & itemoid.Text & "," & ToDouble(BeratBarang.Text) & "," & ToDouble(BeratVolume.Text) & ",'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"

                        cmd.CommandText = sSql : cmd.ExecuteScalar()
                        sSql = "update QL_mstItem_branch set biayaExpedisi = " & HitExpedisi & " Where itemoid in (select a.itemoid from ql_mstitem a left join QL_mstItem_branch b on a.itemoid = b.itemoid Where b.branch_code = '" & branchid.SelectedValue & "' and a.dimensi_p = " & txt_panjang.Text & " and a.dimensi_l = " & txt_lebar.Text & " and A.dimensi_t = " & txt_tinggi.Text & " and b.expedisi_type='" & dd_type.SelectedValue & "')"

                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Response.Redirect("~\Master\expedisibranch.aspx?awal=true")
                    Catch ex As Exception
                        showMessage(ex.Message, 1)
                    End Try
                Else

                    Try
                        sSql = "UPDATE mstexpedisibranch SET price=" & priceid.Text & ",itemoid=" & itemoid.Text & ",beratbarang=" & ToDouble(BeratBarang.Text) & ",beratvolume=" & ToDouble(BeratVolume.Text) & ",upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE id_expedisi=" & Session("oid") & ""
                        cmd.CommandText = sSql : cmd.ExecuteScalar()

                        '' Update
                        sSql = "update QL_mstItem_branch set biayaExpedisi = " & HitExpedisi & " where itemoid in (select a.itemoid from ql_mstitem a left join QL_mstItem_branch b on a.itemoid = b.itemoid where b.branch_code = '" & branchid.SelectedValue & "' and a.dimensi_p = " & txt_panjang.Text & " and a.dimensi_l = " & txt_lebar.Text & " and A.dimensi_t = " & txt_tinggi.Text & " and b.expedisi_type='" & dd_type.SelectedValue & "')"

                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Response.Redirect("~\Master\expedisibranch.aspx?awal=true")
                    Catch ex As Exception
                        showMessage(ex.Message, 1)
                    End Try
                End If
                conn.Close()
            Catch ex As Exception
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\expedisibranch.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If branchid.SelectedValue = "" Then
            showMessage("Branch cant empty!", 1)
            Exit Sub
        End If
        If DeleteData("mstexpedisibranch", "branchcode", branchid.SelectedValue, CompnyCode) = True Then
            Response.Redirect("~\Master\expedisibranch.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 4)
        End If
    End Sub
#End Region

    Protected Sub btnSitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSitem.Click
        BindItem()
    End Sub

    Protected Sub gvItemSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItemSearch.SelectedIndexChanged
        itemoid.Text = gvItemSearch.SelectedDataKey("itemoid")
        itemdesc.Text = gvItemSearch.SelectedDataKey("itemdesc")
        txt_panjang.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("dimensi_p")), 4)
        txt_lebar.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("dimensi_l")), 4)
        txt_tinggi.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("dimensi_t")), 4)
        BeratVolume.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("beratvolume")), 4)
        BeratBarang.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("beratbarang")), 4)
        gvItemSearch.Visible = False
    End Sub
End Class
