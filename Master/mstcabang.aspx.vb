
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction

Partial Class MstCabang
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")

#End Region

#Region "Procedure"

    Private Sub InitCity()
        sSql = "Select genoid,gendesc from QL_mstgen Where cmpcode='" & CompnyCode & "' AND gengroup='CITY'"
        FillDDL(Genother5, sSql)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Try
            sSql = "SELECT gc.cmpcode, gc.genoid, gc.gencode, gc.gendesc, gc.gengroup, gc.genother1, gc.genother2, gc.genother3, gc.genother5, gc.genother6, gc.upduser, gc.updtime, ct.gendesc Kota FROM QL_mstgen gc Inner Join QL_mstgen ct ON ct.genoid=cast(gc.genother6 as int) AND ct.gengroup='CITY' Where gc.gengroup='CABANG' And gc.gengroup IN ('CABANG') AND " & FilterDDL.SelectedValue & " Like '%" & TcharNoTrim(FilterText.Text) & "%' ORDER BY gc.gencode"

            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDs As New DataSet
            mySqlDA.Fill(objDs, "data")

            GVmstgen.DataSource = objDs.Tables("data")
            GVmstgen.DataBind()
            GVmstgen.PageIndex = 0
        Catch ex As Exception
            showMessage(ex.ToString & "<br >" & sSql, 1) : Exit Sub
        End Try
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As Integer)
        Dim OidCity As Integer = 0
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2, genother3, upduser, updtime, Isnull(genother4,'') genother4, genother5, genother6 FROM QL_mstgen Where cmpcode='" & CompnyCode & "' and genoid = " & vGenoid & " AND gengroup='CABANG'"
        xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                gengroup.Enabled = False : gengroup.CssClass = "inpTextDisabled"
                genoid.Text = Integer.Parse(xreader("genoid"))
                gencode.Text = Trim(xreader("gencode").ToString)
                gendesc.Text = Trim(xreader("gendesc").ToString)
                gengroup.SelectedValue = xreader("gengroup").ToString
                GenOther1.Text = xreader("genother1").ToString
                genother2.Text = xreader("genother2").ToString
                genother3.Text = xreader("genother3").ToString
                OidCity = xreader("genother6")
                GenOther4.SelectedValue = xreader("genother4")
                Dim jumRow As Integer = gengroup.Items.Count
                Upduser.Text = Trim(xreader.GetValue(8).ToString)
                Updtime.Text = Trim(xreader.GetValue(9).ToString)
            End While
        End If
        xreader.Close() : conn.Close()
        InitCity()
        Genother5.SelectedValue = OidCity
    End Sub

    Public Sub InitGroup()
        sSql = "Select lastoid,tablename from QL_mstoid Where tablegroup='GEN' AND tablename='CABANG' order by tablename"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        gengroup.Items.Clear()

        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            gengroup.Items.Add(Trim(xreader.GetValue(1).ToString.Trim))
            gengroup.Items.Item(gengroup.Items.Count - 1).Value = Trim(xreader.GetValue(1).ToString.Trim)
        End While
        xreader.Close() : conn.Close()
    End Sub

    Public Sub GenerateGenID()
        genoid.Text = GenerateID("ql_mstgen", CompnyCode)
    End Sub

    Private Sub clearItem()
        gencode.Text = "" : gendesc.Text = ""
        gengroup.SelectedIndex = 0
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
    End Sub

    Private Sub GeneratedCode()
        sSql = "SELECT MAX(CAST(gencode AS int))+1 FROM QL_mstgen Where gengroup='CABANG'"
        gencode.Text = GetScalar(sSql)
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            btnSearch.Visible = True
            btnList.Visible = True
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            If Session("branch_id") = "" Then
                Response.Redirect("~/Other/login.aspx")
            End If
            Response.Redirect("mstcabang.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Data General"

        If Not IsPostBack Then 
            BindData(CompnyCode)
            InitGroup() : InitCity()
            btnSearch.Visible = True
            btnList.Visible = True

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "Update" 
                TabContainer1.ActiveTabIndex = 1
                FillTextBox(Session("oid"))
            Else
                i_u.Text = "New"
                GeneratedCode() : GenerateGenID() 
                Upduser.Text = Session("UserID")
                Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterText.Text = ""
        BindData(CompnyCode)
        GVmstgen.PageIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click  
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If gencode.Text.Trim = "" Then
            sMsg &= "- Maaf, Tolong Isi Code Dahulu !!<BR>"
        End If

        If gendesc.Text.Trim = "" Then
            sMsg &= "- Maaf, Tolong Isi Description Dahulu !!<BR>"
        End If

        If gendesc.Text.Trim.Length > 70 Then
            sMsg &= "- Maaf, Maximum 70 characters untuk Description !!<BR>"
        End If

        'cek code msgen yang kembar
        sSql = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gencode = '" & Tchar(gencode.Text) & "'"

        If Not Session("oid") Is Nothing Then
            sSql &= " AND genoid <> " & genoid.Text
        End If

        If ToDouble(GetScalar(sSql)) > 0 Then
            sMsg &= "- Maaf, Code sudah dipakai di General Group yang sama...!!<br>"
        Else
            sSql = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gendesc = '" & Tchar(gendesc.Text) & "'"

            If Not Session("oid") Is Nothing Then
                sSql &= " AND genoid <> " & genoid.Text
            End If

            If ToDouble(GetScalar(sSql)) > 0 Then
                sMsg &= "- Maaf, Description sudah dipakai di General Group yang sama..!!<br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            GeneratedCode() : GenerateGenID()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate) 

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT into QL_mstgen(cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2, genother3, upduser, updtime, genother4, genother5, genother6, createuser) " & _
                      "VALUES ('" & CompnyCode & "', " & genoid.Text & ", '" & Tchar(gencode.Text) & "', '" & Tchar(gendesc.Text) & "', '" & Tchar(gengroup.Text.Trim) & "', '" & Tchar(GenOther1.Text) & "', '" & Tchar(genother2.Text) & "', '" & Tchar(genother3.Text) & "', '" & Upduser.Text & "', current_timestamp, '" & GenOther4.SelectedValue & "', '" & Genother5.SelectedValue & "', '" & Genother5.SelectedValue & "', '" & Session("UserID") & "')"
            Else 'update data
                sSql = "UPDATE QL_mstgen SET gencode='" & Tchar(gencode.Text) & "', genDesc='" & Tchar(gendesc.Text) & "', gengroup='" & Tchar(gengroup.Text.Trim) & "', genother1='" & Tchar(GenOther1.Text) & "', genother2='" & Tchar(genother2.Text) & "', genother3='" & Tchar(genother3.Text) & "', upduser='" & Upduser.Text & "', updtime=current_timestamp, genother5='" & Genother5.SelectedValue & "', genother6='" & Genother5.SelectedValue & "', genother4='" & GenOther4.SelectedValue & "' WHERE cmpcode = '" & CompnyCode & "' and genoid=" & genoid.Text
            End If

            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "update QL_mstoid set lastoid=" & genoid.Text & " Where tablename='QL_mstgen' and cmpcode LIKE '%" & CompnyCode & "%'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Session("oid") = Nothing
        Response.Redirect("~\Master\mstcabang.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        clearItem()
        Response.Redirect("~\Master\mstcabang.aspx?awal=true")
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

#End Region
End Class