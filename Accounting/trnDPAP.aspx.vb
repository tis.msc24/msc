'Last Update By Juancokkk..!!
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_DownPaymentAR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cRate As New ClassRate
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        'If DDLBusUnit.SelectedValue = "" Then
        '    sError &= "- Please select BUSINESS UNIT field!<BR>"
        'End If
        If dpardate.Text = "" Then
            sError &= "- Please fill DP DATE field!<BR>"
            sErr = "Empty"
        Else
            If Not IsValidDate(dpardate.Text, "dd/MM/yyyy", sErr) Then
                sError &= "- DP DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If custoid.Text = "" Then
            sError &= "- Please select Supplier field!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select DP ACCOUNT field!<BR>"
        End If
        If dparpayacctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If
        If dparpaytype.SelectedValue <> "BKK" Then
            'If dparduedate.Text = "" Then
            '    sError &= "- Please fill DUE DATE field!<BR>"
            'Else
            '    If Not IsValidDate(dparduedate.Text, "dd/MM/yyyy", sErr) Then
            '        sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
            '    Else
            '        If CDate(toDate(dpardate.Text)) > CDate(toDate(dparduedate.Text)) Then
            '            sError &= "- DUE DATE must be more or equal than DP DATE!<BR>"
            '        End If
            '    End If
            'End If
        End If
        If dparpaytype.SelectedValue = "BGK" Then
            If dparpayrefno.Text = "" Then
                sError &= "- Please fill REF. NO. field!<BR>"
            End If
            'If dpartakegiro.Text = "" Then
            '    sError &= "- Please fill DATE TAKE GIRO field!<BR>"
            'Else
            '    If Not IsValidDate(dpartakegiro.Text, "dd/MM/yyyy", sErr) Then
            '        sError &= "- DATE TAKE GIRO is invalid. " & sErr & "<BR>"
            '    Else
            '        If CDate(toDate(dpardate.Text)) > CDate(toDate(dpartakegiro.Text)) Then
            '            sError &= "- DATE TAKE GIRO must be more or equal than DP DATE!<BR>"
            '        End If
            '    End If
            'End If
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If

        If dparamt.Text = "" Then
            sError &= "- Please fill DP AMOUNT field!<BR>"
        Else
            If ToDouble(dparamt.Text) <= 0 Then
                sError &= "- DP AMOUNT must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("dparamt", "QL_TRNDPAP", ToDouble(dparamt.Text), sErrReply) Then
                    sError &= "- DP AMOUNT must be less than MAX DP AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If addacctgoid1.SelectedValue <> 0 Then
            If ToDouble(addacctgamt1.Text) = 0 Then
                sError &= "- Additional Cost Amount 1 can't be equal to 0!<BR>"
            End If
        End If
        If addacctgoid2.SelectedValue <> 0 Then
            If ToDouble(addacctgamt2.Text) = 0 Then
                sError &= "- Additional Cost Amount 2 can't be equal to 0!<BR>"
            End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid2.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 2 must be in different account!<BR>"
                End If
            End If
        End If
        If addacctgoid3.SelectedValue <> 0 Then
            If ToDouble(addacctgamt3.Text) = 0 Then
                sError &= "- Additional Cost Amount 3 can't be equal to 0!<BR>"
            End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
            If addacctgoid2.SelectedValue <> 0 Then
                If addacctgoid2.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 2 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            dparstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Procedures"
    Private Sub ReAmountDP()
        dparnett.Text = ToMaskEdit(ToDouble(dparamt.Text) + ToDouble(addacctgamt1.Text) + ToDouble(addacctgamt2.Text) + ToDouble(addacctgamt3.Text), 4)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_TRNDPAP WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnDPAP.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process Down Payment A/R data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        sSql = "SELECT currencyoid, currencycode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
        InitDDLAdd()
    End Sub

    Private Sub InitDDLAdd()
        ' COA ADDITIONAL 1,2,3
        FillDDLAcctg(addacctgoid1, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid1.Items.Count < 1 Then FillDDL(addacctgoid1, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
        FillDDLAcctg(addacctgoid2, "VAR_ADD_COST", Session("branch_id"), "0", "NONE")

        If addacctgoid2.Items.Count < 1 Then FillDDL(addacctgoid2, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
        FillDDLAcctg(addacctgoid3, "VAR_ADD_COST", Session("branch_id"), "0", "NONE")

        If addacctgoid3.Items.Count < 1 Then FillDDL(addacctgoid3, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
        FillDDLAcctg(acctgoid, "VAR_DPAP", Session("branch_id"))
    End Sub

    Private Sub InitDDLDPAccount()
        ' Fill DDL DP Account
        FillDDLAcctg(acctgoid, "VAR_DPAP", Session("branch_id"))
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        Try
            sSql = "SELECT trndpapoid, trndpapno, CONVERT(VARCHAR(10), trndpapdate, 101) AS dpapdate, suppname, (acctgcode + ' - ' + acctgdesc) AS acctgdesc, (CASE payreftype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'BANK' ELSE 'GIRO' END) AS dpappaytype, trndpapstatus, trndpapnote, 'False' AS checkvalue, (Select cashbankno FROM QL_trncashbankmst cb Where cb.cashbankoid=dp.cashbankoid AND cb.branch_code=dp.branch_code) cbNo FROM QL_TRNDPAP dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.trndpapacctgoid WHERE dp.cmpcode='" & CompnyCode & "' AND trndpapoid >=0 and dp.trndpapstatus <> 'DELETE' AND dp.payreftype <> 'RETUR' " & sSqlPlus & " ORDER BY CONVERT(DATETIME, dp.trndpapdate) DESC, dp.trndpapoid DESC"
            Session("TblMst") = cKon.ambiltabel(sSql, "QL_TRNDPAP")
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind() : lblViewInfo.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
        End Try
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "trndparoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub GenerateCashBankNo()
        'If DDLBusUnit.SelectedValue <> "" Then
        Dim sErr As String = ""
        Dim sCab As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & Session("branch_id") & "' AND gengroup='CABANG'")
        If dpardate.Text <> "" Then
            If IsValidDate(dpardate.Text, "dd/MM/yyyy", sErr) Then
                If dparpayacctgoid.SelectedValue <> "" Then
                    Dim sNo As String = dparpaytype.SelectedValue & "/" & sCab & "/" & Format(CDate(toDate(dpardate.Text)), "yy/MM/dd") & "/"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & CompnyCode & "' AND cashbankno LIKE '%" & sNo & "%' AND cashbankacctgoid=" & dparpayacctgoid.SelectedValue
                    If GetStrData(sSql) = "" Then
                        cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                    Else
                        cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
                    End If
                End If
            End If
        End If
        'End If
    End Sub

    Private Sub BindCustomerData()
        sSql = "SELECT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%'  ORDER BY suppcode, suppname"
        FillGV(gvListCust, sSql, "QL_mstcust")
    End Sub

    Private Sub EnableAddInfo(ByVal bVal As Boolean)
        lblDueDate.Visible = bVal
        lblWarnDueDate.Visible = bVal
        lblSeptDueDate.Visible = bVal
        dparduedate.Visible = bVal
        imbDueDate.Visible = bVal
        lblInfoDueDate.Visible = bVal
    End Sub

    Private Sub EnableAddInfoGiro(ByVal bVal As Boolean)
        lblDTG.Visible = bVal
        lblWarnDTG.Visible = bVal
        lblSeptDTG.Visible = bVal
        dpartakegiro.Visible = False
        imbDTG.Visible = bVal
        lblInfoDTG.Visible = bVal
        lblWarnRefNo.Visible = bVal
    End Sub

    Private Sub GenerateNo()
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & Session("branch_id") & "' AND gengroup='CABANG'")
        Dim sNo As String = "DP/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trndpapno, 2) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_TRNDPAP WHERE cmpcode='" & CompnyCode & "' AND trndpapno LIKE '%" & sNo & "%'"
        dparno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT dp.cmpcode, trndpapoid,  trndpapno, trndpapdate, dp.suppoid, suppname, dp.trndpapacctgoid, dp.cashbankoid, cashbankno, payreftype, payrefno, payduedate , dp.currencyoid, trndpapamt, trndpapnote, trndpapstatus, dp.createuser, dp.createtime, dp.upduser, dp.updtime, dp.cashbankacctgoid FROM QL_TRNDPAP dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid AND cb.branch_code=dp.branch_code WHERE trndpapoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                CompnyCode = Trim(xreader("cmpcode").ToString)
                dparpaytype.SelectedValue = Trim(xreader("payreftype").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                dparoid.Text = Trim(xreader("trndpapoid").ToString)
                'periodacctg.Text = Trim(xreader("periodacctg").ToString)
                dparno.Text = Trim(xreader("trndpapno").ToString)
                dpardate.Text = Format(xreader("trndpapdate"), "dd/MM/yyyy")
                custoid.Text = Trim(xreader("suppoid").ToString)
                custname.Text = Trim(xreader("suppname").ToString)
                acctgoid.SelectedValue = Trim(xreader("trndpapacctgoid").ToString)
                dparpayacctgoid.SelectedValue = Trim(xreader("cashbankacctgoid").ToString)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                'dparpaytype_SelectedIndexChanged(Nothing, Nothing)
                dparpayrefno.Text = Trim(xreader("payrefno").ToString)
                dparduedate.Text = Format(xreader("payduedate"), "dd/MM/yyyy")
                If dparduedate.Text = "01/01/1900" Then
                    dparduedate.Text = ""
                End If
                curroid.SelectedValue = Trim(xreader("currencyoid").ToString)
                dparamt.Text = ToMaskEdit(ToDouble(Trim(xreader("trndpapamt").ToString)), 4)
                ReAmountDP()
                dparnote.Text = Trim(xreader("trndpapnote").ToString)
                dparstatus.Text = Trim(xreader("trndpapstatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            btnSave.Visible = False : btnDelete.Visible = False
            btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        btnShowCOA.Visible = False
        dparpaytype.CssClass = "inpTextDisabled" : dparpaytype.Enabled = False
        dparpayacctgoid.CssClass = "inpTextDisabled" : dparpayacctgoid.Enabled = False
        If dparstatus.Text = "Post" Then
            btnSave.Visible = False : btnDelete.Visible = False
            btnposting.Visible = False : btnShowCOA.Visible = True
            lblTrnNo.Text = "DP No." : dparoid.Visible = False
            dparno.Visible = True
        End If
    End Sub

    'Private Sub ShowReport()
    '    Try
    '        Dim sOid As String = ""
    '        If Session("TblMst") IsNot Nothing Then
    '            Dim dv As DataView = Session("TblMst").DefaultView
    '            dv.RowFilter = "checkvalue='True'"
    '            For C1 As Integer = 0 To dv.Count - 1
    '                sOid &= dv(C1)("trndparoid").ToString & ","
    '            Next
    '            dv.RowFilter = ""
    '        End If
    '        report.Load(Server.MapPath(folderReport & "rptDPAR.rpt"))
    '        Dim sWhere As String = ""
    '        If Session("CompnyCode") <> CompnyCode Then
    '            sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.cmpcode='" & Session("CompnyCode") & "'"
    '        End If
    '        If sOid = "" Then
    '            sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
    '            If cbPeriode.Checked Then
    '                If IsValidPeriod() Then
    '                    sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dpardate>='" & FilterPeriod1.Text & " 00:00:00' AND dpardate<='" & FilterPeriod2.Text & " 23:59:59'"
    '                Else
    '                    Exit Sub
    '                End If
    '            End If
    '            If cbStatus.Checked Then
    '                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dparstatus='" & FilterDDLStatus.SelectedValue & "'"
    '            End If
    '            If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
    '                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.createuser='" & Session("UserID") & "'"
    '            End If
    '        Else
    '            sOid = Left(sOid, sOid.Length - 1)
    '            sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.dparoid IN (" & sOid & ")"
    '        End If
    '        report.SetParameterValue("sWhere", sWhere)
    '        cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
    '        Response.Buffer = False
    '        Response.ClearContent()
    '        Response.ClearHeaders()
    '        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DownPaymentARPrintOut")
    '        report.Close()
    '        report.Dispose()
    '    Catch ex As Exception
    '        showMessage(ex.Message, 1)
    '        report.Close()
    '        report.Dispose()
    '    End Try
    '    Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
    'End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            'Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\Accounting\trnDPAP.aspx")
        End If
        Page.Title = CompnyName & " - Down Payment A/P"
        Session("oid") = Request.QueryString("oid")
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = " SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", 2)
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================

        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If checkPagePermission("Accounting/trnDPAP.aspx", Session("SpecialAccess")) = False Then
                sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
            End If
            If kacabCek("Accounting/trnDPAP.aspx", Session("UserID")) = True Then
                If Session("branch_id") <> "10" Then
                    sSqlPlus &= " AND dp.branch_code='" & Session("branch_id") & "'"
                End If
            End If
            BindTrnData(sSqlPlus) : CheckStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            dparduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                dparoid.Text = GenerateID("QL_TRNDPAP", CompnyCode)
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                dpardate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                dparstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False : btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                dparpaytype_SelectedIndexChanged(Nothing, Nothing)

            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, dp.updtime, GETDATE()) > " & nDays & " AND dparstatus='In Process' "
        If checkPagePermission("~\Accounting\trnDPAP.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND trndpapdate>='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND trndpapdate<='" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND trndpapstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        'If checkPagePermission("Accounting/trnDPAP.aspx", Session("SpecialAccess")) = False Then
        '    sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
        'End If
        'If kacabCek("Accounting/trnDPAP.aspx", Session("UserID")) = True Then
        '    If Session("branch_id") <> "10" Then
        '        sSqlPlus &= " AND dp.branch_code='" & Session("branch_id") & "'"
        '    End If
        'End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = "" : FilterDDL.SelectedIndex = -1
        FilterText.Text = "" : cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("Accounting/trnDPAP.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        If kacabCek("Accounting/trnDPAP.aspx", Session("UserID")) = True Then
            If Session("branch_id") <> "10" Then
                sSqlplus &= " AND dp.branch_code='" & Session("branch_id") & "'"
            End If
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        'UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDPAccount() : InitDDLAdd()
        dparpaytype_SelectedIndexChanged(Nothing, Nothing)
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub dpardate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpardate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        FilterDDLListCust.SelectedIndex = -1
        FilterTextListCust.Text = ""
        gvListCust.SelectedIndex = -1
        BindCustomerData()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custoid.Text = "" : custname.Text = ""
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        If custoid.Text <> gvListCust.SelectedDataKey.Item("suppoid").ToString Then
            btnClearCust_Click(Nothing, Nothing)
        End If
        custoid.Text = gvListCust.SelectedDataKey.Item("suppoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("suppname").ToString
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub dparpaytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparpaytype.SelectedIndexChanged
        dparpayrefno.Text = "" : dparduedate.Text = "" : dpartakegiro.Text = ""
        If dparpaytype.SelectedValue = "BKK" Then
            EnableAddInfo(False)
            EnableAddInfoGiro(False)
            FillDDLAcctg(dparpayacctgoid, "VAR_CASH", Session("branch_id"))
        ElseIf dparpaytype.SelectedValue = "BBK" Then
            EnableAddInfo(True)
            dparduedate.CssClass = "inpTextDisabled" : imbDueDate.Visible = False
            dparduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            EnableAddInfoGiro(False)
            FillDDLAcctg(dparpayacctgoid, "VAR_BANK", Session("branch_id"))
        ElseIf dparpaytype.SelectedValue = "BGK" Then
            EnableAddInfo(True)
            EnableAddInfoGiro(True)
            FillDDLAcctg(dparpayacctgoid, "VAR_GIRO_HUTANG", Session("branch_id"))
        Else
            EnableAddInfo(True)
            EnableAddInfoGiro(False)
            FillDDLAcctg(dparpayacctgoid, "VAR_BANK", Session("branch_id"))
        End If
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub dparpayacctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparpayacctgoid.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub dparamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparamt.TextChanged
        dparamt.Text = ToMaskEdit(ToDouble(dparamt.Text), 4)
        ReAmountDP()
    End Sub

    Protected Sub addacctgoid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid1.SelectedIndexChanged
        addacctgamt1.Text = ""
        addacctgamt1.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt1.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt1)
    End Sub

    Protected Sub addacctgoid2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid2.SelectedIndexChanged
        addacctgamt2.Text = ""
        addacctgamt2.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt2.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt2)
    End Sub

    Protected Sub addacctgoid3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid3.SelectedIndexChanged
        addacctgamt3.Text = ""
        addacctgamt3.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt3.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt3)
    End Sub

    Protected Sub addacctgamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    addacctgamt1.TextChanged, addacctgamt2.TextChanged, addacctgamt3.TextChanged
        sender.Text = ToMaskEdit(ToDouble(sender.Text), 4)
        ReAmountDP()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            '==============
            'Cek Date Exist
            '==============
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_TRNDPAP WHERE trndpapoid=" & dparoid.Text
                If CheckDataExists(sSql) Then
                    dparoid.Text = GenerateID("QL_TRNDPAP", CompnyCode)
                    isRegenOid = True
                End If
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cmpcode='" & CompnyCode & "' AND cashbankno='" & cashbankno.Text & "'"
                If CheckDataExists(sSql) Then
                    GenerateCashBankNo()
                End If
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNDPAP", "trndpapoid", dparoid.Text, "trndpapstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    dparstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            Dim sd As Date = CDate(toDate(dpardate.Text))
            If CDate(toDate(dpardate.Text)) < Format(GetServerTime(), "MM/dd/yyyy") Then
                showMessage("- Tanggal DP (" & dpardate.Text & ") Tidak boleh <= tanggal (" & Format(GetServerTime(), "dd/MM/yyyy") & ") !! <BR> ", 2)
                Exit Sub
            End If
            If CDate(toDate(dpardate.Text)) <= CDate(toDate(CutofDate.Text)) Then
                showMessage("- Tanggal DP " & CDate(toDate(dpardate.Text)) & " Tidak boleh <= CutoffDate (" & CutofDate.Text & ") !! <BR> ", 2)
                Exit Sub
            End If
            '==============================
            'Cek Peroide Bulanan Dari Crdgl
            '==============================
            'CEK PERIODE AKTIF BULANAN
            'sSql = "select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
            'If GetStrData(sSql) <> "" Then
            sSql = "select distinct left(isnull(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl Where glflag='OPEN'"
            If GetPeriodAcctg(CDate(toDate(dpardate.Text))) < GetStrData(sSql) Then
                showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", 2)
                Exit Sub
            End If
            'End If
            '==================
            'Generate ID MSTOID
            '==================
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)

            Dim iSeq As Integer = 1 : Dim mDate As String = ""
            Dim cRate As New ClassRate()
            Dim iGiroAcctgOid As Integer = 0
            periodacctg.Text = GetDateToPeriodAcctg(CDate(toDate(dpardate.Text)))

            If dparstatus.Text = "Post" Then
                GenerateNo()
                cRate.SetRateValue(CInt(curroid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                Dim sVarErr As String = ""
                If dparpaytype.SelectedValue = "BGK" Then
                    If Not IsInterfaceExists("VAR_GIRO_HUTANG", Session("branch_id")) Then
                        sVarErr &= "VAR_GIRO_HUTANG"
                    Else
                        iGiroAcctgOid = GetAcctgOID(GetVarInterface("VAR_GIRO_HUTANG", Session("branch_id")), CompnyCode)
                    End If
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    Exit Sub
                End If
            End If

            If dparpaytype.SelectedValue = "BKK" Then
                mDate = "1/1/1900"
            Else
                mDate = CDate(toDate(dpardate.Text))
            End If
            '===============================
            'Query ambil Rate(Kurs) Currency
            '===============================
            Dim RateIDR As Decimal = GetStrData("select top 1 rate2idrvalue from ql_mstrate2 where currencyoid='" & curroid.Text & "' order by rate2date desc")
            Dim RateUSD As Decimal = GetStrData("select top 1 rate2usdvalue from ql_mstrate2 where currencyoid='" & curroid.Text & "' order by rate2date desc")
            Dim matauang As Double : Dim Currate As Double

            If curroid.SelectedValue = 1 Then
                matauang = RateIDR : Currate = RateUSD
            Else
                matauang = RateUSD : Currate = RateIDR
            End If

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    '========================
                    'Insert QL_TRNCASHBANKMST
                    '========================
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid,branch_code, cashbankno, cashbankdate, cashbanktype, cashbankgroup, cashbankacctgoid,cashbankcurroid,pic,pic_refname, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime,cashbankcurrate,cashbankamount,cashbankamountidr,cashbankamountusd) VALUES ('" & CompnyCode & "','" & cashbankoid.Text & "','" & Session("branch_id") & "','" & cashbankno.Text & "','" & CDate(toDate(dpardate.Text)) & "','" & dparpaytype.SelectedValue & "','DPAP'," & dparpayacctgoid.SelectedValue & "," & curroid.SelectedValue & ", " & custoid.Text & ",'QL_MSTSUPP','DPAP Payment " & cashbankno.Text & " ', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP," & Currate & "," & ToDouble(dparamt.Text) & "," & ToDouble(dparamt.Text) * RateIDR & "," & ToDouble(dparamt.Text) * RateUSD & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '=================
                    'Update QL_TRNDPAP
                    '=================
                    sSql = "INSERT INTO QL_trndpap (cmpcode,trnDPAPoid,branch_code,trnDPAPno,trnDPAPdate,suppoid,cashbankoid,trnDPAPacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate,trnDPAPamt,trnDPAPamtidr,trnDPAPamtusd, taxtype, taxoid, taxpct, taxamt,trnDPAPnote, trnDPAPflag, trnDPAPacumamt,trnDPAPacumamtidr,trnDPAPacumamtusd, trnDPAPstatus, createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "', " & dparoid.Text & ",'" & Session("branch_id") & "','" & dparno.Text & "','" & CDate(toDate(dpardate.Text)) & "', " & custoid.Text & ",'" & cashbankoid.Text & "', " & acctgoid.SelectedValue & ",'" & dparpaytype.SelectedValue & "', " & dparpayacctgoid.SelectedValue & ",'" & mDate & "','" & Tchar(dparpayrefno.Text) & "', " & curroid.SelectedValue & "," & Currate & "," & ToDouble(dparamt.Text) & "," & ToDouble(dparamt.Text) * RateIDR & "," & ToDouble(dparamt.Text) * RateUSD & ",'NONTAX','',0,0,'" & Tchar(dparnote.Text) & "','',0,0,0,'" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & dparoid.Text & " WHERE tablename='QL_TRNDPAP' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    '========================
                    'Update QL_TRNCASHBANKMST
                    '========================
                    sSql = "UPDATE QL_trncashbankmst set cashbankno='" & cashbankno.Text & "',cashbankdate='" & CDate(toDate(dpardate.Text)) & "',cashbanktype='" & dparpaytype.SelectedValue & "',cashbankacctgoid=" & dparpayacctgoid.SelectedValue & ",cashbankcurroid=" & curroid.SelectedValue & ",pic=" & custoid.Text & ",cashbanknote='DPAP Payment " & cashbankno.Text & " ',cashbankstatus='" & dparstatus.Text & "',upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP,cashbankcurrate=" & Currate & ",cashbankamount=" & ToDouble(dparamt.Text) & ",cashbankamountidr=" & ToDouble(dparamt.Text) * RateIDR & ",cashbankamountusd=" & ToDouble(dparamt.Text) * RateUSD & " where cmpcode='" & CompnyCode & "' And cashbankoid='" & cashbankoid.Text & "' and branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '=================
                    'Update QL_TRNDPAP
                    '=================
                    sSql = "UPDATE QL_trndpap SET trnDPAPno='" & dparno.Text & "', trndpapdate='" & CDate(toDate(dpardate.Text)) & "', suppoid=" & custoid.Text & ", payreftype='" & dparpaytype.SelectedValue & "', trndpapacctgoid=" & acctgoid.SelectedValue & ", payrefno='" & Tchar(dparpayrefno.Text) & "', payduedate='" & mDate & "', currencyoid=" & curroid.SelectedValue & ", trndpapamt=" & ToDouble(dparamt.Text) & ", trndpapnote='" & Tchar(dparnote.Text) & "', trndpapstatus='" & dparstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, trndpapamtidr=" & ToDouble(dparamt.Text) * RateIDR & ", trndpapamtusd=" & ToDouble(dparamt.Text) * RateUSD & ",currencyrate=" & Currate & " WHERE cmpcode='" & CompnyCode & "' AND trnDPAPoid=" & dparoid.Text & " and branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If dparstatus.Text = "Post" Then
                    Dim sDate As String = CDate(toDate(dpardate.Text))
                    If dparpaytype.SelectedValue = "BBK" Then
                        sDate = CDate(toDate(dpardate.Text))
                    End If
                    '==================
                    'Insert Auto Jurnal
                    '==================
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid,branch_code, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type) VALUES ('" & CompnyCode & "', " & iGlMstOid & ",'" & Session("branch_id") & "', '" & sDate & "', '" & periodacctg.Text & "', 'DP A/P|No=" & dparno.Text & "', '" & dparstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'DPAP')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    ' Insert Into GL Dtl
                    ' Cash/Bank/Giro
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid,branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1,glpostdate) VALUES ('" & CompnyCode & "', " & iGlDtlOid & ",'" & Session("branch_id") & "', " & iSeq & ", " & iGlMstOid & ", " & acctgoid.SelectedValue & ", 'D', " & ToDouble(dparamt.Text) & ", '" & dparno.Text & "', 'DP A/P | Supp. " & custname.Text & " | No. Cashbank " & cashbankno.Text & " | Note. " & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dparamt.Text) * RateIDR & ", " & ToDouble(dparamt.Text) * RateUSD & ", 'QL_TRNDPAP " & dparoid.Text & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1

                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid,branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1,glpostdate) VALUES ('" & CompnyCode & "', " & iGlDtlOid & ",'" & Session("branch_id") & "', " & iSeq & ", " & iGlMstOid & ", " & IIf(dparpaytype.SelectedValue = "BGK", iGiroAcctgOid, dparpayacctgoid.SelectedValue) & ", 'C', " & ToDouble(dparamt.Text) & ", '" & dparno.Text & "','DP A/P | Supp. " & custname.Text & " | No. Cashbank " & cashbankno.Text & " | Note. " & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dparamt.Text) * RateIDR & ", " & ToDouble(dparamt.Text) * RateUSD & ", 'QL_TRNDPAP " & dparoid.Text & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                dparstatus.Text = "In Process"
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & dparoid.Text & ".<BR>"
            End If
            If dparstatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with DP No. = " & dparno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If dparoid.Text = "" Then
            showMessage("Please select Down Payment A/P data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNDPAP", "trndpapoid", dparoid.Text, "trndpapstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                dparstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & CompnyCode & "' AND cashbankoid IN (SELECT cashbankoid FROM QL_TRNDPAP WHERE cmpcode='" & CompnyCode & "' AND trndpapoid=" & dparoid.Text & ")"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_TRNDPAP WHERE cmpcode='" & CompnyCode & "' AND trndpapoid=" & dparoid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        dparstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(dparno.Text, Session("branch_id"), gvCOAPosting, DDLRateType.SelectedValue, "QL_TRNDPAP " & dparoid.Text & "' OR ISNULL(d.glother1, '')='QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(dparno.Text, CompnyCode, gvCOAPosting, DDLRateType.SelectedValue, "QL_TRNDPAP " & dparoid.Text & "' OR ISNULL(d.glother1, '')='QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        'UpdateCheckedValue()
        'ShowReport()
    End Sub

    Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
        Try
            Dim swhere As String = ""
            swhere = "where dp.branch_code = '" & Session("branch_id") & "' and dp.trndpapoid = '" & gvTRN.SelectedDataKey.Item(0) & "'"
            ' report.Load(Server.MapPath("~/report/rptNotaSO.rpt"))

            report.Load(Server.MapPath("~/report/printoutdpap.rpt"))

            report.SetParameterValue("sWhere", swhere)
            report.SetParameterValue("userid", Session("userid"))
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.PrintOptions.PaperSize = PaperSize.PaperA4

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DPAP" & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imbprintdpar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub
#End Region
End Class