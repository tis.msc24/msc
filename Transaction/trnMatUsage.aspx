<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMatUsage.aspx.vb" Inherits="Transaction_trnMatUsage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Material Usage" CssClass="Title" ForeColor="Navy" Font-Names="Verdana" Font-Size="21px"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Material Usage :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=3><asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w120" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left>Cabang</TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left>: </TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:DropDownList id="fDDLCabang" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w141"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left>Usage&nbsp;No </TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" MaxLength="30" __designer:wfdid="w121"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" id="TD11" class="Label" align=left runat="server" Visible="false">Group </TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" id="TD8" class="Label" align=left runat="server" Visible="false">:</TD><TD id="TD7" align=left colSpan=4 runat="server" Visible="false"><asp:DropDownList id="group" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w122"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" id="TD12" class="Label" align=left runat="server" Visible="false">Sub Group </TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" id="TD10" class="Label" align=left runat="server" Visible="false">:</TD><TD id="TD9" align=left colSpan=4 runat="server" Visible="false"><asp:DropDownList id="subgroup" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w123"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" id="TD1" class="Label" align=left runat="server" Visible="false">Item/Barang</TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" id="TD2" class="Label" align=left runat="server" Visible="false">:</TD><TD id="TD6" align=left colSpan=4 runat="server" Visible="false"><asp:TextBox id="Filteritem" runat="server" Width="219px" CssClass="inpText" MaxLength="30" __designer:wfdid="w124"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="17px" __designer:wfdid="w125"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w126"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" __designer:wfdid="w127" Visible="False"></asp:Label></TD></TR><TR><TD id="TD4" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD3" align=left colSpan=4 runat="server" Visible="false"><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVItemSearch" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w128" Visible="False" DataKeyNames="itemoid" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Item Kode">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle Wrap="True"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:CheckBox id="CbTanggal" runat="server" Width="96px" Text="Tanggal" __designer:wfdid="w142"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w129"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w130"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w131"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w132"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w133"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" align=left></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" align=left></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" align=left colSpan=4><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w134" TargetControlID="tgl2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w135" TargetControlID="tgl1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w136" TargetControlID="tgl2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w137" TargetControlID="tgl1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w138">
                                                            </asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w139">
                                                            </asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" align=left colSpan=6><asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w140" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trfmtrmstoid" DataNavigateUrlFormatString="trnMatUsage.aspx?oid={0}" DataTextField="transferno" HeaderText="Usage No">
<ControlStyle ForeColor="Red"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" ForeColor="Red"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trfmtrdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Usage Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Username">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
&nbsp;<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" ToolTip='<%# eval("trfmtrmstoid") %>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" Text='<%# Eval("trfmtrmstoid") %>'></asp:Label>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Usage&nbsp;No.</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" MaxLength="20" __designer:wfdid="w200" Enabled="False"></asp:TextBox><asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" __designer:wfdid="w201" Visible="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Tanggal</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" __designer:wfdid="w202" Enabled="False" ValidationGroup="MKE"></asp:TextBox>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w203"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>&nbsp;</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>&nbsp;</TD><TD style="HEIGHT: 10px" class="Label" align=left>&nbsp;</TD></TR><TR><TD class="Label" align=left>Cabang</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlFromcabang" runat="server" Width="255px" CssClass="inpText" __designer:wfdid="w204" AutoPostBack="True">
            </asp:DropDownList> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>&nbsp;</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>&nbsp;</TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="ddlTocabang" runat="server" Width="255px" CssClass="inpText" __designer:wfdid="w205" Visible="False" AutoPostBack="True">
            </asp:DropDownList> </TD></TR><TR><TD class="Label" align=left>Location</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="fromlocation" runat="server" Width="255px" CssClass="inpText" __designer:wfdid="w206" AutoPostBack="True">
            </asp:DropDownList> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>&nbsp;</TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="tolocation" runat="server" Width="255px" CssClass="inpText" __designer:wfdid="w207" Visible="False" AutoPostBack="True">
            </asp:DropDownList> </TD></TR><TR><TD class="Label" align=left>Keterangan</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="note" runat="server" Width="251px" CssClass="inpText" MaxLength="200" __designer:wfdid="w208"></asp:TextBox> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:TextBox id="trnstatus" runat="server" Width="152px" CssClass="inpText" MaxLength="20" __designer:wfdid="w209" Visible="False">In Process</asp:TextBox> </TD></TR><TR><TD class="Label" align=left>No. Ref</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="noref" runat="server" Width="152px" CssClass="inpText" MaxLength="20" __designer:wfdid="w210"></asp:TextBox> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>&nbsp;</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>&nbsp;</TD><TD class="Label" align=left>&nbsp;</TD></TR><TR><TD class="Label" align=left>COA</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlCOA" runat="server" Width="256px" CssClass="inpText" __designer:wfdid="w211">
            </asp:DropDownList> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Usage Detail :" __designer:wfdid="w212" Font-Underline="True"></asp:Label> <asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" __designer:wfdid="w213" Visible="False"></asp:Label> </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="GrupCb" runat="server" Width="96px" Text="Grup Barang" __designer:wfdid="w214" Visible="False" AutoPostBack="True" OnCheckedChanged="GrupCb_CheckedChanged"></asp:CheckBox> </TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:TextBox id="txtFilter" runat="server" Width="70px" CssClass="inpTextDisabled" MaxLength="20" __designer:wfdid="w215" Enabled="False" Visible="False" AutoPostBack="True" OnTextChanged="txtFilter_TextChanged"></asp:TextBox> <asp:DropDownList id="DDLGrupItem" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w216" Enabled="False" Visible="False"></asp:DropDownList> </TD></TR><TR><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Katalog" __designer:wfdid="w217"></asp:Label> <asp:Label id="Label26" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w218"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="354px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w219"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w220">
            </asp:ImageButton> <asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w221">
            </asp:ImageButton>&nbsp;<asp:Label id="labelitemoid" runat="server" __designer:wfdid="w222" Visible="False"></asp:Label> </TD></TR><TR><TD id="TD13" class="Label" align=left runat="server" Visible="false">Merk</TD><TD style="VERTICAL-ALIGN: top" id="TD14" class="Label" align=left runat="server" Visible="false">:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="merk" runat="server" Width="251px" CssClass="inpTextDisabled" MaxLength="200" __designer:wfdid="w223" Enabled="False" Visible="False"></asp:TextBox> </TD></TR><TR><TD class="Label" align=left><asp:Label id="labelkonversi1_2" runat="server" __designer:wfdid="w224" Visible="False"></asp:Label> <asp:Label id="labelkonversi2_3" runat="server" __designer:wfdid="w225" Visible="False"></asp:Label> <asp:Label id="labelunit1" runat="server" __designer:wfdid="w226" Visible="False"></asp:Label> <asp:Label id="labelunit2" runat="server" __designer:wfdid="w227" Visible="False"></asp:Label> <asp:Label id="labelunit3" runat="server" __designer:wfdid="w228" Visible="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w229" Visible="False" DataKeyNames="itemoid,satuan1,satuan2,satuan3,unit1,unit2,konversi1_2,konversi2_3" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sisa" DataFormatString="{0:#,##0.00}" HeaderText="Stock">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                    <asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" __designer:wfdid="w230" TargetControlID="qty" ValidChars="0123456789.,">
            </ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left>Quantity <asp:Label id="Label28" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w231"></asp:Label> </TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="qty" runat="server" Width="150px" CssClass="inpText" MaxLength="10" __designer:wfdid="w232" ValidationGroup="MKE" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:Label id="Label30" runat="server" CssClass="label" Text="<=" __designer:wfdid="w233"></asp:Label>&nbsp;<asp:Label id="labelmaxqty" runat="server" CssClass="label" ForeColor="Red" __designer:wfdid="w234">0.00</asp:Label>&nbsp;<asp:DropDownList id="unit" runat="server" Width="95px" CssClass="inpTextDisabled" __designer:wfdid="w235" Enabled="False" AutoPostBack="True">
            </asp:DropDownList>&nbsp; </TD><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:Label id="labelsatuan1" runat="server" __designer:wfdid="w236" Visible="False"></asp:Label> <asp:Label id="labelsatuan2" runat="server" __designer:wfdid="w237" Visible="False"></asp:Label> <asp:Label id="labelsatuan3" runat="server" __designer:wfdid="w238" Visible="False"></asp:Label> </TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" MaxLength="200" __designer:wfdid="w239"></asp:TextBox> </TD><TD class="Label" align=left><asp:Label id="labelseq" runat="server" __designer:wfdid="w240" Visible="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w241">
            </asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w242">
            </asp:ImageButton> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; BACKGROUND-COLOR: beige" class="Label" align=left colSpan=6><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w243" DataKeyNames="itemoid,satuan,satuan1,satuan2,satuan3,unit1,unit2,unit3,konversi1_2,konversi2_3" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
                            <asp:LinkButton id="lbdelete" runat="server" OnClick="lbdelete_Click"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton>
                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                    <asp:Label id="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!"></asp:Label>
                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w244"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w245"></asp:Label> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w246">
            </asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w247">
            </asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w248" Visible="False">
            </asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w249" Visible="False">
            </asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" __designer:wfdid="w250" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w251" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt">LOADING....</SPAN><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w252"></asp:Image></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Material Usage :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

