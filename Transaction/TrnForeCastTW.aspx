<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="TrnForeCastTW.aspx.vb" Inherits="trnForeCastTW" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Permintaan Barang" Font-Size="X-Large" ForeColor="Navy"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Permintaan Barang :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w26"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 8%" class="Label" align=left>Cabang</TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="fCbangDDL" runat="server" CssClass="inpText" __designer:wfdid="w27"></asp:DropDownList>&nbsp;: Cb. Kirim <asp:DropDownList id="DDLToCabang" runat="server" CssClass="inpText" __designer:wfdid="w47"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 8%" class="Label" align=left>Filter</TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w28"><asp:ListItem Value="pbm.trnrequestitemoid">Draft No.</asp:ListItem>
<asp:ListItem Value="trnrequestitemno">No. Transaksi</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w29"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode" __designer:wfdid="w30"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" ToolTip="dd/MM/yyyy" __designer:wfdid="w31"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w32"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w33"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" ToolTip="dd/MM/yyyy" __designer:wfdid="w34"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton> <asp:Label id="lblinfodate" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w36"></asp:Label>&nbsp; </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w37"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w38"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Revised</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w41"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="GvTrn" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnrequestitemoid" GridLines="None" PageSize="8" __designer:wfdid="w42" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trnrequestitemoid,cmpcode" DataNavigateUrlFormatString="~\Transaction\TrnForeCastTW.aspx?oid={0}&amp;comp={1}" DataTextField="trnrequestitemoid" HeaderText="Draft No." SortExpression="trnrequestitemoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnrequestitemno" HeaderText="No. Transaksi" SortExpression="trnrequestitemno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemdate" HeaderText="Tanggal" SortExpression="trnrequestitemdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang" SortExpression="gendesc">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="CbKirim" HeaderText="Cb. Kirim">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
        <ItemStyle HorizontalAlign="Left" Wrap="False" />
    </asp:BoundField>
<asp:BoundField DataField="trnrequestitemstatus" HeaderText="Status" SortExpression="trnrequestitemstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" Font-Italic="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cmpcode" HeaderText="cmpcode" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" __designer:wfdid="w43" PopupButtonID="imbDate1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" __designer:wfdid="w44" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" __designer:wfdid="w45" PopupButtonID="imbDate2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" __designer:wfdid="w46" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvTrn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label16" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Data Header" Font-Underline="True"></asp:Label><asp:Label id="i_u" runat="server" Width="64px" CssClass="Important" Font-Bold="True" Text="New Data" Visible="False"></asp:Label> <asp:Label id="trnrequestitemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Width="82px" Text="Cabang"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DdlCabang" runat="server" Width="205px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Cb. Pengirim"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLCbKirim" runat="server" Width="205px" CssClass="inpText" AutoPostBack="True">
</asp:DropDownList></TD></TR><TR><TD id="BusinessUnit1" class="Label" align=left runat="server" visible="true"><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label></TD><TD id="BusinessUnit2" class="Label" align=center runat="server" visible="true">:</TD><TD id="BusinessUnit3" class="Label" align=left runat="server" visible="true"><asp:TextBox id="trnrequestitemno" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="LblTanggal" runat="server" Text="Tanggal"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="trnrequestitemdate" runat="server" Width="80px" CssClass="inpTextDisabled" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Keterangan"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="trnrequestitemnote" runat="server" Width="200px" CssClass="inpText" Rows="3" TextMode="MultiLine" MaxLength="100"></asp:TextBox></TD><TD class="Label" vAlign=top align=left><asp:Label id="Label9" runat="server" Text="Status"></asp:Label></TD><TD class="Label" vAlign=top align=center>:</TD><TD class="Label" vAlign=top align=left><asp:TextBox id="trnrequestitemstatus" runat="server" Width="80px" CssClass="inpTextDisabled" Enabled="False">IN PROCESS</asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label17" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Data Detail" Font-Underline="True"></asp:Label><asp:Label id="i_u2" runat="server" Font-Bold="True" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Katalog"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="itemdesc" runat="server" Width="180px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchMat" runat="server" Width="19px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="21px"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Quantity"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="trnrequestitemdtlqty" runat="server" Width="80px" CssClass="inpText" MaxLength="12">0.00</asp:TextBox>&nbsp; </TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="No. SO"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="orderno" runat="server" Width="180px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSO" runat="server" Width="19px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="21px" Visible="False"></asp:ImageButton></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GvSO" runat="server" Width="70%" ForeColor="#333333" Visible="False" PageSize="8" GridLines="None" DataKeyNames="ordermstoid,orderno" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red">
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <EmptyDataRowStyle ForeColor="Red" />
                <Columns>
                    <asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                        <ItemStyle Font-Bold="True" Font-Size="X-Small" HorizontalAlign="Left" Wrap="False" />
                    </asp:CommandField>
                    <asp:BoundField DataField="orderno" HeaderText="No. SO">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Width="75px" />
                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="175px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="trncustname" HeaderText="Customer">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Width="275px" />
                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="275px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="trnorderdate" HeaderText="Tanggal">
                        <HeaderStyle CssClass="gvhdr" Width="75px" />
                        <ItemStyle Width="75px" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"
                    HorizontalAlign="Right" />
                <EmptyDataTemplate>
                    <asp:Label ID="Label19" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Item data found!!"></asp:Label>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView> </TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" vAlign=top align=left>Note</TD><TD class="Label" vAlign=top align=center>:</TD><TD class="Label" vAlign=top align=left><asp:TextBox id="trnrequestitemdtlnote" runat="server" Width="200px" CssClass="inpText" Rows="3" TextMode="MultiLine" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6><asp:GridView id="gvPRDtl" runat="server" Width="100%" ForeColor="#333333" PageSize="5" GridLines="None" DataKeyNames="trnrequestitemdtlseq" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnrequestitemdtlseq" HeaderText="No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln3">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln2">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln1">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ReqQty" HeaderText="Req. Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acceptqty" HeaderText="Accept Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="No. SO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemdtlnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="FTEQty" runat="server" TargetControlID="trnrequestitemdtlqty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><asp:Label id="lblbln3" runat="server" Visible="False"></asp:Label> <asp:Label id="lblbln2" runat="server" Visible="False"></asp:Label><asp:Label id="lblbln1" runat="server" Visible="False"></asp:Label><asp:Label id="trnrequestitemdtlseq" runat="server" Visible="False">1</asp:Label><asp:Label id="trnrequestitemdtloid" runat="server" Visible="False"></asp:Label><asp:Label id="itemoid" runat="server" Visible="False">0</asp:Label><asp:Label id="itemcode" runat="server" Visible="False"></asp:Label><asp:Label id="PeriodAcctg" runat="server" Visible="False"></asp:Label><asp:Label id="totalsalesmonth1" runat="server" Visible="False"></asp:Label><asp:Label id="totalsalesmonth2" runat="server" Visible="False"></asp:Label><asp:Label id="totalsalesmonth3" runat="server" Visible="False"></asp:Label><asp:Label id="saldoakhir" runat="server" Visible="False"></asp:Label><asp:Label id="ordermstoid" runat="server" Visible="False">0</asp:Label></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" vAlign=top align=center colSpan=3>Filter : &nbsp;&nbsp;<asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="jenis">Jenis Katalog</asp:ListItem>
<asp:ListItem Value="type">Type Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText"></asp:TextBox>&nbsp;Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 894px; HEIGHT: 13%; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln3" HeaderText="bln3">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln2" HeaderText="bln2">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln1" HeaderText="bln1">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SaldoAkhir" HeaderText="Stok">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Req. Qty"><ItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("ReqQty") %>' MaxLength="12"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" ValidChars="1234567890.," TargetControlID="tbQty"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbNote" runat="server" CssClass="inpText" Text='<%# eval("trnrequestitemdtlnote") %>' Width="200px" MaxLength="100"></asp:TextBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="type" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jenis" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Left" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server" CssClass="green" Font-Bold="True" Font-Underline="False"> Add To List </asp:LinkButton>&nbsp;-&nbsp;<asp:LinkButton id="lkbCloseListMat" runat="server" CssClass="red" Font-Bold="True" Font-Underline="False"> Cancel & Close </asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblTitleListMat" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:HiddenField id="hfStatus" runat="server"></asp:HiddenField></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Permintaan Barang :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>&nbsp;
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpConf" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpConf" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td colspan="2" style="background-color: #cc0000; text-align: left">
                            <asp:Label ID="lblCaptionConf" runat="server" Font-Bold="True" Font-Size="Small"
                                ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imIconConf" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                Width="24px" /></td>
                        <td class="Label" style="text-align: left">
                            <asp:Label ID="lblPopUpConf" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px; text-align: center">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            &nbsp;<asp:ImageButton ID="imbYesPopUpConf" runat="server" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/yes.png" />
                            <asp:ImageButton ID="imbNoPopUpConf" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/no.png" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpConf" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" DropShadow="True" PopupControlID="pnlPopUpConf" PopupDragHandleControlID="lblCaptionConf"
                TargetControlID="bePopUpConf">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpConf" runat="server" CausesValidation="False" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

