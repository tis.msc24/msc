﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnSJBeli.aspx.vb" Inherits="Transaction_trnSJBeli" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Purchase Delivery Order"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlFind" runat="server" DefaultButton="btnfind"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="DDLfCabang" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText"><asp:ListItem Value="trnsjbelino">No.</asp:ListItem>
<asp:ListItem Value="p.trnbelipono">PO No.</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterSJ" runat="server" Width="125px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" AutoPostBack="False"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label16" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label>&nbsp; </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>INVOICED</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnfind" onclick="btnfind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnviewall" onclick="btnviewall_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=6><asp:GridView id="gvTblData" runat="server" Width="100%" ForeColor="#333333" OnSelectedIndexChanged="gvTblData_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnsjbelioid,trnsjbelitype,trnsjbelino,periodacctg,trnsjbelidate,trnbelipodate,trnsuppoid,trnbelimstoid,trnsjbelinote,Link_File,trnsjbelistatus,upduser,updtime,suppname,trnbelipono,branch_code" EnableModelValidation="True" GridLines="None" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjbelino" HeaderText="No. PDO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelitype" HeaderText="Type" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipono" HeaderText="PO. No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipodate" HeaderText="PO. Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="70px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="70px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelidate" HeaderText="Recv. Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelinote" HeaderText="Reff No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Link_File" HeaderText="Link File" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelistatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
                                                            <asp:ImageButton ID="imbPrint" OnClick="imbPrint_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsBottom" ToolTip='<%# Eval("trnsjbelino") %>' CommandArgument='<%# Eval("trnsjbelioid") %>'></asp:ImageButton><br />
                                                            <ajaxToolkit:ConfirmButtonExtender ID="cbePrintFromList" runat="server" ConfirmText="Are You Sure Print This Data ?" TargetControlID="imbPrint"></ajaxToolkit:ConfirmButtonExtender>
                                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" HeaderText="a.branch_code" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:Button ID="BtnPrintExcel" runat="server" CommandArgument='<%# Eval("trnsjbelioid") %>'
                CssClass="btn green" OnClick="BtnPrintExcel_Click" Text="Print Ecxel" ToolTip='<%# Eval("trnsjbelino") %>' />
        </ItemTemplate>
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" BorderStyle="None" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:CalendarExtender id="cePer1" runat="server" TargetControlID="FilterPeriod1" Format="dd/MM/yyyy" PopupButtonID="imbPeriod1">
                                                        </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cePer2" runat="server" TargetControlID="FilterPeriod2" Format="dd/MM/yyyy" PopupButtonID="imbPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" TargetControlID="FilterPeriod2"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" TargetControlID="FilterPeriod1">
                                                        </ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="gvTblData"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: List of Purchase Delivery Order</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel3" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 136px" vAlign=middle align=left><asp:Label id="Label21" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label></TD><TD style="FONT-SIZE: 10pt; WIDTH: 200px" vAlign=middle align=left></TD><TD style="FONT-SIZE: 8pt" vAlign=middle align=left><asp:Label id="oid" runat="server" Font-Size="X-Small" Visible="False"></asp:Label><asp:Label id="aCabang" runat="server" Visible="False"></asp:Label><asp:Label id="LblCabang" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" vAlign=middle align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 136px" vAlign=middle align=left>Cabang</TD><TD style="FONT-SIZE: 10pt; WIDTH: 200px" vAlign=middle align=left><asp:DropDownList id="dd_branch" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD style="FONT-SIZE: 8pt" vAlign=middle align=left><asp:DropDownList id="trnsjbelitype" runat="server" Width="133px" CssClass="inpTextDisabled" Visible="False" Enabled="False" OnSelectedIndexChanged="trnsjbelitype_SelectedIndexChanged">
                                                        <asp:ListItem Value="PO">Purchase Order</asp:ListItem>
                                                        <asp:ListItem Value="BO">Back Order</asp:ListItem>
                                                    </asp:DropDownList> </TD><TD style="FONT-SIZE: 8pt" vAlign=middle align=left colSpan=3>&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 136px" vAlign=middle align=left>Delivery&nbsp;No. </TD><TD style="WIDTH: 200px" vAlign=middle align=left><asp:TextBox id="trnsjbelino" runat="server" Width="100px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox></TD><TD vAlign=middle align=left><SPAN style="FONT-SIZE: x-small">Delivery date</SPAN></TD><TD vAlign=middle align=left colSpan=3><asp:TextBox id="trnsjbelidate" runat="server" Width="75px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><SPAN style="FONT-SIZE: 10pt">&nbsp;</SPAN><asp:Label id="Label23" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label> &nbsp;&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 136px" vAlign=middle align=left><SPAN style="FONT-SIZE: x-small">Purchase Ord<SPAN style="FONT-SIZE: 8pt">er No.</SPAN></SPAN> <asp:Label id="Label6" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 200px" vAlign=middle align=left><asp:TextBox id="orderno" runat="server" Width="130px" CssClass="inpTextDisabled" MaxLength="20"></asp:TextBox> <asp:ImageButton id="btnsearhorder" onclick="btnsearhorder_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD><TD style="FONT-SIZE: 8pt" vAlign=middle align=left><SPAN style="FONT-SIZE: x-small">Receive Date</SPAN></TD><TD style="FONT-SIZE: 8pt" vAlign=middle align=left colSpan=3><asp:TextBox id="trnsjreceivedate" runat="server" Width="75px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="Label9" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label> &nbsp;&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 136px" vAlign=middle align=left>Supplier</TD><TD style="WIDTH: 200px" vAlign=middle align=left><asp:TextBox id="suppname" runat="server" Width="150px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> </TD><TD vAlign=middle align=left>Status</TD><TD vAlign=middle align=left colSpan=3><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="trnsjbelistatus" runat="server" Width="100px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> </SPAN>&nbsp;&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 136px" vAlign=middle align=left>Reference No.</TD><TD style="WIDTH: 200px" vAlign=middle align=left><asp:TextBox id="notes" runat="server" Width="100px" CssClass="inpText" MaxLength="50"></asp:TextBox></TD><TD vAlign=middle align=left>Location</TD><TD vAlign=middle align=left colSpan=3><asp:DropDownList id="matLoc" runat="server" Height="20px" CssClass="inpText" AutoPostBack="True">
                                                    </asp:DropDownList><asp:Label id="Label15" runat="server" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 136px" vAlign=middle align=left><asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="N E W" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 200px; COLOR: #000099" vAlign=middle align=left><asp:TextBox id="identifierno" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" vAlign=middle align=left><asp:Label id="trnreftype" runat="server" Font-Size="X-Small" Visible="False">QL_trnordermst</asp:Label> </TD><TD vAlign=middle align=left colSpan=3><SPAN style="FONT-SIZE: x-small"><asp:Label id="lblApprover" runat="server" Text="Approval User" Visible="False"></asp:Label> <asp:TextBox id="ApprovalUserName" runat="server" Width="130px" CssClass="inpTextDisabled" Visible="False" ReadOnly="True" Wrap="False"></asp:TextBox> <asp:ImageButton id="imbFindUserID" onclick="ImageButton2_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbClearUser" onclick="ImageButton3_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> </SPAN>&nbsp;&nbsp;</TD></TR><TR><TD style="WIDTH: 136px" align=left><asp:Label id="Label14" runat="server" Width="182px" Text="Upload Detail From Excel (.xls)" Visible="False"></asp:Label></TD><TD align=left colSpan=5><asp:FileUpload id="budgetfileloc" runat="server" Width="349px" Height="24px" CssClass="inpTextDisabled" Visible="False"></asp:FileUpload><asp:Button id="btnUpload" onclick="btnUpload_Click" runat="server" Height="24px" CausesValidation="False" CssClass="tombol" Text="Upload" Visible="False"></asp:Button><asp:Label id="Label18" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 136px" vAlign=middle align=left></TD><TD vAlign=middle align=left colSpan=5><asp:Label id="lblMenuPic" runat="server" Font-Size="8pt" Font-Names="Arial" ForeColor="Red"></asp:Label></TD></TR><TR><TD vAlign=middle align=left colSpan=6><asp:Label id="periodacctg" runat="server" Width="1px" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label> <asp:Label id="trnbelimstoid" runat="server" Font-Size="X-Small" Visible="False"></asp:Label> <asp:Label id="posting" runat="server" Font-Size="X-Small" Visible="False">In Process</asp:Label> <asp:Label id="trnsuppoid" runat="server" Font-Size="X-Small" Visible="False"></asp:Label> <asp:Label id="approvaluser" runat="server" Font-Size="X-Small" Visible="False"></asp:Label> <asp:Label id="LabelKode" runat="server" Width="74px" Visible="False"></asp:Label> <asp:Label id="trnbelidtloid" runat="server" Font-Size="X-Small" Visible="False"></asp:Label> <asp:Label id="I_u2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 136px" align=left><asp:Label id="Label22" runat="server" Width="136px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Information"></asp:Label> </TD><TD style="COLOR: #000099" align=left><asp:Label id="matoid" runat="server" Font-Size="X-Small" Visible="False"></asp:Label><asp:Label id="trnsjbelidtlseq" runat="server" Font-Size="X-Small" Visible="False"></asp:Label>&nbsp; </TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="tolerance" runat="server" Visible="False"></asp:Label> </TD><TD style="FONT-SIZE: 8pt" align=left>&nbsp;<asp:Label id="IdDetail" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="trnsjbelidtlunit" runat="server" Font-Size="X-Small" Font-Bold="False" Visible="False"></asp:Label> </TD><TD style="FONT-SIZE: 8pt; width: 158px;" align=left></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 136px; HEIGHT: 28px" align=left>
                                                        Katalog&nbsp;<asp:Label id="Label10" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="HEIGHT: 28px" align=left><asp:TextBox id="matname" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:ImageButton id="imbFindItem" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="imbClearItem" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD><TD style="HEIGHT: 28px" align=left><asp:Label id="lblQty" runat="server" Width="80px" Text="Total Quantity"></asp:Label></TD><TD style="HEIGHT: 28px" align=left><asp:TextBox id="refered" runat="server" Width="125px" CssClass="inpTextDisabled" ReadOnly="True">0.00</asp:TextBox> <asp:Label id="pounit" runat="server" Font-Size="X-Small" Font-Bold="True"></asp:Label></TD><TD style="HEIGHT: 28px" align=left><asp:Label id="Label24" runat="server" Width="86px" Text="Received Qty"></asp:Label></TD><TD style="HEIGHT: 28px; width: 158px;" align=left><asp:TextBox id="acum" runat="server" Width="150px" CssClass="inpTextDisabled" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="FONT-SIZE: x-small; WIDTH: 136px" align=left><asp:Label id="Labelqty" runat="server" Text="Quantity"></asp:Label> <asp:Label id="Label13" runat="server" Width="1px" Height="7px" CssClass="Important" Text="*"></asp:Label> <asp:TextBox id="batchoid" runat="server" Width="35px" CssClass="inpText" Visible="False" MaxLength="20" OnTextChanged="trnsjbelidtlqty_TextChanged"></asp:TextBox></TD><TD align=left><asp:TextBox id="trnsjbelidtlqty" runat="server" Width="125px" CssClass="inpText" AutoPostBack="True" MaxLength="10" OnTextChanged="trnsjbelidtlqty_TextChanged">0.00</asp:TextBox>&nbsp;<asp:Label id="satuan" runat="server" Font-Size="X-Small" Font-Bold="True"></asp:Label>&nbsp;<asp:Label id="Label11" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Visible="False">MAX : </asp:Label><asp:Label id="terkirim" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD><TD style="FONT-SIZE: x-small" align=left>SN</TD><TD align=left><asp:TextBox id="texthas_sn" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> </TD><TD align=left>
                                                        Keterangan</TD><TD align=left style="width: 158px"><asp:TextBox id="trnsjbelidtlnote" runat="server" Width="150px" CssClass="inpText" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 136px" vAlign=top align=left></TD><TD align=left></TD><TD align=left><asp:CheckBox id="cb1" runat="server" Font-Size="7pt" Text="Berat Barang" AutoPostBack="True" OnCheckedChanged="cb1_CheckedChanged"></asp:CheckBox></TD><TD align=left><asp:TextBox id="beratBarang" runat="server" CssClass="inpText"></asp:TextBox></TD><TD align=left>Harga Ekspedisi</TD><TD align=left style="width: 158px"><asp:TextBox id="costEkspedisi" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 136px" vAlign=top align=left></TD><TD align=left></TD><TD style="FONT-SIZE: x-small" vAlign=top align=left><asp:CheckBox id="cb2" runat="server" Width="88px" Font-Size="7pt" Text="Berat Volume" AutoPostBack="True" OnCheckedChanged="cb2_CheckedChanged"></asp:CheckBox></TD><TD align=left><asp:TextBox id="beratVolume" runat="server" CssClass="inpText"></asp:TextBox> <asp:Label id="centimeter" runat="server">cm3</asp:Label></TD><TD style="FONT-SIZE: x-small" vAlign=top align=left></TD><TD align=left style="width: 158px"></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 136px" vAlign=top align=left><SPAN style="FONT-SIZE: x-small">SN<asp:CheckBox id="CheckBoxSN" runat="server" Width="103px" AutoPostBack="True" OnCheckedChanged="CheckBoxSN_CheckedChanged" Enabled="False"></asp:CheckBox> </SPAN></TD><TD align=left><asp:FileUpload id="FileExel" runat="server" Width="161px" Height="24px" CssClass="inpTextDisabled" Visible="False"></asp:FileUpload> <asp:Button id="btnuploadexel" runat="server" Width="78px" Text="Upload File" Visible="False"></asp:Button> <BR /><asp:ImageButton style="FONT-WEIGHT: 700" id="btnPreview" runat="server" ImageAlign="AbsMiddle" AlternateText=">> Upload excel example <<"></asp:ImageButton> <BR /><asp:Label id="Loadupload" runat="server"></asp:Label> <asp:Label id="Label19" runat="server" Width="52px" ForeColor="Red"></asp:Label> <BR /></TD><TD style="FONT-SIZE: x-small" vAlign=top align=left><asp:Image id="Image3" runat="server" Visible="False"></asp:Image></TD><TD align=left><asp:TextBox id="qty2" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" OnTextChanged="trnsjbelidtlqty_TextChanged">1.00</asp:TextBox> <asp:Label id="unit2desc" runat="server" Font-Size="X-Small" Font-Bold="True" Visible="False"></asp:Label> <asp:TextBox id="Expedisiharga" runat="server" Width="125px" CssClass="inpText" Visible="False" AutoPostBack="True" OnTextChanged="trnsjbelidtlqty_TextChanged">0.00</asp:TextBox> </TD><TD align=left><asp:Label id="bonus" runat="server" Visible="False"></asp:Label></TD><TD align=left style="width: 158px"><asp:Label id="trnbelidtlunit" runat="server" Font-Size="X-Small" Font-Bold="False" Visible="False"></asp:Label> <asp:Label id="unit2" runat="server" Font-Size="X-Small" Font-Bold="False" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; height: 34px;" align=left colSpan=3><asp:Label id="Label12" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="* Jml pengiriman di atas jml Order memerlukan Approval!!" Visible="False"></asp:Label><asp:CheckBox id="chkCompleted" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="Mark it as Completed Shipment" Visible="False"></asp:CheckBox></TD><TD align=left style="height: 34px"><asp:Label id="lblTolInfo" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="* Quantity is inside tolerance range." Visible="False"></asp:Label></TD><TD align=left colSpan=2 style="height: 34px"><asp:ImageButton id="btnAddtolist" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 200px; background-color: beige;"><asp:GridView id="tbldtl" runat="server" Width="99%" ForeColor="#333333" OnSelectedIndexChanged="tbldtl_SelectedIndexChanged" GridLines="None" EnableModelValidation="True" DataKeyNames="trnsjbelidtlseq,KodeBR" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjbelidtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="item" HeaderText="Item">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelidtlqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelidtlnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="receivedqty" HeaderText="Received Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typeberat" HeaderText="Type Berat" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="berat" HeaderText="Berat">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="hargaekspedisi" HeaderText="Harga Ekspedisi">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ekspedisi" HeaderText="Ekspedisi">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SNList" HeaderText="SN">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="harga" HeaderText="Expedisi" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="KodeBR" HeaderText="KodeBR" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="bonus" HeaderText="Barang Bonus">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data!!"></asp:Label>

                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:GridView id="GVSN" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False">
                                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                                                <Columns>
                                                                    <asp:BoundField DataField="itemcode" HeaderText="Kode Barang"></asp:BoundField>
                                                                    <asp:BoundField DataField="SN" HeaderText="Serial Number">
                                                                        <HeaderStyle Font-Size="X-Small" Width="125px"></HeaderStyle>

                                                                        <ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>

                                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

                                                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            </asp:GridView> </DIV> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=6><asp:Label id="createtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="CrtTime" Visible="False"></asp:Label>Last Update By <asp:Label id="updUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Upduser"></asp:Label>&nbsp;On <asp:Label id="updTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Updtime"></asp:Label>&nbsp;</TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False" ConfirmText="Are You Sure Print This Data ?"></asp:ImageButton>&nbsp;<asp:Button ID="BtnToExcel" runat="server" CssClass="btn green"
                                                                Text="Print To Excel" />
                                                                <asp:Button id="ButtonA" onclick="ButtonA_Click" runat="server" Text="ButtonA" Visible="False"></asp:Button> </TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel3"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" Enabled="True" TargetControlID="trnsjreceivedate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" CultureTimePlaceholder=":" CultureThousandsPlaceholder="," CultureDecimalPlaceholder="." CultureDatePlaceholder="/" CultureDateFormat="MDY" CultureCurrencySymbolPlaceholder="$" CultureAMPMPlaceholder="AM;PM" CultureName="en-US"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CEtrnsjbelidate" runat="server" Enabled="True" TargetControlID="trnsjbelidate" PopupButtonID="ImageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Enabled="True" TargetControlID="trnsjreceivedate" PopupButtonID="ImageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEqty2" runat="server" TargetControlID="qty2" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEtrnsjbelidtlqty" runat="server" TargetControlID="trnsjbelidtlqty" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEExpedisiharga" runat="server" TargetControlID="Expedisiharga" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender><asp:HiddenField id="hfExcel" runat="server"></asp:HiddenField></TD></TR></TBODY></TABLE><asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" EnableTheming="True"><asp:View id="View1" runat="server"><asp:Label id="Label4" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information" Visible="False"></asp:Label> <asp:Label id="Label5" runat="server" Width="1px" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton1" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">List</asp:LinkButton><BR /></asp:View> <asp:View id="View2" runat="server"><asp:LinkButton id="LinkButton2" onclick="LinkButton2_Click" runat="server" CssClass="submenu" Font-Size="Small">Information</asp:LinkButton>&nbsp;<asp:Label id="Label7" runat="server" ForeColor="#585858" Text="|"></asp:Label>&nbsp;<asp:Label id="Label8" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="List"></asp:Label></asp:View> </asp:MultiView><asp:Panel id="PanelPO" runat="server" Width="800px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblCaptPO" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List of Purchase Order"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:DropDownList id="DDLFilterPO" runat="server" CssClass="inpText">
                            <asp:ListItem Value="po.trnbelipono">PO No</asp:ListItem>
                            <asp:ListItem Value="s.suppname">Supplier</asp:ListItem>
                        </asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterPO" runat="server" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindPO" onclick="imbFindPO_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbAlPO" onclick="imbAlPO_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView id="gvListPO" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" EnableModelValidation="True" DataKeyNames="pomstoid,pono,podate,suppname,suppoid,podeliv,Type,toCabang,branch_code" CellPadding="4" AutoGenerateColumns="False" PageSize="8" AllowPaging="True" OnPageIndexChanging="gvListPO_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="No. PO">
<HeaderStyle CssClass="gvhdr" Width="125px"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podate" HeaderText="Date">
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podeliv" HeaderText="PO Deliv Date">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="prefix" HeaderText="Prefix">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Type" HeaderText="Type">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="dCabang" HeaderText="Tujuan">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="Label89" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="lkbClosePO" onclick="lkbClosePO_Click" runat="server" Font-Size="X-Small">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD vAlign=top align=center colSpan=2><ajaxToolkit:ModalPopupExtender id="mpepo" runat="server" Enabled="True" TargetControlID="btnHidepo" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptPO" PopupControlID="PanelPO" Drag="True" DynamicServicePath=""></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidepo" runat="server" Visible="False"></asp:Button> <asp:Panel id="pnlListUser" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblCaptUser" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of User"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 5px; TEXT-ALIGN: center" align=left></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 97%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 111px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 67%"><asp:GridView id="gvListUser" runat="server" Width="97%" Font-Size="X-Small" ForeColor="#333333" OnSelectedIndexChanged="gvListUser_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="userid,username" EnableModelValidation="True" GridLines="None">
<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="userid" HeaderText="User ID">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="username" HeaderText="User Name">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
                                                                                <asp:Label ID="Label89" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                                            
</EmptyDataTemplate>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD align=center><asp:LinkButton id="lkbCloseUser" onclick="lkbCloseUser_Click" runat="server">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeUser" runat="server" Enabled="True" TargetControlID="btnHideUser" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptUser" PopupControlID="pnlListUser" Drag="True" DynamicServicePath=""></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideUser" runat="server" Visible="False"></asp:Button> </TD></TR><TR><TD vAlign=top align=center colSpan=2><ajaxToolkit:ModalPopupExtender id="mpeRef" runat="server" Enabled="True" TargetControlID="btnHideRef" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptRef" PopupControlID="Panel2" Drag="True" DynamicServicePath=""></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideRef" runat="server" Visible="False"></asp:Button> <asp:Panel id="PanelSF" runat="server" Width="500px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblCaptSF" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List of Sales Forecast"></asp:Label> </TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 136px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div5"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvListSF" runat="server" Width="98%" Font-Size="X-Small" ForeColor="#333333" OnSelectedIndexChanged="gvListSF_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="forcoid,forcno,forcdate,custname,custoid" EnableModelValidation="True" GridLines="None">
<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="forcno" HeaderText="No">
<HeaderStyle Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="forcdate" HeaderText="Date">
<HeaderStyle Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
                                                                                <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                                            
</EmptyDataTemplate>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="lkbCloseSF" onclick="lkbClosePO_Click" runat="server" Font-Size="X-Small">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpesf" runat="server" Enabled="True" TargetControlID="btnHidesf" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptSF" PopupControlID="PanelSF" Drag="True" DynamicServicePath=""></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidesf" runat="server" Visible="False"></asp:Button> </TD></TR></TBODY></TABLE><asp:Panel id="Panel2" runat="server" Width="600px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblCaptRef" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Item"></asp:Label> </TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 204px; BORDER-BOTTOM-STYLE: none" id="Fieldset7"><DIV id="Div9"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 72%"><asp:GridView id="gvReference" runat="server" Width="98%" ForeColor="#333333" OnSelectedIndexChanged="gvReference_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemcodeb,matlongdesc,matoid,qty,unit,satuan,matpictureloc,podtloid,trnsjbelidtlqty" EnableModelValidation="True" GridLines="None">
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True">
                                            <HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
                                        </asp:CommandField>
                                        <asp:BoundField DataField="itemcodeb" HeaderText="CodeItem" Visible="False">
                                            <HeaderStyle CssClass="gvhdr"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matlongdesc" HeaderText="Description">
                                            <HeaderStyle Font-Size="X-Small" Width="250px" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Merk" HeaderText="Merk">
                                            <HeaderStyle CssClass="gvhdr"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="qty" HeaderText="Total Qty">
                                            <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="satuan" HeaderText="Unit">
                                            <HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="trnsjbelidtlqty" HeaderText="Received">
                                            <HeaderStyle CssClass="gvhdr"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="podtloid" HeaderText="id" Visible="False">
                                            <HeaderStyle CssClass="gvhdr"  />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label66" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>

                                    </EmptyDataTemplate>

                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"  />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                </asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="LinkButton3" onclick="LinkButton3_Click" runat="server" Font-Size="X-Small">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:UpdatePanel id="upListMat" runat="server"><ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Purchase Delivery Item</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText">
                                                        <asp:ListItem Enabled="False" Value="matno">Log/Pallet No.</asp:ListItem>
                                                        <asp:ListItem Value="itemcodeb">Code</asp:ListItem>
                                                        <asp:ListItem Value="matlongdesc">Description</asp:ListItem>
                                                    </asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="TD2" class="Label" align=center colSpan=3 runat="server" Visible="false"><asp:CheckBox id="cbCat01" runat="server" Text="Group Item:"></asp:CheckBox> &nbsp; <asp:DropDownList id="DDLCat01" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True">
                                                    </asp:DropDownList> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox id="cbCat02" runat="server" Text="Subgroup Item :"></asp:CheckBox> &nbsp;&nbsp; <asp:DropDownList id="DDLCat02" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True">
                                                    </asp:DropDownList></TD></TR><TR><TD id="TD1" class="Label" align=center colSpan=3 runat="server" Visible="false"><asp:CheckBox id="cbCat03" runat="server" Text="Type Item     :"></asp:CheckBox> &nbsp; <asp:DropDownList id="DDLCat03" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True">
                                                    </asp:DropDownList> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox id="cbCat04" runat="server" Text="Merk Item   :"></asp:CheckBox> &nbsp;&nbsp;&nbsp; <asp:DropDownList id="DDLCat04" runat="server" Width="200px" CssClass="inpText">
                                                    </asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" Width="99%" CssClass="MyTabStyle" ForeColor="#333333" GridLines="None" EnableModelValidation="True" DataKeyNames="itemcodeb,matlongdesc,matoid,qty,unit,satuan,matpictureloc,podtloid,trnsjbelidtlqty,hargaekspedisi,ekspedisi,typeberat" CellPadding="4" AutoGenerateColumns="False" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
                                                <asp:CheckBox ID="cbHdrLM" runat="server" AutoPostBack="true" OnCheckedChanged="cbHdrLM_CheckedChanged" />
                                            
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("podtloid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcodeb" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Mark">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty PO">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelidtlqty" HeaderText="Received">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty Receive"><ItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("matqty") %>' Enabled='<%# eval("enableqty") %>' MaxLength="6"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" TargetControlID="tbQty" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Harga Expedisi (IDR)"><ItemTemplate>
<asp:TextBox id="tbHarga" runat="server" Width="100px" CssClass="inpText" Text='<%# eval("hargaekspedisi") %>' Enabled='<%# eval("enablehargaekspedisi") %>' MaxLength="8"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeHargaLM" runat="server" TargetControlID="tbHarga" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="satuan" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unitoid" HeaderText="unitoid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="typeberat" HeaderText="Type Berat">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Berat"><ItemTemplate>
<asp:TextBox id="tbBerat" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("berat") %>' Enabled='<%# eval("enableberat") %>' MaxLength="6"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeBerat" runat="server" TargetControlID="tbBerat" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="has_SN" HeaderText="SN">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="bonus" HeaderText="Barang Bonus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Width="261px" ForeColor="Red" Text="Maaf, data yang anda maksud tidak ada..!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3>&nbsp;<asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllToList" runat="server" Visible="False">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="lbAddToListMat"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
                                    <asp:PostBackTrigger ControlID="BtnToExcel" />
</triggers>
                            </asp:UpdatePanel>
                            &nbsp;&nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: Form Purchase Delivery Order</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" CssClass="modalMsgBox" Visible="False" DefaultButton="btnMsgBoxOK">
                <table cellspacing="1" cellpadding="1" border="0">
                    <tbody>
                        <tr>
                            <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" valign="top" align="left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                            </td>
                            <td class="Label" valign="top" align="left">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

