<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPending.aspx.vb" Inherits="ReportForm_rptPending" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="970">
        <tr>
            <td align="left" class="header" colspan="3" style="background-color: silver">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Maroon" Text=":: Laporan Pending"></asp:Label></td>
        </tr>
    </table>
    <table width="500">
        <tr>
            <td align="left" colspan="3">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width=500><TBODY><TR><TD><asp:Label id="Label5" runat="server" Text="Cabang" __designer:wfdid="w9"></asp:Label></TD><TD><asp:Label id="Label8" runat="server" Text=":" __designer:wfdid="w10"></asp:Label></TD><TD><asp:DropDownList id="DDLcabang" runat="server" CssClass="inpText" __designer:wfdid="w11"></asp:DropDownList></TD></TR><TR><TD><asp:Label id="Label3" runat="server" Text="Period"></asp:Label></TD><TD><asp:Label id="Label4" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtPeriod1" runat="server" Width="97px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnCalendar1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="97px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD></TD><TD></TD><TD>&nbsp;</TD></TR><TR><TD><asp:Label id="Label1" runat="server" Text="Filter Teknisi" Visible="False"></asp:Label></TD><TD><asp:Label id="Label2" runat="server" Text=":" Visible="False"></asp:Label></TD><TD><asp:TextBox id="txtFilter" runat="server" Width="120px" CssClass="inpText" Visible="False"></asp:TextBox></TD></TR></TBODY></TABLE><TABLE width=500><TBODY><TR><TD colSpan=3><asp:ImageButton id="btnview" onclick="btnview_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnETPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnETExcel" onclick="btnETExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR><TR><TD colSpan=3></TD></TR><TR><TD colSpan=3><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><CR:CrystalReportViewer id="CRVPending" runat="server" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR><TR><TD colSpan=3><ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" TargetControlID="txtPeriod1"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" TargetControlID="txtPeriod2"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CEPeriod1" runat="server" TargetControlID="txtPeriod1" PopupButtonID="btnCalendar1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CEPeriod2" runat="server" TargetControlID="txtPeriod2" PopupButtonID="btnCalendar2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnETExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnETPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnview"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel id="UpdatePanel2" runat="server">
        <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width=250><TR><TD style="HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR></TABLE><TABLE width=250><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD><TD></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" Drag="True" DropShadow="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

