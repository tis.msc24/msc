<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTWServiceSup.aspx.vb" Inherits="trnTwSvcSupp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Service Supplier" CssClass="Title" ForeColor="Navy" Font-Names="Verdana" Font-Size="21px" Width="238px"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
               <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                 <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Service Supplier :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 95px" class="Label" align=left>From Branch</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="drCabang" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w66"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 95px" class="Label" align=left><asp:DropDownList id="DDLFilter" runat="server" Width="90px" CssClass="inpText" __designer:wfdid="w67"><asp:ListItem Value="trfwhserviceNo">Transferno</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w68" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 95px; HEIGHT: 22px" align=left><asp:CheckBox id="CBTanggal" runat="server" Text="Tanggal" __designer:wfdid="w69"></asp:CheckBox></TD><TD style="HEIGHT: 22px" align=left>:</TD><TD style="HEIGHT: 22px" align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w70"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w71"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w72"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w73"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w74"></asp:Label></TD></TR><TR><TD style="WIDTH: 95px" align=left>Type</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="TypeDDL" runat="server" CssClass="inpText" __designer:wfdid="w75"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="EXTERNAL">EXTERNAL SUPPLIER</asp:ListItem>
<asp:ListItem Value="INTERNSUPP">INTERNAL SUPPLIER</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 95px" align=left>Status</TD><TD align=left>: </TD><TD align=left colSpan=4><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" __designer:wfdid="w76"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="In Process">IN PROCESS</asp:ListItem>
<asp:ListItem Value="Post">POST</asp:ListItem>
<asp:ListItem>IN APPROVAL</asp:ListItem>
<asp:ListItem>APPROVED</asp:ListItem>
<asp:ListItem Enabled="False" Value="Closed">CLOSED</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w77">
                                                            </asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w78">
                                                            </asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w79" DefaultButton="btnSearch"><asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w80" DataKeyNames="trfwhserviceoid,trfwhserviceNo,fromBranch,branch_code" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trfwhserviceNo" HeaderText="No. Transfer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfwhservicedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FromBranch" HeaderText="From Branch">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Trfwhservicetype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trfwhservicestatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" Text='<%# Eval("trfwhserviceoid") %>' __designer:wfdid="w1"></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
&nbsp;<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" __designer:wfdid="w5" ToolTip='<%# eval("trfwhserviceoid") %>'>Print Out</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> <ajaxToolkit:MaskedEditExtender id="MEEtgl1" runat="server" __designer:wfdid="w81" TargetControlID="tgl1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MEEtgl2" runat="server" __designer:wfdid="w82" TargetControlID="tgl2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CEEtgl1" runat="server" __designer:wfdid="w83" TargetControlID="tgl1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CEEtgl2" runat="server" __designer:wfdid="w84" TargetControlID="tgl2" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 102px" class="Label" align=left><asp:Label id="Information" runat="server" Font-Bold="True" Text="Information :" Font-Underline="True" __designer:wfdid="w94"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left></TD><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD style="WIDTH: 102px" class="Label" align=left>Transfer No.</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="200px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" __designer:wfdid="w95" Enabled="False" MaxLength="20"></asp:TextBox>&nbsp;<asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" __designer:wfdid="w96" Visible="False"></asp:Label> </TD><TD class="Label" align=left>Type Service</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="TypeService" runat="server" CssClass="inpText" __designer:wfdid="w97" AutoPostBack="True"><asp:ListItem Value="EXTERNAL">EXTERNAL SUPPLIER</asp:ListItem>
<asp:ListItem Value="INTERNSUPP">INTERNAL SUPPLIER</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 102px" class="Label" align=left>Date</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" __designer:wfdid="w98" Enabled="False" ValidationGroup="MKE"></asp:TextBox> <asp:ImageButton id="imbTwDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w99" Visible="False"></asp:ImageButton><asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w100"></asp:Label></TD><TD style="vertical-alin: top" class="Label" align=left><asp:Label id="labelnotr" runat="server" Width="75px" Text="No. Ref" __designer:wfdid="w101"></asp:Label></TD><TD class="Label" align=left>:</TD><TD style="WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left><asp:TextBox id="noref" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w102" MaxLength="200"></asp:TextBox><asp:Label id="pooid" runat="server" __designer:wfdid="w103" Visible="False"></asp:Label> <asp:Label id="FromLoc" runat="server" __designer:wfdid="w104" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 102px" class="Label" align=left>From Branch</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="FromBranch" runat="server" Width="275px" CssClass="inpText" __designer:wfdid="w105" AutoPostBack="True" OnSelectedIndexChanged="FromBranch_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Width="75px" Text="Supplier" __designer:wfdid="w106"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLTujuan" runat="server" Width="275px" CssClass="inpText" __designer:wfdid="w107" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:TextBox id="SuppName" runat="server" Width="200px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w108"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgBtnSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w109"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImgBtnErase" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w110"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 102px" class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVSupp" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w111" Visible="False" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="suppoid,suppcode,suppname,suppaddr" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Supp. Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp. Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 102px" class="Label" align=left>From Location</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="fromlocation" runat="server" Width="275px" CssClass="inpText" __designer:wfdid="w112" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label7" runat="server" Width="90px" Text="Gudang Tujuan" __designer:wfdid="w113" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Width="11px" Text=":" __designer:wfdid="w114" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLTujuanGudang" runat="server" Width="275px" CssClass="inpText" __designer:wfdid="w115" Visible="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 102px; WHITE-SPACE: nowrap" class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="note" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w116" MaxLength="200"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Status</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:TextBox id="trnstatus" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w117" Enabled="False" MaxLength="20">In Process</asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 102px; WHITE-SPACE: nowrap" class="Label" align=left><asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" __designer:wfdid="w118" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:ImageButton id="imbClearPenerimaanDetail" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w119" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbAddToListPenerimaan" onclick="imbAddToListPenerimaan_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w120" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Penerimaan Detail :" Font-Underline="True" __designer:wfdid="w121"></asp:Label><asp:Label id="SuppOid" runat="server" __designer:wfdid="w122" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 102px" class="Label" align=left><asp:Label id="Label4" runat="server" Width="99px" Text="No. Penerimaan" __designer:wfdid="w123"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="txtPenerimaan" runat="server" Width="200px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w124"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSeachPenerimaan" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w125"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearPenerimaan" onclick="imbClearPenerimaan_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w126"></asp:ImageButton> <asp:Label id="penerimaanoid" runat="server" __designer:wfdid="w127" Visible="False"></asp:Label>&nbsp;<asp:Label id="sequence" runat="server" __designer:wfdid="w128" Visible="False"></asp:Label>&nbsp;<asp:Label id="reqoid" runat="server" __designer:wfdid="w129" Visible="False"></asp:Label>&nbsp;<asp:Label id="labelseq" runat="server" __designer:wfdid="w130" Visible="False"></asp:Label> <asp:Label id="snno" runat="server" __designer:wfdid="w131" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 102px" class="Label" align=left><asp:Label id="Label25" runat="server" Text="Item" __designer:wfdid="w132"></asp:Label> <asp:Label id="Label26" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w133" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="354px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w134"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w135"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w136"></asp:ImageButton> <asp:Label id="labelitemoid" runat="server" __designer:wfdid="w137" Visible="False"></asp:Label>&nbsp;<asp:Label id="reqdtloid" runat="server" __designer:wfdid="w138" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 102px" class="Label" align=left>Qty</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="qty" runat="server" CssClass="inpText" __designer:wfdid="w139"></asp:TextBox>&nbsp;<asp:Label id="reqqty" runat="server" __designer:wfdid="w140" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=right></TD></TR><TR><TD style="WIDTH: 102px" class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" __designer:wfdid="w141" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=right><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w142"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w143"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 180px; BACKGROUND-COLOR: beige"><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w144" DataKeyNames="reqoid,itemoid,itemdesc,reqdtljob,reqqty,qty,reqdtloid,reqcode,snno" CellPadding="4" AutoGenerateColumns="False" GridLines="None" OnRowDeleting="GVItemDetail_RowDeleting">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="reqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqcode" HeaderText="No. Penerimaan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="135px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqqty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtljob" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="snno" HeaderText="No. SN">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server" Visible="False" __designer:wfdid="w8"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6>Created By&nbsp;<asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w145"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w146"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w147"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w148"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w149"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w150"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w151"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w152" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnSendApp" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsBottom" __designer:wfdid="w153" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w154" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w155"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" __designer:wfdid="w156" ValidChars="1234567890,." TargetControlID="qty"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:CalendarExtender id="ceTwDate" runat="server" __designer:wfdid="w157" TargetControlID="transferdate" Format="dd/MM/yyyy" PopupButtonID="imbTwDate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="TwDate" runat="server" __designer:wfdid="w158" TargetControlID="transferdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel4" runat="server">
        <contenttemplate>
<asp:Panel id="panelPenerimaan" runat="server" CssClass="modalBox" __designer:wfdid="w160" DefaultButton="imbFindPenerimaan" Visible="False"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptPenerimaan" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Penerimaan" __designer:wfdid="w161"></asp:Label></TD></TR><TR><TD align=center>Filter : <asp:DropDownList id="DDLFilterPenerimaan" runat="server" CssClass="inpText" __designer:wfdid="w162"><asp:ListItem Value="req.reqcode">No. Penerimaan</asp:ListItem>
<asp:ListItem Value="cuk.custname">Customer</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterPenerimaan" runat="server" CssClass="inpText" __designer:wfdid="w163"></asp:TextBox> <asp:ImageButton id="imbFindPenerimaan" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w164"></asp:ImageButton> <asp:ImageButton id="imbViewPenerimaan" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w165"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvPenerimaan" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w166" PageSize="8" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" DataKeyNames="reqoid,reqcode,custoid,custname,reqdate" Visible="False" OnSelectedIndexChanged="gvPenerimaan_SelectedIndexChanged" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="reqcode" HeaderText="No. Penerimaan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" HeaderText="custoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdate" HeaderText="Tgl Penerimaan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center><asp:LinkButton id="lkbClosePenerimaan" runat="server" __designer:wfdid="w167">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePenerimaan" runat="server" __designer:wfdid="w168" TargetControlID="btnHidePenerimaan" PopupDragHandleControlID="lblCaptPenerimaan" BackgroundCssClass="modalBackground" PopupControlID="panelPenerimaan"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePenerimaan" runat="server" __designer:wfdid="w169" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel8" runat="server">
        <contenttemplate>
<asp:Panel id="panelItem" runat="server" CssClass="modalBox" __designer:wfdid="w171" DefaultButton="imbFindItem" Visible="False"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptItem" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Barang" __designer:wfdid="w172"></asp:Label></TD></TR><TR><TD align=center>Filter : <asp:DropDownList id="DDLFilterItem" runat="server" CssClass="inpText" __designer:wfdid="w173"><asp:ListItem Value="itemdesc">Deskripsi</asp:ListItem>
<asp:ListItem>Itemcode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterItem" runat="server" CssClass="inpText" __designer:wfdid="w174"></asp:TextBox> <asp:ImageButton id="imbFindItem" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w175"></asp:ImageButton> <asp:ImageButton id="imbViewItem" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w176"></asp:ImageButton></TD></TR><TR><TD align=center><DIV style="OVERFLOW-Y: scroll; WIDTH: 700px; HEIGHT: 255px; BACKGROUND-COLOR: beige"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" Height="5px" ForeColor="#333333" __designer:wfdid="w177" PageSize="8" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemoid,itemcode,itemdesc,reqdtljob,reqqty,reqdtloid" Visible="False" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" __designer:wfdid="w2" ToolTip='<%# eval("reqdtloid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqqty" DataFormatString="{0:#,##0.00}" HeaderText="Req. Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("QtyOs") %>' __designer:wfdid="w1" MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" __designer:wfdid="w2" ValidChars="1234567890.," TargetControlID="tbQty">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="reqdtljob" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="False" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center><asp:LinkButton id="lbAddToListMat" runat="server" __designer:wfdid="w178">[ Add To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lkbCloseItem" runat="server" __designer:wfdid="w179">[ Close ]</asp:LinkButton></TD></TR><TR><TD><asp:Label id="sValidasi" runat="server" __designer:wfdid="w180" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" __designer:wfdid="w181" TargetControlID="btnHideItem" PopupDragHandleControlID="lblCaptItem" BackgroundCssClass="modalBackground" PopupControlID="panelItem"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideItem" runat="server" __designer:wfdid="w182" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Service Supplier :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg"  runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender>
<span style="display:none"><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button></span>
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;
</asp:Content>

