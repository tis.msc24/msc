Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.Reporting
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms



Partial Class trnBarcode
    Inherits System.Web.UI.Page

#Region "Variables"
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim CProc As New ClassProcedure
    Dim oRegex As Regex
    Dim oMatches As MatchCollection
    Dim orderdtleach As Int32 = 0
    Private pagedData As New PagedDataSource()
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            ImageButton3.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            ImageButton3.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 2 Then 'Warning
            ImageButton3.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        WARNING.Text = strCaption : lblValidasi.Text = sMessage
        CProc.SetModalPopUpExtender(btnExtender, PanelErrMsg, MPEErrMsg, True)
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Response.Redirect("~\other\NotAuthorize.aspx")
        'End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xnomeja As String = Request.QueryString("nomeja")
            Dim xoutletoid As String = Session("outletoid")
            Session.Clear()  ' -->>  clear all session 
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("trnBarcode.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang 
        End If

        Page.Title = CompnyName & " - Print Barcode"

        If Not Page.IsPostBack Then
            FillDDL(itemloc, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' ORDER BY a.gendesc")
            '  itemloc.Items.Add("SEMUA LOKASI")
            ' itemloc.SelectedValue = "SEMUA LOKASI"

            FillDDL(typeM, "select genoid,gendesc from QL_mstgen where gengroup='ITEMGROUP' and cmpcode='" & cmpcode & "' order by gendesc")
            typeM.Items.Add("SEMUA GRUP")
            typeM.SelectedValue = "SEMUA GRUP"

            FillDDL(mattype, "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and cmpcode='" & cmpcode & "' order by gendesc")
            mattype.Items.Add("SEMUA SUB GRUP")
            mattype.SelectedValue = "SEMUA SUB GRUP"

            FillDDL(person, "select personoid, personname from ql_mstperson where STATUS='Aktif' AND cmpcode = '" & cmpcode & "' AND PERSONPOST = (SELECT genoid FROM QL_mstgen WHERE gengroup='JOBPOSITION' AND gendesc = 'SALES PERSON')")

            upduser.Text = Session("UserID")
            updtime.Text = Now.Date
        End If
    End Sub

    Sub BindDataItem()
        Try
            Dim periodeacctg As String = GetStrData("select top 1 periodacctg from ql_crdmtr where closingdate='01/01/1900'").Trim

            Dim swhere As String = ""
            If typeM.SelectedValue <> "SEMUA GRUP" Then
                swhere &= " and m.itemgroupoid=" & typeM.SelectedValue
            End If
            If mattype.SelectedValue <> "SEMUA SUB GRUP" Then
                swhere &= " and m.itemsubgroupoid=" & mattype.SelectedValue
            End If
            Dim lokasi As String = ""
            Dim xsql As String = ""

            If itemloc.SelectedValue = "SEMUA LOKASI" Then

                sSql = "SELECT DISTINCT m.itemoid, itemcode, itemdesc, g.gendesc unit, satuan3 unitoid, 0.0 saldoawal, (SELECT gendesc FROM QL_mstgen WHERE genoid = g1.genother1) + ' - ' + g1.gendesc gudang, g1.genoid, '' note, 0.0 amount FROM QL_mstitem m INNER JOIN ql_crdmtr cr on cr.periodacctg = '" & periodeacctg & "' AND m.itemoid = cr.refoid AND cr.refname = 'ql_mstitem' INNER JOIN QL_mstgen g ON g.genoid = m.satuan3 AND g.cmpcode = m.cmpcode AND m.itemflag = '" & status.SelectedValue & "' INNER JOIN QL_mstgen g1 ON g1.cmpcode = m.cmpcode AND g1.genoid IN (SELECT x.genoid FROM QL_mstgen x INNER JOIN QL_mstgen y ON x.genother1 = y.genoid AND x.cmpcode = y.cmpcode AND x.gengroup = 'LOCATION' AND y.gengroup = 'WAREHOUSE' AND y.genother1 = 'GROSIR') WHERE m.cmpcode = '" & cmpcode & "' AND (m.itemdesc LIKE '%" & Tchar(FilterText.Text) & "%' OR m.itemcode LIKE '%" & Tchar(FilterText.Text) & "%') " & swhere & " AND m.personoid = " & person.SelectedValue & " AND EXISTS (SELECT refoid, mtrlocoid FROM QL_crdmtr WHERE refoid = m.itemoid AND mtrlocoid = g1.genoid AND periodacctg = '" & periodeacctg & "') ORDER BY m.itemoid, g1.genoid"

            Else

                'sSql = "SELECT DISTINCT m.itemoid, itemcode, itemdesc, g.gendesc unit, satuan3 unitoid, 0.0 saldoawal, (SELECT gendesc FROM QL_mstgen WHERE genoid = g1.genother1) + ' - ' + g1.gendesc gudang, cr.mtrlocoid AS genoid, '' note, 0.0 amount FROM QL_mstitem m INNER JOIN ql_crdmtr cr on cr.periodacctg = '" & periodeacctg & "' AND m.itemoid = cr.refoid AND cr.refname = 'QL_MSTITEM' INNER JOIN QL_mstgen g ON g.genoid = m.satuan3 AND g.cmpcode = m.cmpcode AND m.itemflag = '" & status.SelectedValue & "' INNER JOIN QL_mstgen g1 ON g1.cmpcode = cr.cmpcode AND g1.genoid = cr.mtrlocoid AND cr.mtrlocoid = " & itemloc.SelectedValue & " WHERE m.cmpcode = '" & cmpcode & "' AND (m.itemdesc LIKE '%" & Tchar(FilterText.Text) & "%' OR m.itemcode LIKE '%" & Tchar(FilterText.Text) & "%') " & swhere & " AND m.personoid = " & person.SelectedValue & " AND EXISTS (SELECT refoid, mtrlocoid FROM QL_crdmtr WHERE refoid = m.itemoid AND mtrlocoid = g1.genoid AND periodacctg = '" & periodeacctg & "') ORDER BY m.itemoid, g1.genoid"

                sSql = "SELECT DISTINCT m.itemoid, itemcode, itemdesc, merk,g.gendesc unit, satuan3 unitoid, 0.0 saldoawal, isnull(('-'),'-') gudang, 0 AS genoid, '' note, 0.0 amount FROM QL_mstitem m  INNER JOIN QL_mstgen g ON g.genoid = m.satuan3 AND g.cmpcode = m.cmpcode AND m.itemflag = 'Aktif'  WHERE m.cmpcode = '" & cmpcode & "' AND (m.itemdesc LIKE '%" & Tchar(FilterText.Text) & "%' OR m.itemcode LIKE '%" & Tchar(FilterText.Text) & "%' OR m.merk LIKE '%" & Tchar(FilterText.Text) & "%') " & swhere & " AND m.personoid = " & person.SelectedValue & "  ORDER BY m.itemoid"

            End If

            Dim tbDtl As DataTable = cKoneksi.ambiltabel(sSql, "stockadj")
            gvMst.DataSource = tbDtl
            gvMst.DataBind()

            Session("stockadj") = tbDtl
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindDataItem()
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        BindDataItem()
    End Sub

    Protected Sub newprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("stockadj") Is Nothing Then
            Dim iError As Int16 = 0
            Dim objTable As DataTable = Session("stockadj")

            Dim tbox As System.Web.UI.WebControls.TextBox = TryCast(sender, System.Web.UI.WebControls.TextBox)
            Dim tboxval As Double = 0.0
            If Double.TryParse(tbox.Text, tboxval) = True Then
                tbox.Text = Format(tboxval, "#,##0.00")
            Else
                tbox.Text = Format(0, "#,##0.00")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("trnBarcode.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        If Not Session("stockadj") Is Nothing Then
            Dim dtab As DataTable = Session("stockadj")
            If dtab.Rows.Count <= 0 Then
                showMessage("Detail Barcode tidak ditemukan !", 2)
                Exit Sub
            End If

            Dim gvr As GridViewRow
            Dim tbqty As System.Web.UI.WebControls.TextBox
            Dim tbnote As System.Web.UI.WebControls.TextBox
            Dim labelitemoid As System.Web.UI.WebControls.Label
            Dim labelgenoid As System.Web.UI.WebControls.Label
            Dim qty As Double = 0.0
            Dim dtable As DataTable = Session("stockadj")
            Dim dview As DataView = dtable.DefaultView

            For i As Int32 = 0 To gvMst.Rows.Count - 1
                dview.RowFilter = ""

                gvr = gvMst.Rows(i)
                tbqty = gvr.FindControl("newqty")
                tbnote = gvr.FindControl("note")
                labelitemoid = gvr.FindControl("labelitemoid")
                labelgenoid = gvr.FindControl("labelgenoid")

                If tbqty.Text <> "" And Double.TryParse(tbqty.Text, qty) = True Then
                    dview.RowFilter = "itemoid = " & Integer.Parse(labelitemoid.Text) & " AND genoid = " & Integer.Parse(labelgenoid.Text) & ""

                    dview(0).BeginEdit()
                    dview(0).Item("saldoawal") = qty
                    dview(0).Item("note") = tbnote.Text.Trim
                    dview(0).EndEdit()
                Else
                    dview.RowFilter = "itemoid = " & Integer.Parse(labelitemoid.Text) & " AND genoid = " & Integer.Parse(labelgenoid.Text) & ""

                    dview(0).BeginEdit()
                    dview(0).Item("saldoawal") = 0
                    dview(0).EndEdit()
                End If
            Next

            dview.RowFilter = ""
            dtable.AcceptChanges()
            Session("stockadj") = dtable
        Else
            showMessage("Detail Barcode tidak ditemukan !", 2)
            Exit Sub
        End If

        Dim periodene As String = GetDateToPeriodAcctg(GetServerTime).Trim

        'sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & periodene & "' AND closingdate = '01/01/1900'"
        'If cKoneksi.ambilscalar(sSql) <= 0 Then
        '    showMessage("Tanggal ini tidak dalam periode Open Stock !!", 2)
        '    Exit Sub
        'End If

        If Session("stockadj") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("stockadj")
            Dim saldoakhire As Double = 0.0

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim fullstate As Integer = 0
            For c1 As Integer = 0 To objTable.Rows.Count - 1

                If objTable.Rows(c1).Item("saldoawal") < 0 Then
                    'sSql = "SELECT saldoAkhir FROM QL_crdmtr WHERE periodacctg = '" & periodene & "' AND mtrlocoid = " & objTable.Rows(c1).Item("genoid") & " AND refoid=" & objTable.Rows(c1).Item("itemoid") & " AND refname = 'ql_mstitem'"
                    'xCmd.CommandText = sSql
                    'saldoakhire = xCmd.ExecuteScalar
                    'If saldoakhire = Nothing Or saldoakhire = 0 Then
                    '    showMessage("Adjusment Tidak bisa dilakukan untuk barang '" & objTable.Rows(c1).Item("itemdesc") & "' karena Saldo akhir satuan std hanya tersedia 0.00 !", 2)
                    '    conn.Close()
                    '    Exit Sub
                    'Else
                    'If saldoakhire + objTable.Rows(c1).Item("saldoawal") < 0 Then
                    showMessage("Print Barcode Tidak bisa dilakukan untuk barang '" & objTable.Rows(c1).Item("itemdesc") & "' karena Qty Print harus lebih besar daripada 0 !", 2)
                    conn.Close()
                    Exit Sub
                    '    End If
                    'End If
                End If

        If objTable.Rows(c1).Item("saldoawal") <> 0 Then
            fullstate = 1
        End If

            Next

            If fullstate = 0 Then
                showMessage("Print Barcode tidak dilakukan karena quantity untuk setiap barang jumlahnya 0.00 !" & ViewState("redirect"), 2)
                conn.Close()
                Exit Sub
            End If

        End If

        Dim errMessage As String = ""

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        'Dim iglmst As Int64 = GenerateID("QL_trnglmst", cmpcode)
        'Dim igldtl As Int64 = GenerateID("QL_trngldtl", cmpcode)
        'Dim iGlSeq As Int16 = 1

        Try
            'Dim COA_gudang As Integer = 0
            'Dim vargudang As String = GetVarInterface("VAR_GUDANG", cmpcode)
            'If vargudang Is Nothing Or vargudang = "" Then
            '    showMessage("Interface untuk Akun VAR_GUDANG tidak ditemukan!", 2)
            '    objTrans.Rollback()
            '    conn.Close()
            '    Exit Sub
            'Else
            '    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & cmpcode & "' AND ACCTGCODE = '" & vargudang & "'"
            '    xCmd.CommandText = sSql
            '    COA_gudang = xCmd.ExecuteScalar
            '    If COA_gudang = 0 Or COA_gudang = Nothing Then
            '        showMessage("Akun COA untuk VAR_GUDANG tidak ditemukan!", 2)
            '        objTrans.Rollback()
            '        conn.Close()
            '        Exit Sub
            '    End If
            'End If

            'Dim COA_adjust_stock_plus As Integer = 0
            'Dim varadjustplus As String = GetVarInterface("VAR_ADJUST_STOCK+", cmpcode)
            'If varadjustplus Is Nothing Or varadjustplus = "" Then
            '    showMessage("Interface untuk Akun VAR_ADJUST_STOCK+ tidak ditemukan!", 2)
            '    objTrans.Rollback()
            '    conn.Close()
            '    Exit Sub
            'Else
            '    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & cmpcode & "' AND ACCTGCODE = '" & varadjustplus & "'"
            '    xCmd.CommandText = sSql
            '    COA_adjust_stock_plus = xCmd.ExecuteScalar
            '    If COA_adjust_stock_plus = 0 Or COA_adjust_stock_plus = Nothing Then
            '        showMessage("Akun COA untuk VAR_ADJUST_STOCK+ tidak ditemukan!", 2)
            '        objTrans.Rollback()
            '        conn.Close()
            '        Exit Sub
            '    End If
            'End If

            'Dim COA_adjust_stock_minus As Integer = 0
            'Dim varadjustmin As String = GetVarInterface("VAR_ADJUST_STOCK-", cmpcode)
            'If varadjustmin Is Nothing Or varadjustmin = "" Then
            '    showMessage("Interface untuk Akun VAR_ADJUST_STOCK- tidak ditemukan!", 2)
            '    objTrans.Rollback()
            '    conn.Close()
            '    Exit Sub
            'Else
            '    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & cmpcode & "' AND ACCTGCODE = '" & varadjustmin & "'"
            '    xCmd.CommandText = sSql
            '    COA_adjust_stock_minus = xCmd.ExecuteScalar
            '    If COA_adjust_stock_minus = 0 Or COA_adjust_stock_minus = Nothing Then
            '        showMessage("Akun COA untuk VAR_ADJUST_STOCK- tidak ditemukan!", 2)
            '        objTrans.Rollback()
            '        conn.Close()
            '        Exit Sub
            '    End If
            'End If

            'upduser.Text = Session("UserID")
            'updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)

            'Dim conmtroid As Int32 = GenerateID("QL_conmtr", cmpcode)
            'Dim crdmatoid As Int32 = GenerateID("QL_crdmtr", cmpcode)
            'Dim tempoid As Int32 = 0

            ''save to acctg
            ''//////INSERT INTO TRN GL MST

            'sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime) VALUES ('" & cmpcode & "', " & iglmst & ", CURRENT_TIMESTAMP, '" & periodene & "', 'AS', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()

            'sSql = "UPDATE QL_mstoid SET lastoid = " & iglmst & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & cmpcode & "'"
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()

            If Not Session("stockadj") Is Nothing Then
                Dim objTable As DataTable
                objTable = Session("stockadj")

                For c1 As Int16 = 0 To objTable.Rows.Count - 1

                    If objTable.Rows(c1).Item("saldoawal") <> 0 Then

                        ''update kartu stock 
                        'sSql = "UPDATE QL_crdmtr SET qtyadjin = qtyadjin + " & IIf(objTable.Rows(c1).Item("saldoawal") >= 0, objTable.Rows(c1).Item("saldoawal"), 0) & ", qtyadjout = qtyadjout + " & IIf(objTable.Rows(c1).Item("saldoawal") < 0, -1 * objTable.Rows(c1).Item("saldoawal"), 0) & ", saldoakhir = saldoakhir + " & objTable.Rows(c1).Item("saldoawal") & ", lastTranstype = 'AS', lastTrans = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & periodene & "' and mtrlocoid = " & objTable.Rows(c1).Item("genoid") & " and refoid = " & objTable.Rows(c1).Item("itemoid") & " and refname = '" & ddlJenis.SelectedValue & "'"
                        'xCmd.CommandText = sSql

                        'If xCmd.ExecuteNonQuery() <= 0 Then
                        '    'kalau belum ada insertkan baru
                        '    tempoid = tempoid + 1

                        '    sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) values ('" & cmpcode & "', " & crdmatoid & ", '" & periodene & "', " & objTable.Rows(c1).Item("itemoid") & ", '" & ddlJenis.SelectedValue & "', " & objTable.Rows(c1).Item("genoid") & ", 0, 0, " & IIf(objTable.Rows(c1).Item("saldoawal") >= 0, objTable.Rows(c1).Item("saldoawal"), 0) & ", " & IIf(objTable.Rows(c1).Item("saldoawal") < 0, -1 * objTable.Rows(c1).Item("saldoawal"), 0) & ", 0, " & objTable.Rows(c1).Item("saldoawal") & ", 0, 'AS', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', '') "

                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        '    crdmatoid += 1
                        'End If


                        ''update stock inventory(-)( biaya(D)-persediaan(C))
                        'If objTable.Rows(c1).Item("saldoawal") < 0 Then

                        '    '--------------------------
                        '    Dim lasthpp As Double = 0
                        '    sSql = "select hpp  from ql_mstitem where itemoid=" & objTable.Rows(c1).Item("itemoid") & " "
                        '    xCmd.CommandText = sSql
                        '    lasthpp = xCmd.ExecuteScalar
                        '    '---------------------------

                        '    sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & cmpcode & "', " & conmtroid & ", 'AS', CURRENT_TIMESTAMP, '" & periodene & "', 'ADJUSMENT STOCK', 0, '', " & objTable.Rows(c1).Item("itemoid") & ", '" & ddlJenis.SelectedValue & "', " & objTable.Rows(c1).Item("unitoid") & ", " & objTable.Rows(c1).Item("genoid") & ", 0, " & -1 * objTable.Rows(c1).Item("saldoawal") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & person.SelectedValue & ", 0, '" & Tchar(objTable.Rows(c1).Item("note")) & "', " & ToDouble(lasthpp) & ")"
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        '    conmtroid += 1

                        '    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & cmpcode & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_adjust_stock_minus & ", 'D', 0, '', 'Barcode STOCK', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        '    igldtl += 1
                        '    iGlSeq += 1

                        '    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & cmpcode & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_gudang & ", 'C', 0, '', 'Barcode STOCK', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        '    igldtl += 1
                        '    iGlSeq += 1

                        'End If

                        'jika ada stock yg positif(persediaan(D), pendapatan(C)
                        If objTable.Rows(c1).Item("saldoawal") > 0 Then
                            Dim seq As Integer = 0
                            'print begin
                            '--------------------------
                            Dim itemcode As String = ""
                            sSql = "select itembarcode3  from ql_mstitem where itemoid=" & objTable.Rows(c1).Item("itemoid") & " "
                            xCmd.CommandText = sSql
                            itemcode = xCmd.ExecuteScalar

                            Dim itemdesc As String = ""
                            sSql = "select itemdesc  from ql_mstitem where itemoid=" & objTable.Rows(c1).Item("itemoid") & " "
                            xCmd.CommandText = sSql
                            itemdesc = xCmd.ExecuteScalar

                            Dim itempriceunit3 As String = ""
                            sSql = "select merk  from ql_mstitem where itemoid=" & objTable.Rows(c1).Item("itemoid") & " "
                            xCmd.CommandText = sSql
                            itempriceunit3 = xCmd.ExecuteScalar
                            For Cprint As Int16 = 0 To objTable.Rows(c1).Item("saldoawal") - 1
                                seq = seq + 1
                                '---------------------------

                                Try
                                    'Dim itemcode As String = txtNoTanda.Text.Trim
                                    'Dim barcode As String = txtBarcode.Text.Trim
                                    'Dim tglmasuk As Date = Date.ParseExact(txtTglTerima.Text, "dd/MM/yyyy", Nothing, Nothing)
                                    'Dim tglmasukstr As String = Format(tglmasuk, "dd/MM/yy")

                                    Dim appPath As String = Request.PhysicalApplicationPath

                                    Dim barcodetmpPath As String = appPath & "Barcode_Jpt_2.txt"
                                    Dim tmpbarcodePath As String = appPath & "Spooltmp\" & itemcode.Replace("/", "") & "" & seq & ".txt"
                                    Dim newbarcodePath As String = appPath & "Spool\" & itemcode.Replace("/", "") & "" & seq & ".txt"

                                    Dim textline As String = ""
                                    Dim i As Integer = 0
                                    Dim strreader As New StreamReader(barcodetmpPath)
                                    While strreader.Peek <> -1
                                        textline &= strreader.ReadLine
                                        If i = 2 Then
                                            textline &= itempriceunit3
                                        ElseIf i = 3 Then
                                            textline &= itemdesc
                                        ElseIf i = 4 Then
                                            textline &= itemcode
                                        ElseIf i = 5 Then
                                            textline &= itemcode
                                        ElseIf i = 6 Then
                                            textline &= itempriceunit3
                                        ElseIf i = 7 Then
                                            textline &= itemdesc
                                        ElseIf i = 8 Then
                                            textline &= itemcode
                                        ElseIf i = 9 Then
                                            textline &= itemcode
                                        End If
                                        textline &= vbNewLine

                                        i = i + 1
                                    End While

                                    Dim strwriter As StreamWriter = File.CreateText(tmpbarcodePath)
                                    strwriter.Write(textline)
                                    strwriter.Flush()
                                    strwriter.Close()

                                    File.Copy(tmpbarcodePath, newbarcodePath, True)
                                Catch ex As Exception
                                    showMessage(ex.ToString, 2)
                                End Try


                                ''update stock inventory(+)
                                'sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & cmpcode & "', " & conmtroid & ", 'AS', CURRENT_TIMESTAMP, '" & periodene & "', 'ADJUSMENT STOCK', 0, '', " & objTable.Rows(c1).Item("itemoid") & ", '" & ddlJenis.SelectedValue & "', " & objTable.Rows(c1).Item("unitoid") & ", " & objTable.Rows(c1).Item("genoid") & ", " & objTable.Rows(c1).Item("saldoawal") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & person.SelectedValue & ", 0, '" & Tchar(objTable.Rows(c1).Item("note")) & "', " & ToDouble(lasthpp) & ")"
                                'xCmd.CommandText = sSql
                                'xCmd.ExecuteNonQuery()
                                'conmtroid += 1

                                'sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & cmpcode & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_gudang & ", 'D', 0, '', 'Barcode STOCK', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                'xCmd.CommandText = sSql
                                'xCmd.ExecuteNonQuery()
                                'igldtl += 1
                                'iGlSeq += 1

                                'sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & cmpcode & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_adjust_stock_plus & ", 'C', 0, '', 'Barcode STOCK', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                'xCmd.CommandText = sSql
                                'xCmd.ExecuteNonQuery()
                                'igldtl += 1
                                'iGlSeq += 1


                            Next


                        End If
                    End If

                Next

                'sSql = "update QL_mstoid set lastoid = " & conmtroid - 1 & " where tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "' "
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()

                'sSql = "update QL_mstoid set lastoid = " & crdmatoid - 1 & " where tablename = 'QL_crdmtr' and cmpcode = '" & cmpcode & "' "
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()

                'sSql = "update QL_mstoid set lastoid = " & igldtl - 1 & " where tablename = 'QL_trngldtl' and cmpcode = '" & cmpcode & "' "
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()

                'objTrans.Commit()
                'conn.Close()

                Session("oid") = Nothing
                Session("stockadj") = Nothing
                ViewState("redirect") = 1
                showMessage("Barcode berhasil dilakukan !", 2)
                'Response.Redirect("trnBarcode.aspx?awal=true")
            Else
                showMessage("Missing session detail !", 2)
                objTrans.Rollback()
                conn.Close()
                Exit Sub
            End If

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub GVmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        If Not Session("stockadj") Is Nothing Then
            Dim gvr As GridViewRow
            Dim tbqty As System.Web.UI.WebControls.TextBox
            Dim tbnote As System.Web.UI.WebControls.TextBox
            Dim labelitemoid As System.Web.UI.WebControls.Label
            Dim labelgenoid As System.Web.UI.WebControls.Label
            Dim qty As Double = 0.0
            Dim dtable As DataTable = Session("stockadj")
            Dim dview As DataView = dtable.DefaultView

            For i As Int32 = 0 To gvMst.Rows.Count - 1
                dview.RowFilter = ""

                gvr = gvMst.Rows(i)
                tbqty = gvr.FindControl("newqty")
                tbnote = gvr.FindControl("note")
                labelitemoid = gvr.FindControl("labelitemoid")
                labelgenoid = gvr.FindControl("labelgenoid")

                If tbqty.Text <> "" And Double.TryParse(tbqty.Text, qty) = True Then
                    dview.RowFilter = "itemoid = " & Integer.Parse(labelitemoid.Text) & " AND genoid = " & Integer.Parse(labelgenoid.Text) & ""

                    dview(0).BeginEdit()
                    dview(0).Item("saldoawal") = qty
                    dview(0).Item("note") = tbnote.Text.Trim
                    dview(0).EndEdit()
                Else
                    dview.RowFilter = "itemoid = " & Integer.Parse(labelitemoid.Text) & " AND genoid = " & Integer.Parse(labelgenoid.Text) & ""

                    dview(0).BeginEdit()
                    dview(0).Item("saldoawal") = 0
                    dview(0).EndEdit()
                End If
            Next

            dview.RowFilter = ""
            dtable.AcceptChanges()

            gvMst.PageIndex = e.NewPageIndex
            gvMst.DataSource = dtable
            gvMst.DataBind()
            Session("stockadj") = dtable
        Else
            showMessage("@#!&%%@! Missing stock session ! **&#^%^*!", 2)
        End If
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        CProc.SetModalPopUpExtender(btnExtender, PanelErrMsg, MPEErrMsg, False)
        If ViewState("redirect") = 1 Then
            Response.Redirect("trnBarcode.aspx?awal=true")
        End If
    End Sub
#End Region

End Class
