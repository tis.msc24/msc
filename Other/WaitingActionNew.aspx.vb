' Catatan Penting : Jika ada penambahan Waiting Action maka hal-hal yang harus dilakukan :
' 1. Meng-update Procedure InitMenuApp()
' 2. Menambahkan Region Transaksi yang dimaksud
' 3. Meng-update Procedure SetDataList()
' 4. Meng-update Procedure ApprovalAction(sAction As String)
' 5. Meng-update Event gvList_SelectedIndexChanged(sender AS Object, e As System.EventArgs)

Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Other_WaitingActionNew
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterShort")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Const iRoundDigit = 2
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", CompnyName & " - WARNING", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", CompnyName & " - WARNING", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", CompnyName & " - WARNING", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetNumericValues(ByVal sData As String) As String
        Dim sNumeric As String = ""
        Dim sCurr As String = ""
        For C1 As Integer = 1 To Len(sData)
            sCurr = Mid(sData, C1, 1)
            If IsNumeric(sCurr) Then
                sNumeric &= sCurr
            End If
        Next
        GetNumericValues = sNumeric
    End Function

    Private Function FillDDLWithAll(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        FillDDLWithAll = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("All")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "All"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithAll = False
        End If
        Return FillDDLWithAll
    End Function

    Private Function SetBusUnit(ByVal sCode As String) As String
        sSql = "SELECT DISTINCT cmpcode from ql_mstcust"
        Return GetStrData(sSql)
    End Function

    Private Function GetSelectedID() As String
        Dim sResult As String = ""
        For C1 As Integer = 0 To gvList2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvList2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                            sResult &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
                        End If
                    End If
                Next
            End If
        Next
        If sResult <> "" Then
            sResult = Left(sResult, sResult.Length - 1)
        End If
        Return sResult
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub InitMenuApp()
        sSql = "SELECT COUNT(*) AS jmlapp, tablename FROM QL_approval WHERE approvaluser='" & Session("UserID") & "' AND statusrequest='New' AND event='In Approval' GROUP BY tablename"
        Dim dtApp As DataTable = ckon.ambiltabel(sSql, "QL_approval")
        Dim dvApp As DataView = dtApp.DefaultView
        If dvApp.Count > 0 Then
            Dim arColNameList() As String = {"apptype", "appsept", "appcount"}
            Dim arColTypeList() As String = {"System.String", "System.String", "System.Int32"}
            Dim dtList As DataTable = SetTableDetail("tblAppList", arColNameList, arColTypeList)
            Dim drList As DataRow
            Dim arColNameMenu() As String = {"appname", "tablename"}
            Dim arColTypeMenu() As String = {"System.String", "System.String"}
            Dim dtMenu As DataTable = SetTableDetail("tblAppMenu", arColNameMenu, arColTypeMenu)
            Dim drMenu As DataRow
            updateuser.Text = ""
            ' HR Return
            dvApp.RowFilter = "tablename = 'QL_trnnotahrretur' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Nota HR Return" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Nota HR Return" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If

            'dvApp.RowFilter = ""
            dvApp.RowFilter = "tablename = 'QL_TRNREQUESTITEM' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Permintaan Barang" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Permintaan Barang" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            gvListApp.DataSource = dtList
            gvListApp.DataBind()
            gvMenu.DataSource = dtMenu
            gvMenu.DataBind()
        End If
    End Sub

    Private Sub SetDataList()
        MultiView1.SetActiveView(ViewList)
        gvList.DataSource = Nothing
        gvList.DataBind()
        gvList.Columns.Clear()
        Dim field As New System.Web.UI.WebControls.CommandField
        field.ShowSelectButton = True
        field.HeaderStyle.CssClass = "gvhdr"
        field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
        field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
        gvList.Columns.Add(field)
        TDDept1.Visible = False : TDDept2.Visible = False : TDDept3.Visible = False
        gvList.Visible = True : gvList2.Visible = False
        If lblAppType.Text = "QL_TRNNOTAHRRETUR" Then
            SetGVListHRRet()
        End If
        If lblAppType.Text = "QL_TRNREQUESTITEM" Then
            SetGVListPb()
        ElseIf lblAppType.Text.ToUpper = "QL_TRNFORECASTMST" Then
            SetGVListFC()
        End If
        rowVal.Text = gvList.Rows.Count
        btnClose.Visible = gvList2.Visible
    End Sub

    Private Sub SetDataListMulti()
        gvList2.DataSource = Nothing
        gvList2.DataBind()
        If gvList2.Columns.Count > 1 Then
            For C1 As Integer = 1 To gvList2.Columns.Count - 1
                gvList2.Columns.RemoveAt(1)
            Next
        End If
        If lblAppType.Text.ToUpper = "QL_TRNREGISTERMST" Then
            'SetGVListMRReg()
        End If
    End Sub

    Private Sub ApprovalActionMulti()
        If lblAppType.Text.ToUpper = "QL_TRNREGISTERMST" Then
            'UpdateMRReg(GetSelectedID())
        End If
    End Sub

    Private Sub ApprovalAction(ByVal sAction As String)
        If lblAppType.Text = "QL_TRNNOTAHRRETUR" Then
            If sAction = "Approved" Then
                UpdateHRRet(GenerateHRRetNo(), sAction)
            Else
                UpdateHRRet("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNREQUESTITEM" Then
            If sAction = "Approved" Then
                UpdatePb("", sAction)
            Else
                UpdatePb("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNFORECASTMST" Then
            If sAction = "Approved" Then
                UpdateForeCast("", sAction)
            Else
                UpdateForeCast("", sAction)
            End If
        End If
    End Sub

    Private Sub InitDDL() 
        sSql = "SELECT DISTINCT cmpcode, cmpcode from ql_mstcust"
        If CompnyCode <> CompnyCode Then
            sSql &= " AND cmpcode='" & CompnyCode & "'"
        End If 
    End Sub

    Private Sub InitDDLUser(ByVal sName As String)
        'Fill DDL Request User
        Dim sWhere As String = ""
        If sName <> "" Then
            sName = sName.Trim.Replace("� ", "")
            sWhere &= " AND rold.formname LIKE '%" & Tchar(sName) & "%' "
        End If
        sSql = "SELECT DISTINCT prof.USERID, prof.USERID FROM QL_mstprof prof INNER JOIN QL_userrole urol ON urol.USERPROF=prof.USERID WHERE prof.STATUSPROF='ACTIVE' AND roleoid IN (SELECT DISTINCT rolm.roleoid FROM QL_role rolm INNER JOIN QL_roledtl rold ON rold.roleoid=rolm.roleoid WHERE rold.formtype='TRANSACTION' " & sWhere & " )"
        FillDDLWithAll(DDLUser, sSql)
    End Sub  

    Private Sub StockAdjSumTotalIDR()
        If Session("TblDtlMSA") IsNot Nothing Then
            Dim tValueIDR As Double = 0
            Dim dt As DataTable = Session("TblDtlMSA")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvDataAdj.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvDataAdj.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(9).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            dv.RowFilter = "oid=" & CType(myControl, System.Web.UI.WebControls.TextBox).ToolTip
                            Dim dValue As Double = 0
                            dValue = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                            If dv.Count > 0 Then
                                tValueIDR = tValueIDR + (ToDouble(dt.Rows(C1)("adjqty").ToString) * ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text))  'ToDouble(dtTmp.Rows(C1)("stockvalueidr").ToString))
                            End If
                            dv.RowFilter = ""
                        End If
                    Next
                End If
            Next
            TotalValueIDR.Text = ToMaskEdit(tValueIDR, 2)
        End If
    End Sub

    Public Sub GenerateBulan(ByVal bln1 As String, ByVal note As String)
        If bln1 = "1" Then
            If note = "satu" Then
                lblbln1.Text = "Januari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Januari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Januari"
            End If
        ElseIf bln1 = "2" Then
            If note = "satu" Then
                lblbln1.Text = "Februari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Februari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Februari"
            End If
        ElseIf bln1 = "3" Then
            If note = "satu" Then
                lblbln1.Text = "Maret"
            ElseIf note = "dua" Then
                lblbln2.Text = "Maret"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Maret"
            End If
        ElseIf bln1 = "4" Then
            If note = "satu" Then
                lblbln1.Text = "April"
            ElseIf note = "dua" Then
                lblbln2.Text = "April"
            ElseIf note = "tiga" Then
                lblbln3.Text = "April"
            End If
        ElseIf bln1 = "5" Then
            If note = "satu" Then
                lblbln1.Text = "Mei"
            ElseIf note = "dua" Then
                lblbln2.Text = "Mei"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Mei"
            End If
        ElseIf bln1 = "6" Then
            If note = "satu" Then
                lblbln1.Text = "Juni"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juni"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juni"
            End If
        ElseIf bln1 = "7" Then
            If note = "satu" Then
                lblbln1.Text = "Juli"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juli"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juli"
            End If
        ElseIf bln1 = "8" Then
            If note = "satu" Then
                lblbln1.Text = "Agustus"
            ElseIf note = "dua" Then
                lblbln2.Text = "Agustus"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Agustus"
            End If
        ElseIf bln1 = "9" Then
            If note = "satu" Then
                lblbln1.Text = "September"
            ElseIf note = "dua" Then
                lblbln2.Text = "September"
            ElseIf note = "tiga" Then
                lblbln3.Text = "September"
            End If
        ElseIf bln1 = "10" Then
            If note = "satu" Then
                lblbln1.Text = "Oktober"
            ElseIf note = "dua" Then
                lblbln2.Text = "Oktober"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Oktober"
            End If
        ElseIf bln1 = "11" Then
            If note = "satu" Then
                lblbln1.Text = "November"
            ElseIf note = "dua" Then
                lblbln2.Text = "November"
            ElseIf note = "tiga" Then
                lblbln3.Text = "November"
            End If
        ElseIf bln1 = "12" Then
            If note = "satu" Then
                lblbln1.Text = "Desember"
            ElseIf note = "dua" Then
                lblbln2.Text = "Desember"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Desember"
            End If
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = CompnyCode
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim Position As String = Session("Position")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            CompnyCode = cmpcode
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("Type") = ""
            Session("Position") = Position
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Other\WaitingActionNew.aspx")
        End If
        Page.Title = CompnyName & " - Waiting Action"
        Session("directappvalue") = Request.QueryString("formvalue")
        Session("directapptext") = Request.QueryString("formtext")

        btnApprove.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to APPROVE this Transaction ?');")
        btnRevise.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to REVISE this Transaction ?');")
        btnReject.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to REJECT this Transaction ?');")
        btnClose.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE selected Transaction ?');")

        If Not Page.IsPostBack Then
            InitDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitMenuApp()
            If Not Session("directappvalue") Is Nothing And Session("directappvalue") <> "" And Not Session("directapptext") Is Nothing And Session("directapptext") <> "" Then
                lblAppType.Text = Session("directappvalue")
                lblTitleList.Text = Session("directapptext") & " List :"
                lblTitleData.Text = Session("directapptext") & " Data :"
                InitDDLUser(Session("directapptext"))
                SetDataList()
            End If
            'lkbMenuApp_Click(Session("UserID"), e)
        End If
    End Sub

    Protected Sub lkbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbBack.Click
        'MultiView1.SetActiveView(View1)
        'InitMenuApp()
        Response.Redirect("~\Other\WaitingAction.aspx?awal=true")
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
    End Sub

    Protected Sub lkbMenuApp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblAppType.Text = sender.CommandArgument
        lblTitleList.Text = sender.Text & " List :"
        lblTitleData.Text = sender.Text & " Data :"
        InitDDLUser(sender.Text)
        SetDataList()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If Not Session("TblGVList") Is Nothing Then
            Dim dt As DataTable = Session("TblGVList")
            Dim dv As DataView = dt.DefaultView
            Dim sFilter As String = ""
            If cbPeriod.Checked Then
                If IsValidPeriod() Then
                    sFilter &= "trndate>='" & CDate(toDate(FilterPeriod1.Text)) & "' AND trndate<='" & CDate(toDate(FilterPeriod2.Text)) & "'"
                Else
                    Exit Sub
                End If
            End If
            If DDLUser.SelectedValue <> "All" Then
                sFilter &= IIf(sFilter = "", "", " AND ") & "requestuser='" & DDLUser.SelectedValue & "'"
            End If
            'If DDLBusUnit.SelectedValue <> "" Then
            '    sFilter &= IIf(sFilter = "", "", " AND ") & "cmpcode='" & DDLBusUnit.SelectedValue & "'"
            'End If
            'If lblTitleList.Text.Contains("PR") Then
            '    If cbDept.Checked = True Then
            '        If DDLBusUnit.SelectedValue <> "" Then
            '            sFilter &= IIf(sFilter = "", "", " AND ") & "deptoid='" & DDLDept.SelectedValue & "'"
            '        End If
            '    End If
            'End If
            If FilterDraftNo.Text <> "" Then
                Dim SFilterDraftNo() As String = Split(FilterDraftNo.Text, ",")
                sFilter &= IIf(sFilter = "", "", " AND ") & " ("
                For c1 As Integer = 0 To SFilterDraftNo.Length - 1
                    sFilter &= " trnoid = " & ToDouble(SFilterDraftNo(c1))
                    If c1 < SFilterDraftNo.Length - 1 Then
                        sFilter &= " OR"
                    End If
                Next
                sFilter &= ")"
            End If
            dv.RowFilter = sFilter
            gvList.DataSource = dv.ToTable()
            gvList.DataBind()
            dv.RowFilter = ""
        End If
        rowVal.Text = gvList.Rows.Count
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        cbPeriod.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        DDLUser.SelectedIndex = -1
        'DDLBusUnit.SelectedIndex = -1
        DDLDept.SelectedIndex = -1
        FilterDraftNo.Text = ""
        SetDataList()
    End Sub

    Protected Sub gvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvList.SelectedIndexChanged
        btnApprove.Visible = True : btnReject.Visible = True : btnRevise.Visible = False
        lblRow2.Visible = True : rowVal2.Visible = True : gvData.Visible = True
        lblpqqty.Visible = False : pqqty.Visible = False
        updateuser.Text = ""
        tbRevised.Text = ""
        MultiView1.SetActiveView(ViewData)
        gvData.DataSource = Nothing : gvDataAdj.DataSource = Nothing : gvDataPQ.DataSource = Nothing
        gvData.DataBind() : gvDataAdj.DataBind() : gvDataPQ.DataBind()
        gvData.Columns.Clear() : gvData.Visible = True : gvDataAdj.Visible = False : gvDataPQ.Visible = False
        gvDataFooter.DataSource = Nothing
        gvDataFooter.DataBind()
        Session("trnoid") = gvList.SelectedDataKey.Item("trnoid").ToString
        Session("trndate") = gvList.SelectedDataKey.Item("trndate").ToString
        Session("approvaloid") = gvList.SelectedDataKey.Item("approvaloid").ToString
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaloid=" & Session("approvaloid") & " AND statusrequest='New' AND event='In Approval'"
        If CheckDataExists(sSql) = False Then
            showMessage("Your selected data has been used by another user. Please select another data!", CompnyName & " - WARNING", 2)
            MultiView1.SetActiveView(ViewList)
            Exit Sub
        End If
        AppCmpCode.Text = gvList.SelectedDataKey.Item("cmpcode").ToString
        lblBusUnit.Text = SetBusUnit(AppCmpCode.Text)
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String"}
        Dim dtDataHeader As DataTable = SetTableDetail("tblDataHeader", arColNameList, arColTypeList)
        If lblAppType.Text = "QL_TRNNOTAHRRETUR" Then
            SetGVDataHRRet(dtDataHeader)
            updateuser.Text = gvList.SelectedDataKey.Item("requestuser").ToString
        ElseIf lblAppType.Text = "QL_TRNREQUESTITEM" Then
            SetGVDataPb(dtDataHeader)
            updateuser.Text = gvList.SelectedDataKey.Item("requestuser").ToString
        ElseIf lblAppType.Text = "QL_TRNFORECASTMST" Then
            SetGVDataFC(dtDataHeader)
            updateuser.Text = gvList.SelectedDataKey.Item("requestuser").ToString
        End If
        rowVal2.Text = gvData.Rows.Count
    End Sub

    Protected Sub gvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            For C1 As Int16 = 1 To e.Row.Cells.Count - 1
                If ToDouble(e.Row.Cells(C1).Text) <> 0 Then
                    e.Row.Cells(C1).Text = ToMaskEdit(ToDouble(e.Row.Cells(C1).Text), 4)
                End If
                If e.Row.Cells(C1).Text = "0.0000" Then
                    e.Row.Cells(C1).Text = "0"
                End If
            Next
        End If
    End Sub

    Protected Sub gvDataAdj_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDataAdj.PageIndexChanging
        If Session("TblDtlMSA") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlMSA")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvDataAdj.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvDataAdj.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(9).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            dv.RowFilter = "oid=" & CType(myControl, System.Web.UI.WebControls.TextBox).ToolTip
                            Dim dValue As Double = 0
                            dValue = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                            If dv.Count > 0 Then
                                dv(0)("stockvalueidr") = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)                            
                            End If
                            dv.RowFilter = ""
                        End If
                    Next
                End If
            Next
            dt.AcceptChanges()
            Session("TblDtlMSA") = dt
            gvDataAdj.DataSource = Session("TblDtlMSA")
            gvDataAdj.DataBind()            
        End If
        gvDataAdj.PageIndex = e.NewPageIndex
        gvDataAdj.DataSource = Session("TblDtlMSA")
        gvDataAdj.DataBind()
    End Sub

    Protected Sub gvDataPQ_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDataPQ.PageIndexChanging
        If Session("TblDtlPQ") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlPQ")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvDataPQ.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvDataPQ.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(13).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            dv.RowFilter = "pqdtloid=" & CType(myControl, System.Web.UI.WebControls.TextBox).ToolTip
                            Dim dValue As Double = 0
                            dValue = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                            If dv.Count > 0 Then
                                dv(0)("pqappqty") = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                            End If
                            dv.RowFilter = ""
                        End If
                    Next
                End If
            Next
            dt.AcceptChanges()
            Session("TblDtlPQ") = dt
            gvDataPQ.DataSource = Session("TblDtlPQ")
            gvDataPQ.DataBind()
        End If
        gvDataPQ.PageIndex = e.NewPageIndex
        gvDataPQ.DataSource = Session("TblDtlPQ")
        gvDataPQ.DataBind()
    End Sub

    Protected Sub gvDataAdj_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDataAdj.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub gvDataPQ_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDataPQ.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 4)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 4)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 4)
        End If
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnApprove.Click
        If lblAppType.Text = "QL_TRNWOCLOSE" Then
            If btnViewOutTrans.Visible Then
                showMessage("This data still has some outstanding data that must be processed. Please check in the View Report!", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
        End If
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaloid=" & Session("approvaloid") & " AND statusrequest='New' AND event='In Approval'"
        If CheckDataExists(sSql) = False Then
            showMessage("This data has been confirmed by another user. Please select another data!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If 
        ApprovalAction("Approved")
    End Sub

    Protected Sub btnRevise_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRevise.Click
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaloid=" & Session("approvaloid") & " AND statusrequest='New' AND event='In Approval'"
        If CheckDataExists(sSql) = False Then
            showMessage("This data has been confirmed by another user. Please select another data!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        'If updateuser.Text <> "" Then
        '    SendEmail("Revised")
        'End If
        ApprovalAction("Revised")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnReject.Click
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaloid=" & Session("approvaloid") & " AND statusrequest='New' AND event='In Approval'"
        If CheckDataExists(sSql) = False Then
            showMessage("This data has been confirmed by another user. Please select another data!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If 
        ApprovalAction("Rejected")
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
        SetDataList()
        If gvList.Rows.Count = 0 Then
            Response.Redirect("~\Other\WaitingActionNew.aspx?awal=true")
        Else
            gvList.SelectedIndex = -1
        End If
    End Sub

    Protected Sub cbCheckHdr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvList2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvList2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        ApprovalActionMulti()
    End Sub

    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnInsert.Click
        If tbValue.Text <> "" Then
            If Session("TblDtlMSA") IsNot Nothing Then
                Dim dt As DataTable = Session("TblDtlMSA")
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(C1)("stockvalueidr") = ToDouble(tbValue.Text)
                Next
                dt.AcceptChanges()
                Session("TblDtlMSA") = dt
                gvDataAdj.DataSource = Session("TblDtlMSA")
                gvDataAdj.DataBind()
                StockAdjSumTotalIDR()
            End If
        End If
    End Sub

    Protected Sub btnViewOutTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewOutTrans.Click
        'PrintOutstandingTrans()
    End Sub
#End Region

#Region "Nota HR Return Approval"
    Private Sub SetGVListHRRet()
        Dim arHeader() As String = {"Draft No.", "Return Date", "Return Type", "Supplier", "Return Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "hrreturtype", "suppname", "hrreturnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "hrreturnote", "hrreturtype", "requestuser", "trnnotahrno", "trnnotahramtnetto", "trnnotahracumamt", "returamt", "returpi", "saldohr"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1) : field.SortExpression = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        'sSql = "SELECT trnnotahrreturtype FROM QL_trnnotahrretur where trnnotahrreturoid = " & Session("trnoid") & " and trnnotahrreturstatus = 'In Approval'"
        'Dim hrtype As String = GetStrData(sSql)
        'If hrtype = "HR" Then
        sSql = "SELECT hr.cmpcode, trnnotahrreturoid AS trnoid, trnnotahrreturno, CONVERT(VARCHAR(10), trnnotahrreturdate, 103) AS trndate, suppname, trnnotahrreturnote AS hrreturnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 103) AS requestdate, trnnotahrreturtype AS hrreturtype, hrm.trnnotahrno, hr.suppoid, hrm.trnnotahramtnetto - (hrm.trnnotahracumamt + hrm.trnnotahrreturamt) saldohr, hrm.trnnotahramtnetto, hrm.trnnotahracumamt, hrm.trnnotahrreturamt returamt,0 returpi, hrm.trnnotahroid notahroid, hr.trnnotahrreturnote noteretur FROM QL_trnnotahrretur hr LEFT JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hr.trnnotahroid INNER JOIN QL_approval ap ON hr.trnnotahrreturoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstsupp s ON s.suppoid=hr.suppoid WHERE ap.event='In Approval' AND ap.tablename='QL_trnnotahrretur' AND hr.trnnotahrreturstatus='In Approval' AND ap.approvaluser='" & Session("UserID") & "' and hr.trnnotahrreturtype = 'HR' UNION ALL " & _
        "SELECT cmpcode, trnoid, trnnotahrreturno, trndate, suppname, hrreturnote, APPROVALOID, requestuser, requestdate, hrreturtype, trnnotahrno, suppoid, SUM(saldohr) saldohr, SUM(trnnotahramtnetto) trnnotahramtnetto, SUM(trnnotahracumamt) trnnotahracumamt, SUM(returhr) returamt, SUM(returpi) returpi, notahroid, noteretur FROM (" & _
"SELECT hr.cmpcode, hr.trnnotahrreturoid AS trnoid, trnnotahrreturno, CONVERT(VARCHAR(10), trnnotahrreturdate, 103) AS trndate, suppname, trnnotahrreturnote AS hrreturnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 103) AS requestdate, trnnotahrreturtype AS hrreturtype, '' trnnotahrno, hr.suppoid , (ISNULL((SELECT SUM(bd.amtdisc2) FROM QL_trnbelimst bm INNER JOIN QL_trnbelidtl bd ON bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code where bd.trnbelimstoid = hrd.trnbelimstoid and bd.amtdisc2 > 0),0) - ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hrm.trnnotahrstatus = 'POST'),0.0) - ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0) + ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'HR'),0.0)) saldohr, ISNULL((SELECT SUM(bd.amtdisc2) FROM QL_trnbelimst bm INNER JOIN QL_trnbelidtl bd ON bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code where bd.trnbelimstoid = hrd.trnbelimstoid and bd.amtdisc2 > 0),0) AS trnnotahramtnetto, ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hrm.trnnotahrstatus = 'POST'),0.0) trnnotahracumamt, ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'HR'),0.0) returhr, ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0)  returpi, 0 notahroid, hr.trnnotahrreturnote noteretur FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid = hrd.trnbelimstoid INNER JOIN QL_approval ap ON hr.trnnotahrreturoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstsupp s ON s.suppoid=hr.suppoid WHERE ap.event='In Approval' AND ap.tablename='QL_trnnotahrretur' AND hr.trnnotahrreturstatus='In Approval' AND ap.approvaluser='" & Session("UserID") & "' and hr.trnnotahrreturtype = 'PI' GROUP BY hr.cmpcode, hr.trnnotahrreturoid, trnnotahrreturno,trnnotahrreturdate,suppname, trnnotahrreturnote, ap.approvaloid, ap.requestuser,ap.requestdate,trnnotahrreturtype, hr.suppoid, hrd.trnbelimstoid, bm.trnbelimstoid " & _
") HR GROUP BY cmpcode, trnoid, trnnotahrreturno, trndate, suppname, hrreturnote, APPROVALOID, requestuser, requestdate, hrreturtype, trnnotahrno, suppoid,notahroid, noteretur /*ORDER BY CAST(requestdate AS DATETIME) DESC*/"
            'End If
        FillGV(gvList, sSql, "QL_trnnotahrretur")
        Session("TblGVList") = ckon.ambiltabel(sSql, "QL_trnnotahrretur")
    End Sub

    Private Sub SetGVDataHRRet(ByVal dtDataHeader As DataTable)
        'DDLReturnType.SelectedValue = gvList.SelectedDataKey.Item("hrreturtype").ToString
        sSql = "SELECT trnnotahrreturtype FROM QL_trnnotahrretur where trnnotahrreturoid = " & Session("trnoid") & " and trnnotahrreturstatus = 'In Approval'"
        Dim hrtype As String = GetStrData(sSql)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Retur Type", "Return Date", "Supplier", "No. Nota HR", IIf(hrtype = "HR", "HR Amount", "PI Amount"), IIf(hrtype = "HR", "HR Accum", "PI Accum"), "HR Return", "PI Return", IIf(hrtype = "HR", "HR Balance", "PI Balance"), "Return Note"}
        Dim arValue() As String = {"trnoid", "hrreturtype", "trndate", "suppname", "trnnotahrno", "trnnotahramtnetto", "trnnotahracumamt", "returamt", "returpi", "saldohr", "hrreturnote"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "D", "D", "D", "D", "D", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", IIf(hrtype = "HR", "N", "Y"), "N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), iRoundDigit)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Nota PI", "Supplier", "PI Amt", "Retur Amt"}
        Dim arField() As String = {"seq", "notahr", "suppname", "notahramt", "returamt"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT hrd.trnnotahrreturdtlseq as seq, 0 notahroid, bm.trnbelino notahr, s.suppoid, s.suppname,hrd.trnbelidtlamt notahramt, hrd.trnnotahrreturdtlamt returamt, hrd.trnnotahrreturdtlnote AS noteretur, hrd.trnbelimstoid FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid = hrd.trnbelimstoid INNER JOIN QL_mstsupp s ON s.suppoid = bm.trnsuppoid WHERE hr.trnnotahrreturoid=" & Session("trnoid") & " order by trnnotahrreturdtlseq"
        Session("TblDtlHRRet") = ckon.ambiltabel(sSql, "QL_trnnotahrretur")
        gvData.DataSource = Session("TblDtlHRRet")
        gvData.DataBind()
    End Sub

    Private Function GenerateHRRetNo() As String
        Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & Session("branch_id") & "' AND gengroup='CABANG'")
        Dim sNo As String = "HRR/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(trnnotahrreturno,'" & sNo & "',''))),0)+1 FROM QL_trnnotahrretur WHERE trnnotahrreturno LIKE '" & sNo & "%' and branch_code='" & Session("branch_id") & "'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), 4)
        Return sNo
    End Function

    Private Sub UpdateHRRet(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Silahkan Pilih Nota HR Return Terlebih Dahulu", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Silahkan Pilih Nota HR Return Terlebih Dahulu", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim glmstoid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim gldtloid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim conhroid As Integer = GenerateID("QL_conhr", CompnyCode)
        Dim payhroid As Integer = GenerateID("QL_trnpayhrmst", CompnyCode)
        Dim payhrdtloid As Integer = GenerateID("QL_trnpayhrdtl", CompnyCode)
        Dim iDebetAcctgOid, iCreditAcctgOid, iPIRAcctgOid As Integer
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        If sStatus = "Approved" Then
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_HR", AppCmpCode.Text) Then
                sVarErr = "VAR_HR"
            End If
            If Not IsInterfaceExists("VAR_HRS", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_HRS"
            End If
            If Not IsInterfaceExists("VAR_PI_HRRETUR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PI_HRRETUR"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iDebetAcctgOid = GetAcctgOID(GetVarInterface("VAR_HRS", Session("branch_id")), CompnyCode)
            iCreditAcctgOid = GetAcctgOID(GetVarInterface("VAR_HR", Session("branch_id")), CompnyCode)
            iPIRAcctgOid = GetAcctgOID(GetVarInterface("VAR_PI_HRRETUR", Session("branch_id")), CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Sales Return
            sSql = "UPDATE QL_trnnotahrretur SET trnnotahrreturdate = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), trnnotahrreturno='" & sNo & "', trnnotahrreturstatus='" & sStatus & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP "
            sSql &= " WHERE trnnotahrreturstatus='In Approval' AND trnnotahrreturoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnnotahrretur' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then

                ElseIf sStatus = "Approved" Then
                    Dim dt As DataTable = Session("TblDtlHRRet")
                    Dim dHRAmtIDR As Double = ToDouble(dt.Compute("SUM(returamt)", "").ToString)
                    sSql = "SELECT trnnotahroid FROM QL_trnnotahrretur where trnnotahrreturoid = " & Session("trnoid") & ""
                    xCmd.CommandText = sSql : Dim hroid As Integer = xCmd.ExecuteScalar
                    If DDLReturnType.SelectedValue.ToUpper = "HR" Then
                        sSql = "UPDATE QL_trnnotahrmst SET trnnotahrreturamt = trnnotahrreturamt + " & dHRAmtIDR & ", trnnotahrreturamtidr = trnnotahrreturamtidr + " & dHRAmtIDR & " WHERE trnnotahroid = " & hroid & ""
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If

                    ' Insert QL_trnglmst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, branch_code, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type) VALUES ('" & AppCmpCode.Text & "', " & glmstoid & ", '" & Session("branch_id") & "', '" & sDate & "', '" & sPeriod & "', 'Nota HR Return|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'HR Return')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    If DDLReturnType.SelectedValue.ToUpper = "HR" Then
                        If dHRAmtIDR > 0 Then
                            ' Insert QL_trngldtl
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, createuser, createtime) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", '" & Session("branch_id") & "', 1, " & glmstoid & ", " & iDebetAcctgOid & ", 'D', " & dHRAmtIDR & ", '" & sNo & "', 'Nota HR Return|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dHRAmtIDR & ", 0, 'QL_trnnotahrretur " & Session("trnoid") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, createuser, createtime) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", '" & Session("branch_id") & "', 2, " & glmstoid & ", " & iCreditAcctgOid & ", 'C', " & dHRAmtIDR & ", '" & sNo & "', 'Nota HR Return|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dHRAmtIDR & ", 0, 'QL_trnnotahrretur " & Session("trnoid") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            Dim objTable As DataTable = Session("TblGVList")
                            Dim dv As DataView = objTable.DefaultView
                            dv.RowFilter = " hrreturtype = 'HR' and trnoid = " & Session("trnoid") & ""
                            For C1 As Int16 = 0 To dv.Count - 1

                                '--- Insert Conar Untuk Memotong Piutang ---
                                sSql = "INSERT into QL_conhr (cmpcode, conhroid, branch_code, conhrtype, conhrflag, periodacctg, reftype, refoid, refacctgoid, refdate, refamt, refamtidr, refamtusd, payreftype, payrefoid, payacctgoid, payrefdate, payduedate, payrefno, payrefamt, payrefamtidr, payrefamtusd, conhrstatus, conhrnote, conhrres1, conhrres2, crtuser, crttime, upduser, updtime) VALUES" & _
                                " ('" & CompnyCode & "', " & conhroid & ", '" & Session("branch_id") & "', 'RETHR', '', '" & sPeriod & "', 'QL_trnnotahrmst', " & dv.Item(C1)("notahroid") & ", " & iCreditAcctgOid & ", '01/01/1900', 0, 0, 0, 'QL_trnpayhrdtl', " & payhroid & ", 0, " & sDate & ", '01/01/1900', '', " & ToDouble(dv.Item(C1)("returamt")) & ", " & ToDouble(dv.Item(C1)("returamt")) & ", " & ToDouble(dv.Item(C1)("returamt")) & ", 'POST', '" & Tchar(dv.Item(C1)("noteretur").ToString) & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                '--- Insert Payar Untuk Memotong Piutang ---
                                sSql = "insert into QL_trnpayhrmst (cmpcode, payhrmstoid, cashbankoid, branch_code, payhrno,payhrdate, periodacctg, payhrtype, payhracctgoid, suppoid, payhramt, payhramtidr, payhramtusd, payhrnote, payhrstatus, payhrres1, payhrres2, crtuser, crttime, upduser, updtime) VALUES" & _
                                " ('" & CompnyCode & "', " & payhroid & ", 0, " & Session("branch_id") & ", '" & sNo & "', " & sDate & ", '" & sPeriod & "', 'RETUR', 0, " & dv.Item(C1)("suppoid") & ", " & ToDouble(dv.Item(C1)("returamt")) & ", " & ToDouble(dv.Item(C1)("returamt")) & ", 0, '" & Tchar(dv.Item(C1)("noteretur").ToString) & "', 'POST', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP )"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                '--- Insert Payar Untuk Memotong Piutang ---
                                sSql = "insert into QL_trnpayhrdtl (cmpcode, payhrdtloid, payhrmstoid, cashbankoid, branch_code, payreftype, payrefoid, payflag, payacctgoid, payduedate, payrefno, itemoid, itemqty, itemprice, payamt, payamtidr, payamtusd, paynote, paystatus, payres1, payres2, upduser, updtime) VALUES" & _
                                " ('" & CompnyCode & "', " & payhrdtloid & ", " & payhroid & ", 0, '" & Session("branch_id") & "', 'QL_trnnotahrmst', " & dv.Item(C1)("notahroid") & ", '', 0, '01/01/1900', '', 0, 0, 0, " & ToDouble(dv.Item(C1)("returamt")) & ", " & ToDouble(dv.Item(C1)("returamt")) & ", " & ToDouble(dv.Item(C1)("returamt")) & ", '" & Tchar(dv.Item(C1)("noteretur")) & "', 'POST', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            Next

                            sSql = "UPDATE QL_mstoid SET lastoid=" & conhroid & " WHERE tablename='QL_conhr' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & payhroid & " WHERE tablename='QL_trnpayhrmst' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            sSql = "UPDATE QL_mstoid SET lastoid=" & payhrdtloid & " WHERE tablename='QL_trnpayhrdtl' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Else
                        If dHRAmtIDR > 0 Then
                            ' Insert QL_trngldtl
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, createuser, createtime) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", '" & Session("branch_id") & "', 1, " & glmstoid & ", " & iCreditAcctgOid & ", 'D', " & dHRAmtIDR & ", '" & sNo & "', 'Nota HR Return|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dHRAmtIDR & ", 0, 'QL_trnnotahrretur " & Session("trnoid") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, createuser, createtime) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", '" & Session("branch_id") & "', 2, " & glmstoid & ", " & iPIRAcctgOid & ", 'C', " & dHRAmtIDR & ", '" & sNo & "', 'Nota HR Return|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dHRAmtIDR & ", 0, 'QL_trnnotahrretur " & Session("trnoid") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    UpdateHRRet(sNo, sStatus)
                    Exit Sub
                Else
                    showMessage(exSql.Message, CompnyName & " - ERROR", 1) : Exit Sub
                End If
            Else
                showMessage(exSql.Message, CompnyName & " - ERROR", 1) : Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Permintaan Barang"
    Private Function GetTextBoxHppNya(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvPBdtl.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function UpdateHppNya() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDtlReqItem") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDtlReqItem")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True

                For C1 As Integer = 0 To gvPBdtl.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvPBdtl.Rows(C1)
                    dtView.RowFilter = "seq=" & row.Cells(0).Text
                    Dim hargabeli As Double = ToDouble(GetTextBoxHppNya(C1, 8))
                    dtView(0)("acceptqty") = ToDouble(GetTextBoxHppNya(C1, 8))
                    dtView.RowFilter = ""
                Next

                dtTbl = dtView.ToTable
                Session("TblDtlReqItem") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Public Function GetHppNya() As String
        Return ToMaskEdit(ToDouble(Eval("acceptqty")), 3)
    End Function

    Private Sub SetGVListPb()
        Dim arHeader() As String = {"Draft No.", "Cabang", "tanggal", "Cb. Kirim", "Keterangan", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "gendesc", "trndate", "CbKirim", "trnrequestitemnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "trndate", "gendesc", "trnrequestitemnote", "requestuser", "requestdate", "approvaloid", "branchcode", "CbKirim"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1) : field.SortExpression = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey

        Dim QlFis As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNREQUESTITEM' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeFin As String = ""
        Dim FinCode() As String = QlFis.Split(",")
        For C1 As Integer = 0 To FinCode.Length - 1
            If FinCode(C1) <> "" Then
                sCodeFin &= "'" & FinCode(C1).Trim & "',"
            End If
        Next
        sSql = "SELECT pbm.cmpcode,pbm.trnrequestitemoid trnoid,branchcode,cb.gendesc,trnrequestitemno,Convert(Varchar(20),trnrequestitemdate,103) trndate,trnrequestitemstatus,trnrequestitemnote,ap.REQUESTUSER,Convert(Varchar(20),REQUESTDATE,103) requestdate,ap.approvaloid,ct.gendesc CbKirim FROM QL_trnrequestitem pbm INNER JOIN QL_APPROVAL ap ON OID=pbm.trnrequestitemoid AND TABLENAME='QL_TRNREQUESTITEM' INNER JOIN QL_mstgen cb ON cb.gencode=ap.branch_code AND cb.gengroup='CABANG' INNER JOIN QL_mstgen ct ON ct.gencode=pbm.tobranch AND ct.gengroup='CABANG' Where ap.approvaluser='" & Session("UserID") & "' AND trnrequestitemstatus='In Approval' AND pbm.branchcode IN (" & Left(sCodeFin, sCodeFin.Length - 1) & ")"

        FillGV(gvList, sSql, "QL_TRNREQUESTITEM")
        Session("TblGVList") = ckon.ambiltabel(sSql, "QL_trnrequestitem")
    End Sub

    Private Sub SetGVDataPb(ByVal dtDataHeader As DataTable)
        Session("branchcode") = gvList.SelectedDataKey.Item("branchcode").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Cabang", "tanggal", "Keterangan", "Request User", "Request Date"}
        Dim arValue() As String = {"trnoid", "gendesc", "trndate", "trnrequestitemnote", "requestuser", "requestdate"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), iRoundDigit)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next

        Dim arHeader() As String = {"No.", "Kode", "Katalog", "Qty", "Keterangan"}
        Dim arField() As String = {"Seq", "itemcode", "itemdesc", "ReqQty", "Note"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next

        Dim bln1 As String = Month(CDate(toDate(Session("trndate")))) - 1
        Dim bln2 As String = Month(CDate(toDate(Session("trndate")))) - 2
        Dim bln3 As String = Month(CDate(toDate(Session("trndate")))) - 3

        If bln1 <= 0 Then
            bln1 = bln1 + 12
        End If
        If bln2 <= 0 Then
            bln2 = bln2 + 12
        End If
        If bln3 <= 0 Then
            bln3 = bln3 + 12
        End If
        GenerateBulan(bln1, "satu")
        GenerateBulan(bln2, "dua")
        GenerateBulan(bln3, "tiga")

        gvPBdtl.Columns(5).HeaderText = lblbln1.Text
        gvPBdtl.Columns(4).HeaderText = lblbln2.Text
        gvPBdtl.Columns(3).HeaderText = lblbln3.Text

        sSql = "SELECT trnrequestitemdtloid,i.itemoid,itemcode,itemdesc,trnrequestitemdtlseq Seq,trnrequestitemdtlqty ReqQty,totalsalesmonth1 bln1,totalsalesmonth2 bln2,totalsalesmonth3 bln3,saldoakhir,ordermstoid,trnrequestitemdtlnote Note,'' orderno,trnrequestitemdtlqty acceptqty,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya FROM QL_trnrequestitemdtl pd INNER JOIN QL_mstitem i ON i.itemoid=pd.itemoid WHERE pd.cmpcode='" & CompnyCode & "' AND trnrequestitemoid=" & Session("trnoid") & " ORDER BY pd.trnrequestitemdtlseq"
        Session("TblDtlReqItem") = ckon.ambiltabel(sSql, "QL_trnrequestitemdtl")
        gvPBdtl.DataSource = Session("TblDtlReqItem")
        gvPBdtl.DataBind()
        gvData.Visible = False
    End Sub

    Private Sub UpdatePb(ByVal sNo As String, ByVal sStatus As String)
        UpdateHppNya()
        If IsDBNull(Session("trnoid")) Then
            showMessage("Silahkan Pilih data transaksi Terlebih Dahulu", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Silahkan Pilih transaksi Terlebih Dahulu", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & Session("branchcode") & "' AND gengroup='CABANG'")
        Dim pNo As String = "PB/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(trnrequestitemno,'" & pNo & "',''))),0)+1 FROM QL_trnrequestitem WHERE trnrequestitemno LIKE '" & pNo & "%' And branchcode='" & Session("branchcode") & "'"
        pNo = GenNumberString(pNo, "", GetStrData(sSql), 4)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            ' Update PB
            sSql = "UPDATE QL_trnrequestitem SET trnrequestitemno='" & pNo & "', trnrequestitemstatus='" & sStatus & "',approvaluser='" & Session("UserID") & "',approvaltime=CURRENT_TIMESTAMP,upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE trnrequestitemoid=" & Session("trnoid") & " AND trnrequestitemstatus='In Approval'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ' update pd detail
            If Not Session("TblDtlReqItem") Is Nothing Then
                Dim dts As DataTable = Session("TblDtlReqItem")
                For C1 As Integer = 0 To dts.Rows.Count - 1
                    sSql = "UPDATE QL_trnrequestitemdtl SET acceptqty=" & ToDouble(dts.Rows(C1)("acceptqty").ToString) & " WHERE trnrequestitemdtloid=" & Integer.Parse(dts.Rows(C1)("trnrequestitemdtloid")) & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_TRNREQUESTITEM' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    Exit Sub
                Else
                    showMessage(exSql.ToString, CompnyName & " - ERROR", 1) : Exit Sub
                End If
            Else
                showMessage(exSql.ToString, CompnyName & " - ERROR", 1) : Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

    Protected Sub gvPBdtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPBdtl.PageIndexChanging
        UpdateHppNya()
        If Session("TblDtlReqItem") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlReqItem")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvDataAdj.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvPBdtl.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(8).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            dv.RowFilter = "seq=" & CType(myControl, System.Web.UI.WebControls.TextBox).ToolTip
                            Dim dValue As Double = 0
                            dValue = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                            If dv.Count > 0 Then
                                dv(0)("ReqQty") = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                            End If
                            dv.RowFilter = ""
                        End If
                    Next
                End If
            Next
            dt.AcceptChanges()
            Session("TblDtlReqItem") = dt
            gvPBdtl.DataSource = Session("TblDtlReqItem")
            gvPBdtl.DataBind()
        End If
        gvPBdtl.PageIndex = e.NewPageIndex
        gvPBdtl.DataSource = Session("TblDtlReqItem")
        gvPBdtl.DataBind()
    End Sub

    Protected Sub gvPBdtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPBdtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3) 
        End If
    End Sub

#Region "Forecast PO"
    Private Sub SetGVListFC()
        Dim arHeader() As String = {"No. Forecast", "Cabang", "Tanggal", "Keterangan", "Request User", "Request Date"}
        Dim arField() As String = {"transformno", "cabang", "trndate", "Keterangan", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "trndate", "cabang", "Keterangan", "requestuser", "requestdate", "approvaloid", "branch_code", "transformno"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1) : field.SortExpression = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey

        Dim QlFC As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='Ql_trnforecastmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeFC As String = ""
        Dim FcCode() As String = QlFC.Split(",")
        For C1 As Integer = 0 To FcCode.Length - 1
            If FcCode(C1) <> "" Then
                sCodeFC &= "'" & FcCode(C1).Trim & "',"
            End If
        Next

        sSql = "SELECT rm.cmpcode, rm.trnforecastoid trnoid, rm.branch_code, trnforecaststatus [status], (Select gendesc from QL_mstgen cb WHERE cb.gencode=rm.branch_code AND cb.gengroup='CABANG') cabang, Convert(Char(20), rm.trnforecastdate,103) trndate, rm.trnforecastno transformno, rm.trnforecastnote Keterangan, ap.requestuser, ap.requestdate, ap.approvaloid FROM Ql_trnforecastmst rm INNER JOIN QL_APPROVAL ap ON ap.OID=rm.trnforecastoid WHERE rm.trnforecaststatus IN ('In Approval') AND ap.TABLENAME='Ql_trnforecastmst' AND ap.approvaluser='" & Session("UserID") & "' And rm.branch_code IN (" & Left(sCodeFC, sCodeFC.Length - 1) & ")"
        Session("TblListFC") = ckon.ambiltabel(sSql, "Ql_trnforecastmst")
        gvList.DataSource = Session("TblListFC") : gvList.DataBind()
    End Sub

    Private Sub SetGVDataFC(ByVal dtDataHeader As DataTable)
        Session("branch") = gvList.SelectedDataKey.Item("branch_code").ToString
        Dim arCaption() As String = {"No. Forecast", "Cabang", "tanggal", "Keterangan", "Request User", "Request Date"}
        Dim arValue() As String = {"transformno", "cabang", "trndate", "Keterangan", "requestuser", "requestdate"}
        Dim arHeader() As String = {"No.", "Kode", "Katalog", "Price", "Qty", "Amt Netto", "Keterangan"}
        Dim arField() As String = {"trnforecastdtloid", "itemcode", "itemdesc", "itemprice", "itemqty", "amtdtlnetto", "notedtl"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT rd.trnforecastoid, rd.trnforecastdtloid, m.itemoid, m.itemcode, m.itemdesc, rd.itemqty, rd.itemprice , amtdtlnetto, notedtl, 'BUAH' UNIT FROM ql_trnforecastdtl rd INNER join QL_mstitem m ON m.itemoid=rd.itemoid WHERE rd.trnforecastoid=" & Session("trnoid") & " Order By rd.trnforecastoid"
        Session("TrnForeCastdtl") = ckon.ambiltabel(sSql, "ql_trnforecastdtl")
        gvData.DataSource = Session("TrnForeCastdtl")
        gvData.DataBind()
    End Sub

    Private Sub UpdateForeCast(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Silahkan Pilih data transaksi Terlebih Dahulu", CompnyName & " - WARNING", 2)
            Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Silahkan Pilih transaksi Terlebih Dahulu", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
        End If
        Dim conmtroid As Int32 = GenerateID("QL_conmtr", CompnyCode)
        Dim period As String = GetDateToPeriodAcctg(GetServerTime())

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Dim code As String = "", reqcode As String = ""
            sSql = "Select genother1 from ql_mstgen where gengroup='cabang' and gencode='" & Session("branch") & "'"
            xCmd.CommandText = sSql : Dim sCabang As String = xCmd.ExecuteScalar()
            code = "FC/" & sCabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnforecastno,4) AS INT)),0) FROM Ql_trnforecastmst WHERE trnforecastno LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            reqcode = code & sequence

            ' Update TTS
            sSql = "Update Ql_trnforecastmst Set trnforecastno = '" & reqcode & "', updtime = CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', trnforecaststatus='" & sStatus & "' WHERE trnforecastoid=" & Session("trnoid")
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='Ql_trnforecastmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    Exit Sub
                Else
                    showMessage(exSql.ToString, CompnyName & " - ERROR", 1) : Exit Sub
                End If
            Else
                showMessage(exSql.ToString, CompnyName & " - ERROR", 1) : Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region
End Class
