Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction

Partial Class Master_MyProfile
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
		
        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '          Server.Transfer("~\other\NotAuthorize.aspx")
        '      End If
		
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("MyProfile.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - My Profile"
        If Not Page.IsPostBack Then
            userid.Text = Session("UserID")
            username.Text = ckon.ambilscalar("SELECT username FROM ql_mstprof WHERE cmpcode='" & CompnyCode & "' AND userid='" & Session("UserID") & "'")
        End If
    End Sub

    Protected Sub imbBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("~/other/menu.aspx?awal=true")
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        lblWarning.Text = ""

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Connection = conn
        objCmd.Transaction = objTrans
        Try
            sSql = "SELECT count(-1) FROM QL_mstprof WHERE cmpcode='" & CompnyCode & "' AND userid='" & Session("UserID") & _
                "' AND profpass='" & Tchar(oldPassword.Text) & "'"
            If ckon.ambilscalar(sSql) < 1 Then
                objTrans.Rollback() : conn.Close()
                lblWarning.Text = "Invalid Old Password!!" : Exit Sub
            End If
            sSql = "UPDATE QL_mstprof SET username='" & Tchar(username.Text) & "',profpass='" & Tchar(Password1.Text) & _
                "' WHERE cmpcode='" & CompnyCode & "' AND userid='" & Session("UserID") & "'"
            objCmd.CommandText = sSql
            objCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            lblWarning.Text = ex.ToString : Exit Sub
        End Try
        lblWarning.Text = "Your profile data has been saved!!"
    End Sub
End Class
