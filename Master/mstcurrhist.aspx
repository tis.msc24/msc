<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
CodeFile="mstcurrhist.aspx.vb" Inherits="mstcurrhist" %>

<asp:Content ID="MiddleContent" runat="server" ContentPlaceHolderID="MiddleContent">

 <script type="text/javascript">
//    function s()
//    {
//    try {
//        var t = document.getElementById("<%=GVmst.ClientID%>");
//        var t2 = t.cloneNode(true)
//        for(i = t2.rows.length -1;i > 0;i--)
//        t2.deleteRow(i)
//        t.deleteRow(0)
//        divTblData.appendChild(t2)
//        }
//    catch(errorInfo) {}
//    
//    }
//    window.onload = s
  </script>
                <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" style="width: 100%">
                    <tr>
                        <th align="left" class="header" style="width: 100%;" colspan="2">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Data Currency History"></asp:Label><asp:SqlDataSource ID="SDSData" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                                DeleteCommand="DELETE FROM [QL_mstcurrhist] WHERE [cmpcode] = @cmpcode AND [currhistoid] = @currhistoid AND [curroid]=@curroid"
                                InsertCommand="INSERT INTO [QL_mstcurrhist] ([cmpcode], [currhistoid], [curroid], [currdate], [curratestoIDRbeli], [curratestoIDRjual], [curratestoUSDbeli], [curratestoUSDjual], [currnotes], [upduser], [updtime]) VALUES (@cmpcode, @currhistoid, @curroid, @currdate, @curratestoIDRbeli, @curratestoIDRjual, @curratestoUSDbeli, @curratestoUSDjual, @currnotes, @upduser, @updtime)"
                                SelectCommand="SELECT [cmpcode], [currhistoid], [curroid], [currdate], [curratestoIDRbeli], [curratestoIDRjual], [curratestoUSDbeli], [curratestoUSDjual], [currnotes], [upduser], [updtime] FROM [QL_mstcurrhist] WHERE (([cmpcode] = @cmpcode) AND ([currhistoid] = @currhistoid) AND ([curroid] = @curroid))"
                                UpdateCommand="UPDATE [QL_mstcurrhist] SET [curroid] = @curroid, [currdate] = @currdate, [curratestoIDRbeli] = @curratestoIDRbeli, [curratestoIDRjual] = @curratestoIDRjual, [curratestoUSDbeli] = @curratestoUSDbeli, [curratestoUSDjual] = @curratestoUSDjual, [currnotes] = @currnotes, [upduser] = @upduser, [updtime] = @updtime WHERE [cmpcode] = @cmpcode AND [currhistoid] = @currhistoid" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                                <SelectParameters>
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currhistoid" Type="Int32" />
                                    <asp:Parameter Name="curroid" Type="Int32" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currhistoid" Type="Int32" />
                                    <asp:Parameter Name="curroid" />
                                </DeleteParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="curroid" Type="Int32" />
                                    <asp:Parameter Name="currdate" Type="DateTime" />
                                    <asp:Parameter Name="curratestoIDRbeli" Type="Decimal" />
                                    <asp:Parameter Name="curratestoIDRjual" Type="Decimal" />
                                    <asp:Parameter Name="curratestoUSDbeli" Type="Decimal" />
                                    <asp:Parameter Name="curratestoUSDjual" Type="Decimal" />
                                    <asp:Parameter Name="currnotes" Type="String" />
                                    <asp:Parameter Name="upduser" Type="String" />
                                    <asp:Parameter Name="updtime" Type="DateTime" />
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currhistoid" Type="Int32" />
                                </UpdateParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currhistoid" Type="Int32" />
                                    <asp:Parameter Name="curroid" Type="Int32" />
                                    <asp:Parameter Name="currdate" Type="DateTime" />
                                    <asp:Parameter Name="curratestoIDRbeli" Type="Decimal" />
                                    <asp:Parameter Name="curratestoIDRjual" Type="Decimal" />
                                    <asp:Parameter Name="curratestoUSDbeli" Type="Decimal" />
                                    <asp:Parameter Name="curratestoUSDjual" Type="Decimal" />
                                    <asp:Parameter Name="currnotes" Type="String" />
                                    <asp:Parameter Name="upduser" Type="String" />
                                    <asp:Parameter Name="updtime" Type="DateTime" />
                                </InsertParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SDSDataView" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                                SelectCommand="SELECT cmpcode, currhistoid, curroid, currdate, curratestoIDRbeli, curratestoIDRjual, curratestoUSDbeli, curratestoUSDjual, currnotes, upduser, updtime FROM QL_mstcurrhist WHERE (cmpcode LIKE @cmpcode) AND (curroid = @curroid) AND (currdate BETWEEN @currdate AND @currdate2) AND (upduser LIKE @upduser)" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                                <SelectParameters>
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:QueryStringParameter Name="curroid" QueryStringField="oid" Type="Int32" />
                                    <asp:Parameter Name="currdate" Type="DateTime" />
                                    <asp:Parameter Name="currdate2" Type="DateTime" />
                                    <asp:Parameter Name="upduser" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SDSOid" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                                SelectCommand="SELECT [cmpcode], [tablename], [lastoid], [tablegroup] FROM [QL_mstoid] WHERE (([tablename] = @tablename) AND ([cmpcode] = @cmpcode))"
                                UpdateCommand="UPDATE QL_mstoid SET lastoid = @lastoid WHERE (tablename = @tablename) AND (cmpcode = @cmpcode)" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="" Name="tablename" Type="String" />
                                    <asp:Parameter DefaultValue="" Name="cmpcode" Type="String" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="lastoid" />
                                    <asp:Parameter Name="tablename" />
                                    <asp:Parameter Name="cmpcode" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </th>
                    </tr>
                </table>
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                  <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Height="350px" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <table>
                                    <tr>
                                        <td align="left" class="Label" colspan="2">
                                        Periode :
                                        <asp:TextBox ID="txtPeriode1" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>
                                            <asp:ImageButton
                                            ID="btnPeriode1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                                        <asp:Label ID="Label1" runat="server" Text="to"></asp:Label>
                                        <asp:TextBox ID="txtPeriode2" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>
                                            <asp:ImageButton
                                            ID="btnPeriode2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                                            <asp:Label ID="Label2" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></td>
                                        <td align="left">
                                        <asp:ImageButton
                                            ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" /></td>
                                        <td align="left">
                                            <asp:ImageButton
                                            ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" /></td>
                                        <td align="left" class="Label">
                                            <asp:LinkButton ID="LinkButton1"  runat="server">[ Show Master ]</asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <ajaxToolkit:CalendarExtender
                                                ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" PopupButtonID="btnPeriode1"
                                                TargetControlID="txtPeriode1" Enabled="True">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td align="left">
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="MM/dd/yyyy"
                                            PopupButtonID="btnPeriode2" TargetControlID="txtPeriode2" Enabled="True">
                                        </ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td align="left">
                                            <ajaxToolkit:MaskedEditExtender ID="meeDate1" runat="server" Mask="99/99/9999" MaskType="Date"
                                                TargetControlID="txtPeriode1" UserDateFormat="MonthDayYear" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True">
                                            </ajaxToolkit:MaskedEditExtender>
                                        </td>
                                        <td align="left">
                                            <ajaxToolkit:MaskedEditExtender ID="meeDate2" runat="server" Mask="99/99/9999" MaskType="Date"
                                                TargetControlID="txtPeriode2" UserDateFormat="MonthDayYear" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True">
                                            </ajaxToolkit:MaskedEditExtender>
                                        </td>
                                        <td align="left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="5" style="height: 10px">
                                            <asp:Label ID="lblWarn" runat="server" CssClass="Important"></asp:Label></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <fieldset  id="field1" style="height: 275px; width: 99%; text-align: left; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"  > 
    <div id='divTblData'>
    </div>
    <div style="height: 100%; width: 100%;">
                                        <asp:GridView ID="GVmst" runat="server" CellPadding="4" AutoGenerateColumns="False" BackColor="White"
                                            BorderColor="#DEDFDE" BorderWidth="1px" Width="98%" BorderStyle="Solid" DataSourceID="SDSDataView" AllowSorting="True" AllowPaging="True">
                                            <FooterStyle BackColor="#F25407" />
                                            <SelectedRowStyle BackColor="#F25407" ForeColor="White" Font-Bold="True" />
                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Right" />
                                            <HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="History Date" SortExpression="currdate">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LBOid" runat="server" OnClick="LBOid_Click" CommandArgument='<%# Eval("Currhistoid") %>' Text='<%# Format(Eval("currdate"),"MM/dd/yyyy") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" ForeColor="Navy" Width="20%" />
                                                    <FooterStyle Font-Size="X-Small" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="curratestoIDRjual" HeaderText="Sell Rupiah" DataFormatString="{0:N}" HtmlEncode="False" SortExpression="curratestoIDRjual">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" Width="20%" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Right" Width="20%" ForeColor="Navy" />
                                                    
                                                </asp:BoundField>
                                                <asp:BoundField DataField="curratestoUSDjual" HeaderText="Sell USD" DataFormatString="{0:#,##0.00}" HtmlEncode="False" SortExpression="curratestoUSDjual">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" Width="20%" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Right" Width="20%" ForeColor="Navy" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="currnotes" HeaderText="Notes" SortExpression="currnotes" >
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="40%" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="40%" Font-Size="X-Small" ForeColor="Navy" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="currhistoid" HeaderText="id" Visible="False" />
                                            </Columns>
                                            <RowStyle BackColor="#F7F7DE" />
                                            <EmptyDataTemplate>
                                        <asp:Label ID="lblmsg" runat="server" Text="No data found !" CssClass="Important"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        
                                        </div></fieldset>
                            &nbsp;&nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">
                            :: List Of Currency History</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:FormView ID="FrmViewCurrHis" runat="server" DataKeyNames="cmpcode,currhistoid"
                                DataSourceID="SDSData" DefaultMode="Insert">
                                <EditItemTemplate>
                                    <table id="Table1" align="center" border="0" cellpadding="4" cellspacing="2">
                                        <tr style="color: #000099">
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Currency Date
                                                    <asp:Label ID="Label3" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="currdate" runat="server" CssClass="inpText" Text='<%# Bind("currdate", "{0:d}") %>'
                                                    Width="75px" Font-Size="X-Small"></asp:TextBox>&nbsp;<asp:ImageButton ID="btnCurrDate" runat="server"
                                                        ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                                                <asp:Label ID="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label><ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="MM/dd/yyyy"
                                                    PopupButtonID="btncurrdate" TargetControlID="currdate">
                                                </ajaxToolkit:CalendarExtender>
                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender5" runat="server" CultureName="en-US"
                                                    Mask="99/99/9999" MaskType="Date" TargetControlID="currdate" UserDateFormat="MonthDayYear">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr style="color: #000099">
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Sell Rupiah</span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="curratestoIDRjual" runat="server" CssClass="inpText" MaxLength="10"
                                                    size="20" Text='<%# Bind("curratestoIDRjual", "{0:f2}") %>' Width="125px" Font-Size="X-Small"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender
                                                        ID="MaskedEditExtender1" runat="server" TargetControlID="curratestoIDRbeli" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left;">
                                                <span style="font-size: x-small">Buy Rupiah</span></td>
                                            <td style="text-align: left;">
                                                <asp:TextBox ID="curratestoIDRbeli" runat="server" CssClass="inpText" MaxLength="10"
                                                    size="20" Text='<%# Bind("curratestoIDRbeli", "{0:f2}") %>' Width="125px" Font-Size="X-Small"></asp:TextBox><ajaxToolkit:MaskedEditExtender
                                                        ID="MaskedEditExtender2" runat="server" TargetControlID="curratestoIDRjual" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Sell USD</span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="curratestoUSDjual" runat="server" CssClass="inpText" MaxLength="10"
                                                    size="20" Text='<%# Bind("curratestoUSDjual", "{0:f2}") %>' Width="125px" Font-Size="X-Small"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender
                                                        ID="MaskedEditExtender3" runat="server" TargetControlID="curratestoUSDbeli" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Buy USD</span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="curratestoUSDbeli" runat="server" CssClass="inpText" MaxLength="10"
                                                    size="20" Text='<%# Bind("curratestoUSDbeli", "{0:f2}") %>' Width="125px" Font-Size="X-Small"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender
                                                        ID="MaskedEditExtender4" runat="server" TargetControlID="curratestoUSDjual" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small;">Note</span></td>
                                            <td style="text-align: left; font-size: 8pt;">
                                                <asp:TextBox ID="currnotes" runat="server" CssClass="inpText" MaxLength="50"
                                                    size="20" Text='<%# Bind("currnotes", "{0}") %>' Width="175px" Font-Size="X-Small"></asp:TextBox></td>
                                        </tr>
                                        <tr style="font-size: 8pt">
                                            <td colspan="2" style="text-align: left; color: #585858;">
                                                <span style="font-size: x-small">Last Update On</span>
                                                <asp:Label ID="Updtime" runat="server" ForeColor="#585858" Text='<%# Eval("UPDTIME", "{0:G}") %>' Font-Bold="True" Font-Size="X-Small"></asp:Label>
                                                <span style="font-size: x-small">by</span>
                                                <asp:Label ID="Upduser" runat="server" ForeColor="#585858" Text='<%# Eval("UPDUSER", "{0}") %>' Font-Bold="True" Font-Size="X-Small"></asp:Label><asp:TextBox ID="oid" runat="server" CssClass="inpText" Enabled="False" MaxLength="10"
                                                    size="20" Text='<%# Bind("currhistoid", "{0}") %>' Visible="False" Width="25px"></asp:TextBox></td>
                                        </tr>
                                        <tr style="font-size: 8pt">
                                            <td colspan="2">
                                                <asp:ImageButton ID="btnSave" runat="server" CommandName="Update" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" />
                                                <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" OnClick="btnCancel_Click" />
                                                <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" onClientClick="return confirm('Are you sure to delete this data?')" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" /><asp:Label ID="i_u" runat="server" ForeColor="Red" Text="Update" Visible="False"></asp:Label></td>
                                        </tr>
                                    </table>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <table id="Table4" align="center" border="0" cellpadding="4" cellspacing="2">
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Currency Date
                                                    <asp:Label ID="Label5" runat="server" CssClass="Important" Text="*"></asp:Label></span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="currdate" runat="server" CssClass="inpText" Text='<%# Bind("currdate", "{0:d}") %>'
                                                    Width="75px" Font-Size="X-Small"></asp:TextBox>&nbsp;<asp:ImageButton ID="btnCurrDate" runat="server"
                                                        ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                                                <asp:Label ID="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label><asp:Label ID="i_u" runat="server" ForeColor="Red" Text="New Data" Visible="False"></asp:Label><ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="MM/dd/yyyy"
                                                    PopupButtonID="btncurrdate" TargetControlID="currdate">
                                                </ajaxToolkit:CalendarExtender>
                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender5" runat="server" CultureName="en-US"
                                                    Mask="99/99/9999" MaskType="Date" TargetControlID="currdate" UserDateFormat="MonthDayYear">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Sell Rupiah</span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="curratestoIDRjual" runat="server" CssClass="inpText" MaxLength="10"
                                                    size="20" Text='<%# Bind("curratestoIDRjual", "{0}") %>' Width="125px" Font-Size="X-Small"></asp:TextBox><ajaxToolkit:MaskedEditExtender
                                                        ID="MaskedEditExtender1" runat="server" TargetControlID="curratestoIDRbeli" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number">
                                                    </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Buy Rupiah</span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="curratestoIDRbeli" runat="server" CssClass="inpText" MaxLength="10"
                                                    size="20" Text='<%# Bind("curratestoIDRbeli", "{0}") %>' Width="125px" Font-Size="X-Small"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender
                                                        ID="MaskedEditExtender2" runat="server" TargetControlID="curratestoIDRjual" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Sell USD</span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="curratestoUSDjual" runat="server" CssClass="inpText" MaxLength="10"
                                                    size="20" Text='<%# Bind("curratestoUSDjual", "{0}") %>' Width="125px" Font-Size="X-Small"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender
                                                        ID="MaskedEditExtender3" runat="server" TargetControlID="curratestoUSDbeli" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small">Buy USD</span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="curratestoUSDbeli" runat="server" CssClass="inpText" MaxLength="10"
                                                    size="20" Text='<%# Bind("curratestoUSDbeli", "{0}") %>' Width="125px" Font-Size="X-Small"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender
                                                        ID="MaskedEditExtender4" runat="server" TargetControlID="curratestoUSDjual" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number">
                                                </ajaxToolkit:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <span style="font-size: x-small;">Note</span></td>
                                            <td style="text-align: left">
                                                <asp:TextBox ID="currnotes" runat="server" CssClass="inpText" MaxLength="50"
                                                    size="20" Text='<%# Bind("currnotes", "{0}") %>' Width="175px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: left">
                                                <asp:ImageButton ID="btnSave" runat="server" CommandName="insert" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" />
                                                <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" OnClick="btnCancel_click" /><asp:Label ID="UpdTime" runat="server" ForeColor="#585858" Text='<%# Eval("UPDTIME", "{0:G}") %>' Font-Bold="True" Font-Size="X-Small" Visible="False"></asp:Label><asp:Label ID="UpdUser" runat="server" ForeColor="#585858" Text='<%# Eval("UPDUSER", "{0}") %>' Font-Bold="True" Font-Size="X-Small" Visible="False"></asp:Label><asp:TextBox ID="oid" runat="server" CssClass="inpText" Enabled="False" MaxLength="10"
                                                    size="20" Text='<%# Bind("currhistoid", "{0}") %>' Font-Size="X-Small" Visible="False"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </InsertItemTemplate>
                            </asp:FormView>
                            <asp:Label ID="lblWarning" runat="server" Font-Size="X-Small" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">
                            :: Form Currency History</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>





































   
</asp:Content>

        
        
