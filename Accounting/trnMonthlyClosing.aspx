<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMonthlyClosing.aspx.vb" Inherits="Accounting_trnMonthlyClosing" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Monthly Closing" CssClass="Title" Font-Italic="False" Font-Size="21px" ForeColor="Maroon"></asp:Label><asp:SqlDataSource ID="SDSList"
                    runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>"></asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=2>Period :&nbsp;<asp:DropDownList id="DDLMonth" runat="server" CssClass="inpText"></asp:DropDownList> <asp:DropDownList id="DDLYear" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=2>Cabang:&nbsp;<asp:DropDownList id="DDLCabang" runat="server" CssClass="inpText" OnSelectedIndexChanged="DDLCabang_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList> <asp:ImageButton id="imbFindLoan" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" OnClick="imbFindLoan_Click1"></asp:ImageButton>&nbsp;Last Monthly Closing Period : <asp:Label id="lblLastClosing" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:Label id="lastclosemonth" runat="server" Visible="False"></asp:Label><asp:Label id="lastcloseyear" runat="server" Visible="False"></asp:Label> <asp:Label id="nextclosemonth" runat="server" Visible="False"></asp:Label><asp:Label id="nextcloseyear" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:Panel id="pnlNeedPost" runat="server" Width="100%" Visible="False"><TABLE><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: small; COLOR: red" class="Label" align=left><asp:Label id="lblCounter" runat="server" Font-Bold="True" Text="0"></asp:Label>&nbsp;following transaction have&nbsp;to be&nbsp;POST/APPROVED&nbsp;before Monthly&nbsp;Closing can be done !!</TD></TR><TR><TD class="Label" align=left><FIELDSET style="WIDTH: 50%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvPostList" runat="server" Width="99%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
                                                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                                                                    <Columns>
                                                                                        <asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False">
                                                                                            <HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
                                                                                        </asp:CommandField>
                                                                                        <asp:BoundField DataField="seq" HeaderText="No">
                                                                                            <HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="tipe" HeaderText="Jenis">
                                                                                            <HeaderStyle Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                                            <ItemStyle Font-Size="X-Small"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="jml" HeaderText="Jumlah">
                                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:CommandField DeleteImageUrl="~/Images/del.jpeg" ShowDeleteButton="True" ButtonType="Image" Visible="False">
                                                                                            <HeaderStyle HorizontalAlign="Center" Width="25px" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
                                                                                        </asp:CommandField>
                                                                                    </Columns>

                                                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

                                                                                    <PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
                                                                                    <EmptyDataTemplate>
                                                                                        <asp:Label ID="lblEmptyDetail" runat="server" CssClass="Important" Text="Data tidak ditemukan !!"></asp:Label>
                                                                                    </EmptyDataTemplate>

                                                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

                                                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

                                                                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                                                </asp:GridView> </DIV></FIELDSET> </TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=left colSpan=2><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" Text="Closing Detail Data :" Font-Underline="True"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="divgvItemOrdered"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvDtl" runat="server" Width="99%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
                                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                                                <Columns>
                                                                    <asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False">
                                                                        <HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
                                                                    </asp:CommandField>
                                                                    <asp:BoundField DataField="seq" HeaderText="No">
                                                                        <HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="acctgcode" HeaderText="Kode">
                                                                        <HeaderStyle Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle Font-Size="X-Small"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="acctgdesc" HeaderText="Diskripsi">
                                                                        <HeaderStyle Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle Font-Size="X-Small"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="amtopen" HeaderText="Saldo Awal">
                                                                        <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="amtdebet" HeaderText="Debet">
                                                                        <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Font-Strikeout="False"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="amtcredit" HeaderText="Kredit">
                                                                        <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="amtbalance" HeaderText="Saldo Akhir">
                                                                        <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:CommandField DeleteImageUrl="~/Images/del.jpeg" ShowDeleteButton="True" ButtonType="Image" Visible="False">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="25px" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

                                                                        <ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
                                                                    </asp:CommandField>
                                                                </Columns>

                                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

                                                                <PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
                                                                <EmptyDataTemplate>
                                                                    <asp:Label ID="lblEmptyDetail" runat="server" CssClass="Important" Text="Data tidak ditemukan !!"></asp:Label>
                                                                </EmptyDataTemplate>

                                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

                                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

                                                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                            </asp:GridView> </DIV></FIELDSET> </TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Posting.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Save" OnClick="btnSave_Click1"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Delete"></asp:ImageButton></TD><TD style="COLOR: #585858" align=left>By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD><TD style="COLOR: #585858" align=left><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle" Height="24px"></asp:Image>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="Label6" runat="server" Text="Put Modal PopUp here" Visible="False"></asp:Label></TD></TR><TR><TD vAlign=top align=left></TD><TD vAlign=top align=left></TD></TR><TR><TD vAlign=top align=left></TD><TD vAlign=top align=left></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Closing Bulanan :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

