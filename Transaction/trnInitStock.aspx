﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnInitStock.aspx.vb" Inherits="Transaction_trnInitStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
     <script type="text/javascript">
         function EnterEvent(e) {
             __doPostBack('<%=lbAddToListMat.UniqueID%>', "");
             __doPostBack('<%=ButtonA.UniqueID%>', "");
            // if (e.keyCode == 13) {
                
            //    //ModalPopupSN.Show();
            //}
        }
    </script>

    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
              <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Init Stock" CssClass="Title" ForeColor="Maroon" Font-Names="Verdana" Font-Size="21px"></asp:Label>
              </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" valign="top" style="text-align: left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
               <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 800px; TEXT-ALIGN: left"><TBODY><TR><TD id="tdPeriod1" align=left runat="server" visible="true"><asp:CheckBox id="chkPeriod" runat="server" __designer:wfdid="w30"></asp:CheckBox> <asp:Label id="Label6" runat="server" Text="Periode" __designer:wfdid="w31"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" visible="true">: <asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w32"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w34"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w36">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w37" TargetControlID="dateAwal" Format="dd/MM/yyyy" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w38" TargetControlID="dateAkhir" Format="dd/MM/yyyy" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w39" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w40" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD id="td1" align=left runat="server" visible="true"><asp:Label id="Label7" runat="server" Text="Branch " __designer:wfdid="w41"></asp:Label></TD><TD id="td2" align=left runat="server" visible="true">: <asp:DropDownList id="dd_branch" runat="server" Width="230px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w42"></asp:DropDownList></TD></TR><TR><TD id="tdperiod3" align=left runat="server" visible="true"><asp:Label id="Label8" runat="server" Text="Lokasi " __designer:wfdid="w43"></asp:Label></TD><TD align=left colSpan=3 runat="server" visible="true">: <asp:DropDownList id="DDLocation" runat="server" Width="230px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w44"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Width="84px" Text="Item / Barang" __designer:wfdid="w45"></asp:Label></TD><TD align=left colSpan=3>: <asp:TextBox id="itemname" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w46"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w47"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w48"></asp:ImageButton>&nbsp;&nbsp;<asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w49"></asp:Label>&nbsp;&nbsp;<asp:ImageButton id="btnfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:ImageButton> &nbsp;<asp:ImageButton id="btnviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w51"></asp:ImageButton> &nbsp;<asp:ImageButton id="imbLastSearch" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w52"></asp:ImageButton> </TD></TR><TR><TD align=left></TD><TD align=left colSpan=3><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w53" EnableModelValidation="True" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" PageSize="12" UseAccessibleHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle Font-Bold="True" ForeColor="White" CssClass="gvhdr"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE><asp:GridView id="gvTblData" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w54" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="No" HeaderText="No">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="10px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="initoid" HeaderText="initoid" Visible="False">
<HeaderStyle Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch" HeaderText="branch">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="location">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code" HtmlEncode="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Name">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="600px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SN" HeaderText="SN">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="initqty" HeaderText="Qty">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="HargaEcer" HeaderText="Harga Ecer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TolalHarga" HeaderText="Tolal Harga">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
                                                    <asp:ImageButton ID="imbPrintFromList" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" ToolTip=""></asp:ImageButton><ajaxToolkit:ConfirmButtonExtender ID="cbePrintFromList" runat="server" TargetControlID="imbPrintFromList" ConfirmText="Are You Sure Print This Data ?">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                            <asp:Label ID="Label3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!!"></asp:Label>
                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" BorderStyle="None" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR />
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="gvTblData"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="ImageList" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: List of Init Stock</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="imgForm" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Init Stock :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table style="WIDTH: 100%">
                                <tbody>
                                    <tr>
                                        <td style="WIDTH: 106px" class="Label" align="left">
                                            <asp:Label ID="Label9" runat="server" Text="Business Unit"></asp:Label></td>
                                        <td style="WIDTH: 1%" class="Label" align="center">:</td>
                                        <td class="Label" align="left">
                                            <asp:DropDownList ID="DDLBusUnit" runat="server" CssClass="inpText" Width="300px" AutoPostBack="True"></asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td class="Label" align="left" style="width: 106px">
                                            <asp:Label ID="Label5" runat="server" Text="Init Location"></asp:Label></td>
                                        <td class="Label" align="center">:</td>
                                        <td class="Label" align="left">
                                            <asp:DropDownList ID="DDLLocation" runat="server" CssClass="inpText" Width="300px"></asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td class="Label" align="left" style="width: 106px">
                                            <asp:Label ID="Label2" runat="server" Text="Init SN"></asp:Label>&nbsp;<asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" />
                                        </td>
                                        <td class="Label" align="center">:</td>
                                        <td class="Label" align="left">
                                            <asp:FileUpload ID="FileUpload1" runat="server" Visible="False" Height="30px" Width="222px" />
                                            &nbsp;
                                                    <asp:Button ID="BtnUplaodFile" runat="server" Text="Upload" Visible="False" />
                                            <br />
                                            <asp:Label ID="FileExelInitStokSN" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Label" align="left" style="width: 106px"></td>
                                        <td class="Label" align="center"></td>
                                        <td class="Label" align="left">
                                            <asp:ImageButton ID="btnLookUpMat" runat="server" ImageUrl="~/Images/find.png" style="height: 23px"></asp:ImageButton></td>
                                    </tr>
                                    <tr>
                                        <td class="Label" align="left" colspan="3">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
<asp:GridView id="gvDtl" runat="server" Width="99%" CssClass="MyTabStyle" ForeColor="#333333" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="seq,SN,matqty" EnableModelValidation="True" __designer:wfdid="w249">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowEditButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="SteelBlue"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No." ReadOnly="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matwh" HeaderText="Location" ReadOnly="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Code" ReadOnly="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description" ReadOnly="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="35%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="28%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SN" HeaderText="Has SN" ReadOnly="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="25px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Init Qty"><EditItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="75px" CssClass="inpText" Text='<%# eval("matqty") %>' MaxLength="5" Enabled='<%# eval("enableqty") %>' __designer:wfdid="w261"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" TargetControlID="tbQty" ValidChars="1234567890.," __designer:wfdid="w262">
                                                            </ajaxToolkit:FilteredTextBoxExtender> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblQty" runat="server" Text='<%# eval("matqty") %>' __designer:wfdid="w260"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Amount"><EditItemTemplate>
<asp:TextBox id="tbAmount" runat="server" Width="75px" CssClass="inpText" Text='<%# eval("Harga") %>' MaxLength="10" Enabled='<%# eval("enableqty") %>' __designer:wfdid="w258"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeAmount" runat="server" TargetControlID="tbAmount" ValidChars="1234567890.," __designer:wfdid="w259">
                                                            </ajaxToolkit:FilteredTextBoxExtender> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Harga" runat="server" Text='<%# Eval("Harga") %>' __designer:wfdid="w257"></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matunit" HeaderText="Unit" ReadOnly="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unitoid" HeaderText="unitoid" ReadOnly="True" Visible="False">
<HeaderStyle ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> 
</ContentTemplate>
                                            </asp:UpdatePanel>
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="COLOR: #585858" class="Label" align="left" colspan="3">Processed By
                                                    <asp:Label ID="upduser" runat="server" Font-Bold="True"></asp:Label>&nbsp;On
                                                    <asp:Label ID="updtime" runat="server" Font-Bold="True"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="Label" align="left" colspan="3">
                                            <asp:ImageButton ID="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle"></asp:ImageButton>
                                            <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; <asp:ImageButton ID="btnPreview" runat="server" ImageAlign="AbsMiddle" AlternateText =">> Upload excel example <<" style="font-weight: 700" />
                                            &nbsp; <asp:Button runat="server" ID="ButtonA" OnClick="ButtonA_Click" Text="ButtonA" Visible="False" /></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div style="WIDTH: 100%; TEXT-ALIGN: center"></div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
     <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Stock</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Enabled="False" Value="matno">Log/Pallet No.</asp:ListItem>
<asp:ListItem Value="matcode">Code</asp:ListItem>
<asp:ListItem Value="matlongdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01" runat="server" Text="Group Item:"></asp:CheckBox> &nbsp; <asp:DropDownList id="DDLCat01" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox id="cbCat02" runat="server" Text="Subgroup Item :"></asp:CheckBox> &nbsp;&nbsp; <asp:DropDownList id="DDLCat02" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True">
                                                    </asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat03" runat="server" Text="Type Item     :" Visible="False"></asp:CheckBox> &nbsp; <asp:DropDownList id="DDLCat03" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True" Visible="False"></asp:DropDownList> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox id="cbCat04" runat="server" Text="Merk Item   :" Visible="False"></asp:CheckBox> &nbsp;&nbsp;&nbsp; <asp:DropDownList id="DDLCat04" runat="server" Width="200px" CssClass="inpText" Visible="False"></asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" Width="99%" CssClass="MyTabStyle" ForeColor="#333333" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" PageSize="5">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
                                        <asp:CheckBox ID="cbHdrLM" runat="server" AutoPostBack="true" OnCheckedChanged="cbHdrLM_CheckedChanged" />
                                    
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("matoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="100px" CssClass="inpText" Text="0" MaxLength="5" Enabled='<%# eval("enableqty") %>' __designer:wfdid="w216"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" TargetControlID="tbQty" ValidChars="1234567890.," __designer:wfdid="w217">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Init Value (IDR / ITEM)"><ItemTemplate>
<asp:TextBox id="tbHarga" runat="server" Width="120px" CssClass="inpText" Text="0" MaxLength="10" Enabled='<%# eval("enableqty") %>' __designer:wfdid="w214"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeHargaLM" runat="server" TargetControlID="tbHarga" ValidChars="1234567890.," __designer:wfdid="w215">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unitoid" HeaderText="unitoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> &nbsp;</TD></TR><TR><TD align=center colSpan=3>&nbsp;<asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllToList" runat="server" Visible="False">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground" TargetControlID="btnHideListMat">
    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" BackgroundCssClass="modalBackground" TargetControlID="bePopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

