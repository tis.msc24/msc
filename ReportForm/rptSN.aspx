﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSN.aspx.vb" Inherits="ReportForm_rptSN" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan SN"></asp:Label>
                <br />
            </th>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tbody>
                    <tr>
                        <td style="WIDTH: 979px" align="center">&nbsp;&nbsp;&nbsp;&nbsp;
                            <table style="WIDTH: 800px">
                                <tbody>
                                    <tr >
                                        <td style="VERTICAL-ALIGN: top; WIDTH: 246px" align="right" >
                                            &nbsp;</td>
                                        <td align="left" colspan="3">
                                            &nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td style="VERTICAL-ALIGN: top; WIDTH: 246px; HEIGHT: 24px" id="tdPeriod1" align="right" runat="server" visible="true">
                                            <asp:Label ID="Label6" runat="server" Text="Periode : " __designer:wfdid="w7"></asp:Label></td>
                                        <td style="HEIGHT: 24px" id="tdperiod2" align="left" colspan="3" runat="server" visible="true">
                                            <asp:TextBox ID="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w8"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp; -&nbsp;
                                            <asp:TextBox ID="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w10"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w11"></asp:ImageButton>&nbsp;<asp:Label ID="Label4" runat="server" ForeColor="Red" __designer:wfdid="w9">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" __designer:wfdid="w13" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" __designer:wfdid="w14" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender>
                                            <ajaxToolkit:MaskedEditExtender ID="meeAwal" runat="server" __designer:wfdid="w15" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender>
                                            <ajaxToolkit:MaskedEditExtender ID="meeAkhir" runat="server" __designer:wfdid="w16" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="VERTICAL-ALIGN: top; WIDTH: 246px" id="td1" align="right" runat="server" visible="true">
                                            <asp:Label ID="Label9" runat="server" Text="Branch : " __designer:wfdid="w17"></asp:Label></td>
                                        <td style="WIDTH: 126px" id="td2" align="left" runat="server" visible="true">
                                            <asp:DropDownList ID="dd_branch" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w18" AutoPostBack="True"></asp:DropDownList></td>

                                    </tr>
                                    <tr>
                                        <td style="VERTICAL-ALIGN: top; WIDTH: 246px" id="tdperiod3" align="right" runat="server" visible="true">
                                            <asp:Label ID="Label1" runat="server" Text="Lokasi : " __designer:wfdid="w17"></asp:Label></td>
                                        <td style="WIDTH: 126px" id="tdperiod4" align="left" runat="server" visible="true">
                                            <asp:DropDownList ID="ddlLocation" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w18" AutoPostBack="True"></asp:DropDownList></td>
                                        <td id="Td3" style="VERTICAL-ALIGN: top; WIDTH: 246px" align="right" runat="server" visible="false">&nbsp;</td>
                                        <td id="Td4" style="VERTICAL-ALIGN: top; WIDTH: 286px" align="left" runat="server" visible="true">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="VERTICAL-ALIGN: top; WIDTH: 246px" align="right">
                                            <asp:Label ID="Label3" runat="server" Text="Item / Barang :" __designer:wfdid="w22"></asp:Label></td>
                                        <td style="VERTICAL-ALIGN: top" align="left" colspan="3">
                                            <asp:TextBox ID="itemname" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w23"></asp:TextBox>&nbsp;<asp:ImageButton ID="btnSearchItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w24" style="height: 16px"></asp:ImageButton>&nbsp;<asp:ImageButton ID="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w25"></asp:ImageButton>&nbsp;&nbsp;<asp:Label ID="itemoid" runat="server" __designer:wfdid="w26" Visible="False"></asp:Label>&nbsp;&nbsp;<br />
                                            <asp:GridView ID="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w27" BorderColor="#DEDFDE" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3" PageSize="12" UseAccessibleHeader="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" EnableModelValidation="True">
                                                <RowStyle BackColor="#F7F7DE"></RowStyle>
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

                                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="itemcode" HeaderText="Kode Barang"></asp:BoundField>
                                                    <asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang"></asp:BoundField>
                                                    <asp:BoundField DataField="Merk" HeaderText="Merk"></asp:BoundField>
                                                    <asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False"></asp:BoundField>
                                                    <asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False"></asp:BoundField>
                                                    <asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False"></asp:BoundField>
                                                    <asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False"></asp:BoundField>
                                                    <asp:BoundField DataField="satuan3" HeaderText="Sat Std"></asp:BoundField>
                                                    <asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False"></asp:BoundField>
                                                    <asp:BoundField DataField="branch_code" HeaderText="branch" />
                                                </Columns>

                                                <PagerStyle HorizontalAlign="Right"></PagerStyle>
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                </EmptyDataTemplate>

                                                <SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

                                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 979px"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 979px; HEIGHT: 32px" align="center">
                            <asp:ImageButton ID="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton>
                            <asp:ImageButton ID="ibPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton>
                            <asp:ImageButton ID="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton>&nbsp;
                            <asp:ImageButton ID="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton>&nbsp;
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" __designer:wfdid="w38">
                                <ProgressTemplate>
                                    <strong><span style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait....</span></strong><br />
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w39"></asp:Image>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 979px" align="center">
                            <asp:Label ID="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w40"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 979px" align="center">
                            <CR:CrystalReportViewer ID="crv" runat="server" __designer:wfdid="w42" AutoDataBind="true" DisplayGroupTree="False" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False" HasZoomFactorList="False" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ibPDF"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

