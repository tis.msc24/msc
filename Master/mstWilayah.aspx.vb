Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction


Partial Class Master_mstWilayah
    Inherits System.Web.UI.Page


#Region "Variables"
    Dim dttemp As New DataTable
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim cProc As New ClassProcedure
    Dim dv As DataView
    Dim sSQL As String
    Dim cKoneksi As New Koneksi
#End Region

#Region "Function"
    Private Function SetTableDetail() As DataTable
        Dim dtlTable As DataTable = New DataTable("TblDtlregion")
        dtlTable.Columns.Add("seq", Type.GetType("System.String"))
        dtlTable.Columns.Add("areacode", Type.GetType("System.String"))
        dtlTable.Columns.Add("areadesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("areaoid", Type.GetType("System.Int32"))
        'dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        'dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Return dtlTable
    End Function
#End Region

#Region "Procedure"
    Public Sub generateno()
        Dim sNo As String = "REG"
        'sSql = "SELECT ISNULL(MAX(CAST(left(prno,3) AS INTEGER))+1,1) AS IDNEW FROM ql_prmst " & _
        '    "WHERE cmpcode='" & CompnyCode & "' AND prno LIKE '%" & sNo & "' and substring(prno,5,12) like '" & divisi & "/" & romawi & sNo & "%' "
        sSQL = "SELECT ISNULL(MAX(CAST(right(regioncode,3) AS INTEGER))+1,1) AS IDNEW FROM Ql_mstregion " & _
            "WHERE cmpcode='" & CompnyCode & "' and regioncode like '" & sNo & "%' "

        regioncode.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSQL), 3)
    End Sub

    Private Sub FilterGV(ByVal ssql As String)
        ssql = "SELECT regioncode, regionoid, regionname, regiondesc FROM Ql_mstregion where cmpcode = '" & CompnyCode & "' " & ssql
        GVmst.DataBind()
        Dim dtab As DataTable = cKoneksi.ambiltabel(ssql, "Ql_mstregion")
        GVmst.DataSource = dtab
        GVmst.DataBind() : Session("Region") = dtab
    End Sub

    Private Sub InitDDL()
        sSQL = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='CITY' and genoid not in (select areaoid from QL_mstregiondtl) order by gendesc"
        FillDDL(dllarea, sSQL)

    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") : Dim userName As String = Session("UserName")  ' simpan session k variabel spy tidak hilang
            'Dim access As String = Session("Access")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserName") = userName
            Session("Role") = xsetRole
            'Session.Clear() : Session("UserName") = userName   '  clear all session
            Session("UserID") = userId 'insert lagi sesion yg disimpan dan create session lagi
            'Session("Access") = access
            Session("SpecialAccess") = xsetAcc
            Response.Redirect("mstwilayah.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Data Region" '
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete?');")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "UPDATE"
            i_u.Visible = False : lblUpd.Text = "Last Updated"
        Else
            i_u.Text = "N E W"
            i_u.Visible = False : lblUpd.Text = "Created"
        End If

        If Not IsPostBack Then
            FilterGV("")
            InitDDL()
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                generateno()
                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
                TabContainer1.ActiveTabIndex = 0
                btnDelete.Visible = False
            Else

                sSQL = "SELECT regionoid,regioncode,regionname,regiondesc, tarif, upduser,updtime FROM QL_mstregion WHERE cmpcode='" & CompnyCode & "' AND regionoid=" & Session("oid")
                dttemp = cKoneksi.ambiltabel(sSQL, "QL_mstregion")
                If dttemp.Rows.Count > 0 Then
                    regioncode.Text = dttemp.Rows(0)("regioncode").ToString
                    regionname.Text = dttemp.Rows(0)("regionname").ToString
                    regiondesc.Text = dttemp.Rows(0)("regiondesc").ToString
                    txtTarif.Text = dttemp.Rows(0)("tarif").ToString
                    upduser.Text = dttemp.Rows(0)("upduser").ToString
                    updtime.Text = Format(GetServerTime(), "dd/MM/yyyy, HH:mm:ss")
                End If

                sSQL = "SELECT seq, areaoid, gencode areacode, gendesc areadesc  FROM QL_mstregiondtl inner join QL_mstgen on genoid = areaoid WHERE QL_mstregiondtl.cmpcode='" & CompnyCode & "' AND regionoid=" & Session("oid") & " ORDER BY seq"
                dttemp = cKoneksi.ambiltabel(sSQL, "QL_mstregiondtl")
                Session("QL_mstregiondtl") = dttemp
                TabContainer1.ActiveTabIndex = 1
                btnDelete.Visible = True

            End If
            ' IF HAVE TABLE DETAIL, RE-BIND ON POSTBACK
            Dim dtlTbl As DataTable : dtlTbl = Session("QL_mstregiondtl")
            GVRole.DataSource = dtlTbl : GVRole.DataBind()
            'If Not (dtlTbl Is Nothing) Then
            '    imbRemoveDtl.Visible = (dtlTbl.Rows.Count > 0)
            'End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        FilterGV(" and " & FilterDDL.Text & " like '%" & Tchar(FilterText.Text) & "%'")
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterGV("")
        FilterDDL.SelectedIndex = 0
        FilterText.Text = "" : GVmst.PageIndex = 0
    End Sub

    Protected Sub imbOKValidasi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If lblPopUpMsg.Text = "Data telah disimpan !" Or lblPopUpMsg.Text = "Data telah dihapus !" Then
            Me.Response.Redirect("~/Master/mstRole.aspx?awal=true")
        End If
    End Sub

    Protected Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim msg As String = ""
        'If formaddress.Text.Trim = "" Then
        '    msg = "Please Make Group 'FORMNAME' in Master General first"
        '    ModalPopupExtender3.Show()
        '    Exit Sub
        'End If
        If dllarea.Items.Count = 0 Then
            showMessage("semua kota sudah ditambahkan dalam region !!", 2)
            Exit Sub
        End If
        If Session("QL_mstregiondtl") Is Nothing Then
            Dim dtlNew As DataTable = SetTableDetail()
            Session("QL_mstregiondtl") = dtlNew
        End If

        Dim objTable As DataTable : objTable = Session("QL_mstregiondtl")
        Dim objRow As DataRow

        Dim dvtemp As DataView = objTable.DefaultView
        dvtemp.RowFilter = "areaoid = '" & dllarea.SelectedValue & "'"
        If dvtemp.Count > 0 Then
            showMessage("Data sudah ditambahkan sebelumnya !", 2)
            'msg = "Data has been added before."
            Exit Sub
        End If
        dvtemp.RowFilter = ""

        If I_U5.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("seq") = objTable.Rows.Count + 1
        Else 'update
            objRow = objTable.Rows(GVRole.SelectedIndex)
            objRow.BeginEdit()
        End If

        objRow("areacode") = GetStrData("select gencode from Ql_mstgen where genoid = " & dllarea.SelectedValue)
        objRow("areadesc") = dllarea.SelectedItem.Text
        objRow("areaoid") = dllarea.SelectedValue


        If I_U5.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
        End If
        Session("QL_mstregiondtl") = objTable
        GVRole.DataSource = objTable
        GVRole.DataBind()

        I_U5.Text = "New Detail"
        'regiondesc.Text = ""
        '    regionname.Text = ""
        '    txtTarif.Text = ""
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim sMsg As String = ""
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(Tchar(regionname.Text), "regionname", "Ql_mstregion") Then
                sMsg &= " - Nama Region sudah ada ! Tolong gunakan Nama lain !<BR>"
            End If
        Else

        End If
        If regionname.Text.Trim = "" Then
            sMsg &= "- Isi Nama Region !!<BR>"
        End If
        If regiondesc.Text.Trim = "" Then
            sMsg &= "- Isi Deskripsi !!<BR>"
        End If
        'cek apa sudah ada Role Detail
        If txtTarif.Text.Trim = "" Then
            sMsg &= "- Isi Tarif !!<BR>"
        End If
        If Session("QL_mstregiondtl") Is Nothing Then
            sMsg &= "- Isi Detail Region dahulu !!<BR>"
        Else
            Dim objTableCek As DataTable = Session("QL_mstregiondtl")
            If objTableCek.Rows.Count = 0 Then
                sMsg &= "- Isi Detail Region dahulu !!<BR>"
            End If
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        generateno()
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Connection = conn
        objCmd.Transaction = objTrans

        Try
            Session("mstoid") = GenerateID("Ql_mstregion", CompnyCode)
            Session("dtloid") = GenerateID("Ql_mstregiondtl", CompnyCode)
            If Session("oid") = Nothing Or Session("oid") = "" Then
                ' INSERT
                sSQL = "INSERT INTO Ql_mstregion (cmpcode,regionoid,regioncode,regionname,regiondesc,createuser, createtime,upduser,updtime,tarif) " & _
                    "VALUES ('" & CompnyCode & "'," & Session("mstoid") & ",'" & regioncode.Text & "','" & Tchar(regionname.Text) & "','" & Tchar(regiondesc.Text) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDouble(txtTarif.Text.Trim) & "')"
                objCmd.CommandText = sSQL : objCmd.ExecuteNonQuery()
                If Not Session("QL_mstregiondtl") Is Nothing Then
                    Dim objTable As DataTable : objTable = Session("QL_mstregiondtl")
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSQL = "INSERT INTO Ql_mstregiondtl (cmpcode,regiondtloid,regionoid,seq,areaoid) " & _
                            "VALUES ('" & CompnyCode & "'," & Session("dtloid") + C1 & "," & Session("mstoid") & ",'" & Tchar(objTable.Rows(C1)("seq").ToString) & "','" & Tchar(objTable.Rows(C1)("areaoid").ToString) & "')"
                        objCmd.CommandText = sSQL : objCmd.ExecuteNonQuery()
                    Next
                    sSQL = "Update QL_MSTOID set LASTOID=" & (objTable.Rows.Count - 1 + Session("dtloid")) & " Where tablename = 'Ql_mstregiondtl' "
                    objCmd.CommandText = sSQL : objCmd.ExecuteNonQuery()
                End If
                sSQL = "UPDATE QL_MSTOID SET LASTOID = " & Session("mstoid") & " WHERE TABLENAME = 'Ql_mstregion' "
                objCmd.CommandText = sSQL : objCmd.ExecuteNonQuery()
            Else
                ' UPDATE
                sSQL = "UPDATE Ql_mstregion SET regionname='" & Tchar(regionname.Text) & "',regiondesc='" & Tchar(regiondesc.Text) & _
                    "',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP, tarif='" & ToDouble(txtTarif.Text.Trim) & "' WHERE cmpcode='" & CompnyCode & _
                    "' AND regionoid=" & Session("oid")
                objCmd.CommandText = sSQL : objCmd.ExecuteNonQuery()
                ' DELETE Old Data
                sSQL = "DELETE FROM Ql_mstregiondtl WHERE cmpcode='" & CompnyCode & "' AND regionoid=" & Session("oid")
                objCmd.CommandText = sSQL : objCmd.ExecuteNonQuery()
                If Not Session("QL_mstregiondtl") Is Nothing Then
                    Dim objTable As DataTable : objTable = Session("QL_mstregiondtl")
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSQL = "INSERT INTO Ql_mstregiondtl (cmpcode,regiondtloid,regionoid,seq,areaoid) " & _
                            "VALUES ('" & CompnyCode & "'," & Session("dtloid") + C1 & "," & Session("oid") & ",'" & Tchar(objTable.Rows(C1)("seq").ToString) & "','" & Tchar(objTable.Rows(C1)("areaoid").ToString) & "')"
                        objCmd.CommandText = sSQL : objCmd.ExecuteNonQuery()

                    Next
                    sSQL = "update QL_MSTOID set LASTOID=" & (objTable.Rows.Count - 1 + Session("dtloid")) & " where tablename = 'Ql_mstregiondtl' "
                    objCmd.CommandText = sSQL : objCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, 1) : Exit Sub
        End Try
        Response.Redirect("~/Master/mstwilayah.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click

        Response.Redirect("~/Master/mstwilayah.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click

        If Session("oid") = Nothing Or Session("oid") = "" Then
            showMessage("Pilih Region !!", 2) : Exit Sub

        End If

        'cek relationship database to table
        Dim sColomnName() As String = {"outputoid"}
        Dim sTable() As String = {"QL_trnspkProcessItemResult"}

        If CheckDataExists(Session("oid"), sColomnName, sTable) = True Then
            showMessage("Tidak dapat dihapus karena digunakan di tabel lain!!", 2)
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSQL = "SELECT count(-1) FROM QL_trnspkProcessItemResult where cmpcode='" & CompnyCode & "' and outputref = 'QL_MSTREGION' and outputoid =" & Session("oid")
        xCmd.CommandText = sSQL
        If xCmd.ExecuteScalar > 0 Then
            showMessage("Tidak dapat dihapus karena digunakan di tabel lain!!", 2)
            conn.Close()
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Connection = conn
        objCmd.Transaction = objTrans
        Try
            sSQL = "DELETE FROM QL_mstregion WHERE cmpcode='" & CompnyCode & "' AND regionoid='" & Session("oid") & "'"
            objCmd.CommandText = sSQL
            objCmd.ExecuteNonQuery()

            sSQL = "DELETE FROM QL_mstregiondtl WHERE cmpcode='" & CompnyCode & "' AND regionoid='" & Session("oid") & "'"
            objCmd.CommandText = sSQL
            objCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, 1) : Exit Sub
        End Try
        Response.Redirect("~/Master/mstwilayah.aspx?awal=true")
        'If cKoneksi.ambilscalar("SELECT COUNT(-1) FROM QL_userrole WHERE cmpcode='" & CompnyCode & "' AND roleoid='" & Session("oid") & "'") > 0 Then
        '    showMessage("Cannot delete Role, it has been used in Data User Role !!", 2) : Exit Sub
        'End If

        'Dim objTrans As SqlClient.SqlTransaction
        'Dim objCmd As New SqlClient.SqlCommand
        'If conn.State = ConnectionState.Closed Then
        '    conn.Open()
        'End If
        'objTrans = conn.BeginTransaction()
        'objCmd.Connection = conn
        'objCmd.Transaction = objTrans
        'Try
        '    sSQL = "DELETE FROM QL_ROLEDTL WHERE cmpcode='" & CompnyCode & "' AND roleoid='" & Session("oid") & "'"
        '    objCmd.CommandText = sSQL
        '    objCmd.ExecuteNonQuery()

        '    sSQL = "DELETE FROM QL_ROLE WHERE cmpcode='" & CompnyCode & "' AND roleoid='" & Session("oid") & "'"
        '    objCmd.CommandText = sSQL
        '    objCmd.ExecuteNonQuery()
        '    objTrans.Commit() : conn.Close()
        'Catch ex As Exception
        '    objTrans.Rollback() : conn.Close()
        '    showMessage(ex.Message, 1) : Exit Sub
        'End Try
        'showMessage("Data has been deleted !", 3)
        'Response.Redirect("~/Master/mstRole.aspx?awal=true")
    End Sub

    Protected Sub GVRole_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVRole.RowDeleting
        Dim objTable As DataTable = Session("QL_mstregiondtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))

        'resequence po detail ID + sequenceDtl 
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            'MsgBox(dr(3))
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("QL_mstregiondtl") = objTable
        'tbldtl.Visible = True
        GVRole.DataSource = objTable
        GVRole.DataBind()

    End Sub

    Protected Sub GVmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmst.PageIndexChanging
        GVmst.PageIndex = e.NewPageIndex
        FilterGV(" and " & FilterDDL.Text & " like '%" & Tchar(FilterText.Text) & "%'")
    End Sub

    Protected Sub btnDelete_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click

    End Sub
#End Region
End Class
