<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptDPAP.aspx.vb" Inherits="ReportForm_RptDPAP" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Down Payment A/P Report" CssClass="Title" ForeColor="SaddleBrown" Width="204px"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel id="upReportForm" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" DefaultButton="btnSearchSupplier"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD id="TD2" align=left runat="server" Visible="false"><asp:DropDownList id="FilterDDLYear" runat="server" CssClass="inpText" __designer:wfdid="w38"></asp:DropDownList></TD><TD id="TD3" class="Label" align=center runat="server" Visible="false">:</TD><TD id="Td7" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w39">
            </asp:DropDownList> <asp:DropDownList id="FilterType" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True" __designer:wfdid="w40"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem Selected="True">DETAIL</asp:ListItem>
</asp:DropDownList> <asp:Label id="Label7" runat="server" Text="Type" Visible="False" __designer:wfdid="w41"></asp:Label> <asp:DropDownList id="FilterDDLMonth" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w42"></asp:DropDownList> </TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w43"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left colSpan=2><asp:TextBox id="txtStart" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w44"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w46"></asp:Label> <asp:TextBox id="txtFinish" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w47"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w48"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(MM/DD/yyyy)" __designer:wfdid="w49"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="Supplier" __designer:wfdid="w50"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left colSpan=2><asp:RadioButtonList id="rbSupplier" runat="server" Width="128px" AutoPostBack="True" RepeatDirection="Horizontal" __designer:wfdid="w51"><asp:ListItem Selected="True">ALL</asp:ListItem>
<asp:ListItem>SELECT</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD align=left colSpan=2><asp:TextBox id="FilterTextSupplier" runat="server" Width="150px" CssClass="inpText" Visible="False" __designer:wfdid="w52"></asp:TextBox> <asp:ImageButton id="btnSearchSupplier" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="17px" __designer:wfdid="w53"></asp:ImageButton> <asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w54"></asp:ImageButton></TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD align=left colSpan=2><asp:CheckBox id="chkEmpty" runat="server" Text="Hide Supplier with 0 DP A/R amount" Checked="True" __designer:wfdid="w55"></asp:CheckBox> <asp:Label id="custoid" runat="server" Visible="False" __designer:wfdid="w56"></asp:Label></TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD align=left colSpan=2><asp:Panel id="pnlSupplier" runat="server" __designer:wfdid="w57"><asp:GridView id="gvCustomer" runat="server" ForeColor="#333333" Visible="False" __designer:wfdid="w58" PageSize="6" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="custoid,custname">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><EditItemTemplate>
<asp:CheckBox id="CheckBox1" runat="server"></asp:CheckBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("custoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD id="TD6" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Width="96px" Text="Currency Value" __designer:wfdid="w59"></asp:Label></TD><TD id="TD4" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD5" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText" __designer:wfdid="w60"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w61"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w62"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w63"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w64"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4><ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w65" Format="dd/MM/yyyy" PopupButtonID="imbStart" TargetControlID="txtStart"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" __designer:wfdid="w66" TargetControlID="txtStart" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceFin" runat="server" __designer:wfdid="w67" Format="dd/MM/yyyy" PopupButtonID="imbFinish" TargetControlID="txtFinish"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" __designer:wfdid="w68" TargetControlID="txtFinish" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD vAlign=top align=center colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w69" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w70"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 226px">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

