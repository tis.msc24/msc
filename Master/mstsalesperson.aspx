<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstsalesperson.aspx.vb" Inherits="Master_mstsalesperson" %>

<%--
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit, Version=1.0.11119.20010, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <script type="text/javascript">
        function s() {
            //    try {
            //        var t = document.getElementById("<%=GVmstperson.ClientID%>");
        //        var t2 = t.cloneNode(true)
        //        for(i = t2.rows.length -1;i > 0;i--)
        //        t2.deleteRow(i)
        //        t.deleteRow(0)
        //        divTblData.appendChild(t2)
        //        }
        //    catch(errorInfo) {}
        //    
    }
        window.onload = s
    </script>
    <table id="Table2" bgcolor="white" border="0" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <td align="left" class="header" colspan="2" style="background-color: lightgrey" valign="center">
                <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Master User ID Karyawan"></asp:Label></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanelSearch" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE><TBODY><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=5><asp:DropDownList id="FilterDDL" runat="server" Width="96px" CssClass="inpText" OnSelectedIndexChanged="FilterDDL_SelectedIndexChanged">
                                                            <asp:ListItem Value="personnip">Karyawan Code</asp:ListItem>
                                                            <asp:ListItem Value="personname">Karyawan Name</asp:ListItem>
                                                        </asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="128px" CssClass="inpText" MaxLength="30"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton><asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnPostSelected" onclick="BtnPostSelected_Click" runat="server" ImageUrl="~/Images/save.png" ImageAlign="AbsMiddle" __designer:wfdid="w3" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left><asp:CheckBox id="chkPosition" runat="server" Width="85px" Text="Position"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=5><asp:DropDownList id="FilterPosition" runat="server" Width="230px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=5><asp:DropDownList id="StsDDL" runat="server" CssClass="inpText" __designer:wfdid="w43"><asp:ListItem Value="ALL"></asp:ListItem>
<asp:ListItem>AKTIF</asp:ListItem>
<asp:ListItem>IN AKTIF</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:CheckBox id="chkArea" runat="server" Width="85px" Text="Sales Area" Visible="False"></asp:CheckBox></TD><TD align=left></TD><TD align=left colSpan=5><asp:DropDownList id="FilterArea" runat="server" Width="230px" CssClass="inpText" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=7><asp:GridView id="GVmstperson" runat="server" Width="950px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" EmptyDataText="Data not found!" AllowPaging="True" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="personoid" DataNavigateUrlFormatString="~\master\mstsalesperson.aspx?oid={0}" DataTextField="personnip" HeaderText="Karyawan Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="personname" HeaderText="Karyawan Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbStatus" runat="server" ToolTip='<%# eval("personoid") %>' Checked='<%# eval("selected") %>' __designer:wfdid="w4"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="No Data Found !"></asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img alt="" src="../Images/corner.gif" align="absMiddle" />
                            <span style="FONT-SIZE: 9pt"><strong> List of User ID Karyawan</strong></span> <strong>
                                <span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=4><asp:MultiView id="MultiView1" runat="server"><asp:View id="View1" runat="server"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 105px" class="Label" align=left colSpan=6><asp:Label id="Label3" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information" Visible="False"></asp:Label><asp:Label id="Label4" runat="server" Width="4px" ForeColor="#585858" Text="|" Visible="False"></asp:Label><asp:LinkButton id="lbv12" onclick="lbv12_Click" runat="server" Width="119px" CssClass="submenu" Font-Size="Small" Font-Bold="False" Visible="False">More Information</asp:LinkButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 105px" class="Label" align=left colSpan=6><asp:Button id="btnGetPrs" onclick="btnGetPrs_Click" runat="server" Text="Get Data Person" Visible="False" BorderColor="Maroon" BorderWidth="1px"></asp:Button></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Karyawan Code <asp:Label id="Label7" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="salescode" runat="server" Width="88px" CssClass="inpTextDisabled" ReadOnly="True" MaxLength="10"></asp:TextBox> <asp:Label id="personoid" runat="server" CssClass="Important" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Employee&nbsp;Status</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="status" runat="server" Width="121px" CssClass="inpText">
                                                                                <asp:ListItem Value="Permanent">Permanent</asp:ListItem>
                                                                                <asp:ListItem Value="Contract">Contract</asp:ListItem>
                                                                            </asp:DropDownList> <asp:Label id="lblstatus" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Karyawan Name <asp:Label id="Label8" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="salesname" runat="server" Width="256px" CssClass="inpText" Text="" MaxLength="50" OnTextChanged="salesname_TextChanged"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Join Date <asp:Label id="Label1" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="tglmasuk" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label11" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalendar2" Enabled="True" TargetControlID="tglmasuk">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate1" runat="server" Enabled="True" TargetControlID="tglmasuk" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>ID Card No.</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="noktp" runat="server" Width="256px" CssClass="inpText" Text="" MaxLength="25"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FTEIDCard" runat="server" Enabled="True" TargetControlID="noktp" ValidChars="0123456789."></ajaxToolkit:FilteredTextBoxExtender> </TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>City &amp; Date of Birth <asp:Label id="Label10" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="tempatlahir" runat="server" Width="144px" CssClass="inpText" Font-Size="X-Small"></asp:DropDownList><BR /><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalendar" Enabled="True" TargetControlID="tgllahir">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate3" runat="server" Enabled="True" TargetControlID="tgllahir" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Gender</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="salessex" runat="server" Width="144px" CssClass="inpText" Font-Size="X-Small">
                                                                                <asp:ListItem>Male</asp:ListItem>
                                                                                <asp:ListItem>Female</asp:ListItem>
                                                                            </asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="tgllahir" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Marital Status</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="maritalstatus" runat="server" Width="144px" CssClass="inpText" Font-Size="X-Small">
                                                                                <asp:ListItem>Single</asp:ListItem>
                                                                                <asp:ListItem>Married</asp:ListItem>
                                                                                <asp:ListItem>Widow</asp:ListItem>
                                                                                <asp:ListItem>Widower</asp:ListItem>
                                                                            </asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>Phone 1 <asp:Label id="Label5" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="phone1" runat="server" Width="208px" CssClass="inpText" Font-Size="X-Small" Text="" MaxLength="20"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="FTEphone1" runat="server" Enabled="True" TargetControlID="phone1" ValidChars="1234567890 ()+"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEphone2" runat="server" Enabled="True" TargetControlID="phone2" ValidChars="1234567890 ()+"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>Religion</TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>:</TD><TD style="VERTICAL-ALIGN: top" vAlign=top align=left><asp:DropDownList id="religion" runat="server" Width="144px" CssClass="inpText">
                                                                                <asp:ListItem Value="Islam">Islam</asp:ListItem>
                                                                                <asp:ListItem Value="Christian">Kristen</asp:ListItem>
                                                                                <asp:ListItem Value="Catholic">Katolik</asp:ListItem>
                                                                                <asp:ListItem Value="Hindu">Hindu</asp:ListItem>
                                                                                <asp:ListItem Value="Budha">Budha</asp:ListItem>
                                                                                <asp:ListItem Value="Konghucu">Konghucu</asp:ListItem>
                                                                            </asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>Phone 2</TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="phone2" runat="server" Width="208px" CssClass="inpText" Font-Size="X-Small" Text="" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>&nbsp;</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>Current Address <asp:Label id="Label6" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="alamattinggal" runat="server" Width="256px" CssClass="inpText" Text="" MaxLength="50"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Email</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="email" runat="server" Width="208px" CssClass="inpText" Font-Size="X-Small" Text="" MaxLength="50"></asp:TextBox><BR /><asp:Label id="Label22" runat="server" CssClass="Important" Text="Ex : mail@sample.com"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="citytinggaloid" runat="server" Width="144px" CssClass="inpText" Font-Size="X-Small"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Last Education</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="pendidikanterakhir" runat="server" Width="160px" CssClass="inpText" Text="" MaxLength="10"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>Original Address</TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="alamatasal" runat="server" Width="256px" CssClass="inpText" Text="" MaxLength="50"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Name of School</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="asalsekolah" runat="server" Width="256px" CssClass="inpText" Text="" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="asalcityoid" runat="server" Width="144px" CssClass="inpText" Font-Size="X-Small"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>City</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="sekolahcityoid" runat="server" Width="144px" CssClass="inpText" Font-Size="X-Small"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="asalprovince" runat="server" Width="144px" CssClass="inpText" Font-Size="X-Small" Visible="False"></asp:DropDownList> <asp:DropDownList id="crtprovince" runat="server" Width="144px" CssClass="inpText" Font-Size="X-Small" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left><asp:Label id="Label26" runat="server" Text="Emergency Phone" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" vAlign=top align=left></TD><TD style="VERTICAL-ALIGN: top" vAlign=top align=left><asp:TextBox id="emergencyphone" runat="server" Width="208px" CssClass="inpText" Font-Size="X-Small" Text="" Visible="False" MaxLength="20"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTEemergencyphone" runat="server" Enabled="True" TargetControlID="emergencyphone" ValidChars="1234567890 ()+"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="catatanlainlain" runat="server" Width="256px" Height="64px" CssClass="inpText" Font-Size="8pt" Text="" MaxLength="50" TextMode="MultiLine"></asp:TextBox><BR /><asp:Label id="Label12" runat="server" CssClass="Important" Text="* 50 characters"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:GridView id="gvWil" runat="server" Visible="False" AutoGenerateColumns="False">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="salesarea" HeaderText="Territory">
                                                                                        <ControlStyle Width="30px"></ControlStyle>

                                                                                        <HeaderStyle Width="30px"></HeaderStyle>

                                                                                        <ItemStyle Width="22px"></ItemStyle>
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField HeaderText="Authority">
                                                                                        <EditItemTemplate>
                                                                                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("b") %>' />

                                                                                        </EditItemTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# returnAvailable(DataBinder.Eval(Container.DataItem, "itemavailable")) %>'
                                                                                                OnCheckedChanged="CheckBox2_CheckedChanged" />

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="genoid" HeaderText="ID" Visible="False">
                                                                                        <ItemStyle ForeColor="White"></ItemStyle>
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                            </asp:GridView> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Status</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText"><asp:ListItem Value="AKTIF">Active</asp:ListItem>
<asp:ListItem Value="IN AKTIF">Not Active</asp:ListItem>
</asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label16" runat="server" Text="Position"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="position" runat="server" Width="192px" CssClass="inpText" Font-Size="X-Small"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label24" runat="server" Text="Work Contract" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="amtcontract" runat="server" Width="32px" CssClass="inpTextDisabled" Font-Size="X-Small" Text="" Visible="False" MaxLength="2" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label25" runat="server" Text="Month(s)" Visible="False"></asp:Label> <ajaxToolkit:MaskedEditExtender id="meeAmtcontract" runat="server" TargetControlID="amtcontract" MaskType="Number" Mask="99" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender> </TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label18" runat="server" Text="Ceritificates / diplomas (1)" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="ijazah1" runat="server" Width="256px" CssClass="inpText" Text="" Visible="False" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Ceritificates / diplomas (2)" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="ijazah2" runat="server" Width="256px" CssClass="inpText" Text="" Visible="False" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label15" runat="server" Text="Region" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="regionoid" runat="server" CssClass="inpText" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label21" runat="server" Text="Ceritificates / diplomas (3)" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:TextBox id="ijazah3" runat="server" Width="256px" CssClass="inpText" Text="" Visible="False" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:TextBox id="salesoid" runat="server" Width="25px" CssClass="inpText" Text="" Visible="False" ReadOnly="True" Enabled="False"></asp:TextBox><asp:Label id="i_u" runat="server" CssClass="Important" Text="New" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:Label id="lblpersonpicture" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label17" runat="server" Text="Sales Area" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:DropDownList id="salesarea" runat="server" Width="192px" CssClass="inpText" Font-Size="X-Small" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="salesarea_SelectedIndexChanged"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Images</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left><asp:FileUpload id="matpictureloc" runat="server" CssClass="inpText" Font-Size="X-Small" Font-Overline="False"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnUpload" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left><asp:Image id="prspicture" runat="server" Width="77px" ImageUrl="~/Images/no-image.png" Height="77px" BorderColor="#999999" BorderWidth="1px" BorderStyle="Solid"></asp:Image><asp:LinkButton id="lkbFullView" runat="server" Visible="False">[ Full View ]</asp:LinkButton><asp:Label id="lblMenuPic" runat="server" Font-Size="8pt" Font-Names="Arial" ForeColor="Red" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:Panel id="PanelUpdate" runat="server" Width="100%" __designer:dtid="562949953421342" __designer:wfdid="w7">Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="562949953421343" Text="[Updtime]" __designer:wfdid="w8"></asp:Label> &nbsp;by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="562949953421344" Text="[upduser]" __designer:wfdid="w9"></asp:Label> </asp:Panel></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:ImageButton style="HEIGHT: 23px" id="btnSave" tabIndex=39 runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:dtid="562949953421347" __designer:wfdid="w10"></asp:ImageButton> <asp:ImageButton id="BtnCancel" tabIndex=40 runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:dtid="562949953421348" __designer:wfdid="w11"></asp:ImageButton> <asp:ImageButton id="BtnDelete" tabIndex=41 runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421349" CommandName="Delete" __designer:wfdid="w12"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server"><asp:LinkButton id="lbv21" onclick="lbv21_Click" runat="server" CssClass="submenu" Font-Size="Small" Font-Bold="False">Information</asp:LinkButton><asp:Label id="Label9" runat="server" ForeColor="#585858" Text="|"></asp:Label><asp:Label id="Label13" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="More Information"></asp:Label><BR /><TABLE><TBODY><TR><TD><asp:Label id="txtCompName" runat="server" Width="98px" Text="Company Name"></asp:Label></TD><TD style="WIDTH: 100%"><asp:TextBox id="companyname" runat="server" Width="241px" CssClass="inpText" MaxLength="50"></asp:TextBox> <asp:Label id="historyseq" runat="server" Text="HistorySeq" Visible="False"></asp:Label> <asp:Label id="I_u2" runat="server" CssClass="Important">New Detail</asp:Label></TD></TR><TR><TD>Position</TD><TD style="WIDTH: 3px"><asp:TextBox id="divisi" runat="server" Width="240px" CssClass="inpText" MaxLength="50"></asp:TextBox> </TD></TR><TR><TD>Country</TD><TD style="WIDTH: 3px"><asp:DropDownList id="ddlDtlCountry" runat="server" Width="244px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList>&nbsp; </TD></TR><TR><TD>Province</TD><TD style="WIDTH: 3px"><asp:DropDownList id="ddlDtlProvince" runat="server" Width="244px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD>City</TD><TD style="WIDTH: 3px"><asp:DropDownList id="ddlDtlCity" runat="server" Width="244px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD>Time Period</TD><TD><asp:TextBox id="startperiod" runat="server" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnCalendar3" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;to <asp:TextBox id="endperiod" runat="server" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar4" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label14" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label><BR /><ajaxToolkit:MaskedEditExtender id="meestart" runat="server" TargetControlID="startperiod" UserDateFormat="DayMonthYear" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceStart" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalendar3" TargetControlID="startperiod"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceEnd" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalendar4" TargetControlID="endperiod"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeEnd" runat="server" TargetControlID="endperiod" UserDateFormat="DayMonthYear" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD></TD><TD><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:GridView id="tbldtl" runat="server" Width="98%" Font-Size="X-Small" BackColor="White" Visible="False" BorderColor="#DEDFDE" BorderWidth="1px" AutoGenerateColumns="False" BorderStyle="Solid" DataKeyNames="historyseq,salesoid,compname,position,City,Province,Country,startperiod,endperiod" CellPadding="4">
                                                                                <RowStyle BackColor="#F7F7DE" />
                                                                                <Columns>
                                                                                    <asp:CommandField SelectImageUrl="~/picture/select.gif" SelectText="Show" ShowSelectButton="True">
                                                                                        <HeaderStyle Font-Size="X-Small" Width="50px" />
                                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="50px" />
                                                                                    </asp:CommandField>
                                                                                    <asp:BoundField DataField="historyseq" HeaderText="No.">
                                                                                        <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="50px" />
                                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="50px" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="compname" HeaderText="Company Name">
                                                                                        <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="300px" />
                                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="300px" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="position" HeaderText="Position">
                                                                                        <HeaderStyle Font-Size="X-Small" HorizontalAlign="Right" Width="100px" />
                                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" Width="100px" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="City" HeaderText="City">
                                                                                        <HeaderStyle Font-Size="X-Small" HorizontalAlign="Right" Width="100px" />
                                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" Width="100px" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="Province" HeaderText="Province">
                                                                                        <HeaderStyle Font-Size="X-Small" HorizontalAlign="Right" Width="100px" />
                                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" Width="100px" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="Country" HeaderText="Country">
                                                                                        <HeaderStyle Font-Size="X-Small" />
                                                                                        <ItemStyle Font-Size="X-Small" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="startperiod" HeaderText="Join Date">
                                                                                        <HeaderStyle Font-Size="X-Small" />
                                                                                        <ItemStyle Font-Size="X-Small" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="endperiod" HeaderText="Retire Date" />
                                                                                    <asp:CommandField ShowDeleteButton="True" />
                                                                                </Columns>
                                                                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                                <EmptyDataTemplate>
                                                                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="No Person History !!"></asp:Label>
                                                                                </EmptyDataTemplate>
                                                                                <SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                <HeaderStyle BackColor="#124E81" BorderStyle="Solid" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                <AlternatingRowStyle BackColor="White" />
                                                                            </asp:GridView> </TD></TR></TBODY></TABLE></asp:View> </asp:MultiView></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnUpload"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img alt="" src="../Images/corner.gif" align="absMiddle" />
                            <span style="FONT-SIZE: 9pt"><strong> Form User ID Karyawan</strong></span>&nbsp;<strong><span
                                style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" Text="header"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 3px" colspan="2"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/warn.png"></asp:Image></td>
                            <td style="TEXT-ALIGN: left" class="Label">
                                <asp:Label ID="Validasi" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 3px; TEXT-ALIGN: center" colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" colspan="2">
                                <asp:ImageButton ID="btnErrOK" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/ok.png"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            &nbsp;
            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" PopupDragHandleControlID="Validasi" PopupControlID="Panelvalidasi" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="UpdatePanelPIC" runat="server" Visible="False">
        <ContentTemplate>
<asp:Panel id="PanelPIC" runat="server" Width="400px" CssClass="modalBox" Visible="False" __designer:wfdid="w1"><TABLE width="100%"><TBODY><TR><TD style="HEIGHT: 7px; TEXT-ALIGN: center"><asp:Label id="LabelPIC" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Person" __designer:wfdid="w2"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label"><TABLE width="100%"><TBODY><TR><TD align=right>Filter :</TD><TD align=left><asp:DropDownList id="DDLFilterPIC" runat="server" CssClass="inpText" __designer:wfdid="w3">
                                                    <asp:ListItem Value="nip">NIP</asp:ListItem>
                                                    <asp:ListItem Value="personname">Name</asp:ListItem>
                                                </asp:DropDownList> <asp:TextBox id="txtFilterPIC" runat="server" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="imbFindPIC" onclick="imbFindPIC_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton> <asp:ImageButton id="imbAllPIC" onclick="imbAllPIC_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD><FIELDSET style="WIDTH: 97%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 170px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div2"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 92%"><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvPIC" runat="server" Width="98%" Visible="False" BorderWidth="1px" BorderColor="#DEDFDE" AutoGenerateColumns="False" OnSelectedIndexChanged="gvPIC_SelectedIndexChanged" BorderStyle="Solid" __designer:wfdid="w7" CellPadding="4" DataKeyNames="personoid,nip,personname,noktp,status,personsex,religion,amtcontract,maritalstatus,alamattinggal,citytinggaloid,alamatasal,asalcityoid,pendidikanterakhir,asalsekolah,catatanlainlain,tglmasuk,tempatlahir,tgllahir,phone1,phone2,email,personpicture">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="nip" HeaderText="NIP">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Person Name">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" Font-Size="X-Small" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No PIC data found!!"></asp:Label>

                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> &nbsp; </TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="lblClosePIC" onclick="lblClosePIC_Click" runat="server" __designer:wfdid="w8">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEPIC" runat="server" __designer:wfdid="w9" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelPIC" PopupDragHandleControlID="LabelPIC"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" DefaultButton="btnMsgBoxOK" Visible="False"><TABLE height="100%" cellSpacing=1 cellPadding=1 width="100%" border=0><TBODY><TR><TD style="COLOR: white; HEIGHT: 18px; BACKGROUND-COLOR: blue; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption2" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left>&nbsp;</TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" CssClass="Important"></asp:Label> </TD></TR><TR><TD vAlign=top align=left colSpan=2><asp:Image id="Image4" runat="server" Width="450px" Height="97px" Visible="False"></asp:Image></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" TargetControlID="beMsgBox">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="1px" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

