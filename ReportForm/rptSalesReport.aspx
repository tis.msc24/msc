<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSalesReport.aspx.vb" Inherits="ReportForm_rptSalesReport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
        Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
    
    <%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
        Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
                    <tr>
                        <th align="left" class="header" valign="center" style="height: 31px">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Navy"
                                Text=".: Laporan Sales Order" Font-Names="Verdana" Font-Size="21px" Width="293px"></asp:Label></th>
                    </tr>
<tr>
<th align="center" style="background-color: #ffffff; width: 100%; height: 100%;" valign="center">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD align=left>Status </TD><TD align=center>:</TD><TD align=left><asp:RadioButton id="rbSO" runat="server" Text="Status SO" GroupName="rbStatus" Checked="True" AutoPostBack="True"></asp:RadioButton> <asp:RadioButton id="rbSODO" runat="server" Text="Status SO-Sales DO" GroupName="rbStatus" AutoPostBack="True"></asp:RadioButton></TD></TR><TR><TD id="TD2" align=left runat="server">Type </TD><TD align=center><asp:Label id="Label16" runat="server" Text=":"></asp:Label> </TD><TD id="TD3" align=left runat="server"><asp:DropDownList id="ddltype" runat="server" Width="154px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddltype_SelectedIndexChanged">
                                            <asp:ListItem>Summary</asp:ListItem>
                                            <asp:ListItem>Detail</asp:ListItem>
                                        </asp:DropDownList>&nbsp;<asp:CheckBox id="cbSR" runat="server" Text="Special Request"></asp:CheckBox></TD></TR><TR><TD id="TD8" vAlign=top align=left runat="server">Type SO </TD><TD vAlign=top align=center><asp:Label id="Label5" runat="server" Text=":"></asp:Label> </TD><TD id="TD9" align=left runat="server"><asp:CheckBox id="cbCheckAll" runat="server" BackColor="#FFFFC0" Text="CHECK ALL" AutoPostBack="True" BorderColor="Black" __designer:wfdid="w1"></asp:CheckBox> <asp:CheckBoxList id="cbType" runat="server" ForeColor="White" BackColor="#FFFFC0" AutoPostBack="True" OnSelectedIndexChanged="cbType_SelectedIndexChanged" CellPadding="5" BorderColor="Black" __designer:wfdid="w2" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList> <asp:DropDownList id="ddlSOtype" runat="server" Width="154px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddltype_SelectedIndexChanged" Visible="False"></asp:DropDownList></TD></TR><TR><TD id="TD10" align=left runat="server">Jenis SO </TD><TD align=center><asp:Label id="Label6" runat="server" Text=":"></asp:Label> </TD><TD id="TD11" align=left runat="server"><asp:DropDownList id="DDLjenisSO" runat="server" Width="154px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddltype_SelectedIndexChanged">
                                         <asp:ListItem>ALL</asp:ListItem>
                                            <asp:ListItem>GROSIR</asp:ListItem>
                                            <asp:ListItem>PROJECT</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR id="trcabang" runat="server"><TD id="TD4" align=left runat="server">Cabang </TD><TD align=center><asp:Label id="Label3" runat="server" Text=":"></asp:Label> </TD><TD id="TD5" align=left runat="server"><asp:DropDownList id="DDLcbg" runat="server" Width="154px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddltype_SelectedIndexChanged" visible="true"></asp:DropDownList></TD></TR><TR id="trcurr" runat="server"><TD id="TD6" align=left runat="server" visible="false">Currency </TD><TD id="TD15" align=center runat="server" visible="false"><asp:Label id="Label4" runat="server" Text=":"></asp:Label> </TD><TD id="TD7" align=left runat="server" visible="false"><asp:DropDownList id="DDLcurr" runat="server" Width="154px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddltype_SelectedIndexChanged">
                                            <asp:ListItem>ALL</asp:ListItem>
                                        </asp:DropDownList> </TD></TR><TR><TD align=left>Periode</TD><TD align=center>:</TD><TD align=left><asp:TextBox id="range1" runat="server" Width="60px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="60px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD vAlign=top align=left><asp:DropDownList id="DDLSONo" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True">
                                                <asp:ListItem Value="NomerSO">No. SO</asp:ListItem>
                                                <asp:ListItem Value="Draft">No. Draft</asp:ListItem>
                                            </asp:DropDownList></TD><TD vAlign=top align=center>:</TD><TD vAlign=top align=left><asp:TextBox id="SoNo" runat="server" Width="158px" CssClass="inpTextDisabled" Enabled="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton> <asp:Label id="sooid" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD vAlign=top align=left><asp:Label id="Label11" runat="server" Text="Customer"></asp:Label></TD><TD vAlign=top align=center>:</TD><TD vAlign=top align=left><asp:TextBox id="custcode" runat="server" Width="158px" CssClass="inpTextDisabled" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="btnSearchCust" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton> <asp:ImageButton id="btnClearCust" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton></TD></TR><TR><TD align=left>Sales </TD><TD align=center>:</TD><TD align=left><asp:TextBox id="TxtPinjamNo" runat="server" Width="185px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="sPerson" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="eRasePerson" onclick="eRasePerson_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="PersonOid" runat="server" Visible="False"></asp:Label></TD><TD align=center></TD><TD align=left><asp:GridView id="PersonGv" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" OnSelectedIndexChanged="PersonGv_SelectedIndexChanged" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="personoid,personname" UseAccessibleHeader="False" GridLines="None" PageSize="8" OnPageIndexChanging="PersonGv_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="Red"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Maroon" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="personnip" HeaderText="NIP">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Nama Sales">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="PERSONCRTADDRESS" HeaderText="Alamat Sales">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD17" align=left runat="server" visible="false"><asp:Label id="Label9" runat="server" Text="Group Item" Visible="False"></asp:Label></TD><TD id="TD16" align=center runat="server" visible="false"><asp:Label id="Label10" runat="server" Text=":" Visible="False"></asp:Label></TD><TD id="TD14" align=left runat="server" visible="false"><asp:DropDownList id="FilterDDLGrup" runat="server" Width="215px" CssClass="inpText" Visible="False">
                                        </asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="ItemLbl" runat="server" Text="Katalog"></asp:Label> </TD><TD align=center><asp:Label id="Label7" runat="server" Text=":"></asp:Label></TD><TD align=left><asp:TextBox id="ItemName" runat="server" Width="158px" CssClass="inpTextDisabled" Enabled="False" TextMode="MultiLine" Rows="2"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left runat="server" visible="false">Status SO</TD><TD align=center runat="server" visible="false">:</TD><TD align=left runat="server" visible="false"><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" AutoPostBack="True" Visible="True">
                                            <asp:ListItem Value="'All'">All</asp:ListItem>
                                            <asp:ListItem Value="'In Process'">In Process</asp:ListItem>
                                            <asp:ListItem Value="'In Approval'">In Approval</asp:ListItem>
                                            <asp:ListItem Value="'Approved'">Approved</asp:ListItem>
                                            <asp:ListItem Value="'Rejected'">Rejected</asp:ListItem>
                                            <asp:ListItem Value="'Canceled'">Canceled</asp:ListItem>
                                            <asp:ListItem Value="'Closed'">Closed</asp:ListItem>
                                            <asp:ListItem Value="'Revised'">Revised</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR><TD id="TD1" align=left runat="server" visible="false">Status DO</TD><TD id="TD12" align=center runat="server" visible="false">:</TD><TD id="TD13" align=left runat="server" visible="false"><asp:DropDownList id="statusDelivery" runat="server" Width="134px" CssClass="inpText">
                                            <asp:ListItem>ALL</asp:ListItem>
                                            <asp:ListItem>COMPLETE</asp:ListItem>
                                            <asp:ListItem>IN COMPLETE</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR><TD align=left></TD><TD align=center>&nbsp;</TD><TD align=left><asp:DropDownList id="taxable" runat="server" Width="133px" Height="16px" CssClass="inpText" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="NONTAX">NONTAX</asp:ListItem>
<asp:ListItem Value="INC">INCLUDE</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="type_so" runat="server" Width="64px" CssClass="inpText" AutoPostBack="True" Visible="False">
                                            <asp:ListItem Value="'%'">All</asp:ListItem>
                                            <asp:ListItem Value="'SO/%'">SO</asp:ListItem>
                                            <asp:ListItem Value="'SOT/%'">SOT</asp:ListItem>
                                            <asp:ListItem Value="'SJ/%'">SJ</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnView" onclick="btnView_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="BTNpRINT" onclick="BTNpRINT_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:CalendarExtender id="CLE1" runat="server" PopupButtonID="ImageButton1" TargetControlID="range1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" PopupButtonID="ImageButton2" TargetControlID="range2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" TargetControlID="range1" CultureName="en-US" MaskType="Date" ErrorTooltipEnabled="True" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" TargetControlID="range2" CultureName="en-US" MaskType="Date" ErrorTooltipEnabled="True" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                                <ProgressTemplate>
                                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                                    </div>
                                                    <div id="processMessage" class="processMessage">
                                                        <span style="font-weight: bold; font-size: 10pt; color: purple">
                                                            <asp:Image ID="Image4" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/loadingbar.gif"
                                                                Width="81px" /><br />
                                                            Please Wait .....</span><br />
                                                    </div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress> <asp:Label id="Label2" runat="server" ForeColor="Red"></asp:Label> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" AutoDataBind="True" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnView"></asp:PostBackTrigger>
                        <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
                        <asp:PostBackTrigger ControlID="BTNpRINT"></asp:PostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>&nbsp;&nbsp;
</th>
</tr>
                </table>
    <asp:UpdatePanel ID="upListSO" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelListSO" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Large" Font-Bold="True" Text="List Sales Order"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO">
                                    <asp:Label ID="Label24" runat="server" Text="Filter :"></asp:Label>
                                    <asp:DropDownList ID="DDLFilterListSO" runat="server" CssClass="inpText" Width="100px">
                                        <asp:ListItem Value="trnrequestitemno">No. SO</asp:ListItem>
                                        <asp:ListItem Value="trnrequestitemnote">Keterangan</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtFilterListSO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>
                                    <asp:ImageButton ID="btnFindListSO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
                                    <asp:ImageButton ID="btnViewAllListSO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" />
                                </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" ForeColor="#333333" PageSize="5" GridLines="None" DataKeyNames="trnrequestitemoid,trnrequestitemno,trnrequestitemdate,trnrequestitemstatus,trnrequestitemnote" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                <asp:CheckBox ID="cbLMSO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("trnrequestitemoid") %>' />
                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnrequestitemoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemno" HeaderText="No. SO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typeSO" HeaderText="Type SO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                        &nbsp;
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListSO" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListCust" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlListCust" runat="server" CssClass="modalBox" Visible="False" Width="650px">
                <table width="100%">
                    <tr>
                        <td align="center" class="Label" colspan="3">
                            <asp:Label ID="lblListCust" runat="server" Font-Bold="True" Font-Size="Medium" Text="List of Customer"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" valign="top">
                            <asp:Panel ID="pnlFilterListCust" runat="server" DefaultButton="btnFindListCust"
                                Width="100%">
                                Filter :
                                <asp:DropDownList ID="DDLFilterListCust" runat="server" CssClass="inpText" Width="100px">
                                    <asp:ListItem Value="custcode">Code</asp:ListItem>
                                    <asp:ListItem Value="custname">Name</asp:ListItem>
                                    <asp:ListItem Value="custaddr">Address</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtFilterListCust" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton
                                    ID="btnFindListCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />&nbsp;<asp:ImageButton
                                        ID="btnViewAllListCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px" valign="top">
                            <asp:ImageButton ID="btnSelectAllCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectall.png" />
                            <asp:ImageButton
                                ID="btnSelectNoneCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectnone.png" />
                            <asp:ImageButton
                                    ID="btnViewCheckedCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewselected.png" Visible="False" /></td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" valign="top">
                            &nbsp;<asp:GridView ID="gvListCust" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="8" Width="98%">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbLMCust" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="custcode" HeaderText="Kode">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Center"
                                            VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="custname" HeaderText="Customer">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="custaddr" HeaderText="Alamat">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                    HorizontalAlign="Right" />
                                <EmptyDataTemplate>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:LinkButton ID="lkbAddToListListCust" runat="server">[ Add To List ]</asp:LinkButton>
                            -
                            <asp:LinkButton ID="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeListCust" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" PopupControlID="pnlListCust" PopupDragHandleControlID="lblListCust"
                TargetControlID="btnHideListCust">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnHideListCust" runat="server" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlListMat" runat="server" CssClass="modalBox" Visible="False" Width="900px">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:Label ID="lblTitleListMat" runat="server" Font-Bold="True" Font-Size="Large"
                                    Font-Underline="False" Text="List Katalog"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:Panel ID="pnlFindListMat" runat="server" DefaultButton="btnFindListMat" Width="100%">
                                    <table style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    Filter :
                                                    <asp:DropDownList ID="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px">
                                                        <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                                                        <asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
                                                        <asp:ListItem Value="JenisNya">Jenis</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    Jenis :
                                                    <asp:DropDownList ID="stockFlag" runat="server" CssClass="inpText" Font-Size="Small">
                                                        <asp:ListItem>ALL</asp:ListItem>
                                                        <asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
                                                        <asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
                                                        <asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
                                                        <asp:ListItem>ASSET</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:ImageButton ID="btnFindListMat" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
                                                    <asp:ImageButton ID="btnAllListMat" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:ImageButton ID="btnALLSelectItem" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectall.png" />&nbsp;<asp:ImageButton
                                    ID="btnSelectNoneItem" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectnone.png" />&nbsp;<asp:ImageButton
                                        ID="btnViewCheckedItem" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewselected.png" /></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:UpdateProgress ID="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat">
                                    <ProgressTemplate>
                                        <table style="width: 200px">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 10pt; color: purple">
                                                    Load Data, Please Wait ...</td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Image ID="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif" /></td>
                                            </tr>
                                        </table>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="Label" colspan="3">
                                Show
                                <asp:TextBox ID="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox>
                                Data Per Page
                                <asp:LinkButton ID="lbShowData" runat="server">>> View</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <div style="overflow-y: scroll; width: 100%; height: 247px; background-color: beige">
                                    <asp:GridView ID="gvListMat" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        CellPadding="4" DataKeyNames="itemcode" ForeColor="#333333" GridLines="None"
                                        PageSize="8" Width="99%">
                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                <HeaderStyle CssClass="gvhdr" Font-Strikeout="False" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemdesc" HeaderText="Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="merk" HeaderText="Merk">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="satuan3" HeaderText="Satuan">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemflag" HeaderText="Status">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"
                                            HorizontalAlign="Right" />
                                        <EmptyDataTemplate>
                                            &nbsp;
                                            <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="Klik tombol View All atau Find untuk tampilkan data"></asp:Label>
                                        </EmptyDataTemplate>
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </div>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <asp:LinkButton ID="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton>
                                -
                                <asp:LinkButton ID="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
                <ajaxToolkit:ModalPopupExtender ID="mpeListMat" runat="server" BackgroundCssClass="modalBackground"
                    Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat"
                    TargetControlID="btnHideListMat">
                </ajaxToolkit:ModalPopupExtender>
            </asp:Panel>
            <asp:Button ID="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td colspan="2" style="background-color: #cc0000; text-align: left">
                            <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                Width="24px" /></td>
                        <td class="Label" style="text-align: left">
                            <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px; text-align: center">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                TargetControlID="bePopUpMsg">
            </ajaxToolkit:ModalPopupExtender>
            <span style="display: none">
                <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" /></span>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>