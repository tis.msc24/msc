Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptDeadStock
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim report As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim sSql As String = ""
    Dim CKon As New Koneksi
#End Region

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Sub showPrint(ByVal tipe As String)
        Dim swhere As String = ""
        Try

            swhere = "and mst.trnjualstatus = 'POST'"
            swhere &= IIf(custoid.Text = "", "", " and mst.trncustoid='" & custoid.Text & "'")
            CrystalReportViewer1.Visible = True
            report.Load(Server.MapPath("~\Report\rptcustomer.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("periode", period.SelectedValue)
            report.SetParameterValue("swhere", swhere)
            report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("showReport") = "True"
            If tipe = "view" Then
                Session("diprint") = "True"
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = report
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Report_Customer_" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose()
                Response.Redirect("~\ReportForm\rptcustomer.aspx?awal=true")
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Report_Customer_" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose()
                Response.Redirect("~\ReportForm\rptcustomer.aspx?awal=true")
            End If
        Catch ex As Exception
            Label2.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("rptcustomer.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang 
        End If
        Page.Title = CompnyName & " - Report Customer"
        If kacabCek("ReportForm/rptcustomer.aspx", Session("UserID")) = True Or Session("UserID") = "POPY" Then
            BTNpRINT.Visible = True : ibexcel.Visible = True
        Else
            If Session("branch_id") <> "01" Then
                BTNpRINT.Visible = False : ibexcel.Visible = False
            Else
                BTNpRINT.Visible = True : ibexcel.Visible = True

            End If
        End If
        If Not Page.IsPostBack Then
        Else
            If Session("diprint") = "True" Then
                showPrint("view")
            End If
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Session("showReport") = "False" : Response.Redirect("rptcustomer.aspx?awal=true")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        Try

        Catch ex As Exception
            Label2.Text = "Wrong format date"
            Exit Sub
        End Try
        showPrint("excel")
        'showPrintExcel("ListofSalesItem")
    End Sub

    Protected Sub BTNpRINT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try

        Catch ex As Exception
            Label2.Text = "Wrong format date"
            Exit Sub
        End Try
        showPrint("pdf")
        'showPrintExcel("ListofSalesItem")
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
        Catch ex As Exception
            Label2.Text = "Wrong format date"
            Exit Sub
        End Try
        showPrint("view")

    End Sub

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custname.Text = gvCust.SelectedDataKey("custname").ToString().Trim
        custoid.Text = gvCust.SelectedDataKey("custoid").ToString().Trim
        gvCust.Visible = False
    End Sub

    Protected Sub imbcustlist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataSupp()
    End Sub

    Private Sub bindDataSupp()
        sSql = " select distinct cust.custoid,cust.custcode,cust.custname,cust.custaddr from QL_mstcust cust inner join QL_trnjualmst jual on cust.custoid=jual.trncustoid and cust.branch_code=jual.branch_code where  jual.branch_code='" & Session("branch_id") & "' and cust.custname like '%" & Tchar(custname.Text.Trim) & "%'"

        FillGV(gvCust, sSql, "QL_mstcust")
        gvCust.Visible = True
    End Sub

    Protected Sub imbcustdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custoid.Text = ""
        custname.Text = ""
        gvCust.Visible = False
    End Sub

    Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCust.PageIndex = e.NewPageIndex
        bindDataSupp()
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs)
        showPrint("view")
    End Sub
End Class
