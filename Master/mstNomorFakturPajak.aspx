<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstNomorFakturPajak.aspx.vb" Inherits="mstNomorFakturPajak" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
   <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Text=":: Data Nomor Faktur Pajak" CssClass="Title" Font-Size="12pt"></asp:Label></th>
        </tr>
    </table>
     <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanelSearch" runat="server">
                    <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 614px"><TBODY><TR><TD style="HEIGHT: 21px" class="Label" align=left>Doc No.</TD><TD style="WIDTH: 3px; HEIGHT: 21px" class="Label" align=left>:</TD><TD style="HEIGHT: 21px" class="Label" align=left colSpan=5><asp:TextBox id="txtFilternoPajak" runat="server" Width="120px" CssClass="inpText" MaxLength="50" size="20" AutoPostBack="True"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePer1" runat="server" TargetControlID="FilterPeriod1" Format="dd/MM/yyyy" PopupButtonID="imbPeriod1"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePer2" runat="server" TargetControlID="FilterPeriod2" Format="dd/MM/yyyy" PopupButtonID="imbPeriod2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left>Tanggal</TD><TD style="WIDTH: 3px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=5><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label76" runat="server" Text="to"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left>Status</TD><TD style="WIDTH: 3px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=5><asp:DropDownList id="ddlFilterStatus" runat="server" Width="80px" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
<asp:ListItem Value="IN ACTIVE">IN ACTIVE</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=7><asp:Label id="lblWarn" runat="server" CssClass="Important"></asp:Label></TD></TR></TBODY></TABLE><FIELDSET style="WIDTH: 800px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 300px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="field1"><DIV id="divTblData"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 85%"><asp:GridView id="GVmstcurrdtl" runat="server" Width="98%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="Black" BackColor="White" CellPadding="4" EmptyDataText="No data in database." AutoGenerateColumns="False" BorderColor="#DEDFDE" GridLines="None">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="mstfakturoid" DataNavigateUrlFormatString="~\master\mstNomorFakturPajak.aspx?oid={0}" DataTextField="mstfakturno" HeaderText="Doc No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="10%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="date" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="10%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="awal" HeaderText="No Awal">
    <HeaderStyle CssClass="gvhdr" />
</asp:BoundField>
<asp:BoundField DataField="akhir" HeaderText="No Akhir">
    <HeaderStyle CssClass="gvhdr" />
</asp:BoundField>
<asp:BoundField DataField="STATUS" HeaderText="Status">
    <HeaderStyle CssClass="gvhdr" />
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="30%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" ForeColor="Navy" Width="30%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#999999" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                                <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="No Data Found !"></asp:Label>
                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET></asp:Panel>
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <span style="font-size: 9pt"><strong><span>
                List of Nomor Faktur Pajak :.</span></strong></span>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <asp:UpdatePanel id="UpdatePanel1" runat="server">
                            <contenttemplate>
<TABLE><TBODY><TR><TD><TABLE style="WIDTH: 723px" id="Table2" cellSpacing=2 cellPadding=4 align=center border=0><TBODY><TR style="COLOR: #000099"><TD style="TEXT-ALIGN: left" id="TD4" class="Label" runat="server" Visible="false"></TD><TD style="WIDTH: 624px; TEXT-ALIGN: left" id="TD3" colSpan=3 runat="server" Visible="false"><asp:TextBox id="txtNoAkhir" runat="server" Width="97px" CssClass="inpText" MaxLength="8"></asp:TextBox>&nbsp;<asp:TextBox id="txtNoAwal" runat="server" Width="97px" CssClass="inpText" MaxLength="8"></asp:TextBox>&nbsp;<asp:Label id="lblTahunPajak" runat="server" Width="23px"></asp:Label>&nbsp;<asp:TextBox id="txtangka" runat="server" Width="41px" CssClass="inpText" MaxLength="3"></asp:TextBox>&nbsp;</TD></TR><TR style="COLOR: #000099"><TD style="TEXT-ALIGN: left" class="Label">Doc No.</TD><TD style="WIDTH: 624px; TEXT-ALIGN: left" colSpan=3><asp:TextBox id="tbNoDoc" runat="server" Width="91px" CssClass="inpTextDisabled" Font-Size="8pt" MaxLength="150" size="20" Enabled="False"></asp:TextBox>&nbsp; <asp:Label id="oid" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Visible="False"></asp:Label> <asp:Label id="iui" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Visible="False">baru</asp:Label></TD></TR><TR style="COLOR: #000099"><TD style="TEXT-ALIGN: left" class="Label">Date</TD><TD style="WIDTH: 624px; TEXT-ALIGN: left" colSpan=3><asp:TextBox id="txtTglpjk" runat="server" Width="102px" CssClass="inpTextDisabled" MaxLength="4" Enabled="False" AutoPostBack="True"></asp:TextBox> <asp:Label id="Label1" runat="server" CssClass="Important" Text="dd/MM/yyyy"></asp:Label></TD></TR><TR style="COLOR: #000099"><TD style="TEXT-ALIGN: left" class="Label">Status</TD><TD style="WIDTH: 624px; TEXT-ALIGN: left" colSpan=3><asp:DropDownList id="DDLstatus1" runat="server" Width="77px" CssClass="inpText" Font-Bold="True"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>IN ACTIVE</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="minimaldate" runat="server" Width="75px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:ImageButton id="ibfinishdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="Label7" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: left" class="Label">Note</TD><TD style="FONT-SIZE: 10pt; WIDTH: 624px; TEXT-ALIGN: left" colSpan=3><asp:TextBox id="txtNote" runat="server" Width="435px" CssClass="inpText" Font-Size="8pt" MaxLength="150" size="20"></asp:TextBox>&nbsp;</TD></TR><TR><TD style="TEXT-ALIGN: left" class="Label"><ajaxToolkit:CalendarExtender id="ceFinishDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibfinishdate" TargetControlID="minimaldate"></ajaxToolkit:CalendarExtender></TD><TD style="FONT-SIZE: 10pt; WIDTH: 624px; TEXT-ALIGN: left" colSpan=3><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="minimaldate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-SIZE: 10pt; TEXT-ALIGN: left" colSpan=4 rowSpan=1><STRONG><EM><SPAN style="TEXT-DECORATION: underline">Detail No. Faktur Pajak :</SPAN></EM></STRONG></TD></TR><TR><TD style="TEXT-ALIGN: left" class="Label" runat="server" visible="true"><SPAN style="FONT-SIZE: 10pt">Prefix</SPAN></TD><TD style="FONT-SIZE: 10pt; WIDTH: 624px; TEXT-ALIGN: left" colSpan=3 runat="server" visible="true"><asp:TextBox id="TextBox2" runat="server" Width="54px" CssClass="inpText" MaxLength="3"></asp:TextBox>&nbsp;- <asp:TextBox id="TextBox3" runat="server" Width="35px" CssClass="inpText" MaxLength="2"></asp:TextBox>&nbsp;.&nbsp;&nbsp; Nomor Awal : <asp:TextBox id="TextBox4" runat="server" Width="100px" CssClass="inpText" MaxLength="8"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;No Akhir : <asp:TextBox id="TextBox5" runat="server" Width="97px" CssClass="inpText" MaxLength="8"></asp:TextBox></TD></TR><TR><TD style="TEXT-ALIGN: left" id="TD2" class="Label" runat="server" visible="true"><asp:LinkButton id="GENERATE" onclick="GENERATE_Click" runat="server" CausesValidation="False" Font-Size="X-Small">[ GENERATE ]</asp:LinkButton></TD><TD style="FONT-SIZE: 10pt; WIDTH: 624px; TEXT-ALIGN: left" id="TD1" colSpan=3 runat="server" visible="true"><asp:Label id="lblNomorSekarang" runat="server"></asp:Label> <asp:DropDownList id="DDLstatus2" runat="server" Width="77px" CssClass="inpTextDisabled" Font-Bold="True" Visible="False" Enabled="False"><asp:ListItem>OPEN</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="ddlStatus" runat="server" Width="80px" CssClass="inpText" Visible="False"><asp:ListItem Value="1">AKTIF</asp:ListItem>
<asp:ListItem Value="0">NONAKTIF</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: 10pt; WIDTH: 100%; TEXT-ALIGN: left" id="Td6" colSpan=3 runat="server" visible="true"><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 290px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="GVgenerate" runat="server" Width="97%" BackColor="White" BorderColor="#DEDFDE" AutoGenerateColumns="False" EmptyDataText="No data in database." CellPadding="4" BorderWidth="1px" BorderStyle="Solid">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:BoundField DataField="SEQ" HeaderText="No">
    <HeaderStyle HorizontalAlign="Center" Width="50px" />
    <ItemStyle HorizontalAlign="Center" Width="50px" />
</asp:BoundField>
<asp:BoundField DataField="GenerateNumber" HeaderText="No Faktur"></asp:BoundField>
<asp:BoundField DataField="dtlfakturreftype" HeaderText="Type"></asp:BoundField>
<asp:BoundField DataField="dtlfakturrefno" HeaderText="No. SI"></asp:BoundField>
<asp:BoundField DataField="STATUS" HeaderText="Status">
    <HeaderStyle HorizontalAlign="Center" Width="75px" />
    <ItemStyle HorizontalAlign="Center" Width="75px" />
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#F7F7DE" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
    
    
                                        
                                                                             <asp:Label ID="lblmsg" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET></TD></TR><TR><TD style="COLOR: #585858; TEXT-ALIGN: left" colSpan=4><asp:Panel id="PanelUpdate" runat="server">Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="[Updtime]"></asp:Label>&nbsp;by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="[upduser]"></asp:Label></asp:Panel> </TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="HEIGHT: 24px"><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/save.png"></asp:ImageButton></TD><TD style="HEIGHT: 24px"><asp:ImageButton id="BtnCancel" onclick="BtnCancel_Click" runat="server" ImageUrl="~/Images/cancel.png"></asp:ImageButton></TD><TD style="WIDTH: 63px; HEIGHT: 24px"><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" TargetControlID="TextBox2" ValidChars="12345678980.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
            <HeaderTemplate>
                 <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <span style="font-size: 9pt"><strong>Form Nomor Faktur Pajak :.</strong></span>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
            </td>
        </tr>
     </table> 
                <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
                    <ContentTemplate >
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left; color: white;" colSpan=2><asp:Label id="lblCaption1" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" OnClick="ImageButton4_Click"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" DropShadow="True" PopupControlID="PanelValidasi" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel>
</asp:Content>