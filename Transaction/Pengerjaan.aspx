<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="Pengerjaan.aspx.vb" Inherits="Transaction_Pengerjaan" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="100%">
        <tr>
            <td align="left" colspan="3">
                <table width="100%">
                    <tr>
                        <td align="left" char="header" colspan="3" style="background-color: silver">
                            <asp:Label ID="Label249" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="Navy" Text=".: Pengerjaan Servis">
                            </asp:Label></td>
                    </tr>
                </table>
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <strong>
                                <img src="../Images/corner.gif" /> List Pengerjaan Servis :.</strong>&nbsp;
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 90px; HEIGHT: 30px"><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w109"></asp:Label></TD><TD style="WIDTH: 6px; HEIGHT: 30px"><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w110"></asp:Label></TD><TD style="HEIGHT: 30px" colSpan=3><asp:DropDownList id="ddlFilter" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w171"><asp:ListItem Value="QLA.BARCODE">Barcode</asp:ListItem>
<asp:ListItem Value="QLA.REQITEMNAME">Nama Barang</asp:ListItem>
<asp:ListItem Value="QLA2.GENDESC">Jenis Barang</asp:ListItem>
<asp:ListItem Value="QLA3.GENDESC">Merk Barang</asp:ListItem>
<asp:ListItem Value="t2.PERSONNAME">Teknisi Awal</asp:ListItem>
<asp:ListItem Value="t3.PERSONNAME">Teknisi Akhir</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="155px" CssClass="inpText" __designer:wfdid="w172"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w173"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w174"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 90px"><asp:CheckBox id="cbPeriod" runat="server" Text="Periode" __designer:wfdid="w113"></asp:CheckBox></TD><TD style="WIDTH: 6px"><asp:Label id="Label3" runat="server" Text=":" __designer:wfdid="w114"></asp:Label></TD><TD colSpan=3><asp:TextBox id="txtPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w175"></asp:TextBox> <asp:ImageButton id="btnCal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w176"></asp:ImageButton>&nbsp; <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w177"></asp:Label>&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w178"></asp:TextBox> <asp:ImageButton id="btnCal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w179"></asp:ImageButton>&nbsp; <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w180"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w5"></asp:CheckBox></TD><TD style="WIDTH: 6px"></TD><TD colSpan=3><asp:DropDownList id="ddlStatus" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w181"><asp:ListItem Value="%">All</asp:ListItem>
<asp:ListItem>Start</asp:ListItem>
<asp:ListItem>Finish</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD colSpan=5><asp:GridView id="GVListWork" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w182" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" EmptyDataText="Data Not Found">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="White" BorderColor="White" ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="WORKOID" DataNavigateUrlFormatString="~\transaction\Pengerjaan.aspx?idPage={0}" DataTextField="BARCODE" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="120px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="SSTARTTIME" HeaderText="Target Selesai">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMNAME" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMTYPE" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMBRAND" HeaderText="Merk" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="WORKSTARTTIME" HeaderText="Waktu Mulai">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="firstteknisi" HeaderText="Teknisi Awal">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="WORKENDTIME" HeaderText="Waktu Selesai">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastteknisi" HeaderText="Teknisi Akhir">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="STATUS" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" __designer:wfdid="w124" TargetControlID="txtPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CECal1" runat="server" __designer:wfdid="w125" TargetControlID="txtPeriod1" Format="dd/MM/yyyy" PopupButtonID="btnCal1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" __designer:wfdid="w126" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CECal2" runat="server" __designer:wfdid="w127" TargetControlID="txtPeriod2" Format="dd/MM/yyyy" PopupButtonID="btnCal2"></ajaxToolkit:CalendarExtender> <asp:SqlDataSource id="SDSGVList" runat="server" __designer:wfdid="w128" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT DISTINCT QLA.SOID, QLR.REQOID, QJA.PARTDESCSHORT SPARTM, QLR.REQBARCODE, QLR.REQCODE, QLJ.ITSERVDESC, QLZ.GENDESC ITSERVTYPE, QLAA.SPARTNOTE, QLAA.SPARTQTY, QLAA.SPARTPRICE FROM QL_TRNREQUEST QLR INNER JOIN QL_TRNMECHCHECK QLG ON QLG.MSTREQOID = QLR.REQOID INNER JOIN QL_MSTITEMSERV QLJ ON QLG.ITSERVOID = QLJ.ITSERVOID INNER JOIN QL_MSTGEN QLZ ON QLZ.GENOID = QLJ.ITSERVTYPEOID INNER JOIN QL_TRNSERVICES QLA ON QLA.SOID = QLG.ITSERVOID  INNER JOIN QL_TRNSERVICESPART QLAA ON QLAA.SOID = QLA.SOID INNER JOIN QL_MSTSPAREPART QJA ON QJA.PARTOID = QLAA.SPARTMOID WHERE (QLA.CMPCODE = @CMPCODE) AND (QLA.SOID = @SOID)&#13;&#10;"><SelectParameters>
<asp:Parameter Name="CMPCODE"></asp:Parameter>
<asp:Parameter Name="SOID"></asp:Parameter>
</SelectParameters>
</asp:SqlDataSource></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <strong>
                                <img src="../Images/corner.gif" /> Form Pengerjaan Servis</strong><strong> :.</strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" __designer:wfdid="w93"><asp:View id="View1" runat="server" __designer:wfdid="w94"><TABLE width=950><TBODY><TR><TD colSpan=3><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 113px">Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DdlCabang" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w183" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 113px">Barcode</TD><TD>:</TD><TD><asp:TextBox id="lblno" runat="server" Width="148px" CssClass="inpText" __designer:wfdid="w95"></asp:TextBox> <asp:Label id="txtworkOid" runat="server" __designer:wfdid="w5" Visible="False"></asp:Label> <asp:Label id="txtOid" runat="server" __designer:wfdid="w6" Visible="False"></asp:Label>&nbsp;<asp:Label id="CodeCb" runat="server" __designer:wfdid="w7" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label13" runat="server" Text="No. Tanda Terima" __designer:wfdid="w99"></asp:Label></TD><TD><asp:Label id="Label23" runat="server" Text=":" __designer:wfdid="w100"></asp:Label></TD><TD><asp:TextBox id="txtNo" runat="server" Width="148px" CssClass="inpTextDisabled" __designer:wfdid="w101" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w102"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnErase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w103"></asp:ImageButton>&nbsp;<asp:TextBox id="txti_u" runat="server" Width="18px" CssClass="inpText" ForeColor="Red" __designer:wfdid="w104" Visible="False">New</asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px">Teknisi</TD><TD>:</TD><TD><asp:TextBox id="teknisi" runat="server" Width="148px" CssClass="inpTextDisabled" __designer:wfdid="w105" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label14" runat="server" Text="Target Selesai" __designer:wfdid="w106"></asp:Label></TD><TD><asp:Label id="Label24" runat="server" Text=":" __designer:wfdid="w107"></asp:Label></TD><TD><asp:TextBox id="txtTglTarget" runat="server" Width="70px" CssClass="inpTextDisabled" __designer:wfdid="w108" ReadOnly="True"></asp:TextBox>&nbsp;: <asp:TextBox id="txtWaktuTarget" runat="server" Width="60px" CssClass="inpTextDisabled" __designer:wfdid="w109" ReadOnly="True"></asp:TextBox>&nbsp; <asp:Label id="Label22" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w110"></asp:Label>&nbsp;: <asp:Label id="Label63" runat="server" Height="1px" ForeColor="Red" Text="HH:mm:ss" __designer:wfdid="w111"></asp:Label></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label16" runat="server" Text="Nama Barang" __designer:wfdid="w112"></asp:Label></TD><TD><asp:Label id="Label26" runat="server" Text=":" __designer:wfdid="w113"></asp:Label></TD><TD><asp:TextBox id="txtNamaBrg" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w114" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label11" runat="server" Text="Jenis Barang" __designer:wfdid="w115"></asp:Label></TD><TD><asp:Label id="Label35" runat="server" Text=":" __designer:wfdid="w116"></asp:Label></TD><TD><asp:TextBox id="txtJenisBrg" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w117" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label36" runat="server" Text="Merk Barang" __designer:wfdid="w118"></asp:Label></TD><TD><asp:Label id="Label37" runat="server" Text=":" __designer:wfdid="w119"></asp:Label></TD><TD><asp:TextBox id="txtMerk" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w120" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label17" runat="server" Text="Tanggal Mulai" __designer:wfdid="w121"></asp:Label></TD><TD><asp:Label id="Label27" runat="server" Text=":" __designer:wfdid="w122"></asp:Label></TD><TD><asp:TextBox id="tglMulai" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w123" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label18" runat="server" Text="Waktu Mulai" __designer:wfdid="w124"></asp:Label></TD><TD><asp:Label id="Label28" runat="server" Text=":" __designer:wfdid="w125"></asp:Label></TD><TD><asp:TextBox id="wktMulai" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w126" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label19" runat="server" Text="Tanggal Selesai" __designer:wfdid="w127"></asp:Label></TD><TD><asp:Label id="Label29" runat="server" Text=":" __designer:wfdid="w128"></asp:Label></TD><TD><asp:TextBox id="tglSelesai" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w129" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px"><asp:Label id="Label20" runat="server" Text="Waktu Selesai" __designer:wfdid="w130"></asp:Label></TD><TD><asp:Label id="Label30" runat="server" Text=":" __designer:wfdid="w131"></asp:Label></TD><TD><asp:TextBox id="wktSelesai" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w132" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 113px" align=left><asp:Label id="Label7" runat="server" Text="Status" __designer:wfdid="w133"></asp:Label></TD><TD><asp:Label id="Label9" runat="server" Text=":" __designer:wfdid="w134"></asp:Label></TD><TD><asp:TextBox id="txtStatus" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w135" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD id="TD1" align=left colSpan=3 runat="server" Visible="false"><ajaxToolkit:MaskedEditExtender id="MEPtglTarget" runat="server" __designer:wfdid="w136" TargetControlID="txtTglTarget" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEPJam" runat="server" __designer:wfdid="w137" TargetControlID="txtWaktuTarget" Mask="99:99:99" MaskType="Time"></ajaxToolkit:MaskedEditExtender> <asp:TextBox id="txtNamaCust" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w138" Visible="False" ReadOnly="True"></asp:TextBox> <asp:Label id="Label15" runat="server" Text="Nama Customer" __designer:wfdid="w139" Visible="False"></asp:Label><asp:Label id="Label21" runat="server" Text=":" __designer:wfdid="w140" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=3>Created&nbsp;By&nbsp;<asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w141"></asp:Label>&nbsp;On&nbsp;<asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w142"></asp:Label></TD></TR><TR><TD align=left colSpan=3><asp:Button id="btnStart" onclick="btnStart_Click" runat="server" CssClass="orange" Font-Bold="True" ForeColor="White" Text="Start" __designer:wfdid="w143"></asp:Button>&nbsp;<asp:Button id="btnFinish" onclick="btnFinish_Click" runat="server" CssClass="green" Font-Bold="True" ForeColor="White" Text="Finish" __designer:wfdid="w144" Visible="False"></asp:Button>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" __designer:wfdid="w145"></asp:ImageButton></TD></TR></TBODY></TABLE><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:dtid="1407374883553305" __designer:wfdid="w146"><ContentTemplate __designer:dtid="1407374883553306">
<asp:Panel id="PanelInfo" runat="server" CssClass="modalBox" __designer:wfdid="w147" Visible="False" DefaultButton="btnFindInfo"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=6><asp:Label id="lblInfo" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Penerimaan Barang" __designer:wfdid="w148"></asp:Label></TD></TR><TR><TD style="WIDTH: 95px"><asp:Label id="Label8" runat="server" Text="Filter" __designer:wfdid="w149"></asp:Label></TD><TD style="WIDTH: 2px"><asp:Label id="Label10" runat="server" Text=":" __designer:wfdid="w150" Visible="False"></asp:Label></TD><TD colSpan=4><asp:DropDownList id="ddlFilterInfo" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w151"><asp:ListItem Value="QLR.REQCODE">No Tanda Terima</asp:ListItem>
<asp:ListItem Value="QLR.BARCODE">Barcode</asp:ListItem>
<asp:ListItem Value="QLR.REQITEMNAME">Nama Barang</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterInfo" runat="server" Width="145px" CssClass="inpText" __designer:wfdid="w152"></asp:TextBox> <asp:ImageButton id="btnFindInfo" onclick="btnFindInfo_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w153"></asp:ImageButton> <asp:ImageButton id="btnViewAllInfo" onclick="btnViewAllInfo_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w154"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 95px; HEIGHT: 15px"><asp:CheckBox id="cbPeriodInfo" runat="server" Text="Period" __designer:wfdid="w155" AutoPostBack="True" Visible="False"></asp:CheckBox></TD><TD style="WIDTH: 2px; HEIGHT: 15px"><asp:Label id="Label12" runat="server" Text=":" __designer:wfdid="w156" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" colSpan=4><asp:TextBox id="txtPeriod1Info" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w157" Visible="False"></asp:TextBox> <asp:ImageButton id="btnCal1Info" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w158" Visible="False"></asp:ImageButton> <asp:Label id="Label34" runat="server" Text="to" __designer:wfdid="w159" Visible="False"></asp:Label>&nbsp;<asp:TextBox id="txtPeriod2Info" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w160" Visible="False"></asp:TextBox> <asp:ImageButton id="btnCal2Info" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w161" Visible="False"></asp:ImageButton></TD></TR><TR><TD colSpan=6><ajaxToolkit:CalendarExtender id="CETglInfo1" runat="server" __designer:wfdid="w162" TargetControlID="txtPeriod1Info" Format="dd/MM/yyyy" PopupButtonID="btnCal1Info"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEPTglInfo1" runat="server" __designer:wfdid="w163" TargetControlID="txtPeriod1Info" Mask="99/99/9999" MaskType="Date" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CETglInfo2" runat="server" __designer:wfdid="w164" TargetControlID="txtPeriod2Info" Format="dd/MM/yyyy" PopupButtonID="btnCal2Info"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEPTglInfo2" runat="server" __designer:wfdid="w165" TargetControlID="txtPeriod2Info" Mask="99/99/9999" MaskType="Date" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=center colSpan=6><asp:GridView id="GVListInfo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w166" DataKeyNames="MSTREQOID" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="MSTREQOID" HeaderText="MSTREQOID" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="REQCODE" HeaderText="No. Tanda Terima">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="BARCODE" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQCUSTOID" HeaderText="Nama Customer" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMNAME" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMBRAND" HeaderText="Merk" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="SSTATUS" HeaderText="Status" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="SFLAG" HeaderText="Flag" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="tgl" HeaderText="Tanggal Terima" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Teknisi" HeaderText="Teknisi">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label4" runat="server" ForeColor="Red" Text="No data found !" __designer:wfdid="w144"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=6><asp:LinkButton id="lbClose" onclick="lbClose_Click" runat="server" __designer:wfdid="w167">[Close]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenInfo" runat="server" __designer:wfdid="w168" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="MPEInfo" runat="server" __designer:wfdid="w169" TargetControlID="btnHiddenInfo" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelInfo" PopupDragHandleControlID="lblInfo"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel6" runat="server" __designer:dtid="1407374883553307" __designer:wfdid="w170"><ContentTemplate __designer:dtid="1407374883553308">
<asp:Panel id="PanelSpart" runat="server" CssClass="modalMsgBox" __designer:wfdid="w171" Visible="False"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=6><asp:Label id="lbSpart" runat="server" Font-Size="Medium" Font-Bold="True" Text="Reason Terlambat" __designer:wfdid="w172"></asp:Label></TD></TR><TR><TD style="WIDTH: 61px"><asp:Label id="Label47" runat="server" Text="Filter" __designer:wfdid="w173" Visible="False"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label53" runat="server" Text=":" __designer:wfdid="w174" Visible="False"></asp:Label></TD><TD colSpan=4><asp:DropDownList id="ddlFilterSpart" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w175" Visible="False"></asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterSpart" runat="server" Width="166px" CssClass="inpText" __designer:wfdid="w176" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindSpart" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w177" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllSpart" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w178" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 649px; HEIGHT: 136px"><asp:GridView id="GVSpart" runat="server" Width="627px" ForeColor="#333333" __designer:wfdid="w179" DataKeyNames="genoid" AutoGenerateColumns="False" CellPadding="4" GridLines="None" OnSelectedIndexChanged="GVSpart_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Width="20%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="genoid" HeaderText="ID" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Reason">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Width="80%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label71" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=6><asp:LinkButton id="lbCloseSpart" onclick="lbCloseSpart_Click" runat="server" __designer:wfdid="w180">[Close]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenSpart" runat="server" __designer:wfdid="w181" Visible="False"></asp:Button><BR /><ajaxToolkit:ModalPopupExtender id="MPESpart" runat="server" __designer:wfdid="w182" TargetControlID="btnHiddenSpart" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelSpart" PopupDragHandleControlID="lblSpart"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE></asp:View>&nbsp; </asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width=250><TBODY><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="WARNING" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="WIDTH: 29px"><asp:ImageButton id="ImageButton3" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 29px"></TD><TD><asp:Label id="lblState" runat="server"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 29px"></TD><TD align=center><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="WARNING" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

