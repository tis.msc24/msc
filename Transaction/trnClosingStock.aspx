<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnClosingStock.aspx.vb" Inherits="transaction_trnClosingStock" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
        <ContentTemplate>
<asp:Panel style="PADDING-RIGHT: 2px; DISPLAY: none; PADDING-LEFT: 2px; PADDING-BOTTOM: 5px; PADDING-TOP: 2px; TEXT-ALIGN: center" id="MainPanel1" runat="server" Width="450px" CssClass="modalBox" BackColor="White" BorderColor="#646464" BorderStyle="Solid" BorderWidth="2px" Visible="False">
<table width="100%">
<tr>
<td align="center" colspan="3">
<asp:Panel style="CURSOR: hand; PADDING-TOP: 4px; TEXT-ALIGN: center" id="Caption1" runat="server" Width="100%" Height="15px" CssClass="gvhdr">&nbsp; <asp:Label id="CaptionLabel" runat="server" Font-Bold="True" ForeColor="Black">- INFO-</asp:Label></asp:Panel> 
</td>
</tr>
<tr>
<td colspan="3">
<asp:Label id="lblInfo" runat="server" Width="439px">Anda Yakin ingin melakukan proses closing stock pada periode sekarang?</asp:Label></td>
</tr>
<tr>
<td align="center" colspan="3">
<asp:Button id="btnOK" runat="server" Width="52px" CssClass="red" Font-Bold="True" ForeColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Text="Yes"></asp:Button> <asp:Button id="btnCancel" runat="server" Width="52px" CssClass="gray" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderWidth="1px" Text="No"></asp:Button></td>
</tr>
<tr>
<td align="center" colspan="3">
<asp:Panel style="CURSOR: hand; PADDING-TOP: 4px; TEXT-ALIGN: center" id="Panel1" runat="server" Width="100%" Height="10px" CssClass="gvhdr">
</asp:Panel>
</td>
</tr>
</table>
</asp:Panel> <asp:Button id="btnHiddenInfo" runat="server" Width="0px" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="MPEInfo" runat="server" PopupControlID="MainPanel1" BackgroundCssClass="modalBackground" TargetControlID="btnHiddenInfo"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<asp:Panel style="PADDING-RIGHT: 2px; DISPLAY: none; PADDING-LEFT: 2px; PADDING-BOTTOM: 5px; PADDING-TOP: 2px; TEXT-ALIGN: center" id="MainPanel2" runat="server" Width="300px" CssClass="modalBox" BackColor="White" BorderColor="#646464" BorderStyle="Solid" BorderWidth="2px" Visible="False"><asp:Panel style="CURSOR: hand; PADDING-TOP: 4px; TEXT-ALIGN: center" id="Caption2" runat="server" Width="300px" Height="20px" CssClass="gvhdr">&nbsp; <asp:Label id="CaptionLabel1" runat="server" Font-Bold="True" ForeColor="Black"> - INFO-</asp:Label></asp:Panel> <BR /><asp:Label id="lblInfo1" runat="server" Width="283px">Are you sure to closing stock monthly?</asp:Label><br />
<BR /><asp:Button id="OK" runat="server" Width="52px" CssClass="red" Font-Bold="True" ForeColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Text="Ok"></asp:Button></asp:Panel> <asp:Button id="btnHiddenInfo1" runat="server" Width="0px" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="MPEInfo1" runat="server" PopupControlID="MainPanel2" BackgroundCssClass="modalBackground" TargetControlID="btnHiddenInfo1"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlWarning" runat="server" Width="700px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=3>
Maaf, anda belum bisa melakuan proses closing pada periode
<asp:Label id="Label2" runat="server" Text="Label"></asp:Label>, Karena masih ada
data transaksi yang belum di posting atau approved<BR /></TD></TR>
<tr>
<td align="left" colspan="3" style="font-weight: bold">
<asp:GridView id="gvTransaksi" runat="server" Width="100%" ForeColor="#333333" OnPageIndexChanging="gvTransaksi_PageIndexChanging" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="urut" HeaderText="No. Transaksi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tipe" HeaderText="Transaksi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="User Input">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle Font-Bold="True" ForeColor="White" CssClass="gvhdr"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label12" runat="server" ForeColor="Red" Text="No detail BPM on List"></asp:Label>
                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle Font-Bold="True" ForeColor="White" CssClass="gvhdr"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="Label1" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="* Warna biru adalah transaksi yang belum di POSTING" Font-Italic="True"></asp:Label>
</td>
</tr>
<tr>
<td align="center" colspan="3" style="font-weight: bold">
<asp:ImageButton id="ibClose" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></td>
</tr>
</TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenWarning" runat="server" Width="1px" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="MPEWarning" runat="server" TargetControlID="btnHiddenWarning" BackgroundCssClass="modalBackground" PopupControlID="pnlWarning"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
