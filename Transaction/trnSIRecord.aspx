<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="trnSIRecord.aspx.vb" Inherits="trnSIRecord" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
    <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0"
        width="100%" onclick="return tableutama_onclick()" class="tabelhias">
        <tr>
            <th class="header" colspan="3" style="vertical-align: middle; text-align: left; height: 25px;" valign="top" align="left">
                <asp:Label ID="Label111" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Maroon" Text=".: Sales Invoice Record"></asp:Label>
                </Th>                    
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
<asp:Panel id="Panel2" runat="server" __designer:wfdid="w218" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small" align=left>Filter :</TD><TD align=left><asp:DropDownList id="ddlFilter" runat="server" CssClass="inpText" __designer:wfdid="w219"><asp:ListItem Value="trnjualno">Invoice No</asp:ListItem>
<asp:ListItem Value="trnbelipono">PO No</asp:ListItem>
<asp:ListItem Value="trncustname">Customer</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w220" MaxLength="30"></asp:TextBox></TD><TD style="WIDTH: 3px" align=left><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w221" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Periode :</TD><TD align=left><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w222"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w223"></asp:ImageButton>&nbsp;<asp:Label id="Label169" runat="server" Text="to" __designer:wfdid="w224"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w225"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w226"></asp:ImageButton>&nbsp;<asp:Label id="Label179" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w227"></asp:Label> <asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w228" Visible="False" Checked="True" AutoPostBack="False"></asp:CheckBox></TD><TD style="WIDTH: 3px" align=left><ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w229" TargetControlID="FilterPeriod1" Format="dd/MM/yyyy" PopupButtonID="imbPeriod1"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left></TD><TD align=left><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w230" Visible="False"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w231"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w232"></asp:ImageButton> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w233"></asp:ImageButton> <asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w234" Visible="False" Checked="True"></asp:CheckBox></TD><TD style="WIDTH: 3px" align=left><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w235" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left><asp:Label id="Company" runat="server" CssClass="Important" __designer:wfdid="w236" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="InvoiceID" runat="server" CssClass="Important" __designer:wfdid="w237"></asp:Label></TD><TD style="WIDTH: 3px" align=left><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w238" TargetControlID="FilterPeriod2" Format="dd/MM/yyyy" PopupButtonID="imbPeriod2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left colSpan=3><asp:GridView id="tbldata" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w239" GridLines="None" DataKeyNames="trnjualmstoid,trnjualno" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="7" OnPageIndexChanging="tbldata_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trnjualmstoid" DataNavigateUrlFormatString="~\transaction\trnSIRecord.aspx?oid={0}" DataTextField="trnjualno" HeaderText="Invoice No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnjualno" HeaderText="SI No" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Invoice Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="SO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualno" HeaderText="DO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!" __designer:wfdid="w193"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="tbldata"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" /><strong> <span style="font-size: 9pt">
                                :: List Of Sales Invoice Record</span></strong>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel7" runat="server" RenderMode="Inline">
                                <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" __designer:wfdid="w35"><asp:View id="View1" runat="server" __designer:wfdid="w36"><asp:Label id="Label1" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information" __designer:wfdid="w37"></asp:Label><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="FONT-SIZE: x-small; WIDTH: 79px" align=left><asp:Label id="i_u" runat="server" Font-Size="X-Small" ForeColor="Red" Text="N E W" __designer:wfdid="w38" Visible="False"></asp:Label><asp:Label id="oid" runat="server" Font-Size="X-Small" __designer:wfdid="w39" Visible="False"></asp:Label></TD><TD align=left colSpan=5><asp:Label id="trnbelimstoid" runat="server" __designer:wfdid="w66" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 79px" align=left>Customer</TD><TD align=left colSpan=5><asp:TextBox id="custname" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w71" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" onclick="btnSearchCust_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w72"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" onclick="btnClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w73"></asp:ImageButton>&nbsp;<asp:Label id="custoid" runat="server" Text="custoid" __designer:wfdid="w74" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=15><asp:GridView id="gvCustomer" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w75" Visible="False" EmptyDataText="No data in database." OnPageIndexChanging="gvCustomer_PageIndexChanging" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="custoid,custcode,custname,CUSTADDR" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="CUSTCODE" HeaderText="User ID" ReadOnly="True" SortExpression="CUSTCODE">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Person Name" SortExpression="CUSTNAME">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTADDR" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="lblstatusdataCust" runat="server" Text="No Customer Data !" CssClass="Important"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 79px" align=left>SI&nbsp;No. <asp:Label id="Label4" runat="server" Width="1px" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w41"></asp:Label></TD><TD align=left colSpan=5><asp:TextBox id="trnjualno" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w67" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchPO" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w68"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearPO" onclick="btnClearPO_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w69"></asp:ImageButton> <asp:Label id="prefixoid" runat="server" Text="prefixoid" __designer:wfdid="w70" Visible="False"></asp:Label></TD></TR><TR><TD vAlign=top align=left colSpan=6><asp:GridView id="gvListPO" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w77" Visible="False" OnSelectedIndexChanged="gvListPO_SelectedIndexChanged" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnjualmstoid,trnjualno" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualmstoid" HeaderText="trnjualmstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="SI No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="SI Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualno" HeaderText="SJ No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="SO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lkbDetail" onclick="lkbDetail_Click" runat="server" __designer:wfdid="w79" ToolTip='<%# eval("trnjualno") %>'>Detail</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:GridView id="ItemGv" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w78" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdtlqty" HeaderText="Quantity" SortExpression="trnjualdtlqty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 79px" align=left>No. SI Manual</TD><TD align=left colSpan=5><asp:TextBox id="SIno" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w76" MaxLength="20"></asp:TextBox></TD></TR></TBODY></TABLE></asp:View>&nbsp;&nbsp; </asp:MultiView><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left>Last Update By <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w54"></asp:Label>&nbsp;On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w55"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSaveMstr" onclick="btnSaveMstr_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w56"></asp:ImageButton> <asp:ImageButton id="btnCancelMstr" onclick="btnCancelMstr_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w57"></asp:ImageButton> <asp:ImageButton id="btnDeleteMstr" onclick="btnDeleteMstr_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w58"></asp:ImageButton> <asp:ImageButton id="BtnPosting2" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w59" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:wfdid="w60" Visible="False"></asp:ImageButton> <asp:ImageButton id="Export" onclick="Export_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsBottom" __designer:dtid="1407374883553333" __designer:wfdid="w61" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w62" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><asp:SqlDataSource id="SqlDataSource2" runat="server" __designer:wfdid="w63" UpdateCommand="UPDATE [QL_trnbelibiayamst] SET [periodacctg] = @periodacctg, [trnbelimstoid] = @trnbelimstoid, [supplieroid] = @supplieroid, [trnbelibiayano] = @trnbelibiayano, [belibiayadate] = @belibiayadate, [belibiayanote] = @belibiayanote, [belibiayastatus] = @belibiayastatus, [upduser] = @upduser, [updtime] = @updtime, [totalamtbiaya] = @totalamtbiaya WHERE [cmpcode] = @cmpcode AND [belibiayamstoid] = @belibiayamstoid" InsertCommand="INSERT INTO [QL_trnbelibiayamst] ([cmpcode], [belibiayamstoid], [periodacctg], [trnbelimstoid], [supplieroid], [trnbelibiayano], [belibiayadate], [belibiayanote], [belibiayastatus], [upduser], [updtime], [totalamtbiaya]) VALUES (@cmpcode, @belibiayamstoid, @periodacctg, @trnbelimstoid, @supplieroid, @trnbelibiayano, @belibiayadate, @belibiayanote, @belibiayastatus, @upduser, @updtime, @totalamtbiaya)" DeleteCommand="DELETE FROM [QL_trnbelibiayamst] WHERE [cmpcode] = @cmpcode AND [belibiayamstoid] = @belibiayamstoid" SelectCommand="SELECT [cmpcode], [belibiayamstoid], [periodacctg], [trnbelimstoid], [supplieroid], [trnbelibiayano], [belibiayadate], [belibiayanote], [belibiayastatus], [upduser], [updtime], [totalamtbiaya] FROM [QL_trnbelibiayamst]" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"><DeleteParameters>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayamstoid" Type="Int32"></asp:Parameter>
</DeleteParameters>
<UpdateParameters>
<asp:Parameter Name="periodacctg" Type="String"></asp:Parameter>
<asp:Parameter Name="trnbelimstoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="supplieroid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="trnbelibiayano" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayadate" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="belibiayanote" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayastatus" Type="String"></asp:Parameter>
<asp:Parameter Name="upduser" Type="String"></asp:Parameter>
<asp:Parameter Name="updtime" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="totalamtbiaya" Type="Decimal"></asp:Parameter>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayamstoid" Type="Int32"></asp:Parameter>
</UpdateParameters>
<InsertParameters>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayamstoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="periodacctg" Type="String"></asp:Parameter>
<asp:Parameter Name="trnbelimstoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="supplieroid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="trnbelibiayano" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayadate" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="belibiayanote" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayastatus" Type="String"></asp:Parameter>
<asp:Parameter Name="upduser" Type="String"></asp:Parameter>
<asp:Parameter Name="updtime" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="totalamtbiaya" Type="Decimal"></asp:Parameter>
</InsertParameters>
</asp:SqlDataSource><asp:SqlDataSource id="SqlDataSource3" runat="server" __designer:wfdid="w64" UpdateCommand="UPDATE [QL_trnbelibiayadtl] SET [belibiayamstoid] = @belibiayamstoid, [belibiayaseq] = @belibiayaseq, [itembiayaoid] = @itembiayaoid, [itembiayanote] = @itembiayanote, [upduser] = @upduser, [itembiayaamt] = @itembiayaamt, [updtime] = @updtime WHERE [cmpcode] = @cmpcode AND [belibiayadtloid] = @belibiayadtloid" InsertCommand="INSERT INTO [QL_trnbelibiayadtl] ([cmpcode], [belibiayamstoid], [belibiayadtloid], [belibiayaseq], [itembiayaoid], [itembiayanote], [upduser], [itembiayaamt], [updtime]) VALUES (@cmpcode, @belibiayamstoid, @belibiayadtloid, @belibiayaseq, @itembiayaoid, @itembiayanote, @upduser, @itembiayaamt, @updtime)" DeleteCommand="DELETE FROM [QL_trnbelibiayadtl] WHERE [cmpcode] = @cmpcode AND [belibiayadtloid] = @belibiayadtloid" SelectCommand="SELECT [cmpcode], [belibiayamstoid], [belibiayadtloid], [belibiayaseq], [itembiayaoid], [itembiayanote], [upduser], [itembiayaamt], [updtime] FROM [QL_trnbelibiayadtl]" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"><DeleteParameters>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayadtloid" Type="Int32"></asp:Parameter>
</DeleteParameters>
<UpdateParameters>
<asp:Parameter Name="belibiayamstoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="belibiayaseq" Type="Int32"></asp:Parameter>
<asp:Parameter Name="itembiayaoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="itembiayanote" Type="String"></asp:Parameter>
<asp:Parameter Name="upduser" Type="String"></asp:Parameter>
<asp:Parameter Name="itembiayaamt" Type="Decimal"></asp:Parameter>
<asp:Parameter Name="updtime" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayadtloid" Type="Int32"></asp:Parameter>
</UpdateParameters>
<InsertParameters>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="belibiayamstoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="belibiayadtloid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="belibiayaseq" Type="Int32"></asp:Parameter>
<asp:Parameter Name="itembiayaoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="itembiayanote" Type="String"></asp:Parameter>
<asp:Parameter Name="upduser" Type="String"></asp:Parameter>
<asp:Parameter Name="itembiayaamt" Type="Decimal"></asp:Parameter>
<asp:Parameter Name="updtime" Type="DateTime"></asp:Parameter>
</InsertParameters>
</asp:SqlDataSource><asp:SqlDataSource id="SqlDataSource4" runat="server" __designer:wfdid="w65" UpdateCommand="UPDATE [QL_cashbankgl] SET [cashbankoid] = @cashbankoid, [acctgoid] = @acctgoid, [cashbankglamt] = @cashbankglamt, [cashbankglnote] = @cashbankglnote, [cashbankglstatus] = @cashbankglstatus, [cashbankglres1] = @cashbankglres1, [upduser] = @upduser, [updtime] = @updtime, [duedate] = @duedate, [refno] = @refno WHERE [cmpcode] = @cmpcode AND [cashbankgloid] = @cashbankgloid" InsertCommand="INSERT INTO [QL_cashbankgl] ([cmpcode], [cashbankgloid], [cashbankoid], [acctgoid], [cashbankglamt], [cashbankglnote], [cashbankglstatus], [cashbankglres1], [upduser], [updtime], [duedate], [refno]) VALUES (@cmpcode, @cashbankgloid, @cashbankoid, @acctgoid, @cashbankglamt, @cashbankglnote, @cashbankglstatus, @cashbankglres1, @upduser, @updtime, @duedate, @refno)" DeleteCommand="DELETE FROM [QL_cashbankgl] WHERE [cmpcode] = @cmpcode AND [cashbankgloid] = @cashbankgloid" SelectCommand="SELECT [cmpcode], [cashbankgloid], [cashbankoid], [acctgoid], [cashbankglamt], [cashbankglnote], [cashbankglstatus], [cashbankglres1], [upduser], [updtime], [duedate], [refno] FROM [QL_cashbankgl]" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"><DeleteParameters>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="cashbankgloid" Type="Int32"></asp:Parameter>
</DeleteParameters>
<UpdateParameters>
<asp:Parameter Name="cashbankoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="acctgoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="cashbankglamt" Type="Decimal"></asp:Parameter>
<asp:Parameter Name="cashbankglnote" Type="String"></asp:Parameter>
<asp:Parameter Name="cashbankglstatus" Type="String"></asp:Parameter>
<asp:Parameter Name="cashbankglres1" Type="String"></asp:Parameter>
<asp:Parameter Name="upduser" Type="String"></asp:Parameter>
<asp:Parameter Name="updtime" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="duedate" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="refno" Type="String"></asp:Parameter>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="cashbankgloid" Type="Int32"></asp:Parameter>
</UpdateParameters>
<InsertParameters>
<asp:Parameter Name="cmpcode" Type="String"></asp:Parameter>
<asp:Parameter Name="cashbankgloid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="cashbankoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="acctgoid" Type="Int32"></asp:Parameter>
<asp:Parameter Name="cashbankglamt" Type="Decimal"></asp:Parameter>
<asp:Parameter Name="cashbankglnote" Type="String"></asp:Parameter>
<asp:Parameter Name="cashbankglstatus" Type="String"></asp:Parameter>
<asp:Parameter Name="cashbankglres1" Type="String"></asp:Parameter>
<asp:Parameter Name="upduser" Type="String"></asp:Parameter>
<asp:Parameter Name="updtime" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="duedate" Type="DateTime"></asp:Parameter>
<asp:Parameter Name="refno" Type="String"></asp:Parameter>
</InsertParameters>
</asp:SqlDataSource><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD vAlign=top align=left></TD><TD vAlign=top align=left></TD></TR><TR><TD vAlign=top align=left>&nbsp;</TD><TD vAlign=top align=left></TD></TR><TR><TD vAlign=top align=left></TD><TD style="WIDTH: 87px" vAlign=top align=left></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="Export"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" /> <strong><span style="font-size: 9pt">
                                :: Form Sales Invoice Record</span></strong>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" __designer:wfdid="w7" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w9"></asp:Image> </TD><TD class="Label" vAlign=middle align=left><asp:Label id="lblMessage" runat="server" ForeColor="Red" __designer:wfdid="w10"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" __designer:wfdid="w11" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w12"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" __designer:wfdid="w13" TargetControlID="beMsgBox" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" CausesValidation="False" __designer:wfdid="w14" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
        
</asp:Content>
