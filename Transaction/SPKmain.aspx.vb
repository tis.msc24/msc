Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows.Forms
Imports ClassFunction
Imports ClassProcedure
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class SpkMain
    Inherits System.Web.UI.Page

#Region "Variables"
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd2 As New SqlCommand("", conn2)
    Dim xreader2 As SqlDataReader
    Dim cKoneksi As New Koneksi
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function GetMinDateTime() As Date
        Return CDate(cKon.ambilscalar("select CAST(0 AS datetime)"))
    End Function
#End Region

#Region "Procedure"
    Sub clearspkprocessitemresult()
        outputqty1.CssClass = "inpTextDisabled"
        outputqty1.Enabled = False
        outputtype.Enabled = True
        outputref.Text = ""
        outputoid.Text = ""
        outputname.Text = ""
        unit1unit2conv.Text = "0"
        unit2unit3conv.Text = "0"
        outputqty2.Text = 0
        outputqty1.Text = 0
        notedtlitemresult.Text = ""
        outputunit1.Items.Clear()
        outputunit2.Items.Clear()

        If Session("spkprocessitemResult") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("spkprocessitemResult")
            Dim DV As DataView = objTable.DefaultView
            DV.Sort = "spkdtloid, spkprocessoid, outputname"
            objTable = DV.Table
            Session("spkprocessitemResult") = DV.Table
            gvprocessitemresult.DataSource = objTable
            gvprocessitemresult.DataBind()
        End If
        i_u_spkprocessitemresult.Text = "New Detail"
        MultiView1.ActiveViewIndex = 1

        processname_1.Enabled = True
        processname_1.CssClass = "inpText"
        spkdtloid_3.Enabled = True
        spkdtloid_3.CssClass = "inpText"
        outputtype.Enabled = True
        outputtype.CssClass = "inpText"
        CProc.DisposeGridView(GVMat2) : GVMat2.Visible = False
        CProc.DisposeGridView(gvItem2) : gvItem2.Visible = False
        gvprocessitemresult.Columns(0).Visible = True
        gvprocessitemresult.Columns(9).Visible = True
        gvprocessitemresult.SelectedIndex = -1
    End Sub

    Sub ClearProcessMaterial()
        matoid.Text = "" : matdesc.Text = ""
        conv1_2.Text = "" : conv2_3.Text = ""
        matqty2.Text = 0 : mattolerancepct.Text = 0
        matqty.Text = 0 : matqty2.Text = 0
        wastestd.Text = 0 : notedtlprosesmat.Text = ""
        matunit.Items.Clear() : matunit2.Items.Clear()
        i_u3.Text = ""

        If Session("spkprocessmaterial") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("spkprocessmaterial")
            GVprocessmaterial.DataSource = objTable
            GVprocessmaterial.DataBind()
        End If
        i_u_spkprocessmaterial.Text = "New Detail"
        MultiView1.ActiveViewIndex = 2

        processname_2.Enabled = True : spkdtloid_4.Enabled = True
        processname_2.CssClass = "inpText"
        spkdtloid_4.CssClass = "inpText"
        btnSearchMat.Visible = True : btnClearMat.Visible = True
        GVMat3.Visible = False
        GVprocessmaterial.Columns(0).Visible = True
        GVprocessmaterial.Columns(9).Visible = True
        GVprocessmaterial.SelectedIndex = -1
    End Sub

    Public Sub BindDataMatPopup(ByVal sqltext As String)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "select regionoid  matoid, i.regioncode matcode, i.regiondesc  matlongdesc ,i.tarif  qty, '' matunit,0 matunitoid From ql_mstregion i Where i.cmpcode='" & cmpcode & "' and (i.regionname  like '%" & Tchar(outputname.Text) & "%' or i.regiondesc like '%" & Tchar(outputname.Text) & "%')"
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "matWIP")
        GVMat2.Visible = True
        GVMat2.DataSource = objTable
        GVMat2.DataBind() : GVMat2.SelectedIndex = -1
    End Sub

    Private Sub InitLoc()

        sSql = "SELECT a.genoid,ISNULL((SELECT gendesc FROM QL_mstgen g WHERE g.genoid=a.genother1 AND g.gengroup = 'WAREHOUSE'),'GUDANG PERJALANAN') +' - '+a.gendesc AS gendesc,* From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.gencode <> 'EXPEDISI' AND c.gencode='" & division.SelectedValue & "'"
        FillDDL(LocOid, sSql)
    End Sub

    Public Sub BindDataMatPopup2(ByVal sqltext As String)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "Select * FROM (Select i.itemoid matoid, i.itemcode matcode, i.itemdesc matlongdesc,g.gendesc matunit,i.satuan1 matunitoid,Isnull((Select ISNULL(saldoAkhir,0.00) From QL_crdmtr crd Where crd.refoid=i.itemoid AND crd.periodacctg='" & GetDateToPeriodAcctg(CDate(GetServerTime())) & "' AND mtrlocoid=" & LocOid.SelectedValue & " AND crd.branch_code='" & division.SelectedValue & "'),0.00) Qty From ql_mstitem i inner join QL_mstgen g on g.genoid=satuan1 And i.cmpcode='MSC' And (itemcode like '%" & Tchar(matdesc.Text) & "%' or itemdesc like '%" & Tchar(matdesc.Text) & "%')) As t Where Qty>0"
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "ql_mstsparepart")
        GVMat3.Visible = True
        GVMat3.DataSource = objTable
        GVMat3.DataBind() : GVMat3.SelectedIndex = -1
    End Sub

    Sub ClearSPKProcessItem()
        spkprocessoid.Text = ""

        planstartdate.Text = ""
        planenddate.Text = ""
        planstarttime.Text = ""
        planendtime.Text = ""
        outputqtytolerance.Text = "0"
        stdpreptime.Text = 0
        stddowntime.Text = 0
        stdspeed.Text = 0
        stdprodtime.Text = 0
        note.Text = ""
        spkprodstatus.Text = "In Process"

        i_u_spkprocessitem.Text = "New Detail"
        processoid.Enabled = True
        processoid.CssClass = "inpText"
        seqprocess1.Enabled = True
        seqprocess1.CssClass = "inpText"
        spkdtloid_2.Enabled = True
        spkdtloid_2.CssClass = "inpText"

        If Session("spkprocessitem") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("spkprocessitem")
            Dim DV As DataView = objTable.DefaultView
            DV.Sort = "spkdtloid, seqprocessitem, spkprocessoid"
            objTable = DV.Table
            Session("spkprocessitem") = objTable
            gvprocessitem.DataSource = objTable
            gvprocessitem.DataBind()
        End If

        gvprocessitem.SelectedIndex = -1
        gvprocessitem.Columns(12).Visible = True
        gvprocessitem.Columns(0).Visible = True
        MultiView1.ActiveViewIndex = 1
    End Sub

    Sub initDDLMachine()
        'If processoid.SelectedValue <> "None" Then
        FillDDL(machinedtloid, "select 0 machinedtloid, '' machinedesc ")
        '    'If machinedtloid.Items.Count = 0 Then
        '    '    showMessage("No Machine Found for this Process", "Warning")
        '    '    Exit Sub
        '    'End If
        'End If
        'machinedtloid.Items.Add("None")
        'machinedtloid.Items(machinedtloid.Items.Count - 1).Value = "None"
        'machinedtloid.SelectedValue = "None"
    End Sub

    Sub initDDLProcess()
        FillDDL(processoid, "select distinct g.genoid, g.gendesc from ql_mstgen g where g.gengroup = 'PROCESS' order by gendesc")
        processoid.Items.Add("None")
        processoid.Items(processoid.Items.Count - 1).Value = "None"
        processoid.SelectedValue = "None"
        initDDLMachine()
    End Sub

    Sub inittolerance()
        FillDDL(ddltolerance, "select distinct g.gendesc, g.gendesc from ql_mstgen g where g.gengroup = 'tolerancespk' order by g.gendesc")
        ddltolerance.Items.Add("0")
        ddltolerance.Items(ddltolerance.Items.Count - 1).Value = "0"
        ddltolerance.SelectedValue = "0"
        'initDDLMachine()
    End Sub

    Sub initAllDDl()
        Dim kCbg As String = ""
        If Session("UserID").ToString.ToUpper <> "ADMIN" Then
            kCbg &= "And g2.gencode='" & Session("branch_id").ToString & "'"
        End If
        FillDDL(division, "select gencode, gendesc from ql_mstgen Where gengroup='CABANG' " & kCbg & "")
        'division.Items.Add("None")
        'division.Items(division.Items.Count - 1).Value = "None"
        'If division.Items.Count > 1 Then
        '    division.SelectedIndex = 0
        'Else
        '    'division.SelectedValue = "None"
        '    FillDDL(division, "select 0 genoid , '' gendesc  ")
        'End If

        ' DYNAMIC PROCESS SEQ => QL_mstgen Group PROCESS_SEQ_COUNT
        sSql = "SELECT TOP 1 genother1 FROM QL_mstgen WHERE cmpcode='" & cmpcode & "' AND gengroup='PROCESS_SEQ_COUNT' ORDER BY genoid DESC "
        Dim iMaxCountSeq As Integer = ToDouble(CStr(cKon.ambilscalar(sSql)))
        If iMaxCountSeq > 0 Then
            seqprocess1.Items.Clear()
            For C2 As Integer = 1 To iMaxCountSeq
                seqprocess1.Items.Add(New ListItem(C2, C2))
            Next
        Else
            showMessage("Tidak ada Pengaturan data untuk Seq Proses Dinamis.<BR>Silakan Setup pada Data General Grup 'PROCESS_SEQ_COUNT '!!", CompnyName & " - WARNING")
        End If

        sSql = "Select genoid, gendesc, gengroup From QL_mstgen Where gengroup in ('SERVICETYPE') and cmpcode like '%" & cmpcode & "%' And genoid IN (Select Typejob From QL_mstitemserv it Where it.Typejob=genoid) ORDER BY gendesc"
        FillDDL(outputtype, sSql)
        outputtype.Items.Add("Region")
        outputtype.SelectedValue = "Region"

        initDDLProcess()
        inittolerance()
    End Sub

    Sub generatenoSPK()
        sSql = "select genother1 from QL_mstgen WHere gengroup='CABANG' And gencode='" & division.SelectedValue & "'"
        Dim dCode As String = GetStrData(sSql)
        Dim sPrefik As String = "/SPK/" & dCode & "/" & Format(GetServerTime(), "dd/MM/yyyy") & "/"
        sSql = "SELECT isnull(max(abs(replace(spkno, '" & sPrefik & "',''))),0)  from QL_trnspkmainprod WHERE cmpcode='" & cmpcode & "' AND spkno like '%" & sPrefik & "'"
        Dim iNextNo As Integer = cKoneksi.ambilscalar(sSql) + 1
        spkno.Text = GenNumberString("", "", iNextNo, 3) & sPrefik
    End Sub

    Public Sub BindData(ByVal sSqlPlus As String)
        If Not IsDate(toDate(FilterPeriod1.Text)) Then
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        End If
        If Not IsDate(toDate(FilterPeriod2.Text)) Then
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If

        sSql = "Select spkmainoid,division,spkstatus,spkno,spkopendate,spkplanstart,spkplanend,spkaddnote From QL_trnspkmainprod s Where spkopendate Between '" & toDate(FilterPeriod1.Text) & "' and '" & toDate(FilterPeriod2.Text) & "' And spkstatus like '" & FilterStatus.SelectedValue & "' and " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%' Order By spkplanstart desc, spkplanend desc"
        FillGV(GVtrnspkmain, sSql, "QL")
    End Sub

    Private Sub FillTextBox(ByVal id As Integer)
        Dim sMsg As String = ""
        Try
            Dim dr As DataRow
            sSql = "select a.spkmainoid,a.division,a.spkstatus,a.spkno,a.spkopendate,a.spkplanstart,a.spkplanend,a.spkaddnote, a.updtime, a.upduser,a.spkjenisbarang,a.spkpic,b.username personname From QL_trnspkmainprod a left join ql_mstprof b on a.spkpic = b.userid Where a.spkmainoid = " & id
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            While xreader.Read

                If xreader("spkpic").ToString <> "" Then
                    lblpic.Text = xreader("spkpic")
                    tbpic.Text = xreader("personname")
                Else
                    lblpic.Text = "" : tbpic.Text = ""
                End If

                spkmainoid.Text = xreader("spkmainoid")
                division.SelectedValue = xreader("division")
                If xreader("spkstatus").ToString.Trim = "Approval" Then
                    spkstatus.Text = "In Approval"
                Else
                    spkstatus.Text = xreader("spkstatus")
                End If
                spkno.Text = xreader("spkno")
                spkopendate.Text = Format(xreader("spkopendate"), "dd/MM/yyyy")
                spkplanstart.Text = Format(xreader("spkplanstart"), "dd/MM/yyyy")
                spkplanstarttime.Text = Format(xreader("spkplanstart"), "HH:mm")
                spkplanend.Text = Format(xreader("spkplanend"), "dd/MM/yyyy")
                spkplanendtime.Text = Format(xreader("spkplanend"), "HH:mm")
                spkaddnote.Text = xreader("spkaddnote")
                Upduser.Text = xreader("upduser")
                Updtime.Text = Format(xreader("Updtime"), "dd/MM/yyyy HH:mm:ss")
                spkjenisbarang.Text = xreader("spkjenisbarang")
            End While
            xreader.Close()
            conn.Close()

            FillDDL(resultunitoid1, "select  g1.genoid, g1.gendesc from ql_mstgen g1 Where g1.genoid=" & spkjenisbarang.Text & " and gengroup = 'REQITEMTYPE' ")

            ' ============= NEW query perubahan desckripsi BOM  ===========
            sSql = "select 0 ioseq, r.iono, '' custname, r.refoid itemoid, r.refname, id.REQITEMNAME itemlongdesc, id.CREATETIME ioestdeliverydate, r.resultqty1, r.resultunitoid1, g1.gendesc unit1, r.resultqty2, r.resultunitoid2, g2.gendesc sunit2, r.iodtloid, " & _
                     "r.bomoid, 'Without BOM' bomdesc,r.spkdtloid " & _
                     "From QL_trnspkResultItem r " & _
                     "inner join ql_trnrequest id on id.REQOID=r.iodtloid " & _
                     "inner join QL_mstgen g1 on g1.genoid=r.resultunitoid1 " & _
                     "left join QL_mstgen g2 on g2.genoid=r.resultunitoid2 " & _
                     "Where r.spkmainoid=" & spkmainoid.Text & " order by r.spkdtloid "
            Dim oTblRI As DataTable = CreateDataTableFromSQL(sSql)
            spkdtloid_2.Items.Clear()
            spkdtloid_3.Items.Clear()
            spkdtloid_4.Items.Clear()

            For c1 As Int16 = 0 To oTblRI.Rows.Count - 1
                dr = oTblRI.Rows(c1)
                dr.BeginEdit()
                dr("ioseq") = c1 + 1
                dr("spkdtloid") = c1 + 1
                spkdtloid_2.Items.Add(oTblRI.Rows(c1).Item("spkdtloid"))
                spkdtloid_3.Items.Add(oTblRI.Rows(c1).Item("spkdtloid"))
                spkdtloid_4.Items.Add(oTblRI.Rows(c1).Item("spkdtloid"))
                dr.EndEdit()
            Next

            btnAddToList1.Visible = False

            'get data QL_trnspkProcessItem
            sSql = "Select p.spkdtloid , p.seqprocessitem, p.spkprocessoid, g.gendesc spkprocessname, p.processoid, p.planstart,p.planend, p.machineoid, p.machinedtloid, '' machine, p.outputqtytolerance, p.stdpreptime, p.stddowntime, p.stdspeed,p.stdprodtime, p.note, p.spkprodstatus, p.spkprocessoid AS ID_ne_CUK From QL_trnspkProcessItem p inner join ql_mstgen g on g.genoid=p.processoid Where p.spkmainoid=" & spkmainoid.Text & " Order By p.spkdtloid, p.spkprocessoid, p.seqprocessitem"

            Dim oTblPI As DataTable = CreateDataTableFromSQL(sSql)
            Dim iNoTemp As Int64 = 0
            Dim nomere As Int16 = 0
            Dim Tempspkprocessoid As Int64 = 0
            Dim ispkprocessoid As Int64 = 0
            For c1 As Int16 = 0 To oTblPI.Rows.Count - 1
                If iNoTemp <> oTblPI.Rows(c1).Item("spkdtloid") Then
                    nomere += 1
                    iNoTemp = oTblPI.Rows(c1).Item("spkdtloid")
                End If
                If Tempspkprocessoid <> oTblPI.Rows(c1).Item("spkprocessoid") Then
                    ispkprocessoid += 1
                    Tempspkprocessoid = oTblPI.Rows(c1).Item("spkprocessoid")
                End If
                dr = oTblPI.Rows(c1)
                dr.BeginEdit()
                dr("spkdtloid") = nomere
                dr("spkprocessoid") = ispkprocessoid
                dr.EndEdit()

                processname_1.Items.Add(New ListItem(oTblPI.Rows(c1).Item("spkprocessname"), ispkprocessoid))
                processname_2.Items.Add(New ListItem(oTblPI.Rows(c1).Item("spkprocessname"), ispkprocessoid))
                Session("spkprocessoid") = oTblPI.Rows(c1).Item("spkprocessoid") + 1
            Next

            'get data QL_trnspkProcessItemResult
            sSql = "select p.spkdtloid, r.spkprocessoid, r.outputoid, case outputref when 'QL_MSTREGION' then (select re.regiondesc from Ql_mstregion re Where re.regionoid =r.outputoid) else (select m.itservdesc From QL_MSTITEMSERV m where m.itservoid=r.outputoid) end as outputname, r.outputtype, r.outputqty1, r.outputunitoid1, isnull(g1.gendesc,'') outputunit1, r.outputqty2, r.outputunitoid2, isnull(g2.gendesc,'') outputunit2, r.outputref, g0.gendesc spkprocessname, r.note,isnull(job.gendesc,'Region') typejob from QL_trnspkProcessItemResult r inner join QL_trnspkProcessItem p on p.spkprocessoid=r.spkprocessoid left join QL_MSTITEMSERV srv on r.outputoid = srv.ITSERVOID and r.outputref = 'ql_mstitemserv' left join QL_mstgen job on srv.ITSERVTYPEOID = job.genoid left join QL_mstgen g1 on g1.genoid=r.outputunitoid1 inner join QL_mstgen g0 on g0.genoid=p.processoid left join QL_mstgen g2 on g2.genoid=r.outputunitoid2 Where p.spkmainoid=" & spkmainoid.Text & " order by p.spkprocessoid asc, outputref desc  "

            Dim oTblPIr As DataTable = CreateDataTableFromSQL(sSql)
            iNoTemp = 0 : nomere = 0
            Tempspkprocessoid = 0
            ispkprocessoid = 0
            For c1 As Int16 = 0 To oTblPIr.Rows.Count - 1
                If iNoTemp <> oTblPIr.Rows(c1).Item("spkdtloid") Then
                    nomere += 1
                    iNoTemp = oTblPIr.Rows(c1).Item("spkdtloid")
                End If
                If Tempspkprocessoid <> oTblPIr.Rows(c1).Item("spkprocessoid") Then
                    ispkprocessoid += 1
                    Tempspkprocessoid = oTblPIr.Rows(c1).Item("spkprocessoid")
                End If

                dr = oTblPIr.Rows(c1)
                dr.BeginEdit()
                dr("spkdtloid") = nomere
                dr("spkprocessoid") = ispkprocessoid
                dr.EndEdit()
            Next

            'get data QL_trnspkProcessmaterial
            sSql = "Select p.spkdtloid, r.spkprocessmatoid, p.spkprocessoid, r.matoid, r.matref, r.qty1, r.unitoid1, g1.gendesc unit1, r.qty2, r.unitoid2, g2.gendesc unit2, r.wastestd, r.matroutetolerancepct , g0.gendesc spkprocessname, m.itemdesc matlongdesc, r.note, r.mtrlocoid From QL_trnspkProcessmaterial r inner join QL_trnspkProcessItem p on p.spkprocessoid=r.spkprocessoid inner join ql_mstitem m on m.itemoid =r.matoid inner join QL_mstgen g1 on g1.genoid=r.unitoid1 inner join QL_mstgen g0 on g0.genoid=p.processoid left join QL_mstgen g2 on g2.genoid=r.unitoid2 Where p.spkmainoid=" & spkmainoid.Text & " order by p.spkdtloid,r.spkprocessmatoid"

            Dim oTblPm As DataTable = CreateDataTableFromSQL(sSql)
            iNoTemp = 0 : nomere = 0
            Tempspkprocessoid = 0
            ispkprocessoid = 0
            For C1 As Int16 = 0 To oTblPm.Rows.Count - 1
                If iNoTemp <> oTblPm.Rows(C1).Item("spkdtloid") Then
                    nomere += 1
                    iNoTemp = oTblPm.Rows(C1).Item("spkdtloid")
                End If
                If Tempspkprocessoid <> oTblPm.Rows(C1).Item("spkprocessoid") Then
                    ispkprocessoid += 1
                    Tempspkprocessoid = oTblPm.Rows(C1).Item("spkprocessoid")
                End If

                dr = oTblPm.Rows(C1) : dr.BeginEdit()
                dr("spkdtloid") = nomere
                ' dr("spkprocessoid") = ispkprocessoid ====> See Re-Sequence process below !!!
                dr("spkprocessmatoid") = C1 + 1
                dr.EndEdit()
            Next
            Session("spkprocessmatoid") = oTblPm.Rows.Count + 1

            ' =============== Re-Sequence for spkprocessoid => Bingung karo sing duwur c*k !!
            For C5 As Int16 = 0 To oTblPI.Rows.Count - 1
                Dim dvPIM As DataView = oTblPm.DefaultView
                dvPIM.RowFilter = "spkprocessoid=" & oTblPI.Rows(C5)("ID_ne_CUK").ToString
                If dvPIM.Count > 0 Then
                    For C6 As Integer = 0 To dvPIM.Count - 1
                        dvPIM(0)("spkprocessoid") = oTblPI.Rows(C5)("spkprocessoid").ToString
                    Next
                End If
                dvPIM.RowFilter = ""
            Next
            oTblPm.AcceptChanges()
            ' ===============

            Session("spkresultitem") = oTblRI
            Session("spkprocessitem") = oTblPI
            Session("SpkProcessItemResult") = oTblPIr
            Session("SpkProcessMaterial") = oTblPm

            tblspkresultitem.DataSource = oTblRI
            tblspkresultitem.DataBind()
            gvprocessitem.DataSource = oTblPI
            gvprocessitem.DataBind()
            gvprocessitemresult.DataSource = oTblPIr
            gvprocessitemresult.DataBind()
            GVprocessmaterial.DataSource = oTblPm
            GVprocessmaterial.DataBind()
            FillProcessname(spkdtloid_3.SelectedValue, processname_1)
            FillProcessname(spkdtloid_4.SelectedValue, processname_2)
        Catch ex As Exception
            sMsg &= "Unable to load data!!<BR><BR>Info :<BR>" & ex.ToString & "<BR><BR>"
        End Try
        If sMsg <> "" Then
            showMessage(sMsg, "ERROR ")
        End If
        If Session("spkresultitem") Is Nothing = False Then
            GetDataProcess()
        End If
    End Sub

    Sub ProcessCalculateRangePlanSPK()
        Dim planstart As DateTime = "01/01/1900" ' Minimum Start Date
        Dim planend As DateTime = "01/01/1900" ' Maximum End Date

        Dim dt As DataTable = Session("spkprocessitem")
        For c1 As Int16 = 0 To dt.Rows.Count - 1
            If c1 = 0 Then
                planstart = dt.Rows(c1).Item("planstart")
                planend = dt.Rows(c1).Item("planend")
            Else
                If dt.Rows(c1).Item("planstart") < planstart Then
                    planstart = dt.Rows(c1).Item("planstart")
                End If
                If dt.Rows(c1).Item("planend") > planend Then
                    planend = dt.Rows(c1).Item("planend")
                End If
            End If
        Next

        spkplanendtime.Text = Format(planend, "HH:mm")
        spkplanend.Text = Format(planend, "dd/MM/yyyy")
        spkplanstarttime.Text = Format(planstart, "HH:mm")
        spkplanstart.Text = Format(planstart, "dd/MM/yyyy")
    End Sub

    Private Sub ClearDetailIO()
        If Session("spkresultitem") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("spkresultitem")
            ioseq.Text = objTable.Rows.Count + 1
        End If
        i_u_spkresultitem.Text = "New Detail"

        bomunitoid.Text = 0
        bomqty.Text = 0
        iono.Text = ""
        custname.Text = ""
        refoid.Text = ""
        itemlongdesc.Text = ""
        ioestdeliverydate.Text = ""
        resultqty1.Text = 0
        resultqty2.Text = 0
        iodtloid.Text = 0
        bomoid.Text = 0
        bomdesc.Text = ""
        maxqty.Text = "0"
        TABLE1.Visible = False
        MultiView1.ActiveViewIndex = 0



    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
    End Sub

    Public Sub binddataIO(ByVal swhere As String)
        sSql = "Select distinct iono, iorefno, custname, consaddress, itemoid, itemlongdesc, ioestdeliverydate, ioqty, ioqtyunitoid, iounit1, iodtloid, bomoid, itemgroupcode, qtyspk from( Select BARCODE iono, ''iorefno,custname, m.custaddr consaddress, i.REQOID itemoid, i.REQITEMNAME itemlongdesc, i.CREATETIME ioestdeliverydate, 1 ioqty, i.REQITEMTYPE ioqtyunitoid, g.gendesc iounit1, i.REQOID iodtloid, 0 bomoid, 0 itemgroupcode, isnull((select sum(resultqty1) From QL_trnspkresultitem Where iodtloid = i.REQOID AND i.branch_code='" & division.SelectedValue & "'),0) qtySPK from ql_trnrequest i inner join ql_mstcust m on i.cmpcode=m.cmpcode and i.reqcustoid=m.custoid inner join QL_mstgen g on i.REQITEMTYPE = g.genoid Where i.cmpcode='" & cmpcode & "' and i.REQFLAG = 'OUT' AND i.branch_code='" & division.SelectedValue & "' AND REQSTATUS NOT IN ('CLOSED') " & swhere & ") temp Where itemlongdesc like '%" & Tchar(ioitemdescfilter.Text) & "%' and iono like '%" & Tchar(iono.Text) & "%' and custname like '%" & Tchar(iocustnameFilter.Text) & "%' order by iono asc "
        FillGV(gvItem, sSql, "IO")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            Response.Redirect("~\Transaction\spkmain.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

        Session("oid") = Request.QueryString("oid")
        BtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah Anda yakin menghapus data ini ?');")
        BtnPosting.Attributes.Add("OnClick", "javascript:return confirm('Data yang sudah diposting tidak bisa diupdate lagi, \n Anda yakin ingin melanjutkan ?');")

        Page.Title = CompnyName & " - SPK MAIN"

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "UPDATE"
        Else
            i_u.Text = "N E W"
        End If

        If Not IsPostBack Then
            spkopendate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            BindData(cmpcode) : initAllDDl() : InitLoc()
            division_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                If spkstatus.Text = "Approved" Or spkstatus.Text = "CLOSED" Or spkstatus.Text = "In Approval" Then
                    BtnDelete.Visible = False : BtnPosting.Visible = False
                    btnSave.Visible = False : ibapprove.Visible = False
                Else
                    'BtnPosting.Visible = True
                    BtnDelete.Visible = True : ibapprove.Visible = True
                End If
            Else
                spkmainoid.Text = GenerateID("ql_trnspkmainprod", cmpcode)
                Me.spkopendate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                FilterPeriod1.Text = Format(GetServerTime().AddDays(-7), "dd/MM/yyyy")
                FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
                spkno.Text = spkmainoid.Text
                BtnDelete.Visible = False : BtnPosting.Visible = False
                ibapprove.Visible = False : btnSave.Visible = True
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click, LinkButton13.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton6.Click, LinkButton10.Click
        MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton8.Click, LinkButton11.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click
        If spkmainoid.Text.Trim = "" Then
            showMessage("invalid spkmainoid!", CompnyName & " - Warning")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'delete QL_trnspkprocessmaterial
            sSql = "Delete from QL_trnspkprocessmaterial where cmpcode='" & cmpcode & "' and spkprocessoid in ( select s.spkprocessoid from ql_trnspkprocessitem s where s.spkmainoid=" & spkmainoid.Text & "  and s.cmpcode='" & cmpcode & "' )    "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'delete QL_trnspkprocessitemresult
            sSql = "Delete from QL_trnspkprocessitemresult where cmpcode='" & cmpcode & "' and spkprocessoid in ( select s.spkprocessoid from ql_trnspkprocessitem s where s.spkmainoid=" & spkmainoid.Text & "  and s.cmpcode='" & cmpcode & "' )    "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'delete QL_trnspkprocessitem
            sSql = "Delete from  ql_trnspkprocessitem   where  cmpcode='" & cmpcode & "' and  spkmainoid=" & spkmainoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'delete QL_trnspkresultitem
            sSql = "Delete from QL_trnspkresultitem where  cmpcode='" & cmpcode & "' and  spkmainoid = " & spkmainoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'delete QL_trnspkmain
            sSql = "Delete from QL_trnspkmainprod where  cmpcode='" & cmpcode & "' and  spkmainoid = " & spkmainoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR")
            Exit Sub
        End Try
        Response.Redirect("~\transaction\SPKmain.aspx?awal=true")
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\transaction\SPKmain.aspx?awal=true")
    End Sub

    Protected Sub btnsearchIO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearchIO.Click
        Dim sValidate As String = ""
        'If division.SelectedValue = "None" Then sValidate &= "- Pilih departemen dahulu !!<BR>"
        If Not IsDate(toDate(deliverydate1.Text)) And deliverydate1.Text <> "" Then sValidate &= "- Format awal periode salah !!<BR>"
        If Not IsDate(toDate(deliverydate2.Text)) And deliverydate2.Text <> "" Then sValidate &= "- Format akhir periode salah !!<BR>"
        If IsDate(toDate(deliverydate1.Text)) And deliverydate1.Text <> "" And _
        IsDate(toDate(deliverydate2.Text)) And deliverydate2.Text <> "" Then
            If CDate(toDate(deliverydate1.Text)) > CDate(toDate(deliverydate2.Text)) Then
                sValidate &= "- Tanggal awal periode tidak boleh lebih dari akhir periode !!<BR>"
            End If
        End If
        If sValidate <> "" Then
            showMessage(sValidate, CompnyName & " - WARNING") : Exit Sub
        End If

        Dim sWhere As String = ""
        If deliverydate1.Text <> "" Then
            sWhere &= " and i.CREATETIME >= '" & CDate(toDate(deliverydate1.Text)) & "' "
        End If
        If deliverydate2.Text <> "" Then
            sWhere &= " and i.CREATETIME <= '" & CDate(toDate(deliverydate2.Text)) & "' "
        End If

        gvItem.PageIndex = 0 : binddataIO(sWhere)
    End Sub

    Protected Sub btnshowiosearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowiosearch.Click
        Dim sValidate As String = ""
        'If division.SelectedValue = "None" Then division.SelectedValue = "0" 'sValidate &= "- Pilih departemen dahulu !!<BR>"
        If Not IsDate(toDate(deliverydate1.Text)) And deliverydate1.Text <> "" Then sValidate &= "- Format awal periode salah !!<BR>"
        If Not IsDate(toDate(deliverydate2.Text)) And deliverydate2.Text <> "" Then sValidate &= "- Format akhir periode salah !!<BR>"
        If IsDate(toDate(deliverydate1.Text)) And deliverydate1.Text <> "" And _
        IsDate(toDate(deliverydate2.Text)) And deliverydate2.Text <> "" Then
            If CDate(toDate(deliverydate1.Text)) > CDate(toDate(deliverydate2.Text)) Then
                sValidate &= "- Tanggal awal periode tidak boleh lebih dari akhir periode !!<BR>"
            End If
        End If
        If sValidate <> "" Then
            showMessage(sValidate, CompnyName & " - WARNING") : Exit Sub
        End If

        Dim sWhere As String = ""
        If deliverydate1.Text <> "" Then
            sWhere &= " and id.deliverydate>='" & CDate(toDate(deliverydate1.Text)) & "' "
        End If
        If deliverydate2.Text <> "" Then
            sWhere &= " and id.deliverydate<='" & CDate(toDate(deliverydate2.Text)) & "' "
        End If

        gvItem.PageIndex = 0 : binddataIO(sWhere) : TABLE1.Visible = True
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex

        Dim sValidate As String = ""
        If Not IsDate(toDate(deliverydate1.Text)) And deliverydate1.Text <> "" Then sValidate &= "- Format awal periode salah !!<BR>"
        If Not IsDate(toDate(deliverydate2.Text)) And deliverydate2.Text <> "" Then sValidate &= "- Format akhir periode salah !!<BR>"
        If IsDate(toDate(deliverydate1.Text)) And deliverydate1.Text <> "" And _
        IsDate(toDate(deliverydate2.Text)) And deliverydate2.Text <> "" Then
            If CDate(toDate(deliverydate1.Text)) > CDate(toDate(deliverydate2.Text)) Then
                sValidate &= "- Tanggal awal periode tidak boleh lebih dari akhir periode !!<BR>"
            End If
        End If
        If sValidate <> "" Then
            showMessage(sValidate, CompnyName & " - WARNING") : Exit Sub
        End If

        Dim sWhere As String = ""
        If deliverydate1.Text <> "" Then
            sWhere &= " and id.deliverydate>='" & CDate(toDate(deliverydate1.Text)) & "' "
        End If
        If deliverydate2.Text <> "" Then
            sWhere &= " and id.deliverydate<='" & CDate(toDate(deliverydate2.Text)) & "' "
        End If
        binddataIO(sWhere)
    End Sub

    Protected Sub gvItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItem.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(7).Text = "0" Then
                e.Row.Cells(7).Text = "NotFound"
                'If division.SelectedItem.Text = "FM" Then
                '    e.Row.Cells(7).BackColor = Drawing.Color.Orange
                'Else
                '    e.Row.Cells(7).BackColor = Drawing.Color.Red
                'End If
            Else
                e.Row.Cells(7).BackColor = Drawing.Color.Green
                e.Row.Cells(7).ForeColor = Drawing.Color.White
                e.Row.Cells(7).Text = "OK"
            End If
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
        End If
    End Sub

    Public Sub ClearIO()
        bomstatus.Text = ""
        If gvItem.SelectedDataKey("bomoid") = 0 Then
            bomstatus.Text = "BOM Not Found !!"
        End If
        refoid.Text = ""
        REFNAME.Text = ""
        iono.Text = ""
        iorefno.Text = ""
        custname.Text = ""
        itemlongdesc.Text = ""
        ioestdeliverydate.Text = ""
        ioqty.Text = ""
        ioqtyunitoid.Text = ""
        iounit1.Text = ""
        iodtloid.Text = ""
        refoid.Text = ""
        bomdesc.Text = ""
        resultqty1.Text = ""
        'resultqty2.Text = NewMaskEdit(qty2.Text)
        resultunitoid1.Items.Clear()
        TABLE1.Visible = False
    End Sub

    Protected Sub gvitem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        'If division.SelectedValue = "None" Then
        '    showMessage("Please Choose Department !!", CompnyName & " - WARNING") : spkstatus.Text = "In Process"
        '    Exit Sub
        'End If
        bomstatus.Text = ""
        If gvItem.SelectedDataKey("bomoid") = 0 Then
            bomstatus.Text = "BOM Not Found !!"
        End If
        REFNAME.Text = "ql_trnrequest"
        iono.Text = gvItem.SelectedDataKey("iono")
        iorefno.Text = gvItem.SelectedDataKey("iorefno")
        custname.Text = gvItem.SelectedDataKey("custname")
        itemlongdesc.Text = gvItem.SelectedDataKey("itemlongdesc")
        ioestdeliverydate.Text = gvItem.SelectedDataKey("ioestdeliverydate")
        ioqty.Text = gvItem.SelectedDataKey("ioqty")
        orderqty.Text = gvItem.SelectedDataKey("ioqty")
        ioqtyunitoid.Text = gvItem.SelectedDataKey("ioqtyunitoid")
        iounit1.Text = gvItem.SelectedDataKey("iounit1")
        iodtloid.Text = gvItem.SelectedDataKey("iodtloid")
        refoid.Text = gvItem.SelectedDataKey("itemoid")
        qtyspk.Text = gvItem.SelectedDataKey("QtySPK")
        resultqty1.Text = ToMaskEdit((ToDouble(ioqty.Text)), 1) ' Diambil sisa saja  - ToDouble(qtyspk.Text)
        maxqty.Text = ToMaskEdit((ToDouble(ioqty.Text)) * 1.1, 1) ' Diambil sisa + 10%  - ToDouble(qtyspk.Text)
        FillDDL(resultunitoid1, "select  g1.genoid, g1.gendesc from ql_trnrequest i inner join ql_mstgen g1 on g1.genoid=i.REQITEMTYPE where i.REQOID=" & iodtloid.Text & "")
        resultunitoid1.SelectedValue = ioqtyunitoid.Text
        spkjenisbarang.Text = ioqtyunitoid.Text
        i_u_spkresultitem.Text = "New Detail"
        Dim bomdef As String = GetStrData("select isnull(bomoid,0) from QL_mstbomgrouproute where outputtype = 'FG' and outputoid = '" & refoid.Text & "' and bomoid in (select bomoid from QL_mstbom where  itemoid='" & gvItem.SelectedDataKey("itemgroupcode") & "'  and bomstatus='ACTIVE')")

        ' ============= Sementara BOM dan UNIT di kosongkan
        FillDDL(bomoid, "select 0 as bomoid , 'Without BOM' as bomdesc ")
        If bomdef <> "?" And bomdef <> "0" Then
            bomoid.SelectedValue = bomdef
        Else
            bomoid.SelectedIndex = bomoid.Items.Count - 1
        End If
        bomoid_SelectedIndexChanged(sender, e)
        TABLE1.Visible = False : Label20.Visible = False
        CProc.DisposeGridView(sender) : MultiView1.ActiveViewIndex = 0
        division.Enabled = False : DisposeGrid(gvItem)
    End Sub

    Protected Sub btnAddToList1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList1.Click
        Dim dKoefisianItem As Decimal = 1 : Dim iPosisiUnitoid As Int16 = 0

        Dim sMsg As String = ""
        If ToDouble(iodtloid.Text) = 0 Then
            sMsg &= "- Silahkan Pilih No Barcode Dahulu !! <BR>"
        End If
        If ToDouble(resultqty1.Text) <= 0 Then
            sMsg &= "- Quantity Harus lebih besar daripada 0 !! <BR>"
        End If
        'If i_u_spkresultitem.Text = "New Detail" Then
        '    If ToDouble(resultqty1.Text) > ToDouble(ioqty.Text) And resultunitoid1.SelectedValue = ioqtyunitoid.Text Then
        '        sMsg &= "- Quantity must be less than " & ToDouble(ioqty.Text) & " !! <BR>"
        '        'ElseIf ToDouble(resultqty1.Text) > ToDouble(qty2.Text) And resultunitoid1.SelectedValue = unit2.Text Then
        '        '    If qty2.Text <> 0 Then
        '        '        sMsg &= "- Quantity must be less than " & ToDouble(qty2.Text) & " !! <BR>"
        '        '    End If
        '    End If
        '    'If ToDouble(resultqty2.Text) > ToDouble(ioqty.Text) And resultunitoid2.SelectedValue = ioqtyunitoid.Text Then
        '    '    sMsg &= "Quantity 2 must be less than " & ToDouble(ioqty.Text) & " !! <BR>"
        '    'ElseIf ToDouble(resultqty2.Text) > ToDouble(qty2.Text) And resultunitoid2.SelectedValue = unit2.Text Then
        '    '    sMsg &= "Quantity 2 must be less than " & ToDouble(qty2.Text) & " !! <BR>"
        '    'End If
        'End If

        If ToDouble(resultqty1.Text) > ToDouble(maxqty.Text) Then
            sMsg &= "- Maksimum Quantity untuk SPK adalah " & ToDouble(maxqty.Text) & " !! <BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, "Warning") : Exit Sub
        End If

        Try
            If Session("spkresultitem") Is Nothing Then
                sSql = " select 0 ioseq, '' iono, '' custname, 0 itemoid, '' refname, '' itemlongdesc, '01/01/1900' ioestdeliverydate, 0.0 resultqty1, 0 resultunitoid1, '' unit1, 0.0 resultqty2, 0 resultunitoid2, '' unit2, 0 iodtloid, 0 bomoid, '' bomdesc , 0 spkdtloid "
                Session("spkresultitem") = CreateDataTableFromSQL(sSql)
                CType(Session("spkresultitem"), DataTable).Rows.Clear()
                spkdtloid_2.Items.Clear()
                spkdtloid_3.Items.Clear()
                spkdtloid_4.Items.Clear()
            End If
            Dim objTable As DataTable
            objTable = Session("spkresultitem")

            'Cek apa sudah ada item yang sama dalam Tabel Detail
            Dim dv As DataView = objTable.DefaultView
            If i_u_spkresultitem.Text = "New Detail" Then
                dv.RowFilter = "iodtloid = " & iodtloid.Text
                If dv.Count > 0 Then
                    dv.RowFilter = ""
                    showMessage("Data sudah ada,Tolong check kembali !!", "Warning")
                    Exit Sub
                End If
            End If
            dv.RowFilter = "" : Dim total1 As Double = 0.0
            'insert/update to list data
            Dim objRow As DataRow
            If i_u_spkresultitem.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("ioseq") = objTable.Rows.Count + 1

                spkdtloid_2.Items.Add(objRow("ioseq"))
                spkdtloid_3.Items.Add(objRow("ioseq"))
                spkdtloid_4.Items.Add(objRow("ioseq"))
            Else 'update
                objRow = objTable.Rows(ioseq.Text - 1)
                objRow.BeginEdit()
            End If

            objRow("iono") = iono.Text
            objRow("custname") = custname.Text
            objRow("itemoid") = refoid.Text
            objRow("refname") = "ql_trnrequest"
            objRow("itemlongdesc") = itemlongdesc.Text
            objRow("ioestdeliverydate") = ioestdeliverydate.Text
            objRow("resultqty1") = ToDouble(resultqty1.Text) 'Math.Round(CDec(resultqty1.Text), 2)
            objRow("resultunitoid1") = resultunitoid1.SelectedValue
            objRow("unit1") = resultunitoid1.SelectedItem.Text
            'objRow("resultqty2") = Math.Round(CDec(resultqty2.Text), 2)
            'objRow("resultunitoid2") = resultunitoid2.SelectedValue
            'objRow("unit2") = resultunitoid2.SelectedItem.Text
            objRow("iodtloid") = iodtloid.Text
            objRow("bomoid") = bomoid.SelectedValue
            objRow("bomdesc") = bomoid.SelectedItem.Text
            objRow("spkdtloid") = objRow("ioseq")
            spkdtloid.Text = objRow("spkdtloid")
            If i_u_spkresultitem.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else 'update
                objRow.EndEdit()
            End If
            Session("spkresultitem") = objTable
            tblspkresultitem.DataSource = objTable
            tblspkresultitem.DataBind()

            If ToDouble(bomqty.Text) = 0 Then
                bomqty.Text = 1
            End If
            dKoefisianItem = (ToDouble(resultqty1.Text)) / (ToDouble(bomqty.Text))
            dKoefisianItem = Math.Round(dKoefisianItem, 2)

            ''----------------------------------------------------------
            'fill spkprocessitem
            'create table spkprocessitem
            If Session("spkprocessitem") Is Nothing Then
                sSql = " select 0 spkdtloid , 0 seqprocessitem, 0 spkprocessoid, '' spkprocessname, 0 processoid, '01/01/1900 00:00:00' planstart, '01/01/1900 00:00:00' planend, 0 machineoid,0 machinedtloid,'' machine, 0.0 outputqtytolerance, 0.0 stdpreptime, 0.0 stddowntime, 0.0 stdspeed,0.0 stdprodtime, '' note, 'In Process' spkprodstatus"
                Session("spkprocessitem") = CreateDataTableFromSQL(sSql)
                CType(Session("spkprocessitem"), DataTable).Rows.Clear()
                Session("spkprocessoid") = 1
                processname_1.Items.Clear()
                processname_2.Items.Clear()
            End If

            'create table spkprocessitemResult
            If Session("spkprocessitemResult") Is Nothing Then
                sSql = " select 0 spkdtloid, 0 spkprocessoid, 0 outputoid, '' outputref, '' outputtype, 0.0 outputqty1, 0 outputunitoid1, '' outputunit1, 0.0 outputqty2, 0 outputunitoid2, '' outputunit2, '' outputname, '' spkprocessname, '' note ,'' typejob "
                Session("spkprocessitemResult") = CreateDataTableFromSQL(sSql)
                CType(Session("spkprocessitemResult"), DataTable).Rows.Clear()
            End If

            'create table spkprocessMaterial
            If Session("spkprocessmaterial") Is Nothing Then
                sSql = " select 0 spkdtloid, 0 spkprocessmatoid, 0 spkprocessoid, 0 matoid, '' matref,0.0 qty1,0 unitoid1,'' unit1, 0.0 qty2, 0 unitoid2,'' unit2, 0.0 wastestd, 0.0 matroutetolerancepct, '' spkprocessname,'' matlongdesc,'' note,0 mtrlocoid"
                Session("spkprocessmaterial") = CreateDataTableFromSQL(sSql)
                CType(Session("spkprocessmaterial"), DataTable).Rows.Clear()
                Session("spkprocessmatoid") = 1
            End If

            If i_u_spkresultitem.Text = "New Detail" Then
                '-------------------------------------
                '---PROCESS CALCULATE TO INSERTING SPKPROCESSITEM
                sSql = "select current_timestamp planstart,current_timestamp planend, 0 machineoid,0 machinedtloid, '' machine, outputqtytolerance, stdpreptime, stddowntime, stdspeed, stdprodtime, '' note, 'InProcess' spkprodstatus , grouprouteoid, grouprouteseq seqprocessitem, g2.gendesc spkprocessname, outputoid, outputtype, case outputtype when 'WIP' then (select m.ITSERVDESC matlongdesc from QL_MSTITEMSERV m where m.ITSERVOID=outputoid) else (select m.ITSERVDESC itemlongdesc From QL_MSTITEMSERV m where m.ITSERVOID=outputoid) end outputname, outputqtystd outputqty1, outputunitoid outputunitoid1, g1.gendesc outputunit1 , g.routeoid processoid from ql_mstbom b inner join QL_mstbomgrouproute g on g.bomoid=b.bomoid inner join QL_mstgen g1 on g1.genoid=g.outputunitoid inner join ql_mstgen g2 on g2.genoid=g.routeoid where b.bomoid = " & bomoid.SelectedValue & " order by grouprouteseq, routegroupno"
                xCmd.CommandText = sSql
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                Dim oTblPI As DataTable = CType(Session("spkprocessitem"), DataTable)
                Dim oTblPIR As DataTable = CType(Session("SpkProcessItemResult"), DataTable)
                Dim oTblPM As DataTable = CType(Session("SpkProcessMaterial"), DataTable)

                xreader = xCmd.ExecuteReader
                While xreader.Read
                    Dim dvne As DataView = oTblPI.DefaultView
                    dvne.RowFilter = " spkdtloid=" & spkdtloid.Text & " and  spkprocessname='" & Tchar(xreader("spkprocessname")) & "'  "

                    'If dvne.Count = 0 Then 'cek jika tidak nemu dt yg sama, mk oleh mlebu
                    'insert to spkprocessitem
                    Dim drne As DataRow = oTblPI.NewRow
                    drne("spkdtloid") = spkdtloid.Text
                    drne("seqprocessitem") = xreader("seqprocessitem")
                    drne("spkprocessoid") = Session("spkprocessoid")
                    drne("spkprocessname") = xreader("spkprocessname")
                    drne("processoid") = xreader("processoid")
                    drne("planstart") = xreader("planstart")
                    drne("planend") = xreader("planend")
                    drne("machineoid") = xreader("machineoid")
                    drne("machinedtloid") = xreader("machinedtloid")
                    drne("machine") = xreader("machine")
                    drne("outputqtytolerance") = xreader("outputqtytolerance")
                    drne("stdpreptime") = xreader("stdpreptime")
                    drne("stddowntime") = xreader("stddowntime")
                    drne("stdspeed") = xreader("stdspeed")
                    drne("stdprodtime") = xreader("stdprodtime")
                    drne("note") = xreader("note")
                    drne("spkprodstatus") = xreader("spkprodstatus")
                    oTblPI.Rows.Add(drne)
                    'end spkprocessitem

                    ''============Test DUlu
                    ''insert to spkprocessmaterial
                    'sSql = "select b.matoid, 'QL_MSTMAT' matref , b.matunitqty qty1, b.matunitoid unitoid1, g1.gendesc unit1, b.matroutetolerancepct,0.0 qty2, 0 unitoid2, '' unit2 , m.matlongdesc from QL_mstbommatroute b inner join QL_mstmat m on b.matoid=m.matoid inner join QL_mstgen g1 on g1.genoid=b.matunitoid where grouprouteoid =" & xreader("grouprouteoid")
                    'xCmd2.CommandText = sSql
                    'If conn2.State = ConnectionState.Closed Then
                    '    conn2.Open()
                    'End If

                    'xreader2 = xCmd2.ExecuteReader
                    'While xreader2.Read
                    '    drne = oTblPM.NewRow
                    '    drne("spkdtloid") = spkdtloid.Text
                    '    drne("spkprocessmatoid") = Session("spkprocessmatoid")
                    '    drne("spkprocessoid") = Session("spkprocessoid")
                    '    drne("spkprocessname") = xreader("spkprocessname")
                    '    drne("matoid") = xreader2("matoid")
                    '    drne("matlongdesc") = xreader2("matlongdesc")
                    '    drne("matref") = xreader2("matref")
                    '    drne("qty1") = Math.Round(xreader2("qty1") * dKoefisianItem, 2)
                    '    drne("unitoid1") = xreader2("unitoid1")
                    '    drne("unit1") = xreader2("unit1")
                    '    drne("qty2") = Math.Round(xreader2("qty2"), 2)
                    '    drne("unitoid2") = xreader2("unitoid2")
                    '    drne("unit2") = xreader2("unit2")
                    '    drne("matroutetolerancepct") = xreader2("matroutetolerancepct")
                    '    drne("wastestd") = 0
                    '    oTblPM.Rows.Add(drne)
                    '    Session("spkprocessmatoid") = CInt(Session("spkprocessmatoid")) + 1
                    'End While

                    ''end spkprocessmaterial
                    'xreader2.Close()
                    'conn2.Close()
                    ''=========AHhhhhh
                    'End If
                    dvne.RowFilter = ""

                    '============Test DUlu
                    'insert to spkprocessmaterial
                    sSql = "select b.matoid, 'QL_MSTMAT' matref , b.matunitqty qty1, b.matunitoid unitoid1, g1.gendesc unit1, b.matroutetolerancepct, 0.0 qty2, 0 unitoid2, '' unit2 , m.matlongdesc, '' note from QL_mstbommatroute b inner join QL_mstmat m on b.matoid=m.matoid  inner join QL_mstgen g1 on g1.genoid=b.matunitoid where grouprouteoid =" & xreader("grouprouteoid")
                    xCmd2.CommandText = sSql
                    If conn2.State = ConnectionState.Closed Then
                        conn2.Open()
                    End If
                    Dim drneMat As DataRow
                    xreader2 = xCmd2.ExecuteReader
                    While xreader2.Read
                        drneMat = oTblPM.NewRow
                        drneMat("spkdtloid") = spkdtloid.Text
                        drneMat("spkprocessmatoid") = oTblPM.Rows.Count + 1
                        drneMat("spkprocessoid") = Session("spkprocessoid")
                        drneMat("spkprocessname") = xreader("spkprocessname")
                        drneMat("matoid") = xreader2("matoid")
                        drneMat("matlongdesc") = xreader2("matlongdesc")
                        drneMat("matref") = xreader2("matref")
                        drneMat("qty1") = Math.Round(xreader2("qty1") * dKoefisianItem, 2)
                        drneMat("unitoid1") = xreader2("unitoid1")
                        drneMat("unit1") = xreader2("unit1")
                        drneMat("qty2") = Math.Round(xreader2("qty2"), 2)
                        drneMat("unitoid2") = xreader2("unitoid2")
                        drneMat("unit2") = xreader2("unit2")
                        drneMat("matroutetolerancepct") = xreader2("matroutetolerancepct")
                        drneMat("wastestd") = 0
                        drneMat("note") = ""
                        oTblPM.Rows.Add(drneMat)
                    End While

                    'end spkprocessmaterial
                    xreader2.Close()
                    conn2.Close()
                    '=========AHhhhhh

                    'insert to trnspkprocessitemresult
                    Dim dr As DataRow = oTblPIR.NewRow
                    dr("spkdtloid") = spkdtloid.Text
                    dr("spkprocessoid") = Session("spkprocessoid")
                    dr("outputoid") = xreader("outputoid")
                    dr("outputname") = xreader("outputname")
                    dr("outputref") = IIf(xreader("outputtype").trim = "WIP", "QL_MSTMAT", "QL_MSTITEM")
                    dr("outputtype") = xreader("outputtype")
                    ' ========= qty output dtambahkan %toleransi
                    dr("outputqty1") = Math.Round(xreader("outputqty1") * dKoefisianItem, 2) + _
                        ((ToDouble(xreader("outputqtytolerance")) / 100) * Math.Round(xreader("outputqty1") * dKoefisianItem, 2))
                    dr("outputunitoid1") = xreader("outputunitoid1")
                    dr("outputunit1") = xreader("outputunit1")
                    dr("outputqty2") = 0
                    dr("outputunitoid2") = 0
                    dr("outputunit2") = ""
                    dr("spkprocessname") = xreader("spkprocessname")
                    dr("note") = ""
                    oTblPIR.Rows.Add(dr)
                    'end trnspkprocessitemresult

                    Session("spkprocessoid") = CInt(Session("spkprocessoid")) + 1
                End While
                xreader.Close()
                conn.Close()

                Session("spkprocessitem") = oTblPI
                Session("SpkProcessItemResult") = oTblPIR
                Session("SpkProcessMaterial") = oTblPM

                gvprocessitem.DataSource = oTblPI
                gvprocessitem.DataBind()
                gvprocessitemresult.DataSource = oTblPIR
                gvprocessitemresult.DataBind()
                GVprocessmaterial.DataSource = oTblPM
                GVprocessmaterial.DataBind()

                If spkdtloid_3.Items.Count > 0 Then
                    FillProcessname(spkdtloid_3.SelectedValue, processname_1)
                    FillProcessname(spkdtloid_4.SelectedValue, processname_2)
                End If
                GetDataProcess()
            End If

            ClearProcessMaterial()
            ProcessCalculateRangePlanSPK()
            ClearDetailIO()
            btnAddToList1.Visible = False

        Catch ex As Exception
            showMessage(ex.ToString, "Warning")
            If Not spkstatus.Text = "In Approval" Then
                spkstatus.Text = "In Process"
            End If
            Exit Sub
        End Try
    End Sub

    Protected Sub resultqty1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles resultqty1.TextChanged, resultqty2.TextChanged
        sender.text = NewMaskEdit(sender.text)
        If sender.Text.trim = "" Then
            sender.text = 0
        End If
    End Sub

    Protected Sub resultunitoid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles resultunitoid1.SelectedIndexChanged
        If resultunitoid1.SelectedValue = ioqtyunitoid.Text Then
            resultqty1.Text = NewMaskEdit(ioqty.Text)
        Else
            resultqty1.Text = NewMaskEdit(qty2.Text)
        End If
    End Sub

    Protected Sub resultunitoid2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles resultunitoid2.SelectedIndexChanged
        If resultunitoid2.SelectedValue = ioqtyunitoid.Text Then
            resultqty2.Text = NewMaskEdit(ioqty.Text)
        Else
            resultqty2.Text = NewMaskEdit(qty2.Text)
        End If
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, False)
    End Sub

    Protected Sub gvprocessitem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvprocessitem.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 3)
            'e.Row.Cells(8).Text = ToMaskEdit(e.Row.Cells(8).Text, 3)
            'e.Row.Cells(11).Text = ToMaskEdit(e.Row.Cells(11).Text, 3)
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy HH:mm")
            e.Row.Cells(5).Text = Format(CDate(e.Row.Cells(5).Text), "dd/MM/yyyy HH:mm")
        End If
    End Sub

    Protected Sub tblspkresultitem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tblspkresultitem.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            '  e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
        End If
    End Sub

    Protected Sub TblDtlspkresultitem_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tblspkresultitem.RowDeleting
        Dim objTable As DataTable = Session("spkresultitem")
        Dim Del_spkdtloid As Int32 = objTable.Rows(e.RowIndex).Item("spkdtloid")
        objTable.Rows.RemoveAt((e.RowIndex))
        spkdtloid_2.Items.Clear()
        spkdtloid_3.Items.Clear()
        spkdtloid_4.Items.Clear()

        'resequence po detail ID + sequenceDtl + hitung total Amount
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("ioseq") = C1 + 1
            dr("spkdtloid") = C1 + 1
            dr.EndEdit()

            spkdtloid_2.Items.Add(dr("ioseq"))
            spkdtloid_3.Items.Add(dr("ioseq"))
            spkdtloid_4.Items.Add(dr("ioseq"))
        Next
        Session("spkresultitem") = objTable
        tblspkresultitem.Visible = True
        ioseq.Text = objTable.Rows.Count + 1
        ClearDetailIO()
        tblspkresultitem.DataSource = objTable
        tblspkresultitem.DataBind()

        'delete processitem with spkdtloid del
        Dim objTable2 As DataTable = Session("spkprocessitem")
        Dim dvt2 As DataView = objTable2.DefaultView
        dvt2.RowFilter = "spkdtloid=" & Del_spkdtloid
        For c2 As Int16 = 0 To dvt2.Count - 1
            dvt2.Delete(0)
        Next
        dvt2.RowFilter = ""
        objTable2 = dvt2.ToTable
        'resequence  
        For C2 As Int16 = 0 To objTable2.Rows.Count - 1
            Dim dr As DataRow = objTable2.Rows(C2)
            dr.BeginEdit()
            If dr("spkdtloid") > Del_spkdtloid Then
                dr("spkdtloid") = dr("spkdtloid") - 1
            End If
            dr.EndEdit()
        Next
        Session("spkprocessitem") = objTable2
        gvprocessitem.DataSource = objTable2
        gvprocessitem.DataBind()

        'delete processitemresult with spkdtloid del
        Dim objTable3 As DataTable = Session("spkprocessitemresult")
        Dim dvt3 As DataView = objTable3.DefaultView
        dvt3.RowFilter = "spkdtloid=" & Del_spkdtloid
        For c2 As Int16 = 0 To dvt3.Count - 1
            dvt3.Delete(0)
        Next
        dvt3.RowFilter = "" : objTable3 = dvt3.ToTable
        'resequence  
        For C3 As Int16 = 0 To objTable3.Rows.Count - 1
            Dim dr As DataRow = objTable3.Rows(C3)
            dr.BeginEdit()
            If dr("spkdtloid") > Del_spkdtloid Then
                dr("spkdtloid") = dr("spkdtloid") - 1
            End If
            dr.EndEdit()
        Next
        Session("spkprocessitemresult") = objTable3
        gvprocessitemresult.DataSource = objTable3
        gvprocessitemresult.DataBind()

        'delete spkprocessmaterial with spkdtloid del
        Dim objTable4 As DataTable = Session("spkprocessmaterial")
        Dim dvt4 As DataView = objTable4.DefaultView
        dvt4.RowFilter = "spkdtloid=" & Del_spkdtloid
        For c4 As Int16 = 0 To dvt4.Count - 1
            dvt4.Delete(0)
        Next
        dvt4.RowFilter = "" : objTable4 = dvt4.ToTable
        'resequence  
        For C4 As Int16 = 0 To objTable4.Rows.Count - 1
            Dim dr As DataRow = objTable4.Rows(C4)
            dr.BeginEdit()
            If dr("spkdtloid") > Del_spkdtloid Then
                dr("spkdtloid") = dr("spkdtloid") - 1
            End If
            dr("spkprocessmatoid") = C4 + 1
            dr.EndEdit()
        Next
        Session("spkprocessmaterial") = objTable4
        GVprocessmaterial.DataSource = objTable4
        GVprocessmaterial.DataBind()
        If ioseq.Text > 1 Then
            FillProcessname(spkdtloid_3.SelectedValue, processname_1)
            FillProcessname(spkdtloid_4.SelectedValue, processname_2)
        End If
        btnAddToList1.Visible = True
    End Sub

    Protected Sub tblspkresultitem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tblspkresultitem.SelectedIndexChanged
        bomstatus.Text = ""
        If tblspkresultitem.SelectedDataKey("bomoid") = 0 Then
            bomstatus.Text = "BOM Not Found !!"
        End If

        ioseq.Text = tblspkresultitem.SelectedDataKey("ioseq")
        REFNAME.Text = "ql_trnrequest"
        iono.Text = tblspkresultitem.SelectedDataKey("iono")
        iorefno.Text = tblspkresultitem.SelectedDataKey("iorefno")
        custname.Text = tblspkresultitem.SelectedDataKey("custname")
        itemlongdesc.Text = tblspkresultitem.SelectedDataKey("itemlongdesc")
        ioestdeliverydate.Text = tblspkresultitem.SelectedDataKey("ioestdeliverydate")
        ioqty.Text = tblspkresultitem.SelectedDataKey("resultqty1")
        ioqtyunitoid.Text = tblspkresultitem.SelectedDataKey("resultunitoid1")
        iounit1.Text = tblspkresultitem.SelectedDataKey("unit1")
        qty2.Text = tblspkresultitem.SelectedDataKey("resultqty2")
        unit2.Text = tblspkresultitem.SelectedDataKey("resultunitoid2")
        iounit2.Text = tblspkresultitem.SelectedDataKey("unit2")
        berat.Text = tblspkresultitem.SelectedDataKey("berat")
        panjang.Text = tblspkresultitem.SelectedDataKey("panjang")
        iodtloid.Text = tblspkresultitem.SelectedDataKey("iodtloid")

        FillDDL(resultunitoid1, "select  g1.genoid, g1.gendesc from ql_trniodtl i inner join ql_mstgen g1 on g1.genoid=i.ioqtyunitoid where i.iodtloid=" & iodtloid.Text & " union select  g2.genoid, g2.gendesc from ql_trniodtl i inner join   ql_mstgen g2 on g2.genoid=i.unit2 where i.iodtloid=" & iodtloid.Text)
        FillDDL(resultunitoid2, "select  g1.genoid, g1.gendesc from ql_trniodtl i inner join ql_mstgen g1 on g1.genoid=i.ioqtyunitoid where i.iodtloid=" & iodtloid.Text & " union select  g2.genoid, g2.gendesc from ql_trniodtl i inner join   ql_mstgen g2 on g2.genoid=i.unit2 where i.iodtloid=" & iodtloid.Text)
        resultunitoid1.SelectedValue = ioqtyunitoid.Text
        Try
            resultunitoid2.SelectedValue = unit2.Text
        Catch ex As Exception
            resultunitoid1.SelectedIndex = 0
        End Try

        bomoid.Text = tblspkresultitem.SelectedDataKey("bomoid")

        refoid.Text = tblspkresultitem.SelectedDataKey("itemoid")
        bomdesc.Text = tblspkresultitem.SelectedDataKey("bomdesc")
        resultqty1.Text = NewMaskEdit(ToDouble(ioqty.Text))
        resultqty2.Text = NewMaskEdit(ToDouble(qty2.Text))

        i_u_spkresultitem.Text = "Update"
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        'cek spkresultitem
        If Session("spkresultitem") Is Nothing Then
            showMessage("Tolong Isi Barang No !!", "Warning")
            If Not spkstatus.Text = "In Approval" Then
                spkstatus.Text = "In Process"
            End If
            MultiView1.ActiveViewIndex = 0
            Exit Sub
        Else
            Dim objTableCek As DataTable = Session("spkresultitem")
            If objTableCek.Rows.Count = 0 Then
                showMessage("Detail Barang No tidak ada !!", "Warning")
                If Not spkstatus.Text = "In Approval" Then
                    spkstatus.Text = "In Process"
                End If
                MultiView1.ActiveViewIndex = 0
                Exit Sub
            End If
        End If

        If spkstatus.Text.Trim = "Approved" Then
            If tbpic.Text.Trim = "" Or lblpic.Text.Trim = "" Then
                showMessage("PIC harus diisi !!", "Warning")
                If Not spkstatus.Text = "In Approval" Then
                    spkstatus.Text = "In Process"
                End If
                MultiView1.ActiveViewIndex = 0
                Exit Sub
            End If
        End If

        'If division.SelectedValue = "None" Then
        '    showMessage("Please select Department !!", "Warning") : spkstatus.Text = "In Process"
        '    Exit Sub
        'End If

        'cek spkprocessitem
        If Session("spkprocessitem") Is Nothing Then
            showMessage("Tolong Isi Process Service !!", "Warning")
            If Not spkstatus.Text = "In Approval" Then
                spkstatus.Text = "In Process"
            End If
            MultiView1.ActiveViewIndex = 1
            Exit Sub
        Else
            Dim objTableCek As DataTable = Session("spkprocessitem")
            If objTableCek.Rows.Count = 0 Then
                showMessage("Detail Process Service tidak ada !!", "Warning")
                If Not spkstatus.Text = "In Approval" Then
                    spkstatus.Text = "In Process"
                End If
                MultiView1.ActiveViewIndex = 1
                Exit Sub
            End If
        End If

        'cek spkprocessitemResult
        If Session("spkprocessitemResult") Is Nothing Then
            showMessage("Tolong Isi Process Service Job !!", "Warning")
            If Not spkstatus.Text = "In Approval" Then
                spkstatus.Text = "In Process"
            End If
            MultiView1.ActiveViewIndex = 1
            Exit Sub
        Else
            Dim objTableCek As DataTable = Session("spkprocessitemResult")
            If objTableCek.Rows.Count = 0 Then
                showMessage("Detail Process Service Job tidak ada !!", "Warning")
                If Not spkstatus.Text = "In Approval" Then
                    spkstatus.Text = "In Process"
                End If
                MultiView1.ActiveViewIndex = 1
                Exit Sub
            End If
        End If

        ''cek spkprocessmaterial
        'If Session("spkprocessmaterial") Is Nothing Then
        '    showMessage("Please fill Process Material !!", "Warning") : spkstatus.Text = "In Process"
        '    MultiView1.ActiveViewIndex = 2
        '    Exit Sub
        'Else
        '    Dim objTableCek As DataTable = Session("spkprocessmaterial")
        '    If objTableCek.Rows.Count = 0 Then
        '        showMessage("Detail Process Material not found !!", "Warning") : spkstatus.Text = "In Process"
        '        MultiView1.ActiveViewIndex = 2
        '        Exit Sub
        '    End If
        'End If

        Dim oTblIO As DataTable = CType(Session("spkresultitem"), DataTable)
        Dim oTblPI As DataTable = CType(Session("spkprocessitem"), DataTable)
        Dim oTblPIR As DataTable = CType(Session("SpkProcessItemResult"), DataTable)
        Dim oTblPM As DataTable = CType(Session("SpkProcessMaterial"), DataTable)

        'cek all realtionship each table
        For c1 As Int16 = 0 To oTblIO.Rows.Count - 1
            Dim dv As DataView
            'cek relationship in spkresultitem
            dv = oTblPI.DefaultView
            dv.RowFilter = "spkdtloid=" & oTblIO.Rows(c1).Item("spkdtloid")
            If dv.Count = 0 Then
                showMessage("Detail Process Service tidak ditemukan dengan link Barang No " & oTblIO.Rows(c1).Item("spkdtloid"), "Warning")
                MultiView1.ActiveViewIndex = 1 : dv.RowFilter = ""
                If Not spkstatus.Text = "In Approval" Then
                    spkstatus.Text = "In Process"
                End If
                Exit Sub
            End If

            'cek Machine pada Process yg belum di add
            'dv.RowFilter = "machineoid=0"
            'If dv.Count > 0 Then
            '    showMessage("Some Machine must be Choose in Detail Process Item !!", "Warning")
            '    MultiView1.ActiveViewIndex = 1
            '    dv.RowFilter = "" : spkstatus.Text = "In Process"
            '    Exit Sub
            'End If

            'cek relationship in SpkProcessItemResult
            dv = oTblPIR.DefaultView
            dv.RowFilter = "spkdtloid=" & oTblIO.Rows(c1).Item("spkdtloid")
            If dv.Count = 0 Then
                showMessage("Detail Process Service Job tidak ditemukan dengan link Barang No " & oTblIO.Rows(c1).Item("spkdtloid"), "Warning")
                MultiView1.ActiveViewIndex = 1
                dv.RowFilter = ""
                If Not spkstatus.Text = "In Approval" Then
                    spkstatus.Text = "In Process"
                End If
                Exit Sub
            End If

            ''cek relationship in SpkProcessMaterial
            'dv = oTblPM.DefaultView
            'dv.RowFilter = "spkdtloid=" & oTblIO.Rows(c1).Item("spkdtloid")
            'If dv.Count = 0 Then
            '    showMessage("Detail Process Material not found with link Result No " & oTblIO.Rows(c1).Item("spkdtloid"), "Warning")
            '    MultiView1.ActiveViewIndex = 2
            '    dv.RowFilter = "" : spkstatus.Text = "In Process"
            '    Exit Sub
            'End If
            dv.RowFilter = ""
        Next

        ProcessCalculateRangePlanSPK()

        Dim iSPKDtloid As Int64 = GenerateID("QL_trnspkresultitem", cmpcode)
        Dim iSPKProcessoid As Int64 = GenerateID("QL_trnspkprocessitem", cmpcode)
        Dim iSPKProcessMatoid As Int64 = GenerateID("QL_trnspkprocessmaterial", cmpcode)
        If Session("oid") = Nothing Or Session("oid") = "" Then
            spkmainoid.Text = GenerateID("QL_trnspkmainprod", cmpcode)
        End If

        If spkstatus.Text = "Approved" Then
            generatenoSPK()
        Else
            spkno.Text = spkmainoid.Text
        End If

        Dim status As String = spkstatus.Text
        If spkstatus.Text = "In Approval" Then
            status = "Approval"
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                ' insert table master
                sSql = "INSERT INTO [QL_trnspkmainprod] (cmpcode,branch_code,spkmainoid,division,spkstatus,spkno,spkopendate,spkplanstart,spkplanend,spkaddnote,createuser,upduser,updtime,spkjenisbarang,spkpic) " & _
                "Values ('" & cmpcode & "','" & division.SelectedValue & "'," & spkmainoid.Text & ",'" & division.SelectedValue & "','" & status & "', '" & Tchar(spkno.Text) & "', '" & toDate(spkopendate.Text) & "', '" & toDate(spkplanstart.Text) & " " & spkplanstarttime.Text & ":00','" & toDate(spkplanend.Text) & " " & spkplanendtime.Text & ":00','" & Tchar(spkaddnote.Text) & "','" & Session("UserID") & "','" & Session("UserID") & "', CURRENT_TIMESTAMP, " & spkjenisbarang.Text & ", '" & lblpic.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & spkmainoid.Text & " where upper(tablename)='QL_trnspkmainprod' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else 'for updatespkmain prod
                sSql = "update [QL_trnspkmainprod] set division='" & division.SelectedValue & "',spkstatus='" & status & "', spkno='" & Tchar(spkno.Text) & "', spkopendate='" & toDate(spkopendate.Text) & "',spkplanstart= '" & toDate(spkplanstart.Text) & " " & spkplanstarttime.Text & ":00', spkplanend='" & toDate(spkplanend.Text) & " " & spkplanendtime.Text & ":00',spkaddnote='" & Tchar(spkaddnote.Text) & "', UPDUSER='" & Session("UserID") & "', UPDTIME=CURRENT_TIMESTAMP , spkjenisbarang = " & spkjenisbarang.Text & ", spkpic = '" & lblpic.Text & "' Where cmpcode='" & cmpcode & "'  and spkmainoid=" & spkmainoid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnspkprocessmaterial
                sSql = "Delete from QL_trnspkprocessmaterial where cmpcode='" & cmpcode & "' and spkprocessoid in ( select s.spkprocessoid from ql_trnspkprocessitem s where s.spkmainoid=" & spkmainoid.Text & " and s.cmpcode='" & cmpcode & "' )    "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnspkprocessitemresult
                sSql = "Delete from QL_trnspkprocessitemresult where cmpcode='" & cmpcode & "' and spkprocessoid in ( select s.spkprocessoid from ql_trnspkprocessitem s where s.spkmainoid=" & spkmainoid.Text & " And s.cmpcode='" & cmpcode & "' )    "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnspkprocessitem
                sSql = "Delete from  ql_trnspkprocessitem Where cmpcode='" & cmpcode & "' and  spkmainoid=" & spkmainoid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnspkresultitem
                sSql = "Delete from QL_trnspkresultitem Where cmpcode='" & cmpcode & "' and  spkmainoid = " & spkmainoid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'insert into spkresultitem
            For c1 As Int16 = 0 To oTblIO.Rows.Count - 1
                sSql = "insert into QL_trnspkResultItem (cmpcode,branch_code ,spkdtloid ,spkmainoid ,iono ,iodtloid ,refoid ,refname ,resultqty1 ,resultunitoid1 ,resultqty2 ,resultunitoid2 ,bomoid)" & _
                " Values ('" & cmpcode & "','" & division.SelectedValue & "'," & iSPKDtloid & "," & spkmainoid.Text & ",'" & oTblIO.Rows(c1).Item("iono") & "'," & oTblIO.Rows(c1).Item("iodtloid") & "," & oTblIO.Rows(c1).Item("ITEMOID") & ",'" & oTblIO.Rows(c1).Item("refname") & "'," & oTblIO.Rows(c1).Item("resultqty1") & "," & oTblIO.Rows(c1).Item("resultunitoid1") & ",0,0, " & oTblIO.Rows(c1).Item("bomoid") & " )"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If spkstatus.Text = "Approved" Then
                    sSql = " update ql_trnrequest set reqstatus='SPK' Where REQOID = " & oTblIO.Rows(c1).Item("iodtloid") & " and cmpcode = '" & cmpcode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                'insert into spkprocessitem
                Dim dvoTblPI As DataView = oTblPI.DefaultView
                dvoTblPI.RowFilter = "spkdtloid=" & oTblIO.Rows(c1).Item("spkdtloid")
                For c1_1 As Int16 = 0 To dvoTblPI.Count - 1
                    sSql = "insert into QL_trnspkProcessItem (cmpcode,branch_code,spkprocessoid,spkmainoid,spkdtloid,seqprocessitem,processoid ,planstart,planend,machineoid,outputqtytolerance,stdpreptime,stddowntime ,stdspeed,stdprodtime,note,spkprodstatus, machinedtloid) " & _
                    "Values ('" & cmpcode & "','" & division.SelectedValue & "'," & iSPKProcessoid & "," & spkmainoid.Text & "," & iSPKDtloid & "," & dvoTblPI.Item(c1_1).Item("seqprocessitem") & ",'" & dvoTblPI.Item(c1_1).Item("processoid") & "','" & dvoTblPI.Item(c1_1).Item("planstart") & "','" & dvoTblPI.Item(c1_1).Item("planend") & "'," & dvoTblPI.Item(c1_1).Item("machineoid") & "," & dvoTblPI.Item(c1_1).Item("outputqtytolerance") & "," & dvoTblPI.Item(c1_1).Item("stdpreptime") & "," & dvoTblPI.Item(c1_1).Item("stddowntime") & ", " & dvoTblPI.Item(c1_1).Item("stdspeed") & ", " & dvoTblPI.Item(c1_1).Item("stdprodtime") & ", '" & Tchar(dvoTblPI.Item(c1_1).Item("note")) & "', '" & dvoTblPI.Item(c1_1).Item("spkprodstatus") & "'," & dvoTblPI.Item(c1_1).Item("machinedtloid") & " )"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'insert into spkprocessitemresult
                    Dim dvoTblPIr As DataView = oTblPIR.DefaultView
                    dvoTblPIr.RowFilter = "spkdtloid=" & oTblIO.Rows(c1).Item("spkdtloid") & " and spkprocessoid=" & dvoTblPI.Item(c1_1).Item("spkprocessoid")
                    For c1_1_1 As Int16 = 0 To dvoTblPIr.Count - 1
                        sSql = "insert into QL_trnspkProcessItemResult (cmpcode,branch_code ,spkprocessoid ,outputoid ,outputref ,outputtype ,outputqty1 ,outputunitoid1 ,outputqty2 ,outputunitoid2, note)" & _
                        " Values  ('" & cmpcode & "','" & division.SelectedValue & "'," & iSPKProcessoid & ", " & dvoTblPIr.Item(c1_1_1).Item("outputoid") & ",'" & dvoTblPIr.Item(c1_1_1).Item("outputref") & "','" & dvoTblPIr.Item(c1_1_1).Item("outputtype") & "'," & dvoTblPIr.Item(c1_1_1).Item("outputqty1") & "," & dvoTblPIr.Item(c1_1_1).Item("outputunitoid1") & ",0,0, '" & Tchar(dvoTblPIr.Item(c1_1_1).Item("note")) & "' )"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    dvoTblPIr.RowFilter = ""

                    'insert into spkprocessmaterial
                    Dim dvoTblPm As DataView = oTblPM.DefaultView
                    dvoTblPm.RowFilter = "spkdtloid=" & oTblIO.Rows(c1).Item("spkdtloid") & " and spkprocessoid=" & dvoTblPI.Item(c1_1).Item("spkprocessoid")
                    For c1_1_2 As Int16 = 0 To dvoTblPm.Count - 1
                        sSql = "insert into QL_trnspkProcessMaterial (cmpcode,branch_code ,spkprocessmatoid ,spkprocessoid ,matoid ,matref ,qty1 ,unitoid1 ,matroutetolerancepct ,unitoid2 ,qty2 ,wastestd, note, mtrlocoid)" & _
                        " Values ('" & cmpcode & "','" & division.SelectedValue & "'," & iSPKProcessMatoid & "," & iSPKProcessoid & ", " & dvoTblPm.Item(c1_1_2).Item("matoid") & ",'" & dvoTblPm.Item(c1_1_2).Item("matref") & "'," & dvoTblPm.Item(c1_1_2).Item("qty1") & "," & dvoTblPm.Item(c1_1_2).Item("unitoid1") & "," & dvoTblPm.Item(c1_1_2).Item("matroutetolerancepct") & ",0,0," & dvoTblPm.Item(c1_1_2).Item("wastestd") & ", '" & Tchar(dvoTblPm.Item(c1_1_2).Item("note")) & "'," & ToDouble(dvoTblPm.Item(c1_1_2).Item("mtrlocoid")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iSPKProcessMatoid += 1
                    Next
                    dvoTblPm.RowFilter = "" : iSPKProcessoid += 1
                Next
                dvoTblPI.RowFilter = "" : iSPKDtloid += 1
            Next
            sSql = "update QL_mstoid set lastoid=" & iSPKDtloid - 1 & " Where tablename = 'QL_trnspkresultitem' and cmpcode = '" & cmpcode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update QL_mstoid set lastoid=" & iSPKProcessoid - 1 & " Where tablename = 'QL_trnspkprocessitem' and cmpcode = '" & cmpcode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update QL_mstoid set lastoid=" & iSPKProcessMatoid - 1 & " Where tablename = 'QL_trnspkprocessmaterial' and cmpcode = '" & cmpcode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            If Not spkstatus.Text = "In Approval" Then
                spkstatus.Text = "In Process"
            End If
            showMessage(ex.Message & ex.ToString & sSql, "ERROR ")
            conn.Close()
            Exit Sub
        End Try
        Response.Redirect("spkmain.aspx?awal=true")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sValidate As String = ""
        If Not IsDate(toDate(FilterPeriod1.Text)) Then sValidate &= "- Format Filter Awal periode salah !!<BR>"
        If Not IsDate(toDate(FilterPeriod2.Text)) Then sValidate &= "- Format Filter Akhir periode salah !!<BR>"
        If IsDate(toDate(FilterPeriod1.Text)) And IsDate(toDate(FilterPeriod2.Text)) Then
            If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                sValidate &= "- Filter Awal tidak boleh lebih dari Filter Akhir periode !!<BR>"
            End If
        End If
        If sValidate <> "" Then
            showMessage(sValidate, CompnyName & " - WARNING") : Exit Sub
        End If
        BindData("")
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterPeriod1.Text = Format(Now.AddDays(-7), "dd/MM/yyyy")
        FilterPeriod2.Text = Format(Now, "dd/MM/yyyy")
        FilterDDL.SelectedIndex = 0 : FilterText.Text = ""
        FilterStatus.SelectedIndex = 0
        BindData("")
    End Sub

    Protected Sub GVtrnspkmain_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVtrnspkmain.PageIndexChanging
        GVtrnspkmain.PageIndex = e.NewPageIndex
        BindData("")
    End Sub

    Protected Sub GVtrnspkmain_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVtrnspkmain.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy HH:mm")
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy HH:mm")
            If e.Row.Cells(5).Text = "Approval" Then
                e.Row.Cells(5).Text = "In Approval"
            End If
        End If
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnPosting.Click
        spkstatus.Text = "Approved"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnCancelDtl1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelDtl1.Click
        ClearDetailIO()
    End Sub

    Protected Sub gvprocessitem_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvprocessitem.RowDeleting
        Dim indeke As Int16 = e.RowIndex
        Dim objTable As DataTable = Session("spkprocessitem")
        Dim dvtemp As DataView = objTable.DefaultView
        dvtemp.Sort = "spkdtloid, seqprocessitem, spkprocessoid"
        Dim Del_spkprocessoid As Int32 = dvtemp.Item(indeke).Item("spkprocessoid")
        Dim Del_spkdtloid As Int32 = dvtemp.Item(indeke).Item("spkdtloid")
        dvtemp.Delete(indeke) ' objTable.Rows.RemoveAt((e.RowIndex))
        objTable = dvtemp.ToTable

        'resequence processitem
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            If dr("spkprocessoid") > Del_spkprocessoid Then
                dr("spkprocessoid") = dr("spkprocessoid") - 1
                If Del_spkdtloid = objTable.Rows(C1).Item("spkdtloid") Then
                    dr("seqprocessitem") = dr("seqprocessitem") - 1
                End If
            End If
            dr.EndEdit()
        Next
        ioseq.Text = objTable.Rows.Count + 1
        ClearSPKProcessItem()
        Session("spkprocessitem") = objTable
        gvprocessitem.DataSource = objTable
        gvprocessitem.DataBind()

        'delete processitemresult with spkdtloid del
        Dim objTable3 As DataTable = Session("spkprocessitemresult")
        Dim dvt3 As DataView = objTable3.DefaultView
        dvt3.RowFilter = "spkdtloid=" & Del_spkdtloid & " and spkprocessoid=" & Del_spkprocessoid
        For c2 As Int16 = 0 To dvt3.Count - 1
            dvt3.Delete(0)
        Next
        dvt3.RowFilter = "" : objTable3 = dvt3.ToTable
        'resequence  
        For C3 As Int16 = 0 To objTable3.Rows.Count - 1
            Dim dr As DataRow = objTable3.Rows(C3)
            dr.BeginEdit()
            If dr("spkprocessoid") > Del_spkprocessoid Then
                dr("spkprocessoid") = dr("spkprocessoid") - 1
            End If
            dr.EndEdit()
        Next
        Session("spkprocessitemresult") = objTable3
        gvprocessitemresult.DataSource = objTable3
        gvprocessitemresult.DataBind()

        'delete spkprocessmaterial with spkdtloid del
        Dim objTable4 As DataTable = Session("spkprocessmaterial")
        Dim dvt4 As DataView = objTable4.DefaultView
        dvt4.RowFilter = "spkdtloid=" & Del_spkdtloid & " and  spkprocessoid=" & Del_spkprocessoid
        For c4 As Int16 = 0 To dvt4.Count - 1
            dvt4.Delete(0)
        Next
        dvt4.RowFilter = ""
        objTable4 = dvt4.ToTable
        'resequence  
        For C4 As Int16 = 0 To objTable4.Rows.Count - 1
            Dim dr As DataRow = objTable4.Rows(C4)
            dr.BeginEdit()
            If dr("spkprocessoid") > Del_spkprocessoid Then
                dr("spkprocessoid") = dr("spkprocessoid") - 1
            End If
            dr("spkprocessmatoid") = C4 + 1
            dr.EndEdit()
        Next
        Session("spkprocessmaterial") = objTable4
        GVprocessmaterial.DataSource = objTable4
        GVprocessmaterial.DataBind()
        FillProcessname(spkdtloid_3.SelectedValue, processname_1)
        FillProcessname(spkdtloid_4.SelectedValue, processname_2)
    End Sub

    Protected Sub gvprocessitem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvprocessitem.SelectedIndexChanged
        Try
            spkdtloid_2.SelectedValue = gvprocessitem.SelectedDataKey("spkdtloid")
            spkdtloid_3.SelectedValue = gvprocessitem.SelectedDataKey("spkdtloid")
            spkdtloid_4.SelectedValue = gvprocessitem.SelectedDataKey("spkdtloid")
            FillProcessname(spkdtloid_3.SelectedValue, processname_1)
            FillProcessname(spkdtloid_4.SelectedValue, processname_2)
            initDDLProcess()

            seqprocess1.SelectedValue = gvprocessitem.SelectedDataKey("seqprocessitem")
            spkprocessoid.Text = gvprocessitem.SelectedDataKey("spkprocessoid")
            Try
                processoid.SelectedValue = gvprocessitem.SelectedDataKey("processoid")
            Catch ex As Exception

            End Try
            initDDLMachine()

            planstartdate.Text = Format(CDate(gvprocessitem.SelectedDataKey("planstart")), "dd/MM/yyyy")
            planenddate.Text = Format(CDate(gvprocessitem.SelectedDataKey("planend")), "dd/MM/yyyy")
            planstarttime.Text = Format(CDate(gvprocessitem.SelectedDataKey("planstart")), "HH:mm")
            planendtime.Text = Format(CDate(gvprocessitem.SelectedDataKey("planend")), "HH:mm")

            Try
                machineoid.Text = gvprocessitem.SelectedDataKey("machineoid")
                machinedtloid.SelectedValue = gvprocessitem.SelectedDataKey("machinedtloid")
            Catch ex As Exception
                machineoid.Text = 0
            End Try

            If machinedtloid.SelectedValue <> "None" Then
                FillDataFromMachine(machinedtloid.SelectedValue)
            End If

            ddltolerance.SelectedValue = NewMaskEdit(gvprocessitem.SelectedDataKey("outputqtytolerance"))
            stdpreptime.Text = ToDouble(gvprocessitem.SelectedDataKey("stdpreptime"))
            stddowntime.Text = ToDouble(gvprocessitem.SelectedDataKey("stddowntime"))
            stdspeed.Text = gvprocessitem.SelectedDataKey("stdspeed")
            stdprodtime.Text = gvprocessitem.SelectedDataKey("stdprodtime")
            note.Text = gvprocessitem.SelectedDataKey("note")
            spkprodstatus.Text = gvprocessitem.SelectedDataKey("spkprodstatus")

            i_u_spkprocessitem.Text = "Update"
            seqprocess1.Enabled = False
            seqprocess1.CssClass = "inpTextDisabled"
            spkdtloid_2.Enabled = False
            spkdtloid_2.CssClass = "inpTextDisabled"
            gvprocessitem.Columns(12).Visible = False
            gvprocessitem.Columns(0).Visible = False
            processoid.Enabled = False
            processoid.CssClass = "inpTextDisabled"

            MultiView1.ActiveViewIndex = 1
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR")
        End Try
    End Sub

    Protected Sub btnAddToList2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList2.Click
        Dim sMsg As String = ""
        If spkdtloid_2.Items.Count = 0 Then
            sMsg &= "- Tolong Isi Barang No !! <br>"
        End If
        If machinedtloid.SelectedValue = "None" Then
            sMsg &= "- Tolong Pilih Machine !! <br>"
        End If
        If processoid.SelectedValue = "None" Then
            sMsg &= "- Tolong Pilih Nama Process !! <br>"
        End If

        If Not IsDate(toDate(planstartdate.Text) & " " & planstarttime.Text) Then
            sMsg &= "- Tanggal Waktu Rencana Mulai Salah !! <br>"
        Else
            If CheckYearIsValid(toDate(planstartdate.Text)) Then
                sMsg &= "Tanggal Rencana Awal tidak boleh Kurang dari 2000 dan tidak boleh Lebih dari 2072 <br>"
            End If
            'If CDate(toDate(planstartdate.Text) & " " & planstarttime.Text) < GetMinDateTime() Then
            '    sMsg &= "- Minimum Tanggal adalah '" & Format(GetMinDateTime(), "MM/dd/yyyy HH:mm") & "' !! <br>"
            'End If
        End If
        If Not IsDate(toDate(planenddate.Text) & " " & planendtime.Text) Then
            sMsg &= "- Tanggal Waktu Rencana Selesai Salah !! <br>"
        Else
            If CheckYearIsValid(toDate(planenddate.Text)) Then
                sMsg &= "Tanggal Rencana Akhir tidak boleh Kurang dari 2000 dan tidak boleh Lebih dari 2072 <br>"
            End If
            'If CDate(toDate(planenddate.Text) & " " & planendtime.Text) < GetMinDateTime() Then
            '    sMsg &= "- Minimum Tanggal adalah '" & Format(GetMinDateTime(), "MM/dd/yyyy HH:mm") & "' !! <br>"
            'End If
        End If

        If IsDate(toDate(planstartdate.Text) & " " & planstarttime.Text) And IsDate(toDate(planenddate.Text) & " " & planendtime.Text) Then
            If CDate(toDate(planstartdate.Text) & " " & planstarttime.Text) > CDate(toDate(planenddate.Text) & " " & planendtime.Text) Then
                sMsg &= "- Rencana Mulai harus lebih kecil dari Rencana Selesai !! <br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - Warning")
            Exit Sub
        End If

        If Session("spkprocessitem") Is Nothing Then
            sSql = "select 0 spkdtloid , 0 seqprocessitem, 0 spkprocessoid, '' spkprocessname, 0 processoid,  '01/01/1900 00:00:00' planstart, '01/01/1900 00:00:00' planend, 0 machineoid,0 machinedtloid,'' machine,  0.0 outputqtytolerance,  0.0  stdpreptime, 0.0 stddowntime, 0.0 stdspeed,0.0 stdprodtime,  '' note,   'In Process' spkprodstatus  "
            Session("spkprocessitem") = CreateDataTableFromSQL(sSql)
            CType(Session("spkprocessitem"), DataTable).Rows.Clear()
            Session("spkprocessoid") = 1
            processname_1.Items.Clear()
            processname_2.Items.Clear()
        End If

        Dim objTable As DataTable
        objTable = Session("spkprocessitem")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        Dim dv As DataView = objTable.DefaultView
        If i_u_spkprocessitem.Text = "New Detail" Then
            dv.RowFilter = "spkdtloid = " & spkdtloid_2.Text & " and seqprocessitem=" & seqprocess1.SelectedValue
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data Process Service sudah ada, Tolong check lagi !!", "Warning")
                MultiView1.ActiveViewIndex = 1
                Exit Sub
            End If
        End If
        dv.RowFilter = ""

        Dim total1 As Double = 0.0
        'insert/update to list data
        Dim objRow As DataRow
        If i_u_spkprocessitem.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("spkprocessoid") = objTable.Rows.Count + 1
            Session("spkprocessoid") = objTable.Rows.Count + 1
        Else 'update
            objRow = objTable.Select("spkprocessoid=" & spkprocessoid.Text)(0)
            objRow.BeginEdit()
        End If
        objRow("seqprocessitem") = seqprocess1.SelectedValue
        objRow("spkdtloid") = spkdtloid_2.SelectedValue
        objRow("spkprocessname") = processoid.SelectedItem.Text
        objRow("processoid") = processoid.SelectedValue

        objRow("planstart") = CDate(toDate(planstartdate.Text)) & " " & planstarttime.Text & ":00"
        objRow("planend") = CDate(toDate(planenddate.Text)) & " " & planendtime.Text & ":00"
        objRow("machineoid") = machineoid.Text
        objRow("machinedtloid") = machinedtloid.SelectedValue
        objRow("machine") = machinedtloid.SelectedItem.Text
        objRow("outputqtytolerance") = ToDouble(ddltolerance.SelectedValue)
        objRow("stdpreptime") = ToDouble(stdpreptime.Text)
        objRow("stddowntime") = ToDouble(stddowntime.Text)
        objRow("stdspeed") = ToDouble(stdspeed.Text)
        objRow("stdprodtime") = ToDouble(stdprodtime.Text)
        objRow("spkprodstatus") = spkprodstatus.Text
        objRow("note") = note.Text
        If i_u_spkprocessitem.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
            ' objTable.AcceptChanges()
        End If
        Session("spkprocessitem") = objTable
        gvprocessitem.DataSource = objTable
        gvprocessitem.DataBind()

        ClearSPKProcessItem()
        ProcessCalculateRangePlanSPK()
        ClearSPKProcessItem()
        initDDLProcess()
        inittolerance()

        FillProcessname(spkdtloid_3.SelectedValue, processname_1)
        FillProcessname(spkdtloid_4.SelectedValue, processname_2)

        processoid.Enabled = True
        processoid.CssClass = "inpText"
    End Sub

    Protected Sub btnCancelDtl2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelDtl2.Click
        ClearSPKProcessItem()
        initDDLProcess()
    End Sub

    Protected Sub spkdtloid_3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spkdtloid_3.SelectedIndexChanged
        FillProcessname(spkdtloid_3.SelectedValue, processname_1)
    End Sub

    Sub FillProcessname(ByVal ispkdtloid As Int16, ByRef oProcessname As DropDownList)
        oProcessname.Items.Clear()
        If Session("spkprocessitem") Is Nothing = False Then
            Dim otable As DataTable = Session("spkprocessitem")
            Dim oRow() As DataRow = otable.Select("spkdtloid=" & ispkdtloid)
            If oRow.Length > 0 Then
                For C1 As Int16 = 0 To oRow.Length - 1
                    ' If ispkdtloid = otable.Rows(c1).Item("spkdtloid") Then
                    oProcessname.Items.Add(New ListItem(oRow(C1)("spkprocessname"), oRow(C1)("spkprocessoid")))
                    ' End If
                Next
            End If
            otable.Select(Nothing)
        End If
    End Sub

    Protected Sub GVMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVMat2.PageIndexChanging
        GVMat2.PageIndex = e.NewPageIndex
        BindDataMatPopup("")
    End Sub

    Protected Sub GVmat2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVMat2.SelectedIndexChanged
        If Session("PROCESSE") = "MATERIAL" Then
            matoid.Text = GVMat2.SelectedDataKey("matoid").ToString().Trim
            matdesc.Text = GVMat2.SelectedDataKey("matlongdesc").ToString().Trim
            matqty.Text = GVMat2.SelectedDataKey("qty").ToString().Trim
            mattolerancepct.Text = 0 : outputref.Text = "QL_MSTREGION"
            matunit.Items.Clear()
            matunit.Items.Add(GVMat2.SelectedDataKey("matunit"))
            matunit.Items(0).Value = GVMat2.SelectedDataKey("matunitoid")
            MultiView1.ActiveViewIndex = 2
        Else 'WIP
            outputoid.Text = GVMat2.SelectedDataKey("matoid").ToString().Trim
            outputname.Text = GVMat2.SelectedDataKey("matlongdesc").ToString().Trim 
            outputqty1.Text = GVMat2.SelectedDataKey("qty").ToString().Trim
            outputref.Text = "QL_MSTREGION"
            outputunit1.Items.Clear()
            outputunit1.Items.Add(GVMat2.SelectedDataKey("matunit"))
            outputunit1.Items(0).Value = GVMat2.SelectedDataKey("matunitoid")
            MultiView1.ActiveViewIndex = 1
        End If
        GVMat2.Visible = False : gvItem2.Visible = False
        CProc.DisposeGridView(sender)
    End Sub

    Protected Sub btnCancelDtl4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelDtl4.Click
        MultiView1.ActiveViewIndex = 2
        ClearProcessMaterial()
    End Sub

    Protected Sub GVprocessmaterial_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVprocessmaterial.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
            e.Row.Cells(8).Text = ToMaskEdit(e.Row.Cells(8).Text, 3)
        End If
    End Sub

    Protected Sub GVprocessmaterial_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVprocessmaterial.RowDeleting
        Dim objTable As DataTable = Session("spkprocessmaterial")
        objTable.Rows.RemoveAt(e.RowIndex)

        ' Resequence
        For C1 As Integer = 0 To objTable.Rows.Count - 1
            objTable.Rows(C1)("spkprocessmatoid") = C1 + 1
        Next

        Session("spkprocessmaterial") = objTable
        ClearProcessMaterial()
    End Sub

    Protected Sub GVprocessmaterial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVprocessmaterial.SelectedIndexChanged
        i_u_spkprocessmaterial.Text = "Update"
        i_u3.Text = GVprocessmaterial.SelectedDataKey("spkprocessmatoid").ToString
        matoid.Text = GVprocessmaterial.SelectedDataKey("matoid")
        spkdtloid_4.SelectedValue = GVprocessmaterial.SelectedDataKey("spkdtloid")
        FillProcessname(spkdtloid_4.SelectedValue, processname_2)
        processname_2.SelectedValue = GVprocessmaterial.SelectedDataKey("spkprocessoid")
        matoid.Text = GVprocessmaterial.SelectedDataKey("matoid")
        oldmatoid.Text = GVprocessmaterial.SelectedDataKey("matoid")
        sSql = "SELECT g1.gendesc matunit1, g1.genoid matunitoid1 From QL_mstitem m Inner Join QL_mstgen g1 On g1.genoid=m.satuan1 Where m.itemoid =" & matoid.Text
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        matunit.Items.Clear()
        matunit2.Items.Clear()
        While xreader.Read
            matunit.Items.Add(xreader("matunit1"))
            matunit.Items(0).Value = xreader("matunitoid1")
        End While
        xreader.Close()
        conn.Close()
        matdesc.Text = GVprocessmaterial.SelectedDataKey("matlongdesc")
        matref.Text = GVprocessmaterial.SelectedDataKey("matref")
        matqty.Text = NewMaskEdit(GVprocessmaterial.SelectedDataKey("qty1"))
        LocOid.SelectedValue = GVprocessmaterial.SelectedDataKey("mtrlocoid")
        matunit.SelectedValue = GVprocessmaterial.SelectedDataKey("unitoid1")
        notedtlprosesmat.Text = GVprocessmaterial.SelectedDataKey("note")
        Try
        Catch ex As Exception
        End Try
        wastestd.Text = NewMaskEdit(GVprocessmaterial.SelectedDataKey("wastestd"))
        mattolerancepct.Text = NewMaskEdit(GVprocessmaterial.SelectedDataKey("matroutetolerancepct"))
        MultiView1.ActiveViewIndex = 2
        processname_2.Enabled = False : spkdtloid_4.Enabled = False
        processname_2.CssClass = "inpTextDisabled"
        spkdtloid_4.CssClass = "inpTextDisabled"
        btnSearchMat.Visible = True : btnClearMat.Visible = False
        GVprocessmaterial.Columns(0).Visible = False
        GVprocessmaterial.Columns(9).Visible = False
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMat.Click
        ClearProcessMaterial()
        GVMat3.Visible = False
        DisposeGrid(GVMat3)
        MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub spkdtloid_4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spkdtloid_4.SelectedIndexChanged
        FillProcessname(spkdtloid_4.SelectedValue, processname_2)
    End Sub

    Protected Sub btnAddToList4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList4.Click
        Dim sMsg As String = "" : StockAkhir.Visible = False
        If spkdtloid_4.Items.Count = 0 Then
            sMsg &= "Tolong Isi Barang No !! <br>"
        End If
        If ToDouble(matoid.Text) = 0 Then
            sMsg &= "Tolong Isi Sparepart !! <br>"
        End If
        If ToDouble(matqty.Text) = 0 Then
            sMsg &= "Tolong Isi QTY !! <br>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, "Warning")
            Exit Sub
        End If

        'create table spkprocessMaterial
        If Session("spkprocessmaterial") Is Nothing Then
            sSql = " select 0 spkdtloid, 0 spkprocessmatoid, 0 spkprocessoid, 0 matoid,   '' matref, 0.0 qty1, 0 unitoid1, '' unit1, 0.0 qty2, 0 unitoid2, '' unit2, 0.0 wastestd, 0.0 matroutetolerancepct, '' spkprocessname, '' matlongdesc, '' note,0 mtrlocoid"
            Session("spkprocessmaterial") = CreateDataTableFromSQL(sSql)
            CType(Session("spkprocessmaterial"), DataTable).Rows.Clear()
            Session("spkprocessmatoid") = 1
        End If

        Dim objTable As DataTable
        objTable = Session("spkprocessmaterial")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        Dim dv As DataView = objTable.DefaultView
        If i_u_spkprocessmaterial.Text = "New Detail" Then
            dv.RowFilter = "matoid=" & matoid.Text & " and spkdtloid = " & spkdtloid_4.SelectedValue & " and spkprocessoid=" & processname_2.SelectedValue
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data Process Sparepart sudah ada, Tolong check lagi !!", "Warning")
                Exit Sub
            End If
        End If
        dv.RowFilter = ""
        If ToDouble(matqty.Text) > ToDouble(StockAkhir.Text) Then
            showMessage("Qty Sparepart tidak boleh lebih dari Qty Stock !", 2)
            Exit Sub
        End If
        'insert/update to list data
        Dim objRow As DataRow
        If i_u_spkprocessmaterial.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("spkprocessmatoid") = objTable.Rows.Count + 1
        Else 'update
            ' objRow = objTable.Select("matoid=" & oldmatoid.Text & " AND spkdtloid=" & spkdtloid_4.SelectedValue & " AND spkprocessoid=" & processname_2.SelectedValue)(0)
            objRow = objTable.Select("spkprocessmatoid=" & i_u3.Text)(0)
            objRow.BeginEdit()
        End If

        objRow("spkdtloid") = spkdtloid_4.SelectedValue
        objRow("spkprocessoid") = processname_2.SelectedValue
        objRow("spkprocessname") = processname_2.SelectedItem.Text
        objRow("matoid") = matoid.Text
        objRow("matlongdesc") = matdesc.Text
        objRow("matref") = matref.Text
        objRow("qty1") = Math.Round(CDec(matqty.Text), 2)
        objRow("unitoid1") = matunit.SelectedValue
        objRow("unit1") = matunit.SelectedItem.Text
        objRow("qty2") = 0 'Math.Round(ToDouble(matqty2.Text), 2)
        objRow("unitoid2") = 0 ' matunit2.SelectedValue
        objRow("unit2") = "" 'matunit2.SelectedItem.Text
        objRow("wastestd") = ToDouble(wastestd.Text)
        objRow("matroutetolerancepct") = ToDouble(mattolerancepct.Text)
        objRow("note") = notedtlprosesmat.Text
        objRow("mtrlocoid") = LocOid.SelectedValue
        If i_u_spkprocessmaterial.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
            objTable.Select(Nothing)
        End If
        objTable.AcceptChanges()
        Session("spkprocessmaterial") = objTable
        GVprocessmaterial.DataSource = objTable
        GVprocessmaterial.DataBind()
        ClearProcessMaterial()
    End Sub

    Protected Sub btnSearchOutput_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchOutput.Click
        If outputtype.SelectedValue.ToLower = "region" Then
            '    'Session("PROCESSE") = "WIP"

            '    'FillDDLGen("MATCAT", matcat)
            '    'FillDDLGen("MATSUBCAT", subcat, "  genother1='" & matcat.SelectedValue & "'")
            '    'FillDDLGen("MATSUBDTL", subcatdtl, "  genother1='" & subcat.SelectedValue & "'")
            '    ''matcat.Items.Add("ALL")
            '    ''subcat.Items.Add("ALL")
            '    ''subcatdtl.Items.Add("ALL")
            '    ''matcat.SelectedValue = ("ALL")
            '    ''subcat.SelectedValue = ("ALL")
            '    ''subcatdtl.SelectedValue = ("ALL")

            '    'CProc.DisposeGridView(GVMat2)
            '    Dim sqltext As String = " " 'AND " & DDLFilterMat2.SelectedValue & " LIKE '%" & Tchar(FilterMat2.Text) & "%' "
            '    'If Session("PROCESSE") = "WIP" Then
            '    '    sqltext = sqltext & "  and matgroup='WIP'   "
            '    'End If

            BindDataMatPopup("")
            '    'PanelMat2.Visible = True
            '    'ButtonMat2.Visible = True
            '    'MPEMat2.Show()
        Else
            'panelItem2.Visible = True
            'ButtonItem2.Visible = True
            'ModalPopupXX.Show()
            binddataITem("")
        End If
    End Sub

    Protected Sub outputtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles outputtype.SelectedIndexChanged
        clearspkprocessitemresult()
    End Sub

    Protected Sub btnCancelDtl3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelDtl3.Click
        clearspkprocessitemresult()
        btnClearOutput.Visible = True
    End Sub

    Protected Sub gvprocessitemresult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvprocessitemresult.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 3)
        End If
    End Sub

    Protected Sub GVprocessItemresult_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvprocessitemresult.RowDeleting
        Dim indeke As Int16 = e.RowIndex
        Dim objTable As DataTable = Session("spkprocessitemresult")
        Dim Del_spkprocessoid As Int16 = objTable.Rows(e.RowIndex).Item("spkprocessoid")
        objTable.Rows.RemoveAt((e.RowIndex))
        ''resequence processitem
        'For C1 As Int16 = 0 To objTable.Rows.Count - 1
        '    Dim dr As DataRow = objTable.Rows(C1)
        '    dr.BeginEdit()
        '    If dr("spkprocessoid") > Del_spkprocessoid Then
        '        dr("spkprocessoid") = dr("spkprocessoid") - 1
        '    End If
        '    dr.EndEdit()
        'Next
        Session("spkprocessitemResult") = objTable
        gvprocessitemresult.DataSource = objTable
        gvprocessitemresult.DataBind()
        clearspkprocessitemresult()
    End Sub

    Protected Sub btnAddToList3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList3.Click
        Dim sMsg As String = ""
        If spkdtloid_3.Items.Count = 0 Then
            sMsg &= "Tolong Isi Barang No !! <br>"
        End If
        If ToDouble(outputoid.Text) = 0 Then
            sMsg &= "Tolong Isi Nama Job !! <br>"
        End If
        'If ToDouble(outputqty1.Text) = 0 Then
        '    sMsg &= "Please Fill Tarif Job !! <br>"
        'End If

        If sMsg <> "" Then
            showMessage(sMsg, "Warning")
            Exit Sub
        End If

        'create table spkprocessitemResult
        If Session("spkprocessitemResult") Is Nothing Then
            sSql = " select 0 spkdtloid, 0 spkprocessoid, 0 outputoid, '' outputref, '' outputtype, 0.0  outputqty1, 0 outputunitoid1, '' outputunit1, 0.0  outputqty2, 0 outputunitoid2, '' outputunit2, '' outputname, '' spkprocessname, '' note , '' typejob "
            Session("spkprocessitemResult") = CreateDataTableFromSQL(sSql)
            CType(Session("spkprocessitemResult"), DataTable).Rows.Clear()
        End If

        Dim objTable As DataTable
        objTable = Session("spkprocessitemResult")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        Dim dv As DataView = objTable.DefaultView

        'dicoment sehingga update & new tetap di cek out itemnya
        'If i_u_spkprocessitemresult.Text = "New Detail" Then
        dv.RowFilter = "outputoid=" & outputoid.Text & "  and spkdtloid = " & spkdtloid_3.Text & " and spkprocessoid=" & processname_1.SelectedValue & "  and outputref='" & outputref.Text & "' "
        If dv.Count > 0 Then
            dv.RowFilter = ""
            showMessage("Data Process Service Job sudah ada, Tolong check lagi !!", "Warning")
            Exit Sub
        End If
        'End If
        dv.RowFilter = ""

        'insert/update to list data
        Dim objRow As DataRow
        If i_u_spkprocessitemresult.Text = "New Detail" Then
            objRow = objTable.NewRow()
        Else 'update
            Dim posisirow As Int16 = 0
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                If objTable.Rows(c1).Item("outputref") = outputref.Text And objTable.Rows(c1).Item("outputoid") = olditemoid.Text And objTable.Rows(c1).Item("spkdtloid") = spkdtloid_3.SelectedValue And objTable.Rows(c1).Item("spkprocessoid") = processname_1.SelectedValue Then
                    posisirow = c1
                    Exit For
                End If
            Next
            objRow = objTable.Rows(posisirow)
            objRow.BeginEdit()
        End If
        objRow("spkdtloid") = spkdtloid_3.SelectedValue
        objRow("spkprocessoid") = processname_1.SelectedValue
        objRow("spkprocessname") = processname_1.SelectedItem.Text

        objRow("outputoid") = outputoid.Text
        objRow("outputname") = outputname.Text
        objRow("outputref") = outputref.Text
        objRow("outputqty1") = Math.Round(CDec(outputqty1.Text), 2)
        objRow("outputunitoid1") = outputunit1.SelectedValue
        objRow("outputunit1") = outputunit1.SelectedItem.Text
        objRow("outputqty2") = 0 'Math.Round(ToDouble(outputqty2.Text), 2)
        objRow("outputunitoid2") = 0 'outputunit2.SelectedValue
        objRow("outputunit2") = "" 'outputunit2.SelectedItem.Text
        objRow("outputtype") = outputtype.SelectedValue
        objRow("typejob") = outputtype.SelectedItem.Text
        objRow("note") = notedtlitemresult.Text
        If i_u_spkprocessitemresult.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
        End If

        Session("spkprocessitemResult") = objTable
        gvprocessitemresult.DataSource = objTable
        gvprocessitemresult.DataBind()

        clearspkprocessitemresult()
        btnClearOutput.Visible = True
    End Sub

    Protected Sub GVprocessitemresult_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvprocessitemresult.SelectedIndexChanged
        spkdtloid_3.SelectedValue = gvprocessitemresult.SelectedDataKey("spkdtloid")
        FillProcessname(spkdtloid_3.SelectedValue, processname_1)
        processname_1.SelectedValue = gvprocessitemresult.SelectedDataKey("spkprocessoid")
        outputoid.Text = gvprocessitemresult.SelectedDataKey("outputoid")
        olditemoid.Text = gvprocessitemresult.SelectedDataKey("outputoid")
        outputtype.Text = (gvprocessitemresult.SelectedDataKey("outputtype"))
        'If outputtype.Text = "WIP" Then
        sSql = "select  '' matunit1, 0 matunitoid1 "
        'Else 'FG
        'sSql = "select i.itemoid, i.itemcode, i.itemlongdesc, i.itemunitoid matunitoid1, g1.gendesc matunit1    from ql_mstitem i   inner join QL_mstgen g1 on g1.genoid=i.itemunitoid   where itemoid=" & outputoid.Text
        'End If

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        outputunit1.Items.Clear()
        outputunit2.Items.Clear()
        While xreader.Read
            outputunit1.Items.Add(xreader("matunit1"))
            outputunit1.Items(0).Value = xreader("matunitoid1")
            'outputunit1.Items.Add(xreader("matunit"))
            'outputunit1.Items(1).Value = xreader("matunitoid2")
            'outputunit1.Items.Add(xreader("matunit3"))
            'outputunit1.Items(2).Value = xreader("matunitoid3")

            'outputunit2.Items.Add(xreader("matunit1"))
            'outputunit2.Items(0).Value = xreader("matunitoid1")
            'outputunit2.Items.Add(xreader("matunit2"))
            'outputunit2.Items(1).Value = xreader("matunitoid2")
            'outputunit2.Items.Add(xreader("matunit3"))
            'outputunit2.Items(2).Value = xreader("matunitoid3")

            'unit1unit2conv.Text = xreader("matunit1unit2conv")
            'unit2unit3conv.Text = xreader("matunit2unit3conv")
        End While
        xreader.Close()
        conn.Close()

        outputname.Text = gvprocessitemresult.SelectedDataKey("outputname")
        outputref.Text = gvprocessitemresult.SelectedDataKey("outputref")
        outputqty1.Text = NewMaskEdit(gvprocessitemresult.SelectedDataKey("outputqty1"))
        outputunit1.SelectedValue = gvprocessitemresult.SelectedDataKey("outputunitoid1")
        notedtlitemresult.Text = gvprocessitemresult.SelectedDataKey("note")
        'outputqty2.Text = NewMaskEdit(gvprocessitemresult.SelectedDataKey("outputqty2"))
        Try
            'outputunit2.SelectedValue = gvprocessitemresult.SelectedDataKey("outputunitoid2")
        Catch ex As Exception
        End Try
        outputtype.Enabled = False
        MultiView1.ActiveViewIndex = 1

        i_u_spkprocessitemresult.Text = "Update"
        processname_1.Enabled = False
        processname_1.CssClass = "inpTextDisabled"
        spkdtloid_3.Enabled = False
        spkdtloid_3.CssClass = "inpTextDisabled"
        outputtype.Enabled = False
        outputtype.CssClass = "inpTextDisabled"
        btnClearOutput.Visible = False

        gvprocessitemresult.Columns(0).Visible = False
        gvprocessitemresult.Columns(9).Visible = False
    End Sub

    Public Sub binddataITem(ByVal sqlPlus As String)
        sSql = "select i.itservoid itemoid, i.itservcode itemcode, i.itservdesc itemlongdesc, 0 itemunitoid, '' unit , i.itservtarif from QL_MSTITEMSERV i   where i.cmpcode = '" & cmpcode & "' and ITSERVTYPEOID = " & outputtype.SelectedValue & "  and (ITSERVCODE  like '%" & Tchar(outputname.Text) & "%' or ITSERVDESC  like '%" & Tchar(outputname.Text) & "%') and typejob = " & spkjenisbarang.Text & " "

        'select i.itemoid, i.itemcode, i.itemlongdesc, i.itemunitoid, g1.gendesc unit from ql_mstitem i   inner join QL_mstgen g1 on g1.genoid=i.itemunitoid where i.cmpcode = '" & cmpcode & "' and (itemcode like '%" & Tchar(outputname.Text) & "%' or itemlongdesc like '%" & Tchar(outputname.Text) & "%')"
        '"select i.itemoid, i.itemcode, i.itemlongdesc, i.itemunit1unit2conv,  i.itemunit2unit3conv , i.itemunitoid1, i.itemunitoid2,i.itemunitoid3,  g1.gendesc unit1,g2.gendesc unit2,g3.gendesc unit3    from ql_mstitem i   inner join QL_mstgen g1 on g1.genoid=i.itemunitoid1  inner join QL_mstgen g2 on g2.genoid=i.itemunitoid2  inner join QL_mstgen g3 on g3.genoid=i.itemunitoid3 where 1=1 " & IIf(matcatoid.SelectedValue = "ALL", "", " and itemcategoryoid=" & matcatoid.SelectedValue) & IIf(ddlitemsubcat.SelectedValue = "ALL", "", " and  itemsubcategoryoid=" & ddlitemsubcat.SelectedValue) & IIf(ddlitemsubdtl.SelectedValue = "ALL", "", "  and itemsubdtlcategoryoid=" & ddlitemsubdtl.SelectedValue) & sqlPlus
        FillGV(gvItem2, sSql, "QL_mstitemserv")
        gvItem2.Visible = True

    End Sub

    Protected Sub gvItem2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem2.PageIndexChanging
        gvItem2.PageIndex = e.NewPageIndex
        binddataITem("")
    End Sub

    Protected Sub gvItem2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem2.SelectedIndexChanged
        outputoid.Text = gvItem2.SelectedDataKey("itemoid")
        outputname.Text = gvItem2.SelectedDataKey("itemlongdesc")
        If outputname.Text.ToLower = "other" Then
            outputqty1.CssClass = "inpText"
            outputqty1.Enabled = True
        Else
            outputqty1.CssClass = "inpTextDisabled"
            outputqty1.Enabled = False
        End If
        'unit1unit2conv.Text = gvItem2.SelectedDataKey("itemunit1unit2conv")
        'unit2unit3conv.Text = gvItem2.SelectedDataKey("itemunit2unit3conv")

        outputunit1.Items.Clear()
        outputunit1.Items.Add(gvItem2.SelectedDataKey("unit"))
        outputunit1.Items(0).Value = gvItem2.SelectedDataKey("itemunitoid")
        'outputunit1.Items.Add(gvItem2.SelectedDataKey("unit2"))
        'outputunit1.Items(1).Value = gvItem2.SelectedDataKey("itemunitoid2")
        'outputunit1.Items.Add(gvItem2.SelectedDataKey("unit3"))
        'outputunit1.Items(2).Value = gvItem2.SelectedDataKey("itemunitoid3")

        'outputunit2.Items.Clear()
        'outputunit2.Items.Add(gvItem2.SelectedDataKey("unit1"))
        'outputunit2.Items(0).Value = gvItem2.SelectedDataKey("itemunitoid1")
        'outputunit2.Items.Add(gvItem2.SelectedDataKey("unit2"))
        'outputunit2.Items(1).Value = gvItem2.SelectedDataKey("itemunitoid2")
        'outputunit2.Items.Add(gvItem2.SelectedDataKey("unit3"))
        'outputunit2.Items(2).Value = gvItem2.SelectedDataKey("itemunitoid3")
        outputqty1.Text = gvItem2.SelectedDataKey("itservtarif")
        outputref.Text = "QL_MSTITEMSERV"
        'outputqty2.Text = 0
        gvItem2.Visible = False
        GVMat2.Visible = False
        'CProc.SetModalPopUpExtender(ButtonItem2, panelItem2, ModalPopupXX, False)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub outputqty1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles outputqty1.TextChanged
        outputqty1.Text = NewMaskEdit(outputqty1.Text)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub outputqty2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles outputqty2.TextChanged
        outputqty2.Text = NewMaskEdit(outputqty2.Text)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub matqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles matqty.TextChanged
        matqty.Text = ToMaskEdit(ToDouble(matqty.Text), 3)
    End Sub

    Protected Sub bomoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bomoid.SelectedIndexChanged
        sSql = "select itemunitoid, itemqty from ql_mstbom where bomoid =" & bomoid.SelectedValue
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        bomunitoid.Text = 0
        bomqty.Text = 0
        xreader = xCmd.ExecuteReader
        While xreader.Read
            bomunitoid.Text = xreader("itemunitoid")
            bomqty.Text = xreader("itemqty")
        End While
        xreader.Close()
        conn.Close()
    End Sub

    Protected Sub spkdtloid_2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spkdtloid_2.SelectedIndexChanged
        If Session("spkresultitem") Is Nothing = False Then
            GetDataProcess()
        End If
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub processoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles processoid.SelectedIndexChanged
        sSql = " select 0 machinedtloid, '' machinedesc "
        FillDDL(machinedtloid, sSql)
        'If machinedtloid.Items.Count = 0 Then
        '    showMessage("No Machine Found for this Process", "Warning")
        '    Exit Sub
        'End If
        'initDDLMachine()
        If machinedtloid.SelectedValue <> "None" Then
            FillDataFromMachine(machinedtloid.SelectedValue)
        End If
    End Sub

    Sub GetDataProcess()
        'Dim dv As DataView = CType(Session("spkresultitem"), DataTable).DefaultView
        'dv.RowFilter = "ioseq =" & spkdtloid_2.SelectedValue
        'itemoid_resultno.Text = dv.Item(0).Item("itemoid")
        'dv.RowFilter = ""

        'sSql = "select distinct g.genoid, g.gendesc from ql_mstgen g inner join QL_mstbomgrouproute br on br.routeoid = g.genoid inner join QL_mstbom b on b.bomoid = br.bomoid where b.itemoid = ( select itemgroupcode from ql_mstitem where itemoid = " & itemoid_resultno.Text & ")"
        'xCmd.CommandText = sSql
        'If conn.State = ConnectionState.Closed Then
        '    conn.Open()
        'End If
        'qtycylinder.Text = 0
        'jenisfgoid.Text = 0
        'processoid.Items.Clear()
        'xreader = xCmd.ExecuteReader
        'While xreader.Read
        '    qtycylinder.Text = ""
        '    jenisfgoid.Text = ""
        '    processoid.Items.Add(xreader("gendesc"))
        '    processoid.Items(processoid.Items.Count - 1).Value = xreader("genoid")
        'End While
        'xreader.Close()
        'conn.Close()
        ''processoid.SelectedIndex = 0


        'sSql = "select distinct m.machinedtloid, G.machinedesc from QL_mstmachinedtl m   inner join QL_mstmachine g on g.machineoid=m.machineoid         where m.prosesoid = " & processoid.SelectedValue
        'FillDDL(machinedtloid, sSql)
        'If machinedtloid.Items.Count = 0 Then
        '    showMessage("No Machine Found for this Process", "Warning")
        '    Exit Sub
        'End If
        'FillDataFromMachine(ToDouble(machinedtloid.SelectedValue))
    End Sub

    Sub FillDataFromMachine(ByVal machinedtloid As Int32)
        'sSql = "select machineoid, fixwaste, fixwasteunitoid, effmachinepct, varwastepct, speed, speedunit1oid, preptime, shuttime from QL_mstmachinedtl where machinedtloid=" & machinedtloid
        'If conn.State = ConnectionState.Closed Then
        '    conn.Open()
        'End If
        'xCmd.CommandText = sSql
        'xreader = xCmd.ExecuteReader
        'While xreader.Read
        machineoid.Text = "0" 'xreader("machineoid")
        stdpreptime.Text = "0" 'xreader("preptime")
        stddowntime.Text = "0" 'xreader("shuttime")
        stdspeed.Text = "0" 'xreader("speed")
        ''      objRow("stdprodtime") = xreader("preptime")
        'End While
        'xreader.Close()
        'conn.Close()
    End Sub

    Protected Sub machinedtloid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles machinedtloid.SelectedIndexChanged
        If machinedtloid.SelectedValue <> "None" Then
            FillDataFromMachine(machinedtloid.SelectedValue)
        End If
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub ddlitemsubdtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnioclear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Me.ClearDetailIO()
        TABLE1.Visible = False
    End Sub

    Protected Sub btnClearOutput_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        clearspkprocessitemresult()
        GVMat2.Visible = False
        gvItem2.Visible = False
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataMatPopup2("")
    End Sub

    Protected Sub GVMat3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        StockAkhir.Visible = True
        matoid.Text = GVMat3.SelectedDataKey("matoid").ToString().Trim
        matdesc.Text = GVMat3.SelectedDataKey("matlongdesc").ToString().Trim
        matqty.Text = GVMat3.SelectedDataKey("Qty").ToString().Trim
        StockAkhir.Text = GVMat3.SelectedDataKey("Qty").ToString().Trim
        mattolerancepct.Text = 0 : matunit.Items.Clear()
        matunit.Items.Add(GVMat3.SelectedDataKey("matunit"))
        matunit.Items(0).Value = GVMat3.SelectedDataKey("matunitoid")
        MultiView1.ActiveViewIndex = 2
        GVMat3.Visible = False
        DisposeGrid(GVMat3)
    End Sub

    Protected Sub GVMat3_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVMat3.PageIndexChanging
        GVMat3.PageIndex = e.NewPageIndex
        BindDataMatPopup2("")
    End Sub

    Protected Sub LinkButton1_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lb As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lb.NamingContainer, GridViewRow)
        Dim status As String = gvr.Cells(5).Text.ToString
        If status <> "Approved" Then
            showMessage("Print Out SPK hanya untuk SPK dengan status 'Approved' !", 2)
            Exit Sub
        Else
            PrintReport(sender.CommandArgument, sender.ToolTip)
        End If
    End Sub

    Private Sub PrintReport(ByVal id As String, ByVal no As String)
        Try
            'untuk print
            report.Load(Server.MapPath(folderReport & "printspk.rpt"))
            report.SetParameterValue("spkoid", id)
            report.SetParameterValue("spkoid", id, "spksunprocess")
            report.SetParameterValue("spkoid", id, "spksubmat")
            report.SetParameterValue("spkoid", id, "spksubneedmat")

            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
                System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            report.PrintOptions.PaperOrientation = PaperOrientation.Landscape

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no.Replace("/", "_"))
            Catch ex As Exception
                report.Close() : report.Dispose()
            End Try
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR") : Exit Sub
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub ddltolerance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub bindpicdata(ByVal sWhere As String)
        sSql = "SELECT a.userid personnip, a.username personname FROM ql_mstprof a WHERE a.cmpcode = '" & cmpcode & "' AND a.STATUSPROF= 'Active' And USERID Not In (Select personnip From QL_MSTPERSON)AND BRANCH_CODE='" & division.SelectedValue & "' " & sWhere & ""
        Session("datapic") = Nothing
        Session("datapic") = cKon.ambiltabel(sSql, "datapic")
        gvpoppic.DataSource = Session("datapic") : gvpoppic.DataBind()
    End Sub

    Protected Sub ibsearchpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsearchpic.Click
        bindpicdata(" And divisi = 'Approval'")
        CProc.SetModalPopUpExtender(bepopgvpic, panpopgvpic, mpepic, True)
    End Sub

    Protected Sub ibdelpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelpic.Click
        tbpic.Text = ""
        lblpic.Text = ""
    End Sub

    Protected Sub ibpopviewallpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopviewallpic.Click
        bindpicdata("")
        ddlpopfilterpic.SelectedIndex = 0
        tbpopfilterpic.Text = ""
        mpepic.Show()
    End Sub

    Protected Sub ibpopfindpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopfindpic.Click
        Dim sWhere As String = ""

        sWhere &= "AND " & ddlpopfilterpic.SelectedValue & " LIKE '%" & Tchar(tbpopfilterpic.Text) & "%'"

        bindpicdata(sWhere)
        mpepic.Show()
    End Sub

    Protected Sub gvpoppic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvpoppic.SelectedIndexChanged
        lblpic.Text = gvpoppic.SelectedDataKey.Item("personnip")
        tbpic.Text = gvpoppic.SelectedDataKey.Item("personname")
        CProc.SetModalPopUpExtender(bepopgvpic, panpopgvpic, mpepic, False)
    End Sub

    Protected Sub ibpopcancelpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopcancelpic.Click
        CProc.SetModalPopUpExtender(bepopgvpic, panpopgvpic, mpepic, False)
    End Sub

    Private Sub removeApproval(ByVal sjloid As Integer)
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "DELETE FROM ql_approval WHERE tablename = 'QL_TRNSPKMAINPROD' AND oid = " & sjloid & " AND statusrequest = 'New' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, CompnyName & " - Warning")
            Exit Sub
        End Try
    End Sub

    Protected Sub ibapprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapprove.Click

        Dim oid As Integer = 0
        If Integer.TryParse(spkmainoid.Text, oid) = False Then
            showMessage("SPK tidak valid!", CompnyName & " - Warning")
            Exit Sub
        End If

        If tbpic.Text.Trim = "" Or lblpic.Text = "" Then
            showMessage("PIC tidak boleh kosong!", CompnyName & " - Warning")
            Exit Sub
        End If

        If oid > 0 Then
            Dim res As String = ""
            res = sendapproval()
            If res <> "" Then
                showMessage(res, CompnyName & " - Warning")
                'Exit Sub
            Else
                spkstatus.Text = "In Approval"
                btnSave_Click(sender, e)
                'res = postingrecord()
                'If res <> "" Then
                '    'removeApproval(oid)
                '    showMessage(res, 2)
                '    'Exit Sub
                'Else
                '    Response.Redirect("~\Transaction\trnsuratjalan.aspx?awal=true")
                'End If
            End If
        End If

    End Sub

    Private Function sendapproval() As String
        Dim res As String = ""

        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans
        Try
            Dim loid As Integer = 0
            If Integer.TryParse(spkmainoid.Text, loid) Then

                sSql = "UPDATE ql_approval SET statusrequest = 'Canceled' WHERE approvaloid IN (SELECT approvaloid FROM ql_approval WHERE tablename = 'QL_TRNSPKMAINPROD' AND oid = " & loid & " AND cmpcode = '" & cmpcode & "' AND statusrequest = 'New') AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "SELECT lastoid FROM ql_mstoid WHERE tablename = 'QL_APPROVAL' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : Dim oid As Integer = xCmd.ExecuteScalar + 1

                sSql = "INSERT INTO ql_approval (cmpcode,branch_code, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser) VALUES ('" & cmpcode & "','" & division.SelectedValue & "', " & oid & ", '0', '" & Session("UserID") & "', '" & DateTime.Now & "', 'New', 'QL_TRNSPKMAINPROD', " & loid & ", 'In Approval', '0', '" & lblpic.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_mstoid SET lastoid = " & oid & " WHERE tablename = 'QL_APPROVAL' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                res = "Surat jalan tidak valid!"
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                Return res
            End If

            otrans.Commit()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            res = ex.ToString
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return res
        End Try

        Return res
    End Function
#End Region

    Protected Sub division_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitLoc()
    End Sub
End Class
