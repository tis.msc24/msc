'Created By Muji On 30 September 2014
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trncashbon
    Inherits System.Web.UI.Page

#Region "Variable"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim sql1 As String = ""
    Dim cProc As New ClassProcedure
    Private objConnExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Private ws As DataTable
    Public sql_temp As String
    Private cKon As New Koneksi
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        Dim sSql As String = "Select gencode,gendesc from ql_mstgen Where gengroup = 'cabang' order by gencode asc"
        FillDDL(outlet, sSql)
        If Session("branch_id") <> "01" Then
            outlet.SelectedValue = Session("branch_id")
            outlet.Enabled = False
            outlet.CssClass = "inpTextDisabled"
        End If
        If outlet.Items.Count > 0 Then
            InitDDLPerson()
        End If
        FillDDLAcctg(bsaccountoid, "VAR_BS", outlet.SelectedValue)
    End Sub

    Private Sub InitDDLPerson()
        ' Fill DDL PIC
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE STATUS='AKTIF' AND branch_code='" & outlet.SelectedValue & "'"
        FillDDL(personoid, sSql)
        Try
            personoid.SelectedValue = GetStrData("SELECT personoid FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CheckReceiptStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' AND cashbankgroup='RECEIPT'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnCashBon.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbReceiptInProcess.Visible = True
            lkbReceiptInProcess.Text = "You have " & GetStrData(sSql) & " In Process Cash/Bank Receipt data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub GenerateBSNo()
        If outlet.SelectedValue <> "" Then
            Dim sErr As String = ""
            If cashbankdate.Text <> "" Then
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    Dim cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & outlet.SelectedValue & "' And gengroup='CABANG'")
                    Dim sNo As String = "BS/" & cabang & "/" & Format(CDate(cashbankdate.Text), "yy/MM/dd") & "/"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(bsno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnBSmst WHERE cmpcode='" & outlet.SelectedValue & "' AND bsno LIKE '%" & sNo & "%'"
                    If GetStrData(sSql) = "" Then
                        BSNo.Text = GenNumberString(sNo, "", 1, 4)
                    Else
                        BSNo.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub GenerateReceiptNo()
        If outlet.SelectedValue <> "" Then
            Dim sErr As String = ""
            If cashbankdate.Text <> "" Then
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    Dim cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & outlet.SelectedValue & "' And gengroup='CABANG'")
                    Dim sNo As String = "BKK/" & cabang & "/" & Format(CDate(cashbankdate.Text), "yy/MM/dd") & "/"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & outlet.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%'"
                    If GetStrData(sSql) = "" Then
                        cashbankno.Text = GenNumberString(sNo, "", 1, 4)
                    Else
                        cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub EnableAddInfo(ByVal bVal As Boolean)
        lblDueDate.Visible = bVal
        lblWarnDueDate.Visible = bVal
        lblSeptDueDate.Visible = bVal
        cashbankduedate.Visible = bVal
        imbDueDate.Visible = bVal
        lblInfoDueDate.Visible = bVal
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "bsoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        outlet.CssClass = sCss : outlet.Enabled = bVal
    End Sub

    Private Sub CountTotalAmt()
        Dim dVal As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("cashbankglamt").ToString)
            Next
        End If
        cashbankamt.Text = ToMaskEdit(dVal, 4)
    End Sub

    Public Sub filltextbox(ByVal sOid As String)
        Try
            sSql = "SELECT bs.cmpcode,bs.payrefno, bs.cashbankoid, bs.periodacctg, bs.bsnoManual, cb.cashbankno, bs.bsno, bs.bsoid, bs.bsdate cashbankdate, cb.cashbanktype, bs.payacctgoid acctgoid, bs.curroid, bs.bsamt cashbankamt, bs.pic personoid, bs.duedate cashbankduedate, cb.cashbankrefno, cb.cashbanknote, cb.cashbankstatus, cb.createuser, cb.createtime, cb.upduser, cb.updtime, bs.bsacctgoid FROM QL_trnbsmst bs INNER JOIN QL_trncashbankmst cb ON bs.cashbankoid=cb.cashbankoid WHERE bs.bsoid=" & sOid
            xCmd.CommandText = sSql
            If objConn.State = ConnectionState.Closed Then
                objConn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                outlet.SelectedValue = Trim(xreader("cmpcode").ToString)
                outlet_SelectedIndexChanged(Nothing, Nothing)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                cashbankdate.Text = Format(xreader("cashbankdate"), "MM/dd/yyyy")
                cashbanktype.SelectedValue = Trim(xreader("cashbanktype").ToString)
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
                cashbankduedate.Text = Format(xreader("cashbankduedate"), "MM/dd/yyyy")
                bsmstoid.Text = Trim(xreader("bsoid").ToString)
                BSNo.Text = Trim(xreader("bsno").ToString)
                bsnomanual.Text = Trim(xreader("bsnoManual").ToString)
                bsaccountoid.SelectedValue = Trim(xreader("bsacctgoid").ToString)
                If cashbankduedate.Text = "01/01/1900" Then
                    cashbankduedate.Text = ""
                End If
                cashbankrefno.Text = Trim(xreader("cashbankrefno").ToString)
                acctgoid.SelectedValue = Trim(xreader("acctgoid").ToString)
                cashbankamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankamt").ToString)), 1)
                personoid.SelectedValue = Trim(xreader("personoid").ToString)
                cashbanknote.Text = Trim(xreader("cashbanknote").ToString)
                cashbankstatus.Text = Trim(xreader("cashbankstatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
            End While
            xreader.Close()
            objConn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            BtnSave.Visible = False : BtnDelete.Visible = False : BtnPosting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        outlet.CssClass = "inpTextDisabled" : outlet.Enabled = False
        btnShowCOA.Visible = False
        cashbanktype.CssClass = "inpTextDisabled" : cashbanktype.Enabled = False
        acctgoid.CssClass = "inpTextDisabled" : acctgoid.Enabled = False
        If cashbankstatus.Text = "Post" Then
            BtnSave.Visible = False
            BtnDelete.Visible = False
            BtnPosting.Visible = False
            btnShowCOA.Visible = True
            If Session("UserID") = "admin" Then
                btnUnpost.Visible = True
            Else
                btnUnpost.Visible = False
            End If
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT cb.bsoid, cb.bsno, CONVERT(VARCHAR(10), cb.bsdate, 101) AS bsdate, (a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, p.personname, cb.bsstatus, cb.note,cb.bsamt, 'False' AS checkvalue FROM QL_trnbsmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.bsacctgoid INNER JOIN QL_mstperson p ON p.cmpcode=cb.cmpcode AND p.personoid=cb.pic WHERE cb.flag=0 AND cb.cmpcode='" & CompnyCode & "' "
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cb.cmpcode='" & Session("CompnyCode") & "'"
        'End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, cb.bsdate) DESC, cb.bsoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnbsmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub ShowReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("cashbankoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptReceipt.rpt"))
            Dim sWhere As String = "WHERE cashbankgroup='RECEIPT'"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " AND cb.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnCashBon.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND cb.cashbankoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(Report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ReceiptPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnCashBon.aspx?awal=true")
    End Sub

    Private Sub PrintR(Optional ByVal sType As String = "")
        Try
            If sType = "" Then
                report.Load(Server.MapPath(folderReport & "rptReceipt.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptReceiptXls.rpt"))
            End If
            'Dim sWhere As String = " WHERE cmpcode='CORP' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            'If cbCategory.Checked Then
            '    sWhere &= " AND acctggrp1='" & FilterDDLCategory.SelectedValue & "'"
            'End If
            'If cbStatus.Checked Then
            '    If FilterDDLStatus.SelectedValue <> "ALL" Then
            '        sWhere &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            '    End If
            'End If
            'If checkPagePermission("~\Master\mstAcctg.aspx", Session("SpecialAccess")) = False Then
            '    sWhere &= " AND createuser='" & Session("UserID") & "'"
            'End If
            Dim sWhere As String = ""
            Report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(Report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ReceiptReport")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ReceiptReport")
            End If
            Report.Close()
            Report.Dispose()
        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            'showMessage(ex.Message, 1)
        End Try
    End Sub

#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If outlet.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If cashbankdate.Text = "" Then
            sError &= "- Please fill RECEIPT DATE field!<BR>"
        Else
            If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- RECEIPT DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        Dim sErrReply As String = ""
        If Not isLengthAccepted("cashbankamt", "QL_trncashbankmst", ToDouble(cashbankamt.Text), sErrReply) Then
            sError &= "- TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If
        If cashbanktype.SelectedValue = "BBM" Then
            If cashbankduedate.Text = "" Then
                sError &= "- Please fill DUE DATE field!<BR>"
            Else
                If Not IsValidDate(cashbankduedate.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
                Else
                    If CDate(cashbankdate.Text) > CDate(cashbankduedate.Text) Then
                        sError &= "- DUE DATE must be more or equal than RECEIPT DATE<BR>"
                    End If
                End If
            End If
        End If
        If personoid.SelectedValue = "" Then
            sError &= "- Please select PIC field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            cashbankstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("~\Accounting\trnCashBon.aspx")
        End If
        Session("sCmpcode") = Request.QueryString("cmpcode")
        Session("oid") = Request.QueryString("oid")
        BtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        BtnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to post this data?');")
        Page.Title = CompnyName & " - Bon Sementara - "
        Page.Title = CompnyName & " - Bon Sementara"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        BtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        BtnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            CheckReceiptStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                filltextbox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                cashbankdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                cashbankstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                BtnDelete.Visible = False
                btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub lbkHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbkDetil_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub outlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLPerson()
        cashbanktype_SelectedIndexChanged(Nothing, Nothing)
        If i_u.Text = "New Data" Then
            GenerateReceiptNo()
            GenerateBSNo()
        End If
    End Sub

    Protected Sub cashbanktype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If cashbanktype.SelectedValue = "BKK" Then
            cashbankduedate.Text = ""
            EnableAddInfo(False)
            FillDDLAcctg(acctgoid, "VAR_CASH", outlet.SelectedValue)
        Else
            EnableAddInfo(True)
            FillDDLAcctg(acctgoid, "VAR_BANK", outlet.SelectedValue)
        End If
        If i_u.Text = "New Data" Then
            GenerateReceiptNo()
            GenerateBSNo()
        End If
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" & cashbankoid.Text
                If CheckDataExists(sSql) Then
                    cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                End If
                Dim sNo As String = cashbankno.Text
                GenerateReceiptNo()
                GenerateBSNo()
                bsmstoid.Text = GenerateID("QL_TRNBSMST", CompnyCode)
                If sNo <> cashbankno.Text Then
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            'Dim bsoid As String = GenerateID("QL_TRNBSMST", CompnyCode)
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim cashbankdtloid As Int64 = GenerateID("ql_cashbankgl", CompnyCode)
            Dim cRate As New ClassRate()
            periodacctg.Text = GetDateToPeriodAcctg(CDate(cashbankdate.Text))
            Dim objTrans As SqlClient.SqlTransaction
            If objConn.State = ConnectionState.Closed Then
                objConn.Open()
            End If
            objTrans = objConn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "insert into ql_trncashbankmst(cmpcode,cashbankoid,branch_code,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,createuser,createtime,upduser,updtime,cashbankcurroid,cashbankcurrate,pic,pic_refname,link_nobukti,deptoid,bankoid,postuser,posttime,status_giro,girodtloid,giroref,cashbankduedate,cashbankamount,cashbankamountidr,cashbankamountusd,cashbankrefno) values ('" & CompnyCode & "'," & cashbankoid.Text & ",'" & outlet.SelectedValue & "','" & cashbankno.Text & "','" & cashbankstatus.Text & "','" & cashbanktype.SelectedValue & "','BS', " & acctgoid.SelectedValue & ",'" & cashbankdate.Text & "','" & Tchar(cashbanknote.Text) & "','" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP," & cRate.GetRateMonthlyIDRValue & "," & cRate.GetRateMonthlyIDRValue & "," & personoid.SelectedValue & ", 'QL_MSTPERSON', '',1, 0,'" & Session("UserID") & "', CURRENT_TIMESTAMP,'',0,'','" & cashbankduedate.Text & "',0,0,0,'')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'Insertkan ke BS
                    sSql = "INSERT INTO QL_trnbsmst (cmpcode,branch_code,bsoid,BSNo,bsnomanual,bsdate,periodacctg,curroid,bsacctgoid,paytype,payacctgoid,payrefno,DueDate,pic,bsamt,bsamtIDR,bsamtUSD,note,cashbankoid,bsstatus,flag,createUser,createTime,updUser,updTime) VALUES ('" & CompnyCode & "','" & outlet.SelectedValue & "'," & bsmstoid.Text & ",'" & BSNo.Text & "','" & Tchar(bsnomanual.Text) & "','" & cashbankdate.Text & "','" & periodacctg.Text & "',0," & bsaccountoid.SelectedValue & ",'" & cashbanktype.SelectedItem.Text & "'," & acctgoid.SelectedValue & ",'" & Tchar(cashbankrefno.Text) & "','" & IIf(cashbanktype.SelectedValue = "BBM", cashbankduedate.Text, "1/1/1900") & "'," & personoid.SelectedValue & "," & ToDouble(cashbankamt.Text) & ", " & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyUSDValue & ",'" & Tchar(cashbanknote.Text) & "'," & cashbankoid.Text & ",'" & cashbankstatus.Text & "',0, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & bsmstoid.Text & " WHERE tablename='QL_TRNBSMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trncashbankmst SET cashbankno='" & cashbankno.Text & "', cashbankdate='" & cashbankdate.Text & "', cashbanktype='" & cashbanktype.SelectedValue & "', cashbankacctgoid=" & acctgoid.SelectedValue & ", cashbankamount=" & ToDouble(cashbankamt.Text) & ", cashbankamountidr=" & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyIDRValue & ", cashbankamountusd=" & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyUSDValue & ", pic=" & personoid.SelectedValue & ", cashbankduedate='" & IIf(cashbanktype.SelectedValue = "BBM", cashbankduedate.Text, "1/1/1900") & "', cashbankrefno='" & Tchar(cashbankrefno.Text) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', cashbankstatus='" & cashbankstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' and branch_code = '" & outlet.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnbsmst SET bsnomanual='" & Tchar(bsnomanual.Text) & "',bsdate='" & cashbankdate.Text & "',periodacctg='" & periodacctg.Text & "',bsacctgoid=" & bsaccountoid.SelectedValue & ",paytype='" & cashbanktype.SelectedValue & "',payacctgoid=" & acctgoid.SelectedValue & ",payrefno='" & Tchar(cashbankrefno.Text) & "',DueDate='" & IIf(cashbanktype.SelectedValue = "BBM", cashbankduedate.Text, "1/1/1900") & "',pic=" & personoid.SelectedValue & ",bsamt=" & ToDouble(cashbankamt.Text) & ",bsamtIDR=" & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyIDRValue & ",bsamtUSD=" & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyUSDValue & ",note='" & Tchar(cashbanknote.Text) & "',bsstatus='" & cashbankstatus.Text & "',updUser='" & Session("UserID") & "',updTime=CURRENT_TIMESTAMP WHERE bsoid=" & bsmstoid.Text & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "DELETE FROM ql_cashbankgl WHERE branch_code ='" & outlet.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                sSql = "insert into ql_cashbankgl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,cashbankglstatus,cashbankglres1,duedate,refno,createuser,createtime,upduser,updtime,branch_code) values ('" & CompnyCode & "'," & cashbankdtloid & ", " & cashbankoid.Text & ", " & acctgoid.SelectedValue & ", " & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyUSDValue & ",'','','" & cashbanktype.SelectedItem.Text & "','','" & Tchar(cashbankrefno.Text) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,'" & outlet.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankdtloid & " WHERE tablename='ql_cashbankgl' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If cashbankstatus.Text = "Post" Then
                    Dim sDate As String = cashbankdate.Text
                    If cashbanktype.SelectedValue = "BBM" Then
                        sDate = cashbankduedate.Text
                    End If
                    Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_TRNGLMST (cmpcode,glmstoid,branch_code,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime) values ('" & CompnyCode & "', " & iGlMstOid & ",'" & outlet.SelectedValue & "', '" & sDate & "', '" & sPeriod & "', 'BS|No=" & BSNo.Text & "', '" & cashbankstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'Insert Into GL Dtl
                    'Kas/Bank          1000 
                    '   Bon Sementara       1000
                    Dim iSeq As Integer = 1
                    sSql = "INSERT INTO QL_TRNGLDTL (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,upduser,updtime,createuser,createtime) VALUES ('" & CompnyCode & "','" & outlet.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & acctgoid.SelectedValue & ", 'D', " & ToDouble(cashbankamt.Text) & "," & ToDouble(cashbankamt.Text) * cRate.GetRateDailyIDRValue & "," & ToDouble(cashbankamt.Text) * cRate.GetRateDailyUSDValue & ",'" & BSNo.Text & "','" & Tchar(cashbanknote.Text) & "','','','" & cashbankstatus.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1

                    sSql = "INSERT INTO QL_TRNGLDTL (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,upduser,updtime,createuser,createtime) VALUES ('" & CompnyCode & "','" & outlet.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & bsaccountoid.SelectedValue & ", 'C', " & ToDouble(cashbankamt.Text) & "," & ToDouble(cashbankamt.Text) * cRate.GetRateDailyIDRValue & "," & ToDouble(cashbankamt.Text) * cRate.GetRateDailyUSDValue & ",'" & BSNo.Text & "','" & Tchar(cashbanknote.Text) & "','','','" & cashbankstatus.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = " update g set trnid = (select trnid from QL_trncashbankmst where cashbankoid = g.cashbankoid and branch_code = g.branch_code) from QL_cashbankgl g inner join QL_trncashbankmst cb on cb.branch_code=g.branch_code And cb.cashbankoid = g.cashbankoid And g.trnid = 0"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                cashbankstatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Cash/Bank No. have been regenerated because being used by another data. Your new Cash/Bank No. is " & cashbankno.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnCashBon.aspx?awal=true")
            End If
        End If
    End Sub

#End Region

    Protected Sub acctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If i_u.Text = "New Data" Then
            GenerateReceiptNo()
            GenerateBSNo()
        End If
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnCashBon.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND cb.bsdate>='" & FilterPeriod1.Text & " 00:00:00' AND cb.bsdate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND cb.bsstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnCashBon.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedValue()
        ShowReport()
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PrintR("Excel")
    End Sub

    Protected Sub btnUnpost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUnpost.Click
        If cashbankoid.Text = "" Then
            showMessage("Please select BS data first!", 2)
            Exit Sub
        End If
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            'UPDATE QL_trncashbankmst set cashbankstatus='In Process' where cashbankoid=5657

            'UPDATE QL_trnbsmst set bsstatus = 'In Process' where cashbankoid=5657

            'delete from QL_trnglmst where glmstoid=59021
            'delete from QL_trngldtl where glmstoid=59021
            sSql = "UPDATE QL_trncashbankmst set cashbankstatus='In Process' WHERE cmpcode='" & outlet.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnbsmst set bsstatus = 'In Process' WHERE cmpcode='" & outlet.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnglmst WHERE glmstoid IN (SELECT x.glmstoid FROM QL_trngldtl x WHERE x.glother1='QL_trnbsmst " & bsmstoid.Text & "')"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trngldtl WHERE glother1='QL_trnbsmst " & bsmstoid.Text & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnCashBon.aspx?awal=true")
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~\Accounting\trnCashBon.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cashbankstatus.Text = "Post"
        BtnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(BSNo.Text, outlet.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trnbsmst " & bsmstoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 1)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 1)
        End If
    End Sub
End Class
