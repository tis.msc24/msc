Imports System.Data.SqlClient
Imports System.Data
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms

Partial Class Master_saRole
    Inherits System.Web.UI.Page
    Dim cFunction As New ClassFunction
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)

    Dim xreader As SqlDataReader
    Dim xKon As New Koneksi
    Dim sSql As String = ""
    Dim dsData As New DataSet
    Dim dv As DataView
    Dim ckoneksi As New Koneksi
    Dim CProc As New ClassProcedure

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Sub AddRoleList()
        If Session("tbldtl") Is Nothing Then
            Dim dtlDS As DataSet = New DataSet
            Dim dtlTable As DataTable = New DataTable("tbldtl")
            dtlTable.Columns.Add("CMPCODE", Type.GetType("System.String"))
            dtlTable.Columns.Add("SAUSERID", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("PROFID", Type.GetType("System.String"))
            dtlTable.Columns.Add("USERPROF", Type.GetType("System.String"))
            dtlTable.Columns.Add("UPDUSER", Type.GetType("System.String"))
            dtlTable.Columns.Add("UPDTIME", Type.GetType("System.DateTime"))
            dtlTable.Columns.Add("ROLENAME", Type.GetType("System.String"))
            dtlTable.Columns.Add("SPECIAL", Type.GetType("System.String"))
            dtlDS.Tables.Add(dtlTable)
            Session("tbldtl") = dtlTable
        End If
        Dim objTable As DataTable
        objTable = Session("tbldtl")
        If I_U2_2.Text = "New Detail" Then
            dv = objTable.DefaultView

            dv.RowFilter = " PROFID = '" & userTargetoid.SelectedValue & "' "
            If dv.Count > 0 Then
                showMessage(" - Tidak dapat menambahkan data yang sama !", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
        End If

        Dim objRow As DataRow
        Dim sTempRow As String
        sTempRow = userID.SelectedValue
        If (I_U2_2.Text = "New Detail") Then
            objRow = objTable.NewRow()

        Else 'update
            objRow = objTable.Rows(GVRole2.SelectedIndex)
            objRow.BeginEdit()
        End If
        If sTempRow = "" Then
            showMessage(" - can't add row without valid user", 2)
            Exit Sub
        End If
        objRow("CMPCODE") = CompnyCode
        objRow("SAUSERID") = ClassFunction.GenerateID("QL_USERROLEOID", CompnyCode)
        objRow("PROFID") = userTargetoid.SelectedValue
        objRow("USERPROF") = sTempRow
        objRow("upduser") = Session("UserID")
        objRow("updtime") = FormatDateTime(Now(), DateFormat.GeneralDate)
        objRow("ROLENAME") = userTargetoid.SelectedItem.Text
        objRow("SPECIAL") = IIf(Special.Checked = True, "Yes", "No")
        If I_U2_2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
        End If
        Session("tbldtl") = objTable
        userTargetoid.SelectedIndex = 0
        Special.Checked = False
        GVRole2.Visible = True
        GVRole2.DataSource = dv
        GVRole2.DataBind()
        I_U2_2.Text = "New Detail"


    End Sub

    Sub removeList(ByVal sessionname As String, ByVal checkCol As Integer)
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session(sessionname)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = objRow.Length() - 1 To 0 Step -1
                If ckoneksi.getCheckBoxValue(i, checkCol, GVRole2) = True Then
                    objTable.Rows.Remove(objRow(i))
                End If
            Next
            Session(sessionname) = objTable
            GVRole2.DataSource = objTable
            GVRole2.DataBind()

        End If
    End Sub

    Protected Sub ibtn_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        AddRoleList()

    End Sub

    Sub insertingData(ByVal userprof As String, ByVal gvName As String)
        Dim OidTemp As Integer = ClassFunction.GenerateID("QL_SaUserRole", CompnyCode)
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldtl")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        For i As Integer = 0 To objRow.Length - 1
            With SDSData
                .InsertParameters("USERROLEOID").DefaultValue = OidTemp + i
                .InsertParameters("ROLEOID").DefaultValue = Session("tbldtl").rows(i).item("PROFID")
                .InsertParameters("USERPROF").DefaultValue = userprof 'CType(FrmViewRole.FindControl("USERPROF"), DropDownList).SelectedValue
                .InsertParameters("cmpcode").DefaultValue = CompnyCode
                .InsertParameters("UpdUser").DefaultValue = Session("UserID")
                .InsertParameters("UpdTime").DefaultValue = Now
                'If cKoneksi.getCheckBoxValue(i, 1, CType(FrmViewRole.FindControl(gvName), GridView)) = True Then
                .InsertParameters("SPECIAL").DefaultValue = objRow(i).Item("Special").ToString
                'Else
                '.InsertParameters("SPECIAL").DefaultValue = "No"
                'End If



            End With
            SDSData.Insert()
        Next
        With SDSOid
            .UpdateParameters("lastoid").DefaultValue = ClassFunction.GenerateID("QL_UserRole", CompnyCode) + objRow.Length - 1
            .UpdateParameters("tablename").DefaultValue = "QL_UserRole"
            .UpdateParameters("cmpcode").DefaultValue = CompnyCode
        End With
        SDSOid.Update()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect(Page.AppRelativeVirtualPath) '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Data Sales Activity Role"

        If Not IsPostBack Then
            initAllDll()
            FilterGV("", "", Session("UserID"))
            Session("TabelUserRole") = Nothing
            TabContainer1.ActiveTabIndex = 0
            UpdUser.Text = Session("userid")
            UpdTime.Text = Format(DateTime.Now(), "dd/MM/yyyy HH:mm")
        End If
    End Sub


    Private Sub initAllDll()
        sSql = "SELECT USERID, USERNAME FROM QL_MSTPROF P WHERE P.USERID NOT IN (SELECT s.USERPROF FROM QL_SaUserRole s)"
        FillDDL(userID, sSql)

        InituserTarget()
    End Sub



    Sub InituserTarget()
        If (lblID.Text = "") Then
            sSql = "SELECT USERID, USERID + ' - ' + USERNAME  USERNAME FROM QL_MSTPROF P WHERE USERID <> '" & userID.SelectedValue & "' "
        Else
            sSql = "SELECT USERID, USERID + ' - ' + USERNAME  USERNAME FROM QL_MSTPROF P WHERE USERID <> '" & lblID.Text & "'  AND (P.USERID NOT IN(SELECT  s.PROFID FROM QL_SaUserRole s WHERE s.USERPROF = '" & lblID.Text & "') )"
        End If
        FillDDL(userTargetoid, sSql)
    End Sub
    Protected Sub LBOid_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        TabContainer1.ActiveTabIndex = 1

        Dim lnkEdit As LinkButton = sender
        Dim row As GridViewRow = lnkEdit.NamingContainer
        Session.Item("kode") = lnkEdit.Text 'sender.CommandArgument
        With SDSData
            .SelectParameters("userprof").DefaultValue = Session.Item("kode")
            .SelectParameters("cmpcode").DefaultValue = CompnyCode
        End With
        Dim dtTemp As New DataTable
        Dim av As String = Session.Item("Kode")
        lblID.Text = Session.Item("Kode")
        initAllDll()
        lblNama.Text = GetStrData("SELECT USERNAME FROM QL_MSTPROF WHERE USERID = '" & lblID.Text & "'")
        userID.Visible = False
        lblNama.Visible = True
        ' Fill Table Bank

        sSql = "SELECT distinct [QL_SaUserRole].[CMPCODE], [QL_SaUserRole].[SAUSERID], [QL_SaUserRole].[PROFID], [QL_SaUserRole].[USERPROF], [QL_SaUserRole].[UPDUSER], [QL_SaUserRole].[UPDTIME],QL_MSTPROF.USERID  [ROLENAME], [QL_SaUserRole].[SPECIAL]         FROM [QL_SaUserRole] INNER JOIN QL_MSTPROF ON QL_MSTPROF.USERID=[QL_SaUserRole].PROFID WHERE (([QL_SaUserRole].[CMPCODE] = '" & CompnyCode & "') AND ([QL_SaUserRole].[USERPROF] = '" & Session.Item("Kode") & "'))"


        dtTemp = ckoneksi.ambiltabel(sSql, "SAUserRole")
        Session("tbldtl") = dtTemp

        If Session("tbldtl").rows.count > 0 Then
            GVRole2.DataSource = Session("tbldtl")
            GVRole2.Visible = True
            GVRole2.DataBind()
            BtnRemoveRole2.Visible = True
        Else
            BtnRemoveRole2.Visible = False
            GVRole2.Visible = False
            Session("tbldtl") = Nothing
        End If

        UpdUser.Text = dtTemp.Rows(0).Item("UPDUSER")
        UpdTime.Text = Format(CDate(dtTemp.Rows(0).Item("UPDTIME")), "dd/MM/yyyy HH:mm")

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/master/mstsaRole.aspx")
    End Sub

    Protected Sub BtnRemoveRole2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ibtn2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtn2.Click
        AddRoleList()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterText.Text = ""
        If ckoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("SpecialAccess")) = True Then

            FilterGV("", "", "")
        Else
            FilterGV("", "", Session("UserID"))

        End If
    End Sub
    Sub FilterGV(ByVal filterprof As String, ByVal filterName As String, ByVal filterUser As String)
        With SDSDataView
            .SelectParameters("USERPROF").DefaultValue = "%" & filterprof & "%"
            .SelectParameters("USERLONGNAME").DefaultValue = "%" & filterName & "%"
            .SelectParameters("upduser").DefaultValue = "%" & filterUser & "%"
            .SelectParameters("CmpCode").DefaultValue = CompnyCode
        End With
        GVmst.DataBind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Select Case FilterDDL.SelectedValue
            Case "Prof"
                FilterGV(FilterText.Text, "", Session("UserID"))
            Case "Name"
                FilterGV("", FilterText.Text, Session("UserID"))
        End Select
    End Sub

    Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~/master/mstsaRole.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click2(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~/master/mstsaRole.aspx?awal=true")
    End Sub

    Protected Sub BtnRemoveRole2_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If (GVRole2.Rows.Count <> 0) Then
            removeList("tbldtl", 1)
        End If
        InituserTarget()
    End Sub

    Protected Sub BtnRemoveRole_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        removeList("tbldtl", 1)
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If (GVRole2.Rows.Count = 0) Then
            sMsg &= " - Please fill User first ! <br />"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            Dim objtable As DataTable = Session("tbldtl")
            Dim mstID As Integer = cFunction.GenerateID("QL_SaUserRole", CompnyCode)
            If (lblNama.Text = "") Then

                For i As Integer = 0 To objtable.Rows.Count - 1
                    sSql = "INSERT INTO [QL_SaUserRole] ([CMPCODE], SAUSERID, PROFID, [USERPROF], [UPDUSER], [UPDTIME], [SPECIAL]) VALUES ('" & CompnyCode & "', '" & mstID & "','" & objtable.Rows(i).Item("profid") & "', '" & objtable.Rows(i).Item("userprof") & "', '" & Session("userid") & "', getdate(), '" & objtable.Rows(i).Item("special") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    mstID += 1
                Next

                

            Else

                sSql = "DELETE FROM [QL_SaUserRole] WHERE [CMPCODE] = '" & CompnyCode & "' AND USERPROF = '" & lblID.Text & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                For i As Integer = 0 To objtable.Rows.Count - 1
                    sSql = "INSERT INTO [QL_SaUserRole] ([CMPCODE], SAUSERID, PROFID, [USERPROF], [UPDUSER], [UPDTIME], [SPECIAL]) VALUES ('" & CompnyCode & "', '" & mstID & "','" & objtable.Rows(i).Item("profid") & "', '" & lblID.Text & "', '" & Session("userid") & "', getdate(), '" & objtable.Rows(i).Item("special") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    mstID += 1
                Next
            End If
            sSql = "update QL_mstoid set lastoid=" & mstID & " where tablename = 'QL_SaUserRole' " & _
                     "and cmpcode = '" & CompnyCode & "'"

            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, 2) : Exit Sub
        End Try
        Response.Redirect("~\Master\mstSARole.aspx?awal=true")
    End Sub

    Protected Sub userID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles userID.SelectedIndexChanged
        TabContainer1.ActiveTabIndex = 1
        InituserTarget()
    End Sub
End Class
