<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptNotaBeli.aspx.vb" Inherits="rptNotaBeli" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Purchase Invoice"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server" RenderMode="Inline">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=center><TABLE width="100%"><TBODY><TR><TD align=left><asp:Label style="WHITE-SPACE: nowrap" id="Label5" runat="server" Text="Tipe Nota"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="TypeNota" runat="server" Width="80px" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>Normal</asp:ListItem>
<asp:ListItem>Selisih</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label style="WHITE-SPACE: nowrap" id="Label2" runat="server" Text="Tipe Laporan"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="type" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label style="WHITE-SPACE: nowrap" id="Label1" runat="server" Text="Cabang"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="dCabangNya" runat="server" Width="150px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="55px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgBtnDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;- <asp:TextBox id="dateAkhir" runat="server" Width="55px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgBtnDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label></TD></TR><TR><TD align=left runat="server" Visible="true">Supplier</TD><TD id="Td3" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="suppname" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupplier" onclick="btnSearchSupplier_click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton> <asp:ImageButton id="btnEraseSupplier" onclick="btnEraseSupplier_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton><asp:Label id="suppoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="tdperiod3" align=left runat="server" Visible="true"></TD><TD id="tdperiod4" align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="gvSupp" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" CellPadding="4" UseAccessibleHeader="False" DataKeyNames="suppcode,suppoid,suppname,supptype" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" PageSize="8" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged1">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#804040" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supplier">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="supptype" HeaderText="Tipe" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left runat="server" Visible="true">No Nota PI</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="trnbelino" runat="server" Width="158px" CssClass="inpTextDisabled" Enabled="False" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="imbFindPI" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="26px"></asp:ImageButton> <asp:ImageButton id="imbErasePI" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top"></asp:ImageButton></TD></TR><TR><TD id="Td4" align=left runat="server" Visible="true"><asp:Label id="Label3" runat="server" Text="No. SJ" Visible="False"></asp:Label></TD><TD id="Td5" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="txtSjNo" runat="server" Width="200px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="crISjno" onclick="crISjno_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" Visible="False"></asp:ImageButton> <asp:ImageButton id="DelSjNo" onclick="DelSjNo_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" Visible="False"></asp:ImageButton> </TD></TR><TR><TD id="Td6" align=left runat="server" Visible="true"></TD><TD id="Td7" align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="GvSj" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" CellPadding="4" UseAccessibleHeader="False" DataKeyNames="trnsjbelino,trnbelipono,trnbelino" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" PageSize="8" OnSelectedIndexChanged="GvSj_SelectedIndexChanged" OnPageIndexChanging="GvSj_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#804040" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjbelino" HeaderText="No. LPB">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No. PI">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp. Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR id="tritem" runat="server" visible="false"><TD align=left><asp:Label style="WHITE-SPACE: nowrap" id="labelitem" runat="server" Text="Katalog"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="itemcode" runat="server" Width="178px" CssClass="inpTextDisabled" Enabled="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="26px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btntopdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR /><SPAN style="FONT-SIZE: 12pt">Sedang proses....</SPAN><BR /></SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="CEdateAwal" runat="server" PopupButtonID="ImgBtnDate1" TargetControlID="dateAwal" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CEdateAkhir" runat="server" PopupButtonID="ImgBtnDate2" TargetControlID="dateAkhir" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>&nbsp; <CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" AutoDataBind="true"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btntopdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upListPI" runat="server"><contenttemplate>
<asp:Panel id="PanelListPI" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListPI" runat="server" Font-Size="Large" Font-Bold="True" Text="List No. Purchase Invoice"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListPI" runat="server" Width="100%" DefaultButton="btnFindListPI"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListPI" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="trnbelino">No. PI</asp:ListItem>
<asp:ListItem Value="trnbelimstoid">Draft PI</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListPI" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListPI" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListPI" onclick="btnViewAllListPI_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnALLSelectPI" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNonePI" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedPI" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListPI" runat="server" Width="98%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMPI" runat="server" ToolTip='<%# eval("trnbelimstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnbelimstoid" HeaderText="Draft PI">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No. PI">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="suppname" HeaderText="Supplier">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="trnbelistatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListPI" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListPI" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListPI" runat="server" TargetControlID="btnHiddenListPI" BackgroundCssClass="modalBackground" PopupControlID="PanelListPI" PopupDragHandleControlID="lblListPI"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListPI" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
                <asp:UpdatePanel ID="upListMat" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Large" Font-Bold="True" Text="List Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="JenisNya">Jenis</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp; </TD></TR></TBODY></TABLE>Group : <asp:DropDownList id="DDLItemGroup" runat="server" Width="150px" CssClass="inpText"></asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnALLSelectItem" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneItem" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedItem" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat">
                                            <ProgressTemplate>
                                                <table style="width: 200px">
                                                    <tr>
                                                        <td style="font-weight: bold; font-size: 10pt; color: purple">
                                                            Load Data, Please Wait ...</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Image ID="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif" /></td>
                                                    </tr>
                                                </table>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" Width="100%" ForeColor="#333333" PageSize="5" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemcode" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                        <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                &nbsp;
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" Drag="True">
                            </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                            <table>
                                <tr>
                                    <td colspan="2" style="background-color: #cc0000; text-align: left">
                                        <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 10px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                            Width="24px" /></td>
                                    <td class="Label" style="text-align: left">
                                        <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 10px; text-align: center">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center">
                                        &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                            Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                            TargetControlID="bePopUpMsg">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </th>
        </tr>
    </table>
</asp:Content>

