'Prgmr :Shutup_M | LastUpdt:12-01-2016 (MM/dd/YYYY)
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.Globalization

Partial Class Transaction_ServicePlan
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Dim CProc As New ClassProcedure
    Dim folderReport As String = "~/Report/"
#End Region

#Region "Procedure"
    Private Sub InitCabang()
        Dim kCbg As String = ""
        If Session("UserID").ToString.ToUpper <> "ADMIN" Then
            kCbg &= "And g2.gencode='" & Session("branch_id").ToString & "'"
        End If
        sSql = "Select gencode ,g2.gendesc from ql_mstgen g2 where cmpcode = 'MSC' and gengroup = 'CABANG' " & kCbg & " ORDER BY g2.gencode"
        FillDDL(DdlCabang, sSql)
    End Sub

    Private Sub InitLoc()
        sSql = "SELECT a.genoid,ISNULL((SELECT gendesc FROM QL_mstgen g WHERE g.genoid=a.genother1 AND g.gengroup = 'WAREHOUSE'),'GUDANG PERJALANAN') +' - '+a.gendesc AS gendesc,* From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.gencode <> 'EXPEDISI' AND c.gencode='" & DdlCabang.SelectedValue & "'"
        FillDDL(matLoc, sSql)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            ImageButton3.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            ImageButton3.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 2 Then 'Warning
            ImageButton3.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        WARNING.Text = strCaption : lblValidasi.Text = sMessage
        CProc.SetModalPopUpExtender(btnExtender, PanelErrMsg, MPEErrMsg, True)
    End Sub

    Sub ClearDetail()
        lbIDseq.Text = "1"
        If Session("GVDtlServicePlan") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("GVDtlServicePlan")
            lbIDseq.Text = objTable.Rows.Count + 1
        End If
        lbStatus.Text = "New" : xxx.Text = ""
        txtJob.Text = "" : txtType.Text = ""
        txtNote.Text = "" : txtNamaSpart.Text = ""
        txtQty.Text = "" : txtHarga.Text = ""
        itservoid.Text = 0 : itservtarif.Text = ""
        mpartoid.Text = 0 : spartmoid.Text = ""
        btnClear.Visible = False : btnAddToListSPart.Visible = False
    End Sub

    Sub ClearDetail2()
        lbIDseq2.Text = "1"
        If Session("GVDtlServicePlan2") Is Nothing = False Then
            Dim objTable2 As DataTable
            objTable2 = Session("GVDtlServicePlan2")
            lbIDseq2.Text = objTable2.Rows.Count + 1
        End If
        lbStatus2.Text = "New"
        xxx.Text = "" : txtJob.Text = ""
        txtType.Text = "" : txtNote.Text = ""
        txtNamaSpart.Text = "" : txtQty.Text = ""
        txtHarga.Text = "" : itservoid.Text = 0
        itservtarif.Text = "" : mpartoid.Text = 0
        spartmoid.Text = "" : merkspr.Text = ""
        btnClear2.Visible = False : btnAddToListSPart2.Visible = False
        btnSearch2.Visible = True : btnErase2.Visible = True
        stock.Visible = False : ss.Visible = False
        saldoakhir.Visible = False : saldoakhir.Text = ""
    End Sub

    Public Sub GenerateGenID()
        txtPlanOid.Text = GenerateID("QL_trnservices", CompnyCode)
    End Sub

    Public Sub GenerateGenID2()
        lbDtlOid.Text = GenerateID("QL_trnservicesitem", CompnyCode)
    End Sub

    Public Sub GenerateGenID3()
        lbDtlOid2.Text = GenerateID("QL_trnservicespart", CompnyCode)
    End Sub

    Sub BindData(ByVal sWhere As String) 'GV List Awal
        sSql = "SELECT DISTINCT QLA.SOID, QLR.REQOID, QLR.BARCODE, ((ISNULL(CONVERT(VARCHAR(15),QLA.SSTARTTIME,103),'-'))+ ' ' +(ISNULL(CONVERT(VARCHAR(15),QLA.SSTARTTIME,108),'-'))) SSTARTTIME, QLRC.custname, QLR.REQITEMNAME, QLR.reqstatus SSTATUS, QLA.SFLAG FROM QL_TRNREQUEST QLR INNER JOIN QL_TRNMECHCHECK QLG ON QLG.MSTREQOID = QLR.REQOID INNER JOIN QL_TRNSERVICES QLA ON QLA.MSTREQOID = QLR.REQOID INNER JOIN QL_mstcust QLRC ON QLRC.custoid = QLR.REQCUSTOID WHERE QLR.cmpcode='" & CompnyCode & "' " & sWhere & " And QLR.reqstatus not in ('Post') ORDER BY QLA.SOID DESC"
        Session("TblMstplan") = cKoneksi.ambiltabel(sSql, "ChkServicePlan")
        GVListServicePlan.DataSource = Session("TblMstplan")
        GVListServicePlan.DataBind()
        GVListServicePlan.SelectedIndex = -1
    End Sub

    Sub BindDataInfo(ByVal sWhere As String) 'Binding Nota Beli
        sSql = "SELECT DISTINCT QLR.REQOID, QLR.BARCODE, QLR.REQCODE, QLR.REQDATE as tanggal, QLR.REQDATE as waktu, QLC.CUSTNAME, QLM.GENDESC, QLM2.GENDESC as merk, QLR.REQITEMNAME, QOP.CUSTNAME, QLR.REQCUSTOID, QLR.REQSERVICECAT, QLR.REQSTATUS, QLD.CHKFLAG FROM QL_TRNREQUEST QLR INNER JOIN QL_mstcust QLC ON QLR.REQCUSTOID=QLC.CUSTOID INNER JOIN QL_TRNREQUESTDTL QLD ON QLD.REQMSTOID=QLR.REQOID INNER JOIN QL_MSTGEN QLM ON QLR.REQITEMTYPE=QLM.GENOID INNER JOIN QL_MSTCUST QOP ON QOP.CUSTOID = QLR.REQCUSTOID INNER JOIN QL_MSTGEN QLM2 ON QLR.REQITEMBRAND=QLM2.GENOID WHERE QLR.cmpcode='" & CompnyCode & "' AND QLR.REQOID not in (Select mstreqoid from ql_trnservices Where branch_code='" & DdlCabang.SelectedValue & "') AND QLR.branch_code='" & DdlCabang.SelectedValue & "' AND QLR.REQSTATUS = 'Check' " & sWhere & " ORDER BY QLR.REQOID DESC"
        ' QLD.REQDTLJOB, 
        Dim xTableItem2 As DataTable = cKoneksi.ambiltabel(sSql, "Info")
        GVListInfo.DataSource = xTableItem2
        GVListInfo.DataBind()
        GVListInfo.SelectedIndex = -1
        InitLoc()
    End Sub

    Public Sub BindDataInfo3(ByVal sWhere As String)  'Binding Job dan type
        sSql = "SELECT DISTINCT A.ITSERVOID, A.ITSERVDESC, A.ITSERVTARIF, B.GENDESC ITSERVTYPE FROM QL_MSTITEMSERV A INNER JOIN QL_MSTGEN B ON B.GENOID = A.ITSERVOID WHERE A.CMPCODE = '" & CompnyCode & "' " & sWhere & ""
        Dim xTableItem3 As DataTable = cKoneksi.ambiltabel(sSql, "Info2")
        GVListInfo3.DataSource = xTableItem3
        GVListInfo3.DataBind()
        GVListInfo3.SelectedIndex = -1
    End Sub

    Public Sub BindDataInfo2(ByVal sWhere As String) 'Binding Sparepart
        sSql = "Select DISTINCT crd.branch_code,a.itemoid PARTOID,a.itemcode partcode,a.itemdesc partdescshort,a.HPP,isnull((select gendesc from QL_mstgen where genoid = a.itemgroupoid ),'') itemgroupcode,a.merk,a.updtime,Case When b.branch_code ='01' Then a.pricelist else ISNULL(a.pricelist + b.biayaExpedisi,0.00) end priceList,crd.saldoAkhir,periodacctg,a.satuan1,a.itemsafetystockunit1,mtrlocoid mtrloc From QL_crdmtr crd Inner Join ql_mstitem a ON a.itemoid=crd.refoid LEFT JOIN QL_mstItem_branch b ON a.itemcode=b.itemcode Where crd.branch_code=b.branch_code AND crd.mtrlocoid=" & matLoc.SelectedValue & " AND crd.periodacctg='" & GetDateToPeriodAcctg(CDate(GetServerTime())) & "' AND crd.cmpcode = '" & CompnyCode & "'" & sWhere & ""
        Dim xTableItem4 As DataTable = cKoneksi.ambiltabel(sSql, "Info3")
        GVListInfo2.DataSource = xTableItem4
        GVListInfo2.DataBind()
        GVListInfo2.SelectedIndex = -1
    End Sub

    Sub ClearInfo()
        txtNo.Text = "" : txtTglTarget.Text = ""
        txtWaktuTarget.Text = "" : txtNamaCust.Text = "" : txtAlamatCust.Text = ""
        txtHPCust.Text = "" : txtJenisBrg.Text = "" : lblno.Text = ""
        txtMerk.Text = "" : txtNamaBrg.Text = "" : txtKatKerusakan.Text = ""
        txtStatus.Text = "" : txtHargaServis.Text = "" : txtHargaJob.Text = ""
        txtHargaSPart.Text = "" : rbFlag.SelectedValue = "In" : txtOid.Text = ""
        teknisi.Text = "" : lblno.CssClass = "inpText" : lblno.Enabled = True
    End Sub

    Sub ClearInfo2()
        txtNamaSpart.Text = "" : mpartoid.Text = 0 : spartmoid.Text = ""
        txtQty.Text = "" : txtHarga.Text = "" : merkspr.Text = ""
        saldoakhir.Text = ""
    End Sub

    Sub ClearInfo3()
        txtJob.Text = "" : itservoid.Text = 0
        itservtarif.Text = "" : txtType.Text = ""
    End Sub

#End Region

#Region "Function"
    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("SEQ", Type.GetType("System.Int32"))
        dt.Columns.Add("ITSERVOID", Type.GetType("System.Int32"))
        dt.Columns.Add("ITSERVTARIF", Type.GetType("System.String"))
        dt.Columns.Add("ITSERVDESC", Type.GetType("System.String"))
        dt.Columns.Add("ITSERVTYPE", Type.GetType("System.String"))
        dt.Columns.Add("SPARTNOTE", Type.GetType("System.String"))
        Return dt
    End Function

    Private Function setTabelDetail2() As DataTable
        Dim dt2 As New DataTable
        dt2.Columns.Add("SPARTSEQ", Type.GetType("System.Int32"))
        dt2.Columns.Add("SPARTMOID", Type.GetType("System.Int32"))
        dt2.Columns.Add("MPARTOID", Type.GetType("System.Int32"))
        dt2.Columns.Add("ITSERVOID", Type.GetType("System.Int32"))
        dt2.Columns.Add("ITSERVDESC", Type.GetType("System.String"))
        dt2.Columns.Add("ITSERVTYPE", Type.GetType("System.String"))
        dt2.Columns.Add("SPARTM", Type.GetType("System.String"))
        dt2.Columns.Add("SPARTQTY", Type.GetType("System.String"))
        dt2.Columns.Add("SPARTPRICE", Type.GetType("System.String"))
        dt2.Columns.Add("saldoakhir", Type.GetType("System.Double"))
        Return dt2
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = CompnyName & " - Rencana Servis"
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        ' jangan lupa cek hak akses
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            Response.Redirect("ServicePlan.aspx")
        End If
        Session("idPage") = Request.QueryString("idPage")
        btnReady.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin mengubah status menjadi -Ready- ?');")
        btnBatal.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin mengubah status menjadi -Close- ?');")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk menghapus transaksi ini ?');")
        btncheckout.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin mengubah status menjadi -CheckOut- ?');")
        btnfinish.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin mengubah status menjadi -Finish- ?');")
        btnpaid.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin mengubah status menjadi -Paid- ?');")

        If Session("GVDtlServicePlan") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetail()
            Session("GVDtlServicePlan") = dtlTable
        End If

        If Session("GVDtlServicePlan2") Is Nothing Then
            Dim dtlTable2 As DataTable = setTabelDetail2()
            Session("GVDtlServicePlan2") = dtlTable2
        End If

        If Not IsPostBack Then
            txti_u.Text = "New"
            'initAllDDL()
            InitCabang()
            rbFlag.SelectedIndex = 0
            Dim sWhere = ""
            BindData(sWhere) : InitLoc()
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                txtPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
                txtPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
                txtPeriod1Info.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
                txtPeriod2Info.Text = Format(GetServerTime, "dd/MM/yyyy")
                TglTgl.Text = Format(GetServerTime, "dd/MM/yyyy")
                updUser.Text = Session("UserID")
                updTime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0
            ElseIf Session("idPage") <> Nothing And Session("idPage") <> "" Then
                txti_u.Text = "Update"
                FillTextBox(Session("idPage"))
                txtPeriod1Info.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
                txtPeriod2Info.Text = Format(GetServerTime, "dd/MM/yyyy")
                TglTgl.Text = Format(GetServerTime, "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 1
             
            End If
        End If
    End Sub

    Protected Sub lbDetSparepart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDetSparepart.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub lbInformasi2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInformasi2.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click

        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If cbPeriod.Checked Then
            Try
                Dim d As DateTime : Dim errMessage As String = ""
                If txtPeriod1.Text = "" Then
                    errMessage &= "- [Tolong isi tanggal awal !] ! <br>"
                End If

                If txtPeriod2.Text = "" Then
                    errMessage &= "- [Tolong isi tanggal akhir !] ! <br>"
                End If

                If Not DateTime.TryParseExact(txtPeriod1.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tangal salah ! gunakan format dd/MM/yyyy - MAX 31:12:2029] ! <br>"
                End If

                If Not DateTime.TryParseExact(txtPeriod2.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal salah ! gunakan format dd/MM/yyyy - MAX 31:12:2029] ! <br>"
                End If

                If errMessage <> "" Then
                    showMessage(errMessage, 2)
                    Exit Sub
                End If

                txtPeriod1.Enabled = True : txtPeriod2.Enabled = True
                Dim date1 As Date = toDate(txtPeriod1.Text)
                Dim date2 As Date = toDate(txtPeriod2.Text)
                If txtPeriod1.Text = "" And txtPeriod2.Text = "" Then
                    showMessage("Tolong isi periode  !", 2)
                End If
                If date1 < toDate("01/01/1900") Then
                    showMessage("Tanggal awal salah !", 2) : Exit Sub
                End If
                If date2 < toDate("01/01/1900") Then
                    showMessage("Tanggal akhir salah !", 2) : Exit Sub
                End If
                If date1 > date2 Then
                    showMessage("Period 1 harus lebih kecil dari Period 2 !", 2)
                    txtPeriod1.Text = "" : txtPeriod2.Text = ""
                    Exit Sub
                End If
                sWhere &= " AND CONVERT(CHAR(10),QLA.SSTARTTIME,101) BETWEEN '" & toDate(txtPeriod1.Text) & "' AND '" & toDate(txtPeriod2.Text) & "'"
            Catch ex As Exception
                showMessage("Tolong isi tanggal awal dan akhir !", 2)
            End Try
        End If
        If cbStatus.Checked Then
            If ddlStatus.SelectedValue.ToLower <> "all" Then
                sWhere &= " AND QLR.reqstatus = '" & ddlStatus.SelectedValue & "' "
            End If
        End If
        BindData(sWhere)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        txtFilter.Text = "" : ddlFilter.SelectedIndex = 0
        cbPeriod.Checked = False
        txtPeriod1.Text = "" : txtPeriod2.Text = ""
        cbStatus.Checked = False
        ddlStatus.SelectedIndex = 0
        Dim sWhere = " AND QLA.SSTATUS LIKE '%%'"
        BindData(sWhere)
    End Sub

    Sub FillTextInfo(ByVal Code As String) ' No transaksi for pilihan master fill text box
        sSql = "SELECT QLR.REQOID, QLR.BARCODE, QLR.REQCODE, QLR.REQDATE as tanggal, QLR.REQDATE as waktu, QLC.CUSTNAME, QLC.CUSTADDR, QLC.PHONE1, QLM.GENDESC, QLM2.GENDESC as merk, QLR.REQITEMNAME, QLR.REQSERVICECAT, QLR.REQSTATUS, QLR.REQFLAG ,isnull(t.PERSONNAME,'') teknisi,QLR.reqperson FROM QL_TRNREQUEST QLR INNER JOIN QL_mstcust QLC ON QLR.REQCUSTOID=QLC.CUSTOID INNER JOIN QL_MSTGEN QLM ON QLR.REQITEMTYPE=QLM.GENOID left join ql_mstperson t on QLR.reqperson = t.personoid INNER JOIN QL_MSTGEN QLM2 ON QLR.REQITEMBRAND=QLM2.GENOID WHERE QLR.cmpcode='" & CompnyCode & "' AND QLR.REQCODE='" & Tchar(Code) & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        Dim Tanggal As String = "" : Dim Waktu As String = ""
        If xReader.HasRows Then
            While xReader.Read
                txtOid.Text = xReader("REQOID")
                txtNo.Text = xReader("REQCODE").ToString.Trim
                txtTglTarget.Text = Format(GetServerTime(), "dd/MM/yyyy")
                'Format(xReader("tanggal"), "dd/MM/yyyy")
                lblTarget.Text = txtTglTarget.Text
                txtWaktuTarget.Text = Format(GetServerTime(), "HH:mm:ss")
                'Format(xReader("waktu"), "HH:mm:ss")                
                txtNamaCust.Text = xReader("CUSTNAME").ToString.Trim
                txtAlamatCust.Text = xReader("CUSTADDR").ToString.Trim
                txtHPCust.Text = xReader("PHONE1").ToString.Trim
                txtJenisBrg.Text = xReader("GENDESC").ToString.Trim
                txtMerk.Text = xReader("merk").ToString.Trim
                txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                txtKatKerusakan.Text = xReader("REQSERVICECAT").ToString.Trim
                txtStatus.Text = xReader("REQSTATUS").ToString.Trim
                teknisioid.Text = xReader("reqperson").ToString.Trim
                teknisi.Text = xReader("teknisi").ToString.Trim
                lblNo.Text = xReader("BARCODE").ToString.Trim
                'updUser.Text = xReader("UPDUSER").ToString.Trim
                'updTime.Text = Format(xReader("UPDTIME"), "dd/MM/yyyy HH:mm:ss")
                rbFlag.SelectedValue = xReader("REQFLAG").ToString
            End While
        End If

        xReader.Close() : conn.Close()
        lblno.CssClass = "inpTextDisabled" : lblno.Enabled = False
        GVDtlServicePlan.Visible = True : GVDtlServicePlan2.Visible = True

        sSql = "SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY A.MSTREQOID) as SEQ, AC.REQCODE, A.MSTREQOID, A.ITSERVOID, A1.ITSERVDESC, A2.GENDESC ITSERVTYPE, A1.ITSERVTARIF, A.NOTEDTL AS SPARTNOTE FROM QL_TRNMECHCHECK A INNER JOIN QL_TRNREQUEST AC ON AC.REQOID = A.MSTREQOID INNER JOIN QL_MSTITEMSERV A1 ON A1.ITSERVOID = A.ITSERVOID INNER JOIN QL_MSTGEN A2 ON A2.genoid = A1.ITSERVTYPEOID WHERE A.cmpcode='" & CompnyCode & "' AND AC.REQCODE = '" & Tchar(Code) & "' AND AC.REQSTATUS = 'Check'"

        Dim mysqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mysqlDA.Fill(objDs, "data2")
        GVDtlServicePlan.DataSource = objDs.Tables("data2")
        GVDtlServicePlan.DataBind()
        Session("GVDtlServicePlan") = objDs.Tables("data2")

        sSql = "SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY A.MSTREQOID) as SPARTSEQ, AC.REQCODE, ISNULL(B.MSTPARTOID,0) SPARTMOID, ISNULL(B.MPARTOID,0) MPARTOID, A.MSTREQOID, A.ITSERVOID, ISNULL(B.MRCHECKOID,0) MRCHECKOID, A1.ITSERVDESC, A2.GENDESC ITSERVTYPE, ISNULL(B1.itemdesc,'-') SPARTM,b1.merk, ISNULL(B.MPARTQTY,0) SPARTQTY, ISNULL(B.MPARTPRICE,0) SPARTPRICE,b.mtrloc,(Select gendesc From QL_mstgen g Where g.genoid=b.mtrloc AND gengroup='LOCATION') LocDesc, (select isnull(saldoAkhir,'') from QL_crdmtr cok Where refoid = b.MSTPARTOID and branch_code = b.BRANCH_CODE and periodacctg ='" & GetPeriodAcctg(GetServerTime()) & "' AND b.mtrloc=cok.mtrlocoid) saldoakhir FROM QL_TRNMECHCHECK A INNER JOIN QL_TRNREQUEST AC ON AC.REQOID = A.MSTREQOID LEFT JOIN QL_TRNMECHPARTDTL B ON B.MRCHECKOID = A.MCHECKOID INNER JOIN QL_MSTITEMSERV A1 ON A1.ITSERVOID = A.ITSERVOID INNER JOIN QL_MSTGEN A2 ON A2.genoid = A1.ITSERVTYPEOID INNER JOIN ql_mstitem B1 ON B1.itemOID = B.MSTPARTOID WHERE A.cmpcode='" & CompnyCode & "' AND AC.REQCODE = '" & Tchar(Code) & "' AND AC.REQSTATUS = 'Check'"

        Dim mysqlDA2 As New SqlDataAdapter(sSql, conn)
        Dim objDs2 As New DataSet
        mysqlDA2.Fill(objDs2, "data3")
        GVDtlServicePlan2.DataSource = objDs2.Tables("data3")
        GVDtlServicePlan2.DataBind()
        Session("GVDtlServicePlan2") = objDs2.Tables("data3")
    End Sub

    Sub FillTextInfo2(ByVal Code As String) ' Fill text box Job dan type pada detail
        sSql = "SELECT A.ITSERVOID, A.ITSERVDESC, A.ITSERVTARIF, B.GENDESC ITSERVTYPE FROM QL_MSTITEMSERV A INNER JOIN QL_MSTGEN B ON B.GENOID = A.ITSERVTYPEOID WHERE A.CMPCODE = '" & CompnyCode & "' AND A.ITSERVOID = " & Code & " "
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                txtJob.Text = xReader("ITSERVDESC").ToString.Trim
                txtType.Text = xReader("ITSERVTYPE").ToString.Trim
                itservoid.Text = xReader("ITSERVOID").ToString.Trim
                itservtarif.Text = ToDouble(xReader("ITSERVTARIF"))
            End While
        End If
        xReader.Close()
        conn.Close()
    End Sub

    Sub FillTextInfo3(ByVal Code1 As String, ByVal Code2 As String) ' Fill text box Nama Sparepart, Qty sparepart dan Price sparepart pada detail
        sSql = "SELECT A.MPARTOID, A.MSTPARTOID, B.itemdesc PARTDESCSHORT, A.MPARTQTY FROM QL_TRNMECHPARTDTL A INNER JOIN ql_mstitem B ON A.MSTPARTOID = B.itemoid WHERE A.CMPCODE = '" & CompnyCode & "' AND A.MPARTOID='" & Code1 & "' AND A.MSTPARTOID='" & Code2 & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                txtNamaSpart.Text = xReader("PARTDESCSHORT").ToString.Trim
                txtQty.Text = ToDouble(xReader("MPARTQTY").ToString.Trim)
                txtHarga.Text = 0
                mpartoid.Text = xReader("MPARTOID")
                spartmoid.Text = xReader("MSTPARTOID").ToString.Trim
            End While
        End If
    End Sub

    Sub FillTextBox(ByVal idPage As Integer)
        sSql = "SELECT DISTINCT QLR.branch_code,QLA.SOID,qla.sperson, QLA.SSTATUS, QLA.STOTALPRICE, QLA.MSTREQOID, QLA.SSTARTTIME, QLA.SENDTIME, QLR.REQOID, QLR.REQCODE, QLR.BARCODE, QLR.REQDATE , QLC.CUSTNAME, QLC.CUSTADDR, QLC.PHONE1, QLM.GENDESC, QLM2.GENDESC AS merk, QLR.REQITEMNAME, QLR.REQSERVICECAT, QLR.REQSTATUS, QLA.SFLAG, QLa.UPDTIME, QLa.UPDUSER,isnull(t.PERSONNAME,'') teknisi FROM QL_TRNSERVICES QLA INNER JOIN QL_TRNREQUEST QLR ON QLR.REQOID=QLA.MSTREQOID INNER JOIN QL_mstcust QLC ON QLR.REQCUSTOID=QLC.CUSTOID left join ql_mstperson t on QLA.sperson = t.personoid INNER JOIN QL_MSTGEN QLM ON QLR.REQITEMTYPE=QLM.GENOID INNER JOIN QL_MSTGEN QLM2 ON QLR.REQITEMBRAND=QLM2.GENOID WHERE QLR.cmpcode='" & CompnyCode & "' AND QLA.SOID=" & idPage & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                txtPlanOid.Text = idPage
                txtOid.Text = xReader("MSTREQOID")
                DdlCabang.SelectedValue = xReader("branch_code").ToString.Trim
                txtNo.Text = xReader("REQCODE").ToString.Trim
                txtTglTarget.Text = Format(xReader("SSTARTTIME"), "dd/MM/yyyy")
                lblTarget.Text = txtTglTarget.Text
                txtWaktuTarget.Text = Format(xReader("SSTARTTIME"), "HH:mm:sss")
                txtNamaCust.Text = xReader("CUSTNAME").ToString.Trim
                txtAlamatCust.Text = xReader("CUSTADDR").ToString.Trim
                txtHPCust.Text = xReader("PHONE1").ToString.Trim
                txtJenisBrg.Text = xReader("GENDESC").ToString.Trim
                txtMerk.Text = xReader("merk").ToString.Trim
                txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                txtKatKerusakan.Text = xReader("REQSERVICECAT").ToString.Trim
                txtStatus.Text = xReader("REQSTATUS").ToString.Trim
                lblNo.Text = xReader("BARCODE").ToString.Trim
                txtHargaServis.Text = xReader("STOTALPRICE").ToString.Trim
                updUser.Text = xReader("UPDUSER").ToString.Trim
                updTime.Text = Format(xReader("UPDTIME"), "dd/MM/yyyy HH:mm:ss")
                rbFlag.SelectedValue = xReader("SFLAG").ToString
                teknisioid.Text = xReader("sperson").ToString.Trim
                teknisi.Text = xReader("teknisi").ToString.Trim
                InitLoc()
            End While
        End If
        xReader.Close()
        conn.Close()
        lblno.CssClass = "inpTextDisabled"
        lblno.Enabled = False
        GVDtlServicePlan.Visible = True
        GVDtlServicePlan2.Visible = True

        sSql = "SELECT DISTINCT QLJ.ITSERVOID ,QLJ.ITSERVDESC, QLZ.GENDESC ITSERVTYPE, QLAA.SPARTNOTE, QLAA.SEQ, QLAA.ITSERVTARIF FROM QL_TRNREQUEST QLR  INNER JOIN QL_TRNSERVICES QLA ON QLA.MSTREQOID = QLR.REQOID INNER JOIN QL_TRNSERVICESITEM QLAA ON QLAA.SOID = QLA.SOID INNER JOIN QL_MSTITEMSERV QLJ ON QLAA.ITSERVOID = QLJ.ITSERVOID INNER JOIN QL_MSTGEN QLZ ON QLZ.GENOID = QLJ.ITSERVTYPEOID WHERE QLR.cmpcode='" & CompnyCode & "' AND QLAA.SOID = " & idPage & ""
        Dim mysqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mysqlDA.Fill(objDs, "data2")
        GVDtlServicePlan.DataSource = objDs.Tables("data2")
        GVDtlServicePlan.DataBind()
        Session("GVDtlServicePlan") = objDs.Tables("data2")

        sSql = "SELECT DISTINCT QLJ.ITSERVOID ,QLJ.ITSERVDESC, QLZ.GENDESC ITSERVTYPE, QLAA.SPARTMOID, QLAA.SPARTOID, ISNULL(QLQ.itemoid,0) PARTOID, ISNULL(QLQ.itemdesc,'-') SPARTM,qlq.merk, QLAA.SPARTSEQ, QLAA.SPARTQTY, QLAA.MPARTOID, QLAA.SPARTPRICE,QLAA.mtrlocoid mtrloc,Isnull((Select saldoAkhir From QL_crdmtr crd Where crd.mtrlocoid=QLAA.mtrlocoid And crd.branch_code=QLR.branch_code And crd.refoid=qlq.itemoid AND crd.periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'),0.00) saldoakhir FROM QL_TRNREQUEST QLR INNER JOIN QL_TRNSERVICES QLA ON QLA.MSTREQOID = QLR.REQOID INNER JOIN QL_TRNSERVICESPART QLAA ON QLAA.SOID = QLA.SOID LEFT JOIN QL_mstitem QLQ ON QLQ.itemoid = QLAA.SPARTMOID INNER JOIN QL_MSTITEMSERV QLJ ON QLAA.ITSERVOID = QLJ.ITSERVOID INNER JOIN QL_MSTGEN QLZ ON QLZ.GENOID = QLJ.ITSERVTYPEOID WHERE QLR.cmpcode='" & CompnyCode & "' AND QLAA.SOID = " & idPage & ""

        Dim mysqlDA2 As New SqlDataAdapter(sSql, conn)
        Dim objDs2 As New DataSet
        mysqlDA2.Fill(objDs2, "data3")
        GVDtlServicePlan2.DataSource = objDs2.Tables("data3")
        GVDtlServicePlan2.DataBind()
        Session("GVDtlServicePlan2") = objDs2.Tables("data3")

        If txtStatus.Text = "Check" Then
            rbFlag.Enabled = True : btnBatal.Visible = True
            If rbFlag.SelectedValue.ToLower = "in" Then
                btnReady.Visible = True : btncheckout.Visible = False
            Else
                btnReady.Visible = False : btncheckout.Visible = True
            End If
            btnDelete.Visible = False
        ElseIf txtStatus.Text = "Ready" Then
            rbFlag.Enabled = False : btnReady.Visible = False
            btnBatal.Visible = False : btnSave.Visible = False
            btnDelete.Visible = False
        ElseIf txtStatus.Text = "Send" Then
            rbFlag.Enabled = False : btnReady.Visible = False
            btnBatal.Visible = True : btnSave.Visible = False
            btnDelete.Visible = False : btnfinish.Visible = True
        ElseIf txtStatus.Text = "Close" Then
            rbFlag.Enabled = False
            btnBatal.Visible = False : btnReady.Visible = False
            btnSave.Visible = False : btnDelete.Visible = False
            btnfinish.Visible = False : btnpaid.Visible = True
        Else
            rbFlag.Enabled = False : btnBatal.Visible = False
            btnReady.Visible = False : btnSave.Visible = False
            btnDelete.Visible = False : btnfinish.Visible = False
        End If
    End Sub

    Protected Sub GVListServicePlan_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVListServicePlan.PageIndexChanging
        GVListServicePlan.PageIndex = e.NewPageIndex
        'Dim sWhere = " AND QLA.SSTATUS LIKE '%%'"
        'BindData(sWhere)
        GVListServicePlan.DataSource = Session("TblMstplan")
        GVListServicePlan.DataBind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        PanelInfo.Visible = True : btnHiddenInfo.Visible = True
        MPEInfo.Show() : Dim sWhere As String = "" : sWhere = ""
        BindDataInfo(sWhere)
    End Sub

    Protected Sub GVListInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVListInfo.PageIndexChanging
        GVListInfo.PageIndex = e.NewPageIndex
        Dim sWhere As String = "" : sWhere = " AND QLR.REQSTATUS = 'Check' "
        BindDataInfo(sWhere) : MPEInfo.Show()
    End Sub

    Protected Sub GVListInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVListInfo.SelectedIndexChanged
        Dim sCode As String = ""
        btnHiddenInfo.Visible = False : PanelInfo.Visible = False
        MPEInfo.Hide() : sCode = GVListInfo.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextInfo(sCode)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        TabContainer1.ActiveTabIndex = 0
        Response.Redirect("~\Transaction\ServicePlan.aspx?page=true")
    End Sub

    Protected Sub lbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbClose.Click, lbClose2.Click, lbClose3.Click
        btnHiddenInfo.Visible = False : PanelInfo.Visible = False
        btnHiddenInfo2.Visible = False : PanelInfo2.Visible = False
        btnHiddenInfo3.Visible = False : PanelInfo3.Visible = False
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErase.Click
        ClearInfo()
        ClearDetail()
        ClearDetail2()
        Session.Remove("GVDtlServicePlan")
        Session.Remove("GVDtlServicePlan2")
        GVDtlServicePlan.Visible = False
        GVDtlServicePlan2.Visible = False
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToListSPart.Click

    End Sub

    Protected Sub GVDtlServicePlan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlServicePlan.RowDataBound
        Dim total As Double = 0 : Dim temp As Double = 0
        For Each gvr As GridViewRow In GVDtlServicePlan.Rows
            temp = Convert.ToDouble(gvr.Cells(5).Text)
            total += temp
        Next
        'txtHargaServis.Text = Decimal.Parse(total.ToString()).ToString("N")
        txtHargaJob.Text = ToDouble(total)
        txtHargaServis.Text = ToDouble(txtHargaJob.Text) + ToDouble(txtHargaSPart.Text)
        btnAddToListSPart.Visible = False
        btnClear.Visible = False

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble((e.Row.Cells(5).Text)), 3)
        End If
    End Sub

    Protected Sub GVDtlServicePlan_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlServicePlan.RowDeleting
        Dim iIndex As Int16 = e.RowIndex
        Dim objTable As DataTable = Session("GVDtlServicePlan")
        objTable.Rows.RemoveAt(iIndex)
        Session("GVDtlServicePlan") = objTable
        GVDtlServicePlan.Visible = True
        GVDtlServicePlan.DataSource = objTable
        GVDtlServicePlan.DataBind()
        ClearDetail()
    End Sub

    Protected Sub GVDtlServicePlan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtlServicePlan.SelectedIndexChanged
        Try
            Session("index") = GVDtlServicePlan.SelectedIndex
            If Session("GVDtlServicePlan") Is Nothing = False Then
                lbIDseq.Text = GVDtlServicePlan.SelectedDataKey(0).ToString().Trim
                lbStatus.Text = "Update"
                xxx.Text = "A"
                Dim objTable As DataTable
                objTable = Session("GVDtlServicePlan")
                Dim dv As DataView = objTable.DefaultView
                'Dim idx As Integer = Session("GVDtlServicePlan")
                dv.RowFilter = "itservoid='" & GVDtlServicePlan.SelectedDataKey(1).ToString().Trim & "'"

                'Insert / Update List data
                'lbIDseq.Text = dv.Item(0).Row.Item("spartseq").ToString
                'lbDtlOid.Text = dv.Item(0).Row.Item("spartoid").ToString
                itservoid.Text = dv.Item(0).Row.Item("itservoid").ToString
                itservtarif.Text = Decimal.Parse(dv.Item(0).Row.Item("itservtarif").ToString.Trim).ToString("N")
                txtJob.Text = dv.Item(0).Row.Item("itservdesc").ToString

                If txtStatus.Text.ToLower <> "close" Then
                    If txtJob.Text.ToLower <> "other" Then
                        showMessage("Tidak bisa edit selain Pengerjaan Other atau biaya pengecekan dari Service Batal", 2)
                        ClearDetail()
                        Exit Sub
                    End If
                End If

                txtType.Text = dv.Item(0).Row.Item("itservtype").ToString
                txtNote.Text = dv.Item(0).Row.Item("spartnote").ToString
                dv.RowFilter = ""
                btnAddToListSPart.Visible = True
                btnClear.Visible = True
                'If txtNamaSpart.Text = "-" Then
                '    txtQty.Enabled = False
                '    txtHarga.Enabled = False
                'Else
                '    txtQty.Enabled = True
                '    txtHarga.Enabled = True
                'End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Protected Sub lbAddSparepart_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        CProc.SetModalPopUpExtender(btnExtender, PanelErrMsg, MPEErrMsg, False)
        btnExtender.Visible = False : PanelErrMsg.Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click    
        Dim errMessage As String = "" : Dim d As DateTime

        If lblno.Text = "" Then
            errMessage &= "- Barcode harus diisi !<br>"
        End If
        If txtNo.Text = "" Then
            errMessage &= "- No. Tanda Terima harus diisi !<br>"
        End If
        If txtTglTarget.Text = "" Then
            errMessage &= "- Target selesai harus diisi !<br>"
        End If
        If teknisi.Text = "" Then
            errMessage &= "- Teknisi harus diisi !<br>"
        End If
        If txtWaktuTarget.Text = "" Then
            errMessage &= "- Waktu target harus diisi !<br>"
        End If        
        Try
            If txtTglTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal salah ! Gunakan format dd/MM/yyyy - MAX 31:12:2029]! <br>"
                End If            
            End If
            If DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                If txtTglTarget.Text <> "" Then
                    If (Session("idPage") Is Nothing Or Session("idPage") = "") And CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    ElseIf CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    End If
                End If
            End If

            If txtWaktuTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtWaktuTarget.Text, "HH:mm:ss", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format waktu salah ! Gunakan format HH:mm:ss - MAX 24:60:60]! <br>"
                ElseIf (Session("idPage") Is Nothing Or Session("idPage") = "") And txtTglTarget.Text = lblTarget.Text And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                    errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                Else
                    If (txtTglTarget.Text = TglTgl.Text) And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                        errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                    End If
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_trnservices WHERE mstreqoid = '" & Tchar(txtOid.Text) & "' "
        If Session("idPage") = Nothing Or Session("idPage") = "" Then
            If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
                errMessage &= "- No Tanda Terima telah diproses sebelumnya ! <BR>"
            End If
        End If
        
        'If txtOid.Text = "" Then
        '    errMessage &= "- Tolong pilih No Tanda Terima ! <BR>"
        'End If    
        'check if there is any data in GV Dtl Service Plan
        Dim objTableCek As DataTable = Session("GVDtlServicePlan")
        'If objTableCek.Rows.Count = 0 Then
        '    errMessage &= "- [Tidak ada detail data, Tolong cek form 'Check Teknisi' !]! <br>"
        'End If        
        For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
            If ToDouble(objTableCek.Rows(c1).Item("ITSERVTARIF")) <= 0 Then
                errMessage &= "- [Harga Tarif Pengerjaan harus lebih besar dari 0 !] <br>!"
            End If
        Next

        Dim objTableCek2 As DataTable = Session("GVDtlServicePlan2")
        For c2 As Int16 = 0 To objTableCek2.Rows.Count - 1
            If objTableCek2.Rows(c2).RowState <> DataRowState.Deleted Then
                If Double.Parse(objTableCek2.Rows(c2).Item("SPARTQTY")) <> 0.0 Then
                    If Double.Parse(objTableCek2.Rows(c2).Item("SPARTPRICE")) <= 0.0 Then
                        errMessage &= "- [Harga sparepart harus lebih besar dari 0 !]! <br>"
                    End If
                Else
                    errMessage &= "- [Quantity sparepart harus lebih besar dari 0 !]! <br>"
                End If
            End If
        Next
        If txtHargaServis.Text <> "" Then
            If ToDouble(txtHargaServis.Text) <= 0 Then
                errMessage &= "- [Harga Servis harus lebih besar dari 0 !]! <br>"
            End If
        End If

        If errMessage <> "" Then
            showMessage(errMessage, 2)
            Exit Sub
        End If

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            txtPlanOid.Text = GenerateID("QL_trnservices", CompnyCode)
            '  txtStatus.Text = "In Process"
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

            Dim wktjam As String = txtTglTarget.Text & " " & txtWaktuTarget.Text
            'mode insert
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                sSql = "INSERT into QL_trnservices (" & _
                "CMPCODE, " & _
                "BRANCH_CODE," & _
                "SOID, " & _
                "MSTREQOID, " & _
                "SSTARTTIME, " & _
                "SENDTIME, " & _
                "STOTALPRICE, " & _
                "SSTATUS, " & _
                "SFLAG, " & _
                "CREATEUSER, " & _
                "CREATETIME, " & _
                "UPDUSER, " & _
                "UPDTIME, " & _
                "SPERSON) " & _
                "VALUES (" & _
                "'" & CompnyCode & "'," & _
                "'" & DdlCabang.SelectedValue & "'," & _
                " " & txtPlanOid.Text & "," & _
                " " & txtOid.Text & "," & _
                "'" & toDate(wktjam) & "' ," & _
                "'" & toDate(wktjam) & "' ," & _
                " " & ToDouble(txtHargaServis.Text) & "," & _
                "'" & txtStatus.Text & "'," & _
                "'" & rbFlag.SelectedValue & "'," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp" & " ," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp," & _
                " " & teknisioid.Text & ") "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & txtPlanOid.Text & " where tablename = 'QL_trnservices' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ElseIf txti_u.Text = "Update" Then
                'mode update

                'delete QL_trnservicesitem / detail
                sSql = "Delete from QL_trnservicesitem where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnservicespart / detail
                sSql = "Delete from QL_trnservicespart where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnservices SET " & _
                "SOID=" & txtPlanOid.Text & ", " & _
                "MSTREQOID=" & txtOid.Text & ", " & _
                "SSTARTTIME='" & toDate(wktjam) & "', " & _
                "SENDTIME='" & toDate(wktjam) & "', " & _
                "STOTALPRICE=" & ToDouble(txtHargaServis.Text) & ", " & _
                "SSTATUS='" & txtStatus.Text.Trim & "', " & _
                "SFLAG='" & rbFlag.SelectedValue & "', " & _
                "UPDUSER='" & updUser.Text & "', " & _
                "UPDTIME=" & "current_timestamp" & ", " & _
                "SPERSON=" & teknisioid.Text & "" & _
                " WHERE cmpcode='" & CompnyCode & "' and SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'insert tabel QL_trnservicesitem / detail
            Dim objTable As New DataTable
            Dim oidCounter As Integer = 0
            objTable = Session("GVDtlServicePlan")
            'GenerateID("QL_trnservicesitem", CompnyCode)
            'GenerateGenID2()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter = CInt(lbDtlOid.Text)
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT into QL_trnservicesitem (CMPCODE,BRANCH_CODE,SITEMOID,SOID,SEQ,SPARTNOTE,ITSERVOID,ITSERVTARIF,CREATEUSER,CREATETIME,UPDUSER,UPDTIME) " & _
                "VALUES (" & _
                "'" & CompnyCode & "'," & _
                "'" & DdlCabang.SelectedValue & "'," & _
                " " & oidCounter & "," & _
                " " & txtPlanOid.Text & "," & _
                " " & objTable.Rows(c1).Item("SEQ") & "," & _
                "'" & Tchar(objTable.Rows(c1).Item("SPARTNOTE")) & "'," & _
                " " & objTable.Rows(c1).Item("ITSERVOID") & "," & _
                " " & ToDouble(objTable.Rows(c1).Item("ITSERVTARIF")) & "," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp" & " ," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                oidCounter += 1
            Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter - 1 & " where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'insert tabel QL_trnservicespart / detail
            Dim objTable2 As New DataTable
            Dim oidCounter2 As Integer = 0
            objTable2 = Session("GVDtlServicePlan2")
            'GenerateGenID3()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid2.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter2 = CInt(lbDtlOid2.Text)
            For c2 As Int16 = 0 To objTable2.Rows.Count - 1
                If Not objTable2.Rows(c2).RowState = DataRowState.Deleted Then
                    sSql = "INSERT into QL_trnservicespart (CMPCODE,BRANCH_CODE,SPARTOID,SOID,SPARTSEQ,SPARTMOID,SPARTQTY,SPARTPRICE,ITSERVOID,MPARTOID,CREATEUSER,CREATETIME,UPDUSER,UPDTIME,mtrlocoid) " & _
                    "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & oidCounter2 & "," & txtPlanOid.Text & "," & objTable2.Rows(c2).Item("SPARTSEQ") & "," & objTable2.Rows(c2).Item("SPARTMOID") & "," & ToDouble(objTable2.Rows(c2).Item("SPARTQTY")) & "," & ToDouble(objTable2.Rows(c2).Item("SPARTPRICE")) & "," & objTable2.Rows(c2).Item("ITSERVOID") & "," & objTable2.Rows(c2).Item("MPARTOID") & ",'" & updUser.Text & "'," & "current_timestamp" & " ,'" & updUser.Text & "',current_timestamp," & objTable2.Rows(c2).Item("mtrloc") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    oidCounter2 += 1
                End If
            Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter2 - 1 & " where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try

        Session.Remove("idPage")
        Response.Redirect("~\transaction\ServicePlan.aspx?awal=true")
    End Sub

    Protected Sub btnAddToListSPart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim errMessage As String = ""

        If txtNote.Text.Length >= 70 Then
            errMessage &= "- Maksimal karakter untuk keterangan adalah 70 karakter ! <BR>"
        End If

        If errMessage <> "" Then
            showMessage(errMessage, 2)
            Exit Sub
        End If

        If Session("GVDtlServicePlan") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetail()
            Session("GVDtlServicePlan") = dtlTable
        End If

        

        Dim objTable As DataTable
        objTable = Session("GVDtlServicePlan")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = ""
        Try
            If xxx.Text = "A" Then
                'Insert / Update Data
                Dim objRow As DataRow
                If lbStatus.Text = "New" Then
                    objRow = objTable.NewRow()
                    objRow("SEQ") = objTable.Rows.Count + 1 'Session("DTLOID1")
                    Session("SEQ") += 1
                Else
                    objRow = objTable.Rows(Session("index"))
                    objRow.BeginEdit()
                End If
                objRow("ITSERVOID") = itservoid.Text
                objRow("ITSERVTARIF") = ToDouble(itservtarif.Text)
                objRow("ITSERVDESC") = txtJob.Text
                objRow("ITSERVTYPE") = txtType.Text
                objRow("SPARTNOTE") = Tchar(txtNote.Text.Trim)
                If lbStatus.Text = "New" Then
                    objTable.Rows.Add(objRow)
                Else
                    objRow.EndEdit()
                End If
                ClearDetail()
                Session("GVDtlServicePlan") = objTable
                GVDtlServicePlan.Visible = True
                GVDtlServicePlan.DataSource = Nothing
                GVDtlServicePlan.DataSource = objTable
                GVDtlServicePlan.DataBind()
                lbIDseq.Text = objTable.Rows.Count + 1
                GVDtlServicePlan.SelectedIndex = -1
            End If

        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
        GVDtlServicePlan.SelectedIndex = -1
    End Sub

    Protected Sub printGV_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ' Dim temp As String = sender.commandargument.ToString
        '  PrintReport(temp.Remove(temp.IndexOf(";")), temp.Substring(temp.IndexOf(";") + 1))
    End Sub

    Protected Sub btnSearch3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) ' Job Dtl Find Btn
        PanelInfo3.Visible = True
        btnHiddenInfo3.Visible = True
        MPEInfo3.Show()
        Dim sWhere = " AND A.ITSERVDESC LIKE '%%'"
        BindDataInfo3(sWhere)
    End Sub

    Protected Sub btnErase3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) ' Job Dtl Clear Btn
        ClearInfo3()
    End Sub

    Protected Sub btnErase2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) ' Spart Dtl Clear Btn
        ClearInfo2()
    End Sub

    Protected Sub btnSearch2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) ' Spart Dtl Find Btn
        PanelInfo2.Visible = True
        btnHiddenInfo2.Visible = True
        MPEInfo2.Show()
        Dim sWhere = ""
        BindDataInfo2(sWhere)
    End Sub

    Protected Sub GVListInfo2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVListInfo2.PageIndexChanging
        GVListInfo2.PageIndex = e.NewPageIndex
        btnFind2_Click(Nothing, Nothing)
        MPEInfo2.Show()
    End Sub

    Protected Sub GVListInfo2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVListInfo2.SelectedIndexChanged 'sPart GV
        spartmoid.Text = GVListInfo2.SelectedDataKey("PARTOID")
        txtNamaSpart.Text = GVListInfo2.Rows(GVListInfo2.SelectedIndex).Cells(2).Text
        If GVListInfo2.Rows(GVListInfo2.SelectedIndex).Cells(3).Text <> "&nbsp;" Then
            merkspr.Text = GVListInfo2.Rows(GVListInfo2.SelectedIndex).Cells(3).Text
        Else
            merkspr.Text = ""
        End If
        btnClear2.Visible = True : btnAddToListSPart2.Visible = True
        xxx.Text = "B" : btnHiddenInfo2.Visible = False : PanelInfo2.Visible = False
        matLoc.SelectedValue = GVListInfo2.SelectedDataKey("mtrloc").ToString
        txtQty.Text = "0" : txtHarga.Text = "0"
        saldoakhir.Text = GVListInfo2.SelectedDataKey("saldoakhir").ToString
        stock.Visible = True : ss.Visible = True : saldoakhir.Visible = True
        MPEInfo2.Hide()
    End Sub

    Protected Sub GVListInfo3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVListInfo3.SelectedIndexChanged 'Job GV
        Dim sCode As String = ""
        btnHiddenInfo3.Visible = False
        PanelInfo3.Visible = False
        MPEInfo3.Hide()
        sCode = GVListInfo3.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextInfo2(sCode)
    End Sub

    Protected Sub btnFindInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilterInfo.SelectedValue & " LIKE '%" & Tchar(txtFilterInfo.Text) & "%'"
        If cbPeriodInfo.Checked Then
            Try
                Dim date1 As Date = toDate(txtPeriod1Info.Text)
                Dim date2 As Date = toDate(txtPeriod2Info.Text)
                If txtPeriod1Info.Text = "" And txtPeriod2Info.Text = "" Then
                    showMessage("Tolong isi periode !", 2)
                End If
                If date1 < toDate("01/01/1900") Then
                    showMessage("Tanggal awal salah !", 2) : Exit Sub
                End If
                If date2 < toDate("01/01/1900") Then
                    showMessage("Tanggal akhir salah !", 2) : Exit Sub
                End If
                If date1 >= date2 Then
                    showMessage("Period 1 harus lebih kecil dari Period 2 !", 2)
                    txtPeriod1Info.Text = ""
                    txtPeriod2Info.Text = ""
                    Exit Sub
                End If
                sWhere &= " AND QLR.REQDATE BETWEEN '" & toDate(txtPeriod1Info.Text) & "' AND '" & toDate(txtPeriod2Info.Text) & "'"
            Catch ex As Exception
                showMessage("Tolong isi tanggal awal dan akhir !", 2)
            End Try
        End If
        BindDataInfo(sWhere)
        MPEInfo.Show()
    End Sub

    Protected Sub btnBatal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim errMessage As String = ""
        Dim d As DateTime
        If lblno.Text = "" Then
            errMessage &= "- Barcode harus diisi !<br>"
        End If
        If txtNo.Text = "" Then
            errMessage &= "- No. Tanda Terima harus diisi !<br>"
        End If
        If txtTglTarget.Text = "" Then
            errMessage &= "- Target selesai harus diisi !<br>"
        End If
        If teknisi.Text = "" Then
            errMessage &= "- Teknisi harus diisi !<br>"
        End If
        If txtWaktuTarget.Text = "" Then
            errMessage &= "- Waktu target harus diisi !<br>"
        End If
        Try
            If txtTglTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal salah ! Gunakan format dd/MM/yyyy - MAX 31:12:2029]! <br>"
                End If
            End If
            If DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                If txtTglTarget.Text <> "" Then
                    If (Session("idPage") Is Nothing Or Session("idPage") = "") And CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    ElseIf CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    End If
                End If
            End If

            If txtWaktuTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtWaktuTarget.Text, "HH:mm:ss", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format waktu salah ! Gunakan format HH:mm:ss - MAX 24:60:60]! <br>"
                ElseIf (Session("idPage") Is Nothing Or Session("idPage") = "") And txtTglTarget.Text = lblTarget.Text And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                    errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                Else
                    If (txtTglTarget.Text = TglTgl.Text) And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                        errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                    End If
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_trnservices WHERE mstreqoid = '" & Tchar(txtOid.Text) & "' "
        If Session("idPage") = Nothing Or Session("idPage") = "" Then
            If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
                errMessage &= "- No Tanda Terima telah diproses sebelumnya ! <BR>"
            End If
        End If

        'If txtOid.Text = "" Then
        '    errMessage &= "- Tolong pilih No Tanda Terima ! <BR>"
        'End If
        'check if there is any data in GV Dtl Service Plan
        Dim objTableCek As DataTable = Session("GVDtlServicePlan")
        If objTableCek.Rows.Count = 0 Then
            errMessage &= "- [Tidak ada detail data, Tolong cek form Check Teknisi !]! <br>"
        End If

        For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
            If ToDouble(objTableCek.Rows(c1).Item("ITSERVTARIF")) <= 0 Then
                errMessage &= "- [Harga Tarif Pengerjaan harus lebih besar dari 0 !] <br>!"
            End If
        Next

        Dim objTableCek2 As DataTable = Session("GVDtlServicePlan2")

        For c2 As Int16 = 0 To objTableCek2.Rows.Count - 1
            If objTableCek2.Rows(c2).RowState <> DataRowState.Deleted Then
                If Double.Parse(objTableCek2.Rows(c2).Item("SPARTQTY")) <> 0.0 Then
                    If Double.Parse(objTableCek2.Rows(c2).Item("SPARTPRICE")) <= 0.0 Then
                        errMessage &= "- [Harga sparepart harus lebih besar dari 0 !]! <br>"
                    End If
                Else
                    errMessage &= "- [Quantity sparepart harus lebih besar dari 0 !]! <br>"
                End If
            End If
        Next

        If ToDouble(txtHargaServis.Text) <= 0 Then
            errMessage &= "- [Harga Servis harus lebih besar dari 0 !]! <br>"
        End If

        If errMessage <> "" Then
            showMessage(errMessage, 2)
            Exit Sub
        End If

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            txtPlanOid.Text = GenerateID("QL_trnservices", CompnyCode)
            '  txtStatus.Text = "In Process"
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            txtStatus.Text = "Close"
            Dim wktjam As String = txtTglTarget.Text & " " & txtWaktuTarget.Text
            'mode insert
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                sSql = "INSERT into QL_trnservices(" & _
                "CMPCODE, " & _
                "BRANCH_CODE, " & _
                "SOID, " & _
                "MSTREQOID, " & _
                "SSTARTTIME, " & _
                "SENDTIME, " & _
                "STOTALPRICE, " & _
                "SSTATUS, " & _
                "SFLAG, " & _
                "CREATEUSER, " & _
                "CREATETIME, " & _
                "UPDUSER, " & _
                "UPDTIME) " & _
                "VALUES (" & _
                "'" & CompnyCode & "'," & _
                "'" & Session("branch_id") & "'," & _
                " " & txtPlanOid.Text & "," & _
                " " & txtOid.Text & "," & _
                "'" & toDate(wktjam) & "' ," & _
                "'" & toDate(wktjam) & "' ," & _
                " " & ToDouble(txtHargaServis.Text) & "," & _
                "'" & txtStatus.Text & "'," & _
                "'" & rbFlag.SelectedValue & "'," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp" & " ," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & txtPlanOid.Text & " where tablename = 'QL_trnservices' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            ElseIf txti_u.Text = "Update" Then
                'mode update

                'delete QL_trnservicesitem / detail
                sSql = "Delete from QL_trnservicesitem where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                'delete QL_trnservicespart / detail
                sSql = "Delete from QL_trnservicespart where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_trnservices SET " & _
                "SOID=" & txtPlanOid.Text & ", " & _
                "MSTREQOID=" & txtOid.Text & ", " & _
                "SSTARTTIME='" & toDate(wktjam) & "', " & _
                "SENDTIME='" & toDate(wktjam) & "', " & _
                "STOTALPRICE=" & ToDouble(txtHargaServis.Text) & ", " & _
                "SSTATUS='" & txtStatus.Text.Trim & "', " & _
                "SFLAG='" & rbFlag.SelectedValue & "', " & _
                "UPDUSER='" & updUser.Text & "', " & _
                 "UPDTIME=" & "current_timestamp" & ", " & _
                "SPERSON=" & teknisioid.Text & "" & _
                " WHERE cmpcode='" & CompnyCode & "' and SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            'insert tabel QL_trnservicesitem / detail
            Dim objTable As New DataTable
            Dim oidCounter As Integer = 0
            objTable = Session("GVDtlServicePlan")
            'GenerateID("QL_trnservicesitem", CompnyCode)
            'GenerateGenID2()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter = CInt(lbDtlOid.Text)
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT into QL_trnservicesitem(" & _
                "CMPCODE, " & _
                "BRANCH_CODE, " & _
                "SITEMOID, " & _
                "SOID, " & _
                "SEQ, " & _
                "SPARTNOTE, " & _
                "ITSERVOID, " & _
                "ITSERVTARIF, " & _
                "CREATEUSER, " & _
                "CREATETIME, " & _
                "UPDUSER, " & _
                "UPDTIME) " & _
                "VALUES (" & _
                "'" & CompnyCode & "'," & _
                "'" & Session("branch_id") & "'," & _
                " " & oidCounter & "," & _
                " " & txtPlanOid.Text & "," & _
                " " & objTable.Rows(c1).Item("SEQ") & "," & _
                "'" & Tchar(objTable.Rows(c1).Item("SPARTNOTE")) & "'," & _
                " " & objTable.Rows(c1).Item("ITSERVOID") & "," & _
                " " & ToDouble(objTable.Rows(c1).Item("ITSERVTARIF")) & "," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp" & " ," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                oidCounter += 1

            Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter - 1 & " where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'insert tabel QL_trnservicespart / detail
            Dim objTable2 As New DataTable
            Dim oidCounter2 As Integer = 0
            objTable2 = Session("GVDtlServicePlan2")
            'GenerateGenID3()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid2.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter2 = CInt(lbDtlOid2.Text)
            For c2 As Int16 = 0 To objTable2.Rows.Count - 1
                If Not objTable2.Rows(c2).RowState = DataRowState.Deleted Then
                    sSql = "INSERT into QL_trnservicespart(" & _
                    "CMPCODE, " & _
                    "BRANCH_CODE, " & _
                    "SPARTOID, " & _
                    "SOID, " & _
                    "SPARTSEQ, " & _
                    "SPARTMOID, " & _
                    "SPARTQTY, " & _
                    "SPARTPRICE, " & _
                    "ITSERVOID, " & _
                    "MPARTOID, " & _
                    "CREATEUSER, " & _
                    "CREATETIME, " & _
                    "UPDUSER, " & _
                    "UPDTIME, mtrlocoid) " & _
                    "VALUES (" & _
                    "'" & CompnyCode & "'," & _
                    "'" & Session("branch_id") & "'," & _
                    " " & oidCounter2 & "," & _
                    " " & txtPlanOid.Text & "," & _
                    " " & objTable2.Rows(c2).Item("SPARTSEQ") & "," & _
                    " " & objTable2.Rows(c2).Item("SPARTMOID") & "," & _
                    " " & ToDouble(objTable2.Rows(c2).Item("SPARTQTY")) & "," & _
                    " " & ToDouble(objTable2.Rows(c2).Item("SPARTPRICE")) & "," & _
                    " " & objTable2.Rows(c2).Item("ITSERVOID") & "," & _
                    " " & objTable2.Rows(c2).Item("MPARTOID") & "," & _
                    "'" & updUser.Text & "'," & _
                    " " & "current_timestamp" & " ," & _
                    "'" & updUser.Text & "'," & _
                    " " & "current_timestamp, " & objTable2.Rows(c2).Item("mtrloc") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    oidCounter2 += 1
                End If
            Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter2 - 1 & " where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'Update Trn Request, ubah status request menjadi closed
            sSql = "UPDATE QL_trnrequest SET " & _
               "REQSTATUS='" & txtStatus.Text.Trim & "' " & _
               "WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try

        Session.Remove("idPage")
        Response.Redirect("~\transaction\ServicePlan.aspx?awal=true")
    End Sub

    Protected Sub btnReady_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim errMessage As String = ""
        Dim d As DateTime

        If lblno.Text = "" Then
            errMessage &= "- Barcode harus diisi !<br>"
        End If
        If txtNo.Text = "" Then
            errMessage &= "- No. Tanda Terima harus diisi !<br>"
        End If
        If txtTglTarget.Text = "" Then
            errMessage &= "- Target selesai harus diisi !<br>"
        End If
        If teknisi.Text = "" Then
            errMessage &= "- Teknisi harus diisi !<br>"
        End If
        If txtWaktuTarget.Text = "" Then
            errMessage &= "- Waktu target harus diisi !<br>"
        End If
        Try
            If txtTglTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal salah ! Gunakan format dd/MM/yyyy - MAX 31:12:2029]! <br>"
                End If
            End If
            If DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                If txtTglTarget.Text <> "" Then
                    If (Session("idPage") Is Nothing Or Session("idPage") = "") And CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    ElseIf CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    End If
                End If
            End If

            If txtWaktuTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtWaktuTarget.Text, "HH:mm:ss", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format waktu salah ! Gunakan format HH:mm:ss - MAX 24:60:60]! <br>"
                ElseIf (Session("idPage") Is Nothing Or Session("idPage") = "") And txtTglTarget.Text = lblTarget.Text And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                    errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                Else
                    If (txtTglTarget.Text = TglTgl.Text) And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                        errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                    End If
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_trnservices WHERE mstreqoid = '" & Tchar(txtOid.Text) & "' "
        If Session("idPage") = Nothing Or Session("idPage") = "" Then
            If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
                errMessage &= "- No Tanda Terima telah diproses sebelumnya ! <BR>"
            End If
        End If

        'If txtOid.Text = "" Then
        '    errMessage &= "- Tolong pilih No Tanda Terima ! <BR>"
        'End If
        'check if there is any data in GV Dtl Service Plan
        Dim objTableCek As DataTable = Session("GVDtlServicePlan")
        If objTableCek.Rows.Count = 0 Then
            errMessage &= "- [Tidak ada detail data, Tolong cek form Check Teknisi !]! <br>"
        End If

        For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
            If ToDouble(objTableCek.Rows(c1).Item("ITSERVTARIF")) <= 0 Then
                errMessage &= "- [Harga Tarif Pengerjaan harus lebih besar dari 0 !] <br>!"
            End If
        Next

        Dim objTableCek2 As DataTable = Session("GVDtlServicePlan2")

        For c2 As Int16 = 0 To objTableCek2.Rows.Count - 1
            If objTableCek2.Rows(c2).RowState <> DataRowState.Deleted Then
                If Double.Parse(objTableCek2.Rows(c2).Item("SPARTQTY")) <> 0.0 Then
                    If Double.Parse(objTableCek2.Rows(c2).Item("SPARTPRICE")) <= 0.0 Then
                        errMessage &= "- [Harga sparepart harus lebih besar dari 0 !]! <br>"
                    End If
                Else
                    errMessage &= "- [Quantity sparepart harus lebih besar dari 0 !]! <br>"
                End If
            End If
        Next

        If ToDouble(txtHargaServis.Text) <= 0 Then
            errMessage &= "- [Harga Servis harus lebih besar dari 0 !]! <br>"
        End If

        If errMessage <> "" Then
            showMessage(errMessage, 2)
            Exit Sub
        End If

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            txtPlanOid.Text = GenerateID("QL_trnservices", CompnyCode)
            '  txtStatus.Text = "In Process"
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
            txtStatus.Text = "Ready"

            'If rbFlag.SelectedValue = "Out" Then
            '    txtStatus.Text = "Finish"
            '    'Update Trn Request, ubah status request menjadi closed
            '    sSql = "UPDATE QL_trnrequest SET " & _
            '       "REQSTATUS='" & txtStatus.Text.Trim & "', " & _
            '       "REQFLAG='" & rbFlag.SelectedValue & "' " & _
            '       "WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
            '    xCmd.CommandText = sSql
            '    xCmd.ExecuteNonQuery()
            'End If

            Dim wktjam As String = txtTglTarget.Text & " " & txtWaktuTarget.Text

            'mode insert
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                sSql = "INSERT into QL_trnservices (CMPCODE,BRANCH_CODE,SOID,MSTREQOID,SSTARTTIME,SENDTIME,STOTALPRICE,SSTATUS,SFLAG,CREATEUSER,CREATETIME,UPDUSER,PDTIME,SPERSON) " & _
                "VALUES (" & CompnyCode & "', '" & DdlCabang.SelectedValue & "', " & txtPlanOid.Text & ", " & txtOid.Text & ", '" & toDate(wktjam) & "', '" & toDate(wktjam) & "', " & ToDouble(txtHargaServis.Text) & ", '" & txtStatus.Text & "', '" & rbFlag.SelectedValue & "', '" & updUser.Text & "', " & "current_timestamp" & " , '" & updUser.Text & "', " & "current_timestamp, " & teknisioid.Text & ") "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & txtPlanOid.Text & " where tablename = 'QL_trnservices' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ElseIf txti_u.Text = "Update" Then
                'mode update

                'delete QL_trnservicesitem / detail
                sSql = "Delete from QL_trnservicesitem Where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnservicespart / detail
                sSql = "Delete from QL_trnservicespart Where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnservices SET SOID=" & txtPlanOid.Text & ", MSTREQOID=" & txtOid.Text & ", SSTARTTIME='" & toDate(wktjam) & "', SENDTIME='" & toDate(wktjam) & "', STOTALPRICE=" & ToDouble(txtHargaServis.Text) & ", SSTATUS='" & txtStatus.Text.Trim & "', SFLAG='" & rbFlag.SelectedValue & "', UPDUSER='" & updUser.Text & "', UPDTIME=" & "current_timestamp" & ", SPERSON=" & teknisioid.Text & " WHERE cmpcode='" & CompnyCode & "' and SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'insert tabel QL_trnservicesitem / detail
            Dim objTable As New DataTable : Dim oidCounter As Integer = 0
            objTable = Session("GVDtlServicePlan")
            'GenerateID("QL_trnservicesitem", CompnyCode)
            'GenerateGenID2()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter = CInt(lbDtlOid.Text)

            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT into QL_trnservicesitem (CMPCODE,BRANCH_CODE,SITEMOID,SOID, SEQ,SPARTNOTE,ITSERVOID,ITSERVTARIF,CREATEUSER,CREATETIME,UPDUSER,UPDTIME) " & _
                "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "', " & oidCounter & ", " & txtPlanOid.Text & ", " & objTable.Rows(c1).Item("SEQ") & ", '" & Tchar(objTable.Rows(c1).Item("SPARTNOTE")) & "', " & objTable.Rows(c1).Item("ITSERVOID") & ", " & ToDouble(objTable.Rows(c1).Item("ITSERVTARIF")) & ", '" & updUser.Text & "', " & "current_timestamp" & " , '" & updUser.Text & "', " & "current_timestamp)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                oidCounter += 1
            Next

            sSql = "update QL_mstoid set lastoid=" & oidCounter - 1 & " where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'insert tabel QL_trnservicespart / detail
            Dim objTable2 As New DataTable
            Dim oidCounter2 As Integer = 0
            objTable2 = Session("GVDtlServicePlan2")
            'GenerateGenID3()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : lbDtlOid2.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter2 = CInt(lbDtlOid2.Text)
            For c2 As Int16 = 0 To objTable2.Rows.Count - 1
                If Not objTable2.Rows(c2).RowState = DataRowState.Deleted Then
                    sSql = "INSERT into QL_trnservicespart(CMPCODE,BRANCH_CODE,SPARTOID,SOID,SPARTSEQ,SPARTMOID,SPARTQTY,SPARTPRICE,ITSERVOID,MPARTOID,CREATEUSER,CREATETIME,UPDUSER,UPDTIME,mtrlocoid)" & _
                    "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & oidCounter2 & "," & txtPlanOid.Text & "," & objTable2.Rows(c2).Item("SPARTSEQ") & "," & objTable2.Rows(c2).Item("SPARTMOID") & "," & ToDouble(objTable2.Rows(c2).Item("SPARTQTY")) & "," & ToDouble(objTable2.Rows(c2).Item("SPARTPRICE")) & "," & objTable2.Rows(c2).Item("ITSERVOID") & "," & objTable2.Rows(c2).Item("MPARTOID") & ",'" & updUser.Text & "'," & "current_timestamp" & " ,'" & updUser.Text & "'," & "current_timestamp," & objTable2.Rows(c2).Item("mtrloc") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    oidCounter2 += 1
                End If
            Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter2 - 1 & " where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'Update Trn Request, ubah status request menjadi closed
            sSql = "UPDATE QL_trnrequest SET REQSTATUS='" & txtStatus.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try

        Session.Remove("idPage")
        Response.Redirect("~\transaction\ServicePlan.aspx?awal=true")
    End Sub

    Protected Sub btnCcl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTable As DataTable
        objTable = Session("GVDtlServicePlan")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = ""

        'Insert / Update Data
        Dim objRow As DataRow
        objRow = objTable.Rows(Session("index"))

        objRow.EndEdit()

        ClearDetail()
        Session("GVDtlServicePlan") = objTable
        GVDtlServicePlan.Visible = True
        GVDtlServicePlan.DataSource = Nothing
        GVDtlServicePlan.DataSource = objTable
        GVDtlServicePlan.DataBind()

        lbIDseq.Text = objTable.Rows.Count + 1
        GVDtlServicePlan.SelectedIndex = -1
    End Sub

    Protected Sub btnViewAllInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFilterInfo.Text = ""
        ddlFilterInfo.SelectedIndex = 0
        cbPeriodInfo.Checked = False
        txtPeriod1Info.Text = ""
        txtPeriod2Info.Text = ""
        Dim sWhere = ""
        BindDataInfo(sWhere)
        MPEInfo.Show()
    End Sub

    Protected Sub btnFind2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilterInfo2.SelectedValue & " LIKE '%" & Tchar(txtFilterInfo2.Text) & "%'"
        BindDataInfo2(sWhere)
        MPEInfo2.Show()
    End Sub

    Protected Sub btnViewAll2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFilterInfo2.Text = ""
        ddlFilterInfo2.SelectedIndex = 0
        Dim sWhere = ""
        BindDataInfo2(sWhere)
        MPEInfo2.Show()
    End Sub

    Protected Sub btnFind3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilterInfo3.SelectedValue & " LIKE '%" & Tchar(txtFilterInfo3.Text) & "%'"
        BindDataInfo3(sWhere)
        MPEInfo3.Show()
    End Sub

    Protected Sub btnViewAll3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFilterInfo3.Text = ""
        ddlFilterInfo3.SelectedIndex = 0
        Dim sWhere = " AND A.ITSERVDESC LIKE '%%'"
        BindDataInfo3(sWhere)
        MPEInfo3.Show()
    End Sub

    Protected Sub GVDtlServicePlan2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlServicePlan2.RowDataBound
        Dim total As Double = 0 : Dim temp As Double = 0 : Dim temp3 As Double = 0
        For Each gvr2 As GridViewRow In GVDtlServicePlan2.Rows
            temp = Convert.ToDouble(gvr2.Cells(9).Text)
            temp3 = Convert.ToDouble(gvr2.Cells(10).Text)
            total += temp * temp3
        Next
        txtHargaSPart.Text = ToDouble(total)
        txtHargaServis.Text = ToDouble(txtHargaJob.Text) + ToDouble(txtHargaSPart.Text)
        btnAddToListSPart.Visible = False
        btnClear.Visible = False

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble((e.Row.Cells(9).Text)), 3)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble((e.Row.Cells(10).Text)), 3)
        End If
    End Sub

    Protected Sub GVDtlServicePlan2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlServicePlan2.RowDeleting
        If Session("GVDtlServicePlan2") Is Nothing = False Then
            Dim dtab As DataTable = Session("GVDtlServicePlan2")
            Dim cell As String = GVDtlServicePlan2.DataKeys(e.RowIndex).Item("spartmoid").ToString
            Dim drow() As DataRow = dtab.Select("spartmoid = " & Integer.Parse(GVDtlServicePlan2.DataKeys(e.RowIndex).Item("spartmoid").ToString) & "")
            For i As Integer = 0 To drow.Length - 1
                drow(0).Delete()
            Next
            dtab.Select(Nothing)

            Session("GVDtlServicePlan2") = dtab
            GVDtlServicePlan2.DataSource = dtab
            GVDtlServicePlan2.DataBind()
        End If
    End Sub

    Protected Sub GVDtlServicePlan2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtlServicePlan2.SelectedIndexChanged
        Try
            'Session("index2") = GVDtlServicePlan2.SelectedIndex
            Session("index2") = GVDtlServicePlan2.SelectedDataKey(2)
            If Session("GVDtlServicePlan2") Is Nothing = False Then
                lbIDseq2.Text = GVDtlServicePlan2.SelectedDataKey(0).ToString().Trim
                lbStatus2.Text = "Update"
                xxx.Text = "B"
                Dim objTable2 As DataTable
                objTable2 = Session("GVDtlServicePlan2")
                Dim dv2 As DataView = objTable2.DefaultView
                'Dim idx As Integer = Session("GVDtlServicePlan")
                dv2.RowFilter = "mpartoid='" & GVDtlServicePlan2.SelectedDataKey(1).ToString().Trim & "'"

                'Insert / Update List data
                'lbIDseq.Text = dv.Item(0).Row.Item("spartseq").ToString
                'lbDtlOid.Text = dv.Item(0).Row.Item("spartoid").ToString
                itservoid.Text = dv2.Item(0).Row.Item("itservoid").ToString
                txtJob.Text = dv2.Item(0).Row.Item("itservdesc").ToString
                txtType.Text = dv2.Item(0).Row.Item("itservtype").ToString
                spartmoid.Text = dv2.Item(0).Row.Item("spartmoid").ToString
                txtNamaSpart.Text = dv2.Item(0).Row.Item("spartm").ToString
                txtQty.Text = ToDouble(dv2.Item(0).Row.Item("spartqty").ToString.Trim)
                txtHarga.Text = ToDouble(dv2.Item(0).Row.Item("spartprice").ToString.Trim)
                mpartoid.Text = dv2.Item(0).Row.Item("mpartoid").ToString
                merkspr.Text = dv2.Item(0).Row.Item("merk").ToString
                matLoc.SelectedValue = dv2.Item(0).Row.Item("mtrloc").ToString
                stock.Visible = True : ss.Visible = True
                saldoakhir.Visible = True
                saldoakhir.Text = ToMaskEdit(dv2.Item(0).Row.Item("saldoakhir").ToString, 3)
                dv2.RowFilter = ""
                btnAddToListSPart2.Visible = True
                btnClear2.Visible = True
                If txtNamaSpart.Text = "-" Then
                    txtQty.Enabled = False : txtHarga.Enabled = False
                Else
                    txtQty.Enabled = True : txtHarga.Enabled = True
                End If
                btnSearch2.Visible = False : btnErase2.Visible = False
                GVDtlServicePlan2.Columns(11).Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Protected Sub txtHargaJob_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaJob.TextChanged
        txtHargaServis.Text = ToDouble(txtHargaJob.Text) + ToDouble(txtHargaSPart.Text)
    End Sub

    Protected Sub txtHargaSPart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaSPart.TextChanged
        txtHargaServis.Text = ToDouble(txtHargaJob.Text) + ToDouble(txtHargaSPart.Text)
    End Sub

    Protected Sub btnAddToListSPart2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        If Session("GVDtlServicePlan2") Is Nothing Then
            Dim dtlTable2 As DataTable = setTabelDetail2()
            Session("GVDtlServicePlan2") = dtlTable2
        End If

        If ToDouble(txtQty.Text) = 0 Then
            showMessage("Qty Sparepart tidak boleh nol !", 2) : Exit Sub
        End If

        If ToDouble(txtHarga.Text) = 0 Then
            showMessage("Harga Sparepart tidak boleh nol !", 2) : Exit Sub
        End If

        If ToDouble(txtQty.Text) >= ToDouble(saldoakhir.Text) Then
            showMessage("Qty Sparepart tidak boleh lebih dari Qty Stock !", 2)
            Exit Sub
        End If

        Dim objTable2 As DataTable
        objTable2 = Session("GVDtlServicePlan2")
        Dim dv2 As DataView

        Dim objTable As DataTable
        objTable = Session("GVDtlServicePlan2")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        If lbStatus2.Text = "New" Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "spartmoid=" & spartmoid.Text & ""
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah di tambahkan sebelumnya, Silahkan cek ulang!", 2)
                objTable = Session("TblDtl") : GVDtlServicePlan.DataSource = objTable
                GVDtlServicePlan.DataBind() : GVDtlServicePlan.Visible = True
                Exit Sub
            End If
            dv.RowFilter = ""
        Else
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "spartmoid=" & spartmoid.Text & " and spartseq <> " & lbIDseq2.Text & ""
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah di tambahkan sebelumnya, Silahkan cek ulang!", 2)
                objTable = Session("TblDtl") : GVDtlServicePlan.DataSource = objTable
                GVDtlServicePlan.DataBind() : GVDtlServicePlan.Visible = True
                Exit Sub
            End If
            dv.RowFilter = ""
        End If

        Try
            If xxx.Text = "B" Then
                'Insert / Update Data
                Dim objRow2 As DataRow
                Dim orow As DataRowView
                If lbStatus2.Text = "New" Then
                    objRow2 = objTable2.NewRow()
                    objRow2("SPARTSEQ") = objTable2.Rows.Count + 1 'Session("DTLOID1")
                    Session("SPARTSEQ") += 1
                    objRow2("SPARTMOID") = spartmoid.Text
                    objRow2("MPARTOID") = 0
                    objRow2("ITSERVOID") = Integer.Parse(GVDtlServicePlan.DataKeys(0).Values.Item(1).ToString)
                    objRow2("ITSERVDESC") = GVDtlServicePlan.Rows(0).Cells(3).Text
                    objRow2("ITSERVTYPE") = GVDtlServicePlan.Rows(0).Cells(4).Text
                    objRow2("mtrloc") = matLoc.SelectedValue
                    objRow2("SPARTM") = txtNamaSpart.Text
                    objRow2("merk") = merkspr.Text
                    objRow2("SPARTQTY") = ToDouble(txtQty.Text)
                    objRow2("SPARTPRICE") = ToDouble(txtHarga.Text)
                    objRow2("saldoakhir") = ToDouble(saldoakhir.Text)
                    objTable2.Rows.Add(objRow2)
                Else
                    dv2 = objTable2.DefaultView
                    dv2.RowFilter = "spartmoid = " & Integer.Parse(Session("index2")) & ""
                    orow = dv2.Item(0)
                    orow.BeginEdit()
                    orow("MPARTOID") = ToDouble(mpartoid.Text)
                    orow("ITSERVOID") = Integer.Parse(itservoid.Text)
                    orow("ITSERVDESC") = txtJob.Text
                    orow("ITSERVTYPE") = txtType.Text
                    orow("SPARTM") = txtNamaSpart.Text
                    orow("merk") = merkspr.Text
                    orow("SPARTQTY") = ToDouble(txtQty.Text)
                    orow("SPARTPRICE") = ToDouble(txtHarga.Text)
                    orow("saldoakhir") = ToDouble(saldoakhir.Text)
                    orow.EndEdit()
                    dv2.RowFilter = ""

                    'objRow2 = objTable2.Rows(Session("index2"))
                    'objRow2 = objTable2.Rows(0)
                End If

                ClearDetail2()
                Session("GVDtlServicePlan2") = objTable2
                GVDtlServicePlan2.Visible = True
                GVDtlServicePlan2.DataSource = Nothing
                GVDtlServicePlan2.DataSource = objTable2
                GVDtlServicePlan2.DataBind()

                lbIDseq2.Text = objTable2.Rows.Count + 1
                GVDtlServicePlan2.SelectedIndex = -1
                GVDtlServicePlan2.Columns(11).Visible = True
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnClear2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail2()
        GVDtlServicePlan2.SelectedIndex = -1
        GVDtlServicePlan2.Columns(11).Visible = True
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If txti_u.Text = "Update" Then
                'mode update

                'delete QL_trnservices / master
                sSql = "Delete from QL_trnservices where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'delete QL_trnservicesitem / detail
                sSql = "Delete from QL_trnservicesitem where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'delete QL_trnservicespart / detail
                sSql = "Delete from QL_trnservicespart where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            End If

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try

        Session.Remove("idPage")
        Response.Redirect("~\transaction\ServicePlan.aspx?awal=true")
    End Sub

    Protected Sub lblNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblno.TextChanged
        If Not lblno.Text = "" Then
            sSql = "SELECT QLR.REQOID, QLR.BARCODE, QLR.REQCODE, QLR.REQDATE as tanggal, QLR.REQDATE as waktu, QLC.CUSTNAME, QLC.CUSTADDR, QLC.PHONE1, QLM.GENDESC, QLM2.GENDESC as merk, QLR.REQITEMNAME, QLR.REQSERVICECAT, QLR.REQSTATUS, QLR.REQFLAG ,isnull(t.PERSONNAME,'') teknisi, qlr.reqperson FROM QL_TRNREQUEST QLR INNER JOIN QL_mstcust QLC ON QLR.REQCUSTOID=QLC.CUSTOID INNER JOIN QL_MSTGEN QLM ON QLR.REQITEMTYPE=QLM.GENOID left join ql_mstperson t on QLR.reqperson = t.personoid INNER JOIN QL_MSTGEN QLM2 ON QLR.REQITEMBRAND=QLM2.GENOID WHERE QLR.cmpcode='" & CompnyCode & "' AND QLR.barcode='" & Tchar(lblno.Text) & "' AND QLR.REQSTATUS = 'Check' and QLR.REQOID not in (Select mstreqoid from ql_trnservices )"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xReader = xCmd.ExecuteReader
            Dim Tanggal As String = ""
            Dim Waktu As String = ""
            If xReader.HasRows Then
                While xReader.Read
                    txtOid.Text = xReader("REQOID")
                    txtNo.Text = xReader("REQCODE").ToString.Trim
                    txtTglTarget.Text = Format(Now, "dd/MM/yyyy") 'Format(xReader("tanggal"), "dd/MM/yyyy")
                    txtWaktuTarget.Text = Format(Now, "HH:mm:ss") 'Format(xReader("waktu"), "HH:mm:ss")
                    txtNamaCust.Text = xReader("CUSTNAME").ToString.Trim
                    txtAlamatCust.Text = xReader("CUSTADDR").ToString.Trim
                    txtHPCust.Text = xReader("PHONE1").ToString.Trim
                    txtJenisBrg.Text = xReader("GENDESC").ToString.Trim
                    txtMerk.Text = xReader("merk").ToString.Trim
                    txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                    txtKatKerusakan.Text = xReader("REQSERVICECAT").ToString.Trim
                    txtStatus.Text = xReader("REQSTATUS").ToString.Trim
                    teknisioid.Text = xReader("reqperson").ToString.Trim
                    teknisi.Text = xReader("teknisi").ToString.Trim
                    lblno.Text = xReader("BARCODE").ToString.Trim
                    'updUser.Text = xReader("UPDUSER").ToString.Trim
                    'updTime.Text = Format(xReader("UPDTIME"), "dd/MM/yyyy HH:mm:ss")
                    rbFlag.SelectedValue = xReader("REQFLAG").ToString
                End While
            Else
                showMessage("Tidak ada No. Barcode yang ditemukan", 2) : Exit Sub
            End If

            xReader.Close()
            conn.Close()

            lblno.CssClass = "inpTextDisabled"
            lblno.Enabled = False

            Dim code As String = txtNo.Text

            GVDtlServicePlan.Visible = True
            GVDtlServicePlan2.Visible = True

            sSql = "SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY A.MSTREQOID) as SEQ, AC.REQCODE, A.MSTREQOID, A.ITSERVOID, A1.ITSERVDESC, A2.GENDESC ITSERVTYPE, A1.ITSERVTARIF, A.NOTEDTL AS SPARTNOTE FROM QL_TRNMECHCHECK A INNER JOIN QL_TRNREQUEST AC ON AC.REQOID = A.MSTREQOID INNER JOIN QL_MSTITEMSERV A1 ON A1.ITSERVOID = A.ITSERVOID INNER JOIN QL_MSTGEN A2 ON A2.genoid = A1.ITSERVTYPEOID WHERE A.cmpcode='" & CompnyCode & "' AND AC.REQCODE = '" & Tchar(code) & "' AND AC.REQSTATUS = 'Check'"

            Dim mysqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDs As New DataSet
            mysqlDA.Fill(objDs, "data2")
            GVDtlServicePlan.DataSource = objDs.Tables("data2")
            GVDtlServicePlan.DataBind()
            Session("GVDtlServicePlan") = objDs.Tables("data2")

            sSql = "SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY A.MSTREQOID) as SPARTSEQ, AC.REQCODE, ISNULL(B.MSTPARTOID,0) SPARTMOID , ISNULL(B.MPARTOID,0) MPARTOID, A.MSTREQOID, A.ITSERVOID, ISNULL(B.MRCHECKOID,0) MRCHECKOID, A1.ITSERVDESC, A2.GENDESC ITSERVTYPE, ISNULL(B1.PARTDESCSHORT,'-') SPARTM,b1.merk, ISNULL(B.MPARTQTY,0) SPARTQTY, ISNULL(B.MPARTPRICE,0) SPARTPRICE  FROM QL_TRNMECHCHECK A INNER JOIN QL_TRNREQUEST AC ON AC.REQOID = A.MSTREQOID INNER JOIN QL_TRNMECHPARTDTL B ON B.MRCHECKOID = A.MCHECKOID INNER JOIN QL_MSTITEMSERV A1 ON A1.ITSERVOID = A.ITSERVOID INNER JOIN QL_MSTGEN A2 ON A2.genoid = A1.ITSERVTYPEOID INNER JOIN QL_MSTSPAREPART B1 ON B1.PARTOID = B.MSTPARTOID WHERE A.cmpcode='" & CompnyCode & "' AND AC.REQCODE = '" & Tchar(code) & "' AND AC.REQSTATUS = 'Check'"

            Dim mysqlDA2 As New SqlDataAdapter(sSql, conn)
            Dim objDs2 As New DataSet
            mysqlDA2.Fill(objDs2, "data3")
            GVDtlServicePlan2.DataSource = objDs2.Tables("data3")
            GVDtlServicePlan2.DataBind()
            Session("GVDtlServicePlan2") = objDs2.Tables("data3")
        End If
    End Sub

    Sub BindDatateknisi(ByVal sWhere As String)
        sSql = "select personoid,PERSONNIP ,PERSONNAME ,PERSONCRTADDRESS,(select count(reqperson)from QL_TRNREQUEST where REQSTATUS in ('In','Out','Receive','Check','SPK') and REQPERSON =PERSONOID )+(select COUNT(sperson) from QL_TRNSERVICES  where SSTATUS in('Ready','Start') and SPERSON=PERSONOID)as pengerjaan  from QL_MSTPERSON  c where c.cmpcode like '%" & CompnyCode & "%'" & sWhere & ""

        Dim xTableItem2 As DataTable = cKoneksi.ambiltabel(sSql, "teknisi")
        gvListTeknisi.DataSource = xTableItem2
        gvListTeknisi.DataBind()
        gvListTeknisi.SelectedIndex = -1
        gvListTeknisi.Visible = True
    End Sub

    Protected Sub btnSrcTeknisi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSrcTeknisi.Click
        DDLFilterteknisi.SelectedIndex = 0
        txtFilterTeknisi.Text = ""
        panelteknisi.Visible = True
        btnHideteknisi.Visible = True
        mpeteknisi.Show()
        Dim sWhere As String = ""
        sWhere = "  and PERSONPOST IN (SELECT GENOID FROM QL_MSTGEN WHERE GENDESC LIKE 'TEKNISI%' AND GENGROUP = 'JOBPOSITION')"
        BindDatateknisi(sWhere)
    End Sub

    Protected Sub imberaseteknisi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        teknisioid.Text = "" : teknisi.Text = ""
        gvListTeknisi.Visible = False
    End Sub

    Protected Sub imbFindTeknisi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        panelteknisi.Visible = True
        btnHideteknisi.Visible = True
        mpeteknisi.Show()
        Dim sWhere As String = " AND " & DDLFilterteknisi.SelectedValue & " LIKE '%" & Tchar(txtFilterTeknisi.Text) & "%' and PERSONNIP in (select userid from QL_MSTPROF )"
        BindDatateknisi(sWhere)
    End Sub

    Protected Sub imbViewTeknisi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        panelteknisi.Visible = True : btnHideteknisi.Visible = True
        mpeteknisi.Show()
        Dim sWhere As String = ""
        sWhere = "  and PERSONNIP in (select userid from QL_MSTPROF )"
        BindDatateknisi(sWhere)
        txtFilterTeknisi.Text = "" : DDLFilterteknisi.SelectedIndex = 0
    End Sub

    Sub FillTextteknisi(ByVal Code As Integer)
        sSql = "select distinct personoid,PERSONNIP ,PERSONNAME ,PERSONCRTADDRESS  from QL_MSTPERSON c where c.cmpcode='" & CompnyCode & "' AND c.personoid='" & Code & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                teknisioid.Text = xReader("personoid").ToString.Trim
                teknisi.Text = xReader("PERSONNAME").ToString.Trim
            End While
        End If
    End Sub

    Protected Sub gvListTeknisi_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListTeknisi.PageIndexChanging
        gvListTeknisi.PageIndex = e.NewPageIndex
        panelteknisi.Visible = True
        btnHideteknisi.Visible = True : mpeteknisi.Show()
        'Session("gvNoKerusakan") = Nothing
        BindDatateknisi("")
    End Sub

    Protected Sub gvListTeknisi_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sCode As Integer
        btnHideteknisi.Visible = False : panelteknisi.Visible = False
        mpeteknisi.Show() : sCode = gvListTeknisi.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextteknisi(sCode)
    End Sub

    Protected Sub lkbCloseTeknisi_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        panelteknisi.Visible = False : btnHideteknisi.Visible = False
        CProc.DisposeGridView(gvListTeknisi)
    End Sub

    Protected Sub rbFlag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbFlag.SelectedIndexChanged
        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
        Else 'update
            If rbFlag.SelectedValue.ToLower = "in" Then
                btncheckout.Visible = False : btnReady.Visible = True
            Else
                btncheckout.Visible = True : btnReady.Visible = False
            End If
        End If
    End Sub

    Protected Sub btncheckout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncheckout.Click
        Dim errMessage As String = "" : Dim d As DateTime
        If lblno.Text = "" Then
            errMessage &= "- Barcode harus diisi !<br>"
        End If
        If txtNo.Text = "" Then
            errMessage &= "- No. Tanda Terima harus diisi !<br>"
        End If
        If txtTglTarget.Text = "" Then
            errMessage &= "- Target selesai harus diisi !<br>"
        End If
        If teknisi.Text = "" Then
            errMessage &= "- Teknisi harus diisi !<br>"
        End If
        If txtWaktuTarget.Text = "" Then
            errMessage &= "- Waktu target harus diisi !<br>"
        End If
        Try
            If txtTglTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal salah !]! <br>"
                End If
            End If
            If DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                If txtTglTarget.Text <> "" Then
                    If (Session("idPage") Is Nothing Or Session("idPage") = "") And CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    ElseIf CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    End If
                End If
            End If

            If txtWaktuTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtWaktuTarget.Text, "HH:mm:ss", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format waktu salah ! Gunakan format HH:mm:ss - MAX 23:59:59]! <br>"
                ElseIf (Session("idPage") Is Nothing Or Session("idPage") = "") And txtTglTarget.Text = lblTarget.Text And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                    errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                Else
                    If (txtTglTarget.Text = TglTgl.Text) And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                        errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                    End If
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_trnservices WHERE mstreqoid = '" & Tchar(txtOid.Text) & "' "
        If Session("idPage") = Nothing Or Session("idPage") = "" Then
            If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
                errMessage &= "- No Tanda Terima telah diproses sebelumnya ! <BR>"
            End If
        End If

        'If txtOid.Text = "" Then
        '    errMessage &= "- Tolong pilih No Tanda Terima ! <BR>"
        'End If
        'check if there is any data in GV Dtl Service Plan
        Dim objTableCek As DataTable = Session("GVDtlServicePlan")
        If objTableCek.Rows.Count = 0 Then
            errMessage &= "- [Tidak ada detail data, Tolong cek form Check Teknisi !]! <br>"
        End If

        For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
            If ToDouble(objTableCek.Rows(c1).Item("ITSERVTARIF")) < 0 Then
                errMessage &= "- [Harga Tarif Pengerjaan tidak bileh kurang dari 0 !] <br>!"
            End If
        Next

        Dim objTableCek2 As DataTable = Session("GVDtlServicePlan2")

        For c2 As Int16 = 0 To objTableCek2.Rows.Count - 1
            If objTableCek2.Rows(c2).RowState <> DataRowState.Deleted Then
                If Double.Parse(objTableCek2.Rows(c2).Item("SPARTQTY")) <> 0.0 Then
                    If Double.Parse(objTableCek2.Rows(c2).Item("SPARTPRICE")) <= 0.0 Then
                        errMessage &= "- [Harga sparepart harus lebih besar dari 0 !]! <br>"
                    End If
                Else
                    errMessage &= "- [Quantity sparepart harus lebih besar dari 0 !]! <br>"
                End If
            End If
        Next

        If ToDouble(txtHargaServis.Text) < 0 Then
            errMessage &= "- [Harga Servis tidak boleh kurang dari 0 !]! <br>"
        End If

        If errMessage <> "" Then
            showMessage(errMessage, 2)
            Exit Sub
        End If

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            txtPlanOid.Text = GenerateID("QL_trnservices", CompnyCode)
            '  txtStatus.Text = "In Process"
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            txtStatus.Text = "CheckOut"
            Dim wktjam As String = txtTglTarget.Text & " " & txtWaktuTarget.Text

            'mode insert
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                sSql = "INSERT into QL_trnservices(CMPCODE,BRANCH_CODE,SOID,MSTREQOID,SSTARTTIME,SENDTIME,STOTALPRICE,SSTATUS,SFLAG,CREATEUSER,CREATETIME,UPDUSER,UPDTIME,SPERSON) " & _
                "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & txtPlanOid.Text & "," & txtOid.Text & ",'" & toDate(wktjam) & "','" & toDate(wktjam) & "'," & ToDouble(txtHargaServis.Text) & ",'" & txtStatus.Text & "','" & rbFlag.SelectedValue & "','" & updUser.Text & "'," & "current_timestamp" & ",'" & updUser.Text & "',current_timestamp," & teknisioid.Text & ") "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & txtPlanOid.Text & " Where tablename = 'QL_trnservices' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ElseIf txti_u.Text = "Update" Then
                'mode update

                'delete QL_trnservicesitem / detail
                sSql = "Delete from QL_trnservicesitem where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnservicespart / detail
                sSql = "Delete from QL_trnservicespart where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnservices SET " & _
                "SOID=" & txtPlanOid.Text & "," & _
                "MSTREQOID=" & txtOid.Text & "," & _
                "SSTARTTIME='" & toDate(wktjam) & "'," & _
                "SENDTIME='" & toDate(wktjam) & "'," & _
                "STOTALPRICE=" & ToDouble(txtHargaServis.Text) & "," & _
                "SSTATUS='" & txtStatus.Text.Trim & "'," & _
                "SFLAG='" & rbFlag.SelectedValue & "'," & _
                "UPDUSER='" & updUser.Text & "'," & _
                "UPDTIME=current_timestamp," & _
                "SPERSON=" & teknisioid.Text & _
                " WHERE cmpcode='" & CompnyCode & "' and SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'insert tabel QL_trnservicesitem / detail
            Dim objTable As New DataTable
            Dim oidCounter As Integer = 0
            objTable = Session("GVDtlServicePlan")
            'GenerateID("QL_trnservicesitem", CompnyCode)
            'GenerateGenID2()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter = CInt(lbDtlOid.Text)
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT into QL_trnservicesitem(CMPCODE,BRANCH_CODE,SITEMOID,SOID,SEQ,SPARTNOTE,ITSERVOID,ITSERVTARIF,CREATEUSER,CREATETIME,UPDUSER,UPDTIME) " & _
                "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & oidCounter & "," & txtPlanOid.Text & "," & objTable.Rows(c1).Item("SEQ") & ",'" & Tchar(objTable.Rows(c1).Item("SPARTNOTE")) & "'," & objTable.Rows(c1).Item("ITSERVOID") & "," & ToDouble(objTable.Rows(c1).Item("ITSERVTARIF")) & ",'" & updUser.Text & "',current_timestamp,'" & updUser.Text & "',current_timestamp)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() : oidCounter += 1
            Next

            sSql = "update QL_mstoid set lastoid=" & oidCounter - 1 & " where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'insert tabel QL_trnservicespart / detail
            Dim objTable2 As New DataTable
            Dim oidCounter2 As Integer = 0
            objTable2 = Session("GVDtlServicePlan2")
            'GenerateGenID3()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : lbDtlOid2.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter2 = CInt(lbDtlOid2.Text)
            For c2 As Int16 = 0 To objTable2.Rows.Count - 1
                If Not objTable2.Rows(c2).RowState = DataRowState.Deleted Then
                    sSql = "INSERT into QL_trnservicespart(CMPCODE,BRANCH_CODE,SPARTOID,SOID,SPARTSEQ,SPARTMOID,SPARTQTY,SPARTPRICE,ITSERVOID,MPARTOID,CREATEUSER,CREATETIME,UPDUSER,UPDTIME,mtrlocoid) " & _
                    "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & oidCounter2 & "," & txtPlanOid.Text & "," & objTable2.Rows(c2).Item("SPARTSEQ") & "," & objTable2.Rows(c2).Item("SPARTMOID") & "," & ToDouble(objTable2.Rows(c2).Item("SPARTQTY")) & "," & ToDouble(objTable2.Rows(c2).Item("SPARTPRICE")) & "," & objTable2.Rows(c2).Item("ITSERVOID") & "," & objTable2.Rows(c2).Item("MPARTOID") & ",'" & updUser.Text & "',current_timestamp,'" & updUser.Text & "',current_timestamp," & objTable2.Rows(c2).Item("mtrloc") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() : oidCounter2 += 1
                End If
            Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter2 - 1 & " where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'Update Trn Request, ubah status request menjadi closed
            sSql = "UPDATE QL_trnrequest SET REQSTATUS='" & txtStatus.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session.Remove("idPage")
        Response.Redirect("~\transaction\ServicePlan.aspx?awal=true")
    End Sub

    Protected Sub btnpaid_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim errMessage As String = ""
        Dim d As DateTime

        If lblno.Text = "" Then
            errMessage &= "- Barcode harus diisi !<br>"
        End If
        If txtNo.Text = "" Then
            errMessage &= "- No. Tanda Terima harus diisi !<br>"
        End If
        If txtTglTarget.Text = "" Then
            errMessage &= "- Target selesai harus diisi !<br>"
        End If
        If teknisi.Text = "" Then
            errMessage &= "- Teknisi harus diisi !<br>"
        End If
        If txtWaktuTarget.Text = "" Then
            errMessage &= "- Waktu target harus diisi !<br>"
        End If
        Try
            If txtTglTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal salah ! Gunakan format dd/MM/yyyy - MAX 31:12:2029]! <br>"
                End If
            End If

            'gak perlu cek tanggal selesai
            'If DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
            '    If txtTglTarget.Text <> "" Then
            '        If (Session("idPage") Is Nothing Or Session("idPage") = "") And CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
            '            errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
            '        ElseIf CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
            '            errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
            '        End If
            '    End If
            'End If

            'If txtWaktuTarget.Text <> "" Then
            '    If Not DateTime.TryParseExact(txtWaktuTarget.Text, "HH:mm:ss", Nothing, System.Globalization.DateTimeStyles.None, d) Then
            '        errMessage &= "- [Format waktu salah ! Gunakan format HH:mm:ss - MAX 23:59:59]! <br>"
            '    ElseIf (Session("idPage") Is Nothing Or Session("idPage") = "") And txtTglTarget.Text = lblTarget.Text And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
            '        errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
            '    Else
            '        If (txtTglTarget.Text = TglTgl.Text) And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
            '            errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
            '        End If
            '    End If
            'End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_trnservices WHERE mstreqoid = '" & Tchar(txtOid.Text) & "' "
        If Session("idPage") = Nothing Or Session("idPage") = "" Then
            If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
                errMessage &= "- No Tanda Terima telah diproses sebelumnya ! <BR>"
            End If
        End If

        'If txtOid.Text = "" Then
        '    errMessage &= "- Tolong pilih No Tanda Terima ! <BR>"
        'End If
        'check if there is any data in GV Dtl Service Plan
        Dim objTableCek As DataTable = Session("GVDtlServicePlan")
        'If objTableCek.Rows.Count = 0 Then
        '    errMessage &= "- [Tidak ada detail data, Tolong cek form 'Check Teknisi' !]! <br>"
        'End If

        For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
            If ToDouble(objTableCek.Rows(c1).Item("ITSERVTARIF")) < 0 Then
                errMessage &= "- [Harga Tarif Pengerjaan harus lebih besar dari 0 !] <br>!"
            End If
        Next

        Dim objTableCek2 As DataTable = Session("GVDtlServicePlan2")

        For c2 As Int16 = 0 To objTableCek2.Rows.Count - 1
            If objTableCek2.Rows(c2).RowState <> DataRowState.Deleted Then
                If Double.Parse(objTableCek2.Rows(c2).Item("SPARTQTY")) <> 0.0 Then
                    If Double.Parse(objTableCek2.Rows(c2).Item("SPARTPRICE")) <= 0.0 Then
                        errMessage &= "- [Harga sparepart harus lebih besar dari 0 !]! <br>"
                    End If
                Else
                    errMessage &= "- [Quantity sparepart harus lebih besar dari 0 !]! <br>"
                End If
            End If
        Next

        If ToDouble(txtHargaServis.Text) < 0 Then
            errMessage &= "- [Harga Servis harus lebih besar dari 0 !]! <br>"
        End If

        If errMessage <> "" Then
            showMessage(errMessage, 2)
            Exit Sub
        End If

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            txtPlanOid.Text = GenerateID("QL_trnservices", CompnyCode)
            '  txtStatus.Text = "In Process"
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            txtStatus.Text = "Paid"
            Dim wktjam As String = txtTglTarget.Text & " " & txtWaktuTarget.Text

            'mode insert
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                sSql = "INSERT into QL_trnservices(" & _
                "CMPCODE, " & _
                "SOID, " & _
                "MSTREQOID, " & _
                "SSTARTTIME, " & _
                "SENDTIME, " & _
                "STOTALPRICE, " & _
                "SSTATUS, " & _
                "SFLAG, " & _
                "CREATEUSER, " & _
                "CREATETIME, " & _
                "UPDUSER, " & _
                "UPDTIME, " & _
                "SPERSON) " & _
                "VALUES (" & _
                "'" & CompnyCode & "'," & _
                " " & txtPlanOid.Text & "," & _
                " " & txtOid.Text & "," & _
                "'" & toDate(wktjam) & "' ," & _
                "'" & toDate(wktjam) & "' ," & _
                " " & ToDouble(txtHargaServis.Text) & "," & _
                "'" & txtStatus.Text & "'," & _
                "'" & rbFlag.SelectedValue & "'," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp" & " ," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp," & _
                " " & teknisioid.Text & ") "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & txtPlanOid.Text & " where tablename = 'QL_trnservices' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            ElseIf txti_u.Text = "Update" Then
                'mode update

                'delete QL_trnservicesitem / detail
                sSql = "Delete from QL_trnservicesitem where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'delete QL_trnservicespart / detail
                sSql = "Delete from QL_trnservicespart where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnservices SET " & _
                "SOID=" & txtPlanOid.Text & ", " & _
                "MSTREQOID=" & txtOid.Text & ", " & _
                "SSTARTTIME='" & toDate(wktjam) & "', " & _
                "SENDTIME='" & toDate(wktjam) & "', " & _
                "STOTALPRICE=" & ToDouble(txtHargaServis.Text) & ", " & _
                "SSTATUS='" & txtStatus.Text.Trim & "', " & _
                "SFLAG='" & rbFlag.SelectedValue & "', " & _
                "UPDUSER='" & updUser.Text & "', " & _
                "UPDTIME=" & "current_timestamp" & ", " & _
                "SPERSON=" & teknisioid.Text & "" & _
                " WHERE cmpcode='" & CompnyCode & "' and SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If


            'insert tabel QL_trnservicesitem / detail
            Dim objTable As New DataTable
            Dim oidCounter As Integer = 0
            objTable = Session("GVDtlServicePlan")
            'GenerateID("QL_trnservicesitem", CompnyCode)
            'GenerateGenID2()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter = CInt(lbDtlOid.Text)
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT into QL_trnservicesitem(" & _
                "CMPCODE, " & _
                "SITEMOID, " & _
                "SOID, " & _
                "SEQ, " & _
                "SPARTNOTE, " & _
                "ITSERVOID, " & _
                "ITSERVTARIF, " & _
                "CREATEUSER, " & _
                "CREATETIME, " & _
                "UPDUSER, " & _
                "UPDTIME) " & _
                "VALUES (" & _
                "'" & CompnyCode & "'," & _
                " " & oidCounter & "," & _
                " " & txtPlanOid.Text & "," & _
                " " & objTable.Rows(c1).Item("SEQ") & "," & _
                "'" & Tchar(objTable.Rows(c1).Item("SPARTNOTE")) & "'," & _
                " " & objTable.Rows(c1).Item("ITSERVOID") & "," & _
                " " & ToDouble(objTable.Rows(c1).Item("ITSERVTARIF")) & "," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp" & " ," & _
                "'" & updUser.Text & "'," & _
                " " & "current_timestamp)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                oidCounter += 1

            Next

            sSql = "update QL_mstoid set lastoid=" & oidCounter - 1 & " where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'insert tabel QL_trnservicespart / detail
            Dim objTable2 As New DataTable
            Dim oidCounter2 As Integer = 0
            objTable2 = Session("GVDtlServicePlan2")
            'GenerateGenID3()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid2.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter2 = CInt(lbDtlOid2.Text)
            For c2 As Int16 = 0 To objTable2.Rows.Count - 1
                If Not objTable2.Rows(c2).RowState = DataRowState.Deleted Then
                    sSql = "INSERT into QL_trnservicespart(" & _
                    "CMPCODE, " & _
                    "SPARTOID, " & _
                    "SOID, " & _
                    "SPARTSEQ, " & _
                    "SPARTMOID, " & _
                    "SPARTQTY, " & _
                    "SPARTPRICE, " & _
                    "ITSERVOID, " & _
                    "MPARTOID, " & _
                    "CREATEUSER, " & _
                    "CREATETIME, " & _
                    "UPDUSER, " & _
                    "UPDTIME) " & _
                    "VALUES (" & _
                    "'" & CompnyCode & "'," & _
                    " " & oidCounter2 & "," & _
                    " " & txtPlanOid.Text & "," & _
                    " " & objTable2.Rows(c2).Item("SPARTSEQ") & "," & _
                    " " & objTable2.Rows(c2).Item("SPARTMOID") & "," & _
                    " " & ToDouble(objTable2.Rows(c2).Item("SPARTQTY")) & "," & _
                    " " & ToDouble(objTable2.Rows(c2).Item("SPARTPRICE")) & "," & _
                    " " & objTable2.Rows(c2).Item("ITSERVOID") & "," & _
                    " " & objTable2.Rows(c2).Item("MPARTOID") & "," & _
                    "'" & updUser.Text & "'," & _
                    " " & "current_timestamp" & " ," & _
                    "'" & updUser.Text & "'," & _
                    " " & "current_timestamp)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    oidCounter2 += 1
                End If
            Next

            ''insert tabel QL_trnservicespart / detail
            'Dim objTable2 As New DataTable
            'Dim oidCounter2 As Integer = 0
            'objTable2 = Session("GVDtlServicePlan2")
            'GenerateGenID3()
            'oidCounter2 = CInt(lbDtlOid2.Text)
            'For c2 As Int16 = 0 To objTable2.Rows.Count - 1
            '    sSql = "INSERT into QL_trnservicespart(" & _
            '    "CMPCODE, " & _
            '    "SPARTOID, " & _
            '    "SOID, " & _
            '    "SPARTSEQ, " & _
            '    "SPARTMOID, " & _
            '    "SPARTQTY, " & _
            '    "SPARTPRICE, " & _
            '    "ITSERVOID, " & _
            '    "MPARTOID, " & _
            '    "CREATEUSER, " & _
            '    "CREATETIME, " & _
            '    "UPDUSER, " & _
            '    "UPDTIME) " & _
            '    "VALUES (" & _
            '    "'" & CompnyCode & "'," & _
            '    " " & oidCounter2 & "," & _
            '    " " & txtPlanOid.Text & "," & _
            '    " " & objTable2.Rows(c2).Item("SPARTSEQ") & "," & _
            '    " " & objTable2.Rows(c2).Item("SPARTMOID") & "," & _
            '    " " & ToDouble(objTable2.Rows(c2).Item("SPARTQTY")) & "," & _
            '    " " & ToDouble(objTable2.Rows(c2).Item("SPARTPRICE")) & "," & _
            '    " " & objTable2.Rows(c2).Item("ITSERVOID") & "," & _
            '    " " & objTable2.Rows(c2).Item("MPARTOID") & "," & _
            '    "'" & updUser.Text & "'," & _
            '    " " & "current_timestamp" & " ," & _
            '    "'" & updUser.Text & "'," & _
            '    " " & "current_timestamp)"
            '    xCmd.CommandText = sSql
            '    xCmd.ExecuteNonQuery()
            '    oidCounter2 += 1

            'Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter2 - 1 & " where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'Update Trn Request, ubah status request menjadi closed
            sSql = "UPDATE QL_trnrequest SET " & _
               "REQSTATUS='" & txtStatus.Text.Trim & "' , dateclosed= current_timestamp, noteclosed = 'Batal' , userclosed = '" & updUser.Text & "' " & _
               "WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try

        Session.Remove("idPage")
        Response.Redirect("~\transaction\ServicePlan.aspx?awal=true")
    End Sub

    Protected Sub btnfinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfinish.Click
        Dim errMessage As String = "" : Dim d As DateTime
        If lblno.Text = "" Then
            errMessage &= "- Barcode harus diisi !<br>"
        End If
        If txtNo.Text = "" Then
            errMessage &= "- No. Tanda Terima harus diisi !<br>"
        End If
        If txtTglTarget.Text = "" Then
            errMessage &= "- Target selesai harus diisi !<br>"
        End If
        If teknisi.Text = "" Then
            errMessage &= "- Teknisi harus diisi !<br>"
        End If
        If txtWaktuTarget.Text = "" Then
            errMessage &= "- Waktu target harus diisi !<br>"
        End If
        Try
            If txtTglTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal salah ! Gunakan format dd/MM/yyyy - MAX 31:12:2029]! <br>"
                End If
            End If
            If DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                If txtTglTarget.Text <> "" Then
                    If (Session("idPage") Is Nothing Or Session("idPage") = "") And CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    ElseIf CDate(toDate(txtTglTarget.Text)) < CDate(toDate(TglTgl.Text)) Then
                        errMessage &= "- Tanggal target tidak boleh lebih kecil dari tanggal sekarang ! <BR>"
                    End If
                End If
            End If

            If txtWaktuTarget.Text <> "" Then
                If Not DateTime.TryParseExact(txtWaktuTarget.Text, "HH:mm:ss", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format waktu salah ! Gunakan format HH:mm:ss - MAX 23:59:59]! <br>"
                ElseIf (Session("idPage") Is Nothing Or Session("idPage") = "") And txtTglTarget.Text = lblTarget.Text And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                    errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                Else
                    If (txtTglTarget.Text = TglTgl.Text) And CInt(Replace(txtWaktuTarget.Text.Trim, ":", "")) < Format(Date.Now, "HHmmss") Then
                        errMessage &= "- Waktu target tidak boleh lebih kecil dari waktu sekarang ! <BR>"
                    End If
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_trnservices WHERE mstreqoid = '" & Tchar(txtOid.Text) & "' "
        If Session("idPage") = Nothing Or Session("idPage") = "" Then
            If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
                errMessage &= "- No Tanda Terima telah diproses sebelumnya ! <BR>"
            End If
        End If

        'If txtOid.Text = "" Then
        '    errMessage &= "- Tolong pilih No Tanda Terima ! <BR>"
        'End If
        'check if there is any data in GV Dtl Service Plan
        Dim objTableCek As DataTable = Session("GVDtlServicePlan")
        If objTableCek.Rows.Count = 0 Then
            errMessage &= "- [Tidak ada detail data, Tolong cek form Check Teknisi !]! <br>"
        End If

        For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
            If ToDouble(objTableCek.Rows(c1).Item("ITSERVTARIF")) <= 0 Then
                errMessage &= "- [Harga Tarif Pengerjaan harus lebih besar dari 0 !] <br>!"
            End If
        Next

        Dim objTableCek2 As DataTable = Session("GVDtlServicePlan2")

        For c2 As Int16 = 0 To objTableCek2.Rows.Count - 1
            If objTableCek2.Rows(c2).RowState <> DataRowState.Deleted Then
                If Double.Parse(objTableCek2.Rows(c2).Item("SPARTQTY")) <> 0.0 Then
                    If Double.Parse(objTableCek2.Rows(c2).Item("SPARTPRICE")) <= 0.0 Then
                        errMessage &= "- [Harga sparepart harus lebih besar dari 0 !]! <br>"
                    End If
                Else
                    errMessage &= "- [Quantity sparepart harus lebih besar dari 0 !]! <br>"
                End If
            End If
        Next


        If ToDouble(txtHargaServis.Text) <= 0 Then
            errMessage &= "- [Harga Servis harus lebih besar dari 0 !]! <br>"
        End If

        If errMessage <> "" Then
            showMessage(errMessage, 2)
            Exit Sub
        End If

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            txtPlanOid.Text = GenerateID("QL_trnservices", CompnyCode)
            '  txtStatus.Text = "In Process"
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)

            txtStatus.Text = "Finish"

            'If rbFlag.SelectedValue = "Out" Then
            '    txtStatus.Text = "Finish"
            '    'Update Trn Request, ubah status request menjadi closed
            '    sSql = "UPDATE QL_trnrequest SET " & _
            '       "REQSTATUS='" & txtStatus.Text.Trim & "', " & _
            '       "REQFLAG='" & rbFlag.SelectedValue & "' " & _
            '       "WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
            '    xCmd.CommandText = sSql
            '    xCmd.ExecuteNonQuery()
            'End If

            Dim wktjam As String = txtTglTarget.Text & " " & txtWaktuTarget.Text

            'mode insert
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                sSql = "INSERT into QL_trnservices (CMPCODE,SOID,MSTREQOID,SSTARTTIME,SENDTIME,STOTALPRICE,SSTATUS,SFLAG,CREATEUSER,CREATETIME,UPDUSER,UPDTIME,SPERSON,BRANCH_CODE) " & _
                "VALUES ('" & CompnyCode & "'," & txtPlanOid.Text & "," & txtOid.Text & ",'" & toDate(wktjam) & "','" & toDate(wktjam) & "' ," & ToDouble(txtHargaServis.Text) & ",'" & txtStatus.Text & "','" & rbFlag.SelectedValue & "','" & updUser.Text & "'," & "current_timestamp" & ",'" & updUser.Text & "',current_timestamp," & teknisioid.Text & ",'" & DdlCabang.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & txtPlanOid.Text & " where tablename = 'QL_trnservices' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ElseIf txti_u.Text = "Update" Then
                'mode update

                'delete QL_trnservicesitem / detail
                sSql = "Delete from QL_trnservicesitem where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnservicespart / detail
                sSql = "Delete from QL_trnservicespart where SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnservices SET SOID=" & txtPlanOid.Text & ", MSTREQOID=" & txtOid.Text & ",SSTARTTIME='" & toDate(wktjam) & "', SENDTIME='" & toDate(wktjam) & "',STOTALPRICE=" & ToDouble(txtHargaServis.Text) & ", SSTATUS='" & txtStatus.Text.Trim & "', SFLAG='" & rbFlag.SelectedValue & "', UPDUSER='" & updUser.Text & "', UPDTIME=" & "current_timestamp" & ", SPERSON=" & teknisioid.Text & " WHERE cmpcode='" & CompnyCode & "' and SOID=" & txtPlanOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'insert tabel QL_trnservicesitem / detail
            Dim objTable As New DataTable : Dim oidCounter As Integer = 0
            objTable = Session("GVDtlServicePlan")
            'GenerateID("QL_trnservicesitem", CompnyCode)
            'GenerateGenID2()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter = CInt(lbDtlOid.Text)
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT into QL_trnservicesitem (CMPCODE,SITEMOID, " & _
                "SOID,SEQ,SPARTNOTE,ITSERVOID,ITSERVTARIF,CREATEUSER,CREATETIME,UPDUSER,UPDTIME,BRANCH_CODE) " & _
                "VALUES ('" & CompnyCode & "', " & oidCounter & ", " & txtPlanOid.Text & ", " & objTable.Rows(c1).Item("SEQ") & ", '" & Tchar(objTable.Rows(c1).Item("SPARTNOTE")) & "', " & objTable.Rows(c1).Item("ITSERVOID") & ", " & ToDouble(objTable.Rows(c1).Item("ITSERVTARIF")) & ", '" & updUser.Text & "', " & "current_timestamp" & " , '" & updUser.Text & "',current_timestamp,'" & DdlCabang.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                oidCounter += 1
            Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter - 1 & " where tablename = 'QL_trnservicesitem' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'insert tabel QL_trnservicespart / detail
            Dim objTable2 As New DataTable
            Dim oidCounter2 As Integer = 0
            objTable2 = Session("GVDtlServicePlan2")
            'GenerateGenID3()
            sSql = "SELECT ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            lbDtlOid2.Text = (xCmd.ExecuteScalar + 1).ToString
            oidCounter2 = CInt(lbDtlOid2.Text)
            For c2 As Int16 = 0 To objTable2.Rows.Count - 1
                If Not objTable2.Rows(c2).RowState = DataRowState.Deleted Then
                    sSql = "INSERT into QL_trnservicespart (CMPCODE,SPARTOID,SOID,SPARTSEQ,SPARTMOID,SPARTQTY,SPARTPRICE,ITSERVOID,MPARTOID,CREATEUSER,CREATETIME,UPDUSER,UPDTIME,mtrlocoid,BRANCH_CODE) " & _
                    "VALUES ('" & CompnyCode & "'," & oidCounter2 & "," & txtPlanOid.Text & "," & objTable2.Rows(c2).Item("SPARTSEQ") & "," & objTable2.Rows(c2).Item("SPARTMOID") & "," & ToDouble(objTable2.Rows(c2).Item("SPARTQTY")) & "," & ToDouble(objTable2.Rows(c2).Item("SPARTPRICE")) & "," & objTable2.Rows(c2).Item("ITSERVOID") & "," & objTable2.Rows(c2).Item("MPARTOID") & ",'" & updUser.Text & "'," & "current_timestamp" & ",'" & updUser.Text & "'," & "current_timestamp," & objTable2.Rows(c2).Item("mtrloc") & ",'" & DdlCabang.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    oidCounter2 += 1
                End If
            Next
            sSql = "update QL_mstoid set lastoid=" & oidCounter2 - 1 & " where tablename = 'QL_trnservicespart' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'Update Trn Request, ubah status request menjadi closed
            sSql = "UPDATE QL_trnrequest SET REQSTATUS='" & txtStatus.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session.Remove("idPage")
        Response.Redirect("~\transaction\ServicePlan.aspx?awal=true")
    End Sub
#End Region
End Class
