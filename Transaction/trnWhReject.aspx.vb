Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnWhSupplier
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Procedure"

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Sub FillTextbox()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim fromlocationoid As Integer = 0
        Dim tolocationoid As Integer = 0
        Dim transdate As New Date

        sSql = "SELECT TOP 1 a.trnTrfFromReturNo, a.trntrfdate, a.trnTrfToReturNo, a.flag, b.mtrlocoid_from, b.mtrlocoid_to, a.Trntrfnote, a.status, a.updtime, a.upduser,fromBranch,toBranch FROM ql_trnTrfFromReturmst a INNER JOIN ql_trnTrfFromReturDtl b ON a.cmpcode = b.cmpcode AND a.trnTrfFromReturoid = b.trnTrfFromReturoid WHERE a.cmpcode = '" & cmpcode & "' AND a.trnTrfFromReturoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                transferno.Text = xreader("trnTrfFromReturNo")
                transdate = xreader("trntrfdate")
                transferdate.Text = Format(xreader("trntrfdate"), "dd/MM/yyyy")
                noref.Text = xreader("trnTrfToReturNo")
                pooid.Text = xreader("trnTrfToReturNo")
                rblflag.SelectedValue = xreader("flag")
                fromlocationoid = xreader("mtrlocoid_from")
                tolocationoid = xreader("mtrlocoid_to")
                note.Text = xreader("Trntrfnote")
                trnstatus.Text = xreader("status").ToString.Trim
                upduser.Text = xreader("upduser")
                FromBranch.SelectedValue = xreader("fromBranch")
                ToBranch.SelectedValue = xreader("tobranch")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
            End While
        Else
            showMessage("Missing transfer data >.<", 2)
        End If
        xreader.Close()

        rblflag_SelectedIndexChanged(Nothing, Nothing)

        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND a.genoid <> -10  ORDER BY a.gendesc"
        FillDDL(tolocation, sSql)

        fromlocation.SelectedValue = fromlocationoid
        tolocation.SelectedValue = tolocationoid

        sSql = "SELECT a.seq, a.refoid AS itemoid, b.itemdesc, b.merk, a.qty, a.unitoid AS satuan, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3, c.gendesc AS unit, d.gendesc AS unit1, e.gendesc AS unit2, f.gendesc AS unit3, a.trfdtlnote AS note, (case g.flag when 'Purchase' then ((select sum(qty_to) from QL_trntrfmtrmst p INNER JOIN QL_trntrfmtrdtl q ON p.cmpcode = q.cmpcode AND p.trfmtrmstoid = q.trfmtrmstoid where p.transferno = '" & Tchar(pooid.Text) & "' AND q.refoid = b.itemoid and q.refname = 'ql_mstitem') - (SELECT ISNULL(SUM(qty_to), 0) FROM ql_InTransferDtl v INNER JOIN ql_InTransfermst w ON v.cmpcode = w.cmpcode AND v.InTransferoid = w.InTransferoid WHERE w.trnTrfToReturNo = '" & Tchar(pooid.Text) & "' AND v.refoid = b.itemoid AND v.refname = 'ql_mstitem' and v.InTransferoid <> " & Integer.Parse(Session("oid")) & ") - (select isnull(sum(case ab.unitseq when 1 then (ab.trnbelireturdtlqty*cast(id.konversi1_2 as decimal(18,2))*cast(id.konversi2_3 as decimal(18,2))) when 2 then (ab.trnbelireturdtlqty*cast(id.konversi2_3 as decimal(18,2))) when 3 then (ab.trnbelireturdtlqty) end), 0) from QL_trnbeliReturdtl ab inner join QL_trnbeliReturmst cd on ab.cmpcode = cd.cmpcode and ab.trnbeliReturmstoid = cd.trnbeliReturmstoid inner join ql_mstitem id on ab.cmpcode = id.cmpcode and ab.itemoid = id.itemoid where cd.trnTrfToReturNo = '" & Tchar(pooid.Text) & "' and ab.itemoid = a.refoid and cd.trnbelistatus in ('Approved','POST'))) when 'Supplier' then (select saldoakhir-qtybooking from QL_crdmtr where a.refoid = refoid and mtrlocoid = " & fromlocationoid & " AND periodacctg  = '" & GetDateToPeriodAcctg(transdate).Trim & "') end) sisaretur FROM ql_trnTrfFromReturDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.unitoid = c.genoid INNER JOIN QL_mstgen d ON b.cmpcode = d.cmpcode AND b.satuan1 = d.genoid INNER JOIN QL_mstgen e ON b.cmpcode = e.cmpcode AND b.satuan2 = e.genoid INNER JOIN QL_mstgen f ON b.cmpcode = f.cmpcode AND b.satuan2 = f.genoid INNER JOIN ql_trnTrfFromReturmst g ON a.cmpcode = g.cmpcode AND a.trnTrfFromReturoid = g.trnTrfFromReturoid WHERE a.cmpcode = '" & cmpcode & "' AND a.trnTrfFromReturoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "detailts")
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        If trnstatus.Text = "IN PROCESS" Then
            btnSave.Visible = True
            btnDelete.Visible = True
            BtnCancel.Visible = True
            btnPosting.Visible = True
        ElseIf trnstatus.Text = "POST" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            BtnCancel.Visible = True
            btnPosting.Visible = False
        End If

        conn.Close()
    End Sub

    Private Sub BindData(ByVal state As String)
        Dim date1, date2 As New Date

        If state = 0 Then

            date1 = New Date(Date.Now.Year, Date.Now.Month, 1)
            date2 = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)

            sSql = "SELECT trnTrfFromReturoid, trnTrfFromReturNo, trntrfdate, upduser, status FROM ql_trnTrfFromReturmst WHERE cmpcode = '" & cmpcode & "' AND CONVERT(DATE, trntrfdate, 101) BETWEEN '" & date1 & "' AND '" & date2 & "' ORDER BY trnTrfFromReturNo"

        Else

            date1 = Date.ParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing)
            date2 = Date.ParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing)

            Dim swhere As String = ""
            Dim swhere1 As String = ""

            If group.SelectedValue <> "ALL" Then
                swhere &= " AND b.itemgroupoid = " & group.SelectedValue & ""
            End If

            If subgroup.SelectedValue <> "ALL" Then
                swhere &= " AND b.itemsubgroupoid = " & subgroup.SelectedValue & ""
            End If

            Dim searchitemoid As Integer = 0
            If itemoid.Text <> "" Then
                If Integer.TryParse(itemoid.Text, searchitemoid) = False Then
                    showMessage("Item tidak valid !", 2)
                    Exit Sub
                Else
                    swhere &= " AND a.refoid = " & Integer.Parse(itemoid.Text) & ""
                End If
            End If

            If FilterText.Text.Trim <> "" Then
                swhere1 &= " AND c.trnTrfFromReturNo LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
            End If

            sSql = "SELECT DISTINCT c.trnTrfFromReturoid, c.trnTrfFromReturNo, c.trntrfdate, c.upduser, c.status FROM ql_trnTrfFromReturDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM'" & swhere & " INNER JOIN ql_trnTrfFromReturmst c ON a.cmpcode = c.cmpcode AND a.trnTrfFromReturoid = c.trnTrfFromReturoid" & swhere1 & " WHERE CONVERT(DATE, c.trntrfdate, 101) BETWEEN '" & date1 & "' AND '" & date2 & "' ORDER BY c.trnTrfFromReturNo"

        End If

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "ts")
        gvMaster.DataSource = dtab
        gvMaster.DataBind()
        Session("ts") = dtab
    End Sub

    Private Sub BindItem(ByVal state As Integer)
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)

        'If state = 1 Then
        '    sSql = "select a.itemoid, a.itemcode, a.itemdesc, a.merk, (g.saldoakhir-g.qtybooking) as sisa, a.satuan1, d.gendesc AS unit1, a.satuan2, e.gendesc AS unit2, a.satuan3, f.gendesc AS unit3, a.konversi1_2, a.konversi2_3, h.unitoid, j.gendesc as unit, i.trnTrfToReturNo, ((select sum(qty_to) from ql_trnTrfToReturdtl where cmpcode = a.cmpcode and trnTrfToReturoid = i.trnTrfToReturoid and refoid = a.itemoid and refname = 'ql_mstitem') - (select isnull(sum(qty_to), 0) from ql_InTransferDtl m inner join ql_InTransfermst n on m.cmpcode = n.cmpcode and m.InTransferoid = n.InTransferoid and n.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and m.refoid = a.itemoid and m.refname = 'ql_mstitem'" & IIf(i_u.Text <> "new", " and m.InTransferoid <> " & Session("oid") & "", "") & ") - (select isnull(sum(case ab.unitseq when 1 then (ab.trnbelireturdtlqty*cast(id.konversi1_2 as decimal(18,2))*cast(id.konversi2_3 as decimal(18,2))) when 2 then (ab.trnbelireturdtlqty*cast(id.konversi2_3 as decimal(18,2))) when 3 then (ab.trnbelireturdtlqty) end), 0) from QL_trnbeliReturdtl ab inner join QL_trnbeliReturmst cd on ab.cmpcode = cd.cmpcode and ab.trnbeliReturmstoid = cd.trnbeliReturmstoid inner join ql_mstitem id on ab.cmpcode = id.cmpcode and ab.itemoid = id.itemoid where cd.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and ab.itemoid = a.itemoid and cd.trnbelistatus in ('Approved','POST'))) sisaretur from ql_mstitem a inner join QL_mstgen d on a.cmpcode = d.cmpcode and a.satuan1 = d.genoid inner join QL_mstgen e on a.cmpcode = e.cmpcode and a.satuan2 = e.genoid inner join QL_mstgen f on a.cmpcode = f.cmpcode and a.satuan3 = f.genoid inner join ql_crdmtr g on a.cmpcode = g.cmpcode and a.itemoid = g.refoid and g.refname = 'ql_mstitem' and g.mtrlocoid = " & fromlocation.SelectedValue & " and g.periodacctg  = '" & GetDateToPeriodAcctg(transdate).Trim & "' inner join ql_trnTrfToReturdtl h on a.cmpcode = h.cmpcode and a.itemoid = h.refoid and h.refname = 'ql_mstitem' inner join ql_trnTrfToReturmst i on h.cmpcode = i.cmpcode and h.trnTrfToReturoid = i.trnTrfToReturoid inner join QL_mstgen j on h.cmpcode = j.cmpcode and h.unitoid = j.genoid where a.cmpcode = 'jpt' and i.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and ((select sum(qty_to) from ql_trnTrfToReturdtl where cmpcode = a.cmpcode and trnTrfToReturoid = i.trnTrfToReturoid and refoid = a.itemoid and refname = 'ql_mstitem') - (select isnull(sum(qty_to), 0) from ql_InTransferDtl m inner join ql_InTransfermst n on m.cmpcode = n.cmpcode and m.InTransferoid = n.InTransferoid and n.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and m.refoid = a.itemoid and m.refname = 'ql_mstitem'" & IIf(i_u.Text <> "new", " and m.InTransferoid <> " & Session("oid") & "", "") & ") - (select isnull(sum(case ab.unitseq when 1 then (ab.trnbelireturdtlqty*cast(id.konversi1_2 as decimal(18,2))*cast(id.konversi2_3 as decimal(18,2))) when 2 then (ab.trnbelireturdtlqty*cast(id.konversi2_3 as decimal(18,2))) when 3 then (ab.trnbelireturdtlqty) end), 0) from QL_trnbeliReturdtl ab inner join QL_trnbeliReturmst cd on ab.cmpcode = cd.cmpcode and ab.trnbeliReturmstoid = cd.trnbeliReturmstoid inner join ql_mstitem id on ab.cmpcode = id.cmpcode and ab.itemoid = id.itemoid where cd.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and ab.itemoid = a.itemoid and cd.trnbelistatus in ('Approved','POST'))) > 0 and (a.itemdesc LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.itemcode LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.merk LIKE '%" & Tchar(item.Text.Trim) & "%')"
        sSql = "select a.itemoid, a.itemcode, a.itemdesc, a.merk, (g.saldoakhir-g.qtybooking) as sisa, a.satuan1, d.gendesc AS unit1, a.satuan2, e.gendesc AS unit2, a.satuan3, f.gendesc AS unit3, a.konversi1_2, a.konversi2_3, h.unitoid, j.gendesc as unit, i.transferno trnTrfToReturNo, " & _
"((select sum(qty_to) from QL_trntrfmtrdtl where cmpcode = a.cmpcode and trfmtrmstoid = i.trfmtrmstoid and refoid = a.itemoid and refname = 'ql_mstitem') - " & _
" (select isnull(sum(qty_to), 0) from ql_InTransferDtl m inner join ql_InTransfermst n on m.cmpcode = n.cmpcode and m.InTransferoid = n.InTransferoid and n.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and m.refoid = a.itemoid and m.refname = 'ql_mstitem'" & IIf(i_u.Text <> "new", " and m.InTransferoid <> " & Session("oid") & "", "") & ") - " & _
" ((select isnull(sum(i.qty_to), 0) from ql_trnTrfFromReturmst h inner join ql_trnTrfFromReturDtl i on h.cmpcode = i.cmpcode and h.trnTrfFromReturoid = i.trnTrfFromReturoid where h.trnTrfToReturNo  like '%" & Tchar(noref.Text) & "%' and i.refoid  = a.itemoid ))) sisaretur" & _
" from ql_mstitem a " & _
" inner join QL_mstgen d on a.cmpcode = d.cmpcode and a.satuan1 = d.genoid " & _
" inner join QL_mstgen e on a.cmpcode = e.cmpcode and a.satuan2 = e.genoid " & _
" inner join QL_mstgen f on a.cmpcode = f.cmpcode and a.satuan3 = f.genoid " & _
" inner join ql_crdmtr g on a.cmpcode = g.cmpcode and a.itemoid = g.refoid and g.refname = 'ql_mstitem' and g.mtrlocoid = " & fromlocation.SelectedValue & " and g.periodacctg  = '" & GetDateToPeriodAcctg(transdate).Trim & "' " & _
" inner join QL_trntrfmtrdtl h on a.cmpcode = h.cmpcode and a.itemoid = h.refoid and h.refname = 'ql_mstitem' " & _
" inner join QL_trntrfmtrmst i on h.cmpcode = i.cmpcode and h.trfmtrmstoid = i.trfmtrmstoid " & _
" inner join QL_mstgen j on h.cmpcode = j.cmpcode and h.unitoid = j.genoid " & _
" where a.cmpcode = '" & cmpcode & "' and i.transferno like '%" & Tchar(noref.Text) & "%' " & _
" and ((select sum(qty_to) from QL_trntrfmtrdtl where cmpcode = a.cmpcode and trfmtrmstoid = i.trfmtrmstoid and refoid = a.itemoid and refname = 'ql_mstitem') - " & _
" (select isnull(sum(qty_to), 0) from ql_InTransferDtl m inner join ql_InTransfermst n on m.cmpcode = n.cmpcode and m.InTransferoid = n.InTransferoid and n.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and m.refoid = a.itemoid and m.refname = 'ql_mstitem'" & IIf(i_u.Text <> "new", " and m.InTransferoid <> " & Session("oid") & "", "") & ") - " & _
" ((select isnull(sum(i.qty_to), 0) from ql_trnTrfFromReturmst h inner join ql_trnTrfFromReturDtl i on h.cmpcode = i.cmpcode and h.trnTrfFromReturoid = i.trnTrfFromReturoid where h.trnTrfToReturNo  like '%" & Tchar(noref.Text) & "%' and i.refoid  = a.itemoid ))) > 0 " & _
" and (a.itemdesc LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.itemcode LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.merk LIKE '%" & Tchar(item.Text.Trim) & "%')"
        'Else
        '    sSql = "SELECT a.itemoid, a.itemcode, a.itemdesc, a.merk, (e.saldoakhir-e.qtybooking) as sisa, a.satuan1, b.gendesc AS unit1, a.satuan2, c.gendesc AS unit2, a.satuan3, d.gendesc AS unit3, a.konversi1_2, a.konversi2_3, a.satuan3 unitoid, d.gendesc AS unit, '' trnTrfToReturNo, (e.saldoakhir-e.qtybooking) as sisaretur FROM QL_mstItem a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.satuan1 = b.genoid INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.satuan2 = c.genoid INNER JOIN QL_mstgen d ON a.cmpcode = d.cmpcode AND a.satuan3 = d.genoid INNER JOIN QL_crdmtr e ON a.cmpcode = e.cmpcode AND a.itemoid = e.refoid AND e.refname = 'QL_MSTITEM' AND e.mtrlocoid = " & fromlocation.SelectedValue & " AND e.periodacctg  = '" & GetDateToPeriodAcctg(transdate).Trim & "' WHERE a.itemflag = 'Aktif' AND (a.itemdesc LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.itemcode LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.merk LIKE '%" & Tchar(item.Text.Trim) & "%') AND a.cmpcode = '" & cmpcode & "'"
        'End If

        Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
        GVItemList.DataSource = objTable
        GVItemList.DataBind()

        Session("itemlistwhretur") = objTable
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Sub InitAllDDL()
        FillDDLGen("ITEMGROUP", group)
        group.Items.Add("ALL")
        group.SelectedValue = "ALL"
        FillDDLGen("ITEMSUBGROUP", subgroup)
        subgroup.Items.Add("ALL")
        subgroup.SelectedValue = "ALL"
        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang' " ' and gencode = '" & Session("branch_id") & "'"
        FillDDL(ToBranch, sSql)
        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang'  and gencode = '" & Session("branch_id") & "'"
        FillDDL(frombranch, sSql)


        'FillDDLGen("LOCATION", fromlocation)
        FillDDL(fromlocation, "SELECT a.genoid,  a.gendesc FROM QL_mstgen a where a.gengroup = 'LOCATION' and a.gencode = 'EXPEDISI' AND a.cmpcode='" & cmpcode & "' ORDER BY a.gendesc")
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If
        fromlocation.SelectedIndex = 0

        'FillDDLGen("LOCATION", tolocation, " genoid <> " & fromlocation.SelectedValue)
        FillDDL(tolocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND a.genoid <> " & fromlocation.SelectedValue & " ORDER BY a.gendesc")
        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If
        tolocation.SelectedIndex = 0
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()

            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch

            Response.Redirect("~\Transaction\trnWhReject.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Transfer Retur"

        If Not Page.IsPostBack Then
            BindData(0)
            transferno.Text = GenerateID("ql_trnTrfFromReturmst", cmpcode)
            transferdate.Text = Format(Date.Now, "dd/MM/yyyy")
            InitAllDDL()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(date1, "dd/MM/yyyy")
                tgl2.Text = Format(date2, "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0

                qty.Text = "0.00"
                GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                labelseq.Text = "1"
                labelkonversi1_2.Text = "1"
                labelkonversi2_3.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        BindData(0)

        FilterText.Text = ""
        Group.SelectedValue = "ALL"
        subgroup.SelectedValue = "ALL"
        Filteritem.Text = ""

        Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
        Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
        tgl1.Text = Format(date1, "dd/MM/yyyy")
        tgl2.Text = Format(date2, "dd/MM/yyyy")

        itemoid.Text = ""
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Tanggal period harus diisi !", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Tanggal period 1 tidak valid !", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Tanggal period 2 tidak valid !", 2)
            Exit Sub
        End If

        If date1 > date2 Then
            showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
            Exit Sub
        End If

        BindData(1)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Try
            If rblflag.SelectedValue.ToString = "Purchase" Then
                If noref.Text.Trim = "" Then
                    showMessage("Pilih nomor ref (TR) terlebih dahulu!", 2)
                    Exit Sub
                End If
                If noref.Text <> pooid.Text Then
                    showMessage("Pilih nomor ref (TR) terlebih dahulu!", 2)
                    Exit Sub
                End If
                BindItem(1)
            Else
                If fromlocation.Items.Count <= 0 Then
                    showMessage("Pilih gudang supplier terlebih dahulu!", 2)
                    Exit Sub
                End If
                BindItem(2)
            End If
            GVItemList.Visible = True
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = ""
        labelitemoid.Text = ""
        qty.Text = "0.00"
        labelmaxqty.Text = "0.00"
        merk.Text = ""
        notedtl.Text = ""

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        If Not Session("itemlistwhsupplier") Is Nothing Then
            GVItemList.DataSource = Session("itemlistwhsupplier")
            GVItemList.PageIndex = e.NewPageIndex
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        item.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(2).Text
        labelitemoid.Text = GVItemList.SelectedDataKey("itemoid")
        merk.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(3).Text
        'qty.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text
        'qty.Text = Format(GVItemList.SelectedDataKey("sisaretur"), "#,##0.00")
        labelmaxqty.Text = Format(GVItemList.SelectedDataKey("sisaretur"), "#,##0.00")
        labeltempsisaretur.Text = GVItemList.SelectedDataKey("sisaretur")

        labelsatuan.Text = GVItemList.SelectedDataKey("unitoid")
        labelsatuan1.Text = GVItemList.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemList.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemList.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemList.SelectedDataKey("unit1").ToString
        labelunit2.Text = GVItemList.SelectedDataKey("unit2").ToString
        labelunit3.Text = GVItemList.SelectedDataKey("unit3").ToString

        labelkonversi1_2.Text = GVItemList.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemList.SelectedDataKey("konversi2_3")

        unit.Items.Clear()
        unit.Items.Add(GVItemList.SelectedDataKey("unit3"))
        unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan3")
        If GVItemList.SelectedDataKey("satuan2") <> GVItemList.SelectedDataKey("satuan3") Then
            unit.Items.Add(GVItemList.SelectedDataKey("unit2"))
            unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan2")
        End If
        If GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan3") And GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan2") Then
            unit.Items.Add(GVItemList.SelectedDataKey("unit1"))
            unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan1")
        End If
        unit.SelectedValue = GVItemList.SelectedDataKey("unitoid")

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        GVItemList.Visible = False
        GVItemList.DataSource = Nothing
        GVItemList.DataBind()

        unit_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnWhReject.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        Dim qtyval As Double = 0.0
        If qty.Text.Trim <> "" Then
            If Double.TryParse(qty.Text.Trim, qtyval) Then
                qty.Text = Format(qtyval, "#,##0.00")
            Else
                qty.Text = Format(0.0, "#,##0.00")
            End If
        Else
            qty.Text = Format(0.0, "#,##0.00")
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            showMessage("Item tidak boleh kosong!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) = 0.0 Then
            showMessage("Qty harus lebih dari 0!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) > Double.Parse(labelmaxqty.Text.Trim) Then
            showMessage("Qty tidak boleh lebih dari sisa qty dari purchase retur!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable

        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("unit1", Type.GetType("System.String"))
                dtab.Columns.Add("unit2", Type.GetType("System.String"))
                dtab.Columns.Add("unit3", Type.GetType("System.String"))
                dtab.Columns.Add("note", Type.GetType("System.String"))
                dtab.Columns.Add("sisaretur", Type.GetType("System.Double"))
                Session("itemdetail") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetail")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "' AND seq <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list !", 2)
                Exit Sub
            End If
        End If

        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow

            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = Unit.SelectedValue
            drow("unit") = Unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim
            drow("sisaretur") = Double.Parse(labeltempsisaretur.Text)

            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()

            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = Unit.SelectedValue
            drow("unit") = Unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim
            drow("sisaretur") = Double.Parse(labeltempsisaretur.Text)

            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
        merk.Text = ""
        qty.Text = "0.00"
        labelmaxqty.Text = "0.00"
        Unit.Items.Clear()
        notedtl.Text = ""
        labelitemoid.Text = ""
        item.Text = ""
        I_u2.Text = "new"

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new"
        labelitemoid.Text = ""
        qty.Text = "0.00"
        item.Text = ""
        labelmaxqty.Text = "0.00"
        Unit.Items.Clear()
        notedtl.Text = ""
        merk.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        item.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(2).Text
        qty.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(4).Text

        notedtl.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(6).Text.Replace("&nbsp;", "")
        merk.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(3).Text

        Unit.Items.Clear()
        Unit.Items.Add(GVItemDetail.SelectedDataKey("unit3"))
        Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan3")
        If GVItemDetail.SelectedDataKey("satuan2") <> GVItemDetail.SelectedDataKey("satuan3") Then
            Unit.Items.Add(GVItemDetail.SelectedDataKey("unit2"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan2")
        End If
        If GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan3") And GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan2") Then
            Unit.Items.Add(GVItemDetail.SelectedDataKey("unit1"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan1")
        End If
        Unit.SelectedValue = GVItemDetail.SelectedDataKey("satuan")

        labelsatuan1.Text = GVItemDetail.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemDetail.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemDetail.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemDetail.SelectedDataKey("unit1")
        labelunit2.Text = GVItemDetail.SelectedDataKey("unit2")
        labelunit3.Text = GVItemDetail.SelectedDataKey("unit3")

        labelkonversi1_2.Text = GVItemDetail.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemDetail.SelectedDataKey("konversi2_3")

        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text

        labeltempsisaretur.Text = Format(GVItemDetail.SelectedDataKey("sisaretur"))

        Dim maxqty As Double = 0.0
        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi1_2.Text) / Double.Parse(labelkonversi2_3.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text)
        Else
            maxqty = Double.Parse(labeltempsisaretur.Text)
        End If
        labelmaxqty.Text = Format(maxqty, "#,##0.00")

        'Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        'conn.Open()
        'sSql = "SELECT saldoakhir from QL_crdmtr WHERE refname = 'QL_MSTITEM' AND refoid = " & Integer.Parse(labelitemoid.Text) & " AND periodacctg = '" & GetDateToPeriodAcctg(Date.Now).Trim & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND cmpcode = '" & cmpcode & "'"
        'xCmd.CommandText = sSql
        'Dim maxqty As Double = xCmd.ExecuteScalar
        'If Not maxqty = Nothing Then
        '    If GVItemDetail.SelectedDataKey("satuan") = GVItemDetail.SelectedDataKey("satuan1") Then
        '        maxqty = maxqty / GVItemDetail.SelectedDataKey("konversi1_2")
        '    End If
        '    labelmaxqty.Text = Format(maxqty, "#,##0.00")
        'Else
        '    labelmaxqty.Text = "0.00"
        'End If
        'conn.Close()

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        GVItemDetail.Columns(7).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""

        If fromlocation.Items.Count <= 0 Then
            errmsg &= "Lokasi asal transfer tidak boleh kosong !"
        End If
        If tolocation.Items.Count <= 0 Then
            errmsg &= "Lokasi tujuan transfer tidak boleh kosong !"
        End If
        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "Detail transfer tidak boleh kosong !"
        End If

        If errmsg.Trim <> "" Then
            showMessage(errmsg, 2)
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            Dim period As String = GetDateToPeriodAcctg(transdate).Trim

            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = 0 Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Tanggal ini tidak dalam periode Open Stock !", 2)
                Exit Sub
            End If

            'Cek saldoakhir
            If Not Session("itemdetail") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("itemdetail")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "SELECT saldoAkhir-qtybooking FROM QL_crdmtr WHERE periodacctg = '" & period & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND refoid = " & dtab.Rows(j).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql
                    saldoakhire = xCmd.ExecuteScalar
                    If saldoakhire = Nothing Or saldoakhire = 0 Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir satuan std hanya tersedia 0.00 !", 2)
                        Exit Sub
                    Else
                        If dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan3") Then
                            If saldoakhire - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                Exit Sub
                            End If
                        ElseIf dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan2") Then
                            If (saldoakhire / dtab.Rows(j).Item("konversi2_3")) - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                Exit Sub
                            End If
                        ElseIf dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan1") Then
                            If (saldoakhire / dtab.Rows(j).Item("konversi2_3") / dtab.Rows(j).Item("konversi1_2")) - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                Exit Sub
                            End If
                        End If
                    End If
                Next

                'skip dulu validasi ulang stock , next update
                'If trnstatus.Text = "POST" Then
                '    If rblflag.SelectedValue.ToString = "Purchase" Then
                '        For j As Integer = 0 To dtab.Rows.Count - 1
                '            sSql = "select (select sum(qty_to) from ql_trnTrfToReturdtl where cmpcode = a.cmpcode and trnTrfToReturoid = i.trnTrfToReturoid and refoid = a.itemoid and refname = 'ql_mstitem') - (select isnull(sum(qty_to), 0) from ql_InTransferDtl m inner join ql_InTransfermst n on m.cmpcode = n.cmpcode and m.InTransferoid = n.InTransferoid and n.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and m.refoid = a.itemoid and m.refname = 'ql_mstitem') - (select isnull(sum(case ab.unitseq when 1 then (ab.trnbelireturdtlqty*cast(id.konversi1_2 as decimal(18,2))*cast(id.konversi2_3 as decimal(18,2))) when 2 then (ab.trnbelireturdtlqty*cast(id.konversi2_3 as decimal(18,2))) when 3 then (ab.trnbelireturdtlqty) end), 0) from QL_trnbeliReturdtl ab inner join QL_trnbeliReturmst cd on ab.cmpcode = cd.cmpcode and ab.trnbeliReturmstoid = cd.trnbeliReturmstoid inner join ql_mstitem id on ab.cmpcode = id.cmpcode and ab.itemoid = id.itemoid where cd.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and ab.itemoid = a.itemoid and cd.trnbelistatus in ('Approved','POST')) from ql_mstitem a inner join ql_trnTrfToReturdtl h on a.cmpcode = h.cmpcode and a.itemoid = h.refoid and h.refname = 'ql_mstitem' inner join ql_trnTrfToReturmst i on h.cmpcode = i.cmpcode and h.trnTrfToReturoid = i.trnTrfToReturoid where i.trnTrfToReturNo = '" & Tchar(noref.Text) & "' and a.itemoid=" & dtab.Rows(j).Item("itemoid") & ""
                '            xCmd.CommandText = sSql
                '            If xCmd.ExecuteScalar < 0 Then
                '                objTrans.Rollback()
                '                conn.Close()
                '                showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena jumlah transfer gudang supplier melebihi jumlah transfer gudang retur!", 2)
                '                Exit Sub
                '            End If
                '        Next
                '    End If
                'End If
            End If

            'Generate crdmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

            'Generate conmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim crdmtroid As Int32 = xCmd.ExecuteScalar + 1
            Dim tempoid As Int32 = crdmtroid
            'Generate trnglmst ID
            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim glmstoid As Integer = xCmd.ExecuteScalar + 1

            'Generate trngldtl ID
            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trngldtl' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim gldtloid As Integer = xCmd.ExecuteScalar + 1
            Dim glsequence As Integer = 1


            Dim trfmtrmstoid As Integer = 0
            If i_u.Text = "new" Then
                'Generate transfer master ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'ql_trnTrfFromReturmst' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                trfmtrmstoid = xCmd.ExecuteScalar + 1
            End If

            'Generate transfer number
            If trnstatus.Text = "POST" Then
                Dim trnno As String = "TR/" & Format(transdate, "yy") & "/" & Format(transdate, "MM") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnTrfFromReturNo, 4) AS INT)), 0) FROM ql_trnTrfFromReturmst WHERE cmpcode = '" & cmpcode & "' AND trnTrfFromReturNo LIKE '" & trnno & "%'"
                xCmd.CommandText = sSql
                trnno = trnno & Format(xCmd.ExecuteScalar + 1, "0000")
                transferno.Text = trnno
            Else
                If i_u.Text = "new" Then
                    transferno.Text = trfmtrmstoid.ToString
                End If
            End If

            'Generate transfer detail ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'ql_trnTrfFromReturDtl' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim trfmtrdtloid As Int32 = xCmd.ExecuteScalar + 1

            'Insert new record
            If i_u.Text = "new" Then

                'Insert to master
                sSql = "INSERT INTO ql_trnTrfFromReturmst (cmpcode, trnTrfFromReturoid, trnTrfFromReturNo, trnTrfToReturNo, trntrfdate, personoid, Trntrfnote, updtime, upduser, finalappovaldatetime, finalapprovaluser, finalapprovalcode, canceluser, canceltime, cancelnote, flag, status,FromBranch,ToBranch) VALUES ('" & cmpcode & "', " & trfmtrmstoid & ", '" & transferno.Text & "', '" & Tchar(pooid.Text) & "', '" & transdate & "', 0, '" & Tchar(note.Text.Trim) & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', '', '', '1/1/1900', '', '" & rblflag.SelectedValue & "', '" & trnstatus.Text & "','" & FromBranch.SelectedValue & "','" & ToBranch.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_trntrfmtrmst
                sSql = "UPDATE QL_mstoid SET lastoid = " & trfmtrmstoid & " WHERE tablename = 'ql_trnTrfFromReturmst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else

                sSql = "SELECT status FROM ql_trnTrfFromReturmst WHERE trnTrfFromReturoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                Dim srest As String = xCmd.ExecuteScalar
                If srest Is Nothing Or srest = "" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer gudang supplier tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                    Exit Sub
                Else
                    If srest = "POST" Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Data transfer gudang supplier tidak dapat diposting !<br />Periksa bila data telah diposting oleh user lain", 2)
                        Exit Sub
                    End If
                End If

                'Update master record
                sSql = "UPDATE ql_trnTrfFromReturmst SET trnTrfFromReturNo = '" & transferno.Text & "', trnTrfToReturNo = '" & Tchar(pooid.Text) & "', Trntrfnote = '" & Tchar(note.Text.Trim) & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP, finalapprovaluser = '" & Session("UserID") & "', flag = '" & rblflag.SelectedValue & "', status = '" & trnstatus.Text & "' , frombranch = '" & FromBranch.SelectedValue & "' , tobranch = '" & ToBranch.SelectedValue & "' WHERE trnTrfFromReturoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM ql_trnTrfFromReturDtl WHERE trnTrfFromReturoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If

            '-------------------auto jurnal jika transfer gudang supplier ----------------
            If trnstatus.Text = "POST" Then
                If rblflag.SelectedValue = "Supplier" Then
                    'Jurnal Supplier cz Sedian Supplier lawan Sedian Grosir
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime) VALUES ('" & cmpcode & "', " & glmstoid & ", '" & transdate & "', '" & period & "', 'Transfer Gudang Supplier No. " & transferno.Text & "', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid = " & glmstoid & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

            End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0 : Dim qty_to As Double = 0.0
                Dim amthpp As Double = 0.0

                For i As Integer = 0 To objTable.Rows.Count - 1

                    If i_u.Text = "edit" Then
                        trfmtrmstoid = Integer.Parse(Session("oid"))
                    End If

                    If objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan1") Then
                        unitseq = 1
                        qty_to = objTable.Rows(i).Item("qty") * objTable.Rows(i).Item("konversi1_2") * objTable.Rows(i).Item("konversi2_3")
                    ElseIf objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan2") Then
                        unitseq = 2
                        qty_to = objTable.Rows(i).Item("qty") * objTable.Rows(i).Item("konversi2_3")
                    ElseIf objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan3") Then
                        unitseq = 3
                        qty_to = objTable.Rows(i).Item("qty")
                    End If

                    sSql = "INSERT INTO ql_trnTrfFromReturDtl (cmpcode, trnTrfFromReturDtloid, trnTrfFromReturoid, seq, refname, refoid, trfdtlnote, mtrlocoid_from, mtrlocoid_to, unitseq, qty, unitoid, unitseq_to, qty_to, unitoid_to) VALUES ('" & cmpcode & "', " & trfmtrdtloid & ", " & trfmtrmstoid & ", " & i + 1 & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("itemoid") & ", '" & Tchar(objTable.Rows(i).Item("note")) & "', " & fromlocation.SelectedValue & ", " & tolocation.SelectedValue & ", " & unitseq & ", " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan") & ", 3, " & qty_to & ", " & objTable.Rows(i).Item("satuan3") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If trnstatus.Text = "POST" Then
                        '--------------------------
                        Dim lasthpp As Double = 0
                        sSql = "select hpp  from ql_mstitem where itemoid=" & objTable.Rows(i).Item("itemoid") & " "
                        xCmd.CommandText = sSql
                        lasthpp = xCmd.ExecuteScalar
                        '---------------------------
                        amthpp += qty_to * lasthpp
                        'Update crdmtr for item out
                        sSql = "UPDATE QL_crdmtr SET qtyout = qtyout + " & qty_to & ", saldoakhir = saldoakhir - " & qty_to & ", LastTransType = 'TR', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & cmpcode & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND refoid = " & objTable.Rows(i).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "' and branch_Code = '" & FromBranch.SelectedValue & "' "
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,Branch_Code) VALUES ('" & cmpcode & "', " & conmtroid & ", 'TR', '" & transdate & "', '" & period & "', '" & transferno.Text & "', " & trfmtrdtloid & ", 'ql_trnTrfFromReturDtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & fromlocation.SelectedValue & ", 0, " & objTable.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & ToDouble(lasthpp) & ",'" & FromBranch.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1

                        'Update crdmtr for item in
                        sSql = "UPDATE QL_crdmtr SET qtyin = qtyin + " & qty_to & ", saldoakhir = saldoakhir + " & qty_to & ", LastTransType = 'TR', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & cmpcode & "' AND mtrlocoid = " & tolocation.SelectedValue & " AND refoid = " & objTable.Rows(i).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "' and Branch_Code = '" & ToBranch.SelectedValue & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() <= 0 Then
                            'Insert crdmtr if no record found
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser,Branch_Code) VALUES ('" & cmpcode & "', " & crdmtroid & ", '" & period & "', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & tolocation.SelectedValue & ", " & qty_to & ", 0, 0, 0, 0, " & qty_to & ", 0, 'TR', '" & transdate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', ''," & ToBranch.SelectedValue & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            crdmtroid += 1
                        End If

                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,Branch_Code) VALUES ('" & cmpcode & "', " & conmtroid & ", 'TR', '" & transdate & "', '" & period & "', '" & transferno.Text & "', " & trfmtrdtloid & ", 'ql_trnTrfFromReturDtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & tolocation.SelectedValue & ", " & objTable.Rows(i).Item("qty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & ToDouble(lasthpp) & ",'" & ToBranch.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                    End If
                    trfmtrdtloid += 1
                Next

                If trnstatus.Text = "POST" Then
                    If rblflag.SelectedValue = "Supplier" Then
                        'masukkan jurnal
                        '// ======================================
                        '//   Persediaan Barang Supplier    10.000
                        '//   persediaan barang Grosir      10.000
                        '//=======================================
                        '=================================================================
                        '=======================AUTO JURNAL START ========================
                        'insert ke jurnal
                        Dim iMatAccount As String = GetInterfaceValue("VAR_GUDANG")
                        Dim acctgoid As Integer = GetAccountOid(iMatAccount)

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & cmpcode & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & acctgoid & ", 'D', " & ToDouble(amthpp) & ", '" & transferno.Text & "', 'Transfer Gudang Supp No. " & transferno.Text & "', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        gldtloid = gldtloid + 1 : glsequence = glsequence + 1

                        Dim iMatSuppAccount As String = GetInterfaceValue("VAR_GUDANG_SUPP")
                        acctgoid = GetAccountOid(iMatSuppAccount)
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & cmpcode & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & acctgoid & ", 'C', " & ToDouble(amthpp) & ", '" & transferno.Text & "', 'Transfer Gudang Supp No. " & transferno.Text & "', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        gldtloid = gldtloid + 1 : glsequence = glsequence + 1

                        sSql = "UPDATE QL_mstoid SET lastoid =" & gldtloid - 1 & "  WHERE tablename = 'QL_trngldtl' and cmpcode = '" & cmpcode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                'Update lastoid ql_trnTrfFromReturDtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & trfmtrdtloid - 1 & "  WHERE tablename = 'ql_trnTrfFromReturDtl' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_crdmtr
                If tempoid <> crdmtroid Then
                    sSql = "UPDATE QL_mstoid SET lastoid =" & crdmtroid - 1 & "  WHERE tablename = 'QL_crdmtr' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnWhReject.aspx?awal=true")
    End Sub

    Protected Sub unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unit.SelectedIndexChanged

        Dim maxqty As Double = 0.0
        'If unit.SelectedValue <> Integer.Parse(labelsatuan.Text) Then
        '    If Integer.Parse(labelsatuan.Text) = Integer.Parse(labelsatuan1.Text) Then
        '        If unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
        '            maxqty = Double.Parse(labeltempsisaretur.Text) * Double.Parse(labelkonversi1_2.Text)
        '        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan3.Text) Then
        '            maxqty = Double.Parse(labeltempsisaretur.Text) * Double.Parse(labelkonversi1_2.Text) * Double.Parse(labelkonversi2_3.Text)
        '        Else
        '            maxqty = Double.Parse(labeltempsisaretur.Text)
        '        End If
        '    ElseIf Integer.Parse(labelsatuan.Text) = Integer.Parse(labelsatuan2.Text) Then
        '        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
        '            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi1_2.Text)
        '        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan3.Text) Then
        '            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text) / Double.Parse(labelkonversi1_2.Text)
        '        Else
        '            maxqty = Double.Parse(labeltempsisaretur.Text)
        '        End If
        '    ElseIf Integer.Parse(labelsatuan.Text) = Integer.Parse(labelsatuan3.Text) Then
        '        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
        '            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text) / Double.Parse(labelkonversi1_2.Text)
        '        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
        '            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text)
        '        Else
        '            maxqty = Double.Parse(labeltempsisaretur.Text)
        '        End If
        '    End If
        'Else
        '    maxqty = Double.Parse(labeltempsisaretur.Text)
        'End If

        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text) / Double.Parse(labelkonversi1_2.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text)
        Else
            maxqty = Double.Parse(labeltempsisaretur.Text)
        End If

        labelmaxqty.Text = Format(maxqty, "#,##0.00")

        'Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        'conn.Open()

        'sSql = "SELECT a.saldoakhir, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3 from QL_crdmtr a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' WHERE a.refoid = " & Integer.Parse(labelitemoid.Text) & " AND a.periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "' AND a.mtrlocoid = " & fromlocation.SelectedValue & " AND a.cmpcode = '" & cmpcode & "'"
        'xCmd.CommandText = sSql
        'xreader = xCmd.ExecuteReader
        'Dim maxqty As Double = 0.0
        'If xreader.HasRows Then
        '    While xreader.Read
        '        If Unit.SelectedValue = xreader("satuan2") Then
        '            maxqty = xreader("saldoakhir") / xreader("konversi2_3")
        '        ElseIf Unit.SelectedValue = xreader("satuan1") Then
        '            maxqty = xreader("saldoakhir") / xreader("konversi1_2") / xreader("konversi2_3")
        '        End If
        '    End While
        'End If
        'labelmaxqty.Text = Format(maxqty, "#,##0.00")

        'conn.Close()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT status FROM ql_trnTrfFromReturmst WHERE trnTrfFromReturoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transfer gudang retur tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                Exit Sub
            Else
                If srest = "POST" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer gudang retur tidak dapat dihapus !<br />Periksa bila data telah diposting oleh user lain", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM ql_trnTrfFromReturmst WHERE trnTrfFromReturoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trnTrfFromReturDtl WHERE trnTrfFromReturoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnWhReject.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        trnstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub fromlocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromlocation.SelectedIndexChanged
        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND a.genoid <> " & fromlocation.SelectedValue & " ORDER BY a.gendesc"
        FillDDL(tolocation, sSql)

        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        I_u2.Text = "new"
        labelitemoid.Text = ""
        qty.Text = "0.00"
        item.Text = ""
        labelmaxqty.Text = "0.00"
        Unit.Items.Clear()
        notedtl.Text = ""
        merk.Text = ""
        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"
        labelseq.Text = 1

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing

        btnClear_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        sSql = "SELECT itemoid, itemcode, itemdesc FROM QL_mstitem WHERE cmpcode = '" & cmpcode & "' AND (itemcode LIKE '%" & Tchar(Filteritem.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(Filteritem.Text.Trim) & "%') AND itemflag = 'Aktif'"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "mstitem")
        GVItemSearch.DataSource = dtab
        GVItemSearch.DataBind()

        GVItemSearch.Visible = True
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        Filteritem.Text = ""
        itemoid.Text = ""

        GVItemSearch.DataSource = Nothing
        GVItemSearch.DataBind()

        GVItemSearch.Visible = False
    End Sub

    Protected Sub GVItemSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemSearch.PageIndexChanging
        sSql = "SELECT itemoid, itemcode, itemdesc FROM QL_mstitem WHERE cmpcode = '" & cmpcode & "' AND (itemcode LIKE '%" & Tchar(Filteritem.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(Filteritem.Text.Trim) & "%') AND itemflag = 'Aktif'"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "mstitem")
        GVItemSearch.DataSource = dtab
        GVItemSearch.PageIndex = e.NewPageIndex
        GVItemSearch.DataBind()
    End Sub

    Protected Sub GVItemSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemSearch.SelectedIndexChanged
        Filteritem.Text = GVItemSearch.Rows(GVItemSearch.SelectedIndex).Cells(2).Text
        itemoid.Text = GVItemSearch.SelectedDataKey("itemoid")

        GVItemSearch.DataSource = Nothing
        GVItemSearch.DataBind()

        GVItemSearch.Visible = False
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'showMessage("Print out not available yet !", 2)
            'Exit Sub

            Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")

            Dim sWhere As String = " WHERE a.trnTrfFromReturoid = " & Integer.Parse(labeloid.Text) & ""

            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "TWSprintout.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", sWhere)
            report.PrintOptions.PaperSize = PaperSize.PaperA5

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TR_" & labeloid.Text & "")

            report.Close() : report.Dispose()

            Response.Redirect("trnWhReject.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        If Not Session("ts") Is Nothing Then
            Dim dtab As DataTable = Session("ts")
            gvMaster.DataSource = dtab
            gvMaster.PageIndex = e.NewPageIndex
            gvMaster.DataBind()
        Else
            showMessage("Missing something !", 2)
            Exit Sub
        End If
    End Sub
#End Region

    Protected Sub ibposearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposearch.Click
        bindDataPO()
        GVpo.Visible = True
    End Sub

    Protected Sub ibpoerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoerase.Click
        noref.Text = ""
        pooid.Text = ""

        If GVpo.Visible = True Then
            GVpo.Visible = False
            GVpo.DataSource = Nothing
            GVpo.DataBind()
            Session("polistwhsupplier") = Nothing
        End If

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Private Sub bindDataPO()
        sSql = "select a.transferno  trnTrfToReturNo, a.trfmtrdate  trntrfdate,trfmtrnote,ToMtrlocOid,FromMtrLocoid,fromMtrBranch,toMtrBranch ,(select sum(qty_to) from QL_trntrfmtrdtl where cmpcode = a.cmpcode and trfmtrmstoid = a.trfmtrmstoid) totalqty, ((select isnull(sum(i.qty_to), 0) from ql_trnTrfFromReturmst h inner join ql_trnTrfFromReturDtl i on h.cmpcode = i.cmpcode and h.trnTrfFromReturoid = i.trnTrfFromReturoid where h.trnTrfToReturNo = a.transferno)+(select isnull(sum(i.qty_to), 0) from ql_InTransfermst h inner join ql_InTransferDtl i on h.cmpcode = i.cmpcode and h.InTransferoid = i.InTransferoid where h.trnTrfToReturNo = a.transferno)) totaluse " & _
" from QL_trntrfmtrmst a " & _
" where a.cmpcode = '" & cmpcode & "' and a.status = 'Approved' and  fromMtrBranch = '" & Session("branch_id") & "' " & _
" and ((select sum(qty_to) from QL_trntrfmtrdtl where cmpcode = a.cmpcode and trfmtrmstoid  = a.trfmtrmstoid)-((select isnull(sum(i.qty_to), 0) from ql_trnTrfFromReturmst h inner join ql_trnTrfFromReturDtl i on h.cmpcode = i.cmpcode and h.trnTrfFromReturoid = i.trnTrfFromReturoid where h.trnTrfToReturNo = a.transferno)+(select isnull(sum(i.qty_to), 0) from ql_InTransfermst h inner join ql_InTransferDtl i on h.cmpcode = i.cmpcode and h.InTransferoid = i.InTransferoid where h.trnTrfToReturNo = a.transferno))) > 0 " & _
" and a.transferno like '%" & Tchar(noref.Text) & "%'"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "polist")
        GVpo.DataSource = dtab
        GVpo.DataBind()

        Session("polistwhsupplier") = dtab
    End Sub

    Protected Sub GVpo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVpo.PageIndexChanging
        If Not Session("polistwhsupplier") Is Nothing Then
            GVpo.DataSource = Session("polistwhsupplier")
            GVpo.PageIndex = e.NewPageIndex
            GVpo.DataBind()
        End If
    End Sub

    Protected Sub GVpo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVpo.SelectedIndexChanged
        noref.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        pooid.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        tolocation.SelectedValue = GVpo.SelectedDataKey("FromMtrlocOid")
        ToBranch.SelectedValue = GVpo.SelectedDataKey("FromMtrBranch")
        GVpo.Visible = False
        GVpo.DataSource = Nothing
        GVpo.DataBind()
        Session("polistwhsupplier") = Nothing

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub rblflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblflag.SelectedIndexChanged
        If rblflag.SelectedValue = "Purchase" Then
            FillDDL(fromlocation, "SELECT a.genoid,  a.gendesc FROM QL_mstgen a where a.gengroup = 'LOCATION' and a.gencode = 'EXPEDISI' AND a.cmpcode='" & cmpcode & "' ORDER BY a.gendesc")
            If fromlocation.Items.Count = 0 Then
                showMessage("Please create Location data!", 3)
                Exit Sub
            End If

            labelnotr.Visible = True
            noref.Visible = True
            ibposearch.Visible = True
            ibpoerase.Visible = True

            unit.CssClass = "inpTextDisabled"
            unit.Enabled = False

            labelfromloc.Visible = False
            fromlocation.Visible = False
        Else
            FillDDL(fromlocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'SUPPLIER' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' ORDER BY a.gendesc")
            If fromlocation.Items.Count = 0 Then
                showMessage("Please create Location data!", 3)
                Exit Sub
            End If

            labelnotr.Visible = False
            noref.Visible = False
            ibposearch.Visible = False
            ibpoerase.Visible = False

            unit.CssClass = "inpText"
            unit.Enabled = True

            labelfromloc.Visible = True
            fromlocation.Visible = True
        End If
        fromlocation.SelectedIndex = 0
        GVItemList.Visible = False
        GVpo.Visible = False

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
    End Sub
End Class
