<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnnotajual.aspx.vb" Inherits="Transaction_trnnotajual" enableViewStateMac="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Sales Invoice"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w31" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="DdlCabang" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w32"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w33">
                                                        <asp:ListItem Value="trnjualno">No Invoice</asp:ListItem>
                                                        <asp:ListItem Value="custname">Customer</asp:ListItem>
                                                        <asp:ListItem Value="a.orderno">No SO</asp:ListItem>
                                                        <asp:ListItem Value="a.trnsjjualno">No SJ</asp:ListItem>
                                                    </asp:DropDownList> <asp:TextBox id="txtFilter" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w34" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w35" AutoPostBack="False"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w36" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37">
                                                    </asp:ImageButton> <asp:Label id="Label169" runat="server" Text="to" __designer:wfdid="w38"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w39"></asp:TextBox> <asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w40">
                                                    </asp:ImageButton> <asp:Label id="Label179" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w41"></asp:Label> </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w42">
                                                    </asp:CheckBox> </TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w43">
                                                        <asp:ListItem Value="All">All</asp:ListItem>
                                                        <asp:ListItem>In Posting</asp:ListItem>
                                                        <asp:ListItem>Post</asp:ListItem>
                                                    </asp:DropDownList> <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w44">
                                                    </asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w45">
                                                    </asp:ImageButton> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w46" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbExportXLS" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Company" runat="server" CssClass="Important" __designer:wfdid="w48" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left colSpan=4><asp:Label id="InvoiceID" runat="server" CssClass="Important" __designer:wfdid="w49"></asp:Label> <ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w50" TargetControlID="FilterPeriod1" PopupButtonID="imbPeriod1" Format="dd/MM/yyyy">
                                                    </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w51" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999">
                                                    </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w52" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999">
                                                    </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w53" TargetControlID="FilterPeriod2" PopupButtonID="imbPeriod2" Format="dd/MM/yyyy">
                                                    </ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="Tbldata" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w54" DataKeyNames="trnjualmstoid,branch_code" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowPaging="True" PageSize="8" EnableModelValidation="True" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="No. Nota SI">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Date" SortExpression="trnjualdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="No. Nota SO" SortExpression="identifierno">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualno" HeaderText="No. Nota DO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderref" HeaderText="Note" SortExpression="trnorderref">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualstatus" HeaderText="Status" SortExpression="trnjualstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="currencycode" HeaderText="Currency" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="SI"><ItemTemplate>
<asp:ImageButton id="imbPrintInvoice" onclick="imbPrintInvoice_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" AlternateText="Print Invoice" __designer:wfdid="w11" ToolTip='<%# Eval("trnjualmstoid") %>' CommandArgument='<%# Eval("trnjualno") %>' CommandName='<%# Eval("branch_code") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="currencyoid" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="DO"><ItemTemplate>
<asp:ImageButton id="imbPrintDeliveryOrder" onclick="imbPrintDeliveryOrder_Click" runat="server" ImageUrl="~/Images/print.gif" AlternateText="Print Invoice" __designer:wfdid="w14" ToolTip='<%# Eval("trnjualmstoid") %>' CommandArgument='<%# Eval("trnjualno") %>' CommandName='<%# Eval("branch_code") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="XLS"><ItemTemplate>
<asp:CheckBox id="chkXls" runat="server" Enabled="<%# GetEnabled() %>" __designer:wfdid="w12" ToolTip='<%# Eval("trnjualmstoid") %>' Checked="<%# GetSelected() %>"></asp:CheckBox> <asp:ImageButton id="imbXLS" onclick="imbXLS_Click" runat="server" ImageUrl="~/Images/excelexport.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w13" ToolTip='<%# Eval("trnjualno") %>' CommandArgument='<%# Eval("trnjualmstoid") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="imbResetCounter" onclick="imbResetCounter_Click" runat="server" ImageUrl="~/Images/timer.png" ImageAlign="AbsMiddle" __designer:wfdid="w17" ToolTip="<%# GetToolTipCounter() %>" CommandArgument='<%# Eval("trnjualmstoid") & "," & Eval("branch_code") %>'></asp:ImageButton><ajaxToolkit:ConfirmButtonExtender id="cfbReset" runat="server" ConfirmText="Are you sure to Reset Print Counter?" TargetControlID="imbResetCounter" __designer:wfdid="w20"></ajaxToolkit:ConfirmButtonExtender>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblmsg" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> &nbsp; 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="Tbldata"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbExportXLS"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">
                List Of Sales Invoice</span></strong> <strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information" __designer:wfdid="w72"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="refoid" runat="server" Font-Size="X-Small" __designer:wfdid="w75" Visible="False"></asp:Label><asp:Label id="oid" runat="server" Font-Size="X-Small" Text="oid" __designer:wfdid="w76" Visible="False"></asp:Label><asp:Label id="i_u" runat="server" Font-Size="X-Small" ForeColor="Red" Text="N E W" __designer:wfdid="w77" Visible="False"></asp:Label><asp:Label id="dbsino" runat="server" Font-Size="X-Small" __designer:wfdid="w78" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left>&nbsp;<asp:Label id="help" runat="server" CssClass="Important" __designer:wfdid="w74" Visible="True"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="periodacctg" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:wfdid="w73" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="trncustoid" runat="server" __designer:wfdid="w79" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="FONT-SIZE: x-small" align=left>Invoice No <asp:Label id="Label13" runat="server" CssClass="Important" Text="*" __designer:wfdid="w80"></asp:Label></TD><TD align=left><asp:TextBox id="trnjualno" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w81" ReadOnly="True" MaxLength="20"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Invoice Date <asp:Label id="Label14" runat="server" CssClass="Important" Text="*" __designer:wfdid="w82"></asp:Label></TD><TD align=left><asp:TextBox id="trnjualdate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w83" OnTextChanged="trnjualdate_TextChanged" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="ImageButton5" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w84"></asp:ImageButton> <asp:Label id="Label6" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w85"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>Invoice Ref. No.</TD><TD align=left><asp:TextBox id="trnjualres1" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w86" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Sales Order <asp:Label id="Label15" runat="server" CssClass="Important" Text="*" __designer:wfdid="w87"></asp:Label></TD><TD align=left><asp:TextBox id="referenceno" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w88" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btnsearchSO" onclick="btnsearchSO_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w89" Visible="False"></asp:ImageButton> <asp:ImageButton id="ImageButton2" onclick="ImageButton2_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w90" Visible="False"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left>Customer</TD><TD align=left><asp:TextBox id="trnjualcust" runat="server" Width="175px" CssClass="inpTextDisabled" __designer:wfdid="w92" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btncust" onclick="btncust_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w93" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnclearcust" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w94" Visible="False"></asp:ImageButton> <asp:Label id="custoid" runat="server" __designer:wfdid="w91" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>Order Date</TD><TD align=left><asp:TextBox id="trnorderdate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w95"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Payment Type</TD><TD align=left><asp:DropDownList id="trnpaytype" runat="server" Width="155px" CssClass="inpTextDisabled" __designer:wfdid="w96" Enabled="False"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Currency</TD><TD align=left><asp:DropDownList id="currate" runat="server" Width="180px" CssClass="inpTextDisabled" __designer:wfdid="w97" Enabled="False" OnSelectedIndexChanged="currate_SelectedIndexChanged"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Rate</TD><TD align=left><asp:TextBox id="currencyrate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w98" OnTextChanged="currencyrate_TextChanged" AutoPostBack="True" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice Amount</TD><TD align=left><asp:TextBox id="trnamtjual" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w99" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Tax Type</TD><TD align=left><asp:DropDownList id="taxtype" runat="server" Width="180px" CssClass="inpTextDisabled" __designer:wfdid="w100" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="taxtype_SelectedIndexChanged">
                                                                <asp:ListItem Value="NONTAX">NON TAX</asp:ListItem>
                                                                <asp:ListItem>INCLUDE</asp:ListItem>
                                                                <asp:ListItem>EXCLUDE</asp:ListItem>
                                                            </asp:DropDownList></TD><TD align=left colSpan=2></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left><asp:DropDownList id="trndisctype" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w101" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="trndisctype_SelectedIndexChanged">
                                                                <asp:ListItem Value="AMT">Amount</asp:ListItem>
                                                                <asp:ListItem Value="PCT">Percentage</asp:ListItem>
                                                            </asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left><asp:TextBox id="trndiscamt" runat="server" CssClass="inpText" __designer:wfdid="w102" Visible="False" AutoPostBack="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lbl1" runat="server" Font-Size="X-Small" Text="Discount (Amt)" __designer:wfdid="w103" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="amtdischdr" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w104" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblTaxAmt" runat="server" Text="Tax Amount" __designer:wfdid="w105" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trnamttax" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w106" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblTaxPct" runat="server" Text="Tax (%)" __designer:wfdid="w107" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trntaxpct" runat="server" Width="180px" CssClass="inpTextDisabled" __designer:wfdid="w108" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:DropDownList id="trnjualcosttype" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w109" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="trnjualcosttype_SelectedIndexChanged">
                                                                <asp:ListItem>Include</asp:ListItem>
                                                                <asp:ListItem>Exclude</asp:ListItem>
                                                            </asp:DropDownList></TD><TD align=left><asp:TextBox id="trnjualamtcost" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w110" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Total Invoice Netto</TD><TD align=left><asp:TextBox id="totalinvoice" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w111" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label30" runat="server" Width="116px" Text="Total Invoice (Rp.)" __designer:wfdid="w41" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="totalinvoicerp" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w113" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="orderflag" runat="server" __designer:wfdid="w125" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="trnjualtype" runat="server" __designer:wfdid="w124" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblacctg" runat="server" Font-Size="X-Small" Text="For Accounting :" __designer:wfdid="w114" Visible="False"></asp:Label>Note</TD><TD align=left><asp:TextBox id="trnjualnote" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w115" MaxLength="150"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:DropDownList id="trnjualref" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w128" Visible="False" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="trnjualref_SelectedIndexChanged">
                                                                <asp:ListItem Value="ql_trnordermst">Sales Order</asp:ListItem>
                                                                <asp:ListItem Value="ql_trnforcmst">Forecast</asp:ListItem>
                                                            </asp:DropDownList></TD><TD align=left><asp:Label id="tax" runat="server" __designer:wfdid="w121" Visible="False"></asp:Label> <asp:Label id="posting" runat="server" __designer:wfdid="w126" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:TextBox id="amtdiscdtl" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w122" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblnofakturpajak" runat="server" Width="102px" Font-Size="X-Small" Text="No Faktur Pajak" __designer:wfdid="w45" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="txtnofakturpajak" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w117" Visible="False" MaxLength="20"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:ImageButton id="postingfaktur" onclick="postingfaktur_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w118" Visible="False"></asp:ImageButton> <asp:Label id="disc" runat="server" __designer:wfdid="w120" Visible="False">0</asp:Label> <asp:Label id="orderamt" runat="server" __designer:wfdid="w119" Visible="False">0</asp:Label></TD><TD align=left><asp:TextBox id="identifierno" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w127" Visible="False" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="trnjualdtlseq" runat="server" Text="0" __designer:wfdid="w1" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trnamtjualnetto" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w123" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD align=left colSpan=6><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 154px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div2"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 102%; BACKGROUND-COLOR: beige"><asp:GridView id="TblDtl" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w129" DataKeyNames="trnjualdtlseq,salesdeliveryno,itemcode" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualdtlseq" HeaderText="No " SortExpression="trnjualdtlseq">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="salesdeliveryno" HeaderText="Delivery No">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Item">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="175px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="175px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="salesdeliveryqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtjualdisc" HeaderText="Discount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtjualnetto" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdtlnote" HeaderText="Note" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:CommandField SelectImageUrl="~/Images/selectall.png" SelectText="View SN" ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="No Inv" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No invoice detail!!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp;<BR /></DIV><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w130" TargetControlID="trnjualdate" PopupButtonID="ImageButton5" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meetax" runat="server" __designer:wfdid="w131" TargetControlID="trntaxpct" InputDirection="RightToLeft" MaskType="Number" Mask="99.99"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meerate" runat="server" __designer:wfdid="w132" TargetControlID="currencyrate" InputDirection="RightToLeft" MaskType="Number" Mask="999,999.99"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meedisc" runat="server" __designer:wfdid="w133" TargetControlID="trndiscamt" InputDirection="RightToLeft" MaskType="Number" Mask="99,999,999.99" AcceptNegative="Left" MessageValidatorTip="true" ErrorTooltipEnabled="True" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MEEtrnjualdate" runat="server" __designer:wfdid="w134" TargetControlID="trnjualdate" MaskType="Date" Mask="99/99/9999" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" CultureName="en-US"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MEEtrndiscamt" runat="server" __designer:wfdid="w135" TargetControlID="trndiscamt" InputDirection="RightToLeft" MaskType="Number" Mask="99,999,999.99"></ajaxToolkit:MaskedEditExtender><asp:RadioButtonList id="digit" runat="server" __designer:wfdid="w136" Visible="False" Enabled="False" RepeatDirection="Horizontal">
                                                                <asp:ListItem Selected="True" Value="2">2 Digit Report</asp:ListItem>
                                                                <asp:ListItem Value="4">4 Digit Report</asp:ListItem>
                                                                <asp:ListItem Value="6">6 Digit Report</asp:ListItem>
                                                            </asp:RadioButtonList> <asp:SqlDataSource id="SDSDataView" runat="server" __designer:dtid="281474976710661" SelectCommand="SELECT a.trnjualmstoid, a.trnjualno,a.trnjualdate, s.CUSTNAME AS Customer FROM QL_trnjualmst AS a INNER JOIN QL_MSTCUST AS s ON a.trncustoid = s.CUSTOID WHERE (a.trnjualno LIKE @trnjualno) AND (s.CUSTNAME LIKE @custname) AND (a.cmpcode = @cmpcode) AND (a.trnjualstatus LIKE @trnjualstatus) ORDER BY a.trnjualdate DESC,a.trnjualno" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>" __designer:wfdid="w2">
                        <SelectParameters __designer:dtid="281474976710662">
                            <asp:Parameter __designer:dtid="281474976710663" Name="trnjualno"  />
                            <asp:Parameter __designer:dtid="281474976710664" Name="custname"  />
                            <asp:Parameter __designer:dtid="281474976710665" Name="cmpcode"  />
                            <asp:Parameter __designer:dtid="281474976710666" Name="trnjualstatus"  />
                        </SelectParameters>
                    </asp:SqlDataSource></FIELDSET></TD></TR><TR><TD align=left colSpan=6><asp:Label id="lblUpdate" runat="server" Text="Last Update" __designer:wfdid="w137"></asp:Label>&nbsp;<asp:Label id="updUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Upduser" __designer:wfdid="w138"></asp:Label> <asp:Label id="lblOn" runat="server" Text="By" __designer:wfdid="w139"></asp:Label> <asp:Label id="updTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Updtime" __designer:wfdid="w140"></asp:Label> <asp:Label id="lblCollection" runat="server" Font-Size="X-Small" __designer:wfdid="w141"></asp:Label>&nbsp;<asp:Label id="FlagPrint" runat="server" __designer:wfdid="w142" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w143">
                                                    </asp:ImageButton><asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w144">
                                                    </asp:ImageButton><asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w145" Visible="False">
                                                    </asp:ImageButton><asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w146">
                                                    </asp:ImageButton>&nbsp;<asp:ImageButton id="btnSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" __designer:wfdid="w147" Visible="False">
                                                    </asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w148" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w149" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:UpdatePanel id="UpdatePanel11" runat="server" __designer:wfdid="w150"><ContentTemplate>
<asp:Panel id="PanelSO" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w151" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblCaptSO" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List of Sales Order" __designer:wfdid="w152"></asp:Label> </TD></TR><TR><TD style="FONT-SIZE: x-small; TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLFindSO" runat="server" CssClass="inpText" __designer:wfdid="w153">
                                                                                    <asp:ListItem Value="so.orderno">No</asp:ListItem>
                                                                                    <asp:ListItem Value="c.custname">Customer</asp:ListItem>
                                                                                </asp:DropDownList> <asp:TextBox id="txtFindSO" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w154"></asp:TextBox> <asp:ImageButton id="imbFindSO" onclick="imbFindSO_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w155" AlternateText="Find">
                                                                                </asp:ImageButton> <asp:ImageButton id="imbAllSO" onclick="imbAllSO_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w156" AlternateText="View All">
                                                                                </asp:ImageButton> </TD></TR><TR><TD style="HEIGHT: 15px; TEXT-ALIGN: left" align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset6"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvListSO" runat="server" Width="98%" Font-Size="X-Small" __designer:wfdid="w157" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="orderoid,orderno,orderdate,custname,custoid,ordercurroid,ordercurrate,ordertaxpct,hdiscamtpct,digit,paytermoid,hdisctype,taxable" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DEDFDE">
                                                                                            <RowStyle BackColor="#F7F7DE"></RowStyle>
                                                                                            <Columns>
                                                                                                <asp:CommandField ShowSelectButton="True">
                                                                                                    <HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>
                                                                                                    <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px">
                                                                                                    </ItemStyle>
                                                                                                </asp:CommandField>
                                                                                                <asp:BoundField DataField="orderno" HeaderText="No">
                                                                                                    <HeaderStyle Font-Size="X-Small" Width="125px"></HeaderStyle>
                                                                                                    <ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="orderdate" HeaderText="Date">
                                                                                                    <HeaderStyle Font-Size="X-Small" Width="75px"></HeaderStyle>
                                                                                                    <ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="custname" HeaderText="Customer">
                                                                                                    <HeaderStyle Font-Size="X-Small" Width="250px"></HeaderStyle>
                                                                                                    <ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
                                                                                                </asp:BoundField>
                                                                                            </Columns>
                                                                                            <EmptyDataTemplate>
                                                                                                <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                                                            </EmptyDataTemplate>
                                                                                            <SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>
                                                                                            <AlternatingRowStyle BackColor="White">
                                                                                            </AlternatingRowStyle>
                                                                                        </asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" align=left><asp:LinkButton id="lkbCloseSO" onclick="lkbCloseSO_Click" runat="server" __designer:wfdid="w158">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeso" runat="server" __designer:wfdid="w159" TargetControlID="btnHideso" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptSO" PopupControlID="PanelSO" Drag="True">
                                                            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideso" runat="server" __designer:wfdid="w160" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel8" runat="server" __designer:wfdid="w161"><ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" __designer:wfdid="w162" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting" __designer:wfdid="w163"></asp:Label> </TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" __designer:wfdid="w164" AutoGenerateColumns="False" OnRowDataBound="gvakun_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="acctgcode" HeaderText="Kode">
                                                                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="acctgdesc" HeaderText="Akun">
                                                                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="glnote" HeaderText="Catatan">
                                                                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="debet" HeaderText="Debet">
                                                                                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="kredit" HeaderText="Kredit">
                                                                                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w165">[ CLOSE ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" __designer:wfdid="w166" TargetControlID="btnHidePosting2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True">
                                                            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" __designer:wfdid="w167" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel2" runat="server" __designer:wfdid="w168"><ContentTemplate>
<asp:Panel id="panelDso" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w169" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 490px; TEXT-ALIGN: center"><asp:Label id="lblDSO" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List of Sales Delivery Order" __designer:wfdid="w170"></asp:Label> </TD></TR><TR><TD style="WIDTH: 490px"><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset7"><DIV id="Div6"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvSJ" runat="server" Width="98%" __designer:wfdid="w171" OnSelectedIndexChanged="gvSJ_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnsjjualmstoid,salesdeliveryno" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DEDFDE" EmptyDataRowStyle-ForeColor="Red">
                                                                                            <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                                                                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                                                            <Columns>
                                                                                                <asp:CommandField ShowSelectButton="True">
                                                                                                    <HeaderStyle Width="50px" Font-Size="X-Small"></HeaderStyle>
                                                                                                    <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </ItemStyle>
                                                                                                </asp:CommandField>
                                                                                                <asp:BoundField DataField="salesdeliveryno" HeaderText="Delivery No">
                                                                                                    <ItemStyle Width="150px" Font-Size="X-Small"></ItemStyle>
                                                                                                    <HeaderStyle Width="150px" Font-Size="X-Small"></HeaderStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="trnsjjualdate" HeaderText="Delv. Date">
                                                                                                    <ItemStyle Width="100px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </ItemStyle>
                                                                                                    <HeaderStyle Width="100px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </HeaderStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="trnsjjualnote" HeaderText="Reference">
                                                                                                    <ItemStyle Width="125px" Font-Size="X-Small"></ItemStyle>
                                                                                                    <HeaderStyle Width="125px" Font-Size="X-Small"></HeaderStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
                                                                                                    <HeaderStyle Width="50px" HorizontalAlign="Center">
                                                                                                    </HeaderStyle>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="lkbDetail" OnClick="lkbDetail_Click" runat="server" Font-Size="X-Small" ToolTip='<%# eval("trnsjjualmstoid") %>'>Detail</asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <RowStyle BackColor="#F7F7DE"></RowStyle>
                                                                                            <EmptyDataTemplate>
                                                                                                <asp:Label ID="Label18" runat="server" Text="No data in database." CssClass="Important"></asp:Label>
                                                                                            </EmptyDataTemplate>
                                                                                            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                                                                                            <SelectedRowStyle BackColor="Yellow" Font-Bold="True">
                                                                                            </SelectedRowStyle>
                                                                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                                                                                            <HeaderStyle BackColor="#507CD1" ForeColor="White" BorderColor="White" Font-Bold="True"></HeaderStyle>
                                                                                            <AlternatingRowStyle BackColor="White">
                                                                                            </AlternatingRowStyle>
                                                                                        </asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="WIDTH: 490px; TEXT-ALIGN: center"><asp:LinkButton id="closeDSO" onclick="closeDSO_Click" runat="server" __designer:wfdid="w172">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderdso" runat="server" __designer:wfdid="w173" TargetControlID="btnHideDSO" BackgroundCssClass="modalBackground" PopupControlID="panelDso">
                                                            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideDSO" runat="server" __designer:wfdid="w174" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel6" runat="server" __designer:wfdid="w175"><ContentTemplate>
<asp:Panel id="PanelDetilDSO" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w176" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="DetilDSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="Sales Delivery Order Detail" __designer:wfdid="w177"></asp:Label> </TD></TR><TR><TD><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset8"><DIV id="Div7"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="tblDetilDso" runat="server" Width="98%" Font-Size="X-Small" __designer:wfdid="w178" AutoGenerateColumns="False" CellPadding="4" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DEDFDE" OnRowDataBound="tblDetilDso_RowDataBound">
                                                                                            <Columns>
                                                                                                <asp:BoundField DataField="itemlongdesc" HeaderText="Item">
                                                                                                    <ItemStyle Width="250px" Font-Size="X-Small"></ItemStyle>
                                                                                                    <HeaderStyle Width="250px" Font-Size="X-Small"></HeaderStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="itemgroupcode" HeaderText="Item Group">
                                                                                                    <ItemStyle Width="100px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </ItemStyle>
                                                                                                    <HeaderStyle Width="100px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </HeaderStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="trnsjjualdtlqty" HeaderText="Quantity">
                                                                                                    <ItemStyle Width="100px" HorizontalAlign="Right" Font-Size="X-Small">
                                                                                                    </ItemStyle>
                                                                                                    <HeaderStyle Width="100px" HorizontalAlign="Right" Font-Size="X-Small">
                                                                                                    </HeaderStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="gendesc" HeaderText="Unit">
                                                                                                    <ItemStyle Width="100px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </ItemStyle>
                                                                                                    <HeaderStyle Width="100px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </HeaderStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="free" HeaderText="Free">
                                                                                                    <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </ItemStyle>
                                                                                                    <HeaderStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small">
                                                                                                    </HeaderStyle>
                                                                                                </asp:BoundField>
                                                                                            </Columns>
                                                                                            <RowStyle BackColor="#F7F7DE"></RowStyle>
                                                                                            <EmptyDataTemplate>
                                                                                                <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                                                            </EmptyDataTemplate>
                                                                                            <SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>
                                                                                            <AlternatingRowStyle BackColor="White">
                                                                                            </AlternatingRowStyle>
                                                                                        </asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="btnCloseDSO" onclick="btnCloseDSO_Click" runat="server" __designer:wfdid="w179">[ Back ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderDetilDSO" runat="server" __designer:wfdid="w180" TargetControlID="btnHideDetilDso" BackgroundCssClass="modalBackground" PopupDragHandleControlID="DetilDSO" PopupControlID="PanelDetilDSO" Drag="True">
                                                            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideDetilDso" runat="server" __designer:wfdid="w181" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel7" runat="server" __designer:wfdid="w182"><ContentTemplate>
<asp:Panel id="pnlPosting" runat="server" Width="400px" CssClass="modalBox" __designer:wfdid="w183" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="Invoice Posting" __designer:wfdid="w184"></asp:Label> </TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblpaymentype" runat="server" Text="Payment Type" __designer:wfdid="w185"></asp:Label> </TD><TD align=left><asp:DropDownList id="paymentype" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w186" AutoPostBack="True" OnSelectedIndexChanged="paymentype_SelectedIndexChanged">
                                                                                    <asp:ListItem>CASH</asp:ListItem>
                                                                                    <asp:ListItem>NONCASH</asp:ListItem>
                                                                                </asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label22" runat="server" Text="Reference No." __designer:wfdid="w187" Visible="False"></asp:Label> </TD><TD align=left><asp:TextBox id="referensino" runat="server" CssClass="inpText" __designer:wfdid="w188" Visible="False"></asp:TextBox> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblcashbankacctg" runat="server" Text="Cash Account" __designer:wfdid="w189"></asp:Label> </TD><TD align=left><asp:DropDownList id="cashbankacctg" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w190">
                                                                                </asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblapaccount" runat="server" Text="A/R Account" __designer:wfdid="w191" Visible="False"></asp:Label> </TD><TD align=left><asp:DropDownList id="apaccount" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w192" Visible="False">
                                                                                </asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblpurchasingaccount" runat="server" Text="Sales Account" __designer:wfdid="w193"></asp:Label> </TD><TD align=left><asp:DropDownList id="purchasingaccount" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w194">
                                                                                </asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblCostCashBank" runat="server" Text="Cost Cash/Bank Account" __designer:wfdid="w195" Visible="False"></asp:Label> </TD><TD align=left><asp:DropDownList id="CashForCost" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w196" Visible="False">
                                                                                </asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:LinkButton id="lkbPosting" onclick="lkbPosting_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w197">[ POST ]</asp:LinkButton> <asp:LinkButton id="lkbCancel" onclick="lkbCancel_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w198">[ CANCEL ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting" runat="server" __designer:wfdid="w199" TargetControlID="btnHidePosting" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting" PopupControlID="pnlPosting">
                                                            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting" runat="server" __designer:wfdid="w200" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanelSuplier" runat="server" __designer:wfdid="w201"><ContentTemplate>
<asp:Panel id="Panelcust" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w202" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="LaberSuplier" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer" __designer:wfdid="w203"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label">Filter : <asp:DropDownList id="DDLcustid" runat="server" CssClass="inpText" __designer:wfdid="w204">
                                                                                    <asp:ListItem Value="custname">Name</asp:ListItem>
                                                                                    <asp:ListItem Value="custcode">Code</asp:ListItem>
                                                                                </asp:DropDownList> <asp:TextBox id="txtFindCustID" runat="server" CssClass="inpText" __designer:wfdid="w205"></asp:TextBox> <asp:ImageButton id="imbFindcust" onclick="imbFindcust_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w206">
                                                                                </asp:ImageButton> <asp:ImageButton id="imbAllcust" onclick="imbAllcust_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w207">
                                                                                </asp:ImageButton> </TD></TR><TR><TD style="HEIGHT: 315px"><FIELDSET style="WIDTH: 97%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 290px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset9"><DIV id="Div8"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvCustomer" runat="server" Width="98%" __designer:wfdid="w208" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" DataKeyNames="custoid,custname" CellPadding="4" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" OnPageIndexChanging="gvCustomer_PageIndexChanging" AllowPaging="True">
                                                                                            <RowStyle BackColor="#F7F7DE"></RowStyle>
                                                                                            <Columns>
                                                                                                <asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
                                                                                                    <HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px">
                                                                                                    </HeaderStyle>
                                                                                                    <ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px">
                                                                                                    </ItemStyle>
                                                                                                </asp:CommandField>
                                                                                                <asp:BoundField DataField="custcode" HeaderText="Code">
                                                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"></HeaderStyle>
                                                                                                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="custname" HeaderText="Name">
                                                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"></HeaderStyle>
                                                                                                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
                                                                                                </asp:BoundField>
                                                                                            </Columns>
                                                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White">
                                                                                            </FooterStyle>
                                                                                            <PagerStyle HorizontalAlign="Center" BackColor="#2461BF" Font-Size="X-Small" ForeColor="White">
                                                                                            </PagerStyle>
                                                                                            <EmptyDataTemplate>
                                                                                                <asp:Label ID="Label20" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No supplier data found!!"></asp:Label>
                                                                                            </EmptyDataTemplate>
                                                                                            <SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333">
                                                                                            </SelectedRowStyle>
                                                                                            <HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White">
                                                                                            </HeaderStyle>
                                                                                            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                                                                                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                                                        </asp:GridView> </DIV></FIELDSET> &nbsp; </TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="Close" onclick="Close_Click" runat="server" __designer:wfdid="w209">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="Buttoncust" runat="server" __designer:wfdid="w210" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w211" TargetControlID="Buttoncust" Drag="True" PopupControlID="Panelcust" PopupDragHandleControlID="Panelcust" BackgroundCssClass="modalBackground">
                                                            </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="upPreview" runat="server" __designer:wfdid="w212"><ContentTemplate>
<asp:Panel id="pnlPreview" runat="server" Width="750px" CssClass="modalBox" __designer:wfdid="w213" Visible="False"><TABLE width="100%" align=center><TBODY><TR><TD align=center><asp:Label id="lblCaptPreview" runat="server" Font-Size="Medium" Font-Bold="True" Text="Preview Jurnal" __designer:wfdid="w214"></asp:Label> </TD></TR><TR><TD><asp:Panel id="pnlScrollPreview" runat="server" Width="100%" Height="200px" __designer:wfdid="w215" ScrollBars="Vertical"><asp:GridView id="gvPreview" runat="server" Width="97%" __designer:wfdid="w216" AutoGenerateColumns="False" CellPadding="4" BorderWidth="1px" BorderStyle="None" EmptyDataRowStyle-ForeColor="Red" AllowSorting="True">
                                                                                        <RowStyle BackColor="#F7F7DE"></RowStyle>
                                                                                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="seq" HeaderText="No">
                                                                                                <HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>
                                                                                                <ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="code" HeaderText="Code">
                                                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="desc" HeaderText="Description">
                                                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="debet" HeaderText="Debet">
                                                                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="credit" HeaderText="Credit">
                                                                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                                            </asp:BoundField>
                                                                                        </Columns>
                                                                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black">
                                                                                        </FooterStyle>
                                                                                        <PagerStyle HorizontalAlign="Center" BackColor="#999999" ForeColor="Black"></PagerStyle>
                                                                                        <EmptyDataTemplate>
                                                                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No Data Found !!"></asp:Label>
                                                                                        </EmptyDataTemplate>
                                                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White"></SelectedRowStyle>
                                                                                        <HeaderStyle BackColor="#000084" BorderColor="White" Font-Bold="True" ForeColor="Black"></HeaderStyle>
                                                                                        <AlternatingRowStyle BackColor="White">
                                                                                        </AlternatingRowStyle>
                                                                                    </asp:GridView> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 15px" align=center><asp:LinkButton id="lkbPostPreview" onclick="lkbPostPreview_Click" runat="server" Font-Bold="True" __designer:wfdid="w217">[ POST ]</asp:LinkButton> <asp:LinkButton id="lkbClosePreview" onclick="lkbClosePreview_Click" runat="server" Font-Bold="True" __designer:wfdid="w218">[ CLOSE ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="beHidePreview" runat="server" __designer:wfdid="w219" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpePreview" runat="server" __designer:wfdid="w220" TargetControlID="beHidePreview" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptPreview" PopupControlID="pnlPreview" Drag="True">
                                                            </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">
                Form Sales Invoice :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" Visible="False" DefaultButton="btnMsgBoxOK">
                <table cellspacing="1" cellpadding="1" border="0">
                    <tbody>
                        <tr>
                            <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" valign="top" align="left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                            </td>
                            <td valign="top" align="left">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Size="X-Small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

