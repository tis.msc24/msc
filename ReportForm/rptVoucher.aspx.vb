Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Partial Class ReportForm_rptVoucher
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Public Sub initGroup()
        'sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        'FillDDL(FilterDDLGrup, sSql)
        'FilterDDLGrup.Items.Add(New ListItem("ALL GROUP", "ALL GROUP"))
        'FilterDDLGrup.SelectedValue = "ALL GROUP"
    End Sub

    Public Sub initSubGroup()
        'sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        'FillDDL(FilterDDLSubGrup, sSql)
        'FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GROUP", "ALL SUB GROUP"))
        'FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP"
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        lblkonfirmasi.Text = ""
        Session("diprint") = "False"
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Try
            Dim namaPDF As String = "" : Dim sStat As String = ""
            Dim sWhere As String = "WHERE po.trnbelistatus='Approved'"
            sWhere &= IIf(suppoid.Text.Trim = "", " ", " And sup.suppoid = " & suppoid.Text & " ")
            sWhere &= IIf(nota.Text.Trim = "", " ", " And po.trnbelimstoid = '" & oid.Text & "' ")
            If ddlstatusPO.SelectedValue <> "ALL" Then
                sStat &= " Where vStatus = '" & ddlstatusPO.SelectedValue & "'"
            End If

            vReport = New ReportDocument
            If TypeLap.SelectedValue.ToUpper = "PI" Then
                sWhere &= " AND Convert(VarChar(10),po.trnbelipodate,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "'"
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptVoucherSumXls.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptVoucherSum.rpt"))
                End If
                namaPDF = "VOUCHER_SUM_PI"
            Else
                sWhere &= " AND bm.trnbelistatus='POST' AND Convert(VarChar(10),bm.updtime,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "'"
                sWhere &= IIf(txtInv.Text.Trim = "", " ", " And bm.trnbelimstoid = '" & MstoidPI.Text & "'")
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptvoucherDetailXls.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptvoucherDetail.rpt"))
                End If
                namaPDF = "VOUCHER_DETAIL_PO"
            End If

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("sStat", sStat)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"
            If tipe = "" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Public Sub BindDataListItem()
        Dim sWhere As String = ""
        'If FilterDDLGrup.SelectedItem.Text <> "ALL GROUP" Then
        '    sWhere &= " and itemgroupoid=" & FilterDDLGrup.SelectedValue & ""
        'End If
        'If FilterDDLSubGrup.SelectedItem.Text <> "ALL SUB GROUP" Then
        '    sWhere &= " and itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & ""
        'End If
        sSql = "select ql_mstitem.itemcode, ql_mstitem.itemdesc, " & _
        "ql_mstitem.itempriceunit1,ql_mstitem.itempriceunit2," & _
        "ql_mstitem.itempriceunit3,ql_mstitem.itemoid, g.gendesc satuan1," & _
        "g2.gendesc satuan2, g3.gendesc satuan3,konversi1_2, " & _
        "konversi2_3,ql_mstitem.merk from ql_mstitem inner join ql_mstgen g on g.genoid=satuan1 " & _
        "and itemflag='AKTIF' and stockflag='V' inner join ql_mstgen g2 on g2.genoid=satuan2  " & _
        "inner join ql_mstgen g3 on g3.genoid=satuan3  " & _
        "where (itemdesc like '%" & Tchar(itemname.Text) & "%' or ql_mstitem.itemcode like '%" & Tchar(itemname.Text) & "%' or ql_mstitem.merk like '%" & Tchar(itemname.Text) & "%') " & sWhere & ""
        FillGV(gvItem, sSql, "ql_mstitem")
        gvItem.Visible = True
    End Sub

    Public Sub bindDataListSupplier()
        sSql = "SELECT Distinct suppoid, trnsuppoid, suppcode, suppname, supptype From QL_mstsupp supp INNER JOIN QL_trnbelimst bm on supp.suppoid= bm.trnsuppoid WHERE bm.trnbelino LIKE '%" & Tchar(txtInv.Text.Trim) & "%' AND (supp.suppcode LIKE '%" & Tchar(suppname.Text.Trim) & "%' OR supp.suppname LIKE '%" & Tchar(suppname.Text.Trim) & "%')"
        FillGV(gvSupp, sSql, "ql_mstsupp")
        gvSupp.Visible = True
    End Sub

    Public Sub bindDataListPO()
        Try
            Dim dat1 As String = CDate(toDate(dateAwal.Text))
            Dim dat2 As String = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        sSql = "SELECT trnbelimstoid, trnbelipono, suppoid, suppname FROM ql_pomst po INNER JOIN ql_mstsupp on suppoid=trnsuppoid WHERE trnbelipono LIKE '%" & Tchar(nota.Text.Trim) & "%' AND trnbelipono IN (SELECT trnbelino FROM QL_voucherdtl v WHERE v.trnbelimstoid=po.trnbelimstoid) AND po.trnbelipono IN (SELECT bm.trnbelipono FROM QL_trnbelimst bm WHERE bm.trnbelipono=po.trnbelipono AND bm.trnbelidate Between '" & CDate(toDate(dateAwal.Text)) & "' AND '" & CDate(toDate(dateAkhir.Text)) & "') Order By po.trnbelipodate desc"
        FillGV(GVNota, sSql, "ql_pomst")
        GVNota.Visible = True
    End Sub

    Private Sub BindPI()
        sSql = "SELECT bm.cmpcode,bm.branch_code,bm.trnbelino,CONVERT(Varchar(20),bm.trnbelidate,103) trnbelidate,bm.trnbelimstoid FROM QL_trnbelimst bm INNER JOIN QL_pomst pom On bm.trnbelipono=pom.trnbelipono WHERE (bm.trnbelino LIKE '%" & Tchar(txtInv.Text.Trim) & "%' AND bm.trnbelipono LIKE '%" & Tchar(nota.Text.Trim) & "%')  AND pom.trnbelipono IN (SELECT trnbelino FROM QL_voucherdtl v WHERE v.trnbelimstoid=pom.trnbelimstoid) AND bm.trnbelistatus='POST' AND (SELECT SUM(bd.amtdisc2) FROM QL_trnbelidtl bd WHERE bd.trnbelimstoid=bm.trnbelimstoid) > 0 Order By bm.trnbelimstoid Desc"
        FillGV(GvPI, sSql, "ql_trnbelimst")
        GvPI.Visible = True
    End Sub
#End Region

#Region "Event"
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        itemcode.Text = gvItem.SelectedDataKey.Item(0)
        merk.Text = gvItem.SelectedDataKey.Item("merk")
        gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        merk.Text = ""
        itemcode.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataListItem()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptVoucher.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Voucher"

        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrint("")
            End If
        Else
            sSql = CType(("select count(USERPROF) from QL_USERROLE ur join QL_ROLE r on ur.ROLEOID = r.ROLEOID  where ROLENAME = 'WH Spv' and USERPROF = '" & Session("userid") & "' "), String)
            'If (ToDouble(GetStrData(sSql)) > 0) Then
            '    rbPO.Visible = False
            '    rbPOLPB.Checked = True
            'End If
            dateAwal.Text = Format(New Date(Now.Year, Now.Month, 1), "dd/MM/yyyy")
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        Session("diprint") = "True"
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptVoucher.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showPrint("excel")
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListSupplier()
    End Sub

    Protected Sub btnSearchNota_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListPO()
    End Sub

    Protected Sub btnEraseSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppname.Text = ""
        suppoid.Text = ""
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        suppname.Text = gvSupp.SelectedDataKey.Item("suppname")
        suppoid.Text = gvSupp.SelectedDataKey.Item("suppoid")
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupp.PageIndexChanging
        gvSupp.PageIndex = e.NewPageIndex
        bindDataListSupplier()
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        GVNota.PageIndex = e.NewPageIndex
        bindDataListPO()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nota.Text = ""
        oid.Text = ""
        GVNota.Visible = False
    End Sub

    Protected Sub GVNota_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        nota.Text = GVNota.SelectedDataKey.Item("trnbelipono")
        oid.Text = GVNota.SelectedDataKey.Item("trnbelimstoid")
        'suppname.Text = GVNota.SelectedDataKey.Item("suppname")
        'suppoid.Text = GVNota.SelectedDataKey.Item("suppoid")
        GVNota.Visible = False
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        Session("diprint") = "False"
        showPrint("pdf")
    End Sub

    Protected Sub btnSearchPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindPI()
    End Sub

    Protected Sub GvPI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        txtInv.Text = GvPI.SelectedDataKey.Item("trnbelino")
        MstoidPI.Text = GvPI.SelectedDataKey.Item("trnbelimstoid")
        GvPI.Visible = False
    End Sub

    Protected Sub BtnErsaePI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtInv.Text = ""
        MstoidPI.Text = ""
        GvPI.Visible = False
    End Sub

    Protected Sub GvPI_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvPI.PageIndex = e.NewPageIndex
        BindPI()
    End Sub

    Protected Sub type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If TypeLap.SelectedValue.ToUpper = "PI" Then
            NoPI.Visible = False : Titi2PI.Visible = False
            txtInv.Visible = False : btnSearchPI.Visible = False
            BtnErsaePI.Visible = False
        Else
            NoPI.Visible = True : Titi2PI.Visible = True
            txtInv.Visible = True : btnSearchPI.Visible = False
            BtnErsaePI.Visible = True
        End If
    End Sub
#End Region
End Class
