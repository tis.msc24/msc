<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSOClose.aspx.vb" Inherits="ReportForm_rptSOClose"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" width="100%">
        <tr>
            <td colspan="3" rowspan="3" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Navy"
                                Text=".: Laporan SO Close" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                    <tr>
                        <th align="center" style="background-color: #ffffff" valign="center">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE><TBODY><TR><TD align=left>Cabang</TD><TD align=left colSpan=3><asp:DropDownList id="fCabang" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w36"></asp:DropDownList></TD></TR><TR><TD align=left>Periode </TD><TD align=left colSpan=3><asp:TextBox id="range1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w1"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/mm/yyyy)" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD align=left>Tipe laporan</TD><TD align=left colSpan=3><asp:DropDownList id="ddltype" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w6"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Nomor&nbsp;SO</TD><TD align=left colSpan=3><asp:TextBox id="sono" runat="server" CssClass="inpText" __designer:wfdid="w7"></asp:TextBox>&nbsp;<asp:ImageButton id="searchso" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton>&nbsp;<asp:ImageButton id="eraseso" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;<asp:Label id="sooid" runat="server" __designer:wfdid="w10" Visible="False"></asp:Label>&nbsp;&nbsp; </TD></TR><TR><TD align=right></TD><TD align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 0px; POSITION: relative; TOP: 2px; BACKGROUND-COLOR: transparent" id="GVSo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w11" Visible="False" DataKeyNames="ordermstoid,orderno,trnorderdate,custoid,custname" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" PageSize="8" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="Nomor SO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal SO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Customer</TD><TD align=left colSpan=3><asp:TextBox id="custname" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w12"></asp:TextBox>&nbsp;<asp:ImageButton id="searchcust" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="17px" __designer:wfdid="w13"></asp:ImageButton>&nbsp;<asp:ImageButton id="erasecust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w14"></asp:ImageButton>&nbsp;<asp:Label id="custoid" runat="server" __designer:wfdid="w15" Visible="False"></asp:Label> </TD></TR><TR><TD align=right></TD><TD align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVCustomer" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w16" Visible="False" DataKeyNames="custoid,custcode,custname" AutoGenerateColumns="False" CellPadding="4" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" PageSize="8" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Kode">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR id="trbarang" runat="server" visible="false"><TD align=left>Barang</TD><TD align=left colSpan=3><asp:TextBox id="barang" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w17"></asp:TextBox>&nbsp;<asp:ImageButton id="barangsearch" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w18"></asp:ImageButton>&nbsp;<asp:ImageButton id="barangerase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w19"></asp:ImageButton>&nbsp;<asp:Label id="barangoid" runat="server" __designer:wfdid="w20" Visible="False"></asp:Label> </TD></TR><TR id="Tr1" runat="server" visible="false"><TD align=right></TD><TD align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVBarang" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w21" Visible="False" DataKeyNames="itemoid,itemcode,itemdesc,Merk" AutoGenerateColumns="False" CellPadding="4" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" PageSize="8" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label150" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR id="trmerk" runat="server" visible="false"><TD align=left>Merk</TD><TD align=left colSpan=3><asp:TextBox id="merk" runat="server" Width="169px" CssClass="inpTextDisabled" __designer:wfdid="w22" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnshowprint" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w23"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w24"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w42" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w43"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD colSpan=4><ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w29" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'." TargetControlID="range1" PopupButtonID="ImageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w30" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'." TargetControlID="range2" PopupButtonID="ImageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w31" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." TargetControlID="range1" Mask="99/99/9999" ErrorTooltipEnabled="True" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w32" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." TargetControlID="range2" Mask="99/99/9999" ErrorTooltipEnabled="True" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w33"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w34" DisplayGroupTree="False" HasZoomFactorList="False" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" AutoDataBind="true"></CR:CrystalReportViewer> 
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnshowprint"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w35" Visible="False"><TABLE width=250><TBODY><TR><TD style="HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w36"></asp:Label></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%"><TBODY><TR><TD><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png" __designer:wfdid="w37"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red" __designer:wfdid="w38"></asp:Label></TD><TD></TD></TR><TR><TD></TD><TD></TD><TD></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" __designer:wfdid="w39"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" __designer:wfdid="w40" TargetControlID="btnExtender" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelErrMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" __designer:wfdid="w41" Visible="False"></asp:Button> 
</contenttemplate>
            </asp:UpdatePanel></th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>

