<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ServicePlanReportForm.aspx.vb" Inherits="Report_ServicePlanReport" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="970">
        <tr>
            <td align="left" class="header" colspan="3" style="background-color: silver">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Maroon" Text=":: Laporan Rencana Service"></asp:Label></td>
        </tr>
    </table>
    <table width="970">
        <tr>
            <td align="left" colspan="3">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width=500><TBODY><TR><TD style="WIDTH: 67px; HEIGHT: 21px"><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 6px; HEIGHT: 21px"><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD style="WIDTH: 123px; HEIGHT: 21px"><asp:DropDownList id="DDLFilter" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w3"><asp:ListItem Value="QLR.REQCODE">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="QLC.CUSTNAME">Nama Customer</asp:ListItem>
<asp:ListItem Value="QLR.REQITEMNAME">Nama Barang</asp:ListItem>
<asp:ListItem Value="QLR.BARCODE">Barcode</asp:ListItem>
<asp:ListItem Value="QLC.phone1">Telepon 1</asp:ListItem>
</asp:DropDownList></TD><TD style="HEIGHT: 21px"><asp:TextBox id="txtFilter" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 67px; HEIGHT: 10px"><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w5"></asp:Label></TD><TD style="WIDTH: 6px; HEIGHT: 10px"><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w6"></asp:Label></TD><TD style="WIDTH: 123px; HEIGHT: 10px"><asp:TextBox id="txtPeriod1" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w7"></asp:TextBox> <asp:ImageButton id="btnCalendar1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton></TD><TD style="HEIGHT: 10px"><asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w9"></asp:Label>&nbsp; <asp:TextBox id="txtPeriod2" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w10"></asp:TextBox> <asp:ImageButton id="btnCalendar2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w11"></asp:ImageButton> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w12"></asp:Label></TD></TR><TR><TD style="WIDTH: 67px"><asp:RadioButtonList id="cbstatus" runat="server" Width="117px" __designer:wfdid="w13" RepeatDirection="Horizontal"><asp:ListItem Selected="True">Status</asp:ListItem>
<asp:ListItem>Paid</asp:ListItem>
</asp:RadioButtonList></TD><TD style="WIDTH: 6px">:</TD><TD style="WIDTH: 123px"><asp:DropDownList id="DDLStatus" runat="server" Width="91px" CssClass="inpText" __designer:wfdid="w14"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>Check</asp:ListItem>
<asp:ListItem>CheckOut</asp:ListItem>
<asp:ListItem>Ready</asp:ListItem>
<asp:ListItem>Send</asp:ListItem>
<asp:ListItem>Close</asp:ListItem>
<asp:ListItem>Start</asp:ListItem>
<asp:ListItem>Finish</asp:ListItem>
<asp:ListItem>Final</asp:ListItem>
<asp:ListItem>Invoiced</asp:ListItem>
</asp:DropDownList></TD><TD></TD></TR><TR><TD style="WIDTH: 67px"></TD><TD style="WIDTH: 6px"></TD><TD style="WIDTH: 123px"></TD><TD></TD></TR></TBODY></TABLE><TABLE width=500><TBODY><TR><TD style="HEIGHT: 15px" colSpan=3><asp:ImageButton id="btnview" onclick="btnview_Click" runat="server" ImageUrl="~/Images/viewreport.png" __designer:wfdid="w15"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnETPdf" runat="server" ImageUrl="~/Images/topdf.png" __designer:wfdid="w16" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnETExcel" onclick="btnETExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" __designer:wfdid="w17" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w18"></asp:ImageButton> </TD></TR><TR><TD style="HEIGHT: 15px" colSpan=3></TD></TR><TR><TD style="HEIGHT: 15px" colSpan=3><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><CR:CrystalReportViewer id="RCVPLANSERV" runat="server" __designer:wfdid="w19" AutoDataBind="true" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer></TD></TR><TR><TD style="HEIGHT: 15px" colSpan=3><ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" __designer:wfdid="w20" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" TargetControlID="txtPeriod1"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" __designer:wfdid="w21" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" TargetControlID="txtPeriod2"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CEPeriod1" runat="server" __designer:wfdid="w22" TargetControlID="txtPeriod1" PopupButtonID="btnCalendar1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CEPeriod2" runat="server" __designer:wfdid="w23" TargetControlID="txtPeriod2" PopupButtonID="btnCalendar2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnETExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnETPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnview"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel id="UpdatePanel2" runat="server">
        <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width=250><TR><TD style="HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR></TABLE><TABLE width=250><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD><TD></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" Drag="True" DropShadow="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

