<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trncbreceipt.aspx.vb" Inherits="Accounting_trncbreceipt" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server"> 

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Cash/Bank Receipt"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel2" runat="server" DefaultButton="btnSearch" __designer:wfdid="w224"><TABLE style="WIDTH: 700px"><TBODY><TR><TD align=left><asp:CheckBox id="cbPeriode" runat="server" Font-Size="X-Small" Text="Periode" __designer:wfdid="w248" Checked="True"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w225"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w226"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="X-Small" Text="to" __designer:wfdid="w227"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w228"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w229"></asp:ImageButton>&nbsp;&nbsp;</TD></TR><TR><TD align=left>Cabang</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="dd_cabang" runat="server" CssClass="inpText" __designer:wfdid="w231"></asp:DropDownList> </TD></TR><TR><TD align=left>Nomer</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="no" runat="server" Width="143px" CssClass="inpText" __designer:wfdid="w232"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="statuse" runat="server" Width="98px" CssClass="inpText" __designer:wfdid="w233"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value=" ">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w234"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w235"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewLast" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w236"></asp:ImageButton></TD></TR><TR><TD id="Td6" align=left colSpan=4 Visible="false"><asp:ImageButton id="btnCheck" onclick="btnCheck_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w237" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnUncheck" onclick="btnUncheck_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w238" Visible="False"></asp:ImageButton> <asp:ImageButton id="BtnPostSelected" onclick="BtnPostSelected_Click" runat="server" ImageUrl="~/Images/postselect.png" ImageAlign="AbsMiddle" __designer:wfdid="w239" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4 Visible="false"><STRONG><SPAN style="COLOR: #ff0033">Pemberitahuan : * Data transaksi dengan status in procces akan otomatis closed jika melewati tanggal sekarang, mohon untuk segera di posting</SPAN></STRONG></TD></TR><TR><TD align=left colSpan=4><asp:GridView id="gvmstcost" runat="server" Width="950px" ForeColor="#333333" __designer:wfdid="w240" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankno,cashbankoid,cashbankstatus,branch_code" AllowPaging="True" GridLines="None" PageSize="8">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
<asp:CheckBox id="cbPosting" runat="server" __designer:wfdid="w1" Checked='<%# eval("selected") %>' ToolTip='<%# eval("cashbankoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Bold="True" ForeColor="DarkRed"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Transaksi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="ImageButton3" onclick="ImageButton3_Click" runat="server" ImageUrl="~/Images/print.gif" __designer:wfdid="w2" ToolTip='<%# Eval("cashbankno") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data tidak ditemukan !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w241" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy" TargetControlID="txtperiode2" Enabled="True"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w242" TargetControlID="txtPeriode1" CultureName="id-ID" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w243" TargetControlID="txtPeriode2" CultureName="id-ID" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w244" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy" TargetControlID="txtperiode1" Enabled="True"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE></asp:Panel><asp:Label id="Label13" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Expense : Rp. " __designer:wfdid="w245" Visible="False"></asp:Label> <asp:Label id="lblgrandtotal" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w246" Visible="False"></asp:Label> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="gvmstcost"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="FONT-SIZE: 9pt">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image>
                                List Of Cash/Bank Receipt</span></strong>&nbsp; <strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="lblIO" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="" __designer:wfdid="w101" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w102" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left>&nbsp;</TD></TR><TR><TD align=left><asp:Label id="Label6" runat="server" Font-Size="X-Small" Text="Cabang" __designer:wfdid="w103"></asp:Label></TD><TD align=left><asp:DropDownList id="deptoid" runat="server" Width="150px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w104" AutoPostBack="True" OnSelectedIndexChanged="deptoid_SelectedIndexChanged"></asp:DropDownList> <asp:Label id="cashbankoid" runat="server" __designer:wfdid="w105" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label20" runat="server" Font-Size="X-Small" Text="Mata Uang" __designer:wfdid="w106" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="currate" runat="server" Width="98px" CssClass="inpTextDisabled" __designer:wfdid="w107" Visible="False" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="currate_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label9" runat="server" Font-Size="X-Small" Text="Pembayaran" __designer:wfdid="w108"></asp:Label></TD><TD align=left><asp:DropDownList id="payflag" runat="server" Width="105px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w109" AutoPostBack="True" OnSelectedIndexChanged="payflag_SelectedIndexChanged"><asp:ListItem Text="CASH" Value="CASH"></asp:ListItem>
<asp:ListItem Text="NON CASH" Value="NON CASH"></asp:ListItem>
</asp:DropDownList></TD><TD align=left><asp:Label id="CutofDate" runat="server" __designer:wfdid="w110" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="cashbankacctgoid" runat="server" CssClass="inpText" __designer:wfdid="w111" Visible="False"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label17" runat="server" Width="53px" Font-Size="X-Small" Text="No. Rek" __designer:wfdid="w112" Visible="False"></asp:Label> <asp:Label id="Label8" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w113" Visible="False"></asp:Label>&nbsp;</TD><TD align=left><asp:TextBox id="payrefno" runat="server" CssClass="inpText" __designer:wfdid="w114" Visible="False" MaxLength="30"></asp:TextBox></TD><TD id="TD4" align=left runat="server" Visible="false"><asp:Label id="Label10" runat="server" Width="81px" Font-Size="X-Small" Text="Jatuh Tempo" __designer:wfdid="w115" Visible="False"></asp:Label></TD><TD id="TD5" align=left runat="server" Visible="false"><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w116" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w117" Visible="False" BorderColor="White"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label24" runat="server" Font-Size="X-Small" Text="No. Bukti" __designer:wfdid="w118"></asp:Label></TD><TD align=left><asp:TextBox id="CashBankNo" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w119" Enabled="False"></asp:TextBox></TD><TD align=left><asp:Label id="Label26" runat="server" Font-Size="X-Small" Text="Rate" __designer:wfdid="w120" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="currencyrate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w121" Visible="False" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label12" runat="server" Font-Size="X-Small" Text="Tanggal" __designer:wfdid="w122"></asp:Label> <asp:Label id="Label22" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w123"></asp:Label></TD><TD align=left><asp:TextBox id="cashbankdate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w124" Enabled="False" AutoPostBack="True" OnTextChanged="cashbankdate_TextChanged"></asp:TextBox> <asp:ImageButton id="imbCBDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w125" Visible="False"></asp:ImageButton> <asp:Label id="Label23" runat="server" ForeColor="Red" __designer:wfdid="w126">(dd/MM/yyyy)</asp:Label></TD><TD align=left><asp:Label id="lblBankname" runat="server" Width="34px" Font-Size="X-Small" Text="Bank" __designer:wfdid="w127"></asp:Label></TD><TD align=left><asp:DropDownList id="bank" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w128" AutoPostBack="True" OnSelectedIndexChanged="currate_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label16" runat="server" Width="101px" Font-Size="X-Small" Text="Cash/Akun Bank" __designer:wfdid="w129"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="txtCoa" runat="server" Width="468px" CssClass="inpText " __designer:wfdid="w130"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCariCoa" onclick="BtnCariCoa_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w131"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImageButton2" onclick="btnErase_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w132"></asp:ImageButton>&nbsp;<asp:Label id="lblCoa" runat="server" Text="0" __designer:wfdid="w133" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD align=left colSpan=2><asp:GridView id="GvCoa" runat="server" Width="406px" ForeColor="#333333" __designer:wfdid="w134" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc" AllowPaging="True" GridLines="None" PageSize="5" OnSelectedIndexChanged="GvCoa_SelectedIndexChanged" OnPageIndexChanging="GvCoa_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD><TD align=left><asp:GridView id="gvPerson" runat="server" Width="1px" ForeColor="#333333" __designer:wfdid="w135" Visible="False" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="personoid,personnip,personname" AllowPaging="True" GridLines="None" PageSize="1">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="personname" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="350px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="Label4" runat="server" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w136"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="cashbanknote" runat="server" Width="440px" CssClass="inpText" __designer:wfdid="w137" MaxLength="95"></asp:TextBox></TD></TR><TR><TD align=left>&nbsp;</TD><TD align=left><asp:TextBox id="pic" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w138" Visible="False" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchPerson" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w139" Visible="False"></asp:ImageButton> <asp:ImageButton id="ImageButton1" onclick="ImageButton1_Click1" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" Height="16px" __designer:wfdid="w140" Visible="False"></asp:ImageButton> <asp:Label id="picoid" runat="server" Width="1px" __designer:wfdid="w141" Visible="False">0</asp:Label> <asp:Label id="Label25" runat="server" Width="1px" CssClass="inpText" Text="*" __designer:wfdid="w142" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label11" runat="server" Width="77px" Font-Size="X-Small" Font-Bold="True" Text="Detail :" __designer:wfdid="w143" Font-Underline="True"></asp:Label></TD><TD align=left colSpan=3><asp:Label id="cashbankglseq" runat="server" Text="cashbankglseq" __designer:wfdid="w144" Visible="False"></asp:Label> <asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w145" Visible="False"></asp:Label>&nbsp;<asp:LinkButton id="LinkButton2" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w146" Visible="False">List</asp:LinkButton> <asp:Label id="lblPOST" runat="server" Font-Size="X-Small" __designer:wfdid="w147" Visible="False"></asp:Label> <asp:Label id="AddToList" runat="server" Font-Size="X-Small" Text="Add To List" __designer:wfdid="w148" Visible="False"></asp:Label> <asp:Label id="Label7" runat="server" Font-Size="X-Small" Text="Cost ID" __designer:wfdid="w149" Visible="False" Font-Italic="False"></asp:Label> <asp:Label id="lblAcctgOid" runat="server" Font-Size="X-Small" __designer:wfdid="w150" Visible="False"></asp:Label> <asp:Label id="cashbankgloid" runat="server" __designer:wfdid="w151" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="Label15" runat="server" Width="75px" Font-Size="X-Small" Text="COA Receipt" __designer:wfdid="w152"></asp:Label> <asp:Label id="Label19" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w153"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="acctgoid" runat="server" Width="400px" CssClass="inpTextDisabled" __designer:wfdid="w154" ReadOnly="True"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnCari" onclick="ImageButton1_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w155"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lblItemDesc" runat="server" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w156"></asp:Label></TD><TD align=left><asp:TextBox id="cashbankglnote" runat="server" Width="328px" CssClass="inpText" __designer:wfdid="w157" MaxLength="150"></asp:TextBox></TD><TD align=left>&nbsp;</TD><TD align=left>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="Label14" runat="server" Font-Size="X-Small" Text="Amount" __designer:wfdid="w158"></asp:Label> <asp:Label id="Label21" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w159"></asp:Label></TD><TD align=left><asp:TextBox id="cashbankglamt" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w160" AutoPostBack="True" MaxLength="20">0</asp:TextBox> <asp:Label id="lblCurr" runat="server" Font-Bold="True" Text="IDR" __designer:wfdid="w161" Visible="False"></asp:Label> <asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w162"></asp:ImageButton> <asp:ImageButton id="btnClear" onclick="btnClear_Click1" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w163"></asp:ImageButton></TD><TD align=left colSpan=2>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><FIELDSET id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtlCost" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w164" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankglnote,cashbankglamt,acctgOid,acctgdesc,acctgcode,seq" GridLines="None" PageSize="8" OnSelectedIndexChanged="GVDtlCost_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="Blue" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField HeaderText="ID" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="AcctgDesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="400px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="500px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail biaya"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV><TABLE id="tbladd" class="gvhdr" width="98%" runat="server" Visible="false"><TBODY><TR><TD style="WIDTH: 51px"></TD><TD style="WIDTH: 91px">Kode</TD><TD style="WIDTH: 400px">Deskripsi</TD><TD style="WIDTH: 119px" align=center>Total</TD><TD title="Width:300px" align=center colSpan=2>Catatan</TD></TR></TBODY></TABLE></FIELDSET></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><asp:Label id="lblTotExpense" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Receipt : " __designer:wfdid="w165"></asp:Label> <asp:Label id="amtcost" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w166"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><SPAN style="FONT-SIZE: 10pt; COLOR: #696969">Last update On </SPAN><asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w167"></asp:Label> By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w168"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton style="HEIGHT: 23px" id="btnsave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:wfdid="w169"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" CausesValidation="False" __designer:wfdid="w170"></asp:ImageButton> <asp:ImageButton id="btnPosting2" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w171"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:wfdid="w172"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:wfdid="w173" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" runat="server" Width="66px" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="24px" __designer:wfdid="w174" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w175" AssociatedUpdatePanelID="UpdatePanel5" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w176"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w177" PopupButtonID="btnDueDate" Format="dd/MM/yyyy" TargetControlID="payduedate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce7" runat="server" __designer:wfdid="w178" PopupButtonID="imbCBDate" Format="dd/MM/yyyy" TargetControlID="cashbankdate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w179" TargetControlID="payduedate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meedate" runat="server" __designer:wfdid="w180" TargetControlID="cashbankdate" CultureName="id-ID" MaskType="Date" Mask="99/99/9999" InputDirection="RightToLeft" ClearMaskOnLostFocus="true"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="amtdtlext" runat="server" __designer:wfdid="w181" TargetControlID="cashbankglamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="excashbankglamt" runat="server" __designer:wfdid="w182" TargetControlID="cashbankglamt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><asp:HiddenField id="HiddenField2" runat="server" __designer:wfdid="w183"></asp:HiddenField> <asp:HiddenField id="HiddenField1" runat="server" __designer:wfdid="w184"></asp:HiddenField></TD></TR><TR><TD align=center colSpan=4><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w185"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w186" Visible="False" BorderStyle="Solid"><TABLE id="Table2" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD vAlign=middle align=center colSpan=5 rowSpan=1><asp:Label id="Label1" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Akun" __designer:wfdid="w187"></asp:Label></TD></TR><TR><TD vAlign=middle colSpan=5><asp:Label id="lbldesc" runat="server" Font-Size="X-Small" Font-Bold="False" Text="Filter : " __designer:wfdid="w188"></asp:Label>&nbsp;<asp:DropDownList id="ddlFindAcctg" runat="server" CssClass="inpText" __designer:wfdid="w189"><asp:ListItem Value="acctgcode">Code</asp:ListItem>
<asp:ListItem Value="acctgdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFind" runat="server" CssClass="inpText" __designer:wfdid="w190"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind" onclick="btnFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w191"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" onclick="btnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w192"></asp:ImageButton> </TD></TR><TR><TD colSpan=5><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div5"><TABLE class="gvhdr" width="100%"><TBODY><TR><TD style="WIDTH: 64px"></TD><TD style="WIDTH: 91px">Kode</TD><TD>Deskripsi</TD></TR></TBODY></TABLE></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 95%"><asp:GridView id="GvAccCost" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w193" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc" GridLines="None" PageSize="5" OnSelectedIndexChanged="GvAccCost_SelectedIndexChanged" ShowHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="350px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!"></asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp;</DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 18px; TEXT-ALIGN: center" colSpan=5><asp:LinkButton id="LinkButton4" onclick="LinkButton4_Click" runat="server" __designer:wfdid="w194">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHidden" runat="server" __designer:wfdid="w195" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w196" TargetControlID="btnHidden" PopupControlID="Panel1" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="Label1"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>

                        </ContentTemplate>
                        <HeaderTemplate>
                            <span style="font-size: 9pt">
                                <strong><span>
                                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                    Form Cash/Bank Receipt</span></strong></span> <strong><span style="font-size: 9pt">:.</span></strong>


                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Nama COA">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
