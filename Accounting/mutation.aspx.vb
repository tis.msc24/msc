'Prgmr:Bhie Nduuutt | LastUpdt:30.03.2013
'Last update By Shutup_M 
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports Koneksi
Imports ClassFunction
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Accounting_mutation
    Inherits System.Web.UI.Page

#Region "Variabel"
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public DefCounter As String = 4
    Dim cKon As New Koneksi
    Dim cfunction As New ClassFunction
    Public sql_temp As String
    Dim tempPayFlag As String = ""
#End Region

#Region "Prosedure"
    Private Function FillGVCOA(ByRef oGVObject As GridView, ByVal sVar() As String, ByVal sCmpCode As String, Optional ByVal sFilter As String = "") As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillGVCOA = True
        oGVObject.DataSource = Nothing : oGVObject.DataBind()
        Dim sCode As String = "" : Dim sSql As String = ""
        For C1 As Integer = 0 To sVar.Length - 1
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar(C1) & "' AND interfaceres1='" & sCmpCode & "'"
            Dim sTmp As String = GetStrData(sSql)
            If sTmp = "" Then
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar(C1) & "' AND interfaceres1='" & cmpcode & "'"
                sTmp = GetStrData(sSql)
            End If
            If sCode = "" Then
                sCode = sTmp
            Else
                sCode &= ", " & sTmp
            End If
        Next
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' " & sFilter & " AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C2 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C2)) & "%'"
                If C2 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(oGVObject, sSql, "QL_mstacctg")
        End If
        If oGVObject.Rows.Count < 1 Then
            FillGVCOA = False
        End If
        Return FillGVCOA
    End Function

    Private Sub bindAccounting(ByVal sFind As String)
        If lblCoa.Text <> 0 Then
            If payflag.SelectedValue = "BGK" Then
                Dim sVar() As String = {"VAR_MUTATION"}
                FillGVCOA(GvAccCost, sVar, dd_branch.SelectedValue, "AND " & ddlFindAcctg.SelectedValue & " LIKE '%" & Tchar(txtFind.Text) & "%' AND a.acctgoid <> " & lblCoa.Text & "")
            Else
                Dim sVar() As String = {"VAR_MUTATION", "VAR_MUTATION"}
                FillGVCOA(GvAccCost, sVar, dd_branch.SelectedValue, "AND " & ddlFindAcctg.SelectedValue & " LIKE '%" & Tchar(txtFind.Text) & "%' AND a.acctgoid <> " & lblCoa.Text & "")
            End If
            ModalPopupExtender1.Show()
        Else
            showMessage("Cash Account belum dipilih", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End If
        btnHidden.Visible = True : Panel1.Visible = True
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub binddata()
        If IsDate(toDate(txtPeriode1.Text)) = False Or IsDate(toDate(txtPeriode1.Text)) = False Then
            showMessage("Periode awal salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If IsDate(toDate(txtPeriode2.Text)) = False Or IsDate(toDate(txtPeriode2.Text)) = False Then
            showMessage("Periode akhir salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
 
        sSql = "Select * from (SELECT cb.branch_code, cb.cmpcode, cb.cashbankoid, cashbankno, cashbankdate, (SELECT SUM(cashbankglamt) FROM QL_cashbankgl p WHERE p.cashbankoid=cb.cashbankoid AND p.cmpcode=cb.cmpcode AND p.branch_code = cb.branch_code) cashbankglamt, cashbankstatus, cashbanknote, cashbankacctgoid, case Cb.cashbanktype when 'BKK' THEN 'KAS : ' + acc.ACCTGCODE +'-'+ acc.acctgdesc ELSE 'BANK: ' + acc.ACCTGCODE +'-'+ acc.acctgdesc END source,g.currencycode rate, cb.cashbankcurroid, cb.cashbankcurrate FROM QL_trncashbankmst cb inner join ql_mstcurr g on g.currencyoid=cb.cashbankcurroid INNER JOIN ql_mstacctg acc ON cb.cmpcode=acc.cmpcode AND cb.cashbankacctgoid=acc.acctgoid WHERE cb.cashbankno like '%" & TcharNoTrim(nobukti.Text) & "%' and cb.cashbankacctgoid in (select ao.acctgoid from ql_mstacctg ao) AND cashbankgroup='MUTATION' and cashbanktype in ('BKK','BBK') UNION ALL " & _
        "SELECT cb.BRANCH_CODE, cb.cmpcode, cb.cashbankoid, cashbankno, cashbankdate, (SELECT SUM(cashbankglamt) FROM QL_trncbcloseddtl p WHERE p.cashbankoid=cb.cashbankoid AND p.cmpcode=cb.cmpcode) cashbankglamt, cashbankstatus,cashbanknote, cashbankacctgoid, case Cb.cashbanktype when 'BKK' THEN 'KAS : ' + acc.ACCTGCODE +'-'+ acc.acctgdesc ELSE 'BANK: ' + acc.ACCTGCODE +'-'+ acc.acctgdesc END source, g.currencycode rate, cb.cashbankcurroid, cb.cashbankcurrate FROM QL_trncbclosedmst cb inner join ql_mstcurr g on g.currencyoid=cb.cashbankcurroid INNER JOIN ql_mstacctg acc ON cb.cmpcode=acc.cmpcode AND cb.cashbankacctgoid=acc.acctgoid WHERE cb.cashbankno like '%" & TcharNoTrim(nobukti.Text) & "%' and cb.cashbankacctgoid in (select ao.acctgoid from ql_mstacctg ao) AND cashbankgroup='MUTATION' and cashbanktype in ('BKK','BBK') )cb Where cb.cmpcode = '" & cmpcode & "'"
        '" & IIf(sourceFilter.SelectedValue = "ALL", "", " and acc.acctgoid=" & COAsourcefilter.SelectedValue)
 
        If cbPeriode.Checked Then
            If txtPeriode1.Text = "" And txtPeriode2.Text = "" Then
                showMessage("Tolong isi periode!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If CDate(toDate(txtPeriode1.Text)) <= CDate(toDate(txtPeriode2.Text)) Then
                sSql &= " AND cashbankdate>='" & CDate(toDate(txtPeriode1.Text)) & "' and cashbankdate<dateadd(day,1,'" & CDate(toDate(txtPeriode2.Text)) & "') "
            Else
                showMessage("Tanggal akhir harus <= Tanggal awal!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            'If txtPeriode1.Text <> "" And txtPeriode2.Text <> "" Then

            'ElseIf txtPeriode1.Text <> "" And txtPeriode2.Text = "" Then
            '    sSql &= " AND cashbankdate >='" & CDate(toDate(txtPeriode1.Text)) & "'"
            'ElseIf txtPeriode1.Text = "" And txtPeriode2.Text <> "" Then
            '    sSql &= " AND cashbankdate<='" & CDate(toDate(txtPeriode2.Text)) & "'"
            'End If 
        End If

        If cbDDL.SelectedValue <> "ALL" Then
            sSql &= " and cb.BRANCH_CODE='" & cbDDL.SelectedValue & "'"
        End If

        If statuse.SelectedValue <> "ALL" Then
            sSql &= " and cb.cashbankstatus='" & statuse.SelectedValue & "'"
        End If

        sSql &= " ORDER BY cashbankdate desc, cashbankno desc"
        sql_temp = sSql
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "mutation")
        Session("tbldata") = objTable : gvmstcost.DataSource = objTable
        gvmstcost.DataBind()
        'UnabledCheckBox() 
        'calcTotalInGrid()
    End Sub

    Public Sub bindLastSearched()
        If (Not Session("SearchMutation") Is Nothing) Or (Not Session("SearchMutation") = "") Then
            Dim mySqlConn As New SqlConnection(ConnStr)
            Dim sqlSelect As String = Session("SearchMutation")

            Dim objTable As DataTable = cKoneksi.ambiltabel(sqlSelect, "cost")
            Session("tbldata") = objTable
            gvmstcost.DataSource = objTable
            gvmstcost.DataBind()
            UnabledCheckBox()
            mySqlConn.Close()
            calcTotalInGrid()
        End If
    End Sub

    Public Sub initDDLcabangto(ByVal cashbank As String)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        If acctgcode.Text = "" Then
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
            FillDDL(ddlcabangto, sSql)
        Else
            sSql = "select isnull(branch_code,'') from ql_mstacctg where acctgcode = '" & cashbank & "'"
            Dim gencode As String = ""
            If GetStrData(sSql) = "" Then
                showMessage("silahkan set branch code pada akun ini di master COA !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            Else
                gencode = GetStrData(sSql)
            End If
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and gencode='" & gencode & "'"
            FillDDL(ddlcabangto, sSql)
        End If
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(cbDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(cbDDL, sSql)
            Else
                FillDDL(cbDDL, sSql)
                cbDDL.Items.Add(New ListItem("ALL", "ALL"))
                cbDDL.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(cbDDL, sSql)
            cbDDL.Items.Add(New ListItem("ALL", "ALL"))
            cbDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_branch, sSql)
            Else
                FillDDL(dd_branch, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dd_branch, sSql)
        End If
    End Sub

    Public Sub initDDLAll(ByVal cashbank As String)
        sSql = "select currencyoid, currencycode from QL_mstcurr order by currencycode"
        FillDDL(currate, sSql)
        FillCurrencyRate(currate.SelectedValue)

        Dim varCash As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='VAR_PPN_JUAL' ")
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varCash & "%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"

        Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet

        'FillGVCOA()
        If cashbank = "" Then
            ' mengisi data pada DDL cmpcode
            sSql = "SELECT gendesc,genother1 FROM QL_mstgen WHERE gengroup='Branch Code' AND cmpcode='" & cmpcode & "'"
            mySqlDA = New SqlDataAdapter(sSql, ConnStr)
            objDs = New DataSet : mySqlDA.Fill(objDs, "dataA")
        Else
            cashbankacctgoid.Items.Clear()
            ' untuk mengisi data pada DDL CashBank
            If Trim(cashbank) = "CASH" Then
                FillDDLAcctg(cashbankacctgoid, "VAR_CASH", "" & cmpcode & "")
            ElseIf Trim(cashbank) = "BANK" Then
                FillDDLAcctg(cashbankacctgoid, "VAR_BANK", "" & cmpcode & "")
            End If
        End If
    End Sub

    Private Sub initPayactg()
        Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet

        sSql = "SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode like '1103%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )  "

        mySqlDA = New SqlDataAdapter(sSql, ConnStr)
        objDs = New DataSet
        mySqlDA.Fill(objDs, "dataB")

        If objDs.Tables("dataB").Rows.Count <= 0 Then
            showMessage("Isi/Buat account Payable di master accounting!!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End If
    End Sub

    Private Sub initddlcasbank(ByVal cashbank As String)
        cashbankacctgoid.Items.Clear()
        If Trim(cashbank) = "CASH" Then
            FillDDLAcctg(cashbankacctgoid, "VAR_CASH", dd_branch.SelectedValue)
            If cashbankacctgoid.Items.Count < 1 Then
                showMessage("Isi/Buat account CASH di master accounting!!", _
                    CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            End If
        Else
            FillDDLAcctg(cashbankacctgoid, "VAR_BANK", dd_branch.SelectedValue)
            If cashbankacctgoid.Items.Count < 0 Then
                showMessage("Isi/Buat account Bank di master accounting!!", _
                    CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            End If
        End If
    End Sub

    Public Sub fillTextBox(ByVal vpayid As String, ByVal status As String, ByVal branchcode As String)
        Try
            sSql = "SELECT cb.branch_code,cb.cashbankoid,cb.upduser,cb.updtime, case when cashbanktype='BKK' then 'CASH' else 'BANK' end payflag,cashbankacctgoid,cashbankstatus,cb.cmpcode,(Select SUM(p.cashbankglamt) From QL_cashbankgl p Where p.cashbankoid=cb.cashbankoid AND p.branch_code=cb.branch_code), cashbanknote, cashbankdate, cb.cashbankrefno refno, cb.cashbankno, cashbankcurroid, cashbankcurrate, link_nobukti, cb.createtime FROM QL_trncashbankmst cb Where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid=" & vpayid & " AND cashbankstatus='" & status & "' AND cb.branch_code='" & branchcode & "' UNION ALL SELECT cb.branch_code, cb.cashbankoid, cb.upduser, cb.updtime, case when cashbanktype='BKK' then 'CASH' else 'BANK' end payflag, cashbankacctgoid, cashbankstatus, cb.cmpcode, (Select SUM(p.cashbankglamt) From QL_cashbankgl p Where p.cashbankoid=cb.cashbankoid AND p.branch_code=cb.branch_code), cashbanknote, cashbankdate, cb.cashbankrefno refno,cb.cashbankno, cashbankcurroid, cashbankcurrate, link_nobukti, cb.createtime FROM QL_trncbclosedmst cb Where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid=" & vpayid & " AND cashbankstatus='" & status & "' AND cb.branch_code='" & branchcode & "'"

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader

            If xreader.HasRows Then
                While xreader.Read
                    dd_branch.SelectedValue = branchcode
                    Session("vNoCashBank") = Trim(xreader("cashbankoid"))
                    cashbankoid.Text = Trim(xreader("cashbankoid"))
                    payflag.SelectedValue = Trim(xreader("payflag"))
                    HiddenField1.Value = Trim(xreader("payflag"))
                    lblPOST.Text = Trim(xreader("cashbankstatus"))
                    If payflag.SelectedValue = "CASH" Then
                        Label16.Text = "Cash Account"
                        payrefno.Text = "" : payduedate.Text = ""
                    Else
                        Label16.Text = "Bank Account"
                        If IsDBNull(xreader("refno")) Then payrefno.Text = "" Else payrefno.Text = Trim(xreader("refno"))
                        If IsDBNull(xreader("updtime")) Then payduedate.Text = "" Else payduedate.Text = Format(xreader("updtime"), "dd/MM/yyyy")
                    End If
                    cashbankdate.Text = Format(xreader("cashbankdate"), "dd/MM/yyyy")
                    cashbanknote.Text = xreader("cashbanknote")
                    currate.SelectedValue = Trim(xreader.Item("cashbankcurroid"))
                    currencyrate.Text = Trim(xreader.Item("cashbankcurrate"))
                    cashbankno.Text = Trim(xreader.Item("cashbankno"))
                    initddlcasbank(payflag.SelectedValue)
                    If payflag.SelectedValue = "CASH" Then
                        setcontrolCash(False)
                        payrefno.Text = "" : payduedate.Text = ""
                    ElseIf payflag.SelectedValue = "BANK" Then
                        ' PanelNonCash.Visible = True
                        setcontrolCash(True)
                    End If
                    cashbankacctgoid.SelectedValue = Trim(xreader.Item("cashbankacctgoid"))
                    lblCoa.Text = cashbankacctgoid.SelectedValue
                    txtCoa.Text = cashbankacctgoid.SelectedItem.Text.Trim
                    HiddenField2.Value = Trim(xreader("payflag"))
                    upduser.Text = Trim(xreader.Item("upduser"))
                    updtime.Text = Trim(xreader.Item("updtime"))
                    If lblPOST.Text.ToUpper = "POST" Or lblPOST.Text.ToUpper = "CLOSED" Then
                        btnPosting2.Visible = False : btnsave.Visible = False
                        btnDelete.Visible = False : btnAddToList.Visible = False
                        lblPOST.Text = Trim(xreader("cashbankstatus"))
                    Else
                        btnPosting2.Visible = True : btnsave.Visible = True
                        btnDelete.Visible = True : btnAddToList.Visible = True
                        lblPOST.Text = ""
                    End If
                    link_nobukti.Text = Trim(xreader.Item("link_nobukti"))
                    createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss tt")
                End While
            End If
            conn.Close()

            sSql = "SELECT gl.cashbankgloid seq, gl.acctgoid, gl.cashbankglamt, gl.cashbankglnote,acc.acctgcode, acc.acctgdesc, acc.branch_code FROM QL_cashbankgl gl INNER JOIN QL_trncashbankmst cb ON gl.cmpcode=cb.cmpcode AND gl.cashbankoid=cb.cashbankoid AND gl.branch_code=cb.branch_code INNER JOIN QL_mstacctg acc ON acc.acctgoid=gl.acctgoid WHERE cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid=" & vpayid & " AND cashbankstatus='" & status & "' AND cb.branch_code = '" & branchcode & "' UNION ALL SELECT gl.cashbankgloid seq, gl.acctgoid, gl.cashbankglamt, gl.cashbankglnote, acc.acctgcode, acc.acctgdesc, acc.branch_code FROM QL_trncbcloseddtl gl INNER JOIN QL_trncbclosedmst cb ON gl.cmpcode=cb.cmpcode AND gl.cashbankoid=cb.cashbankoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=gl.acctgoid WHERE cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid=" & vpayid & " AND cashbankstatus='" & status & "' AND cb.branch_code='" & branchcode & "'"

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDs As New DataSet

            mySqlDA.Fill(objDs, "dataCost")
            GVDtlCost.Visible = True

            Dim dv As DataView = objDs.Tables("dataCost").DefaultView
            dv.Sort = "seq desc"

            GVDtlCost.DataSource = objDs.Tables("dataCost")
            GVDtlCost.DataBind()
            Session("dtlTable") = objDs.Tables("dataCost")
            conn.Close()
            calcTotalInGridDtl()
            generateCostID()
            TabContainer1.ActiveTabIndex = 1
        Catch ex As Exception
            conn.Close()
            showMessage(ex.ToString & " <br />" & sSql, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Private Sub generateCashBankID()
        cashbankoid.Text = GenerateID("QL_trncashbankmst", cmpcode)
        cashbankno.Text = GenerateID("QL_trncashbankmst", cmpcode)
    End Sub

    Private Sub generateCostID()
        cashbankgloid.Text = GenerateID("QL_cashbankgl", cmpcode)
    End Sub

    Private Sub generateCashBankNo(ByVal cashbanktype As String)
        Dim sCBType As String = ""
        Dim iCurID As Integer = 0

        If cashbanktype = "CASH" Then sCBType = "BKK" Else sCBType = "BBK"
        sSql = "SELECT MAX(ABS(Right(RTRIM(cashbankno),LEN(cashbankno)-3))) cashbankno FROM QL_trncashbankmst WHERE LEFT(cashbankno,3)='" & sCBType & "'"
        xCmd.CommandText = sSql

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        If Not IsDBNull(xCmd.ExecuteScalar) Then
            iCurID = xCmd.ExecuteScalar + 1
        Else
            iCurID = 1
        End If
        conn.Close()
        Session("vNoCashBank") = GenNumberString(sCBType, "", iCurID, DefCounter)
    End Sub

    Protected Sub calcTotalInGrid()
        Dim iCountrow As Integer = gvmstcost.Rows.Count
        Dim iGtotal As Double = 0

        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(gvmstcost.Rows(i).Cells(3).Text)
        Next
        lblgrandtotal.Text = ToMaskEdit(ToDouble(iGtotal), 3)
    End Sub

    Protected Sub calcTotalInGridDtl()
        Dim iCountrow As Integer = GVDtlCost.Rows.Count
        Dim iGtotal As Double = 0
        Dim dt_temp As New DataTable
        dt_temp = Session("dtlTable")
        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(GVDtlCost.Rows(i).Cells(4).Text)

            dt_temp.Rows(i).BeginEdit()
            dt_temp.Rows(i).Item(0) = i + 1
            dt_temp.Rows(i).EndEdit()
        Next
        Session("dtlTable") = dt_temp
        GVDtlCost.DataSource = dt_temp
        GVDtlCost.DataBind()

        Dim sCurr() As String = currate.SelectedItem.Text.Split("-")
        lblTotalMutation.Text = "Total Mutation : " & sCurr(0)
        amtcost.Text = ToMaskEdit(ToDouble(iGtotal), 4)
    End Sub

    Public Sub CheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("cashbankstatus").ToString) <> "POST" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try

    End Sub

    Public Sub UnabledCheckBox()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (gvmstcost.PageIndex * 10) To objRow.Length() - 1
                    Try
                        If Trim(objRow(C1)("cashbankstatus").ToString) = "POST" Then
                            Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        End If
                    Catch ex As Exception
                    End Try
                    irowne += 1
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("cashbankstatus").ToString) <> "POST" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try

    End Sub

    Protected Sub ClearDtlAP()
        acctgoid.Text = "" : lblAcctgOid.Text = ""
        cashbankglnote.Text = ""
        cashbankglamt.Text = "0"
        acctgcode.Text = ""
        AddToList.Text = "Add To List"
    End Sub

    Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 D.curratestoIDRbeli  from QL_mstcurrhist D INNER JOIN ql_mstcurr m on M.CMPCODE=D.cmpcode AND m.currencyoid=d.curroid and  m.cmpcode='" & cmpcode & "' and currencyoid=" & iOid & " order by d.currdate desc"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyrate.Text = ToMaskEdit(xCmd.ExecuteScalar, 2)
        conn.Close()
        'If currate.Items(currate.SelectedIndex).Text.Trim.ToUpper = "IDR" Then
        '    currencyrate.Text = "1.00"
        'End If
    End Sub

    Private Sub BindDataClosed()
        sSql = "Select cmpcode,cb.cashbankoid,cb.cashbankno,cb.cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, PIC_REFNAME, deptoid, createuser,createtime, bankoid,branch_code from ql_trncashbankmst cb Where cashbankgroup='MUTATION' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Order by cb.cashbankoid ASC"
        Dim cbMst As DataTable = cKoneksi.ambiltabel(sSql, "Ql_CbClosed")
        Session("ClosedMst") = cbMst

        sSql = "Select cb.cmpcode,cashbankgloid,cb.cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,cb.upduser,cb.updtime,duedate,refno,cashbankglstatus,cb.branch_code from QL_cashbankgl gl Inner Join QL_trncashbankmst cb ON cb.cashbankoid=gl.cashbankoid AND gl.branch_code=cb.branch_code Where cb.cashbankgroup='MUTATION' AND cb.cashbankstatus='' AND cb.cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Order by cb.cashbankoid ASC"
        Dim cbgl As DataTable = cKoneksi.ambiltabel2(sSql, "Ql_GlClosed")
        Session("ClosedDtl") = cbgl

        'Dim CbMstoid As Integer = GenerateID("QL_trncbclosedmst", cmpcode)
        'Dim CbDtloid As Integer = GenerateID("QL_trncbcloseddtl", cmpcode)
        'proses Pindah data 
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'insert mst
            For c1 As Int32 = 0 To cbMst.Rows.Count - 1
                sSql = "INSERT into QL_trncbclosedmst (cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, PIC_REFNAME, deptoid, createuser,createtime, bankoid,branch_code,flagexpedisi) VALUES " & _
                "('" & cmpcode & "'," & Integer.Parse(cbMst.Rows(c1)("cashbankoid")) & ",'" & cbMst.Rows(c1)("cashbankno") & "','CLOSED','" & cbMst.Rows(c1)("cashbanktype") & "','MUTATION'," & Integer.Parse(cbMst.Rows(c1)("cashbankacctgoid")) & ",'" & cbMst.Rows(c1)("cashbankdate") & "','" & Tchar(cbMst.Rows(c1)("cashbanknote")) & "','" & cbMst.Rows(c1)("upduser") & "',current_timestamp," & cbMst.Rows(c1)("cashbankcurroid") & "," & ToDouble(cbMst.Rows(c1)("cashbankcurrate")) & ", '" & Tchar(cbMst.Rows(c1)("pic")) & "', 'QL_mstperson', '" & Integer.Parse(cbMst.Rows(c1)("deptoid")) & "', '" & cbMst.Rows(c1)("createuser") & "','" & cbMst.Rows(c1)("createtime") & "'," & Integer.Parse(cbMst.Rows(c1)("bankoid")) & ",'" & cbMst.Rows(c1)("branch_code") & "','')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next
            'insert dtl
            For c1 As Int32 = 0 To cbgl.Rows.Count - 1
                sSql = "INSERT into QL_trncbcloseddtl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,upduser,updtime,duedate,refno,cashbankglstatus,branch_code) VALUES " & _
                "('" & cmpcode & "'," & Integer.Parse(cbgl.Rows(c1)("cashbankgloid")) & "," & Integer.Parse(cbgl.Rows(c1)("cashbankoid")) & "," & Integer.Parse(cbgl.Rows(c1)("acctgoid")) & "," & cbgl.Rows(c1)("cashbankglamt") & "," & cbgl.Rows(c1)("cashbankglamtidr") & "," & cbgl.Rows(c1)("cashbankglamtusd") & ",'" & Tchar(cbgl.Rows(c1)("cashbankglnote")) & "','" & Tchar(cbgl.Rows(c1)("upduser")) & "','" & cbgl.Rows(c1)("updtime") & "','" & cbgl.Rows(c1)("duedate") & "','" & Tchar(cbgl.Rows(c1)("refno")) & "','" & Tchar(cbgl.Rows(c1)("cashbankglstatus")) & "','" & Tchar(cbgl.Rows(c1)("branch_code")) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            sSql = "DELETE FROM QL_cashbankgl WHERE cashbankoid IN (Select cashbankoid from ql_trncashbankmst Where cashbankgroup='MUTATION' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)))"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trncashbankmst WHERE cashbankoid IN (Select cashbankoid from ql_trncashbankmst Where cashbankgroup='MUTATION' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)))"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & " <br />" & sSql, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

#End Region

#Region "Function"

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("seq", Type.GetType("System.Int32"))
        dt.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dt.Columns.Add("cashbankglamt", Type.GetType("System.Decimal"))
        dt.Columns.Add("cashbankglnote", Type.GetType("System.String"))
        dt.Columns.Add("acctgcode", Type.GetType("System.String"))
        dt.Columns.Add("acctgdesc", Type.GetType("System.String"))
        dt.Columns.Add("branch_code", Type.GetType("System.String"))
        Return dt
    End Function

    Private Function getNewDT(ByVal xdt As DataTable) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("cmpcode")
        dt.Columns.Add("cashbankgloid")
        dt.Columns.Add("cashbankoid")
        dt.Columns.Add("acctgdesc")
        dt.Columns.Add("acctgoid")
        dt.Columns.Add("cashbankglamt")
        dt.Columns.Add("cashbankglnote")
        dt.Columns.Add("upduser")
        dt.Columns.Add("updtime")
        dt.Columns.Add("gendesc")
        dt.Columns.Add("acctgcode")

        For i As Integer = 0 To xdt.Rows.Count - 1
            Dim xrow As DataRow
            xrow = dt.NewRow
            xrow(0) = xdt.Rows(i).Item(0)
            xrow(1) = xdt.Rows(i).Item(1)
            xrow(2) = xdt.Rows(i).Item(2)
            xrow(3) = xdt.Rows(i).Item(3)
            xrow(4) = xdt.Rows(i).Item(4)
            xrow(5) = xdt.Rows(i).Item(5)
            xrow(6) = xdt.Rows(i).Item(6)
            xrow(7) = xdt.Rows(i).Item(7)
            xrow(9) = xdt.Rows(i).Item(9)
            xrow(10) = xdt.Rows(i).Item(10)
            dt.Rows.Add(xrow)
        Next

        Return dt
    End Function

    Public Function InvoiceRate(ByVal curroid As Integer) As Double
        Dim curRate As Double
        sSql = "SELECT isnull(( CASE WHEN currencyoid = 1 THEN rate2usdvalue ELSE rate2idrvalue END ), 0 ) rateValue FROM ql_mstrate2 WHERE rate2month = MONTH (getdate()) AND rate2year = YEAR (GETDATE()) and currencyoid = " & curroid & " ORDER BY rate2oid DESC"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curRate = xCmd.ExecuteScalar
        Return curRate
        conn.Close()
        Return curRate
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" And Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        sSql = "Select Count(cashbankoid) from ql_trncashbankmst Where cashbankgroup='MUTATION' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))"
        Dim cOs As Double = GetStrData(sSql)
        If ToDouble(cOs) > 0 Then
            BindDataClosed()
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim lokasioid As Int32 = Session("lokasioid")
            Dim lokasi As String = Session("lokasi")
            Dim lokasino As String = Session("lokasino")
            Dim sqlReceipt As String = Session("SearchMutation")
            Dim cbType As String = Session("CashBankType")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("lokasioid") = lokasioid
            Session("lokasi") = lokasi
            Session("lokasino") = lokasino
            Session("SearchMutation") = sqlReceipt
            Response.Redirect("~/accounting/mutation.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("CashBankType") = "ALL"
        Session("oid") = Request.QueryString("cashbankoid")
        Session("status") = Request.QueryString("cashbankstatus")
        Session("branch_code") = Request.QueryString("branch_code")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting2.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        BtnPostSelected.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST selected data?');")
        Page.Title = CompnyName & " - Mutation"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            fDDLBranch() : InitDDLBranch() : initDDLAll("")
            initddlcasbank("CASH") : initDDLcabangto("")
            AddToList.Text = "Add To List"

            If Session("CashBankType") = "" Then
                Response.Redirect("~\Other\NotAuthorize.aspx")
            End If
            If Session("CashBankType") <> "ALL" Then
                payflag.SelectedIndex = 0 : payflag.Enabled = False
                payflag_SelectedIndexChanged(Nothing, Nothing)
            Else
                payflag.SelectedIndex = 0 : payflag.Enabled = True
                payflag_SelectedIndexChanged(Nothing, Nothing)
            End If

            txtPeriode1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            binddata()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                fillTextBox(Session("oid"), Session("status"), Session("branch_code"))
                lblIO.Text = "U P D A T E" : gvmstcost.Visible = True
                gvmstcost.DataSource = Session("tbldata")
                gvmstcost.DataBind() ': UnabledCheckBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                setcontrolCash(False)
                cashbankdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                generateCostID() : generateCashBankID()
                btnDelete.Visible = False
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime() : amtcost.Text = "0.00"
                lblIO.Text = "N E W"
                TabContainer1.ActiveTabIndex = 0
            End If
            'Dim dtlTable As DataTable : dtlTable = Session("dtlTable")
            'GVDtlCost.DataSource = dtlTable : GVDtlCost.DataBind()
            'calcTotalInGridDtl()
        End If

        If lblPOST.Text = "POST" Or lblPOST.Text = "DELETE" Then
            btnshowCOA.Visible = False : btnPrint.Visible = True
        Else
            btnPrint.Visible = False : btnshowCOA.Visible = False
        End If

        If cashbankno.Text.IndexOf("BKM") >= 0 Or cashbankno.Text.IndexOf("BBM") >= 0 Then
            Label5.Text = ".: Bukti Setoran Transfer(Mutation) (Auto BKM/BBM:" & link_nobukti.Text & ")"
            Label9.Text = "Tujuan"
        End If
    End Sub

    Sub setcontrolCash(ByVal status As Boolean)
        Label17.Visible = status
        payrefno.Visible = status
        'payduedate.Visible = status
        'Label10.Visible = status
        'btnDueDate.Visible = status
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initddlcasbank(payflag.SelectedValue)
        If Trim(payflag.SelectedValue) = "BANK" Then
            Label16.Text = "Bank Account"
            setcontrolCash(True)
        Else
            Label16.Text = "Cash Account"
            setcontrolCash(False)
        End If
        BindAkun() : lblCoa.Text = 0 : txtCoa.Text = ""
    End Sub

    Private Sub BindAkun()
        Dim sVar As String = ""
        Dim dVal As Boolean = False
        If payflag.SelectedValue = "CASH" Then
            sVar = "VAR_CASH"
            dVal = True
        ElseIf payflag.SelectedValue = "BANK" Then
            sVar = "VAR_BANK"
            dVal = True
        End If

        sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & dd_branch.SelectedValue & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "?" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & cmpcode & "'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "?" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND (acctgdesc LIKE '%" & Tchar(txtCoa.Text.Trim) & "%' OR acctgcode LIKE '%" & Tchar(txtCoa.Text.Trim) & "%') AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(GvCoa, sSql, "QL_mstacctg")
        End If
        GvCoa.Visible = True
    End Sub

    Protected Sub GvAccCost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlFindAcctg.SelectedIndex = 0 : txtFind.Text = ""
        acctgoid.Text = GvAccCost.SelectedDataKey.Item("acctgcode").ToString().Trim & " - " & GvAccCost.SelectedDataKey.Item("acctgdesc").ToString()
        acctgcode.Text = GvAccCost.SelectedDataKey.Item("acctgcode").ToString()
        lblAcctgOid.Text = GvAccCost.SelectedDataKey.Item("acctgoid").ToString()
        btnHidden.Visible = False
        Panel1.Visible = False
        ModalPopupExtender1.Hide()
        GvAccCost.DataSource = Nothing
        initDDLcabangto(acctgcode.Text)
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sTempSql As String = " AND " & ddlFindAcctg.SelectedValue & " LIKE '%" & Tchar(txtFind.Text) & "%' "
        bindAccounting(sTempSql)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ddlFindAcctg.SelectedIndex = 0 : txtFind.Text = "" : bindAccounting("")
    End Sub

    Protected Sub ClosePopUP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnHidden.Visible = False : Panel1.Visible = False : ModalPopupExtender1.Hide()
    End Sub

    Protected Sub GVDtlCost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        cashbankglnote.Text = GVDtlCost.SelectedDataKey.Item(0).ToString
        cashbankglamt.Text = ToMaskEdit(GVDtlCost.SelectedDataKey.Item(1).ToString, 4)
        acctgoid.Text = GVDtlCost.SelectedDataKey.Item(4).ToString & "-" & GVDtlCost.SelectedDataKey.Item(3).ToString
        lblAcctgOid.Text = GVDtlCost.SelectedDataKey.Item(2).ToString
        acctgcode.Text = GVDtlCost.SelectedDataKey("acctgcode").ToString
        initDDLcabangto(acctgcode.Text)
        AddToList.Text = "EDIT"
    End Sub

    Protected Sub GVDtlCost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlCost.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
        End If
    End Sub

    Protected Sub GVDtlPayAP_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlCost.RowDeleting
        If lblPOST.Text <> "POST" Then
            Dim objTable As DataTable
            objTable = Session("dtlTable")
            Dim iCountrow As Integer = objTable.Rows.Count
            Dim idx As Integer = iCountrow - 1 - e.RowIndex

            objTable.Rows.RemoveAt(idx)
            Session("dtlTable") = objTable
            GVDtlCost.DataSource = objTable
            GVDtlCost.DataBind()
            calcTotalInGridDtl()
        Else
            showMessage("Data terpost tidak dapat di hapus!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing
        gvmstcost.PageIndex = 0 : binddata()
        Session("SearchMutation") = sql_temp
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nobukti.Text = ""
        Session("dtlTable") = Nothing
        statuse.SelectedIndex = 0
        txtPeriode1.Text = Format(GetServerTime(), "01/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        'cbPeriode_CheckedChanged(Nothing, Nothing)
        binddata() : Session("SearchMutation") = sql_temp
    End Sub

    Protected Sub gvmstcost_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvmstcost.PageIndexChanging
        gvmstcost.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UncheckAll()
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP()
        cashbankgloid.Text = Session("vIDCost")
    End Sub

    Protected Sub btnAddToList_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim sMsg As String = ""
            If acctgoid.Text = "" Then
                sMsg &= "- Maaf, Account mutasi belum dipilih..!!<BR>"
            End If

            If ToDouble(cashbankglamt.Text) <= 0 Then
                sMsg &= "- Maaf, Jumlah harus > 0..!!<br >"
            End If

            sSql = "Select isnull(branch_code,'') From ql_mstacctg where acctgoid = " & lblAcctgOid.Text & ""
            If GetScalar(sSql) = "" Then
                sMsg &= "- Maaf, silahkan set branch code pada akun ini di master COA..!!<br >"
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If lblAcctgOid.Text <> "" Then
                '.lblamt.Visible = False
                ' .lblBranchCost.Visible = False
                Dim code As String = cKon.ambilscalar("SELECT acctgcode FROM ql_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgoid=" & lblAcctgOid.Text)
                Dim desc As String = cKon.ambilscalar("SELECT acctgdesc FROM ql_mstacctg WHERE " & _
                    "cmpcode='" & cmpcode & "' AND acctgoid=" & lblAcctgOid.Text)
                If AddToList.Text = "Add To List" Then
                    If IsNothing(Session("dtlTable")) Then
                        Dim dtlTableNew As DataTable = setTabelDetail()
                        Dim xrow As DataRow
                        xrow = dtlTableNew.NewRow
                        xrow(0) = Session("seq") + 1
                        xrow(1) = lblAcctgOid.Text
                        xrow(2) = ToDouble(cashbankglamt.Text)
                        xrow(3) = cashbankglnote.Text
                        xrow(4) = code
                        xrow(5) = desc
                        xrow(6) = ddlcabangto.SelectedValue
                        dtlTableNew.Rows.Add(xrow)
                        Session("seq") += 1
                        Session("dtlTable") = dtlTableNew
                        GVDtlCost.DataSource = dtlTableNew
                        GVDtlCost.DataBind()
                        calcTotalInGridDtl() : generateCostID() : ClearDtlAP()
                    Else
                        Dim dtlTable As DataTable
                        dtlTable = Session("dtlTable")
                        Dim dv As DataView = dtlTable.DefaultView
                        dv.RowFilter = "acctgoid = " & lblAcctgOid.Text & " and cashbankglnote = '" & cashbankglnote.Text & "'"

                        If dv.Count > 0 Then ' cek apakah data telah ada
                            ' kalo data telah ada
                            dv.RowFilter = ""
                            ' lblBranchCost.Visible = True
                            showMessage("Jika akun lebih dari 1 note tidak boleh sama!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                            ClearDtlAP()
                            Exit Sub
                        Else
                            ' kalo data blom ada
                            dv.RowFilter = ""
                            dv.Sort = "seq desc"
                            If dv.Count > 0 Then
                                Session("seq") = dv.Item(0).Item(0)
                            Else
                                Session("seq") = 0
                            End If
                            Dim xrow As DataRow
                            xrow = dtlTable.NewRow
                            xrow(0) = Session("seq") + 1
                            xrow(1) = lblAcctgOid.Text
                            xrow(2) = ToDouble(cashbankglamt.Text)
                            xrow(3) = cashbankglnote.Text
                            xrow(4) = code
                            xrow(5) = desc
                            xrow(6) = ddlcabangto.SelectedValue
                            dtlTable.Rows.Add(xrow)
                            Session("seq") += 1
                            Session("dtlTable") = dtlTable
                            Dim dv2 As DataView = dtlTable.DefaultView
                            dv2.Sort = "seq desc"
                            GVDtlCost.DataSource = dtlTable
                            GVDtlCost.DataBind()
                            calcTotalInGridDtl() : generateCostID() : ClearDtlAP()
                        End If
                    End If
                Else
                    ' untuk transaksi edit
                    'Dim xindex = GVDtlCost.SelectedIndex
                    Dim xindex = GVDtlCost.SelectedDataKey.Item(6).ToString - 1
                    Dim dt_temp As New DataTable
                    dt_temp = Session("dtlTable")
                    dt_temp.Rows(xindex).BeginEdit()
                    dt_temp.Rows(xindex).Item(1) = lblAcctgOid.Text
                    dt_temp.Rows(xindex).Item(2) = ToDouble(cashbankglamt.Text)
                    dt_temp.Rows(xindex).Item(3) = cashbankglnote.Text
                    dt_temp.Rows(xindex).Item(4) = code
                    dt_temp.Rows(xindex).Item(5) = desc
                    dt_temp.Rows(xindex).Item(6) = ddlcabangto.SelectedValue
                    dt_temp.Rows(xindex).EndEdit()

                    Session("dtlTable") = dt_temp
                    GVDtlCost.DataSource = dt_temp
                    GVDtlCost.DataBind()
                    AddToList.Text = "Add To List"
                    calcTotalInGridDtl() : generateCostID() : ClearDtlAP()
                End If
                GVDtlCost.SelectedIndex = -1
            End If
            currate.Enabled = False
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
        
        'payflag.Enabled = False
    End Sub

    Protected Sub btnClear_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP() : GVDtlCost.SelectedIndex = -1
        cashbankgloid.Text = Session("vIDCost")
    End Sub  

    Protected Sub LinkButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlFindAcctg.SelectedIndex = 0 : txtFind.Text = ""
        btnHidden.Visible = False
        Panel1.Visible = False
        ModalPopupExtender1.Hide()
        GvAccCost.DataSource = Nothing
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub BtnPostSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            Dim strSQL As String = ""
            Dim objConn As New SqlClient.SqlConnection(ConnStr)
            Dim objTrans As SqlClient.SqlTransaction
            Dim objCmd As New SqlClient.SqlCommand
            Dim dtCBGL As DataTable

            Dim irowne As Int16 = 0
            For i As Integer = 0 + (gvmstcost.PageIndex * 10) To objRow.Length() - 1
                Dim ambilPayFlag As String = GetStrData("select case when cashbanktype='BKK' then 'CASH' when cashbanktype='BGK' then 'GIRO' else 'NON CASH' end payflag FROM QL_trncashbankmst cb where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid='" & objTable.Rows(i)("cashbankoid") & "'")
                If ambilPayFlag = "CASH" Then
                    tempPayFlag = "CASH"
                ElseIf ambilPayFlag = "NON CASH" Then
                    tempPayFlag = "BANK"
                Else
                    tempPayFlag = "GIRO"
                End If
                Try
                    If getCheckBoxValue(irowne, 0) = True Then
                        Dim vIDMst As Integer = GenerateID("QL_trnglmst", cmpcode)
                        Dim vIDDtl As Integer = GenerateID("QL_trngldtl", cmpcode)
                        Dim vIDCBMst As Integer = GenerateID("QL_trncashbankmst", cmpcode)
                        Dim vIDCBGL As Integer = GenerateID("QL_cashbankgl", cmpcode)
                        dtCBGL = cKon.ambiltabel("SELECT * FROM ql_cashbankgl WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & objTable.Rows(i)("cashbankoid") & " AND branch_code = '" & dd_branch.SelectedValue & "' ", "ql_cashbankgl")

                        objConn.Open()
                        objTrans = objConn.BeginTransaction()
                        objCmd.Connection = objConn
                        objCmd.Transaction = objTrans
                        Try
                            strSQL = "select cashbankcurrate from QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' and cashbankoid=" & objTable.Rows(i)("cashbankoid")
                            objCmd.CommandText = strSQL
                            Dim crate As Decimal = objCmd.ExecuteScalar

                            strSQL = "UPDATE QL_trncashbankmst SET cashbankstatus='POST' WHERE cmpcode='" & cmpcode & "' and cashbankoid=" & objTable.Rows(i)("cashbankoid") & " And branch_code='" & dd_branch.SelectedValue & "'"
                            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                            strSQL = "UPDATE QL_cashbankgl SET cashbankglstatus='POST' WHERE cashbankoid=" & objTable.Rows(i)("cashbankoid") & " AND cmpcode='" & cmpcode & "' and branch_code='" & dd_branch.SelectedValue & "'"
                            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                            '//INSERT CASHBANK INTO GL MST
                            strSQL = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,type,branch_code) VALUES ('" & cmpcode & "'," & vIDMst & ",'" & objTable.Rows(i)("cashbankdate") & "','" & GetDateToPeriodAcctg(objTable.Rows(i)("cashbankdate")) & "','" & tempPayFlag & " - Mutation|No=" & objTable.Rows(i)("cashbankno") & "','POST','" & GetServerTime() & "','" & Session("UserID") & "',current_timestamp,'MUTATION','" & dd_branch.SelectedValue & "')"
                            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                            '//INSERT CASHBANK INTO GL DTL
                            'VARCASHBANK
                            strSQL = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr, glamt,glamtidr,noref,glnote,upduser,updtime,branch_code) VALUES " & _
                            " ('" & cmpcode & "'," & vIDDtl & ",1," & vIDMst & "," & objTable.Rows(i)("cashbankacctgoid") & ",'C'," & objTable.Rows(i)("cashbankglamt") & "," & objTable.Rows(i)("cashbankglamt") & ",'" & objTable.Rows(i)("cashbankno") & "','" & tempPayFlag & " - Mutation|No=" & objTable.Rows(i)("cashbankno") & "','" & Session("UserID") & "',current_timestamp,'" & dd_branch.SelectedValue & "')"
                            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                            For C1 As Integer = 0 To dtCBGL.Rows.Count - 1
                                '//INSERT CASHBANKGL INTO GL DTL
                                'VARCOST 'branch_code='" & Session("branch_id") & "'
                                strSQL = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,noref,glnote,upduser,updtime,branch_code) VALUES " & _
" ('" & cmpcode & "'," & vIDDtl + 1 + C1 & ",2," & vIDMst & "," & dtCBGL.Rows(C1)("acctgoid") & ",'D'," & dtCBGL.Rows(C1)("cashbankglamt") & "," & dtCBGL.Rows(C1)("cashbankglamt") & ",'" & objTable.Rows(i)("cashbankno") & "','" & tempPayFlag & " - Mutation|No=" & objTable.Rows(i)("cashbankno") & "','" & Session("UserID") & "',current_timestamp,'" & dd_branch.SelectedValue & "')"
                                objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                                ' lawan mutasi (IN)
                                Dim sCBType As String : Dim iCurID As Integer = 0 : Dim sCBCode As String = ""
                                sSql = "SELECT interfacevalue FROM ql_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='VAR_CASH'"
                                objCmd.CommandText = sSql
                                Dim sCash As String = objCmd.ExecuteScalar

                                sSql = "SELECT acctgcode FROM ql_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgoid=" & dtCBGL.Rows(C1)("acctgoid")
                                objCmd.CommandText = sSql
                                Dim code As String = objCmd.ExecuteScalar

                                If Left(code, sCash.Length) = sCash Then sCBType = "BKM" Else sCBType = "BBM"
                                ' Cashbank Code
                                Dim CodeBranchNo As String = GetStrData("SELECT ISNULL(genother1,'') FROM QL_mstgen WHERE gengroup='CABANG' AND gencode='" & dd_branch.SelectedValue & "'")
                                Session("vNoCashBank") = sCBType & "/" & CodeBranchNo & "/" & Format(CDate(toDate(cashbankdate.Text)), "yy/MM/dd") & "/"
                                sSql = "SELECT MAX(ABS(replace(cashbankno,'" & Session("vNoCashBank") & "',''))) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & Session("vNoCashBank") & "%'"
                                objCmd.CommandText = sSql
                                If Not IsDBNull(objCmd.ExecuteScalar) Then
                                    iCurID = objCmd.ExecuteScalar + 1
                                Else
                                    iCurID = 1
                                End If
                                Session("vNoCashBank") = GenNumberString(Session("vNoCashBank"), "", iCurID, DefCounter)
                                ''branch_code='" & Session("branch_id") & "'
                                sSql = "INSERT into QL_trncashbankmst(cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype, cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime, posttime, postuser, link_nobukti, cashbankcurroid, cashbankcurrate,branch_code) VALUES ('" & _
                                    cmpcode & "'," & vIDCBMst + C1 & ",'" & Session("vNoCashBank") & "','POST','" & sCBType & "','MUTATION'," & dtCBGL.Rows(C1)("acctgoid") & ",'" & objTable.Rows(i)("cashbankdate") & "','" & Tchar(dtCBGL.Rows(C1).Item("cashbankglnote")) & "','" & Session("UserID") & "',current_timestamp, current_timestamp, '" & Session("User_id") & "', '" & objTable.Rows(i)("cashbankno") & "', " & objTable.Rows(i)("cashbankcurroid") & "," & objTable.Rows(i)("cashbankcurrate") & ",'" & dd_branch.SelectedValue & "')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                                Dim dateStr As String = "null" : Dim payref As String = "null"
                                If Not IsNothing(dtCBGL.Rows(C1).Item("duedate")) Then
                                    dateStr = "'" & dtCBGL.Rows(C1).Item("duedate") & "'"
                                    payref = "'" & dtCBGL.Rows(C1).Item("refno") & "'"
                                End If
                                sSql = "INSERT into QL_cashbankgl(cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglnote,upduser,updtime,duedate,refno,cashbankglstatus,cashbankglres1,branch_code) " & _
                                    "VALUES ('" & cmpcode & "'," & vIDCBGL + C1 & "," & vIDCBMst + C1 & "," & objTable.Rows(i)("cashbankacctgoid") & "," & dtCBGL.Rows(C1).Item("cashbankglamt") & ",'" & Tchar(objTable.Rows(i).Item("cashbanknote")) & "','" & Session("UserID") & "',current_timestamp," & dateStr & "," & payref & ",'POST'," & objTable.Rows(i)("cashbankoid") & ",'" & dd_branch.SelectedValue & "')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            Next
                            strSQL = "UPDATE QL_mstoid SET lastoid=" & vIDMst & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trnglmst'"
                            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                            strSQL = "UPDATE QL_mstoid SET lastoid=" & vIDDtl + dtCBGL.Rows.Count & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trngldtl'"
                            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                            strSQL = "UPDATE QL_mstoid SET lastoid=" & vIDCBMst + dtCBGL.Rows.Count - 1 & " WHERE cmpcode='" & cmpcode & "' AND tablename='QL_trncashbankmst'"
                            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                            strSQL = "UPDATE QL_mstoid SET lastoid=" & vIDCBGL + dtCBGL.Rows.Count - 1 & " WHERE cmpcode='" & cmpcode & "' AND tablename='QL_cashbankgl'"
                            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                            objTrans.Commit() : objCmd.Connection.Close()
                        Catch ex As Exception
                            objTrans.Rollback() : objCmd.Connection.Close()
                            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
                            Exit Sub
                        End Try
                    End If
                Catch ex As Exception
                End Try
                irowne += 1
            Next
        End If
        Response.Redirect("mutation.aspx?type=" & Session("CashBankType") & "&awal=true")
    End Sub

    Protected Sub gvmstcost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvmstcost.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            If e.Row.Cells(7).Text.Trim = "&nbsp;" Then
                e.Row.Cells(7).Text = "In Process"
            End If
        End If
    End Sub

    Protected Sub btnViewLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing
        If Session("SearchMutation") Is Nothing = False Then
            bindLastSearched()
        End If
    End Sub

    Protected Sub currate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles currate.SelectedIndexChanged
        FillCurrencyRate(currate.SelectedValue)
        initddlcasbank(payflag.SelectedValue)
        If Trim(payflag.SelectedValue) = "BANK" Then
            Label16.Text = "Bank Account"
            setcontrolCash(True)
        Else
            Label16.Text = "Cash Account"
            setcontrolCash(False)
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA(cashbankno.Text, dd_branch.SelectedValue, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
            e.Row.Cells(3).Text = NewMaskEdit(e.Row.Cells(3).Text)

        End If
    End Sub

    Protected Sub cashbankglamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankglamt.TextChanged
        If cashbankglamt.Text.Trim = "" Then
            cashbankglamt.Text = 0
        Else
            cashbankglamt.Text = ToMaskEdit(cashbankglamt.Text, 4)
        End If
    End Sub 

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & Tchar(cashbankno.Text) & "'" : PrintReport(cashbankno.Text, cKoneksi.ambilscalar(sSql))
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal no As String, ByVal type As String)
        'untuk print
        If type <> "" Then
            If type = "BBK" Then
                report.Load(Server.MapPath(folderReport & "printMutasiBBK.rpt"))
            ElseIf type = "BKK" Then
                report.Load(Server.MapPath(folderReport & "printMutasiBKK.rpt"))
            ElseIf type = "BGK" Then
                report.Load(Server.MapPath(folderReport & "printMutasiBGK.rpt"))
            End If

            report.SetParameterValue("cmpcode", cmpcode)
            report.SetParameterValue("no", no)
            report.SetParameterValue("companyname", CompnyName)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no)
            report.Close() : report.Dispose() : Session("no") = Nothing
        End If

    End Sub

    Protected Sub gvmstcost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvmstcost.SelectedIndexChanged
        Response.Redirect("~\Accounting\mutation.aspx?cashbankoid=" & gvmstcost.SelectedDataKey("cashbankoid").ToString & "&cashbankstatus=" & gvmstcost.SelectedDataKey("cashbankstatus").ToString & "&branch_code=" & gvmstcost.SelectedDataKey("branch_code").ToString & "")
    End Sub

    Protected Sub imbPrintVoucher_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data sudah tersimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\mutation.aspx?awal=true")
        End If
    End Sub

    Protected Sub GvCoa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtCoa.Text = GvCoa.SelectedDataKey(1).ToString().Trim & " - " & GvCoa.SelectedDataKey(2S).ToString().Trim
        lblCoa.Text = GvCoa.SelectedDataKey(0).ToString().Trim
        cashbankacctgoid.SelectedValue = lblCoa.Text
        GvCoa.Visible = False
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtCoa.Text = ""
        GvCoa.Visible = False
    End Sub

    Protected Sub GvCoa_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvCoa.PageIndex = e.NewPageIndex
        BindAkun()
        GvCoa.Visible = True
    End Sub

    Protected Sub ImgPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PrintReport(gvmstcost.SelectedDataKey.Item("cashbankno").ToString, cKoneksi.ambilscalar(sSql))
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        Dim sMsg As String = ""
        If cashbankacctgoid.SelectedValue = "" Then
            sMsg &= "Account Cash/Bank Salah!!<BR>"
        End If

        If Not Session("dtlTable") Is Nothing Then
            Dim objTblDetail As DataTable
            Dim objRowDetail() As DataRow
            objTblDetail = Session("dtlTable")
            objRowDetail = objTblDetail.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowDetail.Length = 0 Then
                sMsg &= "Tidak ada detail mutasi !!<BR>"
            End If
        Else
            sMsg &= "Tidak ada detail mutasi !!<BR>"
        End If

        'If payrefno.Text.Trim = "" And payflag.SelectedValue = "BANK" Then
        '    sMsg &= "No cek(Payrefno) harus di isi !!<BR>"
        'End If

        'CEK PERIODE AKTIF BULANAN
        sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
        If GetPeriodAcctg(CDate(toDate(cashbankdate.Text))) < GetStrData(sSql) Then
            sMsg &= "Maaf, Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>"
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trncashbankmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf,Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT cashbankstatus FROM QL_trncashbankmst WHERE cashbankoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"

            Dim srest As String = GetStrData(sSql)
            If srest.ToLower = "post" Then
                sMsg &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            lblPOST.Text = ""
            Exit Sub
        End If

        Dim dRate As Double : dRate = InvoiceRate(currate.SelectedValue)
        Dim sCBType As String = "" : Dim sCBCode As String = ""
        Dim iCurID As Integer = 0

        If Session("oid") = Nothing Or Session("oid") = "" Then
            cashbankoid.Text = GenerateID("QL_trncashbankmst", cmpcode)
        Else
            cashbankoid.Text = Session("oid")
            cashbankno.Text = Session("oid")
        End If
        Session("vIDCost") = GenerateID("QL_cashbankgl", cmpcode)

        Dim CodeBranchNo As String = GetStrData("SELECT ISNULL(genother1,'') FROM QL_mstgen WHERE gengroup='CABANG' AND gencode='" & dd_branch.SelectedValue & "'")

        If payflag.SelectedValue = "CASH" Then
            sCBType = "BKK" : Else : sCBType = "BBK"
        End If

        If lblPOST.Text = "POST" Then
            '===Generated Cashbank No=== 
            Dim sNo As String = "BST/" & CodeBranchNo & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1 FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%' And branch_code='" & dd_branch.SelectedValue & "'"
            cashbankno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                '------------------------------
                '=======MUTATION (OUT)=========
                '------------------------------
                sSql = "INSERT into QL_trncashbankmst (cmpcode, cashbankoid, cashbankno, cashbankstatus, cashbanktype, cashbankgroup, cashbankacctgoid, cashbankdate, cashbanknote, upduser, updtime, cashbankcurroid, cashbankcurrate, PIC, PIC_REFNAME, createuser, createtime, branch_code, cashbankamount, cashbankamountidr) VALUES ('" & _
                    cmpcode & "', " & Integer.Parse(cashbankoid.Text) & ", '" & cashbankno.Text & "', '" & lblPOST.Text & "', '" & sCBType & "', 'MUTATION', " & cashbankacctgoid.SelectedValue & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Tchar(cashbanknote.Text) & " ~ " & cashbankno.Text & "', '" & Session("UserID") & "', current_timestamp, " & currate.SelectedValue & ", " & ToDouble(currencyrate.Text) & ", '', '" & Session("USERID") & "', '" & Session("UserID") & "', '" & CDate(toDate(createtime.Text)) & "', '" & dd_branch.SelectedValue & "', " & ToDouble(amtcost.Text) & ", " & ToDouble(amtcost.Text) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                If Not Session("dtlTable") Is Nothing Then
                    Dim objTable As DataTable : Dim objRow() As DataRow
                    objTable = Session("dtlTable")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                    Dim dateStr As String = "'" & toDate(cashbankdate.Text) & "'"
                    If payduedate.Visible Then
                        dateStr = "'" & toDate(payduedate.Text) & "'"
                    End If

                    For C1 As Int16 = 0 To objRow.Length - 1
                        sSql = "INSERT into QL_cashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglnote, upduser, updtime, duedate, refno, cashbankglstatus, branch_code, cashbankglamtidr, cashbankglamtusd) VALUES " & _
                        " ('" & cmpcode & "'," & Session("vIDCost") & "," & Integer.Parse(cashbankoid.Text) & "," & objRow(C1)("acctgoid") & "," & ToDouble(objRow(C1)("cashbankglamt")) & ",'" & Tchar(objRow(C1)("cashbankglnote")) & "','" & Session("UserID") & "',current_timestamp,(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))," & IIf(payrefno.Visible, "'" & Tchar(payrefno.Text) & "'", "''") & ",'" & lblPOST.Text & "','" & dd_branch.SelectedValue & "'," & ToDouble(objRow(C1)("cashbankglamt")) & "," & ToDouble(objRow(C1)("cashbankglamt")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Session("vIDCost") += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & Session("vIDCost") & " WHERE tablename='QL_cashbankgl' AND cmpcode='" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                sSql = "UPDATE QL_mstoid SET lastoid=(select MAX(cashbankoid) from QL_trncashbankmst) WHERE tablename='QL_trncashbankmst' AND cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else 'saat edit data
                Dim bUpdPayType As Boolean = False

                sSql = "UPDATE QL_trncashbankmst SET cashbankno='" & cashbankno.Text & "', cashbanktype='" & sCBType & "',cashbankacctgoid=" & cashbankacctgoid.SelectedValue & ",cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),cashbankstatus='" & lblPOST.Text & "',upduser = '" & Session("UserID") & "',updtime=current_timestamp,cashbanknote='" & Tchar(cashbanknote.Text) & "',PIC_REFNAME='" & Session("USERID") & "', cashbankcurroid=" & currate.SelectedValue & ", cashbankcurrate=" & ToDouble(currencyrate.Text) & " WHERE cmpcode = '" & cmpcode & "' and cashbankoid = " & Integer.Parse(cashbankoid.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("dtlTable") Is Nothing Then
                    Dim objTable As DataTable : Dim objRow() As DataRow
                    objTable = Session("dtlTable")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                    '=======Drop Create Cashbankgl(Detail)==========
                    sSql = "DELETE FROM QL_cashbankgl WHERE cashbankoid = " & Integer.Parse(cashbankoid.Text) & " AND branch_code = '" & dd_branch.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    For C1 As Int16 = 0 To objRow.Length - 1
                        sSql = "INSERT into QL_cashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglnote, upduser, updtime, duedate, refno, cashbankglstatus, branch_code, cashbankglamtidr, cashbankglamtusd) VALUES " & _
                        "('" & cmpcode & "', " & Session("vIDCost") & ", " & Integer.Parse(cashbankoid.Text) & ", " & objRow(C1)("acctgoid") & ", " & ToDouble(objRow(C1)("cashbankglamt")) & ", '" & Tchar(objRow(C1)("cashbankglnote")) & "', '" & Session("UserID") & "', current_timestamp, (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & IIf(payrefno.Visible, "'" & Tchar(payrefno.Text) & "'", "''") & ", '" & lblPOST.Text & "', '" & dd_branch.SelectedValue & "', " & ToDouble(objRow(C1)("cashbankglamt")) & ", " & ToDouble(objRow(C1)("cashbankglamt")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Session("vIDCost") += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & Session("vIDCost") & " WHERE tablename='QL_cashbankgl'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Session("vIDCost") += objRow.Length
                End If
            End If

            If lblPOST.Text = "POST" Then
                'update status cashbankmst
                sSql = "update ql_trncashbankmst set posttime=current_timestamp, cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), postuser='" & Session("userid") & "' Where cashbankoid=" & Integer.Parse(cashbankoid.Text) & " and cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim CodeBranch As String = ""
                '-- Generated ID table --
                sSql = "select top 1 ISNULL(lastoid,0) from QL_mstoid where tablename = 'ql_trncashbankmst' and cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : Dim vIDCashBank1 As Integer = xCmd.ExecuteScalar + 1
                sSql = "select top 1 ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trngldtl' and cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : Dim vIDDtl As Integer = xCmd.ExecuteScalar + 1

                sSql = "select top 1 ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnglmst' and cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : Dim vIDMst As Integer = xCmd.ExecuteScalar + 1

                Dim objTable As DataTable
                objTable = Session("dtlTable")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "branch_code='" & dd_branch.SelectedValue & "'"
                If Not Session("dtlTable") Is Nothing Then
                    For C1 As Int16 = 0 To dv.Count - 1
                        '-------------------------------
                        '========MUTATION (IN)==========
                        '-------------------------------
                        sSql = "SELECT ISNULL(genother1,'') FROM QL_mstgen WHERE gengroup='CABANG' AND gencode='" & dd_branch.SelectedValue & "'"
                        xCmd.CommandText = sSql : CodeBranch = xCmd.ExecuteScalar()

                        sSql = "SELECT interfacevalue FROM ql_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='VAR_CASH' AND interfaceres1='" & dd_branch.SelectedValue & "'"
                        xCmd.CommandText = sSql

                        Dim mCash As String = xCmd.ExecuteScalar
                        If Left(objTable.Rows(C1).Item("acctgcode"), mCash.Length) = mCash Then
                            sCBType = "BKM"
                        Else
                            sCBType = "BBM"
                        End If

                        Session("vNoCashBank2") = sCBType & "/" & CodeBranch & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                        sSql = "SELECT isnull(MAX(ABS(replace(cashbankno,'" & Session("vNoCashBank2") & "',''))),0) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & Session("vNoCashBank2") & "%'"

                        xCmd.CommandText = sSql
                        If Not IsDBNull(xCmd.ExecuteScalar) Then
                            iCurID = xCmd.ExecuteScalar + 1 : Else : iCurID = 1
                        End If
                        Session("vNoCashBank2") = GenNumberString(Session("vNoCashBank2"), "", iCurID, DefCounter)

                        sSql = "INSERT into QL_trncashbankmst (cmpcode, cashbankoid, cashbankno, cashbankstatus, cashbanktype, cashbankgroup, cashbankacctgoid, cashbankdate, cashbanknote, upduser, updtime, cashbankcurroid, cashbankcurrate, link_nobukti, bankoid, branch_code, cashbankamount, cashbankamountidr) VALUES " & _
                        " ('" & cmpcode & "', " & vIDCashBank1 & ", '" & Session("vNoCashBank2") & "', '" & lblPOST.Text & "', '" & sCBType & "', 'MUTATIONTO', " & dv(C1).Item("acctgoid") & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Tchar(dv(C1).Item("cashbankglnote")) & " ~ " & cashbankno.Text & "', '" & Session("UserID") & "', current_timestamp, " & currate.SelectedValue & "," & ToDouble(currencyrate.Text) & ", '" & cashbankno.Text & "', '" & Session("vIDCost") & "', '" & dv(C1).Item("branch_code") & "', " & ToDouble(dv(C1).Item("cashbankglamt")) & ", " & ToDouble(dv(C1).Item("cashbankglamt")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        Dim dateStr As String = "'" & toDate(cashbankdate.Text) & "'"
                        If payduedate.Visible Then
                            dateStr = "'" & toDate(payduedate.Text) & "'"
                        End If

                        sSql = "INSERT into QL_cashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglnote, upduser, updtime, duedate, refno, cashbankglstatus, cashbankglres1, branch_code, cashbankglamtidr, cashbankglamtusd) VALUES " & _
                        " ('" & cmpcode & "', " & Session("vIDCost") & ", " & vIDCashBank1 & ", " & cashbankacctgoid.Text & ", " & ToDouble(dv(C1).Item("cashbankglamt")) & ", '" & Tchar(cashbanknote.Text) & "', '" & Session("UserID") & "', current_timestamp, (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & IIf(payrefno.Visible, "'" & Tchar(payrefno.Text) & "'", "''") & ", '" & lblPOST.Text & "', " & Integer.Parse(cashbankoid.Text) & ", '" & dv(C1).Item("branch_code") & "', " & ToDouble(dv(C1).Item("cashbankglamt")) & ", " & ToDouble(dv(C1).Item("cashbankglamt")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        vIDCashBank1 += 1 : Session("vIDCost") += 1
                    Next
                    dv.RowFilter = ""

                    dv.RowFilter = "branch_code<>'" & dd_branch.SelectedValue & "'"
                    For C1 As Integer = 0 To dv.Count - 1
                        '-----------------------------------
                        '---- Jurnal Untuk Cabang Masuk----
                        '-----------------------------------
                        sSql = "SELECT ISNULL(genother1,'') FROM QL_mstgen WHERE gengroup='CABANG' AND gencode='" & dv(C1).Item("branch_code") & "'"
                        xCmd.CommandText = sSql : CodeBranch = xCmd.ExecuteScalar()

                        sSql = "SELECT interfacevalue FROM ql_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='VAR_CASH' AND interfaceres1='" & dv(C1).Item("branch_code") & "'"
                        xCmd.CommandText = sSql
                        Dim pCash As String = xCmd.ExecuteScalar
                        If Left(dv(C1).Item("acctgcode"), pCash.Length) = pCash Then
                            sCBType = "BKM"
                        Else
                            sCBType = "BBM"
                        End If

                        Session("vNoCashBank3") = sCBType & "/" & CodeBranch & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                        sSql = "SELECT isnull(MAX(ABS(replace(cashbankno,'" & Session("vNoCashBank3") & "',''))),0) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & Session("vNoCashBank3") & "%'"

                        xCmd.CommandText = sSql
                        If Not IsDBNull(xCmd.ExecuteScalar) Then
                            iCurID = xCmd.ExecuteScalar + 1 : Else : iCurID = 1
                        End If
                        Session("vNoCashBank3") = GenNumberString(Session("vNoCashBank3"), "", iCurID, DefCounter)

                        sSql = "INSERT into QL_trncashbankmst (cmpcode, cashbankoid, cashbankno, cashbankstatus, cashbanktype, cashbankgroup, cashbankacctgoid, cashbankdate, cashbanknote, upduser, updtime, cashbankcurroid, cashbankcurrate, link_nobukti, bankoid, branch_code, cashbankamount, cashbankamountidr) VALUES " & _
                       " ('" & cmpcode & "', " & vIDCashBank1 & ", '" & Session("vNoCashBank3") & "', '" & lblPOST.Text & "', '" & sCBType & "', 'MUTATIONTO', " & dv(C1).Item("acctgoid") & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Tchar(dv(C1).Item("cashbankglnote")) & " ~ " & cashbankno.Text & "', '" & Session("UserID") & "', current_timestamp, " & currate.SelectedValue & "," & ToDouble(currencyrate.Text) & ", '" & cashbankno.Text & "', '" & Session("vIDCost") & "', '" & dv(C1).Item("branch_code") & "', " & ToDouble(dv(C1).Item("cashbankglamt")) & ", " & ToDouble(dv(C1).Item("cashbankglamt")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        Dim dateStr As String = "'" & toDate(cashbankdate.Text) & "'"
                        If payduedate.Visible Then
                            dateStr = "'" & toDate(payduedate.Text) & "'"
                        End If

                        sSql = "INSERT into QL_cashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglnote, upduser, updtime, duedate, refno, cashbankglstatus, cashbankglres1, branch_code, cashbankglamtidr, cashbankglamtusd) VALUES " & _
                        " ('" & cmpcode & "', " & Session("vIDCost") & ", " & vIDCashBank1 & ", " & cashbankacctgoid.Text & ", " & ToDouble(dv(C1).Item("cashbankglamt")) & ", '" & Tchar(cashbanknote.Text) & "', '" & Session("UserID") & "', current_timestamp, (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & IIf(payrefno.Visible, "'" & Tchar(payrefno.Text) & "'", "''") & ", '" & lblPOST.Text & "', " & Integer.Parse(cashbankoid.Text) & ", '" & dv(C1).Item("branch_code") & "', " & ToDouble(dv(C1).Item("cashbankglamt")) & ", " & ToDouble(dv(C1).Item("cashbankglamt")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "INSERT into QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, type, branch_code) VALUES " & _
                        " ('" & cmpcode & "'," & vIDMst & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetDateToPeriodAcctg(GetServerTime()) & "', '" & tempPayFlag & " - Mutation|No=" & Session("vNoCashBank3") & "', 'POST', current_timestamp, '" & Session("UserID") & "', current_timestamp, 'MUTATION', '" & dv(C1).Item("branch_code") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        '=== JURNAL CREDIT ===
                        sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, upduser, updtime, GLOTHER2, branch_code, glamtidr, glamtusd, glother1, glflag, glpostdate) VALUES " & _
                        " ('" & cmpcode & "', " & vIDDtl & ", 1, " & vIDMst & ", " & cashbankacctgoid.SelectedValue & ", 'C', " & ToDouble(dv(C1).Item("cashbankglamt")) & ", '" & Session("vNoCashBank3") & "', '" & tempPayFlag & " - Mutation|No=" & Session("vNoCashBank3") & " | Note. " & Tchar(cashbanknote.Text) & "', '" & Session("UserID") & "',current_timestamp, '" & vIDCashBank1 & "', '" & dv(C1).Item("branch_code") & "', " & ToDouble(dv(C1).Item("cashbankglamt")) & ", " & ToDouble(dv(C1).Item("cashbankglamt")) & ", 'ql_trncashbankmst', 'POST', current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        vIDDtl += 1

                        '=== JURNAL DEBET ===
                        sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, upduser, updtime, GLOTHER2, branch_code, glamtidr, glamtusd, glother1, glflag, glpostdate) " & _
                        "VALUES ('" & cmpcode & "', " & vIDDtl & ", " & 2 + C1 & ", " & vIDMst & ", " & dv(C1).Item("acctgoid") & ", 'D', " & ToDouble(dv(C1).Item("cashbankglamt")) & ", '" & Session("vNoCashBank3") & "', '" & tempPayFlag & " - Mutation|No=" & Session("vNoCashBank3") & " | " & Tchar(dv(C1).Item("cashbankglnote")) & "', '" & Session("UserID") & "', current_timestamp, '" & vIDCashBank1 & "', '" & dv(C1).Item("branch_code") & "'," & ToDouble(dv(C1).Item("cashbankglamt")) & ", " & ToDouble(dv(C1).Item("cashbankglamt")) & ", 'ql_trncashbankmst', 'POST', current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        vIDDtl += 1 : vIDMst += 1 : vIDCashBank1 += 1 : Session("vIDCost") += 1
                        '--------------------------------
                    Next

                    '----------------------------------
                    '--- Jurnal Untuk Cabang Keluar ---
                    '---------------------------------- 
                    sSql = "INSERT into QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag ,postdate, upduser, updtime, type, branch_code) VALUES " & _
                    "('" & cmpcode & "', " & vIDMst & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetDateToPeriodAcctg(GetServerTime()) & "', '" & tempPayFlag & " - Mutation|No=" & cashbankno.Text & "', 'POST', current_timestamp,'" & Session("UserID") & "', current_timestamp, 'MUTATION', '" & dd_branch.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, upduser, updtime, GLOTHER2, branch_code, glamtidr, glamtusd, glother1, glflag, glpostdate) VALUES " & _
                    "('" & cmpcode & "', " & vIDDtl & ", 1, " & vIDMst & ", " & cashbankacctgoid.SelectedValue & ", 'C', " & ToDouble(amtcost.Text) & ", '" & cashbankno.Text & "', '" & tempPayFlag & " - Mutation|No=" & cashbankno.Text & "', '" & Session("UserID") & "', current_timestamp, '" & Integer.Parse(cashbankoid.Text) & "', '" & dd_branch.SelectedValue & "', " & ToDouble(amtcost.Text) & ", " & ToDouble(amtcost.Text) & ", 'ql_trncashbankmst', 'POST', current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'vIDDtl += 1

                    For C2 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, upduser, updtime, GLOTHER2, branch_code, glamtidr, glamtusd, glother1, glflag, glpostdate) " & _
                           "VALUES ('" & cmpcode & "', " & vIDDtl + 1 & ", " & 2 + C2 & ", " & vIDMst & ", " & objTable.Rows(C2).Item("acctgoid") & ", 'D', " & ToDouble(objTable.Rows(C2).Item("cashbankglamt")) & ", '" & cashbankno.Text & "', '" & tempPayFlag & " - Mutation|No=" & cashbankno.Text & "', '" & Session("UserID") & "', current_timestamp, '" & Integer.Parse(cashbankoid.Text) & "', '" & dd_branch.SelectedValue & "', " & ToDouble(objTable.Rows(C2).Item("cashbankglamt")) & ", " & ToDouble(objTable.Rows(C2).Item("cashbankglamt")) & ", 'ql_trncashbankmst', 'POST', current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        vIDDtl += 1
                    Next
                End If

                sSql = "update g set trnid = (select trnid from QL_trncashbankmst where cashbankoid = g.cashbankoid and branch_code = g.branch_code) from QL_cashbankgl g inner join QL_trncashbankmst cb on cb.branch_code=g.branch_code AND cb.cashbankoid=g.cashbankoid AND g.trnid=0"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & vIDMst & " WHERE tablename='QL_trnglmst'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & vIDDtl + 1 + objTable.Rows.Count - 1 & " WHERE tablename='QL_trngldtl'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=(select MAX(cashbankoid) from QL_trncashbankmst) WHERE tablename='QL_trncashbankmst' AND cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("vIDCost") & " WHERE tablename='QL_cashbankgl'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & "</br >" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\mutation.aspx?awal=true")
    End Sub

    Protected Sub btnPosting2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting2.Click
        lblPOST.Text = "POST" : btnsave_Click(sender, e)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim strSQL As String
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn : objCmd.Transaction = objTrans
        Try
            strSQL = "DELETE FROM QL_cashbankgl WHERE cashbankoid = " & Trim(cashbankoid.Text) & " AND branch_code = '" & dd_branch.SelectedValue & "'"
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()
            strSQL = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' and cashbankoid = " & Trim(cashbankoid.Text) & " AND branch_code = '" & dd_branch.SelectedValue & "'"
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()
            objTrans.Commit() : objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
        Session("oid") = Nothing : Session("dtlTable") = Nothing
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("oid") = Nothing : Session("dtlTable") = Nothing
        Response.Redirect("mutation.aspx?type=" & Session("CashBankType") & "&awal=true")
    End Sub

    Protected Sub btnCariCoa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCariCoa.Click
        BindAkun()
    End Sub

    Protected Sub BtnCari_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCari.Click
        bindAccounting("")
    End Sub
#End Region
End Class
