<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnFixAsset.aspx.vb" Inherits="Accounting_trnFixAsset" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Fixed Asset"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" 
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView2" runat="server" ActiveViewIndex="0"><asp:View id="View3" runat="server"><asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><asp:Label id="lblPosInformation" runat="server" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label>&nbsp;<asp:Label id="Label36" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="|"></asp:Label><asp:LinkButton id="lbkPostMoreInfo" onclick="lbkPostMoreInfo_Click" runat="server" CssClass="submenu">Monthly Posting</asp:LinkButton>&nbsp;<BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 115px" align=left><asp:CheckBox id="cbPeriode" runat="server" Width="80px" Text="Periode"></asp:CheckBox></TD><TD align=left colSpan=4><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label160" runat="server" Text="to"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:CheckBox id="cbDesc" runat="server" Text="Asset Desc" Visible="False"></asp:CheckBox></TD></TR><TR><TD style="WIDTH: 115px" align=left><asp:Label id="Label4" runat="server" Width="40px" Text="Filter"></asp:Label></TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="f.fixcode">Code</asp:ListItem>
<asp:ListItem Value="g.gendesc">Group</asp:ListItem>
<asp:ListItem Value="f.fixdesc">Description</asp:ListItem>
<asp:ListItem Value="f.fixother">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 115px" align=left><asp:CheckBox id="cbBlmPosting" runat="server" Text="Not Posted Yet"></asp:CheckBox></TD><TD align=left colSpan=4><asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 115px" align=left><ajaxToolkit:CalendarExtender id="ce2" runat="server" TargetControlID="txtPeriode1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce3" runat="server" TargetControlID="txtPeriode2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" TargetControlID="txtPeriode1" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" TargetControlID="txtPeriode2" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="GVFixedAsset" runat="server" Width="100%" ForeColor="#333333" OnSelectedIndexChanged="GVFixedAsset_SelectedIndexChanged" PageSize="8" OnRowDataBound="GVFixedAsset_RowDataBound" OnPageIndexChanging="GVFixedAsset_PageIndexChanging" GridLines="None" DataKeyNames="cmpcode,fixoid" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="fixoid" HeaderText="fixoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FixCode" HeaderText="FixCode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Group">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdate" HeaderText="Fixed Date" SortExpression="fixdate">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixfirstvalue" HeaderText="First Value">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixpresentvalue" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixother" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixflag" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" CssClass="Important" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel></asp:View>&nbsp; <asp:View id="View4" runat="server"><asp:UpdatePanel id="UpdatePanelPostingDtl" runat="server"><ContentTemplate>
<asp:LinkButton id="lbkPostInfo" onclick="lbkPostInfo_Click" runat="server" CssClass="submenu">Information</asp:LinkButton> <asp:Label id="Label37" runat="server" Font-Size="Small" ForeColor="Black" Text="|"></asp:Label> <asp:Label id="Label26" runat="server" Font-Bold="True" ForeColor="Black" Text="Monthly Posting"></asp:Label><BR /><TABLE width="100%"><TBODY><TR><TD align=left colSpan=6><asp:Label id="Label24" runat="server" Font-Bold="False" Text="Bulan"></asp:Label>&nbsp;<asp:DropDownList id="ddlMonth" runat="server" CssClass="inpText"><asp:ListItem Text="January" Value="01"></asp:ListItem>
<asp:ListItem Text="February" Value="02"></asp:ListItem>
<asp:ListItem Text="March" Value="03"></asp:ListItem>
<asp:ListItem Text="April" Value="04"></asp:ListItem>
<asp:ListItem Text="May" Value="05"></asp:ListItem>
<asp:ListItem Text="June" Value="06"></asp:ListItem>
<asp:ListItem Text="July" Value="07"></asp:ListItem>
<asp:ListItem Text="August" Value="08"></asp:ListItem>
<asp:ListItem Text="September" Value="09"></asp:ListItem>
<asp:ListItem Text="October" Value="10"></asp:ListItem>
<asp:ListItem Text="November" Value="11"></asp:ListItem>
<asp:ListItem Text="December" Value="12"></asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Label id="Label25" runat="server" Font-Bold="False" Text="Tahun"></asp:Label>&nbsp;<asp:DropDownList id="ddlYear" runat="server" CssClass="inpText"></asp:DropDownList>&nbsp;<asp:ImageButton id="btnViewDtl" onclick="btnViewDtl_Click" runat="server" ImageUrl="~/Images/find.png"></asp:ImageButton> <asp:ImageButton id="btnCancelDtl" onclick="btnCancelDtl_Click" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPostingDtl" onclick="btnPostingDtl_Click" runat="server" ImageUrl="~/Images/posting.png"></asp:ImageButton> <asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="GVDtlMonth" runat="server" Width="100%" ForeColor="#333333" PageSize="8" OnRowDataBound="GVDtlMonth_RowDataBound" OnPageIndexChanging="GVDtlMonth_PageIndexChanging" GridLines="None" DataKeyNames="cmpcode,fixdtloid,fixoid,fixperiodvalue,fixcode,fixperiod,fixpostdate" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="fixdtloid" HeaderText="ID" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cmpcode" HeaderText="Branch" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperiod" HeaderText="Periode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepvalue" HeaderText="Dep Value">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperiodvalue" HeaderText="Book Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdtloid" HeaderText="fixdtloid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE>
</ContentTemplate>
</asp:UpdatePanel></asp:View></asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt"><asp:Image ID="Image33" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                List of Fixed Asset :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="WIDTH: 723px; HEIGHT: 100%" vAlign=top colSpan=3><asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><DIV style="HEIGHT: 100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 145px" id="TD8" align=left runat="server" Visible="true"><asp:Label id="Outlet" runat="server" Text="Cabang"></asp:Label></TD><TD style="WIDTH: 318px" id="TD7" align=left runat="server" Visible="true"><asp:DropDownList id="DDLoutlet" runat="server" Width="170px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged"></asp:DropDownList></TD><TD id="TD6" align=left runat="server" Visible="false"><asp:Label id="Label19" runat="server" Width="104px" Text="Daily Rate to IDR" Visible="False"></asp:Label></TD><TD style="WIDTH: 449px" id="TD1" align=left runat="server" Visible="false"><asp:TextBox id="RateToIDR" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" Enabled="False">0</asp:TextBox></TD><TD id="TD2" align=left runat="server" Visible="false"><asp:Label id="Label41" runat="server" Text="Daily Rate To USD" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px" id="TD3" align=left runat="server" Visible="false"><asp:TextBox id="RateToUSD" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" Enabled="False">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 145px" align=left><asp:Label id="Label27" runat="server" Text="Asset Code"></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="FixCode" runat="server" Width="150px" CssClass="inpText" MaxLength="20"></asp:TextBox> <asp:Label id="Label18" runat="server" ForeColor="Red" Text="*"></asp:Label>&nbsp;<asp:Label id="FAPmstoid" runat="server" Text="0" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="lblFaMst" runat="server" Width="104px" Text="Akun FA Purchase"></asp:Label></TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="FAPMst" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnCari1" onclick="BtnCari1_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnHapus1" onclick="btnHapus1_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD><TD id="TD5" align=left runat="server" visible="true"><asp:Label id="lblFaDtl" runat="server" Width="112px" Text="FA Purchase Detail"></asp:Label></TD><TD style="WIDTH: 332px" id="TD4" align=left runat="server" visible="true"><asp:TextBox id="FAPDtl" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCari2" onclick="btnCari2_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnHapus2" onclick="btnHapus2_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 145px" align=left><asp:Label id="Label5" runat="server" Text="Asset Date"></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="fixDate" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label14" runat="server" CssClass="Important" Text="dd/MM/yyyy"></asp:Label></TD><TD align=left><asp:Label id="Label6" runat="server" Width="104px" Text="Description Asset"></asp:Label></TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="fixdesc" runat="server" Width="150px" CssClass="inpText" MaxLength="200"></asp:TextBox> <asp:Label id="Label15" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD align=left><asp:Label id="Label28" runat="server" Text="Type asset"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:DropDownList id="fixgroup" runat="server" Width="170px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="fixgroup_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 145px" id="TD14" align=left runat="server" Visible="false"><asp:Label id="Label13" runat="server" Text="Currency" Visible="False"></asp:Label></TD><TD style="WIDTH: 318px" id="TD11" align=left runat="server" Visible="false"><asp:DropDownList id="CurrDDL" runat="server" CssClass="inpTextDisabled" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="CurrDDL_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD><TD id="TD9" align=left runat="server" Visible="false"><asp:Label id="Label20" runat="server" Width="121px" Text="Monthly Rate to IDR" Visible="False"></asp:Label></TD><TD style="WIDTH: 449px" id="TD13" align=left runat="server" Visible="false"><asp:TextBox id="Rate2ToIDR" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" Enabled="False">0</asp:TextBox></TD><TD id="TD10" align=left runat="server" Visible="false"><asp:Label id="Label21" runat="server" Width="125px" Text="Monthly Rate To USD" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px" id="TD12" align=left runat="server" Visible="false"><asp:TextBox id="Rate2ToUsd" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" Enabled="False">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 145px" align=left><asp:Label id="Label240" runat="server" ForeColor="DarkBlue" Text="Accum.  Value"></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="accumDV" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True" OnTextChanged="accumDV_TextChanged">0</asp:TextBox></TD><TD align=left><asp:Label id="Label7" runat="server" Text="Harga Perolehan"></asp:Label></TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="fixfirstvalue" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" MaxLength="16"></asp:TextBox> <asp:Label id="Label2x" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD align=left><asp:Label id="Label29" runat="server" Text="Nilai Buku"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixPresentValue" runat="server" Width="150px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False" MaxLength="16"></asp:TextBox> <asp:Label id="Label40" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD></TR><TR><TD style="WIDTH: 145px" align=left><asp:Label id="Label1" runat="server" Text="Note."></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="FAnote" runat="server" Width="180px" CssClass="inpText" MaxLength="50"></asp:TextBox> <asp:Label id="Label16" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD align=left><asp:Label id="Label8" runat="server" Text="Depreciation"></asp:Label></TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="fixdepmonth" runat="server" Width="50px" CssClass="inpText" AutoPostBack="True" MaxLength="2">-1</asp:TextBox> <asp:Label id="Label9" runat="server" Font-Size="X-Small" ForeColor="Red" Text="(month)"></asp:Label> <asp:Label id="Label33" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD align=left><asp:Label id="Label10" runat="server" Text="Dep. Value"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixdepval" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 145px" class="Label" align=left><asp:Label id="fixmstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="lblCekPage" runat="server" Visible="False"></asp:Label> <asp:Label id="Rate2Oid" runat="server" Visible="False"></asp:Label> <asp:Label id="RateOid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 318px" align=left>&nbsp;<asp:Label id="FAPcmpcode" runat="server" Visible="False"></asp:Label> <asp:Label id="AssetType" runat="server" Visible="False"></asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" ValidChars="1234567890,." TargetControlID="fixfirstvalue"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" ValidChars="1234567890,." TargetControlID="fixLastAsset"></ajaxToolkit:FilteredTextBoxExtender> </TD><TD align=left colSpan=2>&nbsp;<asp:Label id="Label12" runat="server" Width="276px" Font-Bold="True" ForeColor="Red" Text="* Set Depreciation (month)  -1, If have no Depreciation !!"></asp:Label></TD><TD align=left><asp:Label id="Label42" runat="server" Width="115px" ForeColor="DarkBlue" Text="Aset Value Terakhir"></asp:Label></TD><TD style="WIDTH: 332px" align=left>&nbsp;<asp:TextBox id="fixLastAsset" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" OnTextChanged="fixLastAsset_TextChanged">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 145px; HEIGHT: 23px" align=left><asp:Label id="Label17" runat="server" Text="Akun Asset"></asp:Label></TD><TD style="HEIGHT: 23px" align=left colSpan=3><asp:DropDownList id="DDLassets" runat="server" Width="320px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD style="HEIGHT: 23px" align=left><asp:Label id="lblPOST" runat="server" Text="lblPOST" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px; HEIGHT: 23px" align=left><asp:Label id="CutofDate" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 145px" align=left><asp:Label id="Label23" runat="server" Text="Accum. Dep"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLaccum" runat="server" Width="320px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD align=left><asp:Label id="payacctgoid" runat="server" Text="payacctgoid" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="AccumVal" runat="server" Width="150px" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 145px" align=left><asp:Label id="Label22" runat="server" Text="Accum.  Expense"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLadExpense" runat="server" Width="320px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD align=left><asp:Label id="lblNo" runat="server" Text="Draft No." Visible="False"></asp:Label> <asp:Label id="Label30" runat="server" Font-Size="Small" Font-Bold="False" ForeColor="#585858" Text="|" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixlocation" runat="server" Width="150px" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 145px" align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" TargetControlID="fixDate" PopupButtonID="btnDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD><TD align=left colSpan=3><asp:ImageButton id="btnGenerate" onclick="btnGenerate_Click1" runat="server" ImageUrl="~/Images/generate.png"></asp:ImageButton></TD><TD align=left><asp:Label id="Label3" runat="server" Font-Bold="True" ForeColor="Black" Text="Informasi" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixperson" runat="server" Width="150px" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="GVFixedAssetdtl" runat="server" Width="100%" ForeColor="#333333" OnSelectedIndexChanged="GVFixedAssetdtl_SelectedIndexChanged" PageSize="8" OnRowDataBound="GVFixedAssetdtl_RowDataBound" OnPageIndexChanging="GVFixedAssetdtl_PageIndexChanging" GridLines="None" DataKeyNames="fixperiod" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField>
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="fixperiod" HeaderText="Periode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepvalue" DataFormatString="{0:#,##0.00}" HeaderText="Depreciation">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperiodvalue" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepaccum" DataFormatString="{0:#,##0.00}" HeaderText="Accumulation">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixflag" HeaderText="Status">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="lblEmpty" runat="server" CssClass="Important" 
        Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 16px" align=left colSpan=4>Created By&nbsp;<asp:Label id="lblUser" runat="server" Font-Bold="True">User</asp:Label>&nbsp; On <asp:Label id="lblTime" runat="server" Font-Bold="True">Time</asp:Label>,&nbsp; Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label>&nbsp; By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label></TD><TD style="HEIGHT: 16px" align=left><asp:LinkButton id="lbkDetil" runat="server" CssClass="submenu" Visible="False">More Informasi </asp:LinkButton></TD><TD style="WIDTH: 332px; HEIGHT: 16px" align=left><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="fixDate" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" ErrorTooltipEnabled="True" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click1" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" onclick="btnDelete_Click1" runat="server" ImageUrl="~/Images/Delete.png"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click1" runat="server" ImageUrl="~/Images/posting.png"></asp:ImageButton></TD><TD align=left></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixother" runat="server" Width="150px" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left colSpan=4><asp:UpdateProgress id="UpdateProgress5" runat="server"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD><TD align=left></TD><TD style="WIDTH: 332px" align=left></TD></TR></TBODY></TABLE></DIV></asp:View> </asp:MultiView> </TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListSupp" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Fixed Asset Purchase"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp"><asp:Label id="Label11" runat="server" Text="Filter : "></asp:Label>&nbsp;<asp:DropDownList id="FilterFAPDDL" runat="server" CssClass="inpText">
                        <asp:ListItem Value="BeliNo">No FA Purchase</asp:ListItem>
<asp:ListItem>suppname</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterFAP" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="GvFAPmst" runat="server" Width="98%" ForeColor="#333333" OnSelectedIndexChanged="GvFAPmst_SelectedIndexChanged" PageSize="5" OnPageIndexChanging="GvFAPmst_PageIndexChanging" GridLines="None" DataKeyNames="cmpcode,Mstoid,BeliNo,suppname,PayType,branch_code" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cmpcode" HeaderText="Cmpcode" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="White"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Mstoid" HeaderText="FAOid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="White"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="BeliNo" HeaderText="FA Purchase No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PayType" HeaderText="Type" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupDragHandleControlID="lblListSupp" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListSupp"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="UpdatePanel3" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListReg" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListReg" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Fixed Asset Purchase Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListReg" runat="server" Width="100%" DefaultButton="btnFindListReg">Filter :&nbsp;<asp:DropDownList id="FilterDDLListReg" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="ItemCode">Item Code</asp:ListItem>
<asp:ListItem Value="ItemDesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListReg" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListReg" onclick="btnFindListReg_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListReg" onclick="btnAllListReg_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="GvFAPdtl" runat="server" Width="98%" ForeColor="#333333" OnSelectedIndexChanged="GvFAPdtl_SelectedIndexChanged" PageSize="5" OnPageIndexChanging="GvFAPdtl_PageIndexChanging" GridLines="None" DataKeyNames="cmpcode,DtlOid,MstOid,BeliNo,ItemCode,ItemDesc,DtlAmtNetto" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cmpcode" HeaderText="CmpCode" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="White"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="DtlOid" HeaderText="FAdtlOid" SortExpression="pono" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemCode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemDesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="DtlAmtNetto" HeaderText="Sub Total">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListReg" onclick="lkbCloseListReg_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListReg" runat="server" TargetControlID="btnHideListReg" PopupDragHandleControlID="lblListReg" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListReg">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListReg" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Fixed Asset :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
             
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>