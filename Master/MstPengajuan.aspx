<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="MstPengajuan.aspx.vb" Inherits="MstPengajuan" title="Form Pengajuan Cabang" %>


<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text=".: Customer Approval" CssClass="Title" Font-Size="X-Large"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: List of Customer Approval</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w21" DefaultButton=""><TABLE width="100%"><TBODY><TR><TD align=left>Cabang</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDLCabang" runat="server" CssClass="inpText" __designer:wfdid="w22"></asp:DropDownList></TD></TR><TR><TD align=left><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w23"><asp:ListItem Value="custapprovalno">No. Pengajuan</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="custcode">Code Cust</asp:ListItem>
</asp:DropDownList></TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="FilterNya" runat="server" Width="186px" CssClass="inpText" __designer:wfdid="w24"></asp:TextBox></TD></TR><TR><TD align=left>Periode </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w25"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton>&nbsp;-&nbsp;<asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w27"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton>&nbsp;</TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="StatusNya" runat="server" Width="119px" CssClass="inpText" __designer:wfdid="w29"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>IN APPROVAL</asp:ListItem>
<asp:ListItem>APPROVED</asp:ListItem>
<asp:ListItem>REJECTED</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w30" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w31" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="gvMst" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w14" GridLines="None" EmptyDataText="No data in database." AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="8" AllowSorting="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="custapprovaloid" DataNavigateUrlFormatString="MstPengajuan.aspx?oid={0}" DataTextField="custapprovaloid" HeaderText="No. Draft">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="custapprovalno" HeaderText="No. Pengajuan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custapprovaldate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcode" HeaderText="Cust. Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custapprovaltype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custapprovalnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custapprovalstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="flagstatus" HeaderText="Status Flag" Visible="False">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Status Flag" Visible="False"><ItemTemplate>
<asp:Button id="BtnFlagStatus" onclick="BtnFlagStatus_Click" runat="server" CssClass="red" Font-Bold="True" ForeColor="White" Text='<%# Eval("flagstatus") %>' __designer:wfdid="w1" ToolTip='<%# eval("custapprovaloid") %>' CommandName='<%# Eval("custapprovalstatus") %>'></asp:Button> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data not found !!" __designer:wfdid="w4"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w33" TargetControlID="dateAwal" PopupButtonID="imageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w34" TargetControlID="dateAkhir" PopupButtonID="imageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w35" Mask="99/99/9999" MaskType="Date" TargetControlID="dateAwal"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w36" Mask="99/99/9999" MaskType="Date" TargetControlID="dateAkhir"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 527px"><TBODY><TR><TD id="TD2" align=left runat="server" Visible="true">Cabang</TD><TD id="TD4" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="cabangDDL" runat="server" CssClass="inpText" Font-Size="8pt" OnSelectedIndexChanged="cabangDDL_SelectedIndexChanged" AutoPostBack="True" __designer:wfdid="w101"></asp:DropDownList> <asp:Label id="custapprovaloid" runat="server" Visible="False" __designer:wfdid="w102"></asp:Label></TD><TD id="Td5" align=left colSpan=1 runat="server" Visible="true"><asp:Label id="Label9" runat="server" Width="90px" __designer:wfdid="w103">No. Pengajuan</asp:Label></TD><TD align=left colSpan=2 runat="server" Visible="true"><asp:TextBox id="custapprovalno" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w104" MaxLength="20" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left>TYpe</TD><TD align=left colSpan=3><asp:DropDownList id="custapprovaltype" runat="server" CssClass="inpText" Font-Size="8pt" AutoPostBack="True" __designer:wfdid="w105"><asp:ListItem Value="CL AWAL">CL AWAL</asp:ListItem>
<asp:ListItem Value="CL TEMPORARY">CL TEMPORARY</asp:ListItem>
<asp:ListItem>TERMIN</asp:ListItem>
<asp:ListItem Value="OVERDUE">OVER DUE</asp:ListItem>
<asp:ListItem Value="REVISI DATA">REVISI DATA</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=1>Cust. Code</TD><TD align=left colSpan=2><asp:TextBox id="CustCode" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w106" MaxLength="20" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left>Customer<asp:Label id="Label7" runat="server" CssClass="Important" Text="*" __designer:wfdid="w107"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="custname" runat="server" Width="185px" CssClass="inpText" __designer:wfdid="w108" MaxLength="50"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchCust" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w109"></asp:ImageButton> <asp:ImageButton id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w110"></asp:ImageButton> <asp:Label id="custoid" runat="server" Visible="False" __designer:wfdid="w111">0</asp:Label></TD><TD align=left colSpan=1>Tanggal</TD><TD align=left colSpan=2><asp:TextBox id="custapprovaldate" runat="server" Width="60px" CssClass="inpTextDisabled" __designer:wfdid="w112" Enabled="False" ReadOnly="True"></asp:TextBox> <asp:Label id="Label1" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w113"></asp:Label></TD></TR><TR><TD align=left></TD><TD align=left colSpan=3><asp:GridView id="gvCust" runat="server" Width="550px" ForeColor="#333333" Visible="False" OnSelectedIndexChanged="gvCust_SelectedIndexChanged" __designer:wfdid="w114" DataKeyNames="custoid,custcode,custname,TerminOid,Termin,alamat,custcityoid,Kota,custprovoid,Provinsi,TelpRmh,TlpKantor,TelpWa,CrLimit,Catatan,tglGabung,custflag,CrUsage,CrOverDue,CrAwal,CrTempo,Total" GridLines="None" EmptyDataText="No data found" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="False" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcode" HeaderText="Cust. Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Cust. Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tglGabung" HeaderText="Tgl Gabung">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="alamat" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="Label13" runat="server" Width="90px" __designer:wfdid="w115">Termin Old</asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="TerminOld" runat="server" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w116" Enabled="False"></asp:DropDownList>&nbsp;Hari</TD><TD align=left colSpan=1>Tgl. Gabung</TD><TD align=left colSpan=2><asp:TextBox id="TglGabung" runat="server" Width="60px" CssClass="inpTextDisabled" __designer:wfdid="w117" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label25" runat="server" Width="90px" __designer:wfdid="w118">Addres Old</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="addrold" runat="server" Width="250px" CssClass="inpTextDisabled" __designer:wfdid="w119" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD align=left colSpan=1><asp:Label id="Label34" runat="server" Width="90px" __designer:wfdid="w120">City Old</asp:Label> <asp:Label id="OidCityOld" runat="server" Visible="False" __designer:wfdid="w121">0</asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="cityold" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w122" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label4" runat="server" Width="122px" __designer:wfdid="w123">Credit limit Awal Old</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="CLAwalOld" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w124" Enabled="False" ReadOnly="True">0.00</asp:TextBox></TD><TD align=left colSpan=1><asp:Label id="CLTempOldTxt" runat="server" Width="127px" __designer:wfdid="w125">Credit limit Temp Old</asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="CLTempOld" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w126" Enabled="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label26" runat="server" Width="110px" __designer:wfdid="w127">Credit limit Usage</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="clusage" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w128" MaxLength="20" Enabled="False" ReadOnly="True">0.00</asp:TextBox></TD><TD align=left colSpan=1><asp:Label id="Label15" runat="server" Width="110px" __designer:wfdid="w129">Credit limit Sisa</asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="clsisa" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w130" MaxLength="20" Enabled="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label35" runat="server" Width="90px" __designer:wfdid="w131">Provinsi Old</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="ProvOld" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w132" Enabled="False" ReadOnly="True"></asp:TextBox> <asp:Label id="OidProvOld" runat="server" Visible="False" __designer:wfdid="w133">0</asp:Label></TD><TD align=left colSpan=1><asp:Label id="Label36" runat="server" Width="106px" __designer:wfdid="w134">Telp. Rumah Old</asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="telprmhold" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w135" Enabled="False" ReadOnly="True" EnableTheming="True"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label37" runat="server" Width="108px" __designer:wfdid="w136">Telp. Kantor Old</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="telpktrold" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w137" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD align=left colSpan=1><asp:Label id="Label38" runat="server" Width="90px" __designer:wfdid="w138">Telp. WA Old</asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="telpwaold" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w139" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label8" runat="server" Width="108px" __designer:wfdid="w140">Status Cust Old</asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="custflagOld" runat="server" Width="103px" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w141" Enabled="False"><asp:ListItem Value="active">Active</asp:ListItem>
<asp:ListItem Value="inactive">Inactive</asp:ListItem>
<asp:ListItem Value="suspended">Suspended</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=1><asp:Label id="Label11" runat="server" Width="90px" __designer:wfdid="w142">Average Late A/R</asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="averagelate" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w143" Enabled="False" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Lbladdrnew" runat="server" Width="90px" Visible="False" __designer:wfdid="w144">Addres New</asp:Label> </TD><TD align=left colSpan=3><asp:TextBox id="addrnew" runat="server" Width="250px" CssClass="inpText" Visible="False" __designer:wfdid="w145" MaxLength="199"></asp:TextBox> <asp:Label id="sAppOid" runat="server" Text="0" Visible="False" __designer:wfdid="w146"></asp:Label></TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="Lblcitynew" runat="server" Width="90px" Visible="False" __designer:wfdid="w147">City New</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="citynew" runat="server" Width="150px" CssClass="inpText" Visible="False" __designer:wfdid="w148"></asp:TextBox> <asp:ImageButton id="BtnCariCity" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w149"></asp:ImageButton> <asp:ImageButton id="BtnBersihCity" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w150"></asp:ImageButton> <asp:Label id="CityOidNew" runat="server" Visible="False" __designer:wfdid="w151">0</asp:Label></TD><TD align=left colSpan=1><asp:Label id="Lblprovnew" runat="server" Width="90px" Visible="False" __designer:wfdid="w152">Provinsi New</asp:Label><asp:Label id="ProvOidNew" runat="server" Visible="False" __designer:wfdid="w153">0</asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="provnew" runat="server" Width="189px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w154" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD align=left></TD><TD align=left colSpan=3><asp:GridView id="GvCity" runat="server" Width="550px" ForeColor="#333333" Visible="False" __designer:wfdid="w155" DataKeyNames="CityOId,KodeCity,CityNya,ProvOid,Provinsi" GridLines="None" EmptyDataText="No data found" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="False" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="KodeCity" HeaderText="Kode Kota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CityNya" HeaderText="Nama Kota">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Provinsi" HeaderText="Provinsi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CityOId" HeaderText="CityOId" Visible="False"></asp:BoundField>
<asp:BoundField DataField="ProvOid" HeaderText="ProvOid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="Lbltelprmhnew" runat="server" Width="106px" Visible="False" __designer:wfdid="w156">Telp. Rumah New</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="telprmhnew" runat="server" Width="120px" CssClass="inpText" Visible="False" __designer:wfdid="w157" MaxLength="30"></asp:TextBox> <asp:Label id="custdtloid" runat="server" Visible="False" __designer:wfdid="w158"></asp:Label></TD><TD align=left colSpan=1><asp:Label id="Lbltelpktrnew" runat="server" Width="108px" Visible="False" __designer:wfdid="w159">Telp. Kantor New</asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="telpktrnew" runat="server" Width="120px" CssClass="inpText" Visible="False" __designer:wfdid="w160" MaxLength="30"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Lbltelpwanew" runat="server" Width="90px" Visible="False" __designer:wfdid="w161">Telp. WA New</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="telpwanew" runat="server" Width="120px" CssClass="inpText" Visible="False" __designer:wfdid="w162" MaxLength="30"></asp:TextBox></TD><TD align=left colSpan=1><asp:Label id="Label10" runat="server" Width="108px" Visible="False" __designer:wfdid="w163">Status Cust. New</asp:Label></TD><TD align=left colSpan=2><asp:DropDownList id="custflagNew" runat="server" Width="103px" CssClass="inpText" Font-Size="8pt" Visible="False" __designer:wfdid="w164"><asp:ListItem>Active</asp:ListItem>
<asp:ListItem>Inactive</asp:ListItem>
<asp:ListItem>Suspended</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="LblTerminNew" runat="server" Width="90px" Visible="False" __designer:wfdid="w165">Termin New</asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="TerminNew" runat="server" CssClass="inpText" Font-Size="8pt" Visible="False" __designer:wfdid="w166"></asp:DropDownList></TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="Lblclawalnew" runat="server" Width="139px" Visible="False" __designer:wfdid="w167">Credit limit Awal New</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="clawalnew" runat="server" Width="120px" CssClass="inpText" Visible="False" AutoPostBack="True" __designer:wfdid="w168" MaxLength="15">0.00</asp:TextBox></TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="LblCLTempNew" runat="server" Width="133px" Visible="False" __designer:wfdid="w169">Credit limit Temp New</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="CLTempNew" runat="server" Width="120px" CssClass="inpText" Visible="False" AutoPostBack="True" __designer:wfdid="w170" MaxLength="18">0.00</asp:TextBox></TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="Lblcloverdue" runat="server" Width="132px" __designer:wfdid="w171">Credit Limit Overdue</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="cloverdue" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w172" MaxLength="18" Enabled="False">0.00</asp:TextBox> <asp:Button id="btnInfoNota" runat="server" CssClass="btn green" Text="Info Nota" Visible="False" __designer:wfdid="w173"></asp:Button></TD><TD vAlign=top align=left colSpan=1></TD><TD vAlign=top align=left colSpan=1></TD><TD align=left colSpan=1 rowSpan=1></TD></TR><TR><TD vAlign=top align=left><asp:Label id="CatatanTxt" runat="server" Width="61px" __designer:wfdid="w174">Catatan</asp:Label></TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="Catatan" runat="server" Width="250px" Height="45px" CssClass="inpTextDisabled" __designer:wfdid="w175" MaxLength="250" Enabled="False" TextMode="MultiLine"></asp:TextBox></TD><TD vAlign=top align=left colSpan=1>Keterangan</TD><TD vAlign=top align=left colSpan=1><asp:TextBox id="custapprovalnote" runat="server" Width="250px" Height="45px" CssClass="inpText" __designer:wfdid="w176" MaxLength="250" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label2" runat="server" Width="120px" Font-Bold="True" ForeColor="Red" __designer:wfdid="w177">Max 250 karakter</asp:Label></TD><TD align=left colSpan=1 rowSpan=2>&nbsp;</TD></TR><TR><TD align=left><asp:Label id="Label14" runat="server" Width="100px" __designer:wfdid="w178">Credit limit Total</asp:Label></TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="cltotal" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w179" MaxLength="18" Enabled="False">0.00</asp:TextBox></TD><TD vAlign=top align=left colSpan=1></TD><TD vAlign=top align=left colSpan=1></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Width="100px" __designer:wfdid="w180">Status</asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="custapprovalstatus" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w181" Enabled="False">IN PROCESS</asp:TextBox></TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left colSpan=7><asp:Label id="update" runat="server" Font-Bold="False" __designer:wfdid="w182"></asp:Label> <asp:Label id="create" runat="server" Font-Bold="False" __designer:wfdid="w183"></asp:Label> <asp:Label id="createtime" runat="server" Font-Bold="False" Visible="False" __designer:wfdid="w184"></asp:Label></TD></TR><TR><TD align=left colSpan=7><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w185" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w186" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w187" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnSendApp" runat="server" ImageUrl="~/Images/sendapproval.png" __designer:wfdid="w188"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnApproved" runat="server" ImageUrl="~/Images/approve.png" Visible="False" __designer:wfdid="w189"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnRejected" runat="server" ImageUrl="~/Images/reject.png" Visible="False" __designer:wfdid="w190"></asp:ImageButton></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="1688849860263976" __designer:wfdid="w191" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="1688849860263977">
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w192"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:MaskedEditExtender id="MEEAppDate" runat="server" __designer:wfdid="w193" CultureName="id-ID" Mask="99/99/9999" MaskType="Date" TargetControlID="custapprovaldate"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEawalnew" runat="server" __designer:wfdid="w194" TargetControlID="clawalnew" ValidChars="1234567890-,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTECLTempNew" runat="server" __designer:wfdid="w195" TargetControlID="CLTempNew" ValidChars="-1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="BtnSendApp"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            &nbsp;<strong><span style="font-size: 9pt">.: Form Customer Approval</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">Catatan Transaksi Nota</asp:Label></TD></TR><TR><TD style="BACKGROUND-COLOR: beige" class="Label" align=center colSpan=3><asp:GridView id="gvListNya" runat="server" Width="99%" CssClass="MyTabStyle" ForeColor="#333333" DataKeyNames="trnjualno,trnjualdate,payduedate,trnamtjualnetto" PageSize="5" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnjualno" HeaderText="No. Transasksi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Tgl Jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnamtjualnetto" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3>&nbsp;&nbsp; <asp:LinkButton id="lbCloseListMat" runat="server">[ Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat">
</ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="lbCloseListMat"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></td>
        </tr>
    </table>
                          
</asp:Content>