﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPayAR.aspx.vb" Inherits="Accounting_trnPayAR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Pelunasan Piutang"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel3" runat="server" Width="100%" __designer:wfdid="w32" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="Cabang" runat="server" Text="Cabang" __designer:wfdid="w33"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="fCabang" runat="server" CssClass="inpText" __designer:wfdid="w34"></asp:DropDownList></TD></TR><TR><TD align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode" __designer:wfdid="w35" Checked="True"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="txtPeriode1" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w36"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="X-Small" Text="to" __designer:wfdid="w38"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w39"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Filter</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddlFilter" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w41"><asp:ListItem Value="cashbankno">Pay AR No.</asp:ListItem>
<asp:ListItem Value="s.custname">Customer</asp:ListItem>
<asp:ListItem Value="b.trnjualno">Invoice No</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w42"></asp:TextBox> <asp:DropDownList id="ddlpaytype" runat="server" Width="55px" CssClass="inpText" __designer:wfdid="w43" Visible="False">
<asp:ListItem Text="CASH" Value="K"></asp:ListItem>
<asp:ListItem Value="G">GIRO</asp:ListItem>
<asp:ListItem Text="NONCASH" Value="BB"></asp:ListItem>
<asp:ListItem Value="D">DP</asp:ListItem>
<asp:ListItem Value="C">CREDIT CARD</asp:ListItem>
</asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="postinge" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w44"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value=" ">In Process</asp:ListItem>
<asp:ListItem Value="POST">POST</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; &nbsp; &nbsp; </TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD3" align=left runat="server" Visible="false">Order By</TD><TD align=left runat="server" Visible="false"></TD><TD id="TD4" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="orderby" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w45">
                                                                            <asp:ListItem Value="cashbankoid desc">ID</asp:ListItem>
                                                                            <asp:ListItem Value="cashbankno desc">Pay AR No(Z-A)</asp:ListItem>
                                                                            <asp:ListItem Value="cashbankno">Pay AR No(A-Z)</asp:ListItem>
                                                                            <asp:ListItem Value="cashbankdate desc">Pay Date(Z-A)</asp:ListItem>
                                                                            <asp:ListItem Value="cashbankdate">Pay Date(A-Z)</asp:ListItem>
                                                                            <asp:ListItem Value="custname  desc">Customer(Z-A)</asp:ListItem>
                                                                            <asp:ListItem Value="custname">Customer(A-Z)</asp:ListItem>
                                                                        </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=5 runat="server" Visible="true"><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w46"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w48" Visible="False"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=5 runat="server" Visible="true"><asp:Label id="Label8" runat="server" Font-Bold="True" Text="Click button Find or View All to view data" __designer:wfdid="w49"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=5><DIV style="OVERFLOW-Y: scroll; WIDTH: 950px; HEIGHT: 300px; BACKGROUND-COLOR: beige"><asp:GridView id="GVmstPAYAP" runat="server" ForeColor="#333333" __designer:wfdid="w50" OnSelectedIndexChanged="GVmstPAYAP_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankno,cashbankoid,branch_code" GridLines="None" AllowPaging="True" OnPageIndexChanging="GVmstPAYAP_PageIndexChanging" PageSize="8" OnRowCommand="gridCommand" EnableModelValidation="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="25px"></ItemStyle>
</asp:CommandField>
    <asp:BoundField DataField="cashbankno" HeaderText="No. Cash/Bank">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
        <ItemStyle HorizontalAlign="Left" Wrap="False" />
    </asp:BoundField>
<asp:BoundField DataField="suppliername" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left" Width="150px" />
</asp:BoundField>
<asp:BoundField DataField="date" HeaderText="Tanggal Bayar">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payrefno" HeaderText="Pay Ref No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Total Bayar">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
    <asp:TemplateField HeaderText="Print Nota">
        <ItemTemplate>
            <asp:ImageButton ID="btnPrintNya" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/print.gif"
                OnClick="btnPrintNya_Click" ToolTip='<%# Eval("cashbankno") %>' />
        </ItemTemplate>
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" Wrap="False" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Data tidak ditemukan !!</asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=5><asp:UpdateProgress id="ufProgReportForm" runat="server" __designer:wfdid="w51" AssociatedUpdatePanelID="UpdatePanel9" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div5" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w52"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w53" TargetControlID="txtPeriode2" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce4" runat="server" __designer:wfdid="w54" TargetControlID="txtperiode1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee4" runat="server" __designer:wfdid="w55" TargetControlID="txtPeriode1" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce5" runat="server" __designer:wfdid="w56" TargetControlID="txtperiode2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR></TBODY></TABLE></asp:Panel> <asp:Label id="Label13" runat="server" Font-Bold="True" Text="Grand Total : Rp." __designer:wfdid="w57" Visible="False"></asp:Label> <asp:Label id="lblgrandtotal" runat="server" Font-Bold="True" __designer:wfdid="w58" Visible="False">0.00</asp:Label>&nbsp;<BR />
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVmstPAYAP"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            &nbsp;
                            <span style="font-size: 9pt"><strong> List Pelunasan Piutang</strong></span><strong><span
                                style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" /><strong><span style="font-size: 9pt"> Form Pelunasan Piutang</span></strong><strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left><asp:DropDownList style="HEIGHT: 17px" id="ddlcabang" runat="server" Width="101px" CssClass="inpText" AutoPostBack="True" Enabled="true" __designer:wfdid="w218" OnSelectedIndexChanged="ddlcabang_SelectedIndexChanged"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" class="Label" align=left><asp:Label id="cashbankoid" runat="server" Width="50px" __designer:wfdid="w219" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="CutofDate" runat="server" __designer:wfdid="w220" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Pay AR no </TD><TD class="Label" align=left><asp:TextBox id="defcbno" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w221" MaxLength="20"></asp:TextBox> </TD><TD style="FONT-SIZE: x-small" class="Label" align=left><asp:Label id="lblPOST" runat="server" Font-Size="X-Small" __designer:wfdid="w222" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="cashbankno" runat="server" __designer:wfdid="w223" Visible="False"></asp:Label><asp:Label id="dp_currency" runat="server" __designer:wfdid="w224" Visible="False"></asp:Label><asp:Label id="tello" runat="server" __designer:wfdid="w225" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" class="Label" align=left><asp:Label id="trnsuppoid" runat="server" Font-Size="X-Small" __designer:wfdid="w226" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Data" __designer:wfdid="w227" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Tanggal Pembayaran&nbsp;<asp:Label id="Label25" runat="server" CssClass="Important" Text="*" __designer:wfdid="w228"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="PaymentDate" runat="server" Width="75px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w229" OnTextChanged="PaymentDate_TextChanged"></asp:TextBox> <asp:ImageButton id="btnPayDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w230" Visible="False" BorderColor="White"></asp:ImageButton> </TD><TD class="Label" align=left>Customer</TD><TD class="Label" align=left><asp:TextBox id="suppnames" runat="server" Width="200px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False" __designer:wfdid="w231"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w232"></asp:ImageButton> </TD><TD class="Label" align=left>Payment Currency</TD><TD><asp:DropDownList id="CurrencyOid" runat="server" Width="92px" CssClass="inpTextDisabled" AutoPostBack="True" __designer:wfdid="w233" OnSelectedIndexChanged="CurrencyOid_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Jenis Pembayaran</TD><TD class="Label" align=left><asp:DropDownList style="HEIGHT: 17px" id="payflag" runat="server" Width="101px" CssClass="inpText" AutoPostBack="True" Enabled="true" __designer:wfdid="w234"><asp:ListItem>CASH</asp:ListItem>
<asp:ListItem>BANK</asp:ListItem>
<asp:ListItem>GIRO</asp:ListItem>
<asp:ListItem>DP</asp:ListItem>
<asp:ListItem Enabled="False">CREDIT CARD</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left>COA&nbsp;<asp:Label id="lblPayType" runat="server" Text="Cash" __designer:wfdid="w235"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="cashbankacctgoid" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w236" OnSelectedIndexChanged="cashbankacctgoid_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left>Total Pembayaran</TD><TD class="Label" align=left><asp:TextBox id="amtbelinettodtl" runat="server" Width="111px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w237">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblpayduedate" runat="server" Text="Jatuh Tempo" __designer:wfdid="w238" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w239" Visible="true"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w240" Visible="False" BorderColor="White"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="lblpayrefno" runat="server" Width="77px" Text="Giro Number" __designer:wfdid="w241" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="payrefno" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w242" Visible="False" MaxLength="15"></asp:TextBox> <asp:TextBox id="code" runat="server" Width="99px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w243" Visible="False" MaxLength="30"></asp:TextBox>&nbsp;<asp:ImageButton style="WIDTH: 16px" id="creditsearch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w244" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="CREDITCLEAR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w245" Visible="False"></asp:ImageButton> </TD><DIV id="divGiro" runat="server"></DIV><TD class="Label" align=left><asp:Label id="LblBank" runat="server" Text="Bank" __designer:wfdid="w246" Visible="False"></asp:Label></TD><TD><asp:DropDownList id="dd_bankgiro" runat="server" CssClass="inpText" __designer:wfdid="w247" Visible="False"></asp:DropDownList> <asp:Label id="girooid" runat="server" __designer:wfdid="w248" Visible="False"></asp:Label></TD><DIV><asp:Label id="Label1" runat="server" Font-Bold="True" ForeColor="Black" Text="Informasi" __designer:wfdid="w249"></asp:Label></DIV></TR></TBODY><TBODY><TR><TD class="Label" align=left><asp:Label id="lbldpno" runat="server" Text="DP No" __designer:wfdid="w250" Visible="False"></asp:Label> </TD><TD class="Label" align=left><asp:DropDownList id="trndpapoid" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w251" OnSelectedIndexChanged="trndpapoid_SelectedIndexChanged" Visible="False">
                </asp:DropDownList> </TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Total Giro" __designer:wfdid="w252" Visible="False"></asp:Label> </TD><TD class="Label" align=left><asp:TextBox id="AmountGiro" runat="server" Width="140px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w253" Visible="False"></asp:TextBox> </TD><TD class="Label" align=left><asp:Label id="lbldpbalance" runat="server" Text="DP Balance" __designer:wfdid="w254" Visible="False"></asp:Label> </TD><TD><asp:TextBox id="dpbalance" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w255" Visible="False">0.00</asp:TextBox> </TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left colSpan=5><asp:TextBox id="cashbanknote" runat="server" Width="500px" CssClass="inpText" __designer:wfdid="w256" MaxLength="200" TextMode="MultiLine"></asp:TextBox> </TD></TR><TR><TD style="FONT-SIZE: x-small" class="Label" align=left colSpan=6><asp:TextBox id="TotalCost" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w257" Visible="False" ReadOnly="True">0.0000</asp:TextBox> <ajaxToolkit:MaskedEditExtender id="meeCurrRate" runat="server" __designer:wfdid="w258" InputDirection="RightToLeft" ErrorTooltipEnabled="True" TargetControlID="currencyRate" MaskType="Number" Mask="999,999,999.99">
                </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w259" TargetControlID="PaymentDate" MaskType="Date" Mask="99/99/9999" CultureName="id-ID">
                </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w260" TargetControlID="PaymentDate" PopupButtonID="btnPayDate" Format="dd/MM/yyyy">
                </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w261" TargetControlID="payduedate" PopupButtonID="btnDueDate" Format="dd/MM/yyyy">
                </ajaxToolkit:CalendarExtender> <asp:TextBox id="NetPayment" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w262" Visible="False" ReadOnly="True">0.0000</asp:TextBox> <asp:TextBox id="currencyRate" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w263" Visible="False" OnTextChanged="currencyRate_TextChanged"></asp:TextBox> <asp:Label id="lblnotice" runat="server" Font-Size="X-Small" __designer:wfdid="w264"></asp:Label> <asp:Label id="lblBankName" runat="server" Text="Bank Name" __designer:wfdid="w265" Visible="False"></asp:Label> <asp:DropDownList id="ddlBankName" runat="server" Width="151px" CssClass="inpText" __designer:wfdid="w266" Visible="False">
                </asp:DropDownList> <asp:HiddenField id="HiddenField2" runat="server" __designer:wfdid="w267"></asp:HiddenField> <asp:HiddenField id="HiddenField1" runat="server" __designer:wfdid="w268"></asp:HiddenField> </TD></TR></TBODY></TABLE><!--detail -->
<HR />
<asp:Label id="Label5" runat="server" Font-Bold="True" ForeColor="Black" Text="Detail" __designer:wfdid="w269"></asp:Label><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="I_U2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" __designer:wfdid="w270" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="trnbelimstoid" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w271" Visible="False"></asp:Label> <asp:Label id="lbltello" runat="server" __designer:wfdid="w272" Visible="False"></asp:Label> </TD><TD align=left><asp:CheckBox id="CBTax" runat="server" Width="70px" Font-Size="X-Small" AutoPostBack="True" __designer:wfdid="w273" Visible="False" OnCheckedChanged="CBTax_CheckedChanged"></asp:CheckBox></TD><TD align=left><asp:Label id="Payseq" runat="server" __designer:wfdid="w274" Visible="False"></asp:Label>&nbsp;<asp:Label id="paytype" runat="server" __designer:wfdid="w275" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="invCurrOid" runat="server" Font-Bold="False" __designer:wfdid="w276" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="invCurrCode" runat="server" __designer:wfdid="w277" Visible="False"></asp:Label></TD></TR><TR><TD align=left>Nota No <asp:Label id="Label26" runat="server" CssClass="Important" Text="*" __designer:wfdid="w278"></asp:Label></TD><TD align=left><asp:TextBox id="trnbelino" runat="server" Width="123px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w279" MaxLength="20"></asp:TextBox> <asp:ImageButton id="btnSearchPurchasing" onclick="btnSearchPurchasing_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w280"></asp:ImageButton> </TD><TD align=left>A/R Account</TD><TD align=left><asp:TextBox id="APAcc" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w281"></asp:TextBox></TD><TD align=left><asp:Label id="acctgoid" runat="server" Font-Bold="False" __designer:wfdid="w282" Visible="False"></asp:Label></TD><TD align=left></TD></TR><TR><TD align=left></TD><TD align=left><asp:TextBox id="suppname" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w283" Visible="False" MaxLength="20"></asp:TextBox></TD><TD align=left></TD><TD align=left><asp:TextBox id="invCurrDesc" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w284" Visible="False"></asp:TextBox></TD><TD align=left></TD><TD align=left><asp:TextBox id="invCurrRate" runat="server" Width="115px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w285" Visible="False">0</asp:TextBox></TD></TR><TR><TD align=left>Total Nota</TD><TD align=left><asp:TextBox id="amttrans" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w286" MaxLength="100">0</asp:TextBox></TD><TD align=left>Total Bayar</TD><TD align=left><asp:TextBox id="amtpaid" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w287" MaxLength="100">0</asp:TextBox></TD><TD align=left><asp:Label id="Label3" runat="server" Text="Total Retur" __designer:wfdid="w288" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="amtretur" runat="server" Width="115px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w289" Visible="False" MaxLength="100">0</asp:TextBox></TD></TR><TR><TD align=left>A/R Balance</TD><TD align=left><asp:TextBox id="APAmt" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w290" MaxLength="30">0</asp:TextBox></TD><TD align=left>Total Bayar*</TD><TD align=left><asp:TextBox id="amtpayment" runat="server" Width="121px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w291" MaxLength="15" OnTextChanged="amtpayment_TextChanged">0</asp:TextBox></TD><TD align=left>Sub Total Pembayaran</TD><TD align=left><asp:TextBox id="totalpayment" runat="server" Width="115px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w292" MaxLength="30">0</asp:TextBox></TD></TR><TR><TD align=left>Note Detail</TD><TD align=left colSpan=3><asp:TextBox id="txtNote" runat="server" Width="480px" Height="30px" CssClass="inpText" __designer:wfdid="w293" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:CheckBox id="chkOther" runat="server" Width="137px" Text="Selisih Pembayaran" AutoPostBack="True" __designer:wfdid="w294" OnCheckedChanged="chkOther_CheckedChanged"></asp:CheckBox></TD><TD align=left><asp:DropDownList id="DDLOtherType" runat="server" Width="155px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False" __designer:wfdid="w295" OnSelectedIndexChanged="DDLOtherType_SelectedIndexChanged">
                                                        <asp:ListItem Value="+">Kelebihan Bayar</asp:ListItem>
                                                        <asp:ListItem Value="-">Kurang Bayar</asp:ListItem>
                                                    </asp:DropDownList></TD><TD id="TD2" align=left runat="server" visible="true">Selisih</TD><TD id="TD1" align=left runat="server" visible="true"><asp:TextBox id="otheramt" runat="server" Width="129px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False" __designer:wfdid="w296"></asp:TextBox></TD><TD align=left><asp:TextBox id="TaxAmount" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w297" Visible="False" MaxLength="30" ReadOnly="True">0.00000</asp:TextBox> <asp:Label id="lblAmtTax" runat="server" Text="Total Tax" __designer:wfdid="w298" Visible="False"></asp:Label> <asp:Label id="lblTaxAcc" runat="server" Text="Akun Tax" __designer:wfdid="w299" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trnTaxPct" runat="server" Width="50px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w300" Visible="False" MaxLength="20">0.00</asp:TextBox> <asp:TextBox id="txtPaymentNo" runat="server" Width="25px" CssClass="inpText" __designer:wfdid="w301" Visible="False" MaxLength="10" ReadOnly="True"></asp:TextBox><asp:Label id="lblTaxPct" runat="server" Text="Tax (%)" __designer:wfdid="w302" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:CheckBox id="cbDP" runat="server" Text="DP" AutoPostBack="True" __designer:wfdid="w303" Visible="False" OnCheckedChanged="DP_CheckedChanged"></asp:CheckBox></TD><TD align=left><asp:DropDownList id="ddlDPNo" runat="server" Width="155px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w304" OnSelectedIndexChanged="ddlDPNo_SelectedIndexChanged" Visible="False"></asp:DropDownList></TD><TD align=left><asp:Label id="lblDPAmount" runat="server" Text="Total DP" __designer:wfdid="w305" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="DPAmt" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w306" Visible="False">0.00</asp:TextBox></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left colSpan=6><BR /><asp:Label id="Label14" runat="server" Font-Bold="True" Text="Detail Selisih Bayar :" __designer:wfdid="w307" Font-Underline="True"></asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="ftx_payamt" runat="server" __designer:wfdid="w308" TargetControlID="amtpayment" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbdpPayment" runat="server" __designer:wfdid="w309" TargetControlID="DPAmt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteDtlselisih" runat="server" __designer:wfdid="w310" TargetControlID="amtdtlselisih" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <asp:DropDownList id="TaxAccount" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w311" OnSelectedIndexChanged="cashbankacctgoid_SelectedIndexChanged" Visible="False"></asp:DropDownList> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w312" TargetControlID="payduedate" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD align=left>Selisih COA</TD><TD align=left colSpan=3><asp:DropDownList id="otherAcctgoid" runat="server" Width="408px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w313"></asp:DropDownList>&nbsp;</TD><TD align=left>Total Selisih<asp:Label id="Label18" runat="server" CssClass="Important" Text="*" __designer:wfdid="w314"></asp:Label></TD><TD align=left><asp:TextBox id="amtdtlselisih" runat="server" Width="125px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w315"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="stateDtlSls" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Selisih" __designer:wfdid="w316" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="dtlnoteselisih" runat="server" Width="400px" Height="37px" CssClass="inpText" __designer:wfdid="w317" Visible="False" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD><TD vAlign=top align=left colSpan=2><asp:LinkButton id="lkbAddDtlSlisih" onclick="lkbAddDtlSlisih_Click" runat="server" Font-Bold="True" __designer:wfdid="w318" Visible="False">[Tambah Detail Selisih]</asp:LinkButton> <asp:LinkButton id="lkbClearDtlSlisih" onclick="lkbClearDtlSlisih_Click" runat="server" Font-Bold="True" __designer:wfdid="w319" Visible="False">[Batal]</asp:LinkButton> </TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 150px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: beige"><asp:GridView id="gvDtlSelisih" runat="server" Width="98%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w320" OnSelectedIndexChanged="gvDtlSelisih_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="selisihseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Center" 
        Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="selisihseq" HeaderText="No">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Center" 
        Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Selisih COA">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtlselisih" HeaderText="Total">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right" 
        Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dtlnoteselisih" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl=" " DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Center" 
        Width="25px"></HeaderStyle>

<ItemStyle Font-Bold="True" Font-Size="Medium" ForeColor="Red" 
        HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label 
        ID="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" 
        Text="Data selisih bayar tidak ditemukan !!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR></TR><TR><TD align=right><asp:ImageButton id="ibtn" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w321"></asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w322" AlternateText="Clear"></asp:ImageButton></TD><TD align=right>
<HR style="BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: blue; BACKGROUND-COLOR: blue" />
</TD></TR><TR><TD><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 225px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtlPayAP" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w323" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="payseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="COA">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="invCurrCode" HeaderText="Nota Currency" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invCurrRate" HeaderText="Nota Rate" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Total Bayar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtretur" HeaderText="Amt Return">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Pembayaran">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paynote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Overline="False" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payres1" HeaderText="payres1" Visible="False"></asp:BoundField>
<asp:BoundField DataField="paytype" HeaderText="paytype" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label 
        ID="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" 
        Text="Data detail tidak ditemukan !!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="Label21" runat="server" Font-Bold="True" Text="Grand Total :  " __designer:wfdid="w324"></asp:Label>&nbsp;<asp:Label id="amtbelinettodtl4" runat="server" Font-Bold="True" __designer:wfdid="w325">0.0000</asp:Label></FIELDSET> </TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left>Last Updated By <asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w326"></asp:Label>&nbsp;On <asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w327"></asp:Label> </TD></TR><TR><TD align=left><asp:ImageButton style="HEIGHT: 23px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w328" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w329" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton style="HEIGHT: 23px" id="btnPosting2" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w330" AlternateText="Posting"></asp:ImageButton> <asp:ImageButton style="WIDTH: 60px" id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w331" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:wfdid="w332"></asp:ImageButton>&nbsp;&nbsp; <asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w333" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD vAlign=top align=center><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w334" AssociatedUpdatePanelID="UpdatePanel11" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div5" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w335"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD><TD vAlign=top align=left></TD></TR><TR><TD vAlign=top align=left><asp:UpdatePanel id="UpdatePanel7" runat="server" __designer:wfdid="w179" __designer:errorcontrol="Cannot create an object of type 'System.Drawing.Color' from its string representation '#333333' for the 'ForeColor' property."><ContentTemplate>
<asp:Panel id="Panel2" runat="server" Width="90%" CssClass="modalBox" __designer:wfdid="w180" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblPurcdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Akun Piutang(Invoice)" __designer:wfdid="w181"></asp:Label></TD></TR><TR><TD align=center colSpan=3>No. Nota&nbsp;&nbsp;: <asp:TextBox id="txtInputNotaBeli" runat="server" CssClass="inpText" __designer:wfdid="w182"></asp:TextBox>&nbsp;No. Faktur : <asp:TextBox id="txtInputNoFaktur" runat="server" CssClass="inpText" __designer:wfdid="w183"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindInv" onclick="imbFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w184" AlternateText="Find"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbViewAllInv" onclick="imbViewAllInv_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w185" AlternateText="View All"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" onclick="btnClearSupp_Click1" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w186" Visible="False"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=center colSpan=3><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 200px"><asp:GridView id="gvPurchasing" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w187" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelino,suppname,trnbelidate,amttrans,amtpaid,acctgoid,trntaxpct,currencyoid,currencyrate,currencycode,currencydesc,amtretur,payduedate,paytype" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualref" HeaderText="No. Faktur">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota+DN">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Total Bayar+CN">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtretur" HeaderText="Total Retur">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimstoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trntaxpct" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label 
        ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Tidak ada data piutang !!</asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET> </TD></TR><TR><TD vAlign=top align=center colSpan=3></TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="ClosePurc" onclick="ClosePurc_Click" runat="server" CausesValidation="False" Font-Bold="True" __designer:wfdid="w188">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" __designer:wfdid="w189" TargetControlID="hiddenbtnpur" PopupDragHandleControlID="lblPurcdata" PopupControlID="Panel2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtnpur" runat="server" __designer:wfdid="w190" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel4s" runat="server" __designer:wfdid="w191" __designer:errorcontrol="'asp:UpdatePanel' could not be set on property 'ContentTemplate'."><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w192" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Customer" __designer:wfdid="w193"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLSuppID" runat="server" CssClass="inpText" __designer:wfdid="w194"><asp:ListItem 
            Value="custCODE">Code</asp:ListItem>
<asp:ListItem Selected="True" Value="custNAME">Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindSuppID" runat="server" Width="121px" CssClass="inpText" __designer:wfdid="w195"></asp:TextBox> <asp:ImageButton id="ibtnSuppID" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w196" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAlls" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w197" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=left><asp:GridView id="gvSupplier" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w198" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID,Code,Name" GridLines="None" AllowSorting="True" EmptyDataText="No data in database." AllowPaging="True" OnPageIndexChanging="gvSupplier_PageIndexChanging" PageSize="8">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="Code" HeaderText="Cust. Code" SortExpression="Code">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Name" HeaderText="Cust. Name" SortExpression="bank">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ID" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                        <asp:Label 
        ID="lblstatusdatasupp" runat="server" ForeColor="Red" 
        Text="No Suppplier Data !" Visible="False"></asp:Label>
                                                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSupp" onclick="CloseSupp_Click" runat="server" CausesValidation="False" Font-Bold="False" __designer:wfdid="w199">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender3s" runat="server" __designer:wfdid="w200" TargetControlID="hiddenbtn2s" PopupDragHandleControlID="lblSuppdata" PopupControlID="Panel1" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2s" runat="server" __designer:wfdid="w201" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel4sX" runat="server" __designer:wfdid="w202" __designer:errorcontrol="Exception of type 'System.OutOfMemoryException' was thrown."><ContentTemplate>
<asp:Panel id="Panel1X" runat="server" Width="800px" Height="300px" CssClass="modalBox" __designer:wfdid="w203" Visible="False"><TABLE style="HEIGHT: 100px"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdataX" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Giro" __designer:wfdid="w204"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLSuppIDX" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w205">
                                                                                    <asp:ListItem 
            Selected="True" Value="girono">Giro No</asp:ListItem>
                                                                                    <asp:ListItem 
            Value="m.gendesc">Bank</asp:ListItem>
                                                                                </asp:DropDownList> <asp:TextBox id="txtFindSuppIDX" runat="server" Width="121px" CssClass="inpText" __designer:wfdid="w206"></asp:TextBox> <asp:ImageButton id="ibtnSuppIDX" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w207" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAllsX" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w208" AlternateText="View All"></asp:ImageButton> </TD></TR><TR><TD align=left><asp:GridView id="gvSupplierX" runat="server" Width="780px" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w209" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="girodtloid,Trans_No,girono,Bank,bankoid,NoRekening,GiroDueDate,amount" GridLines="None" AllowSorting="True" EmptyDataText="No data in database." AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="girono" HeaderText="No.Giro" SortExpression="girono">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bank" HeaderText="Bank" SortExpression="bank">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="NoRekening" HeaderText="No Rekening" SortExpression="NoRekening">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GiroDueDate" HeaderText="Jatuh Tempo">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bankoid" HeaderText="No.Giro" SortExpression="bankoid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="lblstatusdatasupp" runat="server" 
        ForeColor="Red" Text="No Data Giro" Visible="False"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSuppX" runat="server" CausesValidation="False" Font-Bold="False" __designer:wfdid="w210">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender3sX" runat="server" __designer:wfdid="w211" TargetControlID="hiddenbtn2sX" PopupDragHandleControlID="lblSuppdata" PopupControlID="Panel1x" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2sX" runat="server" Text="hiddenbtn2sX" __designer:wfdid="w212" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w336"><ContentTemplate>
<ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w337" TargetControlID="hiddenbtn2" PopupDragHandleControlID="lblSuppdata" PopupControlID="Panel1" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2" runat="server" __designer:wfdid="w338" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD><TD align=left></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
