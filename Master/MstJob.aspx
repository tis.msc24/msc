<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="MstJob.aspx.vb" Inherits="Master_Master_Job" title="MSC - Data Job" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table>
        <tr>
            <td align="left" colspan="3">
                <table width="960">
                    <tr>
                        <td align="left" char="header" colspan="3" style="background-color: silver">
                            <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                                ForeColor="Navy" Text=".: Data Job"></asp:Label></td>
                    </tr>
                </table>
                &nbsp;<ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 101px"><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w156"></asp:Label></TD><TD style="WIDTH: 11px"><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w157"></asp:Label></TD><TD colSpan=4><asp:DropDownList id="FilterDDL" runat="server" Width="137px" CssClass="inpText" __designer:wfdid="w158"><asp:ListItem Value="a.itservcode">Item Service Code</asp:ListItem>
<asp:ListItem Value="a.itservdesc">Deskripsi</asp:ListItem>
<asp:ListItem Value="c.gendesc">Jenis Barang</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="189px" CssClass="inpText" __designer:wfdid="w159"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w160"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w161"></asp:ImageButton></TD></TR><TR><TD colSpan=6><asp:GridView id="GVmstjob" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w162" GridLines="None" EmptyDataText="Data Not Found" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="White" BorderColor="White" ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="itservoid" DataNavigateUrlFormatString="~\master\MstJob.aspx?idPage={0}" DataTextField="itservcode" HeaderText="Item Service Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="itservtype" HeaderText="Tipe Pengerjaan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itservdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itservtarif" HeaderText="Tarif Pengerjaan">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itservpoint" HeaderText="Poin Pengerjaan">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itservnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE>&nbsp; 
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <strong>
                    <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                    List Of Master Job :.</strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 126px"><asp:Label id="Label8" runat="server" Width="112px" Text="Item Service Code" __designer:wfdid="w623"></asp:Label><asp:Label id="Label3" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w624"></asp:Label></TD><TD><asp:Label id="Label11" runat="server" Text=":" __designer:wfdid="w625"></asp:Label></TD><TD><asp:TextBox id="itservcode" runat="server" CssClass="inpText" __designer:wfdid="w626" ReadOnly="True"></asp:TextBox>&nbsp; <asp:Label id="i_u" runat="server" ForeColor="Red" Text="New" __designer:wfdid="w627" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 126px"><asp:Label id="Label21" runat="server" Width="76px" Text="Jenis Barang" __designer:wfdid="w628"></asp:Label></TD><TD>:</TD><TD><asp:DropDownList id="Jenisddl" runat="server" Width="184px" CssClass="inpText" __designer:wfdid="w629"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 126px"><asp:Label id="Label10" runat="server" Width="96px" Text="Tipe Pengerjaan" __designer:wfdid="w630"></asp:Label><asp:Label id="Label4" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w631"></asp:Label></TD><TD><asp:Label id="Label6" runat="server" Text=":" __designer:wfdid="w632"></asp:Label></TD><TD><asp:DropDownList id="itservtypeoid" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w633"></asp:DropDownList>&nbsp;</TD></TR><TR><TD style="WIDTH: 126px"><asp:Label id="Label9" runat="server" Width="56px" Text="Deskripsi" __designer:wfdid="w634"></asp:Label><asp:Label id="Label5" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w635"></asp:Label></TD><TD><asp:Label id="Label12" runat="server" Text=":" __designer:wfdid="w636"></asp:Label></TD><TD><asp:DropDownList id="itservdesc" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w637"><asp:ListItem>Ringan</asp:ListItem>
<asp:ListItem>Sedang</asp:ListItem>
<asp:ListItem>Berat</asp:ListItem>
<asp:ListItem>Other</asp:ListItem>
</asp:DropDownList> <asp:Label id="Label20" runat="server" CssClass="Important" Text="Maks. 70 Karakter" __designer:wfdid="w638" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 126px"><asp:Label id="Label14" runat="server" Width="96px" Text="Tarif Pengerjaan" __designer:wfdid="w639"></asp:Label><asp:Label id="Label7" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w640"></asp:Label></TD><TD><asp:Label id="Label17" runat="server" Text=":" __designer:wfdid="w641"></asp:Label></TD><TD><asp:TextBox id="itservtarif" runat="server" CssClass="inpText" __designer:wfdid="w642" OnTextChanged="itservtarif_TextChanged" MaxLength="14" AutoPostBack="True"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FTEitservtarif" runat="server" __designer:wfdid="w643" TargetControlID="itservtarif" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="WIDTH: 126px"><asp:Label id="Label15" runat="server" Width="112px" Text="Poin Pengerjaan" __designer:wfdid="w644"></asp:Label></TD><TD><asp:Label id="Label18" runat="server" Text=":" __designer:wfdid="w645"></asp:Label></TD><TD><asp:TextBox id="itservpoint" runat="server" CssClass="inpText" __designer:wfdid="w646" OnTextChanged="itservpoint_TextChanged" MaxLength="14"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FTEitservpoint" runat="server" __designer:wfdid="w647" TargetControlID="itservpoint" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="WIDTH: 126px"><asp:Label id="Label16" runat="server" Text="Keterangan" __designer:wfdid="w648"></asp:Label></TD><TD><asp:Label id="Label19" runat="server" Text=":" __designer:wfdid="w649"></asp:Label></TD><TD><asp:TextBox id="itservnote" runat="server" CssClass="inpText" __designer:wfdid="w650" MaxLength="50" AutoPostBack="True" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:Label id="Label13" runat="server" CssClass="Important" Text="Maks. 70 Karakter" __designer:wfdid="w651"></asp:Label> <asp:TextBox id="itservoid" runat="server" Width="22px" CssClass="inpText" __designer:wfdid="w652" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="hiddencode" runat="server" __designer:wfdid="w653" Visible="False"></asp:TextBox></TD></TR><TR><TD colSpan=3>Created On <asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w654"></asp:Label>&nbsp;By&nbsp;<asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w655"></asp:Label></TD></TR><TR><TD colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w656"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w657"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w658" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <strong>
                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                    Form Of Master Job</strong> <strong>:.</strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
        SelectCommand="SELECT QL_mstitemserv.* FROM QL_mstitemserv"></asp:SqlDataSource>
    <br />
    <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w255" Visible="False"><TABLE width=350><TBODY><TR><TD style="BACKGROUND-COLOR: red"><asp:Label id="lblCaption" runat="server" Width="342px" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w256"></asp:Label></TD></TR><TR><TD align=center><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/warn.png" ImageAlign="AbsMiddle" __designer:wfdid="w257"></asp:Image> <asp:Label id="lblPopUpMsg" runat="server" Width="211px" ForeColor="Red" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD></TD></TR><TR><TD align=center colSpan=1><asp:ImageButton id="imbPopUpOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w259"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w260" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w261" TargetControlID="bePopUpMsg" DropShadow="True" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

