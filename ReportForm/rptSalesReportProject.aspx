<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSalesReportProject.aspx.vb" Inherits="ReportForm_rptSalesReport_Project" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" style="width: 976px; height: 168px">
        <tr>
            <td colspan="3" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                                Text=".: Laporan Sales Order Project" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                </table>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 580px; HEIGHT: 72px"><TBODY><TR><TD style="WIDTH: 128px" align=right>Status :</TD><TD style="HEIGHT: 1px" align=left colSpan=3><asp:RadioButton id="rbSO" runat="server" Text="Status SO Project" AutoPostBack="True" Checked="True" GroupName="rbStatus"></asp:RadioButton> <asp:RadioButton id="rbSODO" runat="server" Text="Status SO-Sales DO Project" AutoPostBack="True" GroupName="rbStatus"></asp:RadioButton></TD></TR><TR><TD style="WIDTH: 128px" id="TD2" align=right runat="server">Type :</TD><TD style="WIDTH: 345px" id="TD3" align=left colSpan=3 runat="server"><asp:DropDownList id="ddltype" runat="server" Width="154px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddltype_SelectedIndexChanged"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 128px" align=right>Periode :</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:TextBox id="range1" runat="server" Width="60px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="60px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD style="WIDTH: 128px" align=right>SO No. :</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:TextBox id="sono" runat="server" Width="159px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="posearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="cancelpo" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="sooid" runat="server" Visible="False"></asp:Label>&nbsp;<asp:DropDownList id="type_so" runat="server" Width="64px" CssClass="inpText" AutoPostBack="True" Visible="False"><asp:ListItem Value="'%'">All</asp:ListItem>
<asp:ListItem Value="'SO/%'">SO</asp:ListItem>
<asp:ListItem Value="'SOT/%'">SOT</asp:ListItem>
<asp:ListItem Value="'SJ/%'">SJ</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 128px" id="TD1" align=right runat="server" visible="false"></TD><TD style="WIDTH: 345px" id="tdSO" align=left colSpan=3 runat="server" visible="false"><asp:GridView id="gvListpo" runat="server" Width="450px" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="ordermstoid,orderno,trnorderdate,trncustname,trncustoid" BorderWidth="1px" BorderStyle="Solid" BorderColor="#DEDFDE" UseAccessibleHeader="False">
                <RowStyle BackColor="#F7F7DE" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True">
                        <HeaderStyle Font-Size="X-Small" Width="50px" />
                        <ItemStyle Font-Size="XX-Small" ForeColor="Red" HorizontalAlign="Center" Width="50px" />
                    </asp:CommandField>
                    <asp:BoundField DataField="orderno" HeaderText="SO No." />
                    <asp:BoundField DataField="trnorderdate" HeaderText="SO Date">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
                <PagerStyle BackColor="Silver" Font-Bold="True" HorizontalAlign="Right" />
                <EmptyDataTemplate>
                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="Yellow" />
                <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView> </TD></TR><TR><TD style="WIDTH: 128px" align=right>So Status :</TD><TD style="WIDTH: 345px" align=left colSpan=3>
        <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" CssClass="inpText" Visible="True">
            <asp:ListItem Value="'All'">All</asp:ListItem>
            <asp:ListItem Value="'In Process'">In Process</asp:ListItem>
            <asp:ListItem Value="'In Approval'">In Approval</asp:ListItem>
            <asp:ListItem Value="'Approved'">Approved</asp:ListItem>
            <asp:ListItem Value="'Rejected'">Rejected</asp:ListItem>
        </asp:DropDownList>
        </TD></TR><TR><TD style="WIDTH: 128px" align=right>Customer Name&nbsp;:</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:TextBox id="custname" runat="server" Width="169px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 128px" align=right></TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:GridView id="gvCustomer" runat="server" Width="98%" ForeColor="#333333" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="Id,Name,code" BorderWidth="1px" BorderStyle="Solid">
<RowStyle BackColor="#F7F7DE" Font-Size="X-Small"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
    <ItemStyle Font-Size="XX-Small" ForeColor="Red" />
</asp:CommandField>
<asp:BoundField DataField="code" HeaderText="Code"></asp:BoundField>
<asp:BoundField DataField="Name" HeaderText="Nama"></asp:BoundField>
<asp:BoundField DataField="Id" HeaderText="Id" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#F25407"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Silver" ForeColor="White" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblstatusdataCust" runat="server" Text="No data found !!" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 128px" align=right>Sales :</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="sales" runat="server" Width="215px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 128px" align=right>Item Group :</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="FilterDDLGrup" runat="server" Width="405px" CssClass="inpText">
            </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 128px" align=right>Item Sub Group :</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="405px" CssClass="inpText">
    </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 128px" align=right>Item/Barang :</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:TextBox id="itemdesc" runat="server" Width="169px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" onclick="btnEraseItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=right colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="gvListForecast" runat="server" Width="98%" OnSelectedIndexChanged="gvListForecast_SelectedIndexChanged1" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemcode,itemdesc,itempriceunit1,itemoid,satuan1,unit,unit2,satuan2,konversi1_2">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Name">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Silver" ForeColor="White" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 128px" align=right>Taxable :</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="taxable" runat="server" Width="64px" CssClass="inpText">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>YES</asp:ListItem>
                <asp:ListItem>NO</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 128px" align=right>Status Delivery :</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="statusDelivery" runat="server" Width="134px" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>COMPLETE</asp:ListItem>
<asp:ListItem>IN COMPLETE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 128px" align=right></TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="paytype" runat="server" Width="67px" CssClass="inpText" AutoPostBack="True" Visible="False"><asp:ListItem Value=" and 1=1 ">All</asp:ListItem>
<asp:ListItem Value="and  (mst.trnpaytype in (select g.genoid from QL_mstGen g where abs(replace(replace(g.gencode,'CRD',''),'CSH',''))=0    and gengroup='PAYTYPE'  ) or mst.trnpaytype=0)">Cash</asp:ListItem>
<asp:ListItem Value="and   mst.trnpaytype in (select g.genoid from QL_mstGen g where abs(replace(replace(g.gencode,'CRD',''),'CSH',''))&gt;0    and gengroup='PAYTYPE'  )  ">Credit</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=center colSpan=4><BR /><asp:ImageButton id="btnView" onclick="btnView_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="BTNpRINT" onclick="BTNpRINT_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 120px" align=right colSpan=4><ajaxToolkit:CalendarExtender id="CLE1" runat="server" PopupButtonID="ImageButton1" TargetControlID="range1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" PopupButtonID="ImageButton2" TargetControlID="range2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" TargetControlID="range1" CultureName="en-US" MaskType="Date" ErrorTooltipEnabled="True" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" TargetControlID="range2" CultureName="en-US" MaskType="Date" ErrorTooltipEnabled="True" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <asp:Label id="Label2" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD align=center colSpan=4>&nbsp; &nbsp; <asp:UpdatePanel id="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK"
                Visible="False" Width="496px">
                <table border="0" cellpadding="1" cellspacing="1" style="width: 495px">
                    <tr>
                        <td align="left" colspan="2" style="height: 10px" valign="top">
                            <asp:Panel ID="panelPesan" runat="server" BackColor="Yellow" Height="25px" Width="100%">
                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Black"></asp:Label></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="height: 10px" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 46px" valign="top">
                            <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                Width="24px" />
                        </td>
                        <td align="left" valign="top">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="height: 10px; text-align: center" valign="top">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="text-align: center" valign="top">
                            <asp:ImageButton ID="btnMsgBoxOK" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/ok.png"
                                OnClick="btnMsgBoxOK_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" DropShadow="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption"
                TargetControlID="beMsgBox">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" CausesValidation="False" Visible="False"
                Width="130px" />
        </ContentTemplate>
    </asp:UpdatePanel> <TABLE><TBODY><TR><TD style="WIDTH: 100px" vAlign=top align=left><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" AutoDataBind="True" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="BTNpRINT"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnView"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        </table>
</asp:Content>

