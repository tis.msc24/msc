
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction

Partial Class mstgudang
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure

#End Region

#Region "Procedure"
    Private Function UpdateCheckedItem() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("GetUser") Is Nothing Then
            Dim dtTbl As DataTable = Session("GetUser")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "personnoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("GetUser") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedItem2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("GetUserView") Is Nothing Then
            Dim dtTbl As DataTable = Session("GetUser")
            Dim dtTbl2 As DataTable = Session("GetUserView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "personnoid=" & cbOid
                                dtView2.RowFilter = "personnoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("GetUser") = dtTbl
                Session("GetUserView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Try
            sSql = "SELECT l.cmpcode, l.genoid, l.gencode, UPPER(l.gendesc) gendesc, UPPER(l.gengroup) gengroup, l.genother1, l.genother2, l.genother3, UPPER(l.upduser) upduser, l.updtime, UPPER(c.gendesc) Cabang FROM QL_mstgen l Inner Join ql_mstgen c ON c.genoid=CAST(l.genother2 AS int) AND c.gengroup='CABANG' Where l.gengroup='LOCATION' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ORDER BY c.gencode"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "DataNya")
            GVmstgen.DataSource = dt : GVmstgen.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As Integer)
        Try
            sSql = "SELECT l.cmpcode, l.genoid, l.gencode, UPPER(l.gendesc) gendesc, UPPER(l.gengroup) gengroup, l.genother1, l.genother3, l.genother4, l.genother5, UPPER(l.upduser) upduser, l.updtime, UPPER(c.gendesc) Cabang, c.genoid cabangoid FROM QL_mstgen l Inner Join ql_mstgen c ON c.genoid=CAST(l.genother2 AS int) AND c.gengroup='CABANG' Where l.gengroup='LOCATION' AND l.genoid = " & vGenoid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    GenOid.Text = Integer.Parse(xreader("genoid"))
                    gencode.Text = Trim(xreader("gencode").ToString)
                    gendesc.Text = Trim(xreader("gendesc").ToString)
                    InitCabang()
                    sCabang.SelectedValue = Integer.Parse(xreader("cabangoid"))
                    DDLWarehouse.SelectedValue = Trim(xreader("genother1").ToString)
                    If xreader("genother4").ToString.ToUpper = "KANVAS" Then
                        genother4.Text = xreader("genother4").ToString.ToUpper
                        DdlType.SelectedValue = "KANVAS"
                    Else
                        DdlType.SelectedValue = Trim(xreader("genother5").ToString)
                    End If
                    Upduser.Text = Trim(xreader("upduser").ToString)
                    Updtime.Text = Trim(xreader("updtime").ToString)
                    genother3.Text = Trim(xreader("genother3").ToString)
                End While
                gencode.Enabled = False : DDLWarehouse.Enabled = False
                sCabang.Enabled = False
            End If
            btnDelete.Enabled = False : btnDelete.Visible = False
            xreader.Close() : conn.Close()

            sSql = "Select ROW_NUMBER() over (order by personoid asc) nomer, p.personnoid, USERID, USERNAME, BRANCH_CODE from QL_MSTPROF p Inner JOIN (SELECT CAST(Name as int) personoid FROM splitstring('" & genother3.Text.Trim & "')) d ON d.personoid=p.personnoid INNER JOIN QL_mstgen c ON p.BRANCH_CODE=c.gencode AND gengroup='CABANG' INNER JOIN QL_mstgen g ON Cast(g.genother2 as int)=c.genoid AND g.gengroup='LOCATION' Where g.genother5='" & DdlType.SelectedValue & "' AND p.personnoid IN (SELECT CAST(Name as int) personoid FROM splitstring('" & genother3.Text.Trim & "'))"
            Dim dtNya As DataTable = cKon.ambiltabel(sSql, "DataNya")
            Session("DataUser") = dtNya
            GvUserNya.DataSource = dtNya
            GvUserNya.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub InitCabang()
        sSql = "Select genoid, gendesc from QL_mstgen Where cmpcode='" & CompnyCode & "' AND gengroup='CABANG'"
        FillDDL(sCabang, sSql)
    End Sub

    Private Sub GetUser()
        sSql = "Select 'False' checkvalue, ROW_NUMBER() over (order by personnoid asc) nomer, p.personnoid, UPPER(USERID) USERID, UPPER(USERNAME) USERNAME, p.BRANCH_CODE, c.gendesc, UPPER(j.gendesc) Jabatan From QL_MSTPROF p Inner Join ql_mstgen c ON c.gencode=p.BRANCH_CODE AND c.gengroup='CABANG' Inner Join QL_MSTPERSON n ON n.PERSONOID=p.personnoid Inner Join QL_mstgen j ON n.PERSONSTATUS=j.genoid --Where c.genoid=" & sCabang.SelectedValue
        Dim dt As DataTable = cKon.ambiltabel(sSql, "GetUser")
        Session("GetUser") = dt
        'gvListMat.DataSource = gvListMat
        'gvListMat.DataBind()
    End Sub

    Public Sub InitWhDDL()
        'DLLPROVINCE'
        sSql = "Select genoid, gendesc from QL_mstgen where gengroup IN ('Warehouse')"
        FillDDL(DDLWarehouse, sSql)
    End Sub

    Public Sub GenerateGenID()
        GenOid.Text = GenerateID("ql_mstgen", CompnyCode)
    End Sub

    Private Sub clearItem()
        gencode.Text = "" : gendesc.Text = ""
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
    End Sub

    Private Sub GenerateCode()
        Dim gDesc, jumlahNol, c, c2, kodegen As String
        jumlahNol = "" : Dim d As Integer = 0
        gDesc = Left(gendesc.Text, 1).ToUpper
        c = ToDouble(GetStrData("SELECT Count(isnull(left(gencode,1) ,0)) FROM ql_mstgen WHERE left(gencode,1) = '" & Tchar(gDesc) & "' And gengroup='LOCATION'"))
        c2 = ToDouble(GetStrData("SELECT Count(isnull(left(gencode,1) ,0))+1 FROM ql_mstgen WHERE left(gencode,1) = '" & Tchar(gDesc) & "' And gengroup='LOCATION'"))
        If CInt(c) > CInt(c2) Then d = CInt(c) + 1 Else d = CInt(c2) + 1
        kodegen = GenNumberString(gDesc, "", d, 5)
        gencode.Text = kodegen
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            btnSearch.Visible = True
            btnList.Visible = True
            Dim userId As String = Session("UserID")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session  
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch

            If Session("branch_id") = "" Then
                Response.Redirect("~/Other/login.aspx")
            End If
            Response.Redirect("mstgudang.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")

        Page.Title = CompnyName & " - Data General"

        If Not IsPostBack Then
            'Me.btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")
            BindData(CompnyCode)
            InitCabang() : InitWhDDL()
            btnSearch.Visible = True
            btnList.Visible = True
            gendesc_TextChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "Update"
                btnDelete.Visible = True
                TabContainer1.ActiveTabIndex = 1
                FillTextBox(Session("oid"))
            Else
                i_u.Text = "New" : GenerateGenID()
                btnDelete.Enabled = False
                Upduser.Text = Session("UserID")
                Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                TabContainer1.ActiveTabIndex = 0
                'gencode.Enabled = True : gencode.CssClass = "inpText"
            End If
        End If
    End Sub

    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session("FilterText") = "" : Session("FilterDDL") = 0
        BindData(CompnyCode) : GVmstgen.PageIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If gencode.Text.Trim = "" Then
            sMsg &= "- Maaf, Tolong Isi Code Dahulu !!<BR>"
        End If

        If gendesc.Text.Trim = "" Then
            sMsg &= "- Maaf, Tolong Isi Description Dahulu !!<BR>"
        End If

        If gendesc.Text.Trim.Length > 70 Then
            sMsg &= "- Maaf, Maximum 70 characters untuk Description !!<BR>"
        End If

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck As String = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = 'LOCATION' AND gendesc = '" & Tchar(gendesc.Text) & "'"

        If Not Session("oid") Is Nothing Then
            sSqlCheck &= " AND genoid <> " & GenOid.Text
        End If

        If ToDouble(GetScalar(sSqlCheck)) > 0 Then
            sMsg &= "- Maaf, Description sudah dipakai di General Group yang lain..!!<br>"
        End If

        'cek code msgen yang kembar
        Dim sSqlCheckcode As String = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = 'LOCATION' AND gencode = '" & Tchar(gencode.Text) & "'"

        If Not Session("oid") Is Nothing Then
            sSqlCheckcode &= " AND genoid <> " & GenOid.Text
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetScalar(sSqlCheckcode)) > 0 Then
                sMsg &= "- Maaf, Code sudah dipakai di General Group yang sama...!!<br>"
            Else
                Dim sSqlCheckdesc As String = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = 'LOCATION' AND gendesc = '" & Tchar(gendesc.Text) & "'"

                If Not Session("oid") Is Nothing Then
                    sSqlCheckdesc &= " AND genoid <> " & GenOid.Text
                End If

                If ToDouble(GetScalar(sSqlCheckdesc)) > 0 Then
                    sMsg &= "- Maaf, Description sudah dipakai di General Group yang sama..!!<br>"
                End If
            End If
        End If

        If DdlType.SelectedValue = "KANVAS" Then
            genother4.Text = "KANVAS" : DdlType.SelectedValue = "UMUM"
        End If

        Dim dataUser As String = ""
        If Session("DataUser") IsNot Nothing Then
            Dim dtUser As DataTable = Session("DataUser")
            If dtUser.Rows.Count > 0 Then
                For i As Int32 = 0 To dtUser.Rows.Count - 1
                    dataUser &= dtUser.Rows(i)("personnoid").ToString.Trim & ","
                Next
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
            'UNTUK MENGISI GENOTHER TEXT PADA SAAT DDL AKTIF
            If Session("oid") = Nothing Or Session("oid") = "" Then
                GenerateGenID()
                sSql = "INSERT into QL_mstgen (cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2, genother3, upduser, updtime, genother4, genother5, genother6) " & _
                   "VALUES ('" & CompnyCode & "', " & GenOid.Text & ", '" & Tchar(gencode.Text) & "', '" & Tchar(gendesc.Text) & "', 'LOCATION', '" & Tchar(DDLWarehouse.SelectedValue) & "', '" & Tchar(sCabang.SelectedValue) & "', '" & dataUser & "', '" & Upduser.Text & "', current_timestamp, '" & genother4.Text & "', '" & DdlType.SelectedValue & "', '" & DdlType.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "update QL_mstoid set lastoid=" & GenOid.Text & " Where tablename = 'QL_mstgen'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else 'update data
                sSql = "UPDATE QL_mstgen SET gencode='" & Tchar(gencode.Text) & "', gendesc='" & Tchar(gendesc.Text) & "', genother1='" & Tchar(DDLWarehouse.SelectedValue) & "', genother2='" & Tchar(sCabang.SelectedValue) & "', genother3='" & dataUser & "', upduser='" & Upduser.Text & "', updtime=current_timestamp, genother4='" & genother4.Text & "', genother5='" & DdlType.SelectedValue & "', genother6='" & DdlType.SelectedValue & "' WHERE genoid=" & GenOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
        Session("oid") = Nothing
        Response.Redirect("~\Master\mstgudang.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If GenOid.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please fill General ID"))
            Exit Sub
        End If
        DeleteData("QL_mstgen", "genoid", GenOid.Text, CompnyCode)
        Response.Redirect("~\Master\mstgudang.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        clearItem()
        Response.Redirect("~\Master\mstgudang.aspx?awal=true")
    End Sub

    Protected Sub gendesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gendesc.TextChanged
        GenerateCode()
    End Sub

    Protected Sub imbPopUpOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPopUpOK.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("GetUser") Is Nothing Then
            GetUser()
            If Session("GetUser").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If UpdateCheckedItem() Then
            Dim dv As DataView = Session("GetUser").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("GetUserView") = dv.ToTable
                gvListMat.DataSource = Session("GetUserView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("GetUserView") = Nothing
                gvListMat.DataSource = Session("GetUserView")
                gvListMat.DataBind()
                Session("warninglistItem") = "Material data can't be found!"
                showMessage(Session("warninglistItem"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub BtnGetUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGetUser.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("GetUser") = Nothing : Session("GetUserView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedItem2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("GetUserView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub BtnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSelectAll.Click
        If Not Session("GetUserView") Is Nothing Then
            Dim dtTbl As DataTable = Session("GetUserView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("GetUser")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "personnoid=" & dtTbl.Rows(C1)("personnoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("GetUser") = objTbl
                Session("GetUserView") = dtTbl
                gvListMat.DataSource = Session("GetUserView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListSO") = "Maaf, Silahkan pilih datanya..!!"
            showMessage(Session("WarningListSO"), 2)
        End If

    End Sub

    Protected Sub BtnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSelectNone.Click
        If Not Session("GetUserView") Is Nothing Then
            Dim dtTbl As DataTable = Session("GetUserView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("GetUser")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "personnoid=" & dtTbl.Rows(C1)("personnoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("GetUser") = objTbl
                Session("GetUserView") = dtTbl
                gvListMat.DataSource = Session("GetUserView")
                gvListMat.DataBind()
            End If
            CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Session("warninglistItem") = "Please show data first!"
            showMessage(Session("warninglistItem"), 2)
        End If
    End Sub

    Protected Sub BtnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnViewChecked.Click
        If Session("GetUser") Is Nothing Then
            Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedItem() Then
            Dim dtTbl As DataTable = Session("GetUser")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("GetUserView") = dtView.ToTable
                gvListMat.DataSource = Session("GetUserView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("GetUserView") = Nothing
                gvListMat.DataSource = Session("GetUserView")
                gvListMat.DataBind()
                Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub BtnAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddToList.Click
        If UpdateCheckedItem() Then
            Dim dtTbl As DataTable = Session("GetUser")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"

            sSql = "Select *from (Select 0 nomer, 0 personnoid, '' USERID, '' USERNAME, '' BRANCH_CODE ) t Where personnoid != 0"
            Dim objTable As DataTable

            If Session("DataUser") IsNot Nothing Then
                objTable = Session("DataUser")
            Else
                objTable = cKon.ambiltabel(sSql, "Data")
            End If

            Dim counter As Integer = objTable.Rows.Count
            Dim dv As DataView = objTable.DefaultView
            Dim objRow As DataRow

            For C1 As Integer = 0 To dtView.Count - 1
                dv.RowFilter = "personnoid=" & dtView(C1)("personnoid")
                If dv.Count > 0 Then
                    dv.AllowEdit = True
                    dv(0)("personnoid") = dtView(C1)("personnoid")
                    dv(0)("USERID") = dtView(C1)("USERID").ToString
                    dv(0)("USERNAME") = dtView(C1)("USERNAME").ToString
                    dv(0)("BRANCH_CODE") = dtView(C1)("BRANCH_CODE")
                Else
                    counter += 1
                    objRow = objTable.NewRow()
                    objRow("nomer") = counter
                    objRow("personnoid") = dtView(C1)("personnoid")
                    objRow("USERID") = dtView(C1)("USERID").ToString
                    objRow("USERNAME") = dtView(C1)("USERNAME").ToString
                    objRow("BRANCH_CODE") = dtView(C1)("BRANCH_CODE")
                    objTable.Rows.Add(objRow)
                End If
                dv.RowFilter = ""
            Next
            dtView.RowFilter = ""
            GvUserNya.DataSource = objTable
            GvUserNya.DataBind()
            Session("DataUser") = objTable
            CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        End If
    End Sub

    Protected Sub CloseList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CloseList.Click
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub BtnViewALL_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnViewALL.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("GetUser") Is Nothing Then
            GetUser()
            If Session("GetUser").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedItem() Then
            Dim dt As DataTable = Session("GetUser")
            Session("GetUserView") = dt
            gvListMat.DataSource = Session("GetUserView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub GvUserNya_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GvUserNya.RowDeleting
        Dim objTable As DataTable = Session("DataUser")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("nomer") = C1 + 1
            dr.EndEdit()
        Next
        Session("DataUser") = objTable
        GvUserNya.DataSource = objTable
        GvUserNya.DataBind()
    End Sub

#End Region
End Class