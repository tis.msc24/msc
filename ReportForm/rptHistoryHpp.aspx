<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptHistoryHpp.aspx.vb" Inherits="rptStock" title="Padjajaran - Laporan Mutasi Stock" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan History HPP Item"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=right><asp:Label id="Label2" runat="server" Text="Tipe Laporan :" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="type" runat="server" Width="78px" CssClass="inpText" Visible="False" AutoPostBack="True"><asp:ListItem>Detail</asp:ListItem>
<asp:ListItem>Summary</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="tdPeriod1" align=right runat="server" Visible="false"><asp:Label id="Label6" runat="server" Text="Periode : "></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=right>Grup Barang :</TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDLGrup" runat="server" Width="230px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=right>
Type Barang :</TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="230px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=right>PIC :</TD><TD align=left colSpan=3><asp:DropDownList id="DDLpic" runat="server" Width="230px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=right><asp:Label id="Label3" runat="server" Width="114px" Text="Item / Barang :"></asp:Label></TD><TD align=left colSpan=3>
<asp:TextBox ID="ItemName" runat="server" CssClass="inpTextDisabled" Enabled="False"
Rows="2" TextMode="MultiLine" Width="255px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top"></asp:ImageButton>&nbsp;&nbsp;<asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=right><asp:Label id="lblfilterqty" runat="server" Text="Stok Akhir :"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="ddlfilterqty" runat="server" Width="71px" CssClass="inpText"><asp:ListItem>IGNORE</asp:ListItem>
<asp:ListItem>&lt;</asp:ListItem>
<asp:ListItem>&lt;=</asp:ListItem>
<asp:ListItem>&gt;</asp:ListItem>
<asp:ListItem>&gt;=</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="filterqty" runat="server" Width="97px" CssClass="inpText" AutoPostBack="True"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="fte5" runat="server" TargetControlID="filterqty" ValidChars=",.0123456789" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD id="Td1" align=right runat="server" Visible="false">SPG :</TD><TD id="Td2" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="person" runat="server" Width="280px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD id="Td3" align=right runat="server" Visible="false">Periode :</TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="period" runat="server" Width="135px" CssClass="inpText" Visible="False"></asp:DropDownList></TD></TR><TR><TD id="Td5" align=center colSpan=4 runat="server" Visible="true"><asp:ImageButton id="export_konversi" runat="server" ImageUrl="~/Images/Icons/InitStock.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="Td6" align=center colSpan=4 runat="server" Visible="false"><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crv" runat="server" AutoDataBind="true" HasCrystalLogo="False" HasGotoPageButton="False" HasPrintButton="False" DisplayGroupTree="False" HasExportButton="False" HasSearchButton="False" HasViewList="False" HasDrillUpButton="False" HasToggleGroupTreeButton="False" HasZoomFactorList="False"></CR:CrystalReportViewer> 
<asp:UpdateProgress ID="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
<div id="progressBackgroundFilter" class="progressBackgroundFilter">
</div>
<div id="processMessage" class="processMessage">
<span style="font-weight: bold; font-size: 10pt; color: purple">
<asp:Image ID="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
Please Wait .....</span><br />
</div>
</ProgressTemplate>
</asp:UpdateProgress>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ibPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="export_konversi"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
<asp:UpdatePanel ID="upListMat" runat="server">
<ContentTemplate>
<asp:Panel ID="pnlListMat" runat="server" CssClass="modalBox" DefaultButton="btnFindListMat"
Visible="False" Width="750px">
<table style="width: 100%">
<tbody>
<tr>
<td align="center" class="Label" colspan="3">
<asp:Label ID="lblListMat" runat="server" Font-Bold="True" Font-Size="Medium" Font-Underline="False">List Katalog</asp:Label></td>
</tr>
<tr>
<td align="center" class="Label" colspan="3">
Filter :
<asp:DropDownList ID="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px">
<asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Kode Katalog</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>
<asp:ImageButton ID="btnFindListMat" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
<asp:ImageButton ID="btnAllListMat" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
</tr>
<tr>
<td align="center" class="Label" colspan="3">
<div style="overflow-y: scroll; width: 100%; height: 325px">
&nbsp;<asp:GridView ID="gvItem" runat="server" AllowPaging="True" AutoGenerateColumns="False"
CellPadding="4" DataKeyNames="itemcode,itemdesc,itemoid,satuan3" Font-Size="X-Small"
ForeColor="#333333" GridLines="None" PageSize="50" UseAccessibleHeader="False" Width="100%">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
<Columns>
<asp:TemplateField HeaderText="Pilih">
<ItemTemplate>
<asp:CheckBox ID="chkSelect" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>' />
</ItemTemplate>
<HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
<ItemStyle HorizontalAlign="Left" />
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Wrap="False" />
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Wrap="False" />
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" />
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" />
</asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px" />
<ItemStyle Font-Bold="True" Font-Size="X-Small" ForeColor="SaddleBrown" HorizontalAlign="Center"
Width="50px" />
</asp:CommandField>
</Columns>
<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
<PagerStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Red" HorizontalAlign="Right" />
<EmptyDataTemplate>
<asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>
<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
<AlternatingRowStyle BackColor="White" />
</asp:GridView>
</div>
</td>
</tr>
<tr>
<td align="center" colspan="3">
<asp:LinkButton ID="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton>
<asp:LinkButton ID="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
</tr>
</tbody>
</table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mpeListMat" runat="server" BackgroundCssClass="modalBackground"
Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblListMat" TargetControlID="btnHideListMat">
</ajaxToolkit:ModalPopupExtender>
<asp:Button ID="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False" />
</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
<asp:Panel ID="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False">
<table width="250">
<tbody>
<tr>
<td align="left" colspan="3" style="width: 294px; height: 15px; background-color: red">
<asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
</tr>
</tbody>
</table>
<table width="250">
<tbody>
<tr>
<td style="width: 30px">
<asp:ImageButton ID="imIcon" runat="server" ImageUrl="~/Images/warn.png" /></td>
<td colspan="2">
<asp:Label ID="lblValidasi" runat="server" ForeColor="Red"></asp:Label></td>
</tr>
<tr>
<td colspan="3">
</td>
</tr>
<tr>
<td style="width: 30px">
</td>
<td colspan="2">
<asp:ImageButton ID="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" OnClick="btnErrOK_Click" /></td>
</tr>
</tbody>
</table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="MPEErrMsg" runat="server" BackgroundCssClass="modalBackground"
Drag="True" DropShadow="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="lblCaption"
TargetControlID="btnExtender">
</ajaxToolkit:ModalPopupExtender>
<asp:Button ID="btnExtender" runat="server" Visible="False" />
</ContentTemplate>
</asp:UpdatePanel>
</th>
        </tr>
    </table>
</asp:Content>

