Imports System.Data
Imports System.Data.SqlClient
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure

Partial Class SPK_Close
    Inherits System.Web.UI.Page

    Dim sSql As String = ""
    Dim CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cmd As New SqlCommand("", conn)
    Dim cKon As New Koneksi
    Dim cFungsi As New ClassFunction
    Dim cProc As New ClassProcedure

    Private Sub showMessage(ByVal isi As String, ByVal judul As String)
        isiPesan.Text = isi
        captionPesan.Text = judul
        'btnExtenderValidasi.Visible = True
        'PanelValidasi.Visible = True
        'mpeMsg.Show()
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If


        ' jangan lupa cek hak akses
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSDO")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSDO") = sqlSearch
            Response.Redirect("~/Transaction/SPK_Close.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        btnCloseBPM.Attributes.Add("OnClick", "javascript:return confirm('Apa kamu yakin ingin Close data ini?');")


        If Not Page.IsPostBack Then
            'gvBarcodeList.DataSource = Nothing
            'gvBarcodeList.DataBind()
            tglAwal.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
            tglAkhir.Text = Format(CDate(DateTime.Now.ToShortDateString()), "dd/MM/yyyy")
            tglAwal2.Text = Format(CDate(DateTime.Now.ToShortDateString()), "dd/MM/yyyy")
            tglAkhir2.Text = Format(CDate(DateTime.Now.ToShortDateString()), "dd/MM/yyyy")
            btnFind_Click(sender, e)
            ddlFilter.SelectedIndex = 0
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim sQuery As String = ""
        Try
            Dim dat1 As Date = CDate(toDate(tglAwal.Text))
            Dim dat2 As Date = CDate(toDate(tglAkhir.Text))
        Catch ex As Exception
            showMessage("Filter tanggal salah!!", CompnyName & " - Warning") : Exit Sub
        End Try
        If CDate(toDate(tglAwal.Text)) > CDate(toDate(tglAkhir.Text)) Then
            showMessage("Tanggal Start Harus Lebih kecil dari pada Tanggal Akhir !!", CompnyName & " - Warning") : Exit Sub
        End If

        sQuery &= " AND " & ddlFilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%' AND dateclosed BETWEEN '" & CDate(toDate(tglAwal.Text)) & " 00:00:00' AND '" & CDate(toDate(tglAkhir.Text)) & " 23:59:59' "

        sQuery &= " AND m.reqstatus in ('SPKCLOSE','In') and userclosed <> '' "
        bindData(sQuery, gvClosedBPM)
    End Sub

    Private Sub bindData(ByVal moreQuery As String, ByVal gvTarget As GridView)
        sSql = "SELECT distinct m.reqoid spmoid, m.reqdate spmdate,noteclosed spmnoteclosed,dateclosed spmdateclosed,userclosed spmuserclosed," & _
                 " m.reqcode spmno, '' spmnote, m.reqstatus spmstatus,barcode machine,m.updtime " & _
                 " FROM ql_trnrequest m "

        sSql = sSql & " WHERE m.cmpcode LIKE '%" & CompnyCode & "%' " & _
        moreQuery & " " & _
        "ORDER BY m.updtime desc"
        FillGV(gvTarget, sSql, "ql_trnspmmst")
        gvTarget.Visible = True
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'btnExtenderValidasi.Visible = False
        'PanelValidasi.Visible = False
        'mpeMsg.Hide()
        Dim x As Integer = 123
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, False)
        'isiPesan.Text = "tes"
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlFilter.SelectedValue <> "spmno" Then
            tglAwal.Enabled = True
            tglAwal.CssClass = "inpText"
            tglAkhir.Enabled = True
            tglAkhir.CssClass = "inpText"
        Else
            tglAwal.Enabled = False
            tglAwal.CssClass = "inpTextDisabled"
            tglAkhir.Enabled = False
            tglAkhir.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        tglAwal.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        tglAkhir.Text = Format(Now, "dd/MM/yyyy")
        ddlFilter.SelectedIndex = 0
        txtFilter.Text = ""

        Dim sQuery As String = ""
        sQuery &= " AND " & ddlFilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%' AND dateclosed BETWEEN '" & CDate(toDate(tglAwal.Text)) & " 00:00:00' AND '" & CDate(toDate(tglAkhir.Text)) & " 23:59:59' "
        sQuery &= " AND m.reqstatus in ('SPKCLOSE','In') "
        bindData(sQuery, gvClosedBPM)
    End Sub

    Protected Sub imbSearchBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchBPM.Click
        Dim sQuery As String = ""

        sQuery &= " AND (m.reqcode like '%" & Tchar(txtBPMNo.Text) & "%' or m.barcode like '%" & Tchar(txtBPMNo.Text) & "%') "
        sQuery &= " AND m.reqstatus in ('SPK','OUT') "

        bindData(sQuery, gvBPM2)
    End Sub

    Protected Sub imbEraseBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseBPM.Click
        txtBPMNo.Text = "" : bpmoid.Text = ""
        gvBPM2.Visible = False
        gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBPM2.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")

            Dim objGrid As GridView = CType(e.Row.Cells(5).FindControl("gvMR"), GridView)
            sSql = "select m.spkno,case m.spkstatus when 'Approval' then 'In Approval' else m.spkstatus end as spkstatus " & _
                   "from ql_trnrequest mst " & _
                   "inner join QL_trnspkResultItem dtl on mst.reqoid=dtl.iodtloid " & _
                   "inner join QL_trnspkmainprod m on m.spkmainoid=dtl.spkmainoid " & _
                   "where mst.reqcode='" & e.Row.Cells(1).Text & "' "
            Dim ods As New SqlDataAdapter(sSql, conn)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)
            objGrid.DataSource = objTablee.DefaultView
            objGrid.DataBind()
        End If
    End Sub

    Protected Sub gvBPM2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBPM2.SelectedIndexChanged
        bpmoid.Text = gvBPM2.SelectedDataKey.Item("spmoid")
        txtBPMNo.Text = gvBPM2.SelectedDataKey.Item("spmno")
        gvBPM2.Visible = False
        gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBPM2.PageIndexChanging
        gvBPM2.PageIndex = e.NewPageIndex

        Dim sQuery As String = ""

        sQuery &= " AND (m.reqcode like '%" & Tchar(txtBPMNo.Text) & "%' or m.barcode like '%" & Tchar(txtBPMNo.Text) & "%') "
        sQuery &= " AND m.reqstatus in ('SPK','OUT') "
        bindData(sQuery, gvBPM2)
    End Sub

    Protected Sub btnCloseBPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseBPM.Click
        If bpmoid.Text.Trim = "" Or txtBPMNo.Text = "" Then
            showMessage("Tolong Isi Dahulu No Penerimaan !<br />", CompnyName & " - WARNING")
            Exit Sub
        End If

        If spmnoteclosed.Text.Trim = "" Then
            showMessage("Tolong Isi Dahulu Note Closed !<br />", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        cmd.Transaction = objTrans

        Try

            If rbtmutation.SelectedValue = "Closed" Then
                sSql = "update ql_trnrequest set reqstatus='SPKClose', dateclosed=CURRENT_TIMESTAMP, userclosed='" & Session("UserID") & "', noteclosed='" & Tchar(spmnoteclosed.Text) & "' where reqoid=" & bpmoid.Text
                cmd.CommandText = sSql
                cmd.ExecuteNonQuery()
            Else
                sSql = "update ql_trnrequest set reqstatus='In' , reqflag = 'In' , dateclosed=CURRENT_TIMESTAMP, userclosed='" & Session("UserID") & "', noteclosed='" & Tchar(spmnoteclosed.Text) & "' where reqoid=" & bpmoid.Text
                cmd.CommandText = sSql
                cmd.ExecuteNonQuery()
            End If

          
            'close semua SPK
            sSql = "update QL_trnspkmainprod set spkstatus = 'CLOSED',spkdateclosed=CURRENT_TIMESTAMP,spkuserclosed='" & Session("UserID") & "', spknoteclosed='" & Tchar(spmnoteclosed.Text) & "'  where spkmainoid in (select spkmainoid  from QL_trnspkResultItem where iodtloid = " & bpmoid.Text & ")"
            cmd.CommandText = sSql
            cmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            cmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR")
            Exit Sub
        End Try

        Response.Redirect("~\Transaction\SPK_Close.aspx?awal=true")
    End Sub

    'Protected Sub ddlFilter2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter2.SelectedIndexChanged
    '    If ddlFilter2.SelectedValue <> "spmno" Then
    '        txtBPMNo.Enabled = False
    '        txtBPMNo.CssClass = "inpTextDisabled"
    '    Else
    '        txtBPMNo.Enabled = True
    '        txtBPMNo.CssClass = "inpText"
    '    End If

    'End Sub

    Protected Sub gvClosedBPM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvClosedBPM.PageIndexChanging
        gvClosedBPM.PageIndex = e.NewPageIndex
        bindData("", gvClosedBPM)
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Redirect("~\Transaction\SPK_Close.aspx")
    End Sub

    Protected Sub gvClosedBPM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvClosedBPM_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvClosedBPM.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")

            Dim objGrid As GridView = CType(e.Row.Cells(4).FindControl("gvMR2"), GridView)
            sSql = "select m.spkno " & _
                   "from ql_trnrequest mst " & _
                   "inner join QL_trnspkResultItem dtl on mst.reqoid=dtl.iodtloid " & _
                   "inner join QL_trnspkmainprod m on m.spkmainoid=dtl.spkmainoid " & _
                   "where mst.reqcode='" & e.Row.Cells(0).Text & "' "
            Dim ods As New SqlDataAdapter(sSql, conn)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)
            objGrid.DataSource = objTablee.DefaultView
            objGrid.DataBind()
        End If
    End Sub

    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

End Class
