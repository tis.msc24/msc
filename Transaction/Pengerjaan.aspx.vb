'Prgmr : Shutup_M | LastUpdt:11.12.2012 - MM/dd/YYYY
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction

Partial Class Transaction_Pengerjaan
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Dim CProc As New ClassProcedure
    'Dim report As New ReportDocument
    Dim folderReport As String = "~/Report/"
#End Region

#Region "Procedure"

    Sub initTypeJob()
        Dim kCbg As String = ""
        If Session("UserID").ToString.ToUpper <> "ADMIN" Then
            kCbg &= "And g2.gencode='" & Session("branch_id").ToString & "'"
        End If
        sSql = "Select gencode ,g2.gendesc from ql_mstgen g2 where cmpcode = 'MSC' and gengroup = 'CABANG' " & kCbg & " ORDER BY g2.gencode"
        FillDDL(DdlCabang, sSql)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            ImageButton3.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            ImageButton3.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 2 Then 'Warning
            ImageButton3.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        WARNING.Text = strCaption : lblValidasi.Text = sMessage
        CProc.SetModalPopUpExtender(btnExtender, PanelErrMsg, MPEErrMsg, True)
    End Sub

    Public Sub GenerateGenID()
        txtworkOid.Text = GenerateID("QL_TRNSVCWORK", CompnyCode)
    End Sub

    Sub BindData(ByVal sWhere As String) 'GV List Awal
        sWhere &= " and qla.reqstatus <> 'Paid' "

        If Session("UserID").ToString.ToUpper <> "ADMIN" Then
            sWhere &= "And l1.USERID = '" & Session("UserID") & "'"
        End If
        sSql = "SELECT QLC.WORKOID, QLA.BARCODE, QLB.SSTARTTIME, QLA.REQITEMNAME,QLA2.GENDESC REQITEMTYPE, QLA3.GENDESC REQITEMBRAND, QLC.WORKSTARTTIME,(ISNULL(CONVERT(VARCHAR(15),QLC.WORKENDTIME,103),'-'))+ ' ' +(ISNULL(CONVERT(VARCHAR(15),QLC.WORKENDTIME,108),'-')) WORKENDTIME,QLC.STATUS,t2.PERSONNAME firstteknisi, isnull(t3.PERSONNAME,'-') lastteknisi FROM QL_TRNREQUEST QLA INNER JOIN QL_TRNSERVICES QLB ON QLB.MSTREQOID = QLA.REQOID INNER JOIN QL_MSTGEN QLA2 ON QLA2.genoid = QLA.REQITEMTYPE INNER JOIN QL_MSTGEN QLA3 ON QLA3.genoid = QLA.REQITEMBRAND INNER JOIN QL_TRNSVCWORK QLC ON QLC.REQOID = QLB.MSTREQOID INNER JOIN QL_MSTPROF l1 on l1.USERID = QLC.CREATEUSER INNER JOIN QL_MSTPERSON t2 on t2.PERSONNAME = l1.USERNAME LEFT JOIN QL_MSTPERSON t3 on QLC.UPDUSER = t3.PERSONNAME INNER JOIN QL_MSTPERSON t on QLB.SPERSON = t.PERSONOID INNER JOIN QL_MSTPROF l on l.USERNAME = t.PERSONNAME WHERE QLC.CMPCODE = '" & CompnyCode & "'" & sWhere & " ORDER BY QLC.WORKOID DESC"
        Dim xTableItem1 As DataTable = cKoneksi.ambiltabel(sSql, "ChkWorkOut")
        GVListWork.DataSource = xTableItem1
        GVListWork.DataBind() : GVListWork.SelectedIndex = -1
    End Sub

    Sub BindDataInfo(ByVal sWhere As String) 'Binding Nota Beli
        sSql = "SELECT QLR.branch_code,QLK.MSTREQOID, QLR.REQOID, QLR.REQCODE, QLR.BARCODE, QLP1.CUSTNAME REQCUSTOID, QLR.REQITEMNAME, QLP2.GENDESC REQITEMTYPE, QLP3.GENDESC REQITEMBRAND, QLK.SSTATUS, QLK.SFLAG, convert(char(10),QLR.REQDATE,103) tgl,isnull(t.PERSONNAME,'') teknisi FROM QL_TRNREQUEST QLR INNER JOIN QL_MSTGEN QLP2 ON QLP2.genoid = QLR.REQITEMTYPE INNER JOIN QL_MSTGEN QLP3 ON QLP3.genoid = QLR.REQITEMBRAND  INNER JOIN QL_TRNSERVICES QLK ON QLK.MSTREQOID = QLR.REQOID INNER JOIN QL_MSTCUST QLP1 ON QLP1.CUSTOID = QLR.REQCUSTOID left join ql_mstperson t on QLK.sperson = t.personoid WHERE QLK.cmpcode='" & CompnyName & "'" & sWhere & " AND QLR.REQSTATUS = 'Ready' AND QLK.SFLAG = 'In' and qlr.branch_code = '" & DdlCabang.SelectedValue & "' ORDER BY QLK.MSTREQOID "
        Dim xTableItem2 As DataTable = cKoneksi.ambiltabel(sSql, "Info")
        GVListInfo.DataSource = xTableItem2
        GVListInfo.DataBind() : GVListInfo.SelectedIndex = -1
    End Sub

    Sub ClearInfo()
        txtNo.Text = "" : txtTglTarget.Text = ""
        txtWaktuTarget.Text = "" : tglMulai.Text = ""
        wktMulai.Text = "" : tglSelesai.Text = ""
        wktSelesai.Text = "" : txtJenisBrg.Text = ""
        txtMerk.Text = "" : txtNamaBrg.Text = ""
        txtOid.Text = "" : txtStatus.Text = ""
        txtworkOid.Text = "" : teknisi.Text = "" : lblno.Text = ""
    End Sub

#End Region

#Region "Function"

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = CompnyName & " - Pengerjaan"
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            Response.Redirect("Pengerjaan.aspx")
        End If
        Session("idPage") = Request.QueryString("idPage")
        btnStart.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin mengubah status menjadi -Start- ?');")
        btnFinish.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin mengubah status menjadi -Finish- ?');")

        If Not IsPostBack Then
            initTypeJob()
            txti_u.Text = "New" : Dim sWhere = "" : BindData(sWhere)
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                txtPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
                txtPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
                txtPeriod1Info.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
                txtPeriod2Info.Text = Format(GetServerTime(), "dd/MM/yyyy")
                updUser.Text = Session("UserID")
                updTime.Text = Now
                TabContainer1.ActiveTabIndex = 0
            ElseIf Session("idPage") <> Nothing And Session("idPage") <> "" Then
                txtPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
                txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
                txtPeriod1Info.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
                txtPeriod2Info.Text = Format(GetServerTime(), "dd/MM/yyyy")
                txti_u.Text = "Update"
                FillTextBox(Session("idPage"))
                btnStart.Visible = False : btnFinish.Visible = True
                btnSearch.Visible = False : btnErase.Visible = False
                If txtStatus.Text = "Finish" Then
                    btnStart.Visible = False : btnFinish.Visible = False
                    btnSearch.Visible = False : btnErase.Visible = False
                End If
                If txtStatus.Text = "Failed" Then
                    btnStart.Visible = False : btnFinish.Visible = False
                    btnSearch.Visible = False : btnErase.Visible = False
                End If
                TabContainer1.ActiveTabIndex = 1
            End If
        End If
    End Sub
#End Region

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If cbPeriod.Checked Then
            Try

                Dim f As DateTime : Dim errMessage As String = ""

                If txtPeriod1.Text = "" Then
                    errMessage &= "- [Tolong isi tanggal awal !] ! <br>"
                End If

                If txtPeriod2.Text = "" Then
                    errMessage &= "- [Tolong isi tanggal akhir !] ! <br>"
                End If

                If Not DateTime.TryParseExact(txtPeriod1.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, f) Then
                    errMessage &= "- [Format tanggal salah ! Gunakan format dd/MM/yyyy - MAX 31:12:2029] ! <br>"
                End If

                If Not DateTime.TryParseExact(txtPeriod2.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, f) Then
                    errMessage &= "- [Format tanggal salah ! Gunakan format dd/MM/yyyy - MAX 31:12:2029] ! <br>"
                End If

                If errMessage <> "" Then
                    showMessage(errMessage, 2)
                    Exit Sub
                End If

                txtPeriod1.Enabled = True
                txtPeriod2.Enabled = True
                Dim date1 As Date = toDate(txtPeriod1.Text)
                Dim date2 As Date = toDate(txtPeriod2.Text)
                If txtPeriod1.Text = "" And txtPeriod2.Text = "" Then
                    showMessage("Tolong isi periode !", 2)
                End If
                If date1 < toDate("01/01/1900") Then
                    showMessage("Tanggal awal salah !", 2) : Exit Sub
                End If
                If date2 < toDate("01/01/1900") Then
                    showMessage("Tanggal akhir salah !", 2) : Exit Sub
                End If
                If date1 > date2 Then
                    showMessage("Period 1 harus lebih kecil dari Period 2 !", 2)
                    txtPeriod1.Text = ""
                    txtPeriod2.Text = ""
                    Exit Sub
                End If
                sWhere &= " AND CONVERT(CHAR(10),QLB.SSTARTTIME,101) BETWEEN '" & toDate(txtPeriod1.Text) & "' AND '" & toDate(txtPeriod2.Text) & "'"
            Catch ex As Exception
                showMessage("Tolong isi tanggal awal dan akhir !", 2)
            End Try
        End If
        If cbStatus.Checked Then
            sWhere &= " AND QLC.STATUS LIKE '%" & ddlStatus.SelectedValue & "%'"
        End If
        BindData(sWhere)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        txtFilter.Text = "" : ddlFilter.SelectedIndex = 0
        cbPeriod.Checked = False : txtPeriod1.Text = ""
        txtPeriod2.Text = "" : Dim sWhere = ""
        BindData(sWhere)
    End Sub

    Sub FillTextInfo(ByVal Code As String)
        ' No transaksi for pilihan master fill text box
        sSql = "SELECT QLR.branch_code,QLK.MSTREQOID, QLK.SSTARTTIME as tanggal, QLK.SSTARTTIME as waktu, QLR.REQOID,qlr.barcode, QLR.REQCODE, QLR.REQITEMNAME, QLA67.CUSTNAME, QLP2.GENDESC REQITEMTYPE, QLP3.GENDESC REQITEMBRAND, QLK.UPDUSER, QLK.UPDTIME, QLK.SSTATUS ,isnull(t.PERSONNAME,'') teknisi FROM QL_TRNREQUEST QLR INNER JOIN QL_MSTGEN QLP2 ON QLP2.genoid = QLR.REQITEMTYPE INNER JOIN QL_MSTGEN QLP3 ON QLP3.genoid = QLR.REQITEMBRAND INNER JOIN QL_TRNSERVICES QLK ON QLK.MSTREQOID = QLR.REQOID INNER JOIN QL_MSTCUST QLA67 ON QLA67.CUSTOID = QLR.REQCUSTOID left join ql_mstperson t on qlk.sperson = t.personoid WHERE QLR.cmpcode='" & CompnyCode & "' AND QLK.MSTREQOID=" & Tchar(Code) & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        'Dim Tanggal As String = "":Dim Waktu As String = ""
        If xReader.HasRows Then
            While xReader.Read
                CodeCb.Text = xReader("branch_code").ToString.Trim
                txtOid.Text = xReader("MSTREQOID")
                txtNo.Text = xReader("REQCODE").ToString.Trim
                txtTglTarget.Text = Format(xReader("tanggal"), "dd/MM/yyyy")
                txtWaktuTarget.Text = Format(xReader("waktu"), "HH:mm:ss")
                txtJenisBrg.Text = xReader("REQITEMTYPE").ToString.Trim
                txtMerk.Text = xReader("REQITEMBRAND").ToString.Trim
                txtNamaCust.Text = xReader("CUSTNAME").ToString.Trim
                txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                txtStatus.Text = xReader("SSTATUS").ToString.Trim
                teknisi.Text = xReader("teknisi").ToString.Trim
                lblno.Text = xReader("barcode").ToString.Trim
                tglMulai.Text = "" : wktMulai.Text = ""
                tglSelesai.Text = "" : wktSelesai.Text = ""
                updUser.Text = xReader("UPDUSER").ToString.Trim
                updTime.Text = Format(xReader("UPDTIME"), "dd/MM/yyyy HH:mm:ss")
            End While
        End If
    End Sub

    Sub FillTextBox(ByVal idPage As Integer)
        sSql = "SELECT qla.branch_code,QLC.WORKOID, QLC.REQOID, QLA.BARCODE, QLA.REQCODE, QLA67.CUSTNAME, QLB.SSTATUS, QLB.SSTARTTIME as tanggal, QLB.SSTARTTIME as waktu, QLA.REQITEMNAME, QLA2.GENDESC REQITEMTYPE, QLA3.GENDESC REQITEMBRAND, QLC.WORKSTARTTIME as tanggal1, QLC.WORKSTARTTIME as waktu1, ISNULL(CONVERT(VARCHAR(15),QLC.WORKENDTIME,103),'-') as tanggal2, ISNULL(CONVERT(VARCHAR(15),QLC.WORKENDTIME,108),'-') as waktu2, QLC.UPDUSER, QLC. UPDTIME ,isnull(t.PERSONNAME,'') teknisi FROM QL_TRNREQUEST QLA INNER JOIN QL_TRNSERVICES QLB ON QLB.MSTREQOID = QLA.REQOID left join ql_mstperson t on QLa.reqperson = t.personoid INNER JOIN QL_MSTGEN QLA2 ON QLA2.genoid = QLA.REQITEMTYPE INNER JOIN QL_MSTGEN QLA3 ON QLA3.genoid = QLA.REQITEMBRAND INNER JOIN QL_TRNSVCWORK QLC ON  QLC.REQOID = QLB.MSTREQOID INNER JOIN QL_MSTCUST QLA67 ON QLA67.CUSTOID = QLA.REQCUSTOID WHERE QLC.cmpcode='" & CompnyCode & "' AND QLC.WORKOID=" & idPage & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                txtworkOid.Text = idPage
                txtOid.Text = xReader("REQOID")
                CodeCb.Text = txtNo.Text = xReader("branch_code").ToString.Trim
                txtNo.Text = xReader("REQCODE").ToString.Trim
                txtTglTarget.Text = Format(xReader("tanggal"), "dd/MM/yyyy")
                txtWaktuTarget.Text = Format(xReader("waktu"), "HH:mm:ss")
                txtJenisBrg.Text = xReader("REQITEMTYPE").ToString.Trim
                lblno.Text = xReader("barcode").ToString.Trim
                teknisi.Text = xReader("teknisi").ToString.Trim
                txtMerk.Text = xReader("REQITEMBRAND").ToString.Trim
                txtNamaCust.Text = xReader("CUSTNAME").ToString.Trim
                txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                txtStatus.Text = xReader("SSTATUS").ToString.Trim
                tglMulai.Text = Format(xReader("tanggal1"), "dd/MM/yyyy")
                wktMulai.Text = Format(xReader("waktu1"), "HH:mm:ss")
                tglSelesai.Text = xReader("tanggal2").ToString.Trim
                wktSelesai.Text = xReader("waktu2").ToString.Trim
                updUser.Text = xReader("UPDUSER").ToString.Trim
                updTime.Text = Format(xReader("UPDTIME"), "dd/MM/yyyy HH:mm:ss")
                DdlCabang.SelectedValue = xReader("branch_code").ToString.Trim
            End While
        End If
        xReader.Close() : conn.Close()

        lblno.Enabled = False : lblno.CssClass = "inpTextDisabled"
        btnStart.Visible = False : btnFinish.Visible = True
        btnSearch.Visible = False : btnErase.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        PanelInfo.Visible = True
        btnHiddenInfo.Visible = True : Dim sWhere As String = ""
        sWhere = " And sperson = (select personoid from QL_MSTPERSON a where a.PERSONOID=qlk.SPERSON)"
        BindDataInfo(sWhere) : MPEInfo.Show()
    End Sub

    Protected Sub GVListInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVListInfo.PageIndexChanging
        GVListInfo.PageIndex = e.NewPageIndex
        Dim sWhere As String = ""
        sWhere = ""
        BindDataInfo(sWhere)
        MPEInfo.Show()
    End Sub

    Protected Sub GVListInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVListInfo.SelectedIndexChanged
        Dim sCode As String = "" : btnHiddenInfo.Visible = False
        PanelInfo.Visible = False : MPEInfo.Hide()
        sCode = GVListInfo.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextInfo(sCode)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        TabContainer1.ActiveTabIndex = 0
        Response.Redirect("~\Transaction\Pengerjaan.aspx?page=true")
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErase.Click
        ClearInfo()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub

    Protected Sub printGV_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ' Dim temp As String = sender.commandargument.ToString
        '  PrintReport(temp.Remove(temp.IndexOf(";")), temp.Substring(temp.IndexOf(";") + 1))
    End Sub

    Protected Sub btnFindInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
       
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilterInfo.SelectedValue & " LIKE '%" & Tchar(txtFilterInfo.Text) & "%' and sperson = (select personoid from QL_MSTPERSON a inner join QL_MSTPROF b on a.PERSONNIP = b.USERID where b.USERID =  '" & Session("UserID") & "') "
        If cbPeriodInfo.Checked Then
            Try
                Dim date1 As Date = toDate(txtPeriod1Info.Text)
                Dim date2 As Date = toDate(txtPeriod2Info.Text)
                If txtPeriod1Info.Text = "" And txtPeriod2Info.Text = "" Then
                    showMessage("Tolong isi periode !", 2)
                End If
                If date1 < toDate("01/01/1900") Then
                    showMessage("Tolong isi tanggal awal !", 2) : Exit Sub
                End If
                If date2 < toDate("01/01/1900") Then
                    showMessage("Tolong isi tanggal akhir !", 2) : Exit Sub
                End If
                If date1 >= date2 Then
                    showMessage("Period 1 harus lebih kecil dari Period 2 !", 2)
                    txtPeriod1Info.Text = ""
                    txtPeriod2Info.Text = ""
                    Exit Sub
                End If
                sWhere &= " AND QLR.REQDATE BETWEEN '" & toDate(txtPeriod1Info.Text) & "' AND '" & toDate(txtPeriod2Info.Text) & "'"
            Catch ex As Exception
                showMessage("Tolong isi tanggal awal dan akhir !", 2)
            End Try
        End If
        BindDataInfo(sWhere)
        MPEInfo.Show()
    End Sub

    Protected Sub btnViewAllInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFilterInfo.Text = ""
        ddlFilterInfo.SelectedIndex = 0
        cbPeriodInfo.Checked = False
        txtPeriod1Info.Text = ""
        txtPeriod2Info.Text = ""
        Dim sWhere As String = ""
        'Dim sWhere = " and sperson = (select personoid from QL_MSTPERSON a inner join QL_MSTPROF b on a.personname = b.USERID where b.USERID =  '" & Session("UserID") & "') "
        BindDataInfo(sWhere)
        MPEInfo.Show()
    End Sub

    Protected Sub btnStart_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim errMessage As String = ""

        If txtOid.Text = "" Then
            errMessage &= "Tolong Pilih NO Tanda Terima !<BR>"
        End If

        If errMessage <> "" Then
            showMessage(errMessage, 2)
            Exit Sub
        End If

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            txtworkOid.Text = GenerateID("QL_TRNSVCWORK", CompnyCode)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
            ' FormatDateTime(Now(), "dd/MM/yyyy HH:mm:ss")
            txtStatus.Text = "Start"

            'mode insert
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                'Dim wktjam As String = ""
                sSql = "INSERT into QL_trnsvcwork (CMPCODE, BRANCH_CODE, WORKOID, REQOID, WORKSTARTTIME, WORKENDTIME, STATUS, CREATEUSER, CREATETIME, UPDUSER, UPDTIME) " & _
                "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & txtworkOid.Text & "," & txtOid.Text & ",current_timestamp,NULL,'" & txtStatus.Text & "','" & updUser.Text & "',current_timestamp,NULL,current_timestamp)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstoid set lastoid=" & txtworkOid.Text & " where tablename = 'QL_TRNSVCWORK' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update Trn Request dan trn Services, ubah status request menjadi Start
                sSql = "UPDATE QL_trnservices SET SSTATUS='" & txtStatus.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' and MSTREQOID=" & txtOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnrequest SET REQSTATUS='" & txtStatus.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                objTrans.Commit() : conn.Close()
            End If
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session.Remove("idPage")
        Response.Redirect("~\transaction\Pengerjaan.aspx?awal=true")
    End Sub

    Sub reasontelat()
        PanelSpart.Visible = True : btnHiddenSpart.Visible = True
        MPESpart.Show() : Dim sWhere As String = ""
        BindDataSpart()
    End Sub

    Sub BindDataSpart()
        sSql = "SELECT genoid, gendesc FROM ql_mstgen Where gengroup = 'REASON'"
        Dim xTableItem4 As DataTable = cKoneksi.ambiltabel(sSql, "reason")
        GVSpart.DataSource = xTableItem4
        GVSpart.DataBind() : GVSpart.SelectedIndex = -1
    End Sub

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If CDate(toDate(txtTglTarget.Text)) < Format(GetServerTime(), "MM/dd/yyyy") Then
            reasontelat()
            Exit Sub
        Else
            If CDate(toDate(txtTglTarget.Text)) = Format(GetServerTime(), "MM/dd/yyyy") Then
                If txtWaktuTarget.Text > Format(GetServerTime(), "HH:mm:ss") Then
                Else
                    reasontelat()
                    Exit Sub
                End If
            End If
        End If
        finish(0)
    End Sub

    Sub finish(ByVal scode As Integer)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            txtStatus.Text = "Finish"
            If txti_u.Text = "Update" Then
                'mode update
                sSql = "UPDATE QL_TRNSVCWORK SET WORKOID=" & txtworkOid.Text & ", REQOID=" & txtOid.Text & ", WORKENDTIME=" & "current_timestamp" & ", STATUS='" & txtStatus.Text.Trim & "', UPDUSER='" & Session("UserID") & "', UPDTIME=" & "current_timestamp" & ", reason = " & scode & " WHERE cmpcode='" & CompnyCode & "' and WORKOID=" & txtworkOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '"WORKENDTIME='" & toDate(DateTime.Now) & "', " & _

                'Update Trn Request dan trn Services, ubah status request menjadi Finish
                sSql = "UPDATE QL_trnservices SET SSTATUS='" & txtStatus.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' and MSTREQOID=" & txtOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnrequest SET REQSTATUS='" & txtStatus.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' and REQOID=" & txtOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If scode = 0 Then
                    sSql = "SELECT garansi FROM QL_TRNREQUEST WHERE REQOID = " & txtOid.Text & ""
                    xCmd.CommandText = sSql
                    Dim garansi As String = xCmd.ExecuteScalar
                    If garansi = "Garansi" Then
                    Else
                        sSql = "Update ql_mstperson set point = POINT +1 where cmpcode = '" & CompnyCode & "' and personnip = '" & Session("UserID") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If
            End If

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session.Remove("idPage")
        Response.Redirect("~\transaction\Pengerjaan.aspx?awal=true")
    End Sub

    Protected Sub GVListWork_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVListWork.PageIndexChanging
        GVListWork.PageIndex = e.NewPageIndex
        Dim sWhere As String = ""
        BindData(sWhere)
    End Sub

    Protected Sub GVListWork_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVListWork.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy HH:mm:ss")
            e.Row.Cells(5).Text = Format(CDate(e.Row.Cells(5).Text), "dd/MM/yyyy HH:mm:ss")
            'e.Row.Cells(6).Text = Format(CDate(e.Row.Cells(6).Text), "dd/MM/yyyy HH:mm:ss").ToString
        End If
    End Sub

    Protected Sub lbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHiddenInfo.Visible = False
        PanelInfo.Visible = False
    End Sub

    Protected Sub lblno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblno.TextChanged
        If Not lblno.Text = "" Then
            sSql = "SELECT QLK.MSTREQOID, QLK.SSTARTTIME as tanggal, QLK.SSTARTTIME as waktu, QLR.REQOID,qlr.barcode, QLR.REQCODE, QLR.REQITEMNAME, QLA67.CUSTNAME, QLP2.GENDESC REQITEMTYPE, QLP3.GENDESC REQITEMBRAND, QLK.UPDUSER, QLK.UPDTIME, QLK.SSTATUS ,isnull(t.PERSONNAME,'') teknisi FROM QL_TRNREQUEST QLR INNER JOIN QL_MSTGEN QLP2 ON QLP2.genoid = QLR.REQITEMTYPE INNER JOIN QL_MSTGEN QLP3 ON QLP3.genoid = QLR.REQITEMBRAND INNER JOIN QL_TRNSERVICES QLK ON QLK.MSTREQOID = QLR.REQOID INNER JOIN QL_MSTCUST QLA67 ON QLA67.CUSTOID = QLR.REQCUSTOID left join ql_mstperson t on qlk.sperson = t.personoid WHERE QLR.cmpcode='" & CompnyCode & "' AND QLr.barcode='" & Tchar(lblno.Text) & "' AND QLR.REQSTATUS = 'Ready' AND QLK.SFLAG = 'In' and sperson = (select personoid from QL_MSTPERSON a inner join QL_MSTPROF b on a.PERSONNIP = b.USERID where b.USERID =  '" & Session("UserID") & "') "
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xReader = xCmd.ExecuteReader
            'Dim Tanggal As String = ""
            'Dim Waktu As String = ""
            If xReader.HasRows Then
                While xReader.Read
                    txtOid.Text = xReader("MSTREQOID")
                    txtNo.Text = xReader("REQCODE").ToString.Trim
                    txtTglTarget.Text = Format(xReader("tanggal"), "dd/MM/yyyy")
                    txtWaktuTarget.Text = Format(xReader("waktu"), "HH:mm:ss")
                    txtJenisBrg.Text = xReader("REQITEMTYPE").ToString.Trim
                    txtMerk.Text = xReader("REQITEMBRAND").ToString.Trim
                    txtNamaCust.Text = xReader("CUSTNAME").ToString.Trim
                    txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                    txtStatus.Text = xReader("SSTATUS").ToString.Trim
                    teknisi.Text = xReader("teknisi").ToString.Trim
                    lblno.Text = xReader("barcode").ToString.Trim
                    tglMulai.Text = ""
                    wktMulai.Text = ""
                    tglSelesai.Text = ""
                    wktSelesai.Text = ""
                    updUser.Text = xReader("UPDUSER").ToString.Trim
                    updTime.Text = Format(xReader("UPDTIME"), "dd/MM/yyyy HH:mm:ss")
                End While
            Else
                showMessage("Tidak ada No. Barcode yang ditemukan", 2) : Exit Sub
            End If
        End If
    End Sub

    Protected Sub GVSpart_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sCode As String = ""
        btnHiddenSpart.Visible = False
        PanelSpart.Visible = False
        MPESpart.Hide()
        sCode = GVSpart.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        finish(sCode)
    End Sub

    Protected Sub lbCloseSpart_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHiddenSpart.Visible = False
        PanelSpart.Visible = False
    End Sub
End Class
