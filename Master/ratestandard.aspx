﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ratestandard.aspx.vb" Inherits="Master_ratestandard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Rate Standard" CssClass="Title" Font-Size="Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px"></td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Rate Standard :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:CheckBox id="cbPeriod" runat="server" Text="Period"></asp:CheckBox></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddlmonth2" runat="server" Width="85px" CssClass="inpText"></asp:DropDownList>&nbsp;- <asp:DropDownList id="ddlyear2" runat="server" Width="85px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbCurrency" runat="server" Text="Currency"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLCurrency" runat="server" Width="85px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" Width="85px" CssClass="inpText" Visible="False">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>ACTIVE</asp:ListItem>
                                                            <asp:ListItem>INACTIVE</asp:ListItem>
                                                        </asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" Width="100%" Font-Size="X-Small" Font-Bold="False" ForeColor="#333333" AllowPaging="True" PageSize="8" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="rate2oid" DataNavigateUrlFormatString="~/Master/ratestandard.aspx?oid={0}" DataTextField="nmr" HeaderText="No" SortExpression="nmr">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="rate2date" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="currencycode" HeaderText="Currency" SortExpression="currencycode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="month" HeaderText="Month" SortExpression="month">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rate2year" HeaderText="Year" SortExpression="rate2year">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rate2date" HeaderText="Create Date" SortExpression="rate2date" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="updtime" HeaderText="Time" SortExpression="updtime">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rate2idrvalue" HeaderText="Rate to IDR" SortExpression="rate2idrvalue">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rate2usdvalue" HeaderText="Rate to USD" SortExpression="rate2usdvalue">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rate2note" HeaderText="Note" SortExpression="rate2note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag" Visible="False">
<HeaderStyle HorizontalAlign="Center" BackColor="#C6C4C4"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="rate2oid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server">Currency</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" Width="85px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server">Create Date</asp:Label> <asp:Label id="Label10" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="createdate" runat="server" Width="60px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label8" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server">Period</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlmonth" runat="server" Width="85px" CssClass="inpText"></asp:DropDownList>&nbsp;- <asp:DropDownList id="ddlyear" runat="server" Width="85px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server">Rate to IDR</asp:Label> <asp:Label id="Label13" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="rate2res1" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:LinkButton id="btnAvg" runat="server">[ Get Daily Average ]</asp:LinkButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server">Rate to USD</asp:Label> <asp:Label id="Label11" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="rate2res2" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server">Note</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="ratenote" runat="server" Width="200px" CssClass="inpText" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Visible="False">Status</asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" Width="85px" CssClass="inpText" Visible="False">
                                                        <asp:ListItem>ACTIVE</asp:ListItem>
                                                        <asp:ListItem>INACTIVE</asp:ListItem>
                                                    </asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:TextBox id="rate2idrvalue" runat="server" Width="25px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:TextBox id="rate2usdvalue" runat="server" Width="25px" CssClass="inpText" Visible="False"></asp:TextBox> </TD></TR><TR><TD class="Label" align=left colSpan=3><ajaxToolkit:CalendarExtender id="ceDate" runat="server" TargetControlID="createdate" PopupButtonID="imbDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="createdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeValue" runat="server" TargetControlID="rate2idrvalue" MaskType="Number" Mask="99,999,999.99" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeValueUSD" runat="server" TargetControlID="rate2usdvalue" MaskType="Number" Mask="99,999,999.999999" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbValueIDR" runat="server" TargetControlID="rate2res1" ValidChars="1234567890,.">
                                                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbValueUSD" runat="server" TargetControlID="rate2res2" ValidChars="1234567890,.">
                                                    </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" Visible="False" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Imageloading" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Rate Standard :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></td>
                            <td style="TEXT-ALIGN: left" class="Label">
                                <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px; TEXT-ALIGN: center" colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" colspan="2">&nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
