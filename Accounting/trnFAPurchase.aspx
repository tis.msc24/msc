<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnFAPurchase.aspx.vb" Inherits="Accounting_trnFAPurchase" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Fixed Asset Purchase"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                    Width="100%"
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView2" runat="server" __designer:wfdid="w354" ActiveViewIndex="0"><asp:View id="View3" runat="server" __designer:wfdid="w355"><asp:Panel id="Panel1" runat="server" __designer:wfdid="w356" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 60px" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Date" __designer:wfdid="w357" Visible="False" AutoPostBack="True" Checked="True" OnCheckedChanged="cbPeriode_CheckedChanged"></asp:CheckBox> <asp:Label id="Label36" runat="server" Text="Date" __designer:wfdid="w358"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w359"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w360"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w361"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w362"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w363"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 60px" align=left><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w364"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLfilter" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w365"><asp:ListItem Value="trnbelifano">No</asp:ListItem>
<asp:ListItem Value="Suppname">Supplier</asp:ListItem>
<asp:ListItem Value="m.trnbelifamstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilter" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w366"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 60px" align=left><asp:Label id="Label4" runat="server" Text="Status" __designer:wfdid="w367"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLstatus" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w368"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w369"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w370"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 60px" align=right><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w371" TargetControlID="txtPeriode1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w372" TargetControlID="txtPeriode2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" __designer:wfdid="w373" MaskType="Date" Mask="99/99/9999" CultureName="en-US" TargetControlID="txtPeriode1"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" __designer:wfdid="w374" MaskType="Date" Mask="99/99/9999" CultureName="en-US" TargetControlID="txtPeriode2"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=4><asp:GridView id="GVFixedAsset" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w350" OnSelectedIndexChanged="GVFixedAsset_SelectedIndexChanged" OnRowDataBound="GVFixedAsset_RowDataBound" OnPageIndexChanging="GVFixedAsset_PageIndexChanging" GridLines="None" AllowPaging="True" DataKeyNames="cmpcode,trnbelifamstoid" AutoGenerateColumns="False" CellPadding="4" AllowSorting="True" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelifano" HeaderText="FA. No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cmpcode" HeaderText="Compny Code" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadate" HeaderText="Date" SortExpression="trnbelifadate">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Pay Term">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifaamtnetto" DataFormatString="{0:#,##0.00}" HeaderText="Netto Amt">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifamstnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifamststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" CssClass="Important" Text="Click button Find or View All to view data" __designer:wfdid="w376"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel></asp:View>&nbsp; </asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">
                                <asp:Image ID="Image43" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                List of Fixed Asset Purchase :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 100%" vAlign=top colSpan=3 rowSpan=3><asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w150" ActiveViewIndex="0"><asp:View id="View1" runat="server" __designer:wfdid="w151"><DIV style="HEIGHT: 100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD id="TD7" align=left><asp:Label id="lblNo" runat="server" Text="Draft No." __designer:wfdid="w152" Visible="False"></asp:Label></TD><TD id="TD2" align=left Visible="false"><asp:Label id="fixmstoid" runat="server" __designer:wfdid="w153" Visible="False"></asp:Label><asp:Label id="CutofDate" runat="server" __designer:wfdid="w154" Visible="False"></asp:Label></TD><TD id="TD3" align=left Visible="true"><asp:Label id="Rateoid" runat="server" __designer:wfdid="w155" Visible="False"></asp:Label><asp:Label id="Rate2oid" runat="server" __designer:wfdid="w156" Visible="False"></asp:Label></TD><TD id="TD1" align=left Visible="true"></TD></TR><TR><TD align=left><asp:Label id="Label38" runat="server" Text="Cabang" __designer:wfdid="w152"></asp:Label></TD><TD align=left Visible="false"><asp:DropDownList id="DDLBranch" runat="server" CssClass="inpText" __designer:wfdid="w2" AutoPostBack="True" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged"></asp:DropDownList></TD><TD align=left Visible="true"></TD><TD align=left Visible="true"></TD></TR><TR><TD align=left><asp:Label id="Label27" runat="server" Text="No." __designer:wfdid="w157"></asp:Label></TD><TD align=left><asp:TextBox id="FixAssetNo" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w158" Enabled="False" MaxLength="20"></asp:TextBox> <asp:Label id="Label18" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w159"></asp:Label></TD><TD align=left><asp:Label id="Label5" runat="server" Text="Asset Date" __designer:wfdid="w160"></asp:Label></TD><TD align=left><asp:TextBox id="fixDate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w161" AutoPostBack="True" OnTextChanged="fixDate_TextChanged"></asp:TextBox> <asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w162"></asp:ImageButton> <asp:Label id="Label14" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w163"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label6" runat="server" Text="Supplier" __designer:wfdid="w164"></asp:Label></TD><TD align=left><asp:TextBox id="Supp" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w165" Enabled="False" MaxLength="200"></asp:TextBox> <asp:ImageButton id="BtnCariSupp" onclick="BtnCariSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w166"></asp:ImageButton> <asp:ImageButton id="btnHapusSupp" onclick="btnHapusSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w167"></asp:ImageButton> <asp:Label id="lblSuppOid" runat="server" __designer:wfdid="w168" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label7" runat="server" Text="Ref No" __designer:wfdid="w169"></asp:Label> </TD><TD align=left><asp:TextBox id="RefNo" runat="server" CssClass="inpText" __designer:wfdid="w170" AutoPostBack="True" MaxLength="16"></asp:TextBox> <asp:Label id="Label2x" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w171"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label39" runat="server" Width="63px" ForeColor="DarkBlue" Text="Currrency" __designer:wfdid="w172"></asp:Label></TD><TD align=left><asp:DropDownList id="CurrDDL" runat="server" Width="80px" CssClass="inpTextDisabled" __designer:wfdid="w173" AutoPostBack="True" OnSelectedIndexChanged="CurrDDL_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD><TD align=left><asp:Label id="Label20" runat="server" Text="Pay Term" __designer:wfdid="w174"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLpayterm" runat="server" CssClass="inpText" __designer:wfdid="w175" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD id="TD15" align=left runat="server" Visible="false"><asp:Label id="Label40" runat="server" Width="104px" Text="Daily Rate to IDR" __designer:wfdid="w176"></asp:Label></TD><TD id="TD11" align=left runat="server" Visible="false"><asp:TextBox id="RateToIDR" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w177" Enabled="False">0</asp:TextBox></TD><TD id="TD9" align=left runat="server" Visible="false"><asp:Label id="Label41" runat="server" Text="Daily Rate To USD" __designer:wfdid="w178"></asp:Label></TD><TD id="TD13" align=left runat="server" Visible="false"><asp:TextBox id="RateToUSD" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w179" Enabled="False">0</asp:TextBox></TD></TR><TR><TD id="TD12" align=left runat="server" Visible="false"><asp:Label id="Label42" runat="server" Width="120px" Text="Monthly Rate to IDR" __designer:wfdid="w180"></asp:Label></TD><TD id="TD14" align=left runat="server" Visible="false"><asp:TextBox id="Rate2ToIDR" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w181" Enabled="False">0</asp:TextBox></TD><TD id="TD10" align=left runat="server" Visible="false"><asp:Label id="Label46" runat="server" Width="128px" Text="Monthly Rate To USD" __designer:wfdid="w182"></asp:Label></TD><TD id="TD16" align=left runat="server" Visible="false"><asp:TextBox id="Rate2ToUsd" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w183" Enabled="False">0</asp:TextBox></TD></TR><TR><TD style="HEIGHT: 25px" align=left><asp:Label id="Label28" runat="server" Width="81px" ForeColor="DarkBlue" Text="Type asset" __designer:wfdid="w184"></asp:Label></TD><TD style="HEIGHT: 25px" align=left><asp:DropDownList id="fixgroup" runat="server" Width="154px" CssClass="inpText" __designer:wfdid="w185" AutoPostBack="True" OnSelectedIndexChanged="fixgroup_SelectedIndexChanged"></asp:DropDownList></TD><TD style="HEIGHT: 25px" align=left><asp:Label id="Label21" runat="server" Width="96px" Text="Total Disc Detail" __designer:wfdid="w186" Visible="False"></asp:Label></TD><TD style="HEIGHT: 25px" align=left><asp:TextBox id="TotalDiscDetail" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w187" Visible="False" AutoPostBack="True" Enabled="False" MaxLength="16" OnTextChanged="TotalDiscDetail_TextChanged"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Total Detail" __designer:wfdid="w188"></asp:Label></TD><TD align=left><asp:TextBox id="TotalDetail" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w189" AutoPostBack="True" Enabled="False" MaxLength="16" OnTextChanged="TotalDetail_TextChanged"></asp:TextBox></TD><TD align=left><asp:Label id="Label22" runat="server" Text="Disc hdr" __designer:wfdid="w190" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="DiscHdr" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w191" Visible="False" AutoPostBack="True" Enabled="False" MaxLength="16"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label13" runat="server" Text="Disc Type" __designer:wfdid="w192" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLtype1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w193" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="DDLtype1_SelectedIndexChanged"><asp:ListItem>AMT</asp:ListItem>
<asp:ListItem>PCT</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterType1" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w194" Visible="False" AutoPostBack="True" MaxLength="16" OnTextChanged="FilterType1_TextChanged"></asp:TextBox></TD><TD align=left><asp:Label id="Label23" runat="server" Text="Taxable" __designer:wfdid="w195" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLtax" runat="server" CssClass="inpText" __designer:wfdid="w196" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="DDLtax_SelectedIndexChanged"><asp:ListItem>NON TAX</asp:ListItem>
<asp:ListItem>INCLUSIVE</asp:ListItem>
<asp:ListItem>EXCLUSIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 20px" id="TD4" align=left runat="server" Visible="false"><asp:Label id="Label15" runat="server" Text="DPP" __designer:wfdid="w197"></asp:Label></TD><TD style="HEIGHT: 20px" id="TD6" align=left runat="server" Visible="false"><asp:TextBox id="DPP" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w198" AutoPostBack="True" Enabled="False" MaxLength="16"></asp:TextBox></TD><TD style="HEIGHT: 20px" align=left><asp:Label id="Label24" runat="server" Text="Netto" __designer:wfdid="w199"></asp:Label></TD><TD style="HEIGHT: 20px" align=left><asp:TextBox id="Netto" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w200" AutoPostBack="True" Enabled="False" MaxLength="16" OnTextChanged="Netto_TextChanged"></asp:TextBox></TD></TR><TR><TD id="TD8" align=left runat="server" Visible="false"><asp:Label id="Label17" runat="server" Text="Tax" __designer:wfdid="w201"></asp:Label></TD><TD id="TD5" align=left runat="server" Visible="false"><asp:TextBox id="Tax" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w202" AutoPostBack="True" Enabled="False" MaxLength="10"></asp:TextBox></TD><TD align=left><asp:Label id="Label25" runat="server" Text="Status" __designer:wfdid="w203"></asp:Label></TD><TD align=left><asp:TextBox id="Status" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w204" Enabled="False" MaxLength="16"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label19" runat="server" Text="Note" __designer:wfdid="w205"></asp:Label></TD><TD align=left><asp:TextBox id="Note1" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w206" MaxLength="150"></asp:TextBox></TD><TD align=left><asp:Label id="lblPOST" runat="server" __designer:wfdid="w207" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLoutlet" runat="server" Width="170px" CssClass="inpTextDisabled" __designer:wfdid="w208" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged" Enabled="False"></asp:DropDownList> <asp:Label id="Outlet" runat="server" Text="Outlet" __designer:wfdid="w209" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label11" runat="server" Width="56px" Font-Bold="True" Text="Detail :" __designer:wfdid="w210" Font-Strikeout="False" Font-Underline="True"></asp:Label></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w211" ErrorTooltipEnabled="True" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date" Mask="99/99/9999" CultureName="en-US" TargetControlID="fixDate"></ajaxToolkit:MaskedEditExtender></TD><TD align=left></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w212" TargetControlID="fixDate" PopupButtonID="btnDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" ForeColor="DarkBlue" Text="Code" __designer:wfdid="w213"></asp:Label></TD><TD align=left><asp:TextBox id="Code" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w214" Enabled="False" MaxLength="200"></asp:TextBox> <asp:ImageButton id="btnSearchCode" onclick="btnSearchCode_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w215"></asp:ImageButton> <asp:ImageButton id="btnHapusCode" onclick="btnHapusCode_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w216"></asp:ImageButton> <asp:Label id="lblItemOid" runat="server" ForeColor="DarkBlue" __designer:wfdid="w217" Visible="False"></asp:Label> <asp:Label id="lblCode" runat="server" ForeColor="DarkBlue" __designer:wfdid="w218" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label29" runat="server" Text="Description" __designer:wfdid="w219"></asp:Label></TD><TD align=left><asp:TextBox id="Desc" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w220" MaxLength="200"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label30" runat="server" ForeColor="DarkBlue" Text="Qty" __designer:wfdid="w221"></asp:Label></TD><TD align=left><asp:TextBox id="Qty" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w222" AutoPostBack="True" MaxLength="10" OnTextChanged="Qty_TextChanged"></asp:TextBox></TD><TD align=left><asp:Label id="Label37" runat="server" Text="Price" __designer:wfdid="w223"></asp:Label></TD><TD align=left><asp:TextBox id="Price" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w224" AutoPostBack="True" MaxLength="12" OnTextChanged="Price_TextChanged"></asp:TextBox> <asp:Label id="Label16" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w225"></asp:Label></TD></TR><TR><TD id="TD19" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label31" runat="server" ForeColor="DarkBlue" Text="Disc Type" __designer:wfdid="w226"></asp:Label></TD><TD id="TD17" align=left runat="server" Visible="false"><asp:DropDownList id="DDLtype2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w227" AutoPostBack="True" OnSelectedIndexChanged="DDLtype2_SelectedIndexChanged"><asp:ListItem>AMT</asp:ListItem>
<asp:ListItem>PCT</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterType2" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w228" AutoPostBack="True" OnTextChanged="FilterType2_TextChanged"></asp:TextBox></TD><TD id="TD18" align=left runat="server" Visible="false"><asp:Label id="Label10" runat="server" Text="Disc Dtl" __designer:wfdid="w229"></asp:Label></TD><TD id="TD20" align=left runat="server" Visible="false"><asp:TextBox id="discDtl" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w230" AutoPostBack="True" Enabled="False" MaxLength="16" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label32" runat="server" Text="Sub Total" __designer:wfdid="w231"></asp:Label></TD><TD align=left><asp:TextBox id="subTotal" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w232" AutoPostBack="True" Enabled="False" MaxLength="18"></asp:TextBox></TD><TD align=left><asp:Label id="Label8" runat="server" Text="Depreciation" __designer:wfdid="w233"></asp:Label></TD><TD align=left><asp:TextBox id="fixdepmonth" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w234" AutoPostBack="True" MaxLength="2">0</asp:TextBox> <asp:Label id="Label9" runat="server" Font-Size="X-Small" ForeColor="Red" Text="(month)" __designer:wfdid="w235"></asp:Label> <asp:Label id="Label33" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w236"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label35" runat="server" Text="Final Value" __designer:wfdid="w237"></asp:Label></TD><TD align=left><asp:TextBox id="FinalValue" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w238" AutoPostBack="True" MaxLength="16" OnTextChanged="FinalValue_TextChanged">0</asp:TextBox></TD><TD align=left><asp:Label id="AddToList" runat="server" Text="addtolist" __designer:wfdid="w239" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label12" runat="server" Font-Bold="True" ForeColor="Red" Text="* Set to  -1, If have no Depreciation !!" __designer:wfdid="w240"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label34" runat="server" Text="Note" __designer:wfdid="w241"></asp:Label></TD><TD align=left><asp:TextBox id="Note2" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w242" MaxLength="100"></asp:TextBox></TD><TD align=left colSpan=2><asp:ImageButton id="btnAddtoList" onclick="btnAddtoList_Click1" runat="server" ImageUrl="~/Images/addtolist.png" __designer:wfdid="w243"></asp:ImageButton> <asp:ImageButton id="btnClear" onclick="btnClear_Click1" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w244"></asp:ImageButton> <asp:Label id="fadtloid" runat="server" Text="fadtloid" __designer:wfdid="w245" Visible="False"></asp:Label> <asp:Label id="famstoid" runat="server" Text="famstoid" __designer:wfdid="w246" Visible="False"></asp:Label></TD></TR><TR><TD align=right colSpan=4><asp:GridView id="GVFixedAssetdtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w247" OnSelectedIndexChanged="GVFixedAssetdtl_SelectedIndexChanged" OnRowDataBound="GVFixedAssetdtl_RowDataBound1" OnRowDeleting="GVFixedAssetdtl_RowDeleting" OnPageIndexChanging="GVFixedAssetdtl_PageIndexChanging" GridLines="None" AllowPaging="True" DataKeyNames="cmpcode,trnbelifadtloid,itemfacode,itemfadesc,trnbelifadtlqty,trnbelifadtlprice,trnbelifadtldiscamt,trnbelifadtlamtnetto,trnbelifadtldep,trnbelifadtllastvalue,trnbelifadtlnote,trnbelifadtldiscvalue,trnbelifadtldisctype" AutoGenerateColumns="False" CellPadding="4" AllowSorting="True" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelifadtlseq" HeaderText="Nomer" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemfacode" HeaderText="FA Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemfadesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtlqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtlprice" DataFormatString="{0:#,##0.00}" HeaderText="Price">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtldiscamt" DataFormatString="{0:#,##0.00}" HeaderText="Discount">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtlamtnetto" DataFormatString="{0:#,##0.00}" HeaderText="Sub Total">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtldep" HeaderText="Depreciation">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtllastvalue" DataFormatString="{0:#,##0.00}" HeaderText="Final Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtlnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtldiscvalue" DataFormatString="{0:#,##0.00}" HeaderText="Disc Value" Visible="False"></asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelifadtldisctype" HeaderText="Disc Type" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=4>Created By&nbsp;<asp:Label id="lblUser" runat="server" Font-Bold="True" __designer:wfdid="w248">User</asp:Label>&nbsp; On <asp:Label id="lblTime" runat="server" Font-Bold="True" __designer:wfdid="w249">Time</asp:Label>,&nbsp; Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w250"></asp:Label>&nbsp; By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w251"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w252"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w253"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w254"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w255"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" __designer:wfdid="w256" Visible="False"></asp:ImageButton><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w257" TargetControlID="FilterType1" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w258" TargetControlID="Qty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" __designer:wfdid="w259" TargetControlID="Price" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender4" runat="server" __designer:wfdid="w260" TargetControlID="FilterType2" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender5" runat="server" __designer:wfdid="w261" TargetControlID="FinalValue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD align=left colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w262"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w263"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></DIV></asp:View> </asp:MultiView> </TD></TR><TR></TR><TR></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListSupp" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" Width="650px" CssClass="modalBox" __designer:wfdid="w265" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier" __designer:wfdid="w266"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" __designer:wfdid="w267" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w268"><asp:ListItem Value="suppname">Supp Name</asp:ListItem>
<asp:ListItem Value="suppcode">Supp Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w269"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w270"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w271"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvSupplier" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w125" PageSize="5" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="suppoid,suppcode,suppname,supptype,coa" AllowPaging="True" GridLines="None" OnPageIndexChanging="gvSupplier_PageIndexChanging" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server" __designer:wfdid="w273">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" __designer:wfdid="w274" TargetControlID="btnHideListSupp" PopupControlID="pnlListSupp" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblListSupp"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" __designer:wfdid="w275" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListMat" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w277" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 897px" class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Item" __designer:wfdid="w278" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 897px; HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD style="WIDTH: 897px" class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" __designer:wfdid="w279" DefaultButton="btnFindListMat">Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w280"><asp:ListItem Value="i.itemcode">Code</asp:ListItem>
<asp:ListItem Value="i.itemShortDescription">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w281"></asp:TextBox> <asp:ImageButton id="btnFindListMat" onclick="btnFindListMat_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w282"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" onclick="btnAllListMat_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w283"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="WIDTH: 897px" class="Label" vAlign=top align=left colSpan=3><asp:GridView id="GVmstitem" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w137" OnSelectedIndexChanged="GVmstitem_SelectedIndexChanged1" OnPageIndexChanging="GVmstitem_PageIndexChanging" GridLines="None" AllowPaging="True" DataKeyNames="itemoid,itemcode,itemShortDescription" AutoGenerateColumns="False" CellPadding="4" AllowSorting="True" PageSize="5">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code" SortExpression="itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemShortDescription" HeaderText="Description" SortExpression="itemShortDescription">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="DarkGoldenrod"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 897px" align=center colSpan=3>&nbsp;<asp:LinkButton id="lkbCloseListMat" onclick="lkbCloseListMat_Click" runat="server" __designer:wfdid="w285">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" __designer:wfdid="w286" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" __designer:wfdid="w287" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="UpdatePanel6" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" __designer:wfdid="w289" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Bold="True" Text="COA - Posting" __designer:wfdid="w290"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" __designer:wfdid="w291" GridLines="None" AutoGenerateColumns="False" CellPadding="4">
<RowStyle CssClass="gvrow"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Nama COA">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<PagerStyle CssClass="gvfooter"></PagerStyle>

<AlternatingRowStyle CssClass="gvalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w292">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" __designer:wfdid="w293" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" __designer:wfdid="w294" Visible="False"></asp:Button>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Fixed Asset Purchase :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>