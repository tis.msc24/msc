' Last Update By Juancoookk On 130924
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_RptDPAP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(toDate(txtStart.Text)), CDate(toDate(txtFinish.Text))) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        If rbSupplier.SelectedValue = "SELECT" Then
            If Not (Session("QL_mstcust") Is Nothing) Then
                Dim dtSupp As DataTable : dtSupp = Session("QL_mstcust")
                If dtSupp.Rows.Count > 0 Then
                    Dim dvSupp As DataView = dtSupp.DefaultView
                    dvSupp.RowFilter = "selected='1'"
                    If dvSupp.Count < 1 Then
                        sReturn &= "- Please select some Supplier first.<BR>"
                    End If
                    dvSupp.RowFilter = ""
                Else
                    sReturn &= "- Please select some Supplier first.<BR>"
                End If
            Else
                sReturn &= "- Please select some Supplier first.<BR>"
            End If
        End If
        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT DISTINCT 0 selected, s.suppoid AS custoid,s.suppcode AS custcode,s.suppname AS custname,s.suppaddr AS custaddr FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid WHERE dp.cmpcode='MSC' AND dp.trndpapstatus IN ('POST') AND (s.suppcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR s.suppname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%') AND dp.trndpapdate BETWEEN '" & CDate(toDate(txtStart.Text)) & "' AND '" & CDate(toDate(txtFinish.Text)) & "'  ORDER BY s.suppname"
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstcust")
        gvCustomer.DataSource = dtSupp : gvCustomer.DataBind()
        Session("QL_mstcust") = dtSupp
        gvCustomer.Visible = True
        gvCustomer.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("QL_mstcust") Is Nothing Then
            Dim dtab As DataTable = Session("QL_mstcust")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvCustomer.Rows.Count - 1
                    cb = gvCustomer.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "custoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_mstcust") = dtab
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim sSuppOid As String = ""
            If rbSupplier.SelectedValue = "SELECT" Then
                Dim dtSupp As DataTable : dtSupp = Session("QL_mstcust")
                Dim dvSupp As DataView = dtSupp.DefaultView
                dvSupp.RowFilter = "selected='1'"

                For R1 As Integer = 0 To dvSupp.Count - 1
                    sSuppOid &= dvSupp(R1)("custoid").ToString & ","
                Next
            End If
            Dim swhere As String = "" : Dim swheresupp As String = ""
            If sSuppOid <> "" Then swhere = " WHERE suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            If sSuppOid <> "" Then swheresupp = " WHERE s.suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"

            If FilterCurrency.SelectedIndex <> "2" Then
                report.Load(Server.MapPath(folderReport & "crDPAP.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "crDPAP.rpt"))
            End If

            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("cmpcode", CompnyCode)
            report.SetParameterValue("start", CDate(toDate(txtStart.Text.Trim)))
            report.SetParameterValue("finish", CDate(toDate(txtFinish.Text.Trim)))
            report.SetParameterValue("swhere", swheresupp)

            If chkEmpty.Checked Then
                report.SetParameterValue("swherezero", " ")
            Else
                report.SetParameterValue("swherezero", "")
            End If
            report.SetParameterValue("companyname", CompnyCode)
            report.SetParameterValue("periodreport", Format(CDate(toDate(txtStart.Text)), "dd MMMM yyyy") & " - " & Format(CDate(toDate(txtFinish.Text)), "dd MMMM yyyy"))

            If FilterCurrency.SelectedIndex <> "2" Then
                report.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            Else
                report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            End If

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DPAPREPORT_" & Format(CDate(toDate(txtStart.Text)), "yyyyMMdd" & "_" & Format(CDate(toDate(txtFinish.Text)), "yyyyMMdd")))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DPAPREPORT_" & Format(CDate(toDate(txtStart.Text)), "yyyyMMdd" & "_" & Format(CDate(toDate(txtFinish.Text)), "yyyyMMdd")))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptDPAP.aspx")
        End If
        Session("idPage") = Request.QueryString("idPage")
        Page.Title = CompnyName & " - Down Payment A/P Report"

        If Not Page.IsPostBack Then
            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False
            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "01/MM/yyyy")
            txtFinish.Text = Format(GetServerTime, "dd/MM/yyyy")
            rbSupplier_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvCustomer.DataSource = Nothing : gvCustomer.DataBind()
        Session("QL_mstcust") = Nothing : gvCustomer.Visible = False
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        BindSupplierData()
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        FilterTextSupplier.Text = ""
        custoid.Text = ""
        gvCustomer.DataSource = Nothing
        gvCustomer.DataBind()
        gvCustomer.Visible = False
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCustomer.PageIndexChanging
        UpdateCheckedGV()

        gvCustomer.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstcust")
        gvCustomer.DataSource = dtSupp
        gvCustomer.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("View")
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\rptDPAP.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region

End Class
