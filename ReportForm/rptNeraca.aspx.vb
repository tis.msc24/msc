' Prgmr : 4n7JuK On 130613
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports ClassFunction

Partial Class ReportForm_rptNeraca
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public cKon As New Koneksi
    Dim vReport As New ReportDocument
    Dim cProc As New ClassProcedure
    Dim sSQL As String = ""
#End Region

#Region "Functions"
    Private Function ValidateInput() As String ' Return empty string if Success
        Dim sReturn As String = ""
        Dim Date1 As Date = New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, 1)
        Dim Date2 As Date = New Date(DDLYear2.SelectedValue, DDLMonth2.SelectedValue, 1)
        If Date1 > Date2 Then
            sReturn &= "- End Period can't be less than start period.<BR>"
        End If
        Return sReturn
    End Function
#End Region

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' BULAN
        If Session("branch_id") = "01" Then
            sSQL = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
            FillDDL(dd_branch, sSQL)
            dd_branch.Enabled = True
            dd_branch.CssClass = "inpText"
        Else
            sSQL = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and gencode='" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSQL)
            dd_branch.Enabled = False
            dd_branch.CssClass = "inpTextDisabled"
        End If

        For C1 As Integer = 1 To 12
            Dim liMonth As New ListItem(MonthName(C1), C1)
            DDLMonth.Items.Add(liMonth)
            Dim liMonth2 As New ListItem(MonthName(C1), C1)
            DDLMonth2.Items.Add(liMonth2)
        Next
        DDLMonth2.SelectedValue = 12
        ' TAHUN
        For C1 As Integer = GetServerTime.Year - 5 To GetServerTime.Year + 5
            Dim liYear As New ListItem(C1, C1)
            DDLYear.Items.Add(liYear)
            Dim liYear2 As New ListItem(C1, C1)
            DDLYear2.Items.Add(liYear2)
        Next
        DDLYear2.SelectedValue = GetServerTime.Year
    End Sub

    Sub showPrint()
        Dim sMsg As String = ""
        Try
            If DDLTipe.SelectedValue = "SUMMARY" Then
                vReport.Load(Server.MapPath("~/report/neraca.rpt"))
            Else
                vReport.Load(Server.MapPath("~/report/neracadtl.rpt"))
            End If 
            If cbFilter.Checked Then
                sSQL = " WHERE amtbalanceidr <> 0 "
            End If
            'cProc.SetDBLogonForReport(vReport)
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.PrintOptions.PaperSize = PaperSize.PaperA4
            vReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            vReport.SetParameterValue("cmpcode", cmpcode)
            vReport.SetParameterValue("periodawal", GetDateToPeriodAcctg(New Date(DDLYear2.SelectedValue, DDLMonth2.SelectedValue, 1)))
            vReport.SetParameterValue("speriode", DDLMonth2.SelectedItem.Text & " " & DDLYear2.SelectedItem.Text)
            vReport.SetParameterValue("branch_code", dd_branch.SelectedValue)
            vReport.SetParameterValue("compnyname", dd_branch.SelectedItem.Text)
            vReport.SetParameterValue("sWhereZero", sSQL)
        Catch ex As Exception
            sMsg = ex.ToString
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Session("branch") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Not (CheckPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        Session.Timeout = 60

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()

            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptNeraca.aspx")
        End If
        Page.Title = CompnyName & " - Balance Sheet Report"
        If Not Page.IsPostBack Then
            Session("showReport") = "false"
            InitDDL()
            DDL_SelectedIndexChanged(Nothing, Nothing)
            date1.Text = "" & GetServerTime.Month & "/01/" & GetServerTime.Year & ""
            date2.Text = Format(GetServerTime, "MM/dd/yyyy")
        End If
        If Session("showReport") = "true" Then
            showPrint()
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        vReport.Close() : vReport.Dispose()
    End Sub

    Protected Sub btnOKPopUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOKPopUp.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub DDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles DDLMonth2.SelectedIndexChanged, DDLYear2.SelectedIndexChanged
        DDLYear.Items.Clear()

        ' Calculate Start Periode for 12 month report periods
        Dim iStartM As Integer = 0 : Dim iStartY As Integer = 0
        If DDLMonth2.SelectedValue - 12 < 0 Then
            iStartM = 12 - (11 - DDLMonth2.SelectedValue)
            iStartY = DDLYear2.SelectedValue - 1
        Else
            iStartM = DDLMonth2.SelectedValue - 11
            iStartY = DDLYear2.SelectedValue
        End If

        DDLMonth.SelectedValue = iStartM
        DDLYear.Items.Add(New ListItem(iStartY, iStartY))
    End Sub

    Protected Sub btView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btView.Click
        Session("viewReport") = "False" : crv.ReportSource = Nothing
        showPrint()
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        Try
            vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "BALANCE_STATEMENT_" & DDLTipe.SelectedItem.Text.ToUpper & _
            "_" & (DDLMonth.SelectedItem.Text & DDLYear.SelectedItem.Text).ToUpper & "_" & (DDLMonth2.SelectedItem.Text & DDLYear2.SelectedItem.Text).ToUpper)
        Catch ex As Exception
            vReport.Close() : vReport.Dispose()
        End Try
    End Sub

    Protected Sub imgExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExcel.Click
        Session("showReport") = "false"
        crv.ReportSource = Nothing
        Dim sMsg As String = ValidateInput()
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        showPrint()
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        Try
            vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "BALANCE_STATEMENT_" & DDLTipe.SelectedItem.Text.ToUpper & _
            "_" & (DDLMonth.SelectedItem.Text & DDLYear.SelectedItem.Text).ToUpper & "_" & (DDLMonth2.SelectedItem.Text & DDLYear2.SelectedItem.Text).ToUpper)
        Catch ex As Exception
            vReport.Close() : vReport.Dispose()
        End Try
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptNeraca.aspx?awal=true")
    End Sub

    Protected Sub btnViewReport_Click(sender As Object, e As ImageClickEventArgs) Handles btnViewReport.Click
        Session("viewReport") = "False"
        crv.ReportSource = Nothing
        showPrint()
        crv.DisplayGroupTree = False
        crv.DisplayToolbar = False
        crv.SeparatePages = False
        Session("viewReport") = "True"
        crv.ReportSource = vReport
    End Sub
End Class
