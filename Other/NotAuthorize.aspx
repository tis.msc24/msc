<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="NotAuthorize.aspx.vb" Inherits="Other_NotAuthorize" title="Multi Sarana Computer  - Not Authorize" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tableutama" align="center" border="0" cellpadding="0" cellspacing="0"
        width="979">
        <tr>
            <td style="width: 310px; height: 350px" valign="top">
                <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="270" style="vertical-align: top; height: 100%">
                    <tr>
                        <th align="left" class="header" style="width: 276px" valign="center">
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; :: User Active</th>
                    </tr>
                    <tr>
                        <td style="width: 276px; vertical-align: top; height: 100%;">
                            <table align="center" border="0" width="257">
                                <tr>
                                    <td align="left" style="width: 26px; text-align: center">
                                        <asp:Image ID="Image2" runat="server" Height="24px" ImageUrl="~/Images/user.png"
                                            Width="24px" /></td>
                                    <td align="left">
                                        <asp:Label ID="lblWelcome" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                                            ForeColor="LightSkyBlue"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 26px; text-align: center">
                                    </td>
                                    <td align="left">
                                        </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="height: 285px" valign="top" width="9">
                <br />
                &nbsp;
                <br />
            </td>
            <td style="height: 350px" valign="top">
                <table id="Table1" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="700" style="vertical-align: top; height: 100%">
                    <tr>
                        <th align="left" class="header" style="width: 276px" valign="center">
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; </th>
                    </tr>
                    <tr>
                        <td style="width: 276px; text-align: center; vertical-align: top; height: 100%;">
                            <table align="center" border="0" width="687">
                                <tr>
                                    <td align="left" colspan="2">
                                        <span style="font-size: 12pt; color: navy"><strong>You are not authorized to access
                                            that page..</strong></span></td>
                                </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <strong><span style="font-size: 12pt">
                                        <br />
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Other/menu.aspx" Font-Size="Small">Back To Home</asp:HyperLink></span></strong></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    &nbsp;&nbsp;
                
</asp:Content>

