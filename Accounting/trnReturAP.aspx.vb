Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunctionAccounting
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnReturAP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = 4
    Public DefCounterCashBankNo As String = "4"
    Public DefaultFormatCounter As Int16 = 4
    Dim conn As New SqlConnection(ConnStr)
    Dim conn2 As New SqlConnection(ConnStr)
    Dim objCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dv As DataView
    Dim dtStaticItem As DataTable
    Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
    Dim objDs As New DataSet
    Dim cKoneksi As New Koneksi
    Dim dp, DPsc As Decimal
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim cProc As New ClassProcedure
    'Dim cf As New ClassFunction
    Public sqlTempSearch As String = ""
    Private cBulan() As Char = {"A", "B", "C", "", "D", "E", "F", "G", "H", "I", "J", "K", "L"}
#End Region

#Region "Functions"
    Public Function InvoiceRate(ByVal curroid As Integer) As Double
        Dim curRate As Double
        sSql = "select isnull((case when c.currencyoid=1 then rate2usdvalue else rate2idrvalue end),0) rateValue  from QL_trncashbankmst cb  left join ql_mstrate2 c on cb.cashbankcurroid = c.currencyoid where cashbankoid = " & RetcbOidAp.Text & ""

        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curRate = objCmd.ExecuteScalar
        Return curRate
        conn.Close()
        Return curRate
    End Function

    Public Function GetCashBankOid(ByVal cashbankNo As String) As Integer
        Dim cashbakoid As Double
        sSql = "select cashbankoid from QL_trncashbankmst where cashbankno = '" & cashbankNo & "'"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        cashbakoid = objCmd.ExecuteScalar
        Return cashbakoid
        conn.Close()
    End Function

    Private Function SetTabelDetailSlisih() As DataTable
        Dim nuDT As New DataTable
        nuDT.Columns.Add("selisihseq", Type.GetType("System.Int32"))
        nuDT.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("acctgdesc", Type.GetType("System.String"))
        nuDT.Columns.Add("amtdtlselisih", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("dtlnoteselisih", Type.GetType("System.String"))
        Return nuDT
    End Function

    Private Function SetTabelDetail() As DataTable
        Dim nuDT As New DataTable
        nuDT.Columns.Add("paymentoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("cashbankoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payreftype", Type.GetType("System.String"))
        nuDT.Columns.Add("payrefoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payacctgoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("paynote", Type.GetType("System.String"))
        nuDT.Columns.Add("payamt", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("trnbelino", Type.GetType("System.String"))
        nuDT.Columns.Add("suppname", Type.GetType("System.String"))
        nuDT.Columns.Add("amttrans", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("amtpaid", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("amtretur", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("acctgdesc", Type.GetType("System.String"))
        nuDT.Columns.Add("trntaxpct", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("payseq", Type.GetType("System.Int32"))
        nuDT.Columns.Add("invCurrOid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("invCurrRate", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("invCurrCode", Type.GetType("System.String"))
        nuDT.Columns.Add("invCurrDesc", Type.GetType("System.String"))
        nuDT.Columns.Add("invPayment", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("payduedate", Type.GetType("System.String"))
        nuDT.Columns.Add("payrefno", Type.GetType("System.String"))
        nuDT.Columns.Add("trndpapoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("DPAmt", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("flagdtl", Type.GetType("System.String"))
        nuDT.Columns.Add("RealOid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payres1", Type.GetType("System.Int32"))
        Return nuDT
    End Function

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal no As String, ByVal type As String)
        'ttello
        Dim cashbankOid As Integer = GetCashBankOid(no)
        'untuk print
        If type = "BBK" Then
            report.Load(Server.MapPath(folderReport & "printBBK.rpt"))
        ElseIf type = "BKK" Then
            report.Load(Server.MapPath(folderReport & "printBKK.rpt"))
        ElseIf type = "BGK" Then
            report.Load(Server.MapPath(folderReport & "printBGK.rpt"))
        ElseIf type = "BDK" Then
            report.Load(Server.MapPath(folderReport & "printBDK.rpt"))
        End If

        report.SetParameterValue("cmpcode", cmpcode)
        report.SetParameterValue("no", no)
        report.SetParameterValue("companyname", CompnyName)
        'report.SetParameterValue("sWhere", "")
        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)

        'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no)
        report.Close() : report.Dispose() ': Session("no") = Nothing
    End Sub
#End Region

#Region "Procedures"
    Private Sub Fill_payflag()
        lbldpno.Visible = False : trndpapoid.Visible = False
        lbldpbalance.Visible = False : dpbalance.Visible = False

        If payflag.SelectedValue = "CASH" Then
            lblPayType.Text = "Cash" : lblpayrefno.Visible = False : payrefno.Visible = False
            lblpayduedate.Visible = False : payduedate.Visible = False
            btnDueDate.Visible = False : lblnotice.Visible = False
            lblBankName.Visible = False : ddlBankName.Visible = False
            lblRealisasi.Visible = False : RealNo.Visible = False : sRealBtn.Visible = False
            RealGv.Visible = False : EraseV.Visible = False
        ElseIf payflag.SelectedValue = "NONCASH" Then
            lblBankName.Visible = False : ddlBankName.Visible = False
            lblPayType.Text = "Bank" : lblpayrefno.Visible = True : payrefno.Visible = True
            lblpayduedate.Visible = True : payduedate.Visible = True
            btnDueDate.Visible = True : lblnotice.Visible = True
            lblRealisasi.Visible = False : RealNo.Visible = False : sRealBtn.Visible = False
            RealGv.Visible = False : EraseV.Visible = False
        ElseIf payflag.SelectedValue = "GIRO" Then
            lblPayType.Text = "Giro" : lblpayrefno.Visible = True : payrefno.Visible = True
            lblpayduedate.Visible = True : payduedate.Visible = True
            btnDueDate.Visible = True : lblnotice.Visible = True
            lblBankName.Visible = False : ddlBankName.Visible = False ' True ' Buat apa sih ??
            lblRealisasi.Visible = False : RealNo.Visible = False
            sRealBtn.Visible = False : EraseV.Visible = False
        ElseIf payflag.SelectedValue = "CREDIT CARD" Then
            lblPayType.Text = "Credit Card" : lblpayrefno.Visible = True
            payrefno.Visible = True : RealGv.Visible = False
            lblpayduedate.Visible = True : payduedate.Visible = True : btnDueDate.Visible = True
            lblnotice.Visible = True : lblBankName.Visible = False
            ddlBankName.Visible = False : EraseV.Visible = False
            lblRealisasi.Visible = False : RealNo.Visible = False : sRealBtn.Visible = False
        ElseIf payflag.SelectedValue = "DP" Then
            lblPayType.Text = "Down paymant" : lblpayrefno.Visible = False
            payrefno.Visible = False : lblpayduedate.Visible = False
            payduedate.Visible = False : btnDueDate.Visible = False
            lblnotice.Visible = False : RealGv.Visible = False
            lblBankName.Visible = False : ddlBankName.Visible = False
            lbldpno.Visible = True : trndpapoid.Visible = True
            lbldpbalance.Visible = True : dpbalance.Visible = True
            lblRealisasi.Visible = False : RealNo.Visible = False
            sRealBtn.Visible = False : EraseV.Visible = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If trnsuppoid.Text.Trim = "" Then
                    showMessage("Pilih Supplier dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If
        ElseIf payflag.SelectedValue = "VOUCHER" Then
            lblRealisasi.Visible = True : RealNo.Visible = True : sRealBtn.Visible = True
            lblpayduedate.Visible = False : payduedate.Visible = False
            btnDueDate.Visible = False : lblpayrefno.Visible = False : payrefno.Visible = False
            lblPayType.Text = "Voucher" : lbldpbalance.Text = "Realisasi Amt"
            dpbalance.Visible = True : cashbankacctgoid.Enabled = False
            cashbankacctgoid.CssClass = "inpTextDisabled" : lbldpbalance.Visible = True
            EraseV.Visible = True : amtpayment.Enabled = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If trnsuppoid.Text.Trim = "" Then
                    showMessage("Pilih Supplier dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If
        Else
            amtpayment.Enabled = True
        End If
        initDDLcashbank(payflag.SelectedValue)
        If cashbankacctgoid.Items.Count < 1 Then
            showMessage("Akun COA Untuk " & payflag.SelectedItem.Text & " belum di buat !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        Else
            GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
        End If

        'If payflag.SelectedValue = "CREDIT CARD" Then
        '    creditsearch.Visible = True
        '    CREDITCLEAR.Visible = True : code.Visible = True
        '    code.Text = "" : payrefno.Text = ""
        '    payrefno.Enabled = False : payrefno.CssClass = "inpTextDisabled"
        'Else
        creditsearch.Visible = False
        CREDITCLEAR.Visible = False : code.Visible = False
        code.Text = "" : payrefno.Text = ""
        payrefno.Enabled = True : payrefno.CssClass = "inpText"
        'End If

        'If payflag.SelectedValue = "DP" Then
        '    InitDPAP(trnsuppoid.Text)
        '    If trndpapoid.Items.Count > 0 Then
        '        FillDPBalance(trndpapoid.SelectedValue)
        '        FillDPAccount(trndpapoid.SelectedValue)
        '    Else
        '        FillDPBalance(0)
        '    End If
        'End If

    End Sub

    Private Sub initCbg()
        Dim sCb As String = ""
        If Session("branch_id") <> "10" Then
            sCb = "And gencode='" & Session("branch_id") & "'"
        Else
            ddlcabang.Enabled = True : ddlcabang.CssClass = "inpText"
        End If
        sSql = "Select gencode,gendesc from QL_mstgen Where gengroup='CABANG'" & sCb & ""
        FillDDL(ddlcabang, sSql)
    End Sub

    Private Sub BindReal()
        sSql = "Select * FROM (Select realisasioid,realisasino,ISNULL(realisasiamt,0.00)-ISNULL((Select SUM(payamtidr) FROM QL_trnpayap ap Where ap.realisasioid=r.realisasioid AND ap.cmpcode=r.cmpcode AND payrefflag <> 'OTHER'),0.00) realisasiamt,realisasistatus,'' voucherno,0 voucheroid,realisasitype,bm.trnbelipono,'' voucherdesc From QL_trnrealisasi r INNER JOIN QL_pomst bm ON bm.trnbelimstoid=r.pomstoid Where realisasistatus='APPROVED' And realisasitype='HUTANG' AND ISNULL(realisasiflag,'') <> 'LUNAS') As Rv Where realisasino LIKE '%" & Tchar(RealNo.Text.Trim) & "%' AND realisasiamt > 0"

        Dim rTab As DataTable = cKoneksi.ambiltabel(sSql, "Ql_realisasi")
        RealGv.DataSource = rTab : RealGv.DataBind()
        Session("rTab") = rTab : RealGv.Visible = True
    End Sub

    Private Sub DataBindSupp()

        sSql = "SELECT suppOID AS ID,suppCODE AS Code, suppNAME AS Name FROM QL_MSTSUPP AS c WHERE (CMPCODE = 'MSC') AND " & DDLSuppID.SelectedValue & " LIKE '%" & Tchar(txtFindSuppID.Text.Trim.ToUpper) & "%' And suppoid in ( SELECT m.suppoid from QL_conap m Where m.trnaptype='PAYAP' ANd m.branch_code='" & ddlcabang.SelectedValue & "') order by suppname ASC"
        Dim Supp As DataTable = cKoneksi.ambiltabel(sSql, "Ql_Supp")
        gvSupplier.DataSource = Supp : gvSupplier.DataBind()
        Session("Supp") = Supp
    End Sub

    Private Sub BindApNo()
        sSql = "Select * From (" & _
        " Select cb.branch_code,cb.cashbankoid,cb.cashbankno,cb.cashbankamount,cb.cashbankgroup,cashbanktype,ISNULL((Select SUM(payamtidr) From ql_trnpayap ap Where ap.cashbankoid=cb.cashbankoid And cb.branch_code=ap.branch_code),0.00) PayAmt,ISNULL((Select DISTINCT suppname From ql_mstsupp sup INNER JOIN ql_trnpayap ap ON ap.suppoid=sup.suppoid And ap.cashbankoid=cb.cashbankoid And cb.branch_code=ap.branch_code),'') SuppName,ISNULL((Select DISTINCT sup.suppoid From ql_mstsupp sup INNER JOIN ql_trnpayap ap ON ap.suppoid=sup.suppoid And ap.cashbankoid=cb.cashbankoid And cb.branch_code=ap.branch_code),0) Suppoid,cb.cashbankacctgoid, (select distinct trndpapoid from ql_trnpayap where cashbankoid = cb.cashbankoid) trndpapoid FROM ql_trncashbankmst cb Where cashbankgroup='AP' and cashbankstatus = 'POST' and cashbankgroup <> 'APK' and cashbankoid not in (select bankoid from QL_trncashbankmst where cashbankgroup = 'APK' and cashbankstatus = 'POST')) cb Where suppoid=" & trnsuppoid.Text & " And Cashbankno LIKE '%" & Tchar(ApNo.Text) & "%' And branch_code='" & ddlcabang.SelectedValue & "'"
        Dim ApN As DataTable = cKoneksi.ambiltabel(sSql, "Ql_CbAp")
        GvRetAp.DataSource = ApN : GvRetAp.DataBind()
        Session("rTab") = ApN : GvRetAp.Visible = True
    End Sub

    Private Sub BindPInya()
        sSql = "SELECT DISTINCT paymentoid, pap.payduedate, 1 trndpapoid, pap.DPAmtIDR AS DPAmt, pap.payrefno, pap.cashbankoid, pap.payreftype, pap.payrefoid, pap.payacctgoid, paynote, payamtIDR payamt, bm.trnbelino, bm.trnsuppoid, s.suppname, ap.amttransidr + ISNULL((SELECT SUM (db.amountidr) FROM QL_CREDITnote db WHERE db.refoid = BM.TRNBELIMSTOID AND db.reftype = 'HUTANG' AND DB.cmpcode = AP.CMPCODE ),0 ) amttrans, (SELECT isnull(SUM(p2.payamtidr), 0) FROM ql_trnpayaP p2 INNER JOIN ql_conap cp ON cp.payrefoid = p2.paymentoid AND cp.reftype = 'QL_trnpayaP' WHERE p2.cmpcode = cb.cmpcode AND p2.cashbankoid < cb.cashbankoid AND p2.paystatus = 'POST' AND p2.payrefoid = pap.payrefoid AND p2.payreftype = 'ql_trnBELImst' AND rtrim(p2.payflag) = '' AND cp.payrefno NOT LIKE '%PR/%') amtpaid, bm.amtreturidr amtretur, (SELECT acc.acctgcode + '-' + acc.acctgdesc FROM ql_mstacctg acc WHERE pap.payacctgoid = acc.acctgoid ) acctgdesc, bm.trntaxpct, '1' AS payseq, ISNULL(bm.currencyoid, 2) AS invCurrOid, ISNULL(bm.currencyrate, 1) AS invCurrRate, (SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode = bm.cmpcode AND cr.currencyoid = ISNULL(bm.currencyoid, 2)) AS invCurrCode, (SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode = bm.cmpcode AND cr.currencyoid = ISNULL(bm.currencyoid, 2)) AS invCurrDesc, pap.payamtidr invPayment, pap.payrefflag AS flagdtl,pap.realisasioid AS RealOid,pap.payres1 FROM ql_trnpayaP pap INNER JOIN ql_trncashbankmst cb ON cb.cmpcode = pap.cmpcode AND cb.cashbankoid = pap.cashbankoid INNER JOIN ql_trnBELImst bm ON bm.cmpcode = pap.cmpcode AND payrefoid = bm.trnBELImstoid INNER JOIN ql_mstSUPP s ON bm.cmpcode = s.cmpcode AND bm.trnSUPPoid = s.SUPPoid INNER JOIN ql_conaP ap ON bm.cmpcode = ap.cmpcode AND bm.trnBELImstoid = ap.refoid AND ap.reftype = 'ql_trnBELImst' WHERE cb.cashbankoid = " & RetcbOidAp.Text & " AND pap.cmpcode = 'MSC' AND cb.branch_code=(Case cb.branch_code when '01' then '01' else '" & ddlcabang.SelectedValue & "' End) " & _
        "UNION ALL " & _
        " SELECT DISTINCT paymentoid, pap.payduedate, 1 trndpapoid, pap.DPAmtIDR AS DPAmt, pap.payrefno, pap.cashbankoid, pap.payreftype, pap.payrefoid, pap.payacctgoid, paynote, payamtIDR payamt, bm.trnbelifano AS trnbelino, bm.suppoid AS trnsuppoid, s.suppname, ap.amttransidr + ISNULL(( SELECT SUM (db.amountidr) FROM QL_CREDITnote db WHERE db.refoid = bm.trnbelifamstoid AND db.reftype = 'HUTANG' AND DB.cmpcode = AP.CMPCODE ), 0 ) amttrans, (SELECT isnull(SUM(p2.payamtidr), 0) FROM ql_trnpayaP p2 INNER JOIN ql_conap cp ON cp.payrefoid = p2.paymentoid AND cp.reftype = 'QL_trnpayaP' WHERE p2.cmpcode = cb.cmpcode AND p2.cashbankoid < cb.cashbankoid AND p2.paystatus = 'POST' AND p2.payrefoid = pap.payrefoid AND p2.payreftype = 'QL_trnbelimst_fa' AND rtrim(p2.payflag) = '' AND cp.payrefno NOT LIKE '%PR/%') amtpaid, 0.00 AS amtretur, ( SELECT acc.acctgcode + '-' + acc.acctgdesc FROM ql_mstacctg acc WHERE pap.payacctgoid = acc.acctgoid ) acctgdesc, bm.trnbelifamsttaxpct AS trntaxpct, '1' AS payseq, ISNULL(bm.curroid, 2) AS invCurrOid, ISNULL(bm.rateoid, 1) AS invCurrRate, (SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode = bm.cmpcode AND cr.currencyoid = ISNULL(bm.curroid, 2)) AS invCurrCode, ( SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode = bm.cmpcode AND cr.currencyoid = ISNULL(bm.curroid, 2)) AS invCurrDesc, pap.payamtidr invPayment, pap.payrefflag AS flagdtl,pap.realisasioid AS RealOid,pap.payres1 FROM ql_trnpayap pap INNER JOIN ql_trncashbankmst cb ON cb.cmpcode = pap.cmpcode AND cb.cashbankoid = pap.cashbankoid INNER JOIN QL_trnbelimst_fa bm ON bm.cmpcode = pap.cmpcode AND payrefoid = bm.trnbelifamstoid INNER JOIN ql_mstSUPP s ON bm.cmpcode = s.cmpcode AND bm.SUPPoid = s.SUPPoid INNER JOIN ql_conaP ap ON bm.cmpcode = ap.cmpcode AND bm.trnbelifamstoid = ap.refoid AND ap.reftype IN ('QL_trnbelimst_fa') WHERE cb.cashbankoid = " & RetcbOidAp.Text & " AND pap.cmpcode = 'MSC' AND cb.branch_code=(Case cb.branch_code when '01' then '01' else '" & ddlcabang.SelectedValue & "' End)"
        Dim ApN As DataTable = cKoneksi.ambiltabel(sSql, "Ql_InvAp")
        GVDtlPayAP.DataSource = ApN : GVDtlPayAP.DataBind()
        Session("tbldtl") = ApN : GVDtlPayAP.Visible = True
    End Sub

    Private Sub CalculateTotalPayment()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim cPay As String = ""
        Dim tbsel As DataTable = Session("DtlSelisih")
        'amtpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 2)
        If ToDouble(amtpayment.Text) > ToDouble(APAmt.Text) Then
            chkOther.Enabled = False
            chkOther.Checked = True : DDLOtherType.SelectedValue = "+"
            SetOtherAccount(True)
            DDLOtherType_SelectedIndexChanged(Nothing, Nothing)
            otheramt.Text = ToMaskEdit(ToDouble(amtpayment.Text) - ToDouble(APAmt.Text), 2)
        ElseIf ToDouble(amtpayment.Text) < ToDouble(APAmt.Text) Then
            'Dim sVar As String = "VAR_VOUCHER"
            'sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='MSC' AND interfacevar='" & sVar & "' AND interfaceres1='MSC'"
            'Dim sCode As String = GetStrData(sSql)
            'If sCode = "" Then
            '    sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='MSC' AND interfacevar='" & sVar & "' AND interfaceres1='All'"
            '    sCode = GetStrData(sSql)
            'End If
            'If sCode <> "" Then
            '    sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='MSC' AND ("
            '    Dim sSplitCode() As String = sCode.Split(",")
            '    For C1 As Integer = 0 To sSplitCode.Length - 1
            '        sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
            '        If C1 < sSplitCode.Length - 1 Then
            '            sSql &= " OR "
            '        End If
            '    Next
            '    sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) And a.acctgoid='" & otherAcctgoid.SelectedValue & "'ORDER BY a.acctgcode"
            'End If
            'cPay = GetStrData(sSql)

            'If GetStrData(sSql) <> "?" Then
            '    chkOther.Checked = True : chkOther.Enabled = False
            'Else
            '    chkOther.Enabled = True : chkOther.Checked = False
            'End If
            DDLOtherType.SelectedValue = "-"
            DDLOtherType_SelectedIndexChanged(Nothing, Nothing)
            SetOtherAccount(True) : chkOther.Enabled = True
            otheramt.Text = ToMaskEdit(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text), 2)
        Else
            chkOther.Checked = False
            SetOtherAccount(False) : otheramt.Text = ToMaskEdit(0, 2)
            totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 2)
        End If
        'CalculateSelisih()
        objCmd.Connection.Close()
    End Sub

    Private Sub CalculateSelisih()
        ' Calculate total Detail Selisih
        If Not Session("DtlSelisih") Is Nothing Then
            Dim objCek As DataTable = Session("DtlSelisih")
            Dim dTotalSelisih As Double = Math.Abs(ToDouble(objCek.Compute("SUM(amtdtlselisih)", "").ToString))
            If dTotalSelisih < 0 Then dTotalSelisih *= -1
            otheramt.Text = ToMaskEdit(dTotalSelisih, 2)
            If ToDouble(amtpayment.Text) < ToDouble(APAmt.Text) Then
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text) + dTotalSelisih, 2)
            Else
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 2)
            End If
        End If
    End Sub

    Public Sub InitAllDDL()
        FillDDL(CurrencyOid, "select currencyoid,currencycode from QL_mstcurr where cmpcode='" & cmpcode & "' order by currencyoid")
        FillCurrencyRate(CurrencyOid.SelectedItem.Value)
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 rate2idrvalue from QL_mstrate2 where rate2month=MONTH(getdate()) and rate2year = YEAR(getdate()) and currencyoid = 2 ORDER BY rate2oid desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyRate.Text = ToMaskEdit(objCmd.ExecuteScalar, 2)
        conn.Close()
    End Sub

    Private Sub CekPaymentCurrencyToInvoice()
        If CurrencyOid.SelectedValue = invCurrOid.Text Then
            If ToDouble(invCurrRate.Text) <> ToDouble(currencyRate.Text) Then
                Session("tbldtl") = Nothing : GVDtlPayAP.DataSource = Nothing : GVDtlPayAP.DataBind()
                currencyRate.Text = ToMaskEdit(ToDouble(invCurrRate.Text), 3)
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub UncheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("cashbankstatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub gridCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If (e.CommandName = "ConfirmPrint") Then
            Response.Redirect("~/reportform/ql_printBK.aspx?paymentoid=" & Trim(e.CommandArgument.ToString()) & "&printtype=8")
        End If
    End Sub

    Public Sub initDDLcashbank(ByVal cashbank As String)
        btnSave.Visible = True : Dim sVar As String = ""
        If Trim(cashbank) = "CASH" Then
            sVar = "VAR_CASH"
        ElseIf Trim(cashbank) = "NONCASH" Then
            sVar = "VAR_BANK"
        ElseIf Trim(cashbank) = "GIRO" Then
            sVar = "VAR_GIRO_HUTANG"
        ElseIf Trim(cashbank) = "DP" Then
            sVar = "VAR_DPAP"
        ElseIf Trim(cashbank) = "VOUCHER" Then
            sVar = "VAR_REALISASI_VOUCHER"
        End If
        FillDDLAcctg(cashbankacctgoid, sVar, Session("branch_id"))
        If cashbankacctgoid.Items.Count < 1 Then
            showMessage("Isi/Buat account " & sVar & " di master accounting!! " & sVar & " !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
    End Sub

    Public Sub binddata()
        Dim st1, st2 As Boolean
        Dim sMsg As String = ""
        If IsDate(toDate(txtPeriode1.Text)) Then : st1 = True
        Else : st1 = False : sMsg &= "- Periode awal salah !!<BR>" : End If
        If IsDate(toDate(txtPeriode2.Text)) Then : st2 = True
        Else : st2 = False : sMsg &= "- Periode akhir salah !!<BR>" : End If
        If st1 And st2 Then
            If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
                sMsg &= "- Periode akhir harus >= periode awal !!<BR>"
            End If
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        Dim mySqlConn As New SqlConnection(ConnStr)

        Dim SqlNya As String = "SELECT distinct cb.cashbankoid,cashbankno,case cashbanktype when 'BKM' then 'CASH' when 'BBM' then 'NONCASH' when 'BCM' then 'CREDIT CARD' when 'BDM' then 'DP' WHEN 'BGM' then 'GIRO' else 'LAIN-LAIN' end payflag,cashbankdate as date,(SELECT FLOOR(SUM(payamt)) FROM QL_trnpayaP pd WHERE pd.cashbankoid=cb.cashbankoid AND p.suppoid=pd.suppoid) AS payamt,(SELECT FLOOR(SUM(payamt)) FROM QL_trnpayaP pd WHERE pd.cashbankoid=cb.cashbankoid AND p.suppoid=pd.suppoid)*cb.cashbankcurrate AS payamtrp,cashbankstatus,cashbanknote,cr.currencycode currencycode,cb.cashbankcurrate, s.suppname suppliername,p.payrefno,'' bank,(Select cashbankno FROM ql_trncashbankmst cap Where cap.cashbankoid=cb.bankoid And cap.branch_code=cb.branch_code) Apno FROM QL_trncashbankmst cb INNER JOIN QL_mstcurr cr ON cr.cmpcode=cb.cmpcode AND cr.currencyoid=cb.cashbankcurroid inner join QL_trnpayaP p on p.cashbankoid=cb.cashbankoid and p.cmpcode=cb.cmpcode inner join ql_mstsupp s on s.cmpcode=p.cmpcode and s.suppoid=p.suppoid WHERE cb.cmpcode='" & cmpcode & "' AND cashbankgroup='APK' And cb.cashbanktype IN ('BKM','BBM','BDM','BGM','GIRO') AND cashbankdate >='" & CDate(toDate(txtPeriode1.Text)) & "' And cashbankdate <='" & CDate(toDate(txtPeriode2.Text)) & "' " & IIf(postinge.SelectedValue.Trim.ToUpper <> "ALL", " AND upper(cashbankstatus) = '" & postinge.SelectedValue.Trim.ToUpper & "' and ", " and ") & IIf(ddlFilter.SelectedValue = "b.trnbelino", " p.payreftype='QL_TRNBELIMST' and p.payrefoid in (select b.trnbelimstoid from QL_trnbelimst b where b.trnbelino like '%" & Tchar(FilterText.Text.Trim) & "%')", ddlFilter.SelectedValue & " like '%" & Tchar(FilterText.Text.Trim) & "%'")
        SqlNya &= " ORDER BY " & orderby.SelectedValue
        Dim objTable As DataTable = cKoneksi.ambiltabel(SqlNya, "ql_trncashbankmst")
        Session("tbldata") = objTable
        GVmstPAYAP.DataSource = objTable
        GVmstPAYAP.DataBind()
        calcTotalInGrid()
    End Sub

    Protected Sub calcTotalInGrid()
        Dim objTable As DataTable : objTable = Session("tbldata")
        Dim gtotal As Double = 0
        For C1 As Integer = 0 To objTable.Rows.Count - 1
            gtotal += ToDouble(objTable.Rows(C1)("payamtrp").ToString)
        Next
        lblgrandtotal.Text = ToMaskEdit(gtotal, 2)
    End Sub

    Protected Sub bindDataPI(ByVal cmpcode As String, ByVal noNotaBeli As String)
        If CurrencyOid.SelectedItem.Text = "IDR" Then
            sSql = "SELECT * FROM (" & _
            " SELECT DISTINCT b.trnbelimstoid, b.trnsuppoid, s.suppname, b.trnbelino, CONVERT (CHAR(10), b.trnbelidate, 103) trnbelidate, CONVERT ( CHAR (10), dateadd( DAY, abs(g.genother1), trnbelidate ), 103 ) payduedate, dateadd( DAY, abs(g.genother1), trnbelidate ) jt, (b.amtbelinettoidr ) amttrans, isnull(( SELECT abs(SUM(kredit)) kredit FROM " & _
            " ( SELECT ISNULL(bm.trnbelino, '') AS notrans,(ISNULL(ap.amtbayaridr,0.00)) AS kredit FROM QL_conap ap INNER JOIN QL_conap ap1 ON ap1.cmpcode = ap.cmpcode AND ap1.refoid = ap.refoid AND ap1.reftype = 'QL_TRNBELIMST' INNER JOIN QL_trnbelimst bm ON bm.cmpcode = ap.cmpcode AND bm.trnbelimstoid = ap.refoid INNER JOIN QL_trnpayap pap ON pap.cmpcode = ap.cmpcode AND pap.paymentoid = ap.payrefoid AND LOWER (pap.payreftype) = 'QL_TRNBELIMST' WHERE ap.suppoid = b.trnsuppoid AND ap.reftype = 'QL_trnpayap' AND ap.trnapstatus = 'POST' AND ( pap.payflag <> 'OTHER' OR ( pap.payflag = 'OTHER' AND pap.payamt < 0 )) AND ap.refoid = B.trnbelimstoid AND ap.payrefno NOT LIKE '%PR/%'" & _
            " UNION ALL" & _
            " SELECT ISNULL(bm.trnbelino, '') AS notrans, ABS(ISNULL(ap.amtbayaridr, 0.00)) AS kredit FROM QL_conap ap LEFT OUTER JOIN QL_trnbelimst bm ON bm.cmpcode = ap.cmpcode AND ap.reftype = 'QL_trnPAYAp' AND bm.trnbelimstoid = ap.refoid INNER JOIN QL_trnpayap pap ON pap.cmpcode = ap.cmpcode AND pap.paymentoid = ap.payrefoid AND LOWER (pap.payreftype) = 'ql_trnbelimst' WHERE ap.suppoid = b.trnsuppoid AND ap.reftype = 'QL_trnpayap' AND ap.trnapstatus = 'POST' AND pap.payflag = 'OTHER' AND pap.payamt < 0 AND ap.refoid = B.trnbelimstoid AND ap.payrefno NOT LIKE '%PR/%'" & _
            " UNION ALL" & _
            "  SELECT ISNULL(bm.trnbelino, '') AS notrans, ISNULL(ap.amtbayaridr,0.00) AS kredit FROM QL_conap ap INNER JOIN QL_trnbelimst bm ON bm.cmpcode = ap.cmpcode /*AND ap.reftype = 'QL_DebitNote'*/ AND bm.trnbelimstoid = ap.refoid INNER JOIN ql_debitnote pap ON pap.cmpcode = ap.cmpcode AND pap.oid = ap.payrefoid INNER JOIN ql_mstacctg a ON a.acctgoid = ap.acctgoid WHERE ap.suppoid = b.trnsuppoid AND ap.reftype IN ('ql_debitnote','ql_creditnote') AND ap.trnapstatus = 'POST' AND ap.refoid = B.trnbelimstoid AND ap.returoid = 0" & _
            " ) AS dt ), 0.00 ) amtpaid, b.amtretur amtretur,( SELECT TOP 1 AP.ACCTGOID FROM ql_conaP ap WHERE b.cmpcode = ap.cmpcode AND ap.reftype = 'ql_trnBELImst' AND b.trnBELImstoid = ap.refoid ) acctgoid, b.currencyoid currencyoid, b.currencyrate currencyrate, cr.currencycode, cr.currencydesc, b.trntaxpct, b.trnbelinote,c.reftype,ISnull((Select SUM(bd.amtdisc2) From QL_trnbelidtl bd Where bd.trnbelimstoid=b.trnbelimstoid ),0.00) AmtVo,b.trnbelipono,ISNULL((Select gp.gendesc From QL_mstgen gp Where gp.genoid=b.prefixoid And gp.gengroup='PREFIXSUPP'),'Other') PreFixSupp FROM ql_trnBELImst b INNER JOIN ql_mstSUPP s ON b.trnSUPPoid = s.SUPPoid AND b.trnSUPPoid = " & trnsuppoid.Text & " AND b.cmpcode = s.cmpcode INNER JOIN ql_mstgen g ON g.genoid = b.trnpaytype INNER JOIN ql_conap C ON B.trnbelimstoid = C.refoid AND C.reftype = 'QL_TRNBELIMST' INNER JOIN QL_mstcurr cr ON cr.currencyoid = b.currencyoid INNER JOIN ql_mstacctg a ON a.acctgoid = c.acctgoid WHERE (b.amtBELInetto - b.amtretur) - accumpayment > 0 AND trnBELIstatus IN ('Approved', 'POST') AND b.cmpcode = '" & cmpcode & "' AND b.trnBELIno NOT LIKE '%R' " & _
            " UNION ALL " & _
            " SELEcT DISTINCT b.trnbelifamstoid, b.suppoid AS trnsuppoid, s.suppname, b.trnbelifano AS trnbelino, cONVERT (cHAR(10), b.trnbelifadate, 103) trnbelidate, cONVERT ( cHAR (10), dateadd( DAY, abs(g.genother1), b.trnbelifadate ), 103 ) payduedate, dateadd( DAY, abs(g.genother1), b.trnbelifadate ) jt, (b.trnbelifaamtnetto) AS amttrans, isnull(( SELECT abs(SUM(kredit)) kredit FROM (SELECT (ISNULL(ap.amtbayaridr, 0.00)) AS kredit FROM QL_conap ap INNER JOIN QL_conap ap1 ON ap1.cmpcode = ap.cmpcode AND ap1.refoid = ap.refoid AND ap1.reftype = 'QL_trnbelimst_fa' INNER JOIN QL_trnbelimst_fa bm ON bm.cmpcode = ap.cmpcode AND bm.trnbelifamstoid = ap.refoid INNER JOIN QL_trnpayap pap ON pap.cmpcode = ap.cmpcode AND pap.paymentoid = ap.payrefoid AND LOWER (pap.payreftype) = 'QL_trnbelimst_fa' WHERE ap.suppoid = b.suppoid AND ap.reftype = 'QL_trnpayap' AND ap.trnapstatus = 'POST' AND ( pap.payflag <> 'OTHER' OR ( pap.payflag = '1' AND pap.payamt < 0 )) AND ap.refoid = b.trnbelifamstoid AND ap.payrefno <> '%PR/%'" & _
            " UNION ALL" & _
            " SELECT ISNULL(ap.amtbayaridr, 0.00) AS kredit FROM QL_conap ap LEFT OUTER JOIN QL_trnbelimst_fa bm ON bm.cmpcode = ap.cmpcode AND ap.reftype = 'QL_trnPAYAp' AND bm.trnbelifamstoid = ap.refoid INNER JOIN QL_trnpayap pap ON pap.cmpcode = ap.cmpcode AND pap.paymentoid = ap.payrefoid AND LOWER (pap.payreftype) = 'QL_trnbelimst_fa' WHERE ap.suppoid = b.suppoid AND ap.reftype = 'QL_trnpayap' AND ap.trnapstatus = 'POST' AND pap.payflag = '1' AND pap.payamt < 0 AND ap.refoid = b.trnbelifamstoid AND ap.payrefno <> '%PR/%' UNION ALL SELECT ABS(ISNULL(ap.amtbayaridr, 0)) AS kredit FROM QL_conap ap INNER JOIN QL_trnbelimst_fa bm ON bm.cmpcode = ap.cmpcode /*AND ap.reftype = 'QL_DebitNote'*/ AND bm.trnbelifamstoid = ap.refoid LEFT JOIN ql_debitnote pap ON pap.cmpcode = ap.cmpcode AND pap.oid = ap.payrefoid INNER JOIN ql_mstacctg a ON a.acctgoid = ap.acctgoid WHERE ap.suppoid = b.suppoid AND ap.reftype IN ('ql_debitnote','ql_creditnote') AND ap.trnapstatus = 'POST' AND ap.refoid = b.trnbelifamstoid AND ap.returoid = 0" & _
            " ) AS dt ),0.00 ) AS amtpaid, 0.00 AS amtretur, ( SELECT TOP 1 AP.AccTGOID FROM ql_conaP ap WHERE b.cmpcode = ap.cmpcode AND ap.reftype = 'ql_trnbelimst_fa' AND b.trnbelifamstoid = ap.refoid ) acctgoid, b.curroid AS currencyoid, b.beliratetoidr AS currencyrate, cr.currencycode, cr.currencydesc, b.trnbelifamsttaxpct AS trntaxpct, b.trnbelifamstnote AS trnbelinote,c.reftype,0.00 AmtVo,b.trnbelifano trnbelipono,ISNULL((Select gp.gendesc From QL_mstgen gp Where gp.genoid=s.prefixsupp And gp.gengroup='PREFIXSUPP'),'Other') PreFixSupp FROM QL_trnbelimst_fa b INNER JOIN ql_mstSUPP s ON b.suppoid = s.SUPPoid AND b.cmpcode = s.cmpcode INNER JOIN ql_mstgen g ON g.genoid = b.trnbelifatype INNER JOIN ql_conap c ON b.trnbelifamstoid = c.refoid AND c.reftype = 'ql_trnbelimst_fa' AND c.branch_code=b.branch_code INNER JOIN QL_mstcurr cr ON cr.currencyoid = b.curroid INNER JOIN ql_mstacctg a ON a.acctgoid = c.acctgoid WHERE (b.trnbelifaamtnetto )> 0 AND b.suppoid = " & trnsuppoid.Text & "  AND trnbelifamststatus IN ('POST') AND b.cmpcode = 'MSC' AND b.trnbelifano NOT LIKE '%R'" & _
            ") dt WHERE " & FilterTxt.SelectedValue & " LIKE '%" & Tchar(noNotaBeli.Trim) & "%' ORDER BY dt.jt"
        End If
        FillGV(gvPurchasing, sSql, "ql_trnbelimst")
        cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, True)
    End Sub

    Protected Sub ClearDtlAP(ByVal bState As Boolean)
        txtPaymentNo.Text = "" : trnbelino.Text = "" : trnbelimstoid.Text = ""
        amttrans.Text = "0.00" : amtpaid.Text = "0.00" : APAmt.Text = "0.00"
        APAcc.Text = "" : acctgoid.Text = "" : suppname.Text = ""
        amtpayment.Text = "0.00" : totalpayment.Text = "0.00"
        trnTaxPct.Text = "0.00" : TaxAmount.Text = "0.00"
        lblTaxPct.Visible = False : trnTaxPct.Visible = False
        lblAmtTax.Visible = False : TaxAmount.Visible = False
        invCurrOid.Text = "" : invCurrRate.Text = ""
        invCurrCode.Text = "" : invCurrDesc.Text = ""
        amtretur.Text = "0.00"
        chkOther.Checked = False : chkOther.Enabled = True
        otherAcctgoid.Enabled = True : otherAcctgoid.CssClass = "inpTextDisabled"
        otheramt.Text = "" : lkbAddDtlSlisih.Visible = False : lkbClearDtlSlisih.Visible = False
        If bState Then
            ' Reset Tabel Detail Selisih
            Session("DtlSelisih") = Nothing
            gvDtlSelisih.DataSource = Nothing
            gvDtlSelisih.DataBind()
            ClearDtlSelisih()
            txtNote.Text = ""
            'GVDtlPayAP.Columns(10).Visible = True
            GVDtlPayAP.SelectedIndex = -1
            I_U2.Text = "New Detail"
        End If
    End Sub

    Public Sub CheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("cashbankstatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Public Sub FillTextBox(ByVal vpayid As String)
        Try
            sSql = "SELECT DISTINCT cb.branch_code,cb.cashbankoid,cb.upduser,cb.updtime, case cashbanktype when 'BKM' then 'CASH' when 'BBM' then 'NONCASH' when 'BGM' then 'GIRO' when 'BDM' then 'DP' when 'BVM' then 'VOUCHER' end payflag,cashbankacctgoid, payrefno, paybankoid,ISNULL(payduedate,'1/1/1900') payduedate,cashbankstatus,suppname,p.suppoid,cb.cashbankdate,cb.upduser,cb.updtime,cashbanknote,cashbankno,cb.cashbankcurroid,cb.cashbankcurrate,ISNULL(p.realisasioid,0) AS RealOid,(Select cp.cashbankoid from ql_trncashbankmst cp Where cp.cashbankoid=cb.bankoid And cp.branch_code=cb.branch_code) apoid,(Select cp.cashbankno from ql_trncashbankmst cp Where cp.cashbankoid=cb.bankoid And cp.branch_code=cb.branch_code) apno FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap p ON p.cashbankoid=cb.cashbankoid INNER JOIN QL_mstSUPP s ON s.SUPPoid=p.SUPPoid AND s.cmpcode=p.cmpcode Where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid='" & vpayid & "'"

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objCmd.CommandText = sSql
            xreader = objCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    ddlcabang.SelectedValue = xreader("branch_code").ToString
                    cashbankoid.Text = xreader("cashbankoid")
                    cashbankno.Text = Trim(xreader("cashbankno").ToString)
                    defcbno.Text = Trim(xreader("cashbankno").ToString)
                    payflag.SelectedValue = xreader("payflag").ToString
                    initDDLcashbank(payflag.SelectedValue)
                    payduedate.Text = Format(xreader("payduedate"), "dd/MM/yyyy")
                    Session("payrefno") = Trim(xreader("payrefno"))
                    cashbankacctgoid.SelectedValue = Trim(xreader.GetValue(4))
                    RetcbOidAp.Text = Trim(xreader("apoid"))
                    ApNo.Text = Trim(xreader("apno").ToString)
                    CurrencyOid.SelectedValue = xreader("cashbankcurroid").ToString
                    currencyRate.Text = ToMaskEdit(ToDouble(xreader("cashbankcurrate").ToString), 2)
                    cashbanknote.Text = Trim(xreader("cashbanknote"))
                    updUser.Text = xreader("upduser")
                    updTime.Text = xreader("updtime")
                    trnsuppoid.Text = xreader("suppoid").ToString
                    PaymentDate.Text = Format(CDate(Trim(xreader("cashbankdate").ToString)), "dd/MM/yyyy")
                    lblPOST.Text = Trim(xreader("cashbankstatus").ToString)
                    If payflag.SelectedValue = "VOUCHER" Then
                        RealOid.Text = xreader("RealOid")
                    Else
                        RealOid.Text = 0
                    End If
                    Session("cashbankoid") = xreader("cashbankacctgoid")
                    If xreader("cashbankstatus").ToString = "POST" Then
                        CurrencyOid.Enabled = False : payflag.Enabled = False
                    End If
                End While
            End If
            conn.Close()
        Catch ex As Exception
            showMessage(ex.ToString & "<BR><BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
        If payflag.SelectedValue = "VOUCHER" Then
            RealNo.Visible = True
            RealNo.Text = GetStrData("Select realisasino FROM ql_trnrealisasi Where realisasioid=" & RealOid.Text & "")
        Else
            RealNo.Visible = False : RealOid.Text = ""
        End If

        Fill_payflag()
        payrefno.Text = Session("payrefno")
        cashbankacctgoid.SelectedValue = Session("cashbankoid")
        defcbno.Text = cashbankno.Text
        trndpapoid.Items.Clear()
        sSql = "select trndpapoid from ql_trndpap where trndpapno='" & payrefno.Text.Trim & "'"
        trndpapoid.Items.Add(payrefno.Text)
        Dim dpoid As String = GetStrData(sSql)
        If dpoid = "?" Then
            dpoid = 0
        Else
            dpoid = dpoid
        End If
        trndpapoid.Items(0).Value = dpoid
        conn.Close()
        If CurrencyOid.SelectedItem.Text = "IDR" Then
            sSql = "SELECT DISTINCT paymentoid, pap.payduedate, 1 trndpapoid, pap.DPAmtIDR AS DPAmt, pap.payrefno, pap.cashbankoid, pap.payreftype, pap.payrefoid, pap.payacctgoid, paynote, payamtIDR payamt, bm.trnbelino, bm.trnsuppoid, s.suppname, ap.amttransidr + ISNULL((SELECT SUM (db.amountidr) FROM QL_CREDITnote db WHERE db.refoid = BM.TRNBELIMSTOID AND db.reftype = 'HUTANG' AND DB.cmpcode = AP.CMPCODE ),0 ) amttrans, (SELECT isnull(SUM(p2.payamtidr), 0) FROM ql_trnpayaP p2 INNER JOIN ql_conap cp ON cp.payrefoid = p2.paymentoid AND cp.reftype = 'QL_trnpayaP' WHERE p2.cmpcode = cb.cmpcode AND p2.cashbankoid < cb.cashbankoid AND p2.paystatus = 'POST' AND p2.payrefoid = pap.payrefoid AND p2.payreftype = 'ql_trnBELImst' AND rtrim(p2.payflag) = '' AND cp.payrefno NOT LIKE '%PR/%') amtpaid, bm.amtreturidr amtretur, (SELECT acc.acctgcode + '-' + acc.acctgdesc FROM ql_mstacctg acc WHERE pap.payacctgoid = acc.acctgoid ) acctgdesc, bm.trntaxpct, '1' AS payseq, ISNULL(bm.currencyoid, 2) AS invCurrOid, ISNULL(bm.currencyrate, 1) AS invCurrRate, (SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode = bm.cmpcode AND cr.currencyoid = ISNULL(bm.currencyoid, 2)) AS invCurrCode, (SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode = bm.cmpcode AND cr.currencyoid = ISNULL(bm.currencyoid, 2)) AS invCurrDesc, pap.payamtidr invPayment, pap.payrefflag AS flagdtl,pap.realisasioid AS RealOid,pap.payres1 FROM ql_trnpayaP pap INNER JOIN ql_trncashbankmst cb ON cb.cmpcode = pap.cmpcode AND cb.cashbankoid = pap.cashbankoid INNER JOIN ql_trnBELImst bm ON bm.cmpcode = pap.cmpcode AND payrefoid = bm.trnBELImstoid INNER JOIN ql_mstSUPP s ON bm.cmpcode = s.cmpcode AND bm.trnSUPPoid = s.SUPPoid INNER JOIN ql_conaP ap ON bm.cmpcode = ap.cmpcode AND bm.trnBELImstoid = ap.refoid AND ap.reftype = 'ql_trnBELImst' WHERE cb.cashbankoid = " & vpayid & " AND pap.cmpcode = '" & cmpcode & "' AND cb.branch_code='" & ddlcabang.SelectedValue & "' " & _
            "UNION ALL " & _
            " SELECT DISTINCT paymentoid, pap.payduedate, 1 trndpapoid, pap.DPAmtIDR AS DPAmt, pap.payrefno, pap.cashbankoid, pap.payreftype, pap.payrefoid, pap.payacctgoid, paynote, payamtIDR payamt, bm.trnbelifano AS trnbelino, bm.suppoid AS trnsuppoid, s.suppname, ap.amttransidr + ISNULL(( SELECT SUM (db.amountidr) FROM QL_CREDITnote db WHERE db.refoid = bm.trnbelifamstoid AND db.reftype = 'HUTANG' AND DB.cmpcode = AP.CMPCODE ), 0 ) amttrans, (SELECT isnull(SUM(p2.payamtidr), 0) FROM ql_trnpayaP p2 INNER JOIN ql_conap cp ON cp.payrefoid = p2.paymentoid AND cp.reftype = 'QL_trnpayaP' WHERE p2.cmpcode = cb.cmpcode AND p2.cashbankoid < cb.cashbankoid AND p2.paystatus = 'POST' AND p2.payrefoid = pap.payrefoid AND p2.payreftype = 'QL_trnbelimst_fa' AND rtrim(p2.payflag) = '' AND cp.payrefno NOT LIKE '%PR/%' ) amtpaid, 0.00 AS amtretur, ( SELECT acc.acctgcode + '-' + acc.acctgdesc FROM ql_mstacctg acc WHERE pap.payacctgoid = acc.acctgoid ) acctgdesc, bm.trnbelifamsttaxpct AS trntaxpct, '1' AS payseq, ISNULL(bm.curroid, 2) AS invCurrOid, ISNULL(bm.rateoid, 1) AS invCurrRate, (SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode = bm.cmpcode AND cr.currencyoid = ISNULL(bm.curroid, 2)) AS invCurrCode, ( SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode = bm.cmpcode AND cr.currencyoid = ISNULL(bm.curroid, 2)) AS invCurrDesc, pap.payamtidr invPayment, pap.payrefflag AS flagdtl,pap.realisasioid AS RealOid, pap.payres1 FROM ql_trnpayap pap INNER JOIN ql_trncashbankmst cb ON cb.cmpcode = pap.cmpcode AND cb.cashbankoid = pap.cashbankoid INNER JOIN QL_trnbelimst_fa bm ON bm.cmpcode = pap.cmpcode AND payrefoid = bm.trnbelifamstoid INNER JOIN ql_mstSUPP s ON bm.cmpcode = s.cmpcode AND bm.SUPPoid = s.SUPPoid INNER JOIN ql_conaP ap ON bm.cmpcode = ap.cmpcode AND bm.trnbelifamstoid = ap.refoid AND ap.reftype IN ('QL_trnbelimst_fa') WHERE cb.cashbankoid = " & vpayid & " AND pap.cmpcode = '" & cmpcode & "' AND cb.branch_code='" & ddlcabang.SelectedValue & "' "
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet

        mySqlDA.Fill(objDs, "dataCost")
        Dim dv As DataView = objDs.Tables("dataCost").DefaultView
        Session("dtlTable") = objDs.Tables("dataCost")
        conn.Close()
        Dim dtlTable As DataTable
        dtlTable = Session("dtlTable")

        If dtlTable.Rows.Count > 0 Then
            'find data acctg
            Dim sVar As String = "VAR_DPAP"
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='MSC' AND interfacevar='" & sVar & "' AND interfaceres1='" & ddlcabang.SelectedValue & "'"
            Dim sCode As String = GetStrData(sSql)
            If sCode = "" Then
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='MSC' AND interfacevar='" & sVar & "' AND interfaceres1='MSC'"
                sCode = GetStrData(sSql)
            End If
            If sCode <> "" Then
                sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='MSC' AND ("
                Dim sSplitCode() As String = sCode.Split(",")
                For C1 As Integer = 0 To sSplitCode.Length - 1
                    sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                    If C1 < sSplitCode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            End If
            Dim DP As DataTable = cKoneksi.ambiltabel(sSql, "acctgoid_dp")

            For C2 As Integer = 0 To dtlTable.Rows.Count - 1
                Dim ed As DataRow = dtlTable.Rows(C2)
                ed.BeginEdit()
                ed("payseq") += C2
                If DP.Rows.Count > 0 Then
                    For c1 As Int16 = 0 To DP.Rows.Count - 1
                        If DP.Rows(c1).Item("acctgoid") = ed("PAYacctgoid").ToString Then
                            ed("trndpapoid") = 1
                        End If
                    Next
                End If
                ed.EndEdit()
            Next
            Session("invCurrOid") = dtlTable.Rows(0).Item("invCurrOid")
            suppnames.Text = dtlTable.Rows(0).Item("suppname")
            trnsuppoid.Text = dtlTable.Rows(0).Item("trnsuppoid")
            Payseq.Text = dtlTable.Rows.Count + 1
            Session("ItemLinePayment") = Payseq.Text
            GVDtlPayAP.Visible = True
            Session("tbldtl") = dtlTable
            GVDtlPayAP.DataSource = dtlTable
            GVDtlPayAP.DataBind()
            calcTotalInGridDtl()
        Else
            showMessage("Tidak dapat membuka data detail pembayaran!!!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        dpbalance.Text = ToMaskEdit(ToDouble(amtbelinettodtl.Text), 2)
        cashbankacctgoid.SelectedValue = Session("cashbankoid")
    End Sub

    Protected Sub initddlDP(ByVal suppoid As String)
        FillDDL(ddlDPNo, "SELECT trndpaPoid, trndpaPno FROM QL_trndpaP WHERE cmpcode='" & cmpcode & "' AND SUPPoid=" & suppoid & " AND trndpaPflag='OPEN'")
    End Sub

    Private Sub InitOtherAcctg(ByVal sType As String)
        If sType = "+" Then
            'FillDDL(otherAcctgoid, sSql)
            FillDDLAcctg(otherAcctgoid, "VAR_PAYAP_DIFFERENCE_+1", ddlcabang.SelectedValue)
            If otherAcctgoid.Items.Count = 0 Then
                showMessage("Tolong atur VAR_PAYAP_DIFFERENCE_+1 di master interface !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        ElseIf sType = "-" Then
            FillDDLAcctg(otherAcctgoid, "VAR_PAYAP_DIFFERENCE_-1", ddlcabang.SelectedValue)
            If otherAcctgoid.Items.Count = 0 Then
                showMessage("Tolong atur VAR_PAYAP_DIFFERENCE_-1 di master interface  !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchAP")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchAP") = sqlSearch
            Response.Redirect(Page.AppRelativeVirtualPath) '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Koreksi Payment AP"
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are You Sure To Delete ?');")
        btnPosting2.Attributes.Add("OnClick", "javascript:return confirm('Are You Sure To POST ?');")
        I_U.Text = "New Data"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = " SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            CutOffDate = CDate(toDate(GetStrData(sSql)))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        If Not IsPostBack Then
            initDDLcashbank(payflag.SelectedValue)
            InitAllDDL() : initCbg()
            payflag.SelectedIndex = 0
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                I_U.Text = "Update Data"
                PaymentDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                trndpapoid.Enabled = False : btnSearchSupp.Visible = False
                PaymentDate.Enabled = False
                PaymentDate.CssClass = "inpTextDisabled" : btnPayDate.Visible = False

                If lblPOST.Text = "POST" Then
                    btnPosting2.Visible = False : btnSave.Visible = False
                    btnDelete.Visible = False : ibtn.Visible = False
                Else
                    btnPosting2.Visible = True : btnSave.Visible = True
                    btnDelete.Visible = True : ibtn.Visible = True
                End If
            Else
                I_U.Text = "New Data"
                Session("mstoid") = GenerateID("QL_trnpayap", cmpcode)
                txtPaymentNo.Text = Session("mstoid")
                Session("mstoid2") = GenerateID("QL_trncashbankmst", cmpcode)
                cashbankoid.Text = Session("mstoid2")
                btnDelete.Visible = False : btnPrint.Visible = False
                lblPOST.Text = "" : Payseq.Text = 1
                Session("ItemLinePayment") = Payseq.Text
                PaymentDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime, "dd/MM/yyyy")
                updUser.Text = Session("UserID")
                updTime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0
                Fill_payflag()
                btnSearchSupp.Visible = True
            End If
            txtPeriode1.Text = Format(GetServerTime, "01/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            binddata()

            Dim objTable As DataTable : objTable = Session("tbldtl")
            GVDtlPayAP.DataSource = objTable : GVDtlPayAP.DataBind()
            calcTotalInGridDtl()
            Dim objTable2 As DataTable : objTable2 = Session("DtlSelisih")
            gvDtlSelisih.DataSource = objTable2 : gvDtlSelisih.DataBind()
        End If
        If lblPOST.Text = "POST" Then
            btnshowCOA.Visible = True : btnPrint.Visible = False
        Else
            btnshowCOA.Visible = False : btnPrint.Visible = False
        End If
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payflag.SelectedIndexChanged
        Fill_payflag()
    End Sub

    Protected Sub btnSearchPurchasing_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPurchasing.Click
        If trnsuppoid.Text.Trim = "" Then
            showMessage("Silahkan pilih supplier !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        bindDataPI(cmpcode, txtInputNotaBeli.Text)
    End Sub

    Protected Sub gvPurchasing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        chkOther.Checked = False
        trnbelimstoid.Text = gvPurchasing.SelectedDataKey(0).ToString()
        trnbelino.Text = gvPurchasing.SelectedDataKey(1).ToString()
        suppname.Text = gvPurchasing.SelectedDataKey("suppname").ToString()
        acctgoid.Text = gvPurchasing.SelectedDataKey("acctgoid").ToString()
        amttrans.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amttrans").ToString), 2)
        RefTypeTbl.Text = gvPurchasing.SelectedDataKey("reftype").ToString()
        Dim iconaroid As Int64 = ToDouble(GetStrData("select top 1 conapoid from QL_conap where refoid=" & trnbelimstoid.Text & " And reftype='QL_trnbelimst' order by conapoid asc"))
        If ToDouble(acctgoid.Text) = 0 Then
            'insert to conar & get acctgoid piutang from mst interface
            Dim Var_AR_x As String = GetVarInterface("VAR_AP", cmpcode)
            acctgoid.Text = GetStrData("select acctgoid from ql_mstacctg where acctgcode ='" & Var_AR_x & "'")
            If iconaroid > 0 Then 'update acctgnya aja deh
                sSql = "update QL_conaP set acctgoid=" & acctgoid.Text & " where cmpcode='" & cmpcode & "' and conaPoid=" & iconaroid
                cKoneksi.ambilscalar(sSql)
            Else
                iconaroid = GenerateID("QL_conaP", cmpcode)
                sSql = "INSERT INTO QL_conaP(cmpcode,conaPoid,reftype,refoid,payrefoid,SUPPoid,acctgoid,trnaPstatus,trnaPtype,trnaPdate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amtbayar,trnaPnote,trnaPres1,upduser,updtime) VALUES ('" & cmpcode & "'," & iconaroid & ",'ql_trnBELImst'," & trnbelimstoid.Text & ",0," & trnsuppoid.Text & ",'" & acctgoid.Text & "','POST','AP','" & CDate(toDate(gvPurchasing.SelectedDataKey("trnbelidate"))) & "','" & GetDateToPeriodAcctg3(CDate(toDate(gvPurchasing.SelectedDataKey("trnbelidate")))) & "',0,'1/1/1900','',0,'" & CDate(toDate(gvPurchasing.SelectedDataKey("payduedate"))) & "'," & ToDouble(amttrans.Text) & ",0,'" & Tchar(defcbno.Text) & "','','" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                cKoneksi.ambilscalar(sSql)

                ' update lastoid ql_conar
                sSql = "update QL_mstoid set lastoid=" & iconaroid & " where tablename ='ql_conaP' and cmpcode = '" & cmpcode & "' "
                cKoneksi.ambilscalar(sSql)
            End If

            APAcc.Text = cKoneksi.ambilscalar("SELECT '('+acctgcode+') '+acctgdesc FROM ql_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgoid='" & acctgoid.Text & "'")
        End If

        APAcc.Text = cKoneksi.ambilscalar("SELECT acctgcode+'-'+acctgdesc FROM ql_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgoid='" & acctgoid.Text & "'")

        invCurrOid.Text = gvPurchasing.SelectedDataKey("currencyoid").ToString()
        invCurrRate.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("currencyrate").ToString()), 2)
        invCurrCode.Text = gvPurchasing.SelectedDataKey("currencycode").ToString()
        invCurrDesc.Text = gvPurchasing.SelectedDataKey("currencycode").ToString() & "-" & gvPurchasing.SelectedDataKey("currencydesc").ToString()
        CekPaymentCurrencyToInvoice()

        amttrans.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amttrans").ToString), 2)
        amtpaid.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amtpaid").ToString), 2)
        amtretur.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amtretur").ToString), 2)
        APAmt.Text = ToMaskEdit(ToDouble(amttrans.Text) - (ToDouble(amtpaid.Text) + ToDouble(amtretur.Text)), 2)
        totalpayment.Text = ToMaskEdit(ToDouble(APAmt.Text), 2)

        If payflag.SelectedValue = "DP" Then
            initddlDP(cKoneksi.ambilscalar("SELECT ISNULL(trnSUPPoid,0) FROM QL_trnbelimst WHERE cmpcode='" & cmpcode & "' AND trnBELImstoid=" & trnbelimstoid.Text))
        End If
        If payflag.SelectedValue = "VOUCHER" Then
            If dpbalance.Text - APAmt.Text > 0 Then
                amtpayment.Text = ToMaskEdit(ToDouble(APAmt.Text), 2)
            Else
                amtpayment.Text = ToMaskEdit(ToDouble(dpbalance.Text), 2)
            End If
            If ToDouble(amttrans.Text) < ToDouble(AmtReal.Text) Then
                showMessage("Maaf, amount hutang tidak boleh < amount realisasi", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        Else
            amtpayment.Text = ToMaskEdit(ToDouble(APAmt.Text), 2)
        End If
        'If ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("AmtVo").ToString), 2) <> 0 Then
        'BindVoPO(gvPurchasing.SelectedDataKey("trnbelipono").ToString(), trnbelimstoid.Text)
        'End If
        txtInputNotaBeli.Text = ""
        CalculateTotalPayment()
        cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, False)
        cProc.DisposeGridView(gvPurchasing)
    End Sub

    Protected Sub calcTotalInGridDtl()
        Dim gtotal As Double = 0
        If Not (Session("tbldtl") Is Nothing) Then
            Dim objTable As DataTable : objTable = Session("tbldtl")
            gtotal = ToDouble(objTable.Compute("SUM(payamt)", "").ToString)
        End If
        amtbelinettodtl.Text = ToMaskEdit(gtotal, 2)
        amtbelinettodtl4.Text = ToMaskEdit(gtotal, 2)
        NetPayment.Text = ToMaskEdit(ToDouble(amtbelinettodtl.Text) - ToDouble(TotalCost.Text), 2)
    End Sub

    Protected Sub ClosePurc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hiddenbtnpur.Visible = False : Panel2.Visible = False : ModalPopupExtender2.Hide()
        gvPurchasing.DataSource = Nothing : gvPurchasing.DataBind()
        txtInputNotaBeli.Text = ""
    End Sub

    Protected Sub GVDtlPayAP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlPayAP.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
        End If
    End Sub

    Protected Sub GVDtlPayAP_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlPayAP.RowDeleting
        If lblPOST.Text <> "POST" Then
            Dim idx As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("tbldtl")

            ' CEK apakah induk transaksi (utang) ato bukan
            Dim dvTemp As DataView = objTable.DefaultView
            dvTemp.RowFilter = "payseq=" & idx + 1
            ' Get Id Induk 
            Dim idInvoice As Integer = objTable.Rows(e.RowIndex).Item("payrefoid")
            Dim payreftype As String = objTable.Rows(e.RowIndex).Item("payreftype")
            If dvTemp.Count > 0 Then
                If dvTemp(0)("flagdtl") = "OTHER" Then
                    showMessage("pilih induk Transaksi !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If
            dvTemp.RowFilter = ""

            ' DELETE data bila ada
            For C1 As Integer = objTable.Rows.Count - 1 To 0 Step -1
                If objTable.Rows(C1).Item("payrefoid") = idInvoice And objTable.Rows(C1).Item("payreftype").ToString.ToUpper = payreftype.ToUpper Then
                    objTable.Rows.RemoveAt(C1)
                End If
            Next

            'resequence Detial 
            For C2 As Int16 = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C2)
                dr.BeginEdit() : dr("payseq") = C2 + 1 : dr.EndEdit()
            Next

            Session("tbldtl") = objTable
            GVDtlPayAP.DataSource = Session("tbldtl")
            GVDtlPayAP.DataBind()
            Payseq.Text = objTable.Rows.Count + 1
            Session("ItemLinePayment") = Payseq.Text
            calcTotalInGridDtl()
        Else
            e.Cancel = True
        End If
    End Sub

    Protected Sub ibtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtn.Click
        Dim sMsg As String = ""
        If trnbelimstoid.Text = "" Then
            sMsg &= "- Pilih invoice !!<BR>" : End If
        If ToDouble(amtpayment.Text) <= 0 Then
            sMsg &= "- Jumlah pembayaran harus > 0 !!<BR>"
        End If
        If payflag.SelectedValue.ToUpper = "VOUCHER" Then
            If ToDouble(amttrans.Text) < ToDouble(AmtReal.Text) Then
                sMsg &= "Maaf, amount hutang tidak boleh < amount realisasi"
            ElseIf ToDouble(amtpayment.Text) <> ToDouble(AmtReal.Text) Then
                sMsg &= "Maaf, amount pembayaran tidak boleh < amount realisasi"
            End If
        End If
        'If payduedate.Text.Trim = "" And payflag.SelectedValue = "CASH" Then
        '    payduedate.Text = "01/01/1900"
        'End If

        If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
            payduedate.Text = PaymentDate.Text
        End If

        If chkOther.Checked Then
            If Session("DtlSelisih") Is Nothing Then
                sMsg &= "- Tidak ada detail perbedaan pembayaran !!<BR>"
            Else
                Dim objCek As DataTable = Session("DtlSelisih")
                If objCek.Rows.Count < 1 Then
                    sMsg &= "- Tidak ada detail perbedaan pembayaran !!<BR>"
                Else
                    Dim dTotalSelisih As Double = ToMaskEdit(ToDouble(objCek.Compute("SUM(amtdtlselisih)", "").ToString), 2)
                    If dTotalSelisih > ToDouble(ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 2)) Then
                        sMsg &= "- Maksimal jumlah total detail di perbedaan bayar = " & ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 2) & " !!<BR>"
                    End If
                    If DDLOtherType.SelectedValue = "+" Then
                        If dTotalSelisih <> ToDouble(ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 2)) Then
                            sMsg &= "- Jumlah total detail di perbedaan bayar harus = " & ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 2) & " !!<BR>"
                        End If
                    End If
                End If
            End If
        End If
        If txtNote.Text.Trim.Length > 200 Then
            sMsg &= "- Maksimal catatan detail adalah 200 karakter !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        'GET ACCTGOID DARI ALL DP
        Dim VAR_DPAR As String = GetVarInterface("VAR_DPAR", cmpcode)

        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_DPAR & "%'  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
        Session("acctgoid_dp") = cKoneksi.ambiltabel(sSql, "acctgoid_dp")

        If (Session("oid") = Nothing Or Session("oid") = "") And Session("tbldtl") Is Nothing Then
            Dim dtlTable As DataTable = SetTabelDetail()
            Session("tbldtl") = dtlTable
        End If

        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        If I_U2.Text = "New Detail" Then
            dv.RowFilter = "payrefoid='" & Trim(trnbelimstoid.Text) & "' AND flagdtl<>'OTHER' "
        Else
            dv.RowFilter = "payrefoid='" & Trim(trnbelimstoid.Text) & "' AND flagdtl<>'OTHER' AND payseq<>" & Payseq.Text
        End If
        If dv.Count > 0 Then
            showMessage("Data sudah ditambahkan sebelumnya !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            dv.RowFilter = ""
            GVDtlPayAP.DataSource = Session("tbldtl")
            GVDtlPayAP.DataBind()
            Exit Sub
        End If
        dv.RowFilter = ""
        ' DELETE data bila ada
        For C1 As Integer = objTable.Rows.Count - 1 To 0 Step -1
            If objTable.Rows(C1).Item("payrefoid") = trnbelimstoid.Text Then
                objTable.Rows.RemoveAt(C1)
            End If
        Next

        Dim oRow As DataRow
        oRow = objTable.NewRow
        oRow("payseq") = objTable.Rows.Count + 1
        oRow("paymentoid") = 0
        oRow("cashbankoid") = 0
        oRow("payreftype") = RefTypeTbl.Text
        oRow("payrefoid") = trnbelimstoid.Text
        oRow("payacctgoid") = acctgoid.Text
        oRow("paynote") = txtNote.Text
        If chkOther.Checked = True Then
            If DDLOtherType.SelectedValue = "+" Then
                oRow("payamt") = ToDouble(APAmt.Text)
                oRow("payres1") = "LEBIH BAYAR"
            Else
                oRow("payamt") = ToDouble(totalpayment.Text)
                oRow("payres1") = "KURANG BAYAR"
            End If
        Else
            oRow("payamt") = ToDouble(amtpayment.Text)
            oRow("payres1") = "NORMAL"
        End If
        oRow("trnbelino") = trnbelino.Text
        oRow("suppname") = suppname.Text
        oRow("amttrans") = ToDouble(amttrans.Text)
        oRow("amtpaid") = ToDouble(amtpaid.Text)
        oRow("amtretur") = ToDouble(amtretur.Text)
        oRow("acctgdesc") = APAcc.Text
        oRow("trntaxpct") = ToDouble(trnTaxPct.Text)
        oRow("invCurrOid") = invCurrOid.Text
        oRow("invCurrRate") = ToDouble(invCurrRate.Text)
        oRow("invCurrCode") = invCurrCode.Text
        oRow("invCurrDesc") = invCurrDesc.Text
        oRow("invPayment") = ToDouble(amtpayment.Text)
        Session("invCurrOid") = invCurrOid.Text
        If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
            payduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
        oRow("payduedate") = "01/01/1900" ''toDate(payduedate.Text)
        oRow("payrefno") = payrefno.Text
        oRow("flagdtl") = ""
        oRow("trndpapoid") = 0
        oRow("DPAmt") = 0
        If payflag.SelectedValue = "VOUCHER" Then
            oRow("RealOid") = RealOid.Text
        Else
            oRow("RealOid") = 0
        End If
        objTable.Rows.Add(oRow)

        ' Bila ada selisih
        If chkOther.Checked Then
            Dim tbSls As DataTable = Session("DtlSelisih")
            Dim dTotalSelisih As Double = ToDouble(tbSls.Compute("SUM(amtdtlselisih)", "").ToString)
            For C2 As Integer = 0 To tbSls.Rows.Count - 1
                Dim PlusRow As DataRow
                PlusRow = objTable.NewRow
                PlusRow("payseq") = objTable.Rows.Count + 1
                PlusRow("paymentoid") = 0
                PlusRow("cashbankoid") = 0
                PlusRow("payreftype") = RefTypeTbl.Text
                PlusRow("payrefoid") = trnbelimstoid.Text
                PlusRow("payacctgoid") = tbSls.Rows(C2)("acctgoid").ToString
                PlusRow("paynote") = tbSls.Rows(C2)("dtlnoteselisih").ToString
                If DDLOtherType.SelectedValue = "+" Then
                    PlusRow("payamt") = ToDouble(tbSls.Rows(C2)("amtdtlselisih").ToString)
                    PlusRow("invPayment") = ToDouble(amtpayment.Text) - dTotalSelisih
                    PlusRow("payres1") = "LEBIH BAYAR"
                Else
                    PlusRow("invPayment") = ToDouble(amtpayment.Text) + dTotalSelisih
                    PlusRow("payamt") = ToDouble(tbSls.Rows(C2)("amtdtlselisih").ToString) * -1
                    PlusRow("payres1") = "KURANG BAYAR"
                End If
                PlusRow("trnbelino") = trnbelino.Text
                PlusRow("suppname") = suppname.Text
                PlusRow("amttrans") = ToDouble(amttrans.Text)
                PlusRow("amtpaid") = ToDouble(amtpaid.Text)
                PlusRow("amtretur") = ToDouble(amtretur.Text)
                PlusRow("acctgdesc") = tbSls.Rows(C2)("acctgdesc").ToString
                PlusRow("trntaxpct") = ToDouble(trnTaxPct.Text)
                PlusRow("invCurrOid") = invCurrOid.Text
                PlusRow("invCurrRate") = ToDouble(invCurrRate.Text)
                PlusRow("invCurrCode") = invCurrCode.Text
                PlusRow("invCurrDesc") = invCurrDesc.Text
                If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
                    payduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                End If
                PlusRow("payduedate") = toDate(payduedate.Text)
                PlusRow("payrefno") = payrefno.Text
                PlusRow("flagdtl") = "OTHER"
                PlusRow("trndpapoid") = 0
                Dim otableDP As DataTable = Session("acctgoid_dp")
                If otableDP.Rows.Count > 0 Then
                    For c1 As Int16 = 0 To otableDP.Rows.Count - 1
                        If otableDP.Rows(c1).Item("acctgoid") = tbSls.Rows(C2)("acctgoid").ToString Then
                            PlusRow("trndpapoid") = 1
                        End If
                    Next
                End If
                If payflag.SelectedValue = "VOUCHER" Then
                    oRow("RealOid") = RealOid.Text
                Else
                    oRow("RealOid") = 0
                End If
                PlusRow("DPAmt") = 0
                objTable.Rows.Add(PlusRow)
            Next
        End If
        ''resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            objTable.Rows(C2)("payseq") = C2 + 1
        Next

        Session("tbldtl") = objTable
        GVDtlPayAP.Columns(11).Visible = True
        GVDtlPayAP.DataSource = Session("tbldtl")
        GVDtlPayAP.DataBind()
        ClearDtlAP(True)
        calcTotalInGridDtl()
        GVDtlPayAP.SelectedIndex = -1

    End Sub

    Protected Sub GVDtlPayAP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtlPayAP.SelectedIndexChanged
        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dvTemp As DataView = objTable.DefaultView
        Payseq.Text = GVDtlPayAP.SelectedDataKey.Item("payseq").ToString
        dvTemp.RowFilter = "payseq=" & Payseq.Text
        trnbelimstoid.Text = dvTemp(0)("payrefoid").ToString
        Dim dtlSelisih As Double = 0 : Dim dTotSelisih As Double = 0
        If dvTemp.Count > 0 Then
            If dvTemp(0)("flagdtl") = "OTHER" Then
                showMessage("Pilih induk transaksi", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                dvTemp.RowFilter = ""
                Exit Sub
            End If
        End If
        dvTemp.RowFilter = ""

        ' Get Table Detail Selisih
        gvDtlSelisih.DataSource = Nothing
        gvDtlSelisih.DataBind()
        otheramt.Text = "0.00"
        dvTemp.RowFilter = "payrefoid=" & trnbelimstoid.Text & " AND flagdtl='OTHER'"

        If dvTemp.Count > 0 Then
            chkOther.Enabled = True : chkOther.Checked = True
            Dim tbSelisih As DataTable = SetTabelDetailSlisih()
            For C4 As Integer = 0 To dvTemp.Count - 1
                Dim nuRow As DataRow : nuRow = tbSelisih.NewRow
                nuRow("selisihseq") = C4 + 1
                nuRow("acctgoid") = dvTemp(C4)("payacctgoid").ToString
                nuRow("acctgdesc") = dvTemp(C4)("acctgdesc").ToString
                nuRow("amtdtlselisih") = Math.Abs(ToDouble(dvTemp(C4)("payamt").ToString))
                dtlSelisih = ToDouble(dvTemp(C4)("payamt").ToString)
                nuRow("dtlnoteselisih") = dvTemp(C4)("paynote").ToString
                dTotSelisih = Math.Abs(ToDouble(dvTemp(C4)("payamt").ToString))
                tbSelisih.Rows.Add(nuRow)
            Next

            ' Cek kurang bayar atau lebih bayar
            If dtlSelisih < 0 Then ' lebih bayar
                DDLOtherType.SelectedValue = "-"
                chkOther.Enabled = True
                SetOtherAccount(chkOther.Checked)
                InitOtherAcctg(DDLOtherType.SelectedValue)
                'chkOther_CheckedChanged(Nothing, Nothing)
            Else ' kurang bayar
                DDLOtherType.SelectedValue = "+"
                'chkOther.Enabled = False
                'chkOther_CheckedChanged(Nothing, Nothing)
                'SetOtherAccount(chkOther.Checked)
                InitOtherAcctg(DDLOtherType.SelectedValue)
            End If
            amtdtlselisih.Text = ToMaskEdit(Math.Abs(ToDouble(dTotSelisih)), 2)
            otheramt.Text = ToMaskEdit(Math.Abs(ToDouble(dTotSelisih)), 2)
            Session("DtlSelisih") = tbSelisih
            gvDtlSelisih.DataSource = tbSelisih
            gvDtlSelisih.DataBind()
        End If
        dvTemp.RowFilter = ""
        ' Detail Payap
        dvTemp.RowFilter = "payseq=" & Payseq.Text
        trnbelimstoid.Text = dvTemp(0)("payrefoid").ToString
        trnbelino.Text = dvTemp(0)("trnbelino").ToString
        suppname.Text = dvTemp(0)("suppname").ToString
        acctgoid.Text = dvTemp(0)("payacctgoid").ToString
        APAcc.Text = dvTemp(0)("acctgdesc").ToString
        invCurrOid.Text = dvTemp(0)("invCurrOid").ToString
        invCurrRate.Text = ToMaskEdit(ToDouble(dvTemp(0)("invCurrRate").ToString), 3)
        invCurrCode.Text = dvTemp(0)("invCurrCode").ToString
        invCurrDesc.Text = dvTemp(0)("invCurrDesc").ToString
        amttrans.Text = ToMaskEdit(ToDouble(dvTemp(0)("amttrans").ToString), 2)
        amtpaid.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtpaid").ToString), 2)
        amtretur.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtretur").ToString), 2)
        APAmt.Text = ToMaskEdit(ToDouble(amttrans.Text) - (ToDouble(amtpaid.Text) + ToDouble(amtretur.Text)), 2)

        If chkOther.Checked Then
            If DDLOtherType.SelectedValue = "+" Then ' Lebih Bayar
                totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) + Math.Abs(dTotSelisih), 2)
                amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) + Math.Abs(dTotSelisih), 2)
            Else ' Kurang Bayar
                totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 2)
                amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) - Math.Abs(dTotSelisih), 2)
            End If
        Else ' Tanpa Selisih
            totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 2)
            amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 2)
        End If

        'If amtpayment.Text = APAmt.Text Then
        '    chkOther.Enabled = False
        'End If

        trnTaxPct.Text = ToMaskEdit(ToDouble(dvTemp(0)("trntaxpct").ToString), 3)
        txtNote.Text = dvTemp(0)("paynote").ToString
        ' payduedate.Text = CDate(dvTemp(0)("payduedate").ToString)
        payrefno.Text = dvTemp(0)("payrefno").ToString
        If CInt(dvTemp(0)("trndpapoid")) > 0 Then
            ddlDPNo.Visible = True : cbDP.Checked = True
            DPAmt.Visible = True : lblDPAmount.Visible = True
            ddlDPNo.SelectedValue = dvTemp(0)("trndpapoid").ToString
        Else
            ddlDPNo.Visible = False
            DPAmt.Visible = False : lblDPAmount.Visible = False
            If dvTemp.Count > 0 Then
                cbDP.Checked = True
            Else
                cbDP.Checked = False
            End If
        End If
        DPAmt.Text = ToMaskEdit(dvTemp(0)("DPAmt").ToString, 3)
        dvTemp.RowFilter = ""
        ' CalculateSelisih()
        GVDtlPayAP.Columns(11).Visible = False
        I_U2.Text = "Update Data"
        'MultiView1.ActiveViewIndex = 1
        CalculateTotalPayment()
        If dTotSelisih <> 0 Then ' lebih bayar
            chkOther.Checked = True
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        GVmstPAYAP.PageIndex = 0
        binddata()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        txtPeriode1.Text = Format(GetServerTime().AddDays(-1), "dd/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        postinge.SelectedIndex = 0 : ddlFilter.SelectedIndex = 0
        orderby.SelectedIndex = 0 : FilterText.Text = ""
        binddata()
    End Sub

    Protected Sub cashbankacctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
    End Sub

    Protected Sub btnClearPurchase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP(True)
    End Sub

    Protected Sub gvPurchasing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPurchasing.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            ' e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
            ' e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 2)
        End If
    End Sub

    Protected Sub currencyRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ToDouble(currencyRate.Text) <= 0 Then
            FillCurrencyRate(CurrencyOid.SelectedValue)
        End If
        ClearDtlAP(True) : Session("tbldtl") = Nothing
        GVDtlPayAP.DataSource = Nothing : GVDtlPayAP.DataBind()
    End Sub

    Protected Sub LBPost_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnPosting2_Click(sender, e)
    End Sub

    Protected Sub ibtnacctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub CBTax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initDDLcashbank(payflag.SelectedValue)
    End Sub

    Protected Sub GVmstPAYAP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstPAYAP.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            If e.Row.Cells(7).Text.Trim = "&nbsp;" Then
                e.Row.Cells(7).Text = "In Process"
            End If

            'Dim HyperLink1 As HyperLink = CType(e.Row.Cells(0).FindControl("HyperLink1"), HyperLink)
            'Dim objGrid As GridView = CType(e.Row.Cells(5).FindControl("gvsubmst"), GridView)
            'sSql = "select distinct b.trnbelino from ql_trnpayap p inner join ql_trnbelimst b on b.trnbelimstoid=p.payrefoid  where p.cashbankoid in (select cashbankoid from QL_trncashbankmst where cashbankno='" & HyperLink1.Text & "') "
            'Dim ods As New SqlDataAdapter(sSql, conn2)
            'Dim objTablee As New DataTable
            'ods.Fill(objTablee)
            'objGrid.DataSource = objTablee.DefaultView
            'objGrid.DataBind()
        End If
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
        If lblState.Text = "INV" Then
            lblState.Text = "" : ModalPopupExtender2.Show()
        End If
    End Sub

    Protected Sub imbFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindInv.Click
        bindDataPI(cmpcode, txtInputNotaBeli.Text)
    End Sub

    Protected Sub imbViewAllInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataPI(cmpcode, "") : txtInputNotaBeli.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP(True) : ClearDtlSelisih()
        Payseq.Text = Session("ItemLinePayment")
        RealOid.Text = 0 : RealNo.Text = ""
        'txtPaymentNo.Text = Session("mstoid")
        'ReAmount()
    End Sub

    Protected Sub lkbMore0_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub lkbInfo1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lkbInfo2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lkbDetail2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("oid") = Nothing : Session("tbldtl") = Nothing
        'Session("tbldtlcost") = Nothing
        Response.Redirect("~\Accounting\trnReturAP.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Try
            sSql = "select cashbankstatus from ql_trncashbankmst Where cashbankoid=" & cashbankoid.Text
            If (GetStrData(sSql) = "POST") Then
                showMessage("Data tidak dapat dihapus karena sudah diposting!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End If
        Catch ex As Exception

        End Try
        Dim strSQL As String
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            strSQL = "Delete from QL_conap where cmpcode='" & cmpcode & "' AND payrefoid IN (SELECT paymentoid FROM QL_trnpayap WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text & ")"

            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            strSQL = "Delete from QL_trnpayap where cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            strSQL = "DELETE FROM QL_trncashbankmst " & _
                "WHERE cmpcode='" & cmpcode & "' and cashbankoid = " & Trim(cashbankoid.Text)
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            ' UPDATE QL_trndpap bila pake DP
            If payflag.SelectedValue = "DP" Then
                sSql = "UPDATE QL_trndpap SET trndpapacumamt=ISNULL(trndpapacumamt,0)-" & ToDouble(amtbelinettodtl.Text) & ",trndpapflag=CASE WHEN (ISNULL(trndpapacumamt,0) - " & ToDouble(amtbelinettodtl.Text) & ")>=ISNULL(trndpapamt,0) THEN 'CLOSE' ELSE 'OPEN' END WHERE cmpcode='" & cmpcode & "' AND trndpapoid='" & trndpapoid.SelectedValue & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
        End Try
        Session("oid") = Nothing : Session("tbldtl") = Nothing
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        'Response.Redirect("~\Accounting\trnPayAP_CASH.aspx?awal=true")
    End Sub

    Protected Sub btnPosting2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting2.Click
        lblPOST.Text = "POST"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Try
            Dim sMsg As String = ""
            If CDate(toDate(PaymentDate.Text)) <= CDate(toDate(CutofDate.Text)) Then
                sMsg &= "- Tanggal Payment Tidak boleh <= CutoffDate (" & CutofDate.Text & ") !! <BR> "
            End If
            'CEK PERIODE AKTIF BULANAN
            'sSql = "select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
            'If GetStrData(sSql) <> "" Then
            sSql = "Select distinct left(isnull(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctgHJ(CDate(toDate(PaymentDate.Text))) < GetStrData(sSql) Then
                showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            'End If
            '============================================================================
            If cashbankacctgoid.SelectedValue = "" Then : sMsg &= "- Tolong pilih COA Cash/Bank/Giro !!<BR>" : End If
            Dim st1, st2 As Boolean
            If ToDouble(trnsuppoid.Text) = 0 Then
                sMsg &= "- Tolong pilih supplier !!<BR>"
            End If
            If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
                payduedate.Text = PaymentDate.Text
            ElseIf payduedate.Text.Trim = "" Then
                sMsg &= "- Tolong pilih tanggal jatuh tempo bayar !!<BR>"
            End If

            If (payflag.SelectedValue = "NONCASH" Or payflag.SelectedValue = "GIRO") Then
                If CDate(toDate(payduedate.Text)) < CDate(toDate(PaymentDate.Text)) Then
                    showMessage("Tanggal jatuh tempo tidak bisa kurang dari tanggal Payment !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If

            If payflag.SelectedValue = "GIRO" Then
                If payrefno.Text.Trim = "" Then
                    sMsg &= "- Ref No masih Kosong !!<BR>"
                End If
            ElseIf payflag.SelectedValue = "DP" Then
                If trndpapoid.Items.Count < 1 Then
                    sMsg &= "- Tidak ada no DP di supplier ini !!<BR>"
                Else
                    'If ToDouble(amtbelinettodtl.Text) > ToDouble(dpbalance.Text) Then
                    '    sMsg &= "- Total bayar harus <= total DP !!<BR>"
                    'End If
                End If
            ElseIf payflag.SelectedValue = "VOUCHER" Then
                If RealOid.Text = 0 Then
                    sMsg &= "- Pilih No. Realisasi !!<BR>"
                End If
            End If

            Try
                Dim dt As Date = CDate(toDate(PaymentDate.Text)) : st2 = True
            Catch ex As Exception
                sMsg &= "- Tanggal bayar salah !!<BR>" : st2 = False
            End Try
            If st1 And st2 And payduedate.Visible Then
                If CDate(toDate(payduedate.Text)) < CDate(toDate(PaymentDate.Text)) Then
                    sMsg &= "- Tanggal jatuh tempo harus >= Tanggal bayar !!<BR>"
                End If
            End If

            'If ddlDPNo.Items.Count > 0 Then
            '    Dim tempDP As Double = ToDouble(cKoneksi.ambilscalar("SELECT trndpapamt FROM QL_trndpap WHERE trndpapoid=" & ddlDPNo.SelectedValue()))
            '    If ToDouble(DPAmt.Text) > tempDP Then
            '        sMsg &= "- Jumlah DP can't > Jumlah DP sebenarnya !!<BR>"
            '    End If
            'End If

            ' cek apakah Payment Detail sudah ada atau belum
            If Session("tbldtl") Is Nothing Then
                sMsg &= "- No detail bayar !!<BR>"
                GVDtlPayAP.DataSource = Session("tbldtl")
                GVDtlPayAP.DataBind()
            Else
                Dim objTableCek As DataTable : objTableCek = Session("tbldtl")
                If objTableCek.Rows.Count <= 0 Then
                    sMsg &= "- No Details Bayar!!<BR>"
                    GVDtlPayAP.DataSource = Session("tbldtl")
                    GVDtlPayAP.DataBind()
                Else 'cek tgl ivoice pd detail apakah ada yg kurang dari tgl bayar
                    For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
                        'If objTableCek.Rows(c1).Item("flagdtl") = "" Then
                        '    sSql = "Select trnbelidate from ql_trnbelimst where cmpcode='" & cmpcode & "' and trnbelimstoid=" & objTableCek.Rows(c1).Item("payrefoid")
                        '    If (conn.State = ConnectionState.Closed) Then
                        '        conn.Open()
                        '    End If
                        '    objCmd.CommandText = sSql
                        '    Dim tgle As Date = objCmd.ExecuteScalar
                        '    conn.Close()

                        '    If tgle > CDate(toDate(PaymentDate.Text)) Then
                        '        sMsg &= "- maaf, Tanggal invoice tidak dapat > Pay Date (tgl inv=" & objTableCek.Rows(c1).Item("trnbelino") & " dan Tanggal bayar= " & Format(tgle, "dd/MM/yyyy") & ")..!!<BR>"
                        '        Exit For
                        '    End If
                        'End If

                        'cek data synchronisasi dengan payment
                        sSql = "Select accumpayment From ql_trnbelimst Where cmpcode='" & cmpcode & "' and trnbelimstoid=" & objTableCek.Rows(c1).Item("payrefoid")

                        Dim sCek As String = GetStrData("SELECT DISTINCT reftype FROM QL_conap WHERE branch_code='" & ddlcabang.SelectedValue & "' AND cmpcode='" & cmpcode & "' AND refoid=" & objTableCek.Rows(c1).Item("payrefoid") & " AND reftype='" & RefTypeTbl.Text & "'")

                        If sCek = "ql_trnbelimst" Then
                            'cek data synchronisasi dengan amt retur payment
                            sSql = "Select amtretur From ql_trnbelimst Where cmpcode='" & cmpcode & "' and trnbelimstoid=" & objTableCek.Rows(c1).Item("payrefoid")
                            If objTableCek.Rows(c1).Item("amtretur") <> Math.Round(ToDouble(GetStrData(sSql)), 1) Then
                                sMsg &= "- Nota beli (" & objTableCek.Rows(c1).Item("trnbelino") & ") salah !!, tolong refresh atau pilih ulang untuk nota beli ini lagi !! <BR>"
                                Exit For
                            End If
                        End If

                        Dim swhere As String = ""
                        If Session("oid") = Nothing Or Session("oid") = "" Then
                            swhere = ""
                        Else
                            swhere = " and cashbankoid <> " & Session("oid") & ""
                        End If
                        'Cek apakah ada invoice yg sudah dibuatkan payment tp blm diposting
                        'If GetStrData("Select count(-1) From QL_trnpayap Where payrefoid=" & objTableCek.Rows(c1).Item("payrefoid") & " and upper(payreftype)='QL_TRNBELIMST' and upper(paystatus)<>'POST' " & swhere & "") > 0 Then
                        '    sMsg &= "- Ada transaksi pembayaran/Retur lain untuk nota beli ini (" & objTableCek.Rows(c1).Item("trnbelino") & ") <BR> Silahkan tekan batal dan cek di transaksi pembayaran yang lain kemudian tekan posting untuk nota (" & objTableCek.Rows(c1).Item("trnbelino") & ") terlebih dahulu !! <BR>"
                        '    Exit For
                        'End If
                    Next
                End If
            End If
            If defcbno.Text.Trim = "" Then
                sMsg &= "- Silahkan isi nomor pembayaran hutang !!<BR>"
            End If

            If cashbanknote.Text.Trim.Length > 200 Then
                sMsg &= "- Maksimal Catatan adalah 200 karakter !!<BR>"
            End If
            If sMsg <> "" Then
                lblPOST.Text = "In Process"
                showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End If
        Catch ex As Exception
            lblPOST.Text = "In Process"
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try

        If lblPOST.Text <> "POST" Then
            lblPOST.Text = "In Process"
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
        End If
        Session("vJurnalDtl") = GenerateID("ql_trngldtl", cmpcode)
        Session("vJurnalMst") = GenerateID("ql_trnglmst", cmpcode)
        Dim iDPapoid = GenerateID("ql_trndpap", cmpcode)
        Dim iCbdtloid As Int64 = GenerateID("QL_CASHBANKGL", cmpcode)
        'Value RateIDR/USD
        Dim dRate As Double
        dRate = InvoiceRate(Session("invCurrOid"))

        Dim sNoDPAP As String = ""
        If payflag.SelectedValue = "CASH" Then
            sNoDPAP = "T"
        ElseIf payflag.SelectedValue = "NONCASH" Then
            sNoDPAP = "B"
        ElseIf payflag.SelectedValue = "GIRO" Then
            sNoDPAP = "G"
        ElseIf payflag.SelectedValue = "DP" Then
            sNoDPAP = "D"
        ElseIf payflag.SelectedValue = "VOUCHER" Then
            sNoDPAP = "V"
        End If

        sNoDPAP = "DP.AP." & sNoDPAP & "-" & Format(CDate(toDate(PaymentDate.Text.Trim)), "yy/MM/")
        sSql = "SELECT isnull(max(abs(replace(trndpapno,'" & sNoDPAP & "',''))),0)+1 AS IDNEW FROM ql_trndpap WHERE cmpcode='" & cmpcode & "' AND trndpapno LIKE '" & sNoDPAP & "%'"
        Dim IcounterDPAPno As Int32 = GetStrData(sSql)

        Session("vDtlSeq") = 1
        Dim iDPOid As Integer = 0 : Dim OidReal As Integer = 0
        If payflag.SelectedValue = "DP" Then
            iDPOid = trndpapoid.SelectedValue
        ElseIf payflag.SelectedValue = "VOUCHER" Then
            OidReal = RealOid.Text
        End If
        Session("conaPoid") = GenerateID("ql_conaP", cmpcode)
        currencyRate.Text = "1.00"
        Dim itrnbelimstoid As Int32 = 0
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans
        Try
            'If lblPOST.Text = "POST" Then
            '    Dim DTCekV As DataTable = Session("tbldtl")
            '    Dim dVou As DataView = DTCekV.DefaultView
            '    dVou.RowFilter = "payamt < 0"
            '    If dVou.Count > 0 Then
            '        For C1 As Int16 = 0 To dVou.Count - 1
            '            '-------------------Update Beli Dtl-------------------
            '            Dim DtlOid As Integer = 0
            '            sSql = "Update QL_trnbelidtl Set trnbelidtlres1='POST' Where trnbelimstoid=" & dVou.Item(C1).Item("payrefoid") & ""
            '            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            '        Next
            '    End If
            '    dVou.RowFilter = ""

            '    '-------------------Cek Voucher dtl-------------------
            '    Dim vCek As DataTable : vCek = Session("tbldtl")
            '    Dim dv As DataView = vCek.DefaultView : Dim AmtV As Double
            '    dv.RowFilter = "payamt < 0"
            '    Dim cDtl As Integer
            '    If dv.Count > 0 Then
            '        For c1 As Int16 = 0 To dv.Count - 1
            '            sSql = "Select Isnull(SUM(amtdisc2)-po.totalvoucher,0.00) from QL_trnbelidtl bd Inner Join QL_trnbelimst bm On bm.trnbelimstoid=bd.trnbelimstoid Inner Join QL_pomst po On po.trnbelipono=bm.trnbelipono Where bm.trnbelipono IN (Select Distinct trnbelipono From QL_trnbelimst Where trnbelimstoid=" & dv.Item(c1).Item("payrefoid") & ") And trnbelidtlres1='POST' Group By po.totalvoucher"
            '            objCmd.CommandText = sSql : cDtl = objCmd.ExecuteScalar
            '            If cDtl <> 0 Then
            '                AmtV += cDtl
            '            Else
            '                sSql = "Update QL_voucherdtl Set voucherstatus='POST' Where trnbelino=(Select trnbelipono From QL_trnbelimst Where trnbelimstoid=" & vCek.Rows(c1).Item("payrefoid") & ")"
            '                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            '            End If
            '        Next
            '    End If
            '    dv.RowFilter = ""
            'End If
            If Session("oid") = Nothing Or Session("oid") = "" Then
                Dim sCBType As String = ""
                Dim iCurID As Integer = 0 : Dim sCBCode As String = ""

                If payflag.SelectedValue = "CASH" Then
                    sCBType = "BKM"
                ElseIf payflag.SelectedValue = "NONCASH" Then
                    sCBType = "BBM"
                ElseIf payflag.SelectedValue = "GIRO" Then
                    sCBType = "BGM"
                ElseIf payflag.SelectedValue = "DP" Then
                    sCBType = "BDM"
                ElseIf payflag.SelectedValue = "VOUCHER" Then
                    sCBType = "BVK"
                End If

                Dim sCashBank As Integer = GenerateID("QL_trncashbankmst", cmpcode)
                cashbankoid.Text = sCashBank
                Dim sTemp As Integer = GenerateID("QL_trnpayaP", cmpcode)

                sSql = "INSERT into QL_trncashbankmst (cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,createuser,createtime,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, pic_refname,branch_code,cashbankamount,cashbankamountidr,bankoid) " & _
                   " VALUES ('" & cmpcode & "'," & sCashBank & ",'" & Tchar(defcbno.Text) & "','" & lblPOST.Text & "','" & sCBType & "','APK'," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & Tchar(cashbanknote.Text) & "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp," & CurrencyOid.SelectedValue & "," & ToDouble(currencyRate.Text) & ", " & trnsuppoid.Text & ", 'QL_MSTSUPP','" & ddlcabang.SelectedValue & "'," & ToDouble(amtbelinettodtl.Text) & "," & ToDouble(amtbelinettodtl.Text) & "," & RetcbOidAp.Text & ")"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                Dim dparacumamt, dparacumamtidr, dparacumamtusd As Double
                If CurrencyOid.SelectedValue = 1 Then
                    dparacumamt = ToDouble(amtbelinettodtl.Text)
                    dparacumamtidr = ToDouble(amtbelinettodtl.Text)
                    If Session("invCurrOid") = 1 Then
                        dparacumamtusd = ToDouble(amtbelinettodtl.Text) * dRate
                    Else
                        dparacumamtusd = ToDouble(amtbelinettodtl.Text) / dRate
                    End If
                Else
                    dparacumamt = ToDouble(amtbelinettodtl.Text)
                    dparacumamtusd = ToDouble(amtbelinettodtl.Text)
                    If Session("invCurrOid") = 1 Then
                        dparacumamtidr = ToDouble(amtbelinettodtl.Text) / dRate
                    Else
                        dparacumamtidr = ToDouble(amtbelinettodtl.Text) * dRate
                    End If
                End If
                ' UPDATE QL_trndpap bila pake DP
                If iDPOid <> 0 Then
                    sSql = "UPDATE QL_trndpaP SET trndpaPacumamt=ISNULL(trndpaPacumamt,0)-" & dparacumamt & ",trndpaPacumamtidr =ISNULL(trndpaPacumamtidr,0)- " & dparacumamtidr & ",trndpapacumamtusd = " & dparacumamtusd & ", trndpaPflag=CASE WHEN (ISNULL(trndpapacumamtusd,0)+" & ToDouble(amtbelinettodtl.Text) & ")>=ISNULL(trndpaPamt,0) THEN 'CLOSE' ELSE 'OPEN' END WHERE cmpcode='" & cmpcode & "' AND trndpaPoid='" & iDPOid & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                'Update lastoid dari QL_mstoid table QL_trncashbankmst
                sSql = "update QL_mstoid set lastoid=" & sCashBank & " Where tablename like 'QL_trncashbank%' and cmpcode like '%" & cmpcode & "%' "
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                '===========================================
                'Start save invoice detail(banyak detail saja)
                '============================================
                Dim totalPayment As Decimal = 0
                Dim totalPaymentIDR As Decimal = 0 : Dim totalPaymentUSD As Decimal = 0
                Dim vPayam, vPayamidr, vPayamusd As Double
                Dim vdpam, vdpamidr, vdpamusd As Double

                If Not Session("tbldtl") Is Nothing Then
                    Dim objTable As DataTable : Dim objRow() As DataRow
                    objTable = Session("tbldtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                    For C1 As Int16 = 0 To objRow.Length - 1
                        'cek apakah supplier ada atau tidak
                     
                        If CurrencyOid.SelectedValue = 1 Then
                            vPayam = ToDouble(objRow(C1)("payamt").ToString)
                            vdpam = ToDouble(objRow(C1)("DPAmt").ToString)
                            vPayamidr = ToDouble(objRow(C1)("payamt").ToString)
                            vdpamidr = ToDouble(objRow(C1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vPayamusd = ToDouble(objRow(C1)("payamt").ToString) * dRate
                                vdpamusd = ToDouble(objRow(C1)("DPAmt").ToString) * dRate
                            Else
                                vPayamusd = ToDouble(objRow(C1)("payamt").ToString) / dRate
                                vdpamusd = ToDouble(objRow(C1)("DPAmt").ToString) / dRate
                            End If
                        Else
                            vPayam = ToDouble(objRow(C1)("payamt").ToString)
                            vdpam = ToDouble(objRow(C1)("DPAmt").ToString)
                            vPayamusd = ToDouble(objRow(C1)("payamt").ToString)
                            vdpamusd = ToDouble(objRow(C1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vPayamidr = vPayam / dRate : vdpamidr = vdpam / dRate
                            Else
                                vPayamidr = vPayam * dRate : vdpamidr = vdpam * dRate
                            End If
                        End If
                        sSql = "INSERT into QL_trnpayaP (cmpcode,paymentoid,cashbankoid,SUPPoid,payreftype,payrefoid,payacctgoid,payrefno,paybankoid,payduedate,paynote,payamt,payamtidr,payamtusd,paystatus,upduser,updtime,trndpaPoid,DPAmt,DPAmtIDR,DPAmtUSD,payflag,branch_code,payrefflag,realisasioid,payres1) VALUES " & _
                        " ('" & cmpcode & "'," & (sTemp + C1) & "," & sCashBank & "," & trnsuppoid.Text & ",'" & objRow(C1)("payreftype").ToString & "'," & objRow(C1)("payrefoid").ToString & "," & ToDouble(objRow(C1)("payacctgoid").ToString) & ",'" & Tchar(payrefno.Text.Trim) & "'," & IIf(iDPOid > 0, iDPOid, cashbankacctgoid.SelectedValue) & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(objRow(C1)("paynote").ToString) & "'," & ToDouble(objRow(C1)("payamt").ToString) & "," & ToDouble(vPayamidr) & "," & 0 & ",'" & lblPOST.Text & "','" & Session("UserID") & "',current_timestamp," & iDPOid & "," & ToDouble(objRow(C1)("DPAmt").ToString) & "," & ToDouble(vPayamidr) & "," & 0 & ",'" & CurrencyOid.SelectedValue & "','" & ddlcabang.SelectedValue & "','" & Tchar(objRow(C1)("flagdtl").ToString) & "'," & OidReal & ",'" & Tchar(objRow(C1)("payres1").ToString) & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        ''INSERT INTO to QL_conap from QL_trnpayap
                        'sSql = "INSERT into QL_conaP (cmpcode,conaPoid,reftype,trnaptype,refoid,payrefoid,SUPPoid,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnaPstatus,trnaPnote,branch_code) VALUES " & _
                        '"('" & cmpcode & "'," & Session("conaPoid") + C1 & ",'QL_trnpayap','APK'," & objRow(C1)("payrefoid") & "," & (sTemp + C1) & "," & trnsuppoid.Text & ",0,0,0, " & ToDouble(objRow(C1)("payamt")) * -1 & "," & ToDouble(vPayamidr) * -1 & "," & 0 & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(defcbno.Text) & "'," & cashbankacctgoid.SelectedValue & "," & objRow(C1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(defcbno.Text) & "','" & ddlcabang.SelectedValue & "')"
                        'objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        If lblPOST.Text = "POST" Then
                            If itrnbelimstoid <> objRow(C1)("payrefoid") And objRow(C1)("payamt") > 0 Then
                                'update ke acumamt ql_trnbeli sby tanda pembayaran
                                sSql = "Update ql_trnBELImst set accumpayment=accumpayment-" & objRow(C1)("payamt") & ", accumpaymentidr = accumpaymentidr- " & ToDouble(vPayamidr) & " , accumpaymentusd=accumpaymentusd-" & 0 & ",lastpaydate='" & CDate(toDate(PaymentDate.Text)) & "' Where cmpcode='" & cmpcode & "' and trnBELImstoid=" & objRow(C1)("payrefoid")
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                itrnbelimstoid = objRow(C1)("payrefoid")

                            ElseIf objRow(C1)("payamt") > 0 And objRow(C1)("trndpapoid") = 1 Then 'insert to trndp
                                sSql = "INSERT INTO QL_trndpap (cmpcode,trndpaPoid,trndpaPno,trndpaPdate,SUPPoid,cashbankoid,trndpaPacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate,trndpaPamt,trndpaPamtidr,trndpaPamtusd,taxtype,taxoid,taxpct,taxamt,trndpaPnote,trndpaPflag,trndpaPacumamt,trndpaPacumamtidr,trndpaPacumamtusd,trndpaPstatus,createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                    "('" & cmpcode & "'," & iDPapoid & ",'" & GenNumberString(sNoDPAP, "", IcounterDPAPno, 4) & "','" & CDate(toDate(PaymentDate.Text)) & "'," & trnsuppoid.Text & "," & sCashBank & "," & objRow(C1)("payacctgoid") & ",'" & sCBType & "'," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(defcbno.Text) & "'," & CurrencyOid.SelectedValue & "," & ToDouble(currencyRate.Text) & "," & ToDouble(objRow(C1)("payamt")) & _
                    ", " & ToDouble(vPayamidr) & "," & 0 & ",'',0,0,0,'Kelebihan bayar " & Tchar(defcbno.Text) & "','OPEN',0,0,0,'" & lblPOST.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("branch_id") & "')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_mstoid SET lastoid=" & iDPapoid & " WHERE tablename='QL_trndpap' AND cmpcode='" & cmpcode & "'"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                IcounterDPAPno += 1 : iDPapoid += 1
                            End If
                        End If
                    Next
                    Dim dMek As DataTable = Session("tbldtl")
                    Dim dTits As DataView = dMek.DefaultView
                    Dim AmtNya As Double

                    dTits.RowFilter = "flagdtl <> 'OTHER'"
                    '======= Insert Nilai Bayar di Ql_conap =======
                    '============================================== 
                    For t1 As Integer = 0 To dTits.Count - 1
                        If dTits(t1)("payres1").ToString = "KURANG BAYAR" Then
                            AmtNya = ToDouble(dMek.Compute("SUM(payamt)", "payrefoid =" & dTits(t1)("payrefoid") & "").ToString)
                        Else
                            AmtNya = ToDouble(dTits(t1)("payamt").ToString)
                        End If

                        If CurrencyOid.SelectedValue = 1 Then
                            vPayam = ToDouble(AmtNya)
                            vdpam = ToDouble(dTits(t1)("DPAmt").ToString)
                            vPayamidr = ToDouble(AmtNya)
                            vdpamidr = ToDouble(dTits(t1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vPayamusd = ToDouble(AmtNya) * dRate
                                vdpamusd = ToDouble(dTits(t1)("DPAmt").ToString) * dRate
                            Else
                                vPayamusd = ToDouble(AmtNya) / dRate
                                vdpamusd = ToDouble(dTits(t1)("DPAmt").ToString) / dRate
                            End If
                        Else
                            vPayam = ToDouble(AmtNya)
                            vdpam = ToDouble(dTits(t1)("DPAmt").ToString)
                            vPayamusd = ToDouble(AmtNya)
                            vdpamusd = ToDouble(dTits(t1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vPayamidr = vPayam / dRate : vdpamidr = vdpam / dRate
                            Else
                                vPayamidr = vPayam * dRate : vdpamidr = vdpam * dRate
                            End If
                        End If

                        sSql = "INSERT into QL_conap (cmpcode,conaPoid,reftype,trnaptype,refoid,payrefoid,SUPPoid,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnaPstatus,trnaPnote,branch_code) VALUES " & _
                        "('" & cmpcode & "'," & Session("conaPoid") & ",'QL_trnpayaP','PAYAP'," & dTits(t1)("payrefoid") & "," & (sTemp + t1) & "," & trnsuppoid.Text & ",0,0,0, " & vPayamidr * -1 & "," & vPayamidr * -1 & "," & vPayamusd * -1 & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(defcbno.Text) & "'," & cashbankacctgoid.SelectedValue & "," & dTits(t1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(defcbno.Text) & "','" & Session("branch_id") & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("conaPoid") += 1
                    Next
                    dTits.RowFilter = ""

                    '======= Insert Nilai Selisih kurang bayar di Ql_conap =======
                    '=============================================================
                    dTits.RowFilter = "flagdtl='OTHER' AND payamt < 0"
                    For t1 As Integer = 0 To dTits.Count - 1

                        If CurrencyOid.SelectedValue = 1 Then
                            vPayam = ToDouble(dTits(t1)("payamt").ToString)
                            vdpam = ToDouble(dTits(t1)("DPAmt").ToString)
                            vPayamidr = ToDouble(dTits(t1)("payamt").ToString)
                            vdpamidr = ToDouble(dTits(t1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vPayamusd = ToDouble(dTits(t1)("payamt").ToString) * dRate
                                vdpamusd = ToDouble(dTits(t1)("DPAmt").ToString) * dRate
                            Else
                                vPayamusd = ToDouble(dTits(t1)("payamt").ToString) / dRate
                                vdpamusd = ToDouble(dTits(t1)("DPAmt").ToString) / dRate
                            End If
                        Else
                            vPayam = ToDouble(dTits(t1)("payamt").ToString)
                            vdpam = ToDouble(dTits(t1)("DPAmt").ToString)
                            vPayamusd = ToDouble(dTits(t1)("payamt").ToString)
                            vdpamusd = ToDouble(dTits(t1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vPayamidr = vPayam / dRate : vdpamidr = vdpam / dRate
                            Else
                                vPayamidr = vPayam * dRate : vdpamidr = vdpam * dRate
                            End If
                        End If
                        sSql = "INSERT into QL_conap (cmpcode,conaPoid,reftype,trnaptype,refoid,payrefoid,SUPPoid,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnaPstatus,trnapnote,branch_code) VALUES " & _
                        "('" & cmpcode & "'," & Session("conaPoid") & ",'QL_trnpayap','PAYAP'," & dTits(t1)("payrefoid") & "," & (sTemp + t1) & "," & trnsuppoid.Text & ",0,0,0, " & vPayamidr & "," & vPayamidr & "," & vPayamusd & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(defcbno.Text) & "'," & cashbankacctgoid.SelectedValue & "," & dTits(t1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(defcbno.Text) & "~" & dTits(t1)("payrefoid") & "','" & Session("branch_id") & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("conaPoid") += 1
                    Next
                    dTits.RowFilter = ""

                    ' Update lastoid conap
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("conapoid") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_conap'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    'Update lastoid dari QL_mstoid table QL_trnpayap
                    sSql = "UPDATE QL_mstoid SET lastoid=" & sTemp + objRow.Length - 1 & " WHERE tablename='QL_trnpayap'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
            Else
                '======
                'Update
                '======
                Dim bUpdPayType As Boolean = False
                Dim sCBType As String = "" : Dim sCBCode As String = ""
                'If payflag.SelectedValue <> HiddenField1.Value Or cashbankacctgoid.SelectedValue <> HiddenField2.Value Then ' jika ada perubahan pada payment type
                Dim iCurID As Integer = 0
                If payflag.SelectedValue = "CASH" Then
                    sCBType = "BKM"
                ElseIf payflag.SelectedValue = "NONCASH" Then
                    sCBType = "BBM"
                ElseIf payflag.SelectedValue = "GIRO" Then
                    sCBType = "BGM"
                ElseIf payflag.SelectedValue = "DP" Then
                    sCBType = "BDM"
                Else
                    sCBType = "BLM"
                End If
                bUpdPayType = True
                '    Else
                '    sSql = "SELECT cashbanktype FROM ql_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & Trim(cashbankoid.Text)
                '    objCmd.CommandText = sSql : sCBType = objCmd.ExecuteScalar
                'End If

                ' Karena No Bisa Manual, no bs diupdate terus
                bUpdPayType = True
                sSql = "UPDATE QL_trncashbankmst SET cashbankacctgoid=" & cashbankacctgoid.SelectedValue & ", cashbankdate='" & CDate(toDate(PaymentDate.Text)) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', upduser = '" & Session("UserID") & "',updtime=current_timestamp" & ",cashbankcurrate=" & ToDouble(currencyRate.Text) & ",cashbankstatus='" & lblPOST.Text & "', PIC=" & trnsuppoid.Text & ", pic_refname='QL_MSTSUPP',cashbankamount=" & ToDouble(amtbelinettodtl.Text) & ",cashbankamountidr=" & ToDouble(amtbelinettodtl.Text) & ",bankoid=" & RetcbOidAp.Text & ""
                If bUpdPayType Then
                    sSql &= ",cashbankno='" & Tchar(defcbno.Text) & "', cashbanktype='" & sCBType & "' "
                End If
                sSql &= " WHERE cmpcode = '" & cmpcode & "' and cashbankoid = " & cashbankoid.Text  'Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                ' Check Current DB if use DP then Reverse DP
                Dim iCurrentDP, iCurrentDPIDR, iCurrentDPUSD As Integer
                Dim dCurrentDPTotal, dCurrentDPTotalIDR, dCurrentDPTotalUSD As Double
                iCurrentDP = 0 : iCurrentDPIDR = 0 : iCurrentDPUSD = 0
                sSql = "SELECT TOP 1 paybankoid FROM QL_trnpayaP WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : iCurrentDP = objCmd.ExecuteScalar()

                sSql = "SELECT SUM(par.payamt) FROM QL_trnpayaP par WHERE par.cmpcode='" & cmpcode & "' AND par.cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : dCurrentDPTotal = objCmd.ExecuteScalar()

                sSql = "SELECT SUM(par.payamtusd) FROM QL_trnpayaP par WHERE par.cmpcode='" & cmpcode & "' AND par.cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : dCurrentDPTotalIDR = objCmd.ExecuteScalar()

                sSql = "SELECT SUM(par.payamtidr) FROM QL_trnpayaP par WHERE par.cmpcode='" & cmpcode & "' AND par.cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : dCurrentDPTotalUSD = objCmd.ExecuteScalar()

                ' UPDATE QL_trndpap bila pake DP
                If iDPOid <> 0 Then
                    If iCurrentDP <> 0 Then
                        sSql = "UPDATE QL_trndpaP SET trndpaPacumamt=ISNULL(trndpaPacumamt,0)-" & ToDouble(dCurrentDPTotal) & ",trndpaPacumamtidr=isnull(trndpaPacumamtidr,0)-" & ToDouble(dCurrentDPTotalIDR) & ",trndpaPacumamtusd=isnull(trndpaPacumamtusd,0)-" & ToDouble(dCurrentDPTotalUSD) & ", trndpaPflag='OPEN' WHERE cmpcode='" & cmpcode & "' AND trndpaPoid='" & iDPOid & "'"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                    Dim dparacumamt, dparacumamtidr, dparacumamtusd As Double
                    If CurrencyOid.SelectedValue = 1 Then
                        dparacumamt = ToDouble(amtbelinettodtl.Text)
                        dparacumamtidr = ToDouble(amtbelinettodtl.Text)
                        If Session("invCurrOid") = 1 Then
                            dparacumamtusd = ToDouble(amtbelinettodtl.Text) * dRate
                        Else
                            dparacumamtusd = ToDouble(amtbelinettodtl.Text) / dRate
                        End If
                    Else
                        dparacumamt = ToDouble(amtbelinettodtl.Text)
                        dparacumamtusd = ToDouble(amtbelinettodtl.Text)
                        If Session("invCurrOid") = 1 Then
                            dparacumamtidr = ToDouble(amtbelinettodtl.Text) / dRate
                        Else
                            dparacumamtidr = ToDouble(amtbelinettodtl.Text) * dRate
                        End If
                    End If
                    sSql = "UPDATE QL_trndpaP SET trndpaPacumamt=ISNULL(trndpaPacumamt,0)+" & ToDouble(amtbelinettodtl.Text) & ",trndpaPflag=CASE WHEN (ISNULL(trndpaPacumamt,0)+" & ToDouble(amtbelinettodtl.Text) & ") >=ISNULL(trndpaPamt,0) THEN 'CLOSE' ELSE 'OPEN' END WHERE cmpcode='" & cmpcode & "' AND trndpaPoid='" & iDPOid & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                Dim sTemp As Integer = GenerateID("QL_trnpayaP", cmpcode)
                Session("conapoid") = GenerateID("ql_conaP", cmpcode)
                If Not Session("tbldtl") Is Nothing Then
                    sSql = "Delete from QL_conap Where cmpcode='" & cmpcode & "' AND payrefoid IN (SELECT paymentoid FROM QL_trnpayaP WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text & ")"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    sSql = "Delete from QL_trnpayap Where cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    '=============================================
                    'Start save invoice detail(banyak detail saja)
                    '=============================================
                    Dim objTable As DataTable
                    Dim objRow() As DataRow
                    objTable = Session("tbldtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                    Dim vpayamt, vpayamtidr, vpayamtusd As Double
                    Dim dpamt, dpamtidr, dpamtusd As Double
                    For C1 As Integer = 0 To objRow.Length - 1
                        If CurrencyOid.SelectedValue = 1 Then
                            vpayamt = ToDouble(objRow(C1)("payamt").ToString)
                            vpayamtidr = vpayamt
                            dpamt = ToDouble(objRow(C1)("DPAmt").ToString)
                            dpamtidr = ToDouble(objRow(C1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vpayamtusd = vpayamt * dRate : dpamtusd = dpamt * dRate
                            Else
                                vpayamtusd = vpayamt / dRate : dpamtusd = dpamt / dRate
                            End If
                        Else
                            vpayamt = ToDouble(objRow(C1)("payamt").ToString)
                            vpayamtusd = vpayamt
                            dpamt = ToDouble(objRow(C1)("DPAmt").ToString)
                            dpamtusd = ToDouble(objRow(C1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vpayamtidr = vpayamt / dRate : dpamtidr = dpamt / dRate
                            Else
                                vpayamtidr = vpayamt * dRate : dpamtidr = dpamt * dRate
                            End If
                        End If

                        sSql = "INSERT into QL_trnpayaP(cmpcode,paymentoid,cashbankoid,SUPPoid,payreftype,payrefoid,payacctgoid,payrefno,paybankoid,payduedate,paynote,payamt,payamtidr,payamtusd,paystatus,upduser,updtime,trndpaPoid,DPAmt,DPAmtIDR,DPAmtUSD,payflag,branch_code,payrefflag,realisasioid,payres1)" & _
                        " VALUES ('" & cmpcode & "'," & (sTemp + C1) & "," & cashbankoid.Text & "," & trnsuppoid.Text & ",'" & objRow(C1)("payreftype").ToString & "'," & objRow(C1)("payrefoid").ToString & "," & ToDouble(objRow(C1)("payacctgoid").ToString) & ",'" & Tchar(payrefno.Text.Trim) & "'," & IIf(iDPOid > 0, iDPOid, cashbankacctgoid.SelectedValue()) & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(objRow(C1)("paynote").ToString) & "'," & ToDouble(objRow(C1)("payamt").ToString) & "," & ToDouble(vpayamtidr) & "," & ToDouble(vpayamtusd) & ",'" & lblPOST.Text & "','" & Session("UserID") & "',current_timestamp," & trndpapoid.SelectedValue & "," & ToDouble(objRow(C1)("DPAmt").ToString) & "," & ToDouble(dpamtidr) & "," & ToDouble(dpamtusd) & ",'" & objRow(C1)("flagdtl").ToString & "','" & ddlcabang.SelectedValue & "','" & Tchar(objRow(C1)("flagdtl").ToString) & "'," & OidReal & ",'" & Tchar(objRow(C1)("payres1").ToString) & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        ''INSERT INTO to QL_conap from QL_trnpayap
                        'sSql = "INSERT into QL_conaP (cmpcode,conaPoid,reftype,refoid,payrefoid,SUPPoid,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnaPstatus,trnaPnote,branch_code,trnaptype) VALUES " & _
                        '"('" & cmpcode & "'," & Session("conaPoid") + C1 & ",'QL_trnpayap'," & objRow(C1)("payrefoid") & "," & (sTemp + C1) & "," & trnsuppoid.Text & ",0,0,0," & ToDouble(objRow(C1)("payamt")) * -1 & "," & ToDouble(vpayamtidr) * -1 & "," & ToDouble(vpayamtusd) * -1 & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Session("mstoid3") & "'," & cashbankacctgoid.SelectedValue & "," & objRow(C1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(defcbno.Text.Trim) & "','" & ddlcabang.SelectedValue & "','APK')"
                        'objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        'update ke acumamt ql_trnbeli sby tanda pembayaran 
                        If lblPOST.Text = "POST" Then
                            If CurrencyOid.SelectedValue = 1 Then
                                vpayamt = ToDouble(objRow(C1)("payamt"))
                                vpayamtidr = ToDouble(objRow(C1)("payamt"))
                                If Session("invCurrOid") = 1 Then
                                    vpayamtusd = ToDouble(objRow(C1)("payamt")) * dRate
                                Else
                                    vpayamtusd = ToDouble(objRow(C1)("payamt")) / dRate
                                End If
                            Else
                                vpayamt = ToDouble(objRow(C1)("payamt"))
                                vpayamtusd = ToDouble(objRow(C1)("payamt"))
                                If Session("invCurrOid") = 1 Then
                                    vpayamtidr = ToDouble(objRow(C1)("payamt")) / dRate
                                Else
                                    vpayamtidr = ToDouble(objRow(C1)("payamt")) * dRate
                                End If
                            End If

                            If itrnbelimstoid <> objRow(C1)("payrefoid") And objRow(C1)("payamt") > 0 Then
                                sSql = "update ql_trnBELImst set accumpayment=accumpayment-" & objRow(C1)("payamt") & ",accumpaymentidr=accumpaymentidr-" & vpayamtidr & ",accumpaymentusd=accumpaymentusd-" & vpayamtusd & " ,lastpaydate='" & CDate(toDate(PaymentDate.Text)) & "' Where cmpcode='" & cmpcode & "' and trnBELImstoid=" & objRow(C1)("payrefoid")
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                itrnbelimstoid = objRow(C1)("payrefoid")

                            ElseIf objRow(C1)("payamt") > 0 And objRow(C1)("trndpapoid") = 1 Then 'insert to trndp
                                sSql = "INSERT INTO QL_trndpap (cmpcode,trndpaPoid,trndpaPno,trndpaPdate,SUPPoid,cashbankoid,trndpaPacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate,trndpaPamt,trndpaPamtidr,trndpaPamtusd,taxtype,taxoid,taxpct,taxamt,trndpaPnote,trndpaPflag,trndpaPacumamt,trndpaPstatus,createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                     "('" & cmpcode & "'," & iDPapoid & ",'" & GenNumberString(sNoDPAP, "", IcounterDPAPno, 4) & "','" & CDate(toDate(PaymentDate.Text)) & "'," & trnsuppoid.Text & "," & Trim(cashbankoid.Text) & "," & objRow(C1)("payacctgoid") & ",'" & sCBType & "'," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(defcbno.Text) & "'," & CurrencyOid.SelectedValue & "," & ToDouble(currencyRate.Text) & "," & ToDouble(objRow(C1)("payamt")) & "," & ToDouble(vpayamtidr) & "," & ToDouble(vpayamtusd) & ",'',0,0,0,'Kelebihan bayar " & Tchar(defcbno.Text) & "','OPEN',0,'" & lblPOST.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ddlcabang.SelectedValue & "')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_mstoid SET lastoid=" & iDPapoid & " WHERE tablename='QL_trndpaP' AND cmpcode='" & cmpcode & "'"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                IcounterDPAPno += 1 : iDPapoid += 1
                            End If
                        End If
                    Next

                    '======= Insert Nilai Bayar di Ql_conap =======
                    '==============================================
                    Dim dMek As DataTable = Session("tbldtl")
                    Dim dTits As DataView = dMek.DefaultView
                    Dim AmtNya As Double
                    dTits.RowFilter = "flagdtl <> 'OTHER'"
                    For t1 As Integer = 0 To dTits.Count - 1
                        If dTits(t1)("payres1").ToString = "KURANG BAYAR" Then
                            AmtNya = ToDouble(dMek.Compute("SUM(payamt)", "payrefoid =" & dTits(t1)("payrefoid") & "").ToString)
                        Else
                            AmtNya = ToDouble(dTits(t1)("payamt").ToString)
                        End If

                        If CurrencyOid.SelectedValue = 1 Then
                            vpayamt = ToDouble(AmtNya)
                            vpayamtidr = vpayamt
                            dpamt = ToDouble(dTits(t1)("DPAmt").ToString)
                            dpamtidr = ToDouble(dTits(t1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vpayamtusd = vpayamt * dRate : dpamtusd = dpamt * dRate
                            Else
                                vpayamtusd = vpayamt / dRate : dpamtusd = dpamt / dRate
                            End If
                        Else
                            vpayamt = ToDouble(AmtNya)
                            vpayamtusd = vpayamt
                            dpamt = ToDouble(dTits(t1)("DPAmt").ToString)
                            dpamtusd = ToDouble(dTits(t1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vpayamtidr = vpayamt / dRate : dpamtidr = dpamt / dRate
                            Else
                                vpayamtidr = vpayamt * dRate : dpamtidr = dpamt * dRate
                            End If
                        End If

                        sSql = "INSERT into QL_conap (cmpcode,conaPoid,reftype,refoid,payrefoid,SUPPoid,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnaPstatus,trnaPnote,branch_code,trnaptype) VALUES " & _
                        "('" & cmpcode & "'," & Session("conaPoid") & ",'QL_trnpayap'," & dTits(t1)("payrefoid") & "," & (sTemp + t1) & "," & trnsuppoid.Text & ",0,0,0," & vpayamtidr * -1 & "," & vpayamtidr * -1 & "," & vpayamtusd * -1 & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Session("mstoid3") & "'," & cashbankacctgoid.SelectedValue & "," & dTits(t1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(defcbno.Text.Trim) & "','" & Session("branch_id") & "','PAYAP')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("conaPoid") += 1
                    Next
                    dTits.RowFilter = ""

                    '======= Insert Nilai Selisih kurang bayar di Ql_conap =======
                    '=============================================================
                    dTits.RowFilter = "flagdtl='OTHER' AND payamt < 0"
                    For t1 As Integer = 0 To dTits.Count - 1
                        If CurrencyOid.SelectedValue = 1 Then
                            vpayamt = ToDouble(dTits(t1)("payamt").ToString)
                            vpayamtidr = vpayamt
                            dpamt = ToDouble(dTits(t1)("DPAmt").ToString)
                            dpamtidr = ToDouble(dTits(t1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vpayamtusd = vpayamt * dRate : dpamtusd = dpamt * dRate
                            Else
                                vpayamtusd = vpayamt / dRate : dpamtusd = dpamt / dRate
                            End If
                        Else
                            vpayamt = ToDouble(dTits(t1)("payamt").ToString)
                            vpayamtusd = vpayamt
                            dpamt = ToDouble(dTits(t1)("DPAmt").ToString)
                            dpamtusd = ToDouble(dTits(t1)("DPAmt").ToString)
                            If Session("invCurrOid") = 1 Then
                                vpayamtidr = vpayamt / dRate : dpamtidr = dpamt / dRate
                            Else
                                vpayamtidr = vpayamt * dRate : dpamtidr = dpamt * dRate
                            End If
                        End If

                        sSql = "INSERT into QL_conap (cmpcode,conaPoid,reftype,refoid,payrefoid,SUPPoid,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnaPstatus,trnaPnote,branch_code,trnaptype) VALUES " & _
                        "('" & cmpcode & "'," & Session("conaPoid") & ",'QL_trnpayap'," & dTits(t1)("payrefoid") & "," & (sTemp + t1) & "," & trnsuppoid.Text & ",0,0,0," & dTits(t1)("payamt") & "," & vpayamtidr & "," & vpayamtusd & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Session("mstoid3") & "'," & cashbankacctgoid.SelectedValue & "," & dTits(t1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(defcbno.Text.Trim) & "~" & dTits(t1)("payrefoid") & "','" & Session("branch_id") & "','PAYAP')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("conaPoid") += 1
                    Next
                    dTits.RowFilter = ""

                    ' Update lastoid conap
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("conaPoid") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_conap'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    'Update lastoid dari QL_mstoid table QL_trnpayap
                    sSql = "UPDATE QL_mstoid SET lastoid=" & sTemp + objRow.Length - 1 & " WHERE tablename='QL_trnpayaP'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                Else
                    showMessage("Pilih invoice", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If

            '===========================================
            'End save invoice Banyk detail saja
            '===========================================
            If lblPOST.Text = "POST" Then
                If Not Session("tbldtl") Is Nothing Then
                    Dim objTable As DataTable : objTable = Session("tbldtl")

                    '//////INSERT INTO TRNGLMST
                    sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,branch_code,type) VALUES ('" & cmpcode & "'," & Session("vJurnalMst") & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','A/P Koreksi|No=" & Tchar(ApNo.Text) & "','" & lblPOST.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',current_timestamp,'" & ddlcabang.SelectedValue & "','APK')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    '//////INSERT INTO TRNGLDTL
                    Dim dvDtl As DataView = objTable.DefaultView
                    Dim vcashbankglamt, vcashbankglamtidr, vcashbankglamtusd As Double
                    ' HUTANG
                    dvDtl.RowFilter = "flagdtl <> 'OTHER'"
                    For c1 As Integer = 0 To dvDtl.Count - 1
                        If CurrencyOid.SelectedValue = 1 Then
                            vcashbankglamt = ToDouble(dvDtl(c1)("payamt"))
                            vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt"))
                            If Session("invCurrOid") = 1 Then
                                vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt")) * dRate
                            Else
                                vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt")) / dRate
                            End If
                        Else
                            vcashbankglamt = ToDouble(dvDtl(c1)("payamt"))
                            vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt"))
                            If Session("invCurrOid") = 1 Then
                                vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt")) / dRate
                            Else
                                vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt")) * dRate
                            End If
                        End If

                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,branch_code,glflag) VALUES " & _
                            "('" & cmpcode & "'," & Session("vJurnalDtl") & "," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & ToDouble(vcashbankglamt) & "," & ToDouble(vcashbankglamtidr) & "," & ToDouble(vcashbankglamtusd) & ",'" & Tchar(defcbno.Text) & "','A/P Koreksi | Supp. " & Tchar(suppnames.Text) & " | No. Cashbank Koreksi. " & ApNo.Text & " | No. Cashbank. " & defcbno.Text & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','" & cashbankoid.Text & "','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & ddlcabang.SelectedValue & "','" & lblPOST.Text.Trim & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""

                    ' LEBIH BAYAR
                    dvDtl.RowFilter = "flagdtl='OTHER' AND payamt > 0"

                    For c1 As Integer = 0 To dvDtl.Count - 1
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,branch_code,glflag) VALUES ('" & cmpcode & "'," & Session("vJurnalDtl") & "," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & ToDouble(vcashbankglamt) & "," & ToDouble(vcashbankglamtidr) & "," & ToDouble(vcashbankglamtusd) & ",'" & Tchar(defcbno.Text) & "','A/P Koreksi (Lebih Bayar) | Supp. " & Tchar(suppnames.Text) & " | No. Cashbank Koreksi. " & ApNo.Text & " | No. Cashbank. " & defcbno.Text & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','" & cashbankoid.Text & "','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & ddlcabang.SelectedValue & "','" & lblPOST.Text.Trim.Trim & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""
                    Dim vcashbankglamt2, vcashbankglamtidr2, vcashbankglamtusd2 As Double
                    If CurrencyOid.SelectedValue = 1 Then
                        vcashbankglamt2 = ToDouble(ToDouble(amtbelinettodtl.Text))
                        vcashbankglamtidr2 = ToDouble(ToDouble(amtbelinettodtl.Text))
                        If Session("invCurrOid") = 1 Then
                            vcashbankglamtusd2 = ToDouble(ToDouble(amtbelinettodtl.Text)) * dRate
                        Else
                            vcashbankglamtusd2 = ToDouble(ToDouble(amtbelinettodtl.Text)) / dRate
                        End If
                    Else
                        vcashbankglamt2 = ToDouble(ToDouble(amtbelinettodtl.Text))
                        vcashbankglamtusd2 = ToDouble(ToDouble(amtbelinettodtl.Text))
                        If Session("invCurrOid") = 1 Then
                            vcashbankglamtidr2 = ToDouble(ToDouble(amtbelinettodtl.Text)) / dRate
                        Else
                            vcashbankglamtidr2 = ToDouble(ToDouble(amtbelinettodtl.Text)) * dRate
                        End If
                    End If

                    ' CASH/BANK
                    Dim jhIdr, jhUsd As Double
                    Dim tbSls As DataTable = Session("tbldtl")
                    If CurrencyOid.SelectedValue = 1 Then
                        jhIdr = ToDouble(tbSls.Compute("SUM(payamt)", "").ToString)
                        jhUsd = ToDouble(tbSls.Compute("SUM(payamt)", "").ToString)
                        If Session("invCurrOid") = 1 Then
                            jhUsd = ToDouble(tbSls.Compute("SUM(payamt)", "").ToString) * dRate
                        Else
                            jhUsd = ToDouble(tbSls.Compute("SUM(payamt)", "").ToString) / dRate
                        End If
                    Else
                        jhIdr = ToDouble(tbSls.Compute("SUM(payamt)", "").ToString)
                        jhUsd = ToDouble(tbSls.Compute("SUM(payamt)", "").ToString)
                        If Session("invCurrOid") = 1 Then
                            jhIdr = ToDouble(tbSls.Compute("SUM(payamt)", "").ToString) / dRate
                        Else
                            jhIdr = ToDouble(tbSls.Compute("SUM(payamt)", "").ToString) * dRate
                        End If
                    End If

                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,branch_code,glflag) " & _
                        "VALUES ('" & cmpcode & "'," & Session("vJurnalDtl") & "," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & cashbankacctgoid.SelectedValue & ",'D'," & ToDouble(jhIdr) & "," & ToDouble(jhIdr) & "," & ToDouble(jhUsd) & ",'" & Tchar(defcbno.Text) & "','A/P Koreksi | Supp. " & Tchar(suppnames.Text) & " | No. Cashbank Koreksi. " & ApNo.Text & " | No. Cashbank. " & defcbno.Text & " | Note. " & Tchar(cashbanknote.Text) & "','" & cashbankoid.Text & "','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & ddlcabang.SelectedValue & "','" & lblPOST.Text & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1

                    ' KURANG BAYAR
                    Dim vcashbankglamt3, vcashbankglamtidr3, vcashbankglamtusd3 As Double
                    Dim DTCekV As DataTable = Session("tbldtl")
                    Dim ds As DataView = DTCekV.DefaultView
                    dvDtl.RowFilter = "flagdtl='OTHER' AND payamt < 0"

                    For c1 As Integer = 0 To dvDtl.Count - 1
                        If CurrencyOid.SelectedValue = 1 Then
                            vcashbankglamt3 = ToDouble(ds(c1)("payamt")) * -1
                            vcashbankglamtidr3 = ToDouble(ds(c1)("payamt")) * -1
                            If Session("invCurrOid") = 1 Then
                                vcashbankglamtusd3 = ToDouble(ds(c1)("payamt")) * -1 * dRate
                            Else
                                vcashbankglamtusd3 = ToDouble(ds(c1)("payamt")) * -1 / dRate
                            End If
                        Else
                            vcashbankglamt3 = ToDouble(ds(c1)("payamt")) * -1
                            vcashbankglamtusd3 = ToDouble(ds(c1)("payamt")) * -1
                            If Session("invCurrOid") = 1 Then
                                vcashbankglamtidr3 = ToDouble(ds(c1)("payamt")) * -1 / dRate
                            Else
                                vcashbankglamtidr3 = ToDouble(ds(c1)("payamt")) * -1 * dRate
                            End If
                        End If
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,branch_code,glflag) VALUES " & _
                            "('" & cmpcode & "'," & Session("vJurnalDtl") & "," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'D'," & ToDouble(vcashbankglamt3) & "," & ToDouble(vcashbankglamt3) & "," & ToDouble(vcashbankglamtusd3) & ",'" & Tchar(defcbno.Text) & "','A/P Koreksi (Kurang Bayar) | Supp. " & Tchar(suppnames.Text) & " | No. Cashbank Koreksi. " & ApNo.Text & " | No. Cashbank. " & defcbno.Text & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','" & cashbankoid.Text & "','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & ddlcabang.SelectedValue & "','" & lblPOST.Text & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""
                    ' Update lastoid GLMST
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalMst") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trnglmst'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    ' Update lastoid GLDTL
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalDtl") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trngldtl'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
            End If

            If lblPOST.Text = "POST" Then
                Dim objtablexx As DataTable : Dim objrowxx() As DataRow
                objtablexx = Session("tbldtl")
                objrowxx = objtablexx.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                Dim vcashbankglamt, vcashbankglamtidr, vcashbankglamtusd As Double
                For C1 As Int16 = 0 To objrowxx.Length - 1
                    If CurrencyOid.SelectedValue = 1 Then
                        vcashbankglamt = ToDouble(objrowxx(C1)("payamt").ToString)
                        vcashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString)
                        If Session("invCurrOid") = 1 Then
                            vcashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString) * dRate
                        Else
                            vcashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString) / dRate
                        End If
                    Else
                        vcashbankglamt = ToDouble(objrowxx(C1)("payamt").ToString)
                        vcashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString)
                        If Session("invCurrOid") = 1 Then
                            vcashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString) / dRate
                        Else
                            vcashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString) * dRate
                        End If
                    End If

                    'INSERT TO CASHBANK GL DULU
                    sSql = "INSERT INTO QL_cashbankgl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,cashbankglstatus,cashbankglres1,duedate,refno,createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                                    "('" & cmpcode & "'," & iCbdtloid & "," & cashbankoid.Text & "," & ToDouble(objrowxx(C1)("payacctgoid").ToString) & "," & ToDouble(vcashbankglamt) & "," & ToDouble(vcashbankglamtidr) & "," & ToDouble(vcashbankglamtusd) & ",'A/P Koreksi|No=" & Tchar(ApNo.Text) & " ~ " & Tchar(objrowxx(C1)("PAYNOTE").ToString) & "','" & lblPOST.Text & "','','" & CDate(toDate(PaymentDate.Text)) & "','" & Tchar(payrefno.Text.Trim) & "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & ddlcabang.SelectedValue & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    iCbdtloid += 1
                    'update realisasi tabel
                Next

                sSql = "Update QL_mstoid set lastoid=" & iCbdtloid & " Where tablename like 'QL_cashbankGL' and cmpcode like '%" & cmpcode & "%' " : objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                If payflag.SelectedValue = "VOUCHER" Then
                    Dim dtReal As DataTable = Session("tbldtl")
                    Dim realDt As DataView = dtReal.DefaultView
                    realDt.RowFilter = "RealOid=" & RealOid.Text & ""
                    Dim sTslunas As String = ""
                    For c1 As Integer = 0 To realDt.Count - 1
                        If ToDouble(realDt(c1)("amttrans").ToString) - ToDouble(realDt(c1)("payamt").ToString) > 0 Then
                            sTslunas = "BELUM"
                        Else
                            sTslunas = "LUNAS"
                        End If
                        sSql = "Update QL_trnrealisasi Set realisasiflag='" & sTslunas & "' Where realisasioid=" & RealOid.Text & ""
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Next
                End If
            End If
            objTrans.Commit() : objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox") : objTrans.Dispose() : Exit Sub
        End Try
        Session("tbldtl") = Nothing : Session("oid") = Nothing
        If lblPOST.Text = "POST" Then
            showMessage("Data telah diposting !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            showMessage("Data telah disimpan !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        End If
    End Sub

    Protected Sub amtpayment_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amtpayment.TextChanged
        CalculateTotalPayment()
    End Sub

    Protected Sub SqlDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs)

    End Sub

    Protected Sub CurrencyOid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillCurrencyRate(CurrencyOid.SelectedValue)
        ClearDtlAP(True) : Session("tbldtl") = Nothing
        GVDtlPayAP.DataSource = Nothing : GVDtlPayAP.DataBind()
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & Tchar(cashbankno.Text) & "'"
        PrintReport(cashbankno.Text, cKoneksi.ambilscalar(sSql))
    End Sub

    Protected Sub GVmstPAYAP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmstPAYAP.SelectedIndexChanged
        sSql = "SELECT count(-1) FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & GVmstPAYAP.SelectedDataKey.Item("cashbankno").ToString & "' and cashbankstatus in ('POST')"
        If cKoneksi.ambilscalar(sSql) <= 0 Then
            showMessage("Silahkan data diposting terlebih dahulu sebelum di print !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & GVmstPAYAP.SelectedDataKey.Item("cashbankno").ToString & "'"
        PrintReport(GVmstPAYAP.SelectedDataKey.Item("cashbankno").ToString, cKoneksi.ambilscalar(sSql))
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sQuery As String = Session("SearchAP")
        If sQuery <> "" Then
            Dim objTable As DataTable = cKoneksi.ambiltabel(Session("SearchAP"), "ql_trncashbankmst")
            Session("tbldata") = objTable
            GVmstPAYAP.DataSource = objTable
            GVmstPAYAP.DataBind()
            calcTotalInGrid()
        End If
    End Sub

    Protected Sub btnClearSupp_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub DP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblDPAmount.Visible = cbDP.Checked
        ddlDPNo.Visible = cbDP.Checked
        DPAmt.Visible = cbDP.Checked
        If cbDP.Checked Then
            initddlDP(cKoneksi.ambilscalar("SELECT trnSUPPoid FROM QL_trnbelimst WHERE cmpcode='" & cmpcode & "' AND trnbelimstoid=" & trnbelimstoid.Text))
            If ddlDPNo.Items.Count > 0 Then
                DPAmt.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT trndpapamt FROM QL_trndpap WHERE trndpapoid=" & ddlDPNo.SelectedValue())), 3)
            Else
                DPAmt.Text = ToMaskEdit(0, 3)
            End If

        End If
    End Sub

    Protected Sub ddlDPNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DPAmt.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT trndpapamt FROM QL_trndpap WHERE trndparoid=" & ddlDPNo.SelectedValue())), 3)
    End Sub

    Protected Sub chkOther_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SetOtherAccount(chkOther.Checked)
        InitOtherAcctg(DDLOtherType.SelectedValue)

        If DDLOtherType.SelectedValue = "-" Then
            If chkOther.Checked Then
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text) + ToDouble(otheramt.Text), 2)
            Else
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 2)
            End If
        End If
    End Sub

    Protected Sub DDLOtherType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitOtherAcctg(DDLOtherType.SelectedValue)
    End Sub

    Private Sub SetOtherAccount(ByVal bState As Boolean)
        otherAcctgoid.Enabled = bState
        lkbAddDtlSlisih.Visible = bState
        lkbClearDtlSlisih.Visible = bState
        If bState Then
            otherAcctgoid.CssClass = "inpText"
        Else
            otherAcctgoid.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub otheramt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DDLOtherType.SelectedValue = "-" Then
            If chkOther.Checked Then
                If ToDouble(amtpayment.Text) + ToDouble(otheramt.Text) > ToDouble(APAmt.Text) Then
                    showMessage("Total pembayaran tidak dapat > total tagihan", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    otheramt.Text = ToMaskEdit(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text), 2)
                End If
            End If
        End If
    End Sub

    Protected Sub lkbAddDtlSlisih_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sMsg As String = ""
        If otherAcctgoid.Items.Count < 1 Then
            sMsg &= "- Tidak ada no coa untuk perbedaan bayar !!<BR>"
        End If
        If ToDouble(amtdtlselisih.Text) <= 0 Then
            sMsg &= "- Jumlah Selisih Pembayaran must be > 0 !!<BR>"
        End If
        If dtlnoteselisih.Text.Trim.Length > 200 Then
            sMsg &= "- Maksimal note adalah 200 karakter !!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If Session("DtlSelisih") Is Nothing Then
            Dim nu As DataTable = SetTabelDetailSlisih()
            Session("DtlSelisih") = nu
        End If

        Dim objTable As DataTable : objTable = Session("DtlSelisih")
        Dim oRow As DataRow
        If stateDtlSls.Text = "New Selisih" Then
            oRow = objTable.NewRow
            oRow("selisihseq") = objTable.Rows.Count + 1
        Else
            oRow = objTable.Rows(gvDtlSelisih.SelectedIndex)
            oRow.BeginEdit()
        End If
        oRow("acctgoid") = otherAcctgoid.SelectedValue
        oRow("acctgdesc") = otherAcctgoid.SelectedItem.Text
        oRow("amtdtlselisih") = ToDouble(amtdtlselisih.Text)
        oRow("dtlnoteselisih") = dtlnoteselisih.Text

        If stateDtlSls.Text = "New Selisih" Then
            objTable.Rows.Add(oRow)
        Else
            oRow.EndEdit()
        End If
        'chkOther.Checked = False
        amtdtlselisih.Enabled = True : amtdtlselisih.CssClass = "inpText"
        NetPayment.Text = ToMaskEdit(ToDouble(amtbelinettodtl.Text) - ToDouble(TotalCost.Text), 2)
        Session("DtlSelisih") = objTable
        gvDtlSelisih.DataSource = Session("DtlSelisih")
        gvDtlSelisih.DataBind()
        ClearDtlSelisih()
        CalculateSelisih()
        RealOid.Text = 0
    End Sub

    Protected Sub lkbClearDtlSlisih_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDtlSelisih()
    End Sub

    Private Sub ClearDtlSelisih()
        If otherAcctgoid.Items.Count > 1 Then
            otherAcctgoid.SelectedIndex = 0
        End If
        amtdtlselisih.Text = "" : gvDtlSelisih.Columns(5).Visible = True
        gvDtlSelisih.SelectedIndex = -1
        dtlnoteselisih.Text = "" : stateDtlSls.Text = "New Selisih"
    End Sub

    Protected Sub gvDtlSelisih_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objTable As DataTable : objTable = Session("DtlSelisih")
        Dim dvTemp As DataView = objTable.DefaultView
        dvTemp.RowFilter = "selisihseq=" & gvDtlSelisih.SelectedDataKey("selisihseq").ToString
        otherAcctgoid.SelectedValue = dvTemp(0)("acctgoid").ToString
        amtdtlselisih.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtdtlselisih").ToString), 2)
        dtlnoteselisih.Text = dvTemp(0)("dtlnoteselisih").ToString
        dvTemp.RowFilter = ""
        gvDtlSelisih.Columns(5).Visible = False
        stateDtlSls.Text = "Update"
    End Sub

    Protected Sub gvDtlSelisih_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtlSelisih.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
        End If
    End Sub

    Protected Sub gvDtlSelisih_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtlSelisih.RowDeleting
        Dim idx As Integer = e.RowIndex
        Dim objTable As DataTable
        objTable = Session("DtlSelisih")
        objTable.Rows.RemoveAt(idx)

        'resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            objTable.Rows(C2)("selisihseq") = C2 + 1
        Next

        Session("DtlSelisih") = objTable
        gvDtlSelisih.DataSource = Session("DtlSelisih")
        gvDtlSelisih.DataBind()
        CalculateSelisih()
    End Sub

    Private Sub GenerateDefaultNo(ByVal sFlag As String, ByVal iAcctgOid As Integer, ByVal dDate As Date)
        ' BKK/10FKBE0001
        Dim sCBType As String
        Select Case sFlag
            Case "CASH"
                sCBType = "BKM"
            Case "NONCASH"
                sCBType = "BBM"
            Case "GIRO"
                sCBType = "BGM"
            Case "DP"
                sCBType = "BDM"
            Case Else
                sCBType = "BVM"
        End Select
        Dim cabang As String = GetStrData("select substring (gendesc,1,3) from ql_mstgen Where gencode='" & ddlcabang.SelectedValue & "' AND gengroup='CABANG'")

        Dim sNo As String = sCBType & "/" & cabang & "/" & Format(dDate, "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1 FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%' and branch_code='" & ddlcabang.SelectedValue & "'"
        defcbno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
    End Sub

    Protected Sub PaymentDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(toDate(PaymentDate.Text)) Then
            GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
        Else
            showMessage("Format tanggal salah !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            PaymentDate.Text = GetServerTime() : Exit Sub
        End If
    End Sub

    Protected Sub amtdtlselisih_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amtdtlselisih.TextChanged, DPAmt.TextChanged
        sender.text = ToMaskEdit(ToDouble(sender.text), 2)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        DataBindSupp()
        hiddenbtn2.Visible = True
        Panel1.Visible = True
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        trnsuppoid.Text = "" : suppname.Text = "" : ClearDtlAP()
        Session("tbldtl") = Nothing : GVDtlPayAP.DataSource = Nothing
        GVDtlPayAP.DataBind() : calcTotalInGridDtl()
        payflag.SelectedIndex = 0 : payflag_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub ClearDtlAP()
        Session("tbldtl") = Nothing
        GVDtlPayAP.DataSource = Nothing : GVDtlPayAP.DataBind()
        Session("DtlSelisih") = Nothing : gvDtlSelisih.DataSource = Nothing
        gvDtlSelisih.DataBind() : ClearDtlAP(True)
    End Sub

    Protected Sub ibtnSuppID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnSuppID.Click
        ModalPopupExtender1.Show()
        DataBindSupp()
        'Select Case DDLSuppID.SelectedValue
        '    Case "Code"
        '        filterGVS(txtFindSuppID.Text, "", cmpcode)
        '    Case "Name"
        '        filterGVS("", txtFindSuppID.Text, cmpcode)
        'End Select
    End Sub

    Protected Sub imbViewAlls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewAlls.Click
        txtFindSuppID.Text = "" : DataBindSupp()
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub CloseSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CloseSupp.Click
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, False)
        cProc.DisposeGridView(gvSupplier)
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        ClearDtlAP()
        Session("tbldtl") = Nothing : GVDtlPayAP.DataSource = Nothing
        GVDtlPayAP.DataBind() : calcTotalInGridDtl()
        payflag.SelectedIndex = 0 : payflag_SelectedIndexChanged(Nothing, Nothing)
        trnsuppoid.Text = gvSupplier.SelectedDataKey.Item("ID")
        suppnames.Text = gvSupplier.SelectedDataKey.Item("Name").ToString
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, False)
        cProc.DisposeGridView(gvSupplier)
        Fill_payflag()

    End Sub

    Protected Sub trndpapoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndpapoid.SelectedIndexChanged
        If trndpapoid.Items.Count > 0 Then
            FillDPBalance(trndpapoid.SelectedValue)
            FillDPAccount(trndpapoid.SelectedValue)
        Else
            FillDPBalance(0)
        End If
    End Sub

    Private Sub FillDPAccount(ByVal iDPOid As Integer)
        sSql = "SELECT trndpaPacctgoid FROM QL_trndpaP WHERE cmpcode='" & cmpcode & "' AND trndpaPoid='" & iDPOid & "'"
        cashbankacctgoid.SelectedValue = CStr(cKoneksi.ambilscalar(sSql))
    End Sub

    Private Sub FillDPBalance(ByVal iDPOid As Integer)
        sSql = "SELECT ISNULL(dp.trndpaPamt-dp.trndpaPacumamt,0) "
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= "+ISNULL((SELECT SUM(pap.payamt) FROM QL_trnpayaP pap " & _
                "WHERE pap.paybankoid=dp.trndpaPoid AND pap.cashbankoid='" & Session("oid") & "'),0) "
        End If
        sSql &= "FROM QL_trndpaP dp WHERE cmpcode='" & cmpcode & "' AND trndpaPoid='" & iDPOid & "'"
        dpbalance.Text = ToMaskEdit(ToDouble(CStr(cKoneksi.ambilscalar(sSql))), 2)

        payrefno.Text = RTrim(GetStrData("select trndpaPno from ql_trndpaP where trndpaPoid=" & iDPOid))
    End Sub

    Private Sub InitDPAP(ByVal iSuppOid As Integer)
        sSql = "SELECT dp.trndpaPoid,dp.trndpaPno+' ('+CONVERT(VARCHAR,dp.trndpaPdate,101)+')' AS no FROM QL_trndpaP dp WHERE cmpcode='" & cmpcode & "' AND dp.trndpaPstatus='POST' AND dp.SUPPoid=" & iSuppOid & " AND dp.trndpaPacumamt > 0 /*AND (ISNULL(dp.trndpaPamt-dp.trndpaPacumamt,0)*/"
        'If Session("oid") <> Nothing And Session("oid") <> "" Then
        '    sSql &= "+ISNULL((SELECT SUM(par.payamt) FROM QL_trnpayaP par WHERE par.paybankoid=dp.trndpaPoid AND par.cashbankoid='" & Session("oid") & "'),0)"
        'End If
        sSql &= "/*)>0*/ ORDER BY dp.trndpaPdate,dp.trndpaPno "
        If GetStrData(sSql) = "?" Then
            showMessage("Tidak ada DP Untuk Supplier " & suppnames.Text & "", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        FillDDL(trndpapoid, sSql)
    End Sub

    Protected Sub CREDITCLEAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CREDITCLEAR.Click
        payrefno.Text = ""
        code.Text = ""
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA(cashbankno.Text, cmpcode, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = NewMaskEdit(e.Row.Cells(3).Text)
            e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
        End If
    End Sub

    Protected Sub GVmstPAYAP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVmstPAYAP.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnReturAP.aspx?awal=true")
        End If
    End Sub

    Protected Sub CurrencyOid_SelectedIndexChanged1(ByVal sender As Object, ByVal e As EventArgs) Handles CurrencyOid.SelectedIndexChanged
        ClearDtlAP()
        If Session("oid") = "" Then
            ClearDtlAP()
        Else
            'FillTextBox(Session("oid"))
        End If
    End Sub

    Protected Sub sRealBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindReal()
    End Sub

    Protected Sub RealGv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        RealNo.Text = RealGv.SelectedDataKey.Item("realisasino").ToString
        RealOid.Text = RealGv.SelectedDataKey.Item("realisasioid").ToString
        dpbalance.Text = ToMaskEdit(ToDouble(RealGv.SelectedDataKey.Item("realisasiamt")), 3)
        AmtReal.Text = ToMaskEdit(ToDouble(RealGv.SelectedDataKey.Item("realisasiamt")), 3)
        RealGv.Visible = False
    End Sub

    Protected Sub RealGv_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub RealGv_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        RealGv.PageIndex = e.NewPageIndex
        BindReal() : RealGv.Visible = True
    End Sub

    Protected Sub EraseV_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        RealOid.Text = 0 : RealNo.Text = ""
        RealGv.Visible = False
    End Sub
#End Region

    Protected Sub ddlcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcabang.SelectedIndexChanged
        Fill_payflag()
    End Sub

    Protected Sub GvRetAp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvRetAp.PageIndexChanging
        GvRetAp.PageIndex = e.NewPageIndex
        BindApNo() : GvRetAp.Visible = True
    End Sub

    Protected Sub GvRetAp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvRetAp.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub GvRetAp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvRetAp.SelectedIndexChanged
        Dim sNoDPAP As String = ""
        RetcbOidAp.Text = GvRetAp.SelectedDataKey.Item("cashbankoid")
        ApNo.Text = GvRetAp.SelectedDataKey.Item("cashbankno").ToString
        If GvRetAp.SelectedDataKey.Item("cashbanktype").ToString.ToUpper = "BKK" Then
            sNoDPAP = "CASH"
        ElseIf GvRetAp.SelectedDataKey.Item("cashbanktype").ToString.ToUpper = "BBK" Then
            sNoDPAP = "NONCASH"
        ElseIf GvRetAp.SelectedDataKey.Item("cashbanktype").ToString.ToUpper = "BGK" Then
            sNoDPAP = "GIRO"
        ElseIf GvRetAp.SelectedDataKey.Item("cashbanktype").ToString.ToUpper = "BDK" Then
            If payflag.SelectedValue = "DP" Then
                InitDPAP(trnsuppoid.Text)
                If trndpapoid.Items.Count > 0 Then
                    FillDPBalance(trndpapoid.SelectedValue)
                    FillDPAccount(trndpapoid.SelectedValue)
                Else
                    FillDPBalance(0)
                End If
            End If
            sNoDPAP = "DP"
            trndpapoid.SelectedValue = GvRetAp.SelectedDataKey.Item("trndpapoid")
            trndpapoid_SelectedIndexChanged(Nothing, Nothing)
        ElseIf GvRetAp.SelectedDataKey.Item("cashbanktype").ToString.ToUpper = "BVK" Then
            sNoDPAP = "VOUCHER"
        End If
        payflag.SelectedValue = sNoDPAP.ToUpper
        amtbelinettodtl.Text = ToMaskEdit(ToDouble(GvRetAp.SelectedDataKey.Item("PayAmt")), 3)
        amtbelinettodtl4.Text = ToMaskEdit(ToDouble(GvRetAp.SelectedDataKey.Item("PayAmt")), 3)
        Fill_payflag()
        cashbankacctgoid.SelectedValue = GvRetAp.SelectedDataKey.Item("cashbankacctgoid").ToString
        GvRetAp.Visible = False
        BindPInya()
    End Sub

    Protected Sub SearchApNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SearchApNo.Click
        If trnsuppoid.Text = "" Or trnsuppoid.Text.Trim = "" Then
            showMessage("Maaf, Anda belum memilih supplier, Silahkan pilih supplier pada kolom supplier kemudian klik icon loop..!", CompnyName & " - INFORMASI", 3, "modalMsgBox")
        Else
            BindApNo()
        End If
    End Sub

    Protected Sub EraseApNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EraseApNo.Click
        RetcbOidAp.Text = "0" : ApNo.Text = ""
        GvRetAp.Visible = False : GVDtlPayAP.Visible = False
    End Sub
End Class