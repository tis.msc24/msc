Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmReportPenerimaanBarang
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)

    Dim xreader As SqlDataReader
    Dim xKon As New Koneksi
    Dim sSql As String = ""
    Dim dsData As New DataSet
    Dim dv As DataView
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    
    Dim report As New ReportDocument
    Dim folderReport As String = "~/report/"
    Dim param As String = ""
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Procedure"

    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 3 Then 'Warning
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        lblCaption.Text = strCaption : Validasi.Text = sMessage
        CProc.SetModalPopUpExtender(btnMsg, panelMsg, mpeMsg, True)
    End Sub

    Private Sub Cabangddl()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLcabang, sSql)
            Else
                FillDDL(DDLcabang, sSql)
                DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
                DDLcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLcabang, sSql)
            DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
            DDLcabang.SelectedValue = "ALL"
        End If
    End Sub

    Sub showPrint(ByVal tipe As String)
        Try
            Dim sWhere As String = "Where r.cmpcode='MSC'"
            If DDLcabang.SelectedValue <> "ALL" Then
                sWhere &= " AND r.branch_code='" & DDLcabang.SelectedValue & "'"
            End If
            sWhere &= " And " & FilterDDL.SelectedValue & " Like '%" & Tchar(FilterText.Text) & "%'"
            If txtTgl1.Text <> "" And txtTgl2.Text <> "" Then
                Try
                    Dim dat1 As Date = CDate(toDate(txtTgl1.Text))
                    Dim dat2 As Date = CDate(toDate(txtTgl2.Text))
                Catch ex As Exception
                    showMessage("Format Tanggal Salah - Gunakan format dd/MM/yyyy !!", 2) : Exit Sub
                End Try
                If CDate(toDate(txtTgl1.Text)) > CDate(toDate(txtTgl2.Text)) Then
                    showMessage("Tanggal mulai tidak boleh lebih besar dari tanggal akhir !!", 2)
                    Exit Sub
                End If
                sWhere &= " And convert(date,r.createtime, 101) Between '" & CDate(toDate(txtTgl1.Text)) & "' AND '" & CDate(toDate(txtTgl2.Text)) & "'"
            End If

            If saldoawal.SelectedValue <> "ALL" Then
                sWhere &= "AND r.reqservicecat='" & saldoawal.SelectedValue & "'"
            End If

            If cbstatus.SelectedValue = "Status" Then
                If StatusDDL.SelectedValue <> "All" Then
                    sWhere &= " AND r.reqstatus = '" & StatusDDL.SelectedValue & "'"
                End If
                sWhere &= " AND r.reqstatus <> 'paid' "
            End If

            If cbstatus.SelectedValue = "Paid" Then
                sWhere &= " AND r.reqstatus = 'paid' "
            End If

            If DDLGaransi.SelectedValue <> "ALL" Then
                sWhere &= " AND t.typegaransi = '" & DDLGaransi.SelectedValue & "'"
            End If

            If tipe = "EXCEL" Then
                report.Load(Server.MapPath(folderReport & "ReportPenerimaanBarangExl.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "ReportPenerimaanBarang.rpt"))
            End If

            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("dPeriode", txtTgl1.Text & " - " & txtTgl2.Text)
            report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            If tipe = "PRINT" Then
                CRVPenerimaanBarang.DisplayGroupTree = False
                CRVPenerimaanBarang.ReportSource = report
            ElseIf tipe = "EXCEL" Then 
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "LaporanPenerimaanService" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose()
            Else 
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "LaporanPenerimaanService" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose()
            End If

        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub resetReport()
        CRVPenerimaanBarang.ReportSource = Nothing
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~\other\login.aspx")
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            'Session.Timeout = 60
            Response.Redirect("frmPenerimaanService.aspx") ' recall lagi untuk clear
        End If
        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Laporan Penerimaan Service"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not Page.IsPostBack Then
            Session("showReport") = Nothing
            txtTgl1.Text = Format(GetServerTime(), "01/" & "MM/yyyy")
            txtTgl2.Text = Format(GetServerTime().Date, "dd/MM/yyyy ")
            Cabangddl()
        End If
        If Session("showReport") = True Then
            showPrint("PRINT")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        resetReport()
        FilterDDL.SelectedIndex = 0 : FilterText.Text = ""
        cbstatus.SelectedIndex = 0 : StatusDDL.SelectedIndex = 0
        txtTgl1.Text = Format(GetServerTime, "01/" & "MM/yyyy")
        txtTgl2.Text = Format(GetServerTime.Date, "dd/MM/yyyy ")
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExcel.Click
        If txtTgl1.Text <> "" And txtTgl2.Text = "" Then
            showMessage("Isi Tanggal akhir !!", 2) : Exit Sub
        ElseIf txtTgl1.Text = "" And txtTgl2.Text <> "" Then
            showMessage("Isi Tanggal awal !!", 2) : Exit Sub
        End If
        showPrint("EXCEL")
    End Sub

    Protected Sub btnPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPDF.Click
        If txtTgl1.Text <> "" And txtTgl2.Text = "" Then
            showMessage("Isi Tanggal akhir !!", 2) : Exit Sub
        ElseIf txtTgl1.Text = "" And txtTgl2.Text <> "" Then
            showMessage("Isi Tanggal awal !!", 2) : Exit Sub
        End If
        showPrint("PDF")
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        showPrint("PRINT")
    End Sub

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        resetReport()
        Response.Redirect("frmPenerimaanService.aspx?awal=true")
        btnMsg.Visible = False
        panelMsg.Visible = False
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
#End Region

    Protected Sub CRVPenerimaanBarang_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CRVPenerimaanBarang.Navigate
        showPrint("PRINT")
    End Sub
End Class
