﻿Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class ReportForm_rptSN
    Inherits System.Web.UI.Page

#Region "Variable"

    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Function Get_Branch() As String
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim refcode As String = String.Empty

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT genoid FROM QL_mstgen where gengroup = 'cabang' and gencode = '" & dd_branch.SelectedValue & "'"
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            refcode = rdr("genoid")
        End While
        conn2.Close()
        Return refcode

    End Function
#End Region

#Region "Procedure"
   

    Public Sub initddl()
        'branch
        If Session("branch_id") = "01" Then
            dd_branch.Enabled = True
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
            FillDDL(dd_branch, sSql)
            dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            dd_branch.SelectedValue = "SEMUA BRANCH"

            ddlLocation.Items.Clear()
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' ORDER BY a.gendesc"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"

        Else
            dd_branch.Enabled = False
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and  gencode = '" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' and a.genother2=" & Get_Branch() & " ORDER BY a.gendesc"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"
        End If

        ' sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & dd_branch.SelectedValue & "' ORDER BY a.gendesc"

        

        '---------------------------------------------------------------------------------------

    End Sub

    Public Sub showPrint(ByVal showtype As String)
        Dim sWhere As String = "  "
        lblkonfirmasi.Text = ""

        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If


        Try
            Dim namaPDF As String = ""
            Dim Periode As String = "and ds.createtime BETWEEN '" & CDate(toDate(dateAwal.Text.Trim)) & "' and '" & CDate(toDate(dateAkhir.Text.Trim)) & "'"
            Dim swherebranch As String = ""
            Dim swhereloc As String = ""
            If Session("branch_id") = "01" Then
                swherebranch = IIf(dd_branch.SelectedValue = "SEMUA BRANCH", " AND ds.BRANCH_CODE Like '%%' ", " AND ds.BRANCH_CODE ='" & dd_branch.SelectedValue & "' ")
                swhereloc = IIf(ddlLocation.SelectedValue = "ALL LOCATION", " ", " and ds.locoid=" & ddlLocation.SelectedValue & " ")
            Else
                swherebranch = IIf(dd_branch.SelectedValue = "SEMUA BRANCH", "AND ds.BRANCH_CODE ='" & Session("branch_id") & "' ", " AND ds.BRANCH_CODE ='" & dd_branch.SelectedValue & "' ")

                swhereloc = IIf(ddlLocation.SelectedValue = "ALL LOCATION", "AND ds.BRANCH_CODE ='" & Session("branch_id") & "' ", " and ds.locoid=" & ddlLocation.SelectedValue & " ")
            End If

            Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, "  and  ds.itemoid=" & itemoid.Text, " ")
           

            Dim swhereawal2 As String = ""
            Dim dwhereawal2 As String = ""
            Dim dwhere As String = ""

            swhere = swherebranch & swhereloc & swhereitem
            vReport = New ReportDocument
            vReport.Load(Server.MapPath("~\Report\rptSN.rpt"))
            vReport.SetParameterValue("sWhare", sWhere)
            vReport.SetParameterValue("Awal", CDate(toDate(dateAwal.Text.Trim)))
            vReport.SetParameterValue("Akhir", CDate(toDate(dateAkhir.Text.Trim)))

            namaPDF = "Stock_Detail_"
            vReport.SetParameterValue("cmpcode", System.Configuration.ConfigurationManager.AppSettings("CompanyName"))

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            If showtype = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "view" Then
                Session("diprint") = "True"
                crv.DisplayGroupTree = False
                crv.ReportSource = vReport
            End If

        Catch ex As Exception

            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))

        End Try
    End Sub

    Public Sub BindDataListItem()
        Dim sWhare As String = ""
        If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
            sWhare = "and d.branch_code = " & dd_branch.SelectedValue
        End If

        If ddlLocation.SelectedValue <> "ALL LOCATION" Then
            sWhare &= sWhare & " and itd.locoid = " & ddlLocation.SelectedValue
        End If


        sSql = "select DISTINCT d.itemoid, m.itemcode, m.itemdesc,m.itempriceunit1,m.itempriceunit2,m.itempriceunit3,m.itemoid, g.gendesc satuan1,g2.gendesc satuan2, g3.gendesc satuan3,m.konversi1_2, m.konversi2_3 , m.merk, (SELECT gendesc FROM QL_mstgen WHERE gencode =  d.branch_code AND gengroup = 'cabang') branch_code  from ql_mstitem m inner JOIN QL_mstItem_branch d ON d.itemcode = m.itemcode and d.itemoid = m.itemoid  INNER JOIN QL_mstitemDtl itd ON itd.itemoid = m.itemoid AND itd.itemcode = m.itemcode and itd.itemoid = d.itemoid and itd.itemcode = d.itemcode inner join ql_mstgen g on g.genoid=m.satuan1 and m.itemflag='AKTIF'  inner join ql_mstgen g2 on g2.genoid=m.satuan2  inner join ql_mstgen g3 on g3.genoid=m.satuan3   WHERE m.cmpcode = '" & CompnyCode & "' and m.has_SN = '1' " & sWhare & "  AND (m.itemdesc like '%" & Tchar(itemname.Text) & "%' or m.itemcode like '%" & Tchar(itemname.Text) & "%' or m.merk like '%" & Tchar(itemname.Text) & "%')"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "listofitem")
        gvItem.DataSource = dtab
        gvItem.DataBind()
        Session("listofitem") = dtab
        gvItem.Visible = True
    End Sub

#End Region

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptSN.aspx")
        End If

  
        Page.Title = CompnyName & " - Serial Number Report "
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrint("view")
                dateAwal.Visible = True
            End If
        Else
            initddl()
            'Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
            dateAwal.Text = Format(Now, "01/MM/yyyy")
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub dd_branch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dd_branch.SelectedIndexChanged
        Dim xbranch As String = String.Empty
        xbranch = Get_Branch()
        If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' and a.genother2=" & xbranch & " ORDER BY a.gendesc"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"
        Else
            ddlLocation.Items.Clear()
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' ORDER BY a.gendesc"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"
        End If
        itemname.Text = ""
        itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub btnSearchItem_Click(sender As Object, e As ImageClickEventArgs) Handles btnSearchItem.Click
        BindDataListItem()
    End Sub

    Protected Sub btnEraseItem_Click(sender As Object, e As ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        itemname.Text = ""
        itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvItem.SelectedIndexChanged
        crv.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
    End Sub

    Protected Sub btnViewReport_Click(sender As Object, e As ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("view")
    End Sub

    Protected Sub ibPDF_Click(sender As Object, e As ImageClickEventArgs) Handles ibPDF.Click
        showPrint("pdf")
    End Sub

    Protected Sub ibexcel_Click(sender As Object, e As ImageClickEventArgs) Handles ibexcel.Click
        showPrint("excel")
    End Sub
End Class
