Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class MstPriceExpedisi
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Private cRate As New ClassRate
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Public Function GetTransID() As String
        Return (Eval("mstexpoid"))
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True" 
                                    dtView(0)("HargaExp") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    dtView(0)("keterangan") = GetTextBoxValue(row.Cells(11).Controls)
                                    If dtView2.Count > 0 Then 
                                        dtView2(0)("priceplatinum") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                        dtView2(0)("keterangan") = GetTextBoxValue(row.Cells(11).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("HargaExp") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    dtView(0)("keterangan") = GetTextBoxValue(row.Cells(11).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedures"

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_mstpriceexpdtl")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("branch_code", Type.GetType("System.String"))
        dtlTable.Columns.Add("mstexpdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("PicItem", Type.GetType("System.String"))
        dtlTable.Columns.Add("Unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("panjang", Type.GetType("System.Double"))
        dtlTable.Columns.Add("lebar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("tinggi", Type.GetType("System.Double"))
        dtlTable.Columns.Add("beratvolume", Type.GetType("System.Double"))
        dtlTable.Columns.Add("kubik", Type.GetType("System.Double"))
        dtlTable.Columns.Add("HargaExp", Type.GetType("System.Double"))
        dtlTable.Columns.Add("keterangan", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                If Session("branch_id") = "01" Then
                    FillDDL(DDLCabang, sSql)
                    DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
                    DDLCabang.SelectedValue = "ALL"
                Else
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(DDLCabang, sSql)
                End If
            Else
                FillDDL(DDLCabang, sSql)
                DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
                DDLCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLCabang, sSql)
            DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
            DDLCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode, gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlBranch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                If Session("branch_id") = "01" Then
                    FillDDL(ddlBranch, sSql)
                Else
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(ddlBranch, sSql)
                End If
            Else
                FillDDL(ddlBranch, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlBranch, sSql)
        End If
    End Sub

    Private Sub BindData()
        Try
            sSql = "Select branch_code, g.gendesc cabang, mstexpoid, containersize, containerprice, crtuser, Convert(Varchar(10), crttime,103) crtdate, Convert(Varchar(10), crttime,108) crttime, active_flag From QL_mstpriceexp e INNER JOIN QL_mstgen g ON g.gencode=e.branch_code AND g.gengroup='CABANG' WHERE e.cmpcode='" & CompnyCode & "' AND " & FilterDDLMst.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextMst.Text) & "%'"
            If DDLActive_flag.SelectedValue <> "ALL" Then
                sSql &= " AND active_flag='" & DDLActive_flag.SelectedValue & "'"
            End If

            If DDLCabang.SelectedValue <> "ALL" Then
                sSql &= " AND branch_code='" & DDLCabang.SelectedValue & "'"
            End If

            sSql &= " Order BY mstexpoid"
            Session("TblMst") = cKon.ambiltabel(sSql, "ql_mst")
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
            lblViewInfo.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
        
    End Sub

    Private Sub InitJenisKatalog()
        sSql = "Select DISTINCT stockflag,JenisNya From (Select stockflag,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya From ql_mstitem i WHERE itemflag='AKTIF') it"
        FillDDL(ddlType, sSql)
        ddlType.Items.Add(New ListItem("ALL", "ALL"))
        ddlType.SelectedValue = "ALL"
    End Sub

    Private Sub BindMaterial()
        Try
            sSql = "Select 'False' AS CheckValue, * From (Select i.itemoid, i.itemcode, i.itemdesc, gr.gendesc ItemGroup, (Select PERSONNAME from QL_MSTPERSON pic Where i.personoid=pic.PERSONOID) PicItem, 'BUAH' Unit, i.merk, i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya, i.dimensi_p panjang, i.dimensi_l lebar, i.dimensi_t tinggi, i.beratvolume, ROUND(1000000/Case When i.beratvolume=0 Then 1 Else i.beratvolume End,4) kubik, '' keterangan, i.createuser, i.crttime, Cast(CEILING(" & ToDouble(containerprice.Text) / ToDouble(containersize.Text) & "/((1000000/Case When i.beratvolume=0 Then 1 Else i.beratvolume End))) AS Money) HargaExp From ql_mstitem i INNER JOIN QL_mstgen gr ON gr.genoid=i.itemgroupoid AND gengroup='ITEMGROUP' WHERE itemflag='AKTIF') dt Order BY itemdesc"
            Dim dtMat As DataTable = cKon.ambiltabel(sSql, "ql_mstitem")
            If dtMat.Rows.Count > 0 Then
                Session("TblMat") = dtMat
                Session("TblMatView") = dtMat
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            Else
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
       
    End Sub

    Private Sub FillTextBox(ByVal sNo As String)
        Try
            ddlBranch.SelectedValue = Session("CommandName")
            sSql = "Select e.mstexpoid, e.branch_code, e.containersize, e.containerprice, e.crtuser, e.crttime, e.updtime, e.upduser From QL_mstpriceexp e INNER JOIN QL_mstgen g ON g.gencode=e.branch_code AND g.gengroup='CABANG' Where e.branch_code='" & ddlBranch.SelectedValue & "' AND e.mstexpoid=" & sNo & ""
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    ddlBranch.SelectedValue = Trim(xreader("branch_code").ToString)
                    mstexpoid.Text = Integer.Parse(xreader("mstexpoid"))
                    containersize.Text = ToMaskEdit(ToDouble(xreader("containersize")), 3)
                    containerprice.Text = ToMaskEdit(ToDouble(xreader("containerprice")), 3)
                    createuser.Text = xreader("crtuser").ToString
                    createtime.Text = xreader("crttime").ToString
                    updtime.Text = Trim(xreader("updtime").ToString)
                    upduser.Text = Trim(xreader("upduser").ToString)
                End While
            End If
            conn.Close()

            sSql = "SELECT ROW_NUMBER() OVER (ORDER BY e.mstexpdtloid ASC) AS seq, mstexpoid, i.itemoid, i.itemcode, i.itemdesc, gr.gendesc ItemGroup, (Select PERSONNAME from QL_MSTPERSON pic Where i.personoid=pic.PERSONOID) PicItem, 'BUAH' Unit, i.merk, i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya,e.panjang, e.lebar, e.tinggi, e.volume beratvolume, e.kubiksize, ROUND(1000000/i.beratvolume,4) kubik, '' keterangan, i.createuser, i.crttime, Cast(CEILING(" & ToDouble(containerprice.Text) / ToDouble(containersize.Text) & "/((1000000/i.beratvolume))) AS Money) HargaExp, e.priceexpedisi, e.branch_code From QL_mstpriceexpdtl e INNER JOIN ql_mstitem i ON i.itemoid=e.itemoid INNER JOIN QL_mstgen gr ON gr.genoid=i.itemgroupoid AND gengroup='ITEMGROUP' WHERE itemflag='AKTIF' AND e.mstexpoid=" & sNo & " AND e.branch_code='" & ddlBranch.SelectedValue & "'"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
            Session("TblDtl") = dt
            gvFindCrd.DataSource = Session("TblDtl")
            gvFindCrd.DataBind() : gvFindCrd.Visible = True
            imbSave.Visible = True : BtnAddItem.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub PrintReport()
        Try
            Dim sWhere As String = "WHERE e.cmpcode='" & CompnyCode & "' AND " & FilterDDLMst.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextMst.Text) & "%'"
            If DDLCabang.SelectedValue <> "ALL" Then
                sWhere &= " AND e.branch_code='" & DDLCabang.SelectedValue & "'"
            End If

            sWhere &= " Order BY itemdesc"
            report.Load(Server.MapPath(folderReport & "RptPriceExpedisi.rpt"))
            report.SetParameterValue("sWhere", sWhere)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "RptPriceExpedisi" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            Catch ex As Exception
                report.Close()
                report.Dispose()
            End Try
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Master\MstPriceExp.aspx")
        End If
        upduser.Text = Session("UserID")
        updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

        Page.Title = CompnyName & " - Master Price Expedisi"
        Session("oid") = Request.QueryString("oid")

        imbPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for APPROVAL?');")
        imbDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            fDDLBranch() : InitJenisKatalog() : InitDDLBranch()
            If (Session("oid") <> Nothing And Session("oid") <> "") Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                imbSave.Visible = False
            Else
                createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
                TabContainer1.ActiveTabIndex = 0
                imbSave.Visible = True
            End If
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub 

    Protected Sub gvFindCrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFindCrd.PageIndexChanging
        UpdateCheckedMat()
        gvFindCrd.PageIndex = e.NewPageIndex
        Dim dtDtl As DataTable = Session("TblCrd")
        gvFindCrd.DataSource = dtDtl : gvFindCrd.DataBind()
    End Sub

    Protected Sub gvFindCrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFindCrd.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
        End If
        'Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
        'For Each myControl As System.Web.UI.Control In cc
        '    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
        '        If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
        '        Else
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
        '        End If
        '    End If
        'Next

        'cc = e.Row.Cells(4).Controls
        'For Each myControl As System.Web.UI.Control In cc
        '    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
        '        If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
        '        Else
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
        '        End If
        '    End If
        'Next

        'cc = e.Row.Cells(5).Controls
        'For Each myControl As System.Web.UI.Control In cc
        '    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
        '        If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
        '        Else
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
        '        End If
        '    End If
        'Next

        'cc = e.Row.Cells(6).Controls
        'For Each myControl As System.Web.UI.Control In cc
        '    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
        '        If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
        '        Else
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
        '        End If
        '    End If
        'Next

        'cc = e.Row.Cells(7).Controls
        'For Each myControl As System.Web.UI.Control In cc
        '    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
        '        If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
        '        Else
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
        '        End If
        '    End If
        'Next

        'cc = e.Row.Cells(8).Controls
        'For Each myControl As System.Web.UI.Control In cc
        '    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
        '        If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
        '        Else
        '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
        '        End If
        '    End If
        'Next
    End Sub

    Protected Sub imbPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPosting.Click
        imbSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        'UpdateCheckedMat()
        Dim sMsg As String = "", objTable As DataTable = Session("TblDtl"), OidItem As String = "", itemdesc As String = ""
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- No detail data.<BR>"
        Else 
            If objTable.Rows.Count < 1 Then
                sMsg &= "- No detail data.<BR>"
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then 
                For i As Integer = 0 To objTable.Rows.Count - 1
                    OidItem &= objTable.Rows(i)("itemoid") & ","
                Next
                Try
                    sSql = "SELECT em.mstexpoid, e.itemoid, i.itemdesc From QL_mstpriceexpdtl e INNER JOIN QL_mstitem i ON i.itemoid=e.itemoid INNER JOIN QL_mstpriceexp em ON em.mstexpoid=e.mstexpoid Where em.active_flag='ACTIVE' AND e.branch_code='" & ddlBranch.SelectedValue & "' AND e.itemoid IN (" & Left(OidItem, OidItem.Length - 1) & ")"
                    Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
                    If dt.Rows.Count > 0 Then
                        For d As Integer = 0 To dt.Rows.Count - 1
                            itemdesc &= "- Draft " & dt.Rows(d)("mstexpoid") & " : " & dt.Rows(d)("itemdesc") & "<br>"
                        Next
                        sMsg &= "- Maaf, <br >" & itemdesc & " sudah pernah pernah di input ..!!<br>"
                    End If
                Catch ex As Exception
                    showMessage(ex.ToString & sSql, 2) : Exit Sub
                End Try
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If
        If Session("oid") = Nothing Or Session("oid") = "" Then
            mstexpoid.Text = GenerateID("QL_mstpriceexp", CompnyCode)
        End If

        Dim mstexpdtloid As Integer = GenerateID("QL_mstpriceexpdtl", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO [QL_mstpriceexp] ([cmpcode], [branch_code], [mstexpoid], [containersize], [containerprice], [crtuser], [crttime], [upduser], [updtime], [active_flag]) VALUES ('" & CompnyCode & "', '" & ddlBranch.SelectedValue & "', " & Integer.Parse(mstexpoid.Text) & ", " & ToDouble(containersize.Text) & ", " & ToDouble(containerprice.Text) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & active_flag.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & Integer.Parse(mstexpoid.Text) & " Where tablename ='QL_mstpriceexp' And cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() 
            Else
                sSql = "UPDATE [QL_mstpriceexp] SET [containersize] = " & ToDouble(containersize.Text) & ", [containerprice] = " & ToDouble(containerprice.Text) & ", [upduser] = '" & Session("UserID") & "', [updtime]=CURRENT_TIMESTAMP, [active_flag]='" & active_flag.SelectedValue & "' WHERE mstexpoid=" & Integer.Parse(mstexpoid.Text) & ""
            End If

            For C1 As Integer = 0 To objTable.Rows.Count - 1
                sSql = "INSERT INTO [QL_mstpriceexpdtl] ([cmpcode], [branch_code], [mstexpdtloid], [mstexpoid], [seq], [itemoid], [panjang], [lebar], [tinggi], [volume], [kubiksize], [priceexpedisi], [crtuser], [crttime], [upduser], [updtime]) VALUES ('" & CompnyCode & "', '" & ddlBranch.SelectedValue & "', " & Integer.Parse(mstexpdtloid) & ", " & Integer.Parse(mstexpoid.Text) & ", " & objTable.Rows.Count + 1 & ", " & Integer.Parse(objTable.Rows(C1)("itemoid")) & ", " & ToDouble(objTable.Rows(C1)("panjang").ToString) & ", " & ToDouble(objTable.Rows(C1)("lebar").ToString) & ", " & ToDouble(objTable.Rows(C1)("tinggi").ToString) & ", " & ToDouble(objTable.Rows(C1)("beratvolume").ToString) & ", " & ToDouble(objTable.Rows(C1)("kubik").ToString) & ", " & ToDouble(objTable.Rows(C1)("HargaExp").ToString) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                mstexpdtloid += 1
            Next

            sSql = "UPDATE ql_mstoid set lastoid = " & mstexpdtloid - 1 & " Where tablename = 'QL_mstpriceexpdtl' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    imbSave_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.ToString & sSql, 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.ToString & sSql, 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, 1) : Exit Sub
        End Try
        Response.Redirect("~\Master\MstPriceExp.aspx?awal=true")
    End Sub

    Protected Sub imbDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, 1) : Exit Sub
        End Try
        Response.Redirect("~\Master\MstPriceExp.aspx?awal=true")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("~\Master\MstPriceExp.aspx?awal=true")
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        'If Session("TblDtl") Is Nothing Then
        '    showMessage("Please define detail data first!", 2, "")
        '    Exit Sub
        'End If
        'If UpdateCheckedMat() Then
        '    ShowReport()
        'End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click 
        FilterTextMst.Text = "" : BindData()
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        'UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dv As DataView = Session("TblMst").DefaultView
            If dv.Count < 1 Then
                dv.RowFilter = ""
                showMessage("Please Select data to Print first.", 2)
            Else
                dv.RowFilter = ""
                PrintReport()
            End If
        Else
            showMessage("Please Select data to Print first.", 2)
        End If
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        'UpdateCheckedValue()
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting 
        Dim dt As DataTable = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then 
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub lkbSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("CommandName") = sender.CommandName
        Response.Redirect("~\Master\MstPriceExp.aspx?oid=" & sender.ToolTip & "&cashbankstatus=" & Session("CommandName") & "")
    End Sub  

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        'FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        'If UpdateCheckedMat() Then
        '    Session("TblMatView") = Session("TblMat")
        '    gvListMat.DataSource = Session("TblMatView")
        '    gvListMat.DataBind()
        'End If
        'mpeListMat.Show()
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "CheckValue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If

                    Dim objTable As DataTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    Dim dDiscAmt As Double = 0 
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "itemoid=" & dtView(C1)("itemoid")
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("branch_code") = ddlBranch.SelectedValue
                            dv(0)("itemoid") = Integer.Parse(dtView(C1)("itemoid"))
                            dv(0)("itemcode") = Tchar(dtView(C1)("itemcode").ToString)
                            dv(0)("itemdesc") = Tchar(dtView(C1)("itemdesc").ToString)
                            dv(0)("PicItem") = Tchar(dtView(C1)("PicItem").ToString)
                            dv(0)("Unit") = Tchar(dtView(C1)("Unit").ToString)
                            dv(0)("panjang") = ToMaskEdit(dtView(C1)("panjang"), 3)
                            dv(0)("lebar") = ToMaskEdit(dtView(C1)("lebar"), 3)
                            dv(0)("tinggi") = ToMaskEdit(dtView(C1)("tinggi"), 3)
                            dv(0)("beratvolume") = ToMaskEdit(dtView(C1)("beratvolume"), 3)
                            dv(0)("kubik") = ToMaskEdit(dtView(C1)("kubik"), 3)
                            dv(0)("HargaExp") = ToMaskEdit(dtView(C1)("HargaExp"), 3)
                            dv(0)("keterangan") = Tchar(dtView(C1)("keterangan"))
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("seq") = counter
                            objRow("branch_code") = ddlBranch.SelectedValue
                            objRow("itemoid") = Integer.Parse(dtView(C1)("itemoid"))
                            objRow("itemcode") = Tchar(dtView(C1)("itemcode").ToString)
                            objRow("itemdesc") = Tchar(dtView(C1)("itemdesc").ToString)
                            objRow("PicItem") = Tchar(dtView(C1)("PicItem").ToString)
                            objRow("Unit") = Tchar(dtView(C1)("Unit").ToString)
                            objRow("panjang") = ToMaskEdit(dtView(C1)("panjang"), 3)
                            objRow("lebar") = ToMaskEdit(dtView(C1)("lebar"), 3)
                            objRow("tinggi") = ToMaskEdit(dtView(C1)("tinggi"), 3)
                            objRow("beratvolume") = ToMaskEdit(dtView(C1)("beratvolume"), 3)
                            objRow("kubik") = ToMaskEdit(dtView(C1)("kubik"), 3)
                            objRow("HargaExp") = ToMaskEdit(dtView(C1)("HargaExp"), 3)
                            objRow("keterangan") = Tchar(dtView(C1)("keterangan"))
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    gvFindCrd.DataSource = objTable
                    gvFindCrd.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False) 
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some katalog data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        gvListMat.PageIndex = e.NewPageIndex
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                showMessage("Data Barang tidak ada..!!", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub
#End Region

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(10).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next
        End If
    End Sub 

    Protected Sub BtnAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddItem.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        If ToDouble(containerprice.Text) = 0.0 And ToDouble(containersize.Text) = 0.0 Then
            showMessage("- Maaf Ukuran dan price container tidak bisa nol..!!, ", 2) : Exit Sub
        End If
        BindMaterial()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub containerprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles containerprice.TextChanged
        containerprice.Text = ToMaskEdit(containerprice.Text, 3)
    End Sub

    Protected Sub containersize_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles containersize.TextChanged
        containersize.Text = ToMaskEdit(containersize.Text, 3)
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected Katalog data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub
End Class