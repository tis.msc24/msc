<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnVoucher.aspx.vb" Inherits="Accounting_trnVoucher" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server"> 
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Realisasi Voucher"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="1" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%"><ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="TabPanel1"><ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 46px" align=left><asp:Label id="Label1" runat="server" Text="Periode" __designer:wfdid="w31"></asp:Label></TD><TD style="FONT-SIZE: 9pt" align=left><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w32"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w34"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w35"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 9pt"><TD style="WIDTH: 46px" align=left><asp:Label id="Label20" runat="server" Text="Filter" __designer:wfdid="w37"></asp:Label></TD><TD vAlign=baseline align=left colSpan=1><asp:DropDownList id="DDlfilter" runat="server" CssClass="inpText" __designer:wfdid="w38"><asp:ListItem Value="realisasino">No. Realisasi</asp:ListItem>
<asp:ListItem Value="trnbelipono">No. PO</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="no" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w39"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 9pt"><TD style="WIDTH: 46px" align=left>Type</TD><TD vAlign=baseline align=left colSpan=1><asp:DropDownList id="payType" runat="server" CssClass="inpText" Font-Bold="False" __designer:wfdid="w40" OnSelectedIndexChanged="payflag_SelectedIndexChanged"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="HUTANG">PELUNASAN HUTANG</asp:ListItem>
<asp:ListItem Text="CASH" Value="CASH"></asp:ListItem>
<asp:ListItem>BANK</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
</asp:DropDownList></TD></TR><TR style="FONT-SIZE: 9pt"><TD style="WIDTH: 46px" align=left><asp:Label id="Label6" runat="server" Text="Status" __designer:wfdid="w41"></asp:Label></TD><TD vAlign=baseline align=left colSpan=1><asp:DropDownList id="statuse" runat="server" CssClass="inpText" __designer:wfdid="w42"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem Value="IN APPROVAL">IN APPROVAL</asp:ListItem>
<asp:ListItem>APPROVED</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Middle" __designer:wfdid="w43"></asp:ImageButton> <asp:ImageButton id="btnViewAll" onclick="btnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Middle" __designer:wfdid="w44"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 9pt"><TD style="WIDTH: 46px" align=left></TD><TD align=left colSpan=1>&nbsp;<asp:Label id="poststatx" runat="server" Text="NORMAL" __designer:wfdid="w45" Visible="False"></asp:Label> <asp:Label id="cabang" runat="server" __designer:wfdid="w46" Visible="False"></asp:Label><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w47" TargetControlID="txtPeriode1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w48" TargetControlID="txtPeriode2" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" __designer:wfdid="w49" TargetControlID="txtPeriode1" Mask="99/99/9999" MaskType="Date" CultureName="en-US"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" __designer:wfdid="w50" TargetControlID="txtPeriode2" Mask="99/99/9999" MaskType="Date" CultureName="en-US"></ajaxToolkit:MaskedEditExtender></TD></TR><TR style="FONT-SIZE: 9pt"><TD align=left colSpan=2><asp:GridView id="gvmstcost" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w51" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="realisasioid,cmpcode" AllowPaging="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
<asp:CheckBox id="cbPosting" runat="server" __designer:wfdid="w1" Enabled="<%# GetEnabled() %>" ToolTip="<%# GetCheckedOid() %>" Checked='<%# eval("selected") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:CommandField ShowSelectButton="True" HeaderText=" ">
<HeaderStyle CssClass="gvhdr" Font-Size="Small" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="realisasino" HeaderText="No. Realisasi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasidate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipono" HeaderText="No. PO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasiamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" Wrap="True" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasistatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Font-Size="Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasitype" HeaderText="Type" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Realisasinote" HeaderText="Catatan">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="150px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:ImageButton id="PrintHdr" onclick="PrintHdr_Click" runat="server" ImageUrl="~/Images/print.gif" __designer:wfdid="w1" ToolTip="<%# GetPrintHdr() %>"></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" VerticalAlign="Top" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Not Found Data Click Button Find Or View All" __designer:wfdid="w399" Visible="False"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" CssClass="gvrow" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE>
</ContentTemplate>
    <triggers>
<asp:PostBackTrigger ControlID="gvmstcost"></asp:PostBackTrigger>
</triggers>
</asp:UpdatePanel> 
</ContentTemplate>
<HeaderTemplate>
<STRONG><SPAN style="FONT-SIZE: 9pt"><asp:Image id="Image2" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image>
    List Of Realisasi Voucher :.</SPAN></STRONG>&nbsp; 
</HeaderTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="TabPanel2"><ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel5" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" __designer:wfdid="w75"><asp:View id="View1" runat="server" __designer:wfdid="w76"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 100px" align=left><asp:Label id="Label21" runat="server" Text="Cabang" __designer:wfdid="w77" Visible="False"></asp:Label> <asp:Label id="i_u2" runat="server" Font-Bold="True" ForeColor="Red" Text="New Data" __designer:wfdid="w78"></asp:Label></TD><TD align=left colSpan=6><asp:DropDownList id="outlet" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w79" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:Label id="Label7" runat="server" Font-Size="X-Small" __designer:wfdid="w80" Visible="False" Font-Italic="False"></asp:Label> <asp:Label id="lblIO" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="" __designer:wfdid="w81" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:Label id="lblcashbankno" runat="server" Width="82px" Text="No. Pencairan" __designer:wfdid="w82" Visible="False"></asp:Label> <asp:Label id="drafno" runat="server" Text="Draft No" __designer:wfdid="w83"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="RealisasiNo" runat="server" Width="138px" CssClass="inpTextDisabled" __designer:wfdid="w84" Enabled="False"></asp:TextBox> <asp:Label id="RealisasiOid" runat="server" __designer:wfdid="w85" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:Label id="Label12" runat="server" Text="Tanggal" __designer:wfdid="w86"></asp:Label> <asp:Label id="Label22" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w87"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="Realisasidate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w88" Enabled="False"></asp:TextBox> <asp:ImageButton id="imbCBDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w89" Visible="False" Enabled="False"></asp:ImageButton> <asp:Label id="CutofDate" runat="server" __designer:wfdid="w90" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" id="TD1" align=left runat="server" Visible="false"><asp:Label id="Label9" runat="server" Text="Type Realisasi" __designer:wfdid="w91"></asp:Label></TD><TD id="TD2" align=left colSpan=6 runat="server" Visible="false"><asp:DropDownList id="payflag" runat="server" Width="135px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w92" AutoPostBack="True" OnSelectedIndexChanged="payflag_SelectedIndexChanged"><asp:ListItem Value="HUTANG">PELUNASAN HUTANG</asp:ListItem>
<asp:ListItem Text="CASH" Value="CASH"></asp:ListItem>
<asp:ListItem>BANK</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
<asp:ListItem>0</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:Label id="LblAkun" runat="server" Text="Akun COA" __designer:wfdid="w93" Visible="False"></asp:Label></TD><TD align=left colSpan=6><asp:DropDownList id="DdlAkun" runat="server" CssClass="inpText" Font-Bold="False" __designer:wfdid="w94"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:Label id="NoPI" runat="server" Width="45px" Text="No. PO" __designer:wfdid="w95"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="NoPoTxt" runat="server" Width="150px" CssClass="inpText " __designer:wfdid="w96"></asp:TextBox> <asp:ImageButton id="btnCariPO" onclick="btnCariPO_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w97"></asp:ImageButton> <asp:ImageButton id="ErasePObtn" onclick="ErasePObtn_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w98"></asp:ImageButton> <asp:Label id="OidPO" runat="server" Text="0" __designer:wfdid="w99" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w100" Visible="False" Enabled="False"></asp:DropDownList></TD><TD align=left colSpan=6><asp:GridView id="gvPO" runat="server" Width="85%" ForeColor="#333333" __designer:wfdid="w101" OnSelectedIndexChanged="gvPO_SelectedIndexChanged" PageSize="5" OnPageIndexChanging="gvPO_PageIndexChanging" OnRowDataBound="gvPO_RowDataBound" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,NoPo,VoucherPI1,VoucherPI" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="NoPI" HeaderText="No. PI" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="NoPo" HeaderText="No. PO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="VoucherPI" HeaderText="Amt Voucher">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:Label id="Label14" runat="server" Text="Voucher PO" __designer:wfdid="w30"></asp:Label> <asp:Label id="Label19" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w103"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="realisasiamt" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w104" AutoPostBack="True" Enabled="False" MaxLength="18">0.0000</asp:TextBox>&nbsp;<asp:Label id="lblPOST" runat="server" __designer:wfdid="w105" Visible="False"></asp:Label> <asp:Label id="AmtPI" runat="server" __designer:wfdid="w106" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:Label id="Label3" runat="server" Text="Amt Realisasi" __designer:wfdid="w30"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="AmtRealHdr" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w151" AutoPostBack="True" Enabled="False" MaxLength="18">0.0000</asp:TextBox></TD></TR><TR><TD style="WIDTH: 100px" id="TD4" align=left runat="server" Visible="false"><asp:Label id="Label8" runat="server" Text="Sisa Realisasi" __designer:wfdid="w30"></asp:Label></TD><TD id="TD3" align=left colSpan=6 runat="server" Visible="false"><asp:TextBox id="selisihamount" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w152" AutoPostBack="True" Enabled="False" MaxLength="18">0.0000</asp:TextBox></TD></TR><TR><TD style="WIDTH: 100px" align=left>Note</TD><TD align=left colSpan=6><asp:TextBox id="NoteHdr" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w107" MaxLength="100"></asp:TextBox></TD></TR><TR><TD align=left colSpan=7><asp:Label id="I_Udtl" runat="server" Font-Bold="True" ForeColor="Red" Text="New Detail" __designer:wfdid="w108"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:Label id="Label16" runat="server" Width="5px" Text="Voucher" __designer:wfdid="w109"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="txtVoucher" runat="server" Width="150px" CssClass="inpText " __designer:wfdid="w110"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCariCOA" onclick="BtnCariCoa_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w111"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseVc" onclick="EraseVc_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w112"></asp:ImageButton> <asp:Label id="lblCoa" runat="server" Text="0" __designer:wfdid="w113" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=left></TD><TD align=left colSpan=6><asp:GridView id="GvVoucher" runat="server" Width="51%" ForeColor="#333333" __designer:wfdid="w114" OnSelectedIndexChanged="GvVoucher_SelectedIndexChanged" PageSize="5" OnPageIndexChanging="GvVoucher_PageIndexChanging" OnRowDataBound="GvVoucher_RowDataBound" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="voucheroid,voucherno,voucherdesc,amtvoucheridr" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="voucherno" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucherdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtvoucheridr" HeaderText="Amt. Voucher">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="130px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 100px" align=left>Amount Realisasi</TD><TD align=left colSpan=6><asp:TextBox id="AmtVdtl" runat="server" Width="150px" CssClass="inpText " __designer:wfdid="w115" AutoPostBack="True" OnTextChanged="AmtVdtl_TextChanged">0.0000</asp:TextBox>&nbsp;</TD></TR><TR><TD style="WIDTH: 100px" align=left>Amount Voucher</TD><TD align=left colSpan=6><asp:TextBox id="vDtlAmt" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w116" AutoPostBack="True" OnTextChanged="AmtVdtl_TextChanged">0.0000</asp:TextBox></TD></TR><TR><TD style="WIDTH: 100px" align=left>Amount Sisa</TD><TD align=left colSpan=6><asp:TextBox id="AmtSisaDtl" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w117" AutoPostBack="True" OnTextChanged="AmtVdtl_TextChanged">0.0000</asp:TextBox></TD></TR><TR><TD style="WIDTH: 100px" align=left>Note</TD><TD align=left colSpan=6><asp:TextBox id="cashbanknote" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w118" MaxLength="100"></asp:TextBox></TD></TR><TR><TD align=center colSpan=7><asp:Label id="labelseq" runat="server" __designer:wfdid="w119" Visible="False"></asp:Label>&nbsp;<asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" __designer:wfdid="w120"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" onclick="btnClear_Click1" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w121"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=7><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="GVvOucherDtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w50" OnSelectedIndexChanged="GVvOucherDtl_SelectedIndexChanged" OnRowDataBound="GVvOucherDtl_RowDataBound" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="seqdtl,voucheroid,voucherdesc,amtrealdtl,amtvoucherdtl,amtsisadtl,notedtl">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="seqdtl" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucheroid" HeaderText="VoucherOid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="voucherdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtvoucherdtl" HeaderText="Amt Voucher PO">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtrealdtl" HeaderText="Ralisasi Amt">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtsisadtl" HeaderText="Amt Sisa">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtltemp" HeaderText="Amt Info">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtltempsisa" HeaderText="Amt Sisa Info">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server" __designer:wfdid="w2"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" CssClass="Important" Font-Size="Small" Text="No Data Detail" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="WIDTH: 100px" id="TD8" align=left runat="server" Visible="false"><asp:Label id="Label11" runat="server" Width="121px" Font-Size="Small" Font-Bold="True" Text="- Total Realisasi" __designer:wfdid="w51"></asp:Label></TD><TD id="TD6" align=left colSpan=6 runat="server" Visible="false"><asp:Label id="lblTampung" runat="server" Font-Size="Small" Font-Bold="True" Text="0.00" __designer:wfdid="w124"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" id="TD5" align=left runat="server" Visible="false"><asp:Label id="Label2" runat="server" Width="120px" Font-Size="Small" Font-Bold="True" Text="- Total Sisa" __designer:wfdid="w53"></asp:Label></TD><TD id="TD7" align=left colSpan=6 runat="server" Visible="false"><asp:Label id="SisaAmtDtl" runat="server" Font-Size="Small" Font-Bold="True" Text="0.00" __designer:wfdid="w124"></asp:Label></TD></TR><TR><TD align=left colSpan=7>Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w127"></asp:Label>&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w128"></asp:Label><BR /><ajaxToolkit:CalendarExtender id="cecashbankdate" runat="server" __designer:wfdid="w129" TargetControlID="realisasidate" Format="dd/MM/yyyy" PopupButtonID="imbCBDate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MEERealisasidate" runat="server" __designer:wfdid="w130" TargetControlID="Realisasidate" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbRealisasiamt" runat="server" __designer:wfdid="w131" TargetControlID="realisasiamt" ValidChars="1234567890,.-"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD align=left colSpan=7><asp:ImageButton id="btnsave" onclick="btnsave_Click" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w132"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w133"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnSendAp" runat="server" ImageUrl="~/Images/sendapproval.png" __designer:wfdid="w134"></asp:ImageButton> <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" __designer:wfdid="w135" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w136"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" __designer:wfdid="w137" Visible="False"></asp:ImageButton> </TD></TR><TR><TD align=left colSpan=7><asp:UpdatePanel id="UpdatePanel6" runat="server" __designer:wfdid="w138"><ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" __designer:wfdid="w139" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting" __designer:wfdid="w140"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" __designer:wfdid="w141" CellPadding="4" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="gvakun_RowDataBound">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w142">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" __designer:wfdid="w143" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" __designer:wfdid="w144" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD></TR></TBODY></TABLE></asp:View> </asp:MultiView> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        
</ContentTemplate>
<HeaderTemplate>
                            <span style="font-size: 9pt">
                             <strong><span>
                                 <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                 Form Of Realisasi Voucher :.</span></strong></span>&nbsp;
                            
                        
</HeaderTemplate>
</ajaxToolkit:TabPanel>
</ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>