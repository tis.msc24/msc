<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="frmOpenPO.aspx.vb" Inherits="OpenPO" title="PT. MULTI SARANA COMPUTER - Open PO" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">

 
    <table id="Table3" align="center" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th class="header" colspan="5" valign="top">
                <asp:Label ID="Label81" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Purchase Order - Open"></asp:Label></th>
        </tr>
    </table>
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"  Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel10" runat="server" HeaderText="TabPanel10">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch" __designer:wfdid="w24"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w25"><asp:ListItem Value="trnbelipono">No. PO</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w26"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w27" MaskType="Date" UserDateFormat="DayMonthYear" Mask="99/99/9999" CultureName="id-ID" TargetControlID="FilterPeriod1"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w28" TargetControlID="FilterPeriod1" Format="dd/MM/yyyy" PopupButtonID="imbPeriod1"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w29" Enabled="False" Checked="True" AutoPostBack="False" Visible="False"></asp:CheckBox>Period</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w30"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton>&nbsp;<asp:Label id="Label76" runat="server" Text="to" __designer:wfdid="w32"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w33"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w35"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w36" Visible="False"></asp:CheckBox></TD><TD class="Label" align=left></TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w37" Visible="False"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="In Process">In Process</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w40" MaskType="Date" UserDateFormat="DayMonthYear" Mask="99/99/9999" CultureName="id-ID" TargetControlID="FilterPeriod2"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w41" TargetControlID="FilterPeriod2" Format="dd/MM/yyyy" PopupButtonID="imbPeriod2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=6><FIELDSET style="WIDTH: 100%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 160px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="divTblData"></DIV><DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 137px; BACKGROUND-COLOR: beige"><asp:GridView id="tbldata" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w42" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="openoid" DataNavigateUrlFormatString="frmOpenPO.aspx?oid={0}" DataTextField="openoid" HeaderText="Nomer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="10px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="10px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnbelipono" HeaderText="No. PO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="170px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="170px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipodate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="22px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="22px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="statusnya" HeaderText="Statusnya">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="20px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="20px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></DIV></FIELDSET></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="tbldata"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">
                                List of Purchase Order</span></strong> <strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel20" runat="server" HeaderText="TabPanel20">
                        <ContentTemplate>
<div style="text-align:left">
    <asp:UpdatePanel id="UpdatePanel4" runat="server">
    <ContentTemplate>
<TABLE id="Table2" cellSpacing=2 cellPadding=4 width="100%" align=center border=0><TBODY><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD style="WIDTH: 50px">No. PO <asp:Label id="Label29" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w62"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" colSpan=5><asp:TextBox id="Nopo" runat="server" Width="129px" CssClass="inpText" __designer:wfdid="w63"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" onclick="btnSearchSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w64"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" onclick="btnClearSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w65"></asp:ImageButton> <asp:Label id="iOid" runat="server" __designer:wfdid="w66" Visible="False"></asp:Label> <asp:Label id="StatusNya" runat="server" Text="In Process" __designer:wfdid="w67" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD style="WIDTH: 50px"><asp:Label id="osPO" runat="server" __designer:wfdid="w68" Visible="False"></asp:Label></TD><TD colSpan=5><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvSupplier" runat="server" Width="60%" ForeColor="#333333" __designer:wfdid="w69" Visible="False" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" AllowPaging="True" DataKeyNames="pomstoid,pono ">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="No. PO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dCabang" HeaderText="Tujuan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" Font-Size="X-Small" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Purchase Order data found!!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD style="WIDTH: 50px">Tujuan</TD><TD colSpan=5><DIV style="OVERFLOW-Y: scroll; WIDTH: 61%; HEIGHT: 143px; BACKGROUND-COLOR: beige"><asp:CheckBoxList id="cbCabang" runat="server" Width="528px" BackColor="#FFFFC0" __designer:wfdid="w70" CellPadding="5" RepeatColumns="4" RepeatDirection="Horizontal" BorderColor="Black">
</asp:CheckBoxList></DIV></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD colSpan=6>Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="1970324836974620" __designer:wfdid="w71"></asp:Label> by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="1970324836974621" __designer:wfdid="w72"></asp:Label></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD colSpan=6><asp:ImageButton id="btnsavemstr" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:dtid="1970324836974624" __designer:wfdid="w73"></asp:ImageButton> <asp:ImageButton id="btnCancelMstr" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" __designer:dtid="1970324836974625" __designer:wfdid="w74"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:dtid="1970324836974624" __designer:wfdid="w75"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnDel" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:dtid="1970324836974624" __designer:wfdid="w76"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="1407374883553325" __designer:wfdid="w77" AssociatedUpdatePanelID="UpdatePanel4"><ProgressTemplate __designer:dtid="1407374883553326">
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w78"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
    </asp:UpdatePanel>
                                        </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">
                            Form Purchase Order :.&nbsp;</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel id="upMsgbox" runat="server">
                    <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" DropShadow="True" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" TargetControlID="beMsgBox">
                        </ajaxToolkit:ModalPopupExtender><asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>
