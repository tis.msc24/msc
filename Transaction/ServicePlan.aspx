<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ServicePlan.aspx.vb" Inherits="Transaction_ServicePlan" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
                <table width="100%">
                    <tr>
                        <td align="left" char="header" colspan="3" style="background-color: silver">
                            <asp:Label ID="Label74" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="Navy" Text=".: Rencana Servis"></asp:Label></td>
                    </tr>
                </table>
    <table width="950">
        <tr>
            <td align="left" colspan="3">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <strong>
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                List Rencana Servis</strong> <strong>:.</strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 90px"><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w51"></asp:Label></TD><TD style="WIDTH: 6px"><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w52"></asp:Label></TD><TD colSpan=3><asp:DropDownList id="ddlFilter" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w75"><asp:ListItem Value="QLR.BARCODE">Barcode</asp:ListItem>
<asp:ListItem Value="QLRC.custname">Nama Customer</asp:ListItem>
<asp:ListItem Value="QLR.REQITEMNAME">Nama Barang</asp:ListItem>
<asp:ListItem Value="QLA.SFLAG">Flag</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="155px" CssClass="inpText" __designer:wfdid="w76"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w77"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w78"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 90px"><asp:CheckBox id="cbPeriod" runat="server" Text="Periode" __designer:wfdid="w57"></asp:CheckBox></TD><TD style="WIDTH: 6px"><asp:Label id="Label3" runat="server" Text=":" __designer:wfdid="w58"></asp:Label></TD><TD colSpan=3><asp:TextBox id="txtPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w79"></asp:TextBox> <asp:ImageButton id="btnCal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w80"></asp:ImageButton>&nbsp; <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w81"></asp:Label>&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w82"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w83"></asp:ImageButton>&nbsp;<asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w84"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w65"></asp:CheckBox></TD><TD style="WIDTH: 6px"><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w66"></asp:Label></TD><TD colSpan=3><asp:DropDownList id="ddlStatus" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w85"><asp:ListItem Value="all">All</asp:ListItem>
<asp:ListItem>Check</asp:ListItem>
<asp:ListItem>CheckOut</asp:ListItem>
<asp:ListItem>Ready</asp:ListItem>
<asp:ListItem>Send</asp:ListItem>
<asp:ListItem>Close</asp:ListItem>
<asp:ListItem>Start</asp:ListItem>
<asp:ListItem>Finish</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 6px" colSpan=5><asp:GridView id="GVListServicePlan" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w68" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowPaging="True" EmptyDataText="Data Not Found">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="White" BorderColor="White" ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="SOID" DataNavigateUrlFormatString="~\transaction\ServicePlan.aspx?idPage={0}" DataTextField="BARCODE" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="SSTARTTIME" HeaderText="Target Selesai">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMNAME" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SFLAG" HeaderText="Flag" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SSTATUS" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 193px; HEIGHT: 29px"><ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" __designer:wfdid="w69" TargetControlID="txtPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD><TD style="WIDTH: 305px; HEIGHT: 29px"><ajaxToolkit:CalendarExtender id="CECal1" runat="server" __designer:wfdid="w70" TargetControlID="txtPeriod1" PopupButtonID="btnCal1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="WIDTH: 193px; HEIGHT: 24px"><ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" __designer:wfdid="w71" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD><TD style="WIDTH: 305px; HEIGHT: 24px"><ajaxToolkit:CalendarExtender id="CECal2" runat="server" __designer:wfdid="w72" TargetControlID="txtPeriod2" PopupButtonID="btnCal2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="WIDTH: 193px; HEIGHT: 5px"><asp:SqlDataSource id="SDSGVList" runat="server" __designer:wfdid="w73" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT DISTINCT QLA.SOID, QLR.REQOID, QJA.PARTDESCSHORT SPARTM, QLR.REQBARCODE, QLR.REQCODE, QLJ.ITSERVDESC, QLZ.GENDESC ITSERVTYPE, QLAA.SPARTNOTE, QLAA.SPARTQTY, QLAA.SPARTPRICE FROM QL_TRNREQUEST QLR INNER JOIN QL_TRNMECHCHECK QLG ON QLG.MSTREQOID = QLR.REQOID INNER JOIN QL_MSTITEMSERV QLJ ON QLG.ITSERVOID = QLJ.ITSERVOID INNER JOIN QL_MSTGEN QLZ ON QLZ.GENOID = QLJ.ITSERVTYPEOID INNER JOIN QL_TRNSERVICES QLA ON QLA.SOID = QLG.ITSERVOID  INNER JOIN QL_TRNSERVICESPART QLAA ON QLAA.SOID = QLA.SOID INNER JOIN QL_MSTSPAREPART QJA ON QJA.PARTOID = QLAA.SPARTMOID WHERE (QLA.CMPCODE = @CMPCODE) AND (QLA.SOID = @SOID)&#13;&#10;"><SelectParameters>
<asp:Parameter Name="CMPCODE"></asp:Parameter>
<asp:Parameter Name="SOID"></asp:Parameter>
</SelectParameters>
</asp:SqlDataSource></TD><TD style="WIDTH: 305px; HEIGHT: 5px"></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <strong>
                                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />Form Rencana Servis</strong><strong> :.</strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><TABLE width=950><TBODY><TR><TD colSpan=6><asp:Label id="lblInformasi" runat="server" Font-Size="Small" Font-Bold="True" Text="Informasi"></asp:Label> <asp:Label id="Label9" runat="server" Font-Size="Small" Text="|"></asp:Label> <asp:LinkButton id="lbDetSparepart" runat="server" CssClass="submenu" Font-Size="Small">Detail Rencana Servis</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 155px">Cabang</TD><TD style="WIDTH: 9px">:</TD><TD style="WIDTH: 312px"><asp:DropDownList id="DdlCabang" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w5" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 179px"></TD><TD style="WIDTH: 8px"></TD><TD style="WIDTH: 345px"></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label21" runat="server" Text="Barcode" __designer:wfdid="w62"></asp:Label> <asp:Label id="Label31" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 9px">:</TD><TD style="WIDTH: 312px"><asp:TextBox id="lblno" runat="server" Width="155px" CssClass="inpText" __designer:wfdid="w76" AutoPostBack="True"></asp:TextBox></TD><TD style="WIDTH: 179px">Teknisi<asp:Label id="Label72" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w4"></asp:Label></TD><TD style="WIDTH: 8px">:</TD><TD style="WIDTH: 345px"><asp:TextBox id="teknisi" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w71" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSrcTeknisi" onclick="btnSrcTeknisi_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w12"></asp:ImageButton> <asp:ImageButton id="imberaseteknisi" onclick="imberaseteknisi_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton> <asp:Label id="teknisioid" runat="server" __designer:wfdid="w14" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label13" runat="server" Text="No. Tanda Terima"></asp:Label> <asp:Label id="Label70" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w2"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label23" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 312px"><asp:TextBox id="txtNo" runat="server" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True"></asp:TextBox>&nbsp; <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnErase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:TextBox id="txti_u" runat="server" Width="56px" CssClass="inpText" ForeColor="Red" Visible="False">New</asp:TextBox></TD><TD style="WIDTH: 179px"><asp:Label id="Label32" runat="server" Text="Status"></asp:Label></TD><TD style="WIDTH: 8px"><asp:Label id="Label33" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 345px"><asp:TextBox id="txtStatus" runat="server" Width="152px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label14" runat="server" Text="Target Selesai"></asp:Label> <asp:Label id="Label71" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w3"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label24" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 312px"><asp:TextBox id="txtTglTarget" runat="server" CssClass="inpText"></asp:TextBox>&nbsp; <asp:ImageButton id="btnCalendar" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label22" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)"></asp:Label>&nbsp; </TD><TD style="WIDTH: 179px"><asp:Label id="Label15" runat="server" Text="Waktu Target" __designer:wfdid="w64"></asp:Label> <asp:Label id="Label73" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w5"></asp:Label></TD><TD style="WIDTH: 8px"><asp:Label id="Label25" runat="server" Text=":" __designer:wfdid="w67"></asp:Label></TD><TD style="WIDTH: 345px"><asp:TextBox id="txtWaktuTarget" runat="server" CssClass="inpText" __designer:wfdid="w65"></asp:TextBox>&nbsp;<asp:Label id="Label63" runat="server" ForeColor="Red" Text="HH:mm:ss" __designer:wfdid="w66"></asp:Label></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label19" runat="server" Text="Nama Barang" __designer:wfdid="w68"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label29" runat="server" Text=":" __designer:wfdid="w70"></asp:Label></TD><TD><asp:TextBox id="txtNamaBrg" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w69" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 179px"><asp:Label id="Label40" runat="server" Width="40px" Text="Flag" __designer:wfdid="w34" Visible="False"></asp:Label></TD><TD><asp:Label id="Label41" runat="server" Text=":" __designer:wfdid="w35" Visible="False"></asp:Label></TD><TD><asp:RadioButtonList id="rbFlag" runat="server" Width="112px" __designer:wfdid="w36" Visible="False" AutoPostBack="True" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbFlag_SelectedIndexChanged"><asp:ListItem Value="In">IN</asp:ListItem>
<asp:ListItem Value="Out">OUT</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label16" runat="server" Text="Nama Customer"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label26" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 312px"><asp:TextBox id="txtNamaCust" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 179px"><asp:Label id="Label66" runat="server" Width="136px" Text="Harga Pengerjaan Total" __designer:wfdid="w31"></asp:Label></TD><TD><asp:Label id="Label68" runat="server" Text=":" __designer:wfdid="w32"></asp:Label></TD><TD><asp:TextBox id="txtHargaJob" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w33" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label11" runat="server" Text="Alamat Customer"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label35" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 312px"><asp:TextBox id="txtAlamatCust" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 179px"><asp:Label id="Label67" runat="server" Width="128px" Text="Harga Sparepart Total" __designer:wfdid="w37"></asp:Label></TD><TD style="WIDTH: 8px"><asp:Label id="Label39" runat="server" Text=":" __designer:wfdid="w38"></asp:Label></TD><TD><asp:TextBox id="txtHargaSPart" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w39" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label36" runat="server" Text="HP Customer"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label37" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 312px"><asp:TextBox id="txtHPCust" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 179px"><asp:Label id="Label38" runat="server" Width="88px" Text="Harga Servis" __designer:wfdid="w40"></asp:Label></TD><TD style="WIDTH: 8px"><asp:Label id="Label69" runat="server" Text=":" __designer:wfdid="w41"></asp:Label></TD><TD><asp:TextBox id="txtHargaServis" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w42" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label17" runat="server" Text="Jenis Barang"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label27" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 312px"><asp:TextBox id="txtJenisBrg" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 179px"></TD><TD style="WIDTH: 8px"></TD><TD style="WIDTH: 4px"></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label18" runat="server" Text="Merk Barang"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label28" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 312px"><asp:TextBox id="txtMerk" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 179px"></TD><TD style="WIDTH: 8px"></TD><TD></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="lblTarget" runat="server" __designer:wfdid="w1" Visible="False"></asp:Label> </TD><TD style="WIDTH: 9px"></TD><TD style="WIDTH: 312px"></TD><TD style="WIDTH: 179px"><ajaxToolkit:FilteredTextBoxExtender id="ftbHargaJob" runat="server" __designer:wfdid="w13" FilterType="Numbers" ValidChars="1234567890," TargetControlID="txtHargaJob"></ajaxToolkit:FilteredTextBoxExtender></TD><TD style="WIDTH: 8px"></TD><TD style="WIDTH: 345px"><ajaxToolkit:FilteredTextBoxExtender id="ftbHargaSPart" runat="server" __designer:wfdid="w14" FilterType="Numbers" ValidChars="1234567890," TargetControlID="txtHargaSPart"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label20" runat="server" Text="Kategori Kerusakan" Visible="False"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label30" runat="server" Text=":" Visible="False"></asp:Label></TD><TD style="WIDTH: 312px"><asp:TextBox id="txtKatKerusakan" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 179px"><ajaxToolkit:FilteredTextBoxExtender id="ftbHargaServis" runat="server" __designer:wfdid="w15" FilterType="Numbers" ValidChars="1234567890," TargetControlID="txtHargaServis"></ajaxToolkit:FilteredTextBoxExtender></TD><TD style="WIDTH: 8px"></TD><TD style="WIDTH: 345px"><ajaxToolkit:CalendarExtender id="CEtglTarget" runat="server" __designer:wfdid="w9" TargetControlID="txtTglTarget" Format="dd/MM/yyyy" PopupButtonID="btnCalendar"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="WIDTH: 155px" align=left><asp:TextBox id="txtOid" runat="server" Width="18px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp;<asp:TextBox id="txtPlanOid" runat="server" Width="18px" CssClass="inpText" Visible="False"></asp:TextBox></TD><TD style="WIDTH: 9px"></TD><TD style="WIDTH: 312px"><asp:TextBox id="TglTgl" runat="server" CssClass="inpText" __designer:wfdid="w1" Visible="False"></asp:TextBox></TD><TD style="WIDTH: 179px"><ajaxToolkit:MaskedEditExtender id="MEPJam" runat="server" __designer:wfdid="w6" TargetControlID="txtWaktuTarget" MaskType="Time" Mask="99:99:99" UserTimeFormat="TwentyFourHour"></ajaxToolkit:MaskedEditExtender></TD><TD style="WIDTH: 8px"></TD><TD style="WIDTH: 345px"><ajaxToolkit:MaskedEditExtender id="MEPtglTarget" runat="server" __designer:wfdid="w8" TargetControlID="txtTglTarget" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:View><asp:View id="View2" runat="server"><TABLE width=950><TBODY><TR><TD colSpan=3><asp:LinkButton id="lbInformasi2" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w1">Informasi</asp:LinkButton> <asp:Label id="Label43" runat="server" Font-Size="Small" Text="|"></asp:Label> <asp:Label id="Label46" runat="server" Font-Size="Small" Font-Bold="True" Text="Detail Rencana Servis"></asp:Label></TD></TR><TR><TD style="WIDTH: 96px"><asp:Label id="Label47" runat="server" Text="Pekerjaan"></asp:Label></TD><TD><asp:Label id="Label53" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtJob" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True"></asp:TextBox>&nbsp; <asp:ImageButton id="btnSearch3" onclick="btnSearch3_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnErase3" onclick="btnErase3_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="lbStatus" runat="server" Width="72px" ForeColor="Red" Visible="False">New</asp:Label>&nbsp;&nbsp;<asp:Label id="itservoid" runat="server" Visible="False">0</asp:Label> <asp:Label id="lbDtlOid" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="lbStatus2" runat="server" Width="72px" ForeColor="Red" __designer:wfdid="w5" Visible="False">New</asp:Label>&nbsp;<asp:Label id="lbDtlOid2" runat="server" __designer:wfdid="w1" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 96px; HEIGHT: 20px"><asp:Label id="Label48" runat="server" Text="Tipe Pengerjaan"></asp:Label></TD><TD style="WIDTH: 10px; HEIGHT: 20px"><asp:Label id="Label54" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtType" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True"></asp:TextBox> <asp:Label id="xxx" runat="server" __designer:wfdid="w6" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 96px; HEIGHT: 20px"><asp:Label id="Label64" runat="server" Text="Tarif Pengerjaan" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 10px; HEIGHT: 20px"><asp:Label id="Label65" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD><asp:TextBox id="itservtarif" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w3" MaxLength="14"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTEitservtarif" runat="server" __designer:wfdid="w4" ValidChars="1234567890," TargetControlID="itservtarif"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="WIDTH: 96px"><asp:Label id="Label49" runat="server" Text="Keterangan"></asp:Label></TD><TD style="WIDTH: 10px"><asp:Label id="Label55" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtNote" runat="server" Width="152px" Height="40px" CssClass="inpText" MaxLength="50" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label62" runat="server" ForeColor="Red" Text="Maks. 70 Karakter"></asp:Label> <asp:Label id="mpartoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=3><asp:ImageButton id="btnAddToListSPart" onclick="btnAddToListSPart_Click" runat="server" ImageUrl="~/Images/addtolist.png" __designer:wfdid="w11" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w12" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCcl" onclick="btnCcl_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w13" Visible="False" Enabled="False"></asp:ImageButton></TD></TR><TR><TD colSpan=3><asp:GridView id="GVDtlServicePlan" runat="server" Width="100%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w10" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="SEQ,ITSERVOID">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="SEQ" HeaderText="SEQ" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVOID" HeaderText="ITSERVOID" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVDESC" HeaderText="Pekerjaan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVTYPE" HeaderText="Tipe Pengerjaan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVTARIF" HeaderText="Tarif Pengerjaan">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SPARTNOTE" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="Medium" ForeColor="Red" Width="5%"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label12" runat="server" ForeColor="Red" Text="No detail on List" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 96px; HEIGHT: 23px">Gudang</TD><TD style="WIDTH: 10px; HEIGHT: 23px">:</TD><TD><asp:DropDownList id="matLoc" runat="server" Height="20px" CssClass="inpText" __designer:dtid="2814749767106643" __designer:wfdid="w2" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 96px; HEIGHT: 23px"><asp:Label id="Label50" runat="server" Text="Sparepart"></asp:Label></TD><TD style="WIDTH: 10px; HEIGHT: 23px"><asp:Label id="Label56" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtNamaSpart" runat="server" Width="250px" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True"></asp:TextBox>&nbsp; <asp:ImageButton id="btnSearch2" onclick="btnSearch2_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;&nbsp;<asp:ImageButton id="btnErase2" onclick="btnErase2_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="spartmoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 96px; HEIGHT: 23px">Merk</TD><TD style="WIDTH: 10px; HEIGHT: 23px">:</TD><TD><asp:TextBox id="merkspr" runat="server" Width="149px" CssClass="inpTextDisabled" __designer:wfdid="w4" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 96px"><asp:Label id="Label42" runat="server" Text="Sparepart Qty"></asp:Label></TD><TD style="WIDTH: 10px"><asp:Label id="Label44" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtQty" runat="server" Width="150px" CssClass="inpText" MaxLength="5"></asp:TextBox> <asp:Label id="stock" runat="server" Text="stock" __designer:wfdid="w1" Visible="False"></asp:Label> <asp:Label id="ss" runat="server" Text=":" __designer:wfdid="w1" Visible="False"></asp:Label> <asp:Label id="saldoakhir" runat="server" __designer:wfdid="w1" Visible="False"></asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="FTEtxtQty" runat="server" __designer:wfdid="w5" ValidChars="1234567890," TargetControlID="txtQty"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="WIDTH: 96px"><asp:Label id="Label52" runat="server" Text="Harga Sparepart"></asp:Label></TD><TD style="WIDTH: 10px"><asp:Label id="Label58" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtHarga" runat="server" Width="150px" CssClass="inpText" MaxLength="14"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FTEtxtHarga" runat="server" __designer:wfdid="w6" ValidChars="1234567890," TargetControlID="txtHarga"></ajaxToolkit:FilteredTextBoxExtender> <asp:Label id="lbIDseq" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="lbIDseq2" runat="server" __designer:wfdid="w3" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=950><TBODY><TR><TD colSpan=3><asp:ImageButton id="btnAddToListSPart2" onclick="btnAddToListSPart2_Click" runat="server" ImageUrl="~/Images/addtolist.png" __designer:wfdid="w14" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClear2" onclick="btnClear2_Click" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w15" Visible="False"></asp:ImageButton></TD></TR><TR><TD colSpan=3><asp:GridView id="GVDtlServicePlan2" runat="server" Width="100%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w4" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="SPARTSEQ,MPARTOID,spartmoid,mtrloc">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="SPARTSEQ" HeaderText="SPARTSEQ" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="MPARTOID" HeaderText="MPARTOID" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="SPARTMOID" HeaderText="SPARTMOID" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVOID" HeaderText="ITSERVOID" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVDESC" HeaderText="Pekerjaan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="120px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVTYPE" HeaderText="Tipe Pengerjaan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="130px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SPARTM" HeaderText="Sparepart">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SPARTQTY" HeaderText="Sparepart Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SPARTPRICE" HeaderText="Sparepart Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="mtrloc" Visible="False"></asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="saldoakhir" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label12" runat="server" ForeColor="Red" Text="No detail on List" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE><TABLE width=950><TBODY><TR><TD colSpan=3></TD></TR></TBODY></TABLE></asp:View></asp:MultiView><TABLE width="100%"><TBODY><TR><TD colSpan=4>Created&nbsp;On <asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w1"></asp:Label>&nbsp;By <asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w2"></asp:Label>&nbsp;</TD></TR><TR><TD colSpan=4><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:wfdid="w1" Visible="False"></asp:ImageButton>&nbsp;<asp:Button id="btnBatal" onclick="btnBatal_Click" runat="server" CssClass="red" Font-Bold="True" ForeColor="White" Text="Batal" Visible="False"></asp:Button>&nbsp;<asp:Button id="btnReady" onclick="btnReady_Click" runat="server" CssClass="orange" Font-Bold="True" ForeColor="White" Text="Ready" Visible="False"></asp:Button> <asp:Button id="btncheckout" onclick="btncheckout_Click" runat="server" Width="78px" CssClass="green" Font-Bold="True" ForeColor="White" Text="Check Out" __designer:wfdid="w1" Visible="False"></asp:Button> <asp:Button id="btnfinish" onclick="btnfinish_Click" runat="server" CssClass="orange" Font-Bold="True" ForeColor="White" Text="Finish" __designer:wfdid="w2" Visible="False"></asp:Button> <asp:ImageButton id="btnpaid" onclick="btnpaid_Click" runat="server" ImageUrl="~/Images/paid.png" ImageAlign="AbsMiddle" __designer:wfdid="w1" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                            <asp:UpdatePanel id="UpdatePanel4" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelInfo" runat="server" CssClass="modalBox" Visible="False" DefaultButton="btnFindInfo"><TABLE width=600><TBODY><TR><TD align=center colSpan=6><asp:Label id="lblInfo" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Penerimaan Barang" __designer:wfdid="w21"></asp:Label></TD></TR><TR><TD style="WIDTH: 95px"><asp:Label id="Label8" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 2px"><asp:Label id="Label10" runat="server" Text=":" Visible="False"></asp:Label></TD><TD colSpan=4><asp:DropDownList id="ddlFilterInfo" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w8"><asp:ListItem Value="QLR.REQCODE">No Tanda Terima</asp:ListItem>
<asp:ListItem Value="QLR.BARCODE">Barcode</asp:ListItem>
<asp:ListItem Value="QOP.CUSTNAME">Nama Customer</asp:ListItem>
<asp:ListItem Value="QLR.REQITEMNAME">Nama Barang</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterInfo" runat="server" Width="145px" CssClass="inpText" __designer:wfdid="w9"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindInfo" onclick="btnFindInfo_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllInfo" onclick="btnViewAllInfo_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w11"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 95px; HEIGHT: 15px"><asp:CheckBox id="cbPeriodInfo" runat="server" Text="Period" Visible="False"></asp:CheckBox></TD><TD style="WIDTH: 2px; HEIGHT: 15px"><asp:Label id="Label12" runat="server" Text=":" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" colSpan=4><asp:TextBox id="txtPeriod1Info" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w16" Visible="False"></asp:TextBox> <asp:ImageButton id="btnCal1Info" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w17" Visible="False"></asp:ImageButton> <asp:Label id="Label34" runat="server" Text="to" __designer:wfdid="w18" Visible="False"></asp:Label>&nbsp;<asp:TextBox id="txtPeriod2Info" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w19" Visible="False"></asp:TextBox> <asp:ImageButton id="btnCal2Info" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w20" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:GridView id="GVListInfo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w23" DataKeyNames="REQCODE" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="5" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="REQCODE" HeaderText="No. Tanda Terima">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="BARCODE" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMNAME" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQSTATUS" HeaderText="Status" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CHKFLAG" HeaderText="Flag" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
&nbsp;<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No Data Found !" __designer:wfdid="w146"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="CETglInfo1" runat="server" __designer:wfdid="w12" TargetControlID="txtPeriod1Info" PopupButtonID="btnCal1Info" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEPTglInfo1" runat="server" __designer:wfdid="w13" TargetControlID="txtPeriod1Info" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CETglInfo2" runat="server" __designer:wfdid="w14" TargetControlID="txtPeriod2Info" PopupButtonID="btnCal2Info" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEPTglInfo2" runat="server" __designer:wfdid="w15" TargetControlID="txtPeriod2Info" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=center colSpan=6><asp:LinkButton id="lbClose" runat="server">[Close]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenInfo" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="MPEInfo" runat="server" TargetControlID="btnHiddenInfo" PopupDragHandleControlID="lblInfo" PopupControlID="PanelInfo" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <br />
                            &nbsp;<asp:UpdatePanel id="UpdatePanel5" runat="server"><contenttemplate>
<asp:Panel id="PanelInfo2" runat="server" CssClass="modalBox" Visible="False" DefaultButton="btnFind2"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=6><asp:Label id="Label45" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Sparepart"></asp:Label></TD></TR><TR><TD style="WIDTH: 95px"><asp:Label id="Label57" runat="server" Text="Filter"></asp:Label></TD><TD style="WHITE-SPACE: nowrap" noWrap colSpan=5><asp:DropDownList id="ddlFilterInfo2" runat="server" CssClass="inpText" __designer:wfdid="w10"><asp:ListItem Value="a.itemcode">Code</asp:ListItem>
<asp:ListItem Value="a.itemdesc">Description</asp:ListItem>
<asp:ListItem Enabled="False" Value="merk">Sparepart Merk</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="txtFilterInfo2" runat="server" Width="224px" CssClass="inpText" __designer:wfdid="w11"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind2" onclick="btnFind2_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w12"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll2" onclick="btnViewAll2_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton>&nbsp;</TD></TR><TR><TD align=center colSpan=6><asp:GridView id="GVListInfo2" runat="server" Width="599px" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="PARTOID,mtrloc,saldoakhir" AllowPaging="True" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="partcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="partdescshort" HeaderText="Description">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Stock">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=6><asp:LinkButton id="lbClose2" runat="server">[Close]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenInfo2" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="MPEInfo2" runat="server" TargetControlID="btnHiddenInfo2" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelInfo2"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate></asp:UpdatePanel>
                            &nbsp;<br />
                            <asp:UpdatePanel id="UpdatePanel6" runat="server"><contenttemplate>
<asp:Panel id="PanelInfo3" runat="server" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=6><asp:Label id="Label51" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Job and  Type"></asp:Label></TD></TR><TR><TD style="WIDTH: 95px"><asp:Label id="Label60" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 2px"><asp:Label id="Label61" runat="server" Text=":"></asp:Label></TD><TD colSpan=4><asp:DropDownList id="ddlFilterInfo3" runat="server" CssClass="inpText" __designer:wfdid="w4"><asp:ListItem Value="A.ITSERVDESC">Job Desc</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterInfo3" runat="server" CssClass="inpText" __designer:wfdid="w5"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind3" onclick="btnFind3_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll3" onclick="btnViewAll3_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:GridView id="GVListInfo3" runat="server" Width="599px" ForeColor="#333333" __designer:wfdid="w3" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ITSERVOID" AllowPaging="True" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="ITSERVOID" HeaderText="ITSERVOID" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itservdesc" HeaderText="Job Desc">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=6><asp:LinkButton id="lbClose3" runat="server">[Close]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenInfo3" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="MPEInfo3" runat="server" TargetControlID="btnHiddenInfo3" Drag="True" PopupControlID="PanelInfo3"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <br />
                            <asp:UpdatePanel id="UpdatePanel7" runat="server">
                                <contenttemplate>
<asp:Panel id="panelteknisi" runat="server" Width="477px" CssClass="modalBox" Visible="False" DefaultButton="btnFindTeknisi"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 658px" align=center><asp:Label id="lblCaptteknisi" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Teknisi"></asp:Label></TD></TR><TR><TD style="WIDTH: 658px" align=left>Filter : <asp:DropDownList id="DDLFilterteknisi" runat="server" CssClass="inpText"><asp:ListItem Value="PERSONNIP">NIP</asp:ListItem>
<asp:ListItem Value="PERSONNAME">Nama</asp:ListItem>
<asp:ListItem Value="PERSONCRTADDRESS">Alamat</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterTeknisi" runat="server" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindTeknisi" onclick="imbFindTeknisi_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbViewTeknisi" onclick="imbViewTeknisi_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 658px"><DIV><asp:GridView id="gvListTeknisi" runat="server" Width="100%" ForeColor="#333333" OnSelectedIndexChanged="gvListTeknisi_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="personoid" AllowPaging="True" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" BorderStyle="None"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="PERSONNIP" HeaderText="NIP">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PERSONNAME" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PERSONCRTADDRESS" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pengerjaan" HeaderText="Pengerjaan">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
No Data Teknisi
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp;</DIV></TD></TR><TR><TD style="WIDTH: 658px" align=center><asp:LinkButton id="lkbCloseTeknisi" onclick="lkbCloseTeknisi_Click" runat="server">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 658px"></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeteknisi" runat="server" TargetControlID="btnHideteknisi" BackgroundCssClass="modalBackground" PopupControlID="panelteknisi" PopupDragHandleControlID="lblCaptteknisi"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideteknisi" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
                            </asp:UpdatePanel>
                            <br />
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width=500><TBODY><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="WARNING" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="WIDTH: 29px"><asp:ImageButton id="ImageButton3" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD style="WIDTH: 800px"><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD style="WIDTH: 3px"></TD></TR><TR><TD></TD><TD style="WIDTH: 800px"><asp:Label id="lblState" runat="server"></asp:Label></TD><TD style="WIDTH: 3px; HEIGHT: 15px"></TD></TR><TR><TD style="WIDTH: 29px"></TD><TD style="WIDTH: 800px" align=center><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD><TD style="WIDTH: 3px"></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="WARNING"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

