Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnPrePaid
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Private ws As DataTable
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
#End Region

#Region "Function"
    Private Function SetTableDetail() As DataTable
        Dim dtlTable As DataTable = New DataTable("TblDtl")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("prepaiddtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("prepaidmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("prepaiddtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("prepaiddtldate", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("prepaiddtlaccumamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prepaiddtlamount", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prepaiddtlaccum", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prepaiddtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("prepaiddtlstatus", Type.GetType("System.String"))
        dtlTable.Columns.Add("crtuser", Type.GetType("System.String"))
        dtlTable.Columns.Add("crttime", Type.GetType("System.String"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Return dtlTable
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(PrePaidDate.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Pre-Paid Date 1 is invalid. ", 2)
            Return False
        End If
        If Not IsValidDate(txtPeriode1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 is invalid. ", 2)
            Return False
        End If
        If Not IsValidDate(txtPeriode2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 is invalid. ", 2)
            Return False
        End If
        If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
            showMessage("Period 1 Can't > Period 2", 2)
            Return False
        End If
        Return True
    End Function

#End Region

#Region "Prosedure"
    Private Sub GenerateCodePrepaid()
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & DDLCabang.SelectedValue & "' and gengroup='CABANG'")
        Dim sNo As String = "PPD/" & cabang & "/" & Format(GetServerTime, "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(prepaidmstcode,'" & sNo & "',''))),0)+1  FROM QL_trnprepaidmst WHERE prepaidmstcode LIKE '" & sNo & "%' and branch_code='" & DDLCabang.SelectedValue & "'"

        prepaidmstcode.Text = GenNumberString(sNo, "", GetScalar(sSql), 4)
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal iType As String)
        Dim iCapt As String = ""
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : iCapt = CompnyName & " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : iCapt = CompnyName & " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : iCapt = CompnyName & " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = iCapt : Validasi.Text = message
        panelMsg.Visible = True : btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub CabangNyaDdl()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                CabangNya.Items.Add(New ListItem("ALL", "ALL"))
                CabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
            CabangNya.Items.Add(New ListItem("ALL", "ALL"))
            CabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLCabang, sSql) 
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLCabang, sSql) 
            Else
                FillDDL(DDLCabang, sSql) 
                DDLCabang.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLCabang, sSql)
            DDLCabang.SelectedValue = "10"
        End If
    End Sub

    Private Sub ReAmountAccum()
        If PrePaidAmt.Text.Trim = "" Then
            PrePaidAmt.Text = 0
        End If
        If PrepaidAccum.Text.Trim = "" Then
            PrepaidAccum.Text = 0
        End If
        If ToDouble(PrePaidAmt.Text) >= ToDouble(PrepaidAccum.Text) Then
            PrepaidmstValue.Text = ToMaskEdit(ToDouble(PrePaidAmt.Text) - ToDouble(PrepaidAccum.Text), 3)
        Else
            PrepaidmstValue.Text = "0.00"
            showMessage("Nilai buku tidak Boleh lebih besar dari Nilai Prepaid Amount !!", 2)
        End If
    End Sub

    Private Sub PrePaidReAmount()
        If fixPresentValue.Text <> "" And PrepaidLenght.Text <> "" Then
            If ToDouble(fixPresentValue.Text <= 0) Then
                showMessage("Nilai buku harus lebih besar dari 0 !!", 2)
                PrepaidLenght.Text = "0" : PrepaidmstValue.Text = "" : Exit Sub
            End If
            If PrepaidLenght.Text.Trim = "" Then
                PrepaidLenght.Text = 1
            End If
            If (ToDouble(PrepaidLenght.Text) < -1) Or (ToDouble(PrepaidLenght.Text) = 0) Then
                showMessage("Penyusutan yang di ijinkan (Bulan) adalah -1 / lebih besar dari 0 !!", 2)
                PrepaidLenght.Text = "0" : PrepaidmstValue.Text = "" : Exit Sub
            End If

            If ToDouble(PrepaidAccum.Text) > 0 Then
                accumAmt.Text = ToMaskEdit(ToDouble(PrePaidAmt.Text) - ToDouble(PrepaidAccum.Text), 3)
            End If
            'If todouble(fixdepmonth.Text) = -1 Then
            '    fixdepval.Text = "0.00"
            'ElseIf todouble(fixdepmonth.Text) > 0 Then
            PrepaidmstValue.Text = ToMaskEdit((ToDouble(PrepaidAccum.Text)) / Val(ToDouble(hasilbagi.Text)), 3)
            PrePaiddtlAccum.Text = ToMaskEdit((ToDouble(PrepaidAccum.Text)) / Val(ToDouble(hasilbagi.Text)), 3)

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                Session("tbldtl") = Nothing
                GVprePaiddtl.DataSource = Nothing
                GVprePaiddtl.DataBind()
            Else
                Session("tbldtl") = Nothing
                GVprePaiddtl.DataSource = Nothing
                GVprePaiddtl.DataBind()
            End If
        End If
    End Sub

    Public Sub binddata()

        sSql = "SELECT mst.cmpcode,mst.branch_code,ol.gendesc outletname,mst.prepaidmstoid,mst.prepaidmstcode,mst.prepaidmstdesc,mst.prepaidmstdate, mst.prepaidmststartdate,mst.prepaidmsttotalamount,mst.prepaidmstamount,mst.prepaidmstvalue,mst.prepaidmstperiode,mst.prepaidmstlength,mst.prepaidmstaccumamount, mst.prepaidmstnote,mst.prepaidmstacctgoid,acc.acctgdesc, mst.prepaidmstpayacctgoid,act.acctgdesc, mst.prepaidmststatus, mst.prepaidmstnote,mst.prepaidmstfinishdate FROM QL_trnprepaidmst mst INNER JOIN QL_mstgen ol ON ol.gencode=mst.branch_code AND ol.gengroup='CABANG' INNER JOIN QL_mstacctg acc ON acc.acctgoid=mst.prepaidmstacctgoid INNER JOIN QL_mstacctg act ON act.acctgoid=mst.prepaidmstpayacctgoid WHERE mst.prepaidmstoid > 0"

        If cbPeriode.Checked Then
            sSql &= " AND mst.prepaidmststartdate BETWEEN '" & CDate(toDate(txtPeriode1.Text)) & "' AND '" & CDate(toDate(txtPeriode2.Text)) & "' "
            If cbDesc.Checked Then
                sSql &= "AND prepaidmstdesc LIKE '%" & Tchar(txtFilter.Text) & "%' "
                If cbBlmPosting.Checked Then
                    sSql &= "AND prepaidmststatus <> 'POST' "
                End If
            ElseIf cbBlmPosting.Checked Then
                sSql &= "AND prepaidmststatus<>'POST' "
            End If
        ElseIf cbDesc.Checked Then
            sSql &= "AND prepaidmstdesc LIKE '%" & Tchar(txtFilter.Text) & "%' "
            If cbBlmPosting.Checked Then
                sSql &= "AND prepaidmststatus <> 'POST' "
            End If
        End If

        If cbBlmPosting.Checked Then
            sSql &= "AND prepaidmststatus <> 'POST' "
        End If

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= "AND mst.branch_code='" & dCabangNya.SelectedValue & "'"
        End If
        sSql &= " AND ((prepaidmstoid > 0)OR(prepaidmstoid < 0 AND prepaidmststatus='POST')) ORDER BY Prepaidmstoid DESC"

        Dim objTable As DataTable = GetDataTable(sSql, "QL_trnprepaidmst")
        Session("tbldata") = objTable
        GVPrePaidmst.DataSource = objTable
        GVPrePaidmst.DataBind()
    End Sub

    Private Sub BindExpense()
        imbClearExp_Click(Nothing, Nothing)

        Try
            If GetAllOidInDDL(prepaidmstacctgoid) <> "" Then
                sSql = "SELECT gm.cashbankoid,gm.cashbankno,gm.cashbankdate,gd.cashbankgloid,gd.acctgoid,gd.cashbankglnote,gd.cashbankglamt,gd.branch_code " & _
                    "FROM QL_trncashbankmst gm INNER JOIN  QL_cashbankgl gd ON gd.cashbankoid=gm.cashbankoid " & _
                    "where gm.cmpcode='" & CompnyCode & "' AND gm.cashbankgroup='COST' AND gm.cashbankstatus='POST' AND gm.branch_code='" & DDLCabang.SelectedValue & "' AND gd.acctgoid in (" & GetAllOidInDDL(prepaidmstacctgoid) & ") AND gm.cashbankno LIKE '%" & Tchar(cashbankno.Text) & "%' AND gd.cashbankgloid NOT IN (SELECT p.cashbankgloid FROM QL_trnprepaidmst p WHERE p.cmpcode=gm.cmpcode) ORDER BY gm.cashbankno "
                Dim dtExpense As DataTable = GetDataTable(sSql, "QL_trncashbankmst")
                gvExpense.DataSource = dtExpense
                gvExpense.DataBind()
                Session("Expense") = dtExpense
                gvExpense.Visible = True
            Else
                showMessage("Tidak ada Setting COA untuk Expense Prepaid Account", 2)
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<BR>" & sSql, 1)
        End Try
    End Sub

    Private Function GetAllOidInDDL(ByVal DDLAcctg As DropDownList) As String
        Dim sAcctgOid As String = ""
        For C1 As Integer = 0 To DDLAcctg.Items.Count - 1
            sAcctgOid &= (DDLAcctg.Items(C1).Value.ToString & ",")
        Next
        If sAcctgOid <> "" Then sAcctgOid = Left(sAcctgOid, sAcctgOid.Length - 1)
        Return sAcctgOid
    End Function

    Protected Sub initAllDDL() 
        FillDDLAcctg(prepaidmstacctgoid, "VAR_AMORTY", DDLCabang.SelectedValue)
        FillDDLAcctg(ddLPayAcctg, "VAR_ACCUMULATION_AMORTYTATION", DDLCabang.SelectedValue)
        'BIND DATA YEAR
        Dim yearrange As Integer = 5
        Dim GetServerTimeyear As String = CStr(GetServerTime().Year)
        ddlYear.Items.Clear()
        For i As Integer = 0 To yearrange - 1
            'ddlYear.Items.Insert(i, GetServerTimeyear + i)
            ddlYear.Items.Insert(i, GetServerTimeyear + i - (yearrange - 3))
            ddlYear.Items(i).Value = GetServerTimeyear + i - (yearrange - 3)
        Next
        ddlYear.SelectedValue = CStr(GetServerTime().Year)
    End Sub

    Private Sub fillTextBox(ByVal sCmpcode As String, ByVal vjurnaloid As String)

        Dim mySqlConn As New SqlConnection(ConnStr)
        Dim sqlSelect As String = "SELECT branch_code,crtuser,crttime, prepaidmstcode,prepaidmstoid,prepaidmstnote, prepaidmstdate,prepaidmststartdate,prepaidmstfinishdate, prepaidmstperiode,prepaidmstlength,prepaidmstlengthvalue,prepaidmsttotalamount, prepaidmstamount,prepaidmstvalue,prepaidmstaccumamount, prepaidmstacctgoid,upduser,updtime, prepaidmstpayacctgoid,prepaidmstnote, prepaidmststatus, prepaidmstdesc,updtime,cashbankgloid FROM QL_trnprepaidmst WHERE prepaidmstoid = " & vjurnaloid & " AND cmpcode = '" & CompnyCode & "'"

        Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
        Dim objDs As New DataSet : Dim objTable As DataTable
        Dim objRow() As DataRow

        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

        If objRow.Length > 0 Then
 
            DDLCabang.SelectedValue = Trim(objRow(0)("branch_code").ToString)
            cashbankgloid.Text = objRow(0)("cashbankgloid").ToString
            sSql = "SELECT c.cashbankno FROM QL_cashbankgl d INNER JOIN QL_trncashbankmst c ON c.cashbankoid=d.cashbankoid WHERE d.cashbankgloid=" & cashbankgloid.Text
            cashbankno.Text = GetScalar(sSql)
            PrePaidmstoid.Text = Trim(objRow(0)("prepaidmstoid").ToString)
            prepaidmstcode.Text = Trim(objRow(0)("prepaidmstcode").ToString)
            PrepaidDate.Text = Format(objRow(0)("prepaidmstdate"), "dd/MM/yyyy")
            StartDate.Text = Format(objRow(0)("prepaidmststartdate"), "dd/MM/yyyy")
            FinishDate.Text = Format(objRow(0)("prepaidmstfinishdate"), "dd/MM/yyyy")
            PrepaidDesc.Text = Trim(objRow(0)("prepaidmstdesc").ToString)
            PaidNote.Text = Trim(objRow(0)("prepaidmstnote").ToString)
            StatusMst.Text = Trim(objRow(0)("prepaidmststatus").ToString)
            prepaidmstacctgoid.SelectedValue = Trim(objRow(0)("prepaidmstacctgoid").ToString)
            PrePaidAmt.Text = ToMaskEdit(Trim(objRow(0)("prepaidmstamount").ToString), 3)
            PrepaidAccum.Text = ToMaskEdit(Trim(objRow(0)("prepaidmsttotalamount").ToString), 3)
            ddLPayAcctg.SelectedValue = Trim(objRow(0)("prepaidmstpayacctgoid").ToString)
            PrepaidmstValue.Text = ToMaskEdit(Trim(objRow(0)("PrepaidmstValue").ToString), 3)
            PrepaidLenght.Text = CInt(Trim(objRow(0)("prepaidmstlength").ToString))
            hasilbagi.Text = CInt(Trim(objRow(0)("prepaidmstlengthvalue").ToString))
            createuser.Text = "Created By <B>" & objRow(0)("crtuser").ToString & "</B> On <B>" & objRow(0)("crttime").ToString & "</B> "
            UpdUser.Text = "; Last Update By <B>" & objRow(0)("upduser").ToString & "</B> On <B>" & objRow(0)("updtime").ToString & "</B> "
            accumAmt.Text = ToMaskEdit(ToDouble(PrePaidAmt.Text) - ToDouble(PrepaidAccum.Text), 3)
            If Trim(objRow(0)("prepaidmststatus").ToString) <> "POST" And Trim(objRow(0)("prepaidmststatus").ToString) <> "DISPOSED" Then
                btnSave.Visible = True
                btnDelete.Visible = True
                btnPosting.Visible = True
            Else
                btnSave.Visible = False
                btnDelete.Visible = False
                btnPosting.Visible = False
                btnGenerate.Visible = False
            End If

            'Data(detail)
            Dim sMonth As String = Format(GetServerTime, "MM")
            Dim sYear As String = Format(GetServerTime, "yyyy")

            sqlSelect = "SELECT branch_code,prepaiddtloid,prepaidmstoid,prepaiddtlseq,prepaiddtlamount ,prepaiddtlaccum,prepaiddtlaccumamt,prepaiddtldate,prepaiddtlnote,prepaiddtlstatus ,crtuser,crttime,upduser,updtime FROM QL_trnprepaiddtl WHERE prepaidmstoid=" & vjurnaloid & " AND cmpcode='" & CompnyCode & "' "
            'If Trim(objRow(0)("prepaidmststatus").ToString) = "DISPOSED" Then
            '    sqlSelect &= "AND MONTH(prepaiddtldate) <= '" & sMonth & "' AND YEAR(prepaiddtldate) <= '" & sYear & "'"
            'End If

            Dim mySqlDAdtl As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
            Dim objDsDtl As New DataSet
            Dim objTableDtl As DataTable
            Dim objRowDtl() As DataRow

            mySqlDAdtl.Fill(objDsDtl)
            objTableDtl = objDsDtl.Tables(0)
            objRowDtl = objTableDtl.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Session("tblDtl") = objDsDtl.Tables(0)

            GVprePaiddtl.DataSource = objDsDtl.Tables(0)
            GVprePaiddtl.DataBind()
        End If
        mySqlConn.Close()
    End Sub

    Private Sub generatedmstoid()
        PrePaidmstoid.Text = GenerateID("QL_trnprepaidmst", CompnyCode)
    End Sub

    Private Sub generetedtloid()
        Session("dtloid") = GenerateID("QL_trnprepaiddtl", CompnyCode)
    End Sub

    Protected Sub ClearDtlPrePaid()
        GVprePaiddtl.SelectedIndex = -1
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnPrePaid.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Prepaid Transaction"

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            fDDLBranch() : CabangNyaDdl()
            InitDDLBranch() : initAllDDL()
            Dim CutOffDate As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
            If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", 2)
                Exit Sub
            Else
                CutOffDate = toDate(GetStrData(sSql))
                Session("CutOffDate") = toDate(GetStrData(sSql))
            End If
            binddata()
            CutoffDatePaid.Text = Format(CutOffDate.AddDays(-1), "dd/MM/yyyy")
            offdate.Text = Format(CutOffDate, "dd/MM/yyyy")
            txtPeriode1.Text = Format(GetServerTime, "01/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            lblViewInfo.Visible = True
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                fillTextBox(CompnyCode, Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                lblPOST.Text = "IN PROCESS"
                DDLCabang.Enabled = False
                DDLCabang.CssClass = "inpTextDisabled"
                fixPresentValue.Text = 0
            Else
                BtnCancel.Visible = True
                StatusMst.Text = "IN PROCESS" : Monthly.Visible = False
                generatedmstoid() : prepaidmstcode.Text = PrePaidmstoid.Text
                btnSave.Visible = True : BtnDelete.Visible = False
                BtnPosting.Visible = True
                createuser.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime()
                PrepaidDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                StartDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                FinishDate.Text = Format(GetServerTime, "01/MM/yyyy")
                accumAmt.Text = "0.00"
                lblPOST.Text = "IN PROCESS"
                TabContainer1.ActiveTabIndex = 0
            End If
            Dim dt As DataTable
            dt = Session("tbldtl")
            GVprePaiddtl.DataSource = dt
            GVprePaiddtl.DataBind()
        End If
    End Sub

    Protected Sub DDLPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DDLPeriodType.SelectedValue = "MONTHLY" Then
            If todouble(PrepaidLenght.Text) > 12 Then
                PrepaidLenght.Text = 0
            End If
        End If
    End Sub

    Protected Sub lbkHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'ClearDtlPrePaid()
        'MultiView1.ActiveViewIndex = 0
    End Sub 

    Protected Sub lbkPostMoreInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 1
    End Sub

    Protected Sub lbkPostInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 0
    End Sub

    Protected Sub GVPrePaidmst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnPrePaid.aspx?oid=" & GVPrePaidmst.SelectedDataKey("prepaidmstoid").ToString & "")
    End Sub

    Protected Sub GVPrePaidmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVPrePaidmst.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub GVPrePaidmst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVPrePaidmst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
            e.Row.Cells(6).Text = Format(CDate(e.Row.Cells(6).Text), "dd/MM/yyyy")
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 3)
            e.Row.Cells(8).Text = ToMaskEdit(e.Row.Cells(8).Text, 3)
        End If
    End Sub

    Protected Sub GVDtlMonth_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(toDate(e.Row.Cells(2).Text)), "dd/MM/yyyy")
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
        End If
    End Sub

    Protected Sub GVprePaiddtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVprePaiddtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")
            e.Row.Cells(2).Text = ToMaskEdit(e.Row.Cells(2).Text, 3)
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
            'FinishDate.Text = Format(CDate(toDate(e.Row.Cells(1).Text)), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub GVprePaiddtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "prepaiddtldate='" & GVprePaiddtl.SelectedDataKey("prepaiddtldate").ToString & "'"
        dv.RowFilter = "prepaiddtlstatus = 'IN PROCESS'"
        If dv.Count <= 0 Then
            showMessage(" Tidak dapat menampilkan data detail !!", 2)
        End If
        dv.RowFilter = ""
    End Sub

    Protected Sub GVprePaiddtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVprePaiddtl.PageIndex = e.NewPageIndex
        GVprePaiddtl.DataSource = Session("tbldtl")
        GVprePaiddtl.DataBind()
    End Sub

    Protected Sub PrePaidAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrePaidAmt.TextChanged
        fixPresentValue.Text = ToDouble(hasilbagi.Text)
        accumAmt.Text = ToMaskEdit(ToDouble(PrePaidAmt.Text) - ToDouble(PrepaidAccum.Text), 3)
        PrePaidAmt.Text = ToMaskEdit(ToDouble(PrePaidAmt.Text), 3)
        PrepaidAccum.Text = ToMaskEdit(ToDouble(PrePaidAmt.Text), 3)
        ReAmountAccum()
        PrePaidReAmount()
    End Sub 

    Protected Sub btnViewALL_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Session("tbldata") = Nothing
        'GVPrePaidmst.PageIndex = 0
        'DDLfilter.SelectedIndex = 0
        txtFilter.Text = ""
        binddata()
    End Sub 

    Protected Sub hasilbagi_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        fixPresentValue.Text = todouble(hasilbagi.Text)
        hasilbagi.Text = todouble(hasilbagi.Text)
        PrePaidReAmount()
    End Sub

    Protected Sub accumAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        accumAmt.Text = ToMaskEdit(ToDouble(accumAmt.Text), 3)
    End Sub

    Protected Sub PrepaidAccum_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        fixPresentValue.Text = todouble(hasilbagi.Text)
        accumAmt.Text = todouble(PrePaidAmt.Text) - todouble(PrepaidAccum.Text)
        PrepaidAccum.Text = ToMaskEdit(ToDouble(PrepaidAccum.Text), 3)
        ReAmountAccum()
        PrePaidReAmount()
    End Sub

    Protected Sub PrepaidmstValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'PrepaidmstValue.Text = ToMaskEdit(todouble(PrepaidmstValue.Text), 2)
        'PrePaiddtlAccum.Text = ToMaskEdit(todouble(PrepaidmstValue.Text), 2)
        PrePaidReAmount()
        ReAmountAccum()
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtlTable As DataTable = SetTableDetail()
        Session("TblDtl") = dtlTable
        ' Validation
        Dim sMsg As String = ""

        If IsDate(toDate(PrepaidDate.Text)) = False Or IsDate(toDate(PrepaidDate.Text)) = False Then
            showMessage("Format Pre-Paid Date salah !!<BR>", 2)
            Exit Sub
        End If

        If IsDate(toDate(StartDate.Text)) = False Or IsDate(toDate(StartDate.Text)) = False Then
            showMessage("Format Tanggal awal salah !!<BR>", 2)
            Exit Sub
        End If

        If CDate(toDate(StartDate.Text)) <= CDate(toDate(CutoffDatePaid.Text)) Then
            sMsg &= "- Tanggal awal harus Lebih dari CutOffDate '" & CutoffDatePaid.Text & "' !<BR>"
        End If

        If PrepaidLenght.Text = "" Then
            sMsg &= "- Susut (bulan) harus lebih besar dari 0 !!<BR>"
        Else
            If CDbl(PrepaidLenght.Text) <> -1 And CDbl(PrepaidLenght.Text) <= 0 Then
                sMsg &= "- Length Period harus lebih besar dari 0 !!<BR>"
            Else
                If CDbl(PrepaidLenght.Text) = -1 Then
                    sMsg &= "Penyusutan bulan tidak di perlukan untuk Pre Paid ini !!<BR>"
                Else
                End If
            End If
        End If

        If todouble(PrepaidmstValue.Text) <= 0 Then
            sMsg &= " - Nilai Prepaid Value harus > 0 !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If

        Dim FAdtlID As String = "1"
        Dim objTableFAdtl As DataTable
        Dim objRowDAdt() As DataRow
        objTableFAdtl = Session("TblDtl")
        objRowDAdt = objTableFAdtl.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        FAdtlID = objRowDAdt.Length + 1
        Dim objTable As DataTable
        Dim objRow As DataRow
        objTable = Session("TblDtl")
        Dim iYear As Integer = CDate(toDate(CutoffDatePaid.Text)).Year
        Dim sMonth As String = ""
        Dim iValue As Decimal = todouble(PrepaidAccum.Text) ' >> total prepaid
        Dim Accum As Decimal = todouble(PrepaidmstValue.Text) ' >> pengurang bulanan
        Dim dValueAsliBulanane As Decimal = todouble(PrepaidmstValue.Text)
        Dim timeServer = GetServerTime()

        For C1 As Int16 = 0 To CInt(hasilbagi.Text) - 1
            Dim iMonth As Date = CDate(toDate(StartDate.Text))
            If iValue < todouble(PrepaidmstValue.Text) Then
                PrepaidmstValue.Text = iValue
            End If
            iValue = iValue - Math.Round(todouble(PrepaidmstValue.Text), 2, MidpointRounding.ToEven)
            If Math.Round(todouble(iValue), 2, MidpointRounding.ToEven) < 1 Then
                Accum += ToMaskEdit(ToDouble(iValue), 3)
                iValue -= ToMaskEdit(ToDouble(iValue), 3)
            End If
            If Accum > PrePaidAmt.Text Then
                Accum = todouble(PrePaidAmt.Text)
            End If

            If C1 = 0 Then
                Accum += Math.Round(todouble(accumAmt.Text), 2, MidpointRounding.ToEven)
            End If

            If Session("TblDtl") IsNot Nothing Then
                objRow = objTable.NewRow()
                objRow("cmpcode") = CompnyCode
                objRow("prepaiddtloid") = FAdtlID + C1
                objRow("prepaidmstoid") = 1
                objRow("prepaiddtlseq") = 1
                If C1 = 0 Then
                    objRow("prepaiddtldate") = CDate(iMonth).ToString
                Else
                    objRow("prepaiddtldate") = CDate(iMonth).AddMonths(Integer.Parse(C1)).ToString
                End If
                objRow("prepaiddtlamount") = ToMaskEdit(ToDouble((iValue)), 3) ' >>> Nilai yg masih ostd pada periode tsb
                objRow("prepaiddtlaccumamt") = todouble(PrepaidmstValue.Text) ' >>> pengurang tiap periode bayar
                objRow("prepaiddtlaccum") = Accum ' >>> Total terbayar ,
                objRow("prepaiddtlnote") = "" 'DDLaccum.SelectedValue
                objRow("prepaiddtlstatus") = "IN PROCESS" 'DDLadExpense.SelectedValue
                objRow("crtuser") = Session("UserID")
                objRow("crttime") = timeServer
                objRow("upduser") = Session("UserID")
                objRow("updtime") = timeServer
                objTable.Rows.Add(objRow)
                If C1 = 0 Then
                    FinishDate.Text = Format(CDate(iMonth).ToString, "dd/MM/yyyy")
                Else
                    FinishDate.Text = Format(CDate(iMonth).AddMonths(Integer.Parse(C1)), "dd/MM/yyyy")
                End If
            End If
            iMonth = iMonth
            Accum += ToMaskEdit(ToDouble(PrepaidmstValue.Text), 3)
        Next

        PrepaidmstValue.Text = ToMaskEdit(dValueAsliBulanane, 3)

        Session("tbldtl") = objTable
        GVprePaiddtl.DataSource = Session("tbldtl")
        GVprePaiddtl.DataBind()
        btnSave.Visible = True
        'MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDtlPrePaid()
    End Sub 

    Protected Sub StartDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("tbldtl") = Nothing
        GVprePaiddtl.DataSource = Nothing
        GVprePaiddtl.DataBind()
    End Sub

#End Region

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        btnValidasi.Visible = False : panelMsg.Visible = False
    End Sub 

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click 
        Dim sMsg As String = ""
        If ToDouble(cashbankgloid.Text) = 0 Then
            sMsg &= "- Silahkan pilih Transaksi Expense dahulu !!<BR>"
        End If
        If IsDate(toDate(PrepaidDate.Text)) = False Or IsDate(toDate(PrepaidDate.Text)) = False Then
            showMessage("Format Pre-Paid Date salah !!<BR>", 2)
            Exit Sub
        End If

        If IsDate(toDate(StartDate.Text)) = False Or IsDate(toDate(StartDate.Text)) = False Then
            showMessage("Format Tanggal awal salah !!<BR>", 2)
            Exit Sub
        End If

        If prepaidmstcode.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Kode Prepaid !!<BR>"
        End If

        If PrepaidDesc.Text = "" Then
            sMsg &= "- Silahkan isi deskripsi Prepaid !!<BR>"
        End If

        If PrePaidAmt.Text = "" Or ToDouble(PrePaidAmt.Text) <= 0 Then
            sMsg &= "- Nilai awal harus lebih besar dari 0!!<BR>"
        End If

        If hasilbagi.Text = "" Or ToDouble(hasilbagi.Text) <= 0 Then
            sMsg &= "- Nilai angsuran harus lebih besar dari 0 bulan!!<BR>"
        End If

        If PrepaidLenght.Text = "" Or ToDouble(PrepaidLenght.Text) <= 0 Then
            sMsg &= "- Nilai Angsuran harus lebih besar dari 0 bulan!!<BR>"
        End If

        If PrepaidAccum.Text = "" Or ToDouble(PrepaidAccum.Text) <= 0 Then
            sMsg &= "- Nilai Value terakhir prepaid harus lebih besar dari 0!!<BR>"
        End If

        If prepaidmstacctgoid.Items.Count <= 0 Then
            sMsg &= "- Silahkan setting interface 'VAR_AMORTY' di master interface!!<BR>"
        End If

        If ToDouble(PrepaidAccum.Text) > ToDouble(PrePaidAmt.Text) Then
            PrepaidmstValue.Text = "0"
            sMsg &= "- Nilai buku tidak Boleh lebih besar dari Nilai Prepaid Amount !!<br>"
        End If

        Dim objdtl As DataTable = Session("TblDtl")
        If objdtl Is Nothing Then
            sMsg &= "- Data Detail belum belum ada <br>"
        Else
            If objdtl.Rows.Count < 1 Then
                sMsg &= "- Data Detail belum belum ada <br>"
            End If
        End If

        If IsValidPeriod() Then
            If CDate(toDate(PrepaidDate.Text)) <= CDate(toDate(CutoffDatePaid.Text)) Then
                sMsg &= "- Prepaid Tanggal tidak boleh kurang dari CutOffDate tanggal '" & offdate.Text & "' !<BR>"
            Else
                'If Not CheckClosingAcctgStatus(CDate(toDate(PrepaidDate.Text)), DDLCabang.SelectedValue) Then
                '    sMsg &= "- Periode Accounting untuk tanggal yang dipilih tidak tersedia atau sudah Closed !!<BR>"
                'End If
            End If
        End If

        sSql = "SELECT COUNT(-1) FROM ql_trnprepaidmst WHERE cmpcode='" & CompnyCode & "' AND prepaidmstcode='" & Tchar(prepaidmstcode.Text) & "' AND prepaidmstoid > 0 "
        If Session("oid") <> Nothing Or Session("oid") <> "" Then
            sSql &= " AND prepaidmstoid <> " & Session("oid")
        End If
        Dim PrepaidCode As Integer = Integer.Parse(GetStrData(sSql))
        If PrepaidCode > 0 Then
            sMsg &= "- Kode ini sudah digunakan oleh Prepaid yang lain!!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            lblPOST.Text = "IN PROCESS"
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Dim MstOid As String = GenerateID("QL_trnprepaidmst", "MSC")
            Dim DtlOid As String = GenerateID("QL_trnprepaiddtl", "MSC")
            If lblPOST.Text = "POST" Then
                GenerateCodePrepaid()
            Else
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    prepaidmstcode.Text = MstOid
                End If
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO QL_trnprepaidmst (cmpcode,branch_code, prepaidmstoid, prepaidmstcode, prepaidmstdesc, prepaidmstdate,prepaidmstamount, prepaidmsttotalamount, prepaidmststartdate, prepaidmstfinishdate,prepaidmstperiode, prepaidmstlength, prepaidmstlengthvalue, prepaidmstvalue, prepaidmstaccumamount, prepaidmstacctgoid, prepaidmstpayacctgoid, prepaidmstnote, prepaidmststatus, crtuser, crttime, upduser, updtime, syncflag, synctime,cashbankgloid) VALUES " & _
                "('" & CompnyCode & "','" & DDLCabang.SelectedValue & "'," & MstOid & ",'" & Tchar(prepaidmstcode.Text) & "','" & Tchar(PrepaidDesc.Text) & "','" & CDate(toDate(PrepaidDate.Text)) & "','" & ToDouble(PrePaidAmt.Text) & "','" & ToDouble(PrepaidAccum.Text) & "','" & CDate(toDate(StartDate.Text)) & "','" & CDate(toDate(FinishDate.Text)) & "','" & DDLPeriodType.SelectedValue & "','" & PrepaidLenght.Text & "','" & hasilbagi.Text & "','" & ToDouble(PrepaidmstValue.Text) & "','" & ToDouble(accumAmt.Text) & "','" & prepaidmstacctgoid.SelectedValue & "','" & ddLPayAcctg.SelectedValue & "','" & Tchar(PaidNote.Text) & "','" & lblPOST.Text & "','" & Session("UserId") & "',CURRENT_TIMESTAMP,'" & Session("UserId") & "',CURRENT_TIMESTAMP,'','1/1/1900'," & cashbankgloid.Text & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ''Update mst lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & MstOid & " WHERE tablename='QL_trnprepaidmst' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                'Update()
                sSql = "UPDATE QL_trnprepaidmst SET prepaidmstdate='" & CDate(toDate(PrepaidDate.Text)) & "',prepaidmstdesc='" & Tchar(PrepaidDesc.Text.Trim) & "', prepaidmstamount='" & ToDouble(PrePaidAmt.Text) & "', prepaidmstnote='" & Tchar(PaidNote.Text) & "', prepaidmsttotalamount=" & ToDouble(PrepaidAccum.Text) & ", prepaidmstacctgoid='" & prepaidmstacctgoid.SelectedValue & "', prepaidmstpayacctgoid='" & ddLPayAcctg.SelectedValue & "', prepaidmststartdate='" & CDate(toDate(StartDate.Text)) & "', prepaidmstvalue =" & ToDouble(PrepaidmstValue.Text) & ", prepaidmstlengthvalue='" & hasilbagi.Text & "', prepaidmststatus='" & lblPOST.Text & "', prepaidmstaccumamount='" & ToDouble(accumAmt.Text) & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, prepaidmstcode='" & Tchar(prepaidmstcode.Text) & "', syncflag='',cashbankgloid=" & cashbankgloid.Text & " WHERE prepaidmstoid='" & PrePaidmstoid.Text & "' AND cmpcode='" & CompnyCode & "' "

                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "DELETE FROM QL_trnprepaiddtl WHERE cmpcode='" & CompnyCode & "' AND prepaidmstoid='" & PrePaidmstoid.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If Not Session("TblDtl") Is Nothing Then
                Dim i As Integer
                Dim objTable As DataTable
                Dim objRow() As DataRow

                'Insert()
                objTable = Session("TblDtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                For i = 0 To objRow.Length - 1
                    'Insert(New dtl)
                    sSql = "INSERT INTO QL_trnprepaiddtl (cmpcode,branch_code, prepaiddtloid, prepaidmstoid, prepaiddtlseq, prepaiddtldate, prepaiddtlamount, prepaiddtlaccumamt, prepaiddtlaccum, prepaiddtlnote, prepaiddtlstatus, crtuser, crttime, upduser, updtime) VALUES " & _
                    "('" & CompnyCode & "','" & DDLCabang.SelectedValue & "'," & DtlOid & "," & PrePaidmstoid.Text & "," & (i + 1) & ",'" & objRow(i)("prepaiddtldate").ToString.Trim & "','" & ToDouble(objRow(i)("prepaiddtlamount").ToString.Trim) & "','" & objRow(i)("prepaiddtlaccumamt").ToString.Trim & "','" & objRow(i)("prepaiddtlaccum").ToString.Trim & "','" & objRow(i)("prepaiddtlnote").ToString.Trim & "','" & objRow(i)("prepaiddtlstatus").ToString.Trim & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    DtlOid += 1
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next

                'Update dtl lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & (DtlOid + objRow.Length - 1) & " WHERE tablename='QL_trnprepaiddtl' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : objConn.Close()
            Session("tbldtl") = Nothing
        Catch ex As Exception
            objTrans.Rollback() : objConn.Close()
            lblPOST.Text = "IN PROCESS"
            showMessage(ex.ToString, 1) : Exit Sub
        End Try
        Session("dtlTable") = Nothing : Session("VIDCashBank") = Nothing
        Session("VIDCost") = Nothing : Session("oid") = Nothing
        Response.Redirect("trnPrepaid.aspx?awal=true")
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        btnSave.Enabled = True : Session("oid") = Nothing
        Session("tbldtl") = Nothing
        Response.Redirect("trnPrePaid.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Connection = objConn
        xCmd.Transaction = objTrans
        Try
            'delete dtl
            sSql = "DELETE FROM QL_trnprepaiddtl WHERE prepaidmstoid='" & Session("oid") & "' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' delete mst
            sSql = "DELETE FROM QL_trnprepaidmst WHERE prepaidmstoid='" & Session("oid") & "' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : Session("tbldtl") = Nothing
            objConn.Close()
        Catch ex As Exception
            objTrans.Rollback() : objConn.Close()
            showMessage(ex.ToString, 1) : Exit Sub
        End Try
        Session("tbldtl") = Nothing : Session("oid") = Nothing
        showMessage("Data telah dihapus !", 3)
        Response.Redirect("trnPrePaid.aspx?awal=true")
    End Sub 

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnPosting.Click
        lblPOST.Text = "POST" : btnSave_Click(sender, e)
    End Sub
 
    Protected Sub DDLCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCabang.SelectedIndexChanged
        FillDDLAcctg(prepaidmstacctgoid, "VAR_AMORTY", DDLCabang.SelectedValue)
        FillDDLAcctg(ddLPayAcctg, "VAR_ACCUMULATION_AMORTYTATION", DDLCabang.SelectedValue)
    End Sub

    Protected Sub FindBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles FindBtn.Click
        sSql = "SELECT m.branch_code,gendesc,d.prepaidmstoid,m.prepaidmstcode,d.prepaiddtloid,CONVERT(CHAR(10),d.prepaiddtldate,103) prepaiddtldate,m.prepaidmstdesc,d.prepaiddtlaccumamt,m.prepaidmstacctgoid,m.prepaidmstpayacctgoid,d.prepaiddtlamount,m.prepaidmsttotalamount, d.prepaiddtlaccum,d.prepaiddtlstatus FROM QL_trnprepaiddtl d INNER JOIN QL_trnprepaidmst m ON m.prepaidmstoid=d.prepaidmstoid AND m.cmpcode=d.cmpcode INNER JOIN QL_mstacctg g ON m.prepaidmstacctgoid=g.acctgoid INNER JOIN QL_mstacctg g1 ON g1.acctgoid=m.prepaidmstpayacctgoid INNER JOIN QL_mstgen cb ON cb.gencode=m.branch_code AND cb.gengroup='CABANG' WHERE d.prepaidmstoid=m.prepaidmstoid AND m.prepaidmstacctgoid=g.acctgoid AND MONTH(d.prepaiddtldate)=" & ddlMonth.SelectedValue & " AND YEAR(d.prepaiddtldate)=" & ddlYear.SelectedValue & " AND m.prepaidmststatus = 'POST' AND d.prepaiddtlstatus ='IN PROCESS' AND d.prepaiddtlstatus <> 'POST' AND m.prepaidmststatus <> 'IN PROCESS' AND d.prepaiddtlstatus <> ('CLOSED')"

        If CabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND m.branch_code='" & CabangNya.SelectedValue & "'"
        End If

        sSql &= " ORDER BY m.prepaidmstoid DESC"
        FillGV(GVDtlMonth, sSql, "ql_trnprepaiddtl")
    End Sub
 
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim tgle As Date = toDate(txtPeriode1.Text)
            tgle = toDate(txtPeriode1.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If
        Session("tbldata") = Nothing
        GVPrePaidmst.PageIndex = 0
        Session("SearchFixAsset") = sql_temp
        lblViewInfo.Visible = False
        binddata()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        txtFilter.Text = ""
        cbPeriode.Checked = False
        cbDesc.Checked = False
        cbBlmPosting.Checked = False
        txtPeriode1.Text = Format(GetServerTime, "01/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
        lblViewInfo.Visible = False
        binddata()
    End Sub

    Protected Sub btnBatal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBatal.Click
        Session("oid") = Nothing
        Session("tbldtl") = Nothing
        Response.Redirect("trnPrepaid.aspx?awal=true")
    End Sub

    Protected Sub btnPostingDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPostingDtl.Click
        Dim C As Integer = GVDtlMonth.Rows.Count
        If C = 0 Then
            showMessage("Data Kosong, Find Data Lagi !!!", 2)
            Exit Sub
        End If

        Dim dPostingDate As Date = New Date(ddlYear.SelectedValue, ddlMonth.SelectedValue, Date.DaysInMonth(ddlYear.SelectedValue, ddlMonth.SelectedValue))

        Dim objTrans As SqlClient.SqlTransaction
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        xCmd.Connection = objConn
        xCmd.Transaction = objTrans

        Session("MstGLOid") = GenerateID("QL_trnglmst", "MSC")
        Session("DtlGLOid") = GenerateID("QL_trngldtl", "MSC")

        Try
            ' ==== POSTING PREPAID TO GL
            For C1 As Int16 = 0 To GVDtlMonth.Rows.Count - 1
                Dim depcostacctgoid As Integer = 0
                Dim accdepacctgoid As Integer = 0
                sSql = "UPDATE QL_trnprepaiddtl SET prepaiddtlstatus='POST' WHERE prepaiddtloid=" & GVDtlMonth.DataKeys(C1).Item("prepaiddtloid").ToString & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' === INSERT INTO TRN GL MST
                sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime,type,branch_code) VALUES" & _
                " ('" & CompnyCode & "'," & Session("MstGLOid") & ",'" & dPostingDate & "','" & GetDateToPeriodAcctg(dPostingDate) & "','PREPAID MONTHLY POSTING | Code=" & Tchar(GVDtlMonth.DataKeys(C1).Item("prepaidmstcode").ToString) & "','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'PREPAID','" & GVDtlMonth.DataKeys(C1).Item("branch_code").ToString & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'insert data detail
                'VAR BEBAN AKUM PENYUSUTAN
                sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,upduser,updtime,branch_code,glother2,glflag,glpostdate) VALUES" & _
                " ('" & CompnyCode & "'," & Session("DtlGLOid") & ",1," & Session("MstGLOid") & "," & GVDtlMonth.DataKeys(C1).Item("prepaidmstacctgoid").ToString & ",'D'," & ToDouble(GVDtlMonth.DataKeys(C1).Item("prepaiddtlaccumamt").ToString) & ",'" & Tchar(GVDtlMonth.DataKeys(C1).Item("prepaidmstcode").ToString) & "','PREPAID MONTHLY POSTING | Code=" & Tchar(GVDtlMonth.DataKeys(C1).Item("prepaidmstcode").ToString) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Tchar(GVDtlMonth.DataKeys(C1).Item("branch_code").ToString) & "','" & GVDtlMonth.DataKeys(C1).Item("prepaiddtloid").ToString & "','POST',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Session("DtlGLOid") += 1

                'VAR PENYUSUTAN
                sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,upduser,updtime,branch_code,glother2,glflag,glpostdate) VALUES " & _
                "('" & CompnyCode & "'," & Session("DtlGLOid") & ",2," & Session("MstGLOid") & "," & GVDtlMonth.DataKeys(C1).Item("prepaidmstpayacctgoid").ToString & ",'C'," & ToDouble(GVDtlMonth.DataKeys(C1).Item("prepaiddtlaccumamt").ToString) & ",'" & Tchar(GVDtlMonth.DataKeys(C1).Item("prepaidmstcode").ToString) & "','PREPAID MONTHLY POSTING | Code=" & Tchar(GVDtlMonth.DataKeys(C1).Item("prepaidmstcode").ToString) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Tchar(GVDtlMonth.DataKeys(C1).Item("branch_code").ToString) & "','" & GVDtlMonth.DataKeys(C1).Item("prepaiddtloid").ToString & "','POST',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Session("DtlGLOid") += 1

                'Update fixmst book value
                sSql = "UPDATE QL_trnprepaidmst SET prepaidmsttotalamount=" & ToDouble(GVDtlMonth.DataKeys(C1).Item("prepaiddtlamount").ToString) & ", syncflag='' WHERE prepaidmstoid=" & GVDtlMonth.DataKeys(C1).Item("prepaidmstoid").ToString
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Session("MstGLOid") += 1
            Next

            sSql = "UPDATE QL_mstoid SET lastoid=" & Session("MstGLOid") - 1 & " WHERE tablename='QL_trnglmst' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_mstoid SET lastoid=" & Session("DtlGLOid") - 1 & " WHERE tablename='QL_trngldtl' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : objConn.Close()
            FindBtn_Click(sender, e)
        Catch ex As Exception
            objTrans.Rollback() : objConn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        'Session("MstGLOid") = Nothing : Session("DtlGLOid") = Nothing
        Response.Redirect("~\Accounting\trnPrepaid.aspx?awal=true")
    End Sub

    Protected Sub gvExpense_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExpense.PageIndexChanging
        Dim dtExp As DataTable = Session("Expense")
        gvExpense.PageIndex = e.NewPageIndex
        gvExpense.DataSource = dtExp
        gvExpense.DataBind()
    End Sub

    Protected Sub imbFindExp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindExp.Click
        BindExpense()
    End Sub

    Protected Sub imbClearExp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearExp.Click
        gvExpense.DataSource = Nothing : gvExpense.DataBind()
        Session("Expense") = Nothing
        gvExpense.SelectedIndex = -1 : gvExpense.Visible = False
        cashbankno.Text = "" : cashbankgloid.Text = ""
    End Sub

    Protected Sub gvExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvExpense.SelectedIndexChanged
        cashbankno.Text = gvExpense.SelectedDataKey("cashbankno").ToString
        cashbankgloid.Text = gvExpense.SelectedDataKey("cashbankgloid").ToString
        PrepaidDate.Text = Format(CDate(gvExpense.SelectedDataKey("cashbankdate").ToString), "dd/MM/yyyy")
        StartDate.Text = Format(CDate(gvExpense.SelectedDataKey("cashbankdate").ToString), "dd/MM/yyyy")
        FinishDate.Text = Format(CDate(gvExpense.SelectedDataKey("cashbankdate").ToString), "dd/MM/yyyy")
        PrePaidAmt.Text = ToMaskEdit(ToDouble(gvExpense.SelectedDataKey("cashbankglamt").ToString), 3)
        PrePaidAmt_TextChanged(Nothing, Nothing)
        prepaidmstacctgoid.SelectedValue = gvExpense.SelectedDataKey("acctgoid").ToString

        gvExpense.DataSource = Nothing : gvExpense.DataBind()
        Session("Expense") = Nothing
        gvExpense.SelectedIndex = -1 : gvExpense.Visible = False
    End Sub
End Class
