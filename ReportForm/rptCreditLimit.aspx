<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptCreditLimit.aspx.vb" Inherits="ReportForm_rptCreditLimit" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
 <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Report Data Customer :."></asp:Label></th>
        </tr>
    </table>
    <table>
        <tr>
            <td>
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 584px; TEXT-ALIGN: center" class="Label" colSpan=1><TABLE width=580><TBODY><TR><TD class="Label" align=right>Periode Type</TD><TD style="TEXT-ALIGN: left" class="Label" align=left>:</TD><TD style="TEXT-ALIGN: left" class="Label" align=left><asp:TextBox id="sodate1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibPOdate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label2" runat="server" Text="to"></asp:Label> <asp:TextBox id="sodate2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibPODate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" TargetControlID="sodate1" PopupButtonID="ibPOdate1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender4" runat="server" TargetControlID="sodate2" PopupButtonID="ibPODate2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="sodate1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="sodate2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=right><asp:CheckBox id="cb1" runat="server" Text="Periode Type"></asp:CheckBox>&nbsp;<asp:DropDownList id="option1" runat="server" Width="44px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="AND">AND</asp:ListItem>
<asp:ListItem Value="OR">OR</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="ddlOption1" runat="server" Width="96px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="iomst.orderdate">SO Date</asp:ListItem>
<asp:ListItem Value="jualmst.trnjualdate">Invoice Date</asp:ListItem>
</asp:DropDownList></TD><TD style="TEXT-ALIGN: left" class="Label" align=left>:</TD><TD style="TEXT-ALIGN: left" class="Label" align=left><asp:TextBox id="optionDate1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibOptionDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="optionDate2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibOptionDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label6" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender5" runat="server" TargetControlID="optionDate1" PopupButtonID="ibOptionDate1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender6" runat="server" TargetControlID="optionDate2" PopupButtonID="ibOptionDate2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender5" runat="server" TargetControlID="optionDate1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender6" runat="server" TargetControlID="optionDate2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=right>Customer</TD><TD style="TEXT-ALIGN: left" class="Label" align=left>:</TD><TD style="TEXT-ALIGN: left" class="Label" align=left><asp:TextBox id="custName" runat="server" Width="168px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchcust" onclick="ibSearchcust_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelCust" onclick="ibDelCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: left" id="TDCustomer" class="Label" align=right colSpan=3 runat="server" Visible="false"><asp:GridView id="gvCust" runat="server" Width="98%" BackColor="White" OnPageIndexChanging="gvCust_PageIndexChanging" OnSelectedIndexChanged="gvCust_SelectedIndexChanged" AllowPaging="True" BorderColor="#DEDFDE" DataKeyNames="custoid,custcode,custname" AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Customer Code">
<HeaderStyle Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer Name">
<HeaderStyle Font-Size="X-Small" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#F7F7DE" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="WIDTH: 584px; TEXT-ALIGN: center" class="Label" colSpan=1></TD></TR><TR><TD style="WIDTH: 584px; HEIGHT: 25px; TEXT-ALIGN: center" colSpan=1><asp:ImageButton id="ToPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbExport" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 584px; HEIGHT: 97px; TEXT-ALIGN: center" vAlign=top><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD style="WIDTH: 584px; TEXT-ALIGN: left" colSpan=1></TD></TR></TBODY></TABLE><asp:UpdatePanel id="upMsgbox" runat="server" __designer:dtid="1125899906842652" __designer:wfdid="w1"><ContentTemplate __designer:dtid="1125899906842653">
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" __designer:wfdid="w2" DefaultButton="btnMsgBoxOK"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow" __designer:wfdid="w3"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black" __designer:wfdid="w4"></asp:Label></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w5"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black" __designer:wfdid="w6"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w8"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" DropShadow="True" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground" __designer:wfdid="w9">
                        </ajaxToolkit:ModalPopupExtender><asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False" __designer:wfdid="w10"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>