<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTTS.aspx.vb" Inherits="trnTTS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Tanda Terima Sementara" CssClass="Title" ForeColor="Maroon" Font-Names="Verdana" Font-Size="21px"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Tanda Terima Sementara :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=3><asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w25"><TABLE style="WIDTH: 533px"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left>Filter&nbsp;</TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:DropDownList id="filterddl" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem Value="a.ttsno">TTS No.</asp:ListItem>
<asp:ListItem Value="b.custname">Customer</asp:ListItem>
<asp:ListItem Value="trnjualno">SO No</asp:ListItem>
<asp:ListItem Value="trnsjjualno">DO No</asp:ListItem>
<asp:ListItem Value="TTSnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="filtertext" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w26" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" align=left>Period</TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w17"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w18"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w19"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w20"></asp:ImageButton>&nbsp;&nbsp; <asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w21"></asp:Label> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w13" MaskType="Date" Mask="99/99/9999" TargetControlID="tgl1"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w14" MaskType="Date" Mask="99/99/9999" TargetControlID="tgl2"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w15" TargetControlID="tgl1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w16" TargetControlID="tgl2" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left>Status</TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:DropDownList id="filterstatus" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w22"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
<asp:ListItem>Batal</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w23">
                                                            </asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w24">
                                                            </asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD></TD><TD></TD><TD></TD></TR><TR><TD colSpan=3><asp:GridView id="gvMaster" runat="server" Width="100%" __designer:dtid="5348024557502511" __designer:wfdid="w22" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333" __designer:dtid="5348024557502521"></RowStyle>
<Columns __designer:dtid="5348024557502513">
<asp:HyperLinkField DataNavigateUrlFields="ttsoid" DataNavigateUrlFormatString="trnTTS.aspx?oid={0}" DataTextField="ttsno" HeaderText="TTS No" __designer:dtid="5348024557502514">
<ControlStyle ForeColor="Red" __designer:dtid="5348024557502515"></ControlStyle>

<ItemStyle ForeColor="Red" __designer:dtid="5348024557502516"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="ttsdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TTS Date" __designer:dtid="5348024557502518"></asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" __designer:dtid="5348024557502519"></asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="SO No."></asp:BoundField>
<asp:BoundField DataField="trnsjjualno" HeaderText="DO No."></asp:BoundField>
<asp:BoundField DataField="ttsnote" HeaderText="Note"></asp:BoundField>
<asp:BoundField DataField="ttsstatus" HeaderText="Status"></asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" __designer:wfdid="w4" Text='<%# Eval("ttsoid") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" __designer:dtid="5348024557502512"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White" __designer:dtid="5348024557502524"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!" __designer:wfdid="w13"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" __designer:dtid="5348024557502523"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" __designer:dtid="5348024557502525"></HeaderStyle>

<AlternatingRowStyle BackColor="White" __designer:dtid="5348024557502526"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:Label id="Label4" runat="server" Font-Bold="True" Text="TTS Header :" __designer:wfdid="w25" Font-Underline="True"></asp:Label><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w35" MaskType="Date" Mask="99/99/9999" TargetControlID="ttsdate"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender10" runat="server" __designer:wfdid="w36" TargetControlID="ttsdate" Format="dd/MM/yyyy" PopupButtonID="ibttsdate"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>TTBS No. <asp:Label id="Label6" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w31"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="ttsno" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" __designer:dtid="3940649673949269" __designer:wfdid="w1" MaxLength="20" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="i_u" runat="server" ForeColor="Red" __designer:dtid="3940649673949270" Text="new" __designer:wfdid="w2" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>TTS Date</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="ttsdate" runat="server" Width="70px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:dtid="3940649673949274" __designer:wfdid="w26" ValidationGroup="MKE"></asp:TextBox>&nbsp;<asp:ImageButton id="ibttsdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w34"></asp:ImageButton> <asp:Label id="Label17" runat="server" CssClass="Important" __designer:dtid="3940649673949277" Text="(dd/MM/yyyy)" __designer:wfdid="w27"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Sales Order <asp:Label id="Label7" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w32"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="ttsso" runat="server" Width="150px" CssClass="inpText" Font-Bold="True" __designer:dtid="3940649673949269" __designer:wfdid="w29" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchso" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton> <asp:ImageButton id="btneraseso" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton> <asp:Label id="sono" runat="server" __designer:wfdid="w82" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Customer</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:TextBox id="ttscustname" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:dtid="3940649673949317" __designer:wfdid="w7" MaxLength="20" Enabled="False"></asp:TextBox> <asp:Label id="ttscustoid" runat="server" __designer:wfdid="w82" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView id="GVSo" runat="server" Width="100%" __designer:wfdid="w241" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" Visible="False" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" DataKeyNames="custoid">
<RowStyle BackColor="White"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="SO No.">
<HeaderStyle Wrap="False" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="SO Date">
<HeaderStyle Wrap="False" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail !!" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>DO Sales <asp:Label id="Label8" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w33"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="ttsDO" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Bold="True" __designer:dtid="3940649673949269" __designer:wfdid="w30" MaxLength="20" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchdo" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton> <asp:ImageButton id="btnerasedo" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w41"></asp:ImageButton> <asp:Label id="dono" runat="server" __designer:wfdid="w82" Visible="False"></asp:Label> <asp:Label id="dooid" runat="server" __designer:wfdid="w82" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Status</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:TextBox id="ttsstatus" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:dtid="3940649673949317" __designer:wfdid="w28" MaxLength="20" Enabled="False">In Process</asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView id="GVDo" runat="server" Width="100%" __designer:wfdid="w241" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" Visible="False" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" DataKeyNames="trnsjjualmstoid,custoid">
<RowStyle BackColor="White"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjjualno" HeaderText="SJ No.">
<HeaderStyle Wrap="False" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="SO No.">
<HeaderStyle Wrap="False" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="SJ Date">
<HeaderStyle Wrap="False" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail !!" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox style="TEXT-ALIGN: justify" id="ttsnote" runat="server" Width="500px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:dtid="3940649673949274" __designer:wfdid="w43" MaxLength="200" ValidationGroup="MKE"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left colSpan=4></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=3><asp:Label id="Label18" runat="server" Font-Bold="True" Text="TTS Detail :" __designer:wfdid="w13" Font-Underline="True"></asp:Label><asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" __designer:wfdid="w14" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label25" runat="server" Text="Item" __designer:wfdid="w21"></asp:Label> <asp:Label id="Label26" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w22"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="354px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w23"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w24"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w25"></asp:ImageButton> <asp:Label id="labelitemoid" runat="server" __designer:wfdid="w26" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Merk</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="merk" runat="server" Width="251px" CssClass="inpTextDisabled" __designer:dtid="3940649673949317" __designer:wfdid="w11" MaxLength="200" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" __designer:wfdid="w132" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" Visible="False" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" DataKeyNames="refoid,satuan1,satuan2,satuan3,unit1,unit2,konversi1_2,konversi2_3,unitoid" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="White"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="False" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtysisa" DataFormatString="{0:#,##0.00}" HeaderText="Stock">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="WhiteSmoke" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFFF80" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Qty <asp:Label id="Label28" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w30"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="qty" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w31" MaxLength="10" ValidationGroup="MKE" AutoPostBack="True">0.00</asp:TextBox>&nbsp; <asp:Label id="Label30" runat="server" CssClass="label" Text="<=" __designer:wfdid="w33"></asp:Label>&nbsp;&nbsp;<asp:Label id="labelmaxqty" runat="server" CssClass="label" ForeColor="Red" __designer:wfdid="w1">0.00</asp:Label>&nbsp; <asp:DropDownList id="unit" runat="server" Width="95px" CssClass="inpTextDisabled" __designer:dtid="3940649673949339" __designer:wfdid="w8" Enabled="False" AutoPostBack="True"></asp:DropDownList> <ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" __designer:wfdid="w35" TargetControlID="qty" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="labelkonversi1_2" runat="server" __designer:wfdid="w37" Visible="False">1</asp:Label> <asp:Label id="labelkonversi2_3" runat="server" __designer:wfdid="w37" Visible="False">1</asp:Label> <asp:Label id="labelunit1" runat="server" __designer:wfdid="w37" Visible="False"></asp:Label> <asp:Label id="labelunit2" runat="server" __designer:wfdid="w37" Visible="False"></asp:Label> <asp:Label id="labelunit3" runat="server" __designer:wfdid="w37" Visible="False"></asp:Label> <asp:Label id="maxqtytemp" runat="server" __designer:wfdid="w37" Visible="False">0.00</asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:Label id="labelsatuan1" runat="server" __designer:wfdid="w37" Visible="False"></asp:Label> <asp:Label id="labelsatuan2" runat="server" __designer:wfdid="w37" Visible="False"></asp:Label> <asp:Label id="labelsatuan3" runat="server" __designer:wfdid="w37" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="labelseq" runat="server" __designer:wfdid="w37" Visible="False">1</asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:GridView id="GVItemDetail" runat="server" Width="857px" __designer:wfdid="w40" CellPadding="2" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" DataKeyNames="itemoid,satuan,satuan1,satuan2,satuan3,unit1,unit2,unit3,konversi1_2,konversi2_3,maxqty">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk"></asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" runat="server" __designer:wfdid="w134" OnClick="lbdelete_Click"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton>
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#E7E7FF" ForeColor="#4A3C8C"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFFF80" Font-Bold="True" ForeColor="#F7F7F7"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="Blue"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w41"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w42"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w43"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w44"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w45" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w46" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" __designer:wfdid="w34" Visible="False"></asp:ImageButton> <asp:ImageButton id="batal" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" __designer:wfdid="w14" Visible="False" OnClick="batal_Click"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            ::
                            <strong><span style="font-size: 9pt">Form Tanda Terima Sementara</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg"  runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender>
<span style="display:none"><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button></span>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

