Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptSalesReport
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim sSql As String = ""
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Private cKon As New Koneksi
#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = "" 
        If CDate(toDate(range1.Text)) > CDate(toDate(range2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnrequestitemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnrequestitemoid=" & cbOid
                                dtView2.RowFilter = "trnrequestitemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedure"
    Private _cabang As DropDownList

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLcbg, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLcbg, sSql)
            Else
                FillDDL(DDLcbg, sSql)
                DDLcbg.Items.Add(New ListItem("ALL", "ALL"))
                DDLcbg.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLcbg, sSql)
            DDLcbg.Items.Add(New ListItem("ALL", "ALL"))
            DDLcbg.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitCBL()
        sSql = "Select Distinct typeSO, Case When typeSO='ecommers' then 'E-Commerce' else typeSO End TypeNya From QL_trnordermst"
        cbType.Items.Clear() 
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            cbType.Items.Add(xreader.GetValue(1).ToString)
            cbType.Items(cbType.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While

        xreader.Close()
        conn.Close()
        Session("LastAllCheck") = False
    End Sub

    Private Sub initDDL()
        Dim sqlcurr As String = "Select currencyoid, currencydesc From QL_mstcurr "
        FillDDL(DDLcurr, sqlcurr)
        DDLcurr.Items.Add(New ListItem("ALL", "ALL"))
        DDLcurr.SelectedValue = "ALL"

        FillDDL(FilterDDLGrup, "Select genoid,gendesc from QL_mstgen Where gengroup='ITEMGROUP' And genoid IN (Select itemgroupoid from QL_mstitem Where itemgroupoid=genoid) order by gendesc")
        FilterDDLGrup.Items.Add("ALL GROUP")
        FilterDDLGrup.SelectedValue = "ALL GROUP"

        'sSql = "Select Distinct typeSO, typeSO TypeNya From QL_trnordermst"
        'FillDDL(ddlSOtype, sSql)
        'ddlSOtype.Items.Add(New ListItem("ALL", "ALL"))
        'ddlSOtype.SelectedValue = "ALL"
    End Sub

    Private Sub SalesDDL()
        Dim tSal As String = ""
        sSql = "select gendesc from ql_mstgen where gengroup = 'JOBPOSITION' and (gendesc like '%sales%' or gendesc like '%kepala%' or gendesc like '%marketing%' or gendesc like '%operasional%' or gendesc like '%AMP%' or gendesc like '%OWNER%' or gendesc like '%ANALISA KEU. PUSAT%' or gendesc like '%ADMIN GUD & SVC CBG%') AND genoid = (SELECT PERSONSTATUS FROM QL_mstperson p INNER JOIN QL_MSTPROF pr ON p.personoid=pr.personnoid Where pr.BRANCH_CODE='" & Session("branch_id") & "' AND USERID='" & Session("UserID") & "')"
        Dim jbt As String = GetStrData(sSql) 

        If Session("UserID").ToString.ToUpper <> "ADMIN" And jbt.ToUpper <> "OWNER" And jbt.ToUpper <> "AMP" And jbt.ToUpper <> "ANALISA KEU. PUSAT" And jbt.ToUpper <> "OPERASIONAL CABANG" And jbt.ToUpper <> "ADMIN GUD & SVC CBG" Then
            If Session("branch_id") = "10" Then
                If Session("UserLevel") = 4 Then
                    tSal &= " AND PERSONNAME IN (Select USERNAME FROM QL_MSTPROF Where personnoid=personnoid AND BRANCH_CODE='" & DDLcbg.SelectedValue & "') AND PERSONOID IN (Select salesoid from QL_trnordermst Where branch_code='" & DDLcbg.SelectedValue & "' AND (orderno LIKE '%" & Tchar(sono.Text.Trim) & "%' Or ordermstoid LIKE '%" & sooid.Text & "%'))"
                End If

            Else
                If Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                    tSal &= "AND PERSONNAME IN (Select USERNAME FROM QL_MSTPROF Where USERNAME=PERSONNAME) AND PERSONOID IN (Select salesoid from QL_trnordermst Where (orderno LIKE '%" & Tchar(sono.Text.Trim) & "%' OR ordermstoid Like '%" & sooid.Text & "%'))"
                ElseIf Session("UserLevel") = 2 Then
                    tSal &= " AND PERSONNAME IN (Select USERNAME FROM QL_MSTPROF Where personnoid=personnoid AND BRANCH_CODE='" & DDLcbg.SelectedValue & "') AND PERSONOID IN (Select salesoid from QL_trnordermst Where branch_code='" & DDLcbg.SelectedValue & "' AND (orderno LIKE '%" & Tchar(sono.Text.Trim) & "%' Or ordermstoid LIKE '%" & sooid.Text & "%'))"

                Else
                    tSal &= "AND PERSONNAME IN (Select USERNAME FROM QL_MSTPROF Where personnoid=personnoid AND BRANCH_CODE='" & DDLcbg.SelectedValue & "' And userid='" & Session("UserID") & "')"
                End If

            End If
        Else
            If DDLcbg.SelectedItem.Text <> "ALL" Then
                tSal &= "AND PERSONNAME IN (Select USERNAME FROM QL_MSTPROF Where personnoid=personnoid AND BRANCH_CODE='" & DDLcbg.SelectedValue & "') AND PERSONOID IN (Select salesoid from QL_trnordermst Where (orderno LIKE '%" & Tchar(sono.Text.Trim) & "%' Or ordermstoid LIKE '%" & sooid.Text & "%'))"
            Else
                tSal &= "AND PERSONNAME IN (Select USERNAME FROM QL_MSTPROF Where personnoid=personnoid) AND PERSONOID IN (Select salesoid from QL_trnordermst Where (orderno LIKE '%" & Tchar(sono.Text.Trim) & "%' Or ordermstoid LIKE '%" & sooid.Text & "%'))"
            End If
        End If 
    End Sub 

    Private Sub BindPerson()
        Dim sCabang As String = "" : Dim sPic As String = ""

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
           sCabang &= " AND branch_code='" & DDLcbg.SelectedValue & "' AND USERID='" & Session("UserID") & "'"
        ElseIf Session("UserLevel") = 2 Then
            If DDLcbg.SelectedValue <> "ALL" Then
                sCabang &= " AND branch_code='" & DDLcbg.SelectedValue & "'"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            If DDLcbg.SelectedValue <> "ALL" Then
                sCabang &= " AND branch_code='" & DDLcbg.SelectedValue & "'"
            End If
        End If

        If Session("branch_id") <> "10" Then
            sPic &= "Where PERSONSTATUS IN (987)"
        Else
            If DDLcbg.SelectedValue <> "ALL" Then
                sPic &= "Where PERSONOID IN (Select spgoid from QL_trnjualmst jm Where jm.spgoid=PERSONOID AND jm.branch_code='" & DDLcbg.SelectedValue & "')"
            Else
                sPic &= "Where PERSONOID IN (Select spgoid from QL_trnjualmst jm Where jm.spgoid=PERSONOID)"
            End If
        End If

        sSql = "Select PERSONOID, PERSONNIP, PERSONNAME, PERSONCRTADDRESS From QL_MSTPERSON " & sPic & " And (personname LIKE '%" & Tchar(TxtPinjamNo.Text.Trim) & "%' OR personnip LIKE '%" & Tchar(TxtPinjamNo.Text.Trim) & "%') AND PERSONNAME IN (Select USERNAME FROM QL_MSTPROF Where personnoid=personnoid " & sCabang & ") AND STATUS='AKTIF'"
        FillGV(PersonGv, sSql, "ql_mstperson")
        PersonGv.Visible = True
    End Sub

    Private Sub BindListCust()
        sSql = "SELECT 'False' AS checkvalue, custoid, custcode, custname, custaddr FROM QL_mstcust c WHERE cmpcode='" & CompnyCode & "' AND c.custoid IN (Select o.trncustoid from QL_trnordermst o Where o.trncustoid=c.custoid AND c.branch_code=o.branch_code AND convert(char(10),o.trnorderdate,121) Between '" & Format(CDate(toDate(range1.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(range2.Text)), "yyyy-MM-dd") & "'"

        If SoNo.Text <> "" Then
            If DDLSONo.SelectedValue = "NomerSO" Then
                Dim sSono() As String = Split(SoNo.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " o.orderno LIKE '%" & Tchar(sSono(c1)) & "%'"
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            Else
                Dim sSono() As String = Split(SoNo.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " o.ordermstoid = " & ToDouble(sSono(c1))
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If
        Dim typeSO As String = ""
        For C1 As Integer = 0 To cbType.Items.Count - 1
            If cbType.Items(C1).Value <> "All" And cbType.Items(C1).Selected = True Then
                typeSO &= "'" & cbType.Items(C1).Value & "',"
            End If
        Next

        If typeSO <> "" Then
            typeSO = Left(typeSO, typeSO.Length - 1)
            sSql &= "And o.typeSO IN (" & typeSO & ")"
        End If

        sSql &= ") ORDER BY custcode"
        Session("TblCust") = cKon.ambiltabel(sSql, "QL_mstcust")
    End Sub

    Private Sub BindListMat()
        Try
            If CDate(toDate(range1.Text.Trim)) > CDate(toDate(range2.Text.Trim)) Then
                showMessage("Period 2 must be more than Period 1 !", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Please check Period value", 2)
            Exit Sub
        End Try

        sSql = "SELECT 'False' AS checkvalue, i.itemoid,i.itemcode, i.itemdesc, i.itemcode,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' End JenisNya,i.itemgroupoid,'Buah' satuan3, i.merk,i.stockflag,i.personoid,i.itemflag FROM QL_mstitem i Where itemoid IN (Select itemoid from QL_trnorderdtl d INNER JOIN QL_trnordermst mst ON d.trnordermstoid=mst.ordermstoid AND d.branch_code=mst.branch_code INNER JOIN QL_mstcust c ON c.custoid=mst.trncustoid AND c.branch_code=mst.branch_code Where convert(char(10),mst.trnorderdate,121) Between '" & Format(CDate(toDate(range1.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(range2.Text)), "yyyy-MM-dd") & "'"
        If SoNo.Text <> "" Then
            If DDLSONo.SelectedValue = "NomerSO" Then
                Dim sSono() As String = Split(SoNo.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " mst.orderno LIKE '%" & Tchar(sSono(c1)) & "%'"
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            Else
                Dim sSono() As String = Split(SoNo.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " mst.ordermstoid = " & ToDouble(sSono(c1))
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If 
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        Dim typeSO As String = ""
        For C1 As Integer = 0 To cbType.Items.Count - 1
            If cbType.Items(C1).Value <> "All" And cbType.Items(C1).Selected = True Then
                typeSO &= "'" & cbType.Items(C1).Value & "',"
            End If
        Next

        If typeSO <> "" Then
            typeSO = Left(typeSO, typeSO.Length - 1)
            sSql &= "And mst.typeSO IN (" & typeSO & ")"
        End If
        sSql &= ") ORDER BY itemdesc"
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showPrint(ByVal tipe As String)
        Try
            Label2.Text = ""
            CrystalReportViewer1.Visible = True
            Dim sWhere As String = "", namaPDF As String = "", typeSO As String = "", vSql As String = ""

            Try
                If CDate(toDate(range1.Text.Trim)) > CDate(toDate(range2.Text.Trim)) Then
                    showMessage("Period 2 must be more than Period 1 !", 2)
                    Exit Sub
                End If
            Catch ex As Exception
                showMessage("Please check Period value", 2)
                Exit Sub
            End Try

            sWhere = " Where convert(char(10),mst.trnorderdate,121) Between '" & Format(CDate(toDate(range1.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(range2.Text)), "yyyy-MM-dd") & "'"

            If ddltype.SelectedValue <> "Summary" Then
                If ItemName.Text <> "" Then
                    Dim sMatcode() As String = Split(ItemName.Text, ";")
                    vSql = " AND ("
                    For c1 As Integer = 0 To sMatcode.Length - 1
                        vSql &= " mat.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                        If c1 < sMatcode.Length - 1 Then
                            vSql &= " OR"
                        End If
                    Next
                    vSql &= ")"
                    sWhere &= vSql
                End If
            End If

            If SoNo.Text <> "" Then
                If DDLSONo.SelectedValue = "NomerSO" Then
                    Dim sSono() As String = Split(SoNo.Text, ";")
                    vSql = " AND ("
                    For c1 As Integer = 0 To sSono.Length - 1
                        vSql &= " mst.orderno LIKE '%" & Tchar(sSono(c1)) & "%'"
                        If c1 < sSono.Length - 1 Then
                            vSql &= " OR "
                        End If
                    Next
                    vSql &= ")"
                Else
                    Dim sSono() As String = Split(SoNo.Text, ";")
                    vSql = " AND ("
                    For c1 As Integer = 0 To sSono.Length - 1
                        vSql &= " mst.ordermstoid = " & ToDouble(sSono(c1))
                        If c1 < sSono.Length - 1 Then
                            vSql &= " OR "
                        End If
                    Next
                    vSql &= ")"
                End If
                sWhere &= vSql
            End If

            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                vSql = " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    vSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        vSql &= " OR "
                    End If
                Next
                vSql &= ")"
                sWhere &= vSql
            End If

            If rbSO.Checked = True Then
                If ddltype.SelectedValue = "Summary" Then
                    vReport.Load(Server.MapPath("~\Report\SalesReport_Sum.rpt"))
                    namaPDF = "Sales_Report_(Sum)_"
                Else
                    vReport.Load(Server.MapPath("~\Report\SalesReport_Dtl.rpt"))
                    namaPDF = "Sales_Report_(Dtl)_"
                End If
            Else
                vReport.Load(Server.MapPath("~\Report\rptStatus_SO_DO.rpt"))
                namaPDF = "Status_SO_DO_"
            End If  
            sWhere &= IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", " ", " And mat.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")

            If PersonOid.Text.Trim <> "" Then
                sWhere &= "And mst.salesoid = " & Integer.Parse(PersonOid.Text) & ""
            End If

            sWhere &= IIf(ddlStatus.SelectedItem.Text = "All", " ", " and mst.trnorderstatus = " & ddlStatus.SelectedValue & " ")
            sWhere &= IIf(DDLcurr.SelectedValue = "ALL", " ", "And mst.currencyoid=" & DDLcurr.SelectedValue & " ")
            sWhere &= IIf(taxable.SelectedValue = "ALL", " ", " And mst.ordertaxtype='" & taxable.SelectedValue & "'")
            'sWhere &= IIf(ddlSOtype.SelectedValue = "ALL", " ", " And mst.typeSO='" & ddlSOtype.SelectedValue & "' ")
            sWhere &= IIf(DDLjenisSO.SelectedValue = "ALL", "", " And mst.trnordertype='" & DDLjenisSO.SelectedValue & "'")
            sWhere &= IIf(cbSR.Checked, " AND mst.flagSR = 'Y'", "")

            For C1 As Integer = 0 To cbType.Items.Count - 1
                If cbType.Items(C1).Value <> "All" And cbType.Items(C1).Selected = True Then
                    typeSO &= "'" & cbType.Items(C1).Value & "',"
                End If
            Next

            If typeSO <> "" Then
                typeSO = Left(typeSO, typeSO.Length - 1)
                sWhere &= "And mst.typeSO IN (" & typeSO & ")"
            End If

            If DDLcbg.SelectedValue <> "ALL" Then
                sWhere &= "AND mst.branch_code='" & DDLcbg.SelectedValue & "'"
            End If

            If statusDelivery.SelectedValue <> "ALL" Then
                If statusDelivery.SelectedValue = "COMPLETE" Then
                    sWhere &= " And upper(dtl.trnorderdtlstatus)='COMPLETED'"
                Else
                    sWhere &= " And upper(dtl.trnorderdtlstatus)<>'COMPLETED'"
                End If
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)

            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("startperiod", toDate(range1.Text))
            vReport.SetParameterValue("endperiod", toDate(range2.Text))
            If rbSO.Checked = True Then
                vReport.SetParameterValue("titledef", "")
            End If
            vReport.SetParameterValue("filterSO", IIf(sooid.Text.Trim = "", "ALL", sono.Text))
            vReport.SetParameterValue("filterGroup", IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", "ALL GROUP", FilterDDLGrup.SelectedItem.Text))

            vReport.SetParameterValue("filterItem", "")

            vReport.SetParameterValue("filterCustomer", "")
            vReport.SetParameterValue("filterTaxable", IIf(taxable.SelectedValue = "ALL", " ", taxable.SelectedValue))
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "view" Then
                CrystalReportViewer1.DisplayGroupTree = False
                'CRVPenerimaanBarang.SeparatePages = False
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptSalesreport.aspx?awal=true")
            Else
                'vReport.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptSalesreport.aspx?awal=true")
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub BindListSO()
        Try
            If CDate(toDate(range1.Text.Trim)) > CDate(toDate(range2.Text.Trim)) Then
                showMessage("Period 2 must be more than Period 1 !", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Please check Period value", 2)
            Exit Sub
        End Try
        Dim typeSO As String = ""
        sSql = "SELECT 'False' AS checkvalue, mst.ordermstoid trnrequestitemoid, Convert(Varchar(20),mst.trnorderdate,103) trnrequestitemdate, mst.orderno trnrequestitemno, mst.branch_code branchcode, cb.gendesc, mst.trnorderstatus trnrequestitemstatus, mst.trnordernote trnrequestitemnote, typeSO FROM QL_trnordermst mst INNER JOIN QL_mstgen cb ON cb.gencode=mst.branch_code AND cb.gengroup='CABANG' Where convert(char(10),mst.trnorderdate,121) Between '" & Format(CDate(toDate(range1.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(range2.Text)), "yyyy-MM-dd") & "' " & IIf(DDLcbg.SelectedValue = "ALL", " ", " And mst.branch_code='" & DDLcbg.SelectedValue & "'") & " "
        If cbSR.Checked Then
            sSql &= " AND mst.flagSR = 'Y'"
        End If

        For C1 As Integer = 0 To cbType.Items.Count - 1
            If cbType.Items(C1).Value <> "ALL" And cbType.Items(C1).Selected = True Then
                typeSO &= "'" & cbType.Items(C1).Value & "',"
            End If
        Next

        If typeSO <> "" Then
            typeSO = Left(typeSO, typeSO.Length - 1)
            sSql &= "And mst.typeSO IN (" & typeSO & ")"
        End If

        sSql &= " ORDER BY ordermstoid"
        Session("TblSO") = cKon.ambiltabel(sSql, "QL_trnordermst")
    End Sub 
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            'Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xnomeja As String = Request.QueryString("nomeja")
            Dim xoutletoid As String = Session("outletoid")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            'Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptSalesreport.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang 
        End If
        Page.Title = CompnyName & " - Sales Report"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            InitCBL() : fDDLBranch()
            DDLcbg_SelectedIndexChanged(Nothing, Nothing)
            If Session("branch_id") <> "10" Then
                ibexcel.Visible = False : BTNpRINT.Visible = False
                trcabang.Visible = True : DDLcbg.Enabled = False
                DDLcbg.CssClass = "inpTextDisabled"
            Else
                ibexcel.Visible = True : BTNpRINT.Visible = True
                trcabang.Visible = True
            End If 
            initDDL() : SalesDDL()
            Session("showReport") = Nothing
            ddltype_SelectedIndexChanged(sender, e)
            range1.Text = Format(GetServerTime(), "01/MM/yyyy")
            range2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If 
    End Sub

    Protected Sub BTNpRINT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("pdf")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Session("showReport") = "False" : vReport.Close() : Label2.Text = ""
        Response.Redirect("rptSalesreport.aspx?awal=true")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        showPrint("excel")
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        'BindDataListItem()
        'gvListForecast.Visible = True
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5"
            gvListMat.PageSize = CInt(tbData.Text)
            CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If

    End Sub  

    Protected Sub ddltype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltype.SelectedIndexChanged
        If ddltype.SelectedValue = "Summary" Then
            FilterDDLGrup.Visible = False ': FilterDDLSubGrup.Visible = True
            Label3.Visible = False : ItemName.Visible = False
            btnSearchItem.Visible = False : btnEraseItem.Visible = False
            ItemLbl.Visible = False : Label7.Visible = False
            Label9.Visible = False : Label10.Visible = False
        Else
            FilterDDLGrup.Visible = True ': FilterDDLSubGrup.Visible = True
            Label3.Visible = True : ItemName.Visible = True
            btnSearchItem.Visible = True : btnEraseItem.Visible = True
            ItemLbl.Visible = True : Label7.Visible = True
            Label9.Visible = True : Label10.Visible = True
        End If
    End Sub  

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        showPrint("view") 
    End Sub 

    Protected Sub rbPO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSO.CheckedChanged, rbSODO.CheckedChanged
        If rbSO.Checked = True Then
            TD2.Visible = True
            TD3.Visible = True
            Label16.Visible = True
            cbSR.Checked = False
        Else
            TD2.Visible = False
            TD3.Visible = False
            Label16.Visible = False
            trcurr.Visible = False
            cbSR.Checked = False
        End If
        Session("diprint") = "False"
    End Sub

    Protected Sub DDLcbg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcbg.SelectedIndexChanged
        'SalesDDL()
    End Sub

    Protected Sub sPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sPerson.Click
        BindPerson()
    End Sub

    Protected Sub PersonGv_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles PersonGv.PageIndexChanging
        PersonGv.PageIndex = e.NewPageIndex
        BindPerson()
    End Sub

    Protected Sub PersonGv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PersonGv.SelectedIndexChanged
        PersonOid.Text = PersonGv.SelectedDataKey.Item("personoid")
        TxtPinjamNo.Text = PersonGv.SelectedDataKey.Item("personname")
        PersonGv.Visible = False
    End Sub

    Protected Sub eRasePerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eRasePerson.Click
        PersonOid.Text = "" : TxtPinjamNo.Text = ""
        PersonGv.Visible = False
    End Sub
#End Region

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("view")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)

        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
    End Sub 

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        Dim sPlus As String = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"

        If stockFlag.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag='" & stockFlag.SelectedValue & "'"
        End If

        'sPlus &= " AND itemflag='" & ddlStatus1.SelectedValue & "'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 

    Protected Sub btnALLSelectItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnALLSelectItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Maaf, Anda belum pilih katalog..!!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedItem.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Mohon maaf, data tidak ditemukan..!! !"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If ItemName.Text <> "" Then
                            ItemName.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            ItemName.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        ItemName.Text = ""
    End Sub

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing
            gvListSO.DataSource = Nothing : gvListSO.DataBind()
            CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        SoNo.Text = ""
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If SoNo.Text <> "" Then
                            If DDLSONo.SelectedValue = "NomerSO" Then
                                If dtView(C1)("trnrequestitemno") <> "" Then
                                    SoNo.Text &= ";" + vbCrLf + dtView(C1)("trnrequestitemno")
                                End If
                            Else
                                SoNo.Text &= ";" + vbCrLf + dtView(C1)("trnrequestitemoid").ToString
                            End If
                        Else
                            If DDLSONo.SelectedValue = "NomerSO" Then
                                If dtView(C1)("trnrequestitemno") <> "" Then
                                    SoNo.Text &= dtView(C1)("trnrequestitemno")
                                End If
                            Else
                                SoNo.Text &= dtView(C1)("trnrequestitemoid").ToString
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "- Maaf, Silahkan pilih data dulu..!!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "- Maaf, Silahkan pilih data dulu..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnrequestitemoid=" & dtTbl.Rows(C1)("trnrequestitemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Maaf, Silahkan pilih nomer transaksi..!!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnrequestitemoid=" & dtTbl.Rows(C1)("trnrequestitemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing
            gvListCust.DataSource = Nothing : gvListCust.DataBind()
            CProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        CProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    CProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub 

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSO")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = ""
    End Sub

    Protected Sub cbCheckAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCheckAll.CheckedChanged
        If cbCheckAll.Checked = True Then
            For Each listItem As ListItem In cbType.Items
                listItem.Selected = True
            Next 
        Else
            For Each listItem As ListItem In cbType.Items
                listItem.Selected = False
            Next 
        End If
    End Sub

    Protected Sub cbType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbType.SelectedIndexChanged
        Dim strcabang As Int32 = 0
        For C1 As Integer = 0 To cbType.Items.Count - 1
            If cbType.Items(C1).Selected = True Then
                strcabang += 1
            End If
        Next
    End Sub
End Class
