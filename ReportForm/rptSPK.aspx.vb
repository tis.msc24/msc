'Programmer: Kharisma | 29.04.2010
'Update: Kharisma | 29.06.2010
'       - tambahkan update panel pada tab1 tabcontainer
'Update: Kharisma | 30.06.2010
'       - remark cc1, cc2, ubah tag cc1,cc2 dengan ajaxToolkit pada halaman aspx
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Report_SPK
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim cKoneksi As New Koneksi
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim folderReport As String = "~/Report/"
#End Region

#Region "Procedure"

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal MyReportDocument As ReportDocument)
        Dim myTables As Tables = MyReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Sub showPrint(ByVal tipe As String)
        Dim errorFlag As Boolean = False
        Dim errMessage As String = ""
        Dim crConnInfo As New ConnectionInfo
        'spkplanstart.Text = Session("spkplanstart")
        'spkplanend.Text = Session("spkplanend")
        ''spkgroup.Text = Session("spkgroup")
        'spkstatus.Text = Session("spkstatus")

        If spkplanstart.Text <> "" And spkplanend.Text <> "" Then

        End If
        Try
            Dim dat1 As Date = CDate(toDate(spkplanend.Text))
            Dim dat2 As Date = CDate(toDate(spkplanstart.Text))

            If dat2 < CDate("01/01/1900") Then
                showMessage("Tanggal awal tidak valid", CompnyName & " - WARNING") : Exit Sub
            End If
            If dat1 < CDate("01/01/1900") Then
                showMessage("Tanggal akhir tidak valid", CompnyName & " - WARNING") : Exit Sub
            End If
            If dat2 > dat1 Then
                showMessage("Periode 1 harus lebih kecil dari periode 2", CompnyName & " - WARNING") : Exit Sub
            End If

        Catch ex As Exception
            showMessage("Format tanggal tidak sesuai!", CompnyName & " - WARNING")
            Exit Sub
        End Try

        'If toDate(spkplanend.Text) < toDate(spkplanstart.Text) Then
        '    showMessage("Tanggal Akhir harus lebih besar dari Tanggal Awal !<BR>", CompnyName & " - WARNING")
        '    Exit Sub
        'End If

        Try
            report = New ReportDocument
            report.Load(Server.MapPath("~\Report\rptSPK.rpt"))

            Dim sWhere As String = ""

            'sWhere &= " where sm.spkplanstart>='" & toDate(spkplanstart.Text) & "' AND sm.spkplanend<='" & toDate(spkplanend.Text) & "' "
            sWhere &= " where convert(date,sm.spkplanstart,101) between '" & CDate(toDate(spkplanstart.Text)) & "' AND '" & CDate(toDate(spkplanend.Text)) & "' "
            If spkstatus.SelectedItem.Text <> "ALL" Then
                sWhere &= " AND sm.spkstatus LIKE '%" & spkstatus.SelectedValue & "%' "
            End If

            Session("showReport") = True
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
                System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            'CrystalReportViewer1.DisplayGroupTree = False
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("dPeriode", spkplanstart.Text & " - " & spkplanend.Text)
            report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            'CrystalReportViewer1.ReportSource = report
            'CrystalReportViewer1.SeparatePages = True
            If tipe = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose() 'Session("no") = Nothing
            Else
                If tipe = "PDF" Then
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                    report.Close() : report.Dispose() 'Session("no") = Nothing
                Else
                    CrystalReportViewer1.DisplayGroupTree = False
                    CrystalReportViewer1.ReportSource = report
                    'CrystalReportViewer1.SeparatePages = False
                End If
            End If
            'report.Close() : report.Dispose()
            'Response.Redirect("~\reportForm\rptSPK.aspx?awal=true")

        Catch ex As Exception
            showMessage(ex.Message, CompnyName & " - WARNING")
            Exit Sub
        End Try

    End Sub

    Private Sub Cabangddl()
        Dim sCabang As String = ""
        If kacabCek("ReportForm/rptservicefinal.aspx", Session("UserID")) = False Then
            If Session("branch_id") <> "01" Then
                sCabang &= " AND gencode='" & Session("branch_id") & "'"
            Else
                sCabang = ""
            End If
        End If
        sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='MSC' And g.gengroup='CABANG' " & sCabang & ""
        FillDDL(DDLcabang, sSql)
    End Sub

    'Public Sub BindData(ByVal sCompCode As String)


    '    'untuk spkplanstart spkplanend
    '    Dim ConvertedDate1, ConvertedDate2 As Date
    '    Dim st1, st2 As Boolean
    '    Try
    '        ConvertedDate1 = CDate(spkplanstart.Text.Substring(3, 2) + "-" + spkplanstart.Text.Remove(2) + "-" + spkplanstart.Text.Remove(0, 6)).ToString("MM/dd/yyyy hh:mm tt")
    '        st1 = True
    '    Catch ex As Exception
    '        st1 = False
    '    End Try

    '    Try
    '        ConvertedDate2 = CDate(spkplanend.Text.Substring(3, 2) + "-" + spkplanend.Text.Remove(2) + "-" + spkplanend.Text.Remove(0, 6)).ToString("MM/dd/yyyy hh:mm tt")
    '        st2 = True
    '    Catch ex As Exception
    '        st2 = False
    '    End Try



    'Dim mySqlDA As New SqlDataAdapter(sSql, conn)
    'Dim objDs As New DataSet
    'mySqlDA.Fill(objDs, "data")

    '    'GVtrnspkmain.DataSource = objDs.Tables("data")
    '    'GVtrnspkmain.DataBind()
    'End Sub

#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Server.Transfer("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptSPK.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - SPK Main"

        If Not Page.IsPostBack Then
            spkplanstart.Text = Format(Now, "01" & "/MM/yyyy")
            spkplanend.Text = Format(Now, "dd/MM/yyyy")
            'spkplanstart.Text = CDate(Now).ToString("dd/MM/yyyy")
            'spkplanend.Text = CDate(Now).ToString("dd/MM/yyyy")
            spkstatus.SelectedIndex = 0
            Cabangddl()
        End If
        If Session("showReport") = True Then
            showPrint("")
        End If
    End Sub

    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
    '    Session("spkplanstart") = spkplanstart.Text
    '    Session("spkplanend") = spkplanend.Text
    '    'Session("spkgroup") = spkgroup.Text
    '    Session("spkstatus") = spkstatus.Text
    '    BindData(CompnyCode)
    'End Sub

    'Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
    '    Session.Remove("spkplanstart")
    '    Session.Remove("spkplanend")
    '    'Session.Remove("spkgroup")
    '    Session.Remove("spkstatus")
    '    BindData(CompnyCode)
    'End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, False)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click

        Dim errorFlag As Boolean = False
        Dim errMessage As String = ""

        Try
            report = New ReportDocument
            report.Load(Server.MapPath("~\Report\printSPK.rpt"))
            
            If toDate(spkplanend.Text) < toDate(spkplanstart.Text) Then
                errMessage &= "End date can't be less than start date!<BR>"
                errorFlag = True
            End If

            Dim sWhere As String = ""

            sWhere &= " AND sm.spkplanstart>='" & toDate(spkplanstart.Text) & "' AND sm.spkplanend<='" & toDate(spkplanend.Text) & "' "
            If spkstatus.SelectedItem.Text = "ALL" Then
            Else
                sWhere &= " AND sm.spkstatus LIKE '%" & spkstatus.SelectedValue & "%' "
            End If


            ' report.SetParameterValue("cmpcode", CompnyCode)
            report.SetParameterValue("sWhere", sWhere)
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("DB-Server"), System.Configuration.ConfigurationManager.AppSettings("DB-Name"))
            report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            report.PrintOptions.PaperSize = PaperSize.PaperA4

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "PP Status")
        Catch ex As Exception
            showMessage(ex.Message, "PT. Sekawan Inti Pratama - WARNING")
            Exit Sub
        End Try
    End Sub

    'Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
    '    If report Is Nothing = False Then
    '        If report.IsLoaded = False Then
    '            report.Dispose()
    '            report.Close()
    '        End If
    '    End If
    'End Sub

    Protected Sub btnReport_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("showReport") = False
        showPrint("")
    End Sub

    Protected Sub btnExcel_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("EXCEL")
        'Dim errorFlag As Boolean = False
        'Dim errMessage As String = ""
        'spkplanstart.Text = Session("spkplanstart")
        'spkplanend.Text = Session("spkplanend")
        ''spkgroup.Text = Session("spkgroup")
        'spkstatus.Text = Session("spkstatus")

        'Try
        '    report = New ReportDocument
        '    report.Load(Server.MapPath("~\Report\rptSPK.rpt"))
        '    Dim crConnInfo As New ConnectionInfo
        '    With crConnInfo
        '        .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
        '        .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
        '        .IntegratedSecurity = True
        '    End With
        '    SetDBLogonForReport(crConnInfo, report)

        '    'Dim ConvertedDate1, ConvertedDate2 As Date
        '    'Dim st1, st2 As Boolean
        '    'Try
        '    '    ConvertedDate1 = CDate(spkplanstart.Text.Substring(3, 2) + "-" + spkplanstart.Text.Remove(2) + "-" + spkplanstart.Text.Remove(0, 6)).ToString("MM/dd/yyyy hh:mm tt")
        '    '    st1 = True
        '    'Catch ex As Exception
        '    '    st1 = False
        '    'End Try

        '    'If st1 And st2 Then
        '    If toDate(spkplanend.Text) < toDate(spkplanstart.Text) Then
        '        errMessage &= "End date can't be less than start date!<BR>"
        '        errorFlag = True
        '        'Else
        '        'ConvertedDate2 = ConvertedDate2.AddDays(1)
        '        'Session("ConvertedDate1") = ConvertedDate1
        '        'Session("ConvertedDate2") = ConvertedDate2
        '    End If
        '    'Else
        '    'errMessage &= "Invalid SPK plan dates!<BR>"
        '    'errorFlag = True
        '    'End If

        '    'If errorFlag Then
        '    '    spkstatus.Text = "UNPOST"
        '    '    showMessage(errMessage, "PT. Sekawan Inti Pratama - WARNING")
        '    '    Exit Sub
        '    'End If

        '    Dim sWhere As String = ""
        '    sWhere &= " AND spkplanstart>='" & spkplanstart.Text & "' AND spkplanend<='" & spkplanend.Text & "' "
        '    'sWhere &= " AND spkgroup LIKE '" & spkgroup.Text & "' "
        '    sWhere &= " AND spkstatus LIKE '" & spkstatus.Text & "' "

        '    If CompnyCode <> "" Then
        '        sSql = "SELECT spkoid, spkno, spkbasedon, spknote, spkstatus FROM QL_trnspk where upper(cmpcode) LIKE '%" & CompnyCode.ToUpper & "%' " & sWhere & "  ORDER BY spkoid"
        '    Else
        '        sSql = "SELECT spkoid, spkno, spkbasedon, spknote, spkstatus FROM QL_trnspk where 1=1 " & sWhere & "  ORDER BY spkoid"
        '    End If

        '    'vReport.SetParameterValue("trnpono", sSql)
        '    report.SetParameterValue("cmpcode", CompnyCode)
        '    report.SetParameterValue("spkplanstart", sSql)
        '    report.SetParameterValue("spkplanend", sSql)
        '    'report.SetParameterValue("spkgroup", Session("spkgroup"))
        '    report.SetParameterValue("spkstatus", sSql)
        '    CrystalReportViewer1.ReportSource = report
        '    CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("DB-Server"), System.Configuration.ConfigurationManager.AppSettings("DB-Name"))

        '    Response.Buffer = False
        '    Response.ClearContent()
        '    Response.ClearHeaders()
        '    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "PP Status")
        '    'Catch ex As Exception


        '    'End Try

        '    'Try
        '    '    report.Load(Server.MapPath(folderReport & "rptSPK.rpt"))
        '    '    CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        '    '        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        '    report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

        '    '    Response.Buffer = False

        '    '    Response.ClearContent()
        '    '    Response.ClearHeaders()
        '    '    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "_" & Format(GetServerTime(), "dd_MM_yy"))
        '    'report.Close() : report.Dispose()
        '    'Response.Redirect("~\reportForm\rptSPK.aspx?awal=true")
        'Catch ex As Exception
        '    showMessage(ex.Message, "PT. Sekawan Inti Pratama - WARNING")
        '    Exit Sub
        'End Try

    End Sub

#End Region

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptSPK.aspx?awal=true")
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("PDF")
    End Sub
End Class
