<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnNotaHR.aspx.vb" Inherits="Accounting_trnNotaHR" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server"> 
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Nota Piutang Gantung"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="1" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%"><ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="TabPanel1"><ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<DIV style="TEXT-ALIGN: left"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 70px" align=left><asp:CheckBox id="cbPeriod" runat="server" Text="Periode" __designer:wfdid="w405"></asp:CheckBox></TD><TD align=left>:</TD><TD style="FONT-SIZE: 9pt" align=left><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w406"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w407"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w408"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w409"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w410"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 9pt"><TD align=left><asp:Label id="Label20" runat="server" Text="Filter" __designer:wfdid="w411"></asp:Label></TD><TD align=left>:</TD><TD vAlign=baseline align=left colSpan=1><asp:DropDownList id="DDlfilter" runat="server" CssClass="inpText" __designer:wfdid="w412"><asp:ListItem Value="trnnotahroid">No Draft.</asp:ListItem>
<asp:ListItem Value="nm.trnnotahrno">No. Nota HR</asp:ListItem>
<asp:ListItem Value="s.suppname">Supplier</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="no" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w413"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 9pt"><TD id="TD3" align=left runat="server" Visible="false">Type</TD><TD align=left runat="server" Visible="false"></TD><TD id="TD4" vAlign=baseline align=left colSpan=1 runat="server" Visible="false"><asp:DropDownList id="payType" runat="server" CssClass="inpText" Font-Bold="False" OnSelectedIndexChanged="payflag_SelectedIndexChanged" __designer:wfdid="w414"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList> <asp:Label id="cabang" runat="server" Visible="False" __designer:wfdid="w420"></asp:Label> <asp:Label id="poststatx" runat="server" Text="NORMAL" Visible="False" __designer:wfdid="w419"></asp:Label></TD></TR><TR style="FONT-SIZE: 9pt"><TD align=left><asp:Label id="Label6" runat="server" Text="Status" __designer:wfdid="w415"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=1><asp:DropDownList id="statuse" runat="server" CssClass="inpText" __designer:wfdid="w416"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="In Process">In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Middle" __designer:wfdid="w417"></asp:ImageButton> <asp:ImageButton id="btnViewAll" onclick="btnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Middle" __designer:wfdid="w418"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 9pt"><TD align=left colSpan=3><asp:GridView id="gvmstcost" runat="server" Width="100%" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnnotahroid,cmpcode" AllowPaging="True" __designer:wfdid="w425">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
<asp:CheckBox id="cbPosting" runat="server" Enabled="<%# GetEnabled() %>" ToolTip="<%# GetCheckedOid() %>" Checked='<%# eval("selected") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:HyperLinkField DataNavigateUrlFields="trnnotahroid" DataNavigateUrlFormatString="~\Transaction\trnnotahr.aspx?oid={0}" DataTextField="trnnotahroid" HeaderText="Draft No." SortExpression="trnnotahroid">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnnotahrno" HeaderText="No. Nota HR">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasiamt" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:ImageButton id="PrintHdr" onclick="PrintHdr_Click" runat="server" ImageUrl="~/Images/print.gif" ToolTip="<%# GetPrintHdr() %>"></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnnotahrstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" VerticalAlign="Top" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Not Found Data Click Button Find Or View All" Visible="False"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" CssClass="gvrow" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:CalendarExtender id="ce2" runat="server" TargetControlID="txtPeriode1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1" __designer:wfdid="w426"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ce3" runat="server" TargetControlID="txtPeriode2" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2" __designer:wfdid="w427"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" TargetControlID="txtPeriode1" Mask="99/99/9999" MaskType="Date" CultureName="en-US" __designer:wfdid="w428"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" TargetControlID="txtPeriode2" Mask="99/99/9999" MaskType="Date" CultureName="en-US" __designer:wfdid="w429"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></TD></TR><TR><TD>&nbsp;</TD></TR></TBODY></TABLE></DIV>
</ContentTemplate>
    <triggers>
<asp:PostBackTrigger ControlID="gvmstcost"></asp:PostBackTrigger>
</triggers>
</asp:UpdatePanel> 
</ContentTemplate>
<HeaderTemplate>
<STRONG><SPAN style="FONT-SIZE: 9pt"><asp:Image id="Image2" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image>
    List Of Nota Piutang Gantung :.</SPAN></STRONG>&nbsp; 
</HeaderTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="TabPanel2"><ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel5" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="Label21" runat="server" Text="Cabang" Visible="False" __designer:wfdid="w347"></asp:Label></TD><TD style="COLOR: #000099" align=left colSpan=6><asp:DropDownList id="outlet" runat="server" Width="144px" CssClass="inpText" Visible="False" __designer:wfdid="w348" AutoPostBack="True"></asp:DropDownList> <asp:Label id="Label7" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w349" Font-Italic="False"></asp:Label> <asp:Label id="lblIO" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="" Visible="False" __designer:wfdid="w350"></asp:Label> <asp:Label id="i_u2" runat="server" Font-Bold="True" ForeColor="Red" Text="New Data" Visible="False" __designer:wfdid="w351"></asp:Label></TD></TR><TR style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #000099"><TD align=left>Status</TD><TD align=left colSpan=6><asp:TextBox id="lblPOST" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w352" Enabled="False"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lblcashbankno" runat="server" Text="No. HR" Visible="False" __designer:wfdid="w353"></asp:Label> <asp:Label id="drafno" runat="server" Text="Draft No" __designer:wfdid="w354"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="RealisasiNo" runat="server" Width="138px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w355" Enabled="False"></asp:TextBox> <asp:Label id="notaHROid" runat="server" __designer:wfdid="w356"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="Label12" runat="server" Text="Tanggal" __designer:wfdid="w357"></asp:Label> <asp:Label id="Label22" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w358"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="Realisasidate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w359" Enabled="False"></asp:TextBox> <asp:ImageButton id="imbCBDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w360" Enabled="False"></asp:ImageButton> <asp:Label id="CutofDate" runat="server" Visible="False" __designer:wfdid="w361"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD1" align=left runat="server" Visible="false"><asp:Label id="Label9" runat="server" Text="Type Realisasi" __designer:wfdid="w362"></asp:Label></TD><TD id="TD2" align=left colSpan=6 runat="server" Visible="false"><asp:DropDownList id="payflag" runat="server" Width="135px" CssClass="inpText" Font-Bold="False" OnSelectedIndexChanged="payflag_SelectedIndexChanged" __designer:wfdid="w363" AutoPostBack="True"><asp:ListItem Value="HUTANG">PELUNASAN HUTANG</asp:ListItem>
<asp:ListItem Text="CASH" Value="CASH"></asp:ListItem>
<asp:ListItem>BANK</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
<asp:ListItem>0</asp:ListItem>
</asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="LblAkun" runat="server" Text="Akun COA" Visible="False" __designer:wfdid="w364"></asp:Label></TD><TD align=left colSpan=6><asp:DropDownList id="DdlAkun" runat="server" CssClass="inpText" Font-Bold="False" __designer:wfdid="w365"></asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="NoPI" runat="server" Width="45px" Text="Supplier" __designer:wfdid="w366"></asp:Label> <asp:Label id="Label1" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w367"></asp:Label></TD><TD align=left colSpan=6><asp:TextBox id="suppname" runat="server" Width="250px" CssClass="inpText " __designer:wfdid="w368"></asp:TextBox> <asp:ImageButton id="btnCariSupplier" onclick="btnCariSupplier_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w369"></asp:ImageButton> <asp:ImageButton id="EraseSuppbtn" onclick="EraseSuppbtn_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w370"></asp:ImageButton> <asp:Label id="supplierOid" runat="server" Text="0" Visible="False" __designer:wfdid="w371"></asp:Label> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w372" Enabled="False"></asp:DropDownList></TD><TD align=left colSpan=6><asp:GridView id="gvPO" runat="server" Width="85%" ForeColor="#333333" OnSelectedIndexChanged="gvPO_SelectedIndexChanged" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="suppcode,suppname,suppoid" AllowPaging="True" __designer:wfdid="w373" OnPageIndexChanging="gvPO_PageIndexChanging" PageSize="5">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Note</TD><TD align=left colSpan=6><asp:TextBox id="NoteHdr" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w374" MaxLength="100"></asp:TextBox>&nbsp; </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=7><asp:Label id="I_Udtl" runat="server" Font-Bold="True" ForeColor="Red" Text="New Detail" __designer:wfdid="w375"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="Label16" runat="server" Text="No. PI" __designer:wfdid="w376"></asp:Label> <asp:Label id="Label3" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w377"></asp:Label></TD><TD align=left><asp:TextBox id="trnbelino" runat="server" Width="150px" CssClass="inpText " __designer:wfdid="w378"></asp:TextBox> <asp:ImageButton id="btnCariPI" onclick="BtnCariPI_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w379"></asp:ImageButton> <asp:ImageButton id="ErasePI" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w380"></asp:ImageButton> <asp:Label id="trnbelioid" runat="server" Text="0" Visible="False" __designer:wfdid="w381"></asp:Label></TD><TD align=left><asp:Label id="Label4" runat="server" Text="Supplier" __designer:wfdid="w382"></asp:Label></TD><TD align=left><asp:TextBox id="suppnota" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w383" ReadOnly="True"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left></TD><TD align=left colSpan=3><asp:GridView id="GvVoucher" runat="server" Width="51%" ForeColor="#333333" OnSelectedIndexChanged="GvVoucher_SelectedIndexChanged" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelino,trnbelidate,voucher,sisavoucher,usevoucher,suppname" AllowPaging="True" __designer:wfdid="w384" OnPageIndexChanging="GvVoucher_PageIndexChanging" PageSize="5" OnRowDataBound="GvVoucher_RowDataBound">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelimstoid" HeaderText="oid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No. PI">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucher" HeaderText="Amt Voucher PI">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="usevoucher" HeaderText="Amt Voucher PI Use">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sisavoucher" HeaderText="Amt Voucher PI Sisa">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Total&nbsp;Voucher PI</TD><TD align=left><asp:TextBox id="AmtPI" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w385" ReadOnly="True"></asp:TextBox></TD><TD align=left>Amt Voucher PI Use</TD><TD align=left><asp:TextBox id="amtuse" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w386" ReadOnly="True"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Voucher PI Balance</TD><TD align=left><asp:TextBox id="amtbalance" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w387" ReadOnly="True"></asp:TextBox></TD><TD align=left>Amount Nota</TD><TD align=left><asp:TextBox id="AmtVdtl" runat="server" Width="150px" CssClass="inpText " __designer:wfdid="w388" AutoPostBack="True" OnTextChanged="AmtVdtl_TextChanged">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:CheckBox id="cbSelisih" runat="server" Text="Selisih Nota" Visible="False" __designer:wfdid="w389" Enabled="False"></asp:CheckBox></TD><TD align=left><asp:DropDownList id="ddlSelisih" runat="server" Width="110px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w390" Enabled="False"><asp:ListItem Value="-">Kurang dari Nota</asp:ListItem>
<asp:ListItem Value="+">Lebih dari Nota</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="ddlSelisihCOA" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w391" AutoPostBack="True"></asp:DropDownList></TD><TD align=left><asp:TextBox id="totalselisih" runat="server" Width="150px" CssClass="inpText " Visible="False" __designer:wfdid="w392" AutoPostBack="True" OnTextChanged="AmtVdtl_TextChanged">0.00</asp:TextBox></TD><TD align=left><asp:TextBox id="selisih" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w393" AutoPostBack="True" OnTextChanged="AmtVdtl_TextChanged"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=1>Biaya Tambahan</TD><TD align=left colSpan=1><asp:DropDownList id="addcostacctgoid" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w394">
            </asp:DropDownList></TD><TD align=left colSpan=1>Amount Biaya</TD><TD align=left colSpan=1><asp:TextBox id="addcostamt" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w395" AutoPostBack="True"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=1>Note</TD><TD align=left colSpan=1><asp:TextBox id="cashbanknote" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w396" MaxLength="100"></asp:TextBox></TD><TD align=right colSpan=1><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" __designer:wfdid="w397"></asp:ImageButton></TD><TD align=left colSpan=1><asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w398"></asp:ImageButton> <asp:Label id="labelseq" runat="server" Visible="False" __designer:wfdid="w399"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=7><DIV style="OVERFLOW-Y: scroll; WIDTH: 950px; HEIGHT: 400px; BACKGROUND-COLOR: beige"><asp:GridView id="GVvOucherDtl" runat="server" Width="99%" ForeColor="#333333" OnSelectedIndexChanged="GVvOucherDtl_SelectedIndexChanged" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="seqdtl,trnbelimstoid,trnbelino,suppname,amtvoucher,trnnotahrdtlnote" __designer:wfdid="w400" OnRowDataBound="GVvOucherDtl_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="seqdtl" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimstoid" HeaderText="trnbelimstoid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No. PI">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalvoucher" HeaderText="Total Voucher PI">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtvoucher" HeaderText="Amt Nota">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="addcostamt" HeaderText="Amt Biaya">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="flagselisih" HeaderText="Flag">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgoid" HeaderText="acctgoid" Visible="False"></asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" CssClass="Important" Font-Size="Small" Text="No Data Detail"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD8" align=left runat="server" Visible="true"><asp:Label id="Label11" runat="server" Width="121px" Font-Size="Small" Font-Bold="True" Text="- Total Nota" __designer:wfdid="w401"></asp:Label> </TD><TD id="TD6" align=left colSpan=6 runat="server" Visible="true"><asp:Label id="lblTampung" runat="server" Font-Size="Small" Font-Bold="True" Text="0.00" __designer:wfdid="w402"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD5" align=left runat="server" Visible="true"><asp:Label id="Label2" runat="server" Width="120px" Font-Size="Small" Font-Bold="True" Text="- Total Biaya" __designer:wfdid="w403"></asp:Label></TD><TD id="TD7" align=left colSpan=6 runat="server" Visible="true"><asp:Label id="SisaAmtDtl" runat="server" Font-Size="Small" Font-Bold="True" Text="0.00" __designer:wfdid="w404"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=7>Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w405"></asp:Label>&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w406"></asp:Label><BR /><ajaxToolkit:CalendarExtender id="cecashbankdate" runat="server" TargetControlID="realisasidate" Format="dd/MM/yyyy" PopupButtonID="imbCBDate" __designer:wfdid="w407"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MEERealisasidate" runat="server" TargetControlID="Realisasidate" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w408" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbAmtVoucher" runat="server" TargetControlID="AmtVdtl" __designer:wfdid="w409" ValidChars=",.1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbSelisih" runat="server" TargetControlID="selisih" __designer:wfdid="w410" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbAddCost" runat="server" TargetControlID="addcostamt" __designer:wfdid="w411" ValidChars="1234567890,.">
</ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=7><asp:ImageButton id="btnsave" onclick="btnsave_Click" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w412"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w413"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnSendAp" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w414"></asp:ImageButton> <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" Visible="False" __designer:wfdid="w415"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w416"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" Visible="False" __designer:wfdid="w417"></asp:ImageButton> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=7><asp:UpdatePanel id="UpdatePanel6" runat="server" __designer:wfdid="w418"><ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" __designer:wfdid="w419" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting" __designer:wfdid="w420"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" CellPadding="4" __designer:wfdid="w421" OnRowDataBound="gvakun_RowDataBound">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w422">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblPosting2" __designer:wfdid="w423"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False" __designer:wfdid="w424"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="TEXT-ALIGN: center" align=left colSpan=7><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w425" AssociatedUpdatePanelID="UpdatePanel5" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w426"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        
</ContentTemplate>
<HeaderTemplate>
                            <span style="font-size: 9pt">
                             <strong><span>
                                 <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                 Form Of Nota Piutang Gantung :.</span></strong></span>&nbsp;
                            
                        
</HeaderTemplate>
</ajaxToolkit:TabPanel>
</ajaxToolkit:TabContainer></td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
            </td>
        </tr>
    </table>
</asp:Content>