Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptstock2
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Function GetTime() As Date
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = "" : Dim TglNya As Date = CDate(toDate(dateAwal.Text))
        Dim dateValue As Date

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "SELECT DATEADD(MONTH,-1,CONVERT(datetime,'" & TglNya & "')) AS AddDate;"
        xCmd.CommandText = sSql
        Try
            dateValue = xCmd.ExecuteScalar
        Catch ex As Exception
            dateValue = Now
        End Try

        conn.Close()
        Return dateValue
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedure"
    Public Sub ShowPrint(ByVal showtype As String)
        Dim sWhere As String = "", dWhere As String = "", namaPDF As String = "", trnDate As String = "", PeriodNya As String = "", PeriodAkhir As String = ""
        dWhere &= " Where i.cmpcode='" & CompnyCode & "'"
        If Type.SelectedValue = "Summary" Then
            sWhere &= "Where con.cmpcode='" & CompnyCode & "' "
        End If

        If JenisBarangDDL.SelectedValue <> "ALL" Then
            sWhere &= " AND i.stockflag='" & JenisBarangDDL.SelectedValue & "'"
            dWhere &= " AND i.stockflag='" & JenisBarangDDL.SelectedValue & "'"
        End If
        Dim sQel As String = "", adr As String = ""
        'For C1 As Integer = 0 To arCode.Length - 1
        '    If arCode(C1) <> "" Then
        '        sCodeIn &= "'" & arCode(C1) & "',"
        '    End If
        'Next

        'If sCodeIn <> "" Then
        '    dWhere &= " AND i.itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
        'End If

        If ItemName.Text <> "" Then
            Dim sMatcode() As String = Split(ItemName.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sMatcode.Length - 1
                sSql &= " i.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                If c1 < sMatcode.Length - 1 Then
                    sSql &= " OR"
                End If
            Next
            sSql &= ")"
        End If
        dWhere &= sSql
        sWhere &= sSql

        If PIC.Checked = True Then
            If ddlPIC.SelectedValue <> "ALL" Then
                dWhere &= " And i.personoid = " & ddlPIC.SelectedValue & ""
            End If
        End If

        'If sCodeIn <> "" Then
        '    sWhere &= " AND itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
        'End If

        If ddlLocation.SelectedValue.ToUpper <> "ALL LOCATION" Then
            sWhere &= " AND con.mtrlocoid=" & ddlLocation.SelectedValue & ""
        End If

        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("Period report is invalid..!", 2)
            Exit Sub
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Period 2 must be more than Period 1!", 2)
            Exit Sub
        End If

        Try
            vReport = New ReportDocument
            If type.SelectedValue = "Summary" Then

                sWhere &= " AND con.trndate >='" & toDate(dateAwal.Text) & " 0:0:0' AND con.trndate <='" & toDate(dateAkhir.Text) & " 23:59:00'"
                If showtype = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptStockSum2Exl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptStockSum2.rpt"))
                End If
                PeriodAkhir = GetDateToPeriodAcctg(CDate(toDate(dateAkhir.Text)))
                PeriodNya = GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text)))
                vReport.SetParameterValue("PeriodAkhir", PeriodAkhir) 
                vReport.SetParameterValue("sWhere", sWhere)
                vReport.SetParameterValue("sWherePeriod", GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))))
                namaPDF = "StockSumAdmin_"

            Else 'DETAIL'
                sWhere &= " AND con.trndate >='" & toDate(dateAwal.Text) & " 0:0:0' AND con.trndate <='" & toDate(dateAkhir.Text) & " 23:59:00'"
                If showtype = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptStockAdminDtlNewExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptStockAdminDtlNew.rpt"))
                End If
                trnDate = toDate(dateAwal.Text)
                vReport.SetParameterValue("sWherePeriod", GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))))
                vReport.SetParameterValue("lDate", trnDate)
                vReport.SetParameterValue("sPeriode", dateAwal.Text & " - " & dateAkhir.Text)
                vReport.SetParameterValue("sWhere", sWhere)
                vReport.SetParameterValue("userid", Session("UserID").ToString.ToUpper)
                namaPDF = "Stock_Detail_"
                'End If
            End If

            PeriodNya = GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text)))
            trnDate = toDate(dateAwal.Text)
            vReport.SetParameterValue("trnDate", toDate(dateAwal.Text))
            vReport.SetParameterValue("dWhere", dWhere) 
            vReport.SetParameterValue("CompanyName", "MULTI SARANA COMPUTER")
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            If showtype = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "view" Then
                Session("diprint") = "True"
                crv.DisplayGroupTree = False
                crv.ReportSource = vReport
            End If

        Catch ex As Exception
            vReport.Close() : vReport.Dispose()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                If Session("branch_id") = "01" Then
                    FillDDL(dd_branch, sSql)
                    dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
                    dd_branch.SelectedValue = "SEMUA BRANCH"
                Else
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(dd_branch, sSql)
                End If
            Else
                FillDDL(dd_branch, sSql)
                dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
                dd_branch.SelectedValue = "SEMUA BRANCH"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dd_branch, sSql)
            dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            dd_branch.SelectedValue = "SEMUA BRANCH"
        End If
    End Sub

    Public Sub InitDDL()
        Dim fCb As String = ""
        If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
            fCb = "AND cb.gencode='" & dd_branch.SelectedValue & "'"
        End If
        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc Gudang FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid Inner Join QL_mstgen cb ON cb.genoid=a.genother2 AND cb.gengroup='Cabang' Where a.cmpcode='" & CompnyCode & "' AND a.gengroup = 'LOCATION' AND a.genother5 NOT IN ('SERVICE','TITIPAN','RUSAK','ROMBENG') AND b.gengroup = 'WAREHOUSE' " & fCb & " UNION ALL Select Distinct gd.genoid,'GUDANG'+' - '+gd.gendesc+' '+cb.gendesc Gudang From QL_conmtr con Inner Join QL_mstgen gd ON gd.genoid=con.mtrlocoid AND gd.gengroup='LOCATION' AND gd.gencode='EXPEDISI' Inner JOin QL_mstgen cb ON cb.gencode=con.branch_code AND cb.gengroup='CABANG' Where gd.cmpcode='" & CompnyCode & "' " & fCb & ""
        FillDDL(ddlLocation, sSql)

        If Session("branch_id") = "10" Or Session("branch_id") = "01" Then
            ddlLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"
        End If
    End Sub

    Private Sub InitDdlPic()
        Dim upd As String = ""
        sSql = "SELECT personoid, personname FROM QL_mstperson prsn INNER JOIN QL_MSTPROF prof ON prof.USERNAME = prsn.PERSONNAME WHERE prof.cmpcode = '" & CompnyCode & "' and prsn.PERSONSTATUS in (select genoid from ql_mstgen Where gengroup = 'JOBPOSITION' AND GENDESC like '%AMP%')"
        If Session("branch_id") = "10" Then
            If Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                FillDDL(ddlPIC, sSql)
                ddlPIC.Items.Add(New ListItem("ALL", "ALL"))
                ddlPIC.SelectedValue = "ALL" : PIC.Enabled = True
            Else
                sSql &= " AND prof.userid='" & Session("UserID") & "'"
                FillDDL(ddlPIC, sSql)
                PIC.Enabled = False : PIC.Checked = True
            End If
        Else
            FillDDL(ddlPIC, sSql)
            ddlPIC.Items.Add(New ListItem("ALL", "ALL"))
            ddlPIC.SelectedValue = "ALL" : PIC.Enabled = True
        End If
    End Sub

    Public Sub showPrintExcel(ByVal name As String)
        Dim sWhere As String = ""

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Period 2 must be more than Period 1!", 2)
            Exit Sub
        End If
        Response.Clear()

        Dim sSql As String = ""


        Response.AddHeader("content-disposition", "inline;filename=stock.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"
        Dim swherebranch As String = IIf(dd_branch.SelectedValue = "SEMUA BRANCH", " ", " and con.branch_code=" & dd_branch.SelectedValue & " ")
        Dim swhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and mtrlocoid=" & ddlLocation.SelectedValue & " ")
        Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, "  and  i.itemoid=" & itemoid.Text, "  ")

        Dim swhereawal2 As String = "   "
        If type.SelectedValue = "Detail" Then
            swhereawal2 = " c.updtime >='" & Format(CDate(toDate(dateAwal.Text)), "MM") & "/01/" & Format(CDate(toDate(dateAwal.Text)), "yyyy") & " 0:0:0' AND c.updtime <'" & toDate(dateAwal.Text) & " 0:0:0' "
        Else 'summary
            swhereawal2 = " WHERE c.periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))) & "'  "
        End If
        Dim swherecommand As String = ""
        If type.SelectedValue = "Detail" Then
            swhereawal2 = " where c.updtime BETWEEN '" & toDate(dateAwal.Text) & " 0:0:0' AND '" & toDate(dateAkhir.Text) & " 23:59:59'  "
        Else 'summary
            swherecommand = " WHERE c.periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))) & "'   "
        End If


        sSql = " select  g.gendesc lokasi, p.personname SPG,i.itemdesc Nama_barang, g2.gendesc satuan1,saldoawal_1,QTYIN_1,QTYOUT_1,qtyAdjIn_1, " & _
               "qtyadjout_1,saldoakhir_1,g3.gendesc satuan2,saldoawal_2,QTYIN_2,QTYOUT_2,qtyadjin_2,qtyadjout_2, saldoakhir_2," & _
               "g4.gendesc satuan3,saldoawal_3, QTYIN_3,QTYOUT_3,qtyadjin_3, qtyadjout_3, saldoakhir_3,c.updtime " & _
               "from ql_crdmtr c inner join ql_mstitem i on i.itemoid=c.refoid  and c.refname='ql_mstitem' " & _
"inner join ql_mstperson p on i.personoid=p.personoid  " & _
"inner join ql_mstgen g on g.genoid=mtrlocoid  " & _
               "inner join ql_mstgen g2 on g2.genoid=satuan1  " & _
               "inner join ql_mstgen g3 on g3.genoid=satuan2 " & _
               "inner join ql_mstgen g4 on g4.genoid=satuan3 " & swherecommand & " " & swhereloc & " " & swherebranch & " " & swhereitem & " ORDER BY g.gendesc,i.itemcode "

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        'Response.Write(ConvertDtToTDF(dt))
        conn.Close()

        Response.End()

    End Sub

    Private Sub BindListMat()
        Try
            sSql = "Select * from (Select 'False' Checkvalue,i.itemcode, i.itemdesc,i.itemoid, g.gendesc satuan3, i.merk,i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya from ql_mstitem i inner join ql_mstgen g on g.genoid=satuan1 and g.gengroup='ITEMUNIT' and itemflag='AKTIF') dt"
            Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstitem")
            Session("TblMat") = dt 
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptStock2.aspx")
        End If
        Page.Title = CompnyName & " - Stock Inc. Value Report "
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            fDDLBranch() : dd_branch_SelectedIndexChanged(Nothing, Nothing)
            initddl() : InitDdlPic()

            Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
            If lastPeriodene = "?" Then
                lastPeriodene = 0
                dateAwal.Text = Format(GetServerTime(), "dd/MM/yyyy")
            Else
                dateAwal.Text = Format(GetServerTime(), "dd/MM/yyyy")
                '"01/" & lastPeriodene.Substring(5).Trim & "/" & lastPeriodene.Substring(0, 4).Trim
            End If
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                showMessage("Data Barang tidak ada..!!", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        'crv.ReportSource = Nothing
        'Session("diprint") = "False"
        'itemname.Text = gvItem.SelectedDataKey.Item(1)
        'itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        'gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        ItemName.Text = "" : itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles type.SelectedIndexChanged
        'Session("diprint") = "False"
        'If Type.SelectedValue = "Summary" Then
        '    Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
        '    dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy") '"01/" & lastPeriodene.Substring(5).Trim & "/" & lastPeriodene.Substring(0, 4).Trim
        '    dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        'Else ' detail
        '    dateAwal.Visible = True : dateAkhir.Visible = True
        'End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click 
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvItem.DataSource = Nothing : gvItem.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("rptStock2.aspx?awal=true")
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowPrint("view")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        ShowPrint("excel")
    End Sub

    Protected Sub ibPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibPDF.Click
        ShowPrint("pdf")
    End Sub

    Protected Sub dd_branch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dd_branch.SelectedIndexChanged
        InitDDL()
    End Sub

    Protected Sub crv_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crv.Navigate
        UpdateCheckedMat() : ShowPrint("view")
    End Sub

    Protected Sub crv_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crv.Search
        ShowPrint("view")
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If JenisBarangDDL.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag ='" & JenisBarangDDL.SelectedValue & "'"
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind()
            gvItem.Visible = True
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If ItemName.Text <> "" Then
                            ItemName.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            ItemName.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    showMessage("- Maaf, Silahkan pilih katalog, dengan beri centang pada kolom pilih..<BR>", 2)
                    Exit Sub
                End If
            End If
        Else
            showMessage("- Maaf, Silahkan pilih katalog, dengan beri centang pada kolom pilih..<BR>", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected Katalog data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
                Session("WarningListMat") = "Selected Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub
#End Region
End Class
