<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstPricelist.aspx.vb" Inherits="Master_mstPricelist" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Master Pricelist"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div style="width: 100%; text-align: left">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table>
                                        <tr>
                                            <td align="left" class="Label">
                                                Filter &nbsp; :</td>
                                            <td align="left">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="promeventcode">Code</asp:ListItem>
                                                    <asp:ListItem Value="promeventname">Pricelist Reason</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" />
                                            </td>
                                            <td align="left">
                                                <asp:ImageButton ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="height: 10px">
                                                Status :</td>
                                            <td align="left" style="height: 10px">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="Outstanding">Outstanding</asp:ListItem>
                                                    <asp:ListItem Value="Updated">Updated</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td align="left" style="height: 10px">
                                            </td>
                                            <td align="left" style="height: 10px">
                                            </td>
                                            <td align="left" style="height: 10px">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <fieldset id="field1" style="height: 250px; width: 99%; text-align: left; border-top-style: none;
                                    border-right-style: none; border-left-style: none; border-bottom-style: none;">
                                    <div id='divTblData'>
                                    </div>
                                    <div style="overflow-y: scroll; height: 100%; width: 100%;">
                                        <asp:GridView ID="GVMst" runat="server" CellPadding="4" AutoGenerateColumns="False"
                                            Width="98%" ForeColor="#333333">
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="promoid" DataNavigateUrlFormatString="mstPricelist.aspx?oid={0}"
                                                    DataTextField="promeventcode" HeaderText="Code">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="promeventname" HeaderText="Pricelist Reason">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="postdate" HeaderText="Post Date">
                                                    <HeaderStyle Width="100px" />
                                                    <ItemStyle Width="100px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promflag" HeaderText="Status">
                                                    <ItemStyle Width="100px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promnote" HeaderText="Note" />
                                            </Columns>
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </fieldset>
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List Master Pricelist :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:Label id="Label48" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Master Pricelist" Font-Underline="False" __designer:wfdid="w1"></asp:Label><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 14px" class="Label" align=left width=150><asp:Label id="promoid" runat="server" Visible="False" __designer:wfdid="w2"></asp:Label></TD><TD style="HEIGHT: 14px" class="Label" align=left>&nbsp;</TD><TD style="HEIGHT: 14px" class="Label" align=left>&nbsp;</TD><TD style="HEIGHT: 14px" class="Label" align=left>&nbsp;&nbsp; </TD><TD style="HEIGHT: 14px" class="Label" align=left>&nbsp;</TD><TD style="HEIGHT: 14px" class="Label" align=left>&nbsp;</TD></TR><TR><TD class="Label" align=left>Code&nbsp;<asp:Label id="Label28" runat="server" CssClass="Important" Text="*" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="promeventcode" runat="server" CssClass="inpText" MaxLength="20" __designer:wfdid="w4"></asp:TextBox></TD><TD class="Label" align=left>Date Posting&nbsp;</TD><TD class="Label" align=left><asp:TextBox id="promstartdate" runat="server" Width="75px" CssClass="inpText" MaxLength="6" __designer:wfdid="w5"></asp:TextBox>&nbsp;<asp:ImageButton id="ibstartdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w7"></asp:Label></TD><TD class="Label" align=left>&nbsp;</TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Pricelist&nbsp;Reason&nbsp;<asp:Label id="Label55" runat="server" CssClass="Important" Text="*" __designer:wfdid="w8"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="promeventname" runat="server" Width="200px" CssClass="inpText" MaxLength="50" __designer:wfdid="w9"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="note" runat="server" Width="556px" CssClass="inpText" MaxLength="150" __designer:wfdid="w10"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="Upload Excel" __designer:wfdid="w11"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:FileUpload id="fuXls" runat="server" CssClass="inpText" __designer:wfdid="w12"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnXls" onclick="btnXls_Click" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton> </TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w14"></asp:Label></TD><TD class="Label" align=left>&nbsp;&nbsp;<ajaxToolkit:CalendarExtender id="ceStartDate" runat="server" TargetControlID="promstartdate" __designer:wfdid="w15" PopupButtonID="ibstartdate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="promstartdate" __designer:wfdid="w16" CultureName="id-ID" UserDateFormat="DayMonthYear" Mask="99/99/9999" MaskType="Date" Enabled="true"></ajaxToolkit:MaskedEditExtender> </TD><TD class="Label" align=left>&nbsp;<asp:Label id="Label57" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w17"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="promfinishdate" runat="server" Width="75px" CssClass="inpText" Visible="False" __designer:wfdid="w18"></asp:TextBox>&nbsp;<asp:ImageButton id="ibfinishdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w19"></asp:ImageButton>&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" Visible="False" __designer:wfdid="w20"></asp:Label><ajaxToolkit:CalendarExtender id="ceFinishDate" runat="server" TargetControlID="promfinishdate" __designer:wfdid="w21" PopupButtonID="ibfinishdate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="promfinishdate" __designer:wfdid="w22" CultureName="id-ID" UserDateFormat="DayMonthYear" Mask="99/99/9999" MaskType="Date" Enabled="true"></ajaxToolkit:MaskedEditExtender> </TD><TD class="Label" align=left></TD><TD class="Label" align=left rowSpan=1></TD></TR><TR><TD class="Label" align=left>&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w23"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="promstarttime" runat="server" Width="75px" CssClass="inpText" MaxLength="5" Visible="False" __designer:wfdid="w24"></asp:TextBox> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender5" runat="server" TargetControlID="promstarttime" __designer:wfdid="w25" CultureName="en-US" Mask="99:99:99" MaskType="Time" Enabled="true" UserTimeFormat="TwentyFourHour" AcceptAMPM="True"></ajaxToolkit:MaskedEditExtender> </TD><TD class="Label" align=left>&nbsp;<asp:Label id="Label6" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w26"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="promfinishtime" runat="server" Width="58px" CssClass="inpText" MaxLength="5" Visible="False" __designer:wfdid="w27"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="promfinishtime" __designer:wfdid="w28" Mask="99:99:99" MaskType="Time" UserTimeFormat="TwentyFourHour" AcceptAMPM="True"></ajaxToolkit:MaskedEditExtender> </TD><TD class="Label" align=left></TD><TD class="Label" align=left rowSpan=1></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=left rowSpan=1><asp:DropDownList id="promtype" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w29" AutoPostBack="True" OnSelectedIndexChanged="promtype_SelectedIndexChanged"><asp:ListItem>DISC % (ITEM ONLY)</asp:ListItem>
<asp:ListItem>Buy 1 Get 1</asp:ListItem>
<asp:ListItem>Buy x Get y</asp:ListItem>
<asp:ListItem>Buy nX get nY</asp:ListItem>
</asp:DropDownList> </TD><TD class="Label" align=left></TD><TD><asp:DropDownList id="membercardrole" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w30"><asp:ListItem>None</asp:ListItem>
<asp:ListItem>Plus</asp:ListItem>
<asp:ListItem>Not Define</asp:ListItem>
<asp:ListItem>Choose</asp:ListItem>
</asp:DropDownList> <asp:Label id="status" runat="server" Visible="False" __designer:wfdid="w31"></asp:Label></TD><TD></TD><TD></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=5 rowSpan=1><asp:CheckBoxList id="cbDay" runat="server" __designer:dtid="281474976710816" CellPadding="5" Visible="False" __designer:wfdid="w32" RepeatDirection="Horizontal" RepeatColumns="4"><asp:ListItem Selected="True" Value="1" __designer:dtid="281474976710817">Sunday</asp:ListItem>
<asp:ListItem Selected="True" Value="2" __designer:dtid="281474976710818">Monday</asp:ListItem>
<asp:ListItem Selected="True" Value="3" __designer:dtid="281474976710819">Tuesday</asp:ListItem>
<asp:ListItem Selected="True" Value="4" __designer:dtid="281474976710820">Wednesday</asp:ListItem>
<asp:ListItem Selected="True" Value="5" __designer:dtid="281474976710821">Thursday</asp:ListItem>
<asp:ListItem Selected="True" Value="6" __designer:dtid="281474976710822">Friday</asp:ListItem>
<asp:ListItem Selected="True" Value="7" __designer:dtid="281474976710823">Saturday</asp:ListItem>
</asp:CheckBoxList></TD></TR><TR><TD class="Label" align=left>&nbsp;<asp:Label id="Label10" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w33"></asp:Label></TD><TD class="Label" align=left colSpan=5 rowSpan=1><asp:TextBox id="autoresponse" runat="server" Width="300px" Height="30px" CssClass="inpText" MaxLength="150" Visible="False" __designer:wfdid="w34" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>&nbsp;<asp:Label id="Label14" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w35"></asp:Label></TD><TD class="Label" align=left colSpan=5 rowSpan=1><asp:TextBox id="generalSMSTemplate" runat="server" Width="300px" Height="30px" CssClass="inpText" MaxLength="150" Visible="False" __designer:wfdid="w36" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=left rowSpan=1><asp:Label id="qtysmsterkirim" runat="server" Visible="False" __designer:wfdid="w37"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="lastsmssend" runat="server" Visible="False" __designer:wfdid="w38"></asp:Label></TD><TD class="Label" align=left>&nbsp;</TD><TD class="Label" align=left>&nbsp; </TD></TR><TR><TD class="Label" align=left colSpan=6><BR /><BR /></TD></TR></TBODY></TABLE><asp:Label id="Label8" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Pricelist Item Detail" Font-Underline="False" __designer:wfdid="w39"></asp:Label><BR /><BR /><asp:Label id="Label9" runat="server" Text="Per : " __designer:wfdid="w40" font-size="X-Small"></asp:Label> <asp:DropDownList id="ddlInsertType" runat="server" CssClass="inpText" __designer:wfdid="w41" AutoPostBack="true"><asp:ListItem>Item</asp:ListItem>
<asp:ListItem Value="GROUP">Group Item</asp:ListItem>
<asp:ListItem Value="SUBGROUP">Sub Group Item</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Label id="lblmerk" runat="server" Text="Merk : " __designer:wfdid="w42" font-size="X-Small"></asp:Label> <asp:TextBox id="Merkfilter" runat="server" Width="121px" CssClass="inpText" __designer:wfdid="w43"></asp:TextBox>&nbsp;<asp:ImageButton id="printExcel" onclick="printExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton><BR /><BR /><TABLE style="WIDTH: 100%" id="tblGroup" runat="server"><TBODY><TR><TD class="Label" colSpan=10>.: Per Group :. <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" TargetControlID="pergroupdisc" __designer:wfdid="w45" FilterType="Numbers">
                                                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender7" runat="server" TargetControlID="pergroupdisc2" __designer:wfdid="w46" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender8" runat="server" TargetControlID="pergroupdisc3" __designer:wfdid="w47" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left width=150>Per&nbsp;Group</TD><TD class="Label" align=left width=300><asp:DropDownList id="ddlGroup" runat="server" CssClass="inpText" __designer:wfdid="w48">
                                                    </asp:DropDownList></TD><TD class="Label" align=left width=300>Sub Group&nbsp;<asp:DropDownList id="ddlSubGroup2" runat="server" CssClass="inpText" __designer:wfdid="w2"></asp:DropDownList></TD><TD class="Label" align=left width=100>Disc Qty</TD><TD class="Label" align=left width=200><asp:TextBox id="pergroupdisc" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w49"></asp:TextBox> (%) </TD><TD class="Label" align=left width=200>Disc Prt</TD><TD class="Label" align=left width=200><asp:TextBox id="pergroupdisc2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w50"></asp:TextBox>&nbsp;(%) </TD><TD class="Label" align=left width=200>Disc Ecer</TD><TD class="Label" align=left width=200><asp:TextBox id="pergroupdisc3" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w51"></asp:TextBox>&nbsp;(%) </TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" id="tblSubGroup" runat="server" visible="false"><TBODY><TR><TD class="Label" colSpan=11>.: Per&nbsp;Sub Group :. <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" TargetControlID="persubgroupdisc" __designer:wfdid="w52" FilterType="Numbers">
                                                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender9" runat="server" TargetControlID="persubgroupdisc2" __designer:wfdid="w53" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender10" runat="server" TargetControlID="persubgroupdisc3" __designer:wfdid="w54" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left width=150><asp:Label id="Label15" runat="server" Width="105px" Text="Per Sub Group" __designer:wfdid="w55"></asp:Label></TD><TD class="Label" align=left width=300><asp:DropDownList id="ddlSubGroup" runat="server" CssClass="inpText" __designer:wfdid="w56">
                                                    </asp:DropDownList></TD><TD class="Label" align=left width=100>Disc Qty</TD><TD class="Label" align=left width=200><asp:TextBox id="persubgroupdisc" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w57"></asp:TextBox> (%) </TD><TD class="Label" align=left width=200>Disc Qty</TD><TD class="Label" align=left width=200><asp:TextBox id="persubgroupdisc2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w58"></asp:TextBox>&nbsp;(%) </TD><TD class="Label" align=left width=200>Disc Qty</TD><TD class="Label" align=left width=200><asp:TextBox id="persubgroupdisc3" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w59"></asp:TextBox>&nbsp;(%) </TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" id="tblMenu" runat="server" visible="false"><TBODY><TR><TD class="Label" colSpan=6>.: Per Menu :. <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="amountdisc1" __designer:wfdid="w60" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender4" runat="server" TargetControlID="Subsidi" __designer:wfdid="w61" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender5" runat="server" TargetControlID="amountdisc2" __designer:wfdid="w62" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender6" runat="server" TargetControlID="amountdisc3" __designer:wfdid="w63" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left width=150>Item&nbsp;1 <asp:Label id="Label11" runat="server" CssClass="Important" Text="*" __designer:wfdid="w64"></asp:Label></TD><TD class="Label" align=left width=300><asp:TextBox id="itemdesc1" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w65"></asp:TextBox> <asp:ImageButton id="imbFindMenu1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w66"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearMenu1" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w67"></asp:ImageButton> <asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w68"></asp:Label></TD><TD style="WIDTH: 100px" class="Label" align=left>Pricelist</TD><TD class="Label" align=left width=200><asp:TextBox id="Subsidi" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w69"></asp:TextBox>&nbsp;Amt</TD><TD class="Label" align=left>Merk</TD><TD class="Label" align=left><asp:TextBox id="txtmerk" runat="server" Width="70px" CssClass="inpTextDisabled" __designer:wfdid="w70" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left width=150>Disc&nbsp;Qty <asp:Label id="Label12" runat="server" CssClass="Important" Text="*" __designer:wfdid="w71"></asp:Label></TD><TD class="Label" align=left width=300><asp:TextBox id="amountdisc1" runat="server" Width="58px" CssClass="inpText" __designer:wfdid="w72"></asp:TextBox>&nbsp;(%) <asp:Label id="konversi1_2" runat="server" Visible="False" __designer:wfdid="w73"></asp:Label><asp:Label id="konversi2_3" runat="server" Visible="False" __designer:wfdid="w74"></asp:Label></TD><TD style="WIDTH: 100px" class="Label" align=left>Disc&nbsp;Prt <asp:Label id="Label1" runat="server" CssClass="Important" Text="*" __designer:wfdid="w75"></asp:Label>&nbsp; </TD><TD class="Label" align=left width=200><asp:TextBox id="amountdisc2" runat="server" Width="51px" CssClass="inpText" __designer:wfdid="w76"></asp:TextBox>&nbsp;(%)</TD><TD class="Label" align=left>Disc&nbsp;Ecer <asp:Label id="Label17" runat="server" CssClass="Important" Text="*" __designer:wfdid="w77"></asp:Label>&nbsp; </TD><TD class="Label" align=left><asp:TextBox id="amountdisc3" runat="server" Width="51px" CssClass="inpText" __designer:wfdid="w78"></asp:TextBox>&nbsp;(%)</TD></TR><TR><TD id="TD1" class="Label" align=left width=150 runat="server" Visible="false">Qty&nbsp;Item 1</TD><TD id="TD2" class="Label" align=left width=300 runat="server" Visible="false"><asp:DropDownList id="qty1" runat="server" Width="44px" CssClass="inpText" __designer:wfdid="w79"><asp:ListItem>1</asp:ListItem>
<asp:ListItem>2</asp:ListItem>
<asp:ListItem>3</asp:ListItem>
<asp:ListItem>4</asp:ListItem>
<asp:ListItem>5</asp:ListItem>
<asp:ListItem>7</asp:ListItem>
<asp:ListItem>8</asp:ListItem>
<asp:ListItem>9</asp:ListItem>
<asp:ListItem>10</asp:ListItem>
<asp:ListItem>11</asp:ListItem>
<asp:ListItem>12</asp:ListItem>
</asp:DropDownList>(Only promo buy&nbsp;X get&nbsp;Y / buy nX get nY)</TD><TD style="WIDTH: 100px" class="Label" align=left></TD><TD class="Label" align=left width=200></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD id="TD4" class="Label" align=left width=150 runat="server" Visible="false">Qty&nbsp;Item 2 <asp:Label id="Label13" runat="server" CssClass="Important" Text="*" __designer:wfdid="w80"></asp:Label>&nbsp;Item&nbsp;2 </TD><TD id="TD3" class="Label" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="qty2" runat="server" Width="44px" CssClass="inpText" __designer:wfdid="w81"><asp:ListItem>1</asp:ListItem>
<asp:ListItem>2</asp:ListItem>
<asp:ListItem>3</asp:ListItem>
<asp:ListItem>4</asp:ListItem>
<asp:ListItem>5</asp:ListItem>
<asp:ListItem>7</asp:ListItem>
<asp:ListItem>8</asp:ListItem>
<asp:ListItem>9</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="itemdesc2" runat="server" Width="200px" CssClass="inpText" MaxLength="6" __designer:wfdid="w82"></asp:TextBox><asp:ImageButton id="imbFindMenu2" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w83"></asp:ImageButton><asp:ImageButton id="imbClearMenu2" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w84"></asp:ImageButton><asp:Label id="itemoid2" runat="server" Visible="False" __designer:wfdid="w85"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6>&nbsp;</TD></TR><TR><TD class="Label" align=left colSpan=6>&nbsp;</TD></TR><TR><TD style="HEIGHT: 14px" class="Label" align=right colSpan=6>&nbsp;</TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" colSpan=6><asp:LinkButton id="LBAddMenu" runat="server" CausesValidation="False" Font-Size="X-Small" __designer:wfdid="w86">[ Add To List ]</asp:LinkButton><asp:Label id="lblmenuygada" runat="server" __designer:wfdid="w87"></asp:Label><BR /><BR /><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 250px"><asp:GridView id="gvPromoDtl" runat="server" Width="97%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w88" EmptyDataText="No Data" DataKeyNames="itemoid">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemdesc" HeaderText="Item 1" SortExpression="itemdesc">
<HeaderStyle Font-Size="X-Small" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Subsidi" HeaderText="Pricelist">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amountdisc" HeaderText="Disc Qty" SortExpression="amountdisc">
<HeaderStyle Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amountdisc2" HeaderText="Disc Prt">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amountdisc3" HeaderText="Disc Ecer">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc2" HeaderText="Item 2(just promo buy x get y/buy nX get nY)" SortExpression="itemdesc2" Visible="False">
<HeaderStyle Font-Size="X-Small" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbDelete" runat="server" Checked='<%# eval("CheckValue") %>' __designer:wfdid="w2"></asp:CheckBox>
</ItemTemplate>

<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle Width="10px"></HeaderStyle>

<ItemStyle Font-Bold="True" Width="10px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="lblmsg3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV><BR /><asp:ImageButton id="ibDelete" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="Right" __designer:wfdid="w89"></asp:ImageButton>&nbsp;<asp:Button id="postedallx" onclick="postedallx_Click" runat="server" CssClass="green" Font-Bold="True" Text="CHECK ALL" __designer:wfdid="w90"></asp:Button>&nbsp;<asp:Button id="postednotallx" onclick="postednotallx_Click" runat="server" CssClass="blue" Font-Bold="True" Text="UNCHECK ALL" __designer:wfdid="w91"></asp:Button>&nbsp;<asp:ImageButton id="preview" onclick="preview_Click" runat="server" ImageUrl="~/Images/preview.png" ImageAlign="AbsMiddle" __designer:wfdid="w92"></asp:ImageButton></TD></TR><TR><TD class="Label" align=right colSpan=6><FIELDSET style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV id="Div3">&nbsp;</DIV></FIELDSET> </TD></TR><TR><TD class="Label" align=right colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label16" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Promo In Outlet" Visible="False" Font-Underline="False" __designer:wfdid="w93"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=6>&nbsp;<asp:GridView id="gvPriceDtl" runat="server" Width="90%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" Visible="False" __designer:wfdid="w94">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="outletcode" HeaderText="Outlet Code">
<HeaderStyle Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outletdesc" HeaderText="Outlet Name">
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Available"><ItemTemplate>
<asp:CheckBox id="itemavailable" runat="server" __designer:wfdid="w8" OnCheckedChanged="itemavailable_CheckedChanged" checked='<%# returnAvailable(DataBinder.Eval(Container.DataItem, "itemavailable")) %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data tidak ditemukan !!"></asp:Label>
                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=6><BR /><BR /><BR /></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="printexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnXls"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="preview"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                            <table>
                                <tr>
                                    <td colspan="4" style="font-size: x-small; color: #585858">
                                        Last Update On
                                        <asp:Label ID="Updtime" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="#585858"></asp:Label>
                                        By
                                        <asp:Label ID="UpdUser" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="#585858"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:ImageButton ID="btnSave" runat="server" CausesValidation="False" CommandName="Update"
                                            ImageAlign="AbsMiddle" ImageUrl="~/Images/Save.png" />
                                        <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/Cancel.png" />
                                        <asp:ImageButton ID="BtnDelete" runat="server" CommandName="Delete" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/Delete.png" />
                                        <asp:ImageButton ID="posting" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/posting.png" Visible="False" />&nbsp;<asp:ImageButton
                                            ID="sendApproval" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/sendapproval.png"
                                            Visible="False" />
                                    </td>
                                </tr>
                            </table>
                            &nbsp;
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
<asp:Panel id="PanelUser" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w216" Visible="False" DefaultButton="btnSearchItem"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="lblListUser" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Item" __designer:wfdid="w217"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center">Item&nbsp;: <asp:TextBox id="filterMenuItem" runat="server" CssClass="inpText" __designer:wfdid="w218"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w219">
                                                                    </asp:ImageButton> <asp:ImageButton id="btnListItem" onclick="btnListItem_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w220"></asp:ImageButton> <asp:Label id="lblstate" runat="server" __designer:wfdid="w221" Visible="False"></asp:Label></TD></TR><TR><TD><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 300px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvMenu" runat="server" Width="98%" __designer:wfdid="w222" CellPadding="4" DataKeyNames="itemoid,itemshortdesc,pricelist,merk,konversi1_2,konversi2_3" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemshortdesc" HeaderText="Item" SortExpression="itemshortdesc">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="lblmsg3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="lbCloseMenu" runat="server" __designer:wfdid="w223">[Close]</asp:LinkButton></TD></TR><TR><TD style="TEXT-ALIGN: center">&nbsp;</TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MpeUser" runat="server" __designer:wfdid="w224" TargetControlID="BtnUser" PopupControlID="PanelUser" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListUser" Drag="True">
                                                </ajaxToolkit:ModalPopupExtender> <asp:Button id="BtnUser" runat="server" __designer:wfdid="w225" Visible="False"></asp:Button> 
</ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Master Pricelist :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False" DefaultButton="ImageButton4"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD vAlign=top><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" CssClass="Important"></asp:Label>&nbsp; </TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" PopupControlID="PanelValidasi" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False">
            </asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
