' DEBET NOTE :  -A/P : Max Amount = A/P Balance
'               +A/R : No Max Amount
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports ClassFunction

Partial Class debitnote
    Inherits System.Web.UI.Page

#Region "variabel"
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cRate As ClassRate()
    Dim cProc As New ClassProcedure
    Dim Report As ReportDocument
    Dim ckoneksi As New Koneksi

#End Region

#Region "Function"
    Private Function IsInputValid() As Boolean
        Dim sErr As String = ""
        Dim sError As String = ""
        If cust_supp_oid.Text = "" Then
            sError &= "- Please choose a Supplier/Customer !!<BR>"
        End If

        If refoid.Text = "" Then
            sError &= "- Please select invoice field !!<BR>"
        End If

        If ToDouble(amount.Text) <= 0 Then
            sError &= "- DN Amount must more than 0 !!<BR>"
        Else
            If reftype.SelectedValue = "AP" Then
                If ToDouble(amount.Text) > ToDouble(maxpayment.Text) Then
                    sError &= "- Amount must be equal or less than " & ToMaskEdit(ToDouble(maxpayment.Text), 4) & " !!<BR>"
                End If
            End If
        End If

        If CDate(toDate(tgl.Text)) <= CDate(toDate(CutofDate.Text)) Then
            sError &= "- DN Date must equal or more than CutoffDate, CutoffDate Date Is (" & CutofDate.Text & ") !! <BR>"
        End If

        If Tchar(note.Text).Length > 200 Then
            sError &= "- Maximum character for note is 200 !!<BR>"
        End If
        If tgl.Text = "" Then
            sError &= "- Please fill Date !!"
        Else
            If Not IsValidDate(tgl.Text, "dd/MM/yyyy", sErr) Then
                sError &= "- DATE is INVALID. " & sErr & " !!<BR>"
            Else
                If invoicedate.Text <> "" Then
                    If CDate(toDate(tgl.Text)) < CDate(toDate(invoicedate.Text)) Then
                        sError &= "- DN Date must equal or more than Invoice date,Invoice Date Is " & invoicedate.Text & "!!<BR>"
                    End If
                End If
            End If
        End If

        If coacredit.Items.Count = 0 Then
            If reftype.SelectedValue = "AR" Then
                sError &= "- Please set  VAR_DEBITNOTE_PIUTANG_CREDIT in master interface first!!<BR>"
            ElseIf reftype.SelectedValue = "AP" Then
                sError &= "- Please set  VAR_DEBITNOTE_HUTANG_CREDIT in master interface first!!<BR>"
            Else
                sError &= "- Please set  VAR_DEBITNOTE_NT_CREDIT in master interface first!!<BR>"
            End If
        End If
        If ToDouble(amount.Text) > 0 Then
            'CEK PERIODE AKTIF BULANAN
            'sSql = "Select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
            'If GetStrData(sSql) <> "" Then
            sSql = "Select top 1 periodacctg FROM QL_crdgl where glflag='CLOSE'"
            If GetPeriodAcctg(CDate(toDate(invoicedate.Text))) = GetStrData(sSql) Then
                sError &= "Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql)
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
            Exit Function
        End If
        Return True
    End Function 'OK

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(tgl1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(tgl2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(tgl1.Text)) > CDate(toDate(tgl2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function CheckValidPeriod(ByVal sDate As String) As Boolean
        Dim sErr As String = ""
        'Dim offDate As String = ""
        'sSql = "SELECT TOP 1 ISNULL(genother2,'') FROM QL_mstgen WHERE genstatus='ACTIVE' AND gengroup='CUTOFFDATE' AND genother3='" & CompnyCode & "'"
        'If GetStrData(sSql) = "?" Then
        '    sErr &= "- Please SET CUTT OFF DATE For Outlet (#" & DDLOutlet.SelectedItem.Text & "#)"
        'Else
        '    offDate = Format(CDate(GetStrData(sSql)), "dd/MM/yyyy")
        '    If CDate(toDate(sDate)) < CDate(toDate(offDate)) Then
        '        sErr &= "- TRANSACTION DATE must more or equal than CUT OFF DATE (" & offDate & ")!"
        '    End If
        'End If
        If ToDouble(amount.Text) > 0 Then
            '=========================
            'CEK PERIODE AKTIF BULANAN
            '=========================
            sSql = "SELECT DISTINCT ISNULL(periodacctg,'') FROM QL_crdgl Where glflag <> 'CLOSE'"
            If GetStrData(sSql) <> "" Then
                sSql = "select distinct left(isnull(periodacctg,''),4)+'-'+substring(isnull(periodacctg,''),6,2) FROM QL_crdgl where glflag='OPEN'"
                If GetPeriodAcctg(CDate(toDate(tgl.Text))) < GetStrData(sSql) Then
                    showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", 2)
                    Exit Function
                End If
            End If
        End If
        If sErr <> "" Then
            showMessage(sErr, 2)
            LblPosting.Text = "IN PROCESS"
            Return False
        End If

        Return True
    End Function
#End Region

#Region "Procedure"
    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\debitnote.aspx?awal=true")
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Sub generateNo()
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & DDLcabang.SelectedValue & "' AND GENGROUP='CABANG'")
        Dim sNo As String = "DN/" & cabang & "/" & Format(CDate(toDate(tgl.Text)), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(no,'" & sNo & "',''))),0)+1  FROM QL_DebitNote WHERE no LIKE '" & sNo & "%'"
        cnno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub 'OK

    Public Sub BindData(ByVal sqlPlus As String)
        If IsValidPeriod() Then
            sSql = "SELECT * FROM ( SELECT dn.cmpcode,dn.branch_code, dn.oid, dn.no, dn.tgl, dn.reftype, CASE WHEN dn.reftype='AP' THEN (SELECT s.suppname FROM QL_mstsupp s WHERE s.suppoid=dn.cust_supp_oid) ELSE (SELECT c.custname FROM QL_mstcust c WHERE c.custoid=dn.cust_supp_oid) END AS name, CASE WHEN dn.reftype='AP' THEN (SELECT ap.amttrans FROM QL_conap ap WHERE ap.refoid=dn.refoid AND ap.cmpcode=dn.cmpcode AND ap.amttrans>0 AND ap.reftype='QL_trnbelimst') WHEN dn.reftype='NT' then (SELECT ar.amttrans FROM QL_conar ar WHERE ar.cmpcode=dn.cmpcode AND ar.refoid=dn.refoid AND ar.amttrans>0 AND ar.reftype='QL_trnbiayaeksmst') ELSE (SELECT ar.amttrans FROM QL_conar ar WHERE ar.cmpcode=dn.cmpcode AND ar.refoid=dn.refoid AND ar.amttrans>0 AND ar.reftype='QL_trnjualmst') END AS invoice, dn.amount, dn.dnstatus, dn.note FROM QL_DebitNote dn ) AS dt WHERE dt.dnstatus<>'DELETE' " & sqlPlus & " ORDER BY tgl DESC, oid DESC"
            Session("TblMst") = GetDataTable(sSql, "QL_DebitNote")
            GVmstgen.DataSource = Session("TblMst")
            GVmstgen.DataBind()
            lblViewInfo.Visible = False
        End If
    End Sub 'NOT OK

    Public Sub FillTextBox(ByVal sCmpcode As String, ByVal sOid As String)
        Try
            sSql = "SELECT * FROM (SELECT dn.cmpcode,dn.branch_code, dn.oid, dn.no, dn.refoid, dn.reftype, dn.tgl, s.suppoid, s.suppname, CASE WHEN dn.reftype='AP' THEN (SELECT bm.trnbelino FROM QL_trnbelimst bm WHERE bm.cmpcode=dn.cmpcode AND bm.trnbelimstoid=dn.refoid) END AS invoice, dn.amount, ap.amttrans, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND ap.cmpcode=ap2.cmpcode AND ap.refoid=ap2.refoid AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar, dn.coa_debet, dn.coa_credit, dn.note, dn.dnstatus, dn.updtime, dn.upduser, a.acctgcode, a.acctgdesc, ap.trnapdate trndate FROM QL_DebitNote dn INNER JOIN QL_mstsupp s ON dn.cust_supp_oid=s.suppoid AND dn.reftype='AP' INNER JOIN QL_conap ap ON ap.cmpcode=dn.cmpcode AND ap.refoid=dn.refoid AND ap.payrefoid=0 INNER JOIN QL_mstacctg a ON a.acctgoid=dn.coa_debet " & _
            "UNION ALL " & _
" SELECT dn.cmpcode,dn.branch_code, dn.oid, dn.no, dn.refoid, dn.reftype, dn.tgl, s.custoid suppoid, s.custname suppname, CASE WHEN dn.reftype='AR' THEN (SELECT bm.trnjualno FROM QL_trnjualmst bm WHERE bm.cmpcode=dn.cmpcode AND bm.trnjualmstoid=dn.refoid) END AS invoice, dn.amount, ap.amttrans, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar, dn.coa_debet, dn.coa_credit, dn.note, dn.dnstatus, dn.updtime, dn.upduser, a.acctgcode, a.acctgdesc, ap.trnardate trndate FROM QL_DebitNote dn INNER JOIN QL_mstcust s ON dn.cust_supp_oid=s.custoid AND dn.reftype='AR' INNER JOIN QL_conar ap ON ap.cmpcode=dn.cmpcode AND ap.refoid=dn.refoid AND ap.payrefoid=0 INNER JOIN QL_mstacctg a ON a.acctgoid=dn.coa_debet " & _
            "UNION ALL " & _
 "SELECT dn.cmpcode,dn.branch_code, dn.oid, dn.no, dn.refoid, dn.reftype, dn.tgl, s.custoid suppoid, s.custname suppname, CASE WHEN dn.reftype='NT' THEN (SELECT bm.trnbiayaeksno FROM ql_trnbiayaeksmst bm WHERE bm.cmpcode=dn.cmpcode AND bm.trnbiayaeksoid=dn.refoid) END AS invoice, dn.amount, ap.amttrans, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)) AS amtbayar, dn.coa_debet, dn.coa_credit, dn.note, dn.dnstatus, dn.updtime, dn.upduser, a.acctgcode, a.acctgdesc, ap.trnardate trndate FROM QL_DebitNote dn INNER JOIN QL_mstcust s ON dn.cust_supp_oid=s.custoid AND dn.reftype='NT' INNER JOIN QL_conar ap ON ap.cmpcode=dn.cmpcode AND ap.refoid=dn.refoid AND ap.payrefoid=0 and trnartype = 'EXP' INNER JOIN QL_mstacctg a ON a.acctgoid=dn.coa_credit ) AS dt " & _
            "WHERE dt.cmpcode='" & CompnyCode & "' AND dt.oid=" & sOid
            Dim dtData As DataTable = GetDataTable(sSql, "QL_DebitNote")
            If dtData.Rows.Count < 1 Then
                showMessage("Failed to load transaction data.", 2)
            Else
                DDLcabang.SelectedValue = dtData.Rows(0)("branch_code").ToString
                cabang.Text = dtData.Rows(0)("branch_code").ToString
                CompnyCode = dtData.Rows(0)("cmpcode").ToString
                cnoid.Text = dtData.Rows(0)("oid").ToString
                draft.Text = dtData.Rows(0)("oid").ToString
                tgl.Text = Format(dtData.Rows(0)("tgl"), "dd/MM/yyyy")
                cnno.Text = dtData.Rows(0)("no").ToString
                refoid.Text = dtData.Rows(0)("refoid").ToString
                reftype.SelectedValue = dtData.Rows(0)("reftype").ToString
                nama.Text = dtData.Rows(0)("suppname").ToString
                cust_supp_oid.Text = dtData.Rows(0)("suppoid").ToString
                InitCOACredit()
                coacredit.SelectedValue = dtData.Rows(0)("coa_credit").ToString
                coadebet.SelectedValue = dtData.Rows(0)("coa_debet").ToString
                amount.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("amount").ToString), 4)
                note.Text = dtData.Rows(0)("note").ToString
                invno.Text = dtData.Rows(0)("invoice").ToString
                update.Text = " Update By " & dtData.Rows(0)("upduser").ToString & " On " & dtData.Rows(0)("updtime").ToString
                LblPosting.Text = dtData.Rows(0)("dnstatus").ToString
                btnDelete.Enabled = True
                maxpayment.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("amttrans").ToString) - ToDouble(dtData.Rows(0)("amtbayar").ToString), 4)
                InitCOADebet(dtData.Rows(0)("coa_debet").ToString, "(" + dtData.Rows(0)("acctgcode").ToString + ") " + dtData.Rows(0)("acctgdesc").ToString)
                invoicedate.Text = Format(CDate(dtData.Rows(0)("trndate").ToString), "dd/MM/yyyy")
                invno.CssClass = "inpTextDisabled" : invno.Enabled = False
            End If
            If LblPosting.Text.ToLower = "approved" Or LblPosting.Text.ToLower = "in approval" Then
                btnsave.Visible = False : btnPost.Visible = False
                btnshowCOA.Visible = True : btnDelete.Visible = False
                draft.Visible = False : cnno.Visible = True
            Else
                btnshowCOA.Visible = False
                btnsave.Visible = True : btnPost.Visible = True
                If Session("oid") <> Nothing And Session("oid") <> "" Then
                    btnDelete.Visible = True
                Else
                    btnDelete.Visible = False
                End If
                draft.Visible = True : cnno.Visible = False
            End If
            DDLOutlet.Enabled = False : DDLOutlet.CssClass = "inpTextDisabled"
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub 'OK

    Public Sub GenerateGenID()
        cnoid.Text = GenerateID("QL_DebitNote", CompnyCode)
    End Sub 'OK

    Private Sub BindDataSupplier()
        sSql = "SELECT DISTINCT s.suppoid oid, s.suppcode code, s.suppname name FROM QL_mstsupp s INNER JOIN QL_trnbelimst bm ON bm.trnsuppoid=s.suppoid WHERE amtbelinetto - accumpayment > 0 AND (s.suppcode LIKE '%" & Tchar(nama.Text) & "%' OR s.suppname LIKE '%" & Tchar(nama.Text) & "%') AND bm.cmpcode='" & CompnyCode & "' and bm.branch_code = '" & DDLcabang.SelectedValue & "' ORDER BY s.suppname"
        Session("TblSupplier") = GetDataTable(sSql, "QL_mstsupp")
        gvSupplier.DataSource = Session("TblSupplier")
        gvSupplier.DataBind()

    End Sub 'OK

    Private Sub BinDataCustomer()
        If reftype.SelectedValue = "AR" Then
            sSql = "SELECT DISTINCT s.custoid oid, s.custcode code, s.custname name FROM QL_mstcust s INNER JOIN QL_trnjualmst bm ON bm.trncustoid=s.custoid WHERE trnamtjualnetto - accumpayment > 0 AND (s.custcode LIKE '%" & Tchar(nama.Text) & "%' OR s.custname LIKE '%" & Tchar(nama.Text) & "%') AND bm.cmpcode='" & CompnyCode & "' and s.branch_code='" & DDLcabang.SelectedValue & "' ORDER BY s.custname"
        ElseIf reftype.SelectedValue = "NT" Then
            sSql = "SELECT DISTINCT oid, code, name FROM ( SELECT s.custoid oid, s.custcode code, s.custname name, bm.amtekspedisi,ISNULL((select SUM(amtbayar) from QL_conar c where c.refoid = bm.trnbiayaeksoid and c.trnartype = 'PAYAREXP'),0) AS amtbayar FROM QL_mstcust s INNER JOIN ql_trnbiayaeksmst bm ON bm.custoid=s.custoid where (s.custcode LIKE '%" & Tchar(nama.Text) & "%' OR s.custname LIKE '%" & Tchar(nama.Text) & "%') AND bm.cmpcode='" & CompnyCode & "' and bm.branch_code='" & DDLcabang.SelectedValue & "') cust where amtekspedisi - amtbayar > 0 ORDER BY cust.name"
        End If
        Session("TblSupplier") = GetDataTable(sSql, "QL_mstsupp")
        gvSupplier.DataSource = Session("TblSupplier")
        gvSupplier.DataBind()
    End Sub 'OK

    Private Sub InitCOADebet(ByVal iAcctgOid As Integer, ByVal sAcctgCodeDesc As String)
        coadebet.Items.Clear()
        coadebet.Items.Add(New ListItem(sAcctgCodeDesc, iAcctgOid))
    End Sub

    Private Sub InitCOACredit()
        coacredit.Items.Clear()
        'Dim sVarAP() As String = {"VAR_DEBET_NOTE_AP"}
        'Dim sVarAR() As String = {"VAR_DEBET_NOTE_AR"}
        If reftype.SelectedValue.ToUpper = "AP" Then
            sSql = "SELECT COUNT(*) FROM QL_mstinterface WHERE interfacevar='VAR_DEBITNOTE_HUTANG_CREDIT'"
            If CheckDataExists(sSql) Then
                FillDDLAcctg(coacredit, "VAR_DEBITNOTE_HUTANG_CREDIT", DDLcabang.SelectedValue)
            Else
                showMessage("Please set VAR_DEBITNOTE_HUTANG_CREDIT in master interface first!", 2)
                Exit Sub
            End If
        ElseIf reftype.SelectedValue.ToUpper = "AR" Then
            sSql = "SELECT COUNT(*) FROM QL_mstinterface WHERE interfacevar='VAR_DEBITNOTE_PIUTANG_CREDIT'"
            If CheckDataExists(sSql) Then
                FillDDLAcctg(coacredit, "VAR_DEBITNOTE_PIUTANG_CREDIT", DDLcabang.SelectedValue)
            Else
                showMessage("Please set VAR_DEBITNOTE_PIUTANG_CREDIT in master interface first!", 2)
                Exit Sub
            End If
        Else
            sSql = "SELECT COUNT(*) FROM QL_mstinterface WHERE interfacevar='VAR_DEBITNOTE_NT_CREDIT'"
            If CheckDataExists(sSql) Then
                FillDDLAcctg(coacredit, "VAR_DEBITNOTE_NT_CREDIT", DDLcabang.SelectedValue)
            Else
                showMessage("Please set VAR_DEBITNOTE_NT_CREDIT in master interface first!", 2)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub ResetAPARData()
        lblAPARNo.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P No", "A/R No")
        lblsupp_cust.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "Supplier", "Customer")
        refoid.Text = 0 : invno.Text = ""
        curroid.SelectedIndex = 0 : aparrateoid.Text = "" : aparrate2oid.Text = ""
        cust_supp_oid.Text = "" : nama.Text = "" : amount.Text = "0" : invoicedate.Text = ""
        cProc.DisposeGridView(gvPurchasing) : gvPurchasing.Visible = False
        cProc.DisposeGridView(gvSupplier) : gvSupplier.Visible = False
        'coadebet.Items.Clear()
        coadebet.SelectedIndex = IIf(coadebet.Items.Count > 0, 0, -1)
        coacredit.SelectedIndex = IIf(coacredit.Items.Count > 0, 0, -1)
        maxpayment.Text = "0.00"
        maxpayment.Visible = (reftype.SelectedValue.ToUpper = "AP")
        lblmaxpayment.Visible = (reftype.SelectedValue.ToUpper = "AP")
        invno.CssClass = "inpText" : invno.Enabled = True
        btnPost.Visible = True : btnshowCOA.Visible = False
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("~\Accounting\debitnote.aspx")
        End If

        Me.btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete this data ?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to post this data ?');")
        Page.Title = "Debet Note"
        Session("oid") = Request.QueryString("oid")
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName)
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "Update Data"
        Else
            i_u.Text = "New Data"
        End If
        If Not IsPostBack Then
            DDLOutlet_SelectedIndexChanged(Nothing, Nothing)
            tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
            tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitCOACredit() : BindData("")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("sCmpcode"), Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                tgl.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 0
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime.ToString & "</B>; "
                update.Text = "Update By - On -"
            End If
        End If
    End Sub 'OK

    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        GVmstgen.DataSource = Session("TblMst")
        GVmstgen.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
            e.Row.Cells(3).Text = NewMaskEdit(e.Row.Cells(3).Text)
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        tipene.SelectedIndex = -1
        FilterDDL.SelectedIndex = -1
        DDLfiltercabang.SelectedIndex = DDLcabang.Items.Count - 0
        FilterText.Text = ""
        tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
        tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        GVmstgen.PageIndex = 0
        BindData("")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GVmstgen.PageIndex = 0
        Dim sqlPlus As String = ""
        sqlPlus = "AND dt.tgl BETWEEN '" & toDate(tgl1.Text) & "' AND '" & toDate(tgl2.Text) & "'"
        If DDLfiltercabang.SelectedValue <> "ALL" Then
            sqlPlus &= " AND dt.branch_code = '" & DDLfiltercabang.SelectedValue & "'"
        End If
        If tipene.SelectedValue <> "ALL" Then
            sqlPlus &= " AND dt.reftype='" & tipene.SelectedValue & "'"
        End If
        If DDLStatus.SelectedValue <> "ALL" Then
            sqlPlus &= " AND dt.dnstatus='" & DDLStatus.SelectedValue & "'"
        End If
        sqlPlus &= " And dt.tgl between '" & toDate(tgl1.Text) & "' AND '" & toDate(tgl2.Text) & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        BindData(sqlPlus)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("debitnote.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If reftype.SelectedValue = "AP" Then
                sSql = "DELETE FROM QL_conap WHERE cmpcode='" & CompnyCode & "' AND reftype='QL_DebitNote' AND payrefoid=" & cnoid.Text & " and branch_code='" & DDLcabang.SelectedValue & "'"
            Else
                sSql = "DELETE FROM QL_conar WHERE cmpcode='" & CompnyCode & "' AND reftype='QL_DebitNote' AND payrefoid=" & cnoid.Text & " and branch_code='" & DDLcabang.SelectedValue & "'"
            End If
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_DebitNote SET dnstatus='DELETE' WHERE cmpcode='" & CompnyCode & "' AND oid=" & cnoid.Text & " and branch_code='" & DDLcabang.SelectedValue & "'"

            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
        Response.Redirect("~\accounting\debitnote.aspx?awal=true")
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch_SC.Click
        If reftype.SelectedValue = "AP" Then
            BindDataSupplier()
        Else
            BinDataCustomer()
        End If
        gvSupplier.Visible = True
    End Sub

    Protected Sub btnSearchPurchasing_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If cust_supp_oid.Text = "" Then
            showMessage("Please choose Supplier/Customer !!", 2)
            Exit Sub
        End If
        If reftype.SelectedValue = "AP" Then
            sSql = "SELECT cmpcode,currencyoid, trntaxpct, refoid, invno, trnbelinote, trndate, payduedate, amttrans, amtpaid + amtdncn AS amtpaid, amtdncn, /*amtpaids,*/ amttrans-(amtpaid+amtdncn) AS amtbalance, acctgoid, apaccount, trnbelistatus, trnsuppoid FROM ( SELECT bm.cmpcode,bm.currencyoid, bm.trnbelimstoid, refoid, bm.trnbelino invno, bm.trnbelinote, con.amttrans, trnbelidate trndate, payduedate ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conap ar2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ar2.cmpcode AND pay.paymentoid=ar2.payrefoid  WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid AND ISNULL(pay.payres1,'')<>'Lebih Bayar' AND ar2.trnaptype IN ('AP','PAYAP')),0.0)) AS amtpaid ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conap ar2 WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid  AND ar2.trnaptype IN ('DNAP','CNAP')),0.0)) AS amtdncn , con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,bm.trnbelistatus, con.reftype, con.payrefoid, bm.trnsuppoid, bm.trntaxpct FROM QL_trnbelimst bm INNER JOIN QL_conap con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.trnbelimstoid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid where bm.branch_code='" & DDLcabang.SelectedValue & "'  AND ISNULL(con.payrefoid, 0)=0 ) AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.trnsuppoid=" & cust_supp_oid.Text & " AND dt.reftype='QL_trnbelimst' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND dt.trnbelistatus='POST' AND amttrans > amtpaid + amtdncn"
        ElseIf reftype.SelectedValue = "AR" Then
            sSql = "SELECT cmpcode,currencyoid, trntaxpct, refoid, invno, trnjualnote, trndate, payduedate, amttrans, amtpaid + amtdncn AS amtpaid, amtdncn, /*amtpaids,*/ amttrans-(amtpaid+amtdncn) AS amtbalance, acctgoid, apaccount, trnjualstatus, trncustoid FROM ( SELECT bm.cmpcode,bm.currencyoid, bm.trnjualmstoid, refoid, bm.trnjualno invno, bm.trnjualnote, con.amttrans, trnjualdate trndate, payduedate ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.paymentoid=ar2.payrefoid  WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid AND ISNULL(pay.payres1,'')<>'Lebih Bayar' AND ar2.trnartype IN ('AR','PAYAR')),0.0)) AS amtpaid ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid  AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS amtdncn , con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,bm.trnjualstatus, con.reftype, con.payrefoid, bm.trncustoid, bm.trntaxpct FROM QL_trnjualmst bm  INNER JOIN QL_conar con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.trnjualmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid where bm.branch_code='" & DDLcabang.SelectedValue & "' AND ISNULL(con.payrefoid, 0)=0 ) AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.trncustoid=" & cust_supp_oid.Text & " AND dt.reftype='QL_trnjualmst' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND dt.trnjualstatus='POST' AND amttrans > amtpaid + amtdncn"
        Else
            sSql = "SELECT cmpcode,currencyoid, trntaxpct, refoid, invno, trnbiayaeksnote, trndate, payduedate, amttrans, amtpaid + amtdncn AS amtpaid, amtdncn, /*amtpaids,*/ amttrans-(amtpaid+amtdncn) AS amtbalance, acctgoid, apaccount, status, custoid FROM ( SELECT bm.cmpcode,1 AS currencyoid, bm.trnbiayaeksoid, refoid, bm.trnbiayaeksno invno, bm.trnbiayaeksnote, con.amttrans, trnbiayaeksdate trndate, payduedate ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.paymentoid=ar2.payrefoid  WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid AND ISNULL(pay.payres1,'')<>'Lebih Bayar' AND ar2.trnartype IN ('EXP','PAYAREXP')),0.0)) AS amtpaid ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid  AND ar2.trnartype IN ('DNNT','CNNT')),0.0)) AS amtdncn , con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,bm.status, con.reftype, con.payrefoid, bm.custoid, 0.00 AS trntaxpct FROM ql_trnbiayaeksmst bm INNER JOIN QL_conar con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.trnbiayaeksoid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid where bm.branch_code='" & DDLcabang.SelectedValue & "' AND ISNULL(con.payrefoid, 0)=0 ) AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.custoid=" & cust_supp_oid.Text & " AND dt.reftype='ql_trnbiayaeksmst' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND dt.status='POST' AND amttrans > amtpaid + amtdncn"
        End If
        Dim dtAPAR As DataTable = ckoneksi.ambiltabel(sSql, "apar")
        Session("apar") = dtAPAR
        gvPurchasing.DataSource = dtAPAR
        gvPurchasing.DataBind()
        gvPurchasing.Visible = True
        'gvPurchasing.Visible = True
    End Sub

    Protected Sub gvPurchasing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPurchasing.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub gvPurchasing_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPurchasing.PageIndex = e.NewPageIndex
        gvPurchasing.DataSource = Session("apar")
        gvPurchasing.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Protected Sub gvPurchasing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPurchasing.SelectedIndexChanged
        Dim dtAPAR As DataTable = Session("apar")
        Dim dvSelectAPAR As DataView = dtAPAR.DefaultView
        Dim cRate As New ClassRate()
        Try
            refoid.Text = gvPurchasing.SelectedDataKey.Item("refoid").ToString
            invno.Text = gvPurchasing.SelectedDataKey.Item("invno").ToString
            invoicedate.Text = Format(CDate(gvPurchasing.SelectedDataKey.Item("trndate").ToString), "dd/MM/yyyy")
            InitCOADebet(gvPurchasing.SelectedDataKey.Item("acctgoid"), gvPurchasing.SelectedDataKey.Item("apaccount").ToString)
            lblmaxpayment.Visible = (reftype.SelectedValue.ToUpper = "AP")
            maxpayment.Visible = (reftype.SelectedValue.ToUpper = "AP")
            curroid.SelectedValue = dvSelectAPAR(0)("currencyoid").ToString
            cRate.SetRateValue(CInt(curroid.SelectedValue), toDate(invoicedate.Text))
            aparrateoid.Text = cRate.GetRateDailyOid
            aparrate2oid.Text = cRate.GetRateMonthlyOid
            If maxpayment.Visible Then
                maxpayment.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amtbalance").ToString), 4)
            Else
                maxpayment.Text = "0.00"
            End If
            gvPurchasing.Visible = False
            invno.CssClass = "inpTextDisabled" : invno.Enabled = False
        Catch ex As Exception
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        gvSupplier.DataSource = Session("TblSupplier")
        gvSupplier.DataBind()
        gvSupplier.Visible = True
    End Sub

    Protected Sub gvsupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        cust_supp_oid.Text = gvSupplier.SelectedDataKey("oid").ToString()
        nama.Text = gvSupplier.SelectedDataKey("name").ToString()
        refoid.Text = 0
        invno.Text = ""
        gvSupplier.Visible = False
        btnSearchPurchasing_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showTableCOA(cnno.Text, cabang.Text, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub reftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reftype.SelectedIndexChanged
        ResetAPARData()
        InitCOACredit()
    End Sub

    Protected Sub amount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        amount.Text = ToMaskEdit(ToDouble(amount.Text), 4)
    End Sub

    Protected Sub GVmstgen_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstgen.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub btnclearSC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cust_supp_oid.Text = "" : nama.Text = "" : refoid.Text = "" : invno.Text = ""
        gvSupplier.Visible = False
        coadebet.SelectedIndex = IIf(coadebet.Items.Count > 0, 0, -1)
        coacredit.SelectedIndex = IIf(coacredit.Items.Count > 0, 0, -1)
    End Sub

    Protected Sub btnClearinv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        refoid.Text = "" : invno.Text = ""

        'ResetAPARData()
    End Sub

    Protected Sub DDLOutlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerateGenID()
        draft.Text = cnoid.Text
        sSql = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang' order by gencode asc"
        FillDDL(DDLcabang, sSql)
        If Session("branch_id") <> "10" Then
            DDLcabang.SelectedValue = Session("branch_id")
            DDLcabang.Enabled = False
            DDLcabang.CssClass = "inpTextDisabled"
        End If

        reftype_SelectedIndexChanged(Nothing, Nothing)
        sSql = "SELECT currencyoid, currencycode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
        Dim sSql1 As String = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'"
        FillDDL(DDLfiltercabang, sSql1)
        DDLfiltercabang.Items.Add(New ListItem("ALL", "ALL"))

        If Session("branch_id") <> "10" Then
            DDLfiltercabang.SelectedValue = Session("branch_id")
            DDLfiltercabang.Enabled = False
            DDLfiltercabang.CssClass = "inpTextDisabled"
        Else
            DDLfiltercabang.SelectedIndex = DDLcabang.Items.Count - 0
        End If
    End Sub

    Protected Sub tgl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'generateNo()
        Try
            Dim dat1 As Date = CDate(toDate(tgl.Text))
            'Dim dat2 As Date = CDate(FilterPeriod2.Text)
            If dat1 < CDate("01/01/1900") Then
                showMessage("Salah Format tanggal...!!", 2) : Exit Sub
            End If
        Catch ex As Exception
            showMessage("Salah Format tanggal...!!", 2) : Exit Sub
        End Try
    End Sub

    Protected Sub GVmstgen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("debitnote.aspx?cmpcode=" & GVmstgen.SelectedDataKey.Item("cmpcode").ToString & "&oid=" & GVmstgen.SelectedDataKey.Item("oid").ToString & "")
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "select * from QL_mstgen where gengroup='TABLENAME' and gencode='ql_debitnote' and cmpcode IN ('" & CompnyCode & "')"
        Dim dt As DataTable = ckoneksi.ambiltabel(sSql, "QL_approval")
        Session("AppPerson") = dt
        LblPosting.Text = "In Approval"
        btnsave_Click(sender, e)
    End Sub
#End Region

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        If IsInputValid() Then
            If CheckValidPeriod(tgl.Text) Then
                Dim cRate As New ClassRate()
                Dim iGldtloid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
                Dim iGlmstoid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
                Dim iDtlseq As Int16 = 1
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    cnoid.Text = GenerateID("QL_DebitNote", CompnyCode)
                End If
                sSql = "SELECT COUNT(*) FROM QL_DebitNote WHERE cmpcode='" & CompnyCode & "' AND refoid=" & refoid.Text & " AND reftype='" & reftype.SelectedValue & "' AND dnstatus='IN PROCESS' " & IIf(Session("oid") = "", "", " AND oid<>" & cnoid.Text & "") & ""
                If CheckDataExists(sSql) Then
                    showMessage("- Ada transaksi DN lain untuk nota ini yang belum ter Posting, silakan ulangi kembali", 2)
                    Exit Sub
                End If

                If LblPosting.Text = "POST" Then
                    generateNo()
                    cRate.SetRateValue(CInt(curroid.SelectedValue), toDate(invoicedate.Text))
                    If cRate.GetRateDailyLastError <> "" Then
                        showMessage(cRate.GetRateDailyLastError, 2)
                        Exit Sub
                    End If
                    If cRate.GetRateMonthlyLastError <> "" Then
                        showMessage(cRate.GetRateMonthlyLastError, 2)
                        Exit Sub
                    End If
                Else
                    cnno.Text = cnoid.Text
                End If
                If ToDouble(amount.Text) > 0 Then
                    '=========================
                    'CEK PERIODE AKTIF BULANAN
                    '=========================
                    sSql = "SELECT DISTINCT ISNULL(periodacctg,'') FROM QL_crdgl Where glflag <> 'CLOSE'"
                    If GetStrData(sSql) <> "" Then
                        sSql = "select distinct left(isnull(periodacctg,''),4)+'-'+substring(isnull(periodacctg,''),6,2) FROM QL_crdgl where glflag='OPEN'"
                        If GetPeriodAcctg(CDate(toDate(tgl.Text))) < GetStrData(sSql) Then
                            showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", 2)
                            Exit Sub
                        End If
                    End If
                End If
                Dim idConap As Integer = GenerateID("QL_CONAP", CompnyCode)
                Dim idConaR As Integer = GenerateID("QL_CONAR", CompnyCode)

                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans

                Dim objTable As DataTable = Session("TblDtl")
                Try
                    If Session("oid") = Nothing Or Session("oid") = "" Then
                        sSql = "INSERT INTO QL_DebitNote(cmpcode,branch_code,oid,tgl,no,reftype,refoid,cust_supp_oid,coa_debet,coa_credit,amount,amountidr,amountusd,note,dnstatus,upduser,updtime) VALUES " & _
                        " ('" & CompnyCode & "','" & DDLcabang.SelectedValue & "', " & cnoid.Text & ", '" & CDate(toDate(tgl.Text)) & "', '" & cnno.Text & "', '" & reftype.SelectedValue & "', " & refoid.Text & ", " & cust_supp_oid.Text & ", " & coadebet.SelectedValue & ", " & coacredit.SelectedValue & ", " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & ", '" & Tchar(note.Text) & "', '" & LblPosting.Text & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "UPDATE QL_mstoid SET lastoid=" & cnoid.Text & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_DebitNote'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        sSql = "UPDATE QL_DebitNote SET no='" & cnno.Text & "', tgl='" & CDate(toDate(tgl.Text)) & "', reftype='" & reftype.SelectedValue & "', refoid=" & refoid.Text & ", cust_supp_oid=" & cust_supp_oid.Text & ", coa_debet=" & coadebet.SelectedValue & " ,coa_credit=" & coacredit.SelectedValue & " ,amount=" & ToDouble(amount.Text) & ", amountidr=" & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & " ,amountusd=" & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & " ,note='" & Tchar(note.Text) & "' ,dnstatus='" & LblPosting.Text & "', upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND oid=" & cnoid.Text & " and branch_code = '" & DDLcabang.SelectedValue & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    'If reftype.SelectedValue = "AR" Then
                    '    sSql = "DELETE FROM QL_conar WHERE cmpcode='" & CompnyCode & "' AND reftype='QL_DebitNote' AND payrefoid=" & cnoid.Text & " and branch_code ='" & DDLcabang.SelectedValue & "'"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '    If LblPosting.Text = "POST" Then
                    '        sSql = " UPDATE QL_trnjualmst SET accumpayment=accumpayment+" & ToDouble(amount.Text) & " ,accumpaymentidr = accumpayment+" & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & ",accumpaymentusd = accumpaymentusd+" & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & "   WHERE trnjualmstoid=" & refoid.Text & "  AND cmpcode='" & CompnyCode & "' and branch_code = '" & DDLcabang.SelectedValue & "'"
                    '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '        sSql = " UPDATE QL_trnJUALmst SET trnamtjualnetto=trnamtjualnetto+" & ToDouble(amount.Text) & ",trnamtjualnettoidr=trnamtjualnettoidr+" & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & ",trnamtjualnettousd=trnamtjualnettousd+" & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & ",statusDN=1  WHERE trnjualmstoid=" & refoid.Text & "  AND  cmpcode='" & CompnyCode & "' "
                    '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '        'update credit limit customer
                    '        If curroid.SelectedValue = 1 Then
                    '            sSql = "UPDATE QL_mstcust SET custcreditlimitusagerupiah=custcreditlimitusagerupiah-" & ToDouble(amount.Text) & " WHERE custoid=" & cust_supp_oid.Text & " and branch_code = '" & DDLcabang.SelectedValue & "'"
                    '            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '        Else
                    '            sSql = "UPDATE QL_mstcust SET custcreditlimitusagerupiah=custcreditlimitusagerupiah-" & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & " WHERE custoid=" & cust_supp_oid.Text & " and branch_code = '" & DDLcabang.SelectedValue & "'"
                    '            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '        End If
                    '    End If

                    '    sSql = "INSERT INTO QL_conar (cmpcode,branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans,amttransidr,amttransusd, amtbayar, amtbayaridr, amtbayarusd, trnarnote, trnarres1,upduser, updtime) VALUES " & _
                    '    "('" & CompnyCode & "','" & DDLcabang.SelectedValue & "', " & idConaR & ", 'QL_DebitNote',  " & refoid.Text & ", " & cnoid.Text & ", " & cust_supp_oid.Text & ", " & coadebet.SelectedValue & ", '" & LblPosting.Text & "', 'DNAR', '1/1/1900', '" & GetDateToPeriodAcctg(CDate(toDate(tgl.Text))) & "', " & coacredit.SelectedValue & ", '" & CDate(toDate(tgl.Text)) & "', '" & cnno.Text & "', 0, '" & CDate(toDate(tgl.Text)) & "', 0,0,0, " & ToDouble(amount.Text) * -1 & "," & (ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue) * -1 & "," & (ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue) * -1 & ", 'DN No. " & cnno.Text & " for A/R | No. " & invno.Text & "', '', '" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '    sSql = "UPDATE ql_mstoid SET lastoid=" & idConaR & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conar'"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'Else
                    '    sSql = "DELETE FROM QL_conap WHERE cmpcode='" & CompnyCode & "' AND reftype='QL_DebitNote' AND payrefoid=" & draft.Text & " and branch_code = '" & DDLcabang.SelectedValue & "'"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '    If LblPosting.Text = "POST" Then
                    '        sSql = " UPDATE QL_trnbelimst SET accumpayment=accumpayment-" & ToDouble(amount.Text) & ",accumpaymentidr=accumpayment-" & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & ",accumpaymentusd=accumpaymentusd-" & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & "  WHERE trnbelimstoid=" & refoid.Text & "  AND cmpcode='" & CompnyCode & "' and branch_code = '" & DDLcabang.SelectedValue & "'"
                    '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '    End If

                    '    sSql = "INSERT INTO QL_conap (cmpcode,branch_code, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans,amttransidr,amttransusd, amtbayar, amtbayaridr, amtbayarusd, trnapnote, trnapres1, upduser, updtime) VALUES " & _
                    '    " ('" & CompnyCode & "','" & DDLcabang.SelectedValue & "', " & idConap & ", 'QL_DebitNote',  " & refoid.Text & ", " & cnoid.Text & ", " & cust_supp_oid.Text & ", " & coadebet.SelectedValue & ", '" & LblPosting.Text & "', 'DNAP', '1/1/1900', '" & GetDateToPeriodAcctg(CDate(toDate(tgl.Text))) & "', " & coacredit.SelectedValue & ", '" & CDate(toDate(tgl.Text)) & "', '', 0, '" & CDate(toDate(tgl.Text)) & "', 0,0,0, " & ToDouble(amount.Text) & "," & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & ", 'DN No. " & cnno.Text & " for A/P | No. " & invno.Text & "', '', '" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '    sSql = "UPDATE QL_mstoid SET lastoid=" & idConap & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conap'"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'End If
                    'If LblPosting.Text = "POST" Then
                    '    '//////INSERT INTO TRN GL MST
                    '    sSql = "INSERT into QL_trnglmst (cmpcode,branch_code,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime) VALUES " & _
                    '    " ('" & CompnyCode & "','" & DDLcabang.SelectedValue & "'," & iGlmstoid & ",'" & CDate(toDate(tgl.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(tgl.Text))) & "','DN No." & cnno.Text & " for " & reftype.SelectedItem.Text & " No. " & invno.Text & "','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '    ' ============================
                    '    ' BIAYA/OTHER         99000
                    '    ' Piutang/hutang      100000
                    '    ' ============================
                    '    sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                    '    "('" & CompnyCode & "','" & DDLcabang.SelectedValue & "'," & iGldtloid & ", 1," & iGlmstoid & "," & coadebet.SelectedValue & ",'D'," & ToDouble(amount.Text) & "," & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & ",'" & cnno.Text & "','DN No. " & cnno.Text & " for " & reftype.SelectedItem.Text & " No. " & invno.Text & "','QL_DebitNote','" & draft.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & LblPosting.Text & "')"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '    iGldtloid += 1

                    '    sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                    '    "('" & CompnyCode & "','" & DDLcabang.SelectedValue & "'," & iGldtloid & ",2," & iGlmstoid & "," & coacredit.SelectedValue & ",'C'," & ToDouble(amount.Text) & "," & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & ",'" & cnno.Text & "','DN No. " & cnno.Text & " for " & reftype.SelectedItem.Text & " No. " & invno.Text & "','QL_DebitNote','" & draft.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & LblPosting.Text & "')"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '    sSql = "UPDATE  QL_mstoid SET lastoid=" & iGldtloid & " WHERE tablename='QL_trngldtl' AND cmpcode='" & CompnyCode & "' "
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '    sSql = "UPDATE  QL_mstoid SET lastoid=" & iGlmstoid & " WHERE tablename='QL_trnglmst' AND cmpcode='" & CompnyCode & "' "
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'End If

                    If LblPosting.Text = "In Approval" Then
                        sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_debitnote' Order By approvallevel"
                        Dim dtData2 As DataTable = ckoneksi.ambiltabel(sSql, "QL_approvalstructure")
                        If dtData2.Rows.Count > 0 Then
                            Session("TblApproval") = dtData2
                        Else
                            showMessage("Tidak Approval User Debit Note untuk Cabang " & DDLcabang.SelectedItem.Text & ", Silahkan contact admin dahulu", 2)
                            Exit Sub
                        End If

                        Session("AppOid") = ClassFunction.GenerateID("QL_Approval", CompnyCode)
                        If Not Session("TblApproval") Is Nothing Then
                            objTable = Session("TblApproval")
                            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                                sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus,branch_code) VALUES " & _
                                "('" & CompnyCode & "'," & Session("AppOid") + c1 & ",'DN" & Session("oid") & "_" & Session("AppOid") + c1 & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','ql_debitnote','" & cnoid.Text & "','In Approval','0','" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1','" & objTable.Rows(c1).Item("approvalstatus") & "','" & DDLcabang.SelectedValue & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            Next
                            sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    End If

                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.ToString, 2)
                    LblPosting.Text = "IN PROCESS"
                    Exit Sub
                End Try
                If LblPosting.Text = "POST" Then
                    Session("SavedInfo") = "Data telah berhasil di posting dengan nomor. " & cnno.Text & " !"
                Else
                    Response.Redirect("debitnote.aspx?awal=true")
                End If
                If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                    showMessage(Session("SavedInfo"), 3)
                Else
                    Response.Redirect("~\Accounting\debitnote.aspx?awal=true")
                End If
            End If
        End If
    End Sub

    Protected Sub DDLcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ResetAPARData()
        InitCOACredit()
    End Sub
End Class