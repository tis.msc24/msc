Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports ClassFunction

Partial Class ReportForm_rptDO
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As ReportDocument
    Dim report As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim cKoneksi As New Koneksi
    Dim sSql As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Server.Transfer("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("rptDOProject.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Report Delivery Order Project"

        If Not Page.IsPostBack Then
            filterGVS("", "", CompnyCode)
            filterGvItem("", "", CompnyCode)
            Session("showReport") = False
            date1.Text = "01/" & Date.Now.Month & "/" & Date.Now.Year & ""
            date2.Text = Format(Date.Now, "dd/MM/yyyy")
        Else
            If Session("showReport") = True Then
                showPrint(dView.SelectedValue)
            End If
        End If

    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Sub showPrint(ByVal tipe As String)
        Try
            Dim dat1 As Date = CDate(toDate(date1.Text))
            Dim dat2 As Date = CDate(toDate(date2.Text))
        Catch ex As Exception
            showMessage("Invalid filter date!!", 2)
            Session("showReport") = False
            Exit Sub
        End Try
        If CDate(toDate(date1.Text)) > CDate(toDate(date2.Text)) Then
            showMessage("Start date must be less or equal than End date!!", 2)
            Session("showReport") = False
            Exit Sub
        End If
        CrystalReportViewer2.Visible = False
        CrystalReportViewer1.Visible = True
        Try
            Dim sWhere As String = ""

            
            If custoid.Text <> "" Then
                sWhere = sWhere & IIf(sWhere.Trim <> " WHERE ", " AND ", "") & " cust.custoid='" & custoid.Text & "'"
            End If
            If lblItemId.Text <> "" Then
                sWhere = sWhere & " AND i.itemoid='" & lblItemId.Text & "'"
            End If

            If DOno.Text <> "" Then
                If DOoid.Text <> "" Then
                    sWhere = sWhere & " AND sod.trnsjjualmstoid='" & DOoid.Text & "'"
                Else
                    showMessage("Please Choose Delivery Order!!", 2)
                    Session("showReport") = False
                    Exit Sub
                End If
            End If

            If SOno.Text <> "" Then
                If SOoid.Text <> "" Then
                    sWhere = sWhere & " AND so.ordermstoid='" & SOoid.Text & "'"
                Else
                    showMessage("Please Choose Sales Order!!", 2)
                    Session("showReport") = False
                    Exit Sub
                End If
            End If

            'If DPno.Text <> "" Then
            '    If DPoid.Text <> "" Then
            '        sWhere = sWhere & " AND dp.salesdeliveryoid='" & DPoid.Text & "'"
            '    Else
            '        showMessage("Please Choose Delivery Plan!!", 2) : Exit Sub
            '    End If
            'End If


            'sWhere = sWhere & IIf(sWhere.Trim <> " WHERE ", " AND ", "") & " sod.salesdeliverydate between '" & CDate(toDate(date1.Text)) & "' and '" & CDate(toDate(date2.Text)) & "' "

            sWhere = sWhere & " and so.trnordertype='PROJECK'"

            vReport = New ReportDocument
            If dView.SelectedValue.ToLower = "sum" Then
                vReport.Load(Server.MapPath("~\Report\rptDOsum.rpt"))
            ElseIf dView.SelectedValue.ToLower = "detail" Then
                vReport.Load(Server.MapPath("~\Report\rptDOdtl.rpt"))
            End If
            'vReport.Load(Server.MapPath("~\Report\rptDOsum.rpt"))
            'vReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                '.UserID = "sa"
                '.Password = "ql"
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("cmpcode", CompnyCode)
            vReport.SetParameterValue("awal", CDate(toDate(date1.Text)))
            vReport.SetParameterValue("akhir", CDate(toDate(date2.Text)))
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            vReport.SetParameterValue("titledef", "Project ")

            If tipe = "excel" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() : Session("no") = Nothing
            Else
                If tipe = "PDF" Then
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                    vReport.Close() : vReport.Dispose() : Session("no") = Nothing
                Else
                    CrystalReportViewer1.DisplayGroupTree = False
                    CrystalReportViewer1.ReportSource = vReport
                    Session("showReport") = True
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    'Sub showPrint2(ByVal tipe As String)
    '    Try
    '        vReport = New ReportDocument
    '        If dView.SelectedValue = "D" Then
    '            vReport.Load(Server.MapPath("~\Report\rptStatusOrderDeliv.rpt"))
    '        ElseIf dView.SelectedValue = "P" Then
    '            vReport.Load(Server.MapPath("~\Report\rptStatusOrderDelivPersen.rpt"))
    '        End If
    '        Dim crConnInfo As New ConnectionInfo
    '        With crConnInfo
    '            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
    '            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
    '            .IntegratedSecurity = True
    '        End With
    '        SetDBLogonForReport(crConnInfo, vReport)
    '        vReport.SetParameterValue("cmpcode", CompnyCode)
    '        Response.Buffer = False
    '        Response.ClearContent()
    '        Response.ClearHeaders()
    '        vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
    '        vReport.Close() : vReport.Dispose() : Session("no") = Nothing
    '    Catch ex As Exception
    '        showMessage(ex.ToString)
    '    End Try
    'End Sub

    Private Sub showMessage(ByVal message As String, ByVal tipe As Int16)
        Validasi.Text = message
        If tipe = 1 Then
            Labelvalidasi.Text = CompnyName & " - ERROR"
            Image1.ImageUrl = "~/images/error.jpg"
        End If
        If tipe = 2 Then
            Labelvalidasi.Text = CompnyName & " - WARNING"
            Image1.ImageUrl = "~/images/warn.png"
        End If
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        'If suppoid.Text = "" Then
        '    showMessage("Select Supplier First!!") : Exit Sub
        'End If

        showPrint("")
    End Sub

    Sub filterGVS(ByVal code As String, ByVal name As String, ByVal cmpcode As String)
        With SqlDataSource1
            .SelectParameters("code").DefaultValue = "%" & code & "%"
            .SelectParameters("name").DefaultValue = "%" & name & "%"
            .SelectParameters("cmpcode").DefaultValue = cmpcode & "%"
        End With
        gvSupplier.DataBind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        filterGVS("", Tchar(custname.Text), CompnyCode)
        gvSupplier.Visible = True
        'CProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, True)
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custname.Text = gvSupplier.SelectedDataKey.Item(1)
        custoid.Text = gvSupplier.SelectedDataKey.Item(0)
        gvSupplier.Visible = False
        'CProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, False)
        ResetReport()
    End Sub

    Protected Sub ibtnSuppID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLSuppID.SelectedValue = "Code" Then
            filterGVS(txtFindSuppID.Text, "", CompnyCode)
        Else
            filterGVS("", txtFindSuppID.Text, CompnyCode)
        End If
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub imbViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLSuppID.SelectedIndex = 0 : txtFindSuppID.Text = ""
        filterGVS("", "", CompnyCode)
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub dView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        ResetReport()
    End Sub

    Protected Sub CloseSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, False)
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, False)
    End Sub

    Protected Sub detailstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ResetReport()
    End Sub

    Private Sub ResetReport()
        Session("showReport") = False
        CrystalReportViewer1.ReportSource = Nothing
        CrystalReportViewer2.ReportSource = Nothing
    End Sub

    Protected Sub imbClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        custname.Text = "" : custoid.Text = ""
        DDLSuppID.SelectedIndex = 0 : txtFindSuppID.Text = ""
        filterGVS("", "", CompnyCode)
        gvSupplier.Visible = False
        ResetReport()
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvSupplier.PageIndex = e.NewPageIndex
        filterGVS("", Tchar(custname.Text), CompnyCode)
        gvSupplier.Visible = True
        'CProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, True)
    End Sub

    Protected Sub imbExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Session("showReport") = True
        Dim tipelaporan As String = "excel"
        showPrint(tipelaporan)
    End Sub


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub imbClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        DDLItemID.SelectedIndex = 0 : txtFindItemID.Text = ""
        filterGvItem("", "", CompnyCode)
        txtItemName.Text = "" : lblItemId.Text = ""
        gvItem.Visible = False
        ResetReport()
    End Sub

    Protected Sub imbSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        'CProc.SetModalPopUpExtender(btnHideItem, pnlSearchItem, ModalPopupExtender2, True)
        filterGvItem("", Tchar(txtItemName.Text), CompnyCode)
        gvItem.Visible = True
    End Sub

    Protected Sub CloseItem_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CProc.SetModalPopUpExtender(btnHideItem, pnlSearchItem, ModalPopupExtender2, False)
    End Sub

    Sub filterGvItem(ByVal code As String, ByVal name As String, ByVal cmpcode As String)
        With SqlDataSource2
            .SelectParameters("icode").DefaultValue = "%" & code & "%"
            .SelectParameters("iname").DefaultValue = "%" & name & "%"
            .SelectParameters("icmpcode").DefaultValue = cmpcode & "%"
        End With
        gvItem.DataBind()
    End Sub

    Protected Sub btnFindItemID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLItemID.SelectedIndex = 1 Then ' filter = Code
            filterGvItem(txtFindItemID.Text, "", CompnyCode)
        Else ' filter = Name
            filterGvItem("", txtFindItemID.Text, CompnyCode)
        End If
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub btnViewAllItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLItemID.SelectedIndex = 0 : txtFindItemID.Text = ""
        filterGvItem("", "", CompnyCode)
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvItem.PageIndex = e.NewPageIndex
        filterGvItem("", Tchar(txtItemName.Text), CompnyCode)
        'CProc.SetModalPopUpExtender(btnHideItem, pnlSearchItem, ModalPopupExtender2, True)
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtItemName.Text = gvItem.SelectedDataKey.Item(1)
        lblItemId.Text = gvItem.SelectedDataKey.Item(0)
        'CProc.SetModalPopUpExtender(btnHideItem, pnlSearchItem, ModalPopupExtender2, False)
        gvItem.Visible = False
        ResetReport()
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToPDF.Click
        showPrint("PDF")
    End Sub

    Protected Sub VAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles VAll.Click
        Response.Redirect("rptDOProject.aspx?awal=true")
    End Sub

    Protected Sub btnsearhorder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        binddataDO()
    End Sub

    Protected Sub eraseDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        DOno.Text = ""
        DOoid.Text = ""
        gvListDO.Visible = False
    End Sub

    Protected Sub gvListDO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DOno.Text = gvListDO.SelectedDataKey.Item("trnsjjualno")
        DOoid.Text = gvListDO.SelectedDataKey.Item("trnsjjualmstoid")
        SOno.Text = gvListDO.SelectedDataKey.Item("orderno")
        SOoid.Text = gvListDO.SelectedDataKey.Item("ordermstoid")
        custname.Text = gvListDO.SelectedDataKey.Item("custname")
        custoid.Text = gvListDO.SelectedDataKey.Item("custoid")
        gvListDO.Visible = False
    End Sub

    Protected Sub gvListDO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDO.PageIndexChanging
        gvListDO.PageIndex = e.NewPageIndex
        binddataDO()
    End Sub
    Sub binddataDO()
        Try
            Dim dat1 As Date = CDate(toDate(date1.Text))
            Dim dat2 As Date = CDate(toDate(date2.Text))
        Catch ex As Exception
            showMessage("Invalid filter date!!", 2) : Exit Sub
        End Try
        If CDate(toDate(date1.Text)) > CDate(toDate(date2.Text)) Then
            showMessage("Start date must be less or equal than End date!!", 2) : Exit Sub
        End If
        sSql = "select trnsjjualmstoid  , trnsjjualno, convert(varchar(10),trnsjjualdate ,103) DOdate,trnsjjualstatus  ,so.orderno ,so.ordermstoid ,c.custoid , c.custname from QL_trnsjjualmst do inner join QL_trnordermst so on do.orderno = so.orderno inner join QL_mstcust c on so.trncustoid = c.custoid where so.trnordertype='PROJECK' and trnsjjualstatus <> 'HISTORY' and do.cmpcode ='" & CompnyCode & "' and trnsjjualno like '%" & Tchar(DOno.Text) & "%' and trnsjjualdate between '" & CDate(toDate(date1.Text)) & "' and '" & CDate(toDate(date2.Text)) & "' order by trnsjjualdate desc"
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "DO")
        gvListDO.DataSource = objTable
        gvListDO.DataBind()
        gvListDO.Visible = True
    End Sub

    Protected Sub btnSearchSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        binddataSO()
    End Sub
    Sub binddataSO()
        Try
            Dim dat1 As Date = CDate(toDate(date1.Text))
            Dim dat2 As Date = CDate(toDate(date2.Text))
        Catch ex As Exception
            showMessage("Invalid filter date!!", 2) : Exit Sub
        End Try
        If CDate(toDate(date1.Text)) > CDate(toDate(date2.Text)) Then
            showMessage("Start date must be less or equal than End date!!", 2) : Exit Sub
        End If
        sSql = "select distinct ordermstoid,so.orderno,CONVERT(varchar(10),so.trnorderdate,103) SOdate,trnorderstatus ,c.custname,c.custoid   from QL_trnordermst so inner join ql_trnsjjualmst  sod on so.orderno = sod.orderno inner join QL_mstcust c on c.custoid = so.trncustoid where so.trnordertype='PROJECK' and so.cmpcode ='" & CompnyCode & "' and so.orderno like '%" & Tchar(SOno.Text) & "%' and trnsjjualdate between '" & CDate(toDate(date1.Text)) & "' and '" & CDate(toDate(date2.Text)) & "' and trnsjjualstatus <> 'HISTORY' order by SOdate desc"

        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "SO")
        GVSO.DataSource = objTable
        GVSO.DataBind()
        GVSO.Visible = True
    End Sub

    Protected Sub eraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = False
        SOno.Text = ""
        SOoid.Text = ""
        GVSO.Visible = False
    End Sub

    Protected Sub GVSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SOno.Text = GVSO.SelectedDataKey.Item("orderno")
        SOoid.Text = GVSO.SelectedDataKey.Item("ordermstoid")
        custoid.Text = GVSO.SelectedDataKey.Item("custoid")
        custname.Text = GVSO.SelectedDataKey.Item("custname")
        GVSO.Visible = False
    End Sub

    Protected Sub GVSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVSO.PageIndexChanging
        GVSO.PageIndex = e.NewPageIndex
        binddataSO()
    End Sub

    Protected Sub btnSearchDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddataDP()
    End Sub
    Sub binddataDP()
        sSql = "select salesdeliveryoid , salesdeliveryno, convert(varchar(10),salesdeliverydate,103) DOdate,salesdeliverystatus  from QL_trnDelivOrdermst where cmpcode ='" & CompnyCode & "' and salesdeliveryno like '%" & Tchar(DPno.Text) & "%' order by salesdeliverydate desc"
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "DP")
        GVDP.DataSource = objTable
        GVDP.DataBind()
        GVDP.Visible = True
    End Sub

    Protected Sub EraseDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DPno.Text = ""
        DPoid.Text = ""
        GVDP.Visible = False
    End Sub

    Protected Sub GVDP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DPno.Text = GVDP.SelectedDataKey.Item("salesdeliveryno")
        DPoid.Text = GVDP.SelectedDataKey.Item("salesdeliveryoid")
        GVDP.Visible = False
    End Sub

    Protected Sub GVDP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVDP.PageIndexChanging
        GVDP.PageIndex = e.NewPageIndex
        binddataDP()
    End Sub
End Class
