Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptFastMoving
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim Report As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function

#Region "Procedure"
    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
 
    Public Sub BindDataListItem()
        Try
            sSql = "Select * from (Select 'False' Checkvalue,i.itemcode, i.itemdesc,i.itemoid, g.gendesc satuan3, i.merk,i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya from ql_mstitem i inner join ql_mstgen g on g.genoid=satuan1 and g.gengroup='ITEMUNIT' and itemflag='AKTIF') dt"
            Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstitem")
            Session("TblMat") = dt
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind() : gvItem.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Public Sub GenerateBulan(ByVal bln1 As String, ByVal note As String)
        If bln1 = "1" Then
            If note = "satu" Then
                lblbln1.Text = "Januari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Januari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Januari"
            End If
        ElseIf bln1 = "2" Then
            If note = "satu" Then
                lblbln1.Text = "Februari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Februari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Februari"
            End If
        ElseIf bln1 = "3" Then
            If note = "satu" Then
                lblbln1.Text = "Maret"
            ElseIf note = "dua" Then
                lblbln2.Text = "Maret"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Maret"
            End If
        ElseIf bln1 = "4" Then
            If note = "satu" Then
                lblbln1.Text = "April"
            ElseIf note = "dua" Then
                lblbln2.Text = "April"
            ElseIf note = "tiga" Then
                lblbln3.Text = "April"
            End If
        ElseIf bln1 = "5" Then
            If note = "satu" Then
                lblbln1.Text = "Mei"
            ElseIf note = "dua" Then
                lblbln2.Text = "Mei"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Mei"
            End If
        ElseIf bln1 = "6" Then
            If note = "satu" Then
                lblbln1.Text = "Juni"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juni"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juni"
            End If
        ElseIf bln1 = "7" Then
            If note = "satu" Then
                lblbln1.Text = "Juli"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juli"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juli"
            End If
        ElseIf bln1 = "8" Then
            If note = "satu" Then
                lblbln1.Text = "Agustus"
            ElseIf note = "dua" Then
                lblbln2.Text = "Agustus"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Agustus"
            End If
        ElseIf bln1 = "9" Then
            If note = "satu" Then
                lblbln1.Text = "September"
            ElseIf note = "dua" Then
                lblbln2.Text = "September"
            ElseIf note = "tiga" Then
                lblbln3.Text = "September"
            End If
        ElseIf bln1 = "10" Then
            If note = "satu" Then
                lblbln1.Text = "Oktober"
            ElseIf note = "dua" Then
                lblbln2.Text = "Oktober"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Oktober"
            End If
        ElseIf bln1 = "11" Then
            If note = "satu" Then
                lblbln1.Text = "November"
            ElseIf note = "dua" Then
                lblbln2.Text = "November"
            ElseIf note = "tiga" Then
                lblbln3.Text = "November"
            End If
        ElseIf bln1 = "12" Then
            If note = "satu" Then
                lblbln1.Text = "Desember"
            ElseIf note = "dua" Then
                lblbln2.Text = "Desember"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Desember"
            End If
        End If
    End Sub

    Private Sub InitDDL()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FilterDDLDiv, sSql)
            FilterDDLDiv.CssClass = "inpTextDisabled" : FilterDDLDiv.Enabled = False
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FilterDDLDiv, sSql)
                FilterDDLDiv.CssClass = "inpTextDisabled" : FilterDDLDiv.Enabled = False
            Else
                FillDDL(FilterDDLDiv, sSql)
                FilterDDLDiv.Items.Add(New ListItem("ALL", "ALL"))
                FilterDDLDiv.SelectedValue = "ALL"
                FilterDDLDiv.CssClass = "inpText" : FilterDDLDiv.Enabled = True
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(FilterDDLDiv, sSql)
            FilterDDLDiv.Items.Add(New ListItem("ALL", "ALL"))
            FilterDDLDiv.SelectedValue = "ALL"
            FilterDDLDiv.CssClass = "inpText" : FilterDDLDiv.Enabled = True
        End If
    End Sub

    Public Sub showPrint(ByVal showtype As String)
        Dim sWhere As String = "" : Dim sWhere1 As String = "" : Dim sWhere2 As String = ""
        Dim sWhereCb As String = "Where cmpcode='" & CompnyCode & "'"

        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(toDate(FilterPeriod1.Text)))
        Dim ValDay As Long = DateDiff(DateInterval.Day, CDate(toDate(FilterPeriod1.Text)), CDate(toDate(FilterPeriod2.Text)))
        'Dim mt As String = Format(jDate.AddMonths(0), "MM")
        Try

            If FilterDDLDiv.SelectedValue <> "ALL" Then
                sWhereCb &= " And branch_code='" & FilterDDLDiv.SelectedValue & "'"
            End If

            Dim arCode() As String = ItemName.Text.Split(";")
            Dim sCodeIn As String = "" : Dim sQel As String = ""
            Dim adr As String = ""
            For C1 As Integer = 0 To arCode.Length - 1
                If arCode(C1) <> "" Then
                    sCodeIn &= "'" & arCode(C1) & "',"
                End If
            Next

            If sCodeIn <> "" Then
                sWhereCb &= " AND itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
            End If

            sWhere1 = "And con.trndate>='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' And con.trndate<='" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
            sWhere2 = "Where jm.cmpcode='" & CompnyCode & "' And jm.trnjualdate>='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' And jm.trnjualdate<='" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"

            If FilterDDLDiv.SelectedValue <> "ALL" Then
                sWhere2 &= "And jm.branch_code = '" & FilterDDLDiv.SelectedValue & "'"
            End If

            Report = New ReportDocument
            If showtype = "excel" Then
                Report.Load(Server.MapPath("~\Report\rptFastMovingExl.rpt"))
            Else
                Report.Load(Server.MapPath("~\Report\rptFastMoving.rpt"))
            End If

            sSql = "Declare @Period Varchar(10); Set @Period='" & sPeriod & "'; Select cmpcode, branch_code, Cabang, itemoid, itemdesc, stoktrans, Sawal, SaldoAkhir, QtyJual, InterVal, itemcode, stockflag, Case When QtyJual=0.00 then 0.00 When Sawal+SaldoAkhir= 0.00 then 0.00 Else InterVal/(QtyJual/((Sawal+SaldoAkhir)/2)) End Rasio From ( Select cmpcode,branch_code, Cabang, itemoid, itemdesc, 0.00 AS stoktrans, SUM(saldoawal) Sawal, SUM(saldoawal)+SUM(QTYIN)-SUM(QTYOUT) SaldoAkhir, SUM(QtyJual) QtyJual," & ValDay & " InterVal, itemcode, stockflag From (" & _
            " Select g5.gencode branch_code, i.itemoid, i.itemcode, i.itemdesc, g.genoid mtrlocoid, 0.00 QTYIN, 0.00 qtyout,SUM(qtyIn) saldoawal, Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag, g5.gendesc Cabang, g5.cmpcode, 0.00 QtyJual From QL_mstitem i INNER Join QL_mstgen g5 ON g5.cmpcode=i.cmpcode AND g5.gengroup='Cabang' INNER Join QL_mstgen g ON g.cmpcode=i.cmpcode AND g.gengroup='LOCATION' AND g.genoid NOT IN (-9) AND g.genother2=g5.genoid AND g.genother6 NOT IN ('RUSAK','TITIPAN') left Join QL_conmtr con ON con.refoid=i.itemoid AND con.mtrlocoid=g.genoid AND con.branch_code=g5.gencode AND con.branch_code=g5.gencode AND con.type IN ('CLS','Init Stock') And periodacctg=@Period Where i.cmpcode='" & CompnyCode & "' Group BY itemoid, con.branch_code, con.mtrlocoid, i.itemdesc, g.genoid, periodacctg, con.periodacctg, g5.gendesc, itemcode, i.stockflag, g5.gencode, FormAction, g5.gendesc, g5.cmpcode UNION ALL Select g5.gencode branch_code, i.itemoid, i.itemcode, i.itemdesc, g.genoid mtrlocoid, 0.00 QTYIN, 0.00 QTYOUT, SUM(qtyIn) saldoawaL, Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag ,g5.gendesc Cabang, g5.cmpcode,0.00 QtyJual From QL_mstitem i INNER Join QL_mstgen g5 ON g5.cmpcode=i.cmpcode AND g5.gengroup='Cabang' INNER Join QL_mstgen g ON g.cmpcode=i.cmpcode AND g.gengroup='LOCATION' AND g.genoid=-10 AND g.genother6 NOT IN ('RUSAK','TITIPAN') Left Join QL_conmtr con ON con.refoid=i.itemoid AND con.mtrlocoid=g.genoid AND con.branch_code=g5.gencode AND con.type IN ('CLS','Init Stock') and periodacctg=@Period Where i.cmpcode='" & CompnyCode & "' Group BY itemoid,con.branch_code,con.mtrlocoid,i.itemdesc,g.genoid,g5.cmpcode,con.periodacctg,g5.gendesc,itemcode,i.stockflag,g5.gencode,g5.gendesc UNION ALL " & _
            "/* --Trans -- */" & _
            " Select con.branch_code, i.itemoid, i.itemcode, i.itemdesc, con.mtrlocoid, qtyIn, qtyOut, 0.00 saldoawal, Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag, g5.gendesc Cabang, g5.cmpcode, 0.00 QtyJual From QL_conmtr con Inner Join ql_mstitem i On i.itemoid=con.refoid inner join ql_mstgen g on g.genoid=con.mtrlocoid And g.gengroup='LOCATION' AND g.genoid NOT IN (-9) AND g.genother6 NOT IN ('RUSAK','TITIPAN') inner join ql_mstgen g5 ON g5.gencode = con.branch_code AND g5.gengroup='CABANG' Where i.cmpcode='" & CompnyCode & "' AND con.type NOT IN ('CLS','Init Stock') " & sWhere1 & " UNION ALL " & _
            "/*-- Trn Jual --*/" & _
            " Select jm.branch_code, i.itemoid, i.itemcode, itemdesc, 0 mtrlocoid, 0.00 qtyin, 0.00 qtyout, 0.00 AS saldoawal, Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag , g1.gendesc Cabang, jm.cmpcode, ISNULL(SUM(jd.trnjualdtlqty),0.00) QtyJual From ql_mstitem i INNER JOIN QL_trnjualdtl jd ON jd.itemoid = i.itemoid INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid = jd.trnjualmstoid and jm.branch_code = jd.branch_code INNER JOIN QL_mstgen g1 ON g1.gencode = jm.branch_code and g1.gengroup = 'CABANG' " & sWhere2 & " Group by jm.cmpcode, jm.branch_code, g1.gendesc, i.itemoid, i.itemdesc,i.itemcode,i.stockflag) Crd /*WHERE mtrlocoid=375*/ Group By branch_code,itemoid,itemcode,stockflag,itemdesc,cmpcode,Cabang HAVING(SUM(saldoawal) <> 0 Or SUM(QTYIN) <> 0 Or SUM(QTYOUT) <> 0)) dt " & sWhereCb & " Order by branch_code,itemdesc,Rasio Asc"

            Dim dtTbl As DataTable = ckon.ambiltabel(sSql, "QL_LapFastMoving")
            Dim dvTbl As DataView = dtTbl.DefaultView
            Report.SetDataSource(dvTbl.ToTable)

            cProc.SetDBLogonForReport(Report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            Report.SetParameterValue("sTartPeriode", CDate(toDate(FilterPeriod1.Text)))
            Report.SetParameterValue("EndPeriode", CDate(toDate(FilterPeriod2.Text)))

            If showtype = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "FM_" & Format(GetServerTime, "dd_MM_yy"))
                Report.Close() : Report.Dispose()
            ElseIf showtype = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "FM_" & Format(GetServerTime, "dd_MM_yy"))
                Report.Close() : Report.Dispose()
            ElseIf showtype = "view" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = Report
            End If

        Catch ex As Exception
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptFastMoving.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Fast Moving "
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            'Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not IsPostBack Then
            InitDDL()
            'If Session("branch_id") <> "10" Then
            '    sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and gencode='" & Session("branch_id") & "'"
            '    FillDDL(FilterDDLDiv, sSql)
            '    FilterDDLDiv.CssClass = "inpTextDisabled" : FilterDDLDiv.Enabled = False
            'Else
            '    sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
            '    FillDDL(FilterDDLDiv, sSql)
            '    FilterDDLDiv.Items.Add(New ListItem("ALL", "ALL"))
            '    FilterDDLDiv.SelectedValue = "ALL"
            '    FilterDDLDiv.CssClass = "inpText" : FilterDDLDiv.Enabled = True
            'End If
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not Report Is Nothing Then
                If Report.IsLoaded Then
                    Report.Dispose()
                    Report.Close()
                End If
            End If
        Catch ex As Exception
            Report.Dispose()
            Report.Close()
        End Try
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("excel")
    End Sub

    Protected Sub ibpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("pdf")
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        If Not Session("listofitem") Is Nothing Then
            gvItem.DataSource = Session("listofitem")
        Else
            gvItem.DataSource = Nothing
        End If
        gvItem.DataBind()
        gvItem.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvItem_PageIndexChanging1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                showMessage("Data Barang tidak ada..!!", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False

    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ItemName.Text = "" : itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("rptFastMoving.aspx?awal=true")
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showPrint("view")
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("view")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        If WarningListMat.Text = "1" Then

        End If
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvItem.DataSource = Nothing : gvItem.DataBind()
        BindDataListItem()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "Checkvalue=True"
        If dv.Count > 0 Then

            If ItemName.Text <> "" Then
                For C1 As Integer = 0 To dv.Count - 1
                    ItemName.Text &= ";" & dv(C1)("itemcode").ToString & ";"
                Next
            Else
                For C1 As Integer = 0 To dv.Count - 1
                    ItemName.Text &= dv(C1)("itemcode").ToString & ";"
                Next
            End If

            If ItemName.Text <> "" Then
                ItemName.Text = Left(ItemName.Text, ItemName.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            showMessage("- Maaf, Silahkan pilih katalog, dengan beri centang pada kolom pilih..<BR>", 2)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub
#End Region

    Protected Sub BtnSelectALL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSelectALL.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
            End If
            mpeListMat.Show()
        Else
            showMessage("- Data tidak ditemukan", "MSC - WARNING")
        End If
    End Sub

    Protected Sub BtnSelectNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
            End If
            mpeListMat.Show()
        Else
            showMessage("- Maaf data tidak ketemu..!!", "MSC - INFORMATION")
        End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""

            sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"

            If dd_stock.SelectedValue <> "ALL" Then
                sFilter &= " AND stockflag='" & dd_stock.SelectedValue & "'"
            End If

            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                WarningListMat.Text = "1"
                showMessage("Maaf, Data barang yang anda cari tidak ada..!!", 2)
                mpeListMat.Show()
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            BindDataListItem()
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind()
        End If
        mpeListMat.Show()
    End Sub
End Class
