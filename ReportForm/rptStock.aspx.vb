Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptstock
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        'If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
        '    showMessage("Your Period 1 is invalid. " & sErr, 2)
        '    Return False
        'End If
        'If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
        '    showMessage("Your Period 2 is invalid. " & sErr, 2)
        '    Return False
        'End If
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedure"

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                If Session("branch_id") = "01" Then
                    FillDDL(dd_branch, sSql)
                    dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
                    dd_branch.SelectedValue = "SEMUA BRANCH"
                Else
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(dd_branch, sSql)
                End If
            Else
                FillDDL(dd_branch, sSql)
                dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
                dd_branch.SelectedValue = "SEMUA BRANCH"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dd_branch, sSql)
            dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            dd_branch.SelectedValue = "SEMUA BRANCH"
        End If
    End Sub

    Public Sub initddl() 
        Dim fCb As String = ""
        If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
            fCb = "AND cb.gencode='" & dd_branch.SelectedValue & "'"
        End If
        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc Gudang FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid Inner Join QL_mstgen cb ON cb.genoid=a.genother2 AND cb.gengroup='Cabang' Where a.cmpcode='MSC' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' " & fCb & " UNION ALL Select Distinct gd.genoid,'GUDANG'+' - '+gd.gendesc+' '+cb.gendesc Gudang From QL_conmtr con Inner Join QL_mstgen gd ON gd.genoid=con.mtrlocoid AND gd.gengroup='LOCATION' AND gd.gencode IN ('EXPEDISI','SERVICE') Inner JOin QL_mstgen cb ON cb.gencode=con.branch_code AND cb.gengroup='CABANG' Where gd.cmpcode='" & CompnyCode & "' " & fCb & ""
        FillDDL(ddlLocation, sSql)

        If Session("branch_id") = "10" Or Session("branch_id") = "01" Then
            ddlLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"
        End If
    End Sub

    Private Sub InitddlGroup()
        sSql = "SELECT genoid,gendesc FROM QL_mstgen WHERE gengroup='ITEMGROUP' AND genoid IN (SELECT i.itemgroupoid FROM QL_mstitem i) ORDER BY gendesc"
        FillDDL(DDLItemGroup, sSql)
        DDLItemGroup.Items.Add(New ListItem("ALL", "ALL"))
        DDLItemGroup.SelectedValue = "ALL"
    End Sub

    Private Sub InitDdlPic()
        Dim upd As String = ""
        Try
            sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "' AND PERSONSTATUS IN (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND genoid IN (4669,1004) AND STATUS='AKTIF') AND PERSONOID IN (Select personnoid from QL_MSTPROF pf Where pf.personnoid=PERSONOID AND STATUSPROF='Active')"
            If Session("branch_id") = "10" Then
                If Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                    sSql &= "UNION ALL SELECT 0 personoid, 'NONE' personname"
                    FillDDL(ddlPIC, sSql)
                    ddlPIC.Items.Add(New ListItem("0", "NONE"))
                    ddlPIC.SelectedValue = "0" : PIC.Enabled = True
                Else
                    sSql &= "UNION ALL SELECT 0 personoid, 'NONE' personname" 
                    FillDDL(ddlPIC, sSql)
                    PIC.Enabled = False : PIC.Checked = True
                End If
            Else
                sSql &= "UNION ALL SELECT 0 personoid, 'NONE' personname"
                FillDDL(ddlPIC, sSql)
                ddlPIC.Items.Add(New ListItem("0", "NONE"))
                ddlPIC.SelectedValue = "0" : PIC.Enabled = True
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
       
    End Sub

    Public Sub ShowPrint(ByVal showtype As String)
        Dim sWhere As String = "", dWhere As String = "", sWhereCb As String = "", namaPDF As String = "", dtNta As String = "", aPeriod As String = ""
        Dim trnDate As String = toDate(dateAwal.Text)
        Dim tahunAwal As Date = CDate(toDate(dateAwal.Text))
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("Period report is invalid..!", 2)
            Exit Sub
        End Try

        dWhere &= " Where i.cmpcode='" & CompnyCode & "'"
        If type.SelectedValue = "Summary" Then
            sWhere &= "Where con.cmpcode='" & CompnyCode & "' /*AND con.updtime >='" & toDate(dateAwal.Text) & "'*/"
        End If

        If JenisBarangDDL.SelectedValue <> "ALL" Then
            sWhere &= " AND i.stockflag='" & JenisBarangDDL.SelectedValue & "'"
            dWhere &= " AND i.stockflag='" & JenisBarangDDL.SelectedValue & "'"
        End If

        If type.SelectedValue = "Summary" Then
            If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
                sWhereCb &= " AND g5.gencode ='" & dd_branch.SelectedValue & "'"
            End If
        Else
            If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
                sWhereCb &= " AND cbg.gencode ='" & dd_branch.SelectedValue & "'"
            End If
        End If

        If ddlLocation.SelectedValue.ToUpper <> "ALL LOCATION" Then
            dWhere &= " AND g.genoid=" & ddlLocation.SelectedValue & ""
        End If

        If ItemName.Text <> "" Then
            Dim sMatcode() As String = Split(ItemName.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sMatcode.Length - 1
                sSql &= " i.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                If c1 < sMatcode.Length - 1 Then
                    sSql &= " OR"
                End If
            Next
            sSql &= ")"
            sWhere &= sSql
            dWhere &= sSql
        End If

        If PIC.Checked = True Then
            If ddlPIC.SelectedValue <> "0" Then
                dWhere &= " And i.personoid = " & ddlPIC.SelectedValue & ""
                sWhere &= " And i.personoid = " & ddlPIC.SelectedValue & ""
            End If
        End If

        If ddlLocation.SelectedValue.ToUpper <> "ALL LOCATION" Then
            sWhere &= " AND con.mtrlocoid=" & ddlLocation.SelectedValue & ""
        End If 
     

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Period 2 must be more than Period 1!", 2)
            Exit Sub
        End If

        Try

            vReport = New ReportDocument
            If type.SelectedValue = "Summary" Then

                If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " AND con.BRANCH_CODE ='" & dd_branch.SelectedValue & "'"
                End If

                If showtype = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptStockSumExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptStockSum.rpt"))
                End If

                vReport.SetParameterValue("sWhere", sWhere)
                vReport.SetParameterValue("sWherePeriod", GetDateToPeriodAcctg(CDate(toDate(dateAkhir.Text))))
                namaPDF = "StockSum_"

            Else 'DETAIL'

                If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " AND con.BRANCH_CODE ='" & dd_branch.SelectedValue & "'"
                End If

                sWhere &= " AND con.trndate >='" & toDate(dateAwal.Text) & " 0:0:0' AND con.trndate <='" & toDate(dateAkhir.Text) & " 23:59:00'"

                If showtype = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptStockDtlNewExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptStockDtlNew.rpt"))
                End If 

                'GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text)))
                If tahunAwal.Year < 2018 Then
                    aPeriod = "2018-01"
                Else
                    aPeriod = GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text)))
                End If
                vReport.SetParameterValue("sWherePeriod", GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))))
                vReport.SetParameterValue("aPeriod", aPeriod)
                vReport.SetParameterValue("trnDate", trnDate)
                vReport.SetParameterValue("lDate", trnDate)
                vReport.SetParameterValue("sWhere", sWhere)
                vReport.SetParameterValue("sPeriode", dateAwal.Text & " - " & dateAkhir.Text)
                vReport.SetParameterValue("userid", Session("UserID").ToString.ToUpper)
                namaPDF = "Stock_Detail_"
                'End If
            End If
            vReport.SetParameterValue("dWhere", dWhere)
            vReport.SetParameterValue("sWhereCb", sWhereCb)
            vReport.SetParameterValue("CompanyName", CompnyName)
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If showtype = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "view" Then
                Session("diprint") = "True"
                crv.DisplayGroupTree = False
                crv.ReportSource = vReport
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT 'False' AS checkvalue, i.itemoid,i.itemcode, i.itemdesc, i.itemcode, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' End JenisNya,i.itemgroupoid,'Buah' satuan3, i.merk, i.stockflag, i.personoid, i.itemflag FROM QL_mstitem i Where i.cmpcode='" & CompnyCode & "'"
        If PIC.Checked = True Then
            If ddlPIC.SelectedValue <> "0" Then
                sSql &= " AND i.personoid = " & ddlPIC.SelectedValue & ""
            End If
        End If
        sSql &= " ORDER BY itemdesc"
        Session("TblMat") = ckon.ambiltabel(sSql, "QL_mstitem")
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptStock.aspx")
        End If
        Page.Title = CompnyName & " - Stock Report "
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            fDDLBranch() : dd_branch_SelectedIndexChanged(Nothing, Nothing)
            initddl() : InitDdlPic() : InitddlGroup()
            Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
            If lastPeriodene = "?" Then
                lastPeriodene = 0
                dateAwal.Text = Format(GetServerTime(), "dd/MM/yyyy")
            Else
                dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy") '"01/" & lastPeriodene.Substring(5).Trim & "/" & lastPeriodene.Substring(0, 4).Trim
            End If
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles type.SelectedIndexChanged
        Session("diprint") = "False"
        If type.SelectedValue = "Summary" Then
            tdpr2.Visible = False
            tdpr1.Visible = False : trtgl.Visible = False
            tdpr3.Visible = False
            Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
            dateAwal.Text = "01/" & lastPeriodene.Substring(5).Trim & "/" & lastPeriodene.Substring(0, 4).Trim
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            tdpr2.Visible = False
            ImageButton1.Visible = False : ImageButton2.Visible = False
            TD44.Visible = False : Label4.Visible = False
            Label6.Visible = False : Label10.Visible = False
            DDLMonth.Visible = True
            DDLYear.Visible = True
            Label11.Visible = False : Label12.Visible = False
        Else ' detail
            trtgl.Visible = True : tdpr3.Visible = False
            tdpr1.Visible = False : Label12.Visible = True
            tdpr2.Visible = False
            TD44.Visible = True : Label4.Visible = True
            ImageButton1.Visible = True : ImageButton2.Visible = True
            dateAwal.Visible = True : dateAkhir.Visible = True
           
            Label6.Visible = True : Label10.Visible = True
            DDLMonth.Visible = True : DDLYear.Visible = True
            Label11.Visible = False
        End If
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("rptStock.aspx?awal=true")
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowPrint("view")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        ShowPrint("excel")
    End Sub

    Protected Sub ibPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibPDF.Click
        ShowPrint("pdf")
    End Sub
    ' get branch code
    Private Function Get_Branch() As String
        Dim sqlstr As String = String.Empty : Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand : Dim refcode As String = String.Empty
        conn2.Open() : cmd.Connection = conn2
        sqlstr = "SELECT genoid FROM QL_mstgen where gengroup = 'cabang' and gencode = '" & dd_branch.SelectedValue & "'"
        cmd.CommandType = CommandType.Text : cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            refcode = rdr("genoid")
        End While
        conn2.Close()
        Return refcode
    End Function

    Protected Sub dd_branch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dd_branch.SelectedIndexChanged
        initddl()
        'Dim fCb As String = ""
        'If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
        '    fCb = "AND cb.gencode='" & dd_branch.SelectedValue & "'"
        'End If
        'sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc Gudang FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid Inner Join QL_mstgen cb ON cb.genoid=a.genother2 AND cb.gengroup='Cabang' Where a.cmpcode='MSC' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' " & fCb & " UNION ALL Select Distinct gd.genoid,'GUDANG'+' '+cb.gendesc+' - '+gd.gendesc+' '+cb.genother1 Gudang From QL_conmtr con Inner Join QL_mstgen gd ON gd.genoid=con.mtrlocoid AND gd.gengroup='LOCATION' AND gd.gencode='EXPEDISI' Inner JOin QL_mstgen cb ON cb.gencode=con.branch_code AND cb.gengroup='CABANG' Where gd.cmpcode='MSC' " & fCb & ""
        'FillDDL(ddlLocation, sSql)
        'ddlLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
        'ddlLocation.SelectedValue = "ALL LOCATION"
    End Sub

    Protected Sub crv_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crv.Navigate
        UpdateCheckedMat()
        ShowPrint("view")
    End Sub

    Protected Sub crv_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crv.Search
        ShowPrint("view")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5"
            gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        ItemName.Text = ""
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        Dim sPlus As String = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If DDLItemGroup.SelectedValue <> "ALL" Then
            sPlus &= " AND itemgroupoid=" & DDLItemGroup.SelectedValue & ""
        End If

        If dd_stock.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag='" & dd_stock.SelectedValue & "'"
        End If

        sPlus &= " AND itemflag='" & ddlStatus.SelectedValue & "'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        DDLItemGroup.SelectedValue = "ALL"
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If ItemName.Text <> "" Then
                            ItemName.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            ItemName.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnALLSelectItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnALLSelectItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Maaf, Anda belum pilih katalog..!!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedItem.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Mohon maaf, data tidak ditemukan..!! !"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub
#End Region
End Class
