Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class Transaction_WorkOrder
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument

    Public folderReport As String = "~/Report/"

#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailItemInputValid() As Boolean
        Dim sError As String = ""
        'If soitemmstoid.Text = "" Then
        '    sError &= "- Please select SO NO. field!<BR>"
        'End If
        If itemoid.Text = "" Then
            sError &= "- Please select FINISH GOOD field!<BR>"
        Else
            'If Not CheckDataExists("SELECT COUNT(*) FROM QL_mstbom bom inner join(select i.itemcode from QL_mstitem i inner join QL_trnsodtl sodtl on sodtl.itemoid = i.itemoid where i.itemoid=" & itemoid.Text & ")dt on SUBSTRING(dt.itemcode,1,2) = bom.cat1code and SUBSTRING(dt.itemcode,4,4) = bom.cat2code where bom.cmpcode='" & DDLBusUnit.SelectedValue & "' ") Then
            '    'If Not CheckDataExists("SELECT COUNT(*) FROM QL_mstbom WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND itemoid=" & itemoid.Text & "") Then
            '    sError &= "- Please create BOM from your selected FINISH GOOD!<BR>"
            'Else

            '    Dim iTotalBOMProcess As Integer = ToInteger(GetStrData("SELECT COUNT(*) FROM QL_mstbomdtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text & ""))

            'End If
        End If

        'If wodtl1qty.Text = "" Then
        '    sError &= "- Please fill QTY field!<BR>"
        'Else
        '    If ToDouble(wodtl1qty.Text) <= 0 Then
        '        sError &= "- QTY field must be more than 0!<BR>"
        '    Else
        '        Dim sErrReply As String = ""
        '        If Not isLengthAccepted("wodtl1qty", "QL_trnwodtl1", ToDouble(wodtl1qty.Text), sErrReply) Then
        '            sError &= "- QTY field must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
        '        Else
        '            'If Not IsQtyRounded(ToDouble(wodtl1qty.Text), ToDouble(itemlimitqty.Text)) Then
        '            '    sError &= "- QTY field must be rounded by ROUNDING QTY!<BR>"
        '            'End If
        '        End If
        '    End If
        'End If
        ''If wodtl1area.Text = "" Then
        ''    sError &= "- Please fill AREA field!<BR>"
        ''Else
        ''    If ToDouble(wodtl1area.Text) <= 0 Then
        ''        sError &= "- AREA field must be more than 0!<BR>"
        ''    Else
        ''        Dim sErrReply As String = ""
        ''        If Not isLengthAccepted("wodtl1area", "QL_trnwodtl1", ToDouble(wodtl1area.Text), sErrReply) Then
        ''            sError &= "- AREA field must be less than MAX AREA (" & sErrReply & ") allowed stored in database!<BR>"
        ''        End If
        ''    End If
        ''End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailProcessInputValid() As Boolean
        Dim sError As String = ""
        'If wodtl2procseq.Text = "" Then
        '    sError = "- Sequence Proses Tidak Boleh Kosong ! <BR>"
        'End If
        If wodtl2qty.Text = "" Then
            'sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(wodtl2qty.Text) <= 0 Then
                'sError &= "- QTY field must be more than 0!<BR>"
            Else
                'Dim sErrReply As String = ""
                'If Not isLengthAccepted("wodtl2qty", "QL_trnwodtl2", ToDouble(wodtl2qty.Text), sErrReply) Then
                '    sError &= "- QTY field must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
                'Else
                '    If Not IsQtyRounded(ToDouble(wodtl2qty.Text), ToDouble(processlimitqty.Text)) Then
                '        sError &= "- QTY field must be rounded by ROUNDING QTY!<BR>"
                '    End If
                'End If
            End If
        End If
        Try
            If CDate(datestart.Text & " " & TimeStart.Text) > CDate(DateFinish.Text & " " & TimeFinish.Text) Then
                sError &= "- Start Date Must be less than Finish Date!<BR>"
            End If
        Catch ex As Exception
            sError &= "- Invalid Start Date or Finish Date !<BR>"
        End Try
        

        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        'If wodate.Text = "" Then
        '    sError &= "- Please fill DATE field!<BR>"
        'Else
        '    If Not IsValidDate(wodate.Text, "MM/dd/yyyy", sErr) Then
        '        sError &= "- DATE is invalid. " & sErr & "<BR>"
        '    End If
        'End If
        If startProd.Text = "" Then
            sError &= "- Please fill DATE field!<BR>"
        Else
            If Not IsValidDate(startProd.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If finishProd.Text = "" Then
            sError &= "- Please fill DATE field!<BR>"
        Else
            If Not IsValidDate(finishProd.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        Dim sErrDtl As String = ""
        If Session("TblDtlFG") Is Nothing Then
            sErrDtl &= "- Please fill DETAIL FINISH GOOD!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtlFG")
            If objTbl.Rows.Count <= 0 Then
                sErrDtl &= "- Please fill DETAIL FINISH GOOD!<BR>"
            End If
        End If
        If Session("TblDtlProcess") Is Nothing Then
            sErrDtl &= "- Please fill DETAIL PROCESS!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtlProcess")
            If objTbl.Rows.Count <= 0 Then
                sErrDtl &= "- Please fill DETAIL PROCESS!<BR>"
            End If
        End If

        If womstnote.Text.Length > 100 Then
            sErrDtl &= "- SPK NOTE HEADER Max Char is 100 !<BR>"
        End If

        If sErrDtl <> "" Then
            sError &= sErrDtl
        Else
            'Dim objTblSO As DataTable = Session("TblDtlFG")
            'Dim sGroupOid As String = objTblSO.Rows(0)("groupoid").ToString
            'Dim objTblProc As DataTable = Session("TblDtlProcess")
            'For C1 As Integer = 0 To objTblProc.Rows.Count - 1
            '    If Not CheckDataExists("SELECT COUNT(*) FROM QL_mstdept de INNER JOIN QL_mstdeptgroupdtl dgd ON dgd.cmpcode=de.cmpcode AND dgd.deptoid=de.deptoid WHERE dgd.groupoid=" & sGroupOid & " AND de.deptoid=" & objTblProc.Rows(C1)("deptoid").ToString & "") Then
            '        sError &= "- Every selected DEPARTMENT in DETAIL PROCESS must be in one Division!<BR>" : Exit For
            '    End If
            'Next
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            womststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckWOStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnwomst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND womststatus='In Process' and woflag = '" & ddlspk.SelectedValue & "'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnWOLeburan.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbWOInProcess.Visible = True
            lkbWOInProcess.Text = "You have " & GetStrData(sSql) & " In Process Job Costing MO data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Division
        sSql = "SELECT 'MSC' , 'Multi Sarana Computer'"
        FillDDL(FilterDDLBusUnit, sSql)
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
        End If
        'Fill DDL type Emas
        sSql = "select genoid,gendesc from ql_mstgen where gengroup = 'TYPEMAS'  and cmpcode = 'MGP' order by gendesc desc"
        FillDDL(tipeEmas, sSql)
        FillDDL(typeEmasdtl1, sSql)
        FillDDL(typeEmasdtl2, sSql)
        ' Fill DDL Unit Finish Good
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL UNIT' order by gendesc desc"
        ' FillDDL(wodtl1unitoid, sSql)
        FillDDL(wodtl1unitoid2, sSql)
        'Fill DDL PIC

    

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql = "SELECT w.personoid, p.personname from ql_trnwomst w inner join ql_mstperson p on p.personoid=w.personoid where w.womstoid=" & Session("oid") & " UNION ALL SELECT personoid, personname from ql_mstperson WHERE cmpcode='" & CompnyCode & "' AND personstatus = 963 AND personoid <> (SELECT personoid from ql_trnwomst where womstoid=" & Session("oid") & ") "
            FillDDL(DDLPIC, sSql)

            sSql = "select deptoid,wodtl2desc from ql_trnwodtl2 where cmpcode='MSC' and womstoid = '" & Session("oid") & "'"
            FillDDL(DDLProcess, sSql)
        Else
            sSql = "SELECT personoid, personname from ql_mstperson WHERE cmpcode='" & CompnyCode & "' AND personstatus = 963"
            FillDDL(DDLPIC, sSql)

        End If

        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL UNIT' order  by gendesc asc"
        FillDDL(wodtl1unitoid1, sSql)
        FillDDL(wodtl3unitbijioid, sSql)
        ' Fill DDL Unit Process
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND (gengroup='ITEM UNIT') order by genoid asc"
        FillDDL(wodtl2unitoid, sSql)

        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND (gengroup='ITEM UNIT' OR gengroup='MATERIAL UNIT') "
        FillDDL(bomdtl2unitoid, sSql)
        ' Fill DDL Unit Material
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL UNIT' "
        FillDDL(wodtl3unitoid, sSql)
        FillDDL(wodtl4unitoid, sSql)
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode='" & CompnyCode & "' AND personstatus in(991,992) order by personname "
        FillDDL(DDLop1, sSql)
        FillDDL(DDLop2, sSql)
        FillDDL(DDLqa, sSql)

       
    End Sub

    Private Sub InitDDLCabang()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'  "
        FillDDL(ddltobranch, sSql)
        'ddltobranch.Items.Add(New ListItem("ALL", "00"))
        'ddltobranch.SelectedIndex = ddltobranch.Items.Count - 1
    End Sub

    Private Sub InitDDLDept()
        'Fill DDL Department
        sSql = "SELECT genoid, gendesc FROM ql_mstgen WHERE gengroup = 'PROCESS' AND cmpcode='" & DDLBusUnit.SelectedValue & "'"
        FillDDL(deptoid, sSql)

        initdeptoid()
    End Sub

    Private Sub initdeptoid()

        'todeptoid.Visible = False
        If deptoid.SelectedValue = "3308" Then
            sSql = "select genoid,gendesc from ql_mstgen where gengroup = 'Process' and genoid =128 and cmpcode= '" & DDLBusUnit.SelectedValue & "' "
            FillDDL(todeptoid, sSql)
        Else
            sSql = "select genoid,gendesc from ql_mstgen where gengroup = 'Process' and genoid =" & deptoid.SelectedValue & " and cmpcode= '" & DDLBusUnit.SelectedValue & "' "
            FillDDL(todeptoid, sSql)
        End If
   
        

    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT wom.womstoid, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, wom.wono, i.itemcode itemcode, i.itemdesc itemlongdesc,(select sum(wod1.wodtl1qty) from QL_trnwodtl1 wod1 where wod1.womstoid = wom.womstoid) wodtl1qty, (CASE WHEN wom.womststatus='In Process' THEN '' ELSE wom.upduser END) AS postuser,womststatus, 'False' AS checkvalue FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid  INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE "
        'If Session("CompnyCode") <> CompnyCode Then
        sSql &= "wom.cmpcode='" & CompnyCode & "'"
        'Else
        '    sSql &= "wom.cmpcode LIKE '%'"
        'End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, wodate) DESC, wom.womstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnwomst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "womstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindSOData()
        If customer.Checked = True Then
            typeorder.Text = "customer"
            sSql = "SELECT som.somstoid AS soitemmstoid, som.sono AS soitemno, CONVERT(VARCHAR(10), som.sodate, 101) AS soitemdate, 'Leburan' AS soitemtype, cust.custname,(select distinct orderkadaremas from QL_trnsodtl where som.somstoid = somstoid)orderkadaremas, som.groupoid,'' groupdesc, somstnote FROM QL_trnsomst som inner join ql_mstcust cust on cust.custoid = som.custoid WHERE som.sotype='LEBURAN' and som.sotype2='254' AND som.cmpcode='" & DDLBusUnit.SelectedValue & "' AND (CASE som.somststatus WHEN 'Closed' THEN (CASE som.somstres3 WHEN 'Closed' THEN som.somststatus ELSE 'Approved' END) ELSE som.somststatus END)='Approved' AND " & FilterDDLListSO.SelectedValue & " LIKE '%" & Tchar(FilterTextListSO.Text) & "%' AND ISNULL(som.somstres3, '')='' ORDER BY som.sodate DESC, som.somstoid DESC"
        Else
            typeorder.Text = "stock"
            sSql = "SELECT som.somstoid AS soitemmstoid, som.sono AS soitemno, CONVERT(VARCHAR(10), som.sodate, 101) AS soitemdate, 'Leburan' AS soitemtype, cust.custname,(select distinct orderkadaremas from QL_trnsodtl where som.somstoid = somstoid)orderkadaremas, som.groupoid,'' groupdesc, somstnote FROM QL_trnsomst som inner join ql_mstcust cust on cust.custoid = som.custoid WHERE som.sotype='LEBURAN' and som.sotype2='252' AND som.cmpcode='" & DDLBusUnit.SelectedValue & "' AND (CASE som.somststatus WHEN 'Closed' THEN (CASE som.somstres3 WHEN 'Closed' THEN som.somststatus ELSE 'Approved' END) ELSE som.somststatus END)='Approved' AND " & FilterDDLListSO.SelectedValue & " LIKE '%" & Tchar(FilterTextListSO.Text) & "%' AND ISNULL(som.somstres3, '')='' ORDER BY som.sodate DESC, som.somstoid DESC"
        End If
        FillGV(gvListSO, sSql, "QL_trnsoitemmst")
    End Sub

    Private Sub BindItemData()
        Dim sAdd As String = ""

        'If Session("oid") IsNot Nothing And Session("oid") <> "" Then
        '    sAdd = " AND wod1.womstoid<>" & Session("oid")
        'End If

        'sSql = "SELECT 'False' AS CheckValue,sod.sodtloid AS soitemdtloid, sod.sodtlnote,sod.somstoid AS soitemmstoid,som.sono AS soitemno,som.groupoid, sod.itemoid, i.matrawcode itemcode, 1 AS itemlimitqty, i.matrawlongdesc AS itemlongdesc, sod.sodtlunitoid soitemunitoid, g.gendesc AS soitemunit, 0 bomoid, (ISNULL((SELECT SUM(wod1.wodtl1qty) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid WHERE wod1.cmpcode=sod.cmpcode AND wod1.soitemdtloid=sod.sodtloid AND wom.womststatus<>'Cancel'" & sAdd & "), 0.0)) AS soitemqty,(sod.sodtlqty - ISNULL((SELECT SUM(wod1.wodtl1qty) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid WHERE wod1.cmpcode=sod.cmpcode AND wod1.soitemdtloid=sod.sodtloid AND wom.womststatus<>'Cancel'" & sAdd & "), 0.0)) AS soitemqty_real,(sod.sodtlqty - ISNULL((SELECT SUM(wod1.wodtl1qty) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid WHERE wod1.cmpcode=sod.cmpcode AND wod1.soitemdtloid=sod.sodtloid AND wom.womststatus<>'Cancel'" & sAdd & "), 0.0)) AS soitemqty_real, '' AS itemres2,0.0 precosttotalmatamt,0.0 precostdlcamt,0.0 precostoverheadamt,0.0 precostmarginamt,0.0 precostsalesprice,0.0 curroid_overhead,0.0 curroid_salesprice,0.0 precostoverheadamtidr,0.0 precostsalespriceidr, '' AS lastupduser_bom, getdate() AS lastupdtime_bom,i.matrawcode,i.matrawoldcode itemoldcode,sod.orderkadaremas kadar,0 beratlilin,0 diameter,sod.sodtlqty as qtyproduksi, 0 beratunit,som.weighttotalorder berattotal, 228 typeEmas FROM QL_trnsodtl sod INNER JOIN QL_trnsomst som on som.somstoid=sod.somstoid INNER JOIN QL_mstmatraw i ON i.matrawoid=sod.itemoid INNER JOIN QL_mstgen g ON g.genoid=sod.sodtlunitoid  WHERE sod.cmpcode='" & DDLBusUnit.SelectedValue & "' AND sod.somstoid=" & soitemmstoid.Text & " AND " & FilterDDLListItem.SelectedValue & " LIKE '%" & Tchar(FilterTextListItem.Text) & "%' ORDER BY i.matrawcode"
        If FilterTextListItem.Text <> "" Then
            sAdd = "WHERE " & FilterDDLListItem.SelectedValue & " like '%" & FilterTextListItem.Text & "%' "
        Else
            sAdd = ""
        End If
        sSql = "SELECT 'False' As CheckValue, 0 soitemdtloid, '' sodtlnote, 0 soitemmstoid, 0 soitemno, 0 groupoid, itemoid, itemcode, 1 as itemlimitqty, itemcode_old itemoldcode,itemdesc itemlongdesc, merk,0 soitemunitoid, 0 bomoid, 0 soitemqty, 0 soitemqty_real, '' itemres2, '' AS itemres2,0.0 precosttotalmatamt,0.0 precostdlcamt,0.0 precostoverheadamt,0.0 precostmarginamt,0.0 precostsalesprice,0.0 curroid_overhead,0.0 curroid_salesprice,0.0 precostoverheadamtidr,0.0 precostsalespriceidr, '' AS lastupduser_bom, getdate() AS lastupdtime_bom, 0 matrawcode, 0 matrawoldcode, 0 kadar,0 beratlilin,0 diameter,0 as qtyproduksi, 0 beratunit,0 berattotal, 0 typeEmas FROM ql_mstitem " & sAdd & ""

        FillGV(gvListItem, sSql, "QL_mstitem")
    End Sub

    Private Sub CreateTblDetailItem()
        Dim dtlTable As DataTable = New DataTable("QL_trnwodtl1")
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("womstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wono", Type.GetType("System.String"))
        dtlTable.Columns.Add("soitemmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("soitemno", Type.GetType("System.String"))
        dtlTable.Columns.Add("groupoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("groupdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("soitemdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("soitemqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1note", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl1area", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1totalmatamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1dlcamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1ohdamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("curroid_ohd", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1ohdamtidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1marginamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1precostamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("curroid_precost", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1precostamtidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("lastupduser_bom", Type.GetType("System.String"))
        dtlTable.Columns.Add("lastupdtime_bom", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("itemoldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("beratlilin", Type.GetType("System.Double"))
        dtlTable.Columns.Add("typeemas", Type.GetType("System.Double"))
        dtlTable.Columns.Add("typeemasdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("kadaremas", Type.GetType("System.Double"))
        dtlTable.Columns.Add("diameter", Type.GetType("System.Double"))
        dtlTable.Columns.Add("beratUnit", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyProduksi", Type.GetType("System.Double"))
        'dtlTable.Columns.Add("lastupduser_precost", Type.GetType("System.String"))
        'dtlTable.Columns.Add("lastupdtime_precost", Type.GetType("System.DateTime"))
        'dtlTable.Columns.Add("lastupduser_dlc", Type.GetType("System.String"))
        'dtlTable.Columns.Add("lastupdtime_dlc", Type.GetType("System.DateTime"))
        Session("TblDtlFG") = dtlTable
    End Sub

    Private Sub GenerateDetailFromBOM()
        If Session("TblDtlProcess") Is Nothing Then
            CreateTblDetailProcess()
        End If
        If Session("TblDtlMatLev5") Is Nothing Then
            CreateTblDetailMatLev5()
        End If
        If Session("TblDtlMatLev3") Is Nothing Then
            CreateTblDetailMatLev3()
        End If
        If Session("TblDtlWip") Is Nothing Then
            CreateTblDtlWip()
        End If

        Dim dtProc As DataTable = Session("TblDtlProcess")
        Dim dtMatLev5 As DataTable = Session("TblDtlMatLev5")
        Dim dtMatLev3 As DataTable = Session("TblDtlMatLev3")
        Dim dtlwip As DataTable = Session("TblDtlWip")

        ' Get Process Data From BOM
        sSql = "SELECT bod1.bomdtl1oid, bod1.bomdtl1seq, bod1.bomdtl1deptoid, de.gendesc deptname, bod1.bomdtl1todeptoid,(CASE bod1.bomdtl1reftype WHEN 'WIP' THEN de2.gendesc  ELSE 'END' END) AS todeptname, bod1.bomdtl1reftype, bod1.bomdtl1refunitoid, g.gendesc AS bomdtl1refunit, bod1.bomdtl1res1,0 tukangOid,'' tukang,0 operator1oid,'' operator1,0 operator2oid,'' operator2,0 qualityoid,'' quality,bomWipoid,bomWIPdesc,bomkadarWip,tolerance,typeemas,bomWipcode,bomwipoldcode FROM QL_mstbomdtl1 bod1 INNER JOIN QL_mstGen  de ON de.cmpcode=bod1.cmpcode AND de.genoid =bod1.bomdtl1deptoid  INNER JOIN ql_mstgen de2 ON de2.cmpcode=bod1.cmpcode AND de2.genoid =bod1.bomdtl1todeptoid INNER JOIN QL_mstgen g ON g.genoid=bod1.bomdtl1refunitoid  WHERE bod1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bod1.bomoid=" & bomoid.Text & " ORDER BY bod1.bomdtl1seq"
        Dim dtProcBOM As DataTable = cKon.ambiltabel(sSql, "QL_mstbomdtl1")

        'Get data wip from BOM
        sSql = "select bomdtl3seq,bomdtl1oid,bomdtl3oid,bomdtl1seq, bomdtl1desc,bomdtl3reftype, bomdtl3qty, bomdtl3wgt, bomdtl3unit,bomdtl3refoid,bomdtl3note,bomdtl3code,bomdtl3desc,bomdtl3tolerance,bomdtl3kadaremas,bomdtl3typeemasoid,bomdtl3typeemas,itempictureloc from ("
        sSql &= "select bod3.bomdtl3oid,bod3.bomdtl3seq,bod3.bomdtl1oid,bod1.bomdtl1seq,gen.gendesc bomdtl1desc,bod3.bomdtl3reftype,0 bomdtl3qty,0 bomdtl3wgt,'' bomdtl3unit,bod3.bomdtl3refoid,bod3.bomdtl3note,bod3.bomdtl3code,bod3.bomdtl3desc,bod3.bomdtl3tolerance,bod3.bomdtl3kadaremas,bod3.bomdtl3typeemasoid,bod3.bomdtl3typeemas,i.itempictureloc from QL_mstbomdtl3 bod3 inner join QL_mstbomdtl1 bod1 on bod1.bomdtl1oid=bod3.bomdtl1oid and bod1.bomoid = bod3.bomoid inner join QL_mstitemWip i on i.itemoid = bod3.bomdtl3refoid and i.itemcode = bod3.bomdtl3code inner join ql_mstgen gen on gen.genoid = bod1.bomdtl1deptoid where bod3.bomoid=" & bomoid.Text & ""
        sSql &= ") DT ORDER BY bomdtl1oid "
        Dim dtwip As DataTable = cKon.ambiltabel(sSql, "QL_mstitemwip")
        Dim dvwip As DataView = dtwip.DefaultView


        ' Get Material Level 5 Data From BOM
        sSql = "SELECT bomdtl2seq, bomdtl1oid, bomdtl2oid, bomdtl2reftype, bomdtl2refoid, bomdtl2refshortdesc, bomdtl2refcode, bomdtl2refqty, bomdtl2refunitoid, bomdtl2refunit, bomdtl2refvalue, weight,stock,tolerance FROM ("
        sSql &= "select distinct bod2.bomdtl2seq, bod2.bomdtl1oid, bod2.bomdtl2oid, bod2.bomdtl2reftype, bod2.bomdtl2refoid,(cat1.cat1shortdesc + ' ' + case when cat2.cat2shortdesc = 'None' then '' else cat2.cat2shortdesc end  + ' ' + case when cat3.cat3shortdesc='None' then '' else cat3.cat3shortdesc end)bomdtl2refshortdesc,(cat1.cat1code + '_' + case when cat2.cat2code='None' then '' else cat2.cat2code end + '_' + case when cat3.cat3code ='None' then '' else cat3.cat3code end)bomdtl2refcode, bod2.bomdtl2refqty, bod2.bomdtl2refunitoid,g.gendesc AS bomdtl2refunit,0.0 bomdtl2refvalue , 0.0 weight,0 stock,0 tolerance from QL_mstbomdtl2 bod2 INNER JOIN QL_mstbomdtl1 bod1 ON bod1.cmpcode=bod2.cmpcode AND bod1.bomoid=bod2.bomoid AND bod1.bomdtl1oid=bod2.bomdtl1oid INNER JOIN QL_mstbom bom ON bom.bomoid=bod2.bomoid inner join QL_mstcat3 cat3 on cat3.cat3oid = bod2.bomdtl2refoid inner join QL_mstcat2 cat2 on  cat2.cat2oid = cat3.cat2oid inner join QL_mstcat1 cat1 on cat1.cat1oid = cat3.cat1oid INNER JOIN QL_mstgen g ON g.genoid=bod2.bomdtl2refunitoid WHERE bod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bod2.bomoid=" & bomoid.Text & " "
        sSql &= ") AS tbltemp ORDER BY bomdtl1oid, bomdtl2seq"
        Dim dtMatLev5BOM As DataTable = cKon.ambiltabel(sSql, "QL_mstbomdtl2")
        Dim dvMatLev5BOM As DataView = dtMatLev5BOM.DefaultView

        Dim iCountWip As Integer = dtlwip.Rows.Count + 1
        Dim iCountProc As Integer = dtProc.Rows.Count + 1
        Dim iCountMatLev5 As Integer = dtMatLev5.Rows.Count + 1
        Dim iCountMatLev3 As Integer = dtMatLev3.Rows.Count + 1


        For C1 As Integer = 0 To dtProcBOM.Rows.Count - 1
            ' Added Process Data
            Dim drProc As DataRow = dtProc.NewRow
            drProc("wodtl2seq") = iCountProc + C1
            drProc("wodtl1seq") = wodtl1seq.Text
            drProc("bomdtl1oid") = dtProcBOM.Rows(C1)("bomdtl1oid")
            drProc("wodtl2procseq") = ToInteger(dtProcBOM.Rows(C1)("bomdtl1res1").ToString)
            drProc("wodtl2type") = dtProcBOM.Rows(C1)("bomdtl1reftype").ToString
            drProc("deptoid") = dtProcBOM.Rows(C1)("bomdtl1deptoid")
            drProc("deptname") = dtProcBOM.Rows(C1)("deptname").ToString
            drProc("todeptoid") = dtProcBOM.Rows(C1)("bomdtl1todeptoid")
            drProc("todeptname") = dtProcBOM.Rows(C1)("todeptname").ToString
            ' drProc("wodtl2qty") = ToDouble(wodtl1qty.Text)
            drProc("wodtl2unitoid") = dtProcBOM.Rows(C1)("bomdtl1refunitoid")
            drProc("wodtl2unit") = dtProcBOM.Rows(C1)("bomdtl1refunit").ToString
            drProc("wodtl2note") = ""
            Dim str As String = dtProcBOM.Rows(C1)("deptname").ToString & " "
            '- " & dtProcBOM.Rows(C1)("todeptname").ToString
            drProc("wodtl2desc") = str
            drProc("processlimitqty") = ToDouble(itemlimitqty.Text)
            drProc("wodtl2dlcvalue") = "0.0"
            drProc("wodtl2fohvalue") = "0.0"
            drProc("Tukang") = (dtProcBOM.Rows(C1)("Tukang").ToString)
            drProc("TukangOid") = (dtProcBOM.Rows(C1)("TukangOid").ToString)
            drProc("operator1") = (dtProcBOM.Rows(C1)("operator1").ToString)
            drProc("operator1oid") = (dtProcBOM.Rows(C1)("operator1oid").ToString)
            drProc("operator2") = (dtProcBOM.Rows(C1)("operator2").ToString)
            drProc("operator2oid") = (dtProcBOM.Rows(C1)("operator2oid").ToString)
            drProc("quality") = (dtProcBOM.Rows(C1)("quality").ToString)
            drProc("qualityoid") = (dtProcBOM.Rows(C1)("qualityoid").ToString)
            drProc("bomWipoid") = dtProcBOM.Rows(C1)("bomWipoid")
            drProc("bomWIPdesc") = (dtProcBOM.Rows(C1)("bomWIPdesc").ToString)
            drProc("bomkadarWip") = dtProcBOM.Rows(C1)("bomkadarWip")
            drProc("tolerance") = dtProcBOM.Rows(C1)("tolerance")
            drProc("typeemas") = dtProcBOM.Rows(C1)("typeemas")
            drProc("bomWipcode") = (dtProcBOM.Rows(C1)("bomWipcode").ToString)
            drProc("bomwipoldcode") = (dtProcBOM.Rows(C1)("bomwipoldcode").ToString)
            drProc("beratmaxoutput") = 0
            drProc("qtyoutput") = 0
            drProc("DateStart") = "1/1/1900 00:00:00"
            drProc("DateFinish") = "1/1/1900 00:00:00"

            dtProc.Rows.Add(drProc)
            DDLProcess.Items.Add(str)
            DDLProcess.Items.Item(DDLProcess.Items.Count - 1).Value = iCountProc + C1
            DDLProcess2.Items.Add(str)
            DDLProcess2.Items.Item(DDLProcess2.Items.Count - 1).Value = iCountProc + C1


            dvwip.RowFilter = "bomdtl1oid=" & dtProcBOM.Rows(C1)("bomdtl1oid") & " "
            For C4 As Integer = 0 To dvwip.Count - 1
                Dim drwip As DataRow = dtlwip.NewRow
                drwip("wipseq") = iCountWip + C4
                Dim d As String = dtwip.Rows(C4)("bomdtl1oid")
                'drwip("bomdtl1oid") = dtProc.Rows(C1)("bomdtl1oid")
                drwip("wodtl2seq") = iCountProc + C1
                drwip("wodtl1seq") = wodtl1seq.Text
                drwip("bomdtl3seq") = dtwip.Rows(C4)("bomdtl3seq")
                'drwip("bomdtl1seq") = dtwip.Rows(C4)("bomdtl1seq")
                drwip("bomdtl3oid") = dtwip.Rows(C4)("bomdtl3oid")
                drwip("bomdtl1desc") = str
                drwip("bomdtl3reftype") = dtwip.Rows(C4)("bomdtl3reftype")
                drwip("bomdtl3qty") = dtwip.Rows(C4)("bomdtl3qty")
                drwip("bomdtl3wgt") = dtwip.Rows(C4)("bomdtl3wgt")
                drwip("bomdtl3unit") = dtwip.Rows(C4)("bomdtl3unit")
                Dim c As String = dtwip.Rows(C4)("bomdtl3refoid")
                drwip("bomdtl3refoid") = dtwip.Rows(C4)("bomdtl3refoid")
                drwip("bomdtl3note") = dtwip.Rows(C4)("bomdtl3note")
                Dim b As String = dtwip.Rows(C4)("bomdtl3code")
                drwip("bomdtl3code") = dtwip.Rows(C4)("bomdtl3code")
                Dim a As String = dtwip.Rows(C4)("bomdtl3desc")
                drwip("bomdtl3desc") = dtwip.Rows(C4)("bomdtl3desc")
                drwip("bomdtl3tolerance") = dtwip.Rows(C4)("bomdtl3tolerance")
                drwip("bomdtl3kadaremas") = dtwip.Rows(C4)("bomdtl3kadaremas")
                drwip("bomdtl3typeemasoid") = dtwip.Rows(C4)("bomdtl3typeemasoid")
                drwip("bomdtl3typeemas") = dtwip.Rows(C4)("bomdtl3typeemas")
                drwip("itempictureloc") = dtwip.Rows(C4)("itempictureloc")
                dtlwip.Rows.Add(drwip)
            Next
            iCountWip += dvwip.Count
            dvwip.RowFilter = ""


            ' Added Material Level 5 Data
            dvMatLev5BOM.RowFilter = "bomdtl1oid=" & dtProcBOM.Rows(C1)("bomdtl1oid")
            For C2 As Integer = 0 To dvMatLev5BOM.Count - 1
                Dim drMat As DataRow = dtMatLev5.NewRow
                drMat("wodtl3seq") = iCountMatLev5 + C2
                drMat("wodtl1seq") = wodtl1seq.Text
                drMat("wodtl2seq") = iCountProc + C1
                drMat("wodtl2desc") = str
                drMat("bomdtl2oid") = dvMatLev5BOM(C2)("bomdtl2oid")
                drMat("wodtl3reftype") = dvMatLev5BOM(C2)("bomdtl2reftype").ToString
                drMat("wodtl3refoid") = dvMatLev5BOM(C2)("bomdtl2refoid")
                drMat("wodtl3refshortdesc") = dvMatLev5BOM(C2)("bomdtl2refshortdesc").ToString
                drMat("wodtl3refcode") = dvMatLev5BOM(C2)("bomdtl2refcode").ToString
                drMat("wodtl3qty") = "0.0"
                ' drMat("weight") = ToMaskEdit((ToDouble(dvMatLev5BOM(C2)("weight").ToString) * ToDouble(wodtl1qty.Text)), 2)
                drMat("wodtl3unitoid") = dvMatLev5BOM(C2)("bomdtl2refunitoid")
                drMat("wodtl3unit") = dvMatLev5BOM(C2)("bomdtl2refunit").ToString
                drMat("wodtl3note") = ""
                drMat("wodtl3qtyref") = ToDouble(dvMatLev5BOM(C2)("bomdtl2refqty").ToString)
                drMat("wodtl3value") = ToDouble(dvMatLev5BOM(C2)("bomdtl2refvalue").ToString)
                drMat("stock") = ToDouble(dvMatLev5BOM(C2)("stock").ToString)
                drMat("tolerance") = ToDouble(dvMatLev5BOM(C2)("tolerance").ToString)
                drMat("qtybom") = ToDouble(dvMatLev5BOM(C2)("bomdtl2refqty").ToString)
                'drMat("typemat") = ToDouble(dvMatLev5BOM(C2)("typemat").ToString)
                'drMat("typeemas") = ToDouble(dvMatLev5BOM(C2)("typeemas").ToString)
                'drMat("matkadaremas") = ToDouble(dvMatLev5BOM(C2)("matkadaremas").ToString)
                'drMat("wodtl3qty") = Math.Ceiling(ToDouble(dvMatLev5BOM(C2)("bomdtl2refqty").ToString) * ToDouble(wodtl1qty.Text))

                dtMatLev5.Rows.Add(drMat)
            Next
            iCountMatLev5 += dvMatLev5BOM.Count
            dvMatLev5BOM.RowFilter = ""
        Next

        Session("TblDtlProcess") = dtProc
        GVDtl2.DataSource = Session("TblDtlProcess")
        GVDtl2.DataBind()
        ClearDetailProcess()
        Session("TblDtlMatLev5") = dtMatLev5
        GVDtl3.DataSource = Session("TblDtlMatLev5")
        GVDtl3.DataBind()
        ClearDetailMaterialLevel5()
        GvDtlWip.DataSource = Session("TblDtlWip")
        GvDtlWip.DataBind()
        ClearDtlWip()
        'Session("TblDtlMatLev3") = dtMatLev3
        'GVDtl4.DataSource = Session("TblDtlMatLev3")
        'GVDtl4.DataBind()
        'ClearDetailMaterialLevel3()
    End Sub

    Private Sub CreateTblDetailProcess()
        Dim dtlTable As DataTable = New DataTable("QL_trnwodtl2")
        dtlTable.Columns.Add("wodtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2procseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2type", Type.GetType("System.String"))
        dtlTable.Columns.Add("deptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("deptname", Type.GetType("System.String"))
        dtlTable.Columns.Add("todeptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("todeptname", Type.GetType("System.String"))
        dtlTable.Columns.Add("operator1", Type.GetType("System.String"))
        dtlTable.Columns.Add("operator1oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("operator2", Type.GetType("System.String"))
        dtlTable.Columns.Add("operator2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("quality", Type.GetType("System.String"))
        dtlTable.Columns.Add("qualityoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2note", Type.GetType("System.String"))
        Session("TblDtlProcess") = dtlTable
    End Sub

    Private Sub CreateTblDetailMatLev5()
        Dim dtlTable As DataTable = New DataTable("QL_trnwodtl3")
        dtlTable.Columns.Add("wodtl3seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3refshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3qtybiji", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3note", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3qtyref", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3value", Type.GetType("System.Double"))
        dtlTable.Columns.Add("weight", Type.GetType("System.Double"))
        'dtlTable.Columns.Add("typemat", Type.GetType("System.String"))
        'dtlTable.Columns.Add("typeemas", Type.GetType("System.Int32"))
        'dtlTable.Columns.Add("matkadaremas", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stock", Type.GetType("System.Double"))
        dtlTable.Columns.Add("tolerance", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtybom", Type.GetType("System.Double"))

        Session("TblDtlMatLev5") = dtlTable
    End Sub

    Private Sub CreateTblDetailMatLev3()
        Dim dtlTable As DataTable = New DataTable("QL_trnwodtl4")
        dtlTable.Columns.Add("wodtl4seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl4reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl4refshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl4unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl4unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4note", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4qtyref", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl4value", Type.GetType("System.Double"))
        Session("TblDtlMatLev3") = dtlTable
    End Sub

    Private Sub CreateTblDtlWip()
        Dim dtlTable As DataTable = New DataTable("QL_mstitemwip")
        dtlTable.Columns.Add("wipseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3code", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bomdtl3wgt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bomdtl3unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3note", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3tolerance", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bomdtl3kadaremas", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bomdtl3typeemasoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3typeemas", Type.GetType("System.String"))
        dtlTable.Columns.Add("itempictureloc", Type.GetType("System.String"))

        Session("TblDtlWip") = dtlTable
    End Sub

    Private Sub ClearDetailProcess()
        wodtl2seq.Text = "1"
        If Session("TblDtlProcess") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlProcess")
            wodtl2seq.Text = objTable.Rows.Count + 1
        End If
        i_u4.Text = "New Detail"
        wodtl2procseq.Text = ""
        wodtl2type.SelectedIndex = -1
        deptoid.SelectedIndex = -1
        deptoid.Enabled = False
        deptoid.CssClass = "inpTextDisabled"
        todeptoid.SelectedIndex = -1
        todeptoid.Enabled = False
        todeptoid.CssClass = "inpTextDisabled"
        DDLop1.SelectedIndex = -1
        DDLop2.SelectedIndex = -1
        DDLqa.SelectedIndex = -1
        'If wodtl2type.SelectedValue = "FG" Then
        '    lbltodept.Visible = False : septtodept.Visible = False : todeptoid.Visible = False
        'Else
        '    lbltodept.Visible = True : septtodept.Visible = True : todeptoid.Visible = True
        '    lbltodept.CssClass = True : septtodept.Visible = True : todeptoid.Visible = True
        'End If
        wodtl2qty.Text = ""
        wodtl2unitoid.SelectedIndex = -1
        wodtl2note.Text = ""
        wodtl2desc.Text = ""
        noteprocess.Text = ""
        GVDtl2.SelectedIndex = -1
        beratoutput.Text = ""
        output.Text = ""
        kodelamadtl1.Text = ""
        kodebarudtl1.Text = ""
        tolerance.Text = ""
        InitAllDDL()
    End Sub

    Private Sub ClearDetailMaterialLevel5()
        wodtl3seq.Text = "1"
        If Session("TblDtlMatLev5") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlMatLev5")
            wodtl3seq.Text = objTable.Rows.Count + 1
        End If
        i_u3.Text = "New Detail"
        DDLProcess.SelectedIndex = -1
        wodtl3reftype.SelectedIndex = -1
        wodtl3refoid.Text = ""
        wodtl3refshortdesc.Text = ""
        wodtl3refcode.Text = ""
        wodtl3qty.Text = ""
        qtypakaibiji.Text = ""
        wodtl3unitoid.SelectedIndex = -1
        wodtl3note.Text = ""
        wodtl3qtyref.Text = ""
        qtypakai.Text = ""
        tolerancedtl3.Text = ""
        stockMat3.Text = ""
        kadaremasdtl2.Text = ""
        QtyBOM.Text = ""
        GVDtl3.SelectedIndex = -1
        findmat2.Visible = True
    End Sub

    Private Sub ClearDetailMaterialLevel3()
        wodtl4seq.Text = "1"
        If Session("TblDtlMatLev3") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlMatLev3")
            wodtl4seq.Text = objTable.Rows.Count + 1
        End If
        i_u5.Text = "New Detail"
        DDLProcess2.SelectedIndex = -1
        wodtl4reftype.SelectedIndex = -1
        wodtl4refoid.Text = ""
        wodtl4refshortdesc.Text = ""
        wodtl4refcode.Text = ""
        wodtl4qty.Text = ""
        wodtl4unitoid.SelectedIndex = -1
        wodtl4note.Text = ""
        wodtl4qtyref.Text = ""
        GVDtl4.SelectedIndex = -1
    End Sub

    Private Sub UpdateDetailProcess()
        If Not Session("TblDtlProcess") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlProcess")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl1seq=" & wodtl1seq.Text
            If dv.Count > 0 Then
                For C1 As Integer = 0 To dv.Count - 1
                    ' dv(C1)("wodtl2qty") = ToDouble(wodtl1qty.Text)
                Next
            End If
            dv.RowFilter = ""
            Session("TblDtlProcess") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtlProcess")
            GVDtl2.DataBind()
        End If
    End Sub

    Private Sub UpdateDetailMatLevel5(ByVal dQty As Double, Optional ByVal sType As String = "")
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            If sType = "" Then
                dv.RowFilter = "wodtl1seq=" & wodtl1seq.Text
            Else
                dv.RowFilter = "wodtl2seq=" & wodtl2seq.Text
            End If
            If dv.Count > 0 Then
                For C1 As Integer = 0 To dv.Count - 1
                    dv(C1)("wodtl3qty") = ToDouble(dv(C1)("wodtl3qtyref").ToString) * dQty
                Next
            End If
            dv.RowFilter = ""
            Session("TblDtlMatLev5") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtlMatLev5")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub UpdateDetailMatLevel3(ByVal dQty As Double, Optional ByVal sType As String = "")
        If Not Session("TblDtlMatLev3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev3")
            Dim dv As DataView = dt.DefaultView
            If sType = "" Then
                dv.RowFilter = "wodtl1seq=" & wodtl1seq.Text
            Else
                dv.RowFilter = "wodtl2seq=" & wodtl2seq.Text
            End If
            If dv.Count > 0 Then
                For C1 As Integer = 0 To dv.Count - 1
                    dv(C1)("wodtl4qty") = ToDouble(dv(C1)("wodtl4qtyref").ToString) * dQty
                Next
            End If
            dv.RowFilter = ""
            Session("TblDtlMatLev3") = dv.ToTable
            GVDtl4.DataSource = Session("TblDtlMatLev3")
            GVDtl4.DataBind()
        End If
    End Sub

    Private Sub ClearDetailFinishGood()
        wodtl1seq.Text = "1"
        If Session("TblDtlFG") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlFG")
            wodtl1seq.Text = objTable.Rows.Count + 1
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        lblmaxqty.Text = ""
        spkno.Text = ""
        soitemmstoid.Text = ""
        'soitemno.Text = ""
        groupoid.Text = ""
        groupdesc.Text = ""
        soitemdtloid.Text = ""
        itemoid.Text = ""
        itemshortdesc.Text = ""
        itemcode.Text = ""
        soitemqty.Text = ""
        ' wodtl1qty.Text = ""
        wodtl1area.Text = ""
        itemlimitqty.Text = ""
        ' wodtl1unitoid.SelectedIndex = -1
        tipeEmas.SelectedIndex = -1
        bomoid.Text = ""
        wodtl1note.Text = ""
        kodelamadtl.Text = ""
        kodebarudtl.Text = ""
        kadarEmas.Text = ""
        beratlilinFG.Text = ""
        diameter.Text = ""
        qtyprod.Text = ""
        berat.Text = ""
        wodtl1totalmatamt.Text = ""
        wodtl1dlcamt.Text = ""
        wodtl1ohdamt.Text = ""
        curroid_ohd.Text = ""
        wodtl1ohdamtidr.Text = ""
        wodtl1marginamt.Text = ""
        wodtl1precostamt.Text = ""
        curroid_precost.Text = ""
        wodtl1precostamtidr.Text = ""

        lastupduser_precost.Text = ""
        lastupdtime_precost.Text = ""
        lastupduser_bom.Text = ""
        lastupdtime_bom.Text = ""
        lastupduser_dlc.Text = ""
        lastupdtime_dlc.Text = ""

        GVDtl1.SelectedIndex = -1
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal
        DDLBusUnit.CssClass = sCss
    End Sub

    Private Sub DeleteDetailProcess(ByVal iSeq As Integer)
        If Not Session("TblDtlProcess") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlProcess")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl1seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                DeleteDetailMatLevel5(CInt(dv(0)("wodtl2seq").ToString))
                DeleteDetailMatLevel3(CInt(dv(0)("wodtl2seq").ToString))
                DeleteDetailWip(CInt(dv(0)("wodtl2seq").ToString))
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                UpdateSeqDetailMatLevel5(CInt(dv(C1)("wodtl2seq").ToString), C1 + 1)
                UpdateSeqDetailMatLevel3(CInt(dv(C1)("wodtl2seq").ToString), C1 + 1)
                UpdateSeqDetailWip(CInt(dv(C1)("wodtl2seq").ToString), C1 + 1)
                dv(C1)("wodtl2seq") = C1 + 1
            Next
            Session("TblDtlProcess") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtlProcess")
            GVDtl2.DataBind()
        End If
    End Sub

    Private Sub DeleteDetailMatLevel5(ByVal iSeq As Integer)
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("wodtl3seq") = C1 + 1
            Next
            Session("TblDtlMatLev5") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtlMatLev5")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub DeleteDetailMatLevel3(ByVal iSeq As Integer)
        If Not Session("TblDtlMatLev3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev3")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("wodtl4seq") = C1 + 1
            Next
            Session("TblDtlMatLev3") = dv.ToTable
            GVDtl4.DataSource = Session("TblDtlMatLev3")
            GVDtl4.DataBind()
        End If
    End Sub

    Private Sub DeleteDetailWip(ByVal iSeq As Integer)
        If Not Session("TblDtlWip") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlWip")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("wipseq") = C1 + 1
            Next
            Session("TblDtlWip") = dv.ToTable
            GvDtlWip.DataSource = Session("TblDtlWip")
            GvDtlWip.DataBind()
        End If
    End Sub

    Private Sub UpdateSeqDetailMatLevel5(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iLastSeq
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("wodtl2seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtlMatLev5") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtlMatLev5")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub UpdateSeqDetailMatLevel3(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtlMatLev3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev3")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iLastSeq
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("wodtl2seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtlMatLev3") = dv.ToTable
            GVDtl4.DataSource = Session("TblDtlMatLev3")
            GVDtl4.DataBind()
        End If
    End Sub

    Private Sub UpdateSeqDetailWip(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtlwip") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlwip")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iLastSeq
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("wodtl2seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtlwip") = dv.ToTable
            GvDtlWip.DataSource = Session("TblDtlwip")
            GvDtlWip.DataBind()
        End If
    End Sub

    Private Sub UpdateSeqDetailProcess(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtlProcess") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlProcess")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl1seq=" & iLastSeq
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("wodtl1seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtlProcess") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtlProcess")
            GVDtl2.DataBind()
        End If
    End Sub

    'Private Sub GenerateWONo()
    '    Dim sNo As String = "SPK-LB" & Format(CDate(wodate.Text), "yyyy.MM") & "-"
    '    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(wono, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnwomst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wono LIKE '" & sNo & "%'"
    '    wono.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    'End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT cmpcode, womstoid, periodacctg, wodate, wono, wodocrefno, womstnote, womststatus, createuser, createtime, upduser, updtime,wostartproduksi,wofinishproduksi,wotype,woordertype, personoid,worevisenote,woflag,tobranch_code FROM QL_trnwomst WHERE womstoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
            womstoid.Text = Trim(xreader("womstoid").ToString)
            PeriodAcctg.Text = Trim(xreader("periodacctg").ToString)
            '  wodate.Text = Format(xreader("wodate"), "MM/dd/yyyy")
            wono.Text = Trim(xreader("wono").ToString)
            wodocrefno.Text = Trim(xreader("wodocrefno").ToString)
            womstnote.Text = Trim(xreader("womstnote").ToString)
            womststatus.Text = Trim(xreader("womststatus").ToString)
            createuser.Text = Trim(xreader("createuser").ToString)
            createtime.Text = Trim(xreader("createtime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
            DDLPIC.SelectedValue = xreader("personoid").ToString
            startProd.Text = Format(xreader("wostartproduksi"), "MM/dd/yyyy")
            finishProd.Text = Format(xreader("wofinishproduksi"), "MM/dd/yyyy")
            typeorder.Text = Trim(xreader("wotype"))
            ddltobranch.SelectedValue = xreader("tobranch_code")
            If typeorder.Text = "customer" Then
                customer.Checked = True
            Else
                stock.Checked = True
            End If

            ddlspk.SelectedValue = Trim(xreader("woflag"))
            typespk.Text = Trim(xreader("woordertype").ToString)
            If typespk.Text = 1 Then
                Stamping.Checked = True
            ElseIf typespk.Text = 2 Then
                Casting.Checked = True
            Else
                all.Checked = True
            End If

            If womststatus.Text = "Revised" Then
                Label11.Visible = True
                Label27.Visible = True
                txtrevise.Visible = True
                txtrevise.CssClass = "inpTextDisabled"
                txtrevise.Text = Trim(xreader("worevisenote").ToString)
            End If
        End While
        xreader.Close()
        conn.Close()

        GenerateDTL1(sOid)
        GenerateDTL2(sOid)
        GenerateDTL3(sOid)
        ''get detail data WIP
        'sSql = "select distinct wod2.wodtl2seq,wod4.wodtl4oid,wod4.wodtl2oid,wod4.wodtl1oid,wod4.womstoid,wod4.wodtl4seq bomdtl3seq,wod4.bomdtl3oid,wod4.bomdtl3seq wipseq,wod4.bomdtl1seq,wod4.wodtl4process bomdtl1desc,wod4.wodtl4reftype bomdtl3reftype,wod4.wodtl4qty bomdtl3qty,wod4.wodtl4weight bomdtl3wgt,wod4.wodtl4unit bomdtl3unit,wod4.wodtl4refoid bomdtl3refoid,wod4.wodtl4note bomdtl3note,i.itemcode bomdtl3code,i.itemlongdesc bomdtl3desc,wod4.wodtl4tolerance bomdtl3tolerance,wod4.wodtl4kadaremas bomdtl3kadaremas,wod4.wodtl4typeemasoid bomdtl3typeemasoid,wod4.wodtl4typeemas bomdtl3typeemas,i.itempictureloc from ql_trnwodtl4 wod4 inner join QL_trnwodtl2 wod2 on wod4.wodtl2oid = wod2.wodtl2oid inner join QL_mstitemWip i on i.itemoid = wod4.wodtl4refoid where wod4.womstoid=" & sOid & ""
        'Dim dtwip As DataTable = cKon.ambiltabel(sSql, "QL_mstitemwip")
        'Dim dvwip As DataView = dtwip.DefaultView
        'For C4 As Integer = 0 To dtwip.Rows.Count - 1
        '    dtwip.Rows(C4)("wipseq") = C4 + 1
        'Next
        'Session("TblDtlWip") = dtwip
        'GvDtlWip.DataSource = Session("TblDtlWip")
        'GvDtlWip.DataBind()

        If womststatus.Text = "Post" Or womststatus.Text = "Closed" Or womststatus.Text = "Cancel" Or womststatus.Text = "Approved" Or womststatus.Text = "In Approval" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            btnApproval.Visible = False
            btnAddToList1.Visible = False
            btnAddToList2.Visible = False
            GVDtl1.Columns(0).Visible = False
            GVDtl1.Columns(GVDtl1.Columns.Count - 1).Visible = False
            GVDtl2.Columns(0).Visible = False
            GVDtl2.Columns(GVDtl2.Columns.Count - 1).Visible = False
            GVDtl3.Columns(0).Visible = False
            lblTrnNo.Text = "KIK No."
            womstoid.Visible = False
            wono.Visible = True
        Else
            btnApproval.Visible = True
        End If
       
        EnableHeader(False)
        ClearDetailFinishGood()
        ClearDetailProcess()
        ClearDetailMaterialLevel5()
        'ClearDetailMaterialLevel3()
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("womstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "PrinOutSPKLeburan.rpt"))
            Dim sWhere As String = ""
            If sOid = "" Then
                sWhere &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If FilterDDL.SelectedIndex = FilterDDL.Items.Count - 1 Then
                    sWhere &= " AND wom.womststatus<>'In Process'"
                End If
                'If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " AND wom.cmpcode='" & CompnyCode & "'"
                'End If
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND wom.bomdate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.bomdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbBusUnit.Checked Then
                    If FilterDDLBusUnit.SelectedValue <> "" Then
                        sWhere &= " AND wom.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
                    End If
                End If
                If cbQtyKIK.Checked Then
                    sWhere &= " AND wod1.wodtl1qty " & FilterDDLQtyKIK.SelectedValue & " " & ToDouble(FilterTextQtyKIK.Text)
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND wom.womststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnWOLeburan.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND wom.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " where wom.womstoid IN (" & sOid & ") "

            End If
            sWhere &= " and wom.woflag ='" & ddlspk.SelectedValue & "'"
            sSql = "select som.sono,wom.wono,wod1.wodtl1oid,wod2.wodtl2oid,wom.wodate,wom.woplandate,g3.gendesc deptname,wod2.wodtl2seq,cus.custname,i.matrawoid itemoid,i.matrawoldcode itemoldcode,i.matrawcode itemcode, i.matrawlongdesc itemlongdesc,i.matrawpictureloc itempictureloc,sod.beratperunit,sod.sodtlqty,sod.weighttotal, sod.orderkadaremas kadaremas,sod.typeemasoid,wod1.wodtl1oid ,g1.gendesc unit,g2.gendesc typeemas,som.somstnote,sod.sodtlnote from ql_trnwomst wom inner join QL_trnwodtl1 wod1 ON wod1.womstoid = wom.womstoid inner join QL_trnwodtl2 wod2 ON wod2.womstoid = wod1.womstoid and wod2.wodtl1oid = wod1.wodtl1oid inner join QL_trnsomst som on som.somstoid = wod1.soitemmstoid inner join QL_trnsodtl sod ON sod.somstoid = wod1.soitemmstoid and sod.sodtloid = wod1.soitemdtloid inner join QL_mstmatraw i ON i.matrawoid = sod.itemoid inner join QL_mstcust cus ON cus.custoid = som.custoid inner join QL_mstgen g1 on g1.genoid = sod.sodtlunitoid inner join QL_mstgen g2 on g2.genoid = sod.typeemasoid and g2.gengroup = 'typemas' inner join QL_mstgen g3 on g3.genoid = wod2.deptoid" & sWhere & ""

            Dim dt As DataTable = CreateDataTableFromSQL(sSql)
            Dim isitablegambar1 As New DataTable
            isitablegambar1.Columns.Add("Imagefg", System.Type.GetType("System.Byte[]"))
            isitablegambar1.Columns.Add("wodtl2oid", System.Type.GetType("System.Int32"))


            Dim fs1 As FileStream = Nothing
            Dim bn1 As BinaryReader = Nothing
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim drg1 As DataRow = isitablegambar1.NewRow
                If (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\Images\FG\" & dt.Rows(i).Item("itempictureloc") & "")) Then
                    fs1 = New FileStream(AppDomain.CurrentDomain.BaseDirectory + "\Images\FG\" & dt.Rows(i).Item("itempictureloc") & "", FileMode.Open)
                Else
                    fs1 = New FileStream(AppDomain.CurrentDomain.BaseDirectory + "\Images\FG\none.jpg", FileMode.Open)
                End If

                bn1 = New BinaryReader(fs1)

                Dim imgbyte(fs1.Length) As Byte
                imgbyte = bn1.ReadBytes(Convert.ToInt32((fs1.Length)))

                drg1("imagefg") = imgbyte
                drg1("wodtl2oid") = dt.Rows(i).Item("wodtl2oid")
                fs1.Close()
                bn1.Close()
                isitablegambar1.Rows.Add(drg1)
            Next


            report.Subreports("Imagefg").SetDataSource(isitablegambar1)
            'report.Subreports("WIPImage").SetDataSource(isitablegambar)
            report.SetDataSource(dt)

            report.SetParameterValue("sWhere", sWhere)
            'report.SetParameterValue("PrintUserID", Session("UserID"))
            'report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 username FROM QL_mstprof WHERE userid='" & Session("UserID") & "'"))
            'cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SPKPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
    End Sub

    Private Sub ClearDtlWip()
        wipseq.Text = "1"
        If Session("TblDtlWip") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlWip")
            wipseq.Text = objTable.Rows.Count + 1
        End If
        i_uwip.Text = "New Detail"
        DDLProcessWip.SelectedIndex = -1
        bomdtl3oid.Text = ""
        bomdtl3seq.Text = ""
        bomdtl1seq.Text = ""
        wodtl2note.Text = ""
        output.Text = ""
        kodebarudtl1.Text = ""
        kadaremasdtl.Text = ""
        tolerance.Text = ""
        wodtl2qty.Text = ""
        beratoutput.Text = ""
        bomdtl3reftype.Text = ""
        bomdtl1desc.Text = ""
        itempictureloc.Text = ""
    End Sub

    Private Sub InitDDLCat01ListMat()
        'Fill DDL Category 1
        sSql = "SELECT cat1code, cat1shortdesc FROM QL_mstcat1 c1 WHERE c1.cmpcode='" & CompnyCode & "' AND c1.activeflag='ACTIVE' "

        sSql &= " AND cat1res1='" & wodtl3reftype.SelectedValue & "'"

        If FillDDL(FilterDDLCat01ListMat, sSql) Then
            InitDDLCat02ListMat()
        Else
            FilterDDLCat02ListMat.Items.Clear()
            FilterDDLCat03ListMat.Items.Clear()
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat02ListMat()
        'Fill DDL Category 2
        sSql = "SELECT cat2code, cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND cat1code='" & FilterDDLCat01ListMat.SelectedValue & "'"

        sSql &= " AND cat2res1='" & wodtl3reftype.SelectedValue & "' AND cat1res1='" & wodtl3reftype.SelectedValue & "'"

        If FillDDL(FilterDDLCat02ListMat, sSql) Then
            InitDDLCat03ListMat()
        Else
            FilterDDLCat03ListMat.Items.Clear()
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat03ListMat()
        'Fill DDL Category 3
        sSql = "SELECT cat3code, cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND cat2code='" & FilterDDLCat02ListMat.SelectedValue & "'"

        sSql &= " AND cat3res1='" & wodtl3reftype.SelectedValue & "' AND cat2res1='" & wodtl3reftype.SelectedValue & "' AND cat1res1='" & wodtl3reftype.SelectedValue & "'"
        FillDDL(FilterDDLCat03ListMat, sSql)
        If FillDDL(FilterDDLCat03ListMat, sSql) Then
            InitDDLCat04ListMat()
        Else
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat04ListMat()
        'Fill DDL Category 4
        sSql = "SELECT cat4code, cat4shortdesc FROM QL_mstcat4 c4 INNER JOIN QL_mstcat3 c3 ON c3.cmpcode=c4.cmpcode AND c3.cat3oid=c4.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c4.cmpcode AND c2.cat2oid=c4.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c4.cmpcode AND c1.cat1oid=c4.cat1oid WHERE c4.cmpcode='" & CompnyCode & "' AND c4.activeflag='ACTIVE' AND cat3code='" & FilterDDLCat03ListMat.SelectedValue & "'"

        sSql &= " AND cat4res1='" & wodtl3reftype.SelectedValue & "' AND cat3res1='" & wodtl3reftype.SelectedValue & "' AND cat2res1='" & wodtl3reftype.SelectedValue & "' AND cat1res1='" & wodtl3reftype.SelectedValue & "'"

        FillDDL(FilterDDLCat04ListMat, sSql)
    End Sub

    Private Sub CreateTblDetailMat()
        Dim dtlTable As DataTable = New DataTable("QL_mstbomdtl3")
        dtlTable.Columns.Add("wodtl3seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3refqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bomdtl3refunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3note", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl2process", Type.GetType("System.String"))
        dtlTable.Columns.Add("roundqtydtl3", Type.GetType("System.Double"))
        dtlTable.Columns.Add("precostdtlrefoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matrefcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3matoldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3kadaremas", Type.GetType("System.String"))
        dtlTable.Columns.Add("stock", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl2desc", Type.GetType("System.String"))

        dtlTable.Columns.Add("wodtl3reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3refshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3note", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3qtyref", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3value", Type.GetType("System.Double"))
        dtlTable.Columns.Add("weight", Type.GetType("System.Double"))
        dtlTable.Columns.Add("typemat", Type.GetType("System.String"))
        dtlTable.Columns.Add("typeemas", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matkadaremas", Type.GetType("System.Int32"))

        Session("TblDtl3") = dtlTable
    End Sub

    Private Sub ClearDetail2()
        'bomdtl2seq.Text = "1"
        'If Session("TblDtlMatLev5") Is Nothing = False Then
        '    Dim objTable As DataTable
        '    objTable = Session("TblDtlMatLev5")
        'End If
        'i_u3.Text = "New Detail"
        'bomdtl2reftype.SelectedIndex = -1
        'bomdtl2refdesc.Text = ""
        'bomdtl2refoid.Text = ""
        'bomdtl2refqty.Text = ""
        'bomdtl2refunitoid.SelectedIndex = -1
        'bomdtl2note.Text = ""
        'roundqtydtl2.Text = ""
        'precostdtlrefoid.Text = ""
        'GVDtl2.SelectedIndex = -1
        'bomdtl2reftype.CssClass = "inpText" : bomdtl2reftype.Enabled = True : btnSearchMat.Visible = True
    End Sub

    Private Sub BindMatData(ByVal sRef As String)

        'If wodtl3reftype.SelectedValue = "WIP" Then
        '    sSql = "SELECT distinct 'False' AS checkvalue,'" & wodtl3refoid.Text & "' cat3oid,substring(m.itemcode,1,2) cat1code,substring(m.itemcode,4,4) cat2code,substring(m.itemcode,9,7) cat3code,substring(m.itemcode,17,4) cat4code,m.itemoid AS matrefoid, m.itemcode AS matrefcode,m.itemoldcode, m.itemlongdesc AS matrefshortdesc, m.itemunitoid AS matrefunitoid, g.gendesc AS matrefunit, 0.0 AS bomdtl3qty,0.0 AS bomdtl3qtybiji,0.0 as tolerance, '' AS bomdtl2note, itemlimitqty AS roundqtydtl2,0 precostdtlrefoid,0 weight,'WIP' typemat,m.typeemas,m.kadarcontoh FROM QL_mstitemwip m INNER JOIN QL_mstcat1 c1 ON LEFT(itemcode, 2)=cat1code INNER JOIN QL_mstgen g ON genoid=itemunitoid WHERE m.cmpcode='" & CompnyCode & "' and cat1res1 like '%" & wodtl3reftype.SelectedValue & "%' and m.itemcode like '" & wodtl3refcode.Text & "%' ORDER BY m.itemcode"
        'Else
        '    sSql = "SELECT distinct 'False' AS checkvalue,'" & wodtl3refoid.Text & "' cat3oid,substring(m.matrawcode,1,2) cat1code,substring(m.matrawcode,4,4) cat2code,substring(m.matrawcode,9,7) cat3code,substring(m.matrawcode,17,4) cat4code,m.matrawoid AS matrefoid, m.matrawcode AS matrefcode,m.matrawoldcode, m.matrawlongdesc AS matrefshortdesc, m.matrawunitoid AS matrefunitoid, g.gendesc AS matrefunit, 0.0 AS bomdtl3qty,0.0 AS bomdtl3qtybiji,0.0 as tolerance, '' AS bomdtl2note, matrawlimitqty AS roundqtydtl2,0 precostdtlrefoid,0 weight,m.typemat,m.typeemas,m.matkadaremas FROM QL_mstmatraw m INNER JOIN QL_mstcat1 c1 ON LEFT(matrawcode, 2)=cat1code INNER JOIN QL_mstgen g ON genoid=matrawunitoid WHERE m.cmpcode='" & CompnyCode & "' and cat1res1 like '%" & wodtl3reftype.SelectedValue & "%' and m.matrawcode like '" & wodtl3refcode.Text & "%' ORDER BY m.matrawcode"
        'End If
        
        sSql = "SELECT DISTINCT 'False' As checkvalue, 0 cat3oid, 0 cat1code, 0 cat2code, 0 cat3code, 0 cat4code, itemoid matrefoid, itemcode matrefcode, '' matrawoldcode, itemdesc matrefshortdesc, 0 matrefunitoid, '' matrefunit, 0.0 AS bomdtl3qty,0.0 AS bomdtl3qtybiji,0.0 as tolerance, '' AS bomdtl2note, 0 roundqtydtl2, 0 precostdtlrefoid,0 weight, statusitem typemat, 0 matkadaremas FROM ql_mstitem"

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = dt
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        Else
            Session("ErrMat") = "Material data can't be found!"
            showMessage(Session("ErrMat"), 2)
        End If
    End Sub

    Private Sub UpdateCheckedMat()
        Dim dv As DataView = Session("TblListMat").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matrefoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("bomdtl3qty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(4).Controls
                            dv(0)("bomdtl3qtybiji") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(6).Controls
                            dv(0)("tolerance") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(7).Controls
                            dv(0)("bomdtl2note") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub showImage(ByVal imageUrl As String)
        Image11.Visible = False
        lblMessage.Visible = False
        lblCaptionpict.Text = "Image Full View"
        Image3.Visible = True
        Image3.ImageUrl = "~/Images/WIP/" & imageUrl
        cProc.SetModalPopUpExtender(beMsgBox, PanelMsgBox, mpeMsgbox, True)
    End Sub

    Sub fillqtywip()
        If Not Session("TblDtlWip") Is Nothing Then
            Dim iError As Int16 = 0
            Dim ContractValue As Decimal = 0
            Dim dAmtdisc As Double = 0
            Dim dAmtbeli As Double = 0
            Dim objTable As DataTable
            objTable = Session("TblDtlWip")
            Try
                For c1 As Integer = 0 To objTable.Rows.Count - 1
                    iError = c1

                    Dim row As System.Web.UI.WebControls.GridViewRow = GvDtlWip.Rows(c1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        'find QTY
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(4).Controls

                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                                Session("bomdtl3qty") = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                                If Session("bomdtl3qty").ToString.Trim = "" Then
                                    Session("bomdtl3qty") = 0
                                Else
                                    Session("bomdtl3qty") = ToDouble(Session("bomdtl3qty"))
                                End If
                            End If
                        Next

                        'update to session datatable
                        Dim dr As DataRow = objTable.Rows(c1)
                        dr.BeginEdit()

                        dr(11) = ToMaskEdit(Session("bomdtl3qty"), 3)
                        ' MsgBox(dr(13))
                        dr.EndEdit()
                    End If
                Next
                Session("TblDtlWip") = objTable
                GvDtlWip.DataSource = objTable
                GvDtlWip.DataBind()
            Catch ex As Exception
                'showMessage(ex.Message, "ERROR", 1)
            End Try
        End If
    End Sub

    Sub fillweightwip()
        If Not Session("TblDtlWip") Is Nothing Then
            Dim iError As Int16 = 0
            Dim ContractValue As Decimal = 0
            Dim dAmtdisc As Double = 0
            Dim dAmtbeli As Double = 0
            Dim objTable As DataTable
            objTable = Session("TblDtlWip")
            Try
                For c1 As Integer = 0 To objTable.Rows.Count - 1
                    iError = c1

                    Dim row As System.Web.UI.WebControls.GridViewRow = GvDtlWip.Rows(c1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        'find QTY
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(5).Controls

                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                                Session("bomdtl3wgt") = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                                If Session("bomdtl3wgt").ToString.Trim = "" Then
                                    Session("bomdtl3wgt") = 0
                                Else
                                    Session("bomdtl3wgt") = ToDouble(Session("bomdtl3wgt"))
                                End If
                            End If
                        Next

                        'update to session datatable
                        Dim dr As DataRow = objTable.Rows(c1)
                        dr.BeginEdit()

                        dr(12) = ToMaskEdit(Session("bomdtl3wgt"), 3)
                        ' MsgBox(dr(13))
                        dr.EndEdit()
                    End If
                Next
                Session("TblDtlWip") = objTable
                GvDtlWip.DataSource = objTable
                GvDtlWip.DataBind()
            Catch ex As Exception
                'showMessage(ex.Message, "ERROR", 1)
            End Try
        End If
    End Sub

    Sub filltolerancewip()
        If Not Session("TblDtlWip") Is Nothing Then
            Dim iError As Int16 = 0
            Dim ContractValue As Decimal = 0
            Dim dAmtdisc As Double = 0
            Dim dAmtbeli As Double = 0
            Dim objTable As DataTable
            objTable = Session("TblDtlWip")
            Try
                For c1 As Integer = 0 To objTable.Rows.Count - 1
                    iError = c1

                    Dim row As System.Web.UI.WebControls.GridViewRow = GvDtlWip.Rows(c1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        'find QTY
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(7).Controls

                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                                Session("bomdtl3tolerance") = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                                If Session("bomdtl3tolerance").ToString.Trim = "" Then
                                    Session("bomdtl3tolerance") = 0
                                Else
                                    Session("bomdtl3tolerance") = ToDouble(Session("bomdtl3tolerance"))
                                End If
                            End If
                        Next

                        'update to session datatable
                        Dim dr As DataRow = objTable.Rows(c1)
                        dr.BeginEdit()

                        dr(18) = ToMaskEdit(Session("bomdtl3tolerance"), 3)
                        ' MsgBox(dr(13))
                        dr.EndEdit()
                    End If
                Next
                Session("TblDtlWip") = objTable
                GvDtlWip.DataSource = objTable
                GvDtlWip.DataBind()
            Catch ex As Exception
                'showMessage(ex.Message, "ERROR", 1)
            End Try
        End If
    End Sub

    Private Sub BindWIP()
        sSql = "SELECT  i.itemoid, itemcode,i.itemoldcode, i.itemlongdesc,i.itemunitoid,i.kadarcontoh,i.beratcontoh,i.itemnote,i.typeemas FROM QL_mstitemWIP i  WHERE i.cmpcode='" & CompnyCode & "' AND " & FilterDDLListWIP.SelectedValue & " LIKE '%" & Tchar(FilterTextListWIP.Text) & "%' AND activeflag='ACTIVE' AND itemoid NOT IN (SELECT itemoid FROM QL_mstbom WHERE cmpcode='" & CompnyCode & "') ORDER BY itemoid"
        FillGV(gvListWIP, sSql, "QL_mstWIP")

        'sSql = "SELECT 'False' AS checkvalue,0 wipseq,  i.itemoid, itemcode,i.itemoldcode, i.itemlongdesc,i.itemunitoid,i.kadarcontoh,i.beratcontoh,i.itemnote,0 depfromid,0 deptoid,'' fromprocess,'' toprocess,0 typeemasoid, '' typeemas, 0 kadaremas, 0 tolerance,0 bomdtl1seq FROM QL_mstitemWIP i  WHERE i.cmpcode='" & CompnyCode & "'  AND " & FilterDDLListWIP1.SelectedValue & " like '%" & Tchar(FilterTextListWIP1.Text) & "%' and activeflag='ACTIVE' "

        Dim dtwip As DataTable = cKon.ambiltabel(sSql, "QL_mstitemwip")

        If dtwip.Rows.Count > 0 Then
            Session("TblListwip") = dtwip
            Session("TblListwipView") = dtwip
            gvListWIP.DataSource = Session("TblListwipView")
            gvListWIP.DataBind()
            mpeListWIP.Show()
        Else
            Session("ErrWip") = "Wip data can't be found!"
            showMessage(Session("ErrWip"), 2)
        End If
    End Sub

    Private Sub UpdateDetailMat(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iLastSeq
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("wodtl2seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtlMatLev5") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtlMatLev5")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub BindListKIK()
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND resd.prodresmstoid<>" & Session("oid")
        End If
        'sSql = "SELECT DISTINCT wom.womstoid, wom.wono, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, ISNULL((SELECT SUM(resd.wodtl1qty) FROM QL_trnwodtl1 resd WHERE resd.cmpcode=wom.cmpcode AND resd.womstoid=wom.womstoid), 0.0) qtyfg, ISNULL((SELECT SUM(resd.prodresqty) FROM QL_trnprodresdtl resd WHERE resd.cmpcode=wom.cmpcode AND resd.prodresdtlstatus<>'Cancel' AND resd.prodrestype='FG' AND resd.womstoid=wom.womstoid), 0.0) AS qtyprodfg FROM QL_trnwomst wom  WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wom.womststatus='Post'  ORDER BY wom.wono"
        sSql = "select prodtl.womstoid,womst.wono,CONVERT(VARCHAR(10), womst.wodate, 101) AS wodate,prodtl.prodresqtybagus + prodtl.prodresqtyrusak qtyprodfg,0.0 qtyfg from QL_trnprodresdtl prodtl inner join QL_trnprodresmst prodmst on prodmst.prodresmstoid = prodtl.prodresmstoid inner join QL_trnwomst womst on womst.womstoid = prodtl.womstoid where prodtl.cmpcode = prodmst.cmpcode and prodtl.prodresdtlstatus<>'Cancel' and prodtl.womstoid = womst.womstoid and womst.womststatus = 'Post' and womst.womstres1 <> 'Closed' and prodmst.deptoid = (select genoid from ql_mstgen where gengroup= 'process' and genother2='karet')"
        Dim dtKIK As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl2")
        'dtKIK.DefaultView.RowFilter = "qtyfg - qtyprodfg > 0"

        If dtKIK.DefaultView.Count > 0 Then
            Session("TblListKIK") = dtKIK.DefaultView.ToTable
            Session("TblListKIKView") = dtKIK.DefaultView.ToTable
            gvListKIK.DataSource = Session("TblListKIKView")
            gvListKIK.DataBind()
            cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
        Else
            cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
            showMessage("Belum ada SPK di Production result dengan proses Precasting karet!", 2)
        End If
    End Sub

    Private Sub ClearWipItem()
        wipoid.Text = ""
        output.Text = ""
        kodebarudtl1.Text = ""
        kadarEmas.Text = 0.0
    End Sub

    Private Sub DeleteDetailMat(ByVal iseq As Integer)
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iseq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("wodtl2seq") = C1 + 1
            Next
            Session("TblDtlMatLev5") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtlMatLev5")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub GenerateDTL1(ByVal sOid As String)
        ' Generate Detail Finish Good
        sSql = "SELECT wod1.wodtl1seq,wod1.womstoid,(select wono from QL_trnwomst where womstoid = wod1.womstoid and woflag = '" & ddlspk.SelectedValue & "')wono, wod1.soitemmstoid,'' groupdesc, wod1.soitemdtloid, wod1.itemoid, i.itemdesc AS itemshortdesc, i.itemcode itemcode,i.itemcode_old itemoldcode, 0.0001 itemlimitqty,0 beratlilin,wod1.typeemasitem typeemas,typeemasdesc,wod1.kadaremasitem kadaremas,wod1.beratperUnit beratunit,wod1.diameter,qtyproduksi, ( ISNULL((SELECT SUM(wodx.wodtl1qty) FROM QL_trnwodtl1 wodx INNER JOIN QL_trnwomst womx ON womx.cmpcode=wodx.cmpcode AND womx.womstoid=wodx.womstoid WHERE wodx.cmpcode=wod1.cmpcode AND wodx.soitemdtloid=wod1.soitemdtloid AND womx.womststatus<>'Cancel' AND wodx.womstoid<>wod1.womstoid), 0)) AS soitemqty, wod1.wodtl1qty, wod1.wodtl1unitoid, g.gendesc AS wodtl1unit, wod1.bomoid, wod1.wodtl1note, wod1.wodtl1area, wodtl1totalmatamt, wodtl1dlcamt, wodtl1ohdamt, curroid_ohd, wodtl1ohdamtidr, wodtl1marginamt, wodtl1precostamt, curroid_precost, wodtl1precostamtidr, lastupduser_precost, lastupdtime_precost, lastupduser_bom, lastupdtime_bom, lastupduser_dlc, lastupdtime_dlc ,itempictureloc itempictureloc FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid INNER JOIN QL_mstgen g ON g.genoid=wod1.wodtl1unitoid WHERE wod1.womstoid=" & sOid & " ORDER BY wod1.wodtl1seq"
        Dim dtFG As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl1")
        For C1 As Integer = 0 To dtFG.Rows.Count - 1
            imgMat.ImageUrl = "~/Images/item/" & dtFG.Rows(C1)("itempictureloc").ToString() & ""
        Next

        'Session("TblDtlFG") = dtFG
        'GVDtl1.DataSource = Session("TblDtlFG")
        'GVDtl1.DataBind()

        Dim mySqlDA2 As New SqlDataAdapter(sSql, conn)
        Dim objDs2 As New DataSet
        mySqlDA2.Fill(objDs2, "data2")
        GVDtl1.DataSource = objDs2.Tables("data2")
        GVDtl1.DataBind()
        Session("TblDtlFG") = objDs2.Tables("data2")
    End Sub

    Private Sub GenerateDTL2(ByVal sOid As String)
        ' Generate Detail Process
        sSql = "SELECT wod2.wodtl2seq, 0 wodtl1seq, wod2.bomdtl1oid, wod2.wodtl2procseq, wod2.wodtl2type, wod2.deptoid, de.gendesc  deptname, wod2.todeptoid, (CASE wod2.wodtl2type WHEN 'WIP' THEN de2.gendesc  ELSE 'END' END) AS todeptname, wod2.wodtl2qty,wod2.wodtl2qty beratmaxoutput, wod2.wodtl2unitoid, '' AS wodtl2unit, wod2.wodtl2note, wod2.wodtl2desc, 1 processlimitqty, wodtl2dlcvalue, wodtl2fohvalue , wod2.tukangoid , isnull(tk.personname,'-') Tukang  ,wod2.datestart ,wod2.datefinish,wod2.operator1oid,wod2.operator1,wod2.operator2oid,wod2.operator2,wod2.qualityoid,wod2.quality,wod2.bomwipoid,wod2.bomwipdesc,wod2.bomkadarwip,wod2.tolerance,wod2.typeemas,wod2.bomwipcode,wod2.bomwipoldcode  FROM QL_trnwodtl2 wod2 INNER JOIN QL_mstGen  de ON de.cmpcode=wod2.cmpcode AND de.genoid =wod2.deptoid INNER JOIN QL_mstGen  de2 ON de2.cmpcode=wod2.cmpcode AND de2.genoid =wod2.todeptoid LEFT join ql_mstperson tk on wod2.tukangoid = tk.personoid WHERE wod2.womstoid=" & sOid & " ORDER BY  wod2.wodtl2seq"
        'Dim dtProc As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl2")
        'For C1 As Integer = 0 To dtProc.Rows.Count - 1
        '    dtProc.Rows(C1)("wodtl2seq") = C1 + 1
        '    DDLProcess.Items.Add(dtProc.Rows(C1)("deptname").ToString)
        '    DDLProcess.Items.Item(DDLProcess.Items.Count - 1).Value = C1 + 1
        '    DDLProcessWip.Items.Add(dtProc.Rows(C1)("deptname").ToString)
        '    DDLProcessWip.Items.Item(DDLProcessWip.Items.Count - 1).Value = C1 + 1
        'Next
        'Session("TblDtlProcess") = dtProc
        'GVDtl2.DataSource = Session("TblDtlProcess")
        'GVDtl2.DataBind()

        Dim mySqlDA2 As New SqlDataAdapter(sSql, conn)
        Dim objDs2 As New DataSet
        mySqlDA2.Fill(objDs2, "data2")
        GVDtl2.DataSource = objDs2.Tables("data2")
        GVDtl2.DataBind()
        Session("TblDtlProcess") = objDs2.Tables("data2")
    End Sub

    Private Sub GenerateDTL3(ByVal sOid As String)
        ' Generate Detail Material Level 5
        sSql = "SELECT DISTINCT wodtl3seq, wodtl1seq, wodtl2seq, wodtl2desc, bomdtl2oid, wodtl3reftype, wodtl3refoid, wodtl3refshortdesc, wodtl3refcode, wodtl3qty,wodtl3qtybiji, wodtl3unitoid, wodtl3unit, wodtl3note, wodtl3qtyref, wodtl3value , weight,typemat,typeemas,matkadaremas,stock,tolerance,qtybom FROM ("
        sSql &= "SELECT DISTINCT wod3.wodtl3seq, 0 wodtl1seq, (wod2.wodtl2seq + (SELECT ISNULL(MAX(wod2x.wodtl2seq), 0) FROM QL_trnwodtl2 wod2x WHERE wod2x.cmpcode=wod2.cmpcode AND wod2x.wodtl1oid<wod2.wodtl1oid AND wod2x.womstoid=wod2.womstoid)) AS wodtl2seq, wod2.wodtl2desc, wod3.bomdtl2oid, wod3.wodtl3reftype, wod3.wodtl3refoid, m.itemdesc AS wodtl3refshortdesc, m.itemcode AS wodtl3refcode, wod3.wodtl3qty,wod3.wodtl3qtybiji, wod3.wodtl3unitoid, g.gendesc AS wodtl3unit, wod3.wodtl3note, 0 AS wodtl3qtyref, wodtl3value , wod3.weight,wod3.typemat,wod3.typeemas,wod3.matkadaremas,wod3.stock,wod3.tolerance,wod3.wodtl3bomqty qtybom FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod3.cmpcode AND wod2.wodtl2oid=wod3.wodtl2oid INNER JOIN QL_mstitem m ON m.itemoid=wod3.wodtl3refoid INNER JOIN QL_mstgen g ON g.genoid=wod3.wodtl3unitoid WHERE  wod3.womstoid=" & sOid
        sSql &= " UNION ALL "
        sSql &= "SELECT DISTINCT wod3.wodtl3seq, 0 wodtl1seq, (wod2.wodtl2seq + (SELECT ISNULL(MAX(wod2x.wodtl2seq), 0) FROM QL_trnwodtl2 wod2x WHERE wod2x.cmpcode=wod2.cmpcode AND wod2x.wodtl1oid<wod2.wodtl1oid AND wod2x.womstoid=wod2.womstoid)) AS wodtl2seq, wod2.wodtl2desc, wod3.bomdtl2oid, wod3.wodtl3reftype, wod3.wodtl3refoid, m.itemdesc AS wodtl3refshortdesc, m.itemcode AS wodtl3refcode, wod3.wodtl3qty,wod3.wodtl3qtybiji, wod3.wodtl3unitoid, g.gendesc AS wodtl3unit, wod3.wodtl3note, 0 AS wodtl3qtyref, wodtl3value , wod3.weight,wod3.typemat,wod3.typeemas,wod3.matkadaremas,wod3.stock,wod3.tolerance,wod3.wodtl3bomqty qtybom FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod3.cmpcode AND wod2.wodtl2oid=wod3.wodtl2oid INNER JOIN QL_mstitem m ON m.itemoid=wod3.wodtl3refoid and wod3.wodtl3reftype = 'WIP' INNER JOIN QL_mstgen g ON g.genoid=wod3.wodtl3unitoid WHERE  wod3.womstoid= " & sOid

        sSql &= ") AS tbltmp ORDER BY wodtl1seq, wodtl2seq, wodtl3seq"
        Dim dtMatLev5 As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl3")
        For C1 As Integer = 0 To dtMatLev5.Rows.Count - 1
            dtMatLev5.Rows(C1)("wodtl3seq") = C1 + 1
        Next
        Session("TblDtlMatLev5") = dtMatLev5
        GVDtl3.DataSource = Session("TblDtlMatLev5")
        GVDtl3.DataBind()

    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnWO.aspx")
        End If
        If checkPagePermission("~\Transaction\trnWO.aspx", Session("Role")) = False Then
            'Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Surat Perintah Kerja"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnSave.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to SAVE this data?');")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        btnApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to SEND APPROVAL this data?');")
        If Not Page.IsPostBack Then
            InitAllDDL()
            InitDDLCabang()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckWOStatus()

            DDLProcess.Items.Clear() : DDLProcess2.Items.Clear()
            wodtl2type_SelectedIndexChanged(Nothing, Nothing)

            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")

            datestart.Text = Format(GetServerTime(), "MM/dd/yyyy")
            DateFinish.Text = Format(GetServerTime(), "MM/dd/yyyy")

            TimeStart.Text = Format(GetServerTime(), "HH:mm:ss")
            TimeFinish.Text = Format(GetServerTime(), "HH:mm:ss")

            MultiView1.SetActiveView(View1) : pnlMatDtl3.Visible = False
            If Stamping.Checked = True Then
                typespk.Text = 1
            ElseIf Casting.Checked = True Then
                typespk.Text = 2
            Else
                typespk.Text = 3
            End If
            If customer.Checked = True Then
                typeorder.Text = "customer"
            Else
                typeorder.Text = "stock"
            End If
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                womstoid.Text = GenerateID("QL_TRNWOMST", CompnyCode)
                ' wodate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                startProd.Text = Format(GetServerTime(), "MM/dd/yyyy")
                finishProd.Text = Format(GetServerTime(), "MM/dd/yyyy")
                womststatus.Text = "In Process"
                PeriodAcctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtlFG") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlFG")
            GVDtl1.DataSource = dt
            GVDtl1.DataBind()
        End If


        If Not Session("TblDtlProcess") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlProcess")
            If dt.Rows.Count <= 0 Then
                GenerateDTL2(Session("oid"))

            Else
                GVDtl2.DataSource = dt
                GVDtl2.DataBind()
            End If


            'GenerateDTL2(Session("oid"))


        End If
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlMatLev5")
          
            GVDtl3.DataSource = dt
            GVDtl3.DataBind()
     
        'dt = Session("TblDtlMatLev5")


        End If
        If Not Session("TblDtlMatLev3") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlMatLev3")
            GVDtl4.DataSource = dt
            GVDtl4.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("ErrMat") Is Nothing And Session("ErrMat") <> "" Then
            Session("ErrMat") = Nothing
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbWOInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbWOInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbBusUnit.Checked = False
        FilterDDLBusUnit.SelectedIndex = -1
        cbQtyKIK.Checked = False
        FilterDDLQtyKIK.SelectedIndex = -1
        FilterTextQtyKIK.Text = ""
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, wom.updtime, GETDATE()) > " & nDays & " AND wom.womststatus='In Process' "
        If checkPagePermission("~\Transaction\trnWO.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND wom.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If FilterDDL.SelectedIndex = FilterDDL.Items.Count - 1 Then
            sSqlPlus &= " AND wom.womststatus<>'In Process'"
        End If
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbBusUnit.Checked Then
            If FilterDDLBusUnit.SelectedValue <> "" Then
                sSqlPlus &= " AND wom.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
            End If
        End If
        If cbQtyKIK.Checked Then
            sSqlPlus &= " AND wod1.wodtl1qty " & FilterDDLQtyKIK.SelectedValue & " " & ToDouble(FilterTextQtyKIK.Text)
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND wom.womststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnWO.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND wom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbBusUnit.Checked = False
        FilterDDLBusUnit.SelectedIndex = -1
        cbQtyKIK.Checked = False
        FilterDDLQtyKIK.SelectedIndex = -1
        FilterTextQtyKIK.Text = ""
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnWO.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND wom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTRN.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
        End If
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
        'btnClearSO_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbkdetWip_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View4)
    End Sub

    Protected Sub lbkDetWip1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View4)
    End Sub

    Protected Sub lbkdetWip3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View4)
    End Sub

    Protected Sub lbkprocess4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View2)
    End Sub

    Protected Sub lbkfg4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub lkbDetProc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDetProc1.Click
        MultiView1.SetActiveView(View2)
    End Sub

    Protected Sub lkbDetMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDetMat1.Click
        MultiView1.SetActiveView(View3)
    End Sub

    Protected Sub lkbDetItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDetItem1.Click
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub lbLevel05_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLevel05.Click
        pnlMatDtl5.Visible = True : pnlMatDtl3.Visible = False
    End Sub

    Protected Sub lbLevel03_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLevel03.Click
        pnlMatDtl5.Visible = False : pnlMatDtl3.Visible = True
    End Sub

    'Protected Sub btnSearchSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSO.Click
    '    If DDLBusUnit.SelectedValue = "" Then
    '        showMessage("Please select Business Unit first!", 2)
    '        Exit Sub
    '    End If
    '    FilterDDLListSO.SelectedIndex = -1 : FilterTextListSO.Text = "" : gvListSO.SelectedIndex = -1
    '    BindSOData()
    '    cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, True)
    'End Sub

    'Protected Sub btnClearSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSO.Click
    '    soitemmstoid.Text = "" : soitemno.Text = "" : groupoid.Text = "" : groupdesc.Text = ""
    '    btnClearItem_Click(Nothing, Nothing)
    'End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub btnAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSO.Click
        FilterDDLListSO.SelectedIndex = -1 : FilterTextListSO.Text = "" : gvListSO.SelectedIndex = -1
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        gvListSO.PageIndex = e.NewPageIndex
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub gvListSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSO.SelectedIndexChanged
        If soitemmstoid.Text <> gvListSO.SelectedDataKey.Item("soitemmstoid").ToString Then
            'btnClearSO_Click(Nothing, Nothing)
        End If
        soitemmstoid.Text = gvListSO.SelectedDataKey.Item("soitemmstoid").ToString
        'soitemno.Text = gvListSO.SelectedDataKey.Item("soitemno").ToString
        groupoid.Text = gvListSO.SelectedDataKey.Item("groupoid").ToString
        groupdesc.Text = gvListSO.SelectedDataKey.Item("groupdesc").ToString
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, False)
        btnSearchItem_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbCloseListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListSO.Click
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, False)
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        'If soitemmstoid.Text = "" Then
        '    showMessage("Please select SO No. first!", 2)
        '    Exit Sub
        'End If
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        BindItemData()
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, True)
    End Sub

    Protected Sub btnClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearItem.Click
        soitemdtloid.Text = "" : itemshortdesc.Text = "" : itemoid.Text = "" : itemcode.Text = "" : itemlimitqty.Text = ""
        'wodtl1unitoid.SelectedIndex = -1 : 
        soitemqty.Text = "" : wodtl1area.Text = ""

        wodtl1totalmatamt.Text = ""
        wodtl1dlcamt.Text = ""
        wodtl1ohdamt.Text = ""
        curroid_ohd.Text = ""
        wodtl1ohdamtidr.Text = ""
        wodtl1marginamt.Text = ""
        wodtl1precostamt.Text = ""
        curroid_precost.Text = ""
        wodtl1precostamtidr.Text = ""

        lastupduser_precost.Text = ""
        lastupdtime_precost.Text = ""
        lastupduser_bom.Text = ""
        lastupdtime_bom.Text = ""
        lastupduser_dlc.Text = ""
        lastupdtime_dlc.Text = ""
    End Sub

    Protected Sub btnFindListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListItem.Click
        BindItemData()
        mpeListItem.Show()
    End Sub

    Protected Sub btnAllListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListItem.Click
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        BindItemData()
        mpeListItem.Show()
    End Sub

    Protected Sub gvListItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListItem.PageIndexChanging
        gvListItem.PageIndex = e.NewPageIndex
        BindItemData()
        mpeListItem.Show()
    End Sub

    Protected Sub gvListItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListItem.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
        'End If
    End Sub

    Protected Sub gvListItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListItem.SelectedIndexChanged
        If itemoid.Text <> gvListItem.SelectedDataKey.Item("itemoid").ToString Then
            btnClearItem_Click(Nothing, Nothing)
        End If

     

        Dim row As System.Web.UI.WebControls.GridViewRow = gvListItem.SelectedRow
        Dim cc2 As System.Web.UI.ControlCollection = row.Cells(4).Controls
        qtyprod.Text = ToDouble(GetTextValue(cc2))

        soitemdtloid.Text = gvListItem.SelectedDataKey("soitemdtloid").ToString().Trim
        itemoid.Text = gvListItem.SelectedDataKey("itemoid").ToString().Trim
        itemcode.Text = gvListItem.SelectedDataKey("itemcode").ToString().Trim
        kodebarudtl.Text = gvListItem.SelectedDataKey("itemcode").ToString().Trim
        kodelamadtl.Text = gvListItem.SelectedDataKey.Item("itemoldcode").ToString
        kadarEmas.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("kadar").ToString), 2)
        beratlilinFG.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("beratlilin").ToString), 2)
        berat.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("beratUnit").ToString), 2)
        ' wodtl1qty.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("berattotal").ToString), 2)
        'qtyprod.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("qtyproduksi").ToString), 2)
        lblmaxqty.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("qtyproduksi").ToString), 2)
        tipeEmas.SelectedValue = gvListItem.SelectedDataKey("typeEmas").ToString().Trim
        itemshortdesc.Text = gvListItem.SelectedDataKey("itemlongdesc").ToString().Trim
        ' wodtl1unitoid.SelectedValue = gvListItem.SelectedDataKey("soitemunitoid").ToString
        itemlimitqty.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("itemlimitqty").ToString), 2)
        soitemqty.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("soitemqty").ToString), 2)
        ' wodtl1qty.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("wodtl1qty").ToString), 2)
        bomoid.Text = gvListItem.SelectedDataKey.Item("bomoid").ToString
        wodtl1area.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("itemres2").ToString), 2)
        wodtl1totalmatamt.Text = ToDouble(gvListItem.SelectedDataKey("precosttotalmatamt").ToString)
        wodtl1dlcamt.Text = ToDouble(gvListItem.SelectedDataKey("precostdlcamt").ToString)
        wodtl1ohdamt.Text = ToDouble(gvListItem.SelectedDataKey("precostoverheadamt").ToString)
        curroid_ohd.Text = ToInteger(gvListItem.SelectedDataKey("curroid_overhead").ToString)
        wodtl1ohdamtidr.Text = ToDouble(gvListItem.SelectedDataKey("precostoverheadamtidr").ToString)
        wodtl1marginamt.Text = ToDouble(gvListItem.SelectedDataKey("precostmarginamt").ToString)
        wodtl1precostamt.Text = ToDouble(gvListItem.SelectedDataKey("precostsalesprice").ToString)
        curroid_precost.Text = ToInteger(gvListItem.SelectedDataKey("curroid_salesprice").ToString)
        wodtl1precostamtidr.Text = ToDouble(gvListItem.SelectedDataKey("precostsalespriceidr").ToString)

        'lastupduser_precost.Text = gvListItem.SelectedDataKey.Item("lastupduser_precost").ToString
        'lastupdtime_precost.Text = gvListItem.SelectedDataKey.Item("lastupdtime_precost").ToString
        lastupduser_bom.Text = gvListItem.SelectedDataKey.Item("lastupduser_bom").ToString
        lastupdtime_bom.Text = gvListItem.SelectedDataKey.Item("lastupdtime_bom").ToString
        'lastupduser_dlc.Text = gvListItem.SelectedDataKey.Item("lastupduser_dlc").ToString
        'lastupdtime_dlc.Text = gvListItem.SelectedDataKey.Item("lastupdtime_dlc").ToString

        'PictureLoc.Text = gvListItem.SelectedDataKey.Item("itempictureloc").ToString
        'imgMat.ImageUrl = "~/Images/item/" & gvListItem.SelectedDataKey.Item("itempictureloc").ToString & ""

        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub lkbCloseListItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListItem.Click
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub btnAddToList1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList1.Click
        If IsDetailItemInputValid() Then
            If Session("TblDtlFG") Is Nothing Then
                CreateTblDetailItem()
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtlFG")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "itemoid=" & itemoid.Text
            Else
                dv.RowFilter = " itemoid=" & itemoid.Text & " AND  wodtl1seq<>" & wodtl1seq.Text
            End If

            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""

            Dim objRow As DataRow
           
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                wodtl1seq.Text = objTable.Rows.Count + 1
                objRow("wodtl1seq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(wodtl1seq.Text - 1)
                objRow.BeginEdit()
            End If
            If ToDouble(qtyprod.Text) <= 0 Then
                showMessage("Masukkan Jumlah QTY!", 2)
                Exit Sub
            Else
                objRow("qtyProduksi") = ToDouble(qtyprod.Text)
            End If
            'objRow("soitemmstoid") = soitemmstoid.Text
            objRow("womstoid") = 0
            objRow("wono") = ""
            ' objRow("soitemno") = soitemno.Text
            'objRow("groupoid") = groupoid.Text
            objRow("groupdesc") = groupdesc.Text
            objRow("soitemdtloid") = 0
            objRow("itemoid") = itemoid.Text
            objRow("itemshortdesc") = itemshortdesc.Text
            objRow("itemcode") = itemcode.Text
            objRow("itemlimitqty") = ToDouble(itemlimitqty.Text)
            objRow("soitemqty") = ToDouble(soitemqty.Text)
            'objRow("wodtl1qty") = ToDouble(wodtl1qty.Text)
            'objRow("wodtl1unitoid") = wodtl1unitoid.SelectedValue
            'objRow("wodtl1unit") = wodtl1unitoid.SelectedItem.Text
            objRow("bomoid") = bomoid.Text
            objRow("wodtl1note") = wodtl1note.Text
            objRow("wodtl1area") = ToDouble(wodtl1area.Text)
            objRow("itemcode") = kodebarudtl.Text
            objRow("itemoldcode") = kodelamadtl.Text
            objRow("beratlilin") = ToDouble(beratlilinFG.Text)
            'objRow("typeemas") = tipeEmas.SelectedValue
            'objRow("typeemasdesc") = tipeEmas.SelectedItem.Text
            objRow("kadaremas") = ToDouble(kadarEmas.Text)
            objRow("diameter") = ToDouble(diameter.Text)
            objRow("beratUnit") = ToDouble(berat.Text)
          
            objRow("wodtl1totalmatamt") = ToDouble(wodtl1totalmatamt.Text)
            objRow("wodtl1dlcamt") = ToDouble(wodtl1dlcamt.Text)
            objRow("wodtl1ohdamt") = ToDouble(wodtl1ohdamt.Text)
            objRow("curroid_ohd") = ToInteger(curroid_ohd.Text)
            objRow("wodtl1ohdamtidr") = ToDouble(wodtl1ohdamtidr.Text)
            objRow("wodtl1marginamt") = ToDouble(wodtl1marginamt.Text)
            objRow("wodtl1precostamt") = ToDouble(wodtl1precostamt.Text)
            objRow("curroid_precost") = ToInteger(curroid_precost.Text)
            objRow("wodtl1precostamtidr") = ToDouble(wodtl1precostamtidr.Text)
            objRow("lastupduser_bom") = lastupduser_bom.Text
            objRow("lastupdtime_bom") = CDate(lastupdtime_bom.Text)
            'objRow("lastupduser_precost") = lastupduser_precost.Text
            'objRow("lastupdtime_precost") = CDate(lastupdtime_precost.Text)
            'objRow("lastupduser_dlc") = lastupduser_dlc.Text
            'objRow("lastupdtime_dlc") = CDate(lastupdtime_dlc.Text)

            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
                'GenerateDetailFromBOM()
            Else
                objRow.EndEdit()
                'UpdateDetailProcess()
                'UpdateDetailMatLevel5(ToDouble(wodtl1qty.Text))
                'UpdateDetailMatLevel3(ToDouble(wodtl1qty.Text))
            End If
            Session("TblDtlFG") = objTable
            GVDtl1.DataSource = objTable
            GVDtl1.DataBind()
            ClearDetailFinishGood()

            imgMat.ImageUrl = "~/Images/item/" & PictureLoc.Text & ""

        End If
    End Sub

    Protected Sub btnClear1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear1.Click
        ClearDetailFinishGood()
    End Sub

    Protected Sub GVDtl1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 2)
            e.Row.Cells(5).Text = NewMaskEdit(ToDouble(e.Row.Cells(5).Text))
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub GVDtl1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl1.RowDeleting
        Dim objTable As DataTable = Session("TblDtlFG")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        DeleteDetailProcess(CInt(objRow(e.RowIndex)("wodtl1seq").ToString))
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            UpdateSeqDetailProcess(CInt(objTable.Rows(C1)("wodtl1seq").ToString), C1 + 1)
            dr.BeginEdit()
            dr("wodtl1seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlFG") = objTable
        GVDtl1.DataSource = Session("TblDtlFG")
        GVDtl1.DataBind()
        ClearDetailFinishGood()

        If GVDtl1.Rows.Count = 0 Then
            Session("TblDtlFG") = Nothing
        End If

    End Sub

    Protected Sub GVDtl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl1.SelectedIndexChanged
        Try
            wodtl1seq.Text = GVDtl1.SelectedDataKey.Item("wodtl1seq").ToString
            If Session("TblDtlFG") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim dt As New DataTable
                dt = Session("TblDtlFG")
                If dt.Rows.Count <= 0 Then
                    GenerateDTL1(Session("oid"))
                End If
                Dim objTable As DataTable = Session("TblDtlFG")

                Dim dv As DataView = objTable.DefaultView

                dv.RowFilter = "wodtl1seq=" & wodtl1seq.Text
               
                lbspkoid.Text = dv.Item(0).Item("womstoid")
                spkno.Text = dv.Item(0).Item("wono").ToString
                soitemmstoid.Text = dv.Item(0).Item("soitemmstoid").ToString
                ' soitemno.Text = dv.Item(0).Item("soitemno").ToString
                'lbspkoid.Text = GetStrData("select wom.womstoid from ql_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 on wom.womstoid = wod1.womstoid where wod1.soitemmstoid = " & dv.Item(0).Item("soitemmstoid") & " and wom.woflag <>'GB'  ")
                'spkno.Text = GetStrData("select wom.wono from ql_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 on wom.womstoid = wod1.womstoid where wod1.soitemmstoid = " & dv.Item(0).Item("soitemmstoid") & " and wom.woflag <>'GB' ")
                'groupoid.Text = dv.Item(0).Item("groupoid").ToString
                groupdesc.Text = dv.Item(0).Item("groupdesc").ToString
                soitemdtloid.Text = dv.Item(0).Item("soitemdtloid").ToString
                itemoid.Text = dv.Item(0).Item("itemoid").ToString
                itemlimitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("itemlimitqty").ToString), 4)
                itemshortdesc.Text = dv.Item(0).Item("itemshortdesc").ToString
                itemcode.Text = dv.Item(0).Item("itemcode").ToString
                soitemqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("soitemqty").ToString), 4)
                ' wodtl1qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl1qty").ToString), 4)
                wodtl1area.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl1area").ToString), 4)
                '   wodtl1unitoid.SelectedValue = dv.Item(0).Item("wodtl1unitoid").ToString
                bomoid.Text = dv.Item(0).Item("bomoid").ToString
                wodtl1note.Text = dv.Item(0).Item("wodtl1note").ToString

                kodebarudtl.Text = dv.Item(0).Item("itemcode").ToString
                kodelamadtl.Text = dv.Item(0).Item("itemoldcode").ToString
                kadarEmas.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("kadaremas").ToString), 2)
                beratlilinFG.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("beratlilin").ToString), 4)
                tipeEmas.SelectedValue = ToMaskEdit(ToDouble(dv.Item(0).Item("typeemas").ToString), 2)
                'tipeEmas.SelectedItem.Text = dv.Item(0).Item("typeemasdesc").ToString
                berat.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("beratUnit").ToString), 4)
                qtyprod.Text = NewMaskEdit(ToDouble(dv.Item(0).Item("qtyProduksi").ToString))
                'lblmaxqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("qtyProduksi").ToString), 4)
                diameter.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("diameter").ToString), 2)

                wodtl1totalmatamt.Text = ToDouble(dv.Item(0).Item("wodtl1totalmatamt").ToString)
                wodtl1dlcamt.Text = ToDouble(dv.Item(0).Item("wodtl1dlcamt").ToString)
                wodtl1ohdamt.Text = ToDouble(dv.Item(0).Item("wodtl1ohdamt").ToString)
                curroid_ohd.Text = ToInteger(dv.Item(0).Item("curroid_ohd").ToString)
                wodtl1ohdamtidr.Text = ToDouble(dv.Item(0).Item("wodtl1ohdamtidr").ToString)
                wodtl1marginamt.Text = ToDouble(dv.Item(0).Item("wodtl1marginamt").ToString)
                wodtl1precostamt.Text = ToDouble(dv.Item(0).Item("wodtl1precostamt").ToString)
                curroid_precost.Text = ToInteger(dv.Item(0).Item("curroid_precost").ToString)
                wodtl1precostamtidr.Text = ToDouble(dv.Item(0).Item("wodtl1precostamtidr").ToString)

                lastupduser_bom.Text = dv.Item(0).Item("lastupduser_bom").ToString
                lastupdtime_bom.Text = dv.Item(0).Item("lastupdtime_bom").ToString
                'lastupduser_precost.Text = dv.Item(0).Item("lastupduser_precost").ToString
                'lastupdtime_precost.Text = dv.Item(0).Item("lastupdtime_precost").ToString
                'lastupduser_dlc.Text = dv.Item(0).Item("lastupduser_dlc").ToString
                'lastupdtime_dlc.Text = dv.Item(0).Item("lastupdtime_dlc").ToString

                dv.RowFilter = ""
                
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub
    Private Sub UpdateCheckedItem()
        Dim dv As DataView = Session("TblDtlFG").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListItem.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "womstoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("bomdtl3qty") = ToDouble(GetTextValue(cc2))

                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub
    Protected Sub btnAddToList2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList2.Click

        If IsDetailProcessInputValid() Then
            If Session("TblDtlProcess") Is Nothing Then
                CreateTblDetailProcess()
            End If
            Dim objtable As DataTable
            objtable = Session("TblDtlProcess")
            Dim dv As DataView = objtable.DefaultView
            If i_u4.Text = "New Detail" Then
                dv.RowFilter = "deptoid=" & deptoid.SelectedValue
            Else
                dv.RowFilter = "deptoid=" & deptoid.SelectedValue & " AND wodtl2seq<>" & wodtl2seq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u4.Text = "New Detail" Then
                objRow = objtable.NewRow()
                wodtl2seq.Text = objtable.Rows.Count + 1
                objRow("wodtl2seq") = objtable.Rows.Count + 1
            Else
                objRow = objtable.Rows(wodtl2seq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("wodtl2procseq") = wodtl2seq.Text
            objRow("wodtl2type") = wodtl2type.SelectedItem.Text
            objRow("deptoid") = deptoid.SelectedValue
            objRow("deptname") = deptoid.SelectedItem.Text

            If todeptoid.SelectedItem.Text = "FINISHING" Then
                objRow("todeptoid") = 128
                objRow("todeptname") = "END"
            Else
                objRow("todeptoid") = todeptoid.SelectedValue
                objRow("todeptname") = todeptoid.SelectedItem.Text
            End If
            Dim str As String = deptoid.SelectedItem.Text
            objRow("operator1") = DDLop1.SelectedItem.Text
            objRow("operator1oid") = DDLop1.SelectedValue
            objRow("operator2") = DDLop2.SelectedItem.Text
            objRow("operator2oid") = DDLop2.SelectedValue
            objRow("quality") = DDLqa.SelectedItem.Text
            objRow("qualityoid") = DDLqa.SelectedValue
            objRow("wodtl2note") = Tchar(noteprocess.Text).ToString
            If i_u4.Text = "New Detail" Then
                objtable.Rows.Add(objRow)
                DDLProcess.Items.Add(str)
                DDLProcess.Items.Item(DDLProcess.Items.Count - 1).Value = wodtl2seq.Text
            Else
                objRow.EndEdit()
            End If
            GVDtl2.DataSource = Session("TblDtlProcess")
            GVDtl2.DataBind()
            ClearDetailProcess()
        End If

    End Sub

    Protected Sub btnClear2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear2.Click
        ClearDetailProcess()
    End Sub

    Protected Sub GVDtl2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub GVDtl2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl2.RowDeleting
        Dim objTable As DataTable = Session("TblDtlProcess")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim iSeq As Integer = objTable.Rows(e.RowIndex)("wodtl2seq")
        If Session("TblDtlMatLev5") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iSeq
            If dv.Count > 0 Then
                showMessage("Please remove all material that using this data first before removing this data!", 2) : dv.RowFilter = "" : Exit Sub
            End If
            dv.RowFilter = ""
        End If
        If Session("TblDtlWip") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlWip")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iSeq
            If dv.Count > 0 Then
                showMessage("Please remove all material that using this data first before removing this data!", 2) : dv.RowFilter = "" : Exit Sub
            End If
            dv.RowFilter = ""
        End If
        objTable.Rows.Remove(objRow(e.RowIndex))
        DeleteDetailMat(iSeq)
        DDLProcess.Items.RemoveAt(e.RowIndex)
        DDLProcessWip.Items.RemoveAt(e.RowIndex)
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            DDLProcess.Items(C1).Value = C1 + 1
            DDLProcessWip.Items(C1).Value = C1 + 1
            UpdateDetailMat(CInt(objTable.Rows(C1)("wodtl2seq").ToString), C1 + 1)
            dr.BeginEdit()
            dr("wodtl2seq") = C1 + 1
            dr.EndEdit()
        Next

        Session("TblDtlProcess") = objTable
        GVDtl2.DataSource = Session("Process")
        GVDtl2.DataBind()
        ClearDetailProcess()
    End Sub

    Protected Sub GVDtl2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl2.SelectedIndexChanged
        Try
            wodtl2seq.Text = GVDtl2.SelectedDataKey.Item("wodtl2seq").ToString
            If Session("TblDtlProcess") Is Nothing = False Then
                i_u4.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlProcess")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "wodtl2seq=" & wodtl2seq.Text
                wodtl2procseq.Text = dv.Item(0).Item("wodtl2procseq").ToString
                wodtl2type.SelectedValue = dv.Item(0).Item("wodtl2type").ToString
                deptoid.SelectedValue = dv.Item(0).Item("deptoid").ToString
                initdeptoid()
                'todeptoid.SelectedValue = dv.Item(0).Item("todeptoid").ToString
                If wodtl2type.SelectedValue = "216" Then
                    todeptoid.Visible = False
                End If
                wodtl2qty.Text = 0
                noteprocess.Text = dv.Item(0).Item("wodtl2note").ToString
                wodtl2procseq_TextChanged(Nothing, Nothing)
                wodtl2type_SelectedIndexChanged(Nothing, Nothing)
                If ToDouble(dv.Item(0).Item("operator1oid").ToString) <> 0 Then
                    DDLop1.SelectedValue = dv.Item(0).Item("operator1oid").ToString
                End If
                If ToDouble(dv.Item(0).Item("operator2oid").ToString) <> 0 Then
                    DDLop2.SelectedValue = dv.Item(0).Item("operator2oid").ToString
                End If
                If ToDouble(dv.Item(0).Item("qualityoid").ToString) <> 0 Then
                    DDLqa.SelectedValue = dv.Item(0).Item("qualityoid").ToString
                End If
                'output.Text = dv.Item(0).Item("bomWIPdesc").ToString
                'kodelamadtl1.Text = dv.Item(0).Item("bomwipoldcode").ToString
                'kodebarudtl1.Text = dv.Item(0).Item("bomWipcode").ToString
                'If (dv.Item(0).Item("typeemas").ToString) <> 0 Then
                '    typeEmasdtl1.SelectedValue = dv.Item(0).Item("typeemas").ToString
                'End If
                'tolerance.Text = dv.Item(0).Item("tolerance").ToString
                'wodtl2desc.Text = dv.Item(0).Item("wodtl2desc").ToString
                'kadaremasdtl.Text = dv.Item(0).Item("bomkadarWip").ToString
                'beratoutput.Text = dv.Item(0).Item("beratmaxoutput").ToString
                'processlimitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("processlimitqty").ToString), 2)
                'wodtl2unitoid.SelectedValue = dv.Item(0).Item("wodtl2unitoid").ToString
                'If ToDouble(dv.Item(0).Item("tukangoid").ToString) <> 0 Then
                '    tukang.SelectedValue = dv.Item(0).Item("tukangoid").ToString
                'End If
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnClear3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear3.Click
        ClearDetailMaterialLevel5()
    End Sub

    Protected Sub GVDtl3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl3.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 1)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl3.SelectedIndexChanged
        Try
            wodtl3seq.Text = GVDtl3.SelectedDataKey.Item("wodtl3seq").ToString
            If Session("TblDtlMatLev5") Is Nothing = False Then
                i_u3.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlMatLev5")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "wodtl3seq=" & wodtl3seq.Text
                DDLProcess.SelectedItem.Text = dv.Item(0).Item("wodtl2desc").ToString
                'wodtl3reftype.SelectedValue = dv.Item(0).Item("wodtl3reftype").ToString
                wodtl3refoid.Text = dv.Item(0).Item("wodtl3refoid").ToString
                wodtl3refshortdesc.Text = dv.Item(0).Item("wodtl3refshortdesc").ToString
                wodtl3refcode.Text = dv.Item(0).Item("wodtl3refcode").ToString
                FilterTextListMat2.Text = dv.Item(0).Item("wodtl3refcode").ToString
                'FilterTextListMat2.CssClass = "inpTextDisabled"
                'FilterTextListMat2.Enabled = False

                wodtl3qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl3qty").ToString), 4)
                qtypakai.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl3qty").ToString), 4)
                qtypakai.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl3qtybiji").ToString), 1)
                'wodtl3unitoid.SelectedValue = dv.Item(0).Item("wodtl3unitoid").ToString
                wodtl3note.Text = dv.Item(0).Item("wodtl3note").ToString
                wodtl3qtyref.Text = ToDouble(dv.Item(0).Item("wodtl3qtyref").ToString)
                weightMat.Text = ToDouble(dv.Item(0).Item("weight").ToString)
                'typeEmasdtl2.SelectedValue = ToDouble(dv.Item(0).Item("typeemas").ToString)
                'kadaremasdtl2.Text = ToDouble(dv.Item(0).Item("matkadaremas").ToString)
                QtyBOM.Text = ToDouble(dv.Item(0).Item("qtybom").ToString)
                stockMat3.Text = ToDouble(dv.Item(0).Item("stock").ToString)
                tolerancedtl3.Text = ToDouble(dv.Item(0).Item("tolerance").ToString)
                dv.RowFilter = ""
                btnlist3.Visible = True
                findmat2.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnClear4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear4.Click
        ClearDetailMaterialLevel3()
    End Sub

    Protected Sub GVDtl4_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl4.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
        End If
    End Sub

    Protected Sub GVDtl4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl4.SelectedIndexChanged
        Try
            wodtl4seq.Text = GVDtl4.SelectedDataKey.Item("wodtl4seq").ToString
            If Session("TblDtlMatLev3") Is Nothing = False Then
                i_u5.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlMatLev3")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "wodtl4seq=" & wodtl4seq.Text
                DDLProcess2.SelectedValue = dv.Item(0).Item("wodtl2seq").ToString
                wodtl4reftype.SelectedValue = dv.Item(0).Item("wodtl4reftype").ToString
                wodtl4refoid.Text = dv.Item(0).Item("wodtl4refoid").ToString
                wodtl4refshortdesc.Text = dv.Item(0).Item("wodtl4refshortdesc").ToString
                wodtl4refcode.Text = dv.Item(0).Item("wodtl4refcode").ToString
                wodtl4qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl4qty").ToString), 2)
                wodtl4unitoid.SelectedValue = dv.Item(0).Item("wodtl4unitoid").ToString
                wodtl4note.Text = dv.Item(0).Item("wodtl4note").ToString
                wodtl4qtyref.Text = ToDouble(dv.Item(0).Item("wodtl4qtyref").ToString)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            wodtl1oid.Text = GenerateID("QL_TRNWODTL1", CompnyCode)
            wodtl2oid.Text = GenerateID("QL_TRNWODTL2", CompnyCode)
            wodtl3oid.Text = GenerateID("QL_TRNWODTL3", CompnyCode)

            Dim dTotalDLC As Double = 0, dTotalFOH As Double = 0
            If Session("TblDtlProcess") Is Nothing Then
                showMessage("Detail Proses Tidak Boleh Kosong", 2)
                Exit Sub
            End If
         

            If Session("TblDtlMatLev5") IsNot Nothing Then
                Dim dtMatLev5 As DataTable = Session("TblDtlMatLev5")
                Dim dvMatLev5 As DataView = dtMatLev5.DefaultView
                For Cek As Integer = 0 To dvMatLev5.Count - 1
                    If dvMatLev5(0)("wodtl3qtybiji") = 0 Then
                        showMessage("Qty yang di isi harus lebih dari 0!", 2)
                        dvMatLev5.RowFilter = ""
                        Exit Sub
                    End If
                Next
            Else
                showMessage("Detail Material Tidak Boleh Kosong", 2)
                Exit Sub
            End If

            If checkApproval("QL_TRNORDERMST", "In Approval", Session("oid"), "New", "FINAL", Session("branch_id")) > 0 Then
                showMessage("SPK ini sudah send for Approval", 2)
                Exit Sub
            End If

            '--------------------------------------------------------------------------------------------------------------------------------
            If womststatus.Text = "In Approval" Then
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus " & _
                                                  "from QL_approvalstructure " & _
                                                  "WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNWOMST' order by approvallevel"


                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                Else
                    showMessage("Tidak ada User untuk Approved SPK, Silahkan hubungi admin dahulu", 2)
                    Exit Sub
                End If


                Dim objTransApp As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTransApp = conn.BeginTransaction()
                xCmd.Transaction = objTransApp

                Session("AppOid") = ClassFunction.GenerateID("QL_Approval", CompnyCode)
                Try
                    If Session("oid") <> Nothing Or Session("oid") <> "" Then
                        If Not Session("TblApproval") Is Nothing Then
                            Dim objTable As DataTable : objTable = Session("TblApproval")
                            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                                sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,branch_code,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus) VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", '" & Session("branch_id") & "','" & "SPK" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_TRNWOMST', '" & Session("oid") & "','In Approval','0', '" & objTable.Rows(c1).Item("approvaluser") & "',CURRENT_TIMESTAMP,'" & objTable.Rows(c1).Item("approvaltype") & "','1', '" & objTable.Rows(c1).Item("approvalstatus") & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            Next

                            sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + _
                                 objTable.Rows.Count - 1 & " " & _
                                 "where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If

                    Else
                        showMessage("Silahkan simpan SPK terlebih dahulu !!", 2)
                        Exit Sub
                    End If
                    objTransApp.Commit()
                    xCmd.Connection.Close()
                Catch ex As OleDb.OleDbException
                    objTransApp.Rollback()
                    xCmd.Connection.Close()
                    showMessage(ex.ToString, 2)
                    Exit Sub
                End Try

            ElseIf womststatus.Text = "Rejected" Then
                womststatus.Text = "In Process"
            End If

            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnwomst WHERE womstoid=" & womstoid.Text
                If CheckDataExists(sSql) Then
                    womstoid.Text = GenerateID("QL_TRNWOMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnwomst", "womstoid", womstoid.Text, "womststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    womststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            ' PeriodAcctg.Text = GetDateToPeriodAcctg(CDate(wodate.Text))
            Dim dBalanceQty As Double = 0
            If womststatus.Text = "Post" Then
                ' GenerateWONo()
                dBalanceQty = ToDouble(Session("TblDtlFG").Rows(0).Item("wodtl1qty").ToString)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnwomst (cmpcode, womstoid, periodacctg, wodate, wono, wodocrefno, womstnote, womststatus, womstres1, womstres2, womstres3, createuser, createtime, upduser, updtime, wototaldlc, wototalfoh, balanceqty_cogm,wostartproduksi,wofinishproduksi,wotype,woordertype,woflag,personoid,branch_code,tobranch_code) VALUES ('" & DDLBusUnit.SelectedValue & "', " & womstoid.Text & ", '" & PeriodAcctg.Text & "', CURRENT_TIMESTAMP, '" & wono.Text & "', '" & Tchar(wodocrefno.Text) & "', '" & Tchar(womstnote.Text.Trim) & "', '" & womststatus.Text & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalDLC & ", " & dTotalFOH & ", " & dBalanceQty & ",'" & startProd.Text & "','" & finishProd.Text & "','" & typeorder.Text & "'," & typespk.Text & ",'" & ddlspk.SelectedValue & "'," & DDLPIC.SelectedValue & ",'" & Session("branch_id") & "','" & ddltobranch.SelectedValue & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & womstoid.Text & " WHERE tablename='QL_TRNWOMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    If womststatus.Text = "In Approval" Then
                        sSql = "update ql_trnwomst set worevisenote='' where womstoid='" & womstoid.Text & "' and branch_code='" & Session("branch_id") & "' "
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    ElseIf womststatus.Text = "Revised" Then
                        womststatus.Text = "In Process"
                    End If
                    sSql = "UPDATE QL_trnwomst SET periodacctg='" & PeriodAcctg.Text & "', wono='" & wono.Text & "', wodocrefno='" & Tchar(wodocrefno.Text) & "', womstnote='" & Tchar(womstnote.Text.Trim) & "', womststatus='" & womststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, balanceqty_cogm=" & dBalanceQty & ",wostartproduksi='" & startProd.Text & "',wofinishproduksi='" & finishProd.Text & "',wotype='" & typeorder.Text & "',woordertype=" & typespk.Text & ",woflag='" & ddlspk.SelectedValue & "', personoid=" & DDLPIC.SelectedValue & ",tobranch_code = '" & ddltobranch.SelectedValue & "' WHERE womstoid=" & womstoid.Text & " and branch_code = '" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    'sSql = "UPDATE QL_trnsodtl SET sodtlres3='' WHERE sodtloid IN (SELECT DISTINCT soitemdtloid FROM QL_trnwodtl1 WHERE womstoid=" & womstoid.Text & ")"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    'sSql = "UPDATE QL_trnsomst SET somstres3='' WHERE somstoid IN (SELECT DISTINCT soitemmstoid FROM QL_trnwodtl1 WHERE womstoid=" & womstoid.Text & ")"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnwodtl3 WHERE womstoid=" & womstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnwodtl2 WHERE womstoid=" & womstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnwodtl1 WHERE womstoid=" & womstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If (Session("TblDtlFG") IsNot Nothing) And (Session("TblDtlProcess") IsNot Nothing) And (Session("TblDtlMatLev5") IsNot Nothing) Then
                    Dim dtItem As DataTable = Session("TblDtlFG")
                    Dim dtProc As DataTable = Session("TblDtlProcess")
                    Dim dvProc As DataView = dtProc.DefaultView
                    Dim dtMatLev5 As DataTable = Session("TblDtlMatLev5")
                    Dim dvMatLev5 As DataView = dtMatLev5.DefaultView

                    For C1 As Int16 = 0 To dtItem.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnwodtl1 (cmpcode, wodtl1oid, womstoid, wodtl1seq, soitemmstoid, soitemdtloid, itemoid, wodtl1qty, wodtl1unitoid, bomoid, wodtl1area, wodtl1status, wodtl1note, wodtl1res1, wodtl1res2, wodtl1res3, upduser, updtime, wodtl1totalmatamt, wodtl1dlcamt, wodtl1ohdamt, curroid_ohd, wodtl1ohdamtidr, wodtl1marginamt, wodtl1precostamt, curroid_precost, wodtl1precostamtidr, lastupduser_precost, lastupdtime_precost, lastupduser_bom, lastupdtime_bom, lastupduser_dlc, lastupdtime_dlc,itemcode,itemoldcode,typeemasitem,typeemasdesc,kadaremasitem,diameter,beratperUnit,beratlilinItem,qtyproduksi) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(wodtl1oid.Text)) & ", " & womstoid.Text & ", " & C1 + 1 & ", 0, " & dtItem.Rows(C1).Item("soitemdtloid") & ", " & dtItem.Rows(C1).Item("itemoid") & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1qty").ToString) & ", 0, " & dtItem.Rows(C1).Item("bomoid") & ", 0, '', '" & Tchar(dtItem.Rows(C1).Item("wodtl1note").ToString) & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtItem.Rows(C1).Item("wodtl1totalmatamt").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1dlcamt").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1ohdamt").ToString) & ", " & ToInteger(dtItem.Rows(C1).Item("curroid_ohd").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1ohdamtidr").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1marginamt").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1precostamt").ToString) & ", " & ToInteger(dtItem.Rows(C1).Item("curroid_precost").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1precostamtidr").ToString) & ", '', '', '" & dtItem.Rows(C1).Item("lastupduser_bom").ToString & "', '" & dtItem.Rows(C1).Item("lastupdtime_bom").ToString & "', '', '','" & dtItem.Rows(C1).Item("itemcode").ToString & "','" & dtItem.Rows(C1).Item("itemoldcode").ToString & "','','','" & dtItem.Rows(C1).Item("kadaremas").ToString & "','" & dtItem.Rows(C1).Item("diameter").ToString & "','" & dtItem.Rows(C1).Item("beratUnit").ToString & "','" & dtItem.Rows(C1).Item("beratlilin").ToString & "','" & dtItem.Rows(C1).Item("qtyproduksi").ToString & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        If womststatus.Text = "POST" Then
                            sSql = "UPDATE QL_trnwomst SET womstres1 ='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If

                        'If dtItem.Rows(C1).Item("soitemmstoid") <> 0 Then

                        '    If ToDouble(dtItem.Rows(C1).Item("wodtl1qty").ToString) >= ToDouble(dtItem.Rows(C1).Item("soitemqty").ToString) Then
                        '        sSql = "UPDATE QL_trnwomst SET womstres1 ='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & ""
                        '        xCmd.CommandText = sSql
                        '        xCmd.ExecuteNonQuery()
                        '        sSql = "UPDATE QL_trnsomst SET somstres3='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & dtItem.Rows(C1).Item("soitemmstoid") & " AND (SELECT COUNT(sodtloid) FROM QL_trnsodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND ISNULL(sodtlres3, '')='' AND somstoid=" & dtItem.Rows(C1).Item("soitemmstoid") & " AND sodtloid<>" & dtItem.Rows(C1).Item("soitemdtloid").ToString & ")=0"
                        '        xCmd.CommandText = sSql
                        '        xCmd.ExecuteNonQuery()
                        '    End If

                        'End If

                    Next
                    'dvProc.RowFilter = "wodtl1seq=" & dtItem.Rows(C1).Item("wodtl1seq").ToString
                    For C2 As Integer = 0 To dvProc.Count - 1
                        sSql = "INSERT INTO QL_trnwodtl2 (cmpcode, wodtl2oid, wodtl1oid, womstoid, wodtl2seq, bomdtl1oid, wodtl2procseq, deptoid, todeptoid, wodtl2desc, wodtl2type, wodtl2qty, wodtl2unitoid, wodtl2status, wodtl2note, wodtl2res1, wodtl2res2, wodtl2res3, upduser, updtime, wodtl2dlcvalue, wodtl2fohvalue,tukangoid,datestart,datefinish,operator1oid,operator1,operator2oid,operator2,qualityoid,quality,bomwipoid,bomWIPdesc,bomkadarWIP,tolerance,typeemas,bomwipcode,bomwipoldcode) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C2 + CInt(wodtl2oid.Text)) & ",  " & (C2 + CInt(wodtl1oid.Text)) & ", " & womstoid.Text & ", " & C2 + 1 & ", 0, " & dvProc(C2).Item("wodtl2procseq").ToString & ", " & dvProc(C2).Item("deptoid").ToString & ", " & dvProc(C2).Item("todeptoid").ToString & ", '" & dvProc(C2).Item("deptname").ToString & "', '" & dvProc(C2).Item("wodtl2type").ToString & "', 0, 0, '', '" & Tchar(dvProc(C2).Item("wodtl2note").ToString) & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0,0,0,'" & CDate(startProd.Text) & "','" & CDate(finishProd.Text) & "','" & dvProc(C2).Item("operator1oid").ToString & "','" & dvProc(C2).Item("operator1").ToString & "','" & dvProc(C2).Item("operator2oid").ToString & "','" & dvProc(C2).Item("operator2").ToString & "','" & dvProc(C2).Item("qualityoid").ToString & "','" & dvProc(C2).Item("quality").ToString & "',0,0,0,0,0,'','')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If dvMatLev5.Count > 0 Then
                            dvMatLev5.RowFilter = "wodtl2seq=" & dvProc(C2).Item("wodtl2seq").ToString
                            For C3 As Integer = 0 To dvMatLev5.Count - 1
                                sSql = "INSERT INTO QL_trnwodtl3 (cmpcode, wodtl3oid, wodtl2oid, wodtl1oid, womstoid, wodtl3seq, bomdtl2oid, wodtl3reftype, wodtl3refoid, wodtl3qty,wodtl3qtybiji,wodtl3unitoid, wodtl3status, wodtl3note, wodtl3res1, wodtl3res2, wodtl3res3, upduser, updtime, wodtl3value,weight,typemat,typeemas,matkadaremas,stock,tolerance,wodtl3bomqty,itemoid,itemshortdesc) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C3 + CInt(wodtl3oid.Text)) & ", '" & (C2 + CInt(wodtl2oid.Text)) & "',  " & (C2 + CInt(wodtl1oid.Text)) & ", " & womstoid.Text & ", " & C3 + 1 & ", '" & dvMatLev5(C3).Item("bomdtl2oid").ToString & "', '', " & dvMatLev5(C3).Item("wodtl3refoid").ToString & ", " & ToDouble(dvMatLev5(C3).Item("wodtl3qty").ToString) & "," & ToDouble(dvMatLev5(C3).Item("wodtl3qtybiji").ToString) & ", " & dvMatLev5(C3).Item("wodtl3unitoid").ToString & ", '', '" & Tchar(dvMatLev5(C3).Item("wodtl3note").ToString) & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvMatLev5(C3).Item("wodtl3value").ToString) & ", " & ToDouble(dvMatLev5(C3).Item("weight").ToString) & ", 0, 0,0, " & ToDouble(dvMatLev5(C3).Item("stock").ToString) & ", " & ToDouble(dvMatLev5(C3).Item("tolerance").ToString) & ",'" & dvMatLev5(C3).Item("qtybom").ToString & "', " & dvMatLev5(C3).Item("wodtl3refoid").ToString & ", '" & dvMatLev5(C3).Item("wodtl3refshortdesc").ToString & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            Next
                            wodtl3oid.Text = CInt(wodtl3oid.Text) + dvMatLev5.Count
                            dvMatLev5.RowFilter = ""
                        End If
                        wodtl2oid.Text = CInt(wodtl2oid.Text) + dvProc.Count
                        dvProc.RowFilter = ""
                    Next


                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtItem.Rows.Count - 1 + CInt(wodtl1oid.Text)) & " WHERE tablename='QL_TRNWODTL1' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & CInt(wodtl2oid.Text) - 1 & " WHERE tablename='QL_TRNWODTL2' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & CInt(wodtl3oid.Text) - 1 & " WHERE tablename='QL_TRNWODTL3' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        womststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    womststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                womststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & womstoid.Text & ".<BR>"
            End If
            If womststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with SPK No. = " & wono.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If womstoid.Text.Trim = "" Then
            showMessage("Please select Job Costing MO data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnwomst", "womstoid", womstoid.Text, "womststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                womststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'sSql = "UPDATE QL_trnsodtl SET sodtlres3='' WHERE sodtloid IN (SELECT DISTINCT sodtloid FROM QL_trnwodtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & ")"
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()
            'sSql = "UPDATE QL_trnsomst SET somstres3='' WHERE somstoid IN (SELECT DISTINCT somstoid FROM QL_trnwodtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & ")"
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()
            'sSql = "DELETE FROM QL_trnwodtl4 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwodtl3 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwodtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwomst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        womststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub customer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If customer.Checked = True Then
            typeorder.Text = "customer"
        Else
            typeorder.Text = "stock"
        End If
    End Sub

    Protected Sub stock_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If stock.Checked = True Then
            typeorder.Text = "stock"
        Else
            typeorder.Text = "customer"
        End If
    End Sub

    Protected Sub btnlist3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        If Session("TblDtlMatLev5") Is Nothing Then
            CreateTblDetailMatLev5()
        End If
        Dim dtMat As DataTable = Session("TblDtlMatLev5")
        Dim dv As DataView = dtMat.DefaultView
        If i_u3.Text = "New Detail" Then
            dv.RowFilter = "wodtl2seq=" & DDLProcess.SelectedValue & " AND wodtl3reftype='" & wodtl3reftype.SelectedValue & "' AND wodtl3refoid=" & wodtl3refoid.Text
        Else
            dv.RowFilter = "wodtl2seq=" & DDLProcess.SelectedValue & " AND wodtl3reftype='" & wodtl3reftype.SelectedValue & "' AND wodtl3refoid=" & wodtl3refoid.Text & " AND wodtl3seq<>" & wodtl3seq.Text
        End If
        If dv.Count > 0 Then
            showMessage("This Data has been added before!", 2)
            dv.RowFilter = ""
            Exit Sub
        End If
        dv.RowFilter = ""
        Dim objRow As DataRow
        If i_u3.Text = "New Detail" Then
            objRow = dtMat.NewRow()
            objRow("wodtl3seq") = dtMat.Rows.Count + 1
        Else
            objRow = dtMat.Rows(wodtl3seq.Text - 1)
            objRow.BeginEdit()
        End If
        If qtypakai.Text <= 0 Then
            showMessage("Masukkan Jumlah Quantity Item Yang Dipilih!", 2)
            Exit Sub
        End If

        objRow("wodtl3seq") = wodtl3seq.Text
        objRow("wodtl2desc") = DDLProcess.SelectedItem.Text
        objRow("wodtl3reftype") = wodtl3reftype.SelectedValue
        objRow("wodtl3refoid") = wodtl3refoid.Text
        objRow("wodtl3refshortdesc") = wodtl3refshortdesc.Text
        objRow("wodtl3refcode") = wodtl3refcode.Text
        ' objRow("wodtl3qty") = (qtypakai.Text)
        objRow("wodtl3qtybiji") = (qtypakai.Text)
        objRow("wodtl3note") = wodtl3note.Text
        objRow("wodtl3qtyref") = wodtl3qtyref.Text
        objRow("tolerance") = (tolerancedtl3.Text)
        objRow("stock") = stockMat3.Text
        objRow("qtybom") = QtyBOM.Text
        objRow("weight") = weightMat.Text
        If i_u3.Text = "New Detail" Then
            dtMat.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If
        Session("TblDtlMatLev5") = dtMat
        GVDtl3.DataSource = dtMat
        GVDtl3.DataBind()
        ClearDetailMaterialLevel5()
    End Sub

    Protected Sub findmat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLProcess.SelectedValue = "" Then
            showMessage(" Inputkan Minimal 1 Process !<BR>", 2)
        Else
            'InitDDLCat01ListMat() : cbCat01ListMat.Checked = False : cbCat02ListMat.Checked = False : cbCat03ListMat.Checked = False : cbCat04ListMat.Checked = False
            'FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = "" : 
            Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat2.DataSource = Session("TblListMatView") : gvListMat2.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        End If
    End Sub

    Protected Sub GVDtl3_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objtable As DataTable = Session("TblDtlMatLev5")
        Dim objrow() As DataRow
        objrow = objtable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objtable.Rows.Remove(objrow(e.RowIndex))
        For C1 As Integer = 0 To objtable.Rows.Count - 1
            Dim dr As DataRow = objtable.Rows(C1)
            dr.BeginEdit()
            dr("wodtl3seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlMatLev5") = objtable
        GVDtl3.DataSource = Session("TblDtlMatLev5")
        GVDtl3.DataBind()
        ClearDetailMaterialLevel5()

    End Sub

    Protected Sub lkbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat2.PageIndexChanging
        If Not Session("TblListMat") Is Nothing Then
            UpdateCheckedMat()
            gvListMat2.PageIndex = e.NewPageIndex
            gvListMat2.DataSource = Session("TblListMat")
            gvListMat2.DataBind()
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat2.Click
        If Session("TblListMat") Is Nothing Then
            BindMatData(wodtl3reftype.SelectedValue)
        End If
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
                'InitDDLCat01ListMat() : cbCat01ListMat.Checked = False : cbCat02ListMat.Checked = False : cbCat03ListMat.Checked = False : cbCat04ListMat.Checked = False
                Session("TblListMatView") = dt
                gvListMat2.DataSource = Session("TblListMatView")
                gvListMat2.DataBind()
                mpeListMat2.Show()
            Else
                Session("ErrMat") = "Material data can't be found!"
                showMessage(Session("ErrMat"), 2)
            End If
        End If
    End Sub

    Protected Sub GvDtlWip_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
    End Sub

    Protected Sub lbkpict_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ambilgambar As String = ClassFunction.GetStrData("select itempictureloc from ql_mstitemwip where itemoid=" & sender.commandargument & "")
        showImage(ambilgambar)
        Session("uploadImage_mstperson") = True
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(beMsgBox, PanelMsgBox, mpeMsgbox, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub FilterDDLCat03ListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub FilterDDLCat01ListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub FilterDDLCat02ListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub GvDtlWip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub addtoListWip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub GvDtlWip_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)

    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        fillqtywip()
    End Sub

    Protected Sub beratperunit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        fillweightwip()
    End Sub

    Protected Sub tolerance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        filltolerancewip()
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat2.Click
        If Session("TblListMat") Is Nothing Then
            BindMatData("")
        End If
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
                'If cbCat04ListMat.Checked And FilterDDLCat04ListMat.SelectedValue <> "" Then
                '    sFilter &= " AND matrefcode LIKE '" & FilterDDLCat04ListMat.SelectedValue & "%'"
                'Else
                '    If cbCat03ListMat.Checked And FilterDDLCat03ListMat.SelectedValue <> "" Then
                '        sFilter &= " AND matrefcode LIKE '" & FilterDDLCat03ListMat.SelectedValue & "%'"
                '    Else
                '        If cbCat02ListMat.Checked And FilterDDLCat02ListMat.SelectedValue <> "" Then
                '            sFilter &= " AND matrefcode LIKE '" & FilterDDLCat02ListMat.SelectedValue & "%'"
                '        Else
                '            If cbCat01ListMat.Checked And FilterDDLCat01ListMat.SelectedValue <> "" Then
                '                sFilter &= " AND matrefcode LIKE '" & FilterDDLCat01ListMat.SelectedValue & "%'"
                '            End If
                '        End If
                '    End If
                'End If
                'If cbCat01ListMat.Checked And FilterDDLCat01ListMat.SelectedValue <> "" Then
                '    sFilter &= " AND cat1code LIKE '" & FilterDDLCat01ListMat.SelectedValue & "%'"
                'End If
                'If cbCat02ListMat.Checked And FilterDDLCat02ListMat.SelectedValue <> "" Then
                '    sFilter &= " AND cat2code LIKE '" & FilterDDLCat02ListMat.SelectedValue & "%'"
                'End If
                'If cbCat03ListMat.Checked And FilterDDLCat03ListMat.SelectedValue <> "" Then
                '    sFilter &= " AND cat3code LIKE '" & FilterDDLCat03ListMat.SelectedValue & "%'"
                'End If
                'If cbCat04ListMat.Checked And FilterDDLCat04ListMat.SelectedValue <> "" Then
                '    sFilter &= " AND cat4code LIKE '" & FilterDDLCat04ListMat.SelectedValue & "%'"
                'End If
                dv.RowFilter = sFilter
                If dv.Count > 0 Then
                    Session("TblListMatView") = dv.ToTable
                    gvListMat2.DataSource = Session("TblListMatView")
                    gvListMat2.DataBind()
                    dv.RowFilter = ""
                    mpeListMat2.Show()
                Else
                    dv.RowFilter = ""
                    Session("ErrMat") = "Material data can't be found!"
                    showMessage(Session("ErrMat"), 2)
                End If
            Else
                Session("ErrMat") = "Material data can't be found!"
                showMessage(Session("ErrMat"), 2)
            End If
        End If

    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count = 0 Then
                    Session("ErrMat") = "Please select Material data!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True' AND bomdtl3qtybiji=0"
                If dv.Count > 0 Then
                    Session("ErrMat") = "Qty for every selected Material must be more than 0!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                If Session("TblDtlMatLev5") Is Nothing Then
                    CreateTblDetailMatLev5()
                    ' ClearDetailMaterialLevel5()
                End If
                Dim dtMat As DataTable = Session("TblDtlMatLev5")
                Dim dvMat As DataView = dtMat.DefaultView
                Dim iSeq As Integer = dvMat.Count + 1
                dvMat.AllowEdit = True
                dvMat.AllowNew = True
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    dvMat.RowFilter = " wodtl3seq = '" & iSeq & "'"
                    If dvMat.Count > 0 Then
                        dvMat(0)("wodtl3qtybiji") = ToDouble(dv(C1)("bomdtl3qtybiji").ToString)
                        If dvMat(0)("wodtl3qtybiji") = 0 Then
                            Session("ErrMat") = "Qty for every selected Material must be more than 0!"
                            showMessage(Session("ErrMat"), 2)
                            dv.RowFilter = ""
                            Exit Sub
                        Else
                            dvMat(0)("wodtl3note") = dv(C1)("bomdtl2note").ToString
                            dvMat(0)("tolerance") = dv(C1)("tolerance").ToString
                            If dvMat(0)("tolerance") > 100 Or dvMat(0)("tolerance") Is Nothing Then
                                Session("ErrMat") = "Tolerance Must Less Than 100% or Tolerance must be more than 0!"
                                showMessage(Session("ErrMat"), 2)
                                dv.RowFilter = ""
                                Exit Sub
                            Else
                                dvMat(0)("wodtl3refshortdesc") = dv(C1)("matrefshortdesc").ToString
                                dvMat(0)("wodtl3refcode") = dv(C1)("matrefcode").ToString
                                dvMat(0)("wodtl3refoid") = dv(C1)("matrefoid").ToString
                                dvMat(0)("wodtl3qty") = dv(C1)("bomdtl3qty").ToString
                                dvMat(0)("wodtl3qtybiji") = dv(C1)("bomdtl3qtybiji").ToString
                            End If
                        End If
                    Else
                        dvMat.RowFilter = "wodtl3refoid = " & dv(C1)("matrefoid").ToString & ""
                        If dvMat.Count > 0 Then
                            Session("ErrMat") = "This Data Has Been Added Before!"
                            showMessage(Session("ErrMat"), 2)
                            dvMat.RowFilter = ""
                            Exit Sub
                        End If
                        Dim rv As DataRowView = dvMat.AddNew()
                        rv.BeginEdit()
                        rv("wodtl3seq") = iSeq
                        rv("wodtl2seq") = DDLProcess.SelectedValue
                        rv("wodtl1seq") = iSeq
                        rv("wodtl3reftype") = wodtl3reftype.SelectedValue
                        rv("wodtl3refoid") = dv(C1)("matrefoid").ToString
                        rv("wodtl2desc") = DDLProcess.SelectedItem
                        rv("wodtl3refshortdesc") = dv(C1)("matrefshortdesc").ToString
                        rv("wodtl3refcode") = dv(C1)("matrefcode").ToString
                        rv("wodtl3unitoid") = dv(C1)("matrefunitoid")
                        rv("wodtl3unit") = dv(C1)("matrefunit").ToString
                        rv("weight") = dv(C1)("weight").ToString
                        rv("stock") = 0
                        rv("wodtl3qty") = ToDouble(dv(C1)("bomdtl3qty").ToString)
                        rv("wodtl3qtybiji") = ToDouble(dv(C1)("bomdtl3qtybiji").ToString)
                        rv("wodtl3note") = dv(C1)("bomdtl2note").ToString
                        rv("tolerance") = dv(C1)("tolerance").ToString
                        rv("qtybom") = 0
                        'rv("bomdtl2oid") = dvMat(C1)("bomdtl2oid")
                        'rv("typemat") = 0
                        'rv("typeemas") = 0
                        'rv("matkadaremas") = 0
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    dvMat.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl3") = dvMat.ToTable
                GVDtl3.DataSource = Session("TblDtl3")
                GVDtl3.DataBind()
                ClearDetailMaterialLevel5()
            End If
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)

        End If
    End Sub

    Protected Sub findspk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
    End Sub

    Protected Sub clearspk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        spkno.Text = ""
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub gvListKIK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lbspkoid.Text = gvListKIK.SelectedDataKey.Item("womstoid").ToString()
        spkno.Text = gvListKIK.SelectedDataKey.Item("wono").ToString()
        wodate.Text = gvListKIK.SelectedDataKey.Item("wodate").ToString()
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
        btnFindListSO_Click(Nothing, Nothing)
    End Sub

    Protected Sub wodtl2procseq_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wodtl2procseq.TextChanged
        'Dim bVal As Boolean = False
        'Dim sCss As String = "inpTextDisabled"
        'If wodtl2procseq.Text <> "" Then
        '    bVal = True : sCss = "inpText"
        'End If
        'deptoid.Enabled = bVal : deptoid.CssClass = sCss
        'todeptoid.Enabled = bVal : todeptoid.CssClass = sCss
    End Sub

    Protected Sub imbfindWIP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub gvListWIP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        wipoid.Text = gvListWIP.SelectedDataKey("itemoid").ToString.Trim
        output.Text = gvListWIP.SelectedDataKey("itemlongdesc").ToString.Trim
        kodebarudtl1.Text = gvListWIP.SelectedDataKey("itemcode").ToString.Trim
        kadarEmas.Text = gvListWIP.SelectedDataKey("kadarcontoh").ToString.Trim
        typeEmasdtl1.SelectedValue = gvListWIP.SelectedDataKey("typeemas").ToString.Trim
        cProc.SetModalPopUpExtender(btnHideListWIP, pnlListWIP, mpeListWIP, False)
    End Sub

    Protected Sub gvListSO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListSO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
        End If
    End Sub

    Protected Sub qtyprod_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qtyprod.TextChanged
        'If ToDouble(qtyprod.Text) > ToDouble(lblmaxqty.Text) Then
        '    showMessage("Qty Produksi tidak boleh melebihi qty SO", 2)
        '    qtyprod.Text = ToDouble(lblmaxqty.Text)
        'Else
        '    '   wodtl1qty.Text = ToDouble(qtyprod.Text) * ToDouble(berat.Text)
        'End If
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        gvListKIK.PageIndex = e.NewPageIndex
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListWIP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListWIP.PageIndexChanging
        gvListWIP.PageIndex = e.NewPageIndex
        BindWIP()
        mpeListWIP.Show()
    End Sub

    Protected Sub deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deptoid.SelectedIndexChanged
        initdeptoid()

    End Sub

    Protected Sub imbclearWip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub clearmat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub clearWip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub wodtl2type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wodtl2type.SelectedIndexChanged
        'If wodtl2type.SelectedValue = "FG" Then
        '    lbltodept.Visible = False : septtodept.Visible = False : todeptoid.Visible = False
        'Else
        '    lbltodept.Visible = True : septtodept.Visible = True : todeptoid.Visible = True
        'End If

    End Sub

    Protected Sub btnApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        womststatus.Text = "In Approval"
        btnSave_Click(sender, e)
    End Sub
#End Region
End Class