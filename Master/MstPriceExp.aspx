<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="MstPriceExp.aspx.vb" Inherits="MstPriceExpedisi" title="" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" colspan="2" valign="middle">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Navy"
                    Text=".: Master Price Expedisi" Font-Size="X-Large"></asp:Label></th>
        </tr>
        <tr>
            <th align="left" colspan="2" style="background-color: transparent" valign="middle">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Price Expedisi :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlList" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w100"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left>Cabang</TD><TD style="WIDTH: 2%" align=center>:</TD><TD align=left colSpan=2><asp:DropDownList id="DDLCabang" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w101"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label4x" runat="server" Text="Filter" __designer:wfdid="w102"></asp:Label></TD><TD style="WIDTH: 2%" align=center>:</TD><TD align=left colSpan=2><asp:DropDownList id="FilterDDLMst" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w103"><asp:ListItem Value="e.mstexpoid">Draft</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextMst" runat="server" Width="200px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w104"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD style="WIDTH: 2%" align=center>:</TD><TD align=left colSpan=2><asp:DropDownList id="DDLActive_flag" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w105"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>IN ACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w106"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w107"></asp:ImageButton> <asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w108"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4><asp:GridView id="gvList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w109" AllowSorting="True" DataKeyNames="mstexpoid" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Draft"><ItemTemplate>
<asp:LinkButton id="lkbSelect" onclick="lkbSelect_Click" runat="server" Text='<%# Eval("mstexpoid") %>' __designer:wfdid="w161" ToolTip="<%# GetTransID() %>" CommandName='<%# Eval("branch_code") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="containersize" HeaderText="Cont. size">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="containerprice" HeaderText="Cont. price">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="crtuser" HeaderText="Create User">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="crtdate" HeaderText="Create date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="crttime" HeaderText="Create time">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="active_flag" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="No data found."></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=4><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w110"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="imgForm" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Price Expedisi :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left>Cabang</TD><TD align=center>:</TD><TD align=left><asp:DropDownList id="ddlBranch" runat="server" CssClass="inpText" Font-Size="8pt" AutoPostBack="True" __designer:wfdid="w146"></asp:DropDownList></TD><TD align=left><asp:Label id="mstexpoid" runat="server" Font-Size="8pt" Text="mstexpoid" __designer:wfdid="w105" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Font-Size="8pt" Text="Ukuran container" __designer:wfdid="w106"></asp:Label></TD><TD align=center>:</TD><TD align=left><asp:TextBox id="containersize" runat="server" CssClass="inpText" Font-Size="8pt" AutoPostBack="True" __designer:wfdid="w149">0</asp:TextBox></TD><TD align=left>Price Container</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="containerprice" runat="server" CssClass="inpText" Font-Size="8pt" AutoPostBack="True" __designer:wfdid="w150">0</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Font-Size="8pt" Text="Status" __designer:wfdid="w109"></asp:Label></TD><TD align=center>:</TD><TD align=left><asp:DropDownList id="active_flag" runat="server" CssClass="inpText" Font-Size="8pt" AutoPostBack="True" __designer:wfdid="w152"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>IN ACTIVE</asp:ListItem>
</asp:DropDownList></TD><TD align=left><asp:Button id="BtnAddItem" runat="server" CssClass="green" Font-Bold="True" Text="Add Katalog" __designer:wfdid="w153"></asp:Button></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 300px; BACKGROUND-COLOR: beige"><asp:GridView id="gvFindCrd" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w112" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode ">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PicItem" HeaderText="PIC">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="panjang" HeaderText="Panjang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lebar" HeaderText="Lebar">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tinggi" HeaderText="Tinggi">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratvolume" HeaderText="Volume">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kubik" HeaderText="Kubikasi">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="HargaExp" HeaderText="Price Exp.">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="keterangan" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="COLOR: #585858" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w155"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w156"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" align=left colSpan=6>Last Updated By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w157">-</asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w158">-</asp:Label></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="imbSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w159"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w160"></asp:ImageButton> <asp:ImageButton id="imbPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w161" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w162" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w163" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w164" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w165"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="FTEcontainersize" runat="server" __designer:wfdid="w166" ValidChars="12345678980,." TargetControlID="containersize"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEcontainerprice" runat="server" __designer:wfdid="w167" ValidChars="12345678980,." TargetControlID="containerprice"></ajaxToolkit:FilteredTextBoxExtender></DIV>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbSave"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
                            <asp:UpdatePanel id="upListMat" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" __designer:wfdid="w169" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Finish Good" __designer:wfdid="w170"></asp:Label></TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" DefaultButton="btnFindListMat" __designer:wfdid="w171"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblFilterInListMat" runat="server" Text="Filter :" __designer:wfdid="w172"></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w173"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="merk">Merk</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w174"></asp:TextBox>&nbsp; </TD></TR><TR><TD align=center colSpan=3>Jenis : <asp:DropDownList id="ddlType" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w175"></asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w176"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w177"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w178"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w179"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w180"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 875px; HEIGHT: 300px; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w139" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbSelectMat" runat="server" Font-Size="8pt" __designer:wfdid="w186" Checked='<%# eval("CheckValue") %>' ToolTip='<%# eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="PicItem" HeaderText="PIC">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="panjang" HeaderText="Panjang">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lebar" HeaderText="Lebar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tinggi" HeaderText="Tinggi">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratvolume" HeaderText="B. Volume">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kubik" HeaderText="Ukuran/Kubik">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Exp. Price"><ItemTemplate>
<asp:TextBox id="tbHargaExp" runat="server" Width="75px" CssClass="inpText" Font-Size="8pt" Text='<%# eval("HargaExp") %>' __designer:wfdid="w187" MaxLength="16"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbtbHargaExp" runat="server" __designer:wfdid="w188" TargetControlID="tbHargaExp" ValidChars="1234567890.,-"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbMatNote" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" Text='<%# eval("keterangan") %>' __designer:wfdid="w189" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddListMat" runat="server" Font-Bold="True" __designer:wfdid="w182">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListMat" runat="server" Font-Bold="True" __designer:wfdid="w183">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" __designer:wfdid="w184" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" Drag="True" TargetControlID="btnHideListMat">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" __designer:wfdid="w185" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp;
</ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></th>
        </tr>
    </table>
                            <asp:UpdatePanel id="upPopUpMsg" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w128" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w129"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w130"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w131"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w132"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w133" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w134" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
</asp:Content>

