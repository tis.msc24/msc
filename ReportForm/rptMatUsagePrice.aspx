<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptMatUsagePrice.aspx.vb" Inherits="rptMatUsagePrice" Title="MSC - Laporan Material Usage On Price" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Material Usage On Price"></asp:Label></th>
        </tr>
<tr>
<th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE><TBODY><TR><TD style="VERTICAL-ALIGN: top" id="tdPeriod1" align=left runat="server" visible="true"><asp:Label id="Label6" runat="server" Text="Periode"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" id="Td1" align=left runat="server" visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="tdperiod2" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="dateAwal" runat="server" Width="62px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="62px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label> &nbsp;&nbsp; </TD></TR><TR><TD align=left>Cabang</TD><TD align=right>:</TD><TD align=left colSpan=3><asp:DropDownList id="DDLFromBranch" runat="server" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="DDLFromBranch_SelectedIndexChanged"></asp:DropDownList> &nbsp;&nbsp;</TD></TR><TR><TD align=left>Lokasi</TD><TD align=right>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddlLocation" runat="server" CssClass="inpText">
                                            </asp:DropDownList> &nbsp;&nbsp;</TD></TR><TR><TD vAlign=top align=left><asp:DropDownList id="DDLSONo" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True">
                <asp:ListItem Value="NomerSO">No. Usage</asp:ListItem>
                <asp:ListItem Value="Draft">No. Draft</asp:ListItem>
            </asp:DropDownList></TD><TD vAlign=top align=right>:</TD><TD align=left colSpan=3><asp:TextBox id="SoNo" runat="server" Width="158px" CssClass="inpTextDisabled" TextMode="MultiLine" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=left><asp:Label id="Label1" runat="server" Width="1px" Text="Katalog"></asp:Label></TD><TD vAlign=top align=right>:</TD><TD align=left colSpan=3><asp:TextBox id="ItemName" runat="server" Width="158px" CssClass="inpTextDisabled" TextMode="MultiLine" Enabled="False" __designer:wfdid="w230" Rows="2"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w231"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseItem" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w232"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnViewReport" onclick="btnViewReport_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Sedang proses....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" HasExportButton="False" HasPrintButton="False" ShowAllPageIds="True" AutoDataBind="true" DisplayGroupTree="False"></CR:CrystalReportViewer> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListSO" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelListSO" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Large" Font-Bold="True" Text="List Of Material Usage"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO">
                                <asp:Label ID="Label24" runat="server" Text="Filter :"></asp:Label>
                                <asp:DropDownList ID="DDLFilterListSO" runat="server" CssClass="inpText" Width="100px">
                                    <asp:ListItem Value="trnrequestitemno">No. Usage</asp:ListItem>
                                    <asp:ListItem Value="trnrequestitemnote">Keterangan</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtFilterListSO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>
                                <asp:ImageButton ID="btnFindListSO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
                                <asp:ImageButton ID="btnViewAllListSO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" />
                            </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" ForeColor="#333333" GridLines="None" PageSize="5" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbLMSO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("trnrequestitemoid") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="trnrequestitemoid" HeaderText="Draft No">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Center"
                                            VerticalAlign="Middle" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="trnrequestitemno" HeaderText="No. Usage">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="trnrequestitemdate" HeaderText="Tanggal">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="trnrequestitemnote" HeaderText="Keterangan">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="trnrequestitemstatus" HeaderText="Status">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                    HorizontalAlign="Right" />
                                <EmptyDataTemplate>
                                    &nbsp;
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListSO" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Large" Font-Bold="True" Text="List Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText">
                                                        <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                                                        <asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
                                                        <asp:ListItem Value="JenisNya">Jenis</asp:ListItem>
                                                    </asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp; </TD></TR><TR><TD class="Label" align=center colSpan=3>Jenis : <asp:DropDownList id="stockFlag" runat="server" CssClass="inpText" Font-Size="Small">
                                                        <asp:ListItem>ALL</asp:ListItem>
                                                        <asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
                                                        <asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
                                                        <asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
                                                        <asp:ListItem>ASSET</asp:ListItem>
                                                    </asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnALLSelectItem" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneItem" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedItem" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 247px; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" GridLines="None" PageSize="8" DataKeyNames="itemcode" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                <HeaderStyle CssClass="gvhdr" Font-Strikeout="False" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemdesc" HeaderText="Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="merk" HeaderText="Merk">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="satuan3" HeaderText="Satuan">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemflag" HeaderText="Status">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"
                                            HorizontalAlign="Right" />
                                        <EmptyDataTemplate>
                                            &nbsp;
                                            <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="Klik tombol View All atau Find untuk tampilkan data"></asp:Label>
                                        </EmptyDataTemplate>
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView> </DIV>&nbsp;</TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" Drag="True">
                </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td colspan="2" style="background-color: #cc0000; text-align: left">
                            <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                Width="24px" /></td>
                        <td class="Label" style="text-align: left">
                            <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px; text-align: center">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                TargetControlID="bePopUpMsg">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
</th>
</tr>
    </table>
</asp:Content>

