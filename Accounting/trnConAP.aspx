<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnConAP.aspx.vb" Inherits="Accounting_trnConAP" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <script type="text/javascript">
        function s() {

            try {
                var t = document.getElementById("<%=gvSupplier.ClientID%>");
        var t2 = t.cloneNode(true)
        for (i = t2.rows.length - 1; i > 0; i--)
            t2.deleteRow(i)
        t.deleteRow(0)
        div1.appendChild(t2)
    }
    catch (errorInfo) { }
    try {
        var t = document.getElementById("<%=gvAcc.ClientID%>");
        var t3 = t.cloneNode(true)
        for (i = t3.rows.length - 1; i > 0; i--)
            t3.deleteRow(i)
        t.deleteRow(0)
        div2.appendChild(t3)
    }
    catch (errorInfo) { }
}
window.onload = s
    </script>

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Saldo Awal Hutang"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="350px"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="800px" __designer:wfdid="w153" DefaultButton="btnFindConap"><TABLE width="100%"><TBODY><TR id="Tr1" runat="server" visible="false"><TD align=left><asp:Label id="Label9" runat="server" Text="Cabang" __designer:wfdid="w154"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="dd_branch" runat="server" Width="220px" CssClass="inpText" __designer:wfdid="w155">
                                                        </asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w156"></asp:Label> </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" CssClass="inpText" __designer:wfdid="w157" AutoPostBack="True" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged">
                                                            <asp:ListItem Value="name">Supplier</asp:ListItem>
                                                            <asp:ListItem Value="no">Invoice No</asp:ListItem>
                                                        </asp:DropDownList> <asp:TextBox id="FilterConap" runat="server" Width="123px" CssClass="inpText" __designer:wfdid="w158"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindConap" onclick="btnFindConap_Click" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w159"></asp:ImageButton> <asp:ImageButton id="btnViewAllConap" onclick="btnViewAllConap_Click" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w160"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="gvTrnConap" runat="server" Width="950px" ForeColor="#333333" __designer:wfdid="w151" OnSelectedIndexChanged="gvTrnConap_SelectedIndexChanged" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid" AllowPaging="True" OnRowDataBound="gvTrnConap_RowDataBound">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelimstoid" HeaderText="trnbelimstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Font-Strikeout="False" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppName" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbayar" HeaderText="Total Bayar" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelistatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; <strong><span style="font-size: 9pt">:: Daftar Saldo Awal Hutang</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<DIV style="WIDTH: 100%"><TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="suppoid" runat="server" Font-Size="X-Small" __designer:wfdid="w99" Visible="False"></asp:Label><asp:Label id="I_U" runat="server" Font-Bold="True" ForeColor="Red" Text="NEW DATA" __designer:wfdid="w108"></asp:Label></TD><TD align=left><asp:TextBox id="conapoid" runat="server" Width="10px" CssClass="inpText" __designer:wfdid="w100" Visible="False" Enabled="False" MaxLength="10"></asp:TextBox> <asp:TextBox id="trnbelimstoid" runat="server" Width="10px" CssClass="inpText" __designer:wfdid="w101" Visible="False" Enabled="False" MaxLength="10"></asp:TextBox> <asp:TextBox id="paymentoid" runat="server" Width="10px" CssClass="inpText" __designer:wfdid="w102" Visible="False" Enabled="False" MaxLength="10"></asp:TextBox> <asp:TextBox id="cashbankoid" runat="server" Width="10px" CssClass="inpText" __designer:wfdid="w103" Visible="False" Enabled="False" MaxLength="10"></asp:TextBox> <asp:TextBox id="trnglmstoid" runat="server" Width="10px" CssClass="inpText" __designer:wfdid="w104" Visible="False" Enabled="False" MaxLength="10"></asp:TextBox> <asp:TextBox id="trngldtloid" runat="server" Width="10px" CssClass="inpText" __designer:wfdid="w105" Visible="False" Enabled="False" MaxLength="10"></asp:TextBox> </TD><TD align=left><asp:Label id="CutofDate" runat="server" __designer:wfdid="w106" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="purchAcctgoid" runat="server" Width="32px" Font-Size="X-Small" __designer:wfdid="w107"></asp:Label></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w108" TargetControlID="paymentdate" PopupButtonID="imbPaymentDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w109" TargetControlID="invoicedate" PopupButtonID="imbInvoiceDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="lblSuppoid" runat="server" Font-Size="X-Small" Text="Supplier" __designer:wfdid="w110"></asp:Label> <asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w111"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 184px; COLOR: #000099" align=left><asp:TextBox id="suppcode" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w112" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btnCariSupp" onclick="btnCariSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w113"></asp:ImageButton></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="Label11" runat="server" Width="83px" Font-Size="X-Small" Text="Akun Hutang" __designer:wfdid="w114"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left colSpan=3><asp:TextBox id="acctgCode" runat="server" Width="325px" CssClass="inpTextDisabled" __designer:wfdid="w115" ReadOnly="True"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearcAcctg" onclick="btnSearcAcctg_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w116" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="Label21" runat="server" Font-Size="X-Small" Text="No.Nota" __designer:wfdid="w117"></asp:Label> <asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w118"></asp:Label> </TD><TD style="FONT-SIZE: 8pt; WIDTH: 184px; COLOR: #000099" align=left><asp:TextBox id="trnbelino" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w119" Enabled="true" MaxLength="20" AutoPostBack="True"></asp:TextBox> </TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="Label4" runat="server" Width="84px" Font-Size="X-Small" Text="Tanggal Nota*" __designer:wfdid="w120"></asp:Label> </TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="invoicedate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w121" Enabled="False" MaxLength="10" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbInvoiceDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w122" Visible="False"></asp:ImageButton> </TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="Label3" runat="server" Width="117px" Font-Size="X-Small" Text="Total Nota (DPP)" __designer:wfdid="w123"></asp:Label> </TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="dpp" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w124" Enabled="true" MaxLength="12" AutoPostBack="True" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 143px" class="Label" align=left>Mata Uang</TD><TD style="FONT-SIZE: 8pt; WIDTH: 184px; COLOR: #000099" align=left><asp:DropDownList id="curroid" runat="server" Width="180px" CssClass="inpTextDisabled" __designer:wfdid="w125" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="curroid_SelectedIndexChanged"></asp:DropDownList> </TD><TD class="Label" align=left>Currency Rate</TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="currate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w126" Enabled="False" MaxLength="30">1</asp:TextBox> </TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="Label24" runat="server" Font-Size="X-Small" Text="Tax (%)" __designer:wfdid="w127"></asp:Label> <asp:TextBox id="trntaxpct" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w128" Enabled="true" MaxLength="4" AutoPostBack="True" OnTextChanged="trntaxpct_TextChanged">0</asp:TextBox></TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="amttax" runat="server" Width="110px" CssClass="inpTextDisabled" __designer:wfdid="w129" Enabled="true" MaxLength="12" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="Label10" runat="server" Width="38px" Font-Size="X-Small" Text="Status" __designer:wfdid="w130"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 184px; COLOR: #000099" align=left><asp:Label id="lblPosting" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" __designer:wfdid="w131">In Process</asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="Label22" runat="server" Font-Size="X-Small" Text="Total Hutang" __designer:wfdid="w132"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="totalAP" runat="server" Width="123px" CssClass="inpTextDisabled" __designer:wfdid="w133" Enabled="true" MaxLength="12" ReadOnly="True" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="Label23" runat="server" Width="114px" Font-Size="X-Small" Text="Total Nota(Nett)" __designer:wfdid="w134"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="amtbeli" runat="server" Width="110px" CssClass="inpTextDisabled" __designer:wfdid="w135" Enabled="true" MaxLength="12" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="Label7" runat="server" Width="51px" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w136"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left colSpan=3><asp:TextBox id="trnapnote" runat="server" Width="404px" CssClass="inpText" __designer:wfdid="w137" Enabled="true" MaxLength="220" TextMode="MultiLine"></asp:TextBox></TD><TD style="FONT-SIZE: 8pt" align=left></TD><TD style="FONT-SIZE: 8pt" align=left></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 143px" align=left></TD><TD style="WIDTH: 184px" align=left><asp:DropDownList id="trnpaytype" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w138" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="trnpaytype_SelectedIndexChanged"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label17" runat="server" Width="94px" Font-Size="X-Small" Text="No.Pembayaran" __designer:wfdid="w139" Visible="False"></asp:Label> </TD><TD align=left colSpan=3>&nbsp;<asp:TextBox id="payrefno" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w140" Visible="False" Enabled="true" MaxLength="15"></asp:TextBox> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 143px" id="TD3" align=left runat="server" Visible="false"><asp:Label id="Label16" runat="server" Width="123px" Font-Size="X-Small" Text="Tanggal Pembayaran" __designer:wfdid="w141"></asp:Label></TD><TD id="TD5" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="paymentdate" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w142" Enabled="true" MaxLength="10"></asp:TextBox> <asp:ImageButton id="imbPaymentDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w143"></asp:ImageButton><ajaxToolkit:MaskedEditExtender id="mee6" runat="server" __designer:wfdid="w144" TargetControlID="paymentdate" Mask="99/99/9999" MaskType="Date" ClearMaskOnLostFocus="true" CultureName="id-ID" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender> <asp:TextBox id="payduedate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w145" Visible="False" Enabled="False" MaxLength="10"></asp:TextBox></TD><TD id="TD4" align=left runat="server" Visible="false"><asp:Label id="Label27" runat="server" Width="113px" Font-Size="X-Small" Text="Total Pembayaran" __designer:wfdid="w146" Visible="False"></asp:Label></TD><TD id="TD6" align=left runat="server" Visible="false"><asp:TextBox id="amtbayar" runat="server" Width="123px" CssClass="inpText" __designer:wfdid="w147" Visible="False" Enabled="true" MaxLength="12" AutoPostBack="True" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="paymentacctgoid" runat="server" Font-Size="X-Small" __designer:wfdid="w148" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="acctgoid" runat="server" Font-Size="X-Small" __designer:wfdid="w149" Visible="False"></asp:Label> <asp:TextBox id="payacctg" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w150" Visible="False" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbPayAcctg" onclick="imbPayAcctg_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w151" Visible="False"></asp:ImageButton> </TD><TD align=left><asp:Label id="postdate" runat="server" Font-Size="X-Small" Text="1/1/1900" __designer:wfdid="w152" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="amtbelinetto" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w153" Visible="False" Enabled="true" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD align=left><asp:Label id="Label5" runat="server" Width="76px" Font-Size="X-Small" Text="Jatuh Tempo" __designer:wfdid="w154" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="purchAcctg" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w155" Visible="False" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbPurch" onclick="imbPurch_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w156" Visible="False"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=6><asp:Label id="Label26" runat="server" Width="155px" Font-Size="X-Small" Text="Akun Pembayaran Hutang" __designer:wfdid="w157" Visible="False"></asp:Label> <BR />Last Update On <asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w158"></asp:Label> by <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w159"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="HEIGHT: 25px" align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w160"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w161"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w162"></asp:ImageButton> <asp:ImageButton id="imbposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w163"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w164" Visible="False"></asp:ImageButton>&nbsp;&nbsp;&nbsp;</TD></TR></TBODY></TABLE><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w165" TargetControlID="invoicedate" Mask="99/99/9999" MaskType="Date" ClearMaskOnLostFocus="true" CultureName="id-ID" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w166" TargetControlID="payduedate" Mask="99/99/9999" MaskType="Date" CultureName="id-ID" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w167" TargetControlID="dpp" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w168" TargetControlID="trntaxpct" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" __designer:wfdid="w169" TargetControlID="dpp" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> &nbsp;&nbsp; <TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:UpdatePanel id="UpdatePanel3" runat="server" __designer:wfdid="w170"><ContentTemplate>
<asp:Panel id="PanelSupp" runat="server" Width="400px" CssClass="modalBox" __designer:wfdid="w171" Visible="False"><TABLE><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSupp" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Supplier" __designer:wfdid="w172"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="Label2" runat="server" Text="Filter :" __designer:wfdid="w173"></asp:Label> <asp:DropDownList id="ddlSup" runat="server" CssClass="inpText" __designer:wfdid="w174">
                                                                                        <asp:ListItem>Name</asp:ListItem>
                                                                                        <asp:ListItem>Code</asp:ListItem>
                                                                                    </asp:DropDownList> <asp:TextBox id="filterSupp" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w175"></asp:TextBox> <asp:ImageButton id="btnFindSupp" onclick="btnFindSupp_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w176"></asp:ImageButton> <asp:ImageButton id="btnListSupp" onclick="btnListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w177"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvSupplier" runat="server" Width="97%" ForeColor="#333333" __designer:wfdid="w178" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged" DataKeyNames="suppoid,suppcode,suppname,coa_hutang,coa" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="40px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="40px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppoid" HeaderText="SuppOid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppName" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
                                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="ClosePopUP" onclick="ClosePopUP_Click" runat="server" __designer:wfdid="w179">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenSupp" runat="server" __designer:wfdid="w180" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w181" TargetControlID="btnHiddenSupp" PopupControlID="panelSupp" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> </TD><TD align=left><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w182"><ContentTemplate>
<asp:Panel id="PanelAcctg" runat="server" Width="400px" CssClass="modalBox" __designer:wfdid="w183" Visible="False"><TABLE><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblCOA" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar COA" __designer:wfdid="w184"></asp:Label> <asp:Label id="lblTypeCOA" runat="server" Text="APAccount" __designer:wfdid="w185" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="Label6" runat="server" Text="Filter :" __designer:wfdid="w186"></asp:Label> <asp:DropDownList id="ddlAcctg" runat="server" CssClass="inpText" __designer:wfdid="w187">
                                                                                        <asp:ListItem Value="Name">Description</asp:ListItem>
                                                                                        <asp:ListItem Value="Code">Number</asp:ListItem>
                                                                                    </asp:DropDownList> <asp:TextBox id="filterAcctg" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w188"></asp:TextBox> <asp:ImageButton id="btnFindAcctg" onclick="btnFindAcctg_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w189"></asp:ImageButton> <asp:ImageButton id="btnListAcctg" onclick="btnListAcctg_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w190"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvAcc" runat="server" Width="97%" ForeColor="#333333" __designer:wfdid="w191" OnSelectedIndexChanged="gvAcc_SelectedIndexChanged" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="AcctgOid" HeaderText="AcctgOid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="No.">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                                    <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
                                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> &nbsp; </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseAcctg" onclick="CloseAcctg_Click" runat="server" __designer:wfdid="w192">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenAcctg" runat="server" __designer:wfdid="w193" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" __designer:wfdid="w194" TargetControlID="btnHiddenAcctg" BackgroundCssClass="modalBackground" PopupControlID="panelAcctg"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> </TD></TR><TR><TD align=left></TD><TD align=left></TD></TR></TBODY></TABLE></DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; <strong><span style="font-size: 9pt">:: Form Saldo Awal Hutang</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK" Visible="False">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" TargetControlID="beMsgBox" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

