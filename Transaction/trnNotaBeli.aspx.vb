'Prgmr:Shutup_M 02-28-16
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnNotaBeli
    Inherits System.Web.UI.Page
    '(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[/](19|20)\d\d 'DMY
    '(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d 'MDY
#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim dv As DataView
    Private cKon As New Koneksi
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Dim cKoneksi As New Koneksi
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String = ""
    Dim crate As ClassRate
#End Region

#Region "Procedures"
    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub POUSer()
        sSql = "Select DISTINCT UPPER(upduser) UserPO, UPPER(upduser) POUser From QL_pomst"
        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " Where branch_code='" & dCabangNya.SelectedValue & "'"
        End If

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND upduser='" & Session("UserID") & "'"
            FillDDL(FilterDDLUserPO, sSql)
        Else 
            FillDDL(FilterDDLUserPO, sSql)
            FilterDDLUserPO.Items.Add(New ListItem("ALL", "ALL"))
            FilterDDLUserPO.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
                TypePOnya.SelectedValue = "Selisih"
                TypePOnya.Enabled = False
                CabangNya.Enabled = False
            Else
                FillDDL(CabangNya, sSql)
                'CabangNya.Items.Add(New ListItem("ALL", "ALL"))
                'CabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
            'CabangNya.Items.Add(New ListItem("ALL", "ALL"))
            'CabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKoneksi.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function DefineLocalOrImport(ByVal iCurrencyOid As Integer) As Integer
        ' 0 as LOCAL ; 1 as IMPORT
        ' 29 sebagai genoid country INDONESIA
        Dim iType As Integer = 0
        sSql = " select suppcountryoid from QL_mstsupp where suppname = '" & Tchar(suppliername.Text) & "'"

        Dim sTemp As String = GetStrData(sSql)
        If sTemp <> "?" Then
            If sTemp <> "29" Then
                iType = 1
            Else
                iType = 0
            End If
        Else
            iType = 0
        End If
        Return iType
    End Function

    Private Function InitAcctgBasedOnMasterData(ByVal sMasterTable As String, ByVal sMasterColumn As String, ByVal sAcctgColumn As String, _
        ByVal iMasterOid As Integer) As Integer
        sSql = "SELECT ISNULL(" & sAcctgColumn & ",0) FROM " & sMasterTable & " WHERE cmpcode='" & CompnyCode & "' AND " & sMasterColumn & "=" & iMasterOid
        Return cKoneksi.ambilscalar(sSql)
    End Function

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub initDDLPosting(ByVal sTypePayment As String, ByVal sInvoiceType As String)
        If sInvoiceType = "CSH00" Then
            If Trim(sTypePayment) = "CASH" Then
                Dim varCash As String = cKon.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_CASH'")
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varCash & "%'  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
                FillDDL(cashbankacctg, sSql)
                If cashbankacctg.Items.Count < 1 Then
                    showMessage("Please create/fill Cash Account in Chart of Accounts!!", _
                        CompnyName & " - WARNING", 2)
                End If
                lblcashbankacctg.Text = "Cash Account"
            Else
                Dim varBank As String = cKon.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_BANK'")
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varBank & "%'  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
                FillDDL(cashbankacctg, sSql)
                If cashbankacctg.Items.Count < 1 Then
                    showMessage("Please create/fill Bank Account in Chart of Accounts!!", _
                        CompnyName & " - WARNING", 2)
                End If
                lblcashbankacctg.Text = "Bank Account"
            End If
            lblpaymentype.Visible = True : paymentype.Visible = True
            lblcashbankacctg.Visible = True : cashbankacctg.Visible = True
            lblapaccount.Visible = False : apaccount.Visible = False
        Else
            Dim varAP As String = cKon.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_AP'")
            sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varAP & "%'  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
            FillDDL(apaccount, sSql)
            If apaccount.Items.Count < 1 Then
                showMessage("Please create/fill AP Account in Chart of Accounts!!", _
                    CompnyName & " - WARNING", 2)
            End If
            lblpaymentype.Visible = False : paymentype.Visible = False
            lblcashbankacctg.Visible = False : cashbankacctg.Visible = False
            lblapaccount.Visible = True : apaccount.Visible = True
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub InitAllDDL()
        FillDDLGen("PAYTYPE", TRNPAYTYPE)
        FillDDL(CurrencyOid, "select currencyoid, currencycode + '-' + currencydesc from QL_mstcurr where cmpcode='" & CompnyCode & "'")

        btnCancelMstr.Enabled = True
        If TRNPAYTYPE.Items.Count = 0 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "please create/fill Master Gen in group PAYTYPE!"))
            btnSaveMstr.Enabled = False
            btnDeleteMstr.Enabled = False : btnCancelMstr.Enabled = False
        ElseIf CurrencyOid.Items.Count = 0 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "please create/fill Master Currency & history!"))
            btnSaveMstr.Enabled = False
            btnDeleteMstr.Enabled = False : btnCancelMstr.Enabled = False
        End If

        Dim varPuch As String = cKon.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_CASH'")
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varPuch & "%'  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
        FillDDL(purchasingaccount, sSql)
        If purchasingaccount.Items.Count < 1 Then
            showMessage("Please create/fill Purchasing Account in Chart of Accounts!!", _
                CompnyName & " - WARNING", 2)
        End If
        'untu ddl kas cost
        Dim varCash As String = cKon.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_CASH'")

        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varCash & "%'  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
        FillDDL(CashForCost, sSql)
        If cashbankacctg.Items.Count < 1 Then
        End If
        lblcashbankacctg.Text = "Cash Account"
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 rate2idrvalue from ql_mstrate2 where currencyoid = " & iOid & " AND cmpcode LIKE '" & CompnyCode & "%' order by rate2date desc"
        Try
            currencyRate.Text = ToMaskEdit(ToDouble(cKon.ambilscalar(sSql).ToString), 3)
        Catch ex As Exception
            currencyRate.Text = ToMaskEdit(1, 3)
        End Try
    End Sub

    Public Sub BindData(ByVal sWhere As String)
        Try
            If chkPeriod.Checked = True Then
                Dim st1, st2 As Boolean : Dim sMsg As String = ""
                Try
                    Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text)) : st1 = True
                    If dat1 < CDate("01/01/1900") Then
                        sMsg &= "- Invalid Start date!!<BR>" : st1 = False
                    End If
                Catch ex As Exception
                    sMsg &= "- Invalid Start date!!<BR>" : st1 = False
                End Try
                Try
                    Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text)) : st2 = True
                    If dat2 < CDate("01/01/1900") Then
                        sMsg &= "- Invalid End date!!<BR>" : st2 = False
                    End If
                Catch ex As Exception
                    sMsg &= "- Invalid End date!!<BR>" : st2 = False
                End Try
                If st1 And st2 Then
                    If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                        sMsg &= "- End date can't be less than Start Date!!"
                    End If
                End If
                If sMsg <> "" Then
                    showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
                End If

            End If
            sSql = "SELECT a.trnbelimstoid,a.trnbelino, a.trnbelipono, a.trnbelidate, a.trnbelistatus, s.suppname supplier, a.trntaxpct trntaxtype, po.upduser FROM QL_trnbelimst a INNER JOIN QL_pomst po ON po.trnbelipono=a.trnbelipono INNER JOIN QL_mstsupp s ON a.trnsuppoid=s.suppoid WHERE a.cmpcode='" & CompnyCode & "' AND A.trnbelitype = 'GROSIR' AND a.trnbelimstoid > 0"

            If chkPeriod.Checked = True Then
                sSql &= " AND a.trnbelidate BETWEEN '" & Format(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy") & "' AND '" & Format(CDate(toDate(FilterPeriod2.Text)), "MM/dd/yyyy") & "' "
            End If

            If chkStatus.Checked = True Then
                If DDLStatus.SelectedIndex <> 0 Then
                    sSql &= " AND a.trnbelistatus LIKE '%" & DDLStatus.SelectedValue & "%'"
                End If
            End If

            If FilterDDLUserPO.SelectedValue <> "ALL" Then
                sSql &= " AND a.upduser='" & FilterDDLUserPO.SelectedValue & "'"
            End If

            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= "AND a.branch_code='" & dCabangNya.SelectedValue & "'"
            End If

            sSql &= sWhere & " ORDER BY a.trnbelidate desc, a.trnbelino desc"
            sqlTempSearch = sSql ' Untuk simpan history search

            FillGV(tbldata, sSql, "ql_trnbelimst")
        Catch ex As Exception
            showMessage(ex.ToString, "", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub FillTextBox(ByVal iOid As Int64)
        sSql = "SELECT a.branch_code,a.cmpcode,a.trnbelimstoid,a.periodacctg,a.trnbelitype,a.trnbelidate,a.trnbelino,a.trnbeliref,a.trnsuppoid,a.trnpaytype,a.trnbelidisc,a.trnbelidisctype,a.trnbelinote,a.trnbelistatus,a.trntaxpct,a.upduser,a.updtime,a.amtdiscdtl,a.amtdischdr,a.amtbeli,a.trnamttax,a.amtbelinetto,s.suppname supplier,a.currencyoid,a.currencyrate,a.trnbelipono, po.trnbelimstoid AS id, po.trnbelipodate,a.prefixoid, (select SUM(amtEkspedisi) biayaekspedisi from QL_trnbelidtl d where d.trnbelimstoid = a.trnbelimstoid) biayaekspedisi,a.typepo,a.Promodate FROM QL_trnbelimst a inner join QL_mstsupp s on a.cmpcode=s.cmpcode AND a.trnsuppoid=s.suppoid inner join ql_pomst po on a.trnbelipono = po.trnbelipono Where a.cmpcode ='" & CompnyCode & "' AND a.trnbelimstoid ='" & iOid & "' AND A.TRNBELItype = 'GROSIR'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                InitDDLBranch()
                CabangNya.SelectedValue = xreader.Item("branch_code").ToString
                oid.Text = xreader.Item("trnbelimstoid")
                periodacctg.Text = Trim(xreader.Item("periodacctg").ToString)

                trnbelidate.Text = Format(CDate(Trim(xreader.Item("trnbelidate").ToString)), "dd/MM/yyyy")
                TglPromo.Text = Format(CDate(Trim(xreader.Item("Promodate"))), "dd/MM/yyyy")

                podate.Text = Format(CDate(Trim(xreader.Item("trnbelipodate").ToString)), "dd/MM/yyyy")
                trnbelino.Text = Trim(xreader.Item("trnbelino"))
                dbpino.Text = Trim(xreader.Item("trnbelino"))
                trnbeliref.Text = IIf(IsDBNull(xreader("trnbeliref")), "", xreader.Item("trnbeliref"))
                trnsuppoid.Text = Trim(xreader.Item("trnsuppoid"))
                TRNPAYTYPE.SelectedValue = Trim(xreader.Item("trnpaytype"))
                trnbelidisc.Text = IIf(IsDBNull(xreader("trnbelidisc")), "", ToMaskEdit(xreader.Item("trnbelidisc"), 4))
                trnbelidisctype.SelectedValue = Trim(xreader.Item("trnbelidisctype"))
                trnbelinote.Text = Trim(xreader.Item("trnbelinote"))
                posting.Text = Trim(xreader.Item("trnbelistatus"))
                TypePOnya.SelectedValue = xreader.Item("typepo").ToString

                If posting.Text = "POST" Then
                    btnshowCOA.Visible = True
                End If

                trntaxpct.Text = ToMaskEdit(xreader.Item("trntaxpct"), 4)
                Upduser.Text = Trim(xreader.Item("upduser"))
                Updtime.Text = CDate(xreader.Item("updtime"))
                amtdiscdtl.Text = ToMaskEdit(xreader.Item("amtdiscdtl"), 4)
                amtdischdr.Text = ToMaskEdit(xreader.Item("amtdischdr"), 4)
                amtbeli.Text = ToMaskEdit(xreader.Item("amtbeli"), 4)
                ekspedisiAmt.Text = ToMaskEdit(xreader.Item("biayaekspedisi"), 4)
                mCabang.Text = Trim(xreader.Item("branch_code").ToString)
                If ToMaskEdit(xreader.Item("trntaxpct"), 4) > 0 Then
                    Tax.SelectedValue = "0"
                Else
                    Tax.SelectedValue = "1"
                End If
                amttax.Text = ToMaskEdit(xreader.Item("trnamttax"), 4)
                Tax_SelectedIndexChanged(Nothing, Nothing)
                amtbelinetto1.Text = ToMaskEdit(xreader.Item("amtbelinetto"), 4)
                suppliername.Text = Trim(xreader.Item("supplier"))
                trnbelipono.Text = Trim(xreader.Item("trnbelipono"))
                trnbelimstoid.Text = Trim(xreader.Item("id").ToString)
                CurrencyOid.SelectedValue = Trim(xreader.Item("currencyoid"))
                currencyoid_SelectedIndexChanged(Nothing, Nothing)
                currencyRate.Text = ToMaskEdit(xreader.Item("currencyrate"), 4)
                temptotalamount.Text = GetStrData("select (amtbeli-amtdiscdtl) temptotal  from QL_pomst where trnbelipono = '" & Trim(xreader.Item("trnbelipono")) & "' ")
                tempdisc.Text = GetStrData("select amtdischdr  from QL_pomst where trnbelipono = '" & Trim(xreader.Item("trnbelipono")) & "'")
                prefixoid.Text = Trim(xreader.Item("prefixoid"))
            End While
        End If
        xreader.Close()
        conn.Close()

        sSql = "SELECT d.trnbelidtloid,d.trnbelimstoid,d.trnbelidtlseq,d.itemoid mtroid, d.trnbelidtlqty,d.trnbelidtlunitoid trnbelidtlunit,d.trnbelidtlprice, d.trnbelidtlqtydisc trnbelidtldisc,d.trnbelidtldisctype,d.trnbelidtlqtydisc2 trnbelidtldisc2,d.trnbelidtldisctype2,d.trnbelidtlflag,d.trnbelidtlnote, d.trnbelidtlstatus,d.amtdisc,d.amtdisc2,d.amtbelinetto, (select itemdesc FROM QL_mstitem where itemoid = d.itemoid ) as material, g.gendesc AS unit,sj.trnsjbelioid sjoid,sj.branch_code to_branch_code,sj.trnsjbelioid sjrefoid,sj.trnsjbelino sjno,d.unitseq ,d.itemloc ,d.trnbelidtloid_po ,amtdisc totamtdisc,d.amtEkspedisi biayaexpedisi,sj.trnsjbelinote FROM QL_trnbelidtl d INNER JOIN ql_trnsjbelimst sj ON sj.cmpcode=d.cmpcode AND sj.trnsjbelino=d.trnsjbelino INNER JOIN ql_mstgen g on g.cmpcode=d.cmpcode AND g.genoid=d.trnbelidtlunitoid WHERE d.trnbelimstoid=" & iOid & " And sj.trnbelipono='" & Tchar(trnbelipono.Text) & "' order by d.trnbelidtlseq"

        Dim dtl As DataTable = cKon.ambiltabel(sSql, "ql_trnbelidtl")
        tbldtl2.DataSource = dtl : tbldtl2.DataBind()
        Session("TblDtl1") = dtl
        SumPriceInInvoiceDetail(Session("TblDtl1"))
        If posting.Text.ToUpper = "POST" Then
            btnSaveMstr.Visible = False : btnDeleteMstr.Visible = False
            BtnPosting2.Visible = False : btnSearchPO.Visible = False
            btnClearPO.Visible = False : btnSearchSJ.Visible = False
            btnClearSj.Visible = False : Export.Visible = True
            btnPrint.Visible = True : trnbelipono.Enabled = False
        Else
            btnSaveMstr.Visible = True
            btnDeleteMstr.Visible = True : BtnPosting2.Visible = True
            btnSearchPO.Visible = True : btnClearPO.Visible = True
            btnSearchSJ.Visible = True : btnClearSj.Visible = True
            Export.Visible = False : btnPrint.Visible = False
            trnbelipono.Enabled = True
        End If
        Dim userlogin As String = Session("UserID")
        Session("no") = ""
    End Sub

    Private Sub ClearItem()
        periodacctg.Text = ""
        trnbelidate.Text = Format(GetServerTime(), "dd/MM/yyyy")
        trnbelipono.Text = "" : trnbeliref.Text = ""
        trnsuppoid.Text = "" : TRNPAYTYPE.SelectedIndex = 0
        trnbelidisc.Text = "0.00" : trnbelidisctype.SelectedIndex = 0
        trnbelinote.Text = "" : trntaxpct.Text = "0.00"
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
        amtdiscdtl.Text = "0.00" : amtdischdr.Text = "0.00"
        amtbeli.Text = "0.00" : netdetailamt.Text = "0.00"
        amttax.Text = "0.00" : amtbelinetto1.Text = "0.00"
        suppliername.Text = "" : identifierno.Text = ""
        noteSJ.Text = "" : sjoid.Text = "" : to_branch_code.Text = ""
        btnAddToList.Enabled = False : btnCancelDtl.Enabled = False
        Session("TblDtl1") = Nothing : Session("tbldtl2") = Nothing
        Session("tbldtl3") = Nothing : tbldtl2.DataSource = Nothing
    End Sub

    Private Sub generateNo()
        Try
            Dim type As String = "" : type = "PI"

            If trnbelipono.Text.Substring(2, 1) = "1" Then
                Tax.SelectedIndex = 0
            Else
                Tax.SelectedIndex = 1
            End If

            sSql = "select genother1 from ql_mstgen where gengroup='cabang' and gencode='" & CabangNya.SelectedValue & "'"
            Dim cabang As String = cKon.ambilscalar(sSql)

            Dim sNo As String = type & trnbelipono.Text.Substring(2, 1) & "/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnbelino,4) AS INTEGER))+1,1) AS IDNEW FROM QL_trnbelimst " & _
                "WHERE cmpcode='" & CompnyCode & "' AND trnbelino LIKE '" & sNo & "%'"
            trnbelino.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
        Catch ex As Exception
            trnbelino.Text = ""
        End Try
    End Sub

    Private Sub generateNoNota()
        oid.Text = GenerateID("QL_trnbelimst", CompnyCode)
        trnbelino.Text = GenNumberString(BranchCode, "PI", oid.Text, DefaultFormatCounter)
    End Sub

    Public Sub binddataSupp()
        sSql = "Select suppoid,suppname from QL_mstsupp where suppname  LIKE '" & Tchar(suppliername.Text) & "%' and suppgroup='" & trnbelitype.Text & "' and cmpcode='" & CompnyCode & "'  ORDER BY suppname"
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

    End Sub

    Private Sub ReAmount()
        If trnbelidisc.Text.Trim = "" Then
            trnbelidisc.Text = "0.0000"
        End If
        If trntaxpct.Text.Trim = "" Then
            trntaxpct.Text = "0.0000"
        End If
        If amttax.Text.Trim = "" Then
            amttax.Text = "0.0000"
        End If
        If amtbeli.Text.Trim = "" Then
            amtbeli.Text = "0.0000"
        End If
        If amtdischdr.Text.Trim = "" Then
            amtdischdr.Text = "0.0000"
        End If
        If amtdiscdtl.Text.Trim = "" Then
            amtdiscdtl.Text = "0.0000"
        End If

        netdetailamt.Text = ToMaskEdit(ToDouble(amtbeli.Text) - ToDouble(amtdiscdtl.Text), 4)
        If tbldtl2.Rows.Count > 0 Then
            Dim agrossreturn As Double = 0.0 : Dim ataxamount As Double = 0.0
            Dim adiscamount As Double = 0.0 : Dim atotaldischeader As Double = 0.0
            Dim atotalamount As Double = 0.0 : Dim aReturndtlNet As Double = 0.0

            Dim drow As DataRow = Nothing
            Dim dtab As New DataTable

            If Not Session("TblDtl1") Is Nothing Then
                dtab = Session("TblDtl1")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    drow = dtab.Rows(i)
                    agrossreturn = agrossreturn + (drow.Item("amtbelinetto") + drow.Item("totamtdisc"))
                    adiscamount = adiscamount + drow.Item("totamtdisc")
                    aReturndtlNet = aReturndtlNet + drow.Item("amtbelinetto")
                Next

            End If
            amtdischdr.Text = ToMaskEdit(adiscamount, 4)
            amtdischdr.Text = IIf(trnbelidisctype.SelectedValue = "AMT", ToMaskEdit(ToDouble(trnbelidisc.Text), 4), ToMaskEdit((agrossreturn - adiscamount) * (ToDouble(trnbelidisc.Text) / 100), 4))
        End If

        amtbelinetto1.Text = ToMaskEdit((ToDouble(netdetailamt.Text) - ToDouble(amtdischdr.Text)), 3)  ' ToDouble(amttax.Text)
        If amtbelinetto1.Text.Trim = "" Then
            amtbelinetto1.Text = "0.0000"
        End If

        If Tax.SelectedIndex = 2 Then ' INCLUDE TAX
            amttax.Text = ToMaskEdit(Math.Ceiling((ToDouble(amtbelinetto1.Text) * ToDouble(trntaxpct.Text)) / (100 + ToDouble(trntaxpct.Text))), 4)
        Else
            amttax.Text = ToMaskEdit((ToDouble(trntaxpct.Text) / 100) * ToDouble(amtbelinetto1.Text), 4)
        End If
        If amttax.Text.Trim = "" Then
            amttax.Text = "0.0000"
        End If

        If Tax.SelectedIndex = 2 Then ' INCLUDE TAX
            amtbelinetto1.Text = ToMaskEdit(ToDouble(amtbelinetto1.Text), 4)
        Else
            amtbelinetto1.Text = ToMaskEdit(ToDouble(amtbelinetto1.Text) + ToDouble(amttax.Text), 4)
        End If
        If amtbelinetto1.Text.Trim = "" Then amtbelinetto1.Text = "0.00"

        totalinvoice.Text = ToMaskEdit(ToDouble(amtbelinetto1.Text) - ToDouble(totalamtbiaya.Text), 4)
    End Sub

    Private Sub ReAmountDetail()
        If trnbelidtlqty.Text.Trim = "" Then
            trnbelidtlqty.Text = "0.0000"
        End If
        If trnbelidtlprice.Text.Trim = "" Then
            trnbelidtlprice.Text = "0.0000"
        End If
        If trnbelidtldisc.Text.Trim = "" Then
            trnbelidtldisc.Text = "0.0000"
        End If

        amtbrutto.Text = ToMaskEdit(ToDouble(trnbelidtlqty.Text) * ToDouble(trnbelidtlprice.Text), 4)

        If trnbelidtldisctype.SelectedValue = "AMT" Then 'amount
            amtdisc.Text = ToMaskEdit(ToDouble(trnbelidtlqty.Text) * ToDouble(trnbelidtldisc.Text), 4)
        Else
            If ToDouble(trnbelidtldisc.Text) > 100 Then
                showMessage("Discount Persent can't be more than 100 !!!", CompnyName & " - WARNING", 2)
                trnbelidtldisc.Text = "0.00"
            End If
            amtdisc.Text = ToMaskEdit(ToDouble(trnbelidtlqty.Text) * ((ToDouble(trnbelidtldisc.Text) / 100)) * ToDouble(trnbelidtlprice.Text), 4)
        End If
        If amtdisc.Text.Trim = "" Then
            amtdisc.Text = "0.0000"
        End If
        If amtdisc2.Text.Trim = "" Then
            amtdisc2.Text = "0.0000"
        End If

        amtbelinetto2.Text = ToMaskEdit(((ToDouble(trnbelidtlprice.Text) * ToDouble(trnbelidtlqty.Text)) - ToDouble(amtdisc.Text) - ToDouble(amtdisc2.Text)), 4)
        If amtbelinetto2.Text.Trim = "" Then
            amtbelinetto2.Text = "0.0000"
        End If

        totdiscdtl.Text = ToMaskEdit(ToDouble(amtdisc.Text) + ToDouble(amtdisc2.Text), 4)
        If totdiscdtl.Text.Trim = "" Then
            totdiscdtl.Text = "0.0000"
        End If
    End Sub

    Private Sub ReAmountDetail2()
        If trnbelidtlqty.Text.Trim = "" Then
            trnbelidtlqty.Text = "0.0000"
        End If
        If trnbelidtlprice.Text.Trim = "" Then
            trnbelidtlprice.Text = "0.0000"
        End If
        If trnbelidtldisc2.Text.Trim = "" Then
            trnbelidtldisc2.Text = "0.0000"
        End If

        amtbrutto.Text = ToMaskEdit(ToDouble(trnbelidtlqty.Text) * ToDouble(trnbelidtlprice.Text), 4)

        If trnbelidtldisctype2.SelectedValue = "VCR" Then 'amount
            amtdisc2.Text = ToMaskEdit(ToDouble(trnbelidtldisc2.Text), 4)
        Else
            If ToDouble(trnbelidtldisc2.Text) > 100 Then
                showMessage("Discount Persent can't be more than 100 !!!", CompnyName & " - WARNING", 2)
                trnbelidtldisc2.Text = "0.0000"
            End If
            amtdisc2.Text = ToMaskEdit((ToDouble(trnbelidtlqty.Text) * ToDouble(trnbelidtlprice.Text) - ToDouble(amtdisc.Text)) * ((ToDouble(trnbelidtldisc2.Text) / 100)), 4)
        End If
        If amtdisc2.Text.Trim = "" Then
            amtdisc2.Text = "0.0000"
        End If

        amtbelinetto2.Text = ToMaskEdit(((ToDouble(trnbelidtlprice.Text) * ToDouble(trnbelidtlqty.Text)) - ToDouble(amtdisc.Text) - ToDouble(amtdisc2.Text)), 4)
        If amtbelinetto2.Text.Trim = "" Then
            amtbelinetto2.Text = "0.0000"
        End If

        totdiscdtl.Text = ToMaskEdit(ToDouble(amtdisc.Text) + ToDouble(amtdisc2.Text), 4)
        If totdiscdtl.Text.Trim = "" Then
            totdiscdtl.Text = "0.0000"
        End If
    End Sub

    Private Sub FillPODetailData()
        'data detail PO
        sSql = "SELECT d.cmpcode, d.trnbelidtloid, d.trnbelimstoid, d.trnbelidtlseq, d.mtrloc,d.mtroid, d.trnbelidtlqty, d.trnbelidtlunit, d.trnbelidtlprice, d.trnbelidtldisc, (case  d.trnbelidtldisctype when 1 then 'AMOUNT' else 'PERCENTAGE' end) AS trnbelidtldisctypeDesc,  d.trnbelidtlnote, d.trnbelidtlstatus, d.updtime, d.upduser,d.amtdisc, d.amtbelinetto  m.mtrdesc material, d.trnbelidtldisctype, (d.trnbelidtlqty*d.trnbelidtlprice) as amount,d.currencyoid, d.currencyrate, isnull((select sum(dod.trnsjbelidtlqty) from QL_trnsjbelimst do inner join QL_trnsjbelidtl dod on do.trnbelipono=p.trnbelipono and dod.trnsjbelimstoid=do.trnsjbelimstoid and dod.mtroid=d.mtroid and do.trnsjbelistatus='POST'),0)-d.trnbelidtlqty as 'plusMinus' FROM QL_podtl d inner join QL_mstgen g on g.genoid=d.mtrloc inner join QL_mstmtr m on m.mtroid=d.mtroid inner join QL_mstgen g2 on g2.genoid=d.mtroid inner join QL_pomst p on p.trnbelimstoid=d.trnbelimstoid where p.trnbelipono='" & trnbelipono.Text & "' order by d.mtroid "
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        Session("TblDtl3") = objDs.Tables("data")
    End Sub

    Public Sub BindDataPO(ByVal view As String, ByVal sPlus As String)
        Try
            sSql = " SELECT DISTINCT p.branch_code, p.trnbelimstoid, p.trnbelipodate, p.trnbelipono, p.trnsuppoid, p.trnbelinote, p.trnpaytype, s.suppname, p.trnbelitype, p.curroid, p.currate, p.trnbelidisctype, p.trnbelidisc, ISNULL(p.digit,2) as digit, p.prefixoid, p.upduser FROM QL_pomst p INNER JOIN QL_mstsupp s on s.suppoid=p.trnsuppoid AND p.cmpcode=s.cmpcode INNER JOIN ql_trnsjbelimst sj ON sj.cmpcode=p.cmpcode AND sj.trnbelipono=p.trnbelipono INNER JOIN ql_trnsjbelidtl sjd ON sjd.cmpcode=sj.cmpcode AND sj.trnsjbelioid=sjd.trnsjbelioid and sjd.branch_code= sj.branch_code Where rtrim(p.trnbelistatus) IN ('Approved', 'CLOSED MANUAL')"

            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " And p.upduser='" & Session("UseriD") & "'"
            End If

            sSql &= "And (p.trnbelipono Like '%" & TcharNoTrim(trnbelipono.Text) & "%' OR p.upduser LIKE '%" & TcharNoTrim(trnbelipono.Text) & "%') AND p.cmpcode='" & CompnyCode & "' AND p.typepo='" & TypePOnya.SelectedValue & "' AND P.TRNBELItype = 'GROSIR' GROUP BY p.trnbelimstoid, p.trnbelipodate, p.trnbelipono, p.branch_code, p.trnsuppoid, p.trnbelinote, sj.trnsjbelistatus, p.trnpaytype, s.suppname, p.trnbelitype, p.curroid, p.currate, p.trnbelidisctype, p.trnbelidisc, p.prefixoid, p.upduser, digit HAVING ISNULL(sj.trnsjbelistatus,'')<>'INVOICED' and sj.trnsjbelistatus = 'Approved' AND p.branch_code='" & CabangNya.SelectedValue & "' Order By p.trnbelipono DESC"
            FillGV(gvListPO, sSql, "ql_pomst")
        Catch ex As Exception
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1)
        End Try
    End Sub

    Private Sub BindSjdtl(ByVal PoNo As String)
        sSql = "Select (Select trnsjbelino From ql_trnsjbelimst mj Where mj.trnsjbelioid=dj.trnsjbelioid AND trnsjbelistatus='Approved') NoSJ,dj.trnsjbelidtlseq,m.itemoid,m.itemcode,m.itemdesc,dj.qty From ql_trnsjbelidtl dj INNER JOIN ql_mstitem m ON m.itemoid=dj.refoid Where dj.trnsjbelioid IN (Select trnsjbelioid From ql_trnsjbelimst mj Where mj.trnsjbelioid=dj.trnsjbelioid AND mj.trnbelipono LIKE '%" & Tchar(PoNo.Trim.Trim) & "%' AND trnsjbelistatus='Approved')"
        FillGV(ItemGv, sSql, "ql_pomst")
    End Sub

    Public Sub BindDataSJ(ByVal sSJNo As String, ByVal sPOoid As String)
        sSql = "SELECT DISTINCT sj.trnsjbelioid,sj.branch_code to_branch_code,sj.trnsjbelino,po.trnbelipodate,sj.trnsjbelidate trnsjreceivedate,sj.trnbelipono,po.trnbelimstoid,sj.trnsjbelinote FROM ql_trnsjbelimst sj INNER JOIN ql_pomst po ON sj.cmpcode=po.cmpcode AND sj.trnbelipono=po.trnbelipono INNER JOIN ql_trnsjbelidtl sjd ON sjd.cmpcode=sj.cmpcode AND sj.trnsjbelioid=sjd.trnsjbelioid and sjd.branch_code = sj.branch_code WHERE sj.cmpcode='" & CompnyCode & "' AND ISNULL(sj.trnsjbelistatus,'')<>'INVOICED' AND po.trnbelimstoid='" & sPOoid & "' AND trnsjbelistatus='Approved' AND sj.trnsjbelino LIKE '%" & Tchar(noteSJ.Text) & "%' ORDER BY sj.trnsjbelino DESC"
        FillGV(gvListSJ, sSql, "ql_trnsjbelimst")
    End Sub

    Private Sub InsertToInvoiceDetail(ByVal sCmpCode As String, ByVal iMtrLoc As Int32, ByVal iMtroid As Int32, ByVal iQty As Int16, ByVal sUnit As String, ByVal sNote As String, ByVal sMaterial As String, ByVal sLocation As String)
        Dim dv2 As DataView
        If Session("tbldtl2") Is Nothing Then 'table nota beli detail
            Dim conn2 As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP"))
            sSql = "SELECT d.cmpcode, d.trnbelidtloid, d.trnbelimstoid, d.trnbelidtlseq, d.sjrefoid, d.mtroid, d.trnbelidtlqty, d.trnbelidtlunit, d.trnbelidtlprice, d.currencyoid, d.currencyrate, d.pricerupiah, d.trnbelidtldisc, d.trnbelidtldisctype, d.trnbelidtlflag, d.trnbelidtlnote, d.trnbelidtlstatus, d.updtime, d.upduser, d.amtdisc, d.amtbelinetto, m.mtrdesc material, (d.trnbelidtlqty*d.trnbelidtlprice) as amount, g.gendesc as location , '' as trnbelidtldisctypeDesc    FROM QL_trnbelidtl d inner join QL_mstmtr m on m.mtroid=d.mtroid inner join QL_mstgen g on g.genoid=d.mtrloc Where d.trnbelimstoid=-111"
            Dim mySqlDA As New SqlDataAdapter(sSql, conn2)
            Dim objDs As New DataSet
            mySqlDA.Fill(objDs, "data2")
            Session("TblDtl2") = objDs.Tables("data2")
        End If

        'cek nota detail
        Dim oTblDtl2 As DataTable = Session("TblDtl2")
        dv2 = oTblDtl2.DefaultView
        dv2.RowFilter = "mtrloc=" & iMtrLoc & "  and  mtroid=" & iMtroid
        If dv2.Count = 0 Then 'new nota beli detail, cause new material and location
            AddToInvoiceDetail(oTblDtl2, sCmpCode, iMtrLoc, iMtroid, iQty, sUnit, sNote, sMaterial, sLocation)
        Else 'update nota beli detail, just update qty doank
            Dim dr2 As DataRow = oTblDtl2.Rows(dv2.Item(0).Item(3) - 1) ' take from seq num-1 to get rownum
            dr2.BeginEdit()
            dr2(6) = dr2(6) + iQty
            dr2(22) = dr2(6) * dr2(8) 'amount

            dr2(19) = IIf(dr2(13) = 1, dr2(12), CDec(CDec(dr2(12)) / 100) * dr2(22)) 'amtdisc( jika type=1, amount, type2=persen, maka dikalikan persen nya dulu
            dr2(20) = dr2(22) - dr2(19) 'amtbelinetto
            dr2.EndEdit()
        End If
        dv2.RowFilter = ""
        Session("tbldtl2") = oTblDtl2
    End Sub

    Private Sub AddToInvoiceDetail(ByRef otblDtl2 As DataTable, ByVal sCmpCode As String, ByVal iMtrLoc As Int32, ByVal iMtroid As Int32, ByVal iQty As Int16, ByVal sUnit As String, ByVal sNote As String, ByVal sMaterial As String, ByVal sLocation As String)
        Dim otblDtl3 As DataTable = Session("tbldtl3") 'tabel detail po
        Dim dv3 As DataView = otblDtl3.DefaultView
        dv3.RowFilter = "mtrloc=" & iMtrLoc & "  and  mtroid=" & iMtroid

        Dim dr2 As DataRow = otblDtl2.NewRow 'new data for invoice detail
        If dv3.Count = 0 Then 'when each data in po not same
            dr2(0) = sCmpCode 'cmpcode
            dr2(1) = 0 'trnbelidtloid
            dr2(2) = 0 'trnbelimstoid
            dr2(3) = otblDtl2.Rows.Count + 1 'trnbelidtlseq
            dr2(4) = iMtrLoc 'mtrloc
            dr2(5) = iMtroid 'mtroid
            dr2(6) = iQty 'trnbelidtlqty
            dr2(7) = sUnit 'trnbelidtlunit
            dr2(8) = 0 'trnbelidtlprice
            dr2(9) = 0 'currencyoid
            dr2(10) = 0 'currencyrate
            dr2(11) = 0 'pricerupiah
            dr2(12) = 0 'trnbelidtldisc
            dr2(13) = 1 'trnbelidtldisctype
            dr2(14) = "-" 'trnbelidtlflag
            dr2(15) = "-" 'trnbelidtlnote
            dr2(16) = "-" 'trnbelidtlstatus
            dr2(17) = Now 'updtime
            dr2(18) = Session("userid") 'upduser
            dr2(19) = 0 'amtdisc
            dr2(20) = 0 'amtbelinetto
            dr2(21) = sMaterial ' material
            dr2(22) = 0 'amount
            dr2(23) = sLocation 'location 
            dr2(24) = "Amount" 'trnbelidtldisctypeDesc
        Else 'when one data in detail PO same, then the data especially price etc, send to invoice detail
            dr2(0) = sCmpCode 'cmpcode
            dr2(1) = 0 'trnbelidtloid
            dr2(2) = 0 'trnbelimstoid
            dr2(3) = otblDtl2.Rows.Count + 1 'trnbelidtlseq
            dr2(4) = iMtrLoc 'mtrloc
            dr2(5) = iMtroid 'mtroid
            dr2(6) = iQty 'trnbelidtlqty
            dr2(7) = sUnit 'trnbelidtlunit
            dr2(8) = dv3.Item(0).Item(8) 'trnbelidtlprice
            dr2(9) = dv3.Item(0).Item(20) 'currencyoid
            dr2(10) = dv3.Item(0).Item(21) 'currencyrate
            dr2(11) = 0 'pricerupiah
            dr2(12) = dv3.Item(0).Item(9) 'trnbelidtldisc
            dr2(13) = dv3.Item(0).Item(18) 'trnbelidtldisctype(1=amount; 2=percentage)
            dr2(14) = "-" 'trnbelidtlflag
            dr2(15) = dv3.Item(0).Item(11) 'trnbelidtlnote
            dr2(16) = "-" 'trnbelidtlstatus
            dr2(17) = Now 'updtime
            dr2(18) = Session("userid") 'upduser
            dr2(22) = iQty * dv3.Item(0).Item(8) 'amount

            dr2(19) = IIf(dv3.Item(0).Item(18) = 1, dv3.Item(0).Item(9), CDec(CDec(dv3.Item(0).Item(9)) / 100) * dr2(22))
            'amtdisc( jika type=1, amount, type2=persen, maka dikalikan persen nya dulu
            dr2(20) = dr2(22) - dr2(19) 'amtbelinetto
            dr2(21) = sMaterial ' material
            dr2(23) = sLocation 'location  
            dr2(24) = dv3.Item(0).Item(10) 'trnbelidtldisctypeDesc

        End If
        otblDtl2.Rows.Add(dr2)
    End Sub

    Private Sub SumPriceInInvoiceDetail(ByVal oTblDtl2 As DataTable)
        Dim dAmountDetail, dDiscDetail, dVoucher, dEkspedisi As Double
        If Not oTblDtl2 Is Nothing Then
            For C1 As Int16 = 0 To oTblDtl2.Rows.Count - 1
                dAmountDetail += ToDouble((oTblDtl2.Rows(C1).Item("trnbelidtlprice"))) * ToDouble((oTblDtl2.Rows(C1).Item("trnbelidtlqty")))
                dDiscDetail += (oTblDtl2.Rows(C1).Item("amtdisc")) ' * oTblDtl2.Rows(C1).Item("currencyrate"))
                dVoucher += (oTblDtl2.Rows(C1).Item("amtdisc2"))
                dEkspedisi += (oTblDtl2.Rows(C1).Item("biayaexpedisi"))
            Next
        End If
        txtamount.Text = ToMaskEdit(dAmountDetail, 3)
        txtAmountNetto.Text = ToMaskEdit(dAmountDetail - dDiscDetail, 3)
        amtbeli.Text = ToMaskEdit(dAmountDetail, 3)
        amtdiscdtl.Text = ToMaskEdit(dDiscDetail, 3)
        voucherAmt.Text = ToMaskEdit(dVoucher, 3)
        ekspedisiAmt.Text = dEkspedisi
    End Sub

    Private Sub sumCostInvoice(ByVal dt As DataTable)
        Dim dTotal As Double = 0
        If Not dt Is Nothing Then
            For c2 As Int16 = 0 To dt.Rows.Count - 1
                dTotal += (dt.Rows(c2).Item("itembiayaamt"))
            Next
        End If
        totalamtbiaya.Text = ToMaskEdit(dTotal, 3)
    End Sub

    Private Sub RemoveDataInInvoiceDetail(ByVal iMtrloc As Int64, ByVal iMtroid As Int64, ByVal iQty As Int64)
        Dim objTable2 As DataTable = Session("tbldtl2")
        Dim iTotCount As Int16 = objTable2.Rows.Count - 1
        Dim C1 As Int16 = 0
        While C1 <= iTotCount
            If objTable2.Rows(C1).Item(4) = iMtrloc And objTable2.Rows(C1).Item(5) = iMtroid Then
                Dim dr2 As DataRow = objTable2.Rows(C1)
                dr2.BeginEdit()
                dr2(6) = dr2(6) - iQty
                dr2(22) = dr2(6) * dr2(8) 'amount

                dr2(19) = IIf(dr2(13) = 1, dr2(12), CDec(CDec(dr2(12)) / 100) * dr2(22)) 'amtdisc( jika type=1, amount, type2=persen, maka dikalikan persen nya dulu
                dr2(20) = dr2(22) - dr2(19) 'amtbelinetto
                dr2.EndEdit()

                If objTable2.Rows(C1).Item(6) <= 0 Then ' maka harus didelete
                    objTable2.Rows.RemoveAt(C1)
                    iTotCount -= 1
                Else
                    C1 += 1
                End If
            Else
                C1 += 1
            End If
        End While
        Session("tbldtl2") = objTable2
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchPI")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchPI") = sqlSearch
            Response.Redirect("~\Transaction\trnNotaBeli.aspx")
        End If

        Page.Title = CompnyName & " - Purchase Invoice"
        Session("oid") = Request.QueryString("oid")
        btnDeleteMstr.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        lkbPostPreview.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data ?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "UPDATE"
            TabContainer1.ActiveTabIndex = 1
        Else
            i_u.Text = "N E W"
        End If 

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            fDDLBranch() : BindData("") : InitDDLBranch() : InitAllDDL()
            Dim tb1 As DataTable : tb1 = Session("TblDtl1")
            tbldtl2.DataSource = tb1 : tbldtl2.DataBind()
            POUSer() : SumPriceInInvoiceDetail(tb1)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                i_u.Text = "UPDATE"
                TabContainer1.ActiveTabIndex = 1
            Else
                i_u.Text = "N E W"
                Upduser.Text = Session("UserID")
                Updtime.Text = GetServerTime()
                posting.Text = "In Process"
                TglPromo.Text = Format(GetServerTime(), "dd/MM/yyyy")
                trnbelidate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                generateNodraf()
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDeleteMstr.Visible = False
                TabContainer1.ActiveTabIndex = 0
                btnPrint.Visible = False
                currencyoid_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
        ReAmount()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sWhere As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        BindData(sWhere) : Session("SearchPI") = sqlTempSearch
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        chkPeriod.Checked = False : chkStatus.Checked = False
        DDLStatus.SelectedIndex = 0
        ddlFilter.SelectedIndex = 0 : txtFilter.Text = ""
        BindData("")
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        binddataSupp()
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppliername.Text = ""
        trnsuppoid.Text = ""
    End Sub

    Protected Sub trnbelidisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidisctype.SelectedIndexChanged
        If trnbelidisctype.SelectedValue = "AMT" Then 'amount
            lbl1.Text = "Disc. Header (Rp.)"
        Else
            lbl1.Text = "Disc. Header (%)"
        End If
        trnbelidisc.Text = "0.00"
        ReAmount()
    End Sub

    Protected Sub trnbeliqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidisc.TextChanged, trntaxpct.TextChanged
        Dim sMsg As String = ""
        If ToDouble(trnbelidisc.Text) > ToDouble(amtbelinetto1.Text) Then
            sMsg &= "- Dicount Header not more than Total Invoice !!<BR>"
            trnbelidisc.Text = "0.00"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        ReAmount()
    End Sub

    Protected Sub btnSearchPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPO.Click
        Dim view As String = "view"
        BindDataPO(view, "") : gvListPO.Visible = True
    End Sub

    Protected Sub btnClearPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearPO.Click
        ReAmount() : ClearItem()
        gvListPO.Visible = False
        DisposeGrid(gvListPO) : ItemGv.Visible = False
        Session("TblDtl1") = Nothing : Session("tbldtl2") = Nothing
        Session("tbldtl3") = Nothing : tbldtl2.DataSource = Nothing
    End Sub

    Protected Sub btnSearchSJ_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSJ.Click
        If trnbelimstoid.Text = "" Then
            showMessage("Please select Purchase Order first!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        BindDataSJ(noteSJ.Text, trnbelimstoid.Text) : gvListSJ.Visible = True
    End Sub

    Protected Sub btnClearsj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSj.Click
        ReAmount()
        sjoid.Text = "" : noteSJ.Text = "" : to_branch_code.Text = ""
        gvListSJ.Visible = False : DisposeGrid(gvListSJ)
        gvListSJDetail.Visible = False : DisposeGrid(gvListSJDetail)
        btnAddToList.Visible = False : btnCancelDtl.Visible = False
    End Sub

    Protected Sub tbldtl2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldtl2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 4)
            e.Row.Cells(8).Text = ToMaskEdit(e.Row.Cells(8).Text, 4)
            e.Row.Cells(9).Text = ToMaskEdit(e.Row.Cells(9).Text, 4)
        End If
    End Sub

    Protected Sub tbldtl2_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles tbldtl2.RowEditing
        Dim objTable2 As DataTable = Session("tbldtl2")
        Material.Text = objTable2.Rows(e.NewEditIndex).Item(21)
        Location.Text = objTable2.Rows(e.NewEditIndex).Item(23)
        trnbelidtlqty.Text = ToMaskEdit(objTable2.Rows(e.NewEditIndex).Item(6), 4)
        trnbelidtlprice.Text = ToMaskEdit(objTable2.Rows(e.NewEditIndex).Item(8), 4)
        trnbelidtldisctype.SelectedValue = objTable2.Rows(e.NewEditIndex).Item(13)
        trnbelidtldisc.Text = ToMaskEdit(objTable2.Rows(e.NewEditIndex).Item(12), 4)
        amtdisc.Text = ToMaskEdit(objTable2.Rows(e.NewEditIndex).Item(19), 4)
        amtbelinetto2.Text = ToMaskEdit(objTable2.Rows(e.NewEditIndex).Item(20), 4)
        trnbelidtlnote.Text = objTable2.Rows(e.NewEditIndex).Item(15)
        iRow.Text = e.NewEditIndex
    End Sub

    Protected Sub trnbelidtldisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidtldisctype.SelectedIndexChanged
        If trnbelidtldisctype.SelectedValue = "AMT" Then 'amount
            lbl2.Text = "Discount (Rp)"
        Else
            lbl2.Text = "Discount (%)"
        End If
        trnbelidtldisc.Text = "0.00"
        ReAmountDetail()
    End Sub

    Protected Sub trnbelidtlqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidtlprice.TextChanged, trnbelidtldisc.TextChanged
        ReAmountDetail()
    End Sub

    Protected Sub currencyoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CurrencyOid.SelectedIndexChanged
        FillCurrencyRate(CurrencyOid.SelectedItem.Value)
    End Sub

    Private Function setTableDetail() As DataTable
        Dim dt As DataTable = New DataTable("TblDtl1")
        dt.Columns.Add("trnbelidtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelimstoid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelidtlseq", Type.GetType("System.Int32"))
        dt.Columns.Add("mtroid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelidtlqty", Type.GetType("System.Double"))
        dt.Columns.Add("trnbelidtlunit", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelidtlprice", Type.GetType("System.Double"))
        dt.Columns.Add("currencyoid", Type.GetType("System.Int32"))
        dt.Columns.Add("currencyrate", Type.GetType("System.Double"))
        dt.Columns.Add("trnbelidtldisc", Type.GetType("System.Double"))
        dt.Columns.Add("trnbelidtldisctype", Type.GetType("System.String"))
        dt.Columns.Add("trnbelidtldisc2", Type.GetType("System.Double"))
        dt.Columns.Add("trnbelidtldisctype2", Type.GetType("System.String"))
        dt.Columns.Add("trnbelidtlflag", Type.GetType("System.String"))
        dt.Columns.Add("trnbelidtlnote", Type.GetType("System.String"))
        dt.Columns.Add("trnbelidtlstatus", Type.GetType("System.String"))
        dt.Columns.Add("amtdisc", Type.GetType("System.Double"))
        dt.Columns.Add("amtdisc2", Type.GetType("System.Double"))
        dt.Columns.Add("totamtdisc", Type.GetType("System.Double"))
        dt.Columns.Add("amtbelinetto", Type.GetType("System.Double"))
        dt.Columns.Add("biayaexpedisi", Type.GetType("System.Double"))
        dt.Columns.Add("material", Type.GetType("System.String"))
        dt.Columns.Add("unit", Type.GetType("System.String"))
        dt.Columns.Add("sjoid", Type.GetType("System.Int32"))
        dt.Columns.Add("sjno", Type.GetType("System.String"))
        dt.Columns.Add("trnsjbelinote", Type.GetType("System.String"))
        dt.Columns.Add("sjrefoid", Type.GetType("System.Int32"))
        dt.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dt.Columns.Add("mtrref", Type.GetType("System.String"))
        dt.Columns.Add("unitseq", Type.GetType("System.Int32"))
        dt.Columns.Add("itemloc", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelidtloid_po", Type.GetType("System.Int32"))
        dt.Columns.Add("to_branch_code", Type.GetType("System.String"))
        Return dt
    End Function 

    Private Sub ClearDetail()
        noteSJ.Text = "" : sjoid.Text = "" : iRow.Text = ""
        Material.Text = "" : unit.Text = ""
        trnbelidtlqty.Text = "0.00" : trnbelidtlprice.Text = "0.00"
        amtbrutto.Text = "0.00" : to_branch_code.Text = ""
        trnbelidtldisctype.SelectedIndex = 0 : trnbelidtldisc.Text = "0.00"
        trnbelidtldisctype2.SelectedIndex = 0 : trnbelidtldisc2.Text = "0.00"
        amtdisc.Text = "0.00" : amtdisc2.Text = "0.00" : amtbelinetto2.Text = "0.00"
        totdiscdtl.Text = "0.00" : lbl2.Text = "Discount (Rp)" : lbl3.Text = "Discount (Rp)"
        trnbelidtlnote.Text = "" : tbldtl2.SelectedIndex = -1 : tbldtl2.Columns(9).Visible = True
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
        btnSearchSJ.Visible = True : btnClearSj.Visible = True
        btnAddToList.Visible = False : btnCancelDtl.Visible = False
    End Sub

    Protected Sub btnCancelMstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing : Session("TblDtl1") = Nothing
        Session("TblDtl2") = Nothing : Session("TblDtl3") = Nothing
        Response.Redirect("~\Transaction\trnNotaBeli.aspx?awal=true")
    End Sub

    Protected Sub btnDeleteMstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If oid.Text.Trim = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please Choose Invoice No!"))
            Exit Sub
        End If

        'without check data in other table
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
       
            ' Reverse SJDetail Status (Invoiced) data lama
            sSql = "UPDATE ql_trnsjbelimst SET trnsjbelistatus='Approved' WHERE trnsjbelino in (select trnsjbelino from QL_trnbelidtl WHERE cmpcode='" & CompnyCode & "' AND trnbelimstoid=" & oid.Text & ") "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnbelidtl where trnbelimstoid=" & oid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from  QL_trnbelimst  where trnbelimstoid=" & oid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & " (SQL = " & sSql & ")", CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        Session("oid") = Nothing : Session("TblDtl1") = Nothing
        Session("TblDtl2") = Nothing : Session("TblDtl3") = Nothing
        Response.Redirect("~\Transaction\trnNotaBeli.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnPosting2.Click
        Dim dtlTable As DataTable = New DataTable("JurnalPreview")
        dtlTable.Columns.Add("code", Type.GetType("System.String"))
        dtlTable.Columns.Add("desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("debet", Type.GetType("System.Double"))
        dtlTable.Columns.Add("credit", Type.GetType("System.Double"))
        dtlTable.Columns.Add("id", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32")) ' utk seq bila 1 transaksi ada lebih 1 jurnal
        dtlTable.Columns.Add("seqdtl", Type.GetType("System.Int32")) ' utk seq bila 1 account bisa muncul dipakai berkali2
        Session("JurnalPreview") = dtlTable

        Dim iSeq As Integer = 1 : Dim iSeqDtl As Integer = 1
        Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
        Dim nuRow As DataRow

        ' FIRST JURNAL ==> Jurnal Purchase Invoice
        '
        ' Persediaan (Gudang Grosir)
        ' Persediaan (Gudang Supplier)
        ' PPN Masukan
        '           Hutang Usaha
        '           Persediaan (Potongan Pembelian)
        '           'UM Pembelian 

        ' ============ Persediaan
        If Not IsNothing(Session("TblDtl1")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl1")
            ' ====== GET matacctgoid 
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iMtrOid As Integer = dtDtl.Rows(C1)("mtroid").ToString
                Dim lokasi As String = GetStrData("select genother1 From ql_mstgen Where genoid = (select genother1 From QL_mstgen Where genoid = " & dtDtl.Rows(C1)("itemloc") & " AND gengroup = 'LOCATION') And gengroup = 'WAREHOUSE'")

                Dim sTockFlag As String = GetStrData("Select stockflag From ql_mstitem Where itemoid= " & dtDtl.Rows(C1)("mtroid").ToString & "")
                'begin pisah gudang

                Dim iMatAccount As String = "" : Dim xBRanch As String = ""
                If lokasi = "GROSIR" Then
                    If sTockFlag = "I" Then
                        xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & dtDtl.Rows(C1)("itemloc").ToString & " AND genother4='" & sTockFlag & "'")
                        iMatAccount = GetVarInterface("VAR_INVENTORY", xBRanch)
                        If iMatAccount = "?" Then
                            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_INVENTORY' !!", CompnyName & " - WARNING", 2)
                            Session("click_post") = "false"
                            Exit Sub
                        End If
                    Else
                        xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & dtDtl.Rows(C1)("itemloc").ToString & " AND genother4='" & sTockFlag & "'")
                        iMatAccount = GetVarInterface("VAR_GUDANG", xBRanch)
                        If iMatAccount = "?" Then
                            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_GUDANG' !!", CompnyName & " - WARNING", 2)
                            Exit Sub
                        End If
                    End If
                    
                    Dim iAPGudOid As Integer = GetAccountOid(iMatAccount, False)
                    If dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                        Dim oRow As DataRow = dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl)(0)
                        oRow.BeginEdit()
                        oRow("debet") += (ToDouble(dtDtl.Rows(C1)("amtbelinetto").ToString) - ToDouble(dtDtl.Rows(C1)("amtdisc2").ToString) + ToDouble(dtDtl.Rows(C1)("biayaexpedisi").ToString))
                        oRow.EndEdit()
                        dtJurnal.AcceptChanges()
                    Else

                        nuRow = dtJurnal.NewRow
                        nuRow("code") = GetCodeAcctg(iAPGudOid)
                        nuRow("desc") = GetDescAcctg(iAPGudOid) & " (Persediaan)"
                        nuRow("debet") = (ToDouble(dtDtl.Rows(C1)("amtbelinetto").ToString) - ToDouble(dtDtl.Rows(C1)("amtdisc2").ToString) + ToDouble(dtDtl.Rows(C1)("biayaexpedisi").ToString))
                        nuRow("credit") = 0
                        nuRow("id") = iAPGudOid
                        nuRow("seq") = iSeq
                        nuRow("seqdtl") = iSeqDtl
                        dtJurnal.Rows.Add(nuRow)
                    End If
                Else
                    xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & dtDtl.Rows(C1)("itemloc").ToString & " AND genother4='" & sTockFlag & "'")
                    iMatAccount = GetVarInterface("VAR_GUDANG", xBRanch)
                    If iMatAccount = "?" Then
                        showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_GUDANG_SUPP' !!", CompnyName & " - WARNING", 2)
                        Session("click_post") = "false"
                        Exit Sub
                    End If

                    Dim iAPGudOid As Integer = GetAccountOid(iMatAccount, False)

                    If dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                        Dim oRow As DataRow = dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl)(0)
                        oRow.BeginEdit()
                        oRow("debet") += (ToDouble(dtDtl.Rows(C1)("amtbelinetto").ToString))
                        oRow.EndEdit()
                        dtJurnal.AcceptChanges()
                    Else
                        nuRow = dtJurnal.NewRow
                        nuRow("code") = GetCodeAcctg(iAPGudOid)
                        nuRow("desc") = GetDescAcctg(iAPGudOid) & " (Persediaan)"
                        nuRow("debet") = (ToDouble(dtDtl.Rows(C1)("amtbelinetto").ToString))
                        nuRow("credit") = 0
                        nuRow("id") = iAPGudOid
                        nuRow("seq") = iSeq
                        nuRow("seqdtl") = iSeqDtl
                        dtJurnal.Rows.Add(nuRow)
                    End If
                End If
            Next
            iSeqDtl += 1
        End If

        ' ============ PPN Masukan
        If ToDouble(amttax.Text) > 0 Then
            Dim sPPNM As String = GetVarInterface("VAR_PPN_BELI", CabangNya.SelectedValue)
            If sPPNM = "?" Then
                showMessage("Please Setup Interface for PPN Masukan on Variable 'VAR_PPN_BELI'", CompnyName & " - WARNING", 2)
                Exit Sub
            Else
                Dim iPPNMOid As Integer = GetAccountOid(sPPNM, False)
                nuRow = dtJurnal.NewRow
                nuRow("code") = GetCodeAcctg(iPPNMOid)
                nuRow("desc") = GetDescAcctg(iPPNMOid) & " (PPN Masukan)"
                nuRow("debet") = (ToDouble(amttax.Text))
                nuRow("credit") = 0
                nuRow("id") = iPPNMOid
                nuRow("seq") = iSeq
                nuRow("seqdtl") = iSeqDtl
                dtJurnal.Rows.Add(nuRow)
            End If
            iSeqDtl += 1
        End If

        ' ============ Voucher (Piutang Ragu - Ragu)
        If ToDouble(voucherAmt.Text) > 0 Then
            Dim svoucher As String = GetVarInterface("VAR_VOUCHER", CabangNya.SelectedValue)
            If svoucher = "?" Then
                showMessage("Please Setup Interface for Voucher on Variable 'VAR_VOUCHER'", CompnyName & " - WARNING", 2)
                Exit Sub
            Else
                Dim voucheroid As Integer = GetAccountOid(svoucher, False)
                nuRow = dtJurnal.NewRow
                nuRow("code") = GetCodeAcctg(voucheroid)
                nuRow("desc") = GetDescAcctg(voucheroid) & " (Voucher)"
                nuRow("debet") = (ToDouble(voucherAmt.Text))
                'nuRow("debet") = (ToDouble(amttax.Text) * ToDouble(currencyRate.Text))
                nuRow("credit") = 0
                nuRow("id") = voucheroid
                nuRow("seq") = iSeq
                nuRow("seqdtl") = iSeqDtl
                dtJurnal.Rows.Add(nuRow)
            End If
            iSeqDtl += 1
        End If

        ' ============ Hutang Usaha
        Dim sVarAP As String = "" : sVarAP = GetVarInterface("VAR_AP", CabangNya.SelectedValue)
        If sVarAP = "?" Then
            showMessage("Please Setup Interface for Account Payable on Variable 'VAR_AP' !!", CompnyName & " - WARNING", 2)
            Exit Sub
        Else
            Dim iAPOid As Integer = GetAccountOid(sVarAP, True)
            nuRow = dtJurnal.NewRow
            nuRow("code") = GetCodeAcctg(iAPOid)
            nuRow("desc") = GetDescAcctg(iAPOid) & " (Hutang Usaha)"
            nuRow("debet") = 0
            nuRow("credit") = ToDouble(totalinvoice.Text)
            nuRow("id") = iAPOid
            nuRow("seq") = iSeq
            nuRow("seqdtl") = iSeqDtl
            dtJurnal.Rows.Add(nuRow)
        End If
        iSeqDtl += 1

        ' ============ Biaya Ekspedisi (Ekspedisi)
        If ToDouble(ekspedisiAmt.Text) > 0 Then
            Dim eksp As String = GetVarInterface("VAR_EKSPEDISI", CabangNya.SelectedValue)
            If eksp = "?" Then
                showMessage("Please Setup Interface for Ekspedisi on Variable 'VAR_EKSPEDISI'", CompnyName & " - WARNING", 2)
                Exit Sub
            Else
                Dim ekspOid As Integer = GetAccountOid(eksp, False)
                nuRow = dtJurnal.NewRow
                nuRow("code") = GetCodeAcctg(ekspOid)
                nuRow("desc") = GetDescAcctg(ekspOid) & " (Ekspedisi)"
                nuRow("debet") = 0
                'nuRow("debet") = (ToDouble(amttax.Text) * ToDouble(currencyRate.Text))
                nuRow("credit") = (ToDouble(ekspedisiAmt.Text))
                nuRow("id") = ekspOid
                nuRow("seq") = iSeq
                nuRow("seqdtl") = iSeqDtl
                dtJurnal.Rows.Add(nuRow)
            End If
            iSeqDtl += 1
        End If
        ' ============ Persediaan (Potongan Pembelian)
        If Not IsNothing(Session("TblDtl1")) Then

            Dim iMatAccount As String = GetVarInterface("VAR_DISC_BELI", CabangNya.SelectedValue)
            If iMatAccount = "?" Then
                showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_DISC_BELI' !!", CompnyName & " - WARNING", 2)
                Session("click_post") = "false"
                Exit Sub
            End If

            ' === Calculate Diskon
            Dim dMatDiscDtl As Double = 0
            Dim dMatDiscHdr As Double = ToDouble(amtdischdr.Text)
            Dim iPDiscOid As Integer = GetAccountOid(iMatAccount, False)

            If (dMatDiscDtl + dMatDiscHdr) > 0 Then
                If dtJurnal.Select("id=" & iPDiscOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iPDiscOid & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += ((dMatDiscDtl + dMatDiscHdr))
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iPDiscOid)
                    nuRow("desc") = GetDescAcctg(iPDiscOid) & " (Potongan Pembelian)"
                    nuRow("debet") = 0
                    nuRow("credit") = ((dMatDiscDtl + dMatDiscHdr))
                    nuRow("id") = iPDiscOid
                    nuRow("seq") = iSeq
                    nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            End If
        End If
        iSeq += 1

        gvPreview.DataSource = dtJurnal : gvPreview.DataBind()
        Session("JurnalPreview") = dtJurnal
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
    End Sub

    Private Function GetCodeAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function GetDescAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function 

    Private Sub generateNodraf()
        trnbelino.Text = GenerateID("QL_trnbelimst", CompnyCode)
    End Sub 

    Protected Sub lkbDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim NoPo As String = sender.Tooltip
        BindSjdtl(NoPo) : ItemGv.Visible = True
    End Sub

    Protected Sub lkbDetailSJ_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("sjOid") = sender.Tooltip
        FillGV(gvListSJDetail, bindSJDetail(Session("sjOid")), "ql_trnsjbelidtl")
        gvListSJDetail.Visible = True
    End Sub

    Private Function bindSJDetail(ByVal sSJmstoid As String) As String
        Return "SELECT pod.trnbelimstoid,sj.trnsjbelioid,sj.branch_code to_branch_code,sj.trnsjbelino,sjd.trnsjbelidtlseq,sjd.mtrlocoid ,sjd.refoid,refname,(select itemdesc From ql_mstitem where itemoid = sjd.refoid) as matlongdesc, 0 as MATACCTGOID,sjd.qty,sjd.unitoid,pod.unitseq,g1.gendesc,pod.trnbelidtlprice,po.curroid,(SELECT c.currencydesc FROM ql_mstcurr c WHERE po.curroid=c.currencyoid)AS currency,po.currate,pod.trnbelidtldisctype,pod.trnbelidtldisctype2,pod.trnbelidtldisc,pod.trnbelidtldisc2,sjd.trnsjbelidtloid,'amtdisc'=case when pod.trnbelidtldisctype= 'AMT' then sjd.qty*pod.trnbelidtldisc Else ((sjd.qty*pod.trnbelidtlprice/100)*pod.trnbelidtldisc) End,'amtdisc2' = (trnbelidtldisc2 /pod.trnbelidtlqty) * sjd.qty,'totamtdisc'=case when pod.trnbelidtldisctype= 'AMT' then sjd.qty*pod.trnbelidtldisc Else ((sjd.qty*pod.trnbelidtlprice/100)*pod.trnbelidtldisc) end,'amtbelinetto' = (sjd.qty * pod.trnbelidtlprice ) - case when pod.trnbelidtldisctype= 'AMT' then sjd.qty*pod.trnbelidtldisc Else ((sjd.qty*pod.trnbelidtlprice/100)*pod.trnbelidtldisc) end,sjd.biayaexpedisi,sjd.mtrlocoid,pod.trnbelidtloid,sj.trnsjbelinote FROM ql_trnsjbelimst sj INNER JOIN ql_trnsjbelidtl sjd ON sj.cmpcode=sjd.cmpcode AND sjd.trnsjbelioid=sj.trnsjbelioid INNER JOIN ql_mstgen g1 ON sjd.cmpcode=g1.cmpcode AND g1.genoid=sjd.unitoid INNER JOIN ql_pomst po ON sj.cmpcode=po.cmpcode AND sj.trnbelipono=po.trnbelipono INNER JOIN ql_podtl pod ON po.cmpcode=pod.cmpcode AND po.trnbelimstoid=pod.trnbelimstoid and pod.trnbelidtloid = sjd.trnbelidtloid AND sjd.refoid=pod.itemoid WHERE sjd.cmpcode='" & CompnyCode & "' AND sjd.trnsjbelioid='" & sSJmstoid & "' order by sjd.trnsjbelidtlseq asc"
    End Function

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
    End Sub 

    Protected Sub tbldtl2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl2.RowDeleting
        Dim objTable As DataTable
        Dim objRow(), objRow2() As DataRow
        objTable = Session("TblDtl1")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objRow2 = objTable.Select("sjoid='" & objRow(e.RowIndex).Item("sjoid") & "'")
        For C1 As Integer = 0 To objRow2.Length - 1
            objTable.Rows.Remove(objRow2(C1))
        Next
        'resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C2)
            dr.BeginEdit() : dr(2) = C2 + 1 : dr.EndEdit()
        Next
        Session("TblDtl1") = objTable
        tbldtl2.DataSource = Session("TblDtl1")
        tbldtl2.DataBind()
        SumPriceInInvoiceDetail(Session("TblDtl1")) : ReAmount()
    End Sub

    Private Sub setControlTax(ByVal status As Boolean)
        PercentageTax.Visible = status : trntaxpct.Visible = status
        TaxAmmount.Visible = status : amttax.Visible = status
    End Sub

    Protected Sub Tax_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Tax.SelectedIndex = 0 Then
            setControlTax(False)
            trntaxpct.Text = "0.00"
        ElseIf Tax.SelectedIndex = 1 Then
            setControlTax(True)
            trntaxpct.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='TAX'")), 4)
        Else
            setControlTax(True)
            trntaxpct.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='TAX'")), 4)
        End If
        ReAmount()
    End Sub

    Protected Sub btnAddTolistCost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("tbldtlcost") Is Nothing Then
            Dim dtCost As DataTable = New DataTable("tbldtlcost")
            dtCost.Columns.Add("itembiayaoid", Type.GetType("System.Int32"))
            dtCost.Columns.Add("belibiayaseq", Type.GetType("System.Int32"))
            dtCost.Columns.Add("belibiayadtloid", Type.GetType("System.Int32"))
            dtCost.Columns.Add("cost", Type.GetType("System.String"))
            dtCost.Columns.Add("itembiayanote", Type.GetType("System.String"))
            dtCost.Columns.Add("itembiayaamt", Type.GetType("System.Double"))
            Session("tbldtlcost") = dtCost
        End If

        Dim objTable As DataTable : objTable = Session("tbldtlcost")
        Dim dv As DataView = objTable.DefaultView
        'Cek apa sudah ada item yang sama dalam Tabel Detail
        
        If dv.Count > 0 Then
            showMessage("This Data has been added before, please check!", CompnyName & " - WARNING", 2)
            dv.RowFilter = "" : Exit Sub
        End If
        dv.RowFilter = ""
        sumCostInvoice(Session("tbldtlcost"))
    End Sub

    Protected Sub gvListSJ_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSJ.PageIndexChanging
        gvListSJ.PageIndex = e.NewPageIndex
        BindDataSJ(noteSJ.Text, trnbelimstoid.Text) : gvListSJ.Visible = True
    End Sub

    Protected Sub gvListSJ_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListSJ.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub currencyRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub 

    Protected Sub lkbPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
        posting.Text = "POST"
        btnSaveMstr_Click(Nothing, Nothing)
    End Sub

    Protected Sub lkbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
        posting.Text = "In Process"
    End Sub

    Protected Sub paymentype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initDDLPosting(paymentype.SelectedValue, TRNPAYTYPE.SelectedItem.Text.Trim)
        mpePosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'untuk print
        report.Load(Server.MapPath(folderReport & "rptNotaBeliPTP.rpt"))
        Dim printID As Integer
        If Session("no") = "" Then
            Session("no") = trnbelino.Text
        End If
        printID = Session("oid")
        report.SetParameterValue("sWhere", " Where m.trnbelimstoid='" & printID & "' ")
        report.SetParameterValue("namaPencetak", Session("UserID"))
        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)
        report.PrintOptions.PaperSize = PaperSize.PaperA5
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Session("no") & "_" & Format(GetServerTime(), "dd_MM_yy"))
    End Sub

    Protected Sub tbldata_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("oid") = tbldata.SelectedDataKey.Item(0)
        Session("no") = tbldata.SelectedDataKey.Item(1)
        Session("taxx") = tbldata.SelectedDataKey.Item(2)
        btnPrint_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("SearchPI") Is Nothing = False Then
            BindLastSearch()
        End If
    End Sub

    Private Sub BindLastSearch()
        FillGV(tbldata, Session("SearchPI"), "ql_trnbelimst")
    End Sub

    Protected Sub gvListPO_PageIndexChanging1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPO.PageIndexChanging
        Dim view As String = "view"
        gvListPO.PageIndex = e.NewPageIndex
        BindDataPO(view, "") : gvListPO.Visible = True
    End Sub

    Protected Sub gvListPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListPO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub  

    Protected Sub trnbelidate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsDate(toDate(trnbelidate.Text)) Then
            showMessage("Invalid Invoice date !!", CompnyName & " - Warning", 2) : Exit Sub
        End If
        periodacctg.Text = GetDateToPeriodAcctg(CDate(toDate(trnbelidate.Text)))
        If Format(CDate(toDate(trnbelidate.Text)), "yy") <> Mid(dbpino.Text, 4, 2) Then
        Else
            If dbpino.Text <> "" Then
                trnbelino.Text = dbpino.Text
            End If
        End If
    End Sub

    Protected Sub tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldata.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Export.Click
        report.Load(Server.MapPath(folderReport & "rptNotaBeliPTP.rpt"))
        Dim printID As Integer

        If Session("no") = "" Then
            Session("no") = trnbelino.Text
        End If
        printID = Session("oid")
        report.SetParameterValue("sWhere", " Where m.trnbelimstoid='" & printID & "' ")
        report.SetParameterValue("namaPencetak", Session("UserID"))
        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)
        report.PrintOptions.PaperSize = PaperSize.PaperA5
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Session("no") & "_" & Format(GetServerTime(), "dd_MM_yy"))
    End Sub

    Public Sub showPrintExcel(ByVal oid As Integer)
        ' lblkonfirmasi.Text = ""
        Session("diprint") = "False"
        Response.Clear()
        Dim swhere As String = "" : Dim shaving As String = "" : Dim swhere2 As String = ""
        ' If dView.SelectedValue = "Master" Then
        sSql = "SELECT mm.trnbelipono, s.suppname,mm.trnbelipono,d.trnbelidtloid,d.trnbelimstoid,d.trnbelidtlseq,d.sjrefoid,d.mtroid,d.trnbelidtlqty,d.trnbelidtlunit,d.trnbelidtlprice,d.currencyoid,d.currencyrate,d.trnbelidtldisc,d.trnbelidtldisctype,d.trnbelidtlflag,d.trnbelidtlnote,d.trnbelidtlstatus,d.amtdisc,d.amtbelinetto,m.matlongdesc material, g.gendesc AS unit,sj.trnsjbelimstoid sjoid,sj.trnsjbelino sjno FROM QL_trnbelidtl d inner join QL_mstmat m on m.matoid=d.mtroid INNER JOIN ql_trnsjbelimst sj ON sj.cmpcode=d.cmpcode AND sj.trnsjbelimstoid=d.sjrefoid inner join ql_trnbelimst mm on mm.trnbelimstoid=d.trnbelimstoid INNER JOIN ql_mstgen g on g.cmpcode=d.cmpcode AND g.genoid=d.trnbelidtlunit inner join ql_mstsupp s on s.suppoid=mm.trnsuppoid WHERE d.trnbelimstoid=" & oid & " order by d.trnbelidtlseq"

        Response.AddHeader("content-disposition", "inline;filename=StatusPO.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub gvListSJDetail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSJDetail.PageIndexChanging
        gvListSJDetail.PageIndex = e.NewPageIndex
        ClassFunction.FillGV(gvListSJDetail, bindSJDetail(Session("sjOid")), "ql_trnsjbelidtl")

        gvListSJDetail.Visible = True
    End Sub

    Protected Sub tbldata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        tbldata.PageIndex = e.NewPageIndex
        Dim sWhere As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        BindData(sWhere)
        Session("SearchPI") = sqlTempSearch
    End Sub 

    Protected Sub lkbClosePreview_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, False)
    End Sub

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sInterfaceVar & "' AND interfaceres1='" & Session("branch_id") & "'"
        Return GetStrData(sSql)
    End Function

    Private Sub FillDDLAccounting(ByVal oDDLAccount As DropDownList, ByVal sFilterCode As String)
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        FillDDL(oDDLAccount, sSql)
    End Sub

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A'"
        If Not bParentAllowed Then
            sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) "
        End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function

    Protected Sub gvPreview_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPreview.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub postingfaktur_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        sSql = "UPDATE ql_trnbelimst SET nofaktur_pajak='" & Tchar(txtnofakturpajak.Text) & "', statusfakturpajak='POST', userfakturpajak='" & Session("UserID") & "' WHERE trnbelimstoid=" & oid.Text
        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

        objTrans.Commit() : xCmd.Connection.Close()
        Response.Redirect("~\Transaction\trnNotaBeli.aspx?awal=true")
    End Sub

    Protected Sub trnbelidtldisctype2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If trnbelidtldisctype2.SelectedValue = "VCR" Then 'amount
            lbl3.Text = "Discount (Rp)"
        Else
            lbl3.Text = "Discount (%)"
        End If
        trnbelidtldisc2.Text = "0.00"
        'ReAmountDetail2()
    End Sub

    Protected Sub trnbelidtldisc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'ReAmountDetail2()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showTableCOA(trnbelino.Text, mCabang.Text, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

#End Region

    Protected Sub btnSaveMstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveMstr.Click
        Dim sMsg As String = ""
        If trnbelipono.Text.Trim = "" Then
            sMsg &= "- Please select Purchase Order !!<BR>"
        End If

        Try
            Dim dt1 As Date = CDate(toDate(trnbelidate.Text))
            If dt1 < CDate("01/01/1900") Then
                sMsg &= "- Invalid Invoice Date !!<BR>"
            End If
            If dt1 < CDate(toDate(podate.Text)) Then
                sMsg &= "- Invoice Date must be greater than PO Date !!<BR>"
            End If

        Catch ex As Exception
            sMsg &= "- Invalid Invoice Date !!<BR>"
        End Try

        If ToDouble(currencyRate.Text) <= 0 Then
            sMsg &= "- Currenct Rate must be greater than 0 !!<BR>"
        End If
        Dim datepromo As String = CDate(toDate(TglPromo.Text))
        'cek apa sudah ada material dari po 
        If Session("TblDtl1") Is Nothing Then
            sMsg &= "- Please create Detail Invoice data !!<BR>"
        Else
            Dim objTableCek As DataTable = Session("TblDtl1")
            If objTableCek.Rows.Count = 0 Then
                sMsg &= "- Please create Detail Invoice data !!<BR>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            posting.Text = "In Process" : Exit Sub
        End If

        ' checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(trnbelino.Text, "trnbelino", "ql_trnbelimst") Then
                generateNodraf()
            End If
        End If
        'cek currency

        Dim cRate As New ClassRate()
        cRate.SetRateValue(CInt(CurrencyOid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            ' insert table master
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try 'INSERT TO QL_trnbelimst
                oid.Text = GenerateID("QL_trnbelimst", CompnyCode)
                'Dim iDigit As Integer = 2
                'If digit.Checked Then
                '    iDigit = 6
                'End If
                sSql = "INSERT INTO QL_trnbelimst (cmpcode,trnbelimstoid,periodacctg,trnbelitype,trnbelino,trnbelidate,trnbelipono,trnbeliref,trnsuppoid,trnpaytype,trnbelidisc,trnbelidiscidr,trnbelidiscusd,trnbelidisctype,trnbelinote,trnbelistatus,trntaxpct,createuser,upduser,updtime,amtdiscdtl,amtdiscdtlidr,amtdiscdtlusd,amtdischdr,amtdischdridr,amtdischdrusd,amtbeli,amtbeliidr,amtbeliusd,trnamttax,trnamttaxidr,trnamttaxusd,amtbelinetto,amtbelinettoidr,amtbelinettousd,currencyoid,currencyrate,curroid,currate,amtbelinettoacctg,amtbelinettoacctgidr,amtbelinettoacctgusd,prefixoid,branch_code,typepo,promodate) VALUES ('" & _
                    CompnyCode & "'," & oid.Text & ",'" & GetDateToPeriodAcctg(GetServerTime()) & "','GROSIR','" & _
                    trnbelino.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & trnbelipono.Text & _
                    "','" & Tchar(trnbeliref.Text) & "'," & trnsuppoid.Text & "," & TRNPAYTYPE.SelectedItem.Value & _
                    "," & ToDouble(trnbelidisc.Text) & _
                    "," & ToDouble(trnbelidisc.Text) * cRate.GetRateMonthlyIDRValue & _
                    "," & ToDouble(trnbelidisc.Text) * cRate.GetRateMonthlyUSDValue & _
                    ",'" & trnbelidisctype.SelectedValue & _
                    "','" & Tchar(trnbelinote.Text) & "','" & posting.Text & "'," & _
                    ToDouble(trntaxpct.Text) & ",'" & Session("UserID") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP," & _
                    ToDouble(amtdiscdtl.Text) & _
                    "," & ToDouble(amtdiscdtl.Text) * cRate.GetRateMonthlyIDRValue & _
                    "," & ToDouble(amtdiscdtl.Text) * cRate.GetRateMonthlyUSDValue & _
                    "," & ToDouble(amtdischdr.Text) & "," & _
                    ToDouble(amtdischdr.Text) * cRate.GetRateMonthlyIDRValue & "," & _
                    ToDouble(amtdischdr.Text) * cRate.GetRateMonthlyUSDValue & "," & _
                    ToDouble(amtbeli.Text) & "," & _
                    ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & "," & _
                    ToDouble(amtbeli.Text) * cRate.GetRateMonthlyUSDValue & "," & _
                    ToDouble(amttax.Text) & "," & _
                    ToDouble(amttax.Text) * cRate.GetRateMonthlyIDRValue & "," & _
                    ToDouble(amttax.Text) * cRate.GetRateMonthlyUSDValue & "," & _
                    ToDouble(amtbelinetto1.Text) & "," & _
                    ToDouble(amtbelinetto1.Text) * cRate.GetRateMonthlyIDRValue & "," & _
                    ToDouble(amtbelinetto1.Text) * cRate.GetRateMonthlyUSDValue & "," & _
                    CurrencyOid.SelectedValue & "," & ToDouble(currencyRate.Text) & " ," & _
                    CurrencyOid.SelectedValue & "," & ToDouble(currencyRate.Text) & _
                    "," & ToDouble(amtbelinetto1.Text) & _
                    "," & ToDouble(amtbelinetto1.Text) * cRate.GetRateMonthlyIDRValue & _
                    "," & ToDouble(amtbelinetto1.Text) * cRate.GetRateMonthlyUSDValue & "," & prefixoid.Text & ",'" & CabangNya.SelectedValue & "','" & TypePOnya.SelectedValue & "','" & CDate(toDate(TglPromo.Text)) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & oid.Text & " where tablename ='QL_trnbelimst' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl1") Is Nothing Then
                    Dim objTable2 As DataTable
                    objTable2 = Session("TblDtl1")
                    Dim trnbelidtloid As Int64 = GenerateID("QL_trnbelidtl", CompnyCode)
                    For c1 As Int16 = 0 To objTable2.Rows.Count - 1

                        'setting satuan 3
                        Dim setupsatuan3 As Decimal
                        'update to last price 
                        Dim konv1_2 As Integer : Dim konv2_3 As Integer

                        sSql = "select konversi1_2 from ql_mstitem where itemoid =" & objTable2.Rows(c1)("mtroid") & ""
                        xCmd.CommandText = sSql : konv1_2 = xCmd.ExecuteScalar

                        sSql = "select konversi1_2 from ql_mstitem where itemoid =" & objTable2.Rows(c1)("mtroid") & ""
                        xCmd.CommandText = sSql : konv2_3 = xCmd.ExecuteScalar

                        If objTable2.Rows(c1).Item("unitseq") = 1 Then
                            setupsatuan3 = objTable2.Rows(c1).Item("trnbelidtlqty") * konv1_2 * konv2_3
                        ElseIf objTable2.Rows(c1).Item("unitseq") = 2 Then
                            setupsatuan3 = objTable2.Rows(c1).Item("trnbelidtlqty") * konv2_3
                        Else
                            setupsatuan3 = objTable2.Rows(c1).Item("trnbelidtlqty")
                        End If

                        sSql = " INSERT INTO QL_trnbelidtl (cmpcode,trnbelidtloid,trnbelimstoid,trnbelidtlseq,itemoid,trnbelidtlqty,trnbelidtlunitoid,trnbelidtlprice,trnbelidtlpriceidr,trnbelidtlpriceusd,trnbelidtlqtydisc,trnbelidtlqtydiscidr,trnbelidtlqtydiscusd,trnbelidtldisctype,trnbelidtlqtydisc2,trnbelidtlqtydisc2idr,trnbelidtlqtydisc2usd,trnbelidtldisctype2,trnbelidtlflag,trnbelidtlnote,trnbelidtlstatus,createuser,updtime,upduser,amtdisc,amtdiscidr,amtdiscusd,amtdisc2,amtdisc2idr,amtdisc2usd,amtbelinetto,amtbelinettoidr,amtbelinettousd,trnsjbelino,trnbelidtloid_po,itemloc,unitseq,trnbelidtlqty_unit3,amtEkspedisi,trnbelidtlres1,selisih,branch_code,trnsjbelioid,to_branch_code) VALUES ('" & CompnyCode & _
                            "'," & trnbelidtloid + c1 & "," & oid.Text & "," & c1 + 1 & "," & _
                            "" & objTable2.Rows(c1).Item("mtroid") & "," & _
                            "" & objTable2.Rows(c1).Item("trnbelidtlqty") & ",'" & _
                            objTable2.Rows(c1).Item("trnbelidtlunit") & "'," & _
                            objTable2.Rows(c1).Item("trnbelidtlprice") & "," & _
                            objTable2.Rows(c1).Item("trnbelidtlprice") * cRate.GetRateMonthlyIDRValue & "," & _
                            objTable2.Rows(c1).Item("trnbelidtlprice") * cRate.GetRateMonthlyUSDValue & "," & _
                            objTable2.Rows(c1).Item("trnbelidtldisc") & "," & _
                            objTable2.Rows(c1).Item("trnbelidtldisc") * cRate.GetRateMonthlyIDRValue & "," & _
                            objTable2.Rows(c1).Item("trnbelidtldisc") * cRate.GetRateMonthlyUSDValue & ",'" & _
                            objTable2.Rows(c1).Item("trnbelidtldisctype") & "'," & _
                            objTable2.Rows(c1).Item("trnbelidtldisc2") & "," & _
                            objTable2.Rows(c1).Item("trnbelidtldisc2") * cRate.GetRateMonthlyIDRValue & "," & _
                            objTable2.Rows(c1).Item("trnbelidtldisc2") * cRate.GetRateMonthlyUSDValue & ",'" & _
                            objTable2.Rows(c1).Item("trnbelidtldisctype2") & "','" & _
                            objTable2.Rows(c1).Item("trnbelidtlflag") & "'," & _
                            "'" & Tchar(objTable2.Rows(c1).Item("trnsjbelinote")) & "'," & _
                            "'" & Tchar(objTable2.Rows(c1).Item("trnbelidtlstatus")) & "','" & Session("UserID") & _
                            "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', " & _
                            objTable2.Rows(c1).Item("amtdisc") & "," & _
                            objTable2.Rows(c1).Item("amtdisc") * cRate.GetRateMonthlyIDRValue & "," & _
                            objTable2.Rows(c1).Item("amtdisc") * cRate.GetRateMonthlyUSDValue & "," & _
                            objTable2.Rows(c1).Item("amtdisc2") & "," & _
                            objTable2.Rows(c1).Item("amtdisc2") * cRate.GetRateMonthlyIDRValue & "," & _
                            objTable2.Rows(c1).Item("amtdisc2") * cRate.GetRateMonthlyUSDValue & "," & _
                            objTable2.Rows(c1).Item("amtbelinetto") & "," & _
                            objTable2.Rows(c1).Item("amtbelinetto") * cRate.GetRateMonthlyIDRValue & "," & _
                            objTable2.Rows(c1).Item("amtbelinetto") * cRate.GetRateMonthlyUSDValue & ",'" & _
                            objTable2.Rows(c1).Item("sjno") & "','" & objTable2.Rows(c1).Item("trnbelidtloid_po") & "','" & objTable2.Rows(c1).Item("itemloc") & "','" & objTable2.Rows(c1).Item("unitseq") & "'," & ToDouble(setupsatuan3) & "," & objTable2.Rows(c1).Item("biayaexpedisi") & ",'IN PROCESS'," & objTable2.Rows(c1).Item("amtdisc2") * cRate.GetRateMonthlyIDRValue & ",'" & CabangNya.SelectedValue & "'," & objTable2.Rows(c1).Item("sjoid") & ",'" & objTable2.Rows(c1).Item("to_branch_code") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next

                    sSql = "update QL_mstoid set lastoid=" & (objTable2.Rows.Count - 1 + trnbelidtloid) & " where tablename = 'QL_trnbelidtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                Dim objTable1 As DataTable = Session("TblDtl1")
                ' Update SJ Detail status to INVOICED
                For C4 As Integer = 0 To objTable1.Rows.Count - 1
                    sSql = "UPDATE ql_trnsjbelimst SET trnsjbelistatus='INVOICED' WHERE trnsjbelino='" & objTable1.Rows(C4).Item("sjno") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next

                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                showMessage(ex.ToString & " (SQL = " & sSql & ")", CompnyName & " - ERROR", 1)
                posting.Text = "In Process" : Exit Sub
            End Try
        Else
            '======================================================================
            '--------------------------- Kondisi UPDATE ---------------------------
            '======================================================================
            'update tabel master    
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "UPDATE QL_trnbelimst" & _
                " SET periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & _
                "',trnbelidate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) " & _
                ",trnbelipono='" & trnbelipono.Text & "',trnbelino='" & trnbelino.Text & _
                "',trnbeliref='" & Tchar(trnbeliref.Text) & "',trnsuppoid=" & trnsuppoid.Text & _
                ",trnpaytype=" & TRNPAYTYPE.SelectedItem.Value & _
                ",trnbelidisc=" & ToDouble(trnbelidisc.Text) & _
                ",trnbelidiscidr=" & ToDouble(trnbelidisc.Text) * cRate.GetRateMonthlyIDRValue & _
                ",trnbelidiscusd=" & ToDouble(trnbelidisc.Text) * cRate.GetRateMonthlyUSDValue & _
                ",trnbelidisctype='" & trnbelidisctype.SelectedValue & "'" & _
                ",trnbelinote='" & Tchar(trnbelinote.Text) & _
                "',trnbelistatus='" & posting.Text & "',trntaxpct=" & ToDouble(trntaxpct.Text) & _
                ",upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP" & _
                ",amtdiscdtl = " & ToDouble(amtdiscdtl.Text) & _
                ",amtdiscdtlidr = " & ToDouble(amtdiscdtl.Text) * cRate.GetRateMonthlyIDRValue & _
                ",amtdiscdtlusd = " & ToDouble(amtdiscdtl.Text) * cRate.GetRateMonthlyUSDValue & _
                ",amtdischdr=" & ToDouble(amtdischdr.Text) & _
                ",amtdischdridr=" & ToDouble(amtdischdr.Text) * cRate.GetRateMonthlyIDRValue & _
                ",amtdischdrusd=" & ToDouble(amtdischdr.Text) * cRate.GetRateMonthlyUSDValue & _
                ",amtbeli=" & ToDouble(amtbeli.Text) & _
                ",amtbeliidr=" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & _
                ",amtbeliusd=" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyUSDValue & _
                ",trnamttax=" & ToDouble(amttax.Text) & _
                ",trnamttaxidr=" & ToDouble(amttax.Text) * cRate.GetRateMonthlyIDRValue & _
                ",trnamttaxusd=" & ToDouble(amttax.Text) * cRate.GetRateMonthlyUSDValue & _
                ",amtbelinetto=" & ToDouble(amtbelinetto1.Text) & _
                ",amtbelinettoidr=" & ToDouble(amtbelinetto1.Text) * cRate.GetRateMonthlyIDRValue & _
                ",amtbelinettousd=" & ToDouble(amtbelinetto1.Text) * cRate.GetRateMonthlyUSDValue & _
                ",amtbelinettoacctg=" & ToDouble(amtbelinetto1.Text) & _
                ",amtbelinettoacctgidr=" & ToDouble(amtbelinetto1.Text) * cRate.GetRateMonthlyIDRValue & _
                ",amtbelinettoacctgusd=" & ToDouble(amtbelinetto1.Text) * cRate.GetRateMonthlyUSDValue & _
                ",currencyrate=" & ToDouble(currencyRate.Text) & " ,currencyoid=" & CurrencyOid.SelectedValue & _
                ",currate=" & ToDouble(currencyRate.Text) & " ,curroid=" & CurrencyOid.SelectedValue & ",typepo='" & TypePOnya.SelectedValue & "',promodate='" & CDate(toDate(TglPromo.Text)) & "' WHERE cmpcode='" & CompnyCode & "' and trnbelimstoid=" & oid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' Reverse SJDetail Status (Invoiced) data lama
                sSql = "UPDATE ql_trnsjbelimst SET trnsjbelistatus='Approved' WHERE trnsjbelino in (select trnsjbelino from QL_trnbelidtl WHERE cmpcode='" & CompnyCode & "' AND trnbelimstoid=" & oid.Text & ") "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' delete old detail
                sSql = "DELETE FROM ql_trnbelidtl WHERE trnbelimstoid=" & oid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl1") Is Nothing Then
                    Dim objTable2 As DataTable
                    objTable2 = Session("TblDtl1")
                    Dim trnbelidtloid As Int64 = GenerateID("QL_trnbelidtl", CompnyCode)
                    For c1 As Int16 = 0 To objTable2.Rows.Count - 1

                        'setting satuan 3
                        Dim setupsatuan3 As Decimal
                        'update to last price 
                        Dim konv1_2 As Integer : Dim konv2_3 As Integer
                        sSql = "select konversi1_2 from ql_mstitem where itemoid =" & objTable2.Rows(c1)("mtroid") & ""
                        xCmd.CommandText = sSql : konv1_2 = xCmd.ExecuteScalar

                        sSql = "select konversi1_2 from ql_mstitem where itemoid =" & objTable2.Rows(c1)("mtroid") & ""
                        xCmd.CommandText = sSql : konv2_3 = xCmd.ExecuteScalar

                        If objTable2.Rows(c1).Item("unitseq") = 1 Then
                            setupsatuan3 = objTable2.Rows(c1).Item("trnbelidtlqty") * konv1_2 * konv2_3
                        ElseIf objTable2.Rows(c1).Item("unitseq") = 2 Then
                            setupsatuan3 = objTable2.Rows(c1).Item("trnbelidtlqty") * konv2_3
                        Else
                            setupsatuan3 = objTable2.Rows(c1).Item("trnbelidtlqty")
                        End If

                        sSql = " INSERT INTO QL_trnbelidtl (cmpcode,trnbelidtloid,trnbelimstoid,trnbelidtlseq,itemoid,trnbelidtlqty,trnbelidtlunitoid,trnbelidtlprice,trnbelidtlpriceidr,trnbelidtlpriceusd,trnbelidtlqtydisc,trnbelidtlqtydiscidr,trnbelidtlqtydiscusd,trnbelidtldisctype,trnbelidtlqtydisc2,trnbelidtlqtydisc2idr,trnbelidtlqtydisc2usd,trnbelidtldisctype2,trnbelidtlflag,trnbelidtlnote,trnbelidtlstatus,createuser,updtime,upduser,amtdisc,amtdiscidr,amtdiscusd,amtdisc2,amtdisc2idr,amtdisc2usd,amtbelinetto,amtbelinettoidr,amtbelinettousd,trnsjbelino,trnbelidtloid_po,itemloc,unitseq,trnbelidtlqty_unit3,trnbelidtlres1,branch_code,trnsjbelioid, to_branch_code) VALUES ('" & CompnyCode & "'," & trnbelidtloid + c1 & "," & oid.Text & "," & c1 + 1 & "," & _
                         "" & objTable2.Rows(c1).Item("mtroid") & "," & _
                         "" & objTable2.Rows(c1).Item("trnbelidtlqty") & ",'" & _
                         objTable2.Rows(c1).Item("trnbelidtlunit") & "'," & _
                         objTable2.Rows(c1).Item("trnbelidtlprice") & "," & _
                         objTable2.Rows(c1).Item("trnbelidtlprice") * cRate.GetRateMonthlyIDRValue & "," & _
                         objTable2.Rows(c1).Item("trnbelidtlprice") * cRate.GetRateMonthlyUSDValue & "," & _
                         objTable2.Rows(c1).Item("trnbelidtldisc") & "," & _
                         objTable2.Rows(c1).Item("trnbelidtldisc") * cRate.GetRateMonthlyIDRValue & "," & _
                         objTable2.Rows(c1).Item("trnbelidtldisc") * cRate.GetRateMonthlyUSDValue & ",'" & _
                         objTable2.Rows(c1).Item("trnbelidtldisctype") & "'," & _
                         objTable2.Rows(c1).Item("trnbelidtldisc2") & "," & _
                         objTable2.Rows(c1).Item("trnbelidtldisc2") * cRate.GetRateMonthlyIDRValue & "," & _
                         objTable2.Rows(c1).Item("trnbelidtldisc2") * cRate.GetRateMonthlyUSDValue & ",'" & _
                         objTable2.Rows(c1).Item("trnbelidtldisctype2") & "','" & _
                         objTable2.Rows(c1).Item("trnbelidtlflag") & "'," & _
                         "'" & Tchar(objTable2.Rows(c1).Item("trnbelidtlnote")) & "'," & _
                         "'" & Tchar(objTable2.Rows(c1).Item("trnbelidtlstatus")) & "','" & Session("UserID") & _
                         "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', " & _
                         objTable2.Rows(c1).Item("amtdisc") & "," & _
                         objTable2.Rows(c1).Item("amtdisc") * cRate.GetRateMonthlyIDRValue & "," & _
                         objTable2.Rows(c1).Item("amtdisc") * cRate.GetRateMonthlyUSDValue & "," & _
                         objTable2.Rows(c1).Item("amtdisc2") & "," & _
                         objTable2.Rows(c1).Item("amtdisc2") * cRate.GetRateMonthlyIDRValue & "," & _
                         objTable2.Rows(c1).Item("amtdisc2") * cRate.GetRateMonthlyUSDValue & "," & _
                         objTable2.Rows(c1).Item("amtbelinetto") & "," & _
                         objTable2.Rows(c1).Item("amtbelinetto") * cRate.GetRateMonthlyIDRValue & "," & _
                         objTable2.Rows(c1).Item("amtbelinetto") * cRate.GetRateMonthlyUSDValue & ",'" & _
                         objTable2.Rows(c1).Item("sjno") & "','" & objTable2.Rows(c1).Item("trnbelidtloid_po") & "','" & objTable2.Rows(c1).Item("itemloc") & "','" & objTable2.Rows(c1).Item("unitseq") & "'," & ToDouble(setupsatuan3) & ",'','" & CabangNya.SelectedValue & "'," & objTable2.Rows(c1).Item("sjoid") & ",'" & objTable2.Rows(c1).Item("to_branch_code") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        If posting.Text = "POST" Then
                            If (objTable2.Rows(c1).Item("unitseq")) = 1 Then
                                sSql = "update ql_mstitem set lastPricebuyUnit1= (" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ") , lastPriceBuyUnit2=(" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ")/" & konv1_2 & " , lastPriceBuyUnit3=((" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ")/" & konv1_2 & ")/" & konv2_3 & " Where itemoid=" & objTable2.Rows(c1).Item("mtroid")
                            ElseIf (objTable2.Rows(c1).Item("unitseq") = 2) Then
                                sSql = "update ql_mstitem set lastPricebuyUnit2=(" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ") , lastPriceBuyUnit1=(" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ")*" & konv1_2 & " , lastPriceBuyUnit3=(" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ")/" & konv2_3 & " Where itemoid=" & objTable2.Rows(c1).Item("mtroid")
                            Else
                                sSql = "update ql_mstitem set lastPricebuyUnit3=(" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ") , lastPriceBuyUnit1=(" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ")*" & konv2_3 & "*" & konv1_2 & ", lastPriceBuyUnit2=(" & objTable2.Rows(c1).Item("amtbelinetto") & " / " & ToDouble(objTable2.Rows(c1).Item("trnbelidtlqty")) & ")*" & konv2_3 & " Where itemoid=" & objTable2.Rows(c1).Item("mtroid")
                            End If
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    Next
                    sSql = "update QL_mstoid set lastoid=" & (objTable2.Rows.Count - 1 + trnbelidtloid) & " Where tablename = 'QL_trnbelidtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                Dim objTable1 As DataTable = Session("TblDtl1")
                ' Update SJ Detail status to INVOICED
                For C4 As Integer = 0 To objTable1.Rows.Count - 1
                    sSql = "UPDATE ql_trnsjbelimst SET trnsjbelistatus='INVOICED' WHERE trnsjbelino='" & objTable1.Rows(C4).Item("sjno") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next

                '=====POSTING=====
                If posting.Text = "POST" Then

                    Dim TotaltoRupiah As Double
                    If CurrencyOid.SelectedValue <> 1 Then
                        TotaltoRupiah = ToDouble(amtbelinetto1.Text) * ToDouble(currencyRate.Text)
                    Else
                        TotaltoRupiah = ToDouble(amtbelinetto1.Text)
                    End If
                    Dim cekday As String = GetStrData("select gencode from QL_mstgen where gengroup = 'paytype' and genoid = " & TRNPAYTYPE.SelectedValue)
                    Dim days As Double = CInt(cekday.Trim.Replace("CSH", "").Replace("CRD", ""))
                    'insert to conap 
                    Dim dPayDueDate As Date = CDate(toDate(trnbelidate.Text)).AddDays(days)
                    Dim iKey As Int64 = GenerateID("QL_conap", CompnyCode)
                    Dim sVarAP As String = ""
                    'If DefineLocalOrImport(CurrencyOid.SelectedValue) = 0 Then
                    sVarAP = GetVarInterface("VAR_AP", CabangNya.SelectedValue)
                    'Else
                    '    sVarAP = GetInterfaceValue("VAR_PI_AP_IMPORT")
                    'End If
                    Dim iAPOid As Integer = GetAccountOid(sVarAP, True)

                    ' ============== AP
                    sSql = "INSERT INTO QL_conap (cmpcode,conapoid,reftype,refoid,payrefoid,suppoid,acctgoid,trnapstatus,trnaptype,trnapdate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,trnapnote,trnapres1,upduser,updtime,branch_code) VALUES" & _
                    "('" & CompnyCode & "'," & iKey & ",'QL_trnbelimst'," & _
                    oid.Text & ",0," & trnsuppoid.Text & ",'" & iAPOid & _
                    "','POST','HUTANG',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg(CDate(toDate(trnbelidate.Text))) & _
                    "',0,'1/1/1900','',0,'" & dPayDueDate & _
                    "'," & ToDouble(totalinvoice.Text) & _
                    "," & ToDouble(totalinvoice.Text) * cRate.GetRateMonthlyIDRValue & _
                    "," & ToDouble(totalinvoice.Text) * cRate.GetRateMonthlyUSDValue & _
                    ",0,0,0,'PURCHASE INVOICE - (" & Tchar(suppliername.Text) & "|NO=" & trnbelino.Text & ")','','" & Session("UserID") & "', CURRENT_TIMESTAMP,'" & CabangNya.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update  QL_mstoid set lastoid=" & iKey & " Where tablename ='QL_conap' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    ' ===================== POSTING GL
                    Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
                    Dim dvJurnal As DataView = dtJurnal.DefaultView
                    Dim vIDMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
                    Dim vIDDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
                    Dim vSeqDtl As Integer = 1

                    dvJurnal.RowFilter = "seq=1" ' JURNAL PURCHASE INVOICE
                    If dvJurnal.Count > 0 Then
                        ' ============== QL_trnglmst ==============
                        sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime,branch_code,type) " & _
                        " VALUES ('" & CompnyCode & "'," & vIDMst & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg(CDate(toDate(trnbelidate.Text))) & "','PURCHASE INVOICE (" & Tchar(suppliername.Text) & "|NO=" & trnbelino.Text & ")','POST','" & Now().Date & "'," & "'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "','PI')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        For C2 As Integer = 0 To dvJurnal.Count - 1
                            Dim dAmt As Double = 0 : Dim sDBCR As String = ""
                            If ToDouble(dvJurnal(C2)("debet").ToString) > 0 Then
                                dAmt = ToDouble(dvJurnal(C2)("debet").ToString) : sDBCR = "D"
                            Else
                                dAmt = ToDouble(dvJurnal(C2)("credit").ToString) : sDBCR = "C"
                            End If
                            ' ============== QL_trngldtl ==============
                            sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,upduser,updtime,branch_code) " & _
                            " VALUES ('" & CompnyCode & "'," & vIDDtl & "," & vSeqDtl & "," & vIDMst & "," & dvJurnal(C2)("id").ToString & ",'" & sDBCR & "'," & dAmt & "," & dAmt * cRate.GetRateMonthlyIDRValue & "," & dAmt * cRate.GetRateMonthlyUSDValue & ",'" & trnbelino.Text & "','PURCHASE INVOICE (" & Tchar(suppliername.Text) & "|NO=" & trnbelino.Text & ")','','" & Session("oid") & "','" & posting.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            vSeqDtl += 1 : vIDDtl += 1
                        Next
                    End If
                    dvJurnal.RowFilter = "" : vIDMst += 1 : vSeqDtl = 1

                    'update lastoid ql_trnglmst
                    sSql = "update QL_mstoid set lastoid=" & vIDMst & " where tablename ='QL_trnglmst' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'update lastoid ql_trngldtl
                    sSql = "update QL_mstoid set lastoid=" & vIDDtl - 1 & " where tablename ='QL_trngldtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                showMessage(ex.ToString & " (SQL = " & sSql & ")", CompnyName & " - ERROR", 1)
                posting.Text = "In Process" : Exit Sub
            End Try
        End If
        Session("oid") = Nothing : Session("TblDtl1") = Nothing
        Session("TblDtl2") = Nothing : Session("TblDtl3") = Nothing
        Response.Redirect("~\Transaction\trnNotaBeli.aspx?awal=true")
    End Sub

    Protected Sub FilterPeriod1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterPeriod1.TextChanged
        chkPeriod.Checked = True
    End Sub

    Protected Sub DDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLStatus.SelectedIndexChanged
        chkStatus.Checked = True
    End Sub

    Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPO.SelectedIndexChanged 
        trnbelimstoid.Text = gvListPO.SelectedDataKey.Item("trnbelimstoid").ToString
        trnbelipono.Text = gvListPO.SelectedDataKey.Item("trnbelipono").ToString
        podate.Text = Format(CDate(gvListPO.SelectedDataKey.Item("trnbelipodate")), "dd/MM/yyy")
        trnsuppoid.Text = gvListPO.SelectedDataKey.Item("trnsuppoid").ToString
        suppliername.Text = gvListPO.SelectedDataKey.Item("suppname").ToString
        CurrencyOid.SelectedValue = gvListPO.SelectedDataKey.Item("curroid").ToString
        currencyRate.Text = gvListPO.SelectedDataKey.Item("currate")
        currencyoid_SelectedIndexChanged(Nothing, Nothing)
        trnbelidisctype.SelectedValue = gvListPO.SelectedDataKey.Item("trnbelidisctype").ToString
        trnbelidisc.Text = ToMaskEdit(ToDouble(gvListPO.SelectedDataKey.Item("trnbelidisc").ToString), 3)
        'digit.SelectedValue = gvListPO.SelectedDataKey.Item("digit").ToString()
        TRNPAYTYPE.SelectedValue = gvListPO.SelectedDataKey.Item("trnpaytype").ToString()
        Tax.SelectedValue = gvListPO.SelectedDataKey(1).ToString.Substring(2, 1)
        prefixoid.Text = gvListPO.SelectedDataKey("prefixoid")
        temptotalamount.Text = GetStrData("select (amtbeli-amtdiscdtl) temptotal from QL_pomst where trnbelipono = '" & gvListPO.SelectedDataKey(1).ToString & "'")
        tempdisc.Text = GetStrData("select amtdischdr from QL_pomst where trnbelipono = '" & gvListPO.SelectedDataKey(1).ToString & "' ")
        mCabang.Text = gvListPO.SelectedDataKey.Item("branch_code")
        If gvListPO.SelectedDataKey(1).ToString.Substring(2, 1) = "0" Then
            trntaxpct.Text = "10.00"
        Else
            trntaxpct.Text = "0.00"
        End If
        generateNodraf()
        Session("TblDtl1") = Nothing : tbldtl2.DataSource = Nothing : tbldtl2.DataBind()
        gvListPO.Visible = False : ItemGv.Visible = False
    End Sub

    Protected Sub lkbPostPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPostPreview.Click
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, False)
        generateNo() : posting.Text = "POST" : btnSaveMstr_Click(Nothing, Nothing)
    End Sub

    Protected Sub gvListSJ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSJ.SelectedIndexChanged
        Try
            If Session("TblDtl1") Is Nothing Then
                Dim dtlTable1 As DataTable = setTableDetail()
                Session("TblDtl1") = dtlTable1
            End If
            Dim objtable As DataTable = Session("TblDtl1")
            Dim dv As DataView = objtable.DefaultView
            dv.RowFilter = "sjoid=" & CInt(gvListSJ.SelectedDataKey("trnsjbelioid").ToString)
            trnbelinote.Text = gvListSJ.SelectedDataKey("trnsjbelinote").ToString
            If dv.Count > 0 Then
                showMessage("This Data has been added before, please check!", CompnyName & " - WARNING", 2)
                dv.RowFilter = ""
                gvListSJ.Visible = False
                DisposeGrid(gvListSJ)

                gvListSJDetail.Visible = False
                DisposeGrid(gvListSJDetail)
                Exit Sub
            End If
            dv.RowFilter = ""

            Dim objTabelSJ As DataTable = cKon.ambiltabel(bindSJDetail(gvListSJ.SelectedDataKey(0).ToString), "ql_trnsjbelidtl")
            Dim objTabelDtl As DataTable : objTabelDtl = Session("TblDtl1")
            Dim C2 As Integer = objTabelDtl.Rows.Count + 1
            For C1 As Integer = 0 To objTabelSJ.Rows.Count - 1
                Dim nuRow As DataRow = objTabelDtl.NewRow
                nuRow("trnbelidtloid") = 0
                nuRow("trnbelimstoid") = 0
                nuRow("trnbelidtlseq") = C2 + C1
                nuRow("sjrefoid") = objTabelSJ.Rows(C1).Item("trnsjbelioid")
                nuRow("mtroid") = objTabelSJ.Rows(C1).Item("refoid")
                nuRow("trnbelidtlqty") = objTabelSJ.Rows(C1).Item("qty")
                nuRow("trnbelidtlunit") = objTabelSJ.Rows(C1).Item("unitoid")
                nuRow("trnbelidtlprice") = objTabelSJ.Rows(C1).Item("trnbelidtlprice")
                nuRow("trnbelidtldisc") = objTabelSJ.Rows(C1).Item("trnbelidtldisc")
                nuRow("trnbelidtldisctype") = objTabelSJ.Rows(C1).Item("trnbelidtldisctype")
                nuRow("trnbelidtldisc2") = objTabelSJ.Rows(C1).Item("trnbelidtldisc2")
                nuRow("trnbelidtldisctype2") = objTabelSJ.Rows(C1).Item("trnbelidtldisctype2")
                nuRow("trnbelidtlflag") = ""
                nuRow("trnsjbelinote") = objTabelSJ.Rows(C1).Item("trnsjbelinote").ToString
                nuRow("trnbelidtlnote") = objTabelSJ.Rows(C1).Item("trnsjbelinote").ToString
                nuRow("trnbelidtlstatus") = ""
                nuRow("amtdisc") = objTabelSJ.Rows(C1).Item("amtdisc")
                nuRow("amtdisc2") = objTabelSJ.Rows(C1).Item("amtdisc2")
                nuRow("totamtdisc") = objTabelSJ.Rows(C1).Item("totamtdisc")
                nuRow("amtbelinetto") = objTabelSJ.Rows(C1).Item("amtbelinetto")
                nuRow("biayaexpedisi") = objTabelSJ.Rows(C1).Item("biayaexpedisi")
                nuRow("material") = objTabelSJ.Rows(C1).Item("matlongdesc")
                nuRow("unit") = objTabelSJ.Rows(C1).Item("gendesc")
                nuRow("sjoid") = gvListSJ.SelectedDataKey(0).ToString
                nuRow("sjno") = objTabelSJ.Rows(C1).Item("trnsjbelino")
                nuRow("unitseq") = objTabelSJ.Rows(C1).Item("unitseq")
                nuRow("itemloc") = objTabelSJ.Rows(C1).Item("mtrlocoid")
                nuRow("trnbelidtloid_po") = objTabelSJ.Rows(C1).Item("trnbelidtloid")
                nuRow("to_branch_code") = objTabelSJ.Rows(C1).Item("to_branch_code")

                objTabelDtl.Rows.Add(nuRow)
            Next

            tbldtl2.DataSource = objTabelDtl : tbldtl2.DataBind()
            Session("TblDtl1") = objTabelDtl
            SumPriceInInvoiceDetail(Session("TblDtl1")) : ReAmount()
            Dim x As Integer = gvListSJ.SelectedRow.RowIndex
            gvListSJ.Visible = False : DisposeGrid(gvListSJ)
            gvListSJDetail.Visible = False : DisposeGrid(gvListSJDetail)
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - WARNING", 2)
        End Try
    End Sub

    Protected Sub tbldtl2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbldtl2.SelectedIndexChanged
        Dim dt As DataTable : dt = Session("TblDtl1")
        iRow.Text = tbldtl2.SelectedDataKey(0).ToString
        Dim objRow() As DataRow = dt.Select("trnbelidtlseq='" & iRow.Text & "'", Nothing, DataViewRowState.CurrentRows)
        noteSJ.Text = objRow(0).Item("sjno").ToString
        Material.Text = objRow(0).Item("material").ToString
        unit.Text = objRow(0).Item("unit").ToString
        trnbelidtlqty.Text = ToMaskEdit(ToDouble(objRow(0).Item("trnbelidtlqty")), 4)
        trnbelidtlprice.Text = ToMaskEdit(ToDouble(objRow(0).Item("trnbelidtlprice")), 4)
        trnbelidtldisctype.SelectedValue = objRow(0).Item("trnbelidtldisctype")
        trnbelidtldisc.Text = ToMaskEdit(ToDouble(objRow(0).Item("trnbelidtldisc")), 4)
        trnbelidtldisctype2.SelectedValue = objRow(0).Item("trnbelidtldisctype2")
        trnbelidtldisc2.Text = ToMaskEdit(ToDouble(objRow(0).Item("trnbelidtldisc2")), 4)
        amtbelinetto2.Text = ToMaskEdit(ToDouble(objRow(0).Item("amtbelinetto")), 4)
        trnbelidtlnote.Text = objRow(0).Item("trnsjbelinote").ToString
        to_branch_code.Text = objRow(0).Item("to_branch_code").ToString
        sjoid.Text = objRow(0).Item("sjoid").ToString
        ReAmountDetail()
        ' ReAmountDetail2()

        If trnbelidtldisctype.SelectedValue = "AMT" Then 'amount
            lbl2.Text = "Discount (Rp)"
        Else
            lbl2.Text = "Discount (%)"
        End If
        If trnbelidtldisctype2.SelectedValue = "VCR" Then 'amount
            lbl3.Text = "Discount (Rp)"
        Else
            lbl3.Text = "Discount (%)"
        End If
        btnSearchSJ.Visible = False : btnClearSj.Visible = False
        btnAddToList.Visible = False : btnCancelDtl.Visible = True
        tbldtl2.Columns(10).Visible = False
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If currencyRate.Text = "" Then
            showMessage("Currency Rate must be greater than 0!!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        If ToDouble(currencyRate.Text) <= 0 Then
            showMessage("Currency Rate must be greater than 0!!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        Dim dt As DataTable : dt = Session("TblDtl1")
        Dim objRow() As DataRow = dt.Select("trnbelidtlseq='" & iRow.Text & "'")
        Dim editRow As DataRow = objRow(0)
        editRow.BeginEdit()
        editRow("trnbelidtlprice") = ToDouble(trnbelidtlprice.Text)
        editRow("trnbelidtldisc") = ToDouble(trnbelidtldisc.Text)
        editRow("trnbelidtldisctype") = trnbelidtldisctype.SelectedValue
        editRow("trnbelidtldisc2") = ToDouble(trnbelidtldisc2.Text)
        editRow("trnbelidtldisctype2") = trnbelidtldisctype2.SelectedValue
        editRow("trnbelidtlnote") = trnbelidtlnote.Text
        editRow("totamtdisc") = ToDouble(totdiscdtl.Text)
        editRow("amtdisc") = ToDouble(amtdisc.Text)
        editRow("amtdisc2") = ToDouble(trnbelidtldisc2.Text)
        editRow("amtbelinetto") = ToDouble(amtbelinetto2.Text)
        editRow("trnbelidtlnote") = trnbelidtlnote.Text
        editRow.EndEdit()

        Session("TblDtl1") = dt : tbldtl2.DataSource = dt : tbldtl2.DataBind()
        SumPriceInInvoiceDetail(Session("TblDtl1")) : ReAmount()
        ClearDetail()
        btnSearchSJ.Visible = True : btnClearSj.Visible = True
        btnAddToList.Visible = False : btnCancelDtl.Visible = False
    End Sub

    Protected Sub dCabangNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dCabangNya.SelectedIndexChanged
        POUSer()
    End Sub
End Class
