'Prgmr:Shutup_M 02-28-16
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnSIRecord
    Inherits System.Web.UI.Page
    '(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[/](19|20)\d\d 'DMY
    '(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d 'MDY
#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim dv As DataView
    Private cKon As New Koneksi
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Dim cKoneksi As New Koneksi
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String = ""
    Dim crate As ClassRate
#End Region

#Region "Procedures"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKoneksi.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function InitAcctgBasedOnMasterData(ByVal sMasterTable As String, ByVal sMasterColumn As String, ByVal sAcctgColumn As String, _
        ByVal iMasterOid As Integer) As Integer
        sSql = "SELECT ISNULL(" & sAcctgColumn & ",0) FROM " & sMasterTable & " WHERE cmpcode='" & CompnyCode & "' AND " & sMasterColumn & "=" & iMasterOid
        Return cKoneksi.ambilscalar(sSql)
    End Function

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub


    Public Sub BindData(ByVal sWhere As String)
        Try

            sSql = "select orderno, trnsjjualno, trnjualno, trnjualdate, trnjualmstoid, trncustname from QL_trnjualmst where trnjualref not in ('','-') and branch_code = '" & Session("branch_id") & "' "

            Dim sOther As String = ""
            If sWhere <> "" Then
                If chkPeriod.Checked Then
                    sOther &= " AND trnjualdate BETWEEN '" & Format(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy") & "' AND '" & Format(CDate(toDate(FilterPeriod2.Text)), "MM/dd/yyyy") & "' "
                Else
                    sOther &= " AND trnjualdate BETWEEN '" & Format(Today, "MM/dd/yyyy") & "' AND '" & Format(Today, "MM/dd/yyyy") & "' "
                End If
            End If
            'If checkPagePermission("Transaction/trnSIRecord.aspx", Session("SpecialAccess")) = False Then
            '    'If Session("branch_id") <> "01" Then
            '    sSql &= " AND upduser='" & Session("UserID") & "'"
            '    'End If
            'End If

            'If kacabCek("Transaction/trnSIRecord.aspx", Session("UserID")) = True Then
            '    If Session("branch_id") <> "01" Then
            '        sSql &= " AND branch_code='" & Session("branch_id") & "'"
            '    End If
            'End If
            sOther &= " " & sWhere & " ORDER BY trnjualdate desc, trnjualno desc "
            sSql &= sOther
            sqlTempSearch = sSql ' Untuk simpan history search

            ClassFunction.FillGV(tbldata, sSql, "ql_trnbelimst")
        Catch ex As Exception
            showMessage(ex.ToString, "", 1)
            Exit Sub
        End Try

    End Sub

    Private Sub FillTextBox(ByVal iOid As Int64)
        sSql = "select trnjualmstoid, trnjualno, trnjualref,trncustoid,trnamtjualnetto,accumpaymentidr, trncustname, upduser, updtime from ql_trnjualmst Where cmpcode ='" & CompnyCode & "' AND trnjualmstoid ='" & iOid & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                oid.Text = Trim(xreader.Item("trnjualmstoid").ToString)
                SIno.Text = Trim(xreader.Item("trnjualref").ToString)
                trnjualno.Text = Trim(xreader.Item("trnjualno").ToString)
                Upduser.Text = Trim(xreader.Item("upduser"))
                Updtime.Text = CDate(xreader.Item("updtime"))
                'trnbelimstoid.Text = Trim(xreader.Item("id").ToString)
                prefixoid.Text = Trim(xreader.Item("trnjualmstoid"))
                custoid.Text = Trim(xreader.Item("trncustoid"))
                custname.Text = Trim(xreader.Item("trncustname"))
                If Trim(xreader.Item("trnamtjualnetto")) = Trim(xreader.Item("accumpaymentidr")) Then
                    btnSaveMstr.Visible = False : SIno.CssClass = "inpTextDisabled"
                End If
            End While
        End If
        xreader.Close()
        conn.Close()

        'sSql = "SELECT d.trnbelidtloid,d.trnbelimstoid,d.trnbelidtlseq,d.itemoid mtroid, d.trnbelidtlqty,d.trnbelidtlunitoid trnbelidtlunit,d.trnbelidtlprice, d.trnbelidtlqtydisc trnbelidtldisc,d.trnbelidtldisctype,d.trnbelidtlqtydisc2 trnbelidtldisc2,d.trnbelidtldisctype2,d.trnbelidtlflag,d.trnbelidtlnote, d.trnbelidtlstatus,d.amtdisc,d.amtdisc2,d.amtbelinetto, (select itemdesc FROM QL_mstitem where itemoid = d.itemoid ) as material, g.gendesc AS unit,sj.trnsjbelioid sjoid,sj.trnsjbelioid sjrefoid,sj.trnsjbelino sjno,d.unitseq ,d.itemloc ,d.trnbelidtloid_po ,amtdisc totamtdisc,d.amtEkspedisi biayaexpedisi,sj.trnsjbelinote FROM QL_trnbelidtl d INNER JOIN ql_trnsjbelimst sj ON sj.cmpcode=d.cmpcode AND sj.trnsjbelino=d.trnsjbelino INNER JOIN ql_mstgen g on g.cmpcode=d.cmpcode AND g.genoid=d.trnbelidtlunitoid WHERE d.trnbelimstoid=" & iOid & " order by d.trnbelidtlseq"

        'Dim dtl As DataTable = cKon.ambiltabel(sSql, "ql_trnbelidtl")
        'Session("TblDtl1") = dtl
        'Dim userlogin As String = Session("UserID")
        'Session("no") = ""
    End Sub

    Sub ClearItem()
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
        Session("TblDtl1") = Nothing : Session("tbldtl2") = Nothing
        Session("tbldtl3") = Nothing
    End Sub

    Sub FillPODetailData()
        'data detail PO
        sSql = "SELECT d.cmpcode, d.trnbelidtloid, d.trnbelimstoid, d.trnbelidtlseq, d.mtrloc,d.mtroid, d.trnbelidtlqty, d.trnbelidtlunit, d.trnbelidtlprice, d.trnbelidtldisc, (case  d.trnbelidtldisctype when 1 then 'AMOUNT' else 'PERCENTAGE' end) AS trnbelidtldisctypeDesc,  d.trnbelidtlnote, d.trnbelidtlstatus, d.updtime, d.upduser,d.amtdisc, d.amtbelinetto  m.mtrdesc material, d.trnbelidtldisctype, (d.trnbelidtlqty*d.trnbelidtlprice) as amount,d.currencyoid, d.currencyrate, isnull((select sum(dod.trnsjbelidtlqty) from QL_trnsjbelimst do inner join QL_trnsjbelidtl dod on do.trnbelipono=p.trnbelipono and dod.trnsjbelimstoid=do.trnsjbelimstoid and dod.mtroid=d.mtroid and do.trnsjbelistatus='POST'),0)-d.trnbelidtlqty as 'plusMinus' FROM QL_podtl d inner join QL_mstgen g on g.genoid=d.mtrloc inner join QL_mstmtr m on m.mtroid=d.mtroid inner join QL_mstgen g2 on g2.genoid=d.mtroid inner join QL_pomst p on p.trnbelimstoid=d.trnbelimstoid where p.trnbelipono='" & trnjualno.Text & "' order by d.mtroid "
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        Session("TblDtl3") = objDs.Tables("data")
    End Sub

    Public Sub BindDataPO(ByVal view As String, ByVal sPlus As String)
        Dim daterange As String = "" : If view = "view" Then
        End If
        Dim filterPR As String = ""
        dim sWhere as String = ""
        If custname.Text <> "" Then
            sWhere = "and trncustoid  = " & custoid.Text & ""
        End If
        sSql = " select trnjualmstoid, orderno, trnsjjualno, trnjualno, trnjualdate,trncustname, trnamtjualnetto from QL_trnjualmst where trnjualno Like '%" & Tchar(trnjualno.Text) & "%' AND cmpcode='" & CompnyCode & "' and branch_code = '" & Session("branch_id") & "' and trnamtjualnetto <> accumpaymentidr " & sWhere & ""
        sSql &= "Order By trnjualmstoid DESC"
        FillGV(gvListPO, sSql, "ql_trnjualmst")
    End Sub

    Public Sub BindDataCust(ByVal view As String, ByVal sPlus As String)
        Dim daterange As String = "" : If view = "view" Then
        End If
        Dim filterPR As String = ""
        sSql = " select distinct custoid, custcode, custname, custaddr from QL_mstcust where custoid in (select trncustoid from QL_trnjualmst where branch_code = '" & Session("branch_id") & "') and custname like '%" & custname.Text & "%' /*and custcode like '%" & custname.Text & "%'*/"
        sSql &= "Order By custname asc"
        FillGV(gvCustomer, sSql, "ql_mstcust")
    End Sub


    Sub InsertToInvoiceDetail(ByVal sCmpCode As String, ByVal iMtrLoc As Int32, ByVal iMtroid As Int32, ByVal iQty As Int16, ByVal sUnit As String, ByVal sNote As String, ByVal sMaterial As String, ByVal sLocation As String)
        Dim dv2 As DataView
        If Session("tbldtl2") Is Nothing Then 'table nota beli detail
            Dim conn2 As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP"))
            sSql = "SELECT d.cmpcode, d.trnbelidtloid, d.trnbelimstoid,   d.trnbelidtlseq, d.sjrefoid, d.mtroid, d.trnbelidtlqty,   d.trnbelidtlunit, d.trnbelidtlprice, d.currencyoid,  d.currencyrate, d.pricerupiah, d.trnbelidtldisc,  d.trnbelidtldisctype, d.trnbelidtlflag, d.trnbelidtlnote,   d.trnbelidtlstatus, d.updtime, d.upduser, d.amtdisc,  d.amtbelinetto, m.mtrdesc material, (d.trnbelidtlqty*d.trnbelidtlprice) as amount, g.gendesc as location , '' as trnbelidtldisctypeDesc    FROM QL_trnbelidtl d inner join QL_mstmtr m   on m.mtroid=d.mtroid inner join QL_mstgen g on g.genoid=d.mtrloc  where  d.trnbelimstoid=-111"
            Dim mySqlDA As New SqlDataAdapter(sSql, conn2)
            Dim objDs As New DataSet
            mySqlDA.Fill(objDs, "data2")
            Session("TblDtl2") = objDs.Tables("data2")
        End If

        'cek nota detail
        Dim oTblDtl2 As DataTable = Session("TblDtl2")
        dv2 = oTblDtl2.DefaultView
        dv2.RowFilter = "mtrloc=" & iMtrLoc & "  and  mtroid=" & iMtroid
        If dv2.Count = 0 Then 'new nota beli detail, cause new material and location
            AddToInvoiceDetail(oTblDtl2, sCmpCode, iMtrLoc, iMtroid, iQty, sUnit, sNote, sMaterial, sLocation)
        Else 'update nota beli detail, just update qty doank
            Dim dr2 As DataRow = oTblDtl2.Rows(dv2.Item(0).Item(3) - 1) ' take from seq num-1 to get rownum
            dr2.BeginEdit()
            dr2(6) = dr2(6) + iQty
            dr2(22) = dr2(6) * dr2(8) 'amount

            dr2(19) = IIf(dr2(13) = 1, dr2(12), CDec(CDec(dr2(12)) / 100) * dr2(22)) 'amtdisc( jika type=1, amount, type2=persen, maka dikalikan persen nya dulu
            dr2(20) = dr2(22) - dr2(19) 'amtbelinetto
            dr2.EndEdit()
        End If
        dv2.RowFilter = ""
        Session("tbldtl2") = oTblDtl2
    End Sub

    Sub AddToInvoiceDetail(ByRef otblDtl2 As DataTable, ByVal sCmpCode As String, ByVal iMtrLoc As Int32, ByVal iMtroid As Int32, ByVal iQty As Int16, ByVal sUnit As String, ByVal sNote As String, ByVal sMaterial As String, ByVal sLocation As String)
        Dim otblDtl3 As DataTable = Session("tbldtl3") 'tabel detail po
        Dim dv3 As DataView = otblDtl3.DefaultView
        dv3.RowFilter = "mtrloc=" & iMtrLoc & "  and  mtroid=" & iMtroid

        Dim dr2 As DataRow = otblDtl2.NewRow 'new data for invoice detail
        If dv3.Count = 0 Then 'when each data in po not same
            dr2(0) = sCmpCode 'cmpcode
            dr2(1) = 0 'trnbelidtloid
            dr2(2) = 0 'trnbelimstoid
            dr2(3) = otblDtl2.Rows.Count + 1 'trnbelidtlseq
            dr2(4) = iMtrLoc 'mtrloc
            dr2(5) = iMtroid 'mtroid
            dr2(6) = iQty 'trnbelidtlqty
            dr2(7) = sUnit 'trnbelidtlunit
            dr2(8) = 0 'trnbelidtlprice
            dr2(9) = 0 'currencyoid
            dr2(10) = 0 'currencyrate
            dr2(11) = 0 'pricerupiah
            dr2(12) = 0 'trnbelidtldisc
            dr2(13) = 1 'trnbelidtldisctype
            dr2(14) = "-" 'trnbelidtlflag
            dr2(15) = "-" 'trnbelidtlnote
            dr2(16) = "-" 'trnbelidtlstatus
            dr2(17) = Now 'updtime
            dr2(18) = Session("userid") 'upduser
            dr2(19) = 0 'amtdisc
            dr2(20) = 0 'amtbelinetto
            dr2(21) = sMaterial ' material
            dr2(22) = 0 'amount
            dr2(23) = sLocation 'location 
            dr2(24) = "Amount" 'trnbelidtldisctypeDesc
        Else 'when one data in detail PO same, then the data especially price etc, send to invoice detail
            dr2(0) = sCmpCode 'cmpcode
            dr2(1) = 0 'trnbelidtloid
            dr2(2) = 0 'trnbelimstoid
            dr2(3) = otblDtl2.Rows.Count + 1 'trnbelidtlseq
            dr2(4) = iMtrLoc 'mtrloc
            dr2(5) = iMtroid 'mtroid
            dr2(6) = iQty 'trnbelidtlqty
            dr2(7) = sUnit 'trnbelidtlunit
            dr2(8) = dv3.Item(0).Item(8) 'trnbelidtlprice
            dr2(9) = dv3.Item(0).Item(20) 'currencyoid
            dr2(10) = dv3.Item(0).Item(21) 'currencyrate
            dr2(11) = 0 'pricerupiah
            dr2(12) = dv3.Item(0).Item(9) 'trnbelidtldisc
            dr2(13) = dv3.Item(0).Item(18) 'trnbelidtldisctype(1=amount; 2=percentage)
            dr2(14) = "-" 'trnbelidtlflag
            dr2(15) = dv3.Item(0).Item(11) 'trnbelidtlnote
            dr2(16) = "-" 'trnbelidtlstatus
            dr2(17) = Now 'updtime
            dr2(18) = Session("userid") 'upduser
            dr2(22) = iQty * dv3.Item(0).Item(8) 'amount

            dr2(19) = IIf(dv3.Item(0).Item(18) = 1, dv3.Item(0).Item(9), CDec(CDec(dv3.Item(0).Item(9)) / 100) * dr2(22))
            'amtdisc( jika type=1, amount, type2=persen, maka dikalikan persen nya dulu
            dr2(20) = dr2(22) - dr2(19) 'amtbelinetto
            dr2(21) = sMaterial ' material
            dr2(23) = sLocation 'location  
            dr2(24) = dv3.Item(0).Item(10) 'trnbelidtldisctypeDesc

        End If
        otblDtl2.Rows.Add(dr2)
    End Sub



    Sub RemoveDataInInvoiceDetail(ByVal iMtrloc As Int64, ByVal iMtroid As Int64, ByVal iQty As Int64)
        Dim objTable2 As DataTable = Session("tbldtl2")
        Dim iTotCount As Int16 = objTable2.Rows.Count - 1
        Dim C1 As Int16 = 0
        While C1 <= iTotCount
            If objTable2.Rows(C1).Item(4) = iMtrloc And objTable2.Rows(C1).Item(5) = iMtroid Then
                Dim dr2 As DataRow = objTable2.Rows(C1)
                dr2.BeginEdit()
                dr2(6) = dr2(6) - iQty
                dr2(22) = dr2(6) * dr2(8) 'amount

                dr2(19) = IIf(dr2(13) = 1, dr2(12), CDec(CDec(dr2(12)) / 100) * dr2(22)) 'amtdisc( jika type=1, amount, type2=persen, maka dikalikan persen nya dulu
                dr2(20) = dr2(22) - dr2(19) 'amtbelinetto
                dr2.EndEdit()

                If objTable2.Rows(C1).Item(6) <= 0 Then ' maka harus didelete
                    objTable2.Rows.RemoveAt(C1)
                    iTotCount -= 1
                Else
                    C1 += 1
                End If
            Else
                C1 += 1
            End If
        End While
        Session("tbldtl2") = objTable2
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSI")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSI") = sqlSearch
            Response.Redirect("~\Transaction\trnSIRecord.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Sales Invoice Record"
        Session("oid") = Request.QueryString("oid")
        btnDeleteMstr.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "UPDATE"
            TabContainer1.ActiveTabIndex = 1
        Else
            i_u.Text = "N E W"
        End If

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            BindData("")
            Dim tb1 As DataTable : tb1 = Session("TblDtl1")

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                i_u.Text = "UPDATE"
                TabContainer1.ActiveTabIndex = 1
                btnDeleteMstr.Visible = False
            Else
                i_u.Text = "N E W"
                Upduser.Text = Session("UserID")
                Updtime.Text = GetServerTime()

                btnDeleteMstr.Visible = False
                TabContainer1.ActiveTabIndex = 0
                btnPrint.Visible = False
            End If
        End If
        Dim tb2 As DataTable : tb2 = Session("tbldtlcost")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If chkPeriod.Checked Then
            Dim st1, st2 As Boolean : Dim sMsg As String = ""
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text)) : st1 = True
                If dat1 < CDate("01/01/1900") Then
                    sMsg &= "- Invalid Start date!!<BR>" : st1 = False
                End If
            Catch ex As Exception
                sMsg &= "- Invalid Start date!!<BR>" : st1 = False
            End Try
            Try
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text)) : st2 = True
                If dat2 < CDate("01/01/1900") Then
                    sMsg &= "- Invalid End date!!<BR>" : st2 = False
                End If
            Catch ex As Exception
                sMsg &= "- Invalid End date!!<BR>" : st2 = False
            End Try
            If st1 And st2 Then
                If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                    sMsg &= "- End date can't be less than Start Date!!"
                End If
            End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim sWhere As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        BindData(sWhere)
        Session("SearchSI") = sqlTempSearch

    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterPeriod1.Text = Format(Now.AddDays(-1), "dd/MM/yyyy") : FilterPeriod2.Text = Format(Now, "dd/MM/yyyy")
        DDLStatus.SelectedIndex = 0
        ddlFilter.SelectedIndex = 0 : txtFilter.Text = ""
        BindData("")
    End Sub

    Protected Sub btnSearchPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPO.Click
        Dim view As String = "view"
        BindDataPO(view, "") : gvListPO.Visible = True
    End Sub


    Private Function setTableDetail() As DataTable
        Dim dt As DataTable = New DataTable("TblDtl1")
        dt.Columns.Add("trnbelidtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelimstoid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelidtlseq", Type.GetType("System.Int32"))
        dt.Columns.Add("mtroid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelidtlqty", Type.GetType("System.Double"))
        dt.Columns.Add("trnbelidtlunit", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelidtlprice", Type.GetType("System.Double"))
        dt.Columns.Add("currencyoid", Type.GetType("System.Int32"))
        dt.Columns.Add("currencyrate", Type.GetType("System.Double"))
        dt.Columns.Add("trnbelidtldisc", Type.GetType("System.Double"))
        dt.Columns.Add("trnbelidtldisctype", Type.GetType("System.String"))
        dt.Columns.Add("trnbelidtldisc2", Type.GetType("System.Double"))
        dt.Columns.Add("trnbelidtldisctype2", Type.GetType("System.String"))
        dt.Columns.Add("trnbelidtlflag", Type.GetType("System.String"))
        dt.Columns.Add("trnbelidtlnote", Type.GetType("System.String"))
        dt.Columns.Add("trnbelidtlstatus", Type.GetType("System.String"))
        dt.Columns.Add("amtdisc", Type.GetType("System.Double"))
        dt.Columns.Add("amtdisc2", Type.GetType("System.Double"))
        dt.Columns.Add("totamtdisc", Type.GetType("System.Double"))
        dt.Columns.Add("amtbelinetto", Type.GetType("System.Double"))
        dt.Columns.Add("biayaexpedisi", Type.GetType("System.Double"))
        dt.Columns.Add("material", Type.GetType("System.String"))
        dt.Columns.Add("unit", Type.GetType("System.String"))
        dt.Columns.Add("sjoid", Type.GetType("System.Int32"))
        dt.Columns.Add("sjno", Type.GetType("System.String"))
        dt.Columns.Add("trnsjbelinote", Type.GetType("System.String"))
        dt.Columns.Add("sjrefoid", Type.GetType("System.Int32"))
        dt.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dt.Columns.Add("mtrref", Type.GetType("System.String"))
        dt.Columns.Add("unitseq", Type.GetType("System.Int32"))
        dt.Columns.Add("itemloc", Type.GetType("System.Int32"))
        dt.Columns.Add("trnbelidtloid_po", Type.GetType("System.Int32"))
        Return dt
    End Function



    Protected Sub btnSaveMstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""
        If trnjualno.Text = "" Then
            sMsg &= "- Please Select No. SI First !!<BR>"
        End If
        If SIno.Text = "" Then
            sMsg &= "- Please input No. SI Nota Lama !!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        'If Session("oid") = Nothing Or Session("oid") = "" Then
        ' insert table master
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "UPDATE ql_trnjualmst" & _
            " SET trnjualref = '" & SIno.Text & "' WHERE cmpcode='" & CompnyCode & "' and trnjualmstoid=" & prefixoid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()


            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString & " (SQL = " & sSql & ")", CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        'End If
        Session("oid") = Nothing : Session("TblDtl1") = Nothing
        Session("TblDtl2") = Nothing : Session("TblDtl3") = Nothing
        Response.Redirect("~\Transaction\trnSIRecord.aspx?awal=true")
    End Sub

    Protected Sub btnCancelMstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing : Session("TblDtl1") = Nothing
        Session("TblDtl2") = Nothing : Session("TblDtl3") = Nothing
        Response.Redirect("~\Transaction\trnSIRecord.aspx?awal=true")
        Upduser.Text = "" : Updtime.Text = ""
    End Sub

    Protected Sub btnDeleteMstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If oid.Text.Trim = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please Choose Invoice No!"))
            Exit Sub
        End If

        'without check data in other table
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans

            ' Reverse SJDetail Status (Invoiced) data lama
            sSql = "UPDATE ql_trnsjbelimst SET trnsjbelistatus='Approved' WHERE trnsjbelino in (select trnsjbelino from QL_trnbelidtl WHERE cmpcode='" & CompnyCode & "' AND trnbelimstoid=" & oid.Text & ") "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnbelidtl where trnbelimstoid=" & oid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from  QL_trnbelimst  where trnbelimstoid=" & oid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & " (SQL = " & sSql & ")", CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        Session("oid") = Nothing : Session("TblDtl1") = Nothing
        Session("TblDtl2") = Nothing : Session("TblDtl3") = Nothing
        Response.Redirect("~\Transaction\trnSIRecord.aspx?awal=true")
    End Sub

    Private Function GetCodeAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function GetDescAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        trnbelimstoid.Text = gvListPO.SelectedDataKey(0).ToString
        trnjualno.Text = gvListPO.SelectedDataKey(1).ToString
        prefixoid.Text = gvListPO.SelectedDataKey("trnjualmstoid")
        Session("TblDtl1") = Nothing
        gvListPO.Visible = False : ItemGv.Visible = False
    End Sub

    Protected Sub lkbDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim NoPo As String = sender.Tooltip
        BindSjdtl(NoPo) : ItemGv.Visible = True
    End Sub

    Private Sub BindSjdtl(ByVal PoNo As String)
        sSql = "select trnjualmstoid, i.itemcode, i.itemdesc, jd.trnjualdtlqty from ql_trnjualdtl jd inner join QL_MSTITEM i on i.itemoid = jd.itemoid where trnjualmstoid in (select trnjualmstoid from QL_trnjualmst where trnjualmstoid = jd.trnjualmstoid and trnjualno like '%" & Tchar(PoNo.Trim.Trim) & "%')"
        FillGV(ItemGv, sSql, "ql_trnjualdtl")
    End Sub

    Private Function bindSJDetail(ByVal sSJmstoid As String) As String
        Return "SELECT pod.trnbelimstoid,sj.trnsjbelioid,sj.trnsjbelino,sjd.trnsjbelidtlseq,sjd.mtrlocoid ,sjd.refoid,refname,(select itemdesc From ql_mstitem where itemoid = sjd.refoid) as matlongdesc, 0 as MATACCTGOID,sjd.qty,sjd.unitoid,pod.unitseq,g1.gendesc,pod.trnbelidtlprice,po.curroid,(SELECT c.currencydesc FROM ql_mstcurr c WHERE po.curroid=c.currencyoid)AS currency,po.currate,pod.trnbelidtldisctype,pod.trnbelidtldisctype2,pod.trnbelidtldisc,pod.trnbelidtldisc2,sjd.trnsjbelidtloid,'amtdisc'=case when pod.trnbelidtldisctype= 'AMT' then sjd.qty*pod.trnbelidtldisc Else ((sjd.qty*pod.trnbelidtlprice/100)*pod.trnbelidtldisc) End,'amtdisc2' = (trnbelidtldisc2 /pod.trnbelidtlqty) * sjd.qty,'totamtdisc'=case when pod.trnbelidtldisctype= 'AMT' then sjd.qty*pod.trnbelidtldisc Else ((sjd.qty*pod.trnbelidtlprice/100)*pod.trnbelidtldisc) end,'amtbelinetto' = (sjd.qty * pod.trnbelidtlprice ) - case when pod.trnbelidtldisctype= 'AMT' then sjd.qty*pod.trnbelidtldisc Else ((sjd.qty*pod.trnbelidtlprice/100)*pod.trnbelidtldisc) end,sjd.biayaexpedisi,sjd.mtrlocoid,pod.trnbelidtloid,sj.trnsjbelinote FROM ql_trnsjbelimst sj INNER JOIN ql_trnsjbelidtl sjd ON sj.cmpcode=sjd.cmpcode AND sjd.trnsjbelioid=sj.trnsjbelioid INNER JOIN ql_mstgen g1 ON sjd.cmpcode=g1.cmpcode AND g1.genoid=sjd.unitoid INNER JOIN ql_pomst po ON sj.cmpcode=po.cmpcode AND sj.trnbelipono=po.trnbelipono INNER JOIN ql_podtl pod ON po.cmpcode=pod.cmpcode AND po.trnbelimstoid=pod.trnbelimstoid and pod.trnbelidtloid = sjd.trnbelidtloid AND sjd.refoid=pod.itemoid WHERE sjd.cmpcode='" & CompnyCode & "' AND sjd.trnsjbelioid='" & sSJmstoid & "' order by sjd.trnsjbelidtlseq asc "

    End Function

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
    End Sub

    Protected Sub lkbList0_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub lkbList1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub lkbInfo2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lkbMore2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'untuk print
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim trnbelioid As String = ""
        Dim cekPrint As String = ""
        sSql = "select printStatus from ql_trnbelimst where printStatus = 'PRINT' and trnbelimstoid like '%" & trnbelioid & "%%' or trnbelino like '%" & trnbelioid & "%'"
        xCmd.CommandText = sSql : cekPrint = xCmd.ExecuteScalar
        If cekPrint = "PRINT" Then
            showMessage("No. PI Ini Sudah Tidak Dapat Di Print Lebih dr 2x !!!", CompnyName & " - WARNING", 2)
        Else
            report.Load(Server.MapPath(folderReport & "rptNotaBeliPTP.rpt"))
            Dim printID As Integer
            printID = Session("oid")
            report.SetParameterValue("sWhere", "  where m.trnbelimstoid='" & printID & "' ")
            report.SetParameterValue("namaPencetak", Session("UserID"))
            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)
            report.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            sSql = "update ql_trnbelimst set printStatus = 'PRINT' where trnbelino like '%" & trnbelioid & "%' or trnbelimstoid like '%" & trnbelioid & "%' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Session("no") & "_" & Format(GetServerTime(), "dd_MM_yy"))
        End If
    End Sub

    Protected Sub tbldata_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("oid") = tbldata.SelectedDataKey.Item(0)
        Session("no") = tbldata.SelectedDataKey.Item(1)
        Session("taxx") = tbldata.SelectedDataKey.Item(2)
        btnPrint_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("SearchSI") Is Nothing = False Then
            BindLastSearch()
        End If
    End Sub

    Private Sub BindLastSearch()
        FillGV(tbldata, Session("SearchSI"), "ql_trnbelimst")
    End Sub

    Protected Sub gvListPO_PageIndexChanging1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPO.PageIndexChanging
        Dim view As String = "view"
        gvListPO.PageIndex = e.NewPageIndex
        BindDataPO(view, "") : gvListPO.Visible = True
    End Sub

    Protected Sub gvListPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListPO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub lkbMore0_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub lkbInfo1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldata.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Export.Click
        report.Load(Server.MapPath(folderReport & "rptNotaBeliPTP.rpt"))
        Dim printID As Integer

        printID = Session("oid")
        report.SetParameterValue("sWhere", "  where m.trnbelimstoid='" & printID & "' ")
        report.SetParameterValue("namaPencetak", Session("UserID"))
        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)
        report.PrintOptions.PaperSize = PaperSize.PaperA5
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Session("no") & "_" & Format(GetServerTime(), "dd_MM_yy"))
    End Sub

    Public Sub showPrintExcel(ByVal oid As Integer)
        ' lblkonfirmasi.Text = ""
        Session("diprint") = "False"
        Response.Clear()
        Dim swhere As String = "" : Dim shaving As String = "" : Dim swhere2 As String = ""
        ' If dView.SelectedValue = "Master" Then
        sSql = "SELECT mm.trnbelipono, s.suppname,mm.trnbelipono,d.trnbelidtloid,d.trnbelimstoid,d.trnbelidtlseq,d.sjrefoid,d.mtroid,d.trnbelidtlqty,d.trnbelidtlunit,d.trnbelidtlprice,d.currencyoid,d.currencyrate,d.trnbelidtldisc,d.trnbelidtldisctype,d.trnbelidtlflag,d.trnbelidtlnote,d.trnbelidtlstatus,d.amtdisc,d.amtbelinetto,m.matlongdesc material, g.gendesc AS unit,sj.trnsjbelimstoid sjoid,sj.trnsjbelino sjno FROM QL_trnbelidtl d inner join QL_mstmat m on m.matoid=d.mtroid INNER JOIN ql_trnsjbelimst sj ON sj.cmpcode=d.cmpcode AND sj.trnsjbelimstoid=d.sjrefoid inner join ql_trnbelimst mm on mm.trnbelimstoid=d.trnbelimstoid INNER JOIN ql_mstgen g on g.cmpcode=d.cmpcode AND g.genoid=d.trnbelidtlunit inner join ql_mstsupp s on s.suppoid=mm.trnsuppoid WHERE d.trnbelimstoid=" & oid & " order by d.trnbelidtlseq"

        Response.AddHeader("content-disposition", "inline;filename=StatusPO.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub tbldata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        tbldata.PageIndex = e.NewPageIndex
        If Session("SearchSI") Is Nothing = False Then
            BindLastSearch()
            Exit Sub
        End If
        If chkPeriod.Checked Then
            Dim st1, st2 As Boolean : Dim sMsg As String = ""
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text)) : st1 = True
                If dat1 < CDate("01/01/1900") Then
                    sMsg &= "- Invalid Start date!!<BR>" : st1 = False
                End If
            Catch ex As Exception
                sMsg &= "- Invalid Start date!!<BR>" : st1 = False
            End Try
            Try
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text)) : st2 = True
                If dat2 < CDate("01/01/1900") Then
                    sMsg &= "- Invalid End date!!<BR>" : st2 = False
                End If
            Catch ex As Exception
                sMsg &= "- Invalid End date!!<BR>" : st2 = False
            End Try
            If st1 And st2 Then
                If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                    sMsg &= "- End date can't be less than Start Date!!"
                End If
            End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim sWhere As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        BindData(sWhere)
        Session("SearchSI") = sqlTempSearch
    End Sub

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Sub FillDDLAccounting(ByVal oDDLAccount As DropDownList, ByVal sFilterCode As String)
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        FillDDL(oDDLAccount, sSql)
    End Sub

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A'"
        If Not bParentAllowed Then
            sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) "
        End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function

    Protected Sub trnbelidtldisc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'ReAmountDetail2()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

#End Region

    Protected Sub btnClearPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvListPO.Visible = False : prefixoid.Text = ""
        trnjualno.Text = "" : ItemGv.Visible = False
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim view As String = "view"
        BindDataCust(view, "") : gvCustomer.Visible = True
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custoid.Text = gvCustomer.SelectedDataKey("custoid").ToString
        custname.Text = gvCustomer.SelectedDataKey("custname").ToString
        gvCustomer.Visible = False
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvCustomer.Visible = False : custoid.Text = ""
        custname.Text = ""
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCustomer.PageIndex = e.NewPageIndex
        gvCustomer.Visible = True
        Dim view As String = "view"
        BindDataCust(view, "")
    End Sub
End Class
