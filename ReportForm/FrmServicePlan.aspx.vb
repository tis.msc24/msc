Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Report_RptServicePlan
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim folderReport As String = "~/report/"
    Dim param As String = ""
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub Cabangddl()
        Dim sCabang As String = ""
        If kacabCek("ReportForm/frmPenerimaanService.aspx", Session("UserID")) = False Then
            If Session("branch_id") <> "01" Then
                sCabang &= " AND gencode='" & Session("branch_id") & "'"
            Else
                sCabang = ""
            End If
        End If
        sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='MSC' And g.gengroup='CABANG' " & sCabang & ""
        FillDDL(DDLcabang, sSql)
    End Sub

    Sub showPrint(ByVal tipe As String)
        Try
            param &= "WHERE QLS.BRANCH_CODE='" & DDLcabang.SelectedValue & "'"
            If txtFilter.Text <> "" Then
                param &= "AND " & DDLFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
            End If

            If txtPeriod1.Text <> "" And txtPeriod2.Text <> "" Then
                Try
                    Dim dDate1 As Date = CDate(toDate(txtPeriod1.Text))
                    Dim dDate2 As Date = CDate(toDate(txtPeriod2.Text))
                    If dDate1 < CDate("01/01/1900") Then
                        showMessage("Tanggal awal tidak valid", CompnyName & " - WARNING", 2) : Exit Sub
                    End If
                    If dDate2 < CDate("01/01/1900") Then
                        showMessage("Tanggal akhir tidak valid", CompnyName & " - WARNING", 2) : Exit Sub
                    End If
                    If dDate1 > dDate2 Then
                        showMessage("Period 1 harus lebih kecil dari period 2", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                Catch ex As Exception
                    showMessage("Format tangal tidak sesuai", CompnyName & " - WARNING", 2) : Exit Sub
                End Try
             
                param &= " and CONVERT(date, QLR.CREATETIME, 101) between '" & CDate(toDate(txtPeriod1.Text)) & "' and '" & CDate(toDate(txtPeriod2.Text)) & "'"
            End If

            If cbstatus.SelectedValue = "Status" Then
                If DDLStatus.SelectedValue <> "All" Then
                    param &= " AND QLR.REQSTATUS = '" & DDLStatus.SelectedValue & "'"
                End If
                param &= " AND QLR.REQSTATUS <> 'Paid' "
            Else
                param &= " AND QLR.REQSTATUS = 'Paid' "
            End If
            report = New ReportDocument
            report.Load(Server.MapPath("~\Report\rptServicePlan.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            report.SetParameterValue("sWhere", param)
            report.SetParameterValue("dPeriode", txtPeriod1.Text & " - " & txtPeriod2.Text)
            report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "PRINT" Then
                RCVPLANSERV.DisplayGroupTree = False
                'RCVPLANSERV.SeparatePages = False
                RCVPLANSERV.ReportSource = report

            ElseIf tipe = "EXCEL" Then
                report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "_" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose()
                Response.Redirect("~ReportForm\RptServicePlan.rpt?page=true")
            Else
                report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "_" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose()
                Response.Redirect("~ReportForm\FrmServicePlan.aspx?page=true")
            End If
            Session("ShowReport") = True
        Catch ex As Exception
        End Try
        param = ""
    End Sub
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("FrmServicePlan.aspx")
        End If
        Session("idPage") = Request.QueryString("idPage")
        Page.Title = CompnyName & " - Laporan Rencana Servis"
        If Not Page.IsPostBack Then
            Cabangddl()
            Session("ShowReport") = Nothing
            txtPeriod1.Text = Format(Date.Now, "01" & "/MM/yyyy")
            txtPeriod2.Text = Format(Date.Now, "dd/MM/yyyy")
        End If
        If Session("ShowReport") = True Then
            showPrint("PRINT")
        End If
    End Sub

    Protected Sub btnETPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnETPdf.Click
        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        showPrint("PDF")
    End Sub

    Protected Sub btnETExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        showPrint("EXCEL")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLFilter.SelectedIndex = 0 : txtFilter.Text = ""
        cbstatus.SelectedIndex = 0 : DDLStatus.SelectedIndex = 0
        txtPeriod1.Text = Format(Date.Now, "01" & "/MM/yyyy")
        txtPeriod2.Text = Format(Date.Now, "dd/MM/yyyy")
        RCVPLANSERV.ReportSource = Nothing
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub
#End Region

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        showPrint("PRINT")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
End Class
