<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnNotaHRReturn.aspx.vb" Inherits="Transaction_trnNotaHRReturn" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Nota HR Return" Width="232px"></asp:Label>
            </th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px; vertical-align: text-top;">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp;List Of Nota HR
                                Return</span><strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Cabang</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="FCabangNya" runat="server" Width="150px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Type</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="fddltype" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="HR">HR (Piutang Gantung)</asp:ListItem>
<asp:ListItem Value="PI">PI (Cadangan Piutang Gantung)</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Filter</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilter" runat="server" Width="120px" CssClass="inpText"><asp:ListItem Value="hr.trnnotahrreturno">No. Retur</asp:ListItem>
<asp:ListItem Value="s.suppname">Supplier</asp:ListItem>
</asp:DropDownList><SPAN style="COLOR: #ff0000">&nbsp;<asp:TextBox id="tbfilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;&nbsp; </SPAN></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px; HEIGHT: 22px"><asp:CheckBox id="CbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px; HEIGHT: 22px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 22px"><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="60px" CssClass="inpText" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;to <asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="60px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;<SPAN style="FONT-SIZE: 11px; COLOR: red"><SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN></SPAN></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Status</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilterstatus" runat="server" Width="123px" CssClass="inpText"><asp:ListItem Selected="True" Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>APPROVED</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;</TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px" colSpan=3><asp:ImageButton id="ibfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton> <asp:ImageButton id="ibviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton></TD></TR><TR><TD colSpan=3><asp:GridView id="GVTrn" runat="server" Width="100%" ForeColor="#333333" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="branch_code,trnnotahrreturoid">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrreturno" HeaderText="No. Retur">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrtype" HeaderText="Type Retur">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrreturdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrreturstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" Text="Print Nota" __designer:wfdid="w2" ToolTip='<%# String.Format("{0},{1}",Eval("trnnotahrreturoid"),Eval("trnnotahrreturno")) %>' Font-Underline="True" CommandName='<%# Eval("branch_code") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w16" Mask="99/99/9999" MaskType="Date" TargetControlID="tbperiodstart"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w17" Mask="99/99/9999" MaskType="Date" TargetControlID="tbperiodend"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender10" runat="server" __designer:wfdid="w18" TargetControlID="tbperiodstart" Format="dd/MM/yyyy" PopupButtonID="ibperiodstart"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w19" TargetControlID="tbperiodend" Format="dd/MM/yyyy" PopupButtonID="ibperiodend"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVTrn"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp;Form of Nota HR
                                Return</span>&nbsp;<strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="HEIGHT: 18px" colSpan=6><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label> <asp:Label id="masterstate" runat="server" Visible="False">new</asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px" id="TD10" runat="server" Visible="true">Cabang</TD><TD id="TD12" runat="server" Visible="true">:</TD><TD id="TD11" runat="server" Visible="true"><asp:DropDownList id="ddlFromcabang" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD>Status</TD><TD style="WIDTH: 8px">:</TD><TD><asp:TextBox id="status" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False" MaxLength="20">In Process</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD style="WIDTH: 101px">No.&nbsp;Retur</TD><TD>:</TD><TD><asp:TextBox id="noreturn" runat="server" Width="181px" CssClass="inpTextDisabled" Enabled="False" MaxLength="50"></asp:TextBox></TD><TD>Return Date</TD><TD style="WIDTH: 8px">:</TD><TD><asp:TextBox id="returndate" runat="server" Width="70px" CssClass="inpTextDisabled" Enabled="False" MaxLength="10" __designer:wfdid="w4"></asp:TextBox> <asp:ImageButton id="btnreturndate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="Label8" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD style="WIDTH: 101px">Type</TD><TD>:</TD><TD><asp:DropDownList id="DDLtype" runat="server" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="HR">HR (Piutang Gantung)</asp:ListItem>
<asp:ListItem Value="PI">PI (Cadangan Piutang Gantung)</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Label id="paytype" runat="server" Visible="False"></asp:Label> <asp:Label id="lblcurr" runat="server" Visible="False"></asp:Label></TD><TD>Supplier</TD><TD style="WIDTH: 8px">:</TD><TD><asp:TextBox id="suppname" runat="server" Width="200px" CssClass="inpText" MaxLength="50"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindSupp" onclick="btnFindSupp_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w1"></asp:ImageButton> <asp:ImageButton id="btnEraseSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton> <asp:Label id="suppoid" runat="server" Visible="False" __designer:wfdid="w3"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px"></TD><TD></TD><TD></TD><TD></TD><TD style="WIDTH: 8px"></TD><TD colSpan=1><asp:GridView id="gvCustomer" runat="server" Width="100%" ForeColor="#333333" Visible="False" EmptyDataText="No data in database." OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" OnPageIndexChanging="gvCustomer_PageIndexChanging" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="suppoid,suppcode,suppname">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Supp Code" ReadOnly="True" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label 
        ID="lblstatusdataCust" runat="server" CssClass="Important" 
        Text="No Customer Data !"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px; HEIGHT: 21px" id="Td1" Visible="true"><asp:Label id="lblNo" runat="server" Width="72px" Text="No. Nota HR" __designer:wfdid="w1"></asp:Label></TD><TD style="HEIGHT: 21px" id="Td2" Visible="true"><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD style="HEIGHT: 21px" id="Td3" Visible="true"><asp:TextBox id="invoiceno" runat="server" Width="150px" CssClass="inpText" MaxLength="50"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchinvoice" onclick="btnsearchinvoice_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btneraseinvoice" onclick="btneraseinvoice_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="JualMstOid" runat="server" Visible="False"></asp:Label><asp:Label id="typeSO" runat="server" Text="typeSO" Visible="False"></asp:Label><asp:Label id="branch_code" runat="server" Visible="False"></asp:Label></TD><TD style="HEIGHT: 21px"><asp:Label id="Label5" runat="server" Width="88px" Text="Date Nota HR" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 8px; HEIGHT: 21px"><asp:Label id="Label9" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD style="HEIGHT: 21px"><asp:TextBox id="trdate" runat="server" Width="70px" CssClass="inpTextDisabled" Enabled="False" MaxLength="20"></asp:TextBox> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px" id="Td13" Visible="true"></TD><TD id="Td14" Visible="true"></TD><TD id="Td15" colSpan=4 Visible="true"><asp:GridView id="GVTr" runat="server" Width="100%" ForeColor="#333333" Visible="False" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="trnnotahroid,trnnotahrno,trnnotahrdate,trnnotahramtnetto,trnnotahracumamt,trnnotahrreturamt,saldohr" OnRowDataBound="GVTr_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnnotahrno" HeaderText="No. Nota HR">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Nota HR Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahramtnetto" HeaderText="Amt Nota HR">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahracumamt" HeaderText="Accum HR">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrreturamt" HeaderText="Retur HR">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldohr" HeaderText="Amt HR Balance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label 
        ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px"><asp:Label id="lblhramt" runat="server" Width="72px" Text="HR Amount" __designer:wfdid="w1"></asp:Label></TD><TD><asp:Label id="Label15" runat="server" Text=":" __designer:wfdid="w7"></asp:Label></TD><TD><asp:TextBox id="hramt" runat="server" Width="130px" CssClass="inpTextDisabled" Font-Bold="True" Enabled="False" MaxLength="20">0.00</asp:TextBox> </TD><TD><asp:Label id="lblhrbalance" runat="server" Width="88px" Text="HR Balance" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 8px"><asp:Label id="Label10" runat="server" Text=":" __designer:wfdid="w3"></asp:Label></TD><TD><asp:TextBox id="hrbalance" runat="server" Width="130px" CssClass="inpTextDisabled" Font-Bold="True" Enabled="False" MaxLength="20">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px"><asp:Label id="lblhraccum" runat="server" Width="88px" Text="HR Accum Amt" __designer:wfdid="w1"></asp:Label></TD><TD><asp:Label id="Label14" runat="server" Text=":" __designer:wfdid="w6"></asp:Label></TD><TD><asp:TextBox id="hraccum" runat="server" Width="130px" CssClass="inpTextDisabled" Font-Bold="True" Enabled="False" MaxLength="20" __designer:wfdid="w4">0.00</asp:TextBox></TD><TD id="Td16" Visible="true"><asp:Label id="lblhrreturamt" runat="server" Width="88px" Text="HR Return" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 8px" id="Td17" Visible="true"><asp:Label id="Label12" runat="server" Text=":" __designer:wfdid="w4"></asp:Label></TD><TD id="Td18" Visible="true"><asp:TextBox id="hrreturamt" runat="server" Width="130px" CssClass="inpTextDisabled" Font-Bold="True" AutoPostBack="True" Enabled="False" MaxLength="20" __designer:wfdid="w7"></asp:TextBox>&nbsp;<asp:Label id="lblmax" runat="server" ForeColor="Red" __designer:wfdid="w8"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px"><asp:Label id="lblhrretur" runat="server" Width="88px" Text="HR Retur Amt" __designer:wfdid="w1"></asp:Label></TD><TD><asp:Label id="Label13" runat="server" Text=":" __designer:wfdid="w5"></asp:Label></TD><TD><asp:TextBox id="hrretur" runat="server" Width="130px" CssClass="inpTextDisabled" Font-Bold="True" Enabled="False" MaxLength="20" __designer:wfdid="w6">0.00</asp:TextBox></TD><TD Visible="true"></TD><TD style="WIDTH: 8px" Visible="true"></TD><TD Visible="true"></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px">Keterangan</TD><TD>:</TD><TD><asp:TextBox id="note" runat="server" Width="433px" CssClass="inpText" MaxLength="200"></asp:TextBox></TD><TD id="TD5" runat="server" Visible="false"></TD><TD style="WIDTH: 8px" id="TD4" runat="server" Visible="false"></TD><TD id="TD6" runat="server" Visible="false"></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 101px"><SPAN style="FONT-SIZE: 10pt; COLOR: #ff0000"><STRONG><SPAN></SPAN></STRONG></SPAN></TD><TD><SPAN style="FONT-SIZE: 10pt; COLOR: #ff0033"></SPAN></TD><TD></TD><TD></TD><TD style="WIDTH: 8px"></TD><TD>&nbsp;</TD></TR><TR><TD colSpan=6><asp:Label id="Label11" runat="server" Width="136px" Font-Size="X-Small" Font-Bold="True" Text="Nota HR Return Detail :" __designer:wfdid="w1" Font-Underline="True"></asp:Label></TD></TR><TR><TD colSpan=6><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False" __designer:wfdid="w16"></asp:Label>&nbsp;<asp:Label id="seq" runat="server" Visible="False" __designer:wfdid="w17">1</asp:Label>&nbsp;<asp:Label id="trnnotahrreturdtloid" runat="server" Visible="False" __designer:wfdid="w18"></asp:Label></TD></TR><TR><TD><asp:Label id="Label1" runat="server" Width="72px" Text="No. Nota PI" __designer:wfdid="w1"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="trnbelino" runat="server" Width="150px" CssClass="inpText" MaxLength="50" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchPI" onclick="btnsearchPI_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnerasePI" onclick="btnerasePI_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:Label id="trnbelimstoid" runat="server" Visible="False" __designer:wfdid="w6"></asp:Label></TD><TD></TD><TD></TD><TD></TD></TR><TR><TD></TD><TD></TD><TD colSpan=4><asp:GridView id="gvBeli" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w9" OnSelectedIndexChanged="gvBeli_SelectedIndexChanged" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="trnnotahroid,trnnotahrno,saldohr,trnnotahramtnetto,trnnotahrdtlamt">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnnotahrno" HeaderText="No. Nota PI">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Nota PI Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahramtnetto" HeaderText="Amt Nota PI">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrdtlamt" HeaderText="Amt Nota HR">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahracumamt" HeaderText="Accum PI">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrreturhramt" HeaderText="Retur PI">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrreturamt" HeaderText="Retur HR">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldohr" HeaderText="Amt PI Balance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label 
        ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD>Retur Amt.</TD><TD>:</TD><TD><asp:TextBox id="returamt" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" MaxLength="50" __designer:wfdid="w7"></asp:TextBox>&nbsp;<asp:Label id="lblmaxdtl" runat="server" ForeColor="Red" __designer:wfdid="w8"></asp:Label>&nbsp;<asp:Label id="lblmaxdtl2" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label></TD><TD></TD><TD></TD><TD></TD></TR><TR><TD>Note</TD><TD>:</TD><TD><asp:TextBox id="notedtl" runat="server" Width="432px" CssClass="inpText" MaxLength="50" __designer:wfdid="w8"></asp:TextBox></TD><TD></TD><TD></TD><TD></TD></TR><TR><TD colSpan=6></TD></TR><TR><TD colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton><asp:ImageButton id="btnClearDtl" runat="server" ImageUrl="~/Images/Clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w14"></asp:ImageButton></TD></TR><TR><TD colSpan=6></TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 150px"><asp:GridView id="gvDtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w15" OnSelectedIndexChanged="gvDtl_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="seq,trnbelimstoid,trnbelino,trnbelidtlamt,trnnotahrreturdtlamt,trnnotahrreturdtlnote" OnRowDataBound="gvDtl_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimstoid" HeaderText="trnbelimstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="PI No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidtlamt" HeaderText="PI Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrreturdtlamt" HeaderText="Retur Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrreturdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail PI" __designer:wfdid="w111"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR><TD colSpan=6><asp:Label id="costamount" runat="server" Visible="False">0</asp:Label><asp:Label id="cashamount" runat="server" Visible="False">0</asp:Label><asp:Label id="returamount" runat="server" Visible="False"></asp:Label><asp:Label id="amtppn" runat="server" Visible="False"></asp:Label><asp:Label id="amtpiutang" runat="server" Visible="False"></asp:Label><asp:Label id="amtpotpenjualan" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=6><asp:Label id="lblcreate" runat="server"></asp:Label> <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label> <asp:Label id="createtime" runat="server" Font-Bold="True" Visible="False" __designer:wfdid="w135"></asp:Label></TD></TR><TR><TD colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibsave" runat="server" ImageUrl="~/Images/Save.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" onclick="ibdelete_Click" runat="server" ImageUrl="~/Images/Delete.png" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibapproval" runat="server" ImageUrl="~/Images/sendapproval.png"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibposting" runat="server" ImageUrl="~/Images/posting.png" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPrint" runat="server" ImageUrl="~/Images/print.png" CausesValidation="False" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress>&nbsp; <ajaxToolkit:MaskedEditExtender id="MEE30" runat="server" __designer:wfdid="w1" Mask="99/99/9999" MaskType="Date" TargetControlID="returndate"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CE1" runat="server" __designer:wfdid="w3" TargetControlID="returndate" Format="dd/MM/yyyy" PopupButtonID="btnreturndate"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="uppopupmsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                            <asp:Label ID="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" colspan="2"></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></td>
                        <td style="TEXT-ALIGN: left" class="Label">
                            <asp:Label ID="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" colspan="2">&nbsp;<asp:ImageButton ID="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpepopupmsg" runat="server" TargetControlID="bepopupmsg" Drag="True" PopupDragHandleControlID="lblcaption" BackgroundCssClass="modalBackground" PopupControlID="pnlpopupmsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bepopupmsg" runat="server" BorderColor="Transparent" BorderStyle="None" BackColor="Transparent" Visible="False" CausesValidation="False"></asp:Button>

        </ContentTemplate>
    </asp:UpdatePanel>
        
</asp:Content>

