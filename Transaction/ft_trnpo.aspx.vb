'Prgmr : Shutup_M ; Update : zipi on 28.02.16
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnpo
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim dtStaticItem As DataTable
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Private cKon As New Koneksi
    Private cProc As New ClassProcedure
    Dim crate As ClassRate
#End Region

#Region "Function"
    Function checkApprovaldata(ByVal sTableName As String, ByVal sEvent As String, ByVal sOid As String, _
     ByVal sRequestUser As String, ByVal sStatus As String) As DataTable
        Dim ssql As String = "SELECT CMPCODE,APPROVALOID,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME," & _
             "OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval WHERE CmpCode='" & CompnyCode & _
             "' and RequestUser LIKE '" & Tchar(sRequestUser) & "%' and STATUSREQUEST = '" & sStatus & "' " & _
             " AND TABLENAME='" & sTableName & "' and OID=" & sOid & " and EVENT='" & sEvent & "' ORDER BY REQUESTDATE DESC"
        Return cKon.ambiltabel(ssql, "QL_APPROVAL")
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function 

    Private Function getMonthname(ByVal dmonth As Integer) As String
        Dim res As String = ""
        If dmonth = 1 Then
            res = "Januari"
        ElseIf dmonth = 2 Then
            res = "Februari"
        ElseIf dmonth = 3 Then
            res = "Maret"
        ElseIf dmonth = 4 Then
            res = "April"
        ElseIf dmonth = 5 Then
            res = "Mei"
        ElseIf dmonth = 6 Then
            res = "Juni"
        ElseIf dmonth = 7 Then
            res = "Juli"
        ElseIf dmonth = 8 Then
            res = "Agustus"
        ElseIf dmonth = 9 Then
            res = "September"
        ElseIf dmonth = 10 Then
            res = "Oktober"
        ElseIf dmonth = 11 Then
            res = "November"
        ElseIf dmonth = 12 Then
            res = "Desember"
        End If
        Return res
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer, ByVal gvObj As GridView) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvObj.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Function insertData(ByVal dtTemp As DataTable) As DataTable
        If Session("TblDtl") Is Nothing Then
            Dim dtlDS As DataSet = New DataSet
            Dim dtlTable As DataTable = New DataTable("TabelpodtlDetail")
            dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
            dtlTable.Columns.Add("podtloid", Type.GetType("System.String"))
            dtlTable.Columns.Add("pomstoid", Type.GetType("System.String"))
            dtlTable.Columns.Add("podtlseq", Type.GetType("System.String"))
            dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("podtlqty", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("ekspedisi", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtlunit", Type.GetType("System.String"))
            dtlTable.Columns.Add("podtlprice", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("Total", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("DiscAmt", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtldisc", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtldisctype", Type.GetType("System.String"))
            dtlTable.Columns.Add("DiscAmt2", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtldisc2", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtldisctype2", Type.GetType("System.String"))
            dtlTable.Columns.Add("totDiscAmt", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("unitseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("pobelidtlnote", Type.GetType("System.String"))
            dtlTable.Columns.Add("pobelidtlstatus", Type.GetType("System.String"))
            dtlTable.Columns.Add("updtime", Type.GetType("System.String"))
            dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
            dtlTable.Columns.Add("poamtdtldisc", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("poamtdtlnetto", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("material", Type.GetType("System.String"))
            dtlTable.Columns.Add("merk", Type.GetType("System.String"))
            dtlTable.Columns.Add("Unit", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("Subcat", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("matcategoryoid", Type.GetType("System.Int32"))
            'dtlTable.Columns.Add("currencyoid", Type.GetType("System.Int32"))
            'dtlTable.Columns.Add("currencyrate", Type.GetType("System.Decimal"))
            'dtlTable.Columns.Add("mtrloc", Type.GetType("System.Int32"))
            dtlDS.Tables.Add(dtlTable)
            Session("TblDtl") = dtlTable
        End If
        Dim objTable As DataTable
        objTable = Session("TblDtl")
        Dim objRow As DataRow

        Dim ErrorAuto As String = ""

        For C1 As Integer = 0 To dtTemp.Rows.Count - 1
            Dim dv As DataView = objTable.DefaultView

            dv.RowFilter = "matoid=" & dtTemp.Rows(C1).Item("itemoid").ToString.Trim
            If dv.Count > 0 Then
                dv.RowFilter = ""

                ErrorAuto &= "Data " & dtTemp.Rows(C1).Item("ItemLDesc").ToString & " merk " & dtTemp.Rows(C1).Item("merk").ToString & " sudah di tambahkan sebelumnya, Silahkan cek ulang! </br>"
                'objTable = Session("TblDtl") : tbldtl.DataSource = objTable
                'tbldtl.DataBind() : tbldtl.Visible = True
            Else
                objRow = objTable.NewRow()
                objRow("cmpcode") = dtTemp.Rows(C1).Item("cmpcode")
                objRow("podtloid") = 0
                objRow("pomstoid") = 0
                objRow("poamtdtlnetto") = 0
                objRow("podtlseq") = objTable.Rows.Count + 1
                objRow("matoid") = dtTemp.Rows(C1).Item("itemoid").ToString.Trim
                objRow("podtlqty") = ToDouble(dtTemp.Rows(C1).Item("orderqty"))
                objRow("podtlunit") = dtTemp.Rows(C1).Item("unit").ToString
                objRow("ekspedisi") = ToDouble(0)
                objRow("podtlprice") = ToDouble(0)
                objRow("podtldisc") = ToMaskEdit(0, 4)
                objRow("podtldisctype") = "AMT"
                objRow("podtldisc2") = ToDouble(0)
                objRow("podtldisctype2") = "AMT"
                objRow("pobelidtlnote") = ""
                objRow("pobelidtlstatus") = posting.Text
                objRow("upduser") = Session("UserID")
                objRow("updtime") = FormatDateTime(Now(), DateFormat.GeneralDate)
                objRow("DiscAmt") = ToDouble(0)
                objRow("DiscAmt2") = ToDouble(0)
                objRow("poamtdtldisc") = ToDouble(0)
                objRow("unitseq") = 3
                objRow("material") = dtTemp.Rows(C1).Item("ItemLDesc").ToString
                objRow("Unit") = dtTemp.Rows(C1).Item("itemunitoid").ToString
                objRow("matcategoryoid") = dtTemp.Rows(C1).Item("Category").ToString
                objRow("merk") = dtTemp.Rows(C1).Item("merk").ToString

                objTable.Rows.Add(objRow)
            End If
            dv.RowFilter = ""

        Next
        Session("TblDtl") = objTable
        Session("ItemLine") = objTable.Rows.Count + 1
        trnbelidtlseq.Text = Session("ItemLine")
        Return Session("TblDtl")
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Public Function isiDS(ByVal squery As String, ByVal snamatabel As String) As DataTable
        Dim ssqlcust As String
        ssqlcust = squery
        Dim mySqlDA As New SqlDataAdapter(ssqlcust, ConnStr)
        Dim objDsRef As New DataSet
        mySqlDA.Fill(objDsRef, snamatabel)
        Return objDsRef.Tables(snamatabel)
    End Function
#End Region

#Region "Procedure"
    Private Sub bindSuppHistory(ByVal Itemnya As Integer, ByVal Harga As String)
        Try

            Dim query = "select top 3 pomst.trnbelipono pono , s.suppname , podtl.trnbelidtlqty orderPO ,podtl.trnbelidtlprice lastprice , g.gendesc unit ,podtl.trnbelidtldisctype disc , podtl.trnbelidtldisctype2 disc2 , podtl.trnbelidtldisc AmtDisc , podtl.trnbelidtldisc2 AmtDisc2 , podtl.amtbelinetto netto From QL_MSTITEM m inner join QL_podtl podtl on podtl.itemoid = m.itemoid inner join QL_pomst pomst on pomst.trnbelimstoid = podtl.trnbelimstoid inner join QL_mstsupp s on s.suppoid = pomst.trnsuppoid inner join QL_mstgen g on g.genoid = podtl.trnbelidtlunit where m.itemoid = " & Itemnya & " and pomst.trnbelistatus = 'Approved' order by pomst.trnbelipodate desc"
            'AND " & DDLFilterMat.SelectedValue & " LIKE '%" & Tchar(txtFilterMat.Text) & "%' 
            FillGV(GVdetailSupp, query, "QL_mstSuppHIst")

            sSql = "select top 1 ISNULL(podtl.trnbelidtlprice,0.0) lastprice from QL_MSTITEM m Inner join QL_podtl podtl on podtl.itemoid = m.itemoid Inner join QL_pomst pomst on pomst.trnbelimstoid = podtl.trnbelimstoid Inner join QL_mstsupp s on s.suppoid = pomst.trnsuppoid Inner join QL_mstgen g on g.genoid = podtl.trnbelidtlunit where m.itemoid = " & matoid.Text & " and pomst.trnbelistatus = 'Approved' order by pomst.trnbelipodate desc"

            trnbelidtlprice.Text = ToMaskEdit(GetScalar(sSql), 3)
            GVdetailSupp.Visible = True
        Catch ex As Exception
            showMessage(ex.Message, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            DDlmatCatmst.SelectedIndex = 0 : cProc.DisposeGridView(GVdetailSupp)
        End Try

    End Sub

    Private Sub Requestinfo()
        Dim sqlPlus As String = "" : Dim GroupBy As String = ""
        Dim unitcurrent As String = GetStrData("SELECT a.gendesc FROM QL_mstgen a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.genoid = b.satuan1 WHERE a.cmpcode = '" & CompnyCode & "' AND b.itemoid = " & Integer.Parse(matoid.Text) & "")

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        'pricelist
        If currencyoid.SelectedValue = "1" Then
            sSql = "SELECT pricelist from ql_mstitem m Where m.itemoid = " & Integer.Parse(matoid.Text) & " AND m.cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql
            PriceListInfo.Text = ToMaskEdit(xCmd.ExecuteScalar, 4)
        Else
            PriceListInfo.Text = "0.00"
        End If
        'unit1
        sSql = "SELECT g1.gendesc from ql_mstitem m inner join ql_mstgen g1 on g1.genoid = m.satuan1 where m.itemoid = " & Integer.Parse(matoid.Text) & " AND m.cmpcode = '" & CompnyCode & "' "
        xCmd.CommandText = sSql
        lblunit1.Text = "Last " & xCmd.ExecuteScalar
        'unit2
        sSql = "SELECT g2.gendesc from ql_mstitem m inner join ql_mstgen g2 on g2.genoid = m.satuan1 where m.itemoid = " & Integer.Parse(matoid.Text) & " AND m.cmpcode = '" & CompnyCode & "' "
        xCmd.CommandText = sSql
        lblunit2.Text = "Last " & xCmd.ExecuteScalar

        sSql = "SELECT lastPricebuyUnit1 from ql_mstitem m where m.itemoid = " & Integer.Parse(matoid.Text) & " AND m.cmpcode = '" & CompnyCode & "' "
        xCmd.CommandText = sSql
        pricebuy1.Text = ToMaskEdit(xCmd.ExecuteScalar, 4)

        'lastbuyunit2
        sSql = "SELECT lastPricebuyUnit2 from ql_mstitem m Where m.itemoid = " & Integer.Parse(matoid.Text) & " AND m.cmpcode = '" & CompnyCode & "' "
        xCmd.CommandText = sSql
        pricebuy2.Text = ToMaskEdit(xCmd.ExecuteScalar, 4)
        ' 
        'CEK TOTAL STOCK SEKARANG
        If TypePOnya.SelectedValue = "Selisih" Then
            sqlPlus &= "AND branch_code='" & CabangNya.SelectedValue & "' And mtrlocoid NOT IN (Select gl.genoid From ql_mstgen gl Where gl.genother1 IN (1010,0) AND gl.genoid=con.mtrlocoid AND gl.gengroup='LOCATION')"
            GroupBy = " ,branch_code,mtrlocoid"
            gvMaterial.Columns(6).Visible = False
        End If

        sSql = "Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.0) From QL_conmtr con Where refoid=" & Integer.Parse(matoid.Text) & " AND periodacctg=(SELECT TOP 1 periodacctg FROM QL_crdmtr WHERE closeuser = '' ORDER BY crdmatoid DESC) " & sqlPlus & " Group By refoid,periodacctg" & GroupBy & ""

        xCmd.CommandText = sSql
        labelcurrentstock.Text = "Total Current Stock : " & Format(xCmd.ExecuteScalar, "#,##0.0000") & ""

        'CEK PEMBELIAN 3 BULAN TERAKHIR
        Dim nowpodate As Date = CDate(toDate(trnbelipodate.Text))

        sSql = "SELECT ISNULL(SUM(qty), 0) FROM (SELECT CASE a.unitseq WHEN 1 THEN ISNULL(a.trnorderdtlqty, 0)*(SELECT konversi1_2 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ")*(SELECT konversi1_2 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ") WHEN 2 THEN ISNULL(a.trnorderdtlqty, 0)*(SELECT konversi1_2 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ") ELSE ISNULL(a.trnorderdtlqty, 0) END AS qty FROM QL_trnorderdtl a INNER JOIN QL_trnordermst b ON a.cmpcode = b.cmpcode AND a.trnordermstoid = b.ordermstoid WHERE a.itemoid = " & Integer.Parse(matoid.Text) & " AND a.cmpcode = '" & CompnyCode & "' AND MONTH(b.trnorderdate) = " & nowpodate.AddMonths(-3).Month & " AND YEAR(b.trnorderdate) = " & nowpodate.AddMonths(-3).Year & " AND b.trnorderstatus = 'Approved' AND b.trnordertype = 'GROSIR') xz"
        xCmd.CommandText = sSql
        labeljual1.Text = "Jual (" & getMonthname(nowpodate.AddMonths(-3).Month) & ")"
        labelqty1.Text = "" & Format(xCmd.ExecuteScalar, "#,##0.00") & " " & unitcurrent & ""

        sSql = "SELECT ISNULL(SUM(qty), 0) FROM (SELECT CASE a.unitseq WHEN 1 THEN ISNULL(a.trnorderdtlqty, 0)*(SELECT konversi1_2 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ")*(SELECT konversi1_2 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ") WHEN 2 THEN ISNULL(a.trnorderdtlqty, 0)*(SELECT konversi2_3 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ") ELSE ISNULL(a.trnorderdtlqty, 0) END AS qty FROM QL_trnorderdtl a INNER JOIN QL_trnordermst b ON a.cmpcode = b.cmpcode AND a.trnordermstoid = b.ordermstoid WHERE a.itemoid = " & Integer.Parse(matoid.Text) & " AND a.cmpcode = '" & CompnyCode & "' AND MONTH(b.trnorderdate) = " & nowpodate.AddMonths(-2).Month & " AND YEAR(b.trnorderdate) = " & nowpodate.AddMonths(-2).Year & " AND b.trnorderstatus = 'Approved' AND b.trnordertype = 'GROSIR') xz"
        xCmd.CommandText = sSql
        labeljual2.Text = "Jual (" & getMonthname(nowpodate.AddMonths(-2).Month) & ")"
        labelqty2.Text = "" & Format(xCmd.ExecuteScalar, "#,##0.00") & " " & unitcurrent & ""

        sSql = "SELECT ISNULL(SUM(qty), 0) FROM (SELECT CASE a.unitseq WHEN 1 THEN ISNULL(a.trnorderdtlqty, 0)*(SELECT konversi1_2 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ")*(SELECT konversi2_3 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ") WHEN 2 THEN ISNULL(a.trnorderdtlqty, 0)*(SELECT konversi2_3 FROM QL_mstitem WHERE itemoid = " & Integer.Parse(matoid.Text) & ") ELSE ISNULL(a.trnorderdtlqty, 0) END AS qty FROM QL_trnorderdtl a INNER JOIN QL_trnordermst b ON a.cmpcode = b.cmpcode AND a.trnordermstoid = b.ordermstoid WHERE a.itemoid = " & Integer.Parse(matoid.Text) & " AND a.cmpcode = '" & CompnyCode & "' AND MONTH(b.trnorderdate) = " & nowpodate.AddMonths(-1).Month & " AND YEAR(b.trnorderdate) = " & nowpodate.AddMonths(-1).Year & " AND b.trnorderstatus = 'Approved' AND b.trnordertype = 'GROSIR') xz"
        xCmd.CommandText = sSql
        labeljual3.Text = "Jual (" & getMonthname(nowpodate.AddMonths(-1).Month) & ")"
        labelqty3.Text = "" & Format(xCmd.ExecuteScalar, "#,##0.00") & " " & unitcurrent & ""

        Dim datea As New Date(nowpodate.AddMonths(-3).Year, nowpodate.AddMonths(-3).Month, 1)
        Dim dateb As New Date(nowpodate.Year, nowpodate.Month, New Date(nowpodate.Year, nowpodate.Month, 1).AddMonths(1).AddDays(-1).Day)
        sSql = "SELECT ISNULL(SUM(trnbelidtlqty_retur_unit3), 0) AS qty FROM QL_trnbelidtl a INNER JOIN QL_trnbelimst b ON a.cmpcode = b.cmpcode AND a.trnbelimstoid = b.trnbelimstoid WHERE a.itemoid = " & Integer.Parse(matoid.Text) & " AND a.cmpcode = '" & CompnyCode & "' AND CONVERT(DATE, b.trnbelidate, 103) BETWEEN '" & datea & "' AND '" & dateb & "' AND b.trnbelistatus = 'POST' AND b.trnbelitype = 'GROSIR'"
        xCmd.CommandText = sSql
        labelretur.Text = "Total (Retur Beli) 3 Bulan Terakhir"
        labelqtyretur.Text = "" & Format(xCmd.ExecuteScalar, "#,##0.00") & " " & unitcurrent & ""
        conn.Close()

        labelcurrentunit.Text = unitcurrent
        labelcurrentstock.Visible = True : labelcurrentunit.Visible = True
        labeljual1.Visible = True : labeljual2.Visible = True
        labeljual3.Visible = True : labelretur.Visible = True
        labelqty1.Visible = True : labelqty2.Visible = True
        labelqty3.Visible = True : labelqtyretur.Visible = True
        pricebuy1.Visible = True : pricebuy2.Visible = True
        pricebuy3.Visible = False : lblunit1.Visible = True
        lblunit2.Visible = True : lblunit3.Visible = False
        trbulan.Visible = True : trqty.Visible = True
    End Sub 

    Private Sub QunitSeq()
        satuan1.Text = GetStrData("select satuan1 From ql_mstITEM m Where m.itemoid = '" & matoid.Text & "'")
        satuan2.Text = GetStrData("select satuan1 From ql_mstITEM m Where m.itemoid = '" & matoid.Text & "'")
        satuan3.Text = GetStrData("select satuan1 From ql_mstITEM m Where m.itemoid = '" & matoid.Text & "'")
    End Sub

    Private Sub fillTotalOrder()
        Dim tbDtl As DataTable = Session("TblDtl")
        Dim NettAmt As Double = 0 : Dim NettDisC As Double = 0
        Dim NettBonus As Double = 0 : Dim AmtBrut As Double = 0
        For c1 As Integer = 0 To tbDtl.Rows.Count - 1
            NettAmt += tbDtl.Rows(c1).Item("poamtdtlnetto")
            NettDisC += tbDtl.Rows(c1).Item("DiscAmt")
            AmtBrut += tbDtl.Rows(c1).Item("podtlprice") * tbDtl.Rows(c1).Item("podtlqty")
            NettBonus += tbDtl.Rows(c1).Item("bonusAmt")
        Next
        amtdiscdtl.Text = ToMaskEdit(NettDisC, 4)
        amtbeli.Text = ToMaskEdit(AmtBrut, 4)
        amtbelinetto1.Text = ToMaskEdit(NettAmt, 4)
        totdtlvoucher.Text = ToMaskEdit(totvoucher.Text, 4)
        tot.Text = ToMaskEdit(NettAmt, 4)
    End Sub

    Private Sub TampilkanQty3()
        Dim unit As String = GetStrData("select case satuan3 when " & ddlUnit.SelectedValue & " then 'satuan3' else case satuan2 when " & ddlUnit.SelectedValue & " then 'satuan2' else case satuan1 when " & ddlUnit.SelectedValue & " then 'satuan1' end end end as harga from QL_mstItem where itemoid = " & matoid.Text & "")

        Dim qty3_to2 As Double = GetStrData("select qty3_to2 from QL_mstItem where itemoid = " & matoid.Text & "")
        Dim qty3_to1 As Double = GetStrData("select qty3_to1 from QL_mstItem where itemoid = " & matoid.Text & "")
        Dim konv1_2 As Double = GetStrData("select konversi1_2 from QL_mstItem where itemoid = " & matoid.Text & "")
        Dim konv2_3 As Double = GetStrData("select konversi1_2 from QL_mstItem where itemoid = " & matoid.Text & "")

        Dim SatuanKe3 As String = GetStrData("select g.gendesc satuan3 from QL_mstItem i inner join ql_mstgen g on i.satuan3 = g.genoid  where itemoid = " & matoid.Text & "")

        If unit = "satuan1" Then
            Konversike3.Text = "" & ToMaskEdit(trnbelidtlqty.Text * konv1_2 * konv2_3, 4) & " " & SatuanKe3
        ElseIf unit = "satuan2" Then
            Konversike3.Text = "" & ToMaskEdit(trnbelidtlqty.Text * konv2_3, 4) & " " & SatuanKe3
        Else
            Konversike3.Text = "" & ToMaskEdit(trnbelidtlqty.Text, 4) & " " & SatuanKe3
        End If
    End Sub

    Private Sub PrintReport(ByVal id As String, ByVal no As String, ByVal report2 As String)
        'untuk print
        Dim LastStock As String = ""
        If Session("branch_id") <> "10" Then
            LastStock &= " AND crd.branch_code='" & dCabangNya.SelectedValue & "' And crd.mtrlocoid <> -10"
        End If

        LastStock &= " And crd.periodacctg = '" & periodacctg.Text & "'"
        If ddlprinter.SelectedValue = "Without" Then
            report.Load(Server.MapPath(folderReport & "rptNotaEmptyNotePO.rpt"))
        Else
            report.Load(Server.MapPath(folderReport & "rptNotaPO.rpt"))
        End If

        report.SetParameterValue("sWhere", " Where m.trnbelimstoid='" & id & "'")
        report.SetParameterValue("namaPencetak", Session("UserID"))
        report.SetParameterValue("LastStock", LastStock)
        cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers

        If report2 = "pdf" Or report2 = "" Then
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        ElseIf report2 = "excel" Then
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        End If
        report.Close() : report.Dispose() : Session("no") = Nothing
    End Sub

    Private Sub PrintReportPTP(ByVal id As String, ByVal no As String, ByVal report2 As String)
        'untuk print
        'report.Load(Server.MapPath(folderReport & "POrder_hrg.rpt"))
        'report.SetParameterValue("Id", id)

        report.Load(Server.MapPath(folderReport & "rptNotaPO.rpt"))
        report.SetParameterValue("sWhere", " where m.trnbelimstoid='" & id & "'")
        report.SetParameterValue("namaPencetak", Session("UserID"))

        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)
        report.PrintOptions.PaperSize = PaperSize.PaperA4
        report.PrintOptions.PrinterName = ddlprinter.SelectedValue
        report.PrintToPrinter(1, True, 0, 0)
        report.Close() : report.Dispose()
    End Sub

    Private Sub PrintReportNoPrice(ByVal id As String, ByVal no As String)
        'untuk print
        report.Load(Server.MapPath(folderReport & "POrder.rpt"))
        report.SetParameterValue("Id", id)

        cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
            System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        'Dim crConnInfo As New ConnectionInfo()
        'With crConnInfo
        '    .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
        '    .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
        '    .IntegratedSecurity = True
        'End With
        'SetDBLogonForReport(crConnInfo, report)

        'report.PrintOptions.PrinterName = printerPOS
        report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        'report.PrintToPrinter(1, False, 0, 0)
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))

        report.Close() : report.Dispose() : Session("no") = Nothing
        Response.Redirect("~\Transaction\ft_trnpo.aspx?awal=true")
    End Sub

    Private Sub bind_data_usr(ByVal splus As String)
        Dim query As String = "SELECT userid, username FROM QL_mstprof where CMPCODE='" & CompnyCode & "' and USERID in (" & GetStrData("select genother1 from QL_mstgen where cmpcode='" & CompnyCode & "' and gendesc like '%Purchase Order%' and gengroup='APPROVAL_USER'") & ")"
        If splus <> "" Then
            query = query & splus
        End If
        FillGV(gvUserList, query, "ql_mstprof")
        btnHiddenPnlUser.Visible = True
        pnlUser.Visible = True : mpe2.Show()
    End Sub 

    Public Sub binddataimp(ByVal sqlPlus As String)
        sSql = "Select suppoid,suppcode,suppname, paymenttermoid from QL_mstsupp where supptype = 'GROSIR' and cmpcode='" & CompnyCode & "' " & sqlPlus & " ORDER BY suppcode ASC"
        FillGV(gvImportir, sSql, "QL_mstsupp")
    End Sub

    Public Sub showPrintExcel(ByVal name As String, ByVal oid As Integer)

        Session("diprint") = "False" : Response.Clear()
        Dim swhere As String = "", shaving As String = "", swhere2 As String = ""

        sSql = "select m.trnbelipono 'PO No', s.suppname 'Supplier', m.TRNBELIdeliverydate 'PO Delivery Date', m.trnbelipodate 'PO Date', m.trnbelistatus 'PO Status', g.gendesc as Paytype, c.currencycode 'Curr' ,m.currate 'Rate', mt.ITEMDESC 'ITEM', d.trnbelidtlnote 'Note Detail', g2.gendesc 'Unit', d.trnbelidtlqty 'Qty', d.trnbelidtlprice 'Price', d.amtdisc + amtdisc2 'Disc Detail', d.amtbelinetto 'Detail Amt', d.upduser, d.updtime from ql_podtl d inner join ql_pomst m on m.trnbelimstoid=d.trnbelimstoid inner join ql_mstsupp s on s.suppoid=m.trnsuppoid inner join ql_mstgen g on g.genoid=m.trnpaytype inner join ql_mstcurr c on c.currencyoid=m.curroid inner join QL_mstgen g2 ON d.trnbelidtlunit=g2.genoid inner join QL_mstITEM mt ON d.ITEMoid=mt.ITEMoid where  d.cmpcode='" & CompnyCode & "' and m.trnbelimstoid = " & oid

        Response.AddHeader("content-disposition", "inline;filename= Nota_PO.xls")
        Response.Charset = "" : Response.ContentType = "application/vnd.ms-excel"
        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close() : Response.End()
    End Sub

    Private Sub cleardtlpr()
        prdtloid.Text = ""
        prno.Text = "" : prno.Text = ""
        trnbelidtlqty.Text = "0.00"
        I_u3.Text = "New Detail"
    End Sub
 
    Private Sub tabindex21true()
        'kdvoucher.Visible = True
        descVoucher.Visible = True : voucherNote.Visible = True
        datevoucher.Visible = True : imbVoucher.Visible = True
        amtVoucher.Visible = True
    End Sub

    Private Sub tabindex21false()
        kdvoucher.Text = "" : descVoucher.Text = ""
        voucherNote.Text = "" : datevoucher.Text = ""
        amtVoucher.Text = "0.00"
    End Sub

    Private Sub marketlocal()
        kdimportir.Text = "" : importir.Text = ""
        lblimportir.Text = "" : amtimportir.Text = "0.00"
        importirnote.Text = ""
    End Sub

    Private Sub createtblGVvoucher()
        Dim dtlDV As DataSet = New DataSet
        Dim dtlTable As DataTable = New DataTable("TabelVoucherDetail")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("voucherseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("trnbelino", Type.GetType("System.String"))
        dtlTable.Columns.Add("voucherno", Type.GetType("System.String"))
        dtlTable.Columns.Add("vouchercode", Type.GetType("System.String"))
        dtlTable.Columns.Add("voucherdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("amtvoucher", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("voucherdate", Type.GetType("System.String"))
        dtlTable.Columns.Add("vouchernote", Type.GetType("System.String"))
        dtlTable.Columns.Add("currencyoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("currencyrate", Type.GetType("System.Decimal"))
        dtlDV.Tables.Add(dtlTable)
        Session("tblGVvoucher") = dtlTable
    End Sub

    Private Sub ClearDetailvoucher()
        trnbelidtlseqvcr.Text = "1"
        If Session("tblGVvoucher") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("tblGVvoucher")
            trnbelidtlseqvcr.Text = objTable.Rows.Count + 1
            GVvoucher.SelectedIndex = -1
        End If
        I_U4.Text = "New Detail Voucher"
        trnbelidtloid.Text = "" : txtnovcr.Text = ""
        descVoucher.Text = "" : amtVoucher.Text = "0.00"
        voucherNote.Text = ""
        datevoucher.Text = Format(GetServerTime(), "dd/MM/yyyy")
        FillDDL(currencyoidvcr, "select currencyoid, currencycode + '-' + currencydesc from QL_mstcurr where cmpcode='" & CompnyCode & "'  and currencydesc<>'NULL'")
        currencyoidvcr_SelectedIndexChanged(Nothing, Nothing)
        gvitemvoucher.Visible = False
    End Sub

    Private Sub bindvoucher()
        sSql = "select itemoid,itemcode,itemdesc from ql_mstitem where itemdesc like '%" & descVoucher.Text & "%' and stockflag= 'V' AND (itemflag<>'' OR itemflag='AKTIF')"
        FillGV(gvitemvoucher, sSql, "QL_VOUCHER")
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(CabangNya, sSql)
                CabangNya.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
            CabangNya.SelectedValue = "10"
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessageprint(ByVal id As Integer, ByVal no As String, ByVal other As String)
        idTemp.Text = id : NoTemp.Text = no
        OtherTemp.Text = other
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub initmattype(ByVal Opt As String)
        Try
            sSql = "SELECT genoid, gendesc FROM ql_mstgen WHERE gengroup LIKE '%ITEMGROUP%' AND cmpcode LIKE '" & CompnyCode & "'"
            FillDDL(DDlmatCatmst, sSql)
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End Try
    End Sub

    Private Sub initCbg()
        sSql = "Select gencode,gendesc from QL_mstgen Where gengroup='CABANG'"
        FillDDL(toCabang, sSql)
    End Sub

    Private Sub initUnit()
        Try
            'Dim sSql As String
            sSql = "select g.genoid, g.gendesc from ql_mstgen g inner join ql_mstITEM m on g.genoid=m.satuan1 Where m.itemoid = '" & matoid.Text & "' and m.ITEMDESC = '" & Tchar(txtMaterial.Text) & "' group by g.genoid,g.gendesc "
            FillDDL(ddlUnit, sSql)
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Public Sub InitAllDDL()
        Dim sGroup() As String = {"PAYTYPE"}
        Dim oDDL() As DropDownList = {TRNPAYTYPE}
        FillDDLGen(sGroup, oDDL)

        FillDDL(currencyoid, "select currencyoid, currencycode + '-' + currencydesc from QL_mstcurr where cmpcode='" & CompnyCode & "'  and currencydesc<>'NULL'")
        FillDDL(currencyoidvcr, "select currencyoid, currencycode + '-' + currencydesc from QL_mstcurr where cmpcode='" & CompnyCode & "'  and currencydesc<>'NULL'")

        FillDDL(DDlmatCatmst, "select genoid, gendesc, genother1  from QL_mstgen where cmpcode='" & CompnyCode & "'  and gengroup='ITEMGROUP'")
        DDlmatCatmst.Items.Add("SEMUA GROUP")
        DDlmatCatmst.SelectedValue = "SEMUA GROUP"

        FillDDL(Catgenerate, "select genoid, gendesc, genother1  from QL_mstgen where cmpcode='" & CompnyCode & "'  and gengroup='ITEMGROUP'")
        Catgenerate.Items.Add("SEMUA GROUP")
        Catgenerate.SelectedValue = "SEMUA GROUP"

        FillDDL(Ekspedisi, "select genoid, gendesc, genother1  from QL_mstgen where cmpcode='" & CompnyCode & "'  and gengroup='EKSPEDISI'  order by gendesc")

        FillDDL(DDLPrefix, "select genoid, gendesc from ql_mstgen where gengroup='PREFIXSUPP' and cmpcode='" & CompnyCode & "'")

        FillDDL(matunit, "SELECT genoid, gendesc FROM ql_mstgen WHERE gengroup LIKE ('%ITEMUNIT%') AND cmpcode LIKE '%" & CompnyCode & "%'")

        FillDDL(ddlprinter, "select gencode ,gendesc from ql_mstgen where gengroup like 'SetupPrinter'")
        ddlprinter.Items.Add(New ListItem("Print To Excel(Data Only)", "EXCEL"))
        ddlprinter.Items.Add(New ListItem("Print To PDF", "PDF"))
        ddlprinter.Items.Add(New ListItem("Print Without Note", "Without"))
        If ToDouble(Session("printapproval")) > 0 Then
            ddlprinter.Items.Add(New ListItem("Print To Excel(Nota)", "notaexcel"))
        End If
        ddlprinter.SelectedValue = "PDF"
        btnCancelMstr.Enabled = True
        If TRNPAYTYPE.Items.Count = 0 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "please create/fill Master Gen in group PAYTYPE!"))
            btnsavemstr.Enabled = False
            btnDeleteMstr.Enabled = False
            btnCancelMstr.Enabled = False
        ElseIf trnbelidisctype.Items.Count = 0 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "please create/fill Master Gen in group DISCTYPE!"))
            btnsavemstr.Enabled = False
            btnDeleteMstr.Enabled = False
            btnCancelMstr.Enabled = False

        ElseIf currencyoid.Items.Count = 0 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "please create/fill Master Currency & history!"))
            btnsavemstr.Enabled = False
            btnDeleteMstr.Enabled = False
            btnCancelMstr.Enabled = False
        Else
            FillCurrencyRate(currencyoid.SelectedItem.Value)
        End If
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        'sSql = "select top 1 isnull(curratestoIDRbeli,1) from QL_mstcurrhist where cmpcode='" & CompnyCode & "' and curroid=" & iOid & " order by currdate desc"
        sSql = "select 1"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyRate.Text = ToMaskEdit(xCmd.ExecuteScalar, 4)
        conn.Close()
    End Sub

    Public Sub BindData(ByVal filterCompCode As String)
        Dim sWhere As String = ""
        Dim userID As String = Session("UserID")

        sSql = "SELECT a.trnbelimstoid pomstoid, a.trnbelipono pono, a.trnbelipodate podate, a.trnbelistatus postatus, s.suppname supplier, case when a.statusPOBackorder = 'N' then 'PO' else 'Back Order' end as potype, a.trnbelistatus postatus, a.branch_code FROM QL_pomst a INNER JOIN QL_mstsupp s ON a.trnsuppoid =s.suppoid Where (" & ddlFilter.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%') And TRNBELItype = 'GROSIR'"

        If chkPeriod.Checked = True Then
            sSql &= " AND a.trnbelipodate BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & "' AND '" & CDate(toDate(FilterPeriod2.Text)) & "'"
        End If

        If chkStatus.Checked Then
            If DDLStatus.SelectedIndex <> 0 Then
                sSql &= " AND trnbelistatus = '" & DDLStatus.SelectedValue & "' "
            End If
        End If

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND a.upduser='" & Session("UserID") & "'"
        End If

        'If checkPagePermission("Transaction/ft_trnpo.aspx", Session("SpecialAccess")) = False Then
        '    If Session("UserLevel") = 4 Or 0 Then
        '        sSql &= " AND a.upduser='" & Session("UserID") & "'"
        '    End If
        'End If

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND a.branch_code='" & dCabangNya.SelectedValue & "'"
        End If
        sSql &= " Order By a.trnbelimstoid dESC"
        FillGV(tbldata, sSql, "QL_pomst")
    End Sub

    Private Sub EnableFill()
        If posting.Text = "Revisi" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False : imbApproval.Visible = False
            btnAddToList.Visible = False : btnFromPR.Visible = False
            btnPrint.Visible = False
        ElseIf posting.Text = "Approved" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False : imbApproval.Visible = False
            btnAddToList.Visible = False : btnFromPR.Visible = False
            btnPrint.Visible = True
        ElseIf posting.Text = "In Approval" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False : imbApproval.Visible = False
            btnAddToList.Visible = False : btnFromPR.Visible = False
            btnPrint.Visible = False
        ElseIf posting.Text = "Closed" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False : imbApproval.Visible = False
            btnAddToList.Visible = True : btnFromPR.Visible = False
            btnPrint.Visible = False
        ElseIf posting.Text = "CLOSED MANUAL" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False : imbApproval.Visible = False
            btnAddToList.Visible = True : btnFromPR.Visible = False
            btnPrint.Visible = False
        ElseIf posting.Text = "Rejected" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False : imbApproval.Visible = False
            btnAddToList.Visible = True : btnFromPR.Visible = False
            btnPrint.Visible = False
        Else
            btnsavemstr.Visible = True : btnDeleteMstr.Visible = True : btnPrint.Visible = False
            btnAddToList.Visible = True : btnFromPR.Visible = False
            imbApproval.Visible = True
        End If

        If posting.Text = "Rejected" Then
            btnSearchmat.Visible = False
            btnClearmat.Visible = False : trnbelidtlqty.Enabled = False : ddlUnit.Enabled = False
            trnbelidtlqty.CssClass = "inpTextDisabled"
            ddlUnit.CssClass = "inpTextDisabled"
        Else
            btnSearchmat.Visible = True
            btnClearmat.Visible = True : trnbelidtlqty.Enabled = True : ddlUnit.Enabled = True
            trnbelidtlqty.CssClass = "inpText"
            ddlUnit.CssClass = "inpText"
        End If
    End Sub

    Private Sub FillTextBox(ByVal iID As Int64)
        sSql = "SELECT a.branch_code,a.cmpcode, a.trnbelimstoid, a.periodacctg, a.trnbelitype, a.TRNBELIdeliverydate, a.trnbelipodate, a.trnbelipono, a.trnsuppoid, a.trnpaytype, a.amtdischdr , a.trnbelidisctype, a.trnbelinote, a.trnbelistatus, a.trntaxpct, a.upduser, a.updtime, a.amtdiscdtl, a.amtdischdr, a.amtbeli, a.amttax, a.amtbelinetto, s.suppname supplier, finalapprovalcode, finalapprovaluser, finalapprovaldatetime,a.curroid,a.currate, d.prdtloid, isnull(a.digit,0) as digit , a.expeoid,A.statusPOBackorder,a.trnbelidisc,a.trnbelidisc,a.flagvcr,a.market, a.totalvoucher, a.PrefixOid,a.tocabang,a.typepo, flagSR FROM QL_pomst a inner join QL_mstsupp s on a.trnsuppoid=s.suppoid inner join QL_podtl d on d.cmpcode = a.cmpcode and d.trnbelimstoid = a.trnbelimstoid Where a.cmpcode ='" & CompnyCode & "' AND a.trnbelimstoid ='" & iID & "' AND A.TRNBELItype = 'GROSIR' "

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                initCbg()
                CabangNya.SelectedValue = xreader.Item("branch_code")
                oid.Text = xreader.Item("trnbelimstoid")
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                DDlmatCatmst.SelectedValue = Trim(xreader.GetValue(3).ToString)
                podeliverydate.Text = Format(CDate(xreader.Item("TRNBELIdeliverydate").ToString), "dd/MM/yyyy")
                trnbelipodate.Text = Format(CDate(xreader.Item("trnbelipodate").ToString), "dd/MM/yyyy")
                trnbelipono.Text = Trim(xreader.Item("trnbelipono"))
                trnbeliporef.Text = Trim(xreader.GetValue(7))
                trnsuppoid.Text = Trim(xreader.Item("trnsuppoid"))
                TRNPAYTYPE.Text = Trim(xreader.Item("trnpaytype"))
                trnbelidisc.Text = ToMaskEdit(Trim(xreader.Item("trnbelidisc")), 3)
                trnbelidisctype.SelectedValue = Trim(xreader.Item("trnbelidisctype"))
                trnbelinote.Text = Trim(xreader.Item("trnbelinote").ToString)
                posting.Text = xreader.Item("trnbelistatus").ToString
                trntaxpct.Text = ToMaskEdit(Trim(xreader.Item(("trntaxpct"))), 3)
                digit.SelectedValue = Trim(xreader.Item("digit"))
                market.SelectedValue = Trim(xreader.Item("market"))
                Taxable.Checked = ToDouble(trntaxpct.Text) > 0
                SOBO.Checked = Trim(xreader.Item("statusPOBackorder")) = "T"
                Upduser.Text = Trim(xreader.Item("upduser").ToString)
                Updtime.Text = Trim(xreader.Item("updtime"))
                amtdiscdtl.Text = ToMaskEdit(Trim(xreader.Item("amtdiscdtl")), 3)
                amtdischdr.Text = ToMaskEdit(Trim(xreader.Item("amtdischdr")), 3)
                amtbeli.Text = ToMaskEdit(Trim(xreader.Item("amtbeli")), 3)
                amttax.Text = ToMaskEdit(Trim(xreader.Item(("amttax"))), 3)
                amtbelinetto1.Text = ToMaskEdit(Trim(xreader.Item(("amtbelinetto"))), 3)
                suppliername.Text = Trim(xreader.Item("supplier"))
                approvalcode.Text = Trim(xreader.Item("finalapprovalcode"))
                approvaluserid.Text = Trim(xreader.Item(("finalapprovaluser")))
                approvalusername.Text = cKon.ambilscalar("SELECT username FROM ql_mstprof WHERE userid = '" & approvaluserid.Text & "'")
                ApprovalDate.Text = Format(CDate(Trim(xreader.Item(("finalapprovaldatetime")))), "dd/MM/yyyy")
                prdtloid.Text = Trim(xreader.Item(("prdtloid")))
                Ekspedisi.SelectedValue = xreader("expeoid")
                currencyoid.SelectedValue = xreader("curroid")
                currencyRate.Text = ToMaskEdit(ToDouble(xreader("currate").ToString), 3)
                tot.Text = ToMaskEdit(amtbeli.Text - amtdiscdtl.Text, 3)
                lblchkvcr.Text = Trim(xreader.Item("flagvcr"))
                cbSR.Checked = IIf(xreader("flagSR") = "Y", True, False)
                totdtlvoucher.Text = ToMaskEdit(ToDouble(Trim(xreader.Item(("totalvoucher")))), 3)
                totvoucher.Text = ToMaskEdit(ToDouble(Trim(xreader.Item(("totalvoucher")))), 3)
                temptotvcr.Text = 0
                Session("TotalVoucher") = xreader.Item(("totalvoucher"))
                Session("temptotvoucher") = 0
                DDLPrefix.SelectedValue = xreader.Item(("PrefixOid"))
                initCbg()
                toCabang.SelectedValue = xreader.Item(("tocabang"))
                TypePOnya.SelectedValue = xreader.Item("typepo").ToString
                If lblchkvcr.Text = "1" Then
                    LinkButton8.Visible = True : LinkButton10.Visible = True
                    chkvcr.Checked = True : tabindex21true()
                Else
                    LinkButton8.Visible = False : LinkButton10.Visible = False
                    chkvcr.Checked = False : tabindex21false()
                End If
            End While
        End If
        xreader.Close()
        conn.Close()

        If posting.Text = "Closed" Or posting.Text = "CLOSED MANUAL" Then
            sSql = "SELECT d.cmpcode, d.trnbelidtloid podtloid, d.trnbelimstoid pomstoid, d.trnbelidtlseq podtlseq, d.ITEMoid matoid,d.tocabang, d.trnbelidtlqty podtlqty, (select gendesc from ql_mstgen where genoid = d.trnbelidtlunit) as podtlunit, d.trnbelidtlprice podtlprice, d.trnbelidtldisc podtldisc, d.trnbelidtldisctype podtldisctype, d.trnbelidtlnote pobelidtlnote, 'In Process' pobelidtlstatus, d.updtime, d.upduser, d.amtdisc DiscAmt, d.amtbelinetto poamtdtlnetto, (select itemdesc from QL_mstitem where itemoid = d.itemoid) as material, (select merk from QL_mstitem where itemoid = d.itemoid) as merk, d.trnbelidtlunit as unit, 0 as Subcat,(select itemgroupoid from QL_mstitem where itemoid = d.itemoid ) as matcategoryoid, isnull(d.berat,0.00) berat, d.prdtloid ,d.trnbelidtldisc2 podtldisc2, d.trnbelidtldisctype2 podtldisctype2, d.amtdisc2 DiscAmt2, d.amtdisc + d.amtdisc2 poamtdtldisc, unitseq, ekspedisi, Isnull(typeberat,2) typeberat, isnull(hargaekspedisi,0.00) hargaekspedisi, bonus, bonusAmt, (Select Case m.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End From QL_mstitem m Where m.itemoid=d.itemoid) flagBarang FROM QL_podtl d inner join QL_mstgen g2 on g2.genoid=d.trnbelidtlunit Where d.trnbelimstoid=" & iID & " order by d.trnbelidtlseq"
        Else
            sSql = "SELECT d.cmpcode, d.trnbelidtloid podtloid, d.trnbelimstoid pomstoid, d.trnbelidtlseq podtlseq, d.ITEMoid matoid, d.trnbelidtlqty podtlqty, (select gendesc from ql_mstgen where genoid = d.trnbelidtlunit) as podtlunit, d.trnbelidtlprice podtlprice, d.trnbelidtldisc podtldisc, d.trnbelidtldisctype podtldisctype, d.trnbelidtlnote pobelidtlnote, d.trnbelidtlstatus pobelidtlstatus, d.updtime, d.upduser, d.amtdisc DiscAmt, d.amtbelinetto poamtdtlnetto, (select itemdesc from QL_mstitem where itemoid = d.itemoid) as material, (select merk from QL_mstitem where itemoid = d.itemoid) as merk, d.trnbelidtlunit as unit, 0 as Subcat,(select itemgroupoid  from QL_mstitem where itemoid = d.itemoid ) as matcategoryoid,isnull(d.berat,0.00) berat, d.prdtloid ,d.trnbelidtldisc2 podtldisc2, d.trnbelidtldisctype2 podtldisctype2, d.amtdisc2 DiscAmt2,d.amtdisc + d.amtdisc2 poamtdtldisc,unitseq, ekspedisi, Isnull(typeberat,2) typeberat, isnull(hargaekspedisi,0.00) hargaekspedisi, bonus, bonusAmt, d.tocabang, (Select Case m.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End From QL_mstitem m Where m.itemoid=d.itemoid) flagBarang FROM QL_podtl d inner join QL_mstgen g2 on g2.genoid=d.trnbelidtlunit Where d.trnbelimstoid=" & iID & " order by d.trnbelidtlseq"
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        tbldtl.DataSource = objDs.Tables("data")
        Session("TblDtl") = objDs.Tables("data")
        Session("TabelDtl") = objDs.Tables("data")
        tbldtl.DataBind()

        sSql = "select * from ql_voucherdtl where trnbelimstoid = " & iID & ""
        Session("tblGVvoucher") = cKon.ambiltabel(sSql, "tblvoucher")
        GVvoucher.DataSource = Session("tblGVvoucher")
        GVvoucher.DataBind()

        datevoucher.Text = Format(GetServerTime(), "dd/MM/yyyy")
        FillDDL(currencyoidvcr, "select currencyoid, currencycode + '-' + currencydesc from QL_mstcurr where cmpcode='" & CompnyCode & "' and currencydesc<>'NULL'")
        currencyoidvcr_SelectedIndexChanged(Nothing, Nothing)
        fillvoucher() : EnableFill()
        trnbelidtlseq.Text = objDs.Tables("data").Rows.Count + 1
        I_u2.Text = "New Detail"
        conn.Close()
    End Sub

    Private Sub generateNoPO() 
        trnbelipono.Text = GenerateID("ql_pomst", CompnyCode)
        voucheroid.Text = GenerateID("VOUCHER", CompnyCode)
    End Sub

    Public Sub binddataSupp(ByVal sqlPlus As String)
        sSql = "Select suppoid,oldsuppcode,suppaddr,suppcode,suppname,paymenttermoid,prefixsupp From QL_mstsupp Where supptype = 'GROSIR' and cmpcode='" & CompnyCode & "' " & sqlPlus & " And suppflag='Active' ORDER BY suppcode ASC"
        FillGV(gvSupplier, sSql, "QL_mstsupp")
    End Sub

    Private Sub BinddataItem(ByVal matcat As String, ByVal sqlPlus As String)
        Dim uFilter As String = ""
        Try

            If TypePOnya.SelectedValue = "Selisih" Then
                uFilter &= " And con.branch_code='" & CabangNya.SelectedValue & "' And mtrlocoid NOT IN (Select gl.genoid From ql_mstgen gl Where genother1 IN (1010,0) AND gl.genoid=con.mtrlocoid AND g1.gengroup='LOCATION')"
                gvMaterial.Columns(6).Visible = False
            End If
            sSql = "Select * From (Select m.Itemoid matoid, m.itemcode matcode, m.ITEMDESC matlongdesc, m.satuan1 as matunit1oid, (cast(cast(m.ITEMsafetystockUNIT1 as decimal) as varchar) + ' ' + m.satuan1) as safetystok, m.satuan1,m.satuan1 satuan2,m.satuan1 satuan3,g1.gendesc unit1, g1.gendesc unit, g1.gendesc unit3, m.lastPricebuyUnit1, m.lastPricebuyUnit2 ,m.lastPricebuyUnit3, m.merk, isnull(m.beratbarang,0.00) beratbarang, isnull(m.beratvolume,0.00) beratvolume, isnull((select top 1 podtl.trnbelidtlprice from QL_podtl podtl Inner Join QL_pomst pomst On pomst.trnbelimstoid=podtl.trnbelimstoid Where podtl.trnbelimstoid=pomst.trnbelimstoid And podtl.itemoid=m.itemoid and pomst.trnbelistatus='Approved' Order by pomst.updtime DESC),0) as lastprice, isnull((Select top 1 mstsupp.suppname from QL_mstsupp mstsupp Inner Join  QL_pomst pomst on pomst.trnsuppoid=mstsupp.suppoid Inner Join QL_podtl podtl on podtl.trnbelimstoid=pomst.trnbelimstoid Where mstsupp.suppoid=pomst.trnsuppoid and pomst.trnbelimstoid=podtl.trnbelimstoid and podtl.itemoid=m.ITEMoid and pomst.trnbelistatus='Approved'),'') as suppname, isnull((Select top 1 pomst.trnbelipono from QL_pomst pomst INNER JOIN QL_podtl podtl on podtl.trnbelimstoid=pomst.trnbelimstoid Where pomst.trnbelimstoid=podtl.trnbelimstoid and podtl.ITEMoid=m.ITEMoid and pomst.trnbelistatus='Approved'),'') as pono,ISNULL((Select SUM(qtyIn)-SUM(qtyOut) From ql_conmtr con Where con.refoid=m.itemoid And con.periodacctg='" & Format(GetServerTime(), "yyyy-MM") & "' " & uFilter & "),0.00) saldoakhir, m.stockflag, Case m.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End flagBarang From ql_mstitem m inner join QL_mstgen g1 on g1.genoid = m.satuan1 WHERE m.cmpcode = 'MSC'" & sqlPlus & " And m.itemflag = 'Aktif' and m.stockflag <> 'ASSET') item Where flagbarang LIKE '%" & Tchar(DDLstockflag.SelectedItem.Text) & "%' Order by matlongdesc"

            FillGV(gvMaterial, sSql, "ql_mstitem")
        Catch ex As Exception
            showMessage(ex.Message, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            DDlmatCatmst.SelectedIndex = 0 : cProc.DisposeGridView(gvMaterial)
        End Try

    End Sub

    Private Sub binddatasparepart(ByVal matcat As String)

        Try
            Dim query = "SELECT m.partoid matoid,m.partcode matcode,m.partdesclong matlongdesc, m.partunit as matunit1oid, " & _
            " (cast(cast(m.partsafestockqty as decimal) as varchar)    )  " & _
            " as safetystok, isnull((select top 1 podtl.podtlprice from QL_podtl podtl,   QL_pomst pomst " & _
            " where(podtl.pomstoid = pomst.pomstoid And podtl.itemoid = m.partoid)" & _
            " and podtl.pobelidtlstatus='COMPLETED'),0) as lastprice, " & _
            " isnull((select top 1 pomst.trnbelipono from QL_pomst pomst, QL_podtl podtl " & _
            " where pomst.pomstoid=podtl.pomstoid and podtl.itemoid=m.partoid and podtl.pobelidtlstatus='COMPLETED'),0) " & _
            " as pono, isnull((select top 1 mstsupp.suppname from QL_mstsupp mstsupp, QL_pomst pomst, QL_podtl podtl " & _
            " where(mstsupp.suppoid = pomst.suppoid And pomst.pomstoid = podtl.pomstoid And podtl.itemoid = m.partoid)" & _
            " and podtl.pobelidtlstatus='COMPLETED' ),0) as suppname from QL_MSTSPAREPART  m WHERE " & _
            " m.partcat='" & matcat & "'  ORDER BY m.partcode"

            ClassFunction.FillGV(gvMaterial, query, "QL_mstITEM")
        Catch ex As Exception
            showMessage("Tidak ada data pada Category ini !! ", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")

            DDlmatCatmst.SelectedIndex = 0
            cProc.DisposeGridView(gvMaterial)
        End Try
    End Sub

    Private Sub ClearDetail()
        trnbelidtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            trnbelidtlseq.Text = objTable.Rows.Count + 1
            tbldtl.SelectedIndex = -1
            'MultiView1.ActiveViewIndex = 2
        End If
        '  mtrloc.SelectedIndex = 0
        I_u2.Text = "New Detail"
        trnbelidtloid.Text = ""
        matoid.Text = "" : trnbelidtlqty.Text = "0.00"
        trnbelidtlunit.Text = "" : trnbelidtlprice.Text = "0.00"
        costEkspedisi.Text = "0.00" : trnbelidtldisc.Text = "0.00"
        trnbelidtldisctype.SelectedIndex = 0
        trnbelidtldisc2.Text = "0.00"
        trnbelidtldisctype2.SelectedIndex = 0
        trnbelidtlnote.Text = ""
        amtdisc.Text = "0.00" : amtdisc2.Text = "0.00"
        totamtdisc.Text = "0.00" : hasildisc1.Text = "0.00"
        satuan1.Text = "" : satuan2.Text = ""
        satuan3.Text = "" : amtbelinetto2.Text = "0.00" : txtMaterial.Text = ""
        prdtloid.Text = 0
        Session("TabelAddDtlPr") = Nothing
        labelcurrentstock.Visible = False : labelcurrentunit.Visible = False
        labeljual1.Visible = False : labeljual2.Visible = False
        labeljual3.Visible = False : labelretur.Visible = False
        labelqty1.Visible = False : labelqty2.Visible = False
        labelqty3.Visible = False : labelqtyretur.Visible = False
        pricebuy1.Visible = False : pricebuy2.Visible = False
        pricebuy3.Visible = False : lblunit1.Visible = False
        lblunit2.Visible = False : lblunit3.Visible = False

        trbulan.Visible = False : trqty.Visible = False
        merk.Text = "" : GVdetailSupp.Dispose()
        GVdetailSupp.Visible = False
        Konversike3.Text = "" : PriceListInfo.Text = ""

        DDlmatCatmst.SelectedValue = "SEMUA GROUP"

        bonus.Checked = False
        lblbonus.Text = "No" : beratVolume.Text = ""
        beratBarang.Text = ""
        cb1.Checked = False : cb2.Checked = False
    End Sub

    Private Sub ReAmount()
        If trnbelidisc.Text.Trim = "" Then
            trnbelidisc.Text = "0.00"
        End If

        If trntaxpct.Text.Trim = "" Then
            trntaxpct.Text = "0.00"
        End If

        If amttax.Text.Trim = "" Then
            amttax.Text = "0.00"
        End If

        If amtbeli.Text.Trim = "" Then
            amtbeli.Text = "0.00"
        End If

        If amtdischdr.Text.Trim = "" Then
            amtdischdr.Text = "0.00"
        End If

        If amtdiscdtl.Text.Trim = "" Then
            amtdiscdtl.Text = "0.00"
        End If

        If trnbelidisctype.SelectedValue = "AMT" Then 'amount
            amtdischdr.Text = ToMaskEdit(CDbl(ToDouble(trnbelidisc.Text)), 4)
        Else
            If ToDouble(trnbelidisc.Text) > 100 Then
                showMessage("Discount Persent tidak bisa lebih dari 100 !!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                trnbelidisc.Text = "0.00"
            End If
            amtdischdr.Text = ToMaskEdit(CDbl(((ToDouble(trnbelidisc.Text) / 100) * (ToDouble(amtbeli.Text) - ToDouble(amtdiscdtl.Text)))), 3)
        End If

        If amtdischdr.Text.Trim = "" Then
            amtdischdr.Text = "0.00"
        End If

        Dim tempnilai As Double = ToDouble(amtbeli.Text) - ToDouble(amtdiscdtl.Text) - ToDouble(amtdischdr.Text)

        amttax.Text = ToMaskEdit(CDbl(((trntaxpct.Text / 100) * ToDouble(tempnilai))), 3)
        If amttax.Text.Trim = "" Then
            amttax.Text = "0.00"
        End If

        amtbelinetto1.Text = ToMaskEdit(CDbl(ToDouble(tempnilai) + ToDouble(amttax.Text)), 3)
        tot.Text = ToMaskEdit(amtbelinetto1.Text, 3)
        If amtbelinetto1.Text.Trim = "" Then
            amtbelinetto1.Text = "0.00"
        End If
    End Sub

    Private Sub ReAmountDetail()
        If trnbelidtlqty.Text.Trim = "" Then
            trnbelidtlqty.Text = "0.00"
        End If

        If trnbelidtlprice.Text.Trim = "" Then
            trnbelidtlprice.Text = "0.00"
        End If

        If trnbelidtldisc.Text.Trim = "" Then
            trnbelidtldisc.Text = "0.0"
        End If

        If hasildisc1.Text = "" Then
            hasildisc1.Text = "0.00"
        End If

        Dim qty As Double = ToDouble(trnbelidtlqty.Text)

        If trnbelidtldisctype.SelectedValue = "AMT" Or trnbelidtldisctype.SelectedValue = "VOUCHER" Then 'amount
            amtdisc.Text = ToMaskEdit(CDbl(qty * ToDouble(trnbelidtldisc.Text)), 3)
        Else
            If ToDouble(trnbelidtldisc.Text) > 100 Then
                showMessage("Discount Persent tidak bisa lebih dari 100 !!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                trnbelidtldisc.Text = "0.00"
            End If
            amtdisc.Text = ToMaskEdit(CDbl((ToDouble(trnbelidtldisc.Text) / 100) * (qty * ToDouble(trnbelidtlprice.Text))), 3)
        End If

        hasildisc1.Text = ToMaskEdit((qty * ToDouble(trnbelidtlprice.Text)) - amtdisc.Text, 3)

        If amtdisc.Text.Trim = "" Then
            amtdisc.Text = "0.00"
        End If

        amtbelinetto2.Text = ToMaskEdit(CDbl((ToDouble(trnbelidtlprice.Text) * qty) - ToDouble(amtdisc.Text)), 3)
        If amtbelinetto2.Text.Trim = "" Then
            amtbelinetto2.Text = "0.00"
        End If

        totamtdisc.Text = ToMaskEdit((qty * ToDouble(trnbelidtlprice.Text) - amtbelinetto2.Text), 3)
        If totamtdisc.Text.Trim = "" Then
            totamtdisc.Text = "0.0"
        End If
    End Sub

    Private Sub ReAmountDetail2()
        If trnbelidtlqty.Text.Trim = "" Then
            trnbelidtlqty.Text = "0.00"
        End If

        If trnbelidtlprice.Text.Trim = "" Then
            trnbelidtlprice.Text = "0.00"
        End If

        If trnbelidtldisc2.Text.Trim = "" Then
            trnbelidtldisc2.Text = "0.00"
        End If

        If hasildisc1.Text = "" Then
            hasildisc1.Text = "0.00"
        End If

        Dim qty As Double = ToDouble(trnbelidtlqty.Text)
        If trnbelidtldisctype2.SelectedValue = "AMT" Then 'amount
            amtdisc2.Text = ToMaskEdit(CDbl(qty * ToDouble(trnbelidtldisc2.Text)), 3)
        ElseIf trnbelidtldisctype2.SelectedValue = "VCR" Then
            amtdisc2.Text = ToMaskEdit(CDbl(ToDouble(trnbelidtldisc2.Text)), 3)
        Else
            If ToDouble(trnbelidtldisc2.Text) > 100 Then
                showMessage("Discount Persent tidak bisa lebih dari 100 !!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                trnbelidtldisc2.Text = "0.00"
            End If
            amtdisc2.Text = ToMaskEdit(CDbl((ToDouble(trnbelidtldisc2.Text) / 100) * (hasildisc1.Text)), 3)
        End If
        If amtdisc2.Text.Trim = "" Then
            amtdisc2.Text = "0.00"
        End If
        amtbelinetto2.Text = ToMaskEdit(CDbl((ToDouble(trnbelidtlprice.Text) * qty) - ToDouble(amtdisc.Text) - ToDouble(amtdisc2.Text)), 3)
        If amtbelinetto2.Text.Trim = "" Then
            amtbelinetto2.Text = "0.00"
        End If
        totamtdisc.Text = ToMaskEdit((qty * ToDouble(trnbelidtlprice.Text) - amtbelinetto2.Text), 3)
        If totamtdisc.Text.Trim = "" Then
            totamtdisc.Text = "0.00"
        End If
    End Sub

    Private Sub CreateTableTBLDTL()
        Dim dtlDS As DataSet = New DataSet
        Dim dtlTable As DataTable = New DataTable("TabelpodtlDetail")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("podtloid", Type.GetType("System.String"))
        dtlTable.Columns.Add("pomstoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("podtlseq", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("podtlqty", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("podtlunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("podtlprice", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("Total", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("DiscAmt", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("podtldisc", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("podtldisctype", Type.GetType("System.String"))
        dtlTable.Columns.Add("DiscAmt2", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("podtldisc2", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("podtldisctype2", Type.GetType("System.String"))
        dtlTable.Columns.Add("totDiscAmt", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("ekspedisi", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("hargaEkspedisi", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("typeBerat", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("berat", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("unitseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("pobelidtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("pobelidtlstatus", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.String"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("poamtdtldisc", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("poamtdtlnetto", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("material", Type.GetType("System.String"))
        dtlTable.Columns.Add("merk", Type.GetType("System.String"))
        dtlTable.Columns.Add("Unit", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("Subcat", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcategoryoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bonus", Type.GetType("System.String"))
        dtlTable.Columns.Add("bonusAmt", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("flagBarang", Type.GetType("System.String"))
        dtlDS.Tables.Add(dtlTable)
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub CreateTableAddPR()
        Dim dtlDS As DataSet = New DataSet
        Dim dtlTable As DataTable = New DataTable("TabelpoprdtlDetail")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("prdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("prno", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("prqty", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dtlDS.Tables.Add(dtlTable)
        Session("TabelAddDtlPr") = dtlTable
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If 

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\ft_trnpo.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Purchase Order"
        Session("oid") = Request.QueryString("oid")
        btnDeleteMstr.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        imbApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Send this data for Approval?');")
        btnPrint.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Print this data?');")
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        sSql = "Select COUNT(*) From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='PRINT EXCEL NOTA PURCHASE' and branch_code Like '%" & Session("branch_id") & "%' AND approvaluser='" & Session("UserID") & "'"
        Session("printapproval") = GetScalar(sSql)
        If Session("branch_id") <> "10" Then
            TypePOnya.SelectedValue = "Selisih"
            TypePOnya.Enabled = False
            toCabang.Enabled = False
        End If

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            datevoucher.Text = Format(GetServerTime(), "dd/MM/yyyy")
            lblbonus.Text = "No" : fDDLBranch()
            BindData(CompnyCode)
            InitDDLBranch() : InitAllDDL() : initCbg()
            CabangNya_SelectedIndexChanged(Nothing, Nothing)
            TypePOnya_SelectedIndexChanged(Nothing, Nothing)

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid")) : i_u.Text = "UPDATE"
                TabContainer1.ActiveTabIndex = 1
            Else
                i_u.Text = "N E W" : generateNoPO()
                posting.Text = "In Process"
                trnbelipodate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                podeliverydate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                datevoucher.Text = Format(GetServerTime(), "dd/MM/yyyy")
                periodacctg.Text = Format(GetServerTime(), "yyyy-MM")
                currencyoidvcr_SelectedIndexChanged(Nothing, Nothing)
                ApprovalDate.Text = Format(#1/1/1900#, "dd/MM/yyyy")
                btnDeleteMstr.Visible = False : Label29.Visible = False
                TabContainer1.ActiveTabIndex = 0
                btnPrint.Visible = False : matunit.Visible = False
                currencyoid_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
  
        Dim objTable As New DataTable : objTable = Session("TblDtl") ': MsgBox(objTable.Rows.Count)
        tbldtl.DataSource = objTable : tbldtl.DataBind() : tbldtl.Visible = True
        Dim objTablevcr As New DataTable : objTablevcr = Session("TblGVvoucher") ': MsgBox(objTable.Rows.Count)
        GVvoucher.DataSource = objTablevcr : GVvoucher.DataBind() : GVvoucher.Visible = True
        'fillTot() ': ReAmount()
    End Sub

    Protected Sub TblData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles tbldata.PageIndexChanging
        tbldata.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If chkPeriod.Checked Then
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text))
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text))
                If dat1 < CDate("01/01/1900") Or dat2 < CDate("01/01/1900") Then
                    showMessage("Format tanggal salah!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                End If
                If dat1 > dat2 Then
                    showMessage("Tanggal Awal harus lebih kecil daripada Tanggal Akhir!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                End If
            Catch ex As Exception
                showMessage("Format tanggal salah!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End Try
        End If
        BindData(CompnyCode)
        'FilterText.Text = str
    End Sub

    Protected Sub TblDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles tbldtl.PageIndexChanging
        tbldtl.PageIndex = e.NewPageIndex
        tbldtl.Visible = True
        tbldtl.DataSource = Session("TblDtl")
        tbldtl.DataBind()
    End Sub

    Protected Sub tbldtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldtl.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
                e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
                e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
                e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
                e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
                e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
                e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
                e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 3)
                e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 3)
                e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 3)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
    End Sub

    Protected Sub TblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow

        Dim podtlseq As Integer = objTable.Rows(e.RowIndex).Item("podtlseq")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.RemoveAt(e.RowIndex)

        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            podtlseq = objTable.Rows(C1).Item("podtlseq")

            dr.BeginEdit()
            dr(3) = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        Session("TabelDtl") = objTable
        Session("ItemLine") = Session("ItemLine") - 1
        trnbelidtlseq.Text = Session("ItemLine")
        tbldtl.DataSource = Session("TabelDtl")
        tbldtl.DataBind() : fillvoucher()
        fillTotalOrder() : ReAmount()
    End Sub

    Protected Sub btnFromPR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFromPR.Click
        If matoid.Text = "" Then
            showMessage("Silahkan pilih Material dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        Dim view As String = "view"
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        binddataSupp(" AND (suppname LIKE '%" & Tchar(suppliername.Text) & "%' OR suppcode LIKE '%" & Tchar(suppliername.Text) & "%')")
        gvSupplier.Visible = True
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppliername.Text = "" : trnsuppoid.Text = ""
        If gvSupplier.Visible = True Then
            gvSupplier.DataSource = Nothing
            gvSupplier.DataBind()
            gvSupplier.Visible = False
        End If
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        Dim sPlus As String = " AND (suppname LIKE '%" & Tchar(suppliername.Text) & "%' OR suppcode LIKE '%" & Tchar(suppliername.Text) & "%')"
        binddataSupp(sPlus)
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        'ButtonSuplier.Visible = False : PanelSuplier.Visible = False : ModalPopupExtender1.Hide()
        trnsuppoid.Text = gvSupplier.SelectedDataKey("suppoid").ToString().Trim
        suppliername.Text = gvSupplier.SelectedDataKey("suppname").ToString().Trim
        PrefixOid.Text = gvSupplier.SelectedDataKey("prefixsupp").ToString().Trim
        Try
            TRNPAYTYPE.SelectedValue = gvSupplier.SelectedDataKey("paymenttermoid")
        Catch ex As Exception
        End Try
        cProc.DisposeGridView(sender) : gvSupplier.Visible = False
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearmat.Click
        txtMaterial.Text = "" : matoid.Text = ""
        prdtloid.Text = ""
        Session("TabelAddDtlPr") = Nothing
        gvMaterial.Visible = False
        gvMaterial.DataSource = Nothing
        gvMaterial.DataBind()

        labelcurrentstock.Visible = False
        labelcurrentunit.Visible = False
        labelcurrentstock.Text = "" : labelcurrentunit.Text = ""

        labeljual1.Visible = False : labeljual2.Visible = False
        labeljual3.Visible = False : labelretur.Visible = False
        labeljual1.Text = "" : labeljual2.Text = ""
        labeljual3.Text = "" : labelretur.Text = ""

        labelqty1.Visible = False : labelqty2.Visible = False
        labelqty3.Visible = False : labelqtyretur.Visible = False
        labelqty1.Text = "" : labelqty2.Text = ""
        labelqty3.Text = "" : labelqtyretur.Text = ""

        trbulan.Visible = False : trqty.Visible = False
        merk.Text = "" : PriceListInfo.Text = ""
        Konversike3.Text = ""

        GVdetailSupp.Dispose()
        GVdetailSupp.Visible = False
        trnbelidtlprice.Text = "0.0000"

    End Sub

    Protected Sub gvMaterial_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaterial.PageIndexChanging
        gvMaterial.PageIndex = e.NewPageIndex
        BinddataItem(DDlmatCatmst.SelectedValue, " AND (itemdesc LIKE '%" & Tchar(txtMaterial.Text) & "%' OR itemcode LIKE '%" & Tchar(txtMaterial.Text) & "%')")
    End Sub

    Protected Sub gvMaterial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaterial.SelectedIndexChanged
        txtMaterial.Text = gvMaterial.SelectedDataKey.Item("matlongdesc").ToString.Trim
        matoid.Text = gvMaterial.SelectedDataKey.Item("matoid")
        trnbelidtlunit.Text = gvMaterial.SelectedDataKey.Item("matunit1oid")
        merk.Text = gvMaterial.SelectedDataKey.Item("merk").ToString
        beratBarang.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("beratbarang"), 3)
        beratVolume.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("beratvolume"), 3)
        DDLstockflag.SelectedItem.Text = gvMaterial.SelectedDataKey.Item("flagBarang")

        If TypePOnya.SelectedValue = "Selisih" Then
            trnbelidtlprice.Text = ToMaskEdit(0.0, 3)
        Else
            trnbelidtlprice.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("lastprice"), 3)
        End If

        initUnit() : prdtloid.Text = 0 : QunitSeq()
        cProc.DisposeGridView(sender) : gvMaterial.Visible = False
        Requestinfo() 'bindSuppHistory(matoid.Text, "TRUE")
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchmat.Click
        Try
            If TypePOnya.SelectedValue = "Selisih" Then
                gvMaterial.Columns(6).Visible = False
            Else
                gvMaterial.Columns(6).Visible = True
            End If
            BinddataItem(DDlmatCatmst.SelectedValue, " AND (itemdesc LIKE '%" & Tchar(txtMaterial.Text) & "%' OR itemcode LIKE '%" & Tchar(txtMaterial.Text) & "%' OR merk LIKE '%" & Tchar(txtMaterial.Text) & "%')")
            gvMaterial.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Protected Sub trnbelipodate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelipodate.TextChanged
        Try
            periodacctg.Text = Now.ToUniversalTime
            periodacctg.Text = Format(CDate(toDate(trnbelipodate.Text)), "yyyy-MM")
        Catch ex As Exception
            periodacctg.Text = ""
        End Try
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub trnbelitype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDlmatCatmst.SelectedIndexChanged
        If DDlmatCatmst.SelectedIndex = 1 Then
            matcab.Text = "23"
        ElseIf DDlmatCatmst.SelectedIndex = 0 Then
            matcab.Text = "13"
        ElseIf DDlmatCatmst.SelectedIndex = 2 Then
            matcab.Text = "300"
        Else
            matcab.Text = "840"
        End If
    End Sub  

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click

        If matoid.Text = "" Then
            showMessage("Silahkan Pilih Item dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If ToDouble(trnbelidtlqty.Text) <= 0 Then
            showMessage("Quantity must be > 0 !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If ToDouble(trnbelidtlprice.Text) = 0 Then
            showMessage("Price must be > 0  !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If ToDouble(trnbelidtlprice.Text) < 0 Then
            showMessage("Harga tidak bisa minus !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        'If cb1.Checked <> True And cb2.Checked <> True Then
        '    showMessage("Pilih Berat Barang atau Berat Volume !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        '    Exit Sub
        'End If

        Dim unitseq As Int16 = 1

        If Session("TblDtl") Is Nothing Then
            CreateTableTBLDTL()
        End If

        Dim objTable As DataTable = Session("TblDtl")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        If I_u2.Text = "New Detail" Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "matoid=" & matoid.Text & " AND bonus = '" & lblbonus.Text & "'"
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah di tambahkan sebelumnya, Silahkan cek ulang!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                objTable = Session("TblDtl") : tbldtl.DataSource = objTable
                tbldtl.DataBind() : tbldtl.Visible = True
                Exit Sub
            End If
            dv.RowFilter = ""
        Else
            Dim dv As DataView = objTable.DefaultView

            dv.RowFilter = "matoid=" & matoid.Text & " AND bonus = '" & lblbonus.Text & "' and podtlseq <> " & trnbelidtlseq.Text
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah di tambahkan sebelumnya, Silahkan cek ulang!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                objTable = Session("TblDtl") : tbldtl.DataSource = objTable
                tbldtl.DataBind() : tbldtl.Visible = True
                Exit Sub
            End If
            dv.RowFilter = ""
        End If

        If ToDouble(amtbelinetto2.Text) < 0 Then
            showMessage("Net Purchase Detil amt tidak bisa minus!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        'If Not Session("temptotvoucher") Is Nothing Then
        Dim tampvor As Double = temptotvcr.Text
        If trnbelidtldisc2.Text > tampvor + ToMaskEdit(ToDouble(tamvoucherlama.Text), 3) Then
            showMessage("Sisa Amount Voucher yang bisa di akumulasi adalah Rp." & ToMaskEdit(ToDouble(tampvor), 3) & "   !!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        temptotvcr.Text = ToMaskEdit((ToDouble(tampvor) + ToDouble(tamvoucherlama.Text)), 3) - ToMaskEdit(ToDouble(trnbelidtldisc2.Text), 3)
        temptotvcr.Text = ToMaskEdit(temptotvcr.Text, 3)

        If ToMaskEdit(ToDouble(temptotvcr.Text), 3) < 0 Then
            showMessage("Sisa Amount Voucher yang bisa di akumulasi adalah Rp." & ToMaskEdit(ToDouble(temptotvcr.Text), 3) - trnbelidtldisc2.Text & " !!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If cb1.Checked = True Or cb2.Checked = True Then
            If costEkspedisi.Text = 0 Then
                showMessage("Biaya Ekspedisi Tidak Boleh Nol!!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
        If Not Session("tblGVvoucher") Is Nothing Then
            Dim tbvc As DataTable
            tbvc = Session("tblGVvoucher")
            Dim Ssplitkode(tbvc.Rows.Count - 1) As String
            Dim Ssplitamt(tbvc.Rows.Count - 1) As Double
            For i As Integer = 0 To tbvc.Rows.Count - 1
                Ssplitkode(i) = tbvc.Rows(i).Item("voucherno")
                Ssplitamt(i) = tbvc.Rows(i).Item("amtvoucher")
            Next

            If ToDouble(trnbelidtldisc2.Text) > 0 Then
            End If
        End If

        'insert/update to list data
        Dim objRow As DataRow
        If I_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("podtlseq") = objTable.Rows.Count + 1
        Else 'update
            objRow = objTable.Rows(trnbelidtlseq.Text - 1)
            objRow.BeginEdit()
        End If

        objRow("podtlqty") = ToDouble(trnbelidtlqty.Text)
        objRow("poamtdtlnetto") = ToDouble(amtbelinetto2.Text)
        objRow("cmpcode") = CompnyCode
        objRow("podtloid") = 0
        objRow("pomstoid") = 0
        objRow("matoid") = matoid.Text
        objRow("podtlunit") = Trim(ddlUnit.SelectedItem.Text)
        If lblbonus.Text = "No" Then
            objRow("podtlprice") = ToDouble(trnbelidtlprice.Text)
            objRow("bonusAmt") = ToDouble(0 * trnbelidtlqty.Text)
        Else
            objRow("bonusAmt") = ToDouble(trnbelidtlprice.Text) * trnbelidtlqty.Text
            objRow("podtlprice") = ToDouble(trnbelidtlprice.Text)
            If objRow("podtlprice") = 0 Then
                showMessage("Price Tidak Boleh Kosong!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
        objRow("podtldisc") = ToDouble(trnbelidtldisc.Text)
        objRow("podtldisctype") = trnbelidtldisctype.SelectedValue
        If cb1.Checked = True Or cb2.Checked = True Then
            If cb2.Checked = True Then 'Berat Volume
                If costEkspedisi.Text = 0 Then
                    objRow("ekspedisi") = ToDouble(costEkspedisi.Text)
                    objRow("berat") = beratVolume.Text
                Else
                    objRow("ekspedisi") = ToDouble(costEkspedisi.Text) * (ToDouble(beratVolume.Text) / 1000000) * ToDouble(trnbelidtlqty.Text)
                    objRow("berat") = ToDouble(beratVolume.Text)
                End If
            Else
                'Berat Barang
                If costEkspedisi.Text = 0 Then
                    objRow("ekspedisi") = ToDouble(costEkspedisi.Text)
                    objRow("berat") = ToDouble(beratBarang.Text)
                Else
                    objRow("ekspedisi") = ToDouble(beratBarang.Text) * ToDouble(trnbelidtlqty.Text) * ToDouble(costEkspedisi.Text)
                    objRow("berat") = ToDouble(beratBarang.Text)
                End If
            End If
        Else
            'None / Global
            objRow("ekspedisi") = ToDouble(costEkspedisi.Text)
            objRow("berat") = 0
        End If
        objRow("hargaEkspedisi") = costEkspedisi.Text
        If cb1.Checked = True Then
            objRow("typeBerat") = 0
        ElseIf cb2.Checked = True Then
            objRow("typeBerat") = 1
        Else
            If costEkspedisi.Text <> 0.0 Then
                objRow("typeBerat") = 3
            Else
                objRow("typeBerat") = 2
            End If
        End If
        objRow("podtldisc2") = ToDouble(trnbelidtldisc2.Text)
        objRow("podtldisctype2") = trnbelidtldisctype2.SelectedValue
        objRow("pobelidtlnote") = trnbelidtlnote.Text
        objRow("pobelidtlstatus") = posting.Text
        objRow("upduser") = Session("UserID")
        objRow("updtime") = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
        objRow("DiscAmt") = ToDouble(amtdisc.Text)
        objRow("DiscAmt2") = ToDouble(trnbelidtldisc2.Text)
        objRow("poamtdtldisc") = ToDouble(totamtdisc.Text)
        objRow("unitseq") = unitseq
        objRow("material") = txtMaterial.Text
        objRow("Unit") = ddlUnit.SelectedValue
        objRow("Bonus") = lblbonus.Text
        objRow("flagBarang") = DDLstockflag.SelectedItem.Text

        Dim category As Integer = 0
        If DDlmatCatmst.SelectedValue = "SEMUA GROUP" Then
            category = GetStrData("select itemgroupoid from ql_mstitem where itemoid = " & matoid.Text & "")
        Else
            category = DDlmatCatmst.SelectedValue
        End If

        objRow("matcategoryoid") = category
        objRow("merk") = merk.Text

        If I_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
        End If

        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        Session("TabelDtl") = objTable
        fillTotalOrder() : ReAmount()

        ClearDetail() : tbldtl.Visible = True
        tbldtl.DataSource = objTable
        tbldtl.DataBind()

        trnbelidtlseq.Text = objTable.Rows.Count + 1
        ddlUnit.Items.Clear()
        tbldtl.SelectedIndex = -1
        tamvoucherlama.Text = 0
    End Sub 

    Protected Sub trnbelidtlqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidtlqty.TextChanged, trnbelidtlprice.TextChanged, trnbelidtldisc.TextChanged
        Dim text As System.Web.UI.WebControls.TextBox = CType(sender, System.Web.UI.WebControls.TextBox)
        text.Text = ToMaskEdit(ToDouble(text.Text), 3)
        ReAmountDetail():TampilkanQty3()
    End Sub 

    Protected Sub trnbelidisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidisctype.SelectedIndexChanged
        If trnbelidisctype.SelectedValue = "AMT" Then 'amount
            trnbelidisc.Text = "0.00" : lbl1.Text = " (Amount) "
        Else
            lbl1.Text = " (%) "
        End If
        trnbelidisc.Text = "0.00" : ReAmount()
    End Sub

    Protected Sub trnbelidtldisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidtldisctype.SelectedIndexChanged
        If trnbelidtldisctype.SelectedValue = "AMT" Or trnbelidtldisctype.SelectedValue = "VOUCHER" Then 'amount
            trnbelidtldisc.Text = "0.00"
            lbl2.Text = "Disc amt"
        Else
            lbl2.Text = "Disc %"
        End If
        ReAmountDetail()
    End Sub

    Protected Sub trnbeliqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidisc.TextChanged, trntaxpct.TextChanged
        Dim text As System.Web.UI.WebControls.TextBox = CType(sender, System.Web.UI.WebControls.TextBox)
        text.Text = ToMaskEdit(ToDouble(text.Text), 3)
        ReAmount()
    End Sub

    Protected Sub tbldtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbldtl.SelectedIndexChanged
        Try
            trnbelidtlseq.Text = tbldtl.SelectedDataKey(0).ToString().Trim
            If Session("TblDtl") Is Nothing = False Then
                I_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "podtlseq=" & trnbelidtlseq.Text

                matoid.Text = dv.Item(0).Item("matoid")
                trnbelidtlqty.Text = ToMaskEdit(dv.Item(0).Item("podtlqty"), 3)
                trnbelidtlprice.Text = ToMaskEdit(dv.Item(0).Item("podtlprice"), 3)

                costEkspedisi.Text = ToMaskEdit(dv.Item(0).Item("hargaekspedisi"), 3)
                trnbelidtldisc.Text = ToMaskEdit(dv.Item(0).Item("podtldisc"), 3)
                trnbelidtldisctype.SelectedValue = dv.Item(0).Item("podtldisctype")
                ReAmountDetail()
                trnbelidtldisc2.Text = ToMaskEdit(dv.Item(0).Item("podtldisc2"), 3) 
                tamvoucherlama.Text = ToMaskEdit(dv.Item(0).Item("podtldisc2"), 3)
                trnbelidtldisctype2.SelectedValue = dv.Item(0).Item("podtldisctype2")
                trnbelidtlnote.Text = dv.Item(0).Item("pobelidtlnote") 
                merk.Text = dv.Item(0).Item("merk")
                amtdisc.Text = ToMaskEdit(dv.Item(0).Item("DiscAmt"), 3)
                amtdisc2.Text = ToMaskEdit(dv.Item(0).Item("DiscAmt2"), 3)
                totamtdisc.Text = ToMaskEdit(dv.Item(0).Item("poamtdtldisc"), 3)
                amtbelinetto2.Text = ToMaskEdit(dv.Item(0).Item("poamtdtlnetto"), 3)
                txtMaterial.Text = dv.Item(0).Item("material")
                lblbonus.Text = dv.Item(0).Item("Bonus")
                DDLstockflag.SelectedItem.Text = dv.Item(0).Item("flagBarang")

                If dv.Item(0).Item("typeBerat") = 2 Or dv.Item(0).Item("typeBerat") = 3 Then
                    beratBarang.Text = ToMaskEdit(dv.Item(0).Item("berat"), 3)
                    beratVolume.Text = ToMaskEdit(dv.Item(0).Item("berat"), 3)
                Else
                    If dv.Item(0).Item("typeBerat") = 0 Then
                        cb1.Checked = True : beratBarang.Text = ToMaskEdit(dv.Item(0).Item("berat"), 3)
                        beratVolume.Enabled = False : beratVolume.CssClass = "inpTextDisabled"
                    Else
                        cb2.Checked = True : beratVolume.Text = ToMaskEdit(dv.Item(0).Item("berat"), 3)
                        beratBarang.Enabled = False : beratBarang.CssClass = "inpTextDisabled"
                    End If
                End If

                If lblbonus.Text = "Yes" Then
                    bonus.Checked = True
                Else
                    bonus.Checked = False
                End If

                initUnit() : ddlUnit.SelectedValue = dv.Item(0).Item("Unit")
                QunitSeq() : dv.RowFilter = ""
            End If
            MultiView1.ActiveViewIndex = 0
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
        TampilkanQty3()
    End Sub

    Protected Sub currencyoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles currencyoid.SelectedIndexChanged
        Dim sErr As String = ""

        If trnbelipodate.Text = "" Then
            showMessage("Please fill SO Date first!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            If Not IsValidDate(trnbelipodate.Text, "dd/MM/yyyy", sErr) Then
                showMessage("SO Date is invalid.!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
        Dim cRate As New ClassRate()

        If currencyoid.SelectedValue <> "" Then
            trnbelipodate.Text = Format(CDate(Now), "dd/MM/yyyy")
            cRate.SetRateValue(CInt(currencyoid.SelectedValue), toDate(trnbelipodate.Text))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage("Please Create Rate for This Week!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage("Please Create Rate for This Month!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            rateoid.Text = cRate.GetRateMonthlyOid
            currencyRate.Text = ToMaskEdit2(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
            'sorate2tousd.Text = ToMaskEdit2(cRate.GetRateMonthlyUSDValue, GetRoundValue(cRate.GetRateMonthlyUSDValue.ToString))
        End If

        If currencyoid.SelectedValue = 1 Then
            Label63.Text = "Rupiah"
        Else
            Label63.Text = "Dollar"
        End If
        trnbelipodate.Text = Format(CDate(Now), "dd/MM/yyyy")
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        MultiView1.ActiveViewIndex = 1
        If chkvcr.Checked = True Then
            LinkButton10.Visible = True
            lblchkvcr.Text = "1"
            tabindex21true()
        Else
            LinkButton10.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
        If market.SelectedValue = "LOCAL" Then
            LinkButton11.Visible = False
            Label56.Visible = False
        ElseIf market.SelectedValue = "IMPORT" Then
            LinkButton11.Visible = True
            Label56.Visible = True
        End If
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        MultiView1.ActiveViewIndex = 0
        If chkvcr.Checked = True Then
            LinkButton8.Visible = True
            lblchkvcr.Text = "1"
            tabindex21true()
        Else
            LinkButton8.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
    End Sub

    Protected Sub Close_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.DisposeGridView(gvSupplier)
    End Sub

    Protected Sub LinkButton6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton6.Click
        'MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub LinkButton7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton7.Click
        'MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub ButtonValidasi_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
        'Validasi.Text = ""
    End Sub

    Protected Sub btnsavemstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsavemstr.Click
        If chkvcr.Checked = True Then
            If Session("tblGVvoucher") Is Nothing Then
                showMessage("Detil Voucher Belum Ada, Silahkan Isi Terlebih Dahulu", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            Else
                Dim cek_voucher As DataTable = Session("tblGVvoucher")
                If cek_voucher.Rows.Count = 0 Then
                    showMessage("Detil Voucher Belum Ada, Silahkan Isi Terlebih Dahulu", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If
        End If

        If trnsuppoid.Text.Trim = "" Then
            showMessage("Silahkan pilih Supplier dahulu!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            posting.Text = "In Process"
            Exit Sub
        End If

        If IsDate(toDate(trnbelipodate.Text)) = False Then
            'Validasi.Text = "Invalid PO Date !"
            showMessage("Format tanggal Purchase Order salah !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            posting.Text = "In Process"
            Exit Sub
        Else
            If CDate(toDate(trnbelipodate.Text)) < CDate("01/01/1900") Then
                showMessage("Maaf, Format tanggal salah !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                posting.Text = "In Process"
                Exit Sub
            End If
            If CDate(toDate(trnbelipodate.Text)) > CDate(toDate("6/6/2079")) Then
                showMessage("Maaf, Tanggal harus kurang dari 06/06/2079 !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                posting.Text = "In Process"
                Exit Sub
            End If
        End If

        If IsDate(toDate(podeliverydate.Text)) = False Then
            'Validasi.Text = "Invalid PO Date !"
            showMessage("Format tanggal Purchase Order Delivery salah !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            posting.Text = "In Process"
            Exit Sub
        Else
            If CDate(toDate(podeliverydate.Text)) < CDate("01/01/1900") Then
                showMessage("Format tanggal Purchase Order Delivery salah !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : posting.Text = "In Process"
                Exit Sub
            End If
            If CDate(toDate(podeliverydate.Text)) > CDate(toDate("6/6/2079")) Then
                showMessage("Tanggal Purchase Order Delivery harus kurang dari 06/06/2079 !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : posting.Text = "In Process"
                Exit Sub
            End If
        End If

        If CDate(toDate(podeliverydate.Text)) < CDate(toDate(trnbelipodate.Text)) Then
            showMessage("Delivery Date harus lebih besar dar PO date !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : posting.Text = "In Process"
            Exit Sub
        End If

        Try
            periodacctg.Text = Format(CDate(toDate(trnbelipodate.Text)), "yyyy-MM")
        Catch ex As Exception
            showMessage("Format tanggal Purchase Order salah !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            posting.Text = "In Process" : Exit Sub
        End Try

        If temptotvcr.Text > 0 Then
            showMessage("Masih Ada Sisa Voucher Sebesar Rp." & ToMaskEdit(ToDouble(temptotvcr.Text), 3) & " harus di subsidikan semua terlebih dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            posting.Text = "In Process"
            Exit Sub
        ElseIf temptotvcr.Text < 0 Then
            showMessage("Kelebihan Akumulasi Voucher Sebesar Rp." & ToMaskEdit(ToDouble(temptotvcr.Text) * -1, 3) & "!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            posting.Text = "In Process"
            Exit Sub
        End If

        'cek apa sudah ada material dari po 
        If Session("TblDtl") Is Nothing Then
            showMessage("Data tidak bisa disimpan !, Tidak ada Data Item !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            posting.Text = "In Process"
            Exit Sub
        Else
            Dim objTableCek As DataTable = Session("TblDtl")
            If objTableCek.Rows.Count = 0 Then
                showMessage("Data tidak bisa disimpan !, Tidak ada Data Item !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                posting.Text = "In Process"
                Exit Sub
            End If
        End If

        If ToDouble(amtbelinetto1.Text) <= 0 Then
            showMessage("Net Purchase tidak boleh minus !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        ' checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(trnbelipono.Text, "trnbelipono", "ql_pomst") Then
                generateNoPO()
            End If
        Else 'cek data ada atau sudah di hapuse
            If CheckDataExists(oid.Text, "trnbelimstoid", "ql_pomst") = False Then
                showMessage("Tidak bisa update Data ini, Data sudah didelete !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If

        If checkApproval("QL_POMST", "In Approval", Session("oid"), "New", "FINAL", CabangNya.SelectedValue) > 0 Then
            showMessage("Purchase Order ini sudah di send for Approval", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        '-----------------------------------------------------------------------------------------
        If posting.Text = "In Approval" Then
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_pomst' and branch_code Like '%" & CabangNya.SelectedValue & "%' order by approvallevel"
            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                showMessage("Tiada Approval User untuk PO, Silahkan contact admin dahulu", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            ' -------- Proses Hitung Ulang --------
            ' =====================================
            Dim objDtl As DataTable : objDtl = Session("TblDtl")
            Dim TotDiscdtl, TotVou As Double
            For c1 As Int16 = 0 To objDtl.Rows.Count - 1
                TotDiscdtl += ToDouble(objDtl.Rows(c1).Item("DiscAmt"))
                TotVou += ToDouble(objDtl.Rows(c1).Item("DiscAmt2"))
            Next
            'amtdiscdtl.Text = ToDouble(TotDiscdtl)
            totvoucher.Text = ToDouble(TotVou)
            ' -------- End proses Hitung Ulang --------
            ' =========================================
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            Try
                If Session("oid") <> Nothing Or Session("oid") <> "" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL(cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus, branch_code) VALUES ('" & CompnyCode & "', " & Session("AppOid") + c1 & ", '" & "PO" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'QL_pomst', '" & Session("oid") & "', 'In Approval', '0', '" & objTable.Rows(c1).Item("approvaluser") & "', '1/1/1900', '" & objTable.Rows(c1).Item("approvaltype") & "', '1', '" & objTable.Rows(c1).Item("approvalstatus") & "', '" & CabangNya.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Else
                    showMessage("Silahkan Simpan PO terlebih dahulu.", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As OleDb.OleDbException
                objTrans.Rollback()
                xCmd.Connection.Close()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
                Exit Sub
            End Try
            '-------------------------------------------------------------------------------------
        ElseIf posting.Text = "Rejected" Then
            posting.Text = "In Process"
        End If

        Dim poprdtloid As Integer = GenerateID("QL_poprdtl", CompnyCode)
        Dim PRno As String = ""

        Dim SO_BO As String = ""
        If SOBO.Checked = True Then
            SO_BO = "T"
        Else
            SO_BO = "F"
        End If

        Dim cRate As New ClassRate()
        cRate.SetRateValue(CInt(currencyoid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            ' insert table master
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            trnbelidtloid.Text = GenerateID("QL_podtl", CompnyCode)
            voucheroid.Text = GenerateID("QL_VOUCHERDTL", CompnyCode)
            xCmd.Transaction = objTrans

            Try
                oid.Text = GenerateID("QL_pomst", CompnyCode)
                sSql = "INSERT INTO QL_pomst (cmpcode, TRNBELImstoid, periodacctg, TRNBELItype, TRNBELIdeliverydate, trnbelipodate, trnbelipono, trnsuppoid, TRNpaytype, curroid, TRNBELIdisc,TRNBELIdiscidr, TRNBELIdiscusd, TRNBELIdisctype, TRNBELInote, TRNBELIstatus, TRNtaxpct, upduser, updtime, amtdiscdtl, amtdiscdtlidr, amtdiscdtlusd, amtdischdr, amtdischdridr, amtdischdrusd,amtbeli, amtbeliidr, amtbeliusd, amttax, amttaxidr, amttaxusd, amtbelinetto, amtbelinettoidr,amtbelinettousd, finalapprovalcode, finalapprovaluser, finalapprovaldatetime, currate, digit, expeoid, statusPOBackorder, flagVcr, market, totalvoucher, branch_code, PrefixOid, tocabang, typepo, flagSR)" & _
                " VALUES ('" & CompnyCode & "'," & oid.Text & ", '" & periodacctg.Text & "', 'GROSIR', '" & toDate(podeliverydate.Text) & "','" & toDate(trnbelipodate.Text) & "', '" & Tchar(trnbelipono.Text) & "', " & trnsuppoid.Text & ", " & TRNPAYTYPE.SelectedValue & ", " & currencyoid.Text & "," & ToDouble(trnbelidisc.Text) & ", " & ToDouble(trnbelidisc.Text) & ", " & ToDouble(trnbelidisc.Text) & ", '" & trnbelidisctype.SelectedValue & "', '" & Tchar(trnbelinote.Text) & "', '" & posting.Text & "', " & ToDouble(trntaxpct.Text) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(amtdiscdtl.Text) & ", " & ToDouble(amtdiscdtl.Text) & ", " & ToDouble(amtdiscdtl.Text) & ", " & ToDouble(amtdischdr.Text) & ", " & ToDouble(amtdischdr.Text) & ", " & ToDouble(amtdischdr.Text) & ", " & ToDouble(amtbeli.Text) & ", " & ToDouble(amtbeli.Text) & ", " & ToDouble(amtbeli.Text) & ", " & ToDouble(amttax.Text) & ", " & ToDouble(amttax.Text) & ", " & ToDouble(amttax.Text) & ", " & ToDouble(amtbelinetto1.Text) & ", " & ToDouble(amtbelinetto1.Text) & ", " & ToDouble(amtbelinetto1.Text) & ", '" & approvalcode.Text & "', '" & approvaluserid.Text & "', '" & toDate(ApprovalDate.Text) & "', '" & ToDouble(currencyRate.Text) & "', '" & digit.SelectedValue & "', '" & Ekspedisi.SelectedValue & "', '" & SO_BO & "', '" & lblchkvcr.Text & "', '" & market.SelectedValue & "', '" & ToDouble(totvoucher.Text) & "', '" & CabangNya.SelectedValue & "', " & DDLPrefix.SelectedValue & ", '" & toCabang.SelectedValue & "', '" & TypePOnya.SelectedValue & "', '" & IIf(cbSR.Checked, "Y", "N") & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "Update QL_mstoid set lastoid=" & oid.Text & " where upper(tablename)='QL_POMST' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Insert Detil Voucher
                If Not Session("tblGVvoucher") Is Nothing Then
                    'voucheroid.Text = GenerateID("QL_VOUCHERDTL", CompnyCode)
                    Dim objtable As DataTable
                    objtable = Session("tblGVvoucher")
                    For c1 As Int32 = 0 To objtable.Rows.Count - 1
                        sSql = "insert into ql_voucherdtl(cmpcode, voucheroid, branch_code, voucherseq, trnbelimstoid, trnbelino, voucherno, voucherdate, voucherdesc, amtvoucher, amtvoucheridr, amtvoucherusd, vouchernote, currencyoid, currencyrate, rate2oidvcr) Values " & _
                        "('" & CompnyCode & "','" & (c1 + CInt(voucheroid.Text)) & "','" & CabangNya.SelectedValue & "','" & objtable.Rows(c1).Item("voucherseq") & "','" & oid.Text & "','" & Tchar(trnbelipono.Text) & "','" & objtable.Rows(c1).Item("voucherno") & "','" & objtable.Rows(c1).Item("voucherdate") & "','" & objtable.Rows(c1).Item("voucherdesc") & "','" & objtable.Rows(c1).Item("amtvoucher") & "','" & objtable.Rows(c1).Item("amtvoucher") & "','" & objtable.Rows(c1).Item("amtvoucher") & "','" & Tchar(objtable.Rows(c1).Item("vouchernote")) & "','" & objtable.Rows(c1).Item("currencyoid") & "','" & objtable.Rows(c1).Item("currencyrate") & "','" & rate2oidvcr.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "Update QL_mstoid set lastoid=" & (objtable.Rows.Count - 1 + CInt(voucheroid.Text)) & " Where tablename = 'QL_VOUCHERDTL' And cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable : objTable = Session("TblDtl")

                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_podtl (cmpcode, trnbelidtloid, trnbelimstoid,trnbelidtlseq, itemoid, trnbelidtlqty, trnbelidtlunit,trnbelidtlprice,trnbelidtlpriceidr,trnbelidtlpriceusd, trnbelidtldisc,trnbelidtldiscidr,trnbelidtldiscusd, trnbelidtldisctype, trnbelidtlnote, trnbelidtlstatus, updtime, upduser, amtdisc, amtdiscidr, amtdiscusd, amtbelinetto, amtbelinettoidr, amtbelinettousd, trnbelidtldisc2, trnbelidtldisc2idr, trnbelidtldisc2usd, trnbelidtldisctype2, amtdisc2, amtdisc2idr, amtdisc2usd, unitseq, ekspedisi, typeberat, berat, hargaekspedisi, ekspedisiidr, ekspedisiusd, bonus, bonusAmt, branch_code, tocabang)" & _
                            " VALUES ('" & CompnyCode & "'," & _
                            (c1 + CInt(trnbelidtloid.Text)) & ", " & oid.Text & "," & objTable.Rows(c1).Item("podtlseq") & "," & objTable.Rows(c1).Item("matoid") & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtlqty")) & ",'" & _
                            objTable.Rows(c1).Item("Unit") & "', " & _
                            ToDouble(objTable.Rows(c1).Item("podtlprice")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtlprice")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtlprice")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtldisc")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtldisc")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtldisc")) & ",'" & _
                            objTable.Rows(c1).Item("podtldisctype") & "','" & _
                            Tchar(objTable.Rows(c1).Item("pobelidtlnote")) & "','" & _
                            Tchar(objTable.Rows(c1).Item("pobelidtlstatus")) & _
                            "',CURRENT_TIMESTAMP,'" & Upduser.Text & "', " & _
                            ToDouble(objTable.Rows(c1).Item("DiscAmt")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("DiscAmt")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("DiscAmt")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("poamtdtlnetto")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("poamtdtlnetto")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("poamtdtlnetto")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtldisc2")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtldisc2")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("podtldisc2")) & ",'" & objTable.Rows(c1).Item("podtldisctype2") & "'," & _
                            ToDouble(objTable.Rows(c1).Item("DiscAmt2")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("DiscAmt2")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("DiscAmt2")) & ",'" & objTable.Rows(c1).Item("unitseq") & "', " & _
                            ToDouble(objTable.Rows(c1).Item("ekspedisi")) & ",'" & _
                            ToDouble(objTable.Rows(c1).Item("typeBerat")) & "','" & _
                            ToDouble(objTable.Rows(c1).Item("berat")) & "'," & _
                            ToDouble(objTable.Rows(c1).Item("hargaEkspedisi")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("ekspedisi")) & "," & _
                            ToDouble(objTable.Rows(c1).Item("ekspedisi")) & ",'" & objTable.Rows(c1).Item("bonus") & "'," & _
                            ToDouble(objTable.Rows(c1).Item("bonusAmt")) & ",'" & CabangNya.SelectedValue & "','" & toCabang.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "update  QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(trnbelidtloid.Text)) & " Where tablename = 'QL_podtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
                xCmd.Connection.Close() : posting.Text = "In Process" : Exit Sub
            End Try
        Else
            'update tabel master    
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try

                'If posting.Text = "POST" Then
                '    Dim tax As String = ""
                '    If Taxable.Checked = True Then
                '        tax = "0"
                '    Else
                '        tax = "1"
                '    End If
                '    Dim type As String = "PO"
                '    sSql = "Select genother1 from ql_mstgen where gengroup='cabang' And gencode='" & CabangNya.SelectedValue & "'"
                '    Dim cabang As String = cKon.ambilscalar(sSql)
                '    Dim sNo As String = type & tax & "/" & cabang & "/" & Format(GetServerTime, "yy") & "/" & Format(GetServerTime, "MM") & "/"
                '    sSql = "SELECT ISNULL(MAX(ABS(REPLACE(trnbelipono, '" & sNo & "',''))),0) FROM ql_pomst WHERE cmpcode='" & CompnyCode & "' AND trnbelipono LIKE '" & sNo & "%'"
                '    xCmd.CommandText = sSql
                '    Dim nomer As String = GenNumberString(sNo, "", xCmd.ExecuteScalar + 1, 4)
                '    trnbelipono.Text = nomer
                'End If

                sSql = "UPDATE QL_pomst SET periodacctg='" & periodacctg.Text & "', TRNBELIdeliverydate='" & toDate(podeliverydate.Text) & "', trnbelipodate='" & toDate(trnbelipodate.Text) & "', trnbelipono='" & trnbelipono.Text & "', trnsuppoid=" & trnsuppoid.Text & ", trnpaytype=" & TRNPAYTYPE.SelectedValue & _
                ", trnbelidisc=" & ToDouble(trnbelidisc.Text) & _
                ", trnbelidiscidr=" & ToDouble(trnbelidisc.Text) & _
                ", trnbelidiscusd=" & ToDouble(trnbelidisc.Text) & _
                ", trnbelidisctype='" & trnbelidisctype.SelectedValue & _
                "', trnbelinote='" & Tchar(trnbelinote.Text) & "', trntaxpct=" & ToDouble(trntaxpct.Text) & ", upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, amtdiscdtl=" & ToDouble(amtdiscdtl.Text) & _
                ", amtdiscdtlidr=" & ToDouble(amtdiscdtl.Text) & _
                ", amtdiscdtlusd=" & ToDouble(amtdiscdtl.Text) & _
                ", amtdischdr=" & ToDouble(amtdischdr.Text) & _
                ", amtdischdridr=" & ToDouble(amtdischdr.Text) & _
                ", amtdischdrusd=" & ToDouble(amtdischdr.Text) & _
                ", amtbeli=" & ToDouble(amtbeli.Text) & _
                ", amtbeliidr=" & ToDouble(amtbeli.Text) & _
                ", amtbeliusd=" & ToDouble(amtbeli.Text) & _
                ", amttax=" & ToDouble(amttax.Text) & _
                ", amttaxidr=" & ToDouble(amttax.Text) & _
                ", amttaxusd=" & ToDouble(amttax.Text) & _
                ", amtbelinetto=" & ToDouble(amtbelinetto1.Text) & _
                ", amtbelinettoidr=" & ToDouble(amtbelinetto1.Text) & _
                ", statusPOBackorder = '" & SO_BO & "', trnbelistatus='" & posting.Text & "', finalapprovaluser='', finalapprovalcode='" & approvalcode.Text & "', finalapprovaldatetime='1/1/1900', curroid = '" & currencyoid.Text & "', currate='" & ToDouble(currencyRate.Text) & "', digit='" & digit.SelectedValue & "', expeoid = '" & Ekspedisi.SelectedValue & "', flagvcr='" & lblchkvcr.Text & "',market='" & market.SelectedValue & "', totalvoucher = '" & ToDouble(totvoucher.Text) & "', PrefixOid = '" & DDLPrefix.SelectedValue & "', tocabang='" & toCabang.SelectedValue & "', typepo='" & TypePOnya.SelectedValue & "', branch_code='" & CabangNya.SelectedValue & "', flagSR = '" & IIf(cbSR.Checked, "Y", "N") & "' WHERE cmpcode='" & CompnyCode & "' and trnbelimstoid=" & Session("oid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                If Not Session("tblGVvoucher") Is Nothing Then
                    sSql = "delete from ql_voucherdtl where trnbelimstoid = '" & Session("oid") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'insert tabel detail voucher
                    voucheroid.Text = GenerateID("QL_VOUCHERDTL", CompnyCode)
                    Dim objtable As DataTable
                    objtable = Session("tblGVvoucher")
                    For c1 As Int32 = 0 To objtable.Rows.Count - 1
                        sSql = "insert into ql_voucherdtl (cmpcode, voucheroid, branch_code, voucherseq, trnbelimstoid, trnbelino, voucherno, voucherdate, voucherdesc, amtvoucher, amtvoucheridr, amtvoucherusd, vouchernote, currencyoid, currencyrate, rate2oidvcr) values ('" & CompnyCode & "', '" & (c1 + CInt(voucheroid.Text)) & "', '" & CabangNya.SelectedValue & "', '" & objtable.Rows(c1).Item("voucherseq") & "', '" & oid.Text & "', '" & Tchar(trnbelipono.Text) & "', '" & objtable.Rows(c1).Item("voucherno") & "', '" & objtable.Rows(c1).Item("voucherdate") & "', '" & objtable.Rows(c1).Item("voucherdesc") & "', '" & objtable.Rows(c1).Item("amtvoucher") & "', '" & objtable.Rows(c1).Item("amtvoucher") & "', '" & objtable.Rows(c1).Item("amtvoucher") & "', '" & Tchar(objtable.Rows(c1).Item("vouchernote")) & "', '" & objtable.Rows(c1).Item("currencyoid") & "', '" & objtable.Rows(c1).Item("currencyrate") & "', '" & rate2oidvcr.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "update  QL_mstoid set lastoid=" & (objtable.Rows.Count - 1 + CInt(voucheroid.Text)) & " Where tablename = 'QL_VOUCHERDTL' and cmpcode = '" & CompnyCode & "'  "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                End If
                If Not Session("TblDtl") Is Nothing Then
                    sSql = "Delete from QL_podtl Where trnbelimstoid = " & Session("oid")
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'insert tabel detail
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    trnbelidtloid.Text = GenerateID("QL_podtl", CompnyCode)
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_podtl (cmpcode, trnbelidtloid, trnbelimstoid,trnbelidtlseq, itemoid, trnbelidtlqty, trnbelidtlunit,trnbelidtlprice, trnbelidtlpriceidr, trnbelidtlpriceusd, trnbelidtldisc, trnbelidtldiscidr, trnbelidtldiscusd, trnbelidtldisctype, trnbelidtlnote,trnbelidtlstatus, updtime, upduser, amtdisc, amtdiscidr, amtdiscusd, amtbelinetto, amtbelinettoidr, amtbelinettousd, trnbelidtldisc2, trnbelidtldisc2idr, trnbelidtldisc2usd, trnbelidtldisctype2, amtdisc2, amtdisc2idr, amtdisc2usd, unitseq, ekspedisi, typeberat, berat, hargaekspedisi, ekspedisiidr, ekspedisiusd, bonus, bonusAmt, tocabang, branch_code)" & _
                        " VALUES ('" & CompnyCode & "', " & (c1 + CInt(trnbelidtloid.Text)) & ", " & oid.Text & ", " & objTable.Rows(c1).Item("podtlseq") & ", " & _
                              objTable.Rows(c1).Item("matoid") & ", " & _
                              ToDouble(objTable.Rows(c1).Item("podtlqty")) & ", '" & _
                              objTable.Rows(c1).Item("Unit") & "', " & _
                              ToDouble(objTable.Rows(c1).Item("podtlprice")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("podtlprice")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("podtlprice")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("podtldisc")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("podtldisc")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("podtldisc")) & ", '" & _
                              objTable.Rows(c1).Item("podtldisctype") & "', '" & _
                              Tchar(objTable.Rows(c1).Item("pobelidtlnote")) & "', '" & _
                              Tchar(objTable.Rows(c1).Item("pobelidtlstatus")) & _
                              "', CURRENT_TIMESTAMP, '" & Upduser.Text & "', " & _
                              ToDouble(objTable.Rows(c1).Item("DiscAmt")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("DiscAmt")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("DiscAmt")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("poamtdtlnetto")) & " ," & _
                              ToDouble(objTable.Rows(c1).Item("poamtdtlnetto")) & " ," & _
                              ToDouble(objTable.Rows(c1).Item("poamtdtlnetto")) & " ," & _
                              ToDouble(objTable.Rows(c1).Item("podtldisc2")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("podtldisc2")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("podtldisc2")) & ", '" & _
                              objTable.Rows(c1).Item("podtldisctype2") & "', " & _
                              ToDouble(objTable.Rows(c1).Item("DiscAmt2")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("DiscAmt2")) & ", " & _
                              ToDouble(objTable.Rows(c1).Item("DiscAmt2")) & ", '" & _
                              objTable.Rows(c1).Item("unitseq") & "'," & _
                              ToDouble(objTable.Rows(c1).Item("ekspedisi")) & "," & _
                              ToDouble(objTable.Rows(c1).Item("typeBerat")) & "," & _
                              ToDouble(objTable.Rows(c1).Item("berat")) & "," & _
                              ToDouble(objTable.Rows(c1).Item("hargaEkspedisi")) & "," & _
                              ToDouble(objTable.Rows(c1).Item("ekspedisi")) & "," & _
                              ToDouble(objTable.Rows(c1).Item("ekspedisi")) & ",'" & _
                              objTable.Rows(c1).Item("bonus") & "'," & _
                              ToDouble(objTable.Rows(c1).Item("bonusAmt")) & ",'" & toCabang.SelectedValue & "','" & CabangNya.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "Update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(trnbelidtloid.Text)) & " Where tablename = 'QL_podtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                posting.Text = "In Process"
                objTrans.Rollback() : xCmd.Connection.Close()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
            End Try
        End If
        Session("TblDtl") = Nothing : Session("oid") = Nothing

        If posting.Text = "In Approval" Then
            showMessage("Data berhasil di Send for Approval !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            showMessage("Data berhasil disimpan !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        End If
    End Sub 

    Protected Sub btnCancelMstr1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelMstr.Click
        Session("oid") = Nothing : Session("TblDtl") = Nothing
        Response.Redirect("~\Transaction\ft_trnpo.aspx?awal=true")
    End Sub

    Protected Sub btnDeleteMstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteMstr.Click
        'MsgBox(oid.Text)
        If oid.Text.Trim = "" Then
            showMessage("ID Purchase Order Salah!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        Dim dtTempWeb As DataTable = checkApprovaldata("QL_pomst", "In Approval", Session("oid"), "", "New")

        If checkApproval("QL_POMST", "In Approval", Session("oid"), "New", "FINAL", Session("branch_id")) > 0 Then
            showMessage("Purchase Order ini sudah di send for Approval", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If


        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        'trnbelidtloid.Text = GenerateID("QL_podtl", CompnyCode)
        xCmd.Transaction = objTrans
        Try
            ' Update status Approval if exist to DELETED
            If dtTempWeb.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtTempWeb.Rows.Count - 1
                    sSql = "UPDATE ql_approval SET statusrequest='Deleted' WHERE cmpcode = '" & CompnyCode & _
                        "' AND approvaloid='" & dtTempWeb.Rows(C1)("approvaloid").ToString & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            sSql = "Delete from QL_podtl where trnbelimstoid = " & oid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Delete from QL_pomst where trnbelimstoid = " & oid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
        End Try
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")

        'Response.Redirect("~\Transaction\ft_trnpo.aspx?awal=true")
    End Sub

    Protected Sub TrnBeliSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
       
    End Sub

    Protected Sub btnCancelDtl_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelDtl.Click
        ClearDetail()

        btnaddpr.Visible = False
        btnclearPR.Visible = False
    End Sub

    Protected Sub lkbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub imbSearchUser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim query = "SELECT userid, username FROM QL_mstprof WHERE cmpcode = '" & CompnyCode & "' and divisi = 'Approval'"

        FillGV(gvUserList, query, "ql_mstprof")
        btnHiddenPnlUser.Visible = True
        pnlUser.Visible = True : mpe2.Show()
    End Sub

    Protected Sub gvUserList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        approvalusername.Text = gvUserList.SelectedDataKey(1).ToString().Trim
        approvaluserid.Text = gvUserList.SelectedDataKey(0).ToString().Trim
        ddlusr.SelectedIndex = 0 : TextBox2.Text = ""
        btnHiddenPnlUser.Visible = False : pnlUser.Visible = False
        mpe2.Hide() : gvUserList.SelectedIndex = -1
    End Sub

    Protected Sub imbClearUser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        approvalusername.Text = "" : approvaluserid.Text = ""
    End Sub

    Protected Sub lkbUser_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlusr.SelectedIndex = 0 : TextBox2.Text = ""
        btnHiddenPnlUser.Visible = False : pnlUser.Visible = False : mpe2.Hide()
    End Sub

    Protected Sub imbApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbApproval.Click
        posting.Text = "In Approval" : btnsavemstr_Click(sender, e)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Dim report2 As String = "pdf"
        showMessageprint(Session("oid"), trnbelipono.Text, report2)
    End Sub 

    Protected Sub LinkButton8_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim report2 As String = ""
        PrintReport(tbldata.SelectedDataKey.Item(0), tbldata.SelectedDataKey.Item(1), report2)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldata.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub imbFindSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sPlus As String = "" : binddataSupp(sPlus)
    End Sub

    Protected Sub imbAllSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddataSupp("")
    End Sub

    Protected Sub Taxable_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Taxable.Checked Then
            trntaxpct.Text = ToMaskEdit(ToDouble(cKon.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & _
                CompnyCode & "' AND gengroup='TAX'")), 3)
        Else
            trntaxpct.Text = "0.00"
        End If
        ReAmount()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ddlFilter.SelectedIndex = 0 : FilterText.Text = ""
        chkStatus.Checked = False : DDLStatus.SelectedIndex = 0
        Session("FilterText") = Nothing : Session("FilterDDL") = Nothing
        chkPeriod.Checked = False
        BindData(CompnyCode)
    End Sub 

    Protected Sub viewAllusr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ddlusr.SelectedIndex = 0 : TextBox2.Text = ""
        bind_data_usr("")
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If TextBox2.Text <> "" Then
            Dim sPlus As String = " AND " & ddlusr.SelectedValue & " LIKE '%" & Tchar(TextBox2.Text) & "%'"
            bind_data_usr(sPlus)
        End If
    End Sub

    Protected Sub btnContinoeReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'generateNoPO()
        Dim objTable As DataTable
        Session("TblDtl") = Nothing
        Session("TblpoprDtl") = Nothing
        Session("TabelAddDtlPr") = Nothing
        ClearDetail()
        btnaddpr.Visible = False
        btnclearPR.Visible = False
        cleardtlpr()
        objTable = Session("TblDtl")
        Panelvalidasi2.Visible = False
        ButtonExtendervalidasi2.Visible = False
        ModalPopupExtenderValidasi2.Hide()
    End Sub

    Protected Sub BtnCancelReset_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Panelvalidasi2.Visible = False
        ButtonExtendervalidasi2.Visible = False
        ModalPopupExtenderValidasi2.Hide()
        If DDlmatCatmst.SelectedIndex = 0 Then
            DDlmatCatmst.SelectedIndex = 1
        Else
            DDlmatCatmst.SelectedIndex = 0
        End If
    End Sub 

    Protected Sub gvMaterial_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
                e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
                e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
    End Sub

    Protected Sub ddlUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        TampilkanQty3()
    End Sub

    Protected Sub btnaddpr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ToDouble(trnbelidtlqty.Text) <= 0 Then
            showMessage("Quantity must be greater than zero (0) !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If matoid.Text = "" Then
            showMessage("Please choose Item !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If Session("TabelAddDtlPr") Is Nothing Then
            CreateTableAddPR()
        End If

        Dim objTable As DataTable
        objTable = Session("TabelAddDtlPr")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        If I_u3.Text = "New Detail" Then
            Dim dv As DataView = objTable.DefaultView
            'dv.RowFilter = "mtrloc=" & mtrloc.SelectedValue & " and matoid=" & matoid.Text
            dv.RowFilter = "prdtloid = " & prdtloid.Text
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                objTable = Session("TabelAddDtlPr")
                Exit Sub
            End If
            dv.RowFilter = ""
        End If

        'insert/update to list data
        Dim objRow As DataRow
        If I_u3.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("seq") = objTable.Rows.Count + 1
        Else 'update
            objRow = objTable.Rows(addprseq.Text - 1)
            objRow.BeginEdit()
        End If

        objRow("prdtloid") = prdtloid.Text
        objRow("prno") = prno.Text
        objRow("matoid") = matoid.Text
        objRow("matdesc") = txtMaterial.Text
        objRow("prqty") = ToDouble(trnbelidtlqty.Text)
        objRow("Unit") = ddlUnit.SelectedItem.Text
        objRow("unitoid") = ddlUnit.SelectedValue

        If I_u3.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
        End If

        Session("TabelAddDtlPr") = objTable
        cleardtlpr()
        Session("TabelAddDtlPr") = objTable
        btnaddpr.Visible = False
        btnclearPR.Visible = False
    End Sub

    Protected Sub btnclearPR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnaddpr.Visible = False
        btnclearPR.Visible = False
        cleardtlpr()
    End Sub

    Protected Sub trnbelidtldisctype2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If trnbelidtldisctype.SelectedValue = "AMT" Or trnbelidtldisctype.SelectedValue = "VOUCHER" Then 'amount
            trnbelidtldisc2.Text = "0.00"
            lbl3.Text = "Disc amt"
        Else
            lbl3.Text = "Disc %"
        End If
        'ReAmountDetail2()
    End Sub

    Protected Sub trnbelidtldisc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidtldisc2.TextChanged
        Dim text As System.Web.UI.WebControls.TextBox = CType(sender, System.Web.UI.WebControls.TextBox)
        text.Text = ToMaskEdit(ToDouble(text.Text), 4)
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If lblMessage.Text = "Data berhasil disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data berhasil diposting !" Or lblMessage.Text = "Data berhasil di Send for Approval !" Then
            Response.Redirect("~\Transaction\ft_trnpo.aspx?awal=true")
        Else
            PanelMsgBox.Visible = False
            beMsgBox.Visible = False
            mpeMsgbox.Hide()
        End If
    End Sub

    Protected Sub btncloseprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
    End Sub

    Protected Sub btpPrintAction_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btpPrintAction.Click
        If ddlprinter.SelectedValue = "PDF" Then
            Dim report2 As String = "pdf"
            PrintReport(idTemp.Text, NoTemp.Text, OtherTemp.Text)
        ElseIf ddlprinter.SelectedValue = "EXCEL" Then
            Session("showReport") = True
            showPrintExcel(NoTemp.Text, CInt(idTemp.Text))
        ElseIf ddlprinter.SelectedValue = "notaexcel" Then
            PrintReport(idTemp.Text, NoTemp.Text, "excel")
        ElseIf ddlprinter.SelectedValue = "Without" Then
            PrintReport(idTemp.Text, NoTemp.Text, OtherTemp.Text)
        Else
            PrintReportPTP(idTemp.Text, NoTemp.Text, OtherTemp.Text)
        End If

        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide() 
    End Sub

    Protected Sub autogenerate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
       
    End Sub

    Protected Sub GVdetailSupp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVdetailSupp.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(3).Text = NewMaskEdit(ToDouble(e.Row.Cells(3).Text))
                e.Row.Cells(5).Text = NewMaskEdit(ToDouble(e.Row.Cells(5).Text))
                e.Row.Cells(7).Text = NewMaskEdit(ToDouble(e.Row.Cells(7).Text))
                e.Row.Cells(9).Text = NewMaskEdit(ToDouble(e.Row.Cells(9).Text))
                e.Row.Cells(10).Text = NewMaskEdit(ToDouble(e.Row.Cells(10).Text))
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
    End Sub

    Protected Sub chkvcr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkvcr.CheckedChanged
        datevoucher.Text = Format(Now, "dd/MM/yyyy")
        If chkvcr.Checked = True Then
            MultiView1.ActiveViewIndex = 2
            lblchkvcr.Text = "1"
        Else
            LinkButton8.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
        If market.SelectedValue = "LOCAL" Then
            marketlocal()
            LinkButton17.Visible = False
            Label54.Visible = False
        Else
            LinkButton17.Visible = True
            Label54.Visible = True
        End If
    End Sub

    Protected Sub imbmportir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddataimp(" AND (suppname LIKE '%" & Tchar(importir.Text) & "%' OR suppcode LIKE '%" & Tchar(importir.Text) & "%') /*and suppoid=-1*/ ")
        gvImportir.Visible = True
    End Sub

    Protected Sub gvImportir_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblimportir.Text = gvImportir.SelectedDataKey(0).ToString().Trim
        importir.Text = gvImportir.SelectedDataKey(1).ToString().Trim
        gvImportir.Visible = False
    End Sub

    Protected Sub gvImportir_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvImportir.PageIndex = e.NewPageIndex
        Dim sPlus As String = " AND (suppname LIKE '%" & Tchar(suppliername.Text) & "%' OR suppcode LIKE '%" & Tchar(suppliername.Text) & "%')"
        binddataimp(sPlus)
    End Sub

    Protected Sub clearimportir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvImportir.Visible = False
        importir.Text = ""
        lblimportir.Text = ""
    End Sub 

    Protected Sub LinkButton15_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
        If chkvcr.Checked = True Then
            LinkButton8.Visible = True
            LinkButton10.Visible = True
            lblchkvcr.Text = "1"
            tabindex21true()
        Else
            LinkButton8.Visible = False
            LinkButton10.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
        If market.SelectedValue = "LOCAL" Then
            marketlocal()
            LinkButton11.Visible = False
            Label56.Visible = False
        ElseIf market.SelectedValue = "IMPORT" Then
            LinkButton11.Visible = True
            Label56.Visible = True
        End If
    End Sub

    Protected Sub LinkButton14_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
        If chkvcr.Checked = True Then
            LinkButton8.Visible = True
            lblchkvcr.Text = "1"
            tabindex21true()
        Else
            LinkButton8.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
    End Sub

    Protected Sub LinkButton8_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 2
        If market.SelectedValue = "LOCAL" Then
            marketlocal()
            LinkButton17.Visible = False
            Label54.Visible = False
        ElseIf market.SelectedValue = "IMPORT" Then
            LinkButton17.Visible = True
            Label54.Visible = True
        End If
    End Sub

    Protected Sub LinkButton10_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 3
        If market.SelectedValue = "LOCAL" Then
            marketlocal()
            LinkButton17.Visible = False 
            Label50.Visible = False
        ElseIf market.SelectedValue = "IMPORT" Then
            LinkButton17.Visible = True
            Label50.Visible = True
        End If
    End Sub

    Protected Sub LinkButton12_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 3
    End Sub

    Protected Sub market_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If market.SelectedValue = "LOCAL" Then
            marketlocal()
            MultiView1.ActiveViewIndex = 0
            LinkButton9.Visible = False
            Label50.Visible = False
        ElseIf market.SelectedValue = "IMPORT" Then
            MultiView1.ActiveViewIndex = 3
            LinkButton9.Visible = True
            Label50.Visible = True
        End If
        If chkvcr.Checked = True Then
            LinkButton21.Visible = True
        Else
            LinkButton21.Visible = False
        End If
    End Sub

    Protected Sub LinkButton9_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 3
        If chkvcr.Checked = True Then
            LinkButton21.Visible = True
            lblchkvcr.Text = "1"
            tabindex21true()
        Else
            LinkButton21.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
    End Sub

    Protected Sub LinkButton11_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 3
        If chkvcr.Checked = True Then
            LinkButton21.Visible = True
            lblchkvcr.Text = "1"
            tabindex21true()
        Else
            LinkButton21.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
    End Sub

    Protected Sub LinkButton21_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 2
        If chkvcr.Checked = True Then
            LinkButton10.Visible = True
            Label54.Visible = True
            lblchkvcr.Text = "1"
            tabindex21true()
        Else
            LinkButton10.Visible = False
            Label54.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
        If market.SelectedValue = "LOCAL" Then
            marketlocal()
            LinkButton11.Visible = False
            LinkButton17.Visible = False
            Label56.Visible = False
        ElseIf market.SelectedValue = "IMPORT" Then
            LinkButton11.Visible = True
            LinkButton17.Visible = True
            Label56.Visible = True
        End If
    End Sub

    Protected Sub LinkButton17_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 3
        If chkvcr.Checked = True Then
            LinkButton10.Visible = True
            lblchkvcr.Text = "1"
            tabindex21true()
        Else
            LinkButton10.Visible = False
            lblchkvcr.Text = "0"
            tabindex21false()
        End If
        If market.SelectedValue = "LOCAL" Then
            marketlocal()
            LinkButton11.Visible = False
            LinkButton17.Visible = False
            LinkButton21.Visible = False
            Label56.Visible = False
        ElseIf market.SelectedValue = "IMPORT" Then
            LinkButton11.Visible = True
            LinkButton17.Visible = True
            LinkButton21.Visible = True
            Label56.Visible = True
        End If
    End Sub

    Protected Sub amtimportir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amtimportir.TextChanged
        amtimportir.Text = ToMaskEdit(ToDouble(amtimportir.Text), 4)
    End Sub

    Protected Sub amtVoucher_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amtVoucher.TextChanged
        Dim text As System.Web.UI.WebControls.TextBox = CType(sender, System.Web.UI.WebControls.TextBox)
        text.Text = ToMaskEdit(ToDouble(text.Text), 4)
    End Sub

    Protected Sub btnlistvcr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnlistvcr.Click

        If txtnovcr.Text = "" Then
            showMessage("Please Fill No Voucher !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If descVoucher.Text = "" Then
            showMessage("Please Fill Description Voucher !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If amtVoucher.Text = "" Or amtVoucher.Text = 0 Then
            showMessage("Please Fill Amount Voucher !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If Session("tblGVvoucher") Is Nothing Then
            createtblGVvoucher()
        End If

        Dim objTable As DataTable
        objTable = Session("tblGVvoucher")
        If I_U4.Text = "New Detail Voucher" Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "voucherno='" & txtnovcr.Text & "'"
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah di tambahkan sebelumnya, Silahkan cek ulang!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                objTable = Session("tblGVvoucher") : GVvoucher.DataSource = objTable
                GVvoucher.DataBind() : GVvoucher.Visible = True
                Exit Sub
            End If
            dv.RowFilter = ""
        Else

            'If Not Session("TblDtl") Is Nothing Then
            '    Dim itemtab As DataTable = Session("TblDtl")
            '    If itemtab.Rows.Count - 1 > 0 Then
            '        If amtVoucher.Text < temptakvalue.Text Then
            '            showMessage("Nilai Voucher tidak boleh lebih kecil dari sebelumnya atau delete item yang telah di input kan !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            '            Exit Sub
            '        End If
            '    End If
            'End If

            Dim dv As DataView = objTable.DefaultView

            dv.RowFilter = "voucherno='" & txtnovcr.Text & "' and voucherseq <> " & trnbelidtlseqvcr.Text & ""
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah di tambahkan sebelumnya, Silahkan cek ulang!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                objTable = Session("tblGVvoucher") : GVvoucher.DataSource = objTable
                GVvoucher.DataBind() : GVvoucher.Visible = True
                Exit Sub
            End If
            dv.RowFilter = ""
        End If

        If ToDouble(amtVoucher.Text) < 0 Then
            showMessage("Amount Voucher tidak boleh minus!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        'start insert into list
        Dim objRow As DataRow
        If I_U4.Text = "New Detail Voucher" Then
            objRow = objTable.NewRow()
            objRow("voucherseq") = objTable.Rows.Count + 1
        Else 'update
            objRow = objTable.Rows(trnbelidtlseqvcr.Text - 1)
            objRow.BeginEdit()
        End If

        objRow("cmpcode") = CompnyCode
        objRow("trnbelino") = trnbelipono.Text
        objRow("voucherno") = txtnovcr.Text
        objRow("voucherdesc") = descVoucher.Text
        objRow("amtvoucher") = ToMaskEdit(ToDouble(amtVoucher.Text), 4)
        objRow("voucherdate") = toDate(datevoucher.Text)
        objRow("vouchernote") = voucherNote.Text
        objRow("currencyoid") = currencyoidvcr.SelectedValue
        If curratevcr.Text = "" Then
            showMessage("Rate Currency Belum Di isi !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If
        objRow("currencyrate") = curratevcr.Text

        '  -- start --- isi total amount voucher

        '  -- finish --- isi total amount voucher

        If temptotvcr.Text < 0 Then
            objRow.CancelEdit()
            showMessage("Total Voucher yang di Akumulasi Melebihi Total Voucher yang ada!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            If I_U4.Text = "New Detail Voucher" Then
                objTable.Rows.Add(objRow)
            Else 'update
                objRow.EndEdit()
            End If

            Session("tblGVvoucher") = objTable
            ClearDetailvoucher()
            fillvoucher()
            GVvoucher.Visible = True
            GVvoucher.DataSource = objTable
            GVvoucher.DataBind()

            trnbelidtlseqvcr.Text = objTable.Rows.Count + 1
            ddlUnit.Items.Clear()
            GVvoucher.SelectedIndex = -1
            ClearDetail()
        End If

    End Sub

    Protected Sub GVvoucher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVvoucher.SelectedIndexChanged
        Try
            trnbelidtlseqvcr.Text = GVvoucher.SelectedDataKey(0).ToString().Trim
            If Session("tblGVvoucher") Is Nothing = False Then
                I_U4.Text = "Update Detail Voucher"
                Dim objTable As DataTable
                objTable = Session("tblGVvoucher")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "voucherseq=" & trnbelidtlseqvcr.Text
                txtnovcr.Text = dv.Item(0).Item("voucherno")
                descVoucher.Text = dv.Item(0).Item("voucherdesc")
                amtVoucher.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("amtvoucher")), 4)
                temptakvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("amtvoucher")), 4)

                datevoucher.Text = toDate(dv.Item(0).Item("voucherdate"))
                voucherNote.Text = dv.Item(0).Item("vouchernote")
                currencyoidvcr.SelectedValue = dv.Item(0).Item("currencyoid")
                curratevcr.Text = dv.Item(0).Item("currencyrate")
                dv.RowFilter = ""
            End If

        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
    End Sub  

    Protected Sub currencyoidvcr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sErr As String = ""

        If datevoucher.Text = "" Then
            showMessage("Please fill Voucher Date first!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            If Not IsValidDate(datevoucher.Text, "dd/MM/yyyy", sErr) Then
                showMessage("Voucher Date is invalid.!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")

                Exit Sub
            End If
        End If
        Dim cRate As New ClassRate()

        If currencyoid.SelectedValue <> "" Then
            trnbelipodate.Text = Format(CDate(Now), "dd/MM/yyyy")
            cRate.SetRateValue(CInt(currencyoidvcr.SelectedValue), toDate(datevoucher.Text))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage("Please Create Rate for This Week!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage("Please Create Rate for This Month!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            rate2oidvcr.Text = cRate.GetRateMonthlyOid
            curratevcr.Text = ToMaskEdit2(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
        End If
        datevoucher.Text = Format(CDate(Now), "dd/MM/yyyy")
    End Sub

    Protected Sub clearvcr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetailvoucher()
    End Sub

    Protected Sub GVvoucher_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
    End Sub

    Protected Sub GVvoucher_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVvoucher.PageIndex = e.NewPageIndex
        GVvoucher.Visible = True
        GVvoucher.DataSource = Session("tblGVvoucher")
        GVvoucher.DataBind()
    End Sub

    Protected Sub GVvoucher_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objTable As DataTable = Session("tblGVvoucher")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim objRows As DataRow = objRow(e.RowIndex)

        objTable.AcceptChanges()
        objTable.Rows.Remove(objRow(e.RowIndex))

        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("voucherseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("tblGVvoucher") = objTable
        GVvoucher.DataSource = objTable
        GVvoucher.DataBind()
        fillvoucher()
    End Sub

    Protected Sub searchvoucher_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindvoucher()
        gvitemvoucher.Visible = True
    End Sub 

    Protected Sub gvitemvoucher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtnovcr.Text = gvitemvoucher.SelectedDataKey(1).ToString().Trim
        descVoucher.Text = gvitemvoucher.SelectedDataKey(2).ToString().Trim
        gvitemvoucher.Visible = False
    End Sub

    Protected Sub fillvoucher()
        Dim totalvoucher, sisavoucher As Double

        If Session("tblGVvoucher") IsNot Nothing Then
            Dim objtable As DataTable = Session("tblGVvoucher")
            If objtable.Rows.Count > 0 Then
                For C1 As Int16 = 0 To objtable.Rows.Count - 1
                    totalvoucher += ToDouble(objtable.Rows(C1).Item("amtvoucher").ToString)
                Next
            End If
        End If
        totvoucher.Text = ToMaskEdit(totalvoucher, 3)
        sisavoucher = ToMaskEdit(totvoucher.Text, 3)

        If Session("tblDtl") IsNot Nothing Then
            Dim objtable As DataTable = Session("tblDtl")
            If objtable.Rows.Count > 0 Then
                For C2 As Integer = 0 To objtable.Rows.Count - 1
                    sisavoucher -= ToDouble(objtable.Rows(C2).Item("DiscAmt2").ToString)
                Next
            End If
            temptotvcr.Text = ToMaskEdit(ToDouble(sisavoucher), 3)
        Else
            temptotvcr.Text = ToMaskEdit(totvoucher.Text, 3)
        End If
    End Sub

    Protected Sub refillvoucher()
        Session("temptotvoucher") = totvoucher.Text
        If Not Session("TabelDtl") Is Nothing Then
            Try
                For c1 As Integer = 0 To Session("TabelDtl").rows.count - 1
                    Session("temptotvoucher") = Session("temptotvoucher") - Session("TabelDtl").rows(c1).item("DiscAmt2")
                Next
            Catch ex As Exception

            End Try
            temptotvcr.Text = ToMaskEdit(ToDouble(Session("temptotvoucher")), 4)
        Else
            temptotvcr.Text = ToMaskEdit(ToDouble(totvoucher.Text), 4)
        End If

    End Sub

    Protected Sub costEkspedisi_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles costEkspedisi.TextChanged
        Dim text As System.Web.UI.WebControls.TextBox = CType(sender, System.Web.UI.WebControls.TextBox)
        text.Text = ToMaskEdit(ToDouble(text.Text), 4)
    End Sub

    Protected Sub bonus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If bonus.Checked = True Then
            lblbonus.Text = "Yes"
        Else
            lblbonus.Text = "No"
        End If
    End Sub

    Protected Sub gvitemvoucher_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvitemvoucher.PageIndex = e.NewPageIndex
        bindvoucher()
    End Sub

    Protected Sub ImageButton5_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        descVoucher.Text = ""
        If gvitemvoucher.Visible = True Then
            gvitemvoucher.DataSource = Nothing
            gvitemvoucher.DataBind()
            gvitemvoucher.Visible = False
        End If
    End Sub

    Protected Sub cb1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If cb1.Checked = True Then
            beratBarang.Enabled = True : beratBarang.CssClass = "inpText"
            beratVolume.Enabled = False : beratVolume.CssClass = "inpTextDisabled"
            beratVolume.Text = ""
            cb2.Checked = False
        Else
            beratVolume.Enabled = True : beratVolume.CssClass = "inpText"
            beratBarang.Enabled = False : beratBarang.CssClass = "inpTextDisabled"
            beratBarang.Text = ""
        End If
    End Sub

    Protected Sub cb2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If cb2.Checked = True Then
            beratVolume.Enabled = True : beratVolume.CssClass = "inpText"
            beratBarang.Enabled = False : beratBarang.CssClass = "inpTextDisabled"
            cb1.Checked = False
            beratBarang.Text = ""
        Else
            beratBarang.Enabled = True : beratBarang.CssClass = "inpText"
            beratVolume.Enabled = False : beratVolume.CssClass = "inpTextDisabled"
            beratVolume.Text = ""
        End If
    End Sub

    Protected Sub CabangNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CabangNya.SelectedIndexChanged
        initCbg()
        toCabang.SelectedValue = CabangNya.SelectedValue
    End Sub

    Protected Sub FilterPeriod1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterPeriod1.TextChanged
        chkPeriod.Checked = True
    End Sub

    Private Sub CekColom(ByVal isTrue As Boolean)
        tbldtl.Columns(5).Visible = isTrue
        tbldtl.Columns(6).Visible = isTrue
        tbldtl.Columns(7).Visible = isTrue
        tbldtl.Columns(8).Visible = isTrue
        tbldtl.Columns(9).Visible = isTrue 
        tbldtl.Columns(11).Visible = isTrue
        tbldtl.Columns(12).Visible = isTrue
        tbldtl.Columns(13).Visible = isTrue
        tbldtl.Columns(14).Visible = isTrue 
    End Sub

    Protected Sub TypePOnya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TypePOnya.SelectedIndexChanged
        If TypePOnya.SelectedValue = "Selisih" Then
            Label35.Visible = False : chkvcr.Visible = False
            CekColom(False)
        Else
            Label35.Visible = True : chkvcr.Visible = True
            CekColom(True)
        End If
    End Sub  

    Protected Sub tbldata_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbldata.SelectedIndexChanged
        showMessageprint(tbldata.SelectedDataKey.Item(0), tbldata.SelectedDataKey.Item(1), "")
        posting.Text = tbldata.SelectedDataKey.Item("postatus")
    End Sub   
#End Region
End Class