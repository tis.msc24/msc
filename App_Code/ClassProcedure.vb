Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class ClassProcedure
    Inherits System.Web.UI.Page

    Public Sub SetModalPopUpExtender(ByVal btnTargetButton As System.Web.UI.WebControls.Button, ByVal pnlTargetPanel As System.Web.UI.WebControls.Panel, _
        ByVal mpeTargetModalPopUp As AjaxControlToolkit.ModalPopupExtender, ByVal bModalPopUpState As Boolean)
        btnTargetButton.Visible = bModalPopUpState
        pnlTargetPanel.Visible = bModalPopUpState
        If bModalPopUpState Then
            mpeTargetModalPopUp.Show()
        Else
            mpeTargetModalPopUp.Hide()
        End If
    End Sub

    Public Sub CheckRegionalSetting()
        If (Now.ToLongTimeString.IndexOf("AM") = -1 Or Now.ToLongTimeString.IndexOf("PM") = -1) = False Then
            Response.Redirect("~\Other\login.aspx")
        End If
    End Sub
    Public Sub SetFocusToControl(ByVal pPage As System.Web.UI.Page, ByVal cControl As System.Web.UI.Control)
        Dim cs As ScriptManager = ScriptManager.GetCurrent(pPage)
        cs.SetFocus(cControl.ClientID)
    End Sub
    Public Sub DisposeGridView(ByVal objDisposedGridView As GridView)
        objDisposedGridView.SelectedIndex = -1
        objDisposedGridView.DataSource = Nothing
        objDisposedGridView.DataBind()
    End Sub

    Public Sub SetDBLogonReport(ByVal reportDoc As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = reportDoc.Database.Tables
        Dim crConnInfo As New CrystalDecisions.Shared.ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
            .IntegratedSecurity = True
        End With
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = crConnInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
	' Masmuh
	Public Sub SetDBLogonForReport2(ByVal reportDoc As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = reportDoc.Database.Tables
        Dim crConnInfo As New CrystalDecisions.Shared.ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
            '.UserID = System.Configuration.ConfigurationManager.AppSettings("DB-User")
            '.Password = System.Configuration.ConfigurationManager.AppSettings("DB-Password")
            .IntegratedSecurity = True
        End With
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = crConnInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
    Public Sub SetDBLogonForReport(ByVal reportDoc As CrystalDecisions.CrystalReports.Engine.ReportDocument, _
    ByVal serverName As String, ByVal dbName As String)
        Dim connection, connection2 As CrystalDecisions.Shared.IConnectionInfo
        ' Set Database Logon to main report
        For Each connection In reportDoc.DataSourceConnections
            connection.SetConnection(serverName, dbName, True)
            ' Set Database Logon to subreport
            Dim subreport As CrystalDecisions.CrystalReports.Engine.ReportDocument
            For Each subreport In reportDoc.Subreports
                For Each connection2 In subreport.DataSourceConnections
                    connection2.SetConnection(serverName, dbName, True)
                Next
            Next
        Next
    End Sub
    Public Sub ResetSessionOnForm()
        '--> simpan session k variabel spy tidak hilang
        Dim sUserID As String = Session("UserID")
        Dim dtSpecAccess As DataTable = Session("SpecialAccess")
        Dim dtRole As DataTable = Session("Role")
        Dim sCmpcode As String = Session("CompnyCode")
        Dim branch As String = Session("gendesc")
        Dim branch_id As String = Session("branch_code")
        Session.Clear()  ' -->>  clear all session 
        '--> insert lagi session yg disimpan dan create session 
        Session("UserID") = sUserID
        Session("SpecialAccess") = dtSpecAccess
        Session("Role") = dtRole
        Session("CompnyCode") = sCmpcode
        Session("gendesc") = branch
        Session("branch_code") = branch_id
    End Sub

End Class
