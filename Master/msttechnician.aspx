<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="msttechnician.aspx.vb" Inherits="Master_msttechnician" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Maroon" Text=".: Data Teknisi" Width="192px"></asp:Label>
            </th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="height: 3px">
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top" colSpan=3></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 61px">Filter</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilter" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w69"><asp:ListItem Value="nip">NIP</asp:ListItem>
<asp:ListItem Value="name">Nama</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="tbfilter" runat="server" CssClass="inpText" __designer:wfdid="w70"></asp:TextBox>&nbsp; <asp:ImageButton id="ibfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top" __designer:wfdid="w71"></asp:ImageButton>&nbsp; <asp:ImageButton id="ibviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top" __designer:wfdid="w72"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=3></TD></TR><TR><TD style="HEIGHT: 15px" colSpan=3><asp:GridView style="WIDTH: 100%" id="gvdata" runat="server" ForeColor="#333333" __designer:wfdid="w73" BorderStyle="Solid" BorderColor="Blue" BorderWidth="1px" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="personoid" GridLines="None">
<RowStyle BackColor="#EFF3FB" BorderColor="White"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="personoid" DataNavigateUrlFormatString="~/Master/msttechnician.aspx?oid={0}" DataTextField="personnip" HeaderText="Nip">
<HeaderStyle HorizontalAlign="Center" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" Width="25%"></HeaderStyle>

<ItemStyle BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" Width="25%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="personname" HeaderText="Nama">
<HeaderStyle HorizontalAlign="Center" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>

<ItemStyle BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personcrtaddress" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Center" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>

<ItemStyle BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red" __designer:wfdid="w246" Text="Data not found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF" BorderColor="White"></EditRowStyle>

<AlternatingRowStyle BackColor="White" BorderColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <span style="font-size: 12px; font-weight: bold;">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />
                                &nbsp;:: List Of Teknisi</span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <span style="font-size: 12px; font-weight: bold;">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />
                                &nbsp;:: Form Teknisi</span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 700px"><TBODY><TR><TD style="VERTICAL-ALIGN: middle" colSpan=3><asp:Label id="lbltitle" runat="server" Font-Size="10px" ForeColor="Red" __designer:wfdid="w51" Visible="False" Font-Underline="True"></asp:Label> <asp:Label id="lblcurrentuser" runat="server" __designer:wfdid="w52" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">NIP</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tbnip" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w53" MaxLength="20"></asp:TextBox> <asp:Label id="lblnip" runat="server" __designer:wfdid="w54" Visible="False"></asp:Label>&nbsp;<asp:DropDownList id="jobposition" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w99" Visible="False" OnSelectedIndexChanged="jobposition_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Nama Teknisi&nbsp;<SPAN style="COLOR: red">*</SPAN></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tbname" runat="server" Width="440px" CssClass="inpText" __designer:wfdid="w55" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">No. Kartu Tanda Pengenal</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tbidno" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w56" MaxLength="25"></asp:TextBox> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Jenis Kelamin</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:DropDownList id="ddlgender" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w57"><asp:ListItem Value="Male">Laki-laki</asp:ListItem>
<asp:ListItem Value="Female">Perempuan</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Status Pernikahan</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:DropDownList id="ddlmarital" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w58"><asp:ListItem Value="Single">Belum Menikah</asp:ListItem>
<asp:ListItem Value="Married">Menikah</asp:ListItem>
<asp:ListItem Value="Widow">Janda</asp:ListItem>
<asp:ListItem Value="Widower">Duda</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Agama</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:DropDownList id="ddlreligion" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w59"><asp:ListItem Value="Islam">Islam</asp:ListItem>
<asp:ListItem Value="Christian">Kristen</asp:ListItem>
<asp:ListItem Value="Catholic">Katolik</asp:ListItem>
<asp:ListItem Value="Hindu">Hindu</asp:ListItem>
<asp:ListItem Value="Budha">Budha</asp:ListItem>
<asp:ListItem Value="Konghucu">Konghucu</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Tempat&nbsp;/ Tanggal Lahir&nbsp;<SPAN style="COLOR: red">*</SPAN></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tbbirthplace" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w60" MaxLength="30"></asp:TextBox>&nbsp; /&nbsp;&nbsp;<asp:TextBox style="TEXT-ALIGN: right" id="tbbirthdate" runat="server" Width="56px" CssClass="inpText" __designer:wfdid="w61"></asp:TextBox>&nbsp; <asp:ImageButton id="ibbirthdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w62"></asp:ImageButton>&nbsp;&nbsp; <SPAN style="FONT-SIZE: 11px; COLOR: red">(dd/mm/yyyy)<ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w63" TargetControlID="tbbirthdate" PopupButtonID="ibbirthdate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w64" TargetControlID="tbbirthdate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></SPAN></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Alamat Sekarang</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px"></TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jalan <SPAN style="COLOR: red">*</SPAN></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tbcurstreet" runat="server" Width="432px" CssClass="inpText" __designer:wfdid="w65" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kota</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:DropDownList id="ddlcurcity" runat="server" Width="216px" CssClass="inpText" __designer:wfdid="w66"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Provinsi</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tbcurprovince" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w67" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kode Pos</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tbcurpostcode" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w68" MaxLength="10"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Alamat Asal</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px"></TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jalan</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tboristreet" runat="server" Width="432px" CssClass="inpText" __designer:wfdid="w69" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kota</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:DropDownList id="ddloricity" runat="server" Width="216px" CssClass="inpText" __designer:wfdid="w70"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">&nbsp; &nbsp; &nbsp; Provinsi</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tboriprovince" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w71" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kode Pos</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tboripostcode" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w72" MaxLength="10"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">No. Telepon&nbsp;1 <SPAN style="COLOR: red">*</SPAN></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="tbphone1" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w73" MaxLength="25"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w74" TargetControlID="tbphone1" ValidChars="1234567890 ()+"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px">No. Telepon&nbsp;2</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="tbphone2" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w75" MaxLength="25"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w76" TargetControlID="tbphone2" ValidChars="1234567890 ()+"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Email</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tbemail" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w77" MaxLength="50"></asp:TextBox> <BR /><asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" __designer:wfdid="w78" ErrorMessage="Warning : Format email tidak valid! <br />Ex: mail@yahoo.com" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="tbemail"></asp:RegularExpressionValidator></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Pendidikan Terakhir</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:DropDownList id="ddllastedu" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w79"><asp:ListItem Value="SD">SD</asp:ListItem>
<asp:ListItem Value="SMP">SMP</asp:ListItem>
<asp:ListItem Value="SMA">SMA</asp:ListItem>
<asp:ListItem Value="D1">D1</asp:ListItem>
<asp:ListItem Value="D3">D3</asp:ListItem>
<asp:ListItem Value="D4">D4</asp:ListItem>
<asp:ListItem Value="S1">S1</asp:ListItem>
<asp:ListItem Value="S2">S2</asp:ListItem>
<asp:ListItem Value="S3">S3</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Nama Sekolah/Institut</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tblasteduschool" runat="server" Width="432px" CssClass="inpText" __designer:wfdid="w80" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Kota</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox id="tblasteducity" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w81" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Status Kerja</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:DropDownList id="ddlstatus" runat="server" Width="216px" CssClass="inpText" __designer:wfdid="w82"><asp:ListItem>Permanen</asp:ListItem>
<asp:ListItem Value="Contract">Kontrak</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Status Karyawan</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:DropDownList id="ddlempstatus" runat="server" Width="216px" CssClass="inpText" __designer:wfdid="w83"><asp:ListItem Value="Aktif">Aktif</asp:ListItem>
<asp:ListItem Value="taktif">Tidak Aktif</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Tanggal Mulai Bekerja</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:TextBox style="TEXT-ALIGN: right" id="tbjoindate" runat="server" Width="56px" CssClass="inpText" __designer:wfdid="w84"></asp:TextBox>&nbsp; <asp:ImageButton id="ibjoindate" runat="server" ImageUrl="~/Images/oCalendar.gif" __designer:wfdid="w85"></asp:ImageButton>&nbsp;&nbsp; <SPAN style="FONT-SIZE: 11px; COLOR: red">(dd/mm/yyyy)</SPAN></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px"></TD><TD style="VERTICAL-ALIGN: top"><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w86" TargetControlID="tbjoindate" PopupButtonID="ibjoindate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w87" TargetControlID="tbjoindate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px">Catatan</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="tbnote" runat="server" Width="280px" Height="80px" CssClass="inpText" __designer:wfdid="w88" TextMode="MultiLine"></asp:TextBox><BR /><asp:Label id="Label2" runat="server" ForeColor="Red" Text="* Maks 250 karakter" __designer:wfdid="w89"></asp:Label><BR /><asp:RegularExpressionValidator id="RegularExpressionValidator2" runat="server" __designer:wfdid="w90" ErrorMessage="* Warning : Catatan lebih dari 250 karakter!" ValidationExpression="^[\s\S]{0,240}$" ControlToValidate="tbnote"></asp:RegularExpressionValidator></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 180px; HEIGHT: 20px">Foto</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 10px; HEIGHT: 20px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:FileUpload style="MARGIN-RIGHT: 10px" id="tbfileupload" runat="server" Width="224px" CssClass="inpText" __designer:wfdid="w91"></asp:FileUpload><asp:ImageButton id="ibupload" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w92"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px" colSpan=2></TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 20px"><asp:Image style="WIDTH: 120px; HEIGHT: 160px" id="imgphoto" runat="server" Width="144px" ImageUrl="~/Images/no-image.png" __designer:wfdid="w93"></asp:Image>&nbsp; <asp:Label id="lblphoto" runat="server" __designer:wfdid="w94" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 20px" colSpan=3><BR /><asp:Label id="lblcreate" runat="server" __designer:wfdid="w95"></asp:Label> <BR /><BR /></TD></TR><TR><TD style="HEIGHT: 20px" colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibsave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w96"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False" __designer:wfdid="w97"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w98" Visible="False"></asp:ImageButton> </TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibupload"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer><br />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 100%">
                </td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel id="uppopupmsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpepopupmsg" runat="server" Drag="True" PopupDragHandleControlID="lblcaption" BackgroundCssClass="modalBackground" PopupControlID="pnlpopupmsg" DropShadow="True" TargetControlID="bepopupmsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bepopupmsg" runat="server" BorderColor="Transparent" Visible="False" BackColor="Transparent" BorderStyle="None" CausesValidation="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>   
</asp:Content>

