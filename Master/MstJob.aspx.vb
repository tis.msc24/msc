'Prgmr :4ncIiI<a | LastUpdt:11.23.2012 - MM/dd/YYYY
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction

Partial Class Master_Master_Job
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Public DefCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub checkDDL()
        'Space for checking DDL
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 2 Then 'Warning
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText")
        FilterDDL.SelectedIndex = Session("FilterDDL")
        If Session("FilterDDL") Is Nothing = False Then
            If Session("FilterText").ToString.Trim <> "" Then
                sWhere = " where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value.Trim & ") like '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%'"
            End If
        Else
            sWhere = " where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value.Trim & ") like '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%'"
        End If

        If sCompCode <> "" Then
            sSql = "select a.cmpcode, a.itservoid, a.itservcode, c.gendesc, a.itservdesc, a.itservtypeoid, b.gendesc as itservtype, a.itservtarif, a.itservpoint, a.itservnote, a.updtime from QL_mstitemserv a INNER JOIN ql_mstgen b ON a.itservtypeoid = b.genoid INNER JOIN QL_mstgen c on a.Typejob=c.genoid " & sWhere & IIf(sWhere <> "", " and ", " where ") & "upper(a.cmpcode) like '%" & sCompCode.ToUpper & "%' order by a.itservtypeoid"
        Else
            sSql = "select a.cmpcode, a.itservoid, a.itservcode, a.itservdesc, a.itservtypeoid, b.gendesc as itservtype, a.itservtarif, a.itservpoint, a.itservnote, a.updtime from QL_mstitemserv a INNER JOIN ql_mstgen b ON a.itservtypeoid = b.genoid" & sWhere & "%' order by a.itservtypeoid"
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstjob.DataSource = objDs.Tables("data")
        GVmstjob.DataBind()
        GVmstjob.PageIndex = 0
    End Sub

    Public Sub FillTextBox(ByVal vitservoid As String)
        sSql = "SELECT cmpcode,itservoid,itservcode,itservdesc,itservtypeoid,itservtarif,itservpoint, itservnote,upduser,updtime,typejob FROM QL_mstitemserv where cmpcode='" & CompnyCode & "' and itservoid = '" & vitservoid & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        While xReader.Read
            itservoid.Text = Trim(xReader.GetValue(1).ToString)
            itservcode.Text = Trim(xReader.GetValue(2).ToString)
            itservdesc.SelectedValue = Trim(xReader.GetValue(3).ToString)
            itservtarif.Text = ToMaskEdit(ToDouble(Trim(xReader.GetValue(5).ToString)), 3)
            itservpoint.Text = ToMaskEdit(ToDouble(Trim(xReader.GetValue(6).ToString)), 3)
            itservnote.Text = Trim(xReader.GetValue(7).ToString)
            itservtypeoid.SelectedValue = Trim(xReader("itservtypeoid").ToString)
            updUser.Text = Trim(xReader.GetValue(8).ToString)
            updTime.Text = Trim(xReader.GetValue(9).ToString)
            Jenisddl.SelectedValue = Trim(xReader("typejob").ToString)

        End While
        xReader.Close()
        conn.Close()
    End Sub

    Sub InitAllDDL()
        sSql = "select genoid, gendesc from QL_mstgen where gengroup in ('SERVICETYPE') and cmpcode like '%" & CompnyCode & "%' order by gendesc desc"
        FillDDL(itservtypeoid, sSql)

        sSql = "Select genoid,gendesc From QL_mstgen Where gengroup='ITEMSUBGROUP' And cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(Jenisddl, sSql)

        Dim sMsg As String = ""
        If itservtypeoid.Items.Count = 0 Then
            sMsg &= "- Tolong buat pada Master General dengan nama grup Service Type !"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

    End Sub

    Public Sub GenerateGenID()
        itservoid.Text = GenerateID("QL_mstitemserv", CompnyCode)
    End Sub

    Public Sub GenCodeMstJob()

        Dim sNo As String = "IS" & "-" & Format(GetServerTime(), "yy") & "-"

        Dim sJobNo As String = ""

        ' GENERATE NO
        sSql = "SELECT CAST(MAX(RIGHT(itservcode," & DefCounter & ")) AS INTEGER) FROM QL_mstitemserv WHERE cmpcode='" & CompnyCode & "' AND itservcode LIKE '" & sNo & "%'"
        xCmd.CommandText = sSql
        xCmd.Connection.Open()

        Try
            sJobNo = GenNumberString(sNo, "", xCmd.ExecuteScalar + 1, DefCounter)
        Catch ex As Exception
            sJobNo = GenNumberString(sNo, "", 1, DefCounter)
        End Try
        xCmd.Connection.Close()

        itservcode.Text = sJobNo
    End Sub

    Sub clearItem()
        itservcode.Text = ""
        Jenisddl.SelectedIndex = 0
        itservdesc.SelectedIndex = 0
        itservtypeoid.SelectedIndex = 0
        itservtarif.Text = ""
        itservpoint.Text = ""
        itservnote.Text = ""
        updUser.Text = Session("UserID")
        updTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)

        Dim sMsg As String = ""
        If itservtypeoid.Items.Count = 0 Then
            sMsg &= "- Tolong buat pada Master General dengan nama grup ServiceType !"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/other/login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("AprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Response.Redirect("MstJob.aspx")
        End If
        Session("idPage") = Request.QueryString("idPage")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin akan menghapus data ini ?');")
        If Not IsPostBack Then
            'clearItem()
            BindData(CompnyCode)
            InitAllDDL()
            If Session("idPage") <> Nothing And Session("idPage") <> "" Then
                i_u.Text = "Update"
                FillTextBox(Session("idPage"))
                TabContainer1.ActiveTabIndex = 1
                btnDelete.Visible = True
            Else
                i_u.Text = "New"
                GenerateGenID()
                GenCodeMstJob()
                updUser.Text = Session("UserID")
                updTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session("FilterText") = "" : Session("FilterDDL") = 0
        BindData(CompnyCode)
        GVmstjob.PageIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        'If itservtypeoid.Items.Count = 0 Then
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "WRNING_MSG", String.Format("<script>alert('{0}')</script>", "Tolong buat pada Master General dengan nama grup ServiceType !"))
        '    Exit Sub
        'End If

        Dim sMsg As String = ""

        If itservnote.Text.Length >= 70 Then
            sMsg &= "- Maksimal karakter untuk Keterangan adalah 70 karakter ! <BR>"
        End If

        If itservdesc.Text.Length >= 70 Then
            sMsg &= "- Maksimal karakter untuk Deskripsi adalah 70 karakter ! <BR>"
        End If

        If itservtypeoid.Items.Count = 0 Then
            sMsg &= "- Tolong buat pada Master General dengan nama grup ServiceType ! <BR>"
        End If

        If itservcode.Text.Trim = "" Then
            sMsg &= "- Tolong isi Kode !<br />"
        End If
        If itservdesc.Text.Trim = "" Then
            sMsg &= "- Tolong isi Deskripsi !<br />"
        End If
        If itservtarif.Text.Trim = "" And ToDouble(itservtarif.Text) <= 0 Then
            sMsg &= "- Tolong isi Tarif Pengerjaan !<br />"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If

        'check if any "double" on description
        If i_u.Text <> "Update" Then
            Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_mstitemserv WHERE itservdesc = '" & Tchar(itservdesc.Text) & "' and typejob = " & Jenisddl.SelectedValue & " "
            If Session("oid") = Nothing Or Session("oid") = "" Then
            Else : sSqlCheck1 &= " AND itservoid <> " & itservoid.Text
            End If
            If cKon.ambilscalar(sSqlCheck1) > 0 Then
                showMessage("Deskripsi sudah ada di database !!", 2)
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)

            hiddencode.Text = itservtypeoid.SelectedValue

            If Session("idPage") = Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                sSql = "insert into QL_mstitemserv(cmpcode,itservoid,itservcode,itservdesc,itservtypeoid,itservtarif,itservpoint, itservnote,createuser,createtime,upduser,updtime,typejob)" & _
                "values ('" & CompnyCode & "', " & itservoid.Text & ", '" & Tchar(itservcode.Text.Trim) & "', '" & Tchar(itservdesc.Text.Trim) & "', " & hiddencode.Text & ", '" & ToDouble(itservtarif.Text.Trim) & "', '" & ToDouble(itservpoint.Text.Trim) & "', '" & Tchar(itservnote.Text.Trim) & "', '" & updUser.Text & "', current_timestamp,'" & updUser.Text & "', current_timestamp," & Jenisddl.SelectedValue & ")"
            Else
                sSql = "update QL_mstitemserv set itservcode='" & Tchar(itservcode.Text) & "', itservdesc = '" & Tchar(itservdesc.Text) & "', itservtypeoid = " & hiddencode.Text & ", itservtarif = '" & ToDouble(itservtarif.Text.Trim) & "', itservpoint = '" & ToDouble(itservpoint.Text.Trim) & "', itservnote = '" & Tchar(itservnote.Text.Trim) & "', upduser = '" & updUser.Text & "', updtime = current_timestamp ,typejob = " & Jenisddl.SelectedValue & " where cmpcode like '%" & CompnyCode & "%' and itservoid = " & itservoid.Text
            End If
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            If Session("idPage") = Nothing Or Session("idPage") = "" Then
                sSql = "update QL_mstoid set lastoid = " & itservoid.Text & " where tablename like '%QL_mstitemserv%' and cmpcode like '%" & CompnyCode & "%'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Session("idPage") = Nothing
        Response.Redirect("~\master\MstJob.aspx?page=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        'clearItem()
        'Dim sMsg As String = ""
        'If itservtypeoid.Items.Count = 0 Then
        '    sMsg &= "- Tolong buat pada Master General dengan nama grup ServiceType !"
        'End If
        'If sMsg <> "" Then
        '    showMessage(sMsg, 2) : Exit Sub
        'End If
        Response.Redirect("~\master\MstJob.aspx?page=true")
    End Sub

    Protected Sub imbPopUpOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPopUpOK.Click

        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        bePopUpMsg.Visible = False
        pnlPopUpMsg.Visible = False
    End Sub

    Protected Sub GVmstjob_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstjob.PageIndexChanging
        GVmstjob.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        If itservoid.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please fill Job ID"))
        End If

        Dim sColmnName() As String = {"itservoid", "outputoid"}
        Dim sTable() As String = {"QL_TRNMECHCHECK", "QL_trnspkProcessItemResult"}

        If CheckDataExists(itservoid.Text, sColmnName, sTable) = True Then
            showMessage("Tidak bisa dihapus karena data telah digunakan oleh transaksi lainnya !", 2)
            Exit Sub
        End If

        If DeleteData("QL_mstitemserv", "itservoid", itservoid.Text, CompnyCode) = True Then
            GenerateGenID()
            GenCodeMstJob()
            clearItem()
            Session("idPage") = ""
        Else : Exit Sub
        End If
        Response.Redirect("~\master\MstJob.aspx?page=true")

    End Sub

    Protected Sub GVmstjob_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstjob.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble((e.Row.Cells(4).Text)), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble((e.Row.Cells(5).Text)), 2)
        End If
    End Sub

    Protected Sub itservpoint_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itservpoint.Text = ToMaskEdit(ToDouble(itservpoint.Text), 2)
    End Sub

    Protected Sub itservtarif_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itservtarif.Text = ToMaskEdit(ToDouble(itservtarif.Text), 2)
    End Sub
#End Region
End Class
