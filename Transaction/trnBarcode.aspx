<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnBarcode.aspx.vb" Inherits="trnBarcode" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                    Text=".: Print Barcode" Font-Names="Verdana" Font-Size="21px"></asp:Label>
            </th>
        </tr>
    </table>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch" __designer:wfdid="w57"><TABLE style="WIDTH: 970px"><TBODY><TR><TD style="TEXT-ALIGN: left" align=right colSpan=12><asp:Label id="lblError" runat="server" CssClass="Important" __designer:wfdid="w58"></asp:Label></TD></TR><TR><TD align=right></TD><TD align=right></TD><TD align=left></TD><TD align=left></TD><TD align=right></TD><TD align=right></TD><TD align=left colSpan=6></TD></TR><TR><TD style="TEXT-ALIGN: left" align=right><asp:Label id="Label3" runat="server" Width="98px" Font-Size="Small" Text="PIC" __designer:wfdid="w59"></asp:Label></TD><TD style="TEXT-ALIGN: left" align=right>:</TD><TD align=left><asp:DropDownList id="person" runat="server" Width="191px" CssClass="inpText" __designer:wfdid="w60"></asp:DropDownList></TD><TD style="WIDTH: 50px" align=left></TD><TD style="TEXT-ALIGN: left" align=right><asp:Label id="lbllokasi" runat="server" Width="98px" Font-Size="Small" Text="Lokasi" __designer:wfdid="w61"></asp:Label></TD><TD style="TEXT-ALIGN: left" align=right>:</TD><TD align=left colSpan=6><asp:DropDownList id="itemloc" runat="server" Width="191px" CssClass="inpText" __designer:wfdid="w62"></asp:DropDownList></TD></TR><TR><TD style="TEXT-ALIGN: left" align=right><asp:Label id="lbltype" runat="server" Width="98px" Font-Size="Small" Text="Grup Barang" __designer:wfdid="w63"></asp:Label></TD><TD style="TEXT-ALIGN: left" align=right>:</TD><TD align=left><asp:DropDownList id="typeM" runat="server" Width="133px" CssClass="inpText" __designer:wfdid="w64"></asp:DropDownList></TD><TD style="WIDTH: 50px" align=left></TD><TD style="TEXT-ALIGN: left" align=right><asp:Label id="lbltypeM" runat="server" Width="122px" Font-Size="Small" Text="Sub Grup Barang" __designer:wfdid="w65"></asp:Label></TD><TD style="TEXT-ALIGN: left" align=right>:</TD><TD align=left colSpan=6><asp:DropDownList id="mattype" runat="server" Width="135px" CssClass="inpText" __designer:wfdid="w66"></asp:DropDownList> <asp:DropDownList id="ddlJenis" runat="server" Width="52px" CssClass="inpText" __designer:wfdid="w67" Visible="False"><asp:ListItem Value="QL_MSTITEM">ITEM</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="TEXT-ALIGN: left" align=right><asp:Label id="Label26" runat="server" Width="128px" Font-Size="Small" Text="Nama/Kode Barang" __designer:wfdid="w68"></asp:Label></TD><TD style="TEXT-ALIGN: left" align=right>:</TD><TD align=left><asp:TextBox id="FilterText" runat="server" Width="249px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w69"></asp:TextBox></TD><TD style="WIDTH: 50px" align=left></TD><TD style="TEXT-ALIGN: left" align=right><asp:Label id="Label2" runat="server" Width="98px" Font-Size="Small" Text="Status" __designer:wfdid="w70"></asp:Label></TD><TD style="TEXT-ALIGN: left" align=right>:</TD><TD align=left colSpan=6><asp:DropDownList id="status" runat="server" Width="76px" CssClass="inpText" __designer:wfdid="w71"><asp:ListItem>Aktif</asp:ListItem>
<asp:ListItem>Tidak Aktif</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w72"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w73"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: left" align=right></TD><TD style="TEXT-ALIGN: left" align=right></TD><TD style="HEIGHT: 25px" align=left></TD><TD style="WIDTH: 50px" align=left></TD><TD style="TEXT-ALIGN: left" align=right></TD><TD style="TEXT-ALIGN: left" align=right></TD><TD align=left colSpan=6></TD></TR><TR><TD style="TEXT-ALIGN: left" align=right colSpan=12><asp:Label id="Label4" runat="server" ForeColor="Red" Text="* Klik Find Untuk menampilkan daftar item" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD style="HEIGHT: 25px; TEXT-ALIGN: left" align=right colSpan=12></TD></TR><TR><TD style="TEXT-ALIGN: left" align=right colSpan=12><asp:GridView id="gvMst" runat="server" Width="100%" Font-Size="X-Small" ForeColor="Black" BackColor="White" __designer:wfdid="w74" BorderColor="White" AllowPaging="True" BorderWidth="1px" BorderStyle="None" CellPadding="4" AutoGenerateColumns="False">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#F7F7DE" Font-Size="X-Small"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Kode"></asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang"></asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk"></asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang"></asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Sat Sdg"></asp:BoundField>
<asp:TemplateField HeaderText="Qty Print"><ItemTemplate>
<asp:TextBox id="newqty" runat="server" Width="70px" CssClass="inpText" BackColor="#C0FFC0" Text='<%# String.Format("{0:#,##0.00}", Eval("saldoawal")) %>' __designer:wfdid="w2" AutoPostBack="True" OnTextChanged="newprice_TextChanged" MaxLength="7"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w3" TargetControlID="newqty" ValidChars="1234567890.,-"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<ItemStyle Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note" Visible="False"><ItemTemplate>
<asp:TextBox id="note" runat="server" Width="250px" CssClass="inpText" Text='<%# Eval("note") %>' __designer:wfdid="w85" MaxLength="200"></asp:TextBox> 
</ItemTemplate>

<ItemStyle Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labelitemoid" runat="server" __designer:wfdid="w86" Text='<%# Eval("itemoid") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labelgenoid" runat="server" __designer:wfdid="w87" Text='<%# Eval("genoid") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#F7F7DE" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                            <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data Tidak Ditemukan !!!"></asp:Label>
                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" Font-Size="X-Small"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: bottom; HEIGHT: 35px; TEXT-ALIGN: left" align=right colSpan=12><SPAN style="FONT-SIZE: 7pt">Update terakhir oleh</SPAN>&nbsp;<asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w75"></asp:Label>&nbsp;<SPAN style="FONT-SIZE: 7pt">pada tanggal</SPAN>&nbsp;<asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w76"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: bottom; HEIGHT: 30px; TEXT-ALIGN: left" align=right colSpan=12><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSave" runat="server" ImageUrl="~/Images/barcodetoprinter.png" ImageAlign="AbsMiddle" __designer:wfdid="w77" AlternateText="Save"></asp:ImageButton><asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w78" AlternateText="Cancel"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: left" align=right colSpan=12></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="gvMst"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="WARNING" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD><asp:ImageButton id="ImageButton3" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD></TD><TD><asp:Label id="lblState" runat="server"></asp:Label></TD><TD></TD></TR><TR><TD></TD><TD align=center><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD><TD></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="WARNING"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

