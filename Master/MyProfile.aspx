<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="MyProfile.aspx.vb" Inherits="Master_MyProfile" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0"
        class="tabelhias" width="100%">
        <tr>
            <th class="header" colspan="5" valign="top">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: My Profile"></asp:Label></th>
        </tr>
        <tr>
            <th colspan="5" style="text-align: left; height: 350px;" valign="top">
                <table>
                    <tr>
                        <td align="left" style="font-weight: bold; height: 15px">
                        </td>
                        <td align="left" style="font-weight: bold; height: 15px">
                        </td>
                        <td align="left" style="height: 15px">
                        </td>
                        <td align="left" style="height: 15px">
                        </td>
                        <td align="left" style="height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 9pt;" colspan="3">
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            .: Profile Data :.</td>
                        <td align="left">
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            User ID</td>
                        <td align="left" style="font-weight: bold">
                            :</td>
                        <td align="left">
                            <asp:Label ID="userid" runat="server"></asp:Label></td>
                        <td align="left">
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            User Name</td>
                        <td align="left" style="font-weight: bold">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="username" runat="server" CssClass="inpText" MaxLength="50" Width="150px" Enabled="False"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="username"
                                ErrorMessage="* Fill User Name"></asp:RequiredFieldValidator></td>
                        <td align="left">
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="username" ValidChars="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr style="color: #000099">
                        <td align="left" style="height: 10px">
                        </td>
                        <td align="left" style="height: 10px">
                        </td>
                        <td align="left" style="height: 10px">
                        </td>
                        <td align="left" style="height: 10px">
                        </td>
                        <td align="left" style="height: 10px">
                        </td>
                    </tr>
                    <tr style="color: #000099">
                        <td align="left" style="font-weight: bold; font-size: 9pt;" colspan="3">
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            .:
                            Change My Password :.</td>
                        <td align="left" colspan="1" style="font-weight: bold; font-size: small; text-decoration: underline">
                        </td>
                        <td align="left" colspan="1" style="font-weight: bold; font-size: small; text-decoration: underline">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Old Password</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="oldPassword" runat="server" CssClass="inpText" MaxLength="50" Width="150px" TextMode="Password"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="oldPassword"
                                ErrorMessage="* Fill Old Password "></asp:RequiredFieldValidator></td>
                        <td align="left"><ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="oldPassword" ValidChars="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM ">
                        </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            New Password</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="Password1" runat="server" CssClass="inpText" MaxLength="50" Width="150px" TextMode="Password"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Password1"
                                ErrorMessage="* Fill New Password"></asp:RequiredFieldValidator></td>
                        <td align="left"><ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="Password1" ValidChars="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM ">
                        </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Re-type Password&nbsp;</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="Password2" runat="server" CssClass="inpText" MaxLength="50" Width="150px" TextMode="Password"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="Password2"
                                ErrorMessage="* Fill Re-type Password"></asp:RequiredFieldValidator></td>
                        <td align="left"><ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="Password2" ValidChars="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM ">
                        </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="font-weight: bold; height: 15px">
                        </td>
                        <td align="left" style="height: 15px">
                        </td>
                        <td align="left" colspan="2" style="height: 15px">
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="Password2"
                                ControlToValidate="Password1" ErrorMessage="* New password didn't match!!"></asp:CompareValidator></td>
                        <td align="left" colspan="1" style="height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="font-weight: bold;">
                        </td>
                        <td align="left">
                        </td>
                        <td align="left">
                            <asp:ImageButton ID="imbSave" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Save.png" />
                            <asp:ImageButton ID="imbBack" runat="server" CausesValidation="False" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/Back.png" /></td>
                        <td align="left">
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="color: red; height: 15px">
                        </td>
                        <td align="left" colspan="1" style="color: red; height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="color: red">
                            <asp:Label ID="lblWarning" runat="server"></asp:Label></td>
                        <td align="left" colspan="1" style="color: red">
                        </td>
                    </tr>
                </table>
            </th>
        </tr>
    </table>
</asp:Content>

