'Created By Widi On 11 August 2014
'Modified By Vriska On 7 October 2014
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class Accounting_trninitdpap
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cKon As New Koneksi
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"

    Public Function GetIDCB() As String
        Return Eval("cmpcode") & "," & Eval("trndpapoid")
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

   Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Function IsInputValid() As Boolean
        Try
            Dim sMsg As String = "" : Dim sErr As String = "" : Dim offDate As String = ""
            If trndpapno.Text = "" Then
                sMsg &= "-  DP No. harus diisi !!<BR>"
            End If

            If suppoid.Text = "" Then
                sMsg &= "- Supplier harus diisi !!<BR>"
            End If

            If Not IsDate(toDate(trndpapdate.Text)) Then
                sMsg &= "- Tanggal harus diisi !!<BR>"
            End If
            If IsDate(toDate(trndpapdate.Text)) = False Then
                sMsg &= "- Tanggal salah !!<BR>"
            End If

            If payduedate.Visible Then
                If Not IsDate(toDate(payduedate.Text)) Then
                    sMsg &= "- Tanggal Jatuh Tempo harus diisi !!<BR>"
                End If
                If IsDate(toDate(payduedate.Text)) = False Then
                    sMsg &= "- Tanggal Jatuh Tempo salah !!<BR>"
                End If
            End If

            If payduedate.Visible And (IsDate(toDate(payduedate.Text))) And (IsDate(toDate(trndpapdate.Text))) Then
                If CDate(toDate(payduedate.Text)) < CDate(toDate(trndpapdate.Text)) Then
                    sMsg &= "- Tanggal jatuh tempo harus >= tanggal transaksi !!<BR>"
                End If
            End If

            If payrefno.Text = "" And payreftype.SelectedValue <> "CASH" Then
                sMsg &= "-  Ref No. harus diisi !!<BR>"
            End If

            If ToDouble(trndpapamt.Text) <= 0 Then
                sMsg &= "- Total DP harus >  0 !!<BR>"
            End If
            If trndpapnote.Text.Trim.Length > 200 Then
                sMsg &= "- Note, maksimal 200 karakter !!<BR>"
            End If

            If CheckDataExists("SELECT COUNT(*) FROM QL_trndpap WHERE branch_code='" & dd_branch.SelectedValue & "' AND trndpapno='" & Tchar(trndpapno.Text) & "' AND trndpapoid<>'" & Session("oid") & "'") = True Then
                sMsg &= "-  DP No sudah ada !!<BR>"
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - Warning", 2)
                trndpapstatus.Text = "In Process"
                Exit Function
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Warning", 1)
            Return False
            Exit Function
        End Try
        Return True
    End Function

#End Region

#Region "Procedures"

    Private Sub InitAllDDL()

        If Session("branch_id") = "01" Then
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
        Else
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and gencode='" & Session("branch_id") & "'"
        End If
        FillDDL(dd_branch, sSql)
        FillDDL(dd_branchCari, sSql)

        sSql = "select currencyoid, currencycode  + '-' + currencydesc from QL_mstcurr where cmpcode='" & CompnyCode & "'"
        FillDDL(curroid, sSql)

        FillDDLAcctg(trndpapacctgoid, "VAR_DPAP", dd_branchCari.SelectedValue)
        If trndpapacctgoid.Items.Count < 1 Then
            showMessage("Isi/Buat account VAR_DPAP di master accounting!!", CompnyName & " - Warning", 1)
        End If
    End Sub

    Private Sub BindSupplier(ByVal sPlus As String)

        sSql = "select  suppoid ,suppcode, suppname  FROM QL_mstsupp s  WHERE s.cmpcode = 'MSC' " & sPlus & " ORDER BY suppname "

        'sSql = "SELECT c.* FROM QL_mstsupp c " & _
        '    "WHERE c.cmpcode='" & dd_branch.SelectedValue & "' AND suppstatus<>'DELETE' " & sPlus & " ORDER BY c.suppcode"
        FillGV(gvSupp, sSql, "QL_mstcreditcard")
    End Sub

    Private Sub BindData(ByVal sPLus As String)
        Try
            Dim tgle As Date = toDate(dateAwal.Text)
            tgle = toDate(dateAkhir.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", CompnyName & " - Warning", 1)
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then

            showMessage("Second/End Period must be greater than First Period !!", CompnyName & " - Warning", 1)
            Exit Sub
        End If

        sSql = "SELECT ar.trndpapoid,ar.trndpapno,ar.trndpapdate,ar.trndpapamt, ar.trndpapstatus,ar.trndpapnote,trndpapflag,payreftype giroNo,ar.cmpcode,g.gendesc as outlet,suppname,payreftype, 0 AS selected FROM QL_trndpap ar INNER JOIN QL_mstgen g ON g.gencode = ar.branch_code and g.gengroup = 'cabang' INNER JOIN QL_mstsupp s ON ar.suppoid=s.suppoid AND ar.cmpcode=s.cmpcode WHERE ar.cmpcode='" & CompnyCode & "'  and trndpapoid<=0 AND trndpapstatus <>'DELETE'" & sPLus & " " & IIf(statuse.SelectedValue = "ALL", "", " and ar.trndpapstatus='" & statuse.SelectedValue & "'") & " "
        sSql += " ORDER BY trndpapno"

        Dim objTable As DataTable = GetDataTable(sSql, "QL_trndpap")
        Session("TblDPAPInit") = objTable
        gvMst.DataSource = Session("TblDPAPInit")
        gvMst.DataBind()

        UnabledCheckBox()
    End Sub

    Private Sub DDLPayreftypeChange()
        If payreftype.SelectedValue = "CASH" Then
            tr1.Visible = False : tr2.Visible = False
        Else
            tr1.Visible = True : tr2.Visible = True
        End If
    End Sub

    Private Sub FillTextBox(ByVal iOid As Integer)
        sSql = "SELECT ar.trndpapoid,ar.cashbankoid,ar.trndpapno,ar.trndpapdate,ar.suppoid,suppname,ar.payreftype,ar.cashbankacctgoid,ar.payduedate,ar.payrefno,ar.trndpapacctgoid,ar.trndpapamt,ar.trndpapnote,ar.trndpapstatus,ar.upduser,ar.updtime,ar.createuser,ar.createtime,ar.branch_code, ar.currencyoid, ar.currencyrate FROM QL_trndpap ar INNER JOIN ql_mstsupp s ON ar.suppoid=s.suppoid WHERE ar.cmpcode='" & CompnyCode & "' AND ar.trndpapoid=" & iOid
        Dim dtMst As DataTable = GetDataTable(sSql, "QL_trndpap")
        If dtMst.Rows.Count < 1 Then
            showMessage("can't load data DP AP Balance !!<BR>Info :<BR>" & sSql, CompnyName & " - Warning", 2)
            Exit Sub
        Else
            trndpapoid.Text = dtMst.Rows(0)("trndpapoid").ToString
            dd_branch.SelectedValue = dtMst.Rows(0)("branch_code").ToString
            trndpapno.Text = dtMst.Rows(0)("trndpapno").ToString
            trndpapdate.Text = Format(CDate(dtMst.Rows(0)("trndpapdate").ToString), "dd/MM/yyyy")
            suppname.Text = dtMst.Rows(0)("suppname").ToString
            suppoid.Text = dtMst.Rows(0)("suppoid").ToString
            payreftype.Text = dtMst.Rows(0)("payreftype").ToString
            payduedate.Text = Format(CDate(dtMst.Rows(0)("payduedate").ToString), "dd/MM/yyyy")
            payrefno.Text = dtMst.Rows(0)("payrefno").ToString
            trndpapacctgoid.SelectedValue = dtMst.Rows(0)("trndpapacctgoid").ToString
            trndpapamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndpapamt").ToString), 4)
            trndpapnote.Text = dtMst.Rows(0)("trndpapnote").ToString
            trndpapstatus.Text = dtMst.Rows(0)("trndpapstatus").ToString
            curroid.SelectedValue = dtMst.Rows(0)("currencyoid").ToString
            currate.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("currencyrate").ToString), 4) 'ToMaskEdit(ToDouble("currencyrate"), 2)
            If payreftype.SelectedValue = "CASH" Then
                tr1.Visible = False : tr2.Visible = False
            Else
                tr1.Visible = True : tr2.Visible = True
            End If

            create.Text = "Created By <B>" & dtMst.Rows(0)("createuser").ToString & "</B> On <B>" & dtMst.Rows(0)("createtime").ToString & "</B> "
            update.Text = "; Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "

            If trndpapstatus.Text <> "POST" Then
                imbSave.Visible = True : imbDelete.Visible = True : btnPosting.Visible = True
            Else
                imbSave.Visible = False : imbDelete.Visible = False : btnPosting.Visible = False
            End If
        End If
        dd_branch.Enabled = False
        dd_branch.CssClass = "inpTextDisabled"

        If trndpapstatus.Text <> "In Process" Then
            imbSave.Visible = False : imbDelete.Visible = False : imbPost.Visible = False
            trndpapdate.CssClass = "inpTextDisabled" : trndpapdate.Enabled = False : imbDPAPDate.Visible = False
            trndpapno.Enabled = False : dd_branch.Enabled = False : suppname.Enabled = False
            imbSearchCust.Visible = False : imbClearCust.Visible = False : trndpapacctgoid.Enabled = False
            payreftype.Enabled = False : payduedate.CssClass = "inpTextDisabled" : payduedate.Enabled = False
            imbDueDate.Visible = False : payrefno.Enabled = False : trndpapamt.Enabled = False
            trndpapnote.Enabled = False
        End If
    End Sub

    Public Sub UnabledCheckBox()

        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPAPInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (gvMst.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("trndpapstatus".ToString)) = "POST" Or Trim(objRow(C1)("trndpapstatus".ToString) = "DELETE") Then
                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub CheckAll()

        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPAPInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("trndpapstatus").ToString) <> "POST" And Trim(objRow(C1)("trndpapstatus").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPAPInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("trndpapstatus").ToString) <> "POST" And Trim(objRow(C1)("trndpapstatus").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub UpdateCheckedPost()
        'Untuk mendapat cek box value multi select
        Dim dv As DataView = Session("TblDPAPInit").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvMst.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        Dim a As String() = sOid.Split(",")
                        dv.RowFilter = "trndpapoid=" & a(1) & " AND cmpcode='" & a(0) & "'"
                        If cbcheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Accounting\trninitdpap.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - DP AP Initial Balance"
        Session("outlet") = Request.QueryString("cmpcode")
        imbDelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan menghapus data ini ?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan mem-posting data ini ?');")
        imbPost.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan mem-posting data ini ?');")

        Dim CutOffDate As Date
        sSql = "SELECT genother1,* FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & " - Warning", 2)
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")

        If Not Page.IsPostBack Then
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            BindData("")
            InitAllDDL()
            FillRate(curroid.SelectedValue)
            DDLPayreftypeChange()
            
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                If CutOffDate = "1/1/1900" Then
                    trndpapdate.Text = Format(CutOffDate.AddDays(-1), "dd/MM/yyyy")
                    payduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                Else
                    trndpapdate.Text = Format(CutOffDate.AddDays(-1), "dd/MM/yyyy")
                    payduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                End If
                TabContainer1.ActiveTabIndex = 0
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
                update.Text = ""
                trndpapstatus.Text = "In Process"
                imbDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchCust.Click
        BindSupplier(" AND (suppname like '%" & Tchar(suppname.Text) & "%' or suppcode like '%" & Tchar(suppname.Text) & "%')")
        gvSupp.Visible = True
    End Sub

    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearCust.Click
        suppoid.Text = "" : suppname.Text = ""
        gvSupp.Visible = False : dd_branch.Enabled = True
        dd_branch.CssClass = "inpText"
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckAll.Click
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUncheckAll.Click
        UncheckAll()
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sErrorku As String = ""
        If Not Session("TblDPAPInit") Is Nothing Then
            Dim dt As DataTable = Session("TblDPAPInit")
            If dt.Rows.Count > 0 Then
                UpdateCheckedPost()
                Dim dv As DataView = dt.DefaultView

                dv.RowFilter = "selected=1"
                If dv.Count = 0 Then
                    sErrorku &= "Please select DP AP first!"
                End If
                dv.RowFilter = ""
                dv.RowFilter = "selected=1 AND trndpapstatus='POST'"
                If dv.Count > 0 Then
                    sErrorku &= "DP AP have been POSTED before!"
                End If
                dv.RowFilter = ""

                If sErrorku <> "" Then
                    showMessage(sErrorku, CompnyName & " - Warning", 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If

                Dim parsingOID As Integer = 0 : Dim sCmpcode As String = ""
                Dim dtAwal As DataTable = Session("TblDPAPInit")
                Dim dvAwal As DataView = dtAwal.DefaultView
                Dim iSeq As Integer = dvAwal.Count + 1
                dvAwal.AllowEdit = True : dvAwal.AllowNew = True
                dv.RowFilter = "selected=1 AND trndpapstatus='In Process'"
                'poststatx.Text = "SPECIAL"

                Dim seq As Integer = 0
                For C1 As Integer = 0 To dv.Count - 1
                    parsingOID = dv(C1)("trndpapoid").ToString
                    sCmpcode = dv(C1)("cmpcode").ToString
                    filltextboxSELECTED(sCmpcode, parsingOID)
                    If Not (Session("errmsg") Is Nothing Or Session("errmsg") = "") Then
                        sErrorku &= "Can't POST DP AP No " & dv(C1)("trndpapno").ToString & " with reason:<BR>" & Session("errmsg")
                    End If
                    seq = C1
                Next

                If seq = dv.Count - 1 Then
                    showMessage("Data Telah di Posting <BR> ", CompnyName & " - Warning", 2)
                    Response.Redirect("trninitdpap.aspx?awal=true")
                End If

            End If
        End If
    End Sub

    Private Sub filltextboxSELECTED(ByVal sCmpcode As String, ByVal selectedoid As Integer)
        If Not selectedoid = Nothing Or selectedoid <> 0 Then
            Dim sMsg As String = ""
            Dim vpayid As Integer = selectedoid
            Session("oid") = vpayid.ToString
            Try
                sSql = "SELECT ar.trndpapoid,ar.cashbankoid,ar.trndpapno,ar.trndpapdate,ar.suppoid,suppname,ar.payreftype,ar.cashbankacctgoid,ar.payduedate,ar.payrefno,ar.trndpapacctgoid,ar.trndpapamt,ar.trndpapnote,ar.trndpapstatus,ar.upduser,ar.updtime,ar.createuser,ar.createtime,ar.cmpcode FROM QL_trndpap ar INNER JOIN ql_mstsupp s ON ar.suppoid=s.suppoid WHERE ar.cmpcode='" & sCmpcode & "' AND ar.trndpapoid=" & selectedoid
                Dim dtMst As DataTable = GetDataTable(sSql, "QL_trndpap")
                If dtMst.Rows.Count < 1 Then
                    showMessage("can't load data DP AP Balance !!<BR>Info :<BR>" & sSql, CompnyName & " - Warning", 2)
                    Exit Sub
                Else
                    trndpapoid.Text = dtMst.Rows(0)("trndpapoid").ToString
                    dd_branch.SelectedValue = dtMst.Rows(0)("cmpcode").ToString
                    trndpapno.Text = dtMst.Rows(0)("trndpapno").ToString
                    trndpapdate.Text = Format(CDate(dtMst.Rows(0)("trndpapdate").ToString), "dd/MM/yyyy")
                    suppname.Text = dtMst.Rows(0)("suppname").ToString
                    suppoid.Text = dtMst.Rows(0)("suppoid").ToString
                    payreftype.Text = dtMst.Rows(0)("payreftype").ToString
                    payduedate.Text = Format(CDate(dtMst.Rows(0)("payduedate").ToString), "dd/MM/yyyy")
                    payrefno.Text = dtMst.Rows(0)("payrefno").ToString
                    DDLPayreftypeChange()
                    trndpapacctgoid.SelectedValue = dtMst.Rows(0)("trndpapacctgoid").ToString
                    trndpapamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndpapamt").ToString), 4)
                    trndpapnote.Text = dtMst.Rows(0)("trndpapnote").ToString
                    trndpapstatus.Text = dtMst.Rows(0)("trndpapstatus").ToString
                    create.Text = "Created By <B>" & dtMst.Rows(0)("createuser").ToString & "</B> On <B>" & dtMst.Rows(0)("createtime").ToString & "</B> "
                    update.Text = "; Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "

                    Session("TblDPAPInitdtl") = dtMst

                    If trndpapstatus.Text <> "POST" Then
                        imbSave.Visible = True : imbDelete.Visible = True : btnPosting.Visible = True
                    Else
                        imbSave.Visible = False : imbDelete.Visible = False : btnPosting.Visible = False
                    End If
                End If
                dd_branch.Enabled = False
                dd_branch.CssClass = "inpTextDisabled"

            Catch ex As Exception
                xreader.Close() : conn.Close()
                sMsg &= "Gagal menampilkan data DP AP Balance.<BR>Info:" & ex.Message & "<BR>" & sSql & "<BR><BR>"
            End Try

        Else
            showMessage("DP AP Balance salah !", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        TabContainer1.ActiveTabIndex = 0
        imbPost_Click(Nothing, Nothing)
        TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & TChar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub gvSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupp.SelectedIndexChanged
        suppoid.Text = gvSupp.SelectedDataKey("suppoid").ToString
        suppname.Text = gvSupp.SelectedDataKey("suppname").ToString
        gvSupp.Visible = False : dd_branch.Enabled = False
        dd_branch.CssClass = "inpTextDisabled"
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupp.PageIndexChanging
        gvSupp.PageIndex = e.NewPageIndex
        BindSupplier(" AND (suppname like '%" & Tchar(suppname.Text) & "%' or suppcode like '%" & Tchar(suppname.Text) & "%')")
         gvSupp.Visible = True
    End Sub

    Protected Sub payreftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payreftype.SelectedIndexChanged
        DDLPayreftypeChange()
    End Sub

    Protected Sub trndpapamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndpapamt.TextChanged
        trndpapamt.Text = ToMaskEdit(ToDouble(trndpapamt.Text), 4)
    End Sub

    Protected Sub imbSave_Click(sender As Object, e As ImageClickEventArgs) Handles imbSave.Click
        If IsInputValid() Then
            Dim sMsg As String = ""
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - Warning", 2)
                trndpapstatus.Text = "In Process"
                Exit Sub
            End If

            If CDate(toDate(trndpapdate.Text)) >= CDate(toDate(CutofDate.Text)) Then
                sMsg &= "- Tanggal DP Tidak boleh >= CutoffDate (" & CutofDate.Text & ") !! <BR> "
            End If

            '==============================
            'Cek Peroide Bulanan Dari Crdgl
            '==============================
            'If ToDouble(trndpapamt.Text) > 0 Then
            '    'CEK PERIODE AKTIF BULANAN
            '    'sSql = "Select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
            '    'Dim cok As String = GetStrData(sSql)
            '    'If GetStrData(sSql) <> "?" Then
            '    sSql = "Select distinct left(isnull(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            '    If GetPeriodAcctg(CDate(toDate(trndpapdate.Text))) < GetStrData(sSql) Then
            '        showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>", CompnyName & " - Warning", 2)
            '        Exit Sub
            '    End If
            'End If
            'End If

            Session("DPAPInit") = GetStrData("SELECT isnull(MIN(trndpapoid)-1,-1) FROM QL_trndpap /*WHERE branch_code='" & dd_branch.SelectedValue & "'*/")

            sSql = "select count(-1) from QL_trnDPAp Where trnDPApno='" & Tchar(trndpapno.Text) & "' and trnDPApstatus='POST' and branch_code='" & dd_branch.SelectedValue & "'"
            If GetStrData(sSql) > 0 Then
                showMessage("Data ini sudah di Posting !, Tekan batal untuk lihat Data di List Information", CompnyName & " - Warning", 2)
                trndpapstatus.Text = "In Process"
                Exit Sub
            End If

            Dim RateIDR As Decimal = GetStrData("select top 1 rate2idrvalue from ql_mstrate2 where currencyoid='" & curroid.SelectedValue & "' order by rate2date desc")
            Dim RateUSD As Decimal = GetStrData("select top 1 rate2usdvalue from ql_mstrate2 where currencyoid='" & curroid.SelectedValue & "' order by rate2date desc")

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try
                If payreftype.SelectedValue = "CASH" Then
                    payduedate.Text = "01/01/1900"
                End If

                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trndpap (cmpcode,trndpapoid,trndpapno,trndpapdate,suppoid,cashbankoid,trndpapacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno, currencyoid, currencyrate,trndpapamt, trndpapamtidr, trndpapamtusd,taxtype,taxoid,taxpct,taxamt,trndpapnote,trndpapflag,trndpapacumamt,trndpapstatus, createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                           "('" & CompnyCode & "'," & Session("DPAPInit") & ",'" & Tchar(trndpapno.Text) & "','" & CDate(toDate(trndpapdate.Text)) & "','" & suppoid.Text & "',0," & trndpapacctgoid.SelectedValue & ",'" & payreftype.SelectedValue & "',0,'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(payrefno.Text) & "','" & curroid.SelectedValue & "','" & ToDouble(currate.Text) & "'," & ToDouble(trndpapamt.Text) & "," & CDec(trndpapamt.Text) * RateIDR & "," & CDec(trndpapamt.Text) * RateUSD & ",'',0,0,0,'" & Tchar(trndpapnote.Text) & "','OPEN',0,'" & trndpapstatus.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("branch_id") & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trndpap SET trndpapno='" & Tchar(trndpapno.Text) & "',trndpapdate='" & CDate(toDate(trndpapdate.Text)) & "',trndpapacctgoid=" & trndpapacctgoid.SelectedValue & ",trndpapamt=" & CDec(trndpapamt.Text) & ", trndpapamtidr = " & CDec(trndpapamt.Text) * RateIDR & ", trndpapamtusd = " & CDec(trndpapamt.Text) * RateUSD & ", payreftype='" & payreftype.SelectedValue & "',payduedate='" & CDate(toDate(payduedate.Text)) & "',payrefno='" & Tchar(payrefno.Text) & "', trndpapnote='" & Tchar(trndpapnote.Text) & "', currencyoid = '" & curroid.SelectedValue & "', currencyrate = '" & CDec(currate.Text) & "',trndpapflag='OPEN',suppoid='" & suppoid.Text & "',trndpapacumamt=0,trndpapstatus='" & trndpapstatus.Text & "',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE branch_code='" & Session("branch_id") & "' AND trndpapoid=" & trndpapoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
                If Session("TblDPAPInitdtl") Is Nothing Then
                    If trndpapstatus.Text = "POST" Then
                        showMessage("Data have been posted with DP AP No. = " & trndpapno.Text & " !", CompnyName & " - INFORMATION", 3)
                        Response.Redirect("~\accounting\trninitdpap.aspx?awal=true")
                    Else
                        Response.Redirect("~\accounting\trninitdpap.aspx?awal=true")
                    End If
                End If
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                trndpapstatus.Text = "In Process"
                showMessage(ex.ToString, CompnyName & " - ERROR", 1)
                Exit Sub
            End Try
        End If
    End Sub

    Protected Sub imbCancel_Click(sender As Object, e As ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("~\accounting\trninitdpap.aspx?awal=true")
    End Sub

    Protected Sub imbPost_Click(sender As Object, e As ImageClickEventArgs) Handles imbPost.Click
        trndpapstatus.Text = "POST"
        imbSave_Click(sender, e)
    End Sub

    Protected Sub imbDelete_Click(sender As Object, e As ImageClickEventArgs) Handles imbDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_trndpap WHERE cmpcode='" & CompnyCode & "' AND trndpapoid=" & trndpapoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
            Response.Redirect("trninitdpap.aspx?awal=true")
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
    End Sub

    Private Sub FillRate(ByVal iCurrOid As Integer)

        currate.Text = NewMaskEdit(ToDouble(cKon.ambilscalar("select top 1 rate2idrvalue from QL_mstrate2 where currencyoid=" & curroid.SelectedValue & " order by rate2date desc ")))

    End Sub

    Protected Sub curroid_SelectedIndexChanged(sender As Object, e As EventArgs) Handles curroid.SelectedIndexChanged
        If trndpapstatus.Text = "In Process" Then
            FillRate(curroid.SelectedValue)
        End If
    End Sub

    Protected Sub imbFindBoM_Click(sender As Object, e As ImageClickEventArgs) Handles imbFindBoM.Click
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub imbAllBoM_Click(sender As Object, e As ImageClickEventArgs) Handles imbAllBoM.Click
        dateAwal.Text = Format(Now, "01/MM/yyyy")
        dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        FilterDDL.SelectedIndex = 0 : FilterText.Text = "" : statuse.SelectedIndex = 0
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()
    End Sub
#End Region
End Class
