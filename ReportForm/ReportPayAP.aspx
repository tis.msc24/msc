﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ReportPayAP.aspx.vb" Inherits="ReportForm_ReportPayAP" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: A/P PAYMENT STATUS REPORT"></asp:Label></th>
        </tr>
    </table>
    <asp:UpdatePanel ID="upReportForm" runat="server">
        <ContentTemplate>
<TABLE style="WIDTH: 100%; HEIGHT: 633px"><TBODY><TR><TD style="WIDTH: 254px; HEIGHT: 1px" align=left></TD><TD style="WIDTH: 12px; HEIGHT: 1px" class="Label" align=center><asp:Label id="Label3" runat="server" Text="Period"></asp:Label> :</TD><TD style="WIDTH: 450px; HEIGHT: 1px" align=left>&nbsp;<asp:TextBox id="txtStart" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w4" Visible="False"></asp:Label> <asp:TextBox id="txtFinish" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w5"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(MM/DD/yyyy)" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD style="WIDTH: 254px; HEIGHT: 1px" align=left></TD><TD style="WIDTH: 12px; HEIGHT: 1px" class="Label" align=center>Supplier:</TD><TD style="WIDTH: 450px; HEIGHT: 1px" align=left><TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><asp:RadioButtonList id="rbSupplier" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True">ALL</asp:ListItem>
                                                <asp:ListItem>SELECT</asp:ListItem>
                                            </asp:RadioButtonList></TD><TD align=left><asp:TextBox id="FilterTextSupplier" runat="server" Width="175px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupplier" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="custoid" runat="server" Visible="False"></asp:Label> </TD></TR></TBODY></TABLE></TD></TR><TR id="tr_supp" runat="server" visible="false"><TD style="WIDTH: 254px; HEIGHT: 1px" align=left></TD><TD style="WIDTH: 12px; HEIGHT: 1px" class="Label" align=center></TD><TD style="WIDTH: 450px; HEIGHT: 1px" align=left><asp:Panel id="pnlSupplier" runat="server"><asp:GridView id="gvSupplier" runat="server" Width="387px" ForeColor="#333333" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" DataKeyNames="suppoid,suppcode,suppname,suppaddr" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><EditItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" __designer:wfdid="w3"></asp:CheckBox>
                                            
</EditItemTemplate>
<ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" ToolTip='<%# Eval("suppoid") %>' Checked='<%# Eval("selected") %>' __designer:wfdid="w2"></asp:CheckBox>
                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label>
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD style="WIDTH: 254px; HEIGHT: 1px" align=left></TD><TD style="WIDTH: 12px; HEIGHT: 1px" class="Label" align=center>Accounnt:</TD><TD style="WIDTH: 450px; HEIGHT: 1px" align=left><TABLE style="WIDTH: 79%"><TBODY><TR><TD style="WIDTH: 117px"><asp:RadioButtonList id="rblAccount" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True">ALL</asp:ListItem>
                                            <asp:ListItem>SELECT</asp:ListItem>
                                        </asp:RadioButtonList> </TD><TD id="td_caricoa" runat="server" visible="false"><asp:TextBox id="acctgdesc" runat="server" Width="168px" CssClass="inpText" __designer:wfdid="w385" Visible="False"></asp:TextBox> <asp:ImageButton id="btnsearchacctg" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w386" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w387" Visible="False"></asp:ImageButton> </TD></TR></TBODY></TABLE></TD></TR><TR id="tr_coa" runat="server" visible="false"><TD style="WIDTH: 254px; HEIGHT: 1px" align=left>&nbsp;</TD><TD style="WIDTH: 12px; HEIGHT: 1px" class="Label" align=center>&nbsp;</TD><TD style="WIDTH: 450px; HEIGHT: 1px" align=left><asp:GridView id="gvAccounting" runat="server" Width="312px" ForeColor="#333333" __designer:wfdid="w369" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer "></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Acctgoid" Visible="False"><ItemTemplate>
                                            <asp:Label ID="lblacctgoid" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
                                            <asp:CheckBox ID="cbAccount" runat="server" Checked='<%# Eval("selected") %>' />
                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="code" HeaderText="Code" SortExpression="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Size="Small" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found."></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate "></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 254px; HEIGHT: 1px" align=left>&nbsp;</TD><TD style="WIDTH: 12px; HEIGHT: 1px" class="Label" align=center><asp:Label id="Label5" runat="server" Text="No.Bukti"></asp:Label> </TD><TD style="WIDTH: 450px; HEIGHT: 1px" align=left><asp:TextBox id="Txtnobukti" runat="server" Width="227px" CssClass="inpText"></asp:TextBox> </TD></TR><TR><TD style="HEIGHT: 1px" align=center colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w10" PopupButtonID="imbStart" Format="dd/MM/yyyy" TargetControlID="txtStart"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" __designer:wfdid="w11" TargetControlID="txtStart" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceFin" runat="server" __designer:wfdid="w12" PopupButtonID="imbFinish" Format="dd/MM/yyyy" TargetControlID="txtFinish"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" __designer:wfdid="w13" TargetControlID="txtFinish" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="HEIGHT: 75px" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReportForm">
                                <ProgressTemplate>
                                    <span></span><span style="font-weight: bold; font-size: 10pt; color: purple">
                                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                        Please Wait .....</span><br />
                                </ProgressTemplate>
                            </asp:UpdateProgress> </TD></TR><TR><TD style="HEIGHT: 75px" vAlign=top align=center colSpan=3><CR:CrystalReportViewer id="crvReportForm" runat="server" AutoDataBind="True" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE>
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnViewReport" />
            <asp:PostBackTrigger ControlID="btnExportToPdf" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></td>
                            <td style="TEXT-ALIGN: left" class="Label">
                                <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px; TEXT-ALIGN: center" colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" colspan="2">&nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
