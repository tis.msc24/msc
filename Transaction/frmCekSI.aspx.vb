Imports System.Data
Imports System.Data.SqlClient
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure

Partial Class frmCekSI
    Inherits System.Web.UI.Page
    Dim sSql As String = ""
    Dim CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cmd As New SqlCommand("", conn)
    Dim cKon As New Koneksi
    Dim cFungsi As New ClassFunction
    Dim cProc As New ClassProcedure

    Private Sub CabDDl()
        sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangDDL, sSql)
            Else
                FillDDL(CabangDDL, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(CabangDDL, sSql)
            CabangDDL.Items.Add(New ListItem("SEMUA BRANCH", "ALL"))
            CabangDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub showMessage(ByVal isi As String, ByVal judul As String)
        isiPesan.Text = isi
        captionPesan.Text = judul
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        '' jangan lupa cek hak akses
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSDO")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSDO") = sqlSearch
            Response.Redirect("~/Transaction/frmCekSI.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Cek Piutang Pernota Nota"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        If Not Page.IsPostBack Then
            CabDDl()
            tglAwal2.Text = Format(CDate(DateTime.Now.ToShortDateString()), "dd/MM/yyyy")
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Private Sub DataBindTrn()
        Try
            If ordermstoid.Text <> "" Then
                sSql = "Select * FROM (" & _
    " Select con.branch_code,gc.gendesc,jm.trnjualmstoid trnOid,jm.trnjualno notrans,Convert(VarChar(20),jm.trnjualdate,103) trnDate,con.amttransidr trnAmtnya,con.amtbayaridr amtBayarNya,trnjualdate datee,'0' orot From ql_trnjualmst jm INNER JOIN ql_conar con ON jm.trnjualmstoid=con.refoid And jm.branch_code=con.branch_code AND con.reftype='QL_trnjualmst' INNER JOIN ql_mstgen gc ON gc.gencode=jm.branch_code AND gc.gengroup='CABANG' " & _
    "UNION ALL" & _
    " Select py.branch_code,gc.gendesc,con.refoid trnOid,cb.cashbankno notrans,Convert(VarChar(20),cb.cashbankdate,103) trnDate,con.amttransidr trnAmtnya,con.amtbayaridr amtBayarNya,cashbankdate datee,'1' orot  From ql_trnpayar py INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid=py.cashbankoid AND cb.branch_code=py.branch_code INNER JOIN ql_conar con ON con.payrefoid=py.paymentoid AND con.branch_code=py.branch_code AND con.reftype='ql_trnpayar' and con.trnartype = 'PAYAR' INNER JOIN ql_mstgen gc ON gc.gencode=con.branch_code AND gc.gengroup='CABANG' AND con.trnarstatus='POST'" & _
    "UNION ALL" & _
    " Select con.branch_code,gc.gendesc,con.refoid trnOid,con.payrefno notrans,Case con.trnardate When '1/1/1900' Then Convert(VarChar(20),con.paymentdate,103) Else Convert(VarChar(20),con.trnardate,103) End trnDate,con.amttransidr trnAmtnya,con.amtbayaridr amtBayarNya,trnardate datee,'1' orot From QL_conar con INNER JOIN QL_mstgen gc ON gencode=con.branch_code AND gengroup='CABANG' AND con.reftype<>'QL_trnjualmst' AND con.trnartype NOT IN ('EXP','PAYAR', 'PAYAREXP', 'ARK') " & _
    "UNION ALL Select con.branch_code, gc.gendesc, jm.trnjualmstoid trnOid, jm.trnjualreturno+ ' - ('+'DP'+')' notrans, Convert(VarChar(20),jm.trnjualdate,103) trnDate, 0.00 trnAmtnya, jm.trnamtjualnetto amtBayarNya, jm.trnjualdate datee, '2' orot From QL_trnjualreturmst jm INNER JOIN QL_trnjualmst con ON jm.trnjualmstoid=con.trnjualmstoid And jm.branch_code=con.branch_code INNER JOIN ql_mstgen gc ON gc.gencode=jm.branch_code AND gc.gengroup='CABANG' AND jm.trnjualreturno IN (SELECT payrefno FROM QL_trndpar) AND typeretur=4" & _
    " ) tr Where trnoid=" & ordermstoid.Text & " AND branch_code='" & CabangDDL.SelectedValue & "' order by 9 asc,8 asc"
                Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
                Session("TblListMat") = dt
                gvListNya.DataSource = Session("TblListMat")
                gvListNya.DataBind() : gvListNya.Visible = True
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            Else
                showMessage("Maaf, Pilih Nomer nota (SI)", CompnyName)
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName)
            Exit Sub
        End Try
    End Sub

    Private Sub bindData(ByVal moreQuery As String, ByVal gvTarget As GridView)
        Dim cBang As String = ""
        Try
            If CabangDDL.SelectedValue <> "ALL" Then
                cBang = "And m.branch_code='" & CabangDDL.SelectedValue & "'"
            End If
            sSql = "Select *, (AmtNetto-amtRetur)-(amtBayar) AmtSisa From (" & _
            "Select top 50 m.branch_code, m.trnjualmstoid orderoid, m.trnjualno orderno, m.trncustoid custoid, c.custname, m.trnjualdate deliverydate, m.trnorderstatus orderstatus, m.periodacctg, ISNULL((Select SUM(amtjualdisc) FRom ql_trnjualdtl dt Where m.trnjualmstoid=dt.trnjualmstoid And dt.branch_code=m.branch_code),0.0) AmtPot, ISNULL((Select SUM(trndparamtidr)-SUM(trndparacumamtidr) From QL_TRNDPAR dt Where m.trncustoid=dt.custoid And dt.branch_code=m.branch_code),0.0) AmtDP, ISNULL((Select ABS(SUM(amtbayar)) from QL_conar con Where con.refoid=m.trnjualmstoid AND con.branch_code=m.branch_code AND con.custoid=m.trncustoid AND trnartype='PAYAR' AND con.trnarstatus='POST'),0.0) + ISNULL((Select ABS(SUM(amtbayar)) from QL_conar con Where con.refoid=m.trnjualmstoid AND con.branch_code=m.branch_code AND con.custoid=m.trncustoid AND trnartype='CNAR' AND con.trnarstatus='POST'),0.0) amtBayar, ISNULL((Select sum(trnjualdtlqty*trnjualdtlprice)-SUM(trnjualdtlqty*trnjualdtldiscqty) From QL_trnjualreturdtl rd INNER JOIN QL_trnjualreturmst rm ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rm.branch_code=rd.branch_code Where rm.branch_code=m.branch_code AND rm.trnjualstatus IN ('APPROVED','POST') AND rm.trncustoid=m.trncustoid AND m.trnjualno=rm.refnotaretur AND rm.typeretur='1'),0.00) amtRetur, m.trnamtjualnettoidr + ISNULL((Select ABS(SUM(amtbayar)) from QL_conar con Where con.refoid=m.trnjualmstoid AND con.branch_code=m.branch_code AND con.custoid=m.trncustoid AND trnartype='DNAR' AND con.trnarstatus='POST'),0.0) AmtNetto, ISNULL((Select SUM(dt.amtjualdisc1) FRom ql_trnjualdtl dt Where m.trnjualmstoid=dt.trnjualmstoid And dt.branch_code=m.branch_code),0.0) DiscUmum, ISNULL((Select SUM(dt.amtjualdisc2) FRom ql_trnjualdtl dt Where m.trnjualmstoid=dt.trnjualmstoid And dt.branch_code=m.branch_code),0.00) DiscIn, ISNULL((Select SUM(om.trnjualdtlqty*trnjualdtlprice) from ql_trnjualdtl om Where om.trnjualmstoid=m.trnjualmstoid AND om.branch_code=m.branch_code),0.00) AmtJualNya, ISNULL((Select trnorderref From ql_trnordermst om Where om.orderno=m.orderno AND om.ordermstoid=m.ordermstoid AND om.branch_code=m.branch_code),'') NotaManual, CASE (Select o.promooid From QL_trnordermst o Where o.orderno=m.orderno AND o.ordermstoid=m.ordermstoid AND o.branch_code=m.branch_code ) WHEN 0 THEN 0 ELSE ISNULL((Select SUM(dt.amtjualdisc) FRom ql_trnjualdtl dt Where m.trnjualmstoid=dt.trnjualmstoid And dt.branch_code=m.branch_code),0.0) END AS discpromo FROM ql_trnjualmst m inner join ql_mstcust c on m.trncustoid=c.custoid AND m.branch_code=c.branch_code /*INNER JOIN QL_trnordermst o ON o.orderno=m.orderno AND o.ordermstoid=m.ordermstoid AND o.branch_code=m.branch_code*/ Where UPPER(m.cmpcode)='" & CompnyCode & "' " & cBang & " and (m.trnjualno like '%" & Tchar(txtorderNo.Text) & "%' OR c.custname LIKE '%" & Tchar(txtorderNo.Text) & "%')" & _
            ") tg Order By orderoid Desc"
            FillGV(gvTarget, sSql, "ql_trnordermst")
            gvTarget.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName)
            Exit Sub
        End Try
    End Sub

    Private Sub bindItem()
        sSql = "Select a.itemoid,item.itemcode ,item.itemdesc,trnjualdtlqty qty,b.trnjualmstoid,(select gendesc From QL_mstgen Where gengroup='itemunit' and genoid = a.trnjualdtlunitoid) unit,a.trnjualdtlprice PriceItem,a.amtjualnetto From ql_trnjualdtl a inner join ql_trnjualmst b ON a.trnjualmstoid=b.trnjualmstoid AND a.branch_code=b.branch_code inner join QL_MSTITEM item on item.itemoid = a.itemoid Where b.trnjualno='" & Tchar(txtorderNo.Text.Trim) & "' and a.branch_code='" & CabangDDL.SelectedValue & "' Order By item.itemcode"
        FillGV(gv_item, sSql, "ql_trnordermstdtl")
        gv_item.Visible = True
    End Sub

    Private Sub BindSODO()
        Try
            sSql = "select om.orderno no,om.trnorderdate tgl,c.custname,om.amtjualnetto TotalTrans,om.createuser,om.createtime,1 AS tipe " & _
                      "FROM QL_trnjualmst jm INNER JOIN QL_trnordermst om ON om.orderno=jm.orderno AND om.ordermstoid=jm.ordermstoid AND om.branch_code=jm.branch_code INNER JOIN QL_mstcust c ON c.custoid=jm.trncustoid Where jm.trnjualmstoid=" & ordermstoid.Text & " " & _
                      "UNION ALL " & _
                      "select sjm.trnsjjualno no,sjm.trnsjjualdate tgl,c.custname,0.0 AS TotalTrans,sjm.upduser createuser,sjm.updtime createtime,2 AS tipe FROM QL_trnjualmst jm INNER JOIN ql_trnsjjualmst sjm ON sjm.trnsjjualmstoid=jm.trnsjjualmstoid AND jm.ordermstoid=sjm.ordermstoid INNER JOIN QL_mstcust c ON c.custoid=jm.trncustoid Where jm.trnjualmstoid=" & ordermstoid.Text & " ORDER BY tipe"
            FillGV(gvSODO, sSql, "QL_trnjualmst")
            gvSODO.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim x As Integer = 123
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, False)
    End Sub

    Protected Sub imbSearchBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchBPM.Click
        bindData("", gvBPM2)
    End Sub

    Protected Sub imbEraseBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseBPM.Click
        txtorderNo.Text = "" : ordermstoid.Text = "" : custname.Text = ""
        gvBPM2.Visible = False : gv_item.Visible = False : gvSODO.Visible = False
        gv_item.SelectedIndex = -1 : gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBPM2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            'e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
        End If
    End Sub

    Protected Sub gvBPM2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBPM2.SelectedIndexChanged
        ordermstoid.Text = gvBPM2.SelectedDataKey.Item("orderoid").ToString
        txtorderNo.Text = gvBPM2.SelectedDataKey.Item("orderno").ToString
        custname.Text = gvBPM2.SelectedDataKey.Item("custname").ToString
        tglAwal2.Text = Format(gvBPM2.SelectedDataKey.Item("deliverydate"), "dd/MM/yyyy")
        TotNota.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("AmtNetto")), 3)
        AmtBayar.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("amtBayar")), 3)
        DiscUmum.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("DiscUmum")), 3)
        DiscInt.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("DiscIn")), 3)
        DiscPromo.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("discpromo")), 3)
        AmtDisc.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("AmtPot")), 3)
        AmtDP.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("AmtDP")), 3)
        AmtBruto.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("AmtJualNya")), 3)
        AmtRet.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("amtRetur")), 3)
        AmtSisaNett.Text = ToMaskEdit(gvBPM2.SelectedDataKey.Item("AmtSisa"), 3)
        CabangDDL.SelectedValue = gvBPM2.SelectedDataKey.Item("branch_code").ToString
        gvBPM2.Visible = False : gvBPM2.SelectedIndex = -1
        btnCloseBPM.Visible = True : btnInfoTrn.Visible = True
        'bindItem()
    End Sub

    Protected Sub gvBPM2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBPM2.PageIndexChanging
        gvBPM2.PageIndex = e.NewPageIndex
        Dim sQuery As String = ""
        sQuery &= " AND m.orderno like '%" & Tchar(txtorderNo.Text) & "%' "
        sQuery &= " AND m.trnorderstatus='Approved' "
        bindData(sQuery, gvBPM2)
    End Sub

    Protected Sub btnCloseBPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseBPM.Click
        Try
            bindItem() : BindSODO() 
        Catch ex As Exception
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnInfoTrn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInfoTrn.Click
        DataBindTrn()
        mpeListMat.Show()
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListNya_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListNya.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub gvListNya_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListNya.PageIndexChanging
        gvListNya.PageIndex = e.NewPageIndex
        DataBindTrn() : mpeListMat.Show()
    End Sub

    Protected Sub gv_item_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_item.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub gv_item_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gv_item.PageIndexChanging
        gv_item.PageIndex = e.NewPageIndex
        bindItem()
        gv_item.Visible = True
    End Sub

    Protected Sub gvSODO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSODO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(5).Text = Format(CDate(e.Row.Cells(5).Text), "dd/MM/yyyy hh:mm:ss")
        End If
    End Sub
End Class
