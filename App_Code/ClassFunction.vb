Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms


Public Class ClassFunction
    Inherits System.Web.UI.Page

    Public Overloads Shared Function GetDataTable(ByVal sSQLQuery As String, ByVal sNamaTabel As String) As DataTable
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim daData As New SqlDataAdapter(sSQLQuery, conn)
        Dim dsData As New DataSet

        daData.Fill(dsData, sNamaTabel)
        Return dsData.Tables(sNamaTabel)
    End Function
    Public Overloads Shared Function GetScalar(ByVal sSQLQuery As String) As Object
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand(sSQLQuery, conn)

        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim oReturn As Object = xCmd.ExecuteScalar
        conn.Close()
        Return oReturn
    End Function
    Public Shared Function GetMultiUserStatus(ByVal sTable As String, ByVal sOidField As String, ByVal sOidValue As String, ByVal sStatusField As String, ByVal sUpdTime As String, ByVal sType As String) As String
        GetMultiUserStatus = ""
        Dim sSql As String = "SELECT COUNT(*) FROM " & sTable & " WHERE " & sOidField & "=" & sOidValue & " AND " & sStatusField & "='In Process' AND (CONVERT(VARCHAR(10), updtime, 101) + ' ' + CONVERT(VARCHAR(10), updtime, 108))<>'" & Format(CDate(sUpdTime), "MM/dd/yyyy HH:mm:ss") & "'"
        If CheckDataExists(sSql) Then
            GetMultiUserStatus = "This data has been updated by another user. Please CANCEL this transaction and try again!"
        Else
            sSql = "SELECT COUNT(*) FROM " & sTable & " WHERE " & sOidField & "=" & sOidValue & " AND " & sStatusField & "='" & IIf(sType = "Post", sType, "In Approval") & "'"
            If CheckDataExists(sSql) Then
                GetMultiUserStatus = "This data has been " & IIf(sType = "Post", "posted", "sent for approval") & " by another user. Please CANCEL this transaction!"
            Else
                sSql = "SELECT COUNT(*) FROM " & sTable & " WHERE " & sOidField & "=" & sOidValue
                If ToDouble(GetStrData(sSql).ToString) = 0 Then
                    GetMultiUserStatus = "This data has been deleted by another user. Please CANCEL this transaction!"
                End If
            End If
        End If
        Return GetMultiUserStatus
    End Function
	
	
    Public Overloads Shared Function ValidateStringDataToDB(ByVal sData As String, ByVal sTableName As String, ByVal sColumnName As String, ByVal sErrorMsg As String) As Boolean
        ' RETURN TRUE and EMPTY STRING if the data is valid
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim sSQL As String = "" : sErrorMsg = ""

        sSQL = "SELECT ISNULL(character_maximum_length,0) FROM information_schema.columns where table_name='" & sTableName & "' AND column_name='" & sColumnName & "' "
        Dim xCmd As New SqlCommand(sSQL, conn)

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        If ToDouble(xCmd.ExecuteScalar) <= 0 Then
            sErrorMsg = "Target column do not exist or not a String related data type."
        Else
            If sData.Length > ToDouble(xCmd.ExecuteScalar) Then
                sErrorMsg = "Maximum character is " & CInt(ToDouble(xCmd.ExecuteScalar)) & "."
            End If
        End If
        conn.Close()
        ValidateStringDataToDB = (sErrorMsg = "")
    End Function

    Public Overloads Shared Function isLengthAccepted(ByVal sField As String, ByVal sTable As String, ByVal dValue As Double, ByRef sErrReply As String) As Boolean
        isLengthAccepted = True
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim sSql As String = "SELECT col.precision, col.scale FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='" & sField & "' AND ta.name='" & sTable & "'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim xCmd As New SqlCommand(sSql, conn)
        Dim iPrec, iScale As Integer
        Try
            Dim xRdr As SqlDataReader = xCmd.ExecuteReader
            While xRdr.Read
                iPrec = CInt(xRdr.Item("precision").ToString)
                iScale = CInt(xRdr.Item("scale").ToString)
            End While
            xRdr.Close()
        Catch ex As Exception
            iPrec = 0 : iScale = 0
            sErrReply = "- Can't check the result. Please check that the parameter has been assigned correctly!"
            isLengthAccepted = False
        End Try
        conn.Close()
        If iPrec > 0 And iScale >= 0 Then
            Dim sTextValue As String = ""
            For C1 As Integer = 0 To iPrec - iScale - 1
                sTextValue &= "9"
            Next
            If iScale > 0 Then
                sTextValue &= "."
                For C1 As Integer = 0 To iScale - 1
                    sTextValue &= "9"
                Next
            End If
            If dValue > ToDouble(sTextValue) Then
                sErrReply = ToMaskEdit(ToDouble(sTextValue), iScale)
                isLengthAccepted = False
            End If
        End If
        Return isLengthAccepted
    End Function

    Public Overloads Shared Function ValidateNumericDataToDB(ByVal dData As Decimal, ByVal sTableName As String, ByVal sColumnName As String, ByVal sErrorMsg As String) As Boolean
        ' RETURN TRUE and EMPTY STRING if the data is valid
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim sSQL As String = ""
        Dim sMsg As String = "" : sErrorMsg = ""

        sSQL = "SELECT ISNULL(numeric_precision,0)-ISNULL(numeric_scale,0) FROM information_schema.columns where table_name='" & sTableName & "' AND column_name='" & sColumnName & "' "
        Dim xCmd As New SqlCommand(sSQL, conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        If ToDouble(xCmd.ExecuteScalar) <= 0 Then
            sErrorMsg = "Target column do not exist or not a Numeric related data type."
        Else
            If Format(Math.Round(dData, 0), "0").ToString.Length > ToDouble(xCmd.ExecuteScalar) Then
                sErrorMsg = "Maximum value is " & CInt(ToDouble(xCmd.ExecuteScalar)) & " digit."
            End If
        End If
        conn.Close()
        ValidateNumericDataToDB = (sErrorMsg = "")
    End Function
  
    Public Overloads Shared Function SetTableDetail(ByVal sName As String, ByVal sColumnName() As String, ByVal sColumnType() As String) As DataTable
        Dim nuTable As New DataTable(sName)
        For C1 As Integer = 0 To sColumnName.Length - 1
            nuTable.Columns.Add(sColumnName(C1), Type.GetType(sColumnType(C1)))
        Next
        Return nuTable
    End Function

    Public Shared Function GetRoundValue(ByVal sVar As String) As Integer
        GetRoundValue = 0
        Dim dValue As Double = ToDouble(sVar)
        If dValue.ToString.Contains(".") Then
            Dim sSplit() As String = dValue.ToString.Split(".")
            If sSplit.Length > 0 Then
                GetRoundValue = sSplit(sSplit.Length - 1).Length
            End If
        End If
        Return GetRoundValue
    End Function

    Public Shared Function GetSortDirection(ByVal column As String, ByRef sExp As String, ByRef sDir As String) As String
        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"
        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(sExp, String)
        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(sDir, String)
                If lastDirection IsNot Nothing AndAlso lastDirection = "ASC" Then
                    sortDirection = "DESC"
                End If
            End If
        End If
        ' Save new values in ViewState.
        sDir = sortDirection
        sExp = column
        Return sortDirection
    End Function
    Public Shared Function checkApproval(ByVal sTableName As String, ByVal sEvent As String, ByVal sOid As String, ByVal sStatus As String, ByVal sApprovaltype As String, ByVal branch As String) As Int64
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_Conn"))
        Dim xCmd As New SqlCommand("SELECT count(-1) from QL_Approval WHERE STATUSREQUEST = '" & sStatus & "' " & _
         " AND TABLENAME='" & sTableName & "' and OID=" & sOid & " and EVENT='" & sEvent & "' and Approvaltype='" & sApprovaltype & "' and branch_code='" & branch & "' ", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Try

            checkApproval = xCmd.ExecuteScalar

        Catch ex As Exception
            checkApproval = 0
        End Try
        conn.Close()
        Return checkApproval
    End Function


    Public Overloads Shared Function CheckYearIsValid(ByVal ddate As Date) As Boolean
        Dim min As Integer = 2000
        Dim max As Integer = 2072


        CheckYearIsValid = False
        If ddate.Year < min Then
            CheckYearIsValid = True
        End If
        If ddate.Year > max Then
            CheckYearIsValid = True
        End If

        Return CheckYearIsValid
    End Function

    Public Overloads Shared Function GetActivePeriode() As String
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("select top 1 periodacctg from ql_crdmtr where convert(char(10),closingdate,103) ='01/01/1900'", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        GetActivePeriode = xCmd.ExecuteScalar
        conn.Close()

        Return GetActivePeriode
    End Function


    Public Overloads Shared Function CheckDateIsClosedMtr(ByVal ddate As Date, ByVal cmpcode As String) As Boolean
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("select COUNT(-1) from ql_crdmtr where periodacctg='" & GetDateToPeriodAcctg(ddate) & "'  and cmpcode='" & cmpcode & "'  and convert(char(10),closingdate,103) <>'01/01/1900'", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        If xCmd.ExecuteScalar > 0 Then
            CheckDateIsClosedMtr = True
        Else
            CheckDateIsClosedMtr = False
        End If
        conn.Close()

        Return CheckDateIsClosedMtr
    End Function


    Public Overloads Shared Function CheckDateIsPeriodMtr(ByVal ddate As Date, ByVal cmpcode As String) As Boolean
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("select COUNT(-1) from ql_crdmtr where periodacctg='" & GetDateToPeriodAcctg(ddate) & "'  and cmpcode='" & cmpcode & "'  and convert(char(10),closingdate,103) ='01/01/1900'", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        If xCmd.ExecuteScalar > 0 Then
            CheckDateIsPeriodMtr = True
        Else
            CheckDateIsPeriodMtr = False
        End If
        conn.Close()

        Return CheckDateIsPeriodMtr
    End Function
  
    Public Shared Sub ShowCOAPosting(ByVal sNoRef As String, ByVal branch As String, ByRef gvData As GridView, Optional ByVal sRateType As String = "Default", Optional ByVal sOther As String = "")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        gvData.DataSource = Nothing
        Dim sFieldAmt As String = "glamt"
        If sRateType = "IDR" Then
            sFieldAmt = "glamtidr"
        ElseIf sRateType = "USD" Then
            sFieldAmt = "glamtusd"
        End If
        Dim sQuery As String = "SELECT a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." & sFieldAmt & " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." & sFieldAmt & " ELSE 0 END) AS acctgcredit FROM QL_trngldtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref='" & sNoRef & "' AND d.branch_code='" & branch & "' AND d." & sFieldAmt & ">0 /*AND ISNULL(d.glother1, '')='" & sOther & "'*/ ORDER BY d.glseq"
        Dim daCOAPosting As New SqlDataAdapter(sQuery, conn)
        Dim dtCOAPosting As New DataTable
        daCOAPosting.Fill(dtCOAPosting)
        If dtCOAPosting.Rows.Count = 0 Then
            sQuery = "SELECT a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." & sFieldAmt & " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." & sFieldAmt & " ELSE 0 END) AS accgtcredit FROM QL_trngldtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref LIKE '%" & sNoRef & "' AND d.branch_code='" & branch & "' AND d." & sFieldAmt & ">0 /*AND ISNULL(d.glother1, '')='" & sOther & "'*/ ORDER BY d.glseq"
            dtCOAPosting.Rows.Clear()
            Dim daCOAPosting2 As New SqlDataAdapter(sQuery, conn)
            daCOAPosting2.Fill(dtCOAPosting)
        End If
        gvData.DataSource = dtCOAPosting
        gvData.DataBind()
    End Sub
    Public Overloads Shared Function GetDateToPeriod(ByVal dtDate As Date) As String
        Return Format(dtDate, "dd-MM-yyyy")
    End Function

    Public Shared Sub showTableCOA(ByVal noRef As String, ByVal cmpcode As String, ByVal Obj As GridView)
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim ssql As String = "select a.acctgcode acctgcode, a.acctgdesc, case d.gldbcr when 'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit', glnote from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid where noref ='" & noRef & "' and d.branch_code='" & cmpcode & "' order by d.gldbcr desc,d.glseq, a.acctgcode"
        Dim xadap As New SqlDataAdapter(ssql, conn)
        Dim otable As New DataTable
        xadap.Fill(otable)
        If otable.Rows.Count = 0 Then
            ssql = "select   a.acctgcode acctgcode , a.acctgdesc, case d.gldbcr when  'D' then d.glamt else 0 end 'Debet',  case d.gldbcr when  'C' then d.glamt else 0 end 'Kredit' , glnote  from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid   where glnote like '%" & noRef & "'  and d.branch_code='" & cmpcode & "'  order by d.gldbcr desc,d.glseq, a.acctgcode"
            otable.Rows.Clear()
            Dim xadap2 As New SqlDataAdapter(ssql, conn)
            xadap2.Fill(otable)
        End If
        Obj.DataSource = otable
        Obj.DataBind()

    End Sub

    Public Shared Sub showTableCOA(ByVal noRef As String, ByVal cmpcode As String, ByVal Obj As GridView, ByVal cashbankoid As Int64)
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim ssql As String = "select   a.acctgcode acctgcode, a.acctgdesc, case d.gldbcr when  'D' then d.glamt else 0 end 'Debet',  case d.gldbcr when  'C' then d.glamt else 0 end 'Kredit' , glnote  from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid  where    glother2=" & cashbankoid & "  AND NOREF='" & noRef & "' and d.cmpcode='" & cmpcode & "'  order by d.gldbcr desc, a.acctgcode"
        Dim xadap As New SqlDataAdapter(ssql, conn)
        Dim otable As New DataTable
        xadap.Fill(otable)
        If otable.Rows.Count = 0 Then
            ssql = "select a.acctgcode acctgcode, a.acctgdesc, case d.gldbcr when 'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit' , glnote  from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid where glnote like '%" & noRef & "'  and d.cmpcode='" & cmpcode & "' order by d.gldbcr desc, a.acctgcode"
            otable.Rows.Clear()
            Dim xadap2 As New SqlDataAdapter(ssql, conn)
            xadap2.Fill(otable)
        End If
        Obj.DataSource = otable
        Obj.DataBind()
    End Sub

    Public Shared Function GetVarInterface(ByVal sVar As String, ByVal sCmpcode As String) As String
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & sVar & "' And cmpcode='MSC' AND interfaceres1='" & sCmpcode & "'", conn)
        Dim cSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & sVar & "' and cmpcode='MSC' AND interfaceres1='" & sCmpcode & "'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Try
            GetVarInterface = xCmd.ExecuteScalar
        Catch ex As Exception
            GetVarInterface = "Kode COA tidak ditemukan !!"
        End Try
        conn.Close()
        Return GetVarInterface
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal sSQL As String) As Boolean ' return true when data exists or false when no data found
        CheckDataExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSQL
        If xCmd.ExecuteScalar > 0 Then
            CheckDataExists = True
        End If
        conn.Close()
        Return CheckDataExists
    End Function

    Public Shared Function GetPeriodAcctg(ByVal sTgl As Date) As String
        Return Format(sTgl, "yyyy-MM")
    End Function

    Public Shared Sub DisposeGrid(ByRef gv As Object)
        gv.DataSource = Nothing
        gv.DataBind()
    End Sub

    Public Overloads Shared Function NewMaskEdit(ByVal sDec As String) As String
        NewMaskEdit = Left(ToDouble(sDec), IIf(sDec.ToString.IndexOf(".") > 0, sDec.ToString.IndexOf("."), sDec.ToString.Length))
        NewMaskEdit = ToMaskEdit(NewMaskEdit, 1) & ToDouble(sDec).ToString.Substring(IIf(ToDouble(sDec).ToString.IndexOf(".") > 0, ToDouble(sDec).ToString.IndexOf("."), ToDouble(sDec).ToString.Length))
        Return NewMaskEdit
    End Function

    Public Overloads Shared Function CreateDataTableFromSQL(ByVal sSql As String) As DataTable
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim oDataTable As New DataTable
        Dim adapter As New SqlDataAdapter("", conn)
        adapter.SelectCommand.CommandText = sSql
        adapter.Fill(oDataTable)
        Return oDataTable
    End Function

    Public Shared Function getRomawi(ByVal iNo As Int16)
        Select Case iNo
            Case 1 : Return "I"
            Case 2 : Return "II"
            Case 3 : Return "III"
            Case 4 : Return "IV"
            Case 5 : Return "V"
            Case 6 : Return "VI"
            Case 7 : Return "VII"
            Case 8 : Return "VIII"
            Case 9 : Return "IX"
            Case 10 : Return "X"
            Case 11 : Return "XI"
            Case 12 : Return "XII"
        End Select
    End Function

    Public Overloads Shared Function GetStrData(ByVal sSql As String) As String ' to get 1 data from query , return '?' if no data was returned
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader

        GetStrData = "?"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            GetStrData = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()

        Return GetStrData
    End Function

    Public Overloads Shared Function GenerateBatchNo(ByVal sRefOid As String, ByVal iOid As Integer, ByVal sDate As String) As String
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""
        Dim sBatchPrefix As String = "" : Dim sType As String = "" : Dim sBatchNo As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        If sRefOid = "ITEM" Then
            sSql = "SELECT TOP 1 itemoid,'batchprefix'=CASE WHEN batchprefix IS NULL THEN itemcode WHEN RTRIM(batchprefix)='' " & _
                "THEN itemcode ELSE batchprefix END,ISNULL(batchtype,'MULTI') AS batchtype " & _
                "FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iOid
        Else
            sSql = "SELECT TOP 1 matoid,'batchprefix'=CASE WHEN batchprefix IS NULL THEN matcode WHEN RTRIM(batchprefix)='' " & _
                "THEN matcode ELSE batchprefix END,ISNULL(batchtype,'SINGLE') AS batchtype " & _
                "FROM QL_mstmat WHERE cmpcode='" & CompnyCode & "' AND matoid=" & iOid
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            sBatchPrefix = xreader("batchprefix").ToString
            sType = xreader("batchtype").ToString
        End While
        conn.Close()

        If sType = "MULTI" Then
            ' MULTI BATCH
            If IsDate(sDate) Then
                sBatchNo = sBatchPrefix & "/" & Format(CDate(sDate), "yyMMdd")
            Else
                sBatchNo = sBatchPrefix & "/" & Format(Now, "yyMMdd")
            End If
        Else
            ' SINGLE BATCH
            sBatchNo = sBatchPrefix
        End If
        Return sBatchNo
    End Function

    Public Overloads Shared Function GetDateToPeriodAcctg(ByVal dtDate As Date) As String
        Return Format(dtDate, "yyyy-MM")
    End Function

    Public Overloads Shared Function checkPagePermission(ByVal PagePath As String, ByVal dtAccess As DataTable) As Boolean
        Dim sTemp As String
        Dim sFill() As String
        Dim sepp As Char = "/"
        sFill = PagePath.Split(sepp)
        sTemp = sFill(sFill.Length - 1)
        Dim dv As DataView = dtAccess.DefaultView
        dv.RowFilter = "FORMADDRESS LIKE '%" & sTemp & "%'"
        If dv.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal iKey As Int64, ByVal sColomnKeyName() As String, ByVal sTable() As String, ByVal sOtherWhere() As String) As Boolean ' return true when data exists or false when no data found
        CheckDataExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        For c1 As Int16 = 0 To UBound(sTable)
            sSql = "SELECT count(-1) FROM " & sTable(c1) & " where cmpcode='" & CompnyCode & "' and " & sColomnKeyName(c1) & "=" & iKey & " " & sOtherWhere(c1)
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar > 0 Then
                CheckDataExists = True
                Exit For
            End If
        Next
        conn.Close()
        Return CheckDataExists
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal sKey As String, ByVal sColomnKeyName As String, ByVal sTable As String) As Boolean ' return true when data exists or false when no data found
        CheckDataExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT count(-1) FROM " & sTable & " where cmpcode='" & CompnyCode & "' and " & sColomnKeyName & "='" & sKey & "'"
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            CheckDataExists = True
        End If

        conn.Close()
        Return CheckDataExists
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal iKey As Int64, ByVal sColomnKeyName() As String, ByVal sTable() As String) As Boolean ' return true when data exists or false when no data found
        CheckDataExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        For c1 As Int16 = 0 To UBound(sTable)
            sSql = "SELECT count(-1) FROM " & sTable(c1) & " where cmpcode='" & CompnyCode & "' and " & sColomnKeyName(c1) & "=" & iKey
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar > 0 Then
                CheckDataExists = True
                Exit For
            End If
        Next
        conn.Close()
        Return CheckDataExists
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal iKey As Int64, ByVal sColomnKeyName As String, ByVal sTable As String) As Boolean ' return true when data exists or false when no data found
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "SELECT count(-1) FROM " & sTable & " where cmpcode='" & CompnyCode & "' and " & sColomnKeyName & "=" & iKey
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            CheckDataExists = True
        End If
        conn.Close()
        Return CheckDataExists
    End Function

    Public Overloads Shared Function FillDDLGen(ByVal sGroup() As String, ByRef oDDLObject() As DropDownList) As Boolean ' to fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""

        FillDDLGen = True
        sSql = ""
        For c1 As Int16 = 0 To UBound(sGroup)
            sSql &= " select genoid, gendesc, gengroup from QL_mstgen where gengroup = '" & sGroup(c1) & "' UNION ALL"
        Next
        sSql = sSql.Remove(sSql.Length - 9)

        For c2 As Int16 = 0 To UBound(oDDLObject)
            oDDLObject(c2).Items.Clear()
        Next

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        Dim iDDL As Int16 = -1
        Dim sGroupTemp As String = ""
        While xreader.Read
            If iDDL = -1 Then
                sGroupTemp = xreader.GetValue(2).ToString.Trim
                iDDL = 0
            End If
            For c3 As Int16 = 0 To UBound(sGroup)
                If xreader.GetValue(2).ToString.Trim = sGroup(c3) Then
                    If xreader.GetValue(2).ToString.Trim <> sGroupTemp Then
                        iDDL += 1
                        sGroupTemp = xreader.GetValue(2).ToString.Trim
                    End If
                    oDDLObject(iDDL).Items.Add(xreader.GetValue(1))
                    oDDLObject(iDDL).Items.Item(oDDLObject(iDDL).Items.Count - 1).Value = xreader.GetValue(0)
                    Exit For
                End If
            Next
        End While
        xreader.Close()
        conn.Close()

        'cek if all data exits
        For c4 As Int16 = 0 To UBound(oDDLObject)
            If oDDLObject(c4).Items.Count = 0 Then
                FillDDLGen = False
                Exit For
            End If
        Next
        Return FillDDLGen
    End Function

    Public Overloads Shared Function FillDDLGenSort(ByVal sGroup As String, ByRef oDDLObject As DropDownList, ByVal sortType As String) As Boolean ' to fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""

        FillDDLGenSort = True
        sSql = "select genoid, gendesc, gengroup from QL_mstgen where gengroup IN ('" & sGroup.Replace(",", "','") & "') ORDER BY gendesc " & sortType

        oDDLObject.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()

        'cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLGenSort = False
        End If
        Return FillDDLGenSort
    End Function

    Public Overloads Shared Function FillDDLGen(ByVal sGroup As String, ByRef oDDLObject As DropDownList) As Boolean ' to fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""

        FillDDLGen = True
        sSql = "select genoid, gendesc, gengroup from QL_mstgen where gengroup IN ('" & sGroup.Replace(",", "','") & "')"

        oDDLObject.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()

        'cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLGen = False
        End If
        Return FillDDLGen
    End Function

    Public Overloads Shared Function FillDDLGen(ByVal sGroup As String, ByRef oDDLObject As DropDownList, ByVal sWhere As String) As Boolean ' to fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""

        FillDDLGen = True
        sSql = "select genoid, gendesc, gengroup from QL_mstgen where gengroup IN ('" & sGroup.Replace(",", "','") & "') and " & sWhere
        oDDLObject.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()

        'cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLGen = False
        End If
        Return FillDDLGen
    End Function

    Public Overloads Shared Function FillGV(ByRef oGVObject As GridView, ByVal sSql As String, ByVal sTableName As String) As Boolean ' Fill data in grid view by 4n7JuK
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim dset As New DataSet
        Dim adapter As New SqlDataAdapter("", conn)

        FillGV = True
        oGVObject.DataSource = Nothing

        adapter.SelectCommand.CommandText = sSql
        'adapter.ReturnProviderSpecificTypes = True
        'adapter.FillSchema(dset, SchemaType.Source)
        adapter.Fill(dset, sTableName)
        oGVObject.DataSource = dset.Tables(sTableName)
        oGVObject.DataBind()

        'cek if all data exits
        If oGVObject.Rows.Count < 1 Then
            FillGV = False
        End If
        Return FillGV
    End Function

    Public Overloads Shared Function FillDDL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean ' to fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader

        FillDDL = True
        oDDLObject.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()

        'cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDL = False
        End If
        Return FillDDL
    End Function
    Public Shared Function GenerateIDbranch(ByVal sTableName As String, ByVal branch As String) As Int64
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))

        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        xCmd.CommandTimeout = 60

        sSql = "select top 1 ISNULL(lastoid,0) from QL_mstoid where tablename = '" & sTableName & "' and branch_code='" & branch & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        GenerateIDbranch = xCmd.ExecuteScalar + 1
        conn.Close()
        Return GenerateIDbranch
    End Function
    Public Shared Function GenerateID(ByVal sTableName As String, ByVal sCompnyCode As String) As Int64
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))

        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        xCmd.CommandTimeout = 60

        sSql = "select top 1 ISNULL(lastoid,0) from QL_mstoid where tablename = '" & sTableName & "' and cmpcode='" & sCompnyCode & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        GenerateID = xCmd.ExecuteScalar + 1
        conn.Close()
        Return GenerateID
    End Function

    Public Overloads Shared Function DeleteData(ByVal sTableName As String, ByVal sColomnName As String, ByVal iKey As Int64, ByVal sCompanyCode As String) As Boolean 'return true if delete was successful
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "delete from " & sTableName & " where  " & sColomnName & "=" & iKey & " and cmpcode='" & sCompanyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            DeleteData = True
            conn.Close()
        Catch ex As Exception
            DeleteData = False
        End Try
        Return DeleteData
    End Function

    Public Overloads Shared Function DeleteMasterDetail(ByVal sTableName() As String, ByVal sColomnName As String, ByVal iKey As Int64, ByVal sCompanyCode As String) As Boolean 'return true if delete was successful
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
            For C1 As Int16 = 0 To UBound(sTableName)
                sSql = "delete from " & sTableName(C1) & " where  " & sColomnName & "=" & iKey & " and cmpcode='" & sCompanyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            DeleteMasterDetail = True
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            DeleteMasterDetail = False
        End Try
        Return DeleteMasterDetail
    End Function

    Public Overloads Shared Function DeleteMasterDetail(ByVal sTableName() As String, ByVal sColomnName() As String, ByVal iKey As Int64, ByVal sCompanyCode As String) As Boolean 'return true if delete was successful
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
            For C1 As Int16 = 0 To UBound(sTableName)
                sSql = "delete from " & sTableName(C1) & " where  " & sColomnName(C1) & "=" & iKey & " and cmpcode='" & sCompanyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            DeleteMasterDetail = True
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            DeleteMasterDetail = False
        End Try
        Return DeleteMasterDetail
    End Function

    Public Shared Function Tchar(ByVal sVar As String) As String
        Return (sVar.Trim.Replace("'", "''"))
    End Function

    Public Shared Function TcharNoTrim(ByVal sVar As String) As String
        Return (sVar.Replace("'", "''"))
    End Function

    Public Shared Function Tnumber(ByVal sVar As String) As String
        Return (sVar.Trim.Replace(",", "").Replace("_", ""))
    End Function

    Public Shared Function ToDouble(ByVal sVar As String) As Double
        Try
            ToDouble = (sVar.Trim.Replace(",", "").Replace("_", ""))
        Catch ex As Exception
            ToDouble = 0
        End Try
    End Function
    Public Shared Function ToMaskEdit2(ByVal dMoney As Double, ByVal iType As Byte) As String
        ToMaskEdit2 = "0"
        Try
            Dim sStr1 As String = CStr(dMoney)
            Dim sStr2 As String
            Dim sFormat As String = ""
            If sStr1.Contains(".") Then
                sStr2 = Left(sStr1, sStr1.IndexOf("."))
            Else
                sStr2 = sStr1
            End If
            For i As Integer = 1 To sStr2.Length
                sFormat = "#" & sFormat
                If i Mod 3 = 0 And i <> sStr2.Length Then
                    sFormat = "," & sFormat
                End If
            Next
            If iType = 0 Then ' 0 digit belakang koma
                ToMaskEdit2 = Format(dMoney, sFormat)
                If ToMaskEdit2.Trim = "" Then
                    ToMaskEdit2 = "0"
                End If
            ElseIf iType = 1 Then ' 1 digit belakang koma
                ToMaskEdit2 = Format(dMoney, sFormat & ".#")
                If ToMaskEdit2.Trim = "" Then
                    ToMaskEdit2 = "0.0"
                ElseIf ToMaskEdit2.IndexOf(".") = -1 Then
                    ToMaskEdit2 &= ".0"
                End If
            Else ' digit belakang koma > 1
                sFormat &= "."
                Dim sNol As String = ""
                For i As Integer = 1 To iType
                    sFormat &= "#"
                    sNol &= "0"
                Next
                ToMaskEdit2 = Format(dMoney, sFormat)
                If ToMaskEdit2.Trim = "" Then
                    ToMaskEdit2 = "0." & sNol
                ElseIf ToMaskEdit2.IndexOf(".") = -1 Then
                    ToMaskEdit2 &= "." & sNol
                Else
                    For j As Integer = 2 To iType
                        If ToMaskEdit2.Length - ToMaskEdit2.IndexOf(".") = j Then
                            ToMaskEdit2 &= Left(sNol, iType - j + 1)
                            Exit For
                        End If
                    Next
                End If
            End If
            If dMoney > 0 And dMoney < 1 Then
                ToMaskEdit2 = "0" & ToMaskEdit2
            End If
            Dim sSplit() As String = ToMaskEdit2.Split(".")
            If sSplit.Length > 1 Then
                Dim sKoma As String = ""
                Dim sTmp As String = sSplit(1)
                For i As Integer = sSplit(1).Length To 1 Step -1
                    If ToInteger(Strings.Mid(sSplit(1), i, 1)) <> 0 Then
                        sKoma = Left(sSplit(1), i)
                        Exit For
                    End If
                Next
                If sKoma <> "" Then
                    ToMaskEdit2 = sSplit(0) & "." & sKoma
                Else
                    ToMaskEdit2 = sSplit(0)
                End If
            End If
        Catch ex As Exception
        End Try
        Return ToMaskEdit2
    End Function
    Public Shared Function ToInteger(ByVal sVar As String) As Integer
        Try
            ToInteger = Integer.Parse((sVar.Trim.Replace(",", "").Replace("_", "")))
        Catch ex As Exception
            ToInteger = 0
        End Try
    End Function
    Public Shared Function ToMaskEdit(ByVal dMoney As Double, ByVal iType As Byte) As String
        ToMaskEdit = "0"
        Try
            If iType = 0 Then
                ToMaskEdit = Format(dMoney, "###,###,###,###")
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0"
                End If
            ElseIf iType = 1 Then
                ToMaskEdit = Format(dMoney, "###,###,###,###,###")
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0"
                End If
            ElseIf iType = 2 Then ' 1 digit blakang koma
                ToMaskEdit = Format(dMoney, "###,###,###,###.#")
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0.0"
                ElseIf ToMaskEdit.IndexOf(".") = -1 Then
                    ToMaskEdit &= ".0"
                End If
            ElseIf iType = 3 Then ' 2 digit blakang koma
                ToMaskEdit = Format(dMoney, "###,###,###,###.##")
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0.00"
                ElseIf ToMaskEdit.IndexOf(".") = -1 Then
                    ToMaskEdit &= ".00"
                Else
                    If ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 2 Then
                        ToMaskEdit &= "0"
                    End If
                End If
            ElseIf iType = 4 Then ' 4 digit blakang koma
                ToMaskEdit = Format(dMoney, "###,###,###,###.##")
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0.00"
                ElseIf ToMaskEdit.IndexOf(".") = -1 Then
                    ToMaskEdit &= ".00"
                Else
                    If ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 2 Then
                        ToMaskEdit &= "000"
                    ElseIf ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 3 Then
                        ToMaskEdit &= "00"
                    ElseIf ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 4 Then
                        ToMaskEdit &= "0"
                    ElseIf ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 5 Then
                        ToMaskEdit &= ""
                    End If
                End If
            ElseIf iType = 5 Then ' 6 digit blakang koma
                ToMaskEdit = Format(dMoney, "###,###,###,###.######")
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0.000000"
                ElseIf ToMaskEdit.IndexOf(".") = -1 Then
                    ToMaskEdit &= ".000000"
                Else
                    If ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 2 Then
                        ToMaskEdit &= "00000"
                    ElseIf ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 3 Then
                        ToMaskEdit &= "0000"
                    ElseIf ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 4 Then
                        ToMaskEdit &= "000"
                    ElseIf ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 5 Then
                        ToMaskEdit &= "00"
                    ElseIf ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = 6 Then
                        ToMaskEdit &= "0"
                    End If
                End If
            End If
            ' New Addition to Display '0' before decimal if 0 < dMoney < 1
            If dMoney > 0 And dMoney < 1 Then ToMaskEdit = "0" & ToMaskEdit
        Catch ex As Exception
        End Try
        Return ToMaskEdit
    End Function

    Public Shared Function GenNumberString(ByVal sPrefix As String, ByVal sMiddle As String, ByVal lNo As Long, ByVal iDefaultCounter As Int16) As String
        Dim iAdd As Int16 = lNo.ToString.Length - iDefaultCounter
        If iAdd > 0 Then
            iDefaultCounter += iAdd
        End If
        Dim sFormat As String = ""
        For c1 As Int16 = 1 To iDefaultCounter
            sFormat = sFormat & "0"
        Next
        Return (sPrefix & sMiddle & Format(lNo, sFormat))
    End Function

    Public Shared Function GenNumberString2(ByVal lNo As Long, ByVal sPrefix As String, ByVal sMiddle As String, ByVal iDefaultCounter As Int16) As String
        Dim iAdd As Int16 = lNo.ToString.Length - iDefaultCounter
        If iAdd > 0 Then
            iDefaultCounter += iAdd
        End If
        Dim sFormat As String = ""
        For c1 As Int16 = 1 To iDefaultCounter
            sFormat = sFormat & "0"
        Next
        Return (Format(lNo, sFormat) & sPrefix & sMiddle)
    End Function

    Public Shared Function toDate(ByVal sTgl As String) As String
        Dim aData() As String = sTgl.Split("/")
        Try
            toDate = aData(1) & "/" & aData(0) & "/" & aData(2)
        Catch ex As Exception
            toDate = ""
        End Try
        Return toDate
    End Function
    Public Shared Function GetAcctgOID(ByVal sVar As String, ByVal Scmpcode As String) As Integer
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("SELECT TOP 1 acctgoid FROM QL_mstacctg WHERE acctgcode='" & sVar & "' and cmpcode='" & Scmpcode & "'", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        GetAcctgOID = xCmd.ExecuteScalar
        conn.Close()
        Return GetAcctgOID
    End Function

    Public Shared Function GetInterfaceWarning(ByVal sVar As String, Optional ByVal aAction As String = "using") As String
        Return "Please Define some COA for Variable " & sVar & " in Accounting -> Master -> Interface before continue " & aAction & " this form!"
    End Function
    Public Shared Function GetServerTime() As Date
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Dim dateValue As Date

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "SELECT getdate() FROM ql_mstacctg"
        xCmd.CommandText = sSql
        Try
            dateValue = xCmd.ExecuteScalar
        Catch ex As Exception
            dateValue = Now
        End Try
        conn.Close()
        Return dateValue
    End Function

    Public Overloads Shared Function FillDDL2(ByRef oDDLObject As DropDownList, ByVal sSql As String, ByVal first As String) As Boolean ' to fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader

        FillDDL2 = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add(first)
        oDDLObject.Items(0).Value = "NULL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()

        'cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDL2 = False
        End If
        Return FillDDL2
    End Function

Public Shared Function IsValidDate(ByVal sDate As String, ByVal sFormat As String, ByRef sError As String) As Boolean
        If sFormat.Trim <> "MM/dd/yyyy" And sFormat.Trim <> "dd/MM/yyyy" Then
            sError = "Date format should be '<STRONG>MM/dd/yyyy</STRONG>' or '<STRONG>dd/MM/yyyy</STRONG>'."
            Return False
        End If
        If sFormat = "dd/MM/yyyy" Then
            sDate = toDate(sDate)
        End If
        Dim splitdate() As String = sDate.Split("/")
        If splitdate.Length < 3 Then
            sError = "Date is incomplete. Date component must be consisted of day, month and year value."
            Return False
        End If
        If splitdate.Length > 3 Then
            sError = "Date is out of range. Date component must be consisted of day, month and year value only."
            Return False
        End If
        If ToDouble(splitdate(2)) >= 1900 And ToDouble(splitdate(2)) <= 9999 Then
            If ToDouble(splitdate(0)) > 0 And ToDouble(splitdate(0)) < 13 Then
                Dim iNDay As Integer = Date.DaysInMonth(CInt(splitdate(2)), CInt(splitdate(0)))
                If ToDouble(splitdate(1)) < 1 Or ToDouble(splitdate(1)) > CDbl(iNDay) Then
                    sError = "Day value is invalid. Day value must be between 1 and " & iNDay.ToString & "."
                    Return False
                End If
            Else
                sError = "Month value is invalid. Month value must be between 1 and 12."
                Return False
            End If
        Else
            sError = "Year value is invalid. Year value must be between 1900 and 9999."
            Return False
        End If
        Return True
    End Function
    Public Overloads Shared Function IsInterfaceExists(ByVal sVar As String, Optional ByVal sCmpCode As String = "All") As Boolean
        IsInterfaceExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT COUNT(-1) FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "'  AND interfacevar='" & sVar & "'"
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            IsInterfaceExists = True
        Else
            sSql = "SELECT COUNT(-1) FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "'  AND interfacevar='" & sVar & "'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar > 0 Then
                IsInterfaceExists = True
            End If
        End If
        conn.Close()
        Return IsInterfaceExists
    End Function

    Public Shared Function FillDDLAcctg(ByRef oDDLObject As DropDownList, ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sNoneValue As String = "", Optional ByVal sNoneText As String = "", Optional ByVal bOnlyActiveData As Boolean = True) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillDDLAcctg = True
        oDDLObject.Items.Clear()
        Dim sSql As String = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & sCmpCode & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "?" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceres1='MSC' AND interfacevar='" & sVar & "'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "?" Then
            sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' " & IIf(bOnlyActiveData, " AND a.acctgflag='A' ", "") & " AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
            If sNoneText <> "" And sNoneValue <> "" Then
                FillDDLWithAdditionalText(oDDLObject, sSql, sNoneText, sNoneValue)
            Else
                FillDDL(oDDLObject, sSql)
            End If
        End If
        If oDDLObject.Items.Count = 0 Then
            FillDDLAcctg = False
        End If
        Return FillDDLAcctg
    End Function

    Public Shared Function FillDDLAcctgOther(ByRef oDDLObject As DropDownList, ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sNoneValue As String = "", Optional ByVal sNoneText As String = "", Optional ByVal bOnlyActiveData As Boolean = True) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillDDLAcctgOther = True
        oDDLObject.Items.Clear()
        Dim sSql As String = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & sCmpCode & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "?" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceres1='MSC' AND interfacevar='" & sVar & "'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "?" Then
            sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' " & IIf(bOnlyActiveData, " AND a.acctgflag='A' ", "") & " and branch_code = '" & sCmpCode & "' AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
            If sNoneText <> "" And sNoneValue <> "" Then
                FillDDLWithAdditionalText(oDDLObject, sSql, sNoneText, sNoneValue)
            Else
                FillDDL(oDDLObject, sSql)
            End If
        End If
        If oDDLObject.Items.Count = 0 Then
            FillDDLAcctgOther = False
        End If
        Return FillDDLAcctgOther
    End Function

    Public Overloads Shared Function FillDDLWithAdditionalText(ByRef oDDLObject As DropDownList, ByVal sSql As String, ByVal sText As String, ByVal sValue As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        FillDDLWithAdditionalText = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add(sText)
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = sValue
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithAdditionalText = False
        End If
        Return FillDDLWithAdditionalText
    End Function
    Public Overloads Shared Function FillGVAcctg(ByRef oGVObject As GridView, ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sFilter As String = "", Optional ByVal bOnlyActiveData As Boolean = True) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillGVAcctg = True
        oGVObject.DataSource = Nothing : oGVObject.DataBind()
        Dim sSql As String = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & sCmpCode & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "?" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & CompnyCode & "'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' " & IIf(bOnlyActiveData, " AND a.acctgflag='A' ", "") & " " & sFilter & " AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(oGVObject, sSql, "QL_mstacctg")
        End If
        If oGVObject.Rows.Count < 1 Then
            FillGVAcctg = False
        End If
        Return FillGVAcctg
    End Function
End Class
