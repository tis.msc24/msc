Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptTW
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedure"

    Sub TypeTW()
        DDLTypeTW.Items.Add(New ListItem("Pengiriman TW", "Pengiriman TW"))
        DDLTypeTW.Items.Add(New ListItem("Penerimaan TW", "Penerimaan TW"))
        DDLTypeTW.SelectedValue = "Pengiriman TW"
    End Sub

    Private Sub DdlCabang()
        Dim Sql, Deloc As String
        If DDLTypeTW.SelectedValue = "Pengiriman TW" Then
            sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='" & CompnyCode & "' And g.gengroup='CABANG'"

            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLFromBranch, sSql)
            ElseIf Session("UserLevel") = 2 Then
                If Session("branch_id") <> "10" Then
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(DDLFromBranch, sSql)
                Else
                    FillDDL(DDLFromBranch, sSql)
                    DDLFromBranch.Items.Add(New ListItem("ALL BRANCH", "ALL BRANCH"))
                    DDLFromBranch.SelectedValue = "ALL BRANCH"
                End If
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                FillDDL(DDLFromBranch, sSql)
                DDLFromBranch.Items.Add(New ListItem("ALL BRANCH", "ALL BRANCH"))
                DDLFromBranch.SelectedValue = "ALL BRANCH"
            End If

            Sql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='" & CompnyCode & "' And g.gengroup='CABANG'"
            FillDDL(DDLToBranch, Sql)
            DDLToBranch.Items.Add("ALL BRANCH")
            DDLToBranch.SelectedValue = "ALL BRANCH"
        Else
            sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='" & CompnyCode & "' And g.gengroup='CABANG'"

            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLToBranch, sSql)
            ElseIf Session("UserLevel") = 2 Then
                If Session("branch_id") <> "10" Then
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(DDLToBranch, sSql)
                Else
                    FillDDL(DDLToBranch, sSql)
                    DDLToBranch.Items.Add(New ListItem("ALL BRANCH", "ALL BRANCH"))
                    DDLToBranch.SelectedValue = "ALL BRANCH"
                End If
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                FillDDL(DDLToBranch, sSql)
                DDLToBranch.Items.Add(New ListItem("ALL BRANCH", "ALL BRANCH"))
                DDLToBranch.SelectedValue = "ALL BRANCH"
            End If
            Deloc = "Select gencode,gendesc From QL_mstgen g Where cmpcode='" & CompnyCode & "' And g.gengroup='CABANG'"
            FillDDL(DDLFromBranch, Deloc)
            DDLFromBranch.Items.Add("ALL BRANCH")
            DDLFromBranch.SelectedValue = "ALL BRANCH"
        End If
    End Sub

    Public Sub initddl()
        Dim FromBranch As String = "" : Dim Deloc As String = ""
        Dim keloc As String = "" : Dim sWhere As String = ""

        If DDLTypeTW.SelectedValue = "Pengiriman TW" Then
            If DDLFromBranch.SelectedValue <> "ALL BRANCH" Then
                sWhere &= " AND c.gencode = '" & DDLFromBranch.SelectedValue & "'"
            End If
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.cmpcode='MSC' " & sWhere & " ORDER BY a.gendesc"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add("ALL LOCATION")
            ddlLocation.SelectedValue = "ALL LOCATION"

            keloc = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 /*WHERE c.gencode = '" & DDLToBranch.SelectedValue & "'*/ ORDER BY a.gendesc"
            FillDDL(ddlLocation1, keloc)
            ddlLocation1.Items.Add("ALL LOCATION")
            ddlLocation1.SelectedValue = "ALL LOCATION"
        Else
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 ORDER BY a.gendesc"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add("ALL LOCATION")
            ddlLocation.SelectedValue = "ALL LOCATION"

            If DDLToBranch.SelectedValue <> "ALL BRANCH" Then
                sWhere &= " AND c.gencode = '" & DDLToBranch.SelectedValue & "'"
            End If

            keloc = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.cmpcode='MSC' " & sWhere & " ORDER BY a.gendesc"
            FillDDL(ddlLocation1, keloc)
            ddlLocation1.Items.Add("ALL LOCATION")
            ddlLocation1.SelectedValue = "ALL LOCATION"
        End If
    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub showPrintReport(ByVal sType As String)
        'lblkonfirmasi.Text = ""
        'Session("diprint") = "False"

        'If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
        '    Try
        '        Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
        '        Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
        '        If dDate1 < CDate("01/01/1900") Then
        '            lblkonfirmasi.Text = "Period 1 is invalid!" : Exit Sub
        '        End If
        '        If dDate2 < CDate("01/01/1900") Then
        '            lblkonfirmasi.Text = "Period 2 is invalid!" : Exit Sub
        '        End If
        '        If dDate1 > dDate2 Then
        '            lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
        '        End If
        '    Catch ex As Exception
        '        lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
        '    End Try
        'Else
        '    lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        'End If

        'Try

        '    Dim namaPDF As String = "" : Dim fBranch As String = ""
        '    Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, " AND i.itemoid=" & itemoid.Text, "")
        '    Dim swhere As String = "" : Dim UpdUser As String = ""
        '    Dim swhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", " ", " AND mtrlocoid=" & ddlLocation.SelectedValue & " ")
        '    '==============Special Akses===============
        '    If checkPagePermission("ReportForm/rptTW.aspx", Session("SpecialAccess")) = False Then
        '        UpdUser = "AND twm.upduser='" & Session("UserID") & "'"
        '    End If
        '    If kacabCek("ReportForm/rptTW.aspx", Session("UserID")) = True Then
        '        If Session("branch_id") <> "01" Then
        '            fBranch = Session("branch_id")
        '        End If
        '    End If

        '    vReport = New ReportDocument
        '    If DDLTypeTW.SelectedValue = "Send TW" Then
        '        vReport.Load(Server.MapPath("~\Report\rptTW.rpt"))
        '        swhere = "WHERE twm.trfmtrdate Between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " AND twm.trfmtrmstoid = '" & oid.Text & "' ") & IIf(ToDouble(itemoid.Text) > 0, " AND i.itemoid=" & itemoid.Text, " ") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", " AND twm.fromMtrBranch LIKE '%" & fBranch & "%'", " AND twm.fromMtrBranch LIKE '%" & fBranch & "%' AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & " ") & IIf(DDLToBranch.SelectedValue = "SEMUA BRANCH", " AND twm.toMtrBranch LIKE '%%' ", " AND twm.toMtrBranch =" & DDLToBranch.SelectedValue & " ") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", "", " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & " ") & " AND twm.transferno LIKE '%" & Tchar(nota.Text) & "%' AND i.itemdesc like '%" & Tchar(itemname.Text) & "%' " & UpdUser & " "
        '    Else
        '        vReport.Load(Server.MapPath("~\Report\rptTWReceive.rpt"))
        '        swhere = "WHERE twm.trfmtrdate Between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " AND twm.trfmtrmstoid = '" & oid.Text & "' ") & IIf(ToDouble(itemoid.Text) > 0, " AND i.itemoid=" & itemoid.Text, " ") & IIf(DDLFromBranch.SelectedValue = "SEMUA BRANCH", " AND twm.fromMtrBranch LIKE '%%' ", " AND twm.fromMtrBranch = '" & DDLFromBranch.SelectedValue & "' AND twm.frommtrlocoid like '%%'") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", " AND twm.frommtrlocoid LIKE '%%' ", " AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & " ") & IIf(DDLToBranch.SelectedValue = "SEMUA BRANCH", " and twm.toMtrBranch LIKE '%" & fBranch & "%'", " AND twm.toMtrBranch LIKE '%" & fBranch & "%'") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", " AND twm.toMtrBranch LIKE '%" & fBranch & "%'", " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & "") & " AND twm.transferno LIKE '%" & Tchar(nota.Text) & "%' AND i.itemdesc LIKE '%" & Tchar(itemname.Text) & "%' " & UpdUser & ""

        '    End If
        '    cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
        '    vReport.SetParameterValue("sWhere", swhere)
        '    vReport.SetParameterValue("startperiod", dateAwal.Text)
        '    vReport.SetParameterValue("TypeTW", DDLTypeTW.SelectedValue)
        '    vReport.SetParameterValue("endperiod", dateAkhir.Text)
        '    vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
        '    Session("diprint") = "True"
        '    crvMutasiStock.DisplayGroupTree = False
        '    crvMutasiStock.ReportSource = vReport
        'Catch ex As Exception
        '    lblkonfirmasi.Text = ex.Message
        '    Exit Sub
        'End Try
    End Sub

    Public Sub ShowPrint(ByVal sType As String)
        lblkonfirmasi.Text = "" : Session("diprint") = "False"
        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 1 is invalid!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 2 is invalid!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If

        Try
            Dim namaPDF As String = "" : Dim UpdUser As String = ""
            Dim fBranch As String = "" : Dim sWhere As String = ""
            Dim sWhereitem As String = IIf(ToDouble(itemoid.Text) > 0, " AND i.itemoid=" & itemoid.Text, "  ")
            Dim sWhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", " ", " AND mtrlocoid=" & ddlLocation.SelectedValue & " ")

            'If kacabCek("ReportForm/rptTW.aspx", Session("UserID")) = True Then
            '    If Session("branch_id") <> "10" Then
            If DDLFromBranch.SelectedValue <> "ALL BRANCH" Then
                fBranch = DDLFromBranch.SelectedValue
            End If
            '    End If
            'End If

            vReport = New ReportDocument
            If DDLTypeTW.SelectedValue = "Pengiriman TW" Then
                If sType = "Excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptTWExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptTW.rpt"))
                End If
                sWhere = "WHERE twm.trfmtrdate BETWEEN '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "' AND twm.transferno LIKE '%" & Tchar(nota.Text) & "%' "

                If DDLStatus.SelectedValue <> "ALL" Then
                    sWhere &= " and twm.status = '" & DDLStatus.SelectedValue & "'"
                End If

                If oid.Text.Trim <> "" Then
                    sWhere &= " AND twm.trfmtrmstoid = '" & oid.Text & "'"
                End If

                Dim arCode() As String = itemname.Text.Split(";")
                Dim sCodeIn As String = "" : Dim sQel As String = ""
                Dim adr As String = ""
                For C1 As Integer = 0 To arCode.Length - 1
                    If arCode(C1) <> "" Then
                        sCodeIn &= "'" & arCode(C1) & "',"
                    End If
                Next

                If sCodeIn <> "" Then
                    sWhere &= " AND i.itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                End If

                If DDLFromBranch.SelectedValue <> "ALL BRANCH" Then
                    sWhere &= " AND twm.fromMtrBranch LIKE '%" & DDLFromBranch.SelectedValue & "%'"
                End If

                If ddlLocation.SelectedValue <> "ALL LOCATION" Then
                    sWhere &= " AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & ""
                End If

                If DDLToBranch.SelectedValue <> "ALL BRANCH" Then
                    sWhere &= "AND twm.toMtrBranch =" & DDLToBranch.SelectedValue & ""
                End If

                If ddlLocation1.SelectedValue <> "ALL LOCATION" Then
                    sWhere &= " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & ""
                End If
 
            Else 'Type Penerimaan

                If sType = "Print Excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptTWReceiveExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptTWReceive.rpt"))
                End If

                sWhere = "WHERE tc.trntrfdate BETWEEN '" & toDate(dateAwal.Text) & " 00:00:00' AND '" & toDate(dateAkhir.Text) & " 23:59:59' AND tc.intransferno LIKE '%" & Tchar(nota.Text) & "%' "
                If oid.Text.Trim <> "" Then
                    sWhere &= " And tc.intransferoid = '" & oid.Text & "'"
                End If
                Dim arCode() As String = itemname.Text.Split(";")
                Dim sCodeIn As String = "" : Dim sQel As String = ""
                Dim adr As String = ""
                For C1 As Integer = 0 To arCode.Length - 1
                    If arCode(C1) <> "" Then
                        sCodeIn &= "'" & arCode(C1) & "',"
                    End If
                Next

                If sCodeIn <> "" Then
                    sWhere &= " AND i.itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                End If
                If DDLFromBranch.SelectedValue <> "ALL BRANCH" Then
                    sWhere &= " And tc.fromBranch = '" & DDLFromBranch.SelectedValue & "'"
                End If

                If DDLToBranch.SelectedValue <> "ALL BRANCH" Then
                    sWhere &= " AND tc.toBranch LIKE '%" & DDLToBranch.SelectedValue & "%'"
                End If

                If ddlLocation.SelectedValue <> "ALL LOCATION" Then
                    sWhere &= " AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & ""
                End If

                If ddlLocation1.SelectedValue <> "ALL LOCATION" Then
                    sWhere &= " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & ""
                End If

            End If

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("TypeTW", DDLTypeTW.SelectedValue)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            If sType = "View" Then
                'crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
                crvMutasiStock.SeparatePages = True
            ElseIf sType = "Pdf" Then
                vReport.PrintOptions.PrinterName = ""
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TW_" & Format(GetServerTime, "dd_MM_yy"))
                Catch ex As Exception
                    vReport.Close() : vReport.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "TW_" & Format(GetServerTime, "dd_MM_yy"))
                Catch ex As Exception
                    Response.Buffer = False
                    Response.ClearHeaders()
                    Response.ClearContent()
                    vReport.Close() : vReport.Dispose()
                End Try
            End If
            'Response.Redirect("~\ReportForm\rptTW.aspx?awal=true")
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Public Sub showPrintExcel()
        lblkonfirmasi.Text = "" : Session("diprint") = "False"
        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 1 is invalid!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 2 is invalid!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If

        Try
            Dim namaPDF As String = "" : Dim swhere As String = ""
            Dim UpdUser As String = "" : Dim fBranch As String = ""
            Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, " AND i.itemoid=" & itemoid.Text, " ")
            Dim swhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and mtrlocoid=" & ddlLocation.SelectedValue & " ")

            If checkPagePermission("ReportForm/rptTW.aspx", Session("SpecialAccess")) = False Then
                UpdUser = " AND twm.upduser='" & Session("UserID") & "'"
            End If
            If kacabCek("ReportForm/rptTW.aspx", Session("UserID")) = True Then
                If Session("branch_id") <> "01" Then
                    fBranch = Session("branch_id")
                End If
            End If

            Dim arCode() As String = itemname.Text.Split(";")
            Dim sCodeIn As String = "" : Dim sQel As String = ""
            Dim adr As String = "" : Dim sItemCode As String = ""
            For C1 As Integer = 0 To arCode.Length - 1
                If arCode(C1) <> "" Then
                    sCodeIn &= "'" & arCode(C1) & "',"
                End If
            Next

            If sCodeIn <> "" Then
                sItemCode &= " AND i.itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
            End If

            vReport = New ReportDocument
            If DDLTypeTW.SelectedValue = "Pengiriman TW" Then
                vReport.Load(Server.MapPath("~\Report\rptTW.rpt"))
                swhere = "WHERE twm.trfmtrdate between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " AND twm.trfmtrmstoid = '" & oid.Text & "' ") & sItemCode & IIf(ddlLocation.SelectedValue = "ALL LOCATION", " AND twm.fromMtrBranch LIKE '%" & fBranch & "%' ", " AND twm.fromMtrBranch LIKE '%" & fBranch & "%' AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & " ") & IIf(DDLToBranch.SelectedValue = "SEMUA BRANCH", " AND twm.toMtrBranch like '%%' ", " AND twm.toMtrBranch =" & DDLToBranch.SelectedValue & " ") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", "", " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & " ") & " AND twm.transferno like '%" & Tchar(nota.Text) & "%' "
            Else
                vReport.Load(Server.MapPath("~\Report\rptTWReceive.rpt"))
                swhere = "WHERE twm.trfmtrdate between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " AND twm.trfmtrmstoid = '" & oid.Text & "' ") & sItemCode & IIf(DDLFromBranch.SelectedValue = "SEMUA BRANCH", " AND twm.fromMtrBranch like '%%' ", " AND twm.fromMtrBranch = '" & DDLFromBranch.SelectedValue & "' AND twm.frommtrlocoid like '%%'") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "AND twm.frommtrlocoid like '%%' ", " AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & " ") & IIf(DDLToBranch.SelectedValue = "SEMUA BRANCH", " AND twm.toMtrBranch LIKE '%" & fBranch & "%' ", " AND twm.toMtrBranch LIKE '%" & fBranch & "%'") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", " AND twm.toMtrBranch LIKE '%" & fBranch & "%'", " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & " ") & " AND twm.transferno like '%" & Tchar(nota.Text) & "%' "

            End If
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.PrintOptions.PrinterName = ""
            vReport.PrintOptions.PaperSize = PaperSize.PaperLegal
            Dim paperMargin As CrystalDecisions.Shared.PageMargins
            paperMargin.leftMargin = 1 * 567
            paperMargin.rightMargin = 1 * 567
            paperMargin.topMargin = 1 * 567
            paperMargin.bottomMargin = 1 * 567
            vReport.PrintOptions.ApplyPageMargins(paperMargin)
            vReport.SetParameterValue("sWhere", swhere)
            vReport.SetParameterValue("TypeTW", DDLTypeTW.SelectedValue)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "TW_" & Format(GetServerTime, "dd_MM_yy"))
            vReport.Close() : vReport.Dispose()
            Response.Redirect("~\ReportForm\rptTW.aspx?awal=true")
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Public Sub showPrintExcel(ByVal name As String)
        Dim sWhere As String = "" : lblkonfirmasi.Text = ""

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        Response.Clear()

        Dim arCode() As String = itemname.Text.Split(";")
        Dim sCodeIn As String = "" : Dim sQel As String = ""
        Dim adr As String = "" : Dim sItemCode As String = ""
        For C1 As Integer = 0 To arCode.Length - 1
            If arCode(C1) <> "" Then
                sCodeIn &= "'" & arCode(C1) & "',"
            End If
        Next

        If sCodeIn <> "" Then
            sItemCode &= " AND i.itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
        End If

        Dim sSql As String = ""
        sWhere = " where twm.transferno like 'TW%' AND twm.trfmtrdate between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " AND twm.trfmtrmstoid = '" & oid.Text & "' ") & sItemCode & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & " ") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", "  ", " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & " ") & " and twm.transferno like '%" & Tchar(nota.Text) & "%' "

        sSql = "select  convert(char(10),twm.trfmtrdate,101) as Tanggal, twm.transferno as [Nomor Transfer], twm.upduser 'User ID', g1.gendesc as [Gudang Asal], " & _
        "g2.gendesc as [Gudang Tujuan], twm.trfmtrnote as [Transfer Note], twd.seq as [No.urut], i.itemcode as [Kode Barang], i.itemdesc as [Nama Barang], twd.qty," & _
        "g3.gendesc as [Satuan], " & _
        "case " & _
        "when twd.unitoid=i.satuan1 " & _
        "  then (select amount1 from ql_conmat cm where cm.type='TW' " & _
        "  AND cm.formoid=twd.trfmtrdtloid " & _
        "  AND cm.refoid=twd.refoid AND cm.typeMin='-1') " & _
        "when twd.unitoid=i.satuan2 " & _
        "  then (select amount2 from ql_conmat cm where cm.type='TW' " & _
        "  AND cm.formoid=twd.trfmtrdtloid " & _
        "  AND cm.refoid=twd.refoid AND cm.typeMin='-1') " & _
        "when twd.unitoid=i.satuan3 " & _
        "  then (select amount3 from ql_conmat cm where cm.type='TW' " & _
        "  AND cm.formoid=twd.trfmtrdtloid " & _
        "  AND cm.refoid=twd.refoid AND cm.typeMin='-1') " & _
        "End 'Harga'," & _
        "twd.trfdtlnote as [Note Barang] from ql_trntrfmtrmst twm " & _
        "inner join ql_trntrfmtrdtl twd on twd.trfmtrmstoid = twm.trfmtrmstoid " & _
        "inner join ql_mstgen g1 on g1.genoid = twm.frommtrlocoid " & _
        "inner join ql_mstgen g2 on g2.genoid = twm.tomtrlocoid " & _
        "inner join ql_mstitem i on i.itemoid = twd.refoid " & _
        "inner join ql_mstgen g3 on g3.genoid = twd.unitoid " & _
        "" & sWhere & " order by  twm.trfmtrdate, twd.seq "

        Response.AddHeader("content-disposition", "inline;filename=Transfer_Warehouse.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
    End Sub

    Public Sub BindDataListItem()
        Try
            Dim sWhere As String = " WHERE twm.trfmtrdate between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "'"

            If DDLFromBranch.SelectedValue <> "ALL BRANCH" Then
                sWhere &= " AND fromMtrBranch = '" & DDLFromBranch.SelectedValue & "'"
            End If
            If ddlLocation.SelectedValue <> "ALL LOCATION" Then
                sWhere &= " AND FromMtrlocoid=" & ddlLocation.SelectedValue & ""
            End If
            If DDLToBranch.SelectedValue <> "ALL BRANCH" Then
                sWhere &= " AND toMtrBranch=" & DDLToBranch.SelectedValue & ""
            End If
            If ddlLocation1.SelectedValue <> "ALL LOCATION" Then
                sWhere &= " AND ToMtrlocOid=" & ddlLocation1.SelectedValue & ""
            End If

            'If DDLTypeTW.SelectedValue = "Pengiriman TW" Then
            '    swhere = "WHERE twm.trfmtrdate between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " AND twm.trfmtrmstoid = '" & oid.Text & "' ") & IIf(ToDouble(itemoid.Text) > 0, "  AND i.itemoid=" & itemoid.Text, "  ") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", " AND twm.fromMtrBranch = '" & Session("branch_id") & "' ", " AND twm.fromMtrBranch = '" & Session("branch_id") & "' AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & " ") & IIf(DDLToBranch.SelectedValue = "SEMUA BRANCH", " AND twm.toMtrBranch like '%%' ", " AND twm.toMtrBranch =" & DDLToBranch.SelectedValue & " ") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", "", " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & " ") & " AND twm.transferno like '%" & Tchar(nota.Text) & "%' AND ql_mstitem.itemdesc like '%" & Tchar(itemname.Text) & "%'"
            'Else
            '    swhere = "WHERE twm.trfmtrdate between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " AND twm.trfmtrmstoid = '" & oid.Text & "' ") & IIf(ToDouble(itemoid.Text) > 0, "  AND i.itemoid=" & itemoid.Text, "  ") & IIf(DDLFromBranch.SelectedValue = "SEMUA BRANCH", " AND twm.fromMtrBranch like '%%' ", " AND twm.fromMtrBranch = '" & DDLFromBranch.SelectedValue & "' AND twm.frommtrlocoid like '%%'") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "AND twm.frommtrlocoid like '%%' ", " AND twm.frommtrlocoid=" & ddlLocation.SelectedValue & " ") & IIf(DDLToBranch.SelectedValue = "SEMUA BRANCH", " AND twm.toMtrBranch = '" & Session("branch_id") & "' ", " AND twm.toMtrBranch ='" & Session("branch_id") & "' ") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", " AND twm.toMtrBranch = '" & Session("branch_id") & "'", " AND twm.tomtrlocoid =" & ddlLocation1.SelectedValue & " ") & "  AND ql_mstitem.itemdesc like '%" & Tchar(itemname.Text) & "%' OR ql_mstitem.itemcode like '%" & Tchar(itemname.Text) & "%'"
            'End If
            sSql = "SELECT DISTINCT 'False' Checkvalue,m.itemcode, m.itemdesc,m.itemoid, g.gendesc satuan3, m.merk, m.stockflag, Case m.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya from ql_mstitem m INNER JOIN QL_trntrfmtrdtl dl ON dl.refoid = m.itemoid INNER JOIN QL_trntrfmtrmst twm ON twm.trfmtrmstoid = dl.trfmtrmstoid inner join ql_mstgen g on g.genoid=satuan1 and g.gengroup='ITEMUNIT' AND itemflag='AKTIF' " & sWhere & " AND twm.transferno LIKE '%" & Tchar(nota.Text.Trim) & "%' AND (itemcode like '%" & Tchar(itemname.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(itemname.Text.Trim) & "%') Order By itemcode Desc"

            Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstitem")
            Session("TblMat") = dt
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind() : gvItem.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Public Sub bindDataListPO()
        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 1 is invalid!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 2 is invalid!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If
        
        sSql = "SELECT trfmtrmstoid, transferno, CONVERT(VarChar(20),trfmtrdate,103) trfmtrdate, status FROM ql_trntrfmtrmst twm WHERE twm.trfmtrdate between '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "' AND transferno LIKE '%" & TcharNoTrim(nota.Text) & "%'"

        If DDLFromBranch.SelectedValue <> "ALL BRANCH" Then
            sSql &= " AND fromMtrBranch = '" & DDLFromBranch.SelectedValue & "'"
        End If

        If ddlLocation.SelectedValue <> "ALL LOCATION" Then
            sSql &= " AND FromMtrlocoid=" & ddlLocation.SelectedValue & ""
        End If

        If DDLToBranch.SelectedValue <> "ALL BRANCH" Then
            sSql &= " AND toMtrBranch=" & DDLToBranch.SelectedValue & ""
        End If

        If ddlLocation1.SelectedValue <> "ALL LOCATION" Then
            sSql &= " AND ToMtrlocOid=" & ddlLocation1.SelectedValue & ""
        End If

        sSql &= " Order By trfmtrmstoid Desc "

        FillGV(GVNota, sSql, "ql_trntrfmtrmst")
        GVNota.Visible = True
    End Sub

#End Region

#Region "Event"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()

            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("SpecialAccess") = xsetAcc
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\ReportForm\rptTW.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Transfer Warehouse "
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            'Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not IsPostBack Then
            TypeTW() : DdlCabang() : initddl()
            dateAwal.Text = Format(GetServerTime, "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime, "dd/MM/yyyy")
            Session("ShowPrint") = False
            DDLTypeTW_SelectedIndexChanged(Nothing, Nothing)
        End If
        'If Session("ShowPrint") = "True" Then
        '    ShowPrint("View")
        'End If
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvItem.DataSource = Nothing : gvItem.DataBind()
        BindDataListItem()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowPrint("View")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptTW.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        'showPrintExcel()
        ShowPrint("Excel")
    End Sub

    Protected Sub btnSearchNota_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchNota.Click
        bindDataListPO()
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        GVNota.PageIndex = e.NewPageIndex
        bindDataListPO()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nota.Text = ""
        oid.Text = ""
        GVNota.Visible = False
    End Sub

    Protected Sub GVNota_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVNota.SelectedIndexChanged
        nota.Text = GVNota.SelectedDataKey.Item("transferno")
        oid.Text = GVNota.SelectedDataKey.Item("trfmtrmstoid")
        GVNota.Visible = False
    End Sub

    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        ShowPrint("Pdf")
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub

    Protected Sub DDLTypeTW_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DDLTypeTW.SelectedIndexChanged
        DdlCabang() : initddl()
        If DDLTypeTW.SelectedValue = "Pengiriman TW" Then
            Label2.Visible = True : Label3.Visible = True
            DDLStatus.Visible = True
        Else
            Label2.Visible = False : Label3.Visible = False
            DDLStatus.Visible = False
        End If
    End Sub

    Protected Sub DDLFromBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DDLFromBranch.SelectedIndexChanged
        initddl()
    End Sub

    Protected Sub DDLToBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DDLToBranch.SelectedIndexChanged
        initddl()
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        ShowPrint("View")
    End Sub
#End Region
    
    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next

            If dd_stock.SelectedValue <> "ALL" Then
                sFilter &= " AND stockflag='" & dd_stock.SelectedValue & "'"
            End If

            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage("Maaf, Data barang yang anda cari tidak ada..!!", 2)
                mpeListMat.Show()
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            BindDataListItem()
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "Checkvalue=True"
        If dv.Count > 0 Then

            If itemname.Text <> "" Then
                For C1 As Integer = 0 To dv.Count - 1
                    itemname.Text &= ";" & dv(C1)("itemcode").ToString & ";"
                Next
            Else
                For C1 As Integer = 0 To dv.Count - 1
                    itemname.Text &= dv(C1)("itemcode").ToString & ";"
                Next
            End If


            If itemname.Text <> "" Then
                itemname.Text = Left(itemname.Text, itemname.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            showMessage("- Maaf, Silahkan pilih katalog, dengan beri centang pada kolom pilih..<BR>", 2)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub
End Class
