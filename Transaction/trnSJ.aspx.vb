﻿Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_trnSJ
    Inherits System.Web.UI.Page

#Region "Variables"
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public toleranceSDO As Double = ConfigurationSettings.AppSettings("tolerance")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dtStaticItem As DataTable
    'Dim cFunction As New ClassFunction
    Dim mySqlConn As New SqlConnection(ConnStr)
    Dim mysql As String
    Dim reportSJ As New ReportDocument
    Dim cKoneksi As New Koneksi
    Dim cfunction As New ClassFunction
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String
    Dim sudahcek As Integer
    Dim ceksave As Integer = 0
    Dim ceklist As Integer = 0

    Dim KeyEdit As String
#End Region

#Region "Functions"

    Private Function checkApprovaldata(ByVal sTableName As String, ByVal sEvent As String, ByVal sOid As String, _
       ByVal sRequestUser As String, ByVal sStatus As String, ByVal sbranch As String) As DataTable
        Dim ssql As String = "SELECT CMPCODE,APPROVALOID,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME," & _
             "OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval WHERE CmpCode='" & CompnyCode & _
             "' and RequestUser LIKE '" & sRequestUser & "%' and STATUSREQUEST = '" & sStatus & "' " & _
             " AND TABLENAME='" & sTableName & "' and OID=" & sOid & " and EVENT='" & sEvent & "' and branch_code='" & sbranch & "' ORDER BY REQUESTDATE DESC"
        Return cKoneksi.ambiltabel(ssql, "QL_APPROVAL")
    End Function

    Private Function setTabelSN() As DataTable
        Dim dtSN As New DataTable
        dtSN.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSN.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSN.Columns.Add("SN", Type.GetType("System.String"))
        Return dtSN
    End Function

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("seqglobal", Type.GetType("System.Int32"))
        dt.Columns.Add("item", Type.GetType("System.String"))
        dt.Columns.Add("Unit", Type.GetType("System.String"))
        dt.Columns.Add("qty", Type.GetType("System.Int32"))
        dt.Columns.Add("Collie", Type.GetType("System.String"))
        dt.Columns.Add("notedtl", Type.GetType("System.String"))
        Return dt
    End Function

    Private Function checkOtherShipment(ByVal sRefType As String, ByVal iRefoid As Integer, _
    ByVal iItemOid As Integer, ByVal cmpcode As String) As Boolean

        sSql = "SELECT COUNT(-1) FROM ql_trnsjjualdtl jd INNER JOIN ql_trnsjjualmst j ON jd.trnsjjualmstoid =j.trnsjjualmstoid  AND j.cmpcode=jd.cmpcode AND j.trnsjjualstatus  not in ('Approved','INVOICED','HISTORY','Rejected') inner join QL_trnordermst o on o.orderno = j.orderno  WHERE jd.refoid =" & iItemOid & " and refname = 'QL_mstitem' AND jd.cmpcode='" & CompnyCode & "' and o.branch_code=j.branch_code AND o.ordermstoid=" & iRefoid
        If I_U.Text <> "New" Then
            sSql &= " AND j.trnsjjualmstoid<>" & Session("oid")
        End If
        Return cKoneksi.ambilscalar(sSql) > 0
    End Function
#End Region

#Region "Procedures"

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub fCabangDdl()
        'sSql = "select gencode,gendesc from ql_mstgen Where gengroup='cabang'"
        'If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
        '    sSql &= " AND gencode='" & Session("branch_id") & "'"
        '    FillDDL(DDLPengirim, sSql)
        'ElseIf Session("UserLevel") = 2 Then
        '    If Session("branch_id") <> "10" Then
        '        sSql &= " AND gencode='" & Session("branch_id") & "'"
        '        FillDDL(DDLPengirim, sSql)
        '    Else
        '        FillDDL(DDLPengirim, sSql)
        '    End If
        'ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
        '    FillDDL(DDLPengirim, sSql)
        'End If
    End Sub

    Private Sub InitAllDDL()
        sSql = "SELECT gendesc, gendesc FROM QL_mstgen WHERE gengroup = 'ITEMUNIT'"
        FillDDL(DDLUnit, sSql)
        sSql = "SELECT genoid, gendesc kota from QL_mstgen WHERE gengroup = 'City'"
        FillDDL(DDLKota, sSql)
        sSql = "select genoid, gendesc from ql_mstgen where gengroup = 'CABANG'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLPengirim, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLPengirim, sSql)
            Else
                FillDDL(DDLPengirim, sSql)
                DDLPengirim.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLPengirim, sSql)
            DDLPengirim.SelectedValue = "10"
        End If 
    End Sub

    Private Sub FillTextBox(ByVal iID As Int64, ByVal branch_code As String)
        'btnDelete.Visible = True
        'lbltext.Text = "Last Update"
        '@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
        sSql = "SELECT sjglobalmstoid, sjglobalno, tglberlaku, noplis, tglSj, formAddres, toAdderes, note, statusSjglobal, upduser , updtime , createuser, createtime, personoid from ql_trnsjglobalmst where sjglobalmstoid = " & iID & " AND branch_code='" & branch_code & "'"
        '@@@@@@@@@@@@@@@@@@@@@@@@@@
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                oid.Text = Trim(xreader.Item("sjglobalmstoid"))
                trnsjjualno.Text = Trim(xreader.Item("sjglobalno"))
                DDLPengirim.SelectedValue = Trim(xreader.Item("personoid"))
                trnBerlakudate.Text = Format(CDate(xreader.Item("tglberlaku").ToString), "dd/MM/yyyy")
                nopolisi.Text = Trim(xreader.Item("noplis"))
                SJdate.Text = Format(CDate(xreader.Item("tglSj").ToString), "dd/MM/yyyy")
                Pengirim.Text = Trim(xreader.Item("formAddres"))
                Tujaun.Text = Trim(xreader.Item("toAdderes"))
                NoteMst.Text = Trim(xreader.Item("note"))
                posting.Text = Trim(xreader.Item("statusSjglobal"))
                updUser.Text = Trim(xreader.Item("upduser"))
                updTime.Text = Format(CDate(xreader.Item("updtime").ToString), "dd/MM/yyyy HH:mm:ss")
                createuser.Text = Trim(xreader.Item("createuser"))
                createtime.Text = Format(CDate(xreader.Item("createtime").ToString), "dd/MM/yyyy  HH:mm:ss")
                If posting.Text = "In Process" Then
                    btnSave.Visible = True
                    btnPosting.Visible = True
                    btnDelete.Visible = True
                    btnAddtolist.Visible = True
                    imbSendApproval.Visible = False
                    btnPrintSJ.Visible = False
                Else
                    btnSave.Visible = False
                    btnPosting.Visible = False
                    btnDelete.Visible = False
                    btnAddtolist.Visible = False
                    imbSendApproval.Visible = False
                    btnPrintSJ.Visible = True
                End If
            End While

        End If
        xreader.Close()
        conn.Close()
        imbSendApproval.Visible = False
        'data detail
        sSql = "SELECT seqglobal, item, qty, unit, notedtl, Collie from QL_trnSjGlobaldtl WHERE sjglobalmstoid = " & iID & " AND branch_code='" & branch_code & "'"
        Dim dtl As DataTable = cKoneksi.ambiltabel(sSql, "Datanya")
        tbldtl.DataSource = dtl : tbldtl.DataBind()
        Session("TblDtl") = dtl : I_u2.Text = "New Detail"
    End Sub

    Private Sub BindData(ByVal sqlPlus As String)
        Try
            sSql = "SELECT sjglobalmstoid, sjglobalno, formAddres kepada, tglSj, toAdderes, statusSjglobal, branch_code, (select gendesc from ql_mstgen m where m.gencode=sj.branch_code AND m.gengroup='CABANG') as cabang, note from ql_trnsjglobalmst sj Where cmpcode='" & CompnyCode & "'"
            Dim sWhere As String = ""

            Dim periodsj1 As Date = CDate(toDate(FilterPeriod1.Text))
            Dim periodsj2 As Date = CDate(toDate(FilterPeriod2.Text))
            Dim periodsj As String
            periodsj = " and tglsj BETWEEN '" & periodsj1 & "' and '" & periodsj2 & "'"

            If txtFindSJ.Text <> "" Then
                sSql &= " and " & ddlFilter.SelectedValue & " like '%" & txtFindSJ.Text & "%'"
            End If

            If DDLStatus.SelectedValue <> "All" Then
                sSql &= " AND statusSjglobal LIKE '%" & DDLStatus.SelectedValue & "%'"
            End If
            If ddlcabang1.SelectedValue <> "ALL" Then
                sSql &= " AND branch_code = '" & ddlcabang1.SelectedValue & "'"
            End If
            If chkUpdateVSTrans.Checked Then
                sSql &= " AND CAST(CONVERT(VARCHAR(10),updtime,111) AS DATE)<>CAST(CONVERT(VARCHAR(10),tglberlaku,111) AS DATE) "
            End If

            sSql &= sqlPlus & periodsj & " ORDER BY tglSj desc, case statusSjglobal when 'In Process' then 1 when 'Post' then 2 end, statusSjglobal desc"
            sSql &= sWhere
            sqlTempSearch = sSql

            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDs As New DataSet
            mySqlDA.Fill(objDs, "data")
            Session("tbldata") = objDs.Tables("data")
            gvTblData.DataSource = objDs.Tables("data")
            gvTblData.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - WARNING", 2)
        End Try
    End Sub

    Private Sub binddataSalesOrder(ByVal sQuery As String)
        sSql = "SELECT itemoid, itemcode, itemdesc, Merk from QL_mstItem where  cmpcode='" & CompnyCode & "' " & sQuery
        FillGV(gvReference, sSql, "ql_trnorderdtl")
        gvReference.Visible = True
    End Sub 

    Private Sub ClearDetail()
        itemname.Text = ""
        QtySJ.Text = ""
        txtLenght.Text = ""
        TextCollie.Text = ""
        DDLUnit.SelectedIndex = 0
        gvReference.Visible = False
        cProc.DisposeGridView(gvReference)
    End Sub

    Private Sub GenerateNumberString()
        Dim awal As String = "" : Dim lokasi As String = ""
        Dim tax As String = ""

        If trnsjjualno.Text <> "" Then
            awal = "SJ" : lokasi = "L"
        End If

        KeyEdit = trnsjjualno.Text
        Dim CodeCabang As String = GetStrData("select gencode from ql_mstgen Where gengroup='CABANG' AND genoid=" & DDLPengirim.SelectedValue & "")
        Dim CabangNya As String = GetStrData("select genother1 from ql_mstgen Where gengroup='CABANG' AND genoid=" & DDLPengirim.SelectedValue & "")

        Dim sNo As String = awal & tax & "/" & CabangNya & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(sjglobalno,'" & sNo & "',''))),0)+1 FROM ql_trnsjglobalmst WHERE sjglobalno LIKE '" & sNo & "%' and branch_code='" & CodeCabang & "'"
        trnsjjualno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
    End Sub

    Private Sub generateNo()
        trnsjjualno.Text = GenerateID("ql_trnsjglobalmst", CompnyCode)
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlcabang1, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlcabang1, sSql)
            Else
                FillDDL(ddlcabang1, sSql)
                ddlcabang1.Items.Add(New ListItem("ALL", "ALL"))
                ddlcabang1.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlcabang1, sSql)
            ddlcabang1.Items.Add(New ListItem("ALL", "ALL"))
            ddlcabang1.SelectedValue = "ALL"
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        'showMessage(Mid("SO1/15/03/L/01/0062", 13, 2), "", 3)
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 1300
            Response.Redirect("~\Transaction\trnSJ.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Travel Document"
        Session("oid") = Request.QueryString("oid")
        Session("code_branch") = Request.QueryString("branch_code")
        lbltext.Text = "Create"
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data ?');")
        imbSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data to Approval?');")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data ?');")
        btnPrintSJ.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to PRINT this data ?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            Session("ddlFilterIndex") = Nothing
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            fDDLBranch() : InitAllDDL() : BindData("")
            Session("salesdeliverydtloid") = GenerateID("ql_trnsjjualdtl", CompnyCode)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                I_U.Text = "Update"
                FillTextBox(Session("oid"), Session("code_branch"))
                TabContainer1.ActiveTabIndex = 1
            Else
                I_U.Text = "New" : generateNo()
                createuser.Text = Session("UserID")
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy  HH:mm:ss")

                Session("ItemLine") = 1
                trnBerlakudate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                SJdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                btnSave.Visible = True
                btnCancel.Visible = True
                btnDelete.Visible = False
                btnPrintSJ.Visible = False
                imbSendApproval.Visible = False
                tbldtl.DataSource = Nothing
                tbldtl.DataSource = Session("itemdetail")
                tbldtl.DataBind()
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        'TabContainer1.Visible = True

        If I_U.Text = "New" Then
            imbSendApproval.Visible = False
        Else
            If posting.Text = "In Approval" Or posting.Text = "Approved" Or posting.Text = "INVOICED" Or posting.Text = "Rejected" Then
                imbSendApproval.Visible = False
            Else
                imbSendApproval.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnMsgBoxOK.Click
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()
    End Sub

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbFindItem.Click
        If itemname.Text.Trim() <> "" Then
            binddataSalesOrder(" AND itemdesc LIKE '%" & Tchar(itemname.Text) & "%'")
        Else
            binddataSalesOrder("")
        End If
    End Sub

    Protected Sub gvReference_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gvReference.PageIndexChanging
        gvReference.PageIndex = e.NewPageIndex
        If itemname.Text.Trim() <> "" Then
            binddataSalesOrder(" AND itemdesc LIKE '%" & Tchar(itemname.Text) & "%'")
        Else
            binddataSalesOrder("")
        End If
    End Sub

    Protected Sub gvReference_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvReference.SelectedIndexChanged
        itemname.Text = gvReference.SelectedDataKey.Item(2)
        gvReference.Visible = False
    End Sub

    Protected Sub imbClearItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbClearItem.Click
        ClearDetail()
    End Sub

    Protected Sub btnAddtolist_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnAddtolist.Click
        Dim sMsg As String = ""
        If txtLenght.Text.Length > 500 Then
            sMsg &= "- No can not more then 500 character !!<BR>"
        End If

        If trnsjjualno.Text = "" Then
            sMsg &= "- No Document can't empty !!<BR>"
        End If

        If Pengirim.Text = "" Then
            sMsg &= "- Kepada  tidak Boleh Kosong !!<BR>"
        End If

        If Tujaun.Text = "" Then
            sMsg &= "- Alamat Tujuan Pengiriman !!<BR>"
        End If
        If itemname.Text = "" Then
            sMsg &= "- Please Item tidak boleh kosong !!<BR>"
        End If
        If QtySJ.Text = "" Then
            sMsg &= "- Quantity must be greater than 0 !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetail()
            Session("TblDtl") = dtlTable
        End If

        Dim objTable As DataTable
        objTable = Session("TblDtl")
        Dim dv As DataView = objTable.DefaultView
        'Cek apa sudah ada item yang sama dalam Tabel Detail

        If I_u2.Text = "New Detail" Then
            dv.RowFilter = "item= '" & Tchar(itemname.Text) & "'"
        Else
            dv.RowFilter = "item= '" & Tchar(itemname.Text) & "'  AND seqglobal<>" & seqglobal.Text
        End If
        If dv.Count > 0 Then
            showMessage("This Data has been added before, please check!", CompnyName & " - WARNING", 2)
            dv.RowFilter = ""
            Exit Sub
        End If
        dv.RowFilter = ""
        'insert/update to list data
        Dim objRow As DataRow
        If I_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("seqglobal") = objTable.Rows.Count + 1
        Else
            Dim selrow As DataRow() = objTable.Select("seqglobal=" & seqglobal.Text)
            objRow = selrow(0)
            objRow.BeginEdit()
        End If
        objRow("item") = itemname.Text
        objRow("Unit") = DDLUnit.SelectedItem.ToString
        objRow("qty") = QtySJ.Text
        objRow("Collie") = TextCollie.Text
        objRow("notedtl") = txtLenght.Text

        If I_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If
        objTable.AcceptChanges()
        ClearDetail()

        Session("TblDtl") = objTable
        tbldtl.Visible = True
        tbldtl.DataSource = Nothing
        tbldtl.DataSource = objTable
        tbldtl.DataBind()

        seqglobal.Text = objTable.Rows.Count + 1
        tbldtl.SelectedIndex = -1

        If I_U.Text = "New" Then
            imbSendApproval.Visible = False
        Else
            If posting.Text = "In Approval" Or posting.Text = "Approved" Or posting.Text = "INVOICED" Then
                imbSendApproval.Visible = False
            Else
                imbSendApproval.Visible = False
            End If
        End If
        I_u2.Text = "New Detail"
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
        tbldtl.SelectedIndex = -1
        If posting.Text = "In Process" Then
            btnAddtolist.Visible = True
        Else
            btnAddtolist.Visible = False
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""
        If Pengirim.Text.Length > 500 Then
            errmsg = "Kolom Pengirim Tidak Boleh > 500 karakter <br/>"
        End If

        If Tujaun.Text.Length > 500 Then
            errmsg = "Kolom Alamat Tidak Boleh > 500 karakter <br/>"
        End If

        If NoteMst.Text.Length > 500 Then
            errmsg = "Kolom Keterngan Tidak Boleh > 500 karakter <br/>"
        End If

        If trnBerlakudate.Text = "" Then
            errmsg = "Tanggal Berlaku Tidak Boleh Kosong <br/>"
        End If

        If Pengirim.Text = "" Then
            errmsg = "Alamat Pengirim Tidak Boleh Kosong <br/>"
        End If

        If Tujaun.Text = "" Then
            errmsg = "Alamat Tujuan Tidak Boleh Kosong <br/>"
        End If

        If DDLPengirim.Text = "" Then
            errmsg = "Nama Pengirim Tidak Boleh Kosong <br/>"
        End If

        If Tujaun.Text = "" Then
            errmsg = "Kota Tidak Boleh Kosong <br/>"
        End If

        If Session("TblDtl") Is Nothing Then
            errmsg = "Detail Item Tidak Boleh Kosong <br/>"
        End If

        If errmsg.Trim <> "" Then
            showMessage(errmsg, CompnyName & " - WARNING", 2)
            posting.Text = "In Process"
            Exit Sub
        End If

        If posting.Text = "POST" Then
            GenerateNumberString()
        End If

        Dim CabangKirim As String = GetStrData("select gencode from ql_mstgen Where gengroup='CABANG' AND genoid=" & DDLPengirim.SelectedValue & "")
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Dim transdateBerlaku As Date = Date.ParseExact(trnBerlakudate.Text, "dd/MM/yyyy", Nothing)
            Dim transdateSj As Date = Date.ParseExact(SJdate.Text, "dd/MM/yyyy", Nothing)
            'If posting.Text = "In Process" Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If I_U.Text = "New" Then
                    oid.Text = GenerateID("ql_trnsjglobalmst", CompnyCode)
                    sSql = "INSERT INTO QL_trnSjGlobalmst (cmpcode, sjglobalmstoid, sjglobalno, branch_code, tglberlaku, tglSj, formAddres, toAdderes, statusSjglobal, note, noplis, fromCity, personoid, createuser, upduser, updtime, createtime) VALUES ('" & CompnyCode & "', '" & oid.Text & "', '" & trnsjjualno.Text & "', '" & CabangKirim & "', '" & transdateBerlaku & "', '" & Tchar(transdateSj) & "', '" & Tchar(Pengirim.Text) & "', '" & Tchar(Tujaun.Text) & "', '" & posting.Text & "', '" & Tchar(NoteMst.Text) & "', '" & Tchar(nopolisi.Text) & "', '" & DDLKota.SelectedValue & "','" & DDLPengirim.SelectedValue & "', '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    Dim objTable As DataTable = Session("TblDtl")
                    For i As Integer = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnSjGlobaldtl (cmpcode, sjglobalmstoid, branch_code, item, qty, unit, notedtl, seqglobal, collie) VALUES ('" & CompnyCode & "', " & oid.Text & ", '" & CabangKirim & "', '" & Tchar(objTable.Rows(i).Item("item")) & "', '" & objTable.Rows(i).Item("qty") & "', '" & objTable.Rows(i).Item("unit") & "', '" & Tchar(objTable.Rows(i).Item("notedtl")) & "', '" & objTable.Rows(i).Item("seqglobal") & "', '" & objTable.Rows(i).Item("collie") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "update QL_mstoid set lastoid=" & oid.Text & " where tablename ='ql_trnsjglobalmst' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

            Else
                sSql = "UPDATE  QL_trnSjGlobalmst set cmpcode = '" & CompnyCode & "', sjglobalno = '" & trnsjjualno.Text & "', tglberlaku = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), tglSj = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), formAddres = '" & Tchar(Pengirim.Text) & "', toAdderes =  '" & Tchar(Tujaun.Text) & "', statusSjglobal = '" & posting.Text & "', note = '" & Tchar(NoteMst.Text) & "', noplis  = '" & Tchar(nopolisi.Text) & "', fromCity = '" & DDLKota.SelectedValue & "', personoid = '" & DDLPengirim.SelectedValue & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "' WHERE sjglobalmstoid = '" & Session("oid") & "' AND branch_code='" & CabangKirim & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "delete QL_trnSjGlobaldtl Where sjglobalmstoid = '" & oid.Text & "' and branch_code='" & CabangKirim & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim objTable As DataTable = Session("TblDtl")
                For i As Integer = 0 To objTable.Rows.Count - 1
                    sSql = "INSERT INTO QL_trnSjGlobaldtl (cmpcode, sjglobalmstoid, branch_code, item, qty, unit, notedtl, seqglobal, collie) VALUES ('" & CompnyCode & "', " & Session("oid") & ", '" & CabangKirim & "', '" & Tchar(objTable.Rows(i).Item("item")) & "', '" & objTable.Rows(i).Item("qty") & "', '" & objTable.Rows(i).Item("unit") & "', '" & Tchar(objTable.Rows(i).Item("notedtl")) & "', '" & objTable.Rows(i).Item("seqglobal") & "', '" & objTable.Rows(i).Item("collie") & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If
            objTrans.Commit() : conn.Close()
            Response.Redirect("trnSJ.aspx?awal=true")
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - WARNING", 2)
            posting.Text = "In Process"
            Exit Sub
        End Try
    End Sub

    Protected Sub btnfind_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnfind.Click
        Dim periodsj1 As Date = CDate(toDate(FilterPeriod1.Text))
        Dim periodsj2 As Date = CDate(toDate(FilterPeriod2.Text))
        If periodsj1 > periodsj2 Then
            showMessage("Period awal > period akhir", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        BindData("")
    End Sub

    Protected Sub tbldtl_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles tbldtl.RowDeleting

        Dim iIndex As Int16 = e.RowIndex
        Dim objTable As DataTable = Session("TblDtl")
        objTable.Rows.RemoveAt(iIndex)
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seqglobal") = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        tbldtl.Visible = True

        tbldtl.DataSource = objTable
        tbldtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub tbldtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles tbldtl.SelectedIndexChanged
        I_u2.Text = "edit"
        seqglobal.Text = tbldtl.Rows(tbldtl.SelectedIndex).Cells(1).Text
        itemname.Text = Replace(tbldtl.Rows(tbldtl.SelectedIndex).Cells(2).Text, "&nbsp;", "")
        QtySJ.Text = tbldtl.Rows(tbldtl.SelectedIndex).Cells(3).Text
        DDLUnit.SelectedValue = tbldtl.Rows(tbldtl.SelectedIndex).Cells(4).Text
        TextCollie.Text = Replace(tbldtl.Rows(tbldtl.SelectedIndex).Cells(5).Text, "&nbsp;", "")
        txtLenght.Text = Replace(tbldtl.Rows(tbldtl.SelectedIndex).Cells(6).Text, "&nbsp;", "")

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
        tbldtl.DataSource = Nothing
        tbldtl.DataBind()
        Response.Redirect("~\Transaction\trnSJ.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnPosting.Click
        posting.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")

            Dim cekStatus As String = (GetStrData("SELECT statussjglobal FROM QL_trnSjGlobalmst WHERE sjglobalmstoid = '" & Integer.Parse(labeloid.Text) & "'"))
            If cekStatus <> "POST" Then
                showMessage("Data ini masih dalam status In Process, untuk print silahkan POSTING terlebih dahulu data ini!", CompnyName & " - WARNING", 2) : Exit Sub
            End If


            Dim sWhere As String = " WHERE m.sjglobalmstoid = '" & Integer.Parse(labeloid.Text) & "'"
            sSql = "SELECT sjglobalno FROM QL_trnSjGlobalmst WHERE sjglobalmstoid = '" & Integer.Parse(labeloid.Text) & "'"
            Dim nojs As String = cKoneksi.ambilscalar(sSql)

            reportSJ = New ReportDocument
            reportSJ.Load(Server.MapPath("~\Report\rptSjGlobal.rpt"))
            cProc.SetDBLogonForReport(reportSJ, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            reportSJ.SetParameterValue("sWhere", sWhere)
            reportSJ.SetParameterValue("printSJ", "SURAT JALAN PENGIRIMAN BARANG ")
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            reportSJ.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SJ_" & nojs)
            reportSJ.Close() : reportSJ.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Error", 1)
        End Try
    End Sub

    Protected Sub btnPrintSJ_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnPrintSJ.Click
        Try

            Dim sWhere As String = " WHERE m.sjglobalmstoid = '" & oid.Text & "'"

            sSql = "SELECT sjglobalno FROM QL_trnSjGlobalmst WHERE sjglobalmstoid = '" & oid.Text & "'"
            Dim nojs As String = cKoneksi.ambilscalar(sSql)

            reportSJ = New ReportDocument
            reportSJ.Load(Server.MapPath("~\Report\rptSjGlobal.rpt"))
            cProc.SetDBLogonForReport(reportSJ, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            reportSJ.SetParameterValue("sWhere", sWhere)
            reportSJ.SetParameterValue("printSJ", "SURAT JALAN PENGIRIMAN BARANG ")
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            reportSJ.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SJ_" & nojs)
            reportSJ.Close() : reportSJ.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Error", 1)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnDelete.Click
        Dim CabangKirim As String = GetStrData("select gencode from ql_mstgen Where gengroup='CABANG' AND genoid=" & DDLPengirim.SelectedValue & "")
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            sSql = "delete QL_trnSjGlobaldtl  where sjglobalmstoid = '" & oid.Text & "' and branch_code='" & CabangKirim & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete QL_trnSjGlobalmst  where sjglobalmstoid = '" & oid.Text & "' and branch_code='" & CabangKirim & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING", 2)
            posting.Text = "In Process"
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnSJ.aspx?awal=true")
    End Sub

    Protected Sub gvTblData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTblData.PageIndexChanging
        gvTblData.PageIndex = e.NewPageIndex
        BindData("")
    End Sub

    Protected Sub gvTblData_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvTblData.RowDataBound

    End Sub 

    Protected Sub btnviewall_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnviewall.Click
        txtFindSJ.Text = ""
        ddlFilter.SelectedIndex = 0
        FilterPeriod1.Text = Format(Now, "01/MM/yyyy")
        FilterPeriod2.Text = Format(Now, "dd/MM/yyyy")

        If Session("branch_id") = "10" Then
            ddlcabang1.SelectedValue = "ALL"
        End If

        Dim periodsj1 As String = Format(Now, "MM/01/yyyy")
        Dim periodsj2 As String = Format(Now, "MM/dd/yyyy")
        Dim periodsj As String
        If periodsj1 > periodsj2 Then
            showMessage("Period awal tidak boleh lebih besar period akhir", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        periodsj = " and tglsj BETWEEN '" & periodsj1 & "' and '" & periodsj2 & "'"
        BindData("")
    End Sub
#End Region 

    Protected Sub gvTblData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTblData.SelectedIndexChanged
        Response.Redirect("~\Transaction\trnSJ.aspx?oid=" & gvTblData.SelectedDataKey("sjglobalmstoid").ToString & "&branch_code=" & gvTblData.SelectedDataKey("branch_code").ToString & "")
    End Sub
End Class
