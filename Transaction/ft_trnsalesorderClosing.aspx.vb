Imports System.Data
Imports System.Data.SqlClient
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure

Partial Class SO_Close
    Inherits System.Web.UI.Page
    Dim sSql As String = ""
    Dim CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cmd As New SqlCommand("", conn)
    Dim cKon As New Koneksi
    Dim cFungsi As New ClassFunction
    Dim cProc As New ClassProcedure

    Private Sub showMessage(ByVal isi As String, ByVal judul As String)
        isiPesan.Text = isi
        captionPesan.Text = judul 
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, True)
    End Sub

    Private Sub CabangInit()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(cBangFilter, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(cBangFilter, sSql)
            Else
                FillDDL(fCabang, sSql)
                cBangFilter.Items.Add(New ListItem("ALL", "ALL"))
                cBangFilter.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(cBangFilter, sSql)
            cBangFilter.Items.Add(New ListItem("ALL", "ALL"))
            cBangFilter.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub fCabangInit()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCabang, sSql)
            Else
                FillDDL(fCabang, sSql)
                fCabang.Items.Add(New ListItem("ALL", "ALL"))
                fCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fCabang, sSql)
            fCabang.Items.Add(New ListItem("ALL", "ALL"))
            fCabang.SelectedValue = "ALL"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        ' '' jangan lupa cek hak akses
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSDO")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSDO") = sqlSearch
            Response.Redirect("~/Transaction/ft_trnsalesorderClosing.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Sales Order Closing"

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            CabangInit() : fCabangInit()
            tglAwal.Text = Format(GetServerTime(), "dd/MM/yyyy")
            tglAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")

            tglAwal2.Text = Format(GetServerTime(), "01/MM/yyyy")
            tglAkhir2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            ddlFilter.SelectedIndex = 1 : TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Private Sub bindData(ByVal sFilter As String, ByVal gvTarget As GridView)
        Try
            sSql = "select m.ordermstoid orderoid,m.orderno,m.trncustoid custoid,c.custname,m.delivdate deliverydate, m.canceltime orderdateclosed, m.canceluser orderuserclosed,m.cancelnote ordernoteclosed, m.trnorderstatus orderstatus,m.periodacctg,m.branch_code from ql_trnordermst m inner join ql_trnorderdtl d on m.ordermstoid=d.trnordermstoid AND m.branch_code=d.branch_code inner join ql_mstcust c on m.trncustoid=c.custoid Where UPPER(m.cmpcode)='" & CompnyCode & "' " & sFilter & ""

            If fCabang.SelectedValue <> "ALL" Then
                sSql &= "and m.branch_code='" & fCabang.SelectedValue & "'"
            End If

            sSql &= " group by m.ordermstoid,m.orderno,m.trncustoid,c.custname,m.delivdate, m.canceltime, m.canceluser, m.cancelnote, m.trnorderstatus ,m.periodacctg,m.branch_code order by m.periodacctg desc,right(m.orderno,4) desc"
            FillGV(gvTarget, sSql, "ql_trnordermst")
            gvTarget.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br /> " & sSql, CompnyName & " - WARNING")
            Exit Sub
        End Try
    End Sub

    Private Sub bindDataSO()

        sSql = "SELECT so.ordermstoid orderoid, so.orderno,so.trnorderdate,so.trncustoid custoid,s.custname,so.delivdate deliverydate from ql_trnordermst so, ql_mstcust s where so.trncustoid=s.custoid and  so.cmpcode=s.cmpcode and so.trnorderstatus='Approved' and so.trnordertype='PROJECK' and so.orderno like '%%' and so.ordermstoid in (select trnordermstoid from QL_trnorderdtl where(trnorderdtlstatus in ('','In Process')) OR trnorderdtlstatus NOT IN ('Completed')) ORDER BY so.trnorderdate DESC,so.orderno DESC "

        FillGV(gvBPM2, sSql, "ql_trnordermst")
        gvBPM2.Visible = True
    End Sub

    Private Sub bindItem(ByVal sCabang As String)
        sSql = "select a.itemoid,item.itemcode ,item.itemdesc,(trnorderdtlqty-orderdelivqty) qty, b.ordermstoid , (select gendesc from QL_mstgen where gengroup='itemunit' and genoid = a.trnorderdtlunitoid) unit from QL_trnorderdtl a inner join QL_trnordermst b on a.trnordermstoid = b.ordermstoid AND a.branch_code=b.branch_code inner join QL_MSTITEM item on item.itemoid = a.itemoid Where b.orderno='" & txtorderNo.Text & "' And a.branch_code='" & sCabang & "' and (trnorderdtlqty-orderdelivqty)>0 and b.trnorderstatus='Approved' And trnorderdtlstatus in ('','In Process') and trnorderdtlstatus NOT IN ('Completed')"
        FillGV(gv_item, sSql, "ql_trnordermstdtl")
        gv_item.Visible = True : Label6.Visible = True
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'btnExtenderValidasi.Visible = False
        'PanelValidasi.Visible = False
        'mpeMsg.Hide()
        Dim x As Integer = 123
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, False)
        'isiPesan.Text = "tes"
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlFilter.SelectedValue <> "spmno" Then
            tglAwal.Enabled = True
            tglAwal.CssClass = "inpText"
            tglAkhir.Enabled = True
            tglAkhir.CssClass = "inpText"
        Else
            tglAwal.Enabled = False
            tglAwal.CssClass = "inpTextDisabled"
            tglAkhir.Enabled = False
            tglAkhir.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub imbSearchBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchBPM.Click 
        sSql = " and m.orderno like '%" & Tchar(txtorderNo.Text.Trim) & "%' And m.trnorderstatus in ('Approved')"
        bindData(sSql, gvBPM2)
    End Sub

    Protected Sub imbEraseBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseBPM.Click
        txtorderNo.Text = "" : ordermstoid.Text = ""
        custname.Text = "" : netto.Text = ""
        gvBPM2.Visible = False
        gv_item.Visible = False
        gv_item.SelectedIndex = -1
        gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBPM2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            'e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
        End If
    End Sub

    Protected Sub gvBPM2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBPM2.SelectedIndexChanged
        ordermstoid.Text = gvBPM2.SelectedDataKey.Item("orderoid")
        txtorderNo.Text = gvBPM2.SelectedDataKey.Item("orderno")
        custoid.Text = gvBPM2.SelectedDataKey.Item("custoid")
        custname.Text = gvBPM2.SelectedDataKey.Item("custname")
        fCabangInit()
        fCabang.SelectedValue = gvBPM2.SelectedDataKey.Item("branch_code")
        netto.Text = ToMaskEdit(GetStrData("select (sum((trnorderdtlqty-isnull(orderdelivqty,0))*(trnorderprice-((trnamountbruto-trnamountdtl)/trnorderdtlqty)))) + ((sum((trnorderdtlqty-isnull(orderdelivqty,0))*(trnorderprice-((trnamountbruto-trnamountdtl)/trnorderdtlqty))))/100) from ql_trnorderdtl d inner join ql_trnordermst m on d.trnordermstoid=m.ordermstoid AND d.branch_code=m.branch_code Where m.ordermstoid=" & ordermstoid.Text & " group by trntaxpct"), 3)
        bindItem(gvBPM2.SelectedDataKey.Item("branch_code"))
        gvBPM2.Visible = False
        gvBPM2.SelectedIndex = -1
        fCabang.Enabled = False
    End Sub

    Protected Sub gvBPM2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBPM2.PageIndexChanging
        gvBPM2.PageIndex = e.NewPageIndex

        Dim sQuery As String = ""
        sQuery &= " AND m.orderno like '%" & Tchar(txtorderNo.Text) & "%' "
        sQuery &= " AND m.trnorderstatus='Approved' "
        bindData(sQuery, gvBPM2)

    End Sub

    Protected Sub btnCloseBPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseBPM.Click
        Dim sErr As String = ""
        If ordermstoid.Text.Trim = "" Or txtorderNo.Text = "" Then
            sErr &= "Silahkan pilih SO !<br />"
            Exit Sub
        End If

        If txtorderNote.Text.Trim = "" Then
            sErr &= "Silahkan Isi Note Closed dahulu !<br />" 
        End If

        sSql = "select count(*) from ql_trnsjjualmst where trnsjjualstatus in ('In Approval') and orderno = '" & txtorderNo.Text & "'" : Dim delivprocess As Decimal = ToDouble(GetStrData(sSql))

        If delivprocess > 0 Then
            sSql = "select trnsjjualno from ql_trnsjjualmst where trnsjjualstatus in ('In Process','In Approval') and orderno = '" & txtorderNo.Text & "'"
            Dim NO_DO As String = GetStrData(sSql)
            sErr &= "SO No " & txtorderNo.Text & " Sudah dibuat SDO/SDO dalam status ('In Process','In Approval')..!!<br/> Silahkan process terlebih dahulu SDO/SDO Project No " & NO_DO & " !<br />" 
        End If

        If sErr <> "" Then
            showMessage(sErr, CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        cmd.Transaction = objTrans

        Try
            sSql = "update QL_trnordermst set trnorderstatus='Closed Manual', canceltime=CURRENT_TIMESTAMP, canceluser='" & Session("UserID") & "', cancelnote='" & Tchar(txtorderNote.Text) & "' Where ordermstoid=" & ordermstoid.Text & " AND branch_code='" & fCabang.SelectedValue & "'"
            cmd.CommandText = sSql : cmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            cmd.Connection.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR")
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\ft_trnsalesorderClosing.aspx?awal=true")
    End Sub

    Protected Sub gvClosedBPM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvClosedBPM.PageIndex = e.NewPageIndex
        'btnFind_Click(sender, e)
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Redirect("~\Transaction\ft_trnsalesorderClosing.aspx")
    End Sub

    Protected Sub gvClosedBPM_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvClosedBPM.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy HH:mm:ss")
        End If
    End Sub

    Protected Sub txtorderNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtorderNo.TextChanged
        'bindItem()
    End Sub

    Protected Sub ImgFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgFind.Click
        Try
            Dim dat1 As Date = CDate(toDate(tglAwal.Text))
            Dim dat2 As Date = CDate(toDate(tglAkhir.Text))
        Catch ex As Exception
            showMessage("Invalid filter date!!", CompnyName & " - WARNING") : Exit Sub
        End Try

        If CDate(toDate(tglAwal.Text)) > CDate(toDate(tglAkhir.Text)) Then
            showMessage("Start date must be less or equal than End date!!", CompnyName & " - WARNING")
            Exit Sub
        End If

        If CbPeriode.Checked = True Then
            sSql &= "AND convert(char(10),m.canceltime,121) between '" & Format(CDate(toDate(tglAwal.Text)), "yyyy-MM-dd") & "' AND '" & Format(CDate(toDate(tglAkhir.Text)), "yyyy-MM-dd") & "'"
        End If

        If cBangFilter.SelectedValue <> "ALL" Then
            sSql &= "AND m.branch_code='" & cBangFilter.SelectedValue & "'"
        End If

        sSql &= " AND m.orderno like '%" & Tchar(txtFilter.Text) & "%' AND m.trnorderstatus='Closed Manual'"
        bindData(sSql, gvClosedBPM)
    End Sub
 
    Protected Sub ImgAllFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAllFind.Click
        tglAwal.Text = Format(GetServerTime().AddDays(-1), "dd/MM/yyyy")
        tglAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        txtFilter.Text = ""
        Dim sQuery As String = ""
        sQuery &= " AND m.trnorderstatus='Closed Manual'"
        bindData(sQuery, gvClosedBPM)
    End Sub
End Class
