<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="MstPriceCabang.aspx.vb" Inherits="MstPriceCabang" title="" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" colspan="2" valign="middle">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Navy"
                    Text=".: Master Price Cabang" Font-Size="X-Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Price Cabang :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlList" runat="server" Width="100%" __designer:wfdid="w120" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 10%" align=left>Cabang</TD><TD style="WIDTH: 2%" align=center>:</TD><TD align=left colSpan=2><asp:DropDownList id="DDLCabang" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w121"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 10%" align=left><asp:Label id="Label4x" runat="server" Text="Filter" __designer:wfdid="w122"></asp:Label></TD><TD style="WIDTH: 2%" align=center>:</TD><TD align=left colSpan=2><asp:DropDownList id="FilterDDLMst" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w123"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextMst" runat="server" Width="200px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w124"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 10%" align=left>Jenis</TD><TD style="WIDTH: 2%" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="JenisDDL" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w125"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w126"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w127"></asp:ImageButton> <asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w128"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4><asp:GridView id="gvList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w129" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" AllowPaging="True" AllowSorting="True" DataKeyNames="itemcode">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Kode"><ItemTemplate>
<asp:LinkButton id="lkbSelect" onclick="lkbSelect_Click" runat="server" Text='<%# Eval("itemcode") %>' __designer:wfdid="w31" ToolTip="<%# GetTransID() %>" CommandName='<%# Eval("branch_code") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricenormal" HeaderText="Normal" SortExpression="pricenormal">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceminim" HeaderText="Minim" SortExpression="pricenota">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricekhusus" HeaderText="Khusus" SortExpression="adjcount">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricesilver" HeaderText="Silver">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricegold" HeaderText="Gold">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceplatinum" HeaderText="Platinum">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="No data found."></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=4><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w130"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="imgForm" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Price Cabang :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 67px" align=left>Cabang</TD><TD align=center>:</TD><TD align=left colSpan=4><asp:DropDownList id="ddlBranch" runat="server" CssClass="inpText" Font-Size="8pt" AutoPostBack="True" __designer:wfdid="w74"></asp:DropDownList>&nbsp;<asp:Button id="BtnAddItem" runat="server" CssClass="green" Font-Bold="True" Text="Add Katalog" __designer:wfdid="w75"></asp:Button></TD></TR><TR><TD align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 300px; BACKGROUND-COLOR: beige"><asp:GridView id="gvFindCrd" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w76" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="createdate" HeaderText="Create Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="createtime" HeaderText="Create Time">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode ">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="hpp" HeaderText="HPP">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceexpedisi" HeaderText="P. Expedisi">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceminim" HeaderText="Price Minim">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricenormal" HeaderText="P. Normal">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricekhusus" HeaderText="P. Khusus">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricenota" HeaderText="P. Nota">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricegold" HeaderText="P. Gold">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceplatinum" HeaderText="P. Platinum">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricesilver" HeaderText="P. Silver">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="addcost" HeaderText="Add cost">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="keterangan" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="active_flag" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="COLOR: #585858" class="\" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w77"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w78"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" align=left colSpan=6>Last Updated By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w79">-</asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w80">-</asp:Label></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="imbSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w81"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w82"></asp:ImageButton> <asp:ImageButton id="imbPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w83" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w84" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w85" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w86"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></DIV>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbSave"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
                            <asp:UpdatePanel id="upListMat" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" __designer:wfdid="w88" Visible="False"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Katalog" __designer:wfdid="w89"></asp:Label></TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" __designer:wfdid="w90" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblFilterInListMat" runat="server" Text="Filter :" __designer:wfdid="w91"></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w92"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="merk">Merk</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w93"></asp:TextBox></TD></TR><TR><TD align=center colSpan=3>Jenis : <asp:DropDownList id="ddlType" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w94"></asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w95"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w96"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w97"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w98"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w99"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 875px; HEIGHT: 300px; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w100" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbSelectMat" runat="server" __designer:wfdid="w10" Checked='<%# eval("CheckValue") %>' ToolTip='<%# eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="HPP" HeaderText="HPP">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Price Expedisi"><ItemTemplate>
<asp:TextBox id="pExpedisi" runat="server" Width="98px" CssClass="inpText" Text='<%# eval("priceexpedisi") %>' __designer:wfdid="w11" OnTextChanged="pExpedisi_TextChanged" MaxLength="18"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTBpExpedisi" runat="server" __designer:wfdid="w12" TargetControlID="pExpedisi" ValidChars="-1234567890."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price Normal"><ItemTemplate>
<asp:TextBox id="pNormal" runat="server" Width="98px" CssClass="inpText" Text='<%# eval("pricenormal") %>' __designer:wfdid="w35" MaxLength="18"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="FTBpNormal" runat="server" __designer:wfdid="w36" TargetControlID="pNormal" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price Minim"><ItemTemplate>
<asp:TextBox id="tbMinim" runat="server" Width="90px" CssClass="inpText" Text='<%# eval("PriceMinim") %>' __designer:wfdid="w37" MaxLength="16"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbMinim" runat="server" __designer:wfdid="w38" TargetControlID="tbMinim" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price Khusus"><ItemTemplate>
<asp:TextBox id="tbKhusus" runat="server" Width="75px" CssClass="inpText" Text='<%# eval("pricekhusus") %>' MaxLength="16" __designer:wfdid="w6"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbKhusus" runat="server" ValidChars="1234567890.,-" TargetControlID="tbKhusus" __designer:wfdid="w7"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price Silver"><ItemTemplate>
<asp:TextBox id="tbSilver" runat="server" Width="75px" CssClass="inpText" Text='<%# eval("pricesilver") %>' MaxLength="16" __designer:wfdid="w8"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbSilver" runat="server" ValidChars="1234567890.,-" TargetControlID="tbSilver" __designer:wfdid="w9"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price Gold"><ItemTemplate>
<asp:TextBox id="tbGold" runat="server" Width="75px" CssClass="inpText" Text='<%# eval("pricegold") %>' MaxLength="16" __designer:wfdid="w10"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbtbGold" runat="server" ValidChars="1234567890.,-" TargetControlID="tbGold" __designer:wfdid="w11"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Platinum"><ItemTemplate>
<asp:TextBox id="tbPlatinum" runat="server" Width="75px" CssClass="inpText" Text='<%# eval("priceplatinum") %>' __designer:wfdid="w2" MaxLength="16"></asp:TextBox>
<ajaxToolkit:FilteredTextBoxExtender id="ftbPlatinum" runat="server" __designer:wfdid="w3" TargetControlID="tbPlatinum" ValidChars="1234567890.,-"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Add Cost"><ItemTemplate>
<asp:TextBox id="tbAddCost" runat="server" Width="75px" CssClass="inpText" Text='<%# eval("addcost") %>' __designer:wfdid="w2" MaxLength="16"></asp:TextBox>
<ajaxToolkit:FilteredTextBoxExtender id="FTEaddcost" runat="server" TargetControlID="tbAddCost" ValidChars="-1234567890.,-"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Keterangan"><ItemTemplate>
<asp:TextBox ID="tbMatNote" runat="server" CssClass="inpText" MaxLength="100" Text='<%# eval("keterangan") %>' Width="150px"></asp:TextBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Status"><ItemTemplate>
<asp:DropDownList id="active_flag" runat="server" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w7" ToolTip='<%# Eval("active_flag") %>' Enabled="False"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>IN ACTIVE</asp:ListItem>
</asp:DropDownList> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddListMat" runat="server" Font-Bold="True" __designer:wfdid="w101">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListMat" runat="server" Font-Bold="True" __designer:wfdid="w102">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" __designer:wfdid="w103" TargetControlID="btnHideListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" __designer:wfdid="w104" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
</ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

