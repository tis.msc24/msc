'Prgmr : Shutup_M ; Update : zipi on 28.02.16
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class TrnForCastBeli
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim dtStaticItem As DataTable
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Private cKon As New Koneksi
    Private cProc As New ClassProcedure
    Dim crate As ClassRate
#End Region

#Region "Function"
    Public Sub showPrintExcel(ByVal name As String, ByVal oid As Integer)
        Session("diprint") = "False" : Response.Clear()
        Dim swhere As String = "", shaving As String = "", swhere2 As String = ""
        sSql = "select m.trnbelipono 'PO No',s.suppname 'Supplier',m.TRNBELIdeliverydate 'PO Delivery Date',m.trnbelipodate 'PO Date',m.trnbelistatus 'PO Status',g.gendesc as Paytype,c.currencycode 'Curr',m.currate 'Rate',mt.ITEMDESC 'ITEM',d.trnbelidtlnote 'Note Detail',g2.gendesc 'Unit',d.trnbelidtlqty 'Qty',d.trnbelidtlprice 'Price',d.amtdisc + amtdisc2 'Disc Detail',d.amtbelinetto 'Detail Amt',d.upduser,d.updtime from ql_podtl d inner join ql_pomst m on m.trnbelimstoid=d.trnbelimstoid inner join ql_mstsupp s on s.suppoid=m.trnsuppoid inner join ql_mstgen g on g.genoid=m.trnpaytype inner join ql_mstcurr c on c.currencyoid=m.curroid inner join QL_mstgen g2 ON d.trnbelidtlunit=g2.genoid inner join QL_mstITEM mt ON d.ITEMoid=mt.ITEMoid where  d.cmpcode='" & CompnyCode & "' and m.trnbelimstoid = " & oid
        Response.AddHeader("content-disposition", "inline;filename= Status_Order_Pembelian.xls")
        Response.Charset = "" : Response.ContentType = "application/vnd.ms-excel"
        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close() : Response.End()
    End Sub

    Public Function isiDS(ByVal squery As String, ByVal snamatabel As String) As DataTable
        Dim ssqlcust As String
        ssqlcust = squery
        Dim mySqlDA As New SqlDataAdapter(ssqlcust, ConnStr)
        Dim objDsRef As New DataSet
        mySqlDA.Fill(objDsRef, snamatabel)
        Return objDsRef.Tables(snamatabel)
    End Function

    Private Function getMonthname(ByVal dmonth As Integer) As String
        Dim res As String = ""
        If dmonth = 1 Then
            res = "Januari"
        ElseIf dmonth = 2 Then
            res = "Februari"
        ElseIf dmonth = 3 Then
            res = "Maret"
        ElseIf dmonth = 4 Then
            res = "April"
        ElseIf dmonth = 5 Then
            res = "Mei"
        ElseIf dmonth = 6 Then
            res = "Juni"
        ElseIf dmonth = 7 Then
            res = "Juli"
        ElseIf dmonth = 8 Then
            res = "Agustus"
        ElseIf dmonth = 9 Then
            res = "September"
        ElseIf dmonth = 10 Then
            res = "Oktober"
        ElseIf dmonth = 11 Then
            res = "November"
        ElseIf dmonth = 12 Then
            res = "Desember"
        End If
        Return res
    End Function

    Private Function checkApprovaldata(ByVal sTableName As String, ByVal sEvent As String, ByVal sOid As String, _
     ByVal sRequestUser As String, ByVal sStatus As String) As DataTable
        Dim ssql As String = "SELECT CMPCODE, APPROVALOID, REQUESTCODE, REQUESTUSER, REQUESTDATE, STATUSREQUEST, TABLENAME, OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval WHERE CmpCode='" & CompnyCode & "' AND RequestUser LIKE '" & Tchar(sRequestUser) & "%' and STATUSREQUEST = '" & sStatus & "' AND TABLENAME='" & sTableName & "' AND OID=" & sOid & " and EVENT='" & sEvent & "' ORDER BY REQUESTDATE DESC"
        Return cKon.ambiltabel(ssql, "QL_APPROVAL")
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function insertData(ByVal dtTemp As DataTable) As DataTable
        If Session("TblDtl") Is Nothing Then
            Dim dtlDS As DataSet = New DataSet
            Dim dtlTable As DataTable = New DataTable("TabelpodtlDetail")
            dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
            dtlTable.Columns.Add("podtloid", Type.GetType("System.String"))
            dtlTable.Columns.Add("pomstoid", Type.GetType("System.String"))
            dtlTable.Columns.Add("podtlseq", Type.GetType("System.String"))
            dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("podtlqty", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("ekspedisi", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtlunit", Type.GetType("System.String"))
            dtlTable.Columns.Add("podtlprice", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("Total", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("DiscAmt", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtldisc", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtldisctype", Type.GetType("System.String"))
            dtlTable.Columns.Add("DiscAmt2", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtldisc2", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("podtldisctype2", Type.GetType("System.String"))
            dtlTable.Columns.Add("totDiscAmt", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("unitseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("pobelidtlnote", Type.GetType("System.String"))
            dtlTable.Columns.Add("pobelidtlstatus", Type.GetType("System.String"))
            dtlTable.Columns.Add("updtime", Type.GetType("System.String"))
            dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
            dtlTable.Columns.Add("poamtdtldisc", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("poamtdtlnetto", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("material", Type.GetType("System.String"))
            dtlTable.Columns.Add("merk", Type.GetType("System.String"))
            dtlTable.Columns.Add("Unit", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("Subcat", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("matcategoryoid", Type.GetType("System.Int32"))
            dtlDS.Tables.Add(dtlTable)
            Session("TblDtl") = dtlTable
        End If
        Dim objTable As DataTable
        objTable = Session("TblDtl")
        Dim objRow As DataRow
        Dim ErrorAuto As String = ""

        For C1 As Integer = 0 To dtTemp.Rows.Count - 1
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "matoid=" & dtTemp.Rows(C1).Item("itemoid").ToString.Trim
            If dv.Count > 0 Then
                dv.RowFilter = ""
                ErrorAuto &= "Data " & dtTemp.Rows(C1).Item("ItemLDesc").ToString & " merk " & dtTemp.Rows(C1).Item("merk").ToString & " sudah di tambahkan sebelumnya, Silahkan cek ulang! </br>"
            Else
                objRow = objTable.NewRow()
                objRow("cmpcode") = dtTemp.Rows(C1).Item("cmpcode")
                objRow("podtloid") = 0
                objRow("pomstoid") = 0
                objRow("poamtdtlnetto") = 0
                objRow("podtlseq") = objTable.Rows.Count + 1
                objRow("matoid") = dtTemp.Rows(C1).Item("itemoid").ToString.Trim
                objRow("podtlqty") = ToDouble(dtTemp.Rows(C1).Item("orderqty"))
                objRow("podtlunit") = dtTemp.Rows(C1).Item("unit").ToString
                objRow("ekspedisi") = ToDouble(0)
                objRow("podtlprice") = ToDouble(0)
                objRow("podtldisc") = ToMaskEdit(0, 4)
                objRow("podtldisctype") = "AMT"
                objRow("podtldisc2") = ToDouble(0)
                objRow("podtldisctype2") = "AMT"
                objRow("pobelidtlnote") = ""
                objRow("pobelidtlstatus") = posting.Text
                objRow("upduser") = Session("UserID")
                objRow("updtime") = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                objRow("DiscAmt") = ToDouble(0)
                objRow("DiscAmt2") = ToDouble(0)
                objRow("poamtdtldisc") = ToDouble(0)
                objRow("unitseq") = 3
                objRow("material") = dtTemp.Rows(C1).Item("ItemLDesc").ToString
                objRow("Unit") = dtTemp.Rows(C1).Item("itemunitoid").ToString
                objRow("matcategoryoid") = dtTemp.Rows(C1).Item("Category").ToString
                objRow("merk") = dtTemp.Rows(C1).Item("merk").ToString

                objTable.Rows.Add(objRow)
            End If
            dv.RowFilter = ""

        Next
        Session("TblDtl") = objTable
        Session("ItemLine") = objTable.Rows.Count + 1
        trnbelidtlseq.Text = Session("ItemLine")
        Return Session("TblDtl")
    End Function

#End Region

#Region "Procedure"

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(CabangNya, sSql)
                CabangNya.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
            CabangNya.SelectedValue = "10"
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessageprint(ByVal id As Integer, ByVal no As String, ByVal other As String)
        idTemp.Text = id : NoTemp.Text = no
        OtherTemp.Text = other
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub initUnit()
        Try
            'Dim sSql As String
            sSql = "Select g.genoid,g.gendesc from ql_mstgen g inner join ql_mstITEM m on g.genoid=m.satuan1 Where m.itemoid = '" & matoid.Text & "' and m.ITEMDESC = '" & Tchar(txtMaterial.Text) & "' group by g.genoid,g.gendesc "
            FillDDL(ddlUnit, sSql)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Public Sub InitAllDDL()
        Dim sGroup() As String = {"PAYTYPE"}
        Dim oDDL() As DropDownList = {TRNPAYTYPE}
        FillDDLGen(sGroup, oDDL)

        FillDDL(DDLPrefix, "select genoid, gendesc from ql_mstgen where gengroup='PREFIXSUPP' and cmpcode='" & CompnyCode & "'")

        FillDDL(ddlprinter, "select gencode ,gendesc from ql_mstgen where gengroup like 'SetupPrinter' ")
        ddlprinter.Items.Add(New ListItem("Print To Excel", "EXCEL"))
        ddlprinter.Items.Add(New ListItem("Print To PDF", "PDF"))
        ddlprinter.SelectedValue = "PDF"
        btnCancelMstr.Enabled = True
        If TRNPAYTYPE.Items.Count = 0 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "please create/fill Master Gen in group PAYTYPE!"))
            btnsavemstr.Enabled = False
            btnDeleteMstr.Enabled = False
            btnCancelMstr.Enabled = False
        End If
    End Sub

    Public Sub BindData(ByVal filterCompCode As String)
        Dim sWhere As String = ""
        Dim userID As String = Session("UserID")
        sSql = "Select fc.cmpcode,fc.branch_code,fc.trnforecastoid,fc.trnforecastno,Convert(Varchar(20),fc.trnforecastdate,103) trnforecastdate,sup.suppoid,sup.suppname,trnforecaststatus,fc.trnforecastnote FROM Ql_trnforecastmst fc Inner Join QL_mstsupp sup ON sup.suppoid=fc.suppoid Inner Join QL_mstgen pr ON pr.genoid=fc.prefixoid AND pr.gengroup='PREFIXSUPP' Where (" & ddlFilter.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%')"

        If chkPeriod.Checked = True Then
            sSql &= " AND fc.trnforecastdate BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & "' AND '" & CDate(toDate(FilterPeriod2.Text)) & "'"
        End If

        If chkStatus.Checked Then
            If DDLStatus.SelectedIndex <> 0 Then
                sSql &= " AND trnforecaststatus = '" & DDLStatus.SelectedValue & "' "
            End If
        End If

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND ,fc.upduser='" & Session("UserID") & "'"
        End If

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND fc.branch_code='" & dCabangNya.SelectedValue & "'"
        End If
        sSql &= " Order By fc.trnforecastoid DESC"
        FillGV(tbldata, sSql, "Ql_trnforecastmst")
    End Sub

    Private Sub FillTextBox(ByVal OidForeCast As Int64)
        sSql = "Select fc.cmpcode,fc.branch_code,fc.trnforecastoid,fc.trnforecastno,Convert(Varchar(20),fc.trnforecastdate,103) trnforecastdate,sup.suppoid,sup.suppname,trnforecaststatus,fc.upduser,amountnett,fc.prefixoid,fc.trnpaytypeoid,fc.periodacctg,fc.trnforecastnote,fc.updtime FROM Ql_trnforecastmst fc Inner Join QL_mstsupp sup ON sup.suppoid=fc.suppoid Inner Join QL_mstgen pr ON pr.genoid=fc.prefixoid AND pr.gengroup='PREFIXSUPP' Inner Join QL_mstgen py ON py.genoid=fc.trnpaytypeoid And py.gengroup='PAYTYPE' Where fc.trnforecastoid=" & OidForeCast & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                InitDDLBranch()
                CabangNya.SelectedValue = xreader.Item("branch_code").ToString
                ForeCastoid.Text = Integer.Parse(xreader.Item("trnforecastoid"))
                trnbelipodate.Text = xreader("trnforecastdate").ToString
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                trnbelipono.Text = Trim(xreader.Item("trnforecastno"))
                trnsuppoid.Text = Integer.Parse(xreader.Item("suppoid"))
                TRNPAYTYPE.Text = Integer.Parse(xreader.Item("trnpaytypeoid"))
                trnbelinote.Text = Trim(xreader.Item("trnforecastnote").ToString)
                posting.Text = xreader.Item("trnforecaststatus").ToString
                Upduser.Text = Trim(xreader.Item("upduser").ToString)
                Updtime.Text = Trim(xreader.Item("updtime"))
                amtbelinetto1.Text = ToMaskEdit(Trim(xreader.Item(("amountnett"))), 4)
                suppliername.Text = Trim(xreader.Item("suppname"))
                DDLPrefix.SelectedValue = xreader.Item(("PrefixOid"))
            End While
        End If
        xreader.Close()
        conn.Close()

        sSql = "Select fcd.cmpcode,fcd.trnforecastdtloid,fcd.seq,i.itemoid,i.itemdesc,i.Merk,fcd.itemqty,fcd.itemprice,fcd.amtdtlnetto,fcd.unitoid,un.gendesc ItemUnit,fcd.trnforecaststatusdtl,fcd.notedtl,fcd.cmpcode,fcd.trnforecastoid from ql_trnforecastdtl fcd Inner Join ql_mstitem i ON i.itemoid=fcd.itemoid Inner Join QL_mstgen un ON un.genoid=fcd.unitoid AND un.gengroup='ITEMUNIT' Where fcd.trnforecastoid=" & OidForeCast & ""
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "ql_trnforecastdtl")
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
        Session("TblDtl") = objTable 
        fillTotalOrder()
        trnbelidtlseq.Text = objTable.Rows.Count + 1

        If posting.Text = "Revisi" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False
            imbApproval.Visible = False : btnAddToList.Visible = False
            btnPrint.Visible = False : BtnPosting.Visible = False
        ElseIf posting.Text = "Approved" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False
            imbApproval.Visible = False : btnAddToList.Visible = False
            btnPrint.Visible = False : BtnPosting.Visible = False
        ElseIf posting.Text = "In Approval" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False
            imbApproval.Visible = False : btnAddToList.Visible = False
            btnPrint.Visible = False : BtnPosting.Visible = False
        ElseIf posting.Text = "Closed" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False
            imbApproval.Visible = False : btnAddToList.Visible = True
            btnPrint.Visible = False : BtnPosting.Visible = False 
        ElseIf posting.Text = "Rejected" Then
            btnsavemstr.Visible = False : btnDeleteMstr.Visible = False
            imbApproval.Visible = False : btnAddToList.Visible = True
            btnPrint.Visible = False : BtnPosting.Visible = False
        Else
            btnsavemstr.Visible = True : btnDeleteMstr.Visible = True
            btnPrint.Visible = False : btnAddToList.Visible = True
            imbApproval.Visible = True
        End If
        I_u2.Text = "New Detail"
    End Sub

    Private Sub generateNoPO()
        trnbelipono.Text = GenerateID("Ql_trnforecastmst", CompnyCode)
        ForeCastoid.Text = GenerateID("Ql_trnforecastmst", CompnyCode)
    End Sub

    Public Sub binddataSupp(ByVal sqlPlus As String)
        sqlPlus = "AND (suppname LIKE '%" & Tchar(suppliername.Text) & "%' OR suppcode LIKE '%" & Tchar(suppliername.Text) & "%')"
        sSql = "Select suppoid,oldsuppcode,suppaddr,suppcode,suppname,paymenttermoid,prefixsupp From QL_mstsupp Where supptype = 'GROSIR' and cmpcode='" & CompnyCode & "' " & sqlPlus & " ORDER BY suppcode ASC"
        FillGV(gvSupplier, sSql, "QL_mstsupp")
    End Sub

    Private Sub BinddataItem(ByVal sqlPlus As String)
        If trnsuppoid.Text = "" Then
            showMessage("- Maaf, Anda belum pilih suplier..!!", 2)
            Exit Sub
        End If
        sqlPlus = " AND (itemdesc LIKE '%" & Tchar(txtMaterial.Text) & "%' OR itemcode LIKE '%" & Tchar(txtMaterial.Text) & "%' OR merk LIKE '%" & Tchar(txtMaterial.Text) & "%')"
        Dim ufilter As String = ""
        Try
            sSql = "Select * From (" & _
            "Select m.Itemoid matoid,m.itemcode matcode,m.ITEMDESC matlongdesc, m.satuan1 as matunit1oid,m.satuan1,g1.gendesc unit1,g1.gendesc unit,iSNULL(m.merk,'NONE') merk, ISNULL((Select SUM(qtyIn)-SUM(qtyOut) From ql_conmtr con Where con.refoid=m.itemoid And con.periodacctg='" & Format(GetServerTime(), "yyyy-MM") & "' And mtrlocoid NOT IN (Select gl.genoid From ql_mstgen gl Where gl.genoid=con.mtrlocoid AND g1.gengroup='LOCATION' AND gl.genother5 NOT IN ('SERVICE','KONSINYASI'))),0.0000) saldoakhir,Isnull(lastPricebuyUnit1,0.0000) lastPricebuy From ql_mstitem m inner join QL_mstgen g1 on g1.genoid = m.satuan1 WHERE m.cmpcode = 'MSC' " & sqlPlus & " And itemflag = 'Aktif' and stockflag <> 'ASSET') item Order by matlongdesc"
            FillGV(gvMaterial, sSql, "ql_mstitem")
        Catch ex As Exception
            showMessage(ex.Message, 1)
            cProc.DisposeGridView(gvMaterial)
        End Try

    End Sub

    Private Sub ClearDetail()
        trnbelidtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            trnbelidtlseq.Text = objTable.Rows.Count + 1
            tbldtl.SelectedIndex = -1
            'MultiView1.ActiveViewIndex = 2
        End If
        '  mtrloc.SelectedIndex = 0
        I_u2.Text = "New Detail"
        trnbelidtloid.Text = "" : trnbelidtlnote.Text = ""
        matoid.Text = "" : trnbelidtlqty.Text = "0.0000"
        trnbelidtlunit.Text = "" : trnbelidtlprice.Text = "0.0000"
        amtbelinetto2.Text = "0.0000" : txtMaterial.Text = ""
    End Sub

    Private Sub ReAmountDetail()
        If trnbelidtlqty.Text.Trim = "" Then
            trnbelidtlqty.Text = "0.0000"
        End If

        If trnbelidtlprice.Text.Trim = "" Then
            trnbelidtlprice.Text = "0.0000"
        End If

        Dim qty As Double = ToDouble(trnbelidtlqty.Text)
        amtbelinetto2.Text = ToMaskEdit(CDbl((ToDouble(trnbelidtlprice.Text) * qty)), 4)
        If amtbelinetto2.Text.Trim = "" Then
            amtbelinetto2.Text = "0.0000"
        End If 
    End Sub

    Private Sub ReAmountDetail2()
        If trnbelidtlqty.Text.Trim = "" Then
            trnbelidtlqty.Text = "0.0000"
        End If

        If trnbelidtlprice.Text.Trim = "" Then
            trnbelidtlprice.Text = "0.0000"
        End If

        Dim qty As Double = ToDouble(trnbelidtlqty.Text)
        amtbelinetto2.Text = ToMaskEdit(CDbl((ToDouble(trnbelidtlprice.Text) * qty)), 4)
        If amtbelinetto2.Text.Trim = "" Then
            amtbelinetto2.Text = "0.00"
        End If
    End Sub

    Private Sub CreateTableTBLDTL()
        Dim dtlDS As DataSet = New DataSet
        Dim dtlTable As DataTable = New DataTable("TabelpodtlDetail")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnforecastdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("merk", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemqty", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("itemprice", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("amtdtlnetto", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnforecaststatusdtl", Type.GetType("System.String"))
        dtlTable.Columns.Add("notedtl", Type.GetType("System.String"))
        dtlDS.Tables.Add(dtlTable)
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub fillTotalOrder()
        Try
            Dim tbDtl As DataTable = Session("TblDtl")
            Dim NettAmt As Double = 0
            For c1 As Integer = 0 To tbDtl.Rows.Count - 1
                NettAmt += ToDouble(tbDtl.Rows(c1).Item("itemprice")) * ToDouble(tbDtl.Rows(c1).Item("itemqty"))
            Next
            amtbelinetto1.Text = ToMaskEdit(NettAmt, 3)
            totalNya.Text = ToMaskEdit(NettAmt, 3)
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub PrintReport(ByVal id As String, ByVal no As String, ByVal report2 As String)
        report.Load(Server.MapPath(folderReport & "rptNotaPO.rpt"))
        report.SetParameterValue("sWhere", " Where m.trnbelimstoid='" & id & "'")
        report.SetParameterValue("namaPencetak", Session("UserID"))
        report.SetParameterValue("LastStock", "And crd.periodacctg = '" & periodacctg.Text & "' And crd.mtrlocoid  <> -10")
        cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
            System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
        report.PrintOptions.PaperSize = PaperSize.PaperA5
        'report.PrintToPrinter(1, False, 0, 0)
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers

        If report2 = "pdf" Or report2 = "" Then
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        ElseIf report2 = "excel" Then
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        End If
        report.Close() : report.Dispose() : Session("no") = Nothing
        'Response.Redirect("~\Transaction\TrnForeCastBeli.aspx?awal=true")
    End Sub

    Private Sub PrintReportPTP(ByVal id As String, ByVal no As String, ByVal report2 As String)
        'untuk print
        'report.Load(Server.MapPath(folderReport & "POrder_hrg.rpt"))
        'report.SetParameterValue("Id", id)

        report.Load(Server.MapPath(folderReport & "rptNotaPO.rpt"))
        report.SetParameterValue("sWhere", " WHERE m.trnbelimstoid='" & id & "'")
        report.SetParameterValue("namaPencetak", Session("UserID"))

        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)
        report.PrintOptions.PaperSize = PaperSize.PaperA4
        report.PrintOptions.PrinterName = ddlprinter.SelectedValue
        report.PrintToPrinter(1, True, 0, 0)
        report.Close() : report.Dispose()
    End Sub

    Private Sub PrintReportNoPrice(ByVal id As String, ByVal no As String)
        'untuk print
        report.Load(Server.MapPath(folderReport & "POrder.rpt"))
        report.SetParameterValue("Id", id)
        cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
        report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        'report.PrintToPrinter(1, False, 0, 0)
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        report.Close() : report.Dispose() : Session("no") = Nothing
        Response.Redirect("~\Transaction\TrnForeCastBeli.aspx?awal=true")
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\TrnForeCastBeli.aspx")
            '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        Page.Title = CompnyName & " - Forecast PO"
        Session("oid") = Request.QueryString("oid")
        btnDeleteMstr.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        BtnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data ?');")
        imbApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Send this data for Approval?');")
        btnPrint.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Print this data?');")

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            fDDLBranch() : BindData(CompnyCode)
            InitDDLBranch() : InitAllDDL()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid")) : i_u.Text = "UPDATE"
                TabContainer1.ActiveTabIndex = 1
            Else
                i_u.Text = "N E W" : generateNoPO()
                posting.Text = "In Process"
                trnbelipodate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                periodacctg.Text = Format(GetServerTime, "yyyy-MM")
                btnDeleteMstr.Visible = False
                TabContainer1.ActiveTabIndex = 0
                btnPrint.Visible = False
            End If
        End If
        Dim objTable As New DataTable : objTable = Session("TblDtl") ': MsgBox(objTable.Rows.Count)
        tbldtl.DataSource = objTable : tbldtl.DataBind() : tbldtl.Visible = True
        'fillTot() ': ReAmount()
    End Sub

    Protected Sub TblData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles tbldata.PageIndexChanging
        tbldata.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If chkPeriod.Checked Then
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text))
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text))
                If dat1 < CDate("01/01/1900") Or dat2 < CDate("01/01/1900") Then
                    showMessage("Maaf, Format tanggal salah!!", 2) : Exit Sub
                End If
                If dat1 > dat2 Then
                    showMessage("Maaf, Tanggal Awal harus lebih kecil daripada Tanggal Akhir!!", 2) : Exit Sub
                End If
            Catch ex As Exception
                showMessage("Maaf, Format tanggal salah!!", 2)
                Exit Sub
            End Try
        End If
        BindData(CompnyCode)
        'FilterText.Text = str
    End Sub

    Protected Sub TblDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles tbldtl.PageIndexChanging
        tbldtl.PageIndex = e.NewPageIndex
        tbldtl.Visible = True
        tbldtl.DataSource = Session("TblDtl")
        tbldtl.DataBind()
    End Sub

    Protected Sub tbldtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldtl.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
                e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
                e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4) 
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Protected Sub TblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow

        Dim podtlseq As Integer = objTable.Rows(e.RowIndex).Item("seq")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.RemoveAt(e.RowIndex)

        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            podtlseq = objTable.Rows(C1).Item("seq")

            dr.BeginEdit()
            dr(3) = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        Session("TabelDtl") = objTable
        Session("ItemLine") = Session("ItemLine") - 1
        trnbelidtlseq.Text = Session("ItemLine")
        tbldtl.DataSource = Session("TabelDtl")
        tbldtl.DataBind() : fillTotalOrder()
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        binddataSupp("")
        gvSupplier.Visible = True
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppliername.Text = "" : trnsuppoid.Text = ""
        If gvSupplier.Visible = True Then
            gvSupplier.DataSource = Nothing
            gvSupplier.DataBind()
            gvSupplier.Visible = False
        End If
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex 
        binddataSupp("")
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        'ButtonSuplier.Visible = False : PanelSuplier.Visible = False : ModalPopupExtender1.Hide()
        trnsuppoid.Text = gvSupplier.SelectedDataKey("suppoid").ToString().Trim
        suppliername.Text = gvSupplier.SelectedDataKey("suppname").ToString().Trim
        PrefixOid.Text = gvSupplier.SelectedDataKey("prefixsupp").ToString().Trim
        Try
            TRNPAYTYPE.SelectedValue = gvSupplier.SelectedDataKey("paymenttermoid")
        Catch ex As Exception
        End Try
        cProc.DisposeGridView(sender) : gvSupplier.Visible = False
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearmat.Click
        txtMaterial.Text = "" : matoid.Text = "" 
        gvMaterial.Visible = False
        gvMaterial.DataSource = Nothing
        gvMaterial.DataBind()
        trnbelidtlprice.Text = "0.0000"
    End Sub

    Protected Sub gvMaterial_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaterial.PageIndexChanging
        gvMaterial.PageIndex = e.NewPageIndex
        BinddataItem("")
    End Sub

    Protected Sub gvMaterial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaterial.SelectedIndexChanged
        txtMaterial.Text = gvMaterial.SelectedDataKey.Item("matlongdesc").ToString.Trim
        matoid.Text = Integer.Parse(gvMaterial.SelectedDataKey.Item("matoid"))
        trnbelidtlunit.Text = gvMaterial.SelectedDataKey.Item("matunit1oid")
        trnbelidtlprice.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("lastPricebuy"), 3)
        initUnit()
        cProc.DisposeGridView(sender) : gvMaterial.Visible = False 
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchmat.Click
        BinddataItem("")
        gvMaterial.Visible = True
    End Sub

    Protected Sub trnbelipodate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelipodate.TextChanged
        Try
            periodacctg.Text = Now.ToUniversalTime
            periodacctg.Text = Format(CDate(toDate(trnbelipodate.Text)), "yyyy-MM")
        Catch ex As Exception
            periodacctg.Text = ""
        End Try
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Dim sPsn As String = ""
        If matoid.Text = "" Then
            sPsn &= "- Maaf, Silahkan Pilih Item dahulu !!<br>"
            Exit Sub
        End If

        If ToDouble(trnbelidtlqty.Text) <= 0 Then
            sPsn &= "- Maaf, Quantity masih nol..!!<br>"
        End If

        If ToDouble(trnbelidtlprice.Text) < 0 Or ToDouble(trnbelidtlprice.Text) = 0 Then
            sPsn &= "- Maaf, Harga tidak bisa minus !!<br>"
        End If

        If Session("TblDtl") Is Nothing Then
            CreateTableTBLDTL()
        End If

        Dim objTable As DataTable
        objTable = Session("TblDtl")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        If I_u2.Text = "New Detail" Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "itemoid=" & matoid.Text & ""
            If dv.Count > 0 Then
                dv.RowFilter = ""
                sPsn &= "- Maaf, Data ini sudah di tambahkan sebelumnya, Silahkan cek ulang..!!<br>"
                objTable = Session("TblDtl") : tbldtl.DataSource = objTable
                tbldtl.DataBind() : tbldtl.Visible = True 
            End If
            dv.RowFilter = ""
        Else
            Dim dv As DataView = objTable.DefaultView

            dv.RowFilter = "itemoid=" & matoid.Text & " AND seq <> " & trnbelidtlseq.Text
            If dv.Count > 0 Then
                dv.RowFilter = ""
                sPsn &= "- Maaf, Data ini sudah di tambahkan sebelumnya, Silahkan cek ulang..!!<br>"
                objTable = Session("TblDtl") : tbldtl.DataSource = objTable
                tbldtl.DataBind() : tbldtl.Visible = True 
            End If
            dv.RowFilter = ""
        End If

        If ToDouble(amtbelinetto2.Text) < 0 Then
            sPsn &= "- Maaf, Amount Netto detail tidak bisa minus..!!<br>"
        End If

        If sPsn <> "" Then
            showMessage(sPsn, 2)
            Exit Sub
        End If

        'insert/update to list data
        Dim objRow As DataRow
        If I_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("seq") = objTable.Rows.Count + 1
        Else 'update
            objRow = objTable.Rows(trnbelidtlseq.Text - 1)
            objRow.BeginEdit()
        End If

        objRow("cmpcode") = CompnyCode
        objRow("trnforecastdtloid") = 0
        objRow("itemoid") = matoid.Text
        objRow("itemdesc") = txtMaterial.Text
        objRow("merk") = ""
        objRow("itemqty") = ToDouble(trnbelidtlqty.Text)
        objRow("itemprice") = ToDouble(trnbelidtlprice.Text)
        If ToDouble(objRow("itemprice")) = 0 Then
            showMessage("- Maaf, Price Tidak Boleh Nol..!!", 2)
            Exit Sub
        End If
        objRow("amtdtlnetto") = ToDouble(amtbelinetto2.Text)
        objRow("unitoid") = ddlUnit.SelectedValue
        objRow("itemunit") = Trim(ddlUnit.SelectedItem.Text)
        objRow("trnforecaststatusdtl") = posting.Text
        objRow("notedtl") = trnbelidtlnote.Text

        If I_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
        End If
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        Session("TabelDtl") = objTable
        fillTotalOrder()

        ClearDetail() : tbldtl.Visible = True
        tbldtl.DataSource = objTable
        tbldtl.DataBind()

        trnbelidtlseq.Text = objTable.Rows.Count + 1
        ddlUnit.Items.Clear()
        tbldtl.SelectedIndex = -1 
    End Sub

    Protected Sub trnbelidtlqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnbelidtlqty.TextChanged, trnbelidtlprice.TextChanged
        Dim text As System.Web.UI.WebControls.TextBox = CType(sender, System.Web.UI.WebControls.TextBox)
        text.Text = ToMaskEdit(ToDouble(text.Text), 4)
        ReAmountDetail()
    End Sub

    Protected Sub tbldtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbldtl.SelectedIndexChanged
        Try
            trnbelidtlseq.Text = tbldtl.SelectedDataKey("seq")
            If Session("TblDtl") Is Nothing = False Then
                I_u2.Text = "Update Detail"
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "seq=" & trnbelidtlseq.Text
                matoid.Text = dv.Item(0).Item("itemoid")
                trnbelidtlqty.Text = ToMaskEdit(dv.Item(0).Item("itemqty"), 3)
                trnbelidtlprice.Text = ToMaskEdit(dv.Item(0).Item("itemprice"), 3)
                trnbelidtlnote.Text = dv.Item(0).Item("notedtl")
                amtbelinetto2.Text = ToMaskEdit(dv.Item(0).Item("amtdtlnetto"), 3)
                txtMaterial.Text = dv.Item(0).Item("itemdesc")
                initUnit()
                ddlUnit.SelectedValue = dv.Item(0).Item("unitoid")
                dv.RowFilter = ""
            End If

        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try 
    End Sub

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer, ByVal gvObj As GridView) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvObj.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Protected Sub Close_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.DisposeGridView(gvSupplier)
    End Sub

    Protected Sub ButtonValidasi_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
        'Validasi.Text = ""
    End Sub

    Protected Sub btnsavemstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsavemstr.Click
        Dim sErr As String = ""
        If trnsuppoid.Text.Trim = "" Then
            sErr &= "- Maaf, Silahkan pilih Supplier dahulu..!!<br>"
            posting.Text = "In Process" 
        End If

        If IsDate(toDate(trnbelipodate.Text)) = False Then
            'Validasi.Text = "Invalid PO Date !"
            sErr &= "Maaf, Format tanggal Purchase Order salah..!!<br>"
            posting.Text = "In Process" 
        Else
            If CDate(toDate(trnbelipodate.Text)) < CDate("01/01/1900") Then
                sErr &= "- Maaf, Format tanggal salah..!!<br>"
                posting.Text = "In Process" 
            End If
            If CDate(toDate(trnbelipodate.Text)) > CDate(toDate("6/6/2079")) Then
                sErr &= "- Maaf, Tanggal harus kurang dari 06/06/2079..!!<br>"
                posting.Text = "In Process"
            End If
        End If

        Try
            periodacctg.Text = Format(CDate(toDate(trnbelipodate.Text)), "yyyy-MM")
        Catch ex As Exception
            sErr &= "- Maaf, Format tanggal Purchase Order salah..!!<br>"
            posting.Text = "In Process"
        End Try

        'cek apa sudah ada material dari po 
        If Session("TblDtl") Is Nothing Then
            sErr &= "- Maaf, Data tidak bisa disimpan, Tidak ada Data detail katalog !!<br>"
            posting.Text = "In Process"
        Else
            Dim objTableCek As DataTable = Session("TblDtl")
            If objTableCek.Rows.Count = 0 Then
                sErr &= "- Maaf, Data tidak bisa disimpan, Tidak ada Data detail katalog !!<br>"
                posting.Text = "In Process"
            End If
        End If
        ' For checking approval user when editing In Approval PR 
        If ToDouble(amtbelinetto1.Text) < 0 Then
            'showMessage("Net Purchase tidak boleh minur !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            'Exit Sub
        End If

        ' checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(trnbelipono.Text, "trnforecastno", "Ql_trnforecastmst") Then
                generateNoPO()
            End If
        Else 'cek data ada atau sudah di hapuse
            If CheckDataExists(ForeCastoid.Text, "trnforecastoid", "Ql_trnforecastmst") = False Then
                sErr &= "- Maaf, Tidak bisa update Data ini, Data sudah didelete !!<br>"
            End If
        End If

        If checkApproval("ql_trnforecastmst", "In Approval", Session("oid"), "New", "FINAL", CabangNya.SelectedValue) > 0 Then
            sErr &= "- Maaf, Data ini sudah di send for Approval..!!<br>"
        End If

        '-----------------------------------------------------------------------------------------
        If posting.Text = "In Approval" Then
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnforecastmst' and branch_code Like '%" & CabangNya.SelectedValue & "%' order by approvallevel"
            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                sErr &= "- Maaf,Approval User untuk Forecast belum disetting, Silahkan contact admin..!!<br> "
            End If

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            Try
                If Session("oid") <> Nothing Or Session("oid") <> "" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus,branch_code) VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", 'FPO" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','ql_trnforecastmst', '" & Session("oid") & "','In Approval','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1', '" & objTable.Rows(c1).Item("approvalstatus") & "','" & CabangNya.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Else
                    sErr &= "- Maaf, Silahkan Simpan data terlebih dahulu..!!<br>"
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As OleDb.OleDbException
                objTrans.Rollback()
                xCmd.Connection.Close()
                showMessage(ex.ToString, 1)
                Exit Sub
            End Try
            '-------------------------------------------------------------------------------------
        ElseIf posting.Text = "Rejected" Then
            posting.Text = "In Process"
        ElseIf posting.Text = "Revised" Then
            posting.Text = "In Process"
        End If
 
        'Dim cRate As New ClassRate()
        'cRate.SetRateValue(CInt(1), Format(GetServerTime(), "MM/dd/yyyy"))
        'If cRate.GetRateDailyLastError <> "" Then
        '    sErr &= cRate.GetRateDailyLastError & "<br>"
        'End If
        'If cRate.GetRateMonthlyLastError <> "" Then
        '    sErr &= cRate.GetRateMonthlyLastError & "<br>"
        'End If
        If sErr <> "" Then
            showMessage(sErr, 2)
            Exit Sub
        End If
        If Session("oid") = Nothing Or Session("oid") = "" Then
            ' insert table master
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            ForeCastoid.Text = GenerateID("Ql_trnforecastmst", CompnyCode)
            trnbelidtloid.Text = GenerateID("Ql_trnforecastdtl", CompnyCode)
            xCmd.Transaction = objTrans
            Dim DD As String = periodacctg.Text
            'GetPeriodAcctg(GetServerTime())
            Try
                sSql = "INSERT INTO [Ql_trnforecastmst] ([cmpcode],[branch_code],[trnforecastoid],[trnforecastno],[trnforecastdate],[periodacctg],[suppoid],[trnpaytypeoid],[amountnett],[trnforecaststatus],[trnforecastnote],[approvaluser],[approvaldate],[upduser],[updtime],prefixoid)" & _
                "VALUES ('MSC','" & CabangNya.SelectedValue & "'," & ForeCastoid.Text & ",'" & Tchar(trnbelipono.Text) & "','" & toDate(trnbelipodate.Text) & "','" & periodacctg.Text & "'," & trnsuppoid.Text & "," & TRNPAYTYPE.SelectedValue & "," & ToDouble(amtbelinetto1.Text) & ",'" & posting.Text & "','" & Tchar(trnbelinote.Text) & "','',1/1/1900,'" & Session("UserID") & "',CURRENT_TIMESTAMP," & DDLPrefix.SelectedValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstoid set lastoid=" & ForeCastoid.Text & " Where upper(tablename)='Ql_trnforecastmst' And cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then

                    Dim objTable As DataTable : objTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO [ql_trnforecastdtl] ([cmpcode],[branch_code],[trnforecastdtloid],[trnforecastoid],[itemoid],[unitoid],[itemqty],[itemprice],[trnforecaststatusdtl],[upduser],[updtime],[seq],[amtdtlnetto],[notedtl]) " & _
                        "VALUES ('MSC','" & CabangNya.SelectedValue & "'," & trnbelidtloid.Text & "," & ForeCastoid.Text & "," & Integer.Parse(objTable.Rows(c1).Item("itemoid")) & "," & Integer.Parse(objTable.Rows(c1).Item("unitoid")) & "," & ToDouble(objTable.Rows(c1).Item("itemqty")) & "," & ToDouble(objTable.Rows(c1).Item("itemprice")) & ",'IN PROCESS','" & Session("UserID") & "',CURRENT_TIMESTAMP," & objTable.Rows(c1).Item("seq") & "," & ToDouble(objTable.Rows(c1).Item("amtdtlnetto")) & ",'" & Tchar(objTable.Rows(c1).Item("notedtl")) & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        trnbelidtloid.Text += 1
                    Next
                    sSql = "update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(trnbelidtloid.Text)) & " Where tablename = 'ql_trnforecastdtl' And cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, 1)
                xCmd.Connection.Close() : posting.Text = "In Process" : Exit Sub
            End Try
        Else
            'update tabel master    
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "UPDATE [Ql_trnforecastmst] SET [trnforecastdate] = '" & toDate(trnbelipodate.Text) & "',[periodacctg] = '" & periodacctg.Text & "',[suppoid] = " & trnsuppoid.Text & ",[trnpaytypeoid] = " & TRNPAYTYPE.SelectedValue & ",[amountnett] = " & ToDouble(amtbelinetto1.Text) & ",[trnforecaststatus] = '" & posting.Text & "',[trnforecastnote] = '" & Tchar(trnbelinote.Text) & "',[upduser] = '" & Session("UserID") & "',[updtime] =CURRENT_TIMESTAMP,[prefixoid] = " & DDLPrefix.SelectedValue & " WHERE trnforecastoid=" & ForeCastoid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    sSql = "Delete from ql_trnforecastdtl Where trnforecastoid = " & Session("oid")
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'insert tabel detail
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    trnbelidtloid.Text = GenerateID("ql_trnforecastdtl", CompnyCode)
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO [ql_trnforecastdtl] ([cmpcode],[branch_code],[trnforecastdtloid],[trnforecastoid],[itemoid],[unitoid],[itemqty],[itemprice],[trnforecaststatusdtl],[upduser],[updtime],[seq],[amtdtlnetto],[notedtl]) " & _
                         "VALUES ('MSC','" & CabangNya.SelectedValue & "'," & trnbelidtloid.Text & "," & ForeCastoid.Text & "," & Integer.Parse(objTable.Rows(c1).Item("itemoid")) & "," & Integer.Parse(objTable.Rows(c1).Item("unitoid")) & "," & ToDouble(objTable.Rows(c1).Item("itemqty")) & "," & ToDouble(objTable.Rows(c1).Item("itemprice")) & ",'IN PROCESS','" & Session("UserID") & "',CURRENT_TIMESTAMP," & objTable.Rows(c1).Item("seq") & "," & ToDouble(objTable.Rows(c1).Item("amtdtlnetto")) & ",'" & Tchar(objTable.Rows(c1).Item("notedtl")) & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        trnbelidtloid.Text += 1
                    Next

                    sSql = "Update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(trnbelidtloid.Text)) & " Where tablename = 'ql_trnforecastdtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                posting.Text = "In Process"
                objTrans.Rollback() : xCmd.Connection.Close()
                showMessage(ex.ToString, 1)
                Exit Sub
            End Try
        End If
        Response.Redirect("~\Transaction\TrnForeCastBeli.aspx")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnPosting.Click
        posting.Text = "In Approval"
        btnsavemstr_Click(sender, e)
    End Sub

    Protected Sub btnCancelMstr1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelMstr.Click
        Session("oid") = Nothing : Session("TblDtl") = Nothing
        Response.Redirect("~\Transaction\TrnForeCastBeli.aspx?awal=true")
    End Sub

    Protected Sub btnDeleteMstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteMstr.Click
        'MsgBox(oid.Text)
        Dim sErr As String = ""
        If ForeCastoid.Text = "" Then
            sErr &= "- Maaf, ID Forecast Salah..!!<br>" 
        End If

        Dim objTrans As SqlClient.SqlTransaction
        Dim dtTempWeb As DataTable = checkApprovaldata("ql_trnforecastmst", "In Approval", Session("oid"), "", "New")
        If checkApproval("ql_trnforecastmst", "In Approval", Session("oid"), "New", "FINAL", CabangNya.SelectedValue) > 0 Then
            sErr &= "- Maaf, Data ini sudah di send for Approval..!!<br>"
        End If
        If sErr <> "" Then
            showMessage(sErr, 2)
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update status Approval if exist to DELETED
            If dtTempWeb.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtTempWeb.Rows.Count - 1
                    sSql = "UPDATE ql_approval SET statusrequest='Deleted' WHERE cmpcode = '" & CompnyCode & _
                        "' AND approvaloid='" & dtTempWeb.Rows(C1)("approvaloid").ToString & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            sSql = "Delete from Ql_trnforecastdtl where trnbelimstoid = " & ForeCastoid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Delete from Ql_trnforecastmst where trnbelimstoid = " & ForeCastoid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString, 1) : Exit Sub
        End Try
        Response.Redirect("~\Transaction\TrnForeCastBeli.aspx")
    End Sub

    Protected Sub btnCancelDtl_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelDtl.Click
        ClearDetail()
    End Sub

    Protected Sub imbApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbApproval.Click
        posting.Text = "In Approval" : btnsavemstr_Click(sender, e)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Dim report2 As String = "pdf"
        showMessageprint(Session("oid"), trnbelipono.Text, report2)
    End Sub

    Protected Sub tbldata_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        showMessageprint(tbldata.SelectedDataKey.Item(0), tbldata.SelectedDataKey.Item(1), "")
    End Sub

    Protected Sub LinkButton8_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim report2 As String = ""
        PrintReport(tbldata.SelectedDataKey.Item(0), tbldata.SelectedDataKey.Item(1), report2)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldata.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        'End If
    End Sub

    Protected Sub imbFindSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sPlus As String = "" : binddataSupp(sPlus)
    End Sub

    Protected Sub imbAllSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddataSupp("")
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ddlFilter.SelectedIndex = 0 : FilterText.Text = ""
        chkStatus.Checked = False : DDLStatus.SelectedIndex = 0
        Session("FilterText") = Nothing : Session("FilterDDL") = Nothing
        chkPeriod.Checked = False : dCabangNya.SelectedValue = "ALL"
        BindData(CompnyCode)
    End Sub

    Protected Sub btnContinoeReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'generateNoPO()
        Dim objTable As DataTable
        Session("TblDtl") = Nothing
        ClearDetail()
        objTable = Session("TblDtl")
    End Sub

    Protected Sub gvMaterial_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4) 
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Protected Sub btncloseprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
    End Sub

    Protected Sub btpPrintAction_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btpPrintAction.Click
        If ddlprinter.SelectedValue = "PDF" Then
            Dim report2 As String = "pdf"
            PrintReport(idTemp.Text, NoTemp.Text, OtherTemp.Text)
        ElseIf ddlprinter.SelectedValue = "EXCEL" Then
            Session("showReport") = True
            showPrintExcel(NoTemp.Text, CInt(idTemp.Text))
        Else
            PrintReportPTP(idTemp.Text, NoTemp.Text, OtherTemp.Text)
        End If

        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
        'Validasi.Text = ""
    End Sub

    Protected Sub autogenerate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Try
        '    sSql = "select i.cmpcode, itemoid , itemdesc ItemLDesc , itemgroupoid Category ,Merk ,satuan3 itemunitoid ,i.maxstockgrosir - ((select isnull(SUM(saldoAkhir - qtyBooking),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = i.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 IN (select genoid from QL_mstgen where genother1 = 'GROSIR' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION'))) orderqty , g.gendesc unit ,i.itemsafetystockunitgrosir minstock,i.maxstockgrosir makstock, (select isnull(SUM(saldoAkhir - qtyBooking ),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = i.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 IN (select genoid from QL_mstgen where genother1 = 'GROSIR' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')) stock from QL_MSTITEM i inner join QL_mstgen g on i.satuan3 = g.genoid where i.itemsafetystockunitgrosir > (select isnull(SUM(saldoAkhir - qtyBooking ),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = i.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 IN (select genoid from QL_mstgen where genother1 = 'GROSIR' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')) and itemflag = 'Aktif' and merk like '%" & Tchar(MerkGenerate.Text) & "%' and itemdesc like '%" & Tchar(ItemdescGenerate.Text) & "%' " & IIf(Catgenerate.SelectedValue = "SEMUA GROUP", "", "and i.itemgroupoid = " & Catgenerate.SelectedValue & "")
        '    dtStaticItem = isiDS(sSql, "ItemOrdered")
        '    Session("TblDtl") = insertData(dtStaticItem)
        '    tbldtl.Visible = True
        '    tbldtl.DataSource = Session("TblDtl")
        '    tbldtl.DataBind()
        '    dtStaticItem.Clear()
        'Catch ex As Exception
        '    showMessage(ex.ToString & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        'End Try
    End Sub

    Protected Sub FilterPeriod1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterPeriod1.TextChanged
        chkPeriod.Checked = True
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub
#End Region
End Class