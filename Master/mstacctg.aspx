<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstacctg.aspx.vb" Inherits="Master_Default" title="MSC - Data Chart of Accounts" %>
<%-- Add content controls here --%>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
  <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0" width="100%" class="tabelhias">
        <tr>
            <th class="header" colspan="5" valign="top">        
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Chart Of Accounts"></asp:Label>
                </th>
        </tr>
    </table>    
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" 
                    Width="100%" Font-Size="10pt">
                   <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                       <ContentTemplate>
                           <asp:UpdatePanel id="UpdatePanel2" runat="server">
                               <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="912px" __designer:wfdid="w79"><TABLE width="100%"><TBODY><TR><TD align=left colSpan=6>Filter : <asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w80"><asp:ListItem Value="acctgdesc">Account Name</asp:ListItem>
<asp:ListItem Value="acctgcode">Account Number</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="121px" CssClass="inpText" __designer:wfdid="w81" MaxLength="30"></asp:TextBox> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w82"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w83"></asp:ImageButton> <asp:ImageButton id="imbPrintCOA" onclick="imbPrintCOA_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w84"></asp:ImageButton></TD></TR><TR><TD colSpan=6><asp:GridView id="GridView1" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w85" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="GridView1_PageIndexChanging" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="Red"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="acctgoid" DataNavigateUrlFormatString="mstacctg.aspx?oid={0}" DataTextField="acctgcode" HeaderText="Code" SortExpression="acctgcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Name" SortExpression="acctgdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="350px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdbcr" HeaderText="Debet/Credit" SortExpression="acctgdbcr">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctggrp1" HeaderText="Category" SortExpression="acctggrp1">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgflag" HeaderText="Status" SortExpression="acctgflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgoid" HeaderText="acctgoid" SortExpression="acctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
            <asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!!"></asp:Label>
            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                               <triggers>
<asp:PostBackTrigger ControlID="imbPrintCOA"></asp:PostBackTrigger>
</triggers>
                           </asp:UpdatePanel>
                           <asp:GridView ID="GVmst" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                               CellPadding="4" DataSourceID="SQLMst" EmptyDataText="No data in database." Font-Names="Microsoft Sans Serif"
                               Font-Size="XX-Small" ForeColor="#333333" GridLines="None" Height="17px" PageSize="15"
                               Visible="False" Width="134px" EnableModelValidation="True">
                               <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                               <Columns>
                                   <asp:BoundField DataField="acctgcode" HeaderText="acctgcode" SortExpression="acctgcode">
                                       <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="acctgdesc" HeaderText="acctgdesc" SortExpression="acctgdesc">
                                       <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="acctgdbcr" HeaderText="acctgdbcr" SortExpression="acctgdbcr">
                                       <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="acctggrp1" HeaderText="acctggrp1" SortExpression="acctggrp1">
                                       <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="acctggrp2" HeaderText="acctggrp2" SortExpression="acctggrp2">
                                       <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
                                   </asp:BoundField>
                               </Columns>
                               <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                               <PagerTemplate>
                               </PagerTemplate>
                               <PagerStyle BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333" HorizontalAlign="Right" />
                               <EmptyDataTemplate>
                                   <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Height="23px" Text="No Data Found !"
                                       Visible="False" Width="83px"></asp:Label>
                               </EmptyDataTemplate>
                               <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                               <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                               <AlternatingRowStyle BackColor="White" />
                           </asp:GridView>
                           <asp:SqlDataSource ID="SQLMst" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                    SelectCommand="SELECT  [cmpcode], [acctgoid], [acctgcode], [acctgdesc], [acctgdbcr], [acctggrp1], [acctggrp2] FROM [QL_mstacctg]&#13;&#10;WHERE [cmpcode] like @cmpcode and  [acctgoid] like @acctgoid and [acctgcode] like @acctgcode and [acctgdesc] like @acctgdesc and [acctgdbcr] like @acctgdbcr and  [acctggrp1] like @acctggrp1 and  [acctggrp2] like @acctggrp2&#13;&#10;ORDER BY ACCTGCODE" InsertCommand="INSERT INTO QL_mstacctg (cmpcode, acctgoid, acctgcode, acctgdesc, acctgdbcr, acctggrp1, acctggrp2, acctggrp3, acctgflag, upduser, updtime)&#13;&#10;VALUES (@cmpcode, @acctgoid, @acctgcode, @acctgdesc, @acctgdbcr, @acctggrp1, @acctggrp2, @acctggrp3, @acctgflag, @upduser, @updtime,@datetime)">
                                            <SelectParameters>
                                                <asp:Parameter Name="cmpcode" />
                                                <asp:Parameter Name="acctgoid" />
                                                <asp:Parameter Name="acctgcode" />
                                                <asp:Parameter Name="acctgdesc" />
                                                <asp:Parameter Name="acctgdbcr" />
                                                <asp:Parameter Name="acctggrp1" />
                                                <asp:Parameter Name="acctggrp2" />
                                            </SelectParameters>
                                            <InsertParameters>
                                                <asp:Parameter Name="cmpcode" />
                                                <asp:Parameter Name="acctgoid" />
                                                <asp:Parameter Name="acctgcode" />
                                                <asp:Parameter Name="acctgdesc" />
                                                <asp:Parameter Name="acctgdbcr" />
                                                <asp:Parameter Name="acctggrp1" />
                                                <asp:Parameter Name="acctggrp2" />
                                                <asp:Parameter Name="acctggrp3" />
                                                <asp:Parameter Name="acctgflag" />
                                                <asp:Parameter Name="upduser" />
                                                <asp:Parameter Name="updtime" />
                                                <asp:Parameter Name="datetime" />
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                       </ContentTemplate>
                       <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                           <span style="font-size: 9pt">
                                <strong>
                                List of Chart Of Accounts</strong></span><strong><span style="font-size: 9pt">:.</span></strong>
                       </HeaderTemplate>
                   </ajaxToolkit:TabPanel>
                   <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                       <ContentTemplate>
                           <asp:UpdatePanel id="UpdatePanel1" runat="server">
                               <contenttemplate>
<TABLE style="WIDTH: 728px; HEIGHT: 123px; TEXT-ALIGN: center" id="Table1" cellSpacing=2 cellPadding=4 border=0><TBODY><TR style="COLOR: #000099"><TD align=left colSpan=2><asp:Label id="i_u" runat="server" ForeColor="Red" Text="New Data" __designer:wfdid="w39" Visible="False"></asp:Label></TD></TR><TR style="COLOR: #000099"><TD align=left>Category</TD><TD style="WIDTH: 254px; TEXT-ALIGN: left"><asp:DropDownList id="ddlAccType" runat="server" Width="100px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w42" OnSelectedIndexChanged="ddlAccType_SelectedIndexChanged" AutoPostBack="True">
                                                       <asp:ListItem>ASSET</asp:ListItem>
                                                       <asp:ListItem>LIABILITY</asp:ListItem>
                                                       <asp:ListItem>CAPITAL</asp:ListItem>
                                                       <asp:ListItem>REVENUE</asp:ListItem>
                                                       <asp:ListItem>EXPENSE</asp:ListItem>
                                                   </asp:DropDownList> &nbsp;<asp:TextBox id="oid" runat="server" Width="28px" CssClass="inpText" MaxLength="10" __designer:wfdid="w43" Visible="False" Enabled="False"></asp:TextBox> &nbsp;<asp:TextBox id="sFilter" runat="server" Width="100px" CssClass="inpText" MaxLength="10" __designer:wfdid="w43"></asp:TextBox></TD></TR><TR style="COLOR: #000099"><TD style="FONT-SIZE: x-small; WIDTH: 20%; TEXT-ALIGN: left" vAlign=top><asp:CheckBox id="chkParent" runat="server" Font-Size="X-Small" Text="Has Parent" __designer:wfdid="w44" AutoPostBack="True" OnCheckedChanged="chkParent_CheckedChanged">
                                                   </asp:CheckBox> <asp:Label id="lblParentID" runat="server" __designer:wfdid="w58" Visible="False"></asp:Label></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:ImageButton id="imbChoose" onclick="imbChoose_Click" runat="server" ImageUrl="~/Images/choose.png" ImageAlign="AbsMiddle" __designer:wfdid="w45" Visible="False">
                                                   </asp:ImageButton> <asp:ImageButton id="imbErase" onclick="imbErase_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w46" Visible="False">
                                                   </asp:ImageButton> &nbsp;<asp:Label id="txtParentCode" runat="server" __designer:wfdid="w47"></asp:Label> &nbsp;<asp:Label id="Label2" runat="server" Text="-" __designer:wfdid="w48"></asp:Label> &nbsp;<asp:Label id="txtParentName" runat="server" __designer:wfdid="w49"></asp:Label> <BR /><asp:GridView id="gvParent" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w50" OnSelectedIndexChanged="gvParent_SelectedIndexChanged" DataKeyNames="acctgoid,acctgcode,acctgdesc" AllowPaging="True" OnPageIndexChanging="gvParent_PageIndexChanging" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
                                                       <RowStyle BackColor="#FFFBD6" ForeColor="#333333">
                                                       </RowStyle>
                                                       <Columns>
                                                           <asp:CommandField ShowSelectButton="True">
                                                               <HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px">
                                                               </HeaderStyle>
                                                               <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px">
                                                               </ItemStyle>
                                                           </asp:CommandField>
                                                           <asp:BoundField DataField="acctgoid" HeaderText="ID" Visible="False">
                                                               <HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Acctgcode" HeaderText="Account Number">
                                                               <HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="200px">
                                                               </HeaderStyle>
                                                               <ItemStyle Font-Size="X-Small" ForeColor="Navy" Width="200px"></ItemStyle>
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Acctgdesc" HeaderText="Account Name">
                                                               <HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="350px">
                                                               </HeaderStyle>
                                                               <ItemStyle Font-Size="X-Small" ForeColor="Navy" Width="350px"></ItemStyle>
                                                           </asp:BoundField>
                                                       </Columns>
                                                       <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                       <PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
                                                       <EmptyDataTemplate>
                                                           <asp:Label id="Label3" runat="server" CssClass="Important" Text="No data found !!" __designer:wfdid="w49"></asp:Label>
                                                       </EmptyDataTemplate>
                                                       <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>
                                                       <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                                       <AlternatingRowStyle BackColor="White">
                                                       </AlternatingRowStyle>
                                                   </asp:GridView> </TD></TR><TR style="COLOR: #000099"><TD style="FONT-SIZE: x-small; WIDTH: 20%; TEXT-ALIGN: left">Account Number <asp:Label id="Label4" runat="server" CssClass="Important" Text="*" __designer:wfdid="w51"></asp:Label> </TD><TD style="WIDTH: 254px; TEXT-ALIGN: left"><asp:TextBox id="lblCode1" runat="server" Width="60px" CssClass="inpTextDisabled" __designer:wfdid="w52" Visible="False" ReadOnly="True"></asp:TextBox> <asp:Label id="lbl" runat="server" Font-Size="X-Small" ForeColor="White" Text="-" __designer:wfdid="w53" Visible="False"></asp:Label> <asp:TextBox id="Acctgcode" runat="server" Width="100px" CssClass="inpText" Font-Size="X-Small" MaxLength="15" __designer:wfdid="w54" OnTextChanged="Acctgcode_TextChanged"></asp:TextBox> </TD></TR><TR style="COLOR: #000099"><TD style="FONT-SIZE: x-small; WIDTH: 20%; TEXT-ALIGN: left">Account Name <asp:Label id="Label5" runat="server" CssClass="Important" Text="*" __designer:wfdid="w55"></asp:Label> </TD><TD style="WIDTH: 254px; TEXT-ALIGN: left"><asp:TextBox id="txtAccName" runat="server" Width="200px" CssClass="inpText" Font-Size="X-Small" MaxLength="100" __designer:wfdid="w56"></asp:TextBox> </TD></TR><TR style="COLOR: #000099"><TD style="FONT-SIZE: x-small; WIDTH: 20%; TEXT-ALIGN: left">Debet / Credit</TD><TD style="WIDTH: 254px; TEXT-ALIGN: left"><asp:DropDownList id="AcctgDbCr" runat="server" Width="100px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w57">
                                                       <asp:ListItem Text="Debet" Value="D"></asp:ListItem>
                                                       <asp:ListItem Text="Credit" Value="C"></asp:ListItem>
                                                   </asp:DropDownList> </TD></TR><TR style="COLOR: #000099"><TD style="FONT-SIZE: x-small; WIDTH: 20%; TEXT-ALIGN: left">Cabang </TD><TD style="WIDTH: 254px; TEXT-ALIGN: left"><asp:DropDownList id="dd_branch" runat="server" CssClass="inpText" __designer:wfdid="w67">
                                                   </asp:DropDownList> </TD></TR><TR style="COLOR: #000099"><TD style="FONT-SIZE: x-small; WIDTH: 20%; TEXT-ALIGN: left">Status</TD><TD style="WIDTH: 254px; TEXT-ALIGN: left"><asp:DropDownList id="acctgflag" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem Value="A">Active</asp:ListItem>
<asp:ListItem Value="I">Inactive</asp:ListItem>
<asp:ListItem Enabled="False" Value="P">Passive</asp:ListItem>
<asp:ListItem Enabled="False" Value="S">Suspended</asp:ListItem>
</asp:DropDownList></TD></TR><TR style="COLOR: #000099"><TD style="FONT-SIZE: x-small; COLOR: gray; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblupd" runat="server" __designer:wfdid="w60"></asp:Label> &nbsp;On <asp:Label id="UpdTime" runat="server" Font-Bold="True" ForeColor="Gray" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w61"></asp:Label> &nbsp;by <asp:Label id="UpdUser" runat="server" Font-Bold="True" ForeColor="Gray" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w62"></asp:Label> </TD></TR><TR style="COLOR: #000099"><TD style="FONT-SIZE: x-small; COLOR: gray; TEXT-ALIGN: left" colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w63">
                                                   </asp:ImageButton> &nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w64">
                                                   </asp:ImageButton> &nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w65">
                                                   </asp:ImageButton> </TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=2><asp:Label id="lblKonfirmasi" runat="server" CssClass="Important" Font-Bold="False" __designer:wfdid="w66"></asp:Label> </TD></TR><TR><TD align=center colSpan=2><ajaxToolkit:FilteredTextBoxExtender id="fte1" runat="server" __designer:wfdid="w59" FilterType="Numbers" TargetControlID="Acctgcode">
                                                   </ajaxToolkit:FilteredTextBoxExtender> <asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w2" DisplayAfter="250" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w3"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><asp:TextBox id="AcctgGrp1" runat="server" Width="24px" CssClass="inpText" MaxLength="50" __designer:wfdid="w4" Visible="False" ReadOnly="True" BorderStyle="Solid"></asp:TextBox><asp:TextBox id="AcctgGrp3" runat="server" Width="16px" CssClass="inpText" MaxLength="50" __designer:wfdid="w5" Visible="False"></asp:TextBox><asp:DropDownList id="ddlLevelCOA" runat="server" Width="68px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w6" Visible="False" OnSelectedIndexChanged="ddlLevelCOA_SelectedIndexChanged" AutoPostBack="True">
                                                   </asp:DropDownList><asp:TextBox id="AcctgDesc" runat="server" Width="2px" Height="2px" CssClass="inpText" MaxLength="50" __designer:wfdid="w8" Visible="False"></asp:TextBox><asp:TextBox id="AcctgGrp2" runat="server" Width="2px" Height="2px" CssClass="inpText" MaxLength="50" __designer:wfdid="w7" Visible="False"></asp:TextBox></TD></TR></TBODY></TABLE>&nbsp; 
</contenttemplate>
                           </asp:UpdatePanel>
                       </ContentTemplate>
                       <HeaderTemplate>
                           <img align="absMiddle" alt="" src="../Images/corner.gif" />
                           <strong><span style="font-size: 9pt">
                               Form Chart Of Accounts</span> <span style="font-size: 9pt">:.</span></strong>
                       </HeaderTemplate>
                   </ajaxToolkit:TabPanel>
               </ajaxToolkit:TabContainer>
                </td>
        </tr>
    </table>
    &nbsp;&nbsp;&nbsp; &nbsp;
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:UpdatePanel id="UpdPanelPrint" runat="server"><ContentTemplate>
<asp:Panel id="pnlPrint" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Chart Of Account"></asp:Label></TD></TR><TR><TD style="HEIGHT: 12px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="printType" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="orderIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label id="orderNoForReport" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 23px; TEXT-ALIGN: center" align=left colSpan=2><asp:ImageButton id="imbPrintPDF" onclick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrintExcel" onclick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbCancelPrint" onclick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblError" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 20px" align=left></TD><TD style="HEIGHT: 20px" align=left></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePrint" runat="server" Drag="True" PopupControlID="pnlPrint" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPrint" TargetControlID="btnHidePrint"></ajaxToolkit:ModalPopupExtender><asp:Button id="btnHidePrint" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> <asp:Panel id="PanelConsignee" runat="server" CssClass="modalBox" Visible="False" EnableTheming="True"><TABLE><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px" align=left colSpan=1></TD><TD style="VERTICAL-ALIGN: middle; HEIGHT: 25px; TEXT-ALIGN: center" align=left colSpan=4><asp:Label id="Label14" runat="server" Font-Bold="True" Text="Consignee"></asp:Label></TD><TD style="WIDTH: 30px" align=left colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px" align=left colSpan=1></TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:Label id="lblconsstate" runat="server" Visible="False"></asp:Label> <asp:Label id="consigneeoid" runat="server" Text="0" Visible="False"></asp:Label></TD><TD style="WIDTH: 30px" align=left colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left>Code <asp:Label id="Label50" runat="server" Width="12px" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="conscode" runat="server" CssClass="inpTextDisabled" MaxLength="10" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 30px" align=left colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Nama <asp:Label id="Label15" runat="server" Width="12px" CssClass="Important" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="consname" runat="server" Width="200px" CssClass="inpText" MaxLength="100"></asp:TextBox></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Alamat <asp:Label id="Label16" runat="server" Width="12px" CssClass="Important" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="consaddress" runat="server" Width="200px" Height="48px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox><BR /><asp:Label id="Label40" runat="server" ForeColor="Red" Text="maks. 100 karakter"></asp:Label></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Provinsi</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:DropDownList id="consprovince" runat="server" Width="130px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Kota</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:DropDownList id="conscity" runat="server" Width="130px" CssClass="inpText"></asp:DropDownList></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Kode Pos</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="conspostcode" runat="server" CssClass="inpText" MaxLength="10"></asp:TextBox></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Telepon</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="consphone" runat="server" Width="125px" CssClass="inpText" MaxLength="25"></asp:TextBox></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Fax</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="consfax" runat="server" Width="125px" CssClass="inpText" MaxLength="25"></asp:TextBox></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">NPWP</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="consnpwp" runat="server" Width="125px" CssClass="inpText" MaxLength="25"></asp:TextBox></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Contact Person</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:DropDownList id="conscpprefix" runat="server" Width="72px" CssClass="inpText"></asp:DropDownList> <asp:TextBox id="conscpname" runat="server" Width="200px" CssClass="inpText" MaxLength="25"></asp:TextBox></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap">Telepon Contact Person</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="conscpphone" runat="server" Width="125px" CssClass="inpText" MaxLength="25"></asp:TextBox></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top">Catatan</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="consnote" runat="server" Width="200px" Height="48px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox><BR /><asp:Label id="Label17" runat="server" ForeColor="Red" Text="maks. 50 karakter"></asp:Label></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px"></TD><TD colSpan=2></TD><TD style="WIDTH: 30px" colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px" align=center colSpan=1></TD><TD style="VERTICAL-ALIGN: middle; HEIGHT: 25px; TEXT-ALIGN: center" align=center colSpan=4><asp:LinkButton style="MARGIN-RIGHT: 20px" id="lbaddconsignee" runat="server" Font-Bold="True">[ Add To List ]</asp:LinkButton><asp:LinkButton id="lbcancelconsignee" runat="server" Font-Bold="True">[ Cancel & Close ]</asp:LinkButton></TD><TD style="WIDTH: 30px" align=center colSpan=1></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 30px" align=left colSpan=1></TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><ajaxToolkit:FilteredTextBoxExtender id="ftecons1" runat="server" TargetControlID="conspostcode" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftecons2" runat="server" TargetControlID="consphone" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftecons3" runat="server" TargetControlID="consfax" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftecons4" runat="server" TargetControlID="consnpwp" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftecons5" runat="server" TargetControlID="conscpphone" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender></TD><TD style="WIDTH: 30px" align=left colSpan=1></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEConsignee" runat="server" Drag="True" PopupControlID="PanelConsignee" BackgroundCssClass="modalBackground" TargetControlID="btnConsignee" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnConsignee" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    <br />
</asp:Content>