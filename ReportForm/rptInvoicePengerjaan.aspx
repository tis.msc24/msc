<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptInvoicePengerjaan.aspx.vb" Inherits="ReportForm_rptInvoicePengerjaan" title="MSC" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 984px; height: 40px">
        <tr>
            <td align="left" style="height: 45px; background-color: silver">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="Maroon"
                    Text=":: Laporan Invoice Pengerjaan Teknisi"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 616px; HEIGHT: 32px"><TBODY><TR><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DDLcabang" runat="server" CssClass="inpText" __designer:wfdid="w4"></asp:DropDownList></TD></TR><TR><TD>Report</TD><TD>:</TD><TD><asp:DropDownList id="DDLsummary" runat="server" Width="113px" CssClass="inpText" __designer:wfdid="w57"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Label id="lblType" runat="server" __designer:wfdid="w87" Visible="False"></asp:Label>&nbsp;<asp:Label id="lblError" runat="server" __designer:wfdid="w88" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 22px">Filter</TD><TD style="HEIGHT: 22px">:</TD><TD style="HEIGHT: 22px"><asp:DropDownList id="FilterDDL" runat="server" Width="113px" CssClass="inpText" __designer:wfdid="w58"><asp:ListItem Value="PERSONNAME">Teknisi</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" __designer:wfdid="w59"></asp:TextBox>&nbsp;</TD></TR><TR><TD style="HEIGHT: 17px">Periode</TD><TD style="HEIGHT: 17px">:</TD><TD><asp:TextBox id="tgl1" runat="server" CssClass="inpText" __designer:wfdid="w60"></asp:TextBox>&nbsp;<asp:ImageButton id="cal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w61"></asp:ImageButton> to <asp:TextBox id="tgl2" runat="server" CssClass="inpText" __designer:wfdid="w62"></asp:TextBox>&nbsp;<asp:ImageButton id="cal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w63"></asp:ImageButton> <asp:Label id="Label2" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w64"></asp:Label></TD></TR><TR><TD style="HEIGHT: 24px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w65" Visible="False"></asp:CheckBox></TD><TD style="HEIGHT: 24px"></TD><TD style="HEIGHT: 24px"><asp:DropDownList id="DDLStatus" runat="server" CssClass="inpText" __designer:wfdid="w66" Visible="False"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>Final</asp:ListItem>
<asp:ListItem>Close</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 22px" colSpan=3><asp:ImageButton id="btnView" onclick="btnView_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w67"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPDF" onclick="btnPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w68"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnExcel" onclick="btnExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w69"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w70"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 22px"></TD><TD></TD><TD style="HEIGHT: 22px"></TD></TR><TR><TD style="HEIGHT: 22px" colSpan=3><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><CR:CrystalReportViewer id="crvOper" runat="server" __designer:wfdid="w71" AutoDataBind="true" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer></TD></TR><TR><TD style="HEIGHT: 22px" colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w72" Format="dd/MM/yyyy" PopupButtonID="cal1" TargetControlID="tgl1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w73" Format="dd/MM/yyyy" PopupButtonID="cal2" TargetControlID="tgl2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w74" TargetControlID="tgl1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w75" TargetControlID="tgl2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnView"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnCancel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel><br />
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<asp:Panel id="panelMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE style="WIDTH: 99%; HEIGHT: 109%"><TR><TD style="HEIGHT: 18px; BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsg" runat="server" TargetControlID="btnMsg" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" DropShadow="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>&nbsp;&nbsp; <asp:Button id="btnMsg" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel3" runat="server"><contenttemplate>
<asp:Panel id="PanelPass" runat="server" CssClass="modalBox" Visible="False" DefaultButton="btnOKPass"><TABLE width=300><TR><TD align=center colSpan=3><asp:Label id="Label17" runat="server" Font-Size="Medium" Font-Bold="True" Text="Masukkan Password"></asp:Label></TD></TR></TABLE><TABLE width=300><TR><TD style="WIDTH: 80px">&nbsp;</TD><TD style="WIDTH: 9px"></TD><TD></TD></TR><TR><TD style="WIDTH: 80px"><asp:Label id="Label13" runat="server" Text="Password"></asp:Label> <asp:Label id="Label16" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label14" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtPass" runat="server" Width="150px" CssClass="inpText" TextMode="Password"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 80px"></TD><TD style="WIDTH: 9px"></TD><TD></TD></TR></TABLE><TABLE width=300><TR><TD></TD><TD></TD><TD></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnOKPass" runat="server" ImageUrl="~/Images/ok.png" OnClick="btnOKPass_Click"></asp:ImageButton> <asp:ImageButton id="btnCancelPass" runat="server" ImageUrl="~/Images/Cancel.png" OnClick="btnCancelPass_Click"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <asp:Button id="btnPass" runat="server" Visible="False"></asp:Button><BR /><ajaxToolkit:ModalPopupExtender id="MPEPass" runat="server" TargetControlID="btnPass" BackgroundCssClass="modalBackground" PopupControlID="PanelPass" PopupDragHandleControlID="Label17" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnOKPass"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

