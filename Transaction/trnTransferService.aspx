<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTransferService.aspx.vb" Inherits="trnTransferService" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: TW SERVICE CUSTOMER" CssClass="Title" ForeColor="Navy" Font-Names="Verdana" Font-Size="21px" Width="320px"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
               <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                 <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Transfer Service :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 90px; WHITE-SPACE: nowrap" class="Label" align=left>Transfer No </TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w26" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" id="TD9" align=left Visible="true"><asp:DropDownList id="DdlFCabang" runat="server" CssClass="inpText" __designer:wfdid="w27" AutoPostBack="True"><asp:ListItem Value="FromBranch">Pengirim</asp:ListItem>
<asp:ListItem Value="ToBranch">Penerima</asp:ListItem>
</asp:DropDownList></TD><TD align=left Visible="true">:</TD><TD id="TD8" align=left colSpan=4 Visible="true"><asp:DropDownList id="drCabang" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w28"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 90px; WHITE-SPACE: nowrap" align=left><asp:CheckBox id="CbTanggal" runat="server" Text="Tanggal" __designer:wfdid="w29"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w30"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w32"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w34"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 90px; WHITE-SPACE: nowrap" align=left>Status</TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" align=left>: </TD><TD align=left colSpan=4><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" __designer:wfdid="w35"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w36">
                                                            </asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w37">
                                                            </asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w38" DefaultButton="btnSearch"><asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w19" OnSelectedIndexChanged="gvMaster_SelectedIndexChanged" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trfwhserviceoid" DataNavigateUrlFormatString="trnTransferService.aspx?oid={0}" DataTextField="trfwhserviceNo" HeaderText="Transfer No">
<ControlStyle ForeColor="Red"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" ForeColor="Red"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trfwhservicedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FromBranch" HeaderText="Pengirim">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tobranch" HeaderText="Penerima">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trfwhservicestatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" Text='<%# Eval("trfwhserviceoid") %>' __designer:wfdid="w46"></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
&nbsp;<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" __designer:wfdid="w47" ToolTip='<%# eval("trfwhserviceoid") %>'>Print Out</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w40" TargetControlID="tgl1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w41" TargetControlID="tgl2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w42" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1" TargetControlID="tgl1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w43" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2" TargetControlID="tgl2"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left>Transfer No.</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="200px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" Enabled="False" __designer:wfdid="w82" MaxLength="20"></asp:TextBox>&nbsp;<asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" __designer:wfdid="w83" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Date</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" Enabled="False" __designer:wfdid="w84" ValidationGroup="MKE"></asp:TextBox> <asp:ImageButton id="imbTwDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w85"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w86"></asp:Label></TD><TD style="vertical-alin: top" class="Label" align=left><asp:Label id="labelnotr" runat="server" Width="75px" Text="No. Ref" __designer:wfdid="w87"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left><asp:TextBox id="noref" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w88" MaxLength="20"></asp:TextBox>&nbsp;&nbsp; <asp:Label id="pooid" runat="server" __designer:wfdid="w89" Visible="False"></asp:Label> <asp:Label id="FromLoc" runat="server" __designer:wfdid="w90" Visible="False"></asp:Label> </TD></TR><TR><TD class="Label" align=left>From Branch</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="FromBranch" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w91" OnSelectedIndexChanged="FromBranch_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD style="vertical-alin: top" class="Label" align=left>To Branch</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="ToBranch" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w92" OnSelectedIndexChanged="ToBranch_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left>From Location</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="fromlocation" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w93" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>To Location</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:DropDownList id="tolocation" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w94" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="note" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w95" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="trnstatus" runat="server" Width="152px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w96" MaxLength="20">In Process</asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left>&nbsp;</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVpo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w99" Visible="False" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="ToMtrlocOid,toMtrBranch,fromMtrBranch,FromMtrlocoid,trfmtrnote" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnTrfToReturNo" HeaderText="Transfer No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntrfdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfmtrnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Penerimaan Detail :" __designer:wfdid="w100" Font-Underline="True"></asp:Label><asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" __designer:wfdid="w101" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>No. Penerimaan</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="txtPenerimaan" runat="server" Width="200px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w110"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSeachPenerimaan" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w111"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearPenerimaan" onclick="imbClearPenerimaan_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w112"></asp:ImageButton><asp:Label id="penerimaanoid" runat="server" __designer:wfdid="w113" Visible="False"></asp:Label><asp:Label id="sequence" runat="server" __designer:wfdid="w114" Visible="False"></asp:Label><asp:Label id="reqoid" runat="server" __designer:wfdid="w115" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=right colSpan=6><asp:ImageButton id="imbAddToListPenerimaan" onclick="imbAddToListPenerimaan_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w116" Visible="False"></asp:ImageButton><asp:ImageButton id="imbClearPenerimaanDetail" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w117" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label4" runat="server" Font-Bold="True" Text="Item Detail :" __designer:wfdid="w118" Font-Underline="True"></asp:Label><asp:Label id="labeltempsisaretur" runat="server" __designer:wfdid="w45" Visible="False"></asp:Label><asp:Label id="labelsatuan1" runat="server" __designer:wfdid="w46" Visible="False"></asp:Label><asp:Label id="labelsatuan" runat="server" __designer:wfdid="w47" Visible="False"></asp:Label><asp:Label id="trfmtrmstoid" runat="server" __designer:wfdid="w48" Visible="False"></asp:Label><asp:Label id="labelitemoid" runat="server" __designer:wfdid="w124" Visible="False"></asp:Label><asp:Label id="reqdtloid" runat="server" __designer:wfdid="w125" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label25" runat="server" Text="Item" __designer:wfdid="w119"></asp:Label> <asp:Label id="Label26" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w120" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="354px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w121"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w122"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w123"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left>Qty</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="qty" runat="server" CssClass="inpText" __designer:wfdid="w137"></asp:TextBox>&nbsp;<asp:Label id="reqqty" runat="server" __designer:wfdid="w138"></asp:Label></TD><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=right></TD></TR><TR><TD class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" __designer:wfdid="w139" MaxLength="150"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="labelseq" runat="server" __designer:wfdid="w140" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=right><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w141"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w142"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 184px; BACKGROUND-COLOR: beige"><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w143" DataKeyNames="reqoid,itemoid,itemdesc,reqdtljob,reqqty,qty,reqdtloid,reqcode" CellPadding="4" AutoGenerateColumns="False" GridLines="None" OnRowDeleting="GVItemDetail_RowDeleting">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="reqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqcode" HeaderText="No. Penerimaan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtljob" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server" Visible="False" __designer:wfdid="w8"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6>Created By&nbsp;<asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w144"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w145"></asp:Label> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w146"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w147"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w149"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w150"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w151" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsBottom" __designer:wfdid="w152" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="1688849860263976" __designer:wfdid="w155" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="1688849860263977">
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w157"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" __designer:wfdid="w148" TargetControlID="qty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:MaskedEditExtender id="TwDate" runat="server" __designer:wfdid="w2" TargetControlID="transferdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceTwDate" runat="server" __designer:wfdid="w1" TargetControlID="transferdate" Format="dd/MM/yyyy" PopupButtonID="imbTwDate"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Transfer Service :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel4" runat="server">
        <contenttemplate>
<asp:Panel id="panelPenerimaan" runat="server" CssClass="modalBox" __designer:wfdid="w1" Visible="False" DefaultButton="imbFindPenerimaan"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptPenerimaan" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Penerimaan" __designer:wfdid="w2"></asp:Label></TD></TR><TR><TD align=center>Filter : <asp:DropDownList id="DDLFilterPenerimaan" runat="server" CssClass="inpText" __designer:wfdid="w3"><asp:ListItem Value="reqcode">No. Penerimaan</asp:ListItem>
<asp:ListItem Enabled="False" Value="custname">Customer</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterPenerimaan" runat="server" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox> <asp:ImageButton id="imbFindPenerimaan" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton> <asp:ImageButton id="imbViewPenerimaan" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvPenerimaan" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w7" Visible="False" OnSelectedIndexChanged="gvPenerimaan_SelectedIndexChanged" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="reqoid,reqcode,custoid,custname,reqdate" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="reqcode" HeaderText="No. Penerimaan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" HeaderText="custoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center><asp:LinkButton id="lkbClosePenerimaan" runat="server" __designer:wfdid="w8">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePenerimaan" runat="server" __designer:wfdid="w9" TargetControlID="btnHidePenerimaan" PopupControlID="panelPenerimaan" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptPenerimaan"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePenerimaan" runat="server" __designer:wfdid="w10" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel8" runat="server">
        <contenttemplate>
<asp:Panel id="panelItem" runat="server" CssClass="modalBox" Visible="False" DefaultButton="imbFindItem"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptItem" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Barang"></asp:Label></TD></TR><TR><TD align=center>Filter : <asp:DropDownList id="DDLFilterItem" runat="server" CssClass="inpText"><asp:ListItem Value="i.itemdesc">Deskripsi</asp:ListItem>
<asp:ListItem Value="i.Itemcode">Itemcode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterItem" runat="server" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="imbFindItem" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbViewItem" onclick="imbViewItem_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w1" Visible="False" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="itemoid,itemcode,itemdesc,reqdtljob,reqqty,reqdtloid,OsQty" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="OsQty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtljob" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="450px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center><asp:LinkButton id="lkbCloseItem" runat="server">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" TargetControlID="btnHideItem" PopupControlID="panelItem" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptItem"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideItem" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w12" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w13"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w14"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w15"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w17" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w18" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

