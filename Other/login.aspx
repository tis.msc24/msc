<%@ Page Language="VB" MasterPageFile="~\MasterDefault.master" AutoEventWireup="false" 
CodeFile="login.aspx.vb" Inherits="login" title="Multi Sarana Computer - Login"%>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">  
    <table id="tableutama" align="center" border="0" cellpadding="0" cellspacing="0"
        width="979">
        <tr>
            <td style="height: 52px; width: 200px;" valign="top">
                <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="200">
                    <tr>
                        <th align="left" class="header" valign="center" style="width: 200px;">
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; :: Login</th>
                    </tr>
                    <tr>
                        <td style="width: 200px; text-align: left;">
                            <asp:Panel ID="pnlLogin" runat="server" DefaultButton="imbLogin" Width="100%">
                                <br />
                            <table align="center" border="0" width="192">
                                <tr>
                                    <td align="left" style="text-align: right">
                                        &nbsp;&nbsp; User ID&nbsp;
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:TextBox id="txtInputuserid" runat="server" maxlength="20" Width="100px" BorderColor="Silver" CssClass="inpText"></asp:TextBox><asp:RequiredFieldValidator ID="rv_userid" runat="server" ControlToValidate="txtInputuserid"
                                            Display="None" ErrorMessage="* Please enter User ID"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="text-align: right">
                                        &nbsp;&nbsp; Password&nbsp;
                                    </td>
                                    <td style="text-align: left;">
                                                    <asp:TextBox id="txtInputPassword" runat="server" maxlength="20" TextMode="Password" Width="100px" BorderColor="Silver" CssClass="inpText"></asp:TextBox><asp:RequiredFieldValidator ID="rv_password" runat="server" ControlToValidate="txtInputPassword"
                                            Display="None" ErrorMessage="* Please enter Password"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr align="middle">
                                    <td colspan="2" style="text-align: center">
                                        <ajaxToolkit:ValidatorCalloutExtender ID="vce_password" runat="server" TargetControlID="rv_password">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="vce_userid" runat="server" TargetControlID="rv_userid">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                        <asp:SqlDataSource ID="SDSHistoryLog" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" DeleteCommand="DELETE FROM [QL_HISTORYLOG] WHERE [CMPCODE] = @CMPCODE AND [HISTORYOID] = @HISTORYOID" InsertCommand="INSERT INTO [QL_HISTORYLOG] ([CMPCODE], [HISTORYOID], [USERID], [ACTIVITY], [TABLENAME], [OID], [ACTIVITYDATE], [NOTE]) VALUES (@CMPCODE, @HISTORYOID, @USERID, @ACTIVITY, @TABLENAME, @OID, @ACTIVITYDATE, @NOTE)" SelectCommand="SELECT [CMPCODE], [HISTORYOID], [USERID], [ACTIVITY], [TABLENAME], [OID], [ACTIVITYDATE], [NOTE] FROM [QL_HISTORYLOG]" UpdateCommand="UPDATE [QL_HISTORYLOG] SET [USERID] = @USERID, [ACTIVITY] = @ACTIVITY, [TABLENAME] = @TABLENAME, [OID] = @OID, [ACTIVITYDATE] = @ACTIVITYDATE, [NOTE] = @NOTE WHERE [CMPCODE] = @CMPCODE AND [HISTORYOID] = @HISTORYOID">
                                            <DeleteParameters>
                                                <asp:Parameter Name="CMPCODE" Type="String" />
                                                <asp:Parameter Name="HISTORYOID" Type="Int32" />
                                            </DeleteParameters>
                                            <UpdateParameters>
                                                <asp:Parameter Name="USERID" Type="String" />
                                                <asp:Parameter Name="ACTIVITY" Type="String" />
                                                <asp:Parameter Name="TABLENAME" Type="String" />
                                                <asp:Parameter Name="OID" Type="Int32" />
                                                <asp:Parameter Name="ACTIVITYDATE" Type="DateTime" />
                                                <asp:Parameter Name="NOTE" Type="String" />
                                                <asp:Parameter Name="CMPCODE" Type="String" />
                                                <asp:Parameter Name="HISTORYOID" Type="Int32" />
                                            </UpdateParameters>
                                            <InsertParameters>
                                                <asp:Parameter Name="CMPCODE" Type="String" />
                                                <asp:Parameter Name="HISTORYOID" Type="Int32" />
                                                <asp:Parameter Name="USERID" Type="String" />
                                                <asp:Parameter Name="ACTIVITY" Type="String" />
                                                <asp:Parameter Name="TABLENAME" Type="String" />
                                                <asp:Parameter Name="OID" Type="Int32" />
                                                <asp:Parameter Name="ACTIVITYDATE" Type="DateTime" />
                                                <asp:Parameter Name="NOTE" Type="String" />
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                                        <asp:SqlDataSource ID="SDSOid" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                                            SelectCommand="SELECT [cmpcode], [tablename], [lastoid], [tablegroup] FROM [QL_mstoid] WHERE (([tablename] = @tablename) AND ([cmpcode] = @cmpcode))"
                                            UpdateCommand="UPDATE QL_mstoid SET lastoid = @lastoid WHERE (tablename = @tablename) AND (cmpcode = @cmpcode)">
                                            <SelectParameters>
                                                <asp:Parameter DefaultValue="" Name="tablename" Type="String" />
                                                <asp:Parameter DefaultValue="" Name="cmpcode" Type="String" />
                                            </SelectParameters>
                                            <UpdateParameters>
                                                <asp:Parameter Name="lastoid" />
                                                <asp:Parameter Name="tablename" />
                                                <asp:Parameter Name="cmpcode" />
                                            </UpdateParameters>
                                        </asp:SqlDataSource>
                                        <br />
                                        <asp:ImageButton ID="imbLogin" runat="server" ImageUrl="~/Images/login.png" ImageAlign="AbsBottom" /><br />
                                        <asp:Label id="lblMessage" runat="server"  ForeColor="red" font-size="8" font-names="Arial, Helvetica, sans-serif"></asp:Label></td>
                                </tr>
                            </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" width="9px">
                <br />
                &nbsp;
                <br />
            </td>
            <td valign="top">
                <table id="Table1" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="100%">
                <tr>
                    <th align="left" class="header" valign="center" style="width: 276px">
                        <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; :: Company Profile</th>
                </tr>
                <tr>
                    <td style="width: 100%; text-align: center; vertical-align:top ">
                        <table width="100%">
                            <tr>
                                <td style="text-align: right; vertical-align:top " colspan="2">
                                    <strong><span style="color: #373744; font-size: 14pt;">MULTI SARANA COMPUTER</span></strong></td>
                            </tr>
                            <tr>
                            </tr>
                               <tr><td>
<div align=justify><p style="color: #373744"><font size="2"><strong><font color="#0095da">THE COMPANY</font></strong><br />
</font><br />
    Company Profile here..<br>
</p>
    <p>
        &nbsp;</p>
</div>
</td></tr>
                           
                            <tr>
                                <td colspan="2" style="vertical-align: top; text-align: center">
                                    <img src="../Images/line.gif"></td>
                            </tr>
                            <tr>
                                <%--<td style="VERTICAL-ALIGN: top; WIDTH: 345px; TEXT-ALIGN: left">
                                    <span style="color: #373744">
                                    Australia&nbsp;
                                    <br />
                                    </span><span style="color: #373744">
                                    HSL Trading Pty Ltd 
                                        <br />
                                        1005/620 Collins St, Melbourne VIC 3000 - Australia 
                                    <br />
                                    </span><span style="color: #373744">
                                    Contact
                                                Person : Alwin Lim 
                                    <br />
                                    </span><span style="color: #373744">
                                    Mobile : +61 403 596 311 (Direct) 
                                    <br />
                                    </span><span style="color: #373744">
                                    Phone : +61 402 508 123, +61
                                                3 9614 1197 
                                    <br />
                                    </span><span style="color: #373744">
                                    Fax : +61 3 9614 1197
                                    <br />
                                    </span><span style="color: #373744">
                                    By E-mail : australia@sdcan.com</span></td>--%>
                                <td style="vertical-align: top; width: 345px; text-align: left; height: 148px;">
                                    <span style="color: #373744">
                                    <span style="color: #373744">
                                        <br />
                                        <br />
                                        <strong>OFFICE / FACTORY :</strong>&nbsp;&nbsp;<br />
                                        <br />
                                        Phone :&nbsp;<br />
                                    Fax :&nbsp;<br />
                                        <br />
                                        Website : -<br />
                                        Email :&nbsp;<br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; <br />
                                    </span>
                                    </span></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; width: 345px; height: 148px; text-align: left">
                                </td>
                            </tr>
                        </table>
                        <asp:UpdatePanel id="upMsgbox" runat="server">
                            <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="100%" DefaultButton="btnMsgBoxOK" CssClass="modalMsgBoxWarn" Visible="False"><TABLE cellSpacing=1 cellPadding=1 width="100%" border=0><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" BackColor="Yellow" Height="25px"><asp:Label id="lblCaption" runat="server" ForeColor="Black" Font-Size="Medium" Font-Bold="True"></asp:Label></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD style="WIDTH: 444px" vAlign=top align=left><asp:Label id="lblMessages" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Visible="False" Font-Size="X-Small"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 25px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/ok.png"></asp:ImageButton> </TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="130px" Visible="False" CausesValidation="False">
            </asp:Button> 
</contenttemplate>
                        </asp:UpdatePanel>
                                 
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
   
</asp:Content>