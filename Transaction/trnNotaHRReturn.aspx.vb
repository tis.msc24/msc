Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports ClassProcedure
Imports Koneksi
Imports System.Globalization
Imports System.IO
Imports System.Drawing

Partial Class Transaction_trnNotaHRReturn
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As ReportDocument
    Dim crate As ClassRate
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(tbperiodstart.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 tidak sesuai. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(tbperiodend.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 tidak sesuai. " & sErr & "", 2)
            Return False
        End If
        If CDate(tbperiodstart.Text) > CDate(tbperiodend.Text) Then
            showMessage("Period 2 harus lebih dari Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If trnbelimstoid.Text = "" Then
            sError &= "- Silahkan pilih Nota PI terlebih dahulu!<BR>"
        End If
        If returamt.Text = "" Then
            sError &= "- silahkan isi Retur Amt!<BR>"
        Else
            If ToDouble(returamt.Text) = 0 Then
                sError &= "- AMOUNT harus lebih dari 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("trnnotahrreturdtlamt", "QL_trnnotahrreturdtl", ToDouble(returamt.Text), sErrReply) Then
                    sError &= "- AMOUNT must be less than MAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If notedtl.Text.Length > 200 Then
            sError &= "- Note tidak boleh lebih dari 200 karakter!<BR>"
        End If
        If DDLtype.SelectedValue <> "HR" Then
            If ToDouble(returamt.Text) > ToDouble(lblmaxdtl2.Text) Then
                sError &= "- Amount retur tidak boleh lebih dari amount PI!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        'Dim sError As String = ""
        'Dim sErr As String = ""
        'If DDLBusUnit.SelectedValue = "" Then
        '    sError &= "- Please select BUSINESS UNIT field!<BR>"
        'End If
        'If cashbankdate.Text = "" Then
        '    sError &= "- Please fill EXPENSE DATE field!<BR>"
        'Else
        '    If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
        '        sError &= "- EXPENSE DATE is invalid. " & sErr & "<BR>"
        '    End If
        'End If
        'If curroid.SelectedValue = "" Then
        '    sError &= "- Please select CURRENCY field!<BR>"
        'End If
        'Dim sErrReply As String = ""
        'If Not isLengthAccepted("cashbankdpp", "QL_trncashbankmst", ToDouble(cashbankamt.Text), sErrReply) Then
        '    sError &= "- TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
        'End If
        'If acctgoid.SelectedValue = "" Then
        '    sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        'End If
        'If cashbanktype.SelectedValue = "BBK" Or cashbanktype.SelectedValue = "BGK" Then
        '    If cashbanktype.SelectedValue = "BBK" Then
        '        If cashbankrefno.Text = "" Then
        '            sError &= "- Please fill REF. NO. field!<BR>"
        '        End If
        '    End If
        '    If cashbanktype.SelectedValue = "BGK" Then
        '        If DDLSuppAccount.SelectedValue = "" Then
        '            sError &= "- Please fill SUPPLIER ACCOUNT field!<BR>"
        '        End If
        '    End If
        '    If cashbankduedate.Text = "" Then
        '        sError &= "- Please fill DUE DATE field!<BR>"
        '    Else
        '        If Not IsValidDate(cashbankduedate.Text, "MM/dd/yyyy", sErr) Then
        '            sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
        '        Else
        '            If CDate(cashbankdate.Text) > CDate(cashbankduedate.Text) Then
        '                sError &= "- DUE DATE must be more or equal than EXPENSE DATE<BR>"
        '            End If
        '        End If
        '    End If
        'ElseIf cashbanktype.SelectedValue = "BLK" Then
        '    If giroacctgoid.Text = "" Then
        '        sError &= "- Please select DP NO. field!<BR>"
        '    End If
        'End If
        'If cashbanktype.SelectedValue = "BGK" Then
        '    If cashbanktakegiro.Text = "" Then
        '        sError &= "- Please fill DATE TAKE GIRO field!<BR>"
        '    Else
        '        If Not IsValidDate(cashbanktakegiro.Text, "MM/dd/yyyy", sErr) Then
        '            sError &= "- DATE TAKE GIRO is invalid. " & sErr & "<BR>"
        '        Else
        '            If cashbankduedate.Text <> "" Then
        '                If CDate(cashbanktakegiro.Text) > CDate(cashbankduedate.Text) Then
        '                    sError &= "- DATE TAKE GIRO must be less or equal than DUE DATE<BR>"
        '                End If
        '            End If
        '            If cashbankdate.Text <> "" Then
        '                If CDate(cashbankdate.Text) > CDate(cashbanktakegiro.Text) Then
        '                    sError &= "- DATE TAKE GIRO must be more or equal than EXPENSE DATE<BR>"
        '                End If
        '            End If
        '        End If
        '    End If
        '    If suppoid.Text = "" Then
        '        sError &= "- Please select SUPPLIER field!<BR>"
        '    End If
        'End If
        'If personoid.SelectedValue = "" Then
        '    sError &= "- Please select PIC field!<BR>"
        'End If
        'If Not isLengthAccepted("cashbanktaxamt", "QL_trncashbankmst", ToDouble(cashbanktaxamt.Text), sErrReply) Then
        '    sError &= "- TAX AMOUNT must be less than MAX TAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
        'End If
        'If cashbankothertaxamt.Text <> "" Then
        '    If Not isLengthAccepted("cashbankothertaxamt", "QL_trncashbankmst", ToDouble(cashbankothertaxamt.Text), sErrReply) Then
        '        sError &= "- OTHER TAX AMOUNT must be less than MAX OTHER TAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
        '    End If
        'End If
        'If ToDouble(cashbankgrandtotal.Text) < 0 Then
        '    sError &= "- GRAND TOTAL must be more than 0!<BR>"
        'Else
        '    If cashbanktype.SelectedValue = "BLK" Then
        '        If ToDouble(cashbankgrandtotal.Text) > ToDouble(dpapamt.Text) Then
        '            sError &= "- GRAND TOTAL must be less than DP AMOUNT!<BR>"
        '        Else
        '            sSql = "SELECT dpapamt - ISNULL(dp.dpapaccumamt, 0.0) FROM QL_trndpap dp WHERE dp.cmpcode='" & DDLBusUnit.SelectedValue & "' AND dp.dpapoid=" & ToInteger(giroacctgoid.Text)
        '            Dim dNewDPBalance As Double = ToDouble(GetStrData(sSql))
        '            If Session("oid") IsNot Nothing And Session("oid") <> "" Then
        '                dNewDPBalance += ToDouble(cashbankgrandtotal.Text)
        '            End If
        '            If ToDouble(dpapamt.Text) <> dNewDPBalance Then
        '                dpapamt.Text = ToMaskEdit(dNewDPBalance, 4)
        '            End If
        '            If ToDouble(cashbankgrandtotal.Text) > ToDouble(dpapamt.Text) Then
        '                sError &= "- DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!<BR>"
        '            End If
        '        End If
        '    End If
        'End If
        'If Not isLengthAccepted("cashbankamt", "QL_trncashbankmst", ToDouble(cashbankgrandtotal.Text), sErrReply) Then
        '    sError &= "- GRAND TOTAL must be less than MAX GRAND TOTAL (" & sErrReply & ") allowed stored in database!<BR>"
        'End If
        'If Session("TblDtl") Is Nothing Then
        '    sError &= "- Please fill Detail Data!"
        'Else
        '    Dim dtTbl As DataTable = Session("TblDtl")
        '    If dtTbl.Rows.Count <= 0 Then
        '        sError &= "- Please fill Detail Data!"
        '    End If
        'End If
        'If sError <> "" Then
        '    showMessage(sError, 2)
        '    cashbankstatus.Text = "In Process"
        '    Return False
        'End If
        'Return True
    End Function

#End Region

#Region "Procedure"
    Private Function setTabelSN() As DataTable
        Dim dtSN As New DataTable
        dtSN.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSN.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSN.Columns.Add("SN", Type.GetType("System.String"))
        Return dtSN
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub BindAllData(ByVal sWhere As String)

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sWhere &= " AND hr.crtuser='" & Session("UserID") & "'"
        End If

        If FCabangNya.SelectedValue <> "ALL" Then
            sWhere &= " AND hr.branch_code='" & FCabangNya.SelectedValue & "'"
        End If
        sSql = "Select hr.branch_code,hr.trnnotahrreturoid, (CASE WHEN hr.trnnotahrreturstatus = 'Approved' THEN hr.trnnotahrreturno ELSE CAST(hr.trnnotahrreturoid as varchar(5)) END) trnnotahrreturno,hr.trnnotahrreturtype AS trnnotahrtype, hr.trnnotahrreturdate, s.suppname, hr.trnnotahrreturstatus,cb.gendesc From QL_trnnotahrretur hr inner join ql_mstsupp s on hr.cmpcode = s.cmpcode and hr.suppoid = s.suppoid Inner Join QL_mstgen cb ON cb.cmpcode =hr.cmpcode AND hr.branch_code=cb.gencode AND cb.gengroup='CABANG' Where hr.cmpcode = '" & CompnyCode & "'" & sWhere & " order by hr.trnnotahrreturoid DESC"
        ViewState("hrretur") = Nothing
        ViewState("hrretur") = cKon.ambiltabel(sSql, "hrretur")
        GVTrn.DataSource = ViewState("hrretur")
        GVTrn.DataBind()
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FCabangNya, sSql)
            Else
                FillDDL(FCabangNya, sSql)
                FCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                FCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(FCabangNya, sSql)
            FCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            FCabangNya.SelectedValue = "ALL"
        End If
        FCabangNya.SelectedValue = Session("branch_id")
    End Sub

    Private Sub DDLBranch()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlFromcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlFromcabang, sSql)
            Else
                FillDDL(ddlFromcabang, sSql)
                'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
                'ddlFromcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(ddlFromcabang, sSql)
            'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
            'ddlFromcabang.SelectedValue = "ALL"
        End If
        ddlFromcabang.SelectedValue = Session("branch_id")
    End Sub

    Private Sub initallddl()
        
    End Sub

    Private Sub printinvoiceservices(ByVal name As String)
        'Try
        '    rpt = New ReportDocument
        '    rpt.Load(Server.MapPath("~\Report\Invoiceservices.rpt"))
        '    'cProc.SetDBLogonForReport(rpt)
        '    rpt.SetParameterValue("sWhere", " AND reqoid = " & Session("oid") & "")
        '    Response.Buffer = False
        '    Response.ClearHeaders()
        '    Response.ClearContent()
        '    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "INVSVC_" & name)
        '    rpt.Close() : rpt.Dispose()
        'Catch ex As Exception
        '    showMessage(ex.ToString, 2)
        'End Try
    End Sub

    Private Sub filltextbox(ByVal branch As String, ByVal id As Integer)
        Dim oid As Integer = 0 : ddlFromcabang.Enabled = False
        If Integer.TryParse(id, oid) = False Then
            Response.Redirect("trnNotaHRReturn.aspx?awal=true")
        Else
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "select hr.trnnotahroid,hr.branch_code, (CASE WHEN hr.trnnotahrreturstatus = 'Approved' THEN hr.trnnotahrreturno ELSE CAST(hr.trnnotahrreturoid as varchar(5)) END) trnnotahrreturno,(CASE WHEN hr.trnnotahrreturtype = 'HR' THEN (SELECT hrm.trnnotahrno FROM QL_trnnotahrmst hrm where hrm.trnnotahroid = hr.trnnotahroid) ELSE (SELECT bm.trnbelino FROM QL_trnbelimst bm where bm.trnbelimstoid = hr.trnnotahroid) END) trnnotahrno,hr.trnnotahrreturtype, hr.trnnotahrreturdate, hr.suppoid, s.suppname,(CASE WHEN hr.trnnotahrreturtype = 'HR' THEN ISNULL((SELECT trnnotahramtnetto FROM QL_trnnotahrmst hrm where hrm.trnnotahroid = hr.trnnotahroid),0.0) ELSE ISNULL((select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = hr.trnnotahroid and bd.trnbelidtldisctype2 = 'VCR'),0.0) END) trnnotahramtnetto,(CASE WHEN hr.trnnotahrreturtype = 'HR' THEN ISNULL((SELECT SUM(c.payrefamt) FROM QL_conhr c WHERE c.cmpcode=hr.cmpcode AND c.refoid=hr.trnnotahroid AND c.payreftype<>''),0.0) ELSE 0 END) trnnotahracumamt,(CASE WHEN hr.trnnotahrreturtype = 'HR' THEN ISNULL((SELECT trnnotahrreturamt FROM QL_trnnotahrmst where trnnotahroid = hr.trnnotahroid and hr.trnnotahrreturtype = 'HR' and hr.trnnotahrreturstatus IN ('Approved', 'In Approval')),0.0) + ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hrx where hrx.trnnotahroid = hr.trnnotahroid and hrx.trnnotahrreturstatus ='In Approval' AND hrx.trnnotahrreturtype = 'HR'),0.0) ELSE ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hrx where hrx.trnnotahroid = hr.trnnotahroid and hrx.trnnotahrreturstatus IN ('In Approval','Approved') AND hrx.trnnotahrreturtype = 'PI'),0.0) END) trnnotahrreturamt,(CASE WHEN hr.trnnotahrreturtype = 'HR' THEN ISNULL((SELECT trnnotahramtnetto - (trnnotahracumamt + trnnotahrreturamt /*+ ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hrx where hrx.trnnotahroid = hr.trnnotahroid and hrx.trnnotahrreturstatus ='In Approval' AND hrx.trnnotahrreturtype = 'HR'),0.0)*/) FROM QL_trnnotahrmst hrm where hrm.trnnotahroid = hr.trnnotahroid),0.0) ELSE ISNULL((select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = hr.trnnotahroid and bd.trnbelidtldisctype2 = 'VCR'),0.0) - ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hrx where hrx.trnnotahroid = hr.trnnotahroid and hrx.trnnotahrreturtype = 'PI' and hrx.trnnotahrreturstatus IN ('In Approval','Approved')),0) END) saldohr, hr.trnnotahrreturamtnetto, hr.trnnotahrreturnote, hr.trnnotahrreturstatus,(CASE WHEN hr.trnnotahrreturtype = 'HR' THEN (CASE WHEN (SELECT hrm.trnnotahrdate FROM QL_trnnotahrmst hrm where hrm.trnnotahroid = hr.trnnotahroid) = '01/01/1900' THEN '1/1/2018' ELSE (SELECT hrm.trnnotahrdate FROM QL_trnnotahrmst hrm where hrm.trnnotahroid = hr.trnnotahroid) END) ELSE '' END) trnnotahrdate, hr.upduser, hr.updtime,hr.crttime from QL_trnnotahrretur hr INNER JOIN QL_mstsupp s ON s.suppoid = hr.suppoid Where hr.trnnotahrreturoid = " & oid & " and hr.cmpcode = '" & CompnyCode & "' and hr.branch_code='" & branch & "'"
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    DDLBranch()
                    DDLtype.SelectedValue = xreader("trnnotahrreturtype")
                    DDLtype_SelectedIndexChanged(Nothing, Nothing)
                    ddlFromcabang.SelectedValue = xreader("branch_code").ToString
                    noreturn.Text = xreader("trnnotahrreturno").ToString
                    returndate.Text = Format(xreader("trnnotahrreturdate"), "dd/MM/yyyy")
                    'paytype.Text = xreader("trnpaytype")
                    initallddl()
                    'fromlocation.SelectedValue = xreader("itemloc")
                    trdate.Text = Format(xreader("trnnotahrdate"), "dd/MM/yyyy")
                    invoiceno.Text = xreader("trnnotahrno").ToString
                    JualMstOid.Text = xreader("trnnotahroid")
                    'lblcurr.Text = xreader("currencyoid")
                    'typeSO.Text = xreader("typeSO").ToString
                    branch_code.Text = xreader("branch_code").ToString
                    suppname.Text = xreader("suppname").ToString
                    suppoid.Text = Integer.Parse(xreader("suppoid"))
                    hramt.Text = ToMaskEdit(xreader("trnnotahramtnetto"), 3)
                    hraccum.Text = ToMaskEdit(xreader("trnnotahracumamt"), 3)
                    hrretur.Text = ToMaskEdit(xreader("trnnotahrreturamt"), 3)
                    hrbalance.Text = ToDouble(xreader("trnnotahramtnetto")) - (ToDouble(xreader("trnnotahracumamt")) + ToDouble(xreader("trnnotahrreturamt")))
                    hrbalance.Text = ToMaskEdit(hrbalance.Text, 3)
                    hrreturamt.Text = ToMaskEdit(xreader("trnnotahrreturamtnetto"), 3)
                    status.Text = xreader("trnnotahrreturstatus")
                    note.Text = xreader("trnnotahrreturnote")
                    upduser.Text = xreader("upduser")
                    updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt")
                    createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                End While
            End If
            xreader.Close()
            conn.Close()

            If status.Text.ToUpper = "APPROVED" Then
                ibcancel.Visible = True : ibsave.Visible = False
                ibdelete.Visible = False : ibapproval.Visible = False
                ibposting.Visible = False : btnPrint.Visible = True
            ElseIf status.Text.ToUpper = "REJECTED" Or status.Text.ToUpper = "IN APPROVAL" Then
                ibcancel.Visible = True : ibsave.Visible = False
                ibdelete.Visible = False : ibapproval.Visible = False
                ibposting.Visible = False : btnPrint.Visible = False
            Else
                ibcancel.Visible = True : ibsave.Visible = True
                ibdelete.Visible = True : ibapproval.Visible = True
                ibposting.Visible = False : btnPrint.Visible = False
            End If

            'If DDLtype.SelectedValue = "HR" Then
            '    lblNo.Text = "No. Nota HR" : lblhramt.Text = "HR Amount"
            '    lblhraccum.Text = "HR Accum Amt" : lblhrretur.Text = "HR Retur Amt"
            '    lblhrbalance.Text = "HR Balance" : lblhrreturamt.Text = "HR Return"
            '    GVTr.Columns(1).HeaderText = "No. Nota HR"
            '    GVTr.Columns(2).HeaderText = "Nota HR Date"
            '    GVTr.Columns(3).HeaderText = "Amt Nota HR"
            '    GVTr.Columns(4).HeaderText = "Accum HR"
            '    GVTr.Columns(5).HeaderText = "Retur HR"
            '    GVTr.Columns(6).HeaderText = "Amt HR Balance"
            'Else
            '    lblNo.Text = "No. PI" : lblhramt.Text = "PI Amount"
            '    lblhraccum.Text = "PI Accum Amt" : lblhrretur.Text = "PI Retur Amt"
            '    lblhrbalance.Text = "PI Balance" : lblhrreturamt.Text = "PI Return"
            '    GVTr.Columns(1).HeaderText = "No. PI"
            '    GVTr.Columns(2).HeaderText = "PI Date"
            '    GVTr.Columns(3).HeaderText = "Amt Nota PI"
            '    GVTr.Columns(4).HeaderText = "Accum PI"
            '    GVTr.Columns(5).HeaderText = "Retur PI+HR"
            '    GVTr.Columns(6).HeaderText = "Amt PI Balance"
            'End If
            sSql = "select trnnotahrreturdtlseq seq, trnbelimstoid, (SELECT trnbelino FROM QL_trnbelimst where trnbelimstoid = hrd.trnbelimstoid) trnbelino, trnbelidtlamt, trnnotahrreturdtlamt, trnnotahrreturdtlnote from QL_trnnotahrreturdtl hrd WHERE hrd.trnnotahrreturoid=" & oid & " ORDER BY trnnotahrreturdtloid"
            Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_trnnotahrreturdtl")
            For C1 As Integer = 0 To dtTable.Rows.Count - 1
                dtTable.Rows(C1)("seq") = C1 + 1
            Next
            Session("TblDtl") = dtTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail() : CountTotalAmt()
        End If
    End Sub

    Private Sub BindSupp()
        Try
            Dim lvl As String = ""
            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                lvl &= " AND hm.crtuser='" & Session("UserID") & "'"
            End If
            If DDLtype.SelectedValue <> "PI" Then
                sSql = "select distinct s.suppoid,s.suppcode,s.suppname from QL_mstsupp s inner join QL_trnnotahrmst hm on s.cmpcode=hm.cmpcode and s.suppoid = hm.suppoid where s.suppname like '%" & Tchar(suppname.Text) & "%' AND hm.branch_code='" & ddlFromcabang.SelectedValue & "' " & lvl & " GROUP BY s.suppoid,s.suppcode,s.suppname, hm.trnnotahroid HAVING SUM(trnnotahramtnetto) - ISNULL((SELECT SUM(c.payrefamt) FROM QL_conhr c WHERE c.refoid=hm.trnnotahroid AND c.payreftype<>''),0.0) - (SUM(trnnotahrreturamt) + ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = hm.trnnotahroid and hr.trnnotahrreturstatus ='In Approval' AND hr.trnnotahrreturtype = 'HR'),0.0)) > 0 AND hm.trnnotahroid > 0 ORDER BY s.suppname"
            Else
                'sSql = "select distinct s.suppoid,s.suppcode,s.suppname from QL_mstsupp s inner join QL_trnbelimst hm on s.cmpcode=hm.cmpcode and s.suppoid = hm.trnsuppoid where s.suppname like '%" & Tchar(suppname.Text) & "%' AND hm.branch_code='" & ddlFromcabang.SelectedValue & "' " & lvl & " "
                sSql = " select distinct suppoid, suppname, suppcode from (" & _
" select s.suppoid, s.suppname, s.suppcode" & _
" , ((select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.trnbelidtldisctype2 = 'VCR') /*PI*/ " & _
" - ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hrm.trnnotahrstatus = 'POST'),0.0) /*ACCUM*/" & _
" - ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0) /*RET PI*/" & _
" + ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'HR'),0.0)) /*RET HR*/" & _
" AS saldohr " & _
" from QL_trnbelimst bm inner join ql_mstsupp s ON s.suppoid = bm.trnsuppoid where bm.trnbelimstoid > 0 " & _
" and trnbelimstoid in (select trnbelimstoid from QL_trnbelidtl where (amtdisc2 > 0 or amtdisc2idr > 0)) and s.suppname like '%" & Tchar(suppname.Text) & "%' ) nt where saldohr > 0 order by suppname"
            End If
            Session("Supp") = cKon.ambiltabel(sSql, "QL_mstsupp")
            gvCustomer.DataSource = Session("Supp")
            gvCustomer.DataBind() : gvCustomer.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindDataSI()
        If suppoid.Text = "" Then
            showMessage("Silahkan Pilih Supplier Terlebih Dahulu!", 2)
            Exit Sub
        End If

        Dim lvl As String = ""
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            lvl &= " AND a.upduser='" & Session("UserID") & "'"
        End If

        Try
            If DDLtype.SelectedValue = "HR" Then
                sSql = "Select hm.trnnotahroid,hm.branch_code,hm.trnnotahrno,s.suppoid, hm.trnnotahrdate, s.suppname,hm.trnnotahramtnetto - ISNULL((SELECT SUM(c.payrefamt) FROM QL_conhr c WHERE c.cmpcode=hm.cmpcode AND c.refoid=hm.trnnotahroid AND c.payreftype<>''),0.0) - (trnnotahrreturamt + ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = hm.trnnotahroid and hr.trnnotahrreturstatus ='In Approval' AND hr.trnnotahrreturtype = 'HR'),0.0)) AS saldohr, hm.trnnotahramtnetto, ISNULL((SELECT SUM(c.payrefamt) FROM QL_conhr c WHERE c.cmpcode=hm.cmpcode AND c.refoid=hm.trnnotahroid AND c.payreftype<>''),0.0) trnnotahracumamt, trnnotahrreturamt + ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = hm.trnnotahroid and hr.trnnotahrreturstatus ='In Approval' AND hr.trnnotahrreturtype = 'HR'),0.0) AS trnnotahrreturamt, ISNULL((Select SUM(hd.trnnotahrdtlamt) from QL_trnnotahrdtl hd Where hd.trnnotahroid=hm.trnnotahroid),0.0) trnnotahrdtlamt From QL_trnnotahrmst hm inner join QL_mstsupp s on s.suppoid = hm.suppoid Where hm.cmpcode = '" & CompnyCode & "' AND hm.trnnotahroid>0 AND trnnotahramtnetto - ISNULL((SELECT SUM(c.payrefamt) FROM QL_conhr c WHERE c.cmpcode=hm.cmpcode AND c.refoid=hm.trnnotahroid AND c.payreftype<>''),0.0) - (trnnotahrreturamt + ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = hm.trnnotahroid and hr.trnnotahrreturstatus ='In Approval' AND hr.trnnotahrreturtype = 'HR'),0.0)) > 0 and hm.trnnotahrno like '%" & Tchar(invoiceno.Text) & "%' and s.suppoid = " & suppoid.Text & " and hm.trnnotahrstatus in ('POST') AND hm.branch_code='" & ddlFromcabang.SelectedValue & "' " & lvl & ""
                '" UNION ALL " & _
                '"Select hm.trnnotahroid,hm.branch_code,hm.trnnotahrno,s.suppoid,hm.trnnotahrdate, s.suppname,trnnotahramtnetto - ISNULL((SELECT SUM(c.payrefamt) FROM QL_conhr c WHERE c.cmpcode=hm.cmpcode AND c.refoid=hm.trnnotahroid AND c.payreftype<>''),0.0) - (trnnotahrreturamt + ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = hm.trnnotahroid and hr.trnnotahrreturstatus ='In Approval' AND hr.trnnotahrreturtype = 'HR'),0.0)) AS saldohr ,hm.trnnotahramtnetto,ISNULL((SELECT SUM(c.payrefamt) FROM QL_conhr c WHERE c.cmpcode=hm.cmpcode AND c.refoid=hm.trnnotahroid AND c.payreftype<>''),0.0) AS trnnotahracumamt, trnnotahrreturamt + ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = hm.trnnotahroid and hr.trnnotahrreturstatus ='In Approval' AND hr.trnnotahrreturtype = 'HR'),0.0) AS trnnotahrreturamt,0.00 trnnotahrdtlamt From QL_trnnotahrmst hm Inner Join QL_mstsupp s ON s.suppoid=hm.suppoid where trnnotahroid < 0 and trnnotahramtnetto - ISNULL((SELECT SUM(c.payrefamt) FROM QL_conhr c WHERE c.cmpcode=hm.cmpcode AND c.refoid=hm.trnnotahroid AND c.payreftype<>''),0.0) - (trnnotahrreturamt + ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = hm.trnnotahroid and hr.trnnotahrreturstatus ='In Approval' AND hr.trnnotahrreturtype = 'HR'),0.0)) > 0 AND hm.cmpcode = '" & CompnyCode & "' AND hm.trnnotahrno like '%" & Tchar(invoiceno.Text) & "%' And s.suppoid = '" & suppoid.Text & "' and hm.trnnotahrstatus in ('POST') AND hm.branch_code='" & ddlFromcabang.SelectedValue & "' " & " ORDER BY trnnotahrno"
                'Else
                'sSql = " select * from (select trnbelimstoid AS trnnotahroid, bm.branch_code, trnbelino AS trnnotahrno, bm.trnsuppoid AS suppoid, bm.trnbelidate AS trnnotahrdate, (SELECT s.suppname FROM QL_mstsupp s where s.suppoid = bm.trnsuppoid) suppname, ((select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.trnbelidtldisctype2 = 'VCR') - ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd where hrd.trnbelimstoid = bm.trnbelimstoid),0.0)+ ISNULL((SELECT SUM(hr.trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hr.trnnotahroid INNER JOIN QL_trnnotahrdtl hrd ON hrd.trnnotahroid = hrm.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturtype = 'HR' and hr.trnnotahrreturstatus IN ('In Approval', 'Approved')),0.0)- ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0)) AS saldohr, (select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.trnbelidtldisctype2 = 'VCR') AS trnnotahramtnetto, ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd where hrd.trnbelimstoid = bm.trnbelimstoid),0.0) AS trnnotahracumamt, (ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0) - ISNULL((SELECT SUM(hr.trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hr.trnnotahroid INNER JOIN QL_trnnotahrdtl hrd ON hrd.trnnotahroid = hrm.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturtype = 'HR' and hr.trnnotahrreturstatus IN ('In Approval', 'Approved')),0.0)) trnnotahrreturamt, 0.0 trnnotahrdtlamt  from QL_trnbelimst bm where bm.trnbelimstoid > 0 and trnbelimstoid in (select trnbelimstoid from QL_trnbelidtl where (amtdisc2 > 0 or amtdisc2idr > 0))  and bm.trnsuppoid = " & suppoid.Text & " and bm.trnbelino like '%" & invoiceno.Text & "%') nt where saldohr > 0"
            End If
            ViewState("trnnotahrmst") = cKon.ambiltabel(sSql, "trnnotahrretur")
            FillGV(GVTr, sSql, "trnnotahrmst")
            GVTr.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindDataPI()
        If suppoid.Text = "" Then
            showMessage("Silahkan Pilih Supplier Terlebih Dahulu!", 2)
            Exit Sub
        End If

        If DDLtype.SelectedValue = "HR" Then
            If JualMstOid.Text = "" Then
                showMessage("Silahkan Pilih Nota HR Terlebih Dahulu!", 2)
                Exit Sub
            End If
        End If

        Dim lvl As String = ""
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            lvl &= " AND a.upduser='" & Session("UserID") & "'"
        End If

        Try
            If DDLtype.SelectedValue = "HR" Then
                sSql = "select * from (" & _
    " select bm.trnbelimstoid AS trnnotahroid, bm.branch_code, trnbelino AS trnnotahrno, bm.trnsuppoid AS suppoid, bm.trnbelidate AS trnnotahrdate, (SELECT s.suppname FROM QL_mstsupp s where s.suppoid = bm.trnsuppoid) suppname, ((select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.amtdisc2 > 0) - ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hrm.trnnotahrstatus = 'POST'),0.0) - ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0) + ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'HR'),0.0)) AS saldohr, (select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.amtdisc2 > 0) AS trnnotahramtnetto, hrd.trnnotahrdtlamt, ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid),0.0) AS trnnotahracumamt, ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'HR'),0.0) trnnotahrreturamt,ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0) trnnotahrreturhramt from QL_trnbelimst bm inner join QL_trnnotahrdtl hrd ON hrd.trnbelimstoid = bm.trnbelimstoid where bm.trnbelimstoid > 0 and bm.trnbelimstoid in (select trnbelimstoid from QL_trnbelidtl where (amtdisc2 > 0 or amtdisc2idr > 0)) AND hrd.trnnotahroid = " & JualMstOid.Text & " and bm.trnbelino like '%" & trnbelino.Text & "%' " & _
    ") nt /*where saldohr > 0*/"
                gvBeli.Columns(5).Visible = False
            Else
                sSql = "select * from (" & _
"select bm.trnbelimstoid AS trnnotahroid, bm.branch_code, trnbelino AS trnnotahrno, bm.trnsuppoid AS suppoid, bm.trnbelidate AS trnnotahrdate, (SELECT s.suppname FROM QL_mstsupp s where s.suppoid = bm.trnsuppoid) suppname" & _
", ((select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code ) /*PI*/  " & _
"- ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hrm.trnnotahrstatus = 'POST'),0.0) /*ACCUM*/ " & _
"- ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0) /*RET PI*/ " & _
"+ ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'HR'),0.0)) /*RET HR*/ AS saldohr " & _
", (select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.trnbelidtldisctype2 = 'VCR') AS trnnotahramtnetto,0 trnnotahrdtlamt" & _
", ISNULL((SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hrm.trnnotahrstatus = 'POST'),0.0) AS trnnotahracumamt" & _
", ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0) trnnotahrreturamt" & _
", ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'HR'),0.0) trnnotahrreturhramt" & _
" from QL_trnbelimst bm where bm.trnbelimstoid > 0 AND bm.trnbelimstoid in (select trnbelimstoid from QL_trnbelidtl where (amtdisc2 > 0 or amtdisc2idr > 0)) " & _
"AND bm.trnsuppoid = " & suppoid.Text & " and bm.trnbelino like '%" & trnbelino.Text & "%'" & _
") nt where saldohr > 0"
                gvBeli.Columns(5).Visible = True
            End If

            ViewState("trnbelimst") = cKon.ambiltabel(sSql, "trnbelimst")
            FillGV(gvBeli, sSql, "trnbelimst")
            gvBeli.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub GVTr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVTr.SelectedIndexChanged
        Try
            invoiceno.Text = GVTr.SelectedDataKey("trnnotahrno").ToString
            trdate.Text = Format(GVTr.SelectedDataKey("trnnotahrdate"), "dd/MM/yyyy")
            JualMstOid.Text = GVTr.SelectedDataKey.Item("trnnotahroid")
            hramt.Text = ToMaskEdit(ToDouble(GVTr.SelectedDataKey.Item("trnnotahramtnetto")), 3)
            hrretur.Text = ToMaskEdit(ToDouble(GVTr.SelectedDataKey.Item("trnnotahrreturamt")), 3)
            hraccum.Text = ToMaskEdit(ToDouble(GVTr.SelectedDataKey.Item("trnnotahracumamt")), 3)
            hrbalance.Text = ToMaskEdit(ToDouble(GVTr.SelectedDataKey.Item("saldohr")), 3)
            lblmax.Text = "MAX = " & ToMaskEdit(ToDouble(GVTr.SelectedDataKey.Item("saldohr")), 3) & ""
            GVTr.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub ClearNota()
        JualMstOid.Text = "" : invoiceno.Text = ""
        trdate.Text = "" : hramt.Text = "0.00"
        hraccum.Text = "0.00" : hrbalance.Text = "0.00"
        hrretur.Text = "0.00" : hrbalance.Text = "0.00"
        lblmax.Text = "" : hrreturamt.Text = ""
        GVTr.Visible = False
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        seq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                seq.Text = dt.Rows.Count + 1
            End If
        End If
        trnbelimstoid.Text = "" : returamt.Text = ""
        notedtl.Text = "" : trnbelino.Text = ""
        lblmaxdtl.Text = "" : lblmaxdtl2.Text = ""
    End Sub

    Private Sub CountTotalAmt()
        Dim iGtotal As Decimal
        Dim objTable As DataTable = Session("TblDtl")
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            iGtotal += ToDouble(objTable.Rows(C1)("trnnotahrreturdtlamt"))
        Next
        Session("TblDtl") = objTable
        hrreturamt.Text = ToMaskEdit(ToDouble(iGtotal), 3)
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLtype.CssClass = sCss : DDLtype.Enabled = bVal
        invoiceno.CssClass = sCss : invoiceno.Enabled = bVal
        suppname.CssClass = sCss : suppname.Enabled = bVal
        btnsearchinvoice.Enabled = bVal : btneraseinvoice.Enabled = bVal
        btnFindSupp.Enabled = bVal : btnEraseSupp.Enabled = bVal
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        'Session.Timeout = 60

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Session("ApprovalLimit") = appLimit
            Response.Redirect("~\Transaction\trnNotaHRReturn.aspx")
        End If

        Page.Title = CompnyName & " - HR Return"

        Session("sCmpcode") = Request.QueryString("branch_code")
        Session("oid") = Request.QueryString("oid")

        ibapproval.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin mengirimkan permintaan approval pada data ini?');")
        ibdelete.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin menghapus data ini?');")
        ibposting.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin melakukan posting data ini?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
            tbperiodstart.Text = Format(GetServerTime(), "01/MM/yyyy")
            tbperiodend.Text = Format(Date.Now, "dd/MM/yyyy")
            noreturn.Text = GenerateID("QL_trnnotahrretur", CompnyCode)
            trnnotahrreturdtloid.Text = GenerateID("QL_trnnotahrreturdtl", CompnyCode)
            InitDDLBranch() : DDLBranch() : initallddl()
            BindAllData(" And convert(date,hr.trnnotahrreturdate,103) between '" & New Date(Date.Now.Year, Date.Now.Month, 1) & "' and '" & Date.Now & "'")

            If Session("oid") = "" Or Session("oid") Is Nothing Then
                TabContainer1.ActiveTabIndex = 0
                masterstate.Text = "new"
                returndate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                lblcreate.Text = "Create On"
            Else
                TabContainer1.ActiveTabIndex = 1
                masterstate.Text = "edit"
                filltextbox(Session("sCmpcode"), Session("oid"))
                lblcreate.Text = "Last Update By"
            End If
        End If
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("trnNotaHRReturn.aspx?awal=true")
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        Dim date1, date2 As New Date
        Dim sWhere As String = ""
        If tbperiodstart.Text.Trim = "" Or tbperiodend.Text.Trim = "" Then
            showMessage("Semua Periode tidak boleh kosong!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodstart.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("period awal tidak sesuai!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodend.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("period akhir tidak sesuai!", 2)
            Exit Sub
        End If
        If CDate(toDate(tbperiodstart.Text)) > CDate(toDate(tbperiodend.Text)) Then
            showMessage("period akhir harus lebih dari period awal!", 2)
            Exit Sub
        End If

        If fddltype.SelectedValue <> "ALL" Then
            sWhere &= " AND hr.trnnotahrreturtype = '" & fddltype.SelectedValue & "'"
        End If

        If CbPeriode.Checked = True Then
            sWhere &= " And convert(date,hr.trnnotahrreturdate,103) between '" & date1 & "' and '" & date2 & "'"
        Else
            sWhere &= " And " & ddlfilter.SelectedValue.ToString & " Like '%" & Tchar(tbfilter.Text) & "%' " & IIf(ddlfilterstatus.SelectedValue.ToString <> "All", " and hr.trnnotahrreturstatus = '" & ddlfilterstatus.SelectedValue.ToString & "'", "") & ""
        End If
        BindAllData(sWhere)
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        tbfilter.Text = ""
        fddltype.SelectedValue = "ALL"
        'FCabangNya.SelectedValue = "ALL"
        ddlfilterstatus.SelectedIndex = 0
        CbPeriode.Checked = False
        BindAllData("")
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub ibapproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapproval.Click
        Dim sMsgApproval As String = ""

        sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnnotahrretur' and branch_code Like '%" & ddlFromcabang.SelectedValue & "%' order by approvallevel"
        Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
        If dtData2.Rows.Count > 0 Then
            Session("TblApproval") = dtData2
        Else
            sMsgApproval += "- Setting Approval Nota HR Return belum ada, silahkan hubungi admin.<BR>"
        End If

        If sMsgApproval <> "" Then
            showMessage(sMsgApproval, 2)
            Exit Sub
        End If

        status.Text = "In Approval"
        ibsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub ibposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposting.Click
        status.Text = "POST"
        'If DDLtype.SelectedValue = 1 Then
        ibsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub ibsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsave.Click
        Dim sMsg As String = ""
        If trdate.Text <> "" Then
            If CDate(toDate(returndate.Text)) < CDate(toDate(trdate.Text)) Then
                sMsg &= "- Maaf, Tanggal retur tidak boleh kurang dari tanggal " & DDLtype.SelectedValue & "..!!<br>"
            End If
        End If

        If suppoid.Text = "" Then sMsg &= "- Pilih supplier terlebih dahulu!<br> "

        If DDLtype.SelectedValue = "HR" Then
            If hrreturamt.Text = "" Then sMsg &= "- HR Return tidak boleh kosong!<br>"
            If ToDouble(hramt.Text) = 0.0 Then sMsg &= "- HR Return tidak boleh nol!<br>"
            If JualMstOid.Text = "" Then sMsg &= "- Pilih No. " & DDLtype.SelectedValue & " terlebih dahulu!<br> "
            If ToDouble(hrreturamt.Text) > ToDouble(hrbalance.Text) Then sMsg &= " - Amount Retur tidak boleh lebih dari Amount HR Balance <br>"
        End If

        If returndate.Text = "" Then sMsg &= "- Tanggal retur tidak boleh kosong..!!<br>"

        Dim returdate As New Date
        If Date.TryParseExact(returndate.Text, "dd/MM/yyyy", Nothing, Nothing, returdate) = False Then
            sMsg &= "- Maaf, Tanggal tidak valid..!!<br>"
        End If

        If returdate < New Date(1900, 1, 1) Then sMsg &= "- Maaf, tanggal kurang dari '1/1/1900'..!!<br>"
        If returdate > New Date(2079, 6, 6) Then sMsg &= "- Maaf, tanggal tidak boleh melebihi '6/6/2079'..!!<br>"

        If gvDtl.Rows.Count = 0 Then
            sMsg &= "- Detail Nota HR Return harus di isi!!<br>"
        End If

        Try
            If DDLtype.SelectedValue = "HR" Then
                If GetStrData("SELECT COUNT(-1) FROM QL_trnnotahrretur where trnnotahroid = " & JualMstOid.Text & " AND trnnotahrreturstatus IN ('In Process') and trnnotahrreturres1='ql_trnnotahrmst' and branch_code= '" & ddlFromcabang.SelectedValue & "' AND trnnotahrreturtype = '" & DDLtype.SelectedValue & "' and trnnotahrreturoid <> " & noreturn.Text & "") > 0 Then
                    sMsg &= "Ada transaksi Retur untuk Nota no " & invoiceno.Text & " yang belum di-SEND APPROVAL. Tekan Cancel lalu HAPUS/SEND APPROVAL dulu Pelunasan tersebut."
                End If

                If GetStrData("select count(-1) from QL_trnpayhrdtl where payrefoid=" & JualMstOid.Text & " and upper(payreftype)='ql_trnnotahrmst' and upper(paystatus)<>'POST' And branch_code='" & ddlFromcabang.SelectedValue & "'") > 0 Then
                    sMsg &= "- Ada transaksi Pelunasan untuk Nota no " & invoiceno.Text & " yang belum di-POSTING. Tekan Cancel lalu HAPUS/POSTING dulu Pelunasan tersebut.<BR>"
                End If
            End If

            'CEK SALDO
            If DDLtype.SelectedValue = "HR" Then
                sSql = "SELECT COUNT(-1) FROM ( SELECT trnnotahramtnetto, trnnotahracumamt, trnnotahrreturamt + ISNULL((SELECT SUM(hr.trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = hrm.trnnotahroid and hr.trnnotahrreturstatus = 'In Approval' and hr.trnnotahrreturtype = 'HR'),0) AS trnnotahrreturamt FROM QL_trnnotahrmst hrm where hrm.trnnotahroid = " & JualMstOid.Text & ") hrr where (trnnotahramtnetto - trnnotahracumamt) + trnnotahrreturamt < " & ToDouble(hrreturamt.Text) & ""
                Dim counthr As Integer = GetStrData(sSql)
                If counthr > 0 Then
                    sMsg &= "- Saldo Nota no " & invoiceno.Text & " tidak mencukupi untuk dilakukan retur. Tekan Cancel lalu cek nota tersebut.<BR>"
                End If
            Else
                'sSql = "SELECT COUNT(-1) FROM ( SELECT (select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.trnbelidtldisctype2 = 'VCR') voucher, ISNULL((SELECT SUM(trnnotahramtnetto) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hrm.trnnotahrstatus = 'POST'),0.0) hramtnetto, ISNULL((SELECT SUM(trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr where hr.trnnotahroid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval','Approved') and hr.trnnotahrreturtype = 'PI'),0.0) pireturamtnetto, ISNULL((SELECT SUM(hr.trnnotahrreturamtnetto) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hr.trnnotahroid INNER JOIN QL_trnnotahrdtl hrd ON hrd.trnnotahroid = hrm.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturtype = 'HR' and hr.trnnotahrreturstatus IN ('In Approval', 'Approved')),0.0) hrreturamtnetto FROM QL_trnbelimst bm where trnbelimstoid = " & JualMstOid.Text & " ) pii where voucher - (hramtnetto + pireturamtnetto - hrreturamtnetto) < " & ToDouble(hrreturamt.Text) & ""

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable : objTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "SELECT COUNT(-1) FROM ( SELECT (select SUM(bd.amtdisc2) from QL_trnbelidtl bd where bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code and bd.trnbelidtldisctype2 = 'VCR') voucher, ISNULL((SELECT SUM(trnnotahramtnetto) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid and hrm.trnnotahrstatus = 'POST'),0.0) hramtnetto, ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'PI'),0.0) pireturamtnetto, ISNULL((SELECT SUM(hrd.trnnotahrreturdtlamt) FROM QL_trnnotahrretur hr INNER JOIN QL_trnnotahrreturdtl hrd ON hrd.trnnotahrreturoid = hr.trnnotahrreturoid where hrd.trnbelimstoid = bm.trnbelimstoid and hr.trnnotahrreturstatus IN ('In Approval', 'Approved') AND hr.trnnotahrreturtype = 'HR'),0.0) hrreturamtnetto FROM QL_trnbelimst bm where trnbelimstoid = " & objTable.Rows(c1).Item("trnbelimstoid") & " ) pii where (voucher - hramtnetto - pireturamtnetto) + hrreturamtnetto < " & ToDouble(objTable.Rows(c1).Item("trnnotahrreturdtlamt")) & ""
                        Dim countpi As Integer = GetStrData(sSql)
                        If countpi > 0 Then
                            sMsg &= "- Saldo Nota PI no " & objTable.Rows(c1).Item("trnbelino") & " tidak mencukupi untuk dilakukan retur. Tekan Cancel lalu cek nota tersebut.<BR>"
                        End If
                    Next
                End If
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trnnotahrretur WHERE crttime='" & CDate(toDate(createtime.Text)) & "' AND branch_code='" & ddlFromcabang.SelectedValue & "'")) > 0 Then
                    sMsg &= "- Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
                End If
            Else
                ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
                sSql = "select trnnotahrreturstatus from QL_trnnotahrretur Where trnnotahrreturoid = " & Integer.Parse(Session("oid")) & " and branch_code='" & ddlFromcabang.SelectedValue & "'"
                Dim stat As String = GetStrData(sSql)
                If stat.ToLower = "post" Then
                    sMsg &= "- Maaf, data sudah posting..!!<br />"
                    status.Text = "In Process"
                ElseIf stat.ToLower = "in approval" Or stat.ToLower = "approved" Then
                    sMsg &= "- Maaf, status transaksi sudah '" & stat & "', tekan Cancel dan cek kembali status transaksi ini..!!<br />"
                    status.Text = "In Process"
                ElseIf stat Is Nothing Or stat = "" Then
                    sMsg &= "- Maaf, HR return tidak ada..!!<br>"
                End If
            End If

            If sMsg <> "" Then
                showMessage(sMsg, 2) : status.Text = "In Process"
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            status.Text = "In Process" : Exit Sub
        End Try
        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            Dim period As String = GetDateToPeriodAcctg(returdate)
            '---- Generated ID tabel -----
            Dim returmstoid As Integer = GenerateID("QL_trnnotahrretur", CompnyCode)
            trnnotahrreturdtloid.Text = GenerateID("QL_trnnotahrreturdtl", CompnyCode)
            Dim AppOid As Integer = GenerateID("QL_approval", CompnyCode)

            If masterstate.Text = "new" Then
                'If Not status.Text = "POST" Then
                '    noreturn.Text = (returmstoid).ToString
                'End If

                sSql = "insert into QL_trnnotahrretur (cmpcode, trnnotahrreturoid, branch_code, trnnotahroid, trnnotahrreturno, trnnotahrreturdate, suppoid, trnnotahrreturdpp, trnnotahrreturtaxtype, trnnotahrreturtaxpct, trnnotahrreturtaxamt, trnnotahrreturamtnetto, trnnotahrreturamtnettoidr, trnnotahrreturamtnettousd, returacctgoid , trnnotahrreturnote , trnnotahrreturstatus, trnnotahrreturres1, trnnotahrreturres2, crtuser, crttime, upduser,updtime, trnnotahrreturtype) Values " & _
                "('" & CompnyCode & "', " & returmstoid & ", '" & ddlFromcabang.SelectedValue & "', " & IIf(DDLtype.SelectedValue = "HR", JualMstOid.Text, 0) & ", '', '" & CDate(toDate(returndate.Text)) & "', " & suppoid.Text & ", " & ToDouble(hrreturamt.Text) & ", 'NON TAX', 10, 0, " & ToDouble(hrreturamt.Text) & ", " & ToDouble(hrreturamt.Text) & ", 0, 0, '" & Tchar(note.Text) & "', '" & status.Text & "', '" & IIf(DDLtype.SelectedValue = "HR", "ql_trnnotahrmst", "ql_trnbelimst") & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DDLtype.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & returmstoid & " where tablename = 'QL_trnnotahrretur' And cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                '================= Proses Edit data ===============
                '--------------------------------------------------
                returmstoid = Session("oid")
                sSql = "Update QL_trnnotahrretur set branch_code = '" & ddlFromcabang.SelectedValue & "', trnnotahroid = " & JualMstOid.Text & ", suppoid = " & suppoid.Text & ", trnnotahrreturdpp = " & ToDouble(hrreturamt.Text) & ", trnnotahrreturamtnetto = " & ToDouble(hrreturamt.Text) & ", trnnotahrreturamtnettoidr = " & ToDouble(hrreturamt.Text) & ", returacctgoid = 0, trnnotahrreturnote = '" & Tchar(note.Text) & "', trnnotahrreturstatus = '" & status.Text & "', trnnotahrreturres1 = '" & IIf(DDLtype.SelectedValue = "HR", "ql_trnnotahrmst", "ql_trnbelimst") & "', trnnotahrreturres2 = '', upduser = '" & Session("UserID") & "',updtime = CURRENT_TIMESTAMP, trnnotahrreturtype = '" & DDLtype.SelectedValue & "' Where trnnotahrreturoid = " & returmstoid & " and cmpcode = '" & CompnyCode & "' and branch_code='" & ddlFromcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If status.Text = "In Approval" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus,branch_code) VALUES " & _
                            "('" & CompnyCode & "'," & AppOid + c1 & ", '" & "HR Return" & ddlFromcabang.SelectedValue & "_" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_trnnotahrretur', '" & Session("oid") & "','In Approval','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1', '" & objTable.Rows(c1).Item("approvalstatus") & "','" & ddlFromcabang.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next

                        sSql = "UPDATE ql_mstoid set lastoid = " & AppOid + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If
                sSql = "DELETE FROM QL_trnnotahrreturdtl WHERE cmpcode='" & CompnyCode & "' AND trnnotahrreturoid=" & returmstoid
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            '----- End Edit Data -----
            If Not Session("TblDtl") Is Nothing Then
                Dim objTable As DataTable = Session("TblDtl")
                For c1 As Int16 = 0 To objTable.Rows.Count - 1
                    sSql = "INSERT INTO QL_trnnotahrreturdtl (cmpcode, branch_code, trnnotahrreturdtlseq, trnnotahrreturdtloid, trnnotahrreturoid, trnnotahroid, trnbelimstoid, trnnotahrreturdtlamt, trnnotahrreturdtlamtidr, trnnotahrreturdtlamtusd, trnbelidtlamt, trnnotahrreturdtlnote, trnnotahrreturdtlres1, trnnotahrreturdtlres2, crtuser, crttime, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlFromcabang.SelectedValue & "', " & objTable.Rows(c1)("seq").ToString & ", " & (c1 + CInt(trnnotahrreturdtloid.Text)) & ", " & returmstoid & ", " & IIf(DDLtype.SelectedValue = "HR", JualMstOid.Text, 0) & ", " & objTable.Rows(c1)("trnbelimstoid").ToString & ", " & ToDouble(objTable.Rows(c1)("trnnotahrreturdtlamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("trnnotahrreturdtlamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("trnnotahrreturdtlamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("trnbelidtlamt").ToString) & ", '" & Tchar(objTable.Rows(c1)("trnnotahrreturdtlnote").ToString) & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(trnnotahrreturdtloid.Text)) & " WHERE tablename='QL_trnnotahrreturdtl' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            otrans.Commit() : conn.Close()
        Catch ex As Exception
            otrans.Rollback() : conn.Close()
            showMessage(ex.ToString, 2)
            status.Text = "In Process" : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnNotaHRReturn.aspx?awal=true")
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click
        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            sSql = "select trnnotahrreturstatus from QL_trnnotahrretur WHERE trnnotahrreturoid = " & Integer.Parse(Session("oid")) & " and branch_code = '" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "post" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Nota HR return has been Send Approval!<br />It can't be deleted!", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Data retur sudah dihapus...!!", 2)
                Exit Sub
            End If

            sSql = "delete from QL_trnnotahrretur where trnnotahrreturoid = " & Integer.Parse(Session("oid")) & " and branch_code = '" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnnotahrretur where trnnotahrreturoid = " & Integer.Parse(Session("oid")) & " and branch_code='" & ddlFromcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            otrans.Commit()
            conn.Close()
            Response.Redirect("trnNotaHRReturn.aspx?awal=true")
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString & "<br />" & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnsearchtr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "select a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid, e.custname,d.trntaxpct, d.trnpaytype from ql_trnTTSmst a inner join ql_TrnTTSDtl b on a.TTSoid = b.ttsoid inner join ql_mstitem c on b.refoid = c.itemoid inner join QL_trnjualmst d on a.cmpcode = d.cmpcode and a.trnsjjualno = d.trnsjjualno inner join QL_mstcust e on a.cmpcode = e.cmpcode and a.custoid = e.custoid where a.cmpcode = '" & CompnyCode & "' and a.TTSstatus = 'post' and  group by a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid,d.trntaxpct, e.custname, d.trnpaytype having (sum(b.qty)-(select isnull(sum(m.trnjualdtlqty), 0) from QL_trnjualreturdtl m inner join QL_trnjualreturmst n on m.cmpcode = n.cmpcode and m.trnjualreturmstoid = n.trnjualreturmstoid inner join ql_mstitem o on m.cmpcode = o.cmpcode and m.itemoid = o.itemoid where n.ttsoid = a.TTSoid)) > 0" & _
        " union all " & _
        "select a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid, e.custname,d.trntaxpct, d.trnpaytype from ql_trnTTSmst a inner join  ql_TrnTTSDtl b on a.TTSoid = b.ttsoid inner join ql_mstitem c on b.refoid = c.itemoid inner join QL_trnjualdtl dd on a.cmpcode = dd.cmpcode and a.trnsjjualno = dd.trnsjjualno and b.refoid = dd.itemoid  inner join QL_trnjualmst d on a.cmpcode = d.cmpcode and dd.trnjualmstoid  = d.trnjualmstoid  inner join QL_mstcust e on a.cmpcode = e.cmpcode and a.custoid = e.custoid where a.cmpcode = '" & CompnyCode & "' and a.TTSstatus = 'post' and d.trnjualstatus = 'POST'  group by a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid,d.trntaxpct, e.custname, d.trnpaytype having (sum(b.qty)-(select isnull(sum(m.trnjualdtlqty), 0) from QL_trnjualreturdtl m inner join QL_trnjualreturmst n on m.cmpcode = n.cmpcode and m.trnjualreturmstoid = n.trnjualreturmstoid inner join ql_mstitem o on m.cmpcode = o.cmpcode and m.itemoid = o.itemoid where n.ttsoid = a.TTSoid)) > 0"

        ViewState("trsalesreturn") = cKon.ambiltabel(sSql, "trsalesreturn")
        GVTr.DataSource = ViewState("trsalesreturn")
        GVTr.DataBind()

        GVTr.Visible = True
        GVTr.PageIndex = 0
    End Sub

    Protected Sub GVTr_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTr.PageIndexChanging
        GVTr.PageIndex = e.NewPageIndex
        BindDataSI()
    End Sub

    Protected Sub btnerasetr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        trdate.Text = "" : paytype.Text = ""
        suppname.Text = "" : suppoid.Text = ""
        invoiceno.Text = ""

        If GVTr.Visible = True Then
            ViewState("trsalesreturn") = Nothing
            GVTr.DataSource = Nothing
            GVTr.DataBind()
            GVTr.Visible = False
        End If
    End Sub

    Protected Sub btnsearchinvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataSI()
    End Sub

    Private Function checkotherretur(ByVal sino As String, ByVal iItemOid As Integer, ByVal cmpcode As String) As Boolean
        sSql = "select COUNT (-1) from ql_trnjualreturmst a inner join QL_trnjualreturdtl b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b. branch_code Where a.trnjualstatus ='In process' and a.cmpcode = '" & cmpcode & "' and a.refnotaretur ='" & sino & "' and  b.itemoid='" & iItemOid & "' AND a.branch_code='" & ddlFromcabang.SelectedValue & "'"

        If masterstate.Text = "edit" Then
            sSql &= " AND a.trnjualreturmstoid <>" & Session("oid")
        ElseIf masterstate.Text = "new" Then
            sSql &= " AND a.trnjualreturmstoid <> '' "
        End If
        Return cKon.ambilscalar(sSql) > 0
    End Function

    Private Sub calculateheader()

    End Sub

    Protected Sub GVTrn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTrn.PageIndexChanging
        If Not ViewState("hrretur") Is Nothing Then
            GVTrn.DataSource = ViewState("hrretur")
            GVTrn.PageIndex = e.NewPageIndex
            GVTrn.DataBind()
        End If
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lb As LinkButton = TryCast(sender, LinkButton)
            Dim str As String() = lb.ToolTip.Split(",")
            Dim cBranch As String = sender.CommandName
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\printsalesreturn.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", " where a.trnjualreturmstoid = " & Integer.Parse(str(0)) & " and a.branch_code='" & cBranch & "'")
            rpt.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Sales_Retur_" & str(1))
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Try
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\printsalesreturn.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", " where a.trnjualreturmstoid = " & Session("oid") & " and a.branch_code='" & Session("branch_id") & "'")
            rpt.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Sales_Return_" & noreturn.Text)
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btneraseinvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearNota()
        GVTr.Visible = False
    End Sub

    Protected Sub btnFindSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindSupp.Click
        BindSupp()
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppname.Text = gvCustomer.Rows(gvCustomer.SelectedIndex).Cells(2).Text
        suppoid.Text = gvCustomer.SelectedDataKey.Item("suppoid")
        gvCustomer.Visible = False
        'BindDataSI()
    End Sub

    Protected Sub btnEraseSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseSupp.Click
        suppoid.Text = "" : suppname.Text = ""
        ClearNota()
        gvCustomer.Visible = False : GVTr.Visible = False
    End Sub

    Protected Sub ibdelete_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim otrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            sSql = "select trnjualstatus from QL_trnjualreturmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "post" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return has been posted!<br />It can't be deleted!", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return data cannot be found!", 2)
                Exit Sub
            End If

            sSql = "delete from QL_trnjualreturmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnjualreturdtl where trnjualreturmsflkbPilihItem_Clicktoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If Not ViewState("dtlsalesreturn") Is Nothing Then
                Dim dtab As DataTable = ViewState("dtlsalesreturn")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    'returdtloid = returdtloid + 1
                    sSql = "update QL_trnjualdtl set trnjualdtlqty_retur = trnjualdtlqty_retur - " & dtab.Rows(i).Item("qty") & ", trnjualdtlqty_retur_unit3 = trnjualdtlqty_retur_unit3 - (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & ") where trnjualdtloid = " & dtab.Rows(i).Item("trnjualdtloid") & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                Next
            End If
            otrans.Commit()
            conn.Close()

            Response.Redirect("trnNotaHRReturn.aspx?awal=true")
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCustomer.DataSource = Session("Supp")
        gvCustomer.PageIndex = e.NewPageIndex
        gvCustomer.DataBind()
    End Sub

    Protected Sub GVTrn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVTrn.SelectedIndexChanged
        Response.Redirect("~\Transaction\trnNotaHRReturn.aspx?branch_code=" & GVTrn.SelectedDataKey("branch_code").ToString & "&oid=" & GVTrn.SelectedDataKey("trnnotahrreturoid").ToString & "")
    End Sub

    Protected Sub ddlFromcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFromcabang.SelectedIndexChanged
        initallddl()
    End Sub

    Protected Sub tbperiodstart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbperiodstart.TextChanged
        CbPeriode.Checked = True
    End Sub

    Protected Sub DDLtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtype.SelectedIndexChanged
        'If DDLtype.SelectedValue = "HR" Then
        '    lblNo.Text = "No. Nota HR" : lblhramt.Text = "HR Amount"
        '    lblhraccum.Text = "HR Accum Amt" : lblhrretur.Text = "HR Retur Amt"
        '    lblhrbalance.Text = "HR Balance" : lblhrreturamt.Text = "HR Return"
        '    GVTr.Columns(1).HeaderText = "No. Nota HR"
        '    GVTr.Columns(2).HeaderText = "Nota HR Date"
        '    GVTr.Columns(3).HeaderText = "Amt Nota HR"
        '    GVTr.Columns(4).HeaderText = "Accum HR"
        '    GVTr.Columns(5).HeaderText = "Retur HR"
        '    GVTr.Columns(6).HeaderText = "Amt HR Balance"
        'Else
        '    lblNo.Text = "No. PI" : lblhramt.Text = "PI Amount"
        '    lblhraccum.Text = "PI Accum Amt" : lblhrretur.Text = "PI Retur Amt"
        '    lblhrbalance.Text = "PI Balance" : lblhrreturamt.Text = "PI Return"
        '    GVTr.Columns(1).HeaderText = "No. PI"
        '    GVTr.Columns(2).HeaderText = "PI Date"
        '    GVTr.Columns(3).HeaderText = "Amt Nota PI"
        '    GVTr.Columns(4).HeaderText = "Accum PI"
        '    GVTr.Columns(5).HeaderText = "Retur PI+HR"
        '    GVTr.Columns(6).HeaderText = "Amt PI Balance"
        'End If
        If DDLtype.SelectedValue = "HR" Then
            lblNo.Visible = True : btnsearchinvoice.Visible = True : btneraseinvoice.Visible = True
            Label4.Visible = True : invoiceno.Visible = True
            Label5.Visible = True : Label9.Visible = True : trdate.Visible = True : Label6.Visible = True
            lblhramt.Visible = True : Label15.Visible = True : hramt.Visible = True
            lblhraccum.Visible = True : Label14.Visible = True : hraccum.Visible = True
            lblhrretur.Visible = True : Label13.Visible = True : hrretur.Visible = True
            lblhrbalance.Visible = True : Label10.Visible = True : hrbalance.Visible = True
            lblhrreturamt.Text = "HR Return"
        Else
            lblNo.Visible = False : btnsearchinvoice.Visible = False : btneraseinvoice.Visible = False
            Label4.Visible = False : invoiceno.Visible = False
            Label5.Visible = False : Label9.Visible = False : trdate.Visible = False : Label6.Visible = False
            lblhramt.Visible = False : Label15.Visible = False : hramt.Visible = False
            lblhraccum.Visible = False : Label14.Visible = False : hraccum.Visible = False
            lblhrretur.Visible = False : Label13.Visible = False : hrretur.Visible = False
            lblhrbalance.Visible = False : Label10.Visible = False : hrbalance.Visible = False
            lblhrreturamt.Text = "PI Return"
        End If
        'Clear
        suppoid.Text = "" : suppname.Text = "" : gvCustomer.Visible = False
        ClearNota() : ClearDetail() : gvBeli.Visible = False
    End Sub

    Protected Sub GVTr_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub hrreturamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hrreturamt.TextChanged
        'hrreturamt.Text = ToMaskEdit(hrreturamt.Text, 4)
        If ToDouble(hrreturamt.Text) > ToDouble(hrbalance.Text) Then
            showMessage("Amount Retur tidak boleh lebih dari Amount HR Balance!", 2)
            Exit Sub
        End If
    End Sub

#End Region

    Protected Sub btnsearchPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataPI()
    End Sub

    Protected Sub btnerasePI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
        gvBeli.Visible = False
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                Dim dtlTable As DataTable = New DataTable("TabelRetur")
                dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("trnbelimstoid", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("trnbelino", Type.GetType("System.String"))
                dtlTable.Columns.Add("trnbelidtlamt", Type.GetType("System.Double"))
                dtlTable.Columns.Add("trnnotahrreturdtlamt", Type.GetType("System.Double"))
                dtlTable.Columns.Add("trnnotahrreturdtlnote", Type.GetType("System.String"))
                Session("TblDtl") = dtlTable
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "trnbelimstoid=" & trnbelimstoid.Text & " "
            Else
                dv.RowFilter = "trnbelimstoid=" & trnbelimstoid.Text & " AND seq<>" & seq.Text & " "
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah ditambahkan sebelumnya, Silahkan cek!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                seq.Text = objTable.Rows.Count + 1
                objRow("seq") = seq.Text
            Else
                objRow = objTable.Rows(seq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("trnbelimstoid") = trnbelimstoid.Text
            objRow("trnbelino") = trnbelino.Text
            objRow("trnbelidtlamt") = ToDouble(lblmaxdtl2.Text)
            objRow("trnnotahrreturdtlamt") = ToDouble(returamt.Text)
            objRow("trnnotahrreturdtlnote") = notedtl.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            CountTotalAmt()
            EnableHeader(False)
        End If
    End Sub

    Protected Sub gvBeli_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBeli.PageIndexChanging
        gvBeli.PageIndex = e.NewPageIndex
        BindDataPI()
    End Sub

    Protected Sub gvBeli_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBeli.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
        End If
    End Sub

    Protected Sub gvBeli_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBeli.SelectedIndexChanged
        Try
            trnbelino.Text = gvBeli.SelectedDataKey("trnnotahrno").ToString
            trnbelimstoid.Text = gvBeli.SelectedDataKey.Item("trnnotahroid")
            If DDLtype.SelectedValue <> "HR" Then
                lblmaxdtl.Text = "MAX = " & ToMaskEdit(ToDouble(gvBeli.SelectedDataKey.Item("saldohr")), 3) & ""
                lblmaxdtl2.Text = ToMaskEdit(ToDouble(gvBeli.SelectedDataKey.Item("trnnotahramtnetto")), 3)
            Else
                lblmaxdtl2.Text = ToMaskEdit(ToDouble(gvBeli.SelectedDataKey.Item("trnnotahrdtlamt")), 3)
            End If

            gvBeli.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub returamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles returamt.TextChanged
        returamt.Text = ToMaskEdit(returamt.Text, 3)
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim iGtotal As Decimal
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            iGtotal += ToDouble(objTable.Rows(C1)("trnnotahrreturdtlamt"))
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
        hrreturamt.Text = ToMaskEdit(ToDouble(iGtotal), 3)
        If gvDtl.Rows.Count = 0 Then
            EnableHeader(True)
        End If

    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            seq.Text = gvDtl.SelectedDataKey.Item("seq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "seq=" & seq.Text
                trnbelimstoid.Text = dv.Item(0).Item("trnbelimstoid")
                trnbelino.Text = dv.Item(0).Item("trnbelino").ToString
                returamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("trnnotahrreturdtlamt")), 3)
                lblmaxdtl2.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("trnbelidtlamt")), 3)
                lblmaxdtl.Text = "MAX = " & ToMaskEdit(ToDouble(dv.Item(0).Item("trnbelidtlamt")), 3) & ""
                notedtl.Text = Tchar(dv.Item(0).Item("trnnotahrreturdtlnote").ToString)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub
    
    Protected Sub btnClearDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDtl.Click
        ClearDetail()
    End Sub
End Class