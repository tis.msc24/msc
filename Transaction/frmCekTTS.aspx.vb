Imports System.Data
Imports System.Data.SqlClient
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure

Partial Class frmCekTTS
    Inherits System.Web.UI.Page
    Dim sSql As String = ""
    Dim CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cmd As New SqlCommand("", conn)
    Dim cKon As New Koneksi
    Dim cFungsi As New ClassFunction
    Dim cProc As New ClassProcedure

    Private Sub CabDDl()
        sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangDDL, sSql)
            Else
                FillDDL(CabangDDL, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(CabangDDL, sSql)
            CabangDDL.Items.Add(New ListItem("SEMUA BRANCH", "ALL"))
            CabangDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub showMessage(ByVal isi As String, ByVal judul As String)
        isiPesan.Text = isi
        captionPesan.Text = judul
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        '' jangan lupa cek hak akses
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSDO")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSDO") = sqlSearch
            Response.Redirect("~/Transaction/frmCekTTS.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Cek Histori Penerimaan"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        If Not Page.IsPostBack Then
            CabDDl()
            tglAwal2.Text = Format(CDate(DateTime.Now.ToShortDateString()), "dd/MM/yyyy")
            'TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Private Sub DataBindTrn()
        If ordermstoid.Text <> "" Then
            sSql = "Select * FROM (" & _
" Select con.branch_code,gc.gendesc,jm.trnbelimstoid trnOid,jm.trnbelino notrans,Convert(VarChar(20),jm.trnbelidate,103) trnDate,con.amttransidr trnAmtnya,con.amtbayaridr amtBayarNya From QL_trnbelimst jm INNER JOIN QL_conap con ON jm.trnbelimstoid=con.refoid AND con.reftype='QL_trnbelimst' INNER JOIN ql_mstgen gc ON gc.gencode=jm.branch_code AND gc.gengroup='CABANG' UNION ALL Select py.branch_code,gc.gendesc,con.refoid trnOid,cb.cashbankno notrans,Convert(VarChar(20),cb.cashbankdate,103) trnDate,con.amttransidr trnAmtnya,con.amtbayaridr amtBayarNya From ql_trnpayap py INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid=py.cashbankoid AND cb.branch_code=py.branch_code INNER JOIN QL_conap con ON con.payrefoid=py.paymentoid AND con.branch_code=py.branch_code AND con.reftype='ql_trnpayap' INNER JOIN ql_mstgen gc ON gc.gencode=con.branch_code AND gc.gengroup='CABANG' AND con.trnapstatus='POST' UNION ALL Select con.branch_code,gc.gendesc,con.refoid trnOid,con.payrefno notrans,Case con.trnapdate When '1/1/1900' Then Convert(VarChar(20),con.paymentdate,103) Else Convert(VarChar(20),con.trnapdate,103) End trnDate,con.amttransidr trnAmtnya,con.amtbayaridr amtBayarNya From QL_conap con INNER JOIN QL_mstgen gc ON gencode=con.branch_code AND gengroup='CABANG' AND con.reftype<>'QL_trnbelimst' AND con.trnaptype<>'PAYAP' AND trnaptype<>'APK'" & _
" ) tr Where trnoid=" & ordermstoid.Text & " AND branch_code='" & CabangDDL.SelectedValue & "'"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
            Session("TblListMat") = dt
            gvListNya.DataSource = Session("TblListMat")
            gvListNya.DataBind() : gvListNya.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Maaf, Pilih Nomer nota (SI)", CompnyName)
        End If
    End Sub

    Private Sub bindData()
        Dim cBang As String = ""
        If CabangDDL.SelectedValue <> "ALL" Then
            cBang = "And rq.branch_code='" & CabangDDL.SelectedValue & "'"
        End If
        sSql = "Select rq.branch_code,Convert(Varchar(20),rq.reqdate,103) ReqDate,cb.gendesc,rq.reqoid,rq.reqcode,cu.custoid,cu.custname,Case rq.reqservicecat When 'M' Then 'MUTATION' Else 'Service' End TypeTTS from QL_TRNREQUEST rq Inner Join QL_mstcust cu ON cu.custoid=rq.reqcustoid AND cu.branch_code=rq.branch_code Inner Join QL_mstgen cb ON cb.gencode=rq.branch_code AND cb.gengroup='CABANG' Where reqstatus IN ('POST','APPROVED') " & cBang & " AND (rq.reqcode LIKE '%" & Tchar(txtorderNo.Text) & "%' Or cu.custname LIKE '%" & Tchar(txtorderNo.Text) & "%') Order By reqdate"
        FillGV(gvBPM2, sSql, "QL_Trnbeli")
        gvBPM2.Visible = True
    End Sub

    Private Sub bindItem()
        Try
            sSql = "SELECT DISTINCT * FROM (Select 1 q, con.conrefoid, gc.gendesc, FormAction, i.itemcode, i.itemdesc, gd.gendesc gudang, qtyIn, qtyOut, Convert(Varchar(20),con.trndate,103) trndate, rq.snno, rq.reqdtljob, con.updtime, rqm.reqoid From QL_conmtr con Inner Join QL_TRNREQUESTDTL rq ON reqdtloid=con.conrefoid Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=rq.reqmstoid Inner Join QL_mstitem i ON i.itemoid=con.refoid Inner Join QL_mstgen gc ON gc.gencode=con.branch_code AND gc.gengroup='CABANG' Inner Join QL_mstgen gd ON gd.genoid=con.mtrlocoid AND gd.gengroup='LOCATION' Where con.mtrlocoid NOT IN (-10,-9) AND con.Formname<>'QL_TRNINVOICEITEM' UNION ALL Select 2 q, reqdtloid conrfeoid, gc.gendesc, fm.trnfinalintno FormAction, i.itemcode, i.itemdesc, gd.gendesc gudang, qty QtyIn, 0.00 Qtyout, Convert(Varchar(20), fm.finaldate,103) trndate, ISNULL((Select snno from QL_TRNREQUESTDTL rd Where rd.reqdtloid=f3.reqdtloid),'') snno, ISNULL((Select reqdtljob from QL_TRNREQUESTDTL rd Where rd.reqdtloid=f3.reqdtloid),'') reqdtljob, f3.updtime, f3.reqoid From QL_trnfinalintdtl3 f3 Inner Join QL_trnfinalintmst fm ON fm.FINOID=f3.finoid Inner Join QL_mstitem i ON i.itemoid=f3.itemoid Inner Join QL_mstgen gd ON gd.genoid=f3.mtrlocoid AND gd.gengroup='LOCATION' Inner Join QL_mstgen gc ON f3.branch_code=gc.gencode AND gc.gengroup='CABANG' UNION ALL Select 3 q, f4.finaldtloid4 conrfeoid, gc.gendesc, fm.trnfinalintno FormAction, i.itemcode, i.itemdesc, gd.gendesc gudang, qty QtyIn, 0.00 Qtyout, Convert(Varchar(20),fm.finaldate,103) trndate, '' snno, '' reqdtljob, f4.updtime, f4.reqoid From QL_trnfinalintdtl4 f4 Inner Join QL_trnfinalintmst fm ON fm.FINOID=f4.finoid Inner Join QL_mstitem i ON i.itemoid=f4.itemoid Inner Join QL_mstgen gd ON gd.genoid=f4.mtrlocoid AND gd.gengroup='LOCATION' Inner Join QL_mstgen gc ON f4.branch_code=gc.gencode Where gc.gengroup='CABANG' AND fm.finitemstatus='APPROVED' UNION ALL Select 4 q, con.conrefoid, gc.gendesc, FormAction, (Select itemcode from QL_mstitem it Where con.refoid=it.itemoid) itemcode, (Select itemdesc from QL_mstitem i Where i.itemoid=con.refoid) itemdesc, gd.gendesc gudang, qtyIn, qtyOut, Convert(Varchar(20),con.trndate,103) trndate, ISNULL((Select snno from QL_TRNREQUESTDTL rd Where rd.reqdtloid=id.reqdtloid),'') snno, ISNULL((Select reqdtljob from QL_TRNREQUESTDTL rd Where rd.reqdtloid=id.reqdtloid),'') reqdtljob, con.updtime, im.MSTREQOID reqoid From QL_conmtr con Inner Join QL_TRNINVOICE im ON con.formoid=im.SOID INNER JOIN QL_TRNINVOICEITEM id ON id.soid=im.SOID AND con.refoid=Case When con.refoid<>id.itemoidtts Then id.itemreqoid Else id.itemoidtts End INNER JOIN QL_mstgen gc ON gc.gencode=con.branch_code AND gc.gengroup='CABANG' INNER JOIN QL_mstgen gd ON gd.genoid=con.mtrlocoid AND gd.gengroup='LOCATION' Where con.Formname='QL_TRNINVOICEITEM') con Where reqoid=" & ordermstoid.Text & " Order by conrefoid, con.updtime ASC"
            FillGV(gv_item, sSql, "ql_trnordermstdtl")
            gv_item.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, "ERROR")
            Exit Sub
        End Try
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim x As Integer = 123
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, False)
    End Sub

    Protected Sub imbSearchBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindData()
    End Sub

    Protected Sub imbEraseBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseBPM.Click
        txtorderNo.Text = "" : ordermstoid.Text = ""
        custname.Text = ""
        gvBPM2.Visible = False : gv_item.Visible = False
        gv_item.SelectedIndex = -1
        gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBPM2.RowDataBound
         
    End Sub

    Protected Sub gvBPM2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBPM2.SelectedIndexChanged
        txtorderNo.Text = gvBPM2.SelectedDataKey.Item("reqcode")
        ordermstoid.Text = gvBPM2.SelectedDataKey.Item("reqoid")
        tglAwal2.Text = gvBPM2.SelectedDataKey.Item("ReqDate")
        custname.Text = gvBPM2.SelectedDataKey.Item("custname")
        bindItem() : gvBPM2.Visible = False : gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBPM2.PageIndexChanging
        gvBPM2.PageIndex = e.NewPageIndex
        bindData()
    End Sub

    Protected Sub gv_item_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_item.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListNya_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub imbSearchBPM_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchBPM.Click
        bindData()
    End Sub
End Class
