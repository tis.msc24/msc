<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptNotaExpedisi.aspx.vb" Inherits="rptNotaExpedisi" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="text-align: left;">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".:  LAPORAN NOTA EXPEDISI"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" class="header" style="background-color: transparent; text-align: center"
                valign="center">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD align=left><asp:Label id="Label5" runat="server" Width="42px" Text="Cabang"></asp:Label></TD><TD>:</TD><TD align=left><asp:DropDownList id="BranchDDL" runat="server" Width="157px" CssClass="inpText">
            </asp:DropDownList></TD></TR><TR><TD align=left>Periode</TD><TD>:</TD><TD align=left><asp:TextBox id="date1" runat="server" Width="60px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="search1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to"></asp:Label> <asp:TextBox id="date2" runat="server" Width="58px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="search2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="(dd/mm/yyyy)"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Width="76px" Text="Type Nota"></asp:Label></TD><TD><asp:Label id="Label6" runat="server" Text=":"></asp:Label></TD><TD align=left><asp:DropDownList id="TypeNotaDDL" runat="server" Width="157px" CssClass="inpText" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>NON EXPEDISI</asp:ListItem>
<asp:ListItem>EXPEDISI</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>No. Nota</TD><TD>:</TD><TD align=left><asp:TextBox id="NotaNya" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="sNotaNo" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="eNotanya" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="OidNota" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD align=left><asp:GridView id="GvNota" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" Visible="False" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnbiayaeksoid,trnbiayaeksno" AllowPaging="True" PageSize="8">
                <RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True">
                        <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
                    </asp:CommandField>
                    <asp:BoundField DataField="trnbiayaeksno" HeaderText="No. Nota">
                        <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="typenota" HeaderText="Type">
                        <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="trnbiayaeksoid" Visible="False" />
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Red" HorizontalAlign="Right" />
                <EmptyDataTemplate>
                    <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView> </TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Width="42px" Text="Customer"></asp:Label></TD><TD><asp:Label id="Label4" runat="server" Text=":"></asp:Label></TD><TD align=left><asp:TextBox id="CustName" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="sCusTomer" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="eCusTomer" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="CustOid" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD align=left><asp:GridView id="GVCustomer" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" Visible="False" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="custoid,custname" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="custname" HeaderText="Cust. Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="noCB" runat="server" Width="101px" Text="No. Bank/Cash"></asp:Label></TD><TD><asp:Label id="Label9" runat="server" Width="2px" Text=":"></asp:Label></TD><TD align=left><asp:TextBox id="CbNo" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="sCbNo" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="eCbNo" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="CbOId" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD align=left><asp:GridView id="gvCbNo" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" Visible="False" GridLines="None" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="cashbankoid,cashbankno" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Cash/Bank">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanktype" HeaderText="Type">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                    <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR>
    <tr>
        <td align="left">
            Status Retur</td>
        <td>
            :</td>
        <td align="left">
            <asp:DropDownList id="DDLStaturRetur" runat="server" Width="157px" CssClass="inpText">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem Value="POST">POST</asp:ListItem>
                <asp:ListItem Value="In Approval">IN APPROVAL</asp:ListItem>
                <asp:ListItem>APPROVED</asp:ListItem>
                <asp:ListItem>Rejected</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <TR><TD colSpan=3><asp:ImageButton id="BtnViewRpt" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ToPDF" onclick="ToPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbExport" onclick="imbExport_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" MaskType="Date" Mask="99/99/9999" TargetControlID="date2"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" MaskType="Date" Mask="99/99/9999" TargetControlID="date1"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="Date2" PopupButtonID="search2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="Date1" PopupButtonID="search1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE> <asp:Label id="labelmsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD align=center colSpan=3><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> 
</ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
                        <asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

