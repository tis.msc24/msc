<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnSalesReturnTB.aspx.vb" Inherits="Transaction_trnSalesReturnTB" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Maroon" Text=".: Sales Return Barang" Width="384px"></asp:Label>
            </th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px; vertical-align: text-top;">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; :: List Of Sales
                                Return</span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Filter</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilter" runat="server" Width="123px" CssClass="inpText" __designer:wfdid="w18"><asp:ListItem Value="a.trnjualreturno">No. Nota</asp:ListItem>
<asp:ListItem Value="b.custname">Customer</asp:ListItem>
</asp:DropDownList>&nbsp;<SPAN style="COLOR: #ff0000"><asp:TextBox id="tbfilter" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w19"></asp:TextBox>&nbsp;&nbsp; </SPAN></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Periode</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="64px" CssClass="inpText" __designer:wfdid="w20"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w21"></asp:ImageButton>&nbsp;to <asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="64px" CssClass="inpText" __designer:wfdid="w22"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w23"></asp:ImageButton> &nbsp; &nbsp;<SPAN style="FONT-SIZE: 11px; COLOR: red"><SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN></SPAN></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Status</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilterstatus" runat="server" Width="123px" CssClass="inpText" __designer:wfdid="w24"><asp:ListItem Selected="True" Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="ibfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top" __designer:wfdid="w25"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top" __designer:wfdid="w26"></asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD colSpan=3><asp:GridView id="GVTrn" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w16" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trnjualreturmstoid" DataNavigateUrlFormatString="trnSalesReturnTB.aspx?oid={0}" DataTextField="trnjualreturno" HeaderText="No Nota">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnjualdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualstatus" HeaderText="Status">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" __designer:wfdid="w31" ToolTip='<%# String.Format("{0},{1}",Eval("trnjualreturmstoid"),Eval("trnjualreturno")) %>' Font-Underline="True">Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="35px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="35px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="tbperiodstart" MaskType="Date" Mask="99/99/9999" __designer:wfdid="w27"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="tbperiodend" MaskType="Date" Mask="99/99/9999" __designer:wfdid="w28"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender10" runat="server" TargetControlID="tbperiodstart" PopupButtonID="ibperiodstart" Format="dd/MM/yyyy" __designer:wfdid="w29"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="tbperiodend" PopupButtonID="ibperiodend" Format="dd/MM/yyyy" __designer:wfdid="w30"></ajaxToolkit:CalendarExtender> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVTrn"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />
                                :: Form of Sales Return</span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=3><asp:MultiView id="MultiView1" runat="server"><asp:View id="View1" runat="server"><TABLE width="100%"><TBODY><TR><TD colSpan=7><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label> <asp:Label id="Label5" runat="server" ForeColor="#585858" Text="|"></asp:Label> <asp:LinkButton id="lbviewdetail" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">More Information</asp:LinkButton></TD></TR><TR><TD colSpan=7><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender30" runat="server" TargetControlID="returndate" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="returndate" PopupButtonID="btnreturndate" Format="dd/MM/yyyy">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="discheader" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft">
                                                                            </ajaxToolkit:MaskedEditExtender> <asp:Label id="masterstate" runat="server" Visible="False">new</asp:Label></TD></TR><TR><TD colSpan=7><asp:Label id="Label9" runat="server" Font-Bold="True" Text=".: Barang Yang Sudah Diretur (IN)" __designer:wfdid="w110" Font-Underline="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 34px">No. Return TB</TD><TD style="HEIGHT: 34px">:</TD><TD style="HEIGHT: 34px"><asp:TextBox id="noreturn" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False"></asp:TextBox></TD><TD style="HEIGHT: 34px">Return TB&nbsp;Date</TD><TD style="HEIGHT: 34px">:</TD><TD style="HEIGHT: 34px"><asp:TextBox id="returndate" runat="server" Width="70px" CssClass="inpText" MaxLength="10"></asp:TextBox> <asp:ImageButton id="btnreturndate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label8" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label></TD><TD style="HEIGHT: 34px"><asp:Label id="paytype" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD>Customer</TD><TD>:</TD><TD><asp:TextBox id="custname" runat="server" Width="200px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btncustomer" onclick="btncustomer_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnerasercustomer" onclick="btnerasercustomer_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD><TD>Retur&nbsp;Date</TD><TD>:</TD><TD><asp:TextBox id="trdate" runat="server" Width="70px" CssClass="inpTextDisabled" __designer:wfdid="w84" MaxLength="20" Enabled="False"></asp:TextBox> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w85"></asp:Label></TD><TD><asp:Label id="lblcurr" runat="server" Visible="False" __designer:wfdid="w86"></asp:Label></TD></TR><TR><TD></TD><TD></TD><TD colSpan=5><asp:GridView id="gvCustomer" runat="server" Width="100%" ForeColor="#333333" Visible="False" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" EmptyDataText="No data in database." DataKeyNames="trncustoid,custcode,trncustname" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="CUSTCODE" HeaderText="User ID" ReadOnly="True" SortExpression="CUSTCODE">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Person Name" SortExpression="trncustname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label 
        ID="lblstatusdataCust" runat="server" CssClass="Important" 
        Text="No Customer Data !"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD>No Retur</TD><TD>:</TD><TD><asp:TextBox id="invoiceno" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchinvoice" onclick="btnsearchinvoice_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btneraseinvoice" onclick="btneraseinvoice_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="sjno" runat="server" Visible="False"></asp:Label> <asp:Label id="sono" runat="server" Visible="False"></asp:Label> <asp:Label id="sino" runat="server" Visible="False"></asp:Label> </TD><TD>Type</TD><TD>:</TD><TD><asp:DropDownList id="DDLtype" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w1" Enabled="False"><asp:ListItem Value="1">Potong Invoice</asp:ListItem>
<asp:ListItem Value="2">Tukar Barang Sama</asp:ListItem>
<asp:ListItem Value="3">Tukar Beda Barang</asp:ListItem>
</asp:DropDownList></TD><TD></TD></TR><TR><TD></TD><TD></TD><TD colSpan=5><asp:GridView id="GVTr" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="trnjualreturmstoid,orderno,trncustoid,currencyoid,typeretur,trnpaytype" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualreturno" HeaderText="Retur No">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="refnotaretur" HeaderText="Invoice No.">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="Sales Order No">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Invoice Date">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail !!" __designer:wfdid="w75"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD></TD><TD></TD><TD colSpan=5><asp:GridView id="GVretur" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w83" DataKeyNames="itemoid,itemcode,itemdesc,merk,trnjualdtlqty,unit" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang ">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdtlqty" DataFormatString="{0:#,##0.00}" HeaderText="Qty Retur">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
                                                                                        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=7><asp:Label id="Label1" runat="server" Font-Bold="True" Text=".: Barang Yang Di tukar (OUT)" __designer:wfdid="w110" Font-Underline="True"></asp:Label></TD></TR><TR><TD>Item</TD><TD>:</TD><TD><asp:TextBox id="itemname" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w76" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchitem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w77"></asp:ImageButton> <asp:ImageButton id="btneraseitem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w78"></asp:ImageButton> <asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w79"></asp:Label> <asp:Label id="itemcode" runat="server" Visible="False" __designer:wfdid="w80"></asp:Label> <asp:Label id="detailstate" runat="server" Visible="False" __designer:wfdid="w8">new</asp:Label></TD><TD>Location</TD><TD>:</TD><TD><asp:DropDownList id="fromlocation" runat="server" Width="193px" CssClass="inpText" __designer:wfdid="w81">
                                                                            </asp:DropDownList></TD><TD><asp:Label id="itemloc" runat="server" Visible="False" __designer:wfdid="w82"></asp:Label></TD></TR><TR><TD></TD><TD></TD><TD colSpan=5><asp:GridView id="GVItem" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w83" DataKeyNames="itemoid,satuan1,unitseq,trnjualdtlprice,trnjualdtloid,itemloc" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty Stock">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
                                                                                        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:GridView id="GVBB" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w83" OnSelectedIndexChanged="GVBB_SelectedIndexChanged" DataKeyNames="itemoid,satuan1,itemloc" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricelist" HeaderText="Pricelist">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" DataFormatString="{0:#,##0.00}" HeaderText="Qty Stock">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
                                                                                        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD>Quantity</TD><TD></TD><TD><asp:TextBox id="qty" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w112" MaxLength="15" AutoPostBack="True">0.00</asp:TextBox>&nbsp;Max: <asp:Label id="maxqty" runat="server" __designer:wfdid="w113">0.00</asp:Label> <asp:Label id="unit" runat="server" __designer:wfdid="w114"></asp:Label> <asp:Label id="satuan1" runat="server" Visible="False" __designer:wfdid="w115"></asp:Label></TD><TD>Price</TD><TD></TD><TD><asp:TextBox id="priceperunit" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w116" MaxLength="15" Enabled="False" AutoPostBack="True">0.00</asp:TextBox></TD><TD><asp:Label id="unitseq" runat="server" Visible="False" __designer:wfdid="w117"></asp:Label> <asp:Label id="trnjualdtloid" runat="server" Visible="False" __designer:wfdid="w118"></asp:Label></TD></TR><TR><TD>Amount</TD><TD></TD><TD><asp:TextBox id="totalamountdtl" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w122" MaxLength="20" Enabled="False">0.00</asp:TextBox>&nbsp;<asp:Label id="labelseq" runat="server" Visible="False" __designer:wfdid="w125"></asp:Label></TD><TD></TD><TD></TD><TD><asp:TextBox id="netamount" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w123" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD></TD></TR><TR><TD>Note</TD><TD></TD><TD><asp:TextBox id="notedtl" runat="server" Width="400px" CssClass="inpText" Visible="False" __designer:wfdid="w124" MaxLength="200"></asp:TextBox></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR><TR><TD></TD><TD></TD><TD><asp:TextBox id="promodisc" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w129" MaxLength="20" Enabled="False">0.00</asp:TextBox> <asp:TextBox id="promodiscamt" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w130" MaxLength="20" Enabled="False">0.00</asp:TextBox> <asp:Label id="lbldate" runat="server" Visible="False" __designer:wfdid="w131"></asp:Label></TD><TD></TD><TD></TD><TD><asp:ImageButton id="btnaddtolist" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsBottom" __designer:wfdid="w127"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnclear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsBottom" __designer:wfdid="w128"></asp:ImageButton></TD><TD></TD></TR><TR><TD colSpan=7><asp:GridView id="GVDtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w126" DataKeyNames="itemoid,netamt,unitoid,maxqty,itemcode,unitseq,trnjualdtloid,locationoid,promodisc,promodiscamt" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No ">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemname" HeaderText="Item">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="price" DataFormatString="{0:#,##0.00}" HeaderText="Price">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="promodiscamt" DataFormatString="{0:#,##0.00}" HeaderText="Promo Disc.">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="netamt" DataFormatString="{0:#,##0.00}" HeaderText="Amount">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="5px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
                                                                                        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD><asp:DropDownList id="discheadertype" runat="server" Width="50px" CssClass="inpText" Visible="False" __designer:wfdid="w4" AutoPostBack="True">
                                                                                <asp:ListItem 
                        Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem 
                        Value="PCT">PCT</asp:ListItem>
                                                                            </asp:DropDownList><asp:TextBox id="totaldischeader" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w5" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD></TD></TR><TR><TD>Note</TD><TD>:</TD><TD><asp:TextBox id="note" runat="server" Width="408px" CssClass="inpText" __designer:wfdid="w3" MaxLength="200"></asp:TextBox></TD><TD>Status</TD><TD>:</TD><TD><asp:TextBox id="status" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w2" MaxLength="20" Enabled="False">In Process</asp:TextBox></TD><TD></TD></TR><TR><TD></TD><TD></TD><TD><asp:TextBox id="txtcurr" runat="server" Width="128px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox></TD><TD></TD><TD></TD><TD><asp:TextBox id="noref" runat="server" Width="125px" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD><TD></TD></TR><TR><TD></TD><TD></TD><TD><asp:TextBox id="grossreturn" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False" Wrap="False">0.00</asp:TextBox> <asp:Label id="amtdiscdtl" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD></TD><TD><asp:TextBox id="taxamount" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox> <asp:Label id="taxpct" runat="server" Visible="False"></asp:Label></TD><TD></TD></TR><TR><TD></TD><TD></TD><TD><asp:TextBox id="totalamount" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD></TD><TD></TD><TD><asp:TextBox id="discheader" runat="server" Width="125px" CssClass="inpText" Visible="False" __designer:wfdid="w6" MaxLength="15" AutoPostBack="True">0.00</asp:TextBox><asp:TextBox id="discamount" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w7" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server"><TABLE width="100%"><TBODY><TR><TD colSpan=4><asp:LinkButton id="lbviewinfo" runat="server" CssClass="submenu" Font-Size="Small">Information</asp:LinkButton> <asp:Label id="Label3" runat="server" ForeColor="#585858" Text="|"></asp:Label> <asp:Label id="Label4" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="More Information"></asp:Label></TD></TR><TR><TD colSpan=4><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" TargetControlID="qty" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender5" runat="server" TargetControlID="priceperunit" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender6" runat="server" TargetControlID="disc1" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender7" runat="server" TargetControlID="disc2" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender8" runat="server" TargetControlID="disc3" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="WIDTH: 132px"><asp:TextBox id="poprice" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD><asp:DropDownList id="disc1type" runat="server" Width="50px" CssClass="inpText" Visible="False" AutoPostBack="True">
                                                                                <asp:ListItem 
                        Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem 
                        Value="PCT">PCT</asp:ListItem>
                                                                            </asp:DropDownList>&nbsp;<asp:TextBox id="disc1" runat="server" Width="125px" CssClass="inpText" Visible="False" MaxLength="15" AutoPostBack="True">0.00</asp:TextBox> <asp:DropDownList id="disc2type" runat="server" Width="50px" CssClass="inpText" Visible="False" AutoPostBack="True">
                                                                                <asp:ListItem 
                        Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem 
                        Value="PCT">PCT</asp:ListItem>
                                                                            </asp:DropDownList>&nbsp;<asp:TextBox id="disc2" runat="server" Width="125px" CssClass="inpText" Visible="False" MaxLength="15" AutoPostBack="True">0.00</asp:TextBox></TD><TD><asp:TextBox id="discamount1" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD><asp:TextBox id="totaldisc" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD style="WIDTH: 132px"></TD><TD><asp:DropDownList id="disc3type" runat="server" Width="50px" CssClass="inpText" Visible="False" AutoPostBack="True">
                                                                                <asp:ListItem 
                        Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem 
                        Value="PCT">PCT</asp:ListItem>
 </asp:DropDownList>&nbsp;<asp:TextBox id="disc3" runat="server" Width="125px" CssClass="inpText" Visible="False" MaxLength="15" AutoPostBack="True">0.00</asp:TextBox> <asp:TextBox id="discamount2" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD><asp:TextBox id="discamount3" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD><asp:TextBox id="gudangreturqty" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD colSpan=4><asp:Label id="costamount" runat="server" Visible="False">0</asp:Label> <asp:Label id="cashamount" runat="server" Visible="False">0</asp:Label> <asp:Label id="returamount" runat="server" Visible="False"></asp:Label> <asp:Label id="amtppn" runat="server" Visible="False"></asp:Label> <asp:Label id="amtpiutang" runat="server" Visible="False"></asp:Label> <asp:Label id="returpiutangamount" runat="server" Visible="False">0.0</asp:Label> <asp:Label id="amtpotpenjualan" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView></TD></TR><TR><TD colSpan=3><asp:Label id="lblcreate" runat="server"></asp:Label> <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibsave" runat="server" ImageUrl="~/Images/Save.png" CausesValidation="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" onclick="ibdelete_Click" runat="server" ImageUrl="~/Images/Delete.png" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibapproval" runat="server" ImageUrl="~/Images/sendapproval.png" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibposting" runat="server" ImageUrl="~/Images/posting.png" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPrint" runat="server" ImageUrl="~/Images/print.png" CausesValidation="False" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="uppopupmsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                            <asp:Label ID="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" colspan="2"></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></td>
                        <td style="TEXT-ALIGN: left" class="Label">
                            <asp:Label ID="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" colspan="2">&nbsp;<asp:ImageButton ID="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpepopupmsg" runat="server" TargetControlID="bepopupmsg" Drag="True" PopupDragHandleControlID="lblcaption" BackgroundCssClass="modalBackground" PopupControlID="pnlpopupmsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bepopupmsg" runat="server" BorderColor="Transparent" BorderStyle="None" BackColor="Transparent" Visible="False" CausesValidation="False"></asp:Button>

        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="UpdatePanelSN" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlInvnSN" runat="server" Width="600px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="LabelDescSn" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Enter SN of Item"></asp:Label> </TD></TR><TR><TD><asp:Label id="NewSN" runat="server" ForeColor="Red" Text="New SN"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Item &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; : <asp:TextBox id="TextItem" runat="server" Width="195px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp;<asp:Label id="KodeItem" runat="server" Visible="False"></asp:Label> &nbsp;<asp:Label id="SNseq" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Qty Item &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; : <asp:TextBox id="TextQty" runat="server" Width="69px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp; <asp:TextBox id="TextSatuan" runat="server" Width="69px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Scan Serial Number &nbsp; &nbsp;&nbsp; : <asp:TextBox id="TextSN" onkeypress="return EnterEvent(event)" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> &nbsp; <asp:ImageButton id="EnterSN" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR></TBODY><CAPTION><DT style="TEXT-ALIGN: center"></DT></CAPTION><TBODY><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV style="TEXT-ALIGN: center" id="Div2"><STRONG><asp:Label id="LabelPesanSn" runat="server" Font-Size="Larger" Font-Bold="True" ForeColor="Red"></asp:Label> </STRONG></DIV><DIV></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="GVSN" runat="server" Width="100%" CssClass="MyTabStyle" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" OnRowDeleting="GVSN_RowDeleting" EnableModelValidation="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemoid" HeaderText="Itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="SN" HeaderText="Serial Number">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="450px"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 15px; TEXT-ALIGN: center" align=left><asp:LinkButton id="lkbCloseSN" runat="server" Font-Size="X-Small">[ Close ]</asp:LinkButton> &nbsp;&nbsp; <asp:LinkButton id="lkbPilihItem" runat="server" Font-Size="X-Small">[ OK ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupSN" runat="server" TargetControlID="btnHideSN" PopupControlID="UpdatePanelSN" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptInvtry" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideSN" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="TextSN"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    &nbsp;&nbsp;
        
</asp:Content>

