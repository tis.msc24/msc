﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="expedisibranch.aspx.vb" Inherits="Master_expedisibranch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Expedisi Branch" CssClass="Title" Font-Size="X-Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle" __designer:wfdid="w91"></asp:Image> <STRONG><SPAN style="FONT-SIZE: 9pt">List of Expedisi Branch :.</SPAN></STRONG> 
</HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel2" runat="server" __designer:wfdid="w92"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w93" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 100px" class="Label" align=left>Jalur Pengiriman </TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="dd_jalur" runat="server" Width="142px" Height="16px" CssClass="inpText" __designer:wfdid="w94" AutoPostBack="True">

                                                            <asp:ListItem Selected="True" Value="All">All</asp:ListItem>
                                                            <asp:ListItem>Darat</asp:ListItem>
                                                            <asp:ListItem>Laut</asp:ListItem>
                                                            <asp:ListItem>Udara</asp:ListItem>

                                                        </asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 74px" class="Label" align=left><asp:CheckBox id="cbBranch" runat="server" Text="Branch" __designer:wfdid="w95"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDL" runat="server" Width="142px" Height="16px" CssClass="inpText" __designer:wfdid="w96"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w97"></asp:ImageButton> <asp:ImageButton style="HEIGHT: 23px" id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w98"></asp:ImageButton> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvMst" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w89" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" GridLines="None" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="id_expedisi" DataNavigateUrlFormatString="~/Master/expedisibranch.aspx?oid={0}" DataTextField="nmr" HeaderText="No" SortExpression="nmr">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="#804000" Width="5px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="idbranchcode" HeaderText="BrancCodeID" Visible="False"></asp:BoundField>
<asp:BoundField DataField="branchcode" HeaderText="Branch Code" Visible="False"></asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Branch" SortExpression="gendesc">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="expedisi_type" HeaderText="Expedisi" SortExpression="expedisi_type">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="panjang" HeaderText="Panjang">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lebar" HeaderText="Lebar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tinggi" HeaderText="Tinggi">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="price" HeaderText="Tarif" SortExpression="price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w100"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle" __designer:wfdid="w138"></asp:Image> <STRONG><SPAN style="FONT-SIZE: 9pt">Form Ekspedisi Branch :.</SPAN></STRONG> 
</HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server" __designer:wfdid="w139"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 111px" class="Label" align=left><asp:Label id="I_U" runat="server" Width="78px" CssClass="Important" Text="New Data" __designer:wfdid="w140"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="rate2oid" runat="server" __designer:wfdid="w141" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 111px" id="TD3" class="Label" align=left runat="server" visible="true"><asp:Label id="Label5" runat="server" Width="80px" __designer:wfdid="w142">Branch Name</asp:Label></TD><TD id="TD2" class="Label" align=center runat="server" visible="true">:</TD><TD id="TD1" class="Label" align=left runat="server" visible="true"><asp:DropDownList id="branchid" runat="server" CssClass="inpText" __designer:wfdid="w143"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 111px">Jenis Expedisi</TD><TD>:</TD><TD><asp:DropDownList id="dd_type" runat="server" CssClass="inpText" __designer:wfdid="w144" AutoPostBack="True"><asp:ListItem Selected="True">Darat</asp:ListItem>
<asp:ListItem>Laut</asp:ListItem>
<asp:ListItem>Udara</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left><LABEL><SPAN style="TEXT-DECORATION: underline"><STRONG>Dimensi Barang</STRONG></SPAN></LABEL></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left>Nama Item</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="itemdesc" runat="server" Width="156px" CssClass="inpText" __designer:wfdid="w145" MaxLength="255"></asp:TextBox> <asp:ImageButton id="btnSitem" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w146"></asp:ImageButton> <asp:ImageButton id="ibtneraseitem" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w147"></asp:ImageButton> <asp:Label id="itemoid" runat="server" __designer:wfdid="w148" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:GridView style="MARGIN-TOP: 0px" id="gvItemSearch" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w149" DataKeyNames="itemoid,itemcode,itemdesc,dimensi_p,dimensi_l,dimensi_t,beratvolume,beratbarang" PageSize="8" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="True" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dimensi_p" HeaderText="Panjang">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dimensi_l" HeaderText="Lebar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dimensi_t" HeaderText="Tinggi">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratvolume" HeaderText="Berat Volume">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratbarang" HeaderText="Berat Barang">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="lblstatusitemdata" runat="server" CssClass="Important" Text="No Item data !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left><LABEL>Panjang</LABEL></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txt_panjang" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w150" AutoPostBack="True" MaxLength="4" Enabled="False"></asp:TextBox> <asp:Label style="COLOR: #cc3300" id="Label6" runat="server" Text="*" __designer:wfdid="w151"></asp:Label> </TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left><LABEL>Lebar</LABEL></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txt_lebar" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w152" AutoPostBack="True" MaxLength="4" Enabled="False"></asp:TextBox> <asp:Label style="COLOR: #cc3300" id="Label7" runat="server" Text="*" __designer:wfdid="w153"></asp:Label> </TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left><LABEL>Tinggi</LABEL></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txt_tinggi" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w154" AutoPostBack="True" MaxLength="4" Enabled="False"></asp:TextBox> <asp:Label style="COLOR: #cc3300" id="Label8" runat="server" Text="*" __designer:wfdid="w155"></asp:Label> </TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left>Berat Volume</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="BeratVolume" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w156" AutoPostBack="True" MaxLength="4" Enabled="False"></asp:TextBox> <asp:Label style="COLOR: #cc3300" id="Label2" runat="server" Text="*" __designer:wfdid="w157"></asp:Label></TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left>Berat Barang</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="BeratBarang" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w158" AutoPostBack="True" MaxLength="4" Enabled="False"></asp:TextBox> <asp:Label style="COLOR: #cc3300" id="Label3" runat="server" Text="*" __designer:wfdid="w159"></asp:Label></TD></TR><TR><TD style="WIDTH: 111px" class="Label" align=left>Harga Expedisi</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="priceid" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w160" AutoPostBack="True" MaxLength="10"></asp:TextBox> <asp:Label style="COLOR: #cc3300" id="Label9" runat="server" Text="*" __designer:wfdid="w161"></asp:Label> </TD></TR><TR><TD class="Label" align=left colSpan=3>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w162"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w163"></asp:Label> &nbsp;</TD></TR><TR><TD class="Label" align=left colSpan=3>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w164"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w165"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w166" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w167" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w168" Visible="False" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w169" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w170"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <cc1:FilteredTextBoxExtender id="fpriceid" runat="server" __designer:wfdid="w171" FilterType="Numbers" TargetControlID="priceid"></cc1:FilteredTextBoxExtender><cc1:FilteredTextBoxExtender id="FTELebar" runat="server" __designer:wfdid="w172" FilterType="Custom" TargetControlID="txt_lebar" ValidChars="0123456789.\ "></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="FTETinggi" runat="server" __designer:wfdid="w173" FilterType="Custom" TargetControlID="txt_tinggi" ValidChars="0123456789.\ "></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="FTEPanjang" runat="server" __designer:wfdid="w174" FilterType="Custom" TargetControlID="txt_panjang" ValidChars="0123456789.\ "></cc1:FilteredTextBoxExtender> </DIV>
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
                    </cc1:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></td>
                            <td style="TEXT-ALIGN: left" class="Label">
                                <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px; TEXT-ALIGN: center" colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" colspan="2">&nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
