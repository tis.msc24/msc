<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptTItem.aspx.vb" Inherits="ReportForm_rptTItem"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" style="width: 976px; height: 168px">
        <tr>
            <td colspan="3">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="MidnightBlue"
                                Text=".: Laporan Transformasi item" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                    <tr>
                        <th align="center" style="background-color: transparent" valign="center">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Periode :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:TextBox id="range1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w80"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w81"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w82"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w83"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w84"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Tipe Transformasi :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:DropDownList id="ddltype" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w85"><asp:ListItem>Create</asp:ListItem>
<asp:ListItem>Release</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Cabang&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:DropDownList id="DDLbranch" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w86" AutoPostBack="True">
        </asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Lokasi&nbsp;Awal :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:DropDownList id="ddllocawal" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w87" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Lokasi&nbsp;Akhir :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:DropDownList id="ddllocakhir" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w88">
    </asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Barang :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left><asp:TextBox id="barang" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w89"></asp:TextBox> &nbsp;<asp:ImageButton id="barangsearch" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w90"></asp:ImageButton> &nbsp;<asp:ImageButton id="barangerase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w91"></asp:ImageButton> &nbsp;<asp:Label id="barangoid" runat="server" __designer:wfdid="w92" Visible="False"></asp:Label> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVBarang" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w93" Visible="False" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="itemoid,itemcode,itemdesc,Merk" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                <asp:Label ID="Label150" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="TD2" align=right runat="server" Visible="false">Merk&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" id="TD1" align=left runat="server" Visible="false"><asp:TextBox id="merk" runat="server" Width="169px" CssClass="inpTextDisabled" __designer:wfdid="w94" Enabled="False"></asp:TextBox> </TD></TR><TR><TD align=center colSpan=2><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnshowprint" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w95"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w96"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w97"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w98"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=2><ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w99" TargetControlID="range1" PopupButtonID="ImageButton1" Format="dd/MM/yyyy" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'.">
        </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w100" TargetControlID="range2" PopupButtonID="ImageButton2" Format="dd/MM/yyyy" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'.">
        </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w101" TargetControlID="range1" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True" CultureName="id-ID">
        </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w102" TargetControlID="range2" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True" CultureName="id-ID">
        </ajaxToolkit:MaskedEditExtender> <asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w103" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w104"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w105"></asp:Label> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w114" HasZoomFactorList="False" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="true"></CR:CrystalReportViewer> 
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnshowprint"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w116" Visible="False"><TABLE width=250><TBODY><TR><TD style="WIDTH: 294px; HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w117"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=250><TBODY><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png" __designer:wfdid="w118"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red" __designer:wfdid="w119"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" __designer:wfdid="w120"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" __designer:wfdid="w121" TargetControlID="btnExtender" Drag="True" DropShadow="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" __designer:wfdid="w122" Visible="False"></asp:Button> 
</contenttemplate>
            </asp:UpdatePanel></th>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

