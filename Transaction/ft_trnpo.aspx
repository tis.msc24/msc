<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ft_trnpo.aspx.vb" Inherits="trnpo" Title="MULTI SARANA COMPUTER - Purchase Order" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
        <table id="Table3" align="center" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th class="header" colspan="5" valign="top">
                <asp:Label ID="Label81" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Purchase Order"></asp:Label></th>
        </tr>
    </table>
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel10" runat="server" HeaderText="TabPanel10">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD align=left colSpan=4><asp:DropDownList id="dCabangNya" runat="server" Width="120px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Filter&nbsp;</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText"><asp:ListItem Value="a.trnbelipono">PO No</asp:ListItem>
<asp:ListItem Value="s.suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp; </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" AutoPostBack="False"></asp:CheckBox></TD><TD class="Label" align=left colSpan=4><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label76" runat="server" Text="to"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label> </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText">
                                                            <asp:ListItem Value="custname">All</asp:ListItem>
                                                            <asp:ListItem Value="In Process">In Process</asp:ListItem>
                                                            <asp:ListItem Value="In Approval">In Approval</asp:ListItem>
                                                            <asp:ListItem>Approved</asp:ListItem>
                                                            <asp:ListItem>Closed</asp:ListItem>
                                                            <asp:ListItem>Rejected</asp:ListItem>
                                                            <asp:ListItem>Closed Manual</asp:ListItem>
                                                        </asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; </TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=5><asp:GridView id="tbldata" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" OnSelectedIndexChanged="tbldata_SelectedIndexChanged" GridLines="None" PageSize="6" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="pomstoid,pono,branch_code,postatus">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="pomstoid" HeaderText="PO ID" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:HyperLinkField DataNavigateUrlFields="pomstoid" DataNavigateUrlFormatString="~\transaction\ft_trnpo.aspx?oid={0}" DataTextField="pono" HeaderText="PO NO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="podate" DataFormatString="{0:d}" HeaderText="PO Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="70px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="70px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="supplier" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="500px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small" Width="500px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="postatus" HeaderText="PO Status">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" SelectText="" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="cePer2" runat="server" Format="dd/MM/yyyy" PopupButtonID="imbPeriod2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cePer1" runat="server" Format="dd/MM/yyyy" PopupButtonID="imbPeriod1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" TargetControlID="FilterPeriod1" MaskType="Date" UserDateFormat="MonthDayYear" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" TargetControlID="FilterPeriod2" MaskType="Date" UserDateFormat="MonthDayYear" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="tbldata"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> List of Purchase Order</span></strong> <strong>
                                <span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel20" runat="server" HeaderText="TabPanel20">
                        <ContentTemplate>
                            <div style="text-align: left">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label> <asp:Label id="Label3" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton1" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Detail Information Item</asp:LinkButton> <asp:Label id="Label13" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton7" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">List</asp:LinkButton>&nbsp;<asp:Label id="Label48" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton8" onclick="LinkButton8_Click1" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Voucher</asp:LinkButton>&nbsp;<asp:Label id="Label50" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton9" onclick="LinkButton9_Click" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Importir</asp:LinkButton> <TABLE id="Table6" cellSpacing=2 cellPadding=4 width="100%" align=center border=0><TBODY><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">Type PO</TD><TD><asp:DropDownList id="TypePOnya" runat="server" CssClass="inpText" AutoPostBack="True"><asp:ListItem>Normal</asp:ListItem>
<asp:ListItem Value="Selisih">Selisih Stok</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Label id="i_u" runat="server" Font-Size="Small" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="N E W" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">Cabang Asal</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD style="FONT-SIZE: 10pt; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:CheckBox id="cbSR" runat="server" Font-Size="X-Small" Text="Special Request"></asp:CheckBox></TD><TD style="FONT-SIZE: 10pt; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">&nbsp;<asp:Label id="approvaluserid" runat="server" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD>PO No</TD><TD><asp:TextBox id="trnbelipono" runat="server" Width="123px" CssClass="inpTextDisabled" MaxLength="20" ReadOnly="True"></asp:TextBox> <asp:Label id="oid" runat="server" Text="oid" Visible="False"></asp:Label></TD><TD>PO Date</TD><TD class="Label" colSpan=2><asp:TextBox style="TEXT-ALIGN: justify" id="trnbelipodate" runat="server" Width="75px" CssClass="inpTextDisabled" AutoPostBack="True" MaxLength="10" ValidationGroup="MKE" Enabled="False"></asp:TextBox>&nbsp;<SPAN style="FONT-SIZE: 10pt"></SPAN><asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><SPAN style="FONT-SIZE: 10pt"> <asp:Label id="Label23" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></SPAN></TD><TD></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">Supplier <asp:Label id="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="suppliername" runat="server" Width="129px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:Label id="PrefixOid" runat="server" Text="0" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">Id Supplier</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:DropDownList id="DDLPrefix" runat="server" CssClass="inpText"></asp:DropDownList></TD><TD class="Label">Cabang tujuan</TD><TD><asp:DropDownList id="toCabang" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD><asp:RadioButtonList id="digit" runat="server" Width="116px" Visible="False"><asp:ListItem Selected="True" Value="2">2 Digit Report</asp:ListItem>
<asp:ListItem Value="4">4 Digit Report</asp:ListItem>
<asp:ListItem Value="6">6 Digit Report</asp:ListItem>
</asp:RadioButtonList></TD><TD class="Label" colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvSupplier" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="suppoid,suppname,paymenttermoid,prefixsupp" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="6" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldsuppcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No supplier data found!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" TargetControlID="podeliverydate" Mask="99/99/9999" UserDateFormat="MonthDayYear" MaskType="Date" CultureName="en-US" ErrorTooltipEnabled="True" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cedeldate" runat="server" TargetControlID="podeliverydate" PopupButtonID="ImageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce1" runat="server" TargetControlID="trnbelipodate" PopupButtonID="ImageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" TargetControlID="trnbelipodate" Mask="99/99/9999" UserDateFormat="MonthDayYear" MaskType="Date" CultureName="en-US" ErrorTooltipEnabled="True" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true"></ajaxToolkit:MaskedEditExtender></TD><TD>&nbsp;<asp:TextBox id="approvalcode" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" Visible="False" MaxLength="30" ReadOnly="True"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">PO Status</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; COLOR: #000099; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="posting" runat="server" Width="129px" CssClass="inpTextDisabled" MaxLength="15" ReadOnly="True"></asp:TextBox></SPAN></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN><asp:Label id="Label35" runat="server" Font-Size="X-Small" Text="Has Voucher"></asp:Label></SPAN></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:CheckBox id="chkvcr" runat="server" Text="Voucher" AutoPostBack="True"></asp:CheckBox> <asp:Label id="lblchkvcr" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small">Delivery Date&nbsp;<asp:Label id="Label22" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></SPAN></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox style="TEXT-ALIGN: justify" id="podeliverydate" runat="server" Width="54px" CssClass="inpText" AutoPostBack="True" MaxLength="10" ValidationGroup="MKE"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label24" runat="server" Width="91px" CssClass="Important" Text="(dd/MM/yyyy)" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 7pt; COLOR: #000099"><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:Label id="Label33" runat="server" Text="Currency"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:DropDownList id="currencyoid" runat="server" Width="136px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:Label id="Label32" runat="server" Text="Currency Rate"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="currencyRate" runat="server" Width="129px" CssClass="inpTextDisabled" MaxLength="30" ReadOnly="True">0.00</asp:TextBox> <asp:Label id="rateoid" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">Market</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:DropDownList id="market" runat="server" Width="136px" CssClass="inpText" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="market_SelectedIndexChanged"><asp:ListItem>LOCAL</asp:ListItem>
<asp:ListItem>IMPORT</asp:ListItem>
</asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small">Pay Type</SPAN></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:DropDownList id="TRNPAYTYPE" runat="server" Width="133px" CssClass="inpTextDisabled" Enabled="False" AppendDataBoundItems="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small">Purchase Amount</SPAN></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="amtbeli" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small"><asp:Label id="Label12" runat="server" Width="129px" Font-Size="X-Small" Text="Disc. Detail Amount"></asp:Label></SPAN></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="amtdiscdtl" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small">Disc. Type</SPAN></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:DropDownList id="trnbelidisctype" runat="server" Width="134px" CssClass="inpText" Font-Size="X-Small" AutoPostBack="True"><asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
<asp:ListItem Enabled="False" Value="PCT">PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">Voucher Amount</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="totdtlvoucher" runat="server" Width="130px" CssClass="inpTextDisabled" AutoPostBack="True" MaxLength="30" ReadOnly="True" Enabled="False">0</asp:TextBox></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small"><asp:Label id="Label11" runat="server" Width="129px" Font-Size="X-Small" Text="Disc. Header Amount"></asp:Label></SPAN></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="amtdischdr" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left">Ekspedisi</TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:DropDownList id="Ekspedisi" runat="server" Width="133px" CssClass="inpText" AppendDataBoundItems="True"></asp:DropDownList> </TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small">Disc Value&nbsp;<asp:Label id="lbl1" runat="server" Font-Size="X-Small" Visible="False"></asp:Label><SPAN style="FONT-SIZE: 10pt"></SPAN></SPAN></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="trnbelidisc" runat="server" Width="130px" CssClass="inpText" AutoPostBack="True" MaxLength="30">0</asp:TextBox> </TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small"><asp:Label id="Label9" runat="server" Width="129px" Font-Size="X-Small" Text="Net Purchase Amount"></asp:Label></SPAN></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="amtbelinetto1" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left">Note</TD><TD colSpan=5><asp:TextBox id="trnbelinote" runat="server" Width="812px" CssClass="inpText" MaxLength="350"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left" id="TD9" runat="server"><asp:Label id="Label70" runat="server" Text="Tax (%)" Visible="False"></asp:Label> <asp:Label id="Label29" runat="server" Text="Unit Export" Visible="False"></asp:Label></TD><TD id="TD10" runat="server"><asp:TextBox id="trntaxpct" runat="server" Width="129px" CssClass="inpTextDisabled" Visible="False" ReadOnly="True">0.00</asp:TextBox> <asp:DropDownList id="matunit" runat="server" Width="155px" CssClass="inpText" Font-Size="X-Small" Visible="False" DataValueField="genoid" DataTextField="gendesc"></asp:DropDownList></TD><TD id="TD7" runat="server"><asp:Label id="Label69" runat="server" Text="Tax Amount" Visible="False"></asp:Label> <asp:Label id="Label36" runat="server" Text="Taxable" Visible="False"></asp:Label></TD><TD id="TD11" runat="server"><asp:CheckBox id="Taxable" runat="server" Visible="False" AutoPostBack="True" OnCheckedChanged="Taxable_CheckedChanged"></asp:CheckBox> <asp:TextBox id="amttax" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" Visible="False" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD id="TD8" runat="server"></TD><TD id="TD12" runat="server"><asp:Label id="matcab" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" Font-Bold="True" Visible="False">13</asp:Label> <asp:CheckBox id="SOBO" runat="server" Visible="False" OnCheckedChanged="Taxable_CheckedChanged"></asp:CheckBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="Td32" runat="server"><asp:Label id="Label30" runat="server" Text="Approval User" Visible="False"></asp:Label></TD><TD id="Td33" runat="server"><asp:TextBox id="approvalusername" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" Visible="False" MaxLength="30" ReadOnly="True"></asp:TextBox><asp:ImageButton id="imbSearchUser" onclick="imbSearchUser_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False"></asp:ImageButton><asp:ImageButton id="imbClearUser" onclick="imbClearUser_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD><TD id="Td34" runat="server"><asp:Label id="Label31" runat="server" Width="114px" Text="Approval Date Time" Visible="False"></asp:Label></TD><TD id="Td35" runat="server"><asp:TextBox id="ApprovalDate" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" Visible="False" MaxLength="30" ReadOnly="True"></asp:TextBox></TD><TD id="Td36" runat="server"><asp:TextBox id="trnbeliporef" runat="server" Width="129px" CssClass="inpText" Visible="False" MaxLength="30"></asp:TextBox></TD><TD id="Td37" runat="server"><asp:TextBox id="identifierno" runat="server" Width="129px" CssClass="inpText" Visible="False" MaxLength="10"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left" colSpan=2><asp:Label id="Label7" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Information Item"></asp:Label>&nbsp;<asp:Label id="trnsuppoid" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" Font-Bold="True" Visible="False"></asp:Label><asp:Label id="periodacctg" runat="server" Width="1px" ForeColor="Red" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left">&nbsp;</TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left">&nbsp;</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="lblcat_2" runat="server" Text="Item"></asp:Label>&nbsp;<asp:Label id="Label10" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD><asp:DropDownList id="DDLstockflag" runat="server" CssClass="inpText" Font-Size="X-Small" DataValueField="genoid" DataTextField="gendesc"><asp:ListItem Value="T">Transaction</asp:ListItem>
<asp:ListItem Value="I">Material Usage</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtMaterial" runat="server" Width="210px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchmat" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearmat" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD><TD><asp:CheckBox id="bonus" runat="server" Text="Barang Bonus" AutoPostBack="True" OnCheckedChanged="bonus_CheckedChanged"></asp:CheckBox></TD><TD><asp:Label id="lblbonus" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="lblcat" runat="server" Text="Item" Visible="False"></asp:Label><asp:Label id="prdtloid" runat="server" Visible="False"></asp:Label><asp:Label id="I_u2" runat="server" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label><asp:Label id="LbCategory" runat="server" ForeColor="Red" Visible="False"></asp:Label><asp:Label id="prno" runat="server" Visible="False"></asp:Label><asp:Label id="addprseq" runat="server" Visible="False"></asp:Label><asp:Label id="trnbelidtloid" runat="server"></asp:Label><asp:Label id="trnbelidtlseq" runat="server" Text="1" Visible="False"></asp:Label></TD><TD colSpan=5><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvMaterial" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="matoid,matlongdesc,matunit1oid,lastprice,merk,beratbarang,beratvolume,flagBarang" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="8" GridLines="None" OnRowDataBound="gvMaterial_RowDataBound" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Last Stock">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stok" HeaderText="Stok" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastprice" DataFormatString="{0:#,##0.0000}" HeaderText="Lastprice">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pono" HeaderText="PO No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="safetystok" HeaderText="SafetyStock" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="flagBarang" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="Label19" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Item data found!!"></asp:Label>

                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Quantity&nbsp;<asp:Label id="Label67" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD><asp:TextBox id="trnbelidtlqty" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" MaxLength="30">0</asp:TextBox><asp:TextBox id="Konversike3" runat="server" Width="16px" CssClass="inpTextDisabled" Visible="False" AutoPostBack="True" MaxLength="30">0</asp:TextBox><asp:DropDownList id="DDlmatCatmst" runat="server" Width="36px" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList><asp:TextBox id="merk" runat="server" Width="24px" CssClass="inpText" Visible="False"></asp:TextBox></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN>Disc Type 1</SPAN></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:DropDownList id="trnbelidtldisctype" runat="server" Width="104px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
<asp:ListItem Enabled="False" Value="PCT">PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN>Disc Type 2</SPAN></TD><TD><asp:DropDownList id="trnbelidtldisctype2" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="trnbelidtldisctype2_SelectedIndexChanged">
                                                                        <asp:ListItem Value="VCR">VOUCHER</asp:ListItem>
                                                                        <asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
                                                                        <asp:ListItem Value="PCT">PERCENTAGE</asp:ListItem>
                                                                    </asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD1" abbr="tdprice" runat="server">Price<asp:Label id="Label72" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD id="TD2" abbr="tdprice" runat="server"><asp:TextBox id="trnbelidtlprice" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" MaxLength="30">0</asp:TextBox> <asp:TextBox id="PriceListInfo" runat="server" Width="16px" CssClass="inpTextDisabled" Visible="False" AutoPostBack="True" MaxLength="30">0</asp:TextBox> </TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN>Disc Value</SPAN><asp:Label id="lbl2" runat="server" Width="72px" Font-Size="X-Small" Text="Disc amt" Visible="False"></asp:Label><SPAN>&nbsp;1</SPAN></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="trnbelidtldisc" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" MaxLength="30">0</asp:TextBox></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: 7pt">Disc Value </SPAN><asp:Label id="lbl3" runat="server" Font-Size="X-Small" Text="Disc amt" Visible="False"></asp:Label><SPAN>&nbsp;2</SPAN></TD><TD><asp:TextBox id="trnbelidtldisc2" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" MaxLength="30" OnTextChanged="trnbelidtldisc2_TextChanged">0</asp:TextBox> <asp:Label id="tamvoucherlama" runat="server" Text="0.00" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Unit <asp:Label id="Label16" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD><asp:DropDownList id="ddlUnit" runat="server" Width="104px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddlUnit_SelectedIndexChanged"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN>Disc Amt 1</SPAN></TD><TD><asp:TextBox id="amtdisc" runat="server" Width="100px" CssClass="inpTextDisabled" Font-Size="X-Small" MaxLength="30" ReadOnly="True">0.00</asp:TextBox> </TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="amtdisc2" runat="server" Width="100px" CssClass="inpTextDisabled" Font-Size="X-Small" Visible="False" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD><asp:Label id="hasildisc1" runat="server" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Note</TD><TD><asp:TextBox id="trnbelidtlnote" runat="server" Width="220px" CssClass="inpText" MaxLength="250"></asp:TextBox></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN>Biaya Ekspedisi /Kg</SPAN></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="costEkspedisi" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" OnTextChanged="costEkspedisi_TextChanged">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN><asp:Label id="Label4" runat="server" Width="129px" Font-Size="X-Small" Text="Net Purchase Amount"></asp:Label></SPAN></TD><TD id="TD3" runat="server"><asp:TextBox id="amtbelinetto2" runat="server" Width="100px" CssClass="inpTextDisabled" Font-Size="X-Small" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD></TD><TD><asp:Label id="labelcurrentstock" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label><asp:Label id="labelcurrentunit" runat="server" Visible="False" __designer:wfdid="w2"></asp:Label><asp:Label id="satuan1" runat="server" Visible="False" __designer:wfdid="w3"></asp:Label><asp:Label id="satuan2" runat="server" Visible="False" __designer:wfdid="w4"></asp:Label><asp:Label id="satuan3" runat="server" Visible="False" __designer:wfdid="w5"></asp:Label></TD><TD><asp:CheckBox id="cb1" runat="server" Width="107px" Font-Size="7pt" Text="Berat Barang /Kg" AutoPostBack="True" OnCheckedChanged="cb1_CheckedChanged"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><asp:TextBox id="beratBarang" runat="server" CssClass="inpText"></asp:TextBox></TD><TD><asp:CheckBox id="cb2" runat="server" Font-Size="7pt" Text="Berat Volume" AutoPostBack="True" OnCheckedChanged="cb2_CheckedChanged"></asp:CheckBox></TD><TD><asp:TextBox id="beratVolume" runat="server" CssClass="inpText"></asp:TextBox> <asp:Label id="centimeter" runat="server">cm3</asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD></TD><TD></TD><TD></TD><TD>&nbsp;</TD><TD align=right><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD><asp:ImageButton id="btnCancelDtl" onclick="btnCancelDtl_Click1" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 200px; BACKGROUND-COLOR: beige; TEXT-ALIGN: left"><asp:GridView id="tbldtl" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" Visible="False" DataKeyNames="podtlseq,podtldisc2,flagBarang" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" SelectText="Show" ShowSelectButton="True">
<HeaderStyle BorderStyle="None" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="podtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="material" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="DiscAmt" HeaderText="Amt Disc">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="DiscAmt2" HeaderText="Amt Voucher">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="poamtdtlnetto" HeaderText="Amt Netto">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="hargaEkspedisi" HeaderText="Harga ekspedisi">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typeBerat" HeaderText="Type Berat" Visible="False"></asp:BoundField>
<asp:BoundField DataField="berat" HeaderText="Berat">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ekspedisi" HeaderText="Ekspedisi">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Bonus" HeaderText="Bonus">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bonusAmt" HeaderText="Amt Bonus" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="flagBarang" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="No PO data !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=2><asp:Label id="Label1" runat="server" Width="29px" Font-Size="1.1em" Font-Bold="False" ForeColor="Navy" Text="Total"></asp:Label>&nbsp;<asp:Label id="Label63" runat="server" Width="48px" Font-Size="1.1em" Font-Bold="False" ForeColor="Navy" Text="Rupiah"></asp:Label> <asp:Label id="tot" runat="server" Width="80px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="0.00"></asp:Label></TD><TD colSpan=4 runat="server" Visible="false"></TD></TR><TR style="FONT-SIZE: 8pt"><TD></TD><TD id="TD24" runat="server" Visible="false"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVdetailSupp" runat="server" Width="44%" ForeColor="#333333" Visible="False" AutoGenerateColumns="False" CellPadding="4" PageSize="3" GridLines="None" OnRowDataBound="gvMaterial_RowDataBound" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="PO No.">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle Wrap="True" CssClass="gvhdr" Width="300px"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderpo" HeaderText="Order PO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastprice" DataFormatString="{0:#,##0.00}" HeaderText="Lastprice">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="disc" HeaderText="Disc Type1">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdisc" HeaderText="Disc 1">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="disc2" HeaderText="Disc Type2" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdisc2" HeaderText="Disc 2" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="netto" HeaderText="Netto">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="Label19" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Item data found!!"></asp:Label>

                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:TextBox id="totamtdisc" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" Visible="False" MaxLength="30" ReadOnly="True">0.00</asp:TextBox> <asp:ImageButton id="btnFromPR" runat="server" ImageUrl="~/Images/takefrompr.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="matoid" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" Font-Bold="True" Visible="False"></asp:Label> <asp:Label id="trnbelidtlunit" runat="server" Visible="False"></asp:Label> <asp:ImageButton id="btnclearPR" onclick="btnclearPR_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnaddpr" onclick="btnaddpr_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:TextBox id="poqty" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD><TD id="TD23" colSpan=4 runat="server" Visible="false"><TABLE id="Table5"><TBODY><TR id="trbulan" runat="server" visible="false"><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="lblunit1" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 3px; TEXT-ALIGN: center">l</TD><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="lblunit2" runat="server" Visible="False"></asp:Label></TD><TD style="TEXT-ALIGN: center">l</TD><TD style="TEXT-ALIGN: center"><asp:Label id="lblunit3" runat="server" Visible="False"></asp:Label></TD><TD style="TEXT-ALIGN: center"></TD><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="labeljual1" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 0px; TEXT-ALIGN: center">|</TD><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="labeljual2" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 0px; TEXT-ALIGN: center">|</TD><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="labeljual3" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 0px; TEXT-ALIGN: center">|</TD><TD style="WIDTH: 250px; TEXT-ALIGN: center"><asp:Label id="labelretur" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><TABLE id="Table7"><TBODY><TR id="trqty" runat="server" visible="false"><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="pricebuy1" runat="server" Visible="False"></asp:Label></TD><TD style="TEXT-ALIGN: center">l</TD><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="pricebuy2" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 5px; TEXT-ALIGN: center">l</TD><TD style="TEXT-ALIGN: center"><asp:Label id="pricebuy3" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 5px; TEXT-ALIGN: center"></TD><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="labelqty1" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 0px; TEXT-ALIGN: center">|</TD><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="labelqty2" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 0px; TEXT-ALIGN: center">|</TD><TD style="WIDTH: 120px; TEXT-ALIGN: center"><asp:Label id="labelqty3" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 0px; TEXT-ALIGN: center">|</TD><TD style="WIDTH: 250px; TEXT-ALIGN: center"><asp:Label id="labelqtyretur" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><ajaxToolkit:FilteredTextBoxExtender id="FTE_trnbelidtldisc2" runat="server" TargetControlID="trnbelidtldisc2" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEtrnbelidtlqty" runat="server" TargetControlID="trnbelidtlqty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEtrnbelidtldisc" runat="server" TargetControlID="trnbelidtldisc" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEcostEkspedisi" runat="server" TargetControlID="costEkspedisi" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftetrnbelidtlprice" runat="server" TargetControlID="trnbelidtlprice" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="mee50" runat="server" TargetControlID="trnbelidisc" ValidChars="1234567890.,">
                                                                </ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="mee1" runat="server" TargetControlID="currencyRate" ValidChars="1234567890.,">
                                                                </ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD25" runat="server" Visible="false">Cat/Merk&nbsp;Auto Generate </TD><TD id="TD26" runat="server" Visible="false"><asp:DropDownList id="Catgenerate" runat="server" Width="131px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList><asp:TextBox id="MerkGenerate" runat="server" Width="119px" CssClass="inpText"></asp:TextBox></TD><TD id="TD27" colSpan=4 runat="server" Visible="false">Item Generate<asp:TextBox id="ItemdescGenerate" runat="server" Width="253px" CssClass="inpText"></asp:TextBox><asp:ImageButton id="autogenerate" onclick="autogenerate_Click" runat="server" ImageUrl="~/Images/generate.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:Label id="I_u3" runat="server" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label><asp:Label id="lblSupp" runat="server" Visible="False">Detail Supplier</asp:Label></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server"><asp:LinkButton id="LinkButton2" runat="server" CssClass="submenu" Font-Size="Small">Information</asp:LinkButton>&nbsp;<asp:Label id="Label8" runat="server" ForeColor="#585858" Text="|"></asp:Label>&nbsp; <asp:Label id="Label14" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton6" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">List</asp:LinkButton> <asp:Label id="Label49" runat="server" ForeColor="#585858" Text="|"></asp:Label>&nbsp;<asp:LinkButton id="LinkButton10" onclick="LinkButton10_Click" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Voucher</asp:LinkButton>&nbsp;<asp:Label id="Label56" runat="server" ForeColor="#585858" Text="|"></asp:Label> <asp:LinkButton id="LinkButton11" onclick="LinkButton11_Click" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Importir</asp:LinkButton></asp:View>&nbsp; <asp:View id="View3" runat="server"><asp:LinkButton id="LinkButton14" onclick="LinkButton14_Click" runat="server" CssClass="submenu" Font-Size="Small">Information</asp:LinkButton>&nbsp;<asp:Label id="Label42" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton15" onclick="LinkButton15_Click" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Detail Information Item</asp:LinkButton> <asp:Label id="Label47" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton16" runat="server" CssClass="submenu" Visible="False">List</asp:LinkButton><asp:Label id="Label53" runat="server" CssClass="submenu" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Voucher"></asp:Label> <asp:Label id="Label54" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label><asp:LinkButton id="LinkButton17" onclick="LinkButton17_Click" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Importir</asp:LinkButton> <BR /><BR /><TABLE><TBODY><TR><TD style="WIDTH: 206px"><asp:Label id="Label39" runat="server" Font-Size="X-Small" Text="Voucher Date"></asp:Label> <asp:Label id="Label65" runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 557px"><asp:TextBox id="datevoucher" runat="server" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="imbVoucher" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> </TD><TD style="WIDTH: 222px"><asp:Label id="Label64" runat="server" Width="88px" Font-Size="X-Small" Text="Code Voucher" Visible="False"></asp:Label></TD><TD>&nbsp;</TD><TD style="WIDTH: 889px"><asp:TextBox id="txtnovcr" runat="server" Width="120px" CssClass="inpText" Visible="False"></asp:TextBox> &nbsp;&nbsp;</TD></TR><TR><TD style="WIDTH: 206px"><asp:Label id="Label37" runat="server" Width="112px" Font-Size="X-Small" Text="Voucher Description"></asp:Label>&nbsp;<asp:Label id="Label58" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 557px"><asp:TextBox id="descVoucher" runat="server" Width="184px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="searchvoucher" onclick="searchvoucher_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImageButton5" onclick="ImageButton5_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD><TD style="WIDTH: 222px"><asp:Label id="Label40" runat="server" Width="88px" Font-Size="X-Small" Text="Taksiran Value"></asp:Label> <asp:Label id="Label59" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 889px"><asp:TextBox id="amtVoucher" runat="server" CssClass="inpText" AutoPostBack="True" OnTextChanged="amtVoucher_TextChanged">0.00</asp:TextBox> <asp:Label id="temptakvalue" runat="server" Text="0" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 206px"></TD><TD colSpan=2><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvitemvoucher" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="itemoid,itemcode,itemdesc" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="8" GridLines="None" OnSelectedIndexChanged="gvitemvoucher_SelectedIndexChanged" OnPageIndexChanging="gvitemvoucher_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Voucher">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" Font-Size="X-Small" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                        <asp:Label ID="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Voucher data found!!"></asp:Label>

                                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD><TD style="WIDTH: 222px"></TD><TD></TD><TD style="WIDTH: 889px"></TD></TR><TR><TD style="WIDTH: 206px"><asp:Label id="Label66" runat="server" Width="53px" Font-Size="X-Small" Text="Currency"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 557px"><asp:DropDownList id="currencyoidvcr" runat="server" Width="168px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="currencyoidvcr_SelectedIndexChanged"></asp:DropDownList></TD><TD style="WIDTH: 222px"><asp:Label id="Label38" runat="server" Font-Size="X-Small" Text="Note"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 889px"><asp:TextBox id="voucherNote" runat="server" Width="400px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 206px"><asp:Label id="Label68" runat="server" Font-Size="X-Small" Text="Currency Rate"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 557px"><asp:TextBox id="curratevcr" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox><asp:Label id="rate2oidvcr" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 222px"></TD><TD></TD><TD style="WIDTH: 889px">&nbsp;</TD></TR><TR><TD style="WIDTH: 206px"><asp:Label id="I_U4" runat="server" ForeColor="Red" Text="New Detail Voucher" Visible="False"></asp:Label></TD><TD></TD><TD style="WIDTH: 557px"><asp:ImageButton id="btnlistvcr" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="clearvcr" onclick="clearvcr_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD style="WIDTH: 222px"></TD><TD></TD><TD style="WIDTH: 889px"><asp:Label id="trnbelidtlseqvcr" runat="server" Visible="False"></asp:Label><asp:Label id="idvoucher" runat="server" Visible="False"></asp:Label> <asp:Label id="voucheroid" runat="server" Visible="False"></asp:Label> <asp:TextBox id="kdvoucher" runat="server" CssClass="inpText" Visible="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 206px; HEIGHT: 25px"><ajaxToolkit:MaskedEditExtender id="meePer5" runat="server" TargetControlID="datevoucher" Mask="99/99/9999" UserDateFormat="MonthDayYear" MaskType="Date"></ajaxToolkit:MaskedEditExtender>&nbsp; </TD><TD style="HEIGHT: 25px"></TD><TD style="WIDTH: 557px; HEIGHT: 25px"><ajaxToolkit:CalendarExtender id="cePer5" runat="server" TargetControlID="datevoucher" PopupButtonID="imbvoucher" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD><TD style="WIDTH: 222px; HEIGHT: 25px"></TD><TD style="HEIGHT: 25px"></TD><TD style="WIDTH: 889px; HEIGHT: 25px"><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender6" runat="server" TargetControlID="amtVoucher" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD colSpan=6><asp:GridView id="GVvoucher" runat="server" Width="944px" ForeColor="#333333" DataKeyNames="voucherseq,amtvoucher" AutoGenerateColumns="False" CellPadding="4" GridLines="None" OnSelectedIndexChanged="GVvoucher_SelectedIndexChanged" OnRowDataBound="GVvoucher_RowDataBound" OnPageIndexChanging="GVvoucher_PageIndexChanging" OnRowDeleting="GVvoucher_RowDeleting">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="voucherseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucherno" HeaderText="Code Voucher">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucherdesc" HeaderText="Desc Voucher">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtvoucher" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="vouchernote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD colSpan=6></TD></TR><TR><TD colSpan=6>&nbsp;<asp:Label id="Label57" runat="server" Text="Total Taksiran Voucher :"></asp:Label>&nbsp; <asp:Label id="totvoucher" runat="server" Font-Bold="True">0.00</asp:Label>&nbsp;<asp:Label id="temptotvcr" runat="server" Visible="False">0</asp:Label></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View4" runat="server"><asp:LinkButton id="LinkButton18" onclick="LinkButton14_Click" runat="server" CssClass="submenu" Font-Size="Small">Information</asp:LinkButton>&nbsp;<asp:Label id="Label51" runat="server" ForeColor="#585858" Text="|"></asp:Label> <asp:LinkButton id="LinkButton19" onclick="LinkButton15_Click" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Detail Information Item</asp:LinkButton> <asp:Label id="Label52" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label> <asp:LinkButton id="LinkButton20" runat="server" CssClass="submenu" Visible="False">List</asp:LinkButton> <asp:LinkButton id="LinkButton21" onclick="LinkButton21_Click" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Voucher</asp:LinkButton> <asp:Label id="Label45" runat="server" ForeColor="#585858" Text="|"></asp:Label> <asp:Label id="Label46" runat="server" CssClass="submenu" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Importir"></asp:Label><BR /><BR /><TABLE width="100%"><TBODY><TR><TD style="HEIGHT: 15px"><asp:Label id="Label41" runat="server" Width="88px" Text="Importir Code"></asp:Label><asp:Label id="Label60" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD style="HEIGHT: 15px" colSpan=2><asp:TextBox id="kdimportir" runat="server" Width="128px" CssClass="inpText"></asp:TextBox></TD><TD style="HEIGHT: 15px"><asp:Label id="Label44" runat="server" Width="72px" Text="Import Cost"></asp:Label> <asp:Label id="Label62" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD style="HEIGHT: 15px"><asp:TextBox id="amtimportir" runat="server" CssClass="inpText" OnTextChanged="amtimportir_TextChanged">0.00</asp:TextBox></TD></TR><TR><TD><asp:Label id="Label43" runat="server" Text="Importir"></asp:Label><asp:Label id="Label61" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD colSpan=2><asp:TextBox id="importir" runat="server" Width="224px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="imbmportir" onclick="imbmportir_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="clearimportir" onclick="clearimportir_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblimportir" runat="server" Visible="False"></asp:Label> </TD><TD>Note</TD><TD><asp:TextBox id="importirnote" runat="server" Width="288px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD></TD><TD colSpan=2><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvImportir" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="suppoid,suppname,paymenttermoid" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="8" GridLines="None" OnSelectedIndexChanged="gvImportir_SelectedIndexChanged" OnPageIndexChanging="gvImportir_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" Font-Size="X-Small" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                        <asp:Label ID="Label20" runat="server"
                                                                            Font-Size="X-Small" ForeColor="Red" Text="No Voucher data found!!"></asp:Label>
                                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD><TD></TD><TD></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView> <TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left>Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858"></asp:Label> by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858"></asp:Label> </TD></TR><TR><TD align=left><asp:ImageButton id="btnsavemstr" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom"></asp:ImageButton> <asp:ImageButton id="btnCancelMstr" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom"></asp:ImageButton> <asp:ImageButton id="btnDeleteMstr" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsBottom" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" Visible="False"></asp:ImageButton> </TD></TR><TR><TD align=center><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel4"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Purchase Order :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>


                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="HEIGHT: 18px; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: center" colSpan=2><asp:Label id="Label15" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White">Print Option</asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Label id="Label34" runat="server" Text="Printer :"></asp:Label></TD><TD style="WIDTH: 8px; TEXT-ALIGN: left" class="Label"><asp:DropDownList id="ddlprinter" runat="server" Width="300px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2><asp:Label id="OtherTemp" runat="server" Visible="False"></asp:Label> <asp:Label id="NoTemp" runat="server" Visible="False"></asp:Label> <asp:Label id="idTemp" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btpPrintAction" onclick="btpPrintAction_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="btncloseprint" onclick="btncloseprint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <BR /><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" DropShadow="True" Drag="True" PopupDragHandleControlID="label15" BackgroundCssClass="modalBackground" PopupControlID="Panelvalidasi"></ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
        <Triggers>
<asp:PostBackTrigger ControlID="btpPrintAction"></asp:PostBackTrigger>
</Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupControlID="PanelMsgBox" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
<asp:Panel id="pnlUser" runat="server" Width="400px" CssClass="modalBox" Visible="False"><TABLE style="HEIGHT: 174px" width="100%"><TBODY><TR><TD style="WIDTH: 338px; TEXT-ALIGN: center" colSpan=3><asp:Label id="lblUser" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of User" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 338px; TEXT-ALIGN: center" colSpan=3>&nbsp;<asp:DropDownList id="ddlusr" runat="server" CssClass="inpText" Visible="False">
                                                                        <asp:ListItem Value="userid">User ID</asp:ListItem>
                                                                        <asp:ListItem Value="username">User Name</asp:ListItem>
                                                                    </asp:DropDownList> <asp:TextBox id="TextBox2" runat="server" Width="79px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:ImageButton id="findusr" onclick="ImageButton2_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="viewAllusr" onclick="viewAllusr_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD colSpan=3><FIELDSET style="HEIGHT: 3px" id="Fieldset7"><DIV id="Div7"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvUserList" runat="server" Width="98%" ForeColor="#333333" DataKeyNames="userid,username" AutoGenerateColumns="False" CellPadding="4" GridLines="None" OnSelectedIndexChanged="gvUserList_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="userid" HeaderText="User ID">
<HeaderStyle Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="username" HeaderText="User Name">
<HeaderStyle Font-Size="X-Small" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                        <asp:Label ID="Label21" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No user data found !!"></asp:Label>
                                                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbUser" onclick="lkbUser_Click" runat="server">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpe2" runat="server" TargetControlID="btnHiddenPnlUser" PopupControlID="pnlUser" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblUser"></ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHiddenPnlUser" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
                                            </asp:UpdatePanel>
    &nbsp;
    <asp:UpdatePanel ID="Updatevalidasi2" runat="server">
        <ContentTemplate>
<asp:Panel id="Panelvalidasi2" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="Label25" runat="server" Width="295px" Font-Size="Small" Font-Bold="True" ForeColor="White" Text="MULTI SARANA COMPUTER - WARNING"></asp:Label></TD></TR><TR><TD colSpan=2></TD></TR><TR><TD style="WIDTH: 62px; TEXT-ALIGN: center"><asp:Image id="Image2" runat="server" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="WIDTH: 100px; TEXT-ALIGN: center"><asp:Label id="Label26" runat="server" Width="199px" ForeColor="Red" Text="If you change this category"></asp:Label><BR /><asp:Label id="Label27" runat="server" Width="202px" ForeColor="Red" Text="Data in detail list will be lost !"></asp:Label><BR /><asp:Label id="Label28" runat="server" Width="218px" ForeColor="Red" Text="Are you sure want to continoe ??"></asp:Label></TD></TR><TR><TD colSpan=2></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnContinoeReset" onclick="btnContinoeReset_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton><asp:ImageButton id="BtnCancelReset" onclick="BtnCancelReset_Click1" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi2" runat="server" TargetControlID="ButtonExtendervalidasi2" DropShadow="True" Drag="True" PopupDragHandleControlID="label25" BackgroundCssClass="modalBackground" PopupControlID="Updatevalidasi2"></ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="ButtonExtendervalidasi2" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
