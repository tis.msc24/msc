' Prgmr : 4n7JuK Supported By Pryz; Last Update : 06/01/09
Imports ClassFunction
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trntransformorder
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim conn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader : Dim sSql As String = ""
    Private cKon As New Koneksi : Private cProc As New ClassProcedure
    Dim report As New ReportDocument : Dim dtab As DataTable
    Dim SNUrut As String : Dim SNGenerate As String
    Dim N As Integer = 1 : Dim JSN As Integer = 1
#End Region

#Region "Procedures"
    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssclass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub fillTextBox(ByVal sOid As Integer)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Try
            sSql = "SELECT a.branch_code, a.transformno, a.transformdate, a.transformnote, a.updtime, a.upduser, a.status, a.transformtype, (select distinct locoid from QL_TransformItemDtlMst tdm where tdm.transformoid = a.transformoid) locoiddtlmst, (select distinct locoid from QL_TransformItemDtl td where td.transformoid = a.transformoid) locoiddtl,a.createtime FROM QL_TransformItemMst a WHERE a.transformoid = " & sOid & " AND a.cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    trnno.Text = xreader("transformno")
                    trndate.Text = Format(xreader("transformdate"), "dd/MM/yyyy")
                    trnnote.Text = xreader("transformnote")
                    txtStatus.Text = xreader("status")
                    transtype.SelectedValue = xreader("transformtype")
                    lblUpdTime.Text = Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt")
                    lblUpdUser.Text = xreader("upduser")
                    ddlCabang.SelectedValue = xreader("branch_code")
                    InitAllDDL()
                    trnloc.SelectedValue = xreader("locoiddtlmst")
                    dtlloc.SelectedValue = xreader("locoiddtl")
                    createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                End While
            End If
            xreader.Close()

            'Get detail master
            sSql = "SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY a.transformdtloid) AS seq, a.itemoid, b.itemdesc, b.itemcode, b.merk, a.locoid AS locationoid, (SELECT gendesc FROM QL_mstgen WHERE genoid = f.genother1 AND cmpcode = f.cmpcode) + ' - ' + f.gendesc AS location, a.itemqty AS qty, a.unitoid AS satuan, b.satuan1, b.satuan1 satuan2, b.satuan1 satuan3, b.konversi1_2, b.konversi2_3, g.gendesc AS unit, c.gendesc AS unit1, c.gendesc AS unit2, c.gendesc AS unit3, a.transformnotedtl AS note, b.stockflag,Case b.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End itemflag FROM QL_TransformItemDtlMst a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.itemoid = b.itemoid INNER JOIN QL_mstgen c ON b.cmpcode = c.cmpcode AND b.satuan1 = c.genoid AND c.gengroup='ITEMUNIT' INNER JOIN QL_mstgen f ON a.cmpcode = f.cmpcode AND a.locoid = f.genoid INNER JOIN QL_mstgen g ON a.cmpcode = g.cmpcode AND a.unitoid = g.genoid WHERE a.transformoid = " & sOid & " AND a.cmpcode = '" & CompnyCode & "'"
            Dim adap As New SqlDataAdapter(sSql, conn)
            Dim dset As New DataSet
            adap.Fill(dset, "itemdetailheader")
            Dim dtab1 As DataTable = dset.Tables("itemdetailheader")

            GVHeader.DataSource = dtab1
            GVHeader.DataBind()
            Session("itemdetailheader") = dtab1

            'Dim dtab1 As DataTable = cKon.ambiltabel(sSql, "itemdetailheader")

            conn.Close()

            'Get detail
            sSql = "SELECT ROW_NUMBER() OVER(ORDER BY a.transformdtloid) AS seq, a.itemoid, b.itemdesc, b.itemcode, b.merk, a.locoid AS locationoid, (SELECT gendesc FROM QL_mstgen WHERE genoid = f.genother1 AND cmpcode = f.cmpcode) + ' - ' + f.gendesc AS location, a.itemqty AS qty, a.unitoid AS satuan, b.satuan1, b.satuan1 satuan2, b.satuan1 satuan3, b.konversi1_2, b.konversi2_3, g.gendesc AS unit, c.gendesc AS unit1, c.gendesc AS unit2, c.gendesc AS unit3, a.transformnotedtl AS note, b.stockflag,Case b.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End itemflag FROM QL_TransformItemDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.itemoid = b.itemoid INNER JOIN QL_mstgen c ON b.cmpcode = c.cmpcode AND b.satuan1 = c.genoid AND c.gengroup='ITEMUNIT' INNER JOIN QL_mstgen f ON a.cmpcode = f.cmpcode AND a.locoid = f.genoid INNER JOIN QL_mstgen g ON a.cmpcode = g.cmpcode AND a.unitoid = g.genoid WHERE a.transformoid = " & sOid & " AND a.cmpcode = '" & CompnyCode & "'"
            Dim dtab As DataTable = cKon.ambiltabel(sSql, "itemdetail")
            GVDtl.DataSource = dtab : GVDtl.DataBind()
            Session("itemdetail") = dtab

            If txtStatus.Text = "POST" Then
                imbCancel.Visible = True : imbSave.Visible = False
                imbDelete.Visible = False : imbPost.Visible = False
            Else
                imbCancel.Visible = True : imbSave.Visible = True
                imbDelete.Visible = True : imbPost.Visible = True
            End If
            transtype.Enabled = False
        Catch ex As Exception
            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If
            conn.Close()
            showMessage(ex.ToString, CompnyName & "- Error", 1, "modalMsgBox")
        End Try

    End Sub

    Private Sub InitAllDDL()
        sSql = "Select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & ddlCabang.SelectedValue & "' and gengroup = 'cabang') AND a.genother5 IN ('UMUM')"
        FillDDL(trnloc, sSql)
        trnloc.Items.Add("None")
        If Session("oid") Is Nothing And Session("oid") = "" Then
            trnloc.Items(trnloc.Items.Count - 1).Value = "None"
            trnloc.SelectedValue = "None"
        End If

        FillDDL(dtlloc, sSql)
        If Session("oid") Is Nothing And Session("oid") = "" Then
            dtlloc.Items.Add("None")
            dtlloc.Items(trnloc.Items.Count - 1).Value = "None"
            dtlloc.SelectedValue = "None"
        End If
    End Sub

    Private Sub InitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlCabang, sSql)
            Else
                FillDDL(ddlCabang, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlCabang, sSql)
        End If
    End Sub

    Private Sub fInitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlfCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlfCabang, sSql)
            Else
                FillDDL(ddlfCabang, sSql)
                ddlfCabang.Items.Add(New ListItem("ALL", "ALL"))
                ddlfCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlfCabang, sSql)
            ddlfCabang.Items.Add(New ListItem("ALL", "ALL"))
            ddlfCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub BindData(ByVal sWhere As String)
        sSql = "SELECT a.transformoid, a.transformno, a.transformdate, a.transformnote, a.status, a.transformtype FROM QL_TransformItemMst a WHERE a.cmpcode = '" & CompnyCode & "' " & sWhere & ""

        If ddlfCabang.SelectedValue <> "ALL" Then
            sSql &= " and a.branch_code = '" & ddlfCabang.SelectedValue & "'"
        End If

        sSql &= "ORDER BY a.transformdate DESC"

        Dim dtab As DataTable = cKon.ambiltabel(sSql, "TI")
        GVmst.DataSource = dtab : GVmst.DataBind()
        Session("TI") = dtab : GVmst.Visible = True
    End Sub

    Private Sub BindItem(ByVal e As Integer)
        Dim notoid As String = "0"
        If e = 0 Then
            If trnloc.SelectedValue = "None" Then
                showMessage("Lokasi Tidak Boleh None", CompnyName & "- Informasi", 2, "modalMsgBox")
                Exit Sub
            End If

            Dim transdate As Date = Date.ParseExact(trndate.Text, "dd/MM/yyyy", Nothing)

            If GVDtl.Rows.Count > 0 Then
                For i As Integer = 0 To GVDtl.Rows.Count - 1
                    notoid = notoid & GVDtl.DataKeys.Item(0).Value.ToString
                    If i < GVDtl.Rows.Count - 1 Then
                        notoid = notoid & ","
                    End If
                Next
            Else
                notoid = 0
            End If

            sSql = "SELECT /*DISTINCT*/ i.itemoid, i.itemcode, i.itemdesc, i.merk, i.satuan1, i.satuan1 satuan2, i.satuan1 satuan3, g1.gencode Unit1, g1.gencode Unit2, g1.gencode Unit3, i.konversi1_2, i.konversi2_3, (SELECT isnull(SUM(qtyIn),0)-isnull(sum(qtyOut),0) from QL_conmtr WHERE branch_code = '" & ddlCabang.SelectedValue & "' AND refoid = i.itemoid AND mtrlocoid = '" & trnloc.SelectedValue & "' and periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' ) AS saldoAkhir, i.stockflag, Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End itemflag FROM QL_mstItem i INNER JOIN QL_mstgen g1 ON g1.genoid = i.satuan1 AND g1.gengroup='ITEMUNIT' WHERE i.itemflag = 'Aktif' AND (i.itemdesc like '%" & Tchar(trnitem.Text.Trim) & "%' OR i.itemcode like '%" & Tchar(trnitem.Text.Trim) & "%' OR i.merk like '%" & Tchar(trnitem.Text.Trim) & "%') and i.itemflag = 'Aktif' /*and i.stockflag IN ('I','T')*/ AND i.itemoid NOT IN (" & notoid & ")"
            Dim objTable As DataTable = cKon.ambiltabel(sSql, "itemsearch")
            gvItemSearch.DataSource = objTable
            gvItemSearch.DataBind()
            Session("itemsearch") = objTable
        Else
            Dim transdate As Date = Date.ParseExact(trndate.Text, "dd/MM/yyyy", Nothing)
            'If trnitemoid.Text <> "" Then
            '    notoid = trnitemoid.Text
            'End If
            If GVHeader.Rows.Count > 0 Then
                For i As Integer = 0 To GVHeader.Rows.Count - 1
                    notoid = notoid & GVHeader.DataKeys.Item(0).Value.ToString
                    If i < GVHeader.Rows.Count - 1 Then
                        notoid = notoid & ","
                    End If
                Next
            Else
                notoid = 0
            End If


            'If GVHeader.Rows.Count > 0 Then
            '    If notoid <> "0" Then
            '        notoid = notoid.Substring(1)
            '        notoid = Left(notoid, notoid.Length - 1)
            '    End If
            'End If
            sSql = "SELECT DISTINCT i.itemoid, i.itemcode, i.itemdesc, i.merk, i.satuan1, i.satuan1 satuan2, i.satuan1 satuan3, g1.gencode Unit1, g1.gencode Unit2, g1.gencode Unit3, i.konversi1_2, i.konversi2_3, (SELECT isnull(SUM(qtyIn),0)-isnull(sum(qtyOut),0) from QL_conmtr WHERE branch_code = '" & ddlCabang.SelectedValue & "' AND refoid = i.itemoid AND mtrlocoid = '" & dtlloc.SelectedValue & "' and periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' ) AS sisa, i.stockflag, Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End itemflag FROM QL_mstItem i INNER JOIN QL_mstgen g1 ON g1.genoid = i.satuan1 AND g1.gengroup='ITEMUNIT' WHERE i.itemflag = 'Aktif' AND (i.itemdesc like '%" & Tchar(itemdetail.Text.Trim) & "%' OR i.itemcode like '%" & Tchar(itemdetail.Text.Trim) & "%' OR i.merk like '%" & Tchar(itemdetail.Text.Trim) & "%') and i.itemflag = 'Aktif' /*and i.stockflag IN ('I','T')*/ AND i.itemoid NOT IN (" & notoid & ")"

            Dim objTable As DataTable = cKon.ambiltabel(sSql, "itemsearch2")
            gvItem2.DataSource = objTable
            gvItem2.DataBind()
            Session("itemsearch2") = objTable
        End If
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\trntransformorder.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        imbDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        imbPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to post this data?')")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Transform Item"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            Dim datenow As Date = GetServerTime()
            InitCabang() : fInitCabang() : InitAllDDL() : BindData("")

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "edit"
                TabContainer1.ActiveTabIndex = 1
                fillTextBox(Integer.Parse(Session("oid")))
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                periodacctg.Text = GetPeriodAcctg(GetServerTime())
                i_u.Text = "new" : i_u2.Text = "new"
                trndate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                txtStatus.Text = "IN PROCESS"
                lblUpdUser.Text = Session("UserID")
                lblUpdTime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                trnno.Text = GenerateID("QL_TransformItemMst", CompnyCode)
                TabContainer1.ActiveTabIndex = 0

                imbDelete.Visible = False : imbPost.Visible = False

                GVHeader.DataSource = Nothing
                GVHeader.DataSource = Session("itemdetailheader")
                GVHeader.DataBind()
                labelseqhead.Text = "1" : transtype.Enabled = True
                GVDtl.DataSource = Nothing : GVDtl.DataSource = Session("itemdetail")
                GVDtl.DataBind()
                labelseq.Text = "1"
            End If
        End If
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim lb As LinkButton = TryCast(sender, LinkButton)
            Dim str As String() = lb.ToolTip.Split(",")

            report = New ReportDocument
            If str(1).ToString.ToLower = "create" Then
                report.Load(Server.MapPath("~\Report\printticreate.rpt"))
            Else
                report.Load(Server.MapPath("~\Report\printtirelease.rpt"))
            End If
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", " where a.transformno = '" & str(0) & "'")
            If str(1).ToString.ToLower = "create" Then
                report.SetParameterValue("sWhere", " where x.transformno = '" & str(0) & "'", "subticreate")
            Else
                report.SetParameterValue("sWhere", " where x.transformno = '" & str(0) & "'", "subtirelease")
            End If
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TI_" & str(1) & str(0))
            report.Close() : report.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "- Error", 1, "modalMsgBox")
        End Try
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("trntransformorder.aspx?awal=true")
    End Sub

    Protected Sub ibfinditem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfinditem.Click
        BindItem(0)
        gvItemSearch.Visible = True
    End Sub

    Protected Sub gvItemSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItemSearch.PageIndexChanging

        Dim objTable As DataTable = Session("itemsearch")
        gvItemSearch.DataSource = objTable
        gvItemSearch.PageIndex = e.NewPageIndex
        gvItemSearch.DataBind()
    End Sub

    Protected Sub gvItemSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItemSearch.SelectedIndexChanged
        gvItem2.Visible = False
        'trnitem.Text = Replace(gvItemSearch.Rows(gvItemSearch.SelectedIndex).Cells(2).Text, "&quot;", "")
        ItemcodeHeHeader.Text = gvItemSearch.Rows(gvItemSearch.SelectedIndex).Cells(1).Text
        merk.Text = Replace(gvItemSearch.Rows(gvItemSearch.SelectedIndex).Cells(3).Text, "&nbsp;", "")
        trnitemoid.Text = gvItemSearch.SelectedDataKey("itemoid")
        itemqty.Text = "0"

        itemunit.Items.Clear()
        itemunit.Items.Add(gvItemSearch.Rows(gvItemSearch.SelectedIndex).Cells(4).Text)
        itemunit.Items(itemunit.Items.Count - 1).Value = gvItemSearch.SelectedDataKey("satuan3")

        If gvItemSearch.SelectedDataKey("satuan2") <> gvItemSearch.SelectedDataKey("satuan3") Then
            itemunit.Items.Add(gvItemSearch.SelectedDataKey("unit2"))
            itemunit.Items(itemunit.Items.Count - 1).Value = gvItemSearch.SelectedDataKey("satuan2")
        End If

        If gvItemSearch.SelectedDataKey("satuan1") <> gvItemSearch.SelectedDataKey("satuan2") And gvItemSearch.SelectedDataKey("satuan1") <> gvItemSearch.SelectedDataKey("satuan3") Then
            itemunit.Items.Add(gvItemSearch.SelectedDataKey("unit1"))
            itemunit.Items(itemunit.Items.Count - 1).Value = gvItemSearch.SelectedDataKey("satuan1")
        End If

        trnitem.Text = gvItemSearch.SelectedDataKey("itemdesc").ToString
        labelsatuan1.Text = gvItemSearch.SelectedDataKey("satuan1")
        labelsatuan2.Text = gvItemSearch.SelectedDataKey("satuan2")
        labelsatuan3.Text = gvItemSearch.SelectedDataKey("satuan3")
        labelunit1.Text = gvItemSearch.SelectedDataKey("unit1")
        labelunit2.Text = gvItemSearch.SelectedDataKey("unit2")
        labelunit3.Text = gvItemSearch.Rows(gvItemSearch.SelectedIndex).Cells(4).Text

        labelkonversi1_2.Text = gvItemSearch.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = gvItemSearch.SelectedDataKey("konversi2_3")
        stockflag.Text = gvItemSearch.SelectedDataKey("itemflag")
        FlagItem1.Text = gvItemSearch.SelectedDataKey("stockflag").ToString
        gvItemSearch.DataSource = Nothing
        gvItemSearch.DataBind()
        gvItemSearch.Visible = False
        gvItemSearch.SelectedIndex = -1
    End Sub

    Protected Sub ibdelitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelitem.Click
        trnitem.Text = ""
        merk.Text = ""
        trnitemoid.Text = ""
        itemqty.Text = "0.00"
        maxQty.Text = "0.00"
        itemunit.Items.Clear()

        If gvItemSearch.Visible = True Then
            gvItemSearch.DataSource = Nothing
            gvItemSearch.DataBind()
            gvItemSearch.Visible = False
            Session("itemsearch") = Nothing
        End If
    End Sub

    Protected Sub ibfinditemdetail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfinditemdetail.Click
        If dtlloc.SelectedValue = "None" Then
            showMessage("Pilih lokasi terlebih dahulu!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        BindItem(1)
        gvItem2.Visible = True
    End Sub

    Protected Sub gvItem2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem2.PageIndexChanging
        'BindItem(1)

        Dim objTable As DataTable = Session("itemsearch2")
        gvItem2.DataSource = objTable
        gvItem2.PageIndex = e.NewPageIndex
        gvItem2.DataBind()
    End Sub

    Protected Sub gvItem2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem2.SelectedIndexChanged

        gvItemSearch.Visible = False
        itemdetail.Text = Replace(gvItem2.Rows(gvItem2.SelectedIndex).Cells(2).Text, "&quot;", "")
        merkdtl.Text = gvItem2.Rows(gvItem2.SelectedIndex).Cells(3).Text
        ItemcodeFooter.Text = gvItem2.Rows(gvItem2.SelectedIndex).Cells(1).Text
        itemdtloid.Text = gvItem2.SelectedDataKey("itemoid")
        maxQty.Text = gvItem2.Rows(gvItem2.SelectedIndex).Cells(4).Text
        itemdtlqty.Text = gvItem2.Rows(gvItem2.SelectedIndex).Cells(4).Text

        unitdtl.Items.Clear()
        unitdtl.Items.Add(gvItem2.Rows(gvItem2.SelectedIndex).Cells(5).Text)
        unitdtl.Items(unitdtl.Items.Count - 1).Value = gvItem2.SelectedDataKey("satuan3")

        If gvItem2.SelectedDataKey("satuan2") <> gvItem2.SelectedDataKey("satuan3") Then
            unitdtl.Items.Add(gvItem2.SelectedDataKey("unit2"))
            unitdtl.Items(unitdtl.Items.Count - 1).Value = gvItem2.SelectedDataKey("satuan2")
        End If

        If gvItem2.SelectedDataKey("satuan3") <> gvItem2.SelectedDataKey("satuan1") And gvItem2.SelectedDataKey("satuan3") <> gvItem2.SelectedDataKey("satuan2") Then
            unitdtl.Items.Add(gvItem2.SelectedDataKey("unit1"))
            unitdtl.Items(unitdtl.Items.Count - 1).Value = gvItem2.SelectedDataKey("satuan1")
        End If

        labelsatuan1dtl.Text = gvItem2.SelectedDataKey("satuan1")
        labelsatuan2dtl.Text = gvItem2.SelectedDataKey("satuan2")
        labelsatuan3dtl.Text = gvItem2.SelectedDataKey("satuan3")
        labelunit1dtl.Text = gvItem2.SelectedDataKey("unit1").ToString
        labelunit2dtl.Text = gvItem2.SelectedDataKey("unit2").ToString
        labelunit3dtl.Text = gvItem2.Rows(gvItem2.SelectedIndex).Cells(5).Text

        labelkonversi1_2dtl.Text = gvItem2.SelectedDataKey("konversi1_2")
        labelkonversi2_3dtl.Text = gvItem2.SelectedDataKey("konversi2_3")
        labelkonversi2_3dtl.Text = gvItem2.SelectedDataKey("konversi2_3")
        stockflagdtl.Text = gvItem2.SelectedDataKey("itemflag")
        FlagItem2.Text = gvItem2.SelectedDataKey("stockflag")

        gvItem2.DataSource = Nothing
        gvItem2.DataBind()
        gvItem2.Visible = False
        gvItem2.SelectedIndex = -1
    End Sub

    Protected Sub ibdelitemdetail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelitemdetail.Click
        itemdetail.Text = ""
        merkdtl.Text = ""
        itemdtloid.Text = ""
        itemdtlqty.Text = "0.00"
        unitdtl.Items.Clear()

        If gvItem2.Visible = True Then
            gvItem2.DataSource = Nothing
            gvItem2.DataBind()
            gvItem2.Visible = False
            Session("itemsearch2") = Nothing
        End If
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        dtlloc.SelectedValue = "None"
        itemdetail.Text = ""
        merkdtl.Text = ""
        itemdtloid.Text = ""
        itemdtlqty.Text = "0.00"
        unitdtl.Items.Clear()
        notedtl.Text = ""
        i_u2.Text = "new"
        maxQty.Text = "0.00"

        If gvItem2.Visible = True Then
            gvItem2.DataSource = Nothing
            gvItem2.DataBind()
            gvItem2.Visible = False
            Session("itemsearch2") = Nothing
        End If

        GVDtl.SelectedIndex = -1
        GVDtl.Columns(9).Visible = True
    End Sub

    Protected Sub imbAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAddToList.Click
        LabelPesanSn.Text = ""
        Try
            If Session("itemdetailheader") Is Nothing Then
                showMessage("Item Header Tidak Boleh Kosong!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If itemdetail.Text.Trim = "" Or itemdtloid.Text = "" Then
                showMessage("Item tidak boleh kosong!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If Double.Parse(itemdtlqty.Text.Trim) = 0.0 Then
                showMessage("Qty harus lebih dari 0!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If ToDouble(itemdtlqty.Text) > ToDouble(maxQty.Text) Then
                showMessage("Jumlah item tidak boleh melebihi jumlah maksimumnya!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If dtlloc.SelectedValue = "None" Then
                showMessage("Pilih lokasi terlebih dahulu!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If transtype.SelectedValue = "Type" Then
                showMessage("Pilih Tipe Transformation terlebih dahulu!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If i_u2.Text = "new" Then
                If transtype.SelectedValue = "Release" Then
                    If GVDtl.Rows.Count >= 1 Then
                        showMessage("Jumlah item detail untuk tipe tranformasi 'Release' maksimal 1!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                        Exit Sub
                    End If
                End If
            End If

            Dim dtab As DataTable

            If i_u2.Text = "new" Then
                If Session("itemdetail") Is Nothing Then
                    dtab = New DataTable
                    dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                    dtab.Columns.Add("merk", Type.GetType("System.String"))
                    dtab.Columns.Add("locationoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("location", Type.GetType("System.String"))
                    dtab.Columns.Add("qty", Type.GetType("System.Double"))
                    dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                    dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                    dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                    dtab.Columns.Add("unit", Type.GetType("System.String"))
                    dtab.Columns.Add("unit1", Type.GetType("System.String"))
                    dtab.Columns.Add("unit2", Type.GetType("System.String"))
                    dtab.Columns.Add("unit3", Type.GetType("System.String"))
                    dtab.Columns.Add("note", Type.GetType("System.String"))
                    dtab.Columns.Add("itemcode", Type.GetType("System.String"))
                    dtab.Columns.Add("itemflag", Type.GetType("System.String"))
                    dtab.Columns.Add("stockflag", Type.GetType("System.String"))
                    Session("itemdetail") = dtab
                    labelseq.Text = "1"
                Else
                    dtab = Session("itemdetail")
                    labelseq.Text = (dtab.Rows.Count + 1).ToString
                End If
            Else
                dtab = Session("itemdetail")
            End If

            If dtab.Rows.Count > 0 Then
                Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(itemdtloid.Text) & " AND merk = '" & merkdtl.Text & "' AND locationoid = " & dtlloc.SelectedValue & " AND seq <> " & Integer.Parse(labelseq.Text) & "")
                If drowc.Length > 0 Then
                    showMessage("Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list !", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If

            sSql = "SELECT DISTINCT isnull(i.has_sn,0) from QL_mstItem i INNER JOIN QL_mstItem_branch ib ON i.itemoid = ib.itemoid WHERE i.itemoid = '" & itemdtloid.Text & "' and ib.branch_code = '" & ddlCabang.SelectedValue & "'"
            Dim has_sn As String = cKon.ambilscalar(sSql)

            '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            If has_sn = "1" Then

                '@@@@@@@@@@@@@@@@@@@@@@@@ Jika Menggunakan SN
                If transtype.SelectedValue = "Create" Then
                    StatusIsiSN.Text = "Footer"
                    'btnHideSN.Visible = False
                    TextSN.Focus()
                    If i_u.Text = "new" Then
                        If Session("TblSNFooter") Is Nothing Then

                            pnlInvnSN.Visible = True
                            btnHideSN.Visible = True
                            TextItem.Text = itemdetail.Text
                            TextQty.Text = itemdtlqty.Text
                            TextSatuan.Text = unitdtl.SelectedItem.ToString
                            KodeItem.Text = itemdtloid.Text
                            GVSN.DataSource = Nothing
                            GVSN.DataBind() : ModalPopupSN.Show()
                            TextSN.Text = "" : LabelPesanSn.Text = ""
                            TextSN.Focus()
                            btnGenerateSN.Visible = False
                        Else

                            Dim objTableSN As DataTable
                            objTableSN = Session("TblSNFooter")
                            Dim dvjum As DataView = objTableSN.DefaultView
                            dvjum.RowFilter = "itemoid= '" & itemdtloid.Text & "'"
                            pnlInvnSN.Visible = True
                            btnHideSN.Visible = True
                            TextQty.Text = itemdtlqty.Text
                            TextSatuan.Text = unitdtl.SelectedItem.ToString
                            TextItem.Text = itemdetail.Text
                            KodeItem.Text = itemdtloid.Text
                            GVSN.DataSource = Nothing
                            GVSN.DataSource = dvjum
                            GVSN.DataSource = dvjum
                            GVSN.DataBind() : ModalPopupSN.Show()
                            TextSN.Text = "" : TextSN.Focus()
                            btnGenerateSN.Visible = False
                        End If

                    Else
                        If txtStatus.Text = "IN PROCESS" Or txtStatus.Text = "In Approval" Or txtStatus.Text = "Approved" Or txtStatus.Text = "POST" Then
                            pnlInvnSN.Visible = True
                            btnHideSN.Visible = True
                            sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid, s.id_sn from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntioid = '" & trnno.Text & "' AND i.itemoid = '" & itemdtloid.Text & "' "
                            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
                            Dim objDs As New DataSet
                            mySqlDA.Fill(objDs, "dataSN")
                            GVSN.DataSource = Nothing
                            GVSN.DataSource = objDs.Tables("dataSN")
                            GVSN.DataBind()
                            Session("TblSN") = objDs.Tables("dataSN")
                            Session("TblSNFooter") = objDs.Tables("dataSN")
                            TextQty.Text = itemdtlqty.Text
                            TextSatuan.Text = unitdtl.SelectedItem.ToString
                            TextItem.Text = itemdetail.Text
                            KodeItem.Text = itemdtloid.Text
                            ModalPopupSN.Show()
                            TextSN.Text = "" : LabelPesanSn.Text = ""
                            TextSN.Focus() : btnGenerateSN.Visible = False
                        End If
                    End If
                Else
                    StatusIsiSN.Text = "Footer"
                    'btnHideSN.Visible = False
                    TextSN.Focus()
                    If i_u.Text = "new" Then
                        If Session("TblSNFooter") Is Nothing Then
                            pnlInvnSN.Visible = True
                            btnHideSN.Visible = True
                            TextItem.Text = itemdetail.Text
                            TextQty.Text = itemdtlqty.Text
                            TextSatuan.Text = unitdtl.SelectedItem.ToString
                            KodeItem.Text = itemdtloid.Text
                            GVSN.DataSource = Nothing
                            GVSN.DataBind() : ModalPopupSN.Show()
                            TextSN.Text = "" : LabelPesanSn.Text = ""
                            TextSN.Focus() : btnGenerateSN.Visible = False
                        Else
                            Dim objTableSN As DataTable
                            objTableSN = Session("TblSNFooter")
                            Dim dvjum As DataView = objTableSN.DefaultView
                            dvjum.RowFilter = "itemoid= '" & itemdtloid.Text & "'"

                            pnlInvnSN.Visible = True
                            btnHideSN.Visible = True
                            TextQty.Text = itemdtlqty.Text
                            TextSatuan.Text = unitdtl.SelectedItem.ToString
                            TextItem.Text = itemdetail.Text
                            KodeItem.Text = itemdtloid.Text
                            GVSN.DataSource = Nothing
                            GVSN.DataSource = dvjum
                            GVSN.DataSource = dvjum
                            GVSN.DataBind() : ModalPopupSN.Show()
                            TextSN.Text = "" : TextSN.Focus()
                            btnGenerateSN.Visible = False
                        End If

                    Else
                        If txtStatus.Text = "IN PROCESS" Or txtStatus.Text = "In Approval" Or txtStatus.Text = "Approved" Or txtStatus.Text = "POST" Then
                            pnlInvnSN.Visible = True : btnHideSN.Visible = True
                            sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntioid = '" & trnno.Text & "' AND i.itemoid = '" & itemdtloid.Text & "' "
                            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
                            Dim objDs As New DataSet
                            mySqlDA.Fill(objDs, "dataSN")
                            GVSN.DataSource = Nothing
                            GVSN.DataSource = objDs.Tables("dataSN")
                            GVSN.DataBind()
                            Session("TblSN") = objDs.Tables("dataSN")
                            TextQty.Text = itemdtlqty.Text
                            TextSatuan.Text = unitdtl.SelectedItem.ToString
                            TextItem.Text = itemdetail.Text
                            KodeItem.Text = itemdtloid.Text
                            ModalPopupSN.Show()
                            TextSN.Text = "" : LabelPesanSn.Text = ""
                            TextSN.Focus()
                            btnGenerateSN.Visible = False
                        End If
                    End If
                End If
                cProc.SetFocusToControl(Me.Page, TextSN)
            Else

                Dim drow As DataRow
                Dim drowedit() As DataRow
                If i_u2.Text = "new" Then
                    drow = dtab.NewRow

                    drow("seq") = Integer.Parse(labelseq.Text)
                    drow("itemoid") = Integer.Parse(itemdtloid.Text)
                    drow("itemdesc") = itemdetail.Text.Trim
                    drow("merk") = merkdtl.Text
                    drow("locationoid") = dtlloc.SelectedValue
                    drow("location") = dtlloc.SelectedItem.ToString
                    drow("qty") = Double.Parse(itemdtlqty.Text)
                    drow("satuan") = unitdtl.SelectedValue
                    drow("unit") = unitdtl.SelectedItem.Text

                    drow("satuan1") = Integer.Parse(labelsatuan1dtl.Text)
                    drow("satuan2") = Integer.Parse(labelsatuan2dtl.Text)
                    drow("satuan3") = Integer.Parse(labelsatuan3dtl.Text)
                    drow("unit1") = labelunit1dtl.Text
                    drow("unit2") = labelunit2dtl.Text
                    drow("unit3") = labelunit3dtl.Text

                    drow("konversi1_2") = Double.Parse(labelkonversi1_2dtl.Text)
                    drow("konversi2_3") = Double.Parse(labelkonversi2_3dtl.Text)
                    drow("note") = notedtl.Text.Trim
                    drow("itemcode") = ItemcodeFooter.Text
                    drow("itemflag") = stockflagdtl.Text
                    drow("stockflag") = FlagItem2.Text
                    dtab.Rows.Add(drow)
                    dtab.AcceptChanges()
                    transtype.Enabled = False
                Else
                    drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
                    drow = drowedit(0)
                    drowedit(0).BeginEdit()

                    drow("itemoid") = Integer.Parse(itemdtloid.Text)
                    drow("itemdesc") = itemdetail.Text.Trim
                    drow("merk") = merkdtl.Text
                    drow("locationoid") = dtlloc.SelectedValue
                    drow("location") = dtlloc.SelectedItem.ToString
                    drow("qty") = Double.Parse(itemdtlqty.Text)
                    drow("satuan") = unitdtl.SelectedValue
                    drow("unit") = unitdtl.SelectedItem.Text

                    drow("satuan1") = Integer.Parse(labelsatuan1dtl.Text)
                    drow("satuan2") = Integer.Parse(labelsatuan2dtl.Text)
                    drow("satuan3") = Integer.Parse(labelsatuan3dtl.Text)
                    drow("unit1") = labelunit1dtl.Text
                    drow("unit2") = labelunit2dtl.Text
                    drow("unit3") = labelunit3dtl.Text

                    drow("konversi1_2") = Double.Parse(labelkonversi1_2dtl.Text)
                    drow("konversi2_3") = Double.Parse(labelkonversi2_3dtl.Text)
                    drow("note") = notedtl.Text.Trim
                    drow("itemcode") = ItemcodeFooter.Text
                    drow("itemflag") = stockflagdtl.Text
                    drow("stockflag") = FlagItem2.Text
                    drowedit(0).EndEdit()
                    dtab.Select(Nothing, Nothing)
                    dtab.AcceptChanges()
                    transtype.Enabled = False
                End If

                GVDtl.DataSource = dtab
                GVDtl.DataBind()
                Session("itemdetail") = dtab

                labelseq.Text = (GVDtl.Rows.Count + 1).ToString
                merkdtl.Text = "" : itemdtlqty.Text = "0.00"
                unitdtl.Items.Clear()
                notedtl.Text = "" : itemdtloid.Text = ""
                itemdetail.Text = "" : i_u2.Text = "new"
                maxQty.Text = "0.00"

                labelsatuan1dtl.Text = "" : labelsatuan2dtl.Text = ""
                labelsatuan3dtl.Text = "" : labelunit1dtl.Text = ""
                labelunit2dtl.Text = "" : labelunit3dtl.Text = ""
                labelkonversi1_2dtl.Text = "1" : labelkonversi2_3dtl.Text = "1"
                stockflagdtl.Text = ""

                GVDtl.SelectedIndex = -1
                GVDtl.Columns(10).Visible = True

                If gvItem2.Visible = True Then
                    gvItem2.DataSource = Nothing
                    gvItem2.DataBind()
                    gvItem2.Visible = False
                    gvItem2.SelectedIndex = -1
                End If
            End If
            '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        Catch ex As Exception
            showMessage(ex.ToString & "<BR />" & sSql, CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
        
    End Sub

    Protected Sub GVDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl.SelectedIndexChanged
        i_u2.Text = "edit"

        dtlloc.SelectedValue = GVDtl.SelectedDataKey("locationoid")
        itemdtloid.Text = GVDtl.SelectedDataKey("itemoid")
        itemdetail.Text = Replace(GVDtl.Rows(GVDtl.SelectedIndex).Cells(3).Text, "&quot;", "")
        ItemcodeFooter.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(2).Text
        itemdtlqty.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(5).Text

        notedtl.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(8).Text.Replace("&nbsp;", "")
        merkdtl.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(4).Text

        unitdtl.Items.Clear()
        unitdtl.Items.Add(GVDtl.SelectedDataKey("unit3"))
        unitdtl.Items(unitdtl.Items.Count - 1).Value = GVDtl.SelectedDataKey("satuan3")
        If GVDtl.SelectedDataKey("satuan2") <> GVDtl.SelectedDataKey("satuan3") Then
            unitdtl.Items.Add(GVDtl.SelectedDataKey("unit2"))
            unitdtl.Items(unitdtl.Items.Count - 1).Value = GVDtl.SelectedDataKey("satuan2")
        End If
        If GVDtl.SelectedDataKey("satuan1") <> GVDtl.SelectedDataKey("satuan3") And GVDtl.SelectedDataKey("satuan1") <> GVDtl.SelectedDataKey("satuan2") Then
            unitdtl.Items.Add(GVDtl.SelectedDataKey("unit1"))
            unitdtl.Items(unitdtl.Items.Count - 1).Value = GVDtl.SelectedDataKey("satuan1")
        End If
        unitdtl.SelectedValue = GVDtl.SelectedDataKey("satuan")

        labelsatuan1dtl.Text = GVDtl.SelectedDataKey("satuan1")
        labelsatuan2dtl.Text = GVDtl.SelectedDataKey("satuan2")
        labelsatuan3dtl.Text = GVDtl.SelectedDataKey("satuan3")
        labelunit1dtl.Text = GVDtl.SelectedDataKey("unit1")
        labelunit2dtl.Text = GVDtl.SelectedDataKey("unit2")
        labelunit3dtl.Text = GVDtl.SelectedDataKey("unit3")

        labelkonversi1_2dtl.Text = GVDtl.SelectedDataKey("konversi1_2")
        labelkonversi2_3dtl.Text = GVDtl.SelectedDataKey("konversi2_3")
        stockflagdtl.Text = GVDtl.SelectedDataKey("itemflag")
        FlagItem2.Text = GVDtl.SelectedDataKey("stockflag")
        labelseq.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(1).Text

        Dim transdate As Date = Date.ParseExact(trndate.Text, "dd/MM/yyyy", Nothing)
        conn.Open()
        sSql = "SELECT isnull(SUM(qtyIn),0)-isnull(sum(qtyOut),0) from QL_conmtr WHERE refname = 'QL_MSTITEM' AND refoid = " & Integer.Parse(itemdtloid.Text) & " AND periodacctg = '" & GetDateToPeriodAcctg(Date.Now).Trim & "' AND cmpcode = '" & CompnyCode & "'"
        xCmd.CommandText = sSql
        Dim totmaxqty As Double = xCmd.ExecuteScalar
        If Not totmaxqty = Nothing Then
            If GVDtl.SelectedDataKey("satuan") = GVDtl.SelectedDataKey("satuan1") Then
                totmaxqty = totmaxqty / GVDtl.SelectedDataKey("konversi1_2")
            End If
            maxQty.Text = Format(totmaxqty, "#,##0.00")
        Else
            maxQty.Text = "0.00"
        End If
        conn.Close()

        If gvItem2.Visible = True Then
            gvItem2.DataSource = Nothing
            gvItem2.DataBind()
            gvItem2.Visible = False
        End If

        GVDtl.Columns(10).Visible = False
    End Sub

    Protected Sub GVDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl.RowDeleting
        If Not Session("itemdetail") Is Nothing Then
            Dim cseq As Integer = 0

            Dim dtab As DataTable = Session("itemdetail")
            Dim ioid As Integer = Integer.Parse(GVDtl.Rows(e.RowIndex).Cells(1).Text)
            If i_u.Text = "new" Then
                If Not Session("TblSNFooter") Is Nothing Then
                    Dim objTableSNFotter As DataTable = Session("TblSNFooter")
                    Dim dvSN As DataView = objTableSNFotter.DefaultView
                    Dim itemid As String = dtab.Rows(e.RowIndex).Item("itemoid")
                    dvSN.RowFilter = "itemoid = " & itemid
                    If Not dvSN.Count = 0 Then
                        For kdsn As Integer = 0 To dvSN.Count - 1
                            objTableSNFotter.Rows.RemoveAt(0)
                        Next
                    End If
                    objTableSNFotter.AcceptChanges()
                End If
            Else
                If Not Session("TblSNFooter") Is Nothing Then
                    Dim objTableSNFotter As DataTable = Session("TblSNFooter")
                    Dim dvSN As DataView = objTableSNFotter.DefaultView
                    Dim itemid As String = dtab.Rows(e.RowIndex).Item("itemoid")
                    dvSN.RowFilter = "itemoid = " & itemid
                    If Not dvSN.Count = 0 Then
                        For kdsn As Integer = 0 To dvSN.Count - 1
                            objTableSNFotter.Rows.RemoveAt(0)
                        Next
                    End If
                    objTableSNFotter.AcceptChanges()
                End If
                'sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntioid = '" & trnno.Text & "' AND i.itemoid = '" & dtab.Rows(e.RowIndex).Item("itemoid") & "' "
                'Dim TBSVSN As DataTable = cKon.ambiltabel(sSql, "SNsave")

                'Dim objTrans As SqlClient.SqlTransaction
                'If conn.State = ConnectionState.Closed Then
                '    conn.Open()
                'End If
                'objTrans = conn.BeginTransaction
                'xCmd.Transaction = objTrans
                'Try
                '    Dim AwalStatus As String = ""
                '    For isave As Integer = 0 To TBSVSN.Rows.Count - 1
                '        sSql = "SELECT CASE last_trans_type WHEN 'MATERIAL INIT' THEN  'Post' WHEN 'Retur Jual' THEN 'Post' ELSE 'Approved' END  from QL_mstitemDtl WHERE sn ='" & TBSVSN.Rows(isave).Item("id_sn") & "'"
                '        xCmd.CommandText = sSql
                '        AwalStatus = xCmd.ExecuteScalar

                '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = CURRENT_TIMESTAMP, status_item = '" & AwalStatus & "'  where sn ='" & TBSVSN.Rows(isave).Item("id_sn") & "'  and last_trans_type <> 'TI'"
                '        xCmd.CommandText = sSql
                '        xCmd.ExecuteNonQuery()

                '        sSql = "update QL_Mst_SN set status_approval = 'No PROCESS', trntioid = NULL, tglti = NULL  where id_sn ='" & TBSVSN.Rows(isave).Item("id_sn") & "'  and last_trans_type <> 'TI'"
                '        xCmd.CommandText = sSql
                '        xCmd.ExecuteNonQuery()
                '    Next
                '    objTrans.Commit()
                '    conn.Close()
                'Catch ex As Exception
                '    txtStatus.Text = "IN PROCESS"
                '    objTrans.Rollback()
                '    conn.Close()
                '    showMessage(ex.ToString, CompnyName & "- Error", 1, "modalMsgBox")
                'End Try
            End If

            Dim dv As DataView = dtab.DefaultView
            dv.RowFilter = "seq = " & ioid & ""
            If dv.Count > 0 Then
                dv.Delete(0)
            Else
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            dtab.AcceptChanges()

            Dim dr As DataRow
            For i As Integer = 0 To dtab.Rows.Count - 1
                dr = dtab.Rows(i)
                dr.BeginEdit()
                dr("seq") = i + 1
                dr.EndEdit()
            Next
            dtab.AcceptChanges()
            Session("itemdetail") = dtab
            GVDtl.DataSource = dtab
            GVDtl.DataBind()
            If Session("itemdetail") Is Nothing And Session("itemdetailheader") Is Nothing Then
                transtype.Enabled = True
            End If
        End If
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim sMsg As String = "" : Dim trdate As New Date
        Dim period As String = GetDateToPeriodAcctg(GetServerTime())
        Dim transformoid As Integer = 0
        Dim hppitem As Double = 0.0, qtyin As Double = 0.0

        If trnloc.Items.Count <= 0 Or dtlloc.Items.Count <= 0 Then
            sMsg &= "Data lokasi tidak tersedia !<br />" 
        End If
     
        If trndate.Text.Trim = "" Then
            sMsg &= "Tanggal transaksi tidak boleh kosong !<br />"
        End If

        If Date.TryParseExact(trndate.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, trdate) = False Then
            sMsg &= "Format tanggal transaksi salah !<br />"
        End If

        If GVDtl.Rows.Count <= 0 Then
            sMsg &= "Detail item tidak boleh kosong !<br />" 
        End If

        If GVHeader.Rows.Count <= 0 Then
            sMsg &= "Detail item header tidak boleh kosong !<br />" 
        End If

        If GVDtl.Rows.Count > 1 And transtype.SelectedValue.ToString = "Release" Then
            sMsg &= "Jumlah item detail untuk tipe transformasi 'Release' maksimum 1!<br />"
        End If

        If GVHeader.Rows.Count > 1 And transtype.SelectedValue.ToString = "Create" Then
            sMsg &= "Jumlah item header untuk tipe transformasi 'Create' maksimum 1..!!<br />" 
        End If

        'Cek period is open stock
        sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & CompnyCode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'" 
        If GetScalar(sSql) = 0 Then 
            sMsg &= "Tanggal ini tidak dalam periode Open Stock..!!!<br />"
        End If

        'Cek saldoakhir
        If Not Session("itemdetail") Is Nothing Then
            Dim saldoakhire As Double = 0.0
            Dim dtab As DataTable = Session("itemdetail")
            For j As Integer = 0 To dtab.Rows.Count - 1
                sSql = "SELECT isnull(SUM(qtyIn),0.00)-isnull(sum(qtyOut),0.00) FROM QL_conmtr WHERE periodacctg = '" & period & "' AND mtrlocoid = " & dtab.Rows(j).Item("locationoid") & " AND refoid = " & dtab.Rows(j).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : saldoakhire = GetScalar(sSql)
                If saldoakhire = Nothing Or saldoakhire = 0 Then 
                    sMsg &= "Transformasi tidak bisa dilakukan untuk item '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia 0.00 !<br />" 
                End If
            Next
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_TransformItemMst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf, Data sudah ter input, Klik tombol cancel dan mohon untuk cek data pada tab List form..!!<br>"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT status FROM QL_TransformItemMst WHERE transformoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & CompnyCode & "'"

            Dim srest As String = GetStrData(sSql)
            If srest Is Nothing Or srest = "" Then
                txtStatus.Text = "IN PROCESS"
                sMsg &= "Maaf, Data tidak ditemukan..!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
            Else
                If srest.ToLower = "post" Then
                    sMsg &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
                End If
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            txtStatus.Text = "IN PROCESS"
            Exit Sub
        End If
        Dim DrafNo As String = trnno.Text.Trim

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans

        Try

            If i_u.Text = "new" Then
                sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_TransformItemMst' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : transformoid = xCmd.ExecuteScalar + 1
            Else
                transformoid = Session("oid")
            End If

            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_TransformItemDtl' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim transformdtloid As Integer = xCmd.ExecuteScalar + 1

            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_TransformItemDtlMst' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim transformdtlmstoid As Integer = xCmd.ExecuteScalar + 1

            '@@@@@@ Pembuatan Nomor TI @@@@@@@@@@@@@@
            If txtStatus.Text = "POST" Then
                Dim iCurID As Integer = 0
                sSql = "select genother1 from ql_mstgen Where gencode='" & ddlCabang.SelectedValue & "' And gengroup='CABANG'"
                xCmd.CommandText = sSql : Dim cabang As String = xCmd.ExecuteScalar
                Dim noTrn As String = "TI/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transformno, 4) AS INT)), 0) FROM QL_TransformItemMst WHERE cmpcode = '" & CompnyCode & "' AND transformno LIKE '" & noTrn & "%'"
                xCmd.CommandText = sSql : iCurID = xCmd.ExecuteScalar + 1
                trnno.Text = GenNumberString(noTrn, "", iCurID, 4)
            Else
                If i_u.Text = "new" Then
                    trnno.Text = transformoid.ToString 
                End If
            End If

            If i_u.Text = "new" Then
                sSql = "INSERT INTO QL_TransformItemMst (cmpcode, transformoid, transformno, transformdate, transformnote, pic, itemoid, itemqty, unitoid, unitseq, createtime, updtime, upduser, createuser, locoid, status, transformtype, branch_code) VALUES ('" & CompnyCode & "', " & transformoid & ", '" & trnno.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Tchar(trnnote.Text) & "', 0, 0, 0, 0, 0, '" & CDate(toDate(createtime.Text)) & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', '" & Session("UserID") & "', 0, '" & txtStatus.Text & "', '" & transtype.SelectedValue.ToString & "','" & ddlCabang.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & transformoid & " WHERE tablename = 'QL_TransformItemMst' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                sSql = "UPDATE QL_TransformItemMst SET transformno = '" & trnno.Text & "', transformdate = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), transformnote = '" & Tchar(trnnote.Text) & "', pic = 0, itemoid = 0, itemqty = 0, unitoid = 0, unitseq = 0, updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "', locoid = 0, status = '" & txtStatus.Text & "', transformtype = '" & transtype.SelectedValue.ToString & "' WHERE transformoid = " & transformoid & " AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "DELETE FROM QL_TransformItemDtl WHERE transformoid = " & transformoid & " AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "DELETE FROM QL_TransformItemDtlMst WHERE transformoid = " & transformoid & " AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'Generate crdmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

            'Generate trnglmst ID
            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim glmstoid As Integer = xCmd.ExecuteScalar + 1

            'Generate trngldtl ID
            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trngldtl' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim gldtloid As Integer = xCmd.ExecuteScalar + 1

            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_HistHPP' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim oidTemp As Integer = xCmd.ExecuteScalar + 1
            Dim glsequence As Integer = 1

            If txtStatus.Text = "POST" Then
                'Jurnal Hidden cz sama sediaan lawan sediaan, buka jika sediaan per detail item
                sSql = "INSERT INTO QL_trnglmst (cmpcode, branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime,type) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & glmstoid & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', 'Transform Item No. " & trnno.Text & "', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,'TI')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & glmstoid & " WHERE tablename='QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            '----------------- Setup Zipi tuk hpp -----------------

            Dim hppitemheader As Decimal = 0.0 : Dim hppheaderasline As Decimal = 0.0
            Dim totalamounthppfromdetail As Decimal = 0.0
            Dim totalOrder As Decimal = 0.0 : Dim totalDebet As Decimal = 0.0
            Dim hpptukcreate As Decimal = 0.0

            'ambil data2 yang ditambahkan
            If Not Session("itemdetailheader") Is Nothing Then
                Dim dtab As DataTable = Session("itemdetailheader")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    Dim qtyout As Double = qtyout = dtab.Rows(i).Item("qty")

                    sSql = "SELECT ISNULL(hpp,0.00) FROM QL_mstItem WHERE itemoid = " & dtab.Rows(i).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : Dim lastHpp As Double = xCmd.ExecuteScalar
                    If transtype.SelectedValue = "Create" Then
                        ' atas nambah 1 bawah minus banyak
                        hppheaderasline = xCmd.ExecuteScalar ' HPP Barang In
                    Else
                        ' minus itemnya banyak itemdesc merk
                        If xCmd.ExecuteScalar = 0 Then
                            showMessage("Data transform item Release Hpp harus lebih besar dari 0 !<br />Pada Item " & dtab.Rows(i).Item("itemdesc") & " - " & dtab.Rows(i).Item("merk") & "", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                            txtStatus.Text = "IN PROCESS"
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                        totalamounthppfromdetail += xCmd.ExecuteScalar * dtab.Rows(i).Item("qty")
                    End If

                Next
            End If

            'ambil data2 yang dikeluarkan
            If Not Session("itemdetail") Is Nothing Then
                Dim dtab As DataTable = Session("itemdetail")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    Dim qtyout As Double = dtab.Rows(i).Item("qty")
                    sSql = "SELECT ISNULL(hpp,0.00) FROM QL_mstitem WHERE itemoid = " & dtab.Rows(i).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    If transtype.SelectedValue = "Create" Then 'atas nambah banyak bawah minus 1
                        hppitemheader += xCmd.ExecuteScalar * dtab.Rows(i).Item("qty")
                        hpptukcreate += xCmd.ExecuteScalar
                    Else ' minus item hanya 1
                        hppitemheader = xCmd.ExecuteScalar * dtab.Rows(i).Item("qty")
                        hppheaderasline = xCmd.ExecuteScalar
                    End If
                Next
            End If

            If Not Session("itemdetailheader") Is Nothing Then
                Dim dtab As DataTable = Session("itemdetailheader")
                qtyin = 0.0
                Dim xBRanch As String = ddlCabang.SelectedValue : Dim COA_gudang As Integer = 0
                Dim vargudang As String = "" : Dim iMatAccount As String = ""
                Dim mVar As String = "" : Dim sTxt As String = ""

                For i As Integer = 0 To dtab.Rows.Count - 1
                    Dim sTockFlag As String = dtab.Rows(i)("stockflag").ToString

                    If sTockFlag = "I" Then
                        mVar = "VAR_INVENTORY" : sTxt = "PERSEDIAAN NON JUAL"
                    ElseIf sTockFlag = "ASSET" Then
                        mVar = "VAR_GUDANG_ASSET" : sTxt = "PERSEDIAAN ASSET"
                    Else
                        mVar = "VAR_GUDANG" : sTxt = "PERSEDIAAN BARANG DAGANGAN"
                    End If

                    iMatAccount = GetVarInterface(mVar, ddlCabang.SelectedValue)
                    If iMatAccount = "?" Then
                        sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                    ElseIf iMatAccount Is Nothing Or iMatAccount = "" Then
                        sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                    Else
                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccount & "'"
                        xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar
                        If COA_gudang = 0 Or COA_gudang = Nothing Then
                            sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                        End If
                    End If

                    If sMsg <> "" Then
                        txtStatus.Text = "IN PROCESS"
                        showMessage(sMsg, CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    End If

                    sSql = "INSERT INTO QL_TransformItemDtlMst (cmpcode, transformdtloid, transformoid, itemoid, itemqty, unitoid, unitseq, transformnotedtl, locoid) VALUES ('" & CompnyCode & "', " & transformdtlmstoid & ", " & transformoid & ", " & dtab.Rows(i).Item("itemoid") & ", " & dtab.Rows(i).Item("qty") & ", " & dtab.Rows(i).Item("satuan") & ", " & IIf(dtab.Rows(i).Item("satuan") = dtab.Rows(i).Item("satuan1"), 1, IIf(dtab.Rows(i).Item("satuan") = dtab.Rows(i).Item("satuan2"), 2, IIf(dtab.Rows(i).Item("satuan") = dtab.Rows(i).Item("satuan3"), 3, 0))) & ", '" & Tchar(dtab.Rows(i).Item("note")) & "', " & dtab.Rows(i).Item("locationoid") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If txtStatus.Text = "POST" Then
                        'Update stock in
                        '// ==========================
                        '// Persediaan Barang   10.000
                        '// persediaan barang   5000
                        '// persediaan barang   5000
                        '//===========================

                        '======================================================
                        '============AUTO JURNAL & HPP START MASTER============
                        Dim acctgoid As Integer = GetAccountOid(iMatAccount)
                        Dim hppperitem As Double = 0.0, saldoakhir As Double = 0.0
                        qtyin = dtab.Rows(i).Item("qty")
                        'Perhitungan HPP, Saldo Akhir Qty harus dihitung dari Stock All Cabang
                        sSql = "SELECT ISNULL(SUM(qtyIn),0.00)-isnull(sum(qtyOut),0.00) from QL_conmtr WHERE refoid = " & dtab.Rows(i).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "' and periodacctg = '" & GetDateToPeriodAcctg(GetServerTime) & "' "
                        xCmd.CommandText = sSql : saldoakhir = xCmd.ExecuteScalar

                        If saldoakhir < 0 Then
                            txtStatus.Text = "IN PROCESS"
                            objTrans.Rollback() : conn.Close()
                            showMessage("Maaf, Stok akhir ada yang minus..!!", CompnyName & "- Error", 1, "modalMsgBox")
                            Exit Sub
                        End If

                        sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtab.Rows(i).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                        xCmd.CommandText = sSql : hppitem = xCmd.ExecuteScalar()

                        '=================================================================
                        'calculate HPP 
                        Dim qtycreate As Double = 0.0, hpps As Double = 0.0
                        Dim hppcreate As Double = 0.0, hpph As Double = 0.0

                        If transtype.SelectedValue = "Create" Then
                            If Not Session("itemdetail") Is Nothing Then
                                Dim dtl2 As DataTable = Session("itemdetail")
                                For v1 As Integer = 0 To dtl2.Rows.Count - 1
                                    sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtl2.Rows(v1).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql
                                    hpps += xCmd.ExecuteScalar * ToDouble(dtl2.Rows(v1).Item("qty"))
                                Next
                                qtycreate = ToDouble(dtab.Rows(i).Item("qty"))
                                hppcreate = hpps / qtycreate
                            End If

                            hppperitem = ((hppcreate * qtycreate) + (hppheaderasline * saldoakhir)) / (qtyin + saldoakhir)

                            Dim pricePerItem As Decimal = 0.0
                            If transtype.SelectedValue = "Create" Then
                                pricePerItem = ToDouble(hppitemheader) / ToDouble(qtyin)
                            Else
                                pricePerItem = ToDouble(hppheaderasline)
                            End If

                            'Insertkan Ke History HPP
                            sSql = "INSERT INTO QL_HistHPP (cmpcode,oid,transOid,transName,refname,refoid,groupOid,lastHpp,newHpp,totalOldStock,qtyTrans,pricePerItem,periodacctg,updtime,upduser,branch_code)" & _
                            " Values ('" & CompnyCode & "'," & oidTemp & "," & transformdtlmstoid & ",'QL_TransformItemDtlMst','QL_MSTITEM'," & dtab.Rows(i).Item("itemoid") & ",0," & ToDouble(hppitem) & "," & ToDouble(hppperitem) & "," & ToDouble(saldoakhir) & "," & ToDouble(qtyin) & "," & pricePerItem & ",'" & period & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "','" & ddlCabang.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            oidTemp += 1

                            '----End History HPPds----
                            '-------update Hpp--------
                            sSql = "Update QL_mstitem set hpp = " & ToDouble(hppperitem) & " Where itemoid = " & dtab.Rows(i).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            If GVHeader.Rows.Count - 1 = i Then
                                totalOrder = hppitemheader - totalDebet
                            Else
                                totalOrder = hppperitem * qtyin
                            End If

                            '==============AUTO JURNAL & HPP END MASTER==============
                            '========================================================
                            sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code) VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TI',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & trnno.Text & "', " & transformdtlmstoid & ", 'QL_TransformItemDtlMst', " & dtab.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & dtab.Rows(i).Item("satuan") & ", " & dtab.Rows(i).Item("locationoid") & ", " & dtab.Rows(i).Item("qty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(hppperitem) * ToDouble(dtab.Rows(i).Item("qty").ToString) & ", '" & Tchar(dtab.Rows(i).Item("note")) & "', " & ToDouble(hppperitem) & ", '" & ddlCabang.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            conmtroid += 1
                        End If
                    End If
                    '--- Akhir Kondisi Type Create ---
                    transformdtlmstoid = transformdtlmstoid + 1
                Next


                If txtStatus.Text = "POST" Then
                    'insert ke jurnal
                    Dim totalhpphdr As Double = 0.0 : Dim hppdtl As Double = 0.0
                    Dim hppshdr As Double = 0.0 : Dim sa As Double = 0.0
                    Dim hpprelease As Double = 0.0 : Dim hpphdr As Double = 0.0

                    If transtype.SelectedValue = "Create" Then
                        hppitem = 0
                        If Not Session("itemdetail") Is Nothing Then

                            Dim dtl2 As DataTable = Session("itemdetail")
                            For v1 As Integer = 0 To dtl2.Rows.Count - 1
                                sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtl2.Rows(v1).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                Dim hpps As Double = xCmd.ExecuteScalar
                                Dim qty As Double = ToDouble(dtl2.Rows(v1).Item("qty"))
                                Dim https As Double = hppitem
                                hppitem += ToDouble(hpps) * ToDouble(dtl2.Rows(v1).Item("qty"))
                            Next

                            sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'D', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                        End If
                    Else '-- Akhir kondisi type Create --
                        If Not Session("itemdetailheader") Is Nothing Then
                            For i As Integer = 0 To dtab.Rows.Count - 1
                                sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtab.Rows(i).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                hpphdr = xCmd.ExecuteScalar
                                totalhpphdr += ToDouble(hpphdr) * ToDouble(dtab.Rows(i).Item("qty"))
                            Next

                            Dim dtl2 As DataTable = Session("itemdetailheader")
                            If Not Session("itemdetail") Is Nothing Then
                                Dim itemdtl As DataTable = Session("itemdetail")
                                For v1 As Integer = 0 To itemdtl.Rows.Count - 1
                                    sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & itemdtl.Rows(v1).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql
                                    hppdtl = xCmd.ExecuteScalar * itemdtl.Rows(v1).Item("qty")
                                Next
                            End If

                            '----- stockflag = 'I' -----
                            '===========================
                            Dim dtl2v As DataView = dtl2.DefaultView
                            dtl2v.RowFilter = " stockflag = 'I'" : hppitem = 0

                            If dtl2v.Count > 0 Then
                                For v1 As Integer = 0 To dtl2v.Count - 1
                                    sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : Dim hpps As Double = xCmd.ExecuteScalar
                                    sSql = "SELECT isnull(SUM(qtyIn),0.00)-isnull(sum(qtyOut),0.00) from QL_conmtr WHERE refoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "' and periodacctg = '" & GetDateToPeriodAcctg(GetServerTime()) & "' and branch_code = '" & ddlCabang.SelectedValue & "' "
                                    xCmd.CommandText = sSql : sa = xCmd.ExecuteScalar
                                    hppshdr = ((ToDouble(hpps) * ToDouble(dtl2v.Item(v1)("qty"))) / totalhpphdr) * hppdtl
                                    hpprelease = ((sa * ToDouble(hpps) + hppshdr) / (ToDouble(dtl2v.Item(v1)("qty")) + sa))
                                    sSql = "Update QL_mstitem set hpp = " & ToDouble(hpprelease) & " where itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                    'Insertkan Ke History HPP
                                    sSql = "INSERT INTO QL_HistHPP (cmpcode,oid,transOid,transName,refname,refoid,groupOid,lastHpp,newHpp,totalOldStock,qtyTrans,pricePerItem,periodacctg,updtime,upduser,branch_code) " & _
                                    " Values('" & CompnyCode & "'," & oidTemp & "," & transformdtlmstoid & ",'QL_TransformItemDtlMst','QL_MSTITEM'," & dtl2v.Item(v1)("itemoid") & ",0," & ToDouble(hppshdr) & "," & ToDouble(hpprelease) & "," & ToDouble(sa) & "," & ToDouble(dtl2v.Item(v1)("qty")) & "," & ToDouble(hppshdr) & ",'" & period & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "','" & ddlCabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    oidTemp += 1 : hppitem += hppshdr

                                    sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code) VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TI', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & trnno.Text & "', " & transformdtlmstoid & ", 'QL_TransformItemDtlMst', " & dtl2v.Item(v1)("itemoid") & ", 'QL_MSTITEM', " & dtl2v.Item(v1)("satuan") & ", " & dtl2v.Item(v1)("locationoid") & ", " & ToDouble(dtl2v.Item(v1)("qty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtl2v.Item(v1)("note")) & "', " & ToDouble(hpps) & ", '" & ddlCabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    conmtroid += 1
                                    transformdtlmstoid = transformdtlmstoid + 1
                                Next

                                Dim iMatAccounts As String = GetVarInterface("VAR_INVENTORY", ddlCabang.SelectedValue)
                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccounts & "'"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'D', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            End If
                            dtl2v.RowFilter = ""

                            '----- stockflag = T ------
                            '==========================
                            dtl2v.RowFilter = " stockflag='T'"
                            hppitem = 0 : hppshdr = 0 : hpprelease = 0
                            If dtl2v.Count > 0 Then
                                For v1 As Integer = 0 To dtl2v.Count - 1
                                    sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : Dim hpps As Double = xCmd.ExecuteScalar

                                    sSql = "SELECT isnull(SUM(qtyIn),0)-isnull(sum(qtyOut),0) from QL_conmtr WHERE refoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "' and periodacctg = '" & GetDateToPeriodAcctg(CDate(toDate(trndate.Text))) & "' and branch_code = '" & ddlCabang.SelectedValue & "' "
                                    xCmd.CommandText = sSql : sa = xCmd.ExecuteScalar
                                    hppshdr = ((ToDouble(hpps) * ToDouble(dtl2v.Item(v1)("qty"))) / totalhpphdr) * hppdtl
                                    hpprelease = ((sa * ToDouble(hpps) + hppshdr) / (ToDouble(dtl2v.Item(v1)("qty")) + sa))

                                    sSql = "update QL_mstitem set hpp = " & ToDouble(hpprelease) & " where itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    'Insertkan Ke History HPP
                                    sSql = "INSERT INTO QL_HistHPP (cmpcode,oid,transOid,transName,refname,refoid,groupOid,lastHpp,newHpp,totalOldStock,qtyTrans,pricePerItem,periodacctg,updtime,upduser,branch_code) " & _
                                    " Values('" & CompnyCode & "'," & oidTemp & "," & transformdtlmstoid & ",'QL_TransformItemDtlMst','QL_MSTITEM'," & dtl2v.Item(v1)("itemoid") & ",0," & ToDouble(hppshdr) & "," & ToDouble(hpprelease) & "," & ToDouble(sa) & "," & ToDouble(dtl2v.Item(v1)("qty")) & "," & ToDouble(hppshdr) & ",'" & period & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "','" & ddlCabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    oidTemp += 1 : hppitem += hppshdr

                                    sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code) VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TI',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & trnno.Text & "', " & transformdtlmstoid & ", 'QL_TransformItemDtlMst', " & dtl2v.Item(v1)("itemoid") & ", 'QL_MSTITEM', " & dtl2v.Item(v1)("satuan") & ", " & dtl2v.Item(v1)("locationoid") & ", " & ToDouble(dtl2v.Item(v1)("qty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtl2v.Item(v1)("note")) & "', " & ToDouble(hpps) & ", '" & ddlCabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    conmtroid += 1
                                    transformdtlmstoid = transformdtlmstoid + 1
                                Next

                                Dim iMatAccounts As String = GetVarInterface("VAR_GUDANG", ddlCabang.SelectedValue)
                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccounts & "'"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'D', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            End If
                            dtl2v.RowFilter = ""

                            '----- stockflag = ASSET ------
                            '==============================
                            dtl2v.RowFilter = " stockflag='ASSET'"
                            hppitem = 0 : hppshdr = 0 : hpprelease = 0
                            If dtl2v.Count > 0 Then
                                For v1 As Integer = 0 To dtl2v.Count - 1
                                    sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : Dim hpps As Double = xCmd.ExecuteScalar

                                    sSql = "SELECT isnull(SUM(qtyIn),0)-isnull(sum(qtyOut),0) from QL_conmtr WHERE refoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "' and periodacctg = '" & GetDateToPeriodAcctg(CDate(toDate(trndate.Text))) & "' and branch_code = '" & ddlCabang.SelectedValue & "' "
                                    xCmd.CommandText = sSql : sa = xCmd.ExecuteScalar
                                    hppshdr = ((ToDouble(hpps) * ToDouble(dtl2v.Item(v1)("qty"))) / totalhpphdr) * hppdtl
                                    hpprelease = ((sa * ToDouble(hpps) + hppshdr) / (ToDouble(dtl2v.Item(v1)("qty")) + sa))

                                    sSql = "update QL_mstitem set hpp = " & ToDouble(hpprelease) & " where itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    'Insertkan Ke History HPP
                                    sSql = "INSERT INTO QL_HistHPP (cmpcode,oid,transOid,transName,refname,refoid,groupOid,lastHpp,newHpp,totalOldStock,qtyTrans,pricePerItem,periodacctg,updtime,upduser,branch_code) " & _
                                    " Values('" & CompnyCode & "'," & oidTemp & "," & transformdtlmstoid & ",'QL_TransformItemDtlMst','QL_MSTITEM'," & dtl2v.Item(v1)("itemoid") & ",0," & ToDouble(hppshdr) & "," & ToDouble(hpprelease) & "," & ToDouble(sa) & "," & ToDouble(dtl2v.Item(v1)("qty")) & "," & ToDouble(hppshdr) & ",'" & period & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "','" & ddlCabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    oidTemp += 1 : hppitem += hppshdr

                                    sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code) VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TI',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & trnno.Text & "', " & transformdtlmstoid & ", 'QL_TransformItemDtlMst', " & dtl2v.Item(v1)("itemoid") & ", 'QL_MSTITEM', " & dtl2v.Item(v1)("satuan") & ", " & dtl2v.Item(v1)("locationoid") & ", " & ToDouble(dtl2v.Item(v1)("qty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtl2v.Item(v1)("note")) & "', " & ToDouble(hpps) & ", '" & ddlCabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    conmtroid += 1
                                    transformdtlmstoid = transformdtlmstoid + 1
                                Next

                                Dim iMatAccounts As String = GetVarInterface("VAR_GUDANG_ASSET", ddlCabang.SelectedValue)
                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccounts & "'"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'D', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            End If

                            dtl2v.RowFilter = ""

                            '------- stockflag = V --------
                            '==============================
                            dtl2v.RowFilter = " stockflag='V'"
                            hppitem = 0 : hppshdr = 0 : hpprelease = 0
                            If dtl2v.Count > 0 Then
                                For v1 As Integer = 0 To dtl2v.Count - 1
                                    sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : Dim hpps As Double = xCmd.ExecuteScalar

                                    sSql = "SELECT isnull(SUM(qtyIn),0)-isnull(sum(qtyOut),0) from QL_conmtr WHERE refoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "' and periodacctg = '" & GetDateToPeriodAcctg(CDate(toDate(trndate.Text))) & "' and branch_code = '" & ddlCabang.SelectedValue & "' "
                                    xCmd.CommandText = sSql : sa = xCmd.ExecuteScalar
                                    hppshdr = ((ToDouble(hpps) * ToDouble(dtl2v.Item(v1)("qty"))) / totalhpphdr) * hppdtl
                                    hpprelease = ((sa * ToDouble(hpps) + hppshdr) / (ToDouble(dtl2v.Item(v1)("qty")) + sa))

                                    sSql = "update QL_mstitem set hpp = " & ToDouble(hpprelease) & " where itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    'Insertkan Ke History HPP
                                    sSql = "INSERT INTO QL_HistHPP (cmpcode, oid, transOid, transName, refname, refoid, groupOid, lastHpp, newHpp, totalOldStock, qtyTrans, pricePerItem, periodacctg, updtime, upduser, branch_code) " & _
                                    " Values('" & CompnyCode & "'," & oidTemp & "," & transformdtlmstoid & ",'QL_TransformItemDtlMst','QL_MSTITEM'," & dtl2v.Item(v1)("itemoid") & ",0," & ToDouble(hppshdr) & "," & ToDouble(hpprelease) & "," & ToDouble(sa) & "," & ToDouble(dtl2v.Item(v1)("qty")) & "," & ToDouble(hppshdr) & ",'" & period & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "','" & ddlCabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    oidTemp += 1 : hppitem += hppshdr

                                    sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code) VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TI',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & trnno.Text & "', " & transformdtlmstoid & ", 'QL_TransformItemDtlMst', " & dtl2v.Item(v1)("itemoid") & ", 'QL_MSTITEM', " & dtl2v.Item(v1)("satuan") & ", " & dtl2v.Item(v1)("locationoid") & ", " & ToDouble(dtl2v.Item(v1)("qty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtl2v.Item(v1)("note")) & "', " & ToDouble(hpps) & ", '" & ddlCabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    conmtroid += 1
                                    transformdtlmstoid = transformdtlmstoid + 1
                                Next

                                Dim iMatAccounts As String = GetVarInterface("VAR_GUDANG", ddlCabang.SelectedValue)
                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccounts & "'"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'D', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            End If

                            dtl2v.RowFilter = ""

                        End If
                    End If
                End If
                '-- Akhir kondisi type release --
            End If

            If Not Session("itemdetail") Is Nothing Then
                Dim dtab As DataTable = Session("itemdetail")
                Dim qtyout As Double = 0.0 : Dim xBRanch As String = ddlCabang.SelectedValue
                Dim COA_gudang As Integer = 0 : Dim vargudang As String = ""
                Dim iMatAccount As String = "" : Dim sTockFlag As String = ""
                Dim mVar As String = "" : Dim sTxt As String = ""

                For i As Integer = 0 To dtab.Rows.Count - 1
                    Dim sSql As String = "Select stockflag From ql_mstitem Where itemoid= " & dtab.Rows(i)("itemoid").ToString & ""
                    xCmd.CommandText = sSql : sTockFlag = xCmd.ExecuteScalar()

                    If sTockFlag = "I" Then
                        mVar = "VAR_INVENTORY" : sTxt = "PERSEDIAAN NON JUAL"
                    ElseIf sTockFlag = "ASSET" Then
                        mVar = "VAR_GUDANG_ASSET" : sTxt = "PERSEDIAAN ASSET"
                    Else
                        mVar = "VAR_GUDANG" : sTxt = "PERSEDIAAN BARANG DAGANGAN"
                    End If

                    iMatAccount = GetVarInterface(mVar, ddlCabang.SelectedValue)
                    If iMatAccount = "?" Then
                        sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                    ElseIf iMatAccount Is Nothing Or iMatAccount = "" Then
                        sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                    Else
                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccount & "'"
                        xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar
                        If COA_gudang = 0 Or COA_gudang = Nothing Then
                            sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                        End If
                    End If

                    If sMsg <> "" Then
                        txtStatus.Text = "IN PROCESS"
                        showMessage(sMsg, CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    End If

                    sSql = "INSERT INTO QL_TransformItemDtl (cmpcode, transformdtloid, transformoid, itemoid, itemqty, unitoid, unitseq,transformnotedtl,locoid) VALUES " & _
                    "('" & CompnyCode & "', " & transformdtloid & ", " & transformoid & ", " & dtab.Rows(i).Item("itemoid") & ", " & dtab.Rows(i).Item("qty") & ", " & dtab.Rows(i).Item("satuan") & ", " & IIf(dtab.Rows(i).Item("satuan") = dtab.Rows(i).Item("satuan1"), 1, IIf(dtab.Rows(i).Item("satuan") = dtab.Rows(i).Item("satuan2"), 2, IIf(dtab.Rows(i).Item("satuan") = dtab.Rows(i).Item("satuan3"), 3, 0))) & ", '" & Tchar(dtab.Rows(i).Item("note")) & "', " & dtab.Rows(i).Item("locationoid") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If txtStatus.Text = "POST" Then
                        'Update stock out
                        qtyout = dtab.Rows(i).Item("qty")

                        sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtab.Rows(i).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                        xCmd.CommandText = sSql : hppitem = xCmd.ExecuteScalar

                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code) VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TI', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & trnno.Text & "', " & transformdtloid & ", 'QL_TransformItemDtl', " & dtab.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & dtab.Rows(i).Item("satuan") & ", " & dtab.Rows(i).Item("locationoid") & ", 0, " & dtab.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtab.Rows(i).Item("note")) & "', " & ToDouble(hppitem) & ", '" & ddlCabang.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                    End If
                    transformdtloid = transformdtloid + 1
                Next

                '---- Ketika Type Realease ----
                If txtStatus.Text = "POST" Then
                    If transtype.SelectedValue <> "Create" Then
                        'RELEASE DETAIL
                        If Not Session("itemdetail") Is Nothing Then
                            Dim dtl2 As DataTable = Session("itemdetail")
                            Dim hppperitem As Double = 0.0 : hppitem = 0
                            Dim saldoakhir As Double = 0.0

                            For v1 As Integer = 0 To dtl2.Rows.Count - 1
                                sSql = "SELECT ISNULL(HPP, 0.00) FROM QL_mstitem WHERE itemoid = " & dtl2.Rows(v1).Item("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                Dim hpps As Double = xCmd.ExecuteScalar
                                hppitem += ToDouble(hpps) * ToDouble(dtl2.Rows(v1).Item("qty"))

                                Dim sTockFlags As String = dtl2.Rows(v1).Item("stockflag")
                                If sTockFlag = "I" Then
                                    mVar = "VAR_INVENTORY" : sTxt = "PERSEDIAAN NON JUAL"
                                ElseIf sTockFlag = "ASSET" Then
                                    mVar = "VAR_GUDANG_ASSET" : sTxt = "PERSEDIAAN ASSET"
                                Else
                                    mVar = "VAR_GUDANG" : sTxt = "PERSEDIAAN BARANG DAGANGAN"
                                End If

                                iMatAccount = GetVarInterface(mVar, ddlCabang.SelectedValue)
                                If iMatAccount = "?" Then
                                    sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                                ElseIf iMatAccount Is Nothing Or iMatAccount = "" Then
                                    sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                                Else
                                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccount & "'"
                                    xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar
                                    If COA_gudang = 0 Or COA_gudang = Nothing Then
                                        sMsg &= "Interface untuk Akun " & mVar & " tidak ditemukan!<br />"
                                    End If
                                End If

                                If sMsg <> "" Then
                                    txtStatus.Text = "IN PROCESS"
                                    showMessage(sMsg, CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                                    objTrans.Rollback() : conn.Close()
                                    Exit Sub
                                End If

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'C', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            Next
                        End If
                    Else
                        'CREATE DETAIL
                        If Not Session("itemdetail") Is Nothing Then
                            Dim dtl2 As DataTable = Session("itemdetail")
                            Dim dtl2v As DataView = dtl2.DefaultView
                            hppitem = 0
                            Dim hppperitem As Double = 0.0, saldoakhir As Double = 0.0

                            '----- stockflag = INVENTORY ------
                            '==================================
                            dtl2v.RowFilter = " stockflag = 'I'"
                            If dtl2v.Count > 0 Then
                                For v1 As Integer = 0 To dtl2v.Count - 1
                                    sSql = "SELECT ISNULL(HPP,0.00) FROM QL_mstitem WHERE itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql
                                    Dim hpps As Double = xCmd.ExecuteScalar
                                    hppitem += ToDouble(hpps) * ToDouble(dtl2v.Item(v1)("qty"))
                                Next

                                Dim iMatAccounts As String = GetVarInterface("VAR_INVENTORY", ddlCabang.SelectedValue)

                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccounts & "'"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'C', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            End If
                            dtl2v.RowFilter = ""

                            '----- stockflag = TRANSAKSI ------
                            '==================================
                            dtl2v.RowFilter = " stockflag = 'T'"
                            hppitem = 0
                            If dtl2v.Count > 0 Then
                                For v1 As Integer = 0 To dtl2v.Count - 1
                                    sSql = "SELECT ISNULL(HPP,0.00) FROM QL_mstitem WHERE itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : Dim hpps As Double = xCmd.ExecuteScalar
                                    hppitem += ToDouble(hpps) * ToDouble(dtl2v.Item(v1)("qty"))
                                Next
                                Dim iMatAccounts As String = GetVarInterface("VAR_GUDANG", ddlCabang.SelectedValue)

                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccounts & "'" : xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'C', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            End If
                            dtl2v.RowFilter = ""

                            '----- stockflag = ASSET ------
                            '==============================                            
                            dtl2v.RowFilter = " stockflag = 'ASSET'"
                            hppitem = 0
                            If dtl2v.Count > 0 Then
                                For v1 As Integer = 0 To dtl2v.Count - 1
                                    sSql = "SELECT ISNULL(HPP,0.00) FROM QL_mstitem WHERE itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : Dim hpps As Double = xCmd.ExecuteScalar
                                    hppitem += ToDouble(hpps) * ToDouble(dtl2v.Item(v1)("qty"))
                                Next
                                Dim iMatAccounts As String = GetVarInterface("VAR_GUDANG_ASSET", ddlCabang.SelectedValue)

                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccounts & "'"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'C', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            End If
                            dtl2v.RowFilter = ""

                            '----- stockflag = V ------
                            '==============================                            
                            dtl2v.RowFilter = " stockflag = 'V'"
                            hppitem = 0
                            If dtl2v.Count > 0 Then
                                For v1 As Integer = 0 To dtl2v.Count - 1
                                    sSql = "SELECT ISNULL(HPP,0.00) FROM QL_mstitem WHERE itemoid = " & dtl2v.Item(v1)("itemoid") & " AND cmpcode = '" & CompnyCode & "'"
                                    xCmd.CommandText = sSql : Dim hpps As Double = xCmd.ExecuteScalar
                                    hppitem += ToDouble(hpps) * ToDouble(dtl2v.Item(v1)("qty"))
                                Next
                                Dim iMatAccounts As String = GetVarInterface("VAR_GUDANG", ddlCabang.SelectedValue)

                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccounts & "'"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', '" & ddlCabang.SelectedValue & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'C', " & hppitem & ", " & hppitem & ", '" & trnno.Text & "', 'Transform Item No. " & trnno.Text & "', '" & transformoid & "', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid = gldtloid + 1 : glsequence = glsequence + 1
                            End If
                            dtl2v.RowFilter = ""
                        End If
                    End If
                End If

                'Update lastoid QL_TransformItemDtl
                sSql = "UPDATE QL_mstoid SET lastoid = " & transformdtloid - 1 & " WHERE tablename = 'QL_TransformItemDtl' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_TransformItemDtlMst
                sSql = "UPDATE QL_mstoid SET lastoid = " & transformdtlmstoid - 1 & " WHERE tablename = 'QL_TransformItemDtlMst' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid set lastoid=" & oidTemp - 1 & " Where tablename = 'QL_HistHPP' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_trngldtl
                sSql = "UPDATE QL_mstoid SET lastoid = " & gldtloid - 1 & " WHERE tablename = 'QL_trngldtl' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If

            objTrans.Commit() : conn.Close()

        Catch ex As Exception
            txtStatus.Text = "IN PROCESS"
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & "- Error", 1, "modalMsgBox")
            Exit Sub
        End Try 
        Response.Redirect("trntransformorder.aspx?awal=true")
    End Sub

    Protected Sub imbDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbDelete.Click
 
        sSql = "SELECT id_sn from QL_Mst_SN WHERE trntioid = '" & trnno.Text.Trim & "' and last_trans_type = 'TI'"
        Session("SamaTI") = cKon.ambiltabel(sSql, "DelSN")
        Dim del As DataTable = Session("SamaTI")

        sSql = "SELECT id_sn from QL_Mst_SN WHERE trntioid = '" & trnno.Text.Trim & "' and last_trans_type <> 'TI'"
        Session("TidakTI") = cKon.ambiltabel2(sSql, "UPSN")
        Dim TBUPSN As DataTable = Session("TidakTI")

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT status FROM QL_TransformItemMst WHERE transformoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transform item tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            Else
                If srest = "POST" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transform item tidak dapat diposting !<br />Periksa bila data telah diposting oleh user lain", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If

            If del.Rows.Count > 0 Then
                For i As Integer = 0 To del.Rows.Count - 1
                    sSql = "DELETE QL_mstitemDtl WHERE sn = '" & del.Rows(i).Item("id_sn").ToString.Trim & "'  and last_trans_type = 'TI'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "DELETE QL_Mst_SN WHERE id_sn = '" & del.Rows(i).Item("id_sn").ToString.Trim & "'  and last_trans_type = 'TI' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Next
            End If

            If TBUPSN.Rows.Count > 0 Then
                For j As Integer = 0 To TBUPSN.Rows.Count - 1
                    Dim AwalStatus As String = ""

                    sSql = "SELECT CASE last_trans_type WHEN 'MATERIAL INIT' THEN  'Post' WHEN 'Retur Jual' THEN 'Post' ELSE 'Approved' END  from QL_mstitemDtl WHERE sn ='" & TBUPSN.Rows(j).Item("id_sn").ToString.Trim & "'"

                    xCmd.CommandText = sSql : AwalStatus = xCmd.ExecuteScalar

                    sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = CURRENT_TIMESTAMP, status_item = '" & AwalStatus & "' where sn ='" & TBUPSN.Rows(j).Item("id_sn").ToString.Trim & "' and last_trans_type <> 'TI'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update QL_Mst_SN set status_approval = 'No PROCESS', trntioid = NULL, tglti = NULL where id_sn ='" & TBUPSN.Rows(j).Item("id_sn").ToString.Trim & "' and last_trans_type <> 'TI'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            sSql = "DELETE FROM QL_TransformItemMst WHERE transformoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_TransformItemDtl WHERE transformoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_TransformItemDtlMst WHERE transformoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & "- Error", 1, "modalMsgBox")
            Exit Sub
        End Try
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        'Response.Redirect("trntransformorder.aspx?awal=true")
    End Sub

    Protected Sub imbPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPost.Click
        txtStatus.Text = "POST"
        If CheckDateIsClosedMtr(toDate(trndate.Text), CompnyCode) Then
            showMessage("This Periode was closed !", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            txtStatus.Text = "IN PROCESS"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(trndate.Text), CompnyCode) = False Then
            showMessage("This is not active periode !", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            txtStatus.Text = "IN PROCESS"
            Exit Sub
        End If
        imbSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub GVmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmst.PageIndexChanging
        GVmst.PageIndex = e.NewPageIndex
        Dim dtab As DataTable = Session("TI")
        GVmst.DataSource = dtab
        GVmst.DataBind()
    End Sub

    Protected Sub imbFindBoM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindBoM.Click
        BindData(" AND " & FilterDDL.SelectedValue.ToString & " LIKE '%" & Tchar(FilterText.Text) & "%'")
    End Sub

    Protected Sub imbAllBoM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAllBoM.Click
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        BindData("")
    End Sub

    Protected Sub unitdtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unitdtl.SelectedIndexChanged
        Dim transdate As Date = Date.ParseExact(trndate.Text, "dd/MM/yyyy", Nothing)
        conn.Open()

        sSql = "SELECT isnull(SUM(a.qtyIn),0)-isnull(sum(a.qtyOut),0) AS saldoakhir, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3 from QL_conmtr a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' WHERE a.refoid = " & Integer.Parse(itemdtloid.Text) & " AND a.periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "' AND a.mtrlocoid = " & dtlloc.SelectedValue & " AND a.cmpcode = '" & CompnyCode & "'"
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        Dim totmaxqty As Double = 0.0
        If xreader.HasRows Then
            While xreader.Read
                If unitdtl.SelectedValue = xreader("satuan2") Then
                    totmaxqty = xreader("saldoakhir") / xreader("konversi2_3")
                ElseIf unitdtl.SelectedValue = xreader("satuan1") Then
                    totmaxqty = xreader("saldoakhir") / xreader("konversi1_2") / xreader("konversi2_3")
                Else
                    totmaxqty = xreader("saldoakhir")
                End If
            End While
        End If
        maxQty.Text = Format(totmaxqty, "#,##0.00")

        conn.Close()
    End Sub

    Protected Sub imbAddToListHead_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAddToListHead.Click
        LabelPesanSn.Text = ""
        If trnitem.Text.Trim = "" Or trnitemoid.Text = "" Then
            showMessage("Item tidak boleh kosong!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If trnloc.SelectedValue = "None" Then
            showMessage("Pilih lokasi terlebih dahulu!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If transtype.SelectedValue = "Type" Then
            showMessage("Pilih Tipe Transformation terlebih dahulu! <br/> Note <br/> <strong> >> Create << </strong> <br/>banyak Item yang ada Detail Menjadi Menjadi 1 item yang di Header <br/><br/> <strong> >> Release << </strong> <br/> banyak Item yang ada Header Menjadi Menjadi 1 item yang di Detail  ", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If Double.Parse(itemqty.Text.Trim) = 0.0 Then
            showMessage("Item quantity harus lebih dari 0!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If i_u_head.Text = "new" Then
            If transtype.SelectedValue = "Create" Then
                If GVHeader.Rows.Count >= 1 Then
                    showMessage("Jumlah item header untuk tipe tranformasi 'Create' maksimal 1!", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If
        End If

        If i_u_head.Text = "new" Then
            If Session("itemdetailheader") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("locationoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("location", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("unit1", Type.GetType("System.String"))
                dtab.Columns.Add("unit2", Type.GetType("System.String"))
                dtab.Columns.Add("unit3", Type.GetType("System.String"))
                dtab.Columns.Add("note", Type.GetType("System.String"))
                dtab.Columns.Add("itemcode", Type.GetType("System.String"))
                dtab.Columns.Add("itemflag", Type.GetType("System.String"))
                dtab.Columns.Add("stockflag", Type.GetType("System.String"))
                Session("itemdetailheader") = dtab
                labelseqhead.Text = "1"
            Else 'stockflag
                dtab = Session("itemdetailheader")
                labelseqhead.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetailheader")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(trnitemoid.Text) & " AND merk = '" & merk.Text & "' AND locationoid = " & trnloc.SelectedValue & " AND seq <> " & Integer.Parse(labelseqhead.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Item tidak bisa ditambahkan ke dalam list !<br /> Item ini telah ada di dalam list !", CompnyName & "- Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If

        sSql = "SELECT DISTINCT isnull(i.has_sn,0) from QL_mstItem i INNER JOIN QL_mstItem_branch ib ON i.itemoid = ib.itemoid WHERE i.itemcode = '" & ItemcodeHeHeader.Text & "' and ib.branch_code = '" & ddlCabang.SelectedValue & "'"
        Dim has_sn As String = cKon.ambilscalar(sSql)
        If has_sn = "1" Then
            '@@@@@@@@@@@@@@@@@@@@@@@@ Jika Menggunakan SN
            If transtype.SelectedValue = "Release" Then
                '@@@@@@@@@@@@@@@@@@@@@@@@ Jika Type Sama Dengan Release MAsukan SN
                StatusIsiSN.Text = "Header"
                'btnHideSN.Visible = False
                TextSN.Focus()
                If i_u.Text = "new" Then
                    If Session("TblSN") Is Nothing Then
                        pnlInvnSN.Visible = True
                        btnHideSN.Visible = True
                        TextItem.Text = trnitem.Text
                        TextQty.Text = itemqty.Text
                        TextSatuan.Text = itemunit.SelectedItem.ToString
                        KodeItem.Text = ItemcodeHeHeader.Text
                        GVSN.DataSource = Nothing
                        GVSN.DataBind()
                        ModalPopupSN.Show()
                        TextSN.Text = ""
                        LabelPesanSn.Text = ""
                        TextSN.Focus()
                        btnGenerateSN.Visible = True
                        btnGenerateSN.Visible = True
                    Else
                        Dim objTableSN As DataTable
                        objTableSN = Session("TblSN")
                        Dim dvjum As DataView = objTableSN.DefaultView
                        dvjum.RowFilter = "itemoid= '" & trnitemoid.Text & "'"
                        pnlInvnSN.Visible = True
                        btnHideSN.Visible = True
                        TextQty.Text = itemqty.Text
                        TextSatuan.Text = itemunit.SelectedItem.ToString
                        TextItem.Text = trnitem.Text
                        KodeItem.Text = ItemcodeHeHeader.Text
                        GVSN.DataSource = Nothing
                        GVSN.DataSource = dvjum
                        GVSN.DataSource = dvjum
                        GVSN.DataBind()
                        ModalPopupSN.Show()
                        TextSN.Text = ""
                        TextSN.Focus()
                        btnGenerateSN.Visible = True
                    End If

                Else
                    If txtStatus.Text = "IN PROCESS" Or txtStatus.Text = "In Approval" Or txtStatus.Text = "Approved" Or txtStatus.Text = "POST" Then
                        pnlInvnSN.Visible = True
                        btnHideSN.Visible = True
                        sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntioid = '" & trnno.Text & "' AND i.itemoid = '" & trnitemoid.Text & "' "
                        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
                        Dim objDs As New DataSet
                        mySqlDA.Fill(objDs, "dataSN")
                        GVSN.DataSource = Nothing
                        GVSN.DataSource = objDs.Tables("dataSN")
                        GVSN.DataBind()
                        Session("TblSN") = objDs.Tables("dataSN")
                        TextQty.Text = itemqty.Text
                        TextSatuan.Text = itemunit.SelectedItem.ToString
                        TextItem.Text = trnitem.Text
                        KodeItem.Text = ItemcodeHeHeader.Text
                        ModalPopupSN.Show()
                        TextSN.Text = ""
                        LabelPesanSn.Text = ""
                        TextSN.Focus()
                        btnGenerateSN.Visible = True
                    End If
                End If
            ElseIf transtype.SelectedValue = "Create" Then
                '@@@@@@@@@@@@@@@@@@@@@@@@ Jika Type Sama Dengan Create Lansung Add To List
                Dim drow As DataRow
                Dim drowedit() As DataRow
                If i_u_head.Text = "new" Then
                    drow = dtab.NewRow

                    drow("seq") = Integer.Parse(labelseqhead.Text)
                    drow("itemoid") = Integer.Parse(trnitemoid.Text)
                    drow("itemdesc") = trnitem.Text.Trim
                    drow("merk") = merk.Text
                    drow("locationoid") = trnloc.SelectedValue
                    drow("location") = trnloc.SelectedItem.ToString
                    drow("qty") = Double.Parse(itemqty.Text)
                    drow("satuan") = itemunit.SelectedValue
                    drow("unit") = itemunit.SelectedItem.Text

                    drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                    drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                    drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                    drow("unit1") = labelunit1.Text
                    drow("unit2") = labelunit2.Text
                    drow("unit3") = labelunit3.Text

                    drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                    drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                    drow("note") = trnnote.Text.Trim
                    drow("itemcode") = ItemcodeHeHeader.Text
                    drow("itemflag") = stockflag.Text
                    drow("stockflag") = FlagItem1.Text
                    'dtab.Columns.Add("stockflag", Type.GetType("System.String"))
                    dtab.Rows.Add(drow)
                    dtab.AcceptChanges()
                    transtype.Enabled = False
                Else
                    drowedit = dtab.Select("seq = " & Integer.Parse(labelseqhead.Text) & "", "")
                    drow = drowedit(0)
                    drowedit(0).BeginEdit()

                    drow("itemoid") = Integer.Parse(trnitemoid.Text)
                    drow("itemdesc") = trnitem.Text.Trim
                    drow("merk") = merk.Text
                    drow("locationoid") = trnloc.SelectedValue
                    drow("location") = trnloc.SelectedItem.ToString
                    drow("qty") = Double.Parse(itemqty.Text)
                    drow("satuan") = itemunit.SelectedValue
                    drow("unit") = itemunit.SelectedItem.Text

                    drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                    drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                    drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                    drow("unit1") = labelunit1.Text
                    drow("unit2") = labelunit2.Text
                    drow("unit3") = labelunit3.Text

                    drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                    drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                    drow("note") = trnnote.Text.Trim
                    drow("itemcode") = ItemcodeHeHeader.Text
                    drow("itemflag") = stockflag.Text
                    drow("stockflag") = FlagItem1.Text
                    drowedit(0).EndEdit()
                    dtab.Select(Nothing, Nothing)
                    dtab.AcceptChanges()
                    transtype.Enabled = False
                End If

                GVHeader.DataSource = dtab
                GVHeader.DataBind()
                Session("itemdetailheader") = dtab

                labelseqhead.Text = (GVHeader.Rows.Count + 1).ToString
                merk.Text = ""
                itemqty.Text = "0.00"
                itemunit.Items.Clear()
                trnnote.Text = "" : trnitemoid.Text = ""
                trnitem.Text = "" : i_u_head.Text = "new"

                labelsatuan1.Text = "" : labelsatuan2.Text = ""
                labelsatuan3.Text = "" : labelunit1.Text = ""
                labelunit2.Text = "" : labelunit3.Text = ""
                labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
                stockflag.Text = ""
                GVHeader.SelectedIndex = -1
                GVHeader.Columns(10).Visible = True

                If gvItemSearch.Visible = True Then
                    gvItemSearch.DataSource = Nothing
                    gvItemSearch.DataBind()
                    gvItemSearch.Visible = False
                    gvItemSearch.SelectedIndex = -1
                    Session("itemsearch") = Nothing
                End If
            End If
            cProc.SetFocusToControl(Me.Page, TextSN)
        Else

            '@@@@@@@@@@@@@@@@@@@@@@@@ Jika Tidak Menggunakan SN
            Dim drow As DataRow
            Dim drowedit() As DataRow
            If i_u_head.Text = "new" Then
                drow = dtab.NewRow

                drow("seq") = Integer.Parse(labelseqhead.Text)
                drow("itemoid") = Integer.Parse(trnitemoid.Text)
                drow("itemdesc") = trnitem.Text.Trim
                drow("merk") = merk.Text
                drow("locationoid") = trnloc.SelectedValue
                drow("location") = trnloc.SelectedItem.ToString
                drow("qty") = Double.Parse(itemqty.Text)
                drow("satuan") = itemunit.SelectedValue
                drow("unit") = itemunit.SelectedItem.Text

                drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                drow("unit1") = labelunit1.Text
                drow("unit2") = labelunit2.Text
                drow("unit3") = labelunit3.Text

                drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                drow("note") = trnnote.Text.Trim
                drow("itemcode") = ItemcodeHeHeader.Text
                drow("itemflag") = stockflag.Text
                drow("stockflag") = FlagItem1.Text
                dtab.Rows.Add(drow)
                dtab.AcceptChanges()
                transtype.Enabled = False
            Else
                drowedit = dtab.Select("seq = " & Integer.Parse(labelseqhead.Text) & "", "")
                drow = drowedit(0)
                drowedit(0).BeginEdit()

                drow("itemoid") = Integer.Parse(trnitemoid.Text)
                drow("itemdesc") = trnitem.Text.Trim
                drow("merk") = merk.Text
                drow("locationoid") = trnloc.SelectedValue
                drow("location") = trnloc.SelectedItem.ToString
                drow("qty") = Double.Parse(itemqty.Text)
                drow("satuan") = itemunit.SelectedValue
                drow("unit") = itemunit.SelectedItem.Text

                drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                drow("unit1") = labelunit1.Text
                drow("unit2") = labelunit2.Text
                drow("unit3") = labelunit3.Text

                drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                drow("note") = trnnote.Text.Trim
                drow("itemcode") = ItemcodeHeHeader.Text
                drow("itemflag") = stockflag.Text
                drow("stockflag") = FlagItem1.Text
                drowedit(0).EndEdit()
                dtab.Select(Nothing, Nothing)
                dtab.AcceptChanges()
                transtype.Enabled = False
            End If

            GVHeader.DataSource = dtab
            GVHeader.DataBind()
            Session("itemdetailheader") = dtab

            labelseqhead.Text = (GVHeader.Rows.Count + 1).ToString
            merk.Text = ""
            itemqty.Text = "0.00"
            itemunit.Items.Clear()
            trnnote.Text = "" : trnitemoid.Text = ""
            trnitem.Text = "" : i_u_head.Text = "new"

            labelsatuan1.Text = "" : labelsatuan2.Text = ""
            labelsatuan3.Text = "" : labelunit1.Text = ""
            labelunit2.Text = "" : labelunit3.Text = ""
            labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
            stockflag.Text = "" : GVHeader.SelectedIndex = -1
            GVHeader.Columns(10).Visible = True

            If gvItemSearch.Visible = True Then
                gvItemSearch.DataSource = Nothing
                gvItemSearch.DataBind()
                gvItemSearch.Visible = False
                gvItemSearch.SelectedIndex = -1
                Session("itemsearch") = Nothing
            End If
        End If

    End Sub

    Protected Sub imbClearHead_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearHead.Click
        trnloc.SelectedValue = "None"
        trnitem.Text = ""
        merk.Text = ""
        trnitemoid.Text = ""
        itemqty.Text = "0.00"
        itemunit.Items.Clear()
        trnnote.Text = ""
        i_u_head.Text = "new"
        labelref.Text = ""

        If gvItemSearch.Visible = True Then
            gvItemSearch.DataSource = Nothing
            gvItemSearch.DataBind()
            gvItemSearch.Visible = False
            gvItemSearch.SelectedIndex = -1
            Session("itemsearch") = Nothing
        End If

        GVHeader.SelectedIndex = -1
        GVHeader.Columns(8).Visible = True
    End Sub

    Protected Sub GVHeader_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVHeader.RowDeleting
        If Not Session("itemdetailheader") Is Nothing Then

            Dim cseq As Integer = 0
            Dim dtab As DataTable = Session("itemdetailheader")
            Dim ioid As Integer = Integer.Parse(GVHeader.Rows(e.RowIndex).Cells(1).Text)
            If i_u.Text = "new" Then
                '@@@@@@@@@@@@@@@@@@ Hapus SN Per Item @@@@@@@@@@@@@@@
                If Not Session("TblSN") Is Nothing Then
                    Dim objTableSN As DataTable = Session("TblSN")
                    Dim dvSN As DataView = objTableSN.DefaultView
                    Dim itemid As String = dtab.Rows(e.RowIndex).Item("itemoid")
                    dvSN.RowFilter = "itemoid = " & itemid
                    If Not dvSN.Count = 0 Then
                        For kdsn As Integer = 0 To dvSN.Count - 1
                            objTableSN.Rows.RemoveAt(0)
                        Next
                    End If
                    objTableSN.AcceptChanges()
                End If
            Else

                sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntioid = '" & trnno.Text & "' AND i.itemoid = '" & dtab.Rows(e.RowIndex).Item("itemoid") & "' "
                Dim TBSVSN As DataTable = cKon.ambiltabel(sSql, "SNsave")

                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction
                xCmd.Transaction = objTrans
                Try

                    For isave As Integer = 0 To TBSVSN.Rows.Count - 1
                        sSql = "DELETE QL_mstitemDtl WHERE sn = '" & TBSVSN.Rows(isave).Item("id_sn") & "'  and last_trans_type = 'TI'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "DELETE QL_Mst_SN WHERE id_sn = '" & TBSVSN.Rows(isave).Item("id_sn") & "'  and last_trans_type = 'TI' "
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    txtStatus.Text = "IN PROCESS"
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.ToString, CompnyName & "- Error", 1, "modalMsgBox")
                End Try
               
            End If


            Dim dv As DataView = dtab.DefaultView
            dv.RowFilter = "seq = " & ioid & ""
            If dv.Count > 0 Then
                dv.Delete(0)
            Else
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            dtab.AcceptChanges()

            Dim dr As DataRow
            For i As Integer = 0 To dtab.Rows.Count - 1
                dr = dtab.Rows(i)
                dr.BeginEdit()
                dr("seq") = i + 1
                dr.EndEdit()
            Next
            dtab.AcceptChanges()

            Session("itemdetailheader") = dtab
            GVHeader.DataSource = dtab
            GVHeader.DataBind()
            GVHeader.SelectedIndex = -1

            If dtab.Rows.Count = 0 Then
                transtype.Enabled = True
            End If
           
        End If
    End Sub

    Protected Sub GVHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVHeader.SelectedIndexChanged
        i_u_head.Text = "edit"

        trnloc.SelectedValue = GVHeader.SelectedDataKey("locationoid")
        trnitemoid.Text = GVHeader.SelectedDataKey("itemoid")
        ItemcodeHeHeader.Text = GVHeader.Rows(GVHeader.SelectedIndex).Cells(2).Text
        trnitem.Text = Replace(GVHeader.Rows(GVHeader.SelectedIndex).Cells(3).Text, "&quot;", "")
        itemqty.Text = GVHeader.Rows(GVHeader.SelectedIndex).Cells(5).Text

        trnnote.Text = GVHeader.Rows(GVHeader.SelectedIndex).Cells(8).Text.Replace("&nbsp;", "")
        merk.Text = GVHeader.Rows(GVHeader.SelectedIndex).Cells(4).Text

        itemunit.Items.Clear()
        itemunit.Items.Add(GVHeader.SelectedDataKey("unit3"))
        itemunit.Items(itemunit.Items.Count - 1).Value = GVHeader.SelectedDataKey("satuan3")
        If GVHeader.SelectedDataKey("satuan2") <> GVHeader.SelectedDataKey("satuan3") Then
            itemunit.Items.Add(GVHeader.SelectedDataKey("unit2"))
            itemunit.Items(itemunit.Items.Count - 1).Value = GVHeader.SelectedDataKey("satuan2")
        End If
        If GVHeader.SelectedDataKey("satuan1") <> GVHeader.SelectedDataKey("satuan3") And GVHeader.SelectedDataKey("satuan1") <> GVHeader.SelectedDataKey("satuan2") Then
            itemunit.Items.Add(GVHeader.SelectedDataKey("unit1"))
            itemunit.Items(itemunit.Items.Count - 1).Value = GVHeader.SelectedDataKey("satuan1")
        End If
        itemunit.SelectedValue = GVHeader.SelectedDataKey("satuan")

        labelsatuan1.Text = GVHeader.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVHeader.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVHeader.SelectedDataKey("satuan3")
        labelunit1.Text = GVHeader.SelectedDataKey("unit1")
        labelunit2.Text = GVHeader.SelectedDataKey("unit2")
        labelunit3.Text = GVHeader.SelectedDataKey("unit3")

        labelkonversi1_2.Text = GVHeader.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVHeader.SelectedDataKey("konversi2_3")
        stockflag.Text = GVHeader.SelectedDataKey("itemflag")
        FlagItem1.Text = GVHeader.SelectedDataKey("stockflag").ToString
        labelseqhead.Text = GVHeader.Rows(GVHeader.SelectedIndex).Cells(1).Text

        If gvItemSearch.Visible = True Then
            gvItemSearch.DataSource = Nothing
            gvItemSearch.DataBind()
            gvItemSearch.Visible = False
        End If

        GVHeader.Columns(10).Visible = False
       
    End Sub

    Protected Sub dtlloc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtlloc.SelectedIndexChanged
        itemdetail.Text = ""
        merkdtl.Text = ""
        itemdtloid.Text = ""
        itemdtlqty.Text = "0.00"
        unitdtl.Items.Clear()
        notedtl.Text = ""

        If gvItem2.Visible = True Then
            gvItem2.DataSource = Nothing
            gvItem2.DataBind()
            gvItem2.Visible = False
            Session("itemsearch2") = Nothing
        End If
    End Sub

    Protected Sub transtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvItemSearch.DataSource = Nothing
        gvItemSearch.DataBind()
        gvItemSearch.Visible = False
        gvItemSearch.SelectedIndex = -1
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("trntransformorder.aspx?awal=true")
        End If
    End Sub

    Protected Sub lkbCloseSN_Click(sender As Object, e As EventArgs) Handles lkbCloseSN.Click

        '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@

        If NewSN.Text = "New SN" Then
            If StatusIsiSN.Text = "Header" Then
                If Not Session("TblSN") Is Nothing Then
                    dtab = Session("itemdetailheader")
                    Dim dvheader As DataView = dtab.DefaultView
                    dvheader.RowFilter = "itemoid = " & trnitemoid.Text
                    If dvheader.Count = 0 Then
                        Dim objTableSN As DataTable = Session("TblSN")
                        Dim dvSN As DataView = objTableSN.DefaultView
                        Dim itemid As Integer = trnitemoid.Text
                        dvSN.RowFilter = "itemoid = " & itemid
                        If Not dvSN.Count = 0 Then
                            For kdsn As Integer = 0 To dvSN.Count - 1
                                objTableSN.Rows.RemoveAt(0)
                            Next
                        End If
                        objTableSN.AcceptChanges()
                    End If
                    'Dim country As String = GVItemDetail.Rows(e.RowIndex).Cells(5).Text
                End If
                trnloc.SelectedValue = "None"
                trnitem.Text = ""
                merk.Text = ""
                trnitemoid.Text = ""
                itemqty.Text = "0.00"
                itemunit.Items.Clear()
                trnnote.Text = ""
                i_u_head.Text = "new"
                labelref.Text = ""

                labelsatuan1.Text = ""
                labelsatuan2.Text = ""
                labelsatuan3.Text = ""
                labelunit1.Text = ""
                labelunit2.Text = ""
                labelunit3.Text = ""
                labelkonversi1_2.Text = "1"
                labelkonversi2_3.Text = "1"

                If gvItemSearch.Visible = True Then
                    gvItemSearch.Visible = False
                    gvItemSearch.DataSource = Nothing
                    gvItemSearch.DataBind()
                End If
                GVHeader.SelectedIndex = -1
                GVHeader.Columns(9).Visible = True
            Else
                If Not Session("TblSNFooter") Is Nothing Then

                    dtab = Session("itemdetail")
                    Dim dvfooter As DataView = dtab.DefaultView
                    dvfooter.RowFilter = "itemoid = " & itemdtloid.Text
                    If dvfooter.Count = 0 Then
                        Dim objTableSNFotter As DataTable = Session("TblSNFooter")
                        Dim dvSN As DataView = objTableSNFotter.DefaultView
                        Dim itemid As Integer = itemdtloid.Text
                        dvSN.RowFilter = "itemoid = " & itemid
                        If Not dvSN.Count = 0 Then
                            For kdsn As Integer = 0 To dvSN.Count - 1
                                objTableSNFotter.Rows.RemoveAt(0)
                            Next
                        End If
                        objTableSNFotter.AcceptChanges()
                    End If
                End If
                merkdtl.Text = ""
                itemdtlqty.Text = "0.00"
                unitdtl.Items.Clear()
                notedtl.Text = ""
                itemdtloid.Text = ""
                itemdetail.Text = ""
                'dtlloc.SelectedValue = "None"
                i_u2.Text = "new"
                maxQty.Text = "0.00"

                labelsatuan1dtl.Text = ""
                labelsatuan2dtl.Text = ""
                labelsatuan3dtl.Text = ""
                labelunit1dtl.Text = ""
                labelunit2dtl.Text = ""
                labelunit3dtl.Text = ""
                labelkonversi1_2dtl.Text = "1"
                labelkonversi2_3dtl.Text = "1"
                GVDtl.SelectedIndex = -1
                GVDtl.Columns(9).Visible = True
            End If
        End If

        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       
    End Sub

    Private Function setTabelSN() As DataTable
        Dim dtSN As New DataTable
        dtSN.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSN.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSN.Columns.Add("id_SN", Type.GetType("System.String"))
        Return dtSN
    End Function

    Private Function setTabelSNFooter() As DataTable
        Dim dtSNFooter As New DataTable
        dtSNFooter.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSNFooter.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSNFooter.Columns.Add("id_SN", Type.GetType("System.String"))
        Return dtSNFooter
    End Function

    Protected Sub EnterSNTI_Click(sender As Object, e As ImageClickEventArgs) Handles EnterSNTI.Click

        LabelPesanSn.Text = ""
        If StatusIsiSN.Text = "Header" Then
            If transtype.SelectedValue = "Release" Then
                sSql = "select COUNT(a.sn) FROM QL_mstitemDtl a  WHERE a.sn = '" & TextSN.Text.Trim & "'"
                Dim SN As Object = cKon.ambilscalar(sSql)
                If Not SN = 0 Then
                    LabelPesanSn.Text = "Serial Number Sudah Ada"
                    cProc.SetFocusToControl(Me.Page, TextSN)
                    ModalPopupSN.Show()
                    Exit Sub
                End If
            End If

            If Session("TblSN") Is Nothing Then
                Dim dtlTableSN As DataTable = setTabelSN()
                Session("TblSN") = dtlTableSN
            End If

            Dim objTableSN As DataTable
            Dim objTableJUmSN As DataTable
            objTableSN = Session("TblSN")
            Dim dv As DataView = objTableSN.DefaultView
            If dv.Count = 0 Then
                NewSN.Text = "New SN"
            End If
            'Cek apa sudah ada item yang sama dalam Tabel Detail
            objTableJUmSN = Session("TblSN")
            Dim jumDvSN As DataView = objTableJUmSN.DefaultView

            If NewSN.Text = "New SN" Then
                dv.RowFilter = "itemoid= '" & trnitemoid.Text & "' and id_SN = '" & TextSN.Text.Trim & "'"
            Else
                If Not Session("TblSN") Is Nothing Then
                    dv.RowFilter = "itemoid= '" & trnitemoid.Text & "' and id_SN = '" & TextSN.Text.Trim & "' AND SNseq <> '" & SNseq.Text & "'"
                Else
                    dv.RowFilter = "itemoid= '" & trnitemoid.Text & "' and id_SN = '" & TextSN.Text.Trim & "'"
                End If

            End If

            If dv.Count > 0 Then
                LabelPesanSn.Text = "Serial Number Sudah Entry"
                dv.RowFilter = ""
                ModalPopupSN.Show()
                TextSN.Text = ""
                cProc.SetFocusToControl(Me.Page, TextSN)
                Exit Sub
            End If

            jumDvSN.RowFilter = "itemoid= '" & trnitemoid.Text & "'"
            If jumDvSN.Count >= TextQty.Text Then
                LabelPesanSn.Text = "Jumlah Item Tidak Sesuai Dengan jumlah Serial Number"
                jumDvSN.RowFilter = ""
                ModalPopupSN.Show()
                TextSN.Text = ""
                cProc.SetFocusToControl(Me.Page, TextSN)
                Exit Sub
            End If

            dv.RowFilter = ""
            'insert/update to list data
            Dim objRow As DataRow
            If NewSN.Text = "New SN" Then
                objRow = objTableSN.NewRow()
                objRow("SNseq") = objTableSN.Rows.Count + 1
            Else
                Dim selrow As DataRow() = objTableSN.Select("SNseq = " & SNseq.Text)
                objRow = selrow(0)
                objRow.BeginEdit()
            End If

            objRow("itemoid") = trnitemoid.Text
            objRow("id_SN") = TextSN.Text

            If NewSN.Text = "New SN" Then
                objTableSN.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If

            Session("TblSN") = objTableSN
            GVSN.Visible = True
            GVSN.DataSource = Nothing
            GVSN.DataSource = objTableSN
            GVSN.DataBind()
            LabelPesanSn.Text = ""
            SNseq.Text = objTableSN.Rows.Count + 1
            GVSN.SelectedIndex = -1
            cProc.SetFocusToControl(Me.Page, TextSN)
            LabelPesanSn.Text = ""
            ModalPopupSN.Show()
        Else
            '@@@@@@@@@@@@@@@@@@@ Footer @@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            sSql = "select COUNT(a.sn) FROM QL_mstitemDtl a  WHERE a.last_trans_type not in( 'Jual') AND a.sn = '" & TextSN.Text.Trim & "' and a.itemoid = " & itemdtloid.Text & " AND branch_code = '" & ddlCabang.SelectedValue & "' AND status_item not in ('In Process' , 'In Approval', 'Approved') AND locoid = " & dtlloc.SelectedValue & " and status_in_out = 'In'"

            Dim SN As Object = cKon.ambilscalar(sSql)
            If Not SN = 0 Then

                If Session("TblSNFooter") Is Nothing Then
                    Dim dtlTableSNFooter As DataTable = setTabelSNFooter()
                    Session("TblSNFooter") = dtlTableSNFooter
                End If

                Dim objTableSNFotter As DataTable
                Dim objTableJUmSN As DataTable
                objTableSNFotter = Session("TblSNFooter")
                Dim dvFooter As DataView = objTableSNFotter.DefaultView
                If dvFooter.Count = 0 Then
                    NewSN.Text = "New SN"
                End If
                'Cek apa sudah ada item yang sama dalam Tabel Detail
                objTableJUmSN = Session("TblSNFooter")
                Dim jumDvSN As DataView = objTableJUmSN.DefaultView

                If NewSN.Text = "New SN" Then
                    dvFooter.RowFilter = "itemoid= '" & itemdtloid.Text & "' and id_SN = '" & TextSN.Text.Trim & "'"
                Else
                    If Not Session("TblSNFooter") Is Nothing Then
                        dvFooter.RowFilter = "itemoid= '" & itemdtloid.Text & "' and id_SN = '" & TextSN.Text.Trim & "' AND SNseq <> '" & SNseq.Text & "'"
                    Else
                        dvFooter.RowFilter = "itemoid= '" & itemdtloid.Text & "' and id_SN = '" & TextSN.Text.Trim & "'"
                    End If

                End If

                If dvFooter.Count > 0 Then
                    LabelPesanSn.Text = "Serial Number Sudah Entry"
                    dvFooter.RowFilter = ""
                    ModalPopupSN.Show()
                    TextSN.Text = ""
                    cProc.SetFocusToControl(Me.Page, TextSN)
                    Exit Sub
                End If

                jumDvSN.RowFilter = "itemoid= '" & itemdtloid.Text & "'"
                If jumDvSN.Count >= TextQty.Text Then
                    LabelPesanSn.Text = "Jumlah Item Tidak Sesuai Dengan jumlah Serial Number"
                    jumDvSN.RowFilter = ""
                    ModalPopupSN.Show()
                    TextSN.Text = ""
                    cProc.SetFocusToControl(Me.Page, TextSN)
                    Exit Sub
                End If

                dvFooter.RowFilter = ""
                'insert/update to list data
                Dim objRow As DataRow
                If NewSN.Text = "New SN" Then
                    objRow = objTableSNFotter.NewRow()
                    objRow("SNseq") = objTableSNFotter.Rows.Count + 1
                Else
                    Dim selrow As DataRow() = objTableSNFotter.Select("SNseq = " & SNseq.Text)
                    objRow = selrow(0)
                    objRow.BeginEdit()
                End If

                objRow("itemoid") = itemdtloid.Text
                objRow("id_SN") = TextSN.Text

                If NewSN.Text = "New SN" Then
                    objTableSNFotter.Rows.Add(objRow)
                Else
                    objRow.EndEdit()
                End If

                Session("TblSNFooter") = objTableSNFotter
                GVSN.Visible = True
                GVSN.DataSource = Nothing
                GVSN.DataSource = objTableSNFotter
                GVSN.DataBind()
                LabelPesanSn.Text = ""
                SNseq.Text = objTableSNFotter.Rows.Count + 1
                GVSN.SelectedIndex = -1
                cProc.SetFocusToControl(Me.Page, TextSN)
            Else
                LabelPesanSn.Text = "Serial Number Unknow"
                cProc.SetFocusToControl(Me.Page, TextSN)
            End If
        End If

        cProc.SetFocusToControl(Me.Page, TextSN)
        ModalPopupSN.Show()
        TextSN.Text = ""
    End Sub

    Protected Sub itemqty_TextChanged(sender As Object, e As EventArgs) Handles itemqty.TextChanged
        ToMaskEdit(ToDouble(itemqty.Text), 3)
    End Sub

    Protected Sub lkbPilihItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbPilihItem.Click
        If StatusIsiSN.Text = "Header" Then
            If i_u_head.Text = "new" Then
                If Session("itemdetailheader") Is Nothing Then
                    dtab = New DataTable
                    dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                    dtab.Columns.Add("merk", Type.GetType("System.String"))
                    dtab.Columns.Add("locationoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("location", Type.GetType("System.String"))
                    dtab.Columns.Add("qty", Type.GetType("System.Double"))
                    dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                    dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                    dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                    dtab.Columns.Add("unit", Type.GetType("System.String"))
                    dtab.Columns.Add("unit1", Type.GetType("System.String"))
                    dtab.Columns.Add("unit2", Type.GetType("System.String"))
                    dtab.Columns.Add("unit3", Type.GetType("System.String"))
                    dtab.Columns.Add("note", Type.GetType("System.String"))
                    dtab.Columns.Add("itemcode", Type.GetType("System.String"))
                    Session("itemdetailheader") = dtab
                    labelseqhead.Text = "1"
                Else
                    dtab = Session("itemdetailheader")
                    labelseqhead.Text = (dtab.Rows.Count + 1).ToString
                End If
            Else
                dtab = Session("itemdetailheader")
            End If

            If Not Session("TblSN") Is Nothing Then
                Dim objTableSN As DataTable
                objTableSN = Session("TblSN")
                Dim dvjum As DataView = objTableSN.DefaultView
                dvjum.RowFilter = "itemoid = " & trnitemoid.Text
                If dvjum.Count = TextQty.Text Then

                    Dim drow As DataRow
                    Dim drowedit() As DataRow
                    If i_u_head.Text = "new" Then
                        drow = dtab.NewRow

                        drow("seq") = Integer.Parse(labelseqhead.Text)
                        drow("itemoid") = Integer.Parse(trnitemoid.Text)
                        drow("itemdesc") = trnitem.Text.Trim
                        drow("merk") = merk.Text
                        drow("locationoid") = trnloc.SelectedValue
                        drow("location") = trnloc.SelectedItem.ToString
                        drow("qty") = Double.Parse(itemqty.Text)
                        drow("satuan") = itemunit.SelectedValue
                        drow("unit") = itemunit.SelectedItem.Text

                        drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                        drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                        drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                        drow("unit1") = labelunit1.Text
                        drow("unit2") = labelunit2.Text
                        drow("unit3") = labelunit3.Text

                        drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                        drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                        drow("note") = trnnote.Text.Trim
                        drow("itemcode") = ItemcodeHeHeader.Text
                        dtab.Rows.Add(drow)
                        dtab.AcceptChanges()
                        transtype.Enabled = False
                    Else
                        drowedit = dtab.Select("seq = " & Integer.Parse(labelseqhead.Text) & "", "")
                        drow = drowedit(0)
                        drowedit(0).BeginEdit()

                        drow("itemoid") = Integer.Parse(trnitemoid.Text)
                        drow("itemdesc") = trnitem.Text.Trim
                        drow("merk") = merk.Text
                        drow("locationoid") = trnloc.SelectedValue
                        drow("location") = trnloc.SelectedItem.ToString
                        drow("qty") = Double.Parse(itemqty.Text)
                        drow("satuan") = itemunit.SelectedValue
                        drow("unit") = itemunit.SelectedItem.Text

                        drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                        drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                        drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                        drow("unit1") = labelunit1.Text
                        drow("unit2") = labelunit2.Text
                        drow("unit3") = labelunit3.Text

                        drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                        drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                        drow("note") = trnnote.Text.Trim
                        drow("itemcode") = ItemcodeHeHeader.Text
                        drowedit(0).EndEdit()
                        dtab.Select(Nothing, Nothing)
                        dtab.AcceptChanges()
                        transtype.Enabled = False
                    End If

                    GVHeader.DataSource = dtab
                    GVHeader.DataBind()
                    Session("itemdetailheader") = dtab

                    labelseqhead.Text = (GVHeader.Rows.Count + 1).ToString
                    merk.Text = ""
                    itemqty.Text = "0.00"
                    itemunit.Items.Clear()
                    trnnote.Text = ""
                    trnitemoid.Text = ""
                    trnitem.Text = ""
                    'trnloc.SelectedValue = "None"
                    i_u_head.Text = "new"

                    labelsatuan1.Text = "" : labelsatuan2.Text = ""
                    labelsatuan3.Text = "" : labelunit1.Text = ""
                    labelunit2.Text = "" : labelunit3.Text = ""
                    labelkonversi1_2.Text = "1"
                    labelkonversi2_3.Text = "1"
                    GVHeader.SelectedIndex = -1
                    GVHeader.Columns(9).Visible = True

                    If gvItemSearch.Visible = True Then
                        gvItemSearch.DataSource = Nothing
                        gvItemSearch.DataBind()
                        gvItemSearch.Visible = False
                        gvItemSearch.SelectedIndex = -1
                        Session("itemsearch") = Nothing
                    End If
                Else
                    LabelPesanSn.Text = "Jumlah Item Tidak Sesuai Dengan jumlah Serial Number"
                    ModalPopupSN.Show()
                    TextSN.Text = ""
                    cProc.SetFocusToControl(Me.Page, TextSN)
                End If

            Else
                If i_u2.Text = "new" Then
                    LabelPesanSn.Text = "Mohon Masukan Kode Serial Number"
                    ModalPopupSN.Show()
                    TextSN.Text = ""
                    cProc.SetFocusToControl(Me.Page, TextSN)
                Else
                    GVHeader.SelectedIndex = -1
                    GVHeader.Columns(9).Visible = True
                End If
            End If
        Else
            ' @@@@@@@@@@@@@@@@ Footer @@@@@@@@@@@@@@@@@@@@@@@2
            If i_u2.Text = "new" Then
                If Session("itemdetail") Is Nothing Then
                    dtab = New DataTable
                    dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                    dtab.Columns.Add("merk", Type.GetType("System.String"))
                    dtab.Columns.Add("locationoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("location", Type.GetType("System.String"))
                    dtab.Columns.Add("qty", Type.GetType("System.Double"))
                    dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                    dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                    dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                    dtab.Columns.Add("unit", Type.GetType("System.String"))
                    dtab.Columns.Add("unit1", Type.GetType("System.String"))
                    dtab.Columns.Add("unit2", Type.GetType("System.String"))
                    dtab.Columns.Add("unit3", Type.GetType("System.String"))
                    dtab.Columns.Add("note", Type.GetType("System.String"))
                    dtab.Columns.Add("itemcode", Type.GetType("System.String"))
                    dtab.Columns.Add("itemflag", Type.GetType("System.String"))
                    Session("itemdetail") = dtab
                    labelseq.Text = "1"
                Else
                    dtab = Session("itemdetail")
                    labelseq.Text = (dtab.Rows.Count + 1).ToString
                End If
            Else
                dtab = Session("itemdetail")
            End If

            If Not Session("TblSNFooter") Is Nothing Then
                Dim objTableSN As DataTable
                objTableSN = Session("TblSNFooter")
                Dim dvjumFooter As DataView = objTableSN.DefaultView
                dvjumFooter.RowFilter = "itemoid = " & itemdtloid.Text
                If dvjumFooter.Count = TextQty.Text Then
                    Dim drow As DataRow
                    Dim drowedit() As DataRow
                    If i_u2.Text = "new" Then
                        drow = dtab.NewRow

                        drow("seq") = Integer.Parse(labelseq.Text)
                        drow("itemoid") = Integer.Parse(itemdtloid.Text)
                        drow("itemdesc") = itemdetail.Text.Trim
                        drow("merk") = merkdtl.Text
                        drow("locationoid") = dtlloc.SelectedValue
                        drow("location") = dtlloc.SelectedItem.ToString
                        drow("qty") = Double.Parse(itemdtlqty.Text)
                        drow("satuan") = unitdtl.SelectedValue
                        drow("unit") = unitdtl.SelectedItem.Text

                        drow("satuan1") = Integer.Parse(labelsatuan1dtl.Text)
                        drow("satuan2") = Integer.Parse(labelsatuan2dtl.Text)
                        drow("satuan3") = Integer.Parse(labelsatuan3dtl.Text)
                        drow("unit1") = labelunit1dtl.Text
                        drow("unit2") = labelunit2dtl.Text
                        drow("unit3") = labelunit3dtl.Text

                        drow("konversi1_2") = Double.Parse(labelkonversi1_2dtl.Text)
                        drow("konversi2_3") = Double.Parse(labelkonversi2_3dtl.Text)
                        drow("note") = notedtl.Text.Trim
                        drow("itemcode") = ItemcodeFooter.Text.Trim
                        dtab.Rows.Add(drow)
                        dtab.AcceptChanges()
                        transtype.Enabled = False
                    Else
                        drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
                        drow = drowedit(0)
                        drowedit(0).BeginEdit()

                        drow("itemoid") = Integer.Parse(itemdtloid.Text)
                        drow("itemdesc") = itemdetail.Text.Trim
                        drow("merk") = merkdtl.Text
                        drow("locationoid") = dtlloc.SelectedValue
                        drow("location") = dtlloc.SelectedItem.ToString
                        drow("qty") = Double.Parse(itemdtlqty.Text)
                        drow("satuan") = unitdtl.SelectedValue
                        drow("unit") = unitdtl.SelectedItem.Text

                        drow("satuan1") = Integer.Parse(labelsatuan1dtl.Text)
                        drow("satuan2") = Integer.Parse(labelsatuan2dtl.Text)
                        drow("satuan3") = Integer.Parse(labelsatuan3dtl.Text)
                        drow("unit1") = labelunit1dtl.Text
                        drow("unit2") = labelunit2dtl.Text
                        drow("unit3") = labelunit3dtl.Text

                        drow("konversi1_2") = Double.Parse(labelkonversi1_2dtl.Text)
                        drow("konversi2_3") = Double.Parse(labelkonversi2_3dtl.Text)
                        drow("note") = notedtl.Text.Trim
                        drow("itemcode") = ItemcodeFooter.Text.Trim
                        drowedit(0).EndEdit()
                        dtab.Select(Nothing, Nothing)
                        dtab.AcceptChanges()
                        transtype.Enabled = False
                    End If

                    GVDtl.DataSource = Nothing
                    GVDtl.DataSource = dtab
                    GVDtl.DataBind()
                    Session("itemdetail") = dtab

                    labelseq.Text = (GVDtl.Rows.Count + 1).ToString
                    merkdtl.Text = "" : itemdtlqty.Text = "0.00"
                    unitdtl.Items.Clear() : notedtl.Text = ""
                    itemdtloid.Text = "" : itemdetail.Text = ""
                    i_u2.Text = "new" : maxQty.Text = "0.00"

                    labelsatuan1dtl.Text = "" : labelsatuan2dtl.Text = ""
                    labelsatuan3dtl.Text = "" : labelunit1dtl.Text = ""
                    labelunit2dtl.Text = "" : labelunit3dtl.Text = ""
                    labelkonversi1_2dtl.Text = "1" : labelkonversi2_3dtl.Text = "1"

                    GVDtl.SelectedIndex = -1
                    GVDtl.Columns(10).Visible = True

                    If gvItem2.Visible = True Then
                        gvItem2.DataSource = Nothing
                        gvItem2.DataBind()
                        gvItem2.Visible = False
                        gvItem2.SelectedIndex = -1
                    End If
                Else
                    LabelPesanSn.Text = "Jumlah Item Tidak Sesuai Dengan jumlah Serial Number"
                    ModalPopupSN.Show()
                    TextSN.Text = ""
                    cProc.SetFocusToControl(Me.Page, TextSN)
                End If
            Else
                If i_u2.Text = "new" Then
                    LabelPesanSn.Text = "Mohon Masukan Kode Serial Number"
                    ModalPopupSN.Show() : TextSN.Text = ""
                    cProc.SetFocusToControl(Me.Page, TextSN)
                Else
                    GVDtl.SelectedIndex = -1
                    GVDtl.Columns(10).Visible = True
                End If
            End If
        End If

    End Sub

    Protected Sub GVSN_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVSN.RowDeleting
        Dim iIndex As Int16 = e.RowIndex

        If StatusIsiSN.Text = "Header" Then
            Dim objTableSN As DataTable = Session("TblSN")
            objTableSN.Rows.RemoveAt(iIndex)
            'resequence sjDtlsequence 
            For C1 As Int16 = 0 To objTableSN.Rows.Count - 1
                Dim dr As DataRow = objTableSN.Rows(C1)
                dr.BeginEdit()
                dr("SNseq") = C1 + 1
                dr.EndEdit()
            Next

            Session("TblSN") = objTableSN
            GVSN.Visible = True
            GVSN.DataSource = objTableSN
            GVSN.DataBind()

        Else
            Dim objTableSNFotter As DataTable = Session("TblSNFooter")
            objTableSNFotter.Rows.RemoveAt(iIndex)
            'resequence sjDtlsequence 
            For C1 As Int16 = 0 To objTableSNFotter.Rows.Count - 1
                Dim dr As DataRow = objTableSNFotter.Rows(C1)
                dr.BeginEdit()
                dr("SNseq") = C1 + 1
                dr.EndEdit()
            Next

            Session("TblSNFooter") = objTableSNFotter
            GVSN.Visible = True

            GVSN.DataSource = objTableSNFotter
            GVSN.DataBind()
        End If
        ModalPopupSN.Show()
        LabelPesanSn.Text = ""
        TextSN.Text = ""
        cProc.SetFocusToControl(Me.Page, TextSN)
    End Sub

    Protected Sub btnGenerateSN_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerateSN.Click
        Dim sNo As String
        Dim GNOfix As String = ""
        LabelPesanSn.Text = ""
        If StatusIsiSN.Text = "Header" Then
            If Session("TblSN") Is Nothing Then
                Dim dtlTableSN As DataTable = setTabelSN()
                Session("TblSN") = dtlTableSN
            End If
            Dim objTableSN As DataTable = Session("TblSN")
            Dim dvFooter As DataView = objTableSN.DefaultView
            dvFooter.RowFilter = "itemoid = '" & trnitemoid.Text & "'"

            If dvFooter.Count = 0 Then
                sNo = Format(GetServerTime, "yy") & Format(GetServerTime, "MM") & ItemcodeHeHeader.Text & ddlCabang.SelectedValue
                sSql = "SELECT ISNULL(MAX(CAST(right(SN,4) AS INTEGER))+" & JSN & ",1) AS IDNEW FROM QL_mstitemDtl WHERE cmpcode='" & CompnyCode & "' AND SN LIKE '" & sNo & "%'"
                SNGenerate = GenNumberString(sNo, "", cKon.ambilscalar(sSql), 4)
                sSql = "SELECT count(sn) from 	QL_mstitemDtl WHERE sn = '" & SNGenerate.Trim & "'"
                Dim Sndb As Integer = cKon.ambilscalar(sSql)
                If Sndb = 0 Then
                    TextSN.Text = SNGenerate
                    ModalPopupSN.Show()
                Else
                    JSN += 1
                    btnGenerateSN_Click(Nothing, Nothing)
                End If

            Else

                If dvFooter.Count < 9 Then
                    SNUrut = "000" & N
                ElseIf dvFooter.Count < 99 Then
                    SNUrut = "00" & N
                ElseIf dvFooter.Count < 999 Then
                    SNUrut = "0" & N
                ElseIf dvFooter.Count < 9999 Then
                    SNUrut = N
                End If
                sNo = Format(GetServerTime, "yy") & Format(GetServerTime, "MM") & ItemcodeHeHeader.Text & ddlCabang.SelectedValue & SNUrut

                sSql = "SELECT count(sn) from QL_mstitemDtl WHERE sn = '" & sNo & "'"
                Dim adaSn As Integer = cKon.ambilscalar(sSql)
                If adaSn = 0 Then
                    dvFooter.RowFilter = "id_sn = '" & sNo & "'"
                    If dvFooter.Count = 0 Then
                        TextSN.Text = sNo
                        ModalPopupSN.Show()
                    Else
                        N += 1
                        btnGenerateSN_Click(Nothing, Nothing)
                    End If
                Else
                    N += 1
                    btnGenerateSN_Click(Nothing, Nothing)
                End If
            End If
        End If

    End Sub

    Protected Sub ddlCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCabang.SelectedIndexChanged
        If Page.IsPostBack Then
            InitAllDDL()
        End If
    End Sub
#End Region

End Class
