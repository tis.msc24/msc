<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnnotajualProjeck.aspx.vb" Inherits="Transaction_trnnotajual" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<!--script type="text/javascript">
<//
function popup(){
window.open("<%=help.Text%>","","width=800,height=600,resizable=yes,scrollbars=yes,menubar=no,status=yes,directories=no");
}
//>
</script-->

<!--script type="text/javascript">
<//
function popup2(ID){
var str = "../reportform/PrintReport.aspx?type=SI&cmpcode=<%=getCompany()%>&oid=" + ID;
window.open(str,"","width=800,height=600,resizable=yes,scrollbars=yes,menubar=no,status=yes,directories=no,left=100,top=0");
}
//>
</script-->
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Sales Invoice Project"></asp:Label><asp:SqlDataSource ID="SDSDataView"
                        runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                        ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>"
                        SelectCommand="SELECT a.trnjualmstoid, a.trnjualno,a.trnjualdate, s.CUSTNAME AS Customer FROM QL_trnjualmst AS a INNER JOIN QL_MSTCUST AS s ON a.trncustoid = s.CUSTOID WHERE (a.trnjualno LIKE @trnjualno) AND (s.CUSTNAME LIKE @custname) AND (a.cmpcode = @cmpcode) AND (a.trnjualstatus LIKE @trnjualstatus) ORDER BY a.trnjualdate DESC,a.trnjualno">
                        <SelectParameters>
                            <asp:Parameter Name="trnjualno" />
                            <asp:Parameter Name="custname" />
                            <asp:Parameter Name="cmpcode" />
                            <asp:Parameter Name="trnjualstatus" />
                        </SelectParameters>
                    </asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
<ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel3" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w3" DefaultButton="btnSearch"><TABLE><TBODY><TR><TD class="Label" align=left>Filter : </TD><TD align=left colSpan=2><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w4"><asp:ListItem Value="trnjualno">No Invoice</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="a.orderno">No SO</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w5" MaxLength="30"></asp:TextBox></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w6" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w7" TargetControlID="FilterPeriod1" PopupButtonID="imbPeriod1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left>Periode : </TD><TD class="Label" align=left colSpan=3><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w8"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;<asp:Label id="Label169" runat="server" Text="to" __designer:wfdid="w10"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w11"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w12"></asp:ImageButton>&nbsp;<asp:Label id="Label179" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w17"></asp:Label> <asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w14" Visible="False" AutoPostBack="False" Checked="True"></asp:CheckBox></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w15" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w16"></asp:CheckBox></TD><TD align=left colSpan=3><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w17"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w18"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w19"></asp:ImageButton> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w20"></asp:ImageButton></TD><TD align=left><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w21" TargetControlID="FilterPeriod2" PopupButtonID="imbPeriod2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left><asp:Label id="Company" runat="server" CssClass="Important" __designer:wfdid="w22" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="InvoiceID" runat="server" CssClass="Important" __designer:wfdid="w23"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR></TBODY></TABLE><asp:GridView id="Tbldata" runat="server" Width="850px" Font-Size="X-Small" BackColor="White" __designer:wfdid="w24" DataKeyNames="trnjualmstoid" CellPadding="3" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderWidth="1px" BorderStyle="Solid" AllowPaging="True" EmptyDataText="No data in database." PageSize="11">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trnjualmstoid" DataNavigateUrlFormatString="~/Transaction/trnnotajualProjeck.aspx?oid={0}" DataTextField="trnjualno" HeaderText="No" SortExpression="trnjualmstoid">
<HeaderStyle Font-Size="X-Small" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnjualdate" HeaderText="Date" SortExpression="trnjualdate">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="SO No" SortExpression="identifierno">
<HeaderStyle Font-Size="X-Small" Width="130px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualno" HeaderText="SJ No" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnjualnote" HeaderText="Note" SortExpression="trnjualnote">
<HeaderStyle Font-Size="X-Small" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualstatus" HeaderText="Status" SortExpression="trnjualstatus">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="imbPrintInvoice" onclick="imbPrintInvoice_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" __designer:wfdid="w1" AlternateText="Print Invoice" ToolTip='<%# Eval("trnjualmstoid") %>' CommandArgument='<%# Eval("trnjualno") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#999999" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" Text="No data found !!" __designer:wfdid="w6" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel>&nbsp; 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="Tbldata"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <strong><span style="font-size: 9pt">::
                List Of Sales Invoice Project</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w1" ActiveViewIndex="0"><asp:View id="View1" runat="server" __designer:wfdid="w2"><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information" __designer:wfdid="w3"></asp:Label> <asp:Label id="Label5" runat="server" ForeColor="#585858" __designer:dtid="562949953421340" Text="|" __designer:wfdid="w4"></asp:Label> <asp:LinkButton id="lkbDetil0" onclick="LinkButton1_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w5">Invoice Detail</asp:LinkButton><asp:Label id="Label7" runat="server" ForeColor="#585858" Text="|" __designer:wfdid="w6" Visible="False"></asp:Label><asp:LinkButton id="lbkCost0" onclick="lbkCost0_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w7" Visible="False">Cost Detail</asp:LinkButton><asp:Label id="help" runat="server" CssClass="Important" __designer:wfdid="w8" Visible="True"></asp:Label><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 15px" align=left><asp:Label id="periodacctg" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:dtid="2251799813685370" __designer:wfdid="w9" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><asp:Label id="refoid" runat="server" Font-Size="X-Small" __designer:wfdid="w10" Visible="False"></asp:Label><asp:Label id="oid" runat="server" Font-Size="X-Small" __designer:dtid="2251799813685291" Text="oid" __designer:wfdid="w11" Visible="False"></asp:Label><asp:Label id="i_u" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:dtid="2251799813685290" Text="N E W" __designer:wfdid="w12" Visible="False"></asp:Label><asp:Label id="dbsino" runat="server" Font-Size="X-Small" __designer:wfdid="w13" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w14" TargetControlID="trnjualdate" Format="dd/MM/yyyy" PopupButtonID="ImageButton5"></ajaxToolkit:CalendarExtender></TD><TD style="HEIGHT: 15px" align=left><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w15" MaskType="Date" Mask="99/99/9999" TargetControlID="trnjualdate" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" CultureName="en-US"></ajaxToolkit:MaskedEditExtender></TD><TD style="HEIGHT: 15px" align=left><asp:Label id="trncustoid" runat="server" __designer:wfdid="w16" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w17" MaskType="Number" Mask="99,999,999.99" TargetControlID="trndiscamt" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice Project&nbsp;No <asp:Label id="Label13" runat="server" CssClass="Important" Text="*" __designer:wfdid="w18"></asp:Label></TD><TD align=left><asp:TextBox id="trnjualno" runat="server" Width="134px" CssClass="inpTextDisabled" __designer:wfdid="w19" MaxLength="20" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Invoice Date <asp:Label id="Label14" runat="server" CssClass="Important" Text="*" __designer:wfdid="w20"></asp:Label></TD><TD align=left><asp:TextBox id="trnjualdate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w21" AutoPostBack="True" OnTextChanged="trnjualdate_TextChanged"></asp:TextBox> <asp:ImageButton id="ImageButton5" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w22"></asp:ImageButton> <asp:Label id="Label6" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w23"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>Invoice Ref. No.</TD><TD align=left><asp:TextBox id="trnjualres1" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w24" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Sales Order Project&nbsp;<asp:Label id="Label15" runat="server" CssClass="Important" Text="*" __designer:wfdid="w25"></asp:Label></TD><TD align=left><asp:TextBox id="referenceno" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w26"></asp:TextBox> <asp:ImageButton id="btnsearchSO" onclick="btnsearchSO_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w27"></asp:ImageButton> <asp:ImageButton id="ImageButton2" onclick="ImageButton2_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w28"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left>Customer<asp:Label id="custoid" runat="server" __designer:wfdid="w29" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trnjualcust" runat="server" Width="175px" CssClass="inpTextDisabled" __designer:wfdid="w30" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btncust" onclick="btncust_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w31" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnclearcust" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w32" Visible="False"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left>Order Date</TD><TD align=left><asp:TextBox id="trnorderdate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w33"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" Font-Size="X-Small" __designer:wfdid="w3" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="orderoid,orderno,orderdate,custname,custoid,ordercurroid,ordercurrate,ordertaxpct,hdiscamtpct,digit,paytermoid,hdisctype,taxable" OnSelectedIndexChanged="gvListSO_SelectedIndexChanged">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="No">
<HeaderStyle Font-Size="X-Small" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderdate" HeaderText="Date">
<HeaderStyle Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle Font-Size="X-Small" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!" __designer:wfdid="w13"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Payment Type</TD><TD align=left><asp:DropDownList id="trnpaytype" runat="server" Width="105px" CssClass="inpTextDisabled" __designer:wfdid="w34" Enabled="False"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Currency</TD><TD align=left><asp:DropDownList id="currate" runat="server" Width="180px" CssClass="inpTextDisabled" __designer:wfdid="w35" OnSelectedIndexChanged="currate_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Rate</TD><TD align=left><asp:TextBox id="currencyrate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w36" AutoPostBack="True" OnTextChanged="currencyrate_TextChanged" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice Amount</TD><TD align=left><asp:TextBox id="trnamtjual" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w37" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Tax Type</TD><TD align=left><asp:DropDownList id="taxtype" runat="server" Width="105px" CssClass="inpTextDisabled" __designer:wfdid="w38" AutoPostBack="True" OnSelectedIndexChanged="taxtype_SelectedIndexChanged" Enabled="False"><asp:ListItem>NON TAX</asp:ListItem>
<asp:ListItem>TAX</asp:ListItem>
<asp:ListItem>INCLUDE TAX</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=2><asp:RadioButtonList id="digit" runat="server" __designer:wfdid="w39" Visible="False" Enabled="False" RepeatDirection="Horizontal"><asp:ListItem Selected="True" Value="2">2 Digit Report</asp:ListItem>
<asp:ListItem Value="4">4 Digit Report</asp:ListItem>
<asp:ListItem Value="6">6 Digit Report</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left><asp:DropDownList id="trndisctype" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w40" AutoPostBack="True" Visible="False" OnSelectedIndexChanged="trndisctype_SelectedIndexChanged"><asp:ListItem Value="AMT">Amount</asp:ListItem>
<asp:ListItem Value="PCT">Percentage</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left><asp:TextBox id="trndiscamt" runat="server" CssClass="inpText" __designer:wfdid="w41" AutoPostBack="True" Visible="False">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lbl1" runat="server" Font-Size="X-Small" Text="Discount (Amt)" __designer:wfdid="w42" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="amtdischdr" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w43" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblTaxPct" runat="server" Text="Tax (%)" __designer:wfdid="w44" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trntaxpct" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w45" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblTaxAmt" runat="server" Text="Tax Amount" __designer:wfdid="w46" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trnamttax" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w47" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:DropDownList id="trnjualcosttype" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w48" AutoPostBack="True" Visible="False" OnSelectedIndexChanged="trnjualcosttype_SelectedIndexChanged"><asp:ListItem>Include</asp:ListItem>
<asp:ListItem>Exclude</asp:ListItem>
</asp:DropDownList></TD><TD align=left><asp:TextBox id="trnjualamtcost" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w49" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left>Total Invoice Netto</TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="totalinvoice" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w50" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left>Total Invoice (Rp.)</TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="totalinvoicerp" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w51" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left>Note</TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="trnjualnote" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w52" MaxLength="150"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left><asp:Label id="lblacctg" runat="server" Font-Size="X-Small" Text="For Accounting :" __designer:wfdid="w53" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left></TD><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblnofakturpajak" runat="server" Font-Size="X-Small" Text="No Faktur Pajak" __designer:wfdid="w54" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="txtnofakturpajak" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w55" MaxLength="20" Visible="False"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:ImageButton id="postingfaktur" onclick="postingfaktur_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421349" __designer:wfdid="w56" Visible="False"></asp:ImageButton></TD><TD align=left></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left><asp:Label id="orderamt" runat="server" __designer:wfdid="w57" Visible="False">0</asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="disc" runat="server" __designer:wfdid="w58" Visible="False">0</asp:Label></TD><TD align=left><asp:Label id="tax" runat="server" __designer:wfdid="w59" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:TextBox id="amtdiscdtl" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w60" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD><TD align=left><asp:TextBox id="trnamtjualnetto" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w61" Visible="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="HEIGHT: 15px" align=left><asp:Label id="trnjualtype" runat="server" __designer:dtid="2251799813685335" __designer:wfdid="w62" Visible="False"></asp:Label> <asp:Label id="orderflag" runat="server" __designer:wfdid="w63" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><ajaxToolkit:MaskedEditExtender id="meetax" runat="server" __designer:wfdid="w64" MaskType="Number" Mask="99.99" TargetControlID="trntaxpct" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD><TD style="HEIGHT: 15px" id="TD1" align=left runat="server" Visible="false"><ajaxToolkit:MaskedEditExtender id="meedisc" runat="server" __designer:wfdid="w65" MaskType="Number" Mask="99,999,999.99" TargetControlID="trndiscamt" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" InputDirection="RightToLeft" AcceptNegative="Left" MessageValidatorTip="true" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender></TD><TD style="HEIGHT: 15px" id="TD2" align=left runat="server" Visible="false"><asp:Label id="posting" runat="server" __designer:wfdid="w66" Visible="False"></asp:Label> <asp:TextBox id="identifierno" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w67" ReadOnly="True"></asp:TextBox></TD><TD style="HEIGHT: 15px" align=left><ajaxToolkit:MaskedEditExtender id="meerate" runat="server" __designer:wfdid="w68" MaskType="Number" Mask="999,999.99" TargetControlID="currencyrate" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD><TD style="HEIGHT: 15px" align=left><asp:DropDownList id="trnjualref" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w69" AutoPostBack="True" Visible="False" OnSelectedIndexChanged="trnjualref_SelectedIndexChanged" Enabled="False"><asp:ListItem Value="ql_trnordermst">Sales Order</asp:ListItem>
<asp:ListItem Value="ql_trnforcmst">Forecast</asp:ListItem>
</asp:DropDownList></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server" __designer:wfdid="w70"><asp:LinkButton id="lkbInfo1" onclick="LinkButton2_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w71">Information</asp:LinkButton> <asp:Label id="Label3" runat="server" ForeColor="#585858" __designer:dtid="562949953421340" Text="|" __designer:wfdid="w72"></asp:Label> <asp:Label id="Label4" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Invoice Detail" __designer:wfdid="w73"></asp:Label><asp:Label id="Label9" runat="server" ForeColor="#585858" Text="|" __designer:wfdid="w74" Visible="False"></asp:Label><asp:LinkButton id="lkbCost1" onclick="lkbCost1_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w75" Visible="False">Cost Detail</asp:LinkButton><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="I_u2" runat="server" ForeColor="Red" Text="New Detail" __designer:wfdid="w76" Visible="False"></asp:Label> </TD><TD align=left><asp:Label id="sjoid" runat="server" __designer:wfdid="w77" Visible="False"></asp:Label> <asp:Label id="trnjualdtluom" runat="server" __designer:wfdid="w78" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="itemoid" runat="server" __designer:wfdid="w79" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="trnjualdtloid" runat="server" __designer:wfdid="w80" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="trnjualdtlamttax" runat="server" __designer:wfdid="w81" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="trnjualdtltaxtype" runat="server" __designer:wfdid="w82" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label25" runat="server" Text="Delivery Project No." __designer:wfdid="w83"></asp:Label></TD><TD align=left><asp:TextBox id="Sjno" runat="server" Width="145px" CssClass="inpTextDisabled" __designer:wfdid="w84" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btnSearhDSO" onclick="btnSearhDSO_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w85"></asp:ImageButton> <asp:ImageButton id="btnClearDSO" onclick="btnClearDSO_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w86"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label26" runat="server" Text="Item" __designer:wfdid="w87"></asp:Label></TD><TD align=left><asp:TextBox id="itemname" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w88" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btnSearchItem" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w89" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnEraseItem" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w90" Visible="False"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label28" runat="server" Text="Quantity" __designer:wfdid="w91"></asp:Label></TD><TD align=left><asp:TextBox id="trnjualdtlqty" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w92" ReadOnly="True">0</asp:TextBox> <asp:Label id="gendesc" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w93"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=2><asp:GridView id="gvSJ" runat="server" Width="98%" __designer:wfdid="w4" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnsjjualmstoid,salesdeliveryno" OnSelectedIndexChanged="gvSJ_SelectedIndexChanged" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="salesdeliveryno" HeaderText="Delivery No">
<HeaderStyle Font-Size="X-Small" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualdate" HeaderText="Delv. Date">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualnote" HeaderText="Reference">
<HeaderStyle Font-Size="X-Small" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lkbDetail" onclick="lkbDetail_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w3" ToolTip='<%# eval("trnsjjualmstoid") %>'>Detail</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Text="No data in database." __designer:wfdid="w12" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD style="FONT-SIZE: x-small" align=left colSpan=4><asp:GridView id="tblDetilDso" runat="server" Width="98%" Font-Size="X-Small" __designer:wfdid="w183" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" AutoGenerateColumns="False" CellPadding="4" OnRowDataBound="tblDetilDso_RowDataBound">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:BoundField DataField="itemlongdesc" HeaderText="Item">
<HeaderStyle Font-Size="X-Small" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemgroupcode" HeaderText="Item Group">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualdtlqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!" __designer:wfdid="w14"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label24" runat="server" Text="Total Quantity" __designer:wfdid="w94"></asp:Label></TD><TD align=left><asp:TextBox id="totalqty" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w95" ReadOnly="True">0</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label27" runat="server" Text="Price" __designer:wfdid="w96"></asp:Label>&nbsp;<asp:Label id="Label16" runat="server" CssClass="Important" Text="*" __designer:wfdid="w97"></asp:Label></TD><TD align=left><asp:TextBox id="trnjualdtlprice" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w98" AutoPostBack="True" ReadOnly="True" OnTextChanged="trnjualdtlprice_TextChanged">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label29" runat="server" Text="Total Amount" __designer:wfdid="w99"></asp:Label></TD><TD align=left><asp:TextBox id="amtjualnetto" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w100" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label19" runat="server" Text="Width" __designer:wfdid="w101" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="txtwidth" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w102" Visible="False" ReadOnly="True">0</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label20" runat="server" Text="Lenght" __designer:wfdid="w103" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="txtlenght" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w104" Visible="False" ReadOnly="True">0</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label21" runat="server" Text="Weight" __designer:wfdid="w105" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="Txtweight" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w106" Visible="False" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="Detail Note" __designer:wfdid="w107" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trnjualdtlnote" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w108" MaxLength="200" Visible="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD align=left></TD><TD align=left colSpan=2><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click1" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w109" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w110" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="trnjualdtlseq" runat="server" __designer:wfdid="w111" Visible="False"></asp:Label></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w112" MaskType="Number" Mask="999,999,999,999.99" TargetControlID="trnjualdtlprice" CultureName="en-US" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><asp:TextBox id="amtdisc" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w113" Visible="False"></asp:TextBox>&nbsp;</TD><TD align=left><asp:DropDownList id="trnjualdtldisctype" runat="server" Width="133px" CssClass="inpText" __designer:wfdid="w114" Visible="False"></asp:DropDownList></TD><TD id="TD4" align=left runat="server" Visible="false">Packing</TD><TD id="TD3" align=left runat="server" Visible="false"><asp:TextBox id="trnjualpackingqty" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w115" ReadOnly="True">0</asp:TextBox></TD></TR></TBODY></TABLE><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 175px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="TblDtl" runat="server" Width="100%" Font-Size="X-Small" __designer:wfdid="w116" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnjualdtlseq" OnSelectedIndexChanged="TblDtl_SelectedIndexChanged">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualdtlseq" HeaderText="No " SortExpression="trnjualdtlseq">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="salesdeliveryno" HeaderText="Delivery No">
<HeaderStyle Font-Size="X-Small" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Item">
<HeaderStyle Font-Size="X-Small" Width="175px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="175px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="salesdeliveryqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtjualdisc" HeaderText="DiscPromo">
<HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtjualnetto" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdtlnote" HeaderText="Note" Visible="False">
<HeaderStyle Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No invoice detail!!" __designer:wfdid="w16"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET></asp:View> <asp:View id="View3" runat="server" __designer:wfdid="w117"><asp:LinkButton id="lkbInfo2" onclick="lkbInfo2_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w118">Information</asp:LinkButton> <asp:Label id="Label10" runat="server" ForeColor="#585858" Text="|" __designer:wfdid="w119"></asp:Label> <asp:LinkButton id="lkbDetil2" onclick="lkbDetil2_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w120">Invoice Detail</asp:LinkButton> <asp:Label id="Label11" runat="server" ForeColor="#585858" Text="|" __designer:wfdid="w121"></asp:Label> <asp:Label id="Label12" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Cost Detail" __designer:wfdid="w122"></asp:Label><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 15px" align=left><asp:Label id="jualbiayaseq" runat="server" __designer:wfdid="w123" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><asp:Label id="jualbiayamstoid" runat="server" __designer:wfdid="w124" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><asp:Label id="i_u3" runat="server" CssClass="Important" Text="New Detail" __designer:wfdid="w125" Visible="False"></asp:Label></TD><TD style="WIDTH: 311px; HEIGHT: 15px" align=left><ajaxToolkit:MaskedEditExtender id="meebiayaamt" runat="server" __designer:wfdid="w126" MaskType="Number" Mask="999,999,999.99" TargetControlID="itembiayaamt" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD><TD style="HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Cost&nbsp;Account</TD><TD align=left><asp:DropDownList id="itembiayaoid" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w127"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Cost Amount <asp:Label id="Label17" runat="server" CssClass="Important" Text="*" __designer:wfdid="w128"></asp:Label></TD><TD style="WIDTH: 311px" align=left><asp:TextBox id="itembiayaamt" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w129">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Note</TD><TD align=left><asp:TextBox id="itembiayanote" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w130" MaxLength="200"></asp:TextBox></TD><TD align=left><asp:ImageButton id="imbAddBiaya" onclick="imbAddBiaya_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w131"></asp:ImageButton> <asp:ImageButton id="imbClearBiaya" onclick="imbClearBiaya_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w132"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD><TD style="WIDTH: 311px; HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD></TR></TBODY></TABLE><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 175px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvBiaya" runat="server" Width="100%" Font-Size="X-Small" __designer:wfdid="w133" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="jualbiayaseq" OnSelectedIndexChanged="gvBiaya_SelectedIndexChanged"><Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Width="50px" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="jualbiayaseq" HeaderText="No ">
<ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>

<HeaderStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Cost Account">
<ItemStyle Width="400px" Font-Size="X-Small"></ItemStyle>

<HeaderStyle Width="400px" Font-Size="X-Small"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itembiayaamt" HeaderText="Amount">
<ItemStyle Width="150px" HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>

<HeaderStyle Width="150px" HorizontalAlign="Right" Font-Size="X-Small"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itembiayanote" HeaderText="Note">
<ItemStyle Width="300px" Font-Size="X-Small"></ItemStyle>

<HeaderStyle Width="300px" Font-Size="X-Small"></HeaderStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle Width="25px" HorizontalAlign="Center"></HeaderStyle>

<ItemStyle Width="25px" HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<RowStyle BackColor="#F7F7DE"></RowStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Text="No cost detail!!" __designer:wfdid="w15" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET></asp:View></asp:MultiView> <TABLE __designer:dtid="562949953421339"><TBODY><TR __designer:dtid="562949953421340"><TD colSpan=4 __designer:dtid="562949953421341"><SPAN style="FONT-SIZE: x-small; COLOR: dimgray" __designer:dtid="562949953421342"><SPAN style="FONT-SIZE: x-small"><asp:Label id="lblUpdate" runat="server" Text="Last Update" __designer:wfdid="w134"></asp:Label>&nbsp;</SPAN><asp:Label id="updUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="562949953421343" Text="Upduser" __designer:wfdid="w135"></asp:Label> <asp:Label id="lblOn" runat="server" Text="By" __designer:wfdid="w136"></asp:Label> <asp:Label id="updTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="562949953421344" Text="Updtime" __designer:wfdid="w137"></asp:Label><BR /><BR /><asp:Label id="lblCollection" runat="server" Font-Size="X-Small" __designer:wfdid="w3"></asp:Label></SPAN></TD></TR><TR __designer:dtid="562949953421345"><TD style="HEIGHT: 25px" colSpan=4 __designer:dtid="562949953421346"><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421347" __designer:wfdid="w138"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421348" __designer:wfdid="w139"></asp:ImageButton> <asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421349" __designer:wfdid="w140" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421350" __designer:wfdid="w141"></asp:ImageButton> <asp:ImageButton id="btnSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421351" __designer:wfdid="w142" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbPrint" onclick="imbPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421352" __designer:wfdid="w143" Visible="False" OnClientClick="javascript:popup()"></asp:ImageButton> <asp:ImageButton id="Printout" onclick="printout_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421352" __designer:wfdid="w144" Visible="False"></asp:ImageButton> <asp:ImageButton id="Export" onclick="Export_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:dtid="1970324836974645" __designer:wfdid="w145" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w146" Visible="False" ConfirmText="Are You Sure Print This Data ?"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnExcel" onclick="btnExcel_Click" runat="server" ImageUrl="~/Images/printexport.gif" ImageAlign="AbsMiddle" __designer:wfdid="w147" Visible="False" ConfirmText="Are You Sure Print This Data ?"></asp:ImageButton> </TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0 __designer:dtid="562949953421353"><TBODY><TR __designer:dtid="562949953421354"><TD vAlign=top align=left __designer:dtid="562949953421355"><asp:UpdatePanel id="UpdatePanel11" runat="server" __designer:dtid="562949953421356" __designer:wfdid="w148"><ContentTemplate __designer:dtid="562949953421357">
<asp:Panel id="PanelSO" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w149" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblCaptSO" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List of Sales Order" __designer:wfdid="w150"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLFindSO" runat="server" CssClass="inpText" __designer:wfdid="w151"><asp:ListItem Value="so.orderno">No</asp:ListItem>
<asp:ListItem Value="c.custname">Customer</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindSO" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w152"></asp:TextBox> <asp:ImageButton id="imbFindSO" onclick="imbFindSO_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w153" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbAllSO" onclick="imbAllSO_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w154" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 15px; TEXT-ALIGN: left" align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div5"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"></DIV></FIELDSET></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="lkbCloseSO" onclick="lkbCloseSO_Click" runat="server" __designer:wfdid="w156">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeso" runat="server" __designer:wfdid="w157" TargetControlID="btnHideso" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptSO" PopupControlID="PanelSO" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideso" runat="server" __designer:wfdid="w158" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD><TD vAlign=top align=left __designer:dtid="562949953421358"><asp:UpdatePanel id="UpdatePanel2" runat="server" __designer:dtid="562949953421359" __designer:wfdid="w159"><ContentTemplate __designer:dtid="562949953421360">
<asp:Panel id="panelDso" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w160" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="lblDSO" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List of Sales Delivery Order" __designer:wfdid="w161"></asp:Label></TD></TR><TR><TD><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset10"><DIV id="Div10"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"></DIV></FIELDSET></TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="closeDSO" onclick="closeDSO_Click" runat="server" __designer:wfdid="w163">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderdso" runat="server" __designer:wfdid="w164" TargetControlID="btnHideDSO" PopupControlID="panelDso" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideDSO" runat="server" __designer:wfdid="w165" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD></TR><TR __designer:dtid="562949953421361"><TD vAlign=top align=left __designer:dtid="562949953421362"><asp:UpdatePanel id="UpdatePanel5" runat="server" __designer:dtid="562949953421363" __designer:wfdid="w166"><ContentTemplate __designer:dtid="562949953421364">
<asp:Panel id="PanelListForecast" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w167" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="DetilForecast" runat="server" Font-Size="Medium" Font-Bold="True" Text="Detil Forecast" __designer:wfdid="w168"></asp:Label></TD></TR><TR><TD><asp:GridView id="tblDetilForecast" runat="server" Width="99%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w169" OnSelectedIndexChanged="gvListSO_SelectedIndexChanged" DataKeyNames="itemoid,itemlongdesc,forcqty" CellPadding="4" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderWidth="1px" BorderStyle="Solid">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemlongdesc" HeaderText="ItemName"></asp:BoundField>
<asp:BoundField DataField="forcqty" HeaderText="Order Qty"></asp:BoundField>
<asp:BoundField DataField="forcacumqty" HeaderText="Order Sent"></asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!" __designer:wfdid="w15"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:Label id="btncloseDetilForc" runat="server" Text="[Close]" __designer:wfdid="w170"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderDetilForecast" runat="server" __designer:wfdid="w171" TargetControlID="btnhideDetilForecast" PopupControlID="PanelListForecast" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnhideDetilForecast" runat="server" __designer:wfdid="w172" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD><TD vAlign=top align=left __designer:dtid="562949953421365"><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:dtid="562949953421366" __designer:wfdid="w173"><ContentTemplate __designer:dtid="562949953421367">
<asp:Panel id="PanelForecast" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w174" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="labelForecast" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Forecast" __designer:wfdid="w175"></asp:Label></TD></TR><TR><TD><asp:GridView id="gvListForecast" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w176" OnSelectedIndexChanged="gvListForecast_SelectedIndexChanged" DataKeyNames="forcoid,forcno,forcdate,custname,custoid" CellPadding="4" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderWidth="1px" BorderStyle="Solid" UseAccessibleHeader="False">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="forcno" HeaderText="Forecast No"></asp:BoundField>
<asp:BoundField DataField="forcdate" HeaderText="Forecast Date"></asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer"></asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="btnCloseForecast" onclick="btnCloseForecast_Click" runat="server" __designer:wfdid="w177">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderForecast" runat="server" __designer:wfdid="w178" TargetControlID="btnHideForecast" PopupControlID="PanelForecast"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideForecast" runat="server" __designer:wfdid="w179" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD></TR><TR __designer:dtid="562949953421368"><TD vAlign=top align=left __designer:dtid="562949953421369"><asp:UpdatePanel id="UpdatePanel6" runat="server" __designer:dtid="562949953421370" __designer:wfdid="w180"><ContentTemplate __designer:dtid="562949953421371">
<asp:Panel id="PanelDetilDSO" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w181" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="DetilDSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="Sales Delivery Order Detail" __designer:wfdid="w182"></asp:Label></TD></TR><TR><TD><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset11"><DIV id="Div11"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"></DIV></FIELDSET></TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="btnCloseDSO" onclick="btnCloseDSO_Click" runat="server" __designer:wfdid="w184">[ Back ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderDetilDSO" runat="server" __designer:wfdid="w185" TargetControlID="btnHideDetilDso" Drag="True" PopupControlID="PanelDetilDSO" PopupDragHandleControlID="DetilDSO" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideDetilDso" runat="server" __designer:wfdid="w186" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD><TD vAlign=top align=left __designer:dtid="562949953421372"><asp:UpdatePanel id="UpdatePanel7" runat="server" __designer:wfdid="w187"><ContentTemplate>
<asp:Panel id="pnlPosting" runat="server" Width="400px" CssClass="modalBox" __designer:wfdid="w188" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="Invoice Posting" __designer:wfdid="w189"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblpaymentype" runat="server" Text="Payment Type" __designer:wfdid="w190"></asp:Label></TD><TD align=left><asp:DropDownList id="paymentype" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w191" AutoPostBack="True" OnSelectedIndexChanged="paymentype_SelectedIndexChanged"><asp:ListItem>CASH</asp:ListItem>
<asp:ListItem>NONCASH</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label22" runat="server" Text="Reference No." __designer:wfdid="w192" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="referensino" runat="server" CssClass="inpText" __designer:wfdid="w193" Visible="False"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblcashbankacctg" runat="server" Text="Cash Account" __designer:wfdid="w194"></asp:Label></TD><TD align=left><asp:DropDownList id="cashbankacctg" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w195"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblapaccount" runat="server" Text="A/R Account" __designer:wfdid="w196" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="apaccount" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w197" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblpurchasingaccount" runat="server" Text="Sales Account" __designer:wfdid="w198"></asp:Label></TD><TD align=left><asp:DropDownList id="purchasingaccount" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w199"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblCostCashBank" runat="server" Text="Cost Cash/Bank Account" __designer:wfdid="w200" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="CashForCost" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w201" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left></TD><TD style="HEIGHT: 15px" align=left></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:LinkButton id="lkbPosting" onclick="lkbPosting_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w202">[ POST ]</asp:LinkButton> <asp:LinkButton id="lkbCancel" onclick="lkbCancel_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w203">[ CANCEL ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting" runat="server" __designer:wfdid="w204" TargetControlID="btnHidePosting" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting" PopupControlID="pnlPosting"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting" runat="server" __designer:wfdid="w205" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR><TR><TD vAlign=top align=left><asp:UpdatePanel id="UpdatePanelSuplier" runat="server" __designer:dtid="1125899906842656" __designer:wfdid="w206"><ContentTemplate __designer:dtid="1125899906842657">
<asp:Panel id="Panelcust" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w207" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="LaberSuplier" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer" __designer:wfdid="w208"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label">Filter : <asp:DropDownList id="DDLcustid" runat="server" CssClass="inpText" __designer:wfdid="w209"><asp:ListItem Value="custname">Name</asp:ListItem>
<asp:ListItem Value="custcode">Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindCustID" runat="server" CssClass="inpText" __designer:wfdid="w210"></asp:TextBox> <asp:ImageButton id="imbFindcust" onclick="imbFindcust_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w211"></asp:ImageButton> <asp:ImageButton id="imbAllcust" onclick="imbAllcust_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w212"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 315px"><FIELDSET style="WIDTH: 97%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 290px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div9"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvCustomer" runat="server" Width="98%" __designer:wfdid="w213" AllowPaging="True" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="custoid,custname" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" OnPageIndexChanging="gvCustomer_PageIndexChanging">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" Font-Size="X-Small" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No supplier data found!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET>&nbsp; </TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="Close" onclick="Close_Click" runat="server" __designer:wfdid="w214">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><asp:Button id="Buttoncust" runat="server" __designer:wfdid="w215" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w216" TargetControlID="Buttoncust" Drag="True" PopupControlID="Panelcust" PopupDragHandleControlID="Panelcust" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD><TD vAlign=top align=left><asp:UpdatePanel id="upPreview" runat="server" __designer:dtid="562949953421348" __designer:wfdid="w217"><ContentTemplate __designer:dtid="562949953421349">
<asp:Panel id="pnlPreview" runat="server" Width="750px" CssClass="modalBox" __designer:wfdid="w218" Visible="False"><TABLE width="100%" align=center><TBODY><TR><TD align=center><asp:Label id="lblCaptPreview" runat="server" Font-Size="Medium" Font-Bold="True" Text="Preview Jurnal" __designer:wfdid="w219"></asp:Label></TD></TR><TR><TD><asp:Panel id="pnlScrollPreview" runat="server" Width="100%" Height="200px" __designer:wfdid="w220" ScrollBars="Vertical"><asp:GridView id="gvPreview" runat="server" Width="97%" __designer:wfdid="w221" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False" CellPadding="4" EmptyDataRowStyle-ForeColor="Red" AllowSorting="True">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="code" HeaderText="Code">
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="desc" HeaderText="Description">
<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="credit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#999999" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No Data Found !!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#000084" BorderColor="White" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> </TD></TR><TR><TD align=center><asp:LinkButton id="lkbPostPreview" onclick="lkbPostPreview_Click" runat="server" Font-Bold="True" __designer:wfdid="w222">[ POST ]</asp:LinkButton> <asp:LinkButton id="lkbClosePreview" onclick="lkbClosePreview_Click" runat="server" Font-Bold="True" __designer:wfdid="w223">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><asp:Button id="beHidePreview" runat="server" __designer:wfdid="w224" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpePreview" runat="server" __designer:wfdid="w225" TargetControlID="beHidePreview" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptPreview" PopupControlID="pnlPreview" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="Export"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <strong><span style="font-size: 9pt">::
                Form Sales Invoice Project</span></strong>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
                            <asp:UpdatePanel id="upMsgbox" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" DefaultButton="btnMsgBoxOK" __designer:wfdid="w25" CssClass="modalMsgBox" Visible="False"><TABLE cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w26"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w27"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Red" Font-Size="X-Small" __designer:wfdid="w28"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w29"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w30"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" __designer:wfdid="w31" DropShadow="True" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" TargetControlID="beMsgBox">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" __designer:wfdid="w32" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
                            </asp:UpdatePanel>
    
</asp:Content>

