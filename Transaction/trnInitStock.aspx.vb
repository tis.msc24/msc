﻿Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data
Imports ClassFunction
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_trnInitStock
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim kumpulanSN As String
    Dim has_sn As String
    '@@@@@@@@@@@ Konek Exel @@@@@@@@@
    Dim KodeITemExcel As String
    Dim JumItemExcel As Integer
    Dim adap As OleDbDataAdapter
    Dim SNExcel As String
#End Region

#Region "Functions"
    Private Function IsFilterValid()
        Dim sError As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If DDLLocation.SelectedValue = "" Then
            sError &= "- Please select INIT LOCATION field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, CompnyName & " - Warning", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            strCaption &= " - INFORMATION"
            lblCaption.ForeColor = Drawing.Color.White
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"

        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Sub BindData(ByVal sqlPlus As String)
        Dim sWhere As String = ""
        If chkPeriod.Checked Then
            sWhere &= " AND ik.initdate BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "yyyy/MM/dd") & "' AND '" & _
                Format(CDate(toDate(dateAkhir.Text)), "yyyy/MM/dd") & "' "
        Else
            sWhere &= " AND ik.initdate BETWEEN '" & Format(Now, "yyyy/MM/01") & _
                "' AND '" & Format(Now, "yyyy/MM/dd") & "' "
        End If

        If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
            sWhere &= " and ik.initbrach = " & dd_branch.SelectedValue
        End If

        If DDLocation.SelectedValue <> "ALL LOCATION" Then
            sWhere &= " and ik.initlocation = " & DDLocation.SelectedValue
        End If

        If itemoid.Text <> "" Then
            sWhere &= " and i.itemoid = " & itemoid.Text
        End If


        sSql = "SELECT row_number() OVER(ORDER BY ik.initoid ) AS 'No', ik.initoid,  g1.gendesc branch,g3.gendesc +' - '+ g2.gendesc location, i.itemcode, i.itemdesc, CASE i.has_sn WHEN 1 THEN 'Yes' ELSE 'No' END SN, CAST(ik.initqty as INTEGER) initqty, g4.gendesc Unit, CAST( ik.initvalueidr AS DECIMAL (18,2)) HargaEcer, CAST((ik.initqty * ik.initvalueidr ) AS DECIMAL(18,2)) TolalHarga FROM QL_initstock ik INNER JOIN QL_mstItem i ON i.itemoid = ik.initrefoid INNER JOIN QL_mstgen g1 ON g1.gencode = ik.initbrach AND g1.gengroup='CABANG' INNER JOIN QL_mstgen g2 ON g2.genoid = ik.initlocation INNER JOIN QL_mstgen g3 ON g3.genoid = g2.genother1 INNER JOIN QL_mstgen g4 ON g4.genoid = i.satuan3 WHERE ik.cmpcode = 'MSC' " & sWhere & ""

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        Session("tbldata") = objDs.Tables("data")
        gvTblData.DataSource = objDs.Tables("data")
        gvTblData.DataBind()
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        If Session("branch_id") = "10" Then
            sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang'"
            FillDDL(DDLBusUnit, sSql)
            FillDDL(dd_branch, sSql)
            dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            dd_branch.SelectedValue = "SEMUA BRANCH"
        Else
            sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang' and gencode = '" & Session("branch_id") & "'"
            FillDDL(DDLBusUnit, sSql)
            FillDDL(dd_branch, sSql)
            dd_branch.Enabled = False
        End If

        btnCancel.Visible = True
        btnPost.Visible = True
        If DDLBusUnit.Items.Count = 0 Then
            showMessage("Please create/fill Data General in group Cabang!", CompnyName & " - Warning", 2)
            btnPost.Visible = False
            btnCancel.Visible = False
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLLocation()
        End If
        InitDDLLocationForm()
    End Sub

    Private Sub InitDDLLocation()
        'Fill DDL Init Location

        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & DDLBusUnit.SelectedValue & "' and gengroup = 'cabang')"
        FillDDL(DDLLocation, sSql)
    End Sub

    Private Sub InitDDLLocationForm()
        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & dd_branch.SelectedValue & "' and gengroup = 'cabang')"
        FillDDL(DDLocation, sSql)
        DDLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
        DDLocation.SelectedValue = "ALL LOCATION"
    End Sub

    Private Sub BindMaterial()
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim sType As String = "", sRef As String = ""
        
        sSql = "Select i.itemoid AS matoid, '' AS matno,i.itemcode matcode,i.itemdesc +' - '+ i.merk matlongdesc ,i.itemgroupoid, i.itemsubgroupoid, i.itemtype,'' AS matqty, '' AS Harga, gen2.gendesc matunit, gen2.genoid unitoid , 'False' AS checkvalue,'True' AS enableqty from QL_mstitem i INNER JOIN QL_mstgen gen ON gen.genoid= i.itemgroupoid  INNER JOIN QL_mstgen gen2 on gen2.genoid = i.satuan3 where i.has_SN = 0 and i.itemoid NOT IN (SELECT initrefoid FROM QL_initstock init WHERE init.cmpcode='" & CompnyCode & "' AND init.initbrach='" & DDLBusUnit.SelectedValue & "' AND initlocation ='" & DDLLocation.SelectedValue & "') And i.stockflag <> 'ASSET'"
        FilterTextListMat.TextMode = TextBoxMode.SingleLine
        FilterTextListMat.Rows = 0
        FilterDDLListMat.Items(0).Enabled = False
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")

        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", CompnyName & " - INFORMATION", 3)
        End If
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category 1
        sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('itemgroup') and cmpcode like '%" & CompnyCode & "%' order by gendesc"
        If FillDDL(DDLCat01, sSql) Then
            InitDDLCat2()
        Else
            DDLCat02.Items.Clear()
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        sSql = "select distinct g.genoid, g.gencode, g.gendesc from ql_mstgen g inner join ql_mstitem i2 on i2.itemsubgroupoid=g.genoid where g.gengroup='itemsubgroup' and i2.itemgroupoid= '" & DDLCat01.SelectedValue & "'"
        'sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('ITEMSUBGROUP') and cmpcode like '%" & CompnyCode & "%' and genother1 = '" & DDLCat01.SelectedValue & "'"
        If FillDDL(DDLCat02, sSql) Then
            InitDDLCat3()
        Else
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('ITEMTYPE') and cmpcode like '%" & CompnyCode & "%' and genother2 = '" & DDLCat02.SelectedValue & "'"

        If FillDDL(DDLCat03, sSql) Then
            InitDDLCat4()
        Else
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat4()
        'Fill DDL Category 3
        sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('ITEMMERK') and cmpcode like '%" & CompnyCode & "%' and genother3 = '" & DDLCat03.SelectedValue & "'"
        FillDDL(DDLCat04, sSql)
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(3).Controls))
                            dv(0)("Harga") = ToDouble(GetTextValue(row.Cells(4).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(3).Controls))
                            dv(0)("Harga") = ToDouble(GetTextValue(row.Cells(4).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If

    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TableDetail")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matwh", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("SN", Type.GetType("System.String"))
        dtlTable.Columns.Add("matqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("Harga", Type.GetType("System.Double"))
        dtlTable.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("enableqty", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal
        DDLBusUnit.CssClass = sCss
        DDLLocation.Enabled = bVal
        DDLLocation.CssClass = sCss
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Session("branch_id") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"


            Response.Redirect("~\Transaction\trnInitStock.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang

        End If
        Page.Title = CompnyName & " - Init Stock"
        Session("oid") = Request.QueryString("oid")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data?');")
        upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
        Page.Title = CompnyName & " - Material Init Stock"
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True

        If Not Page.IsPostBack Then
            btnPost.Visible = True
            btnCancel.Visible = True
            btnLookUpMat.Visible = True
            InitAllDDL()
            BindData("")
            TabContainer1.ActiveTabIndex = 0
            dateAwal.Text = Format(Now, "01/MM/yyyy")
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        Else
            btnPost.Visible = True
            btnCancel.Visible = True
            btnLookUpMat.Visible = True

        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLLocation()
        TabContainer1.ActiveTabIndex = 1
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True
    End Sub

    Private Sub CreateTblDetailSN()
        Dim dtlTableSN As DataTable = New DataTable("TableDetailSN")
        dtlTableSN.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("matno", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matqty", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("Harga", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matunit", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("checkvalue", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("enableqty", Type.GetType("System.String"))
        Session("TblDtlSN") = dtlTableSN
    End Sub

    Private Sub BindMaterialSN()

        has_sn = "Yes"
        Session("Y") = has_sn
        Dim SaveLocation As String
        SaveLocation = ""
        SaveLocation = FileExelInitStokSN.Text
        Dim ConnStrExe As String = "provider=Microsoft.jet.OLEDB.4.0; Data Source=" & SaveLocation & "; Extended Properties=Excel 8.0;"

        Dim connexc As New OleDbConnection(ConnStrExe)
        Dim commecel As OleDbCommand
        Dim TbSNAda As New DataTable
        Dim TbSNAda1 As New DataTable
        Dim TBCekSN As New DataTable
        Dim jumlahSnSama As Integer = 0
        Dim TampungSnSama As String
        Dim SNDami As Integer = 0
        connexc.Open()
        Try
            sSql = "select itemcode, sn from [SN_Upload$]"
            adap = New OleDbDataAdapter(sSql, connexc)
            adap.Fill(TbSNAda)
        Catch ex As Exception
            showMessage("Data Serial Init Stock Tidak Sesuai ", CompnyName & " - Warning", 2)
            connexc.Close()
            connexc.Dispose()
            Exit Sub
        End Try

        '@@@@@@@@@@@@@ Cek Item Di Gudang Init @@@@@@@@@@@@@@@
        sSql = "select DISTINCT(itemcode) from [SN_Upload$] "
        adap = New OleDbDataAdapter(sSql, connexc)
        adap.Fill(TbSNAda1)
        For itemDataSn As Integer = 0 To TbSNAda1.Rows.Count - 1
            sSql = "SELECT count(itemoid) FROM QL_mstItem where itemcode = '" & TbSNAda1.Rows(itemDataSn).Item("itemcode") & "' and has_sn = 1"
            Dim ItemID As Integer = cKon.ambilscalar(sSql)
            If ItemID = 0 Then
                showMessage("ITEM Di Upload Ada yang Tidak Menggunakan SN !, <strong>check Data Excel</strong> ", CompnyName & " - INFORMATION", 3)
                connexc.Close()
                connexc.Dispose()
                Exit Sub
            End If

            sSql = "SELECT itemoid FROM QL_mstItem where itemcode = '" & TbSNAda1.Rows(itemDataSn).Item("itemcode") & "'"
            Dim ItemIDInit As Integer = cKon.ambilscalar(sSql)

            sSql = "SELECT count(*) FROM QL_initstock where initrefoid = '" & ItemIDInit & "' and initbrach = '" & DDLBusUnit.SelectedValue & "' and initlocation = '" & DDLLocation.SelectedValue & "'"
            Dim ItemData As Integer = cKon.ambilscalar(sSql)
            If Not ItemData = 0 Then
                showMessage("Init Stock Sudah Ada Di Gudang or </br> Material data can't be found!, <strong>check Data Excel</strong> ", CompnyName & " - INFORMATION", 3)
                connexc.Close()
                connexc.Dispose()
                Exit Sub
            End If
        Next

        '@@@@@@@@@@@@ Cek SN Per Item @@@@@@@@@@@@@@@@@@@@@@@
        sSql = "select Sn from [SN_Upload$] "
        adap = New OleDbDataAdapter(sSql, connexc)
        adap.Fill(TBCekSN)
        For SNup As Integer = 0 To TBCekSN.Rows.Count - 1
            sSql = "SELECT count(SN) FROM QL_mstitemDtl WHERE sn = '" & TBCekSN.Rows(SNup).Item("Sn") & "'"
            Dim JumSn As Integer = cKon.ambilscalar(sSql)
            If JumSn = 1 Then
                sSql = "SELECT SN FROM QL_mstitemDtl WHERE sn = '" & TBCekSN.Rows(SNup).Item("Sn") & "'"
                TampungSnSama = cKon.ambilscalar(sSql)
                kumpulanSN = kumpulanSN & " " & TampungSnSama
                jumlahSnSama = jumlahSnSama + 1
                If jumlahSnSama < TBCekSN.Rows.Count - 1 Then
                    kumpulanSN &= ","
                    If (jumlahSnSama / 2) = 5 Then
                        kumpulanSN &= "<br/>"
                    End If
                End If
            End If
        Next

        If Not jumlahSnSama = 0 Then
            showMessage("<strong>Ada SN Sama Yang Di Upload </strong> <br/> <br> " & kumpulanSN & " ", CompnyName & " - INFORMATION", 3)
            connexc.Close()
            connexc.Dispose()
            Exit Sub
        End If

        For TamSN As Integer = 0 To TbSNAda1.Rows.Count - 1
            sSql = "SELECT itemoid FROM QL_mstItem where itemcode = '" & TbSNAda1.Rows(TamSN).Item("itemcode") & "'"
            Dim ItemID As Integer = cKon.ambilscalar(sSql)
            SNExcel &= "'" & ItemID & "'"
            If TamSN < TbSNAda1.Rows.Count - 1 Then
                SNExcel &= ","
            End If
        Next

        UpdateCheckedMat()
        Session("TblListMatView") = Nothing
        If Session("TblListMatView") Is Nothing Then
            sSql = "select i.itemoid  AS matoid, '' AS matno,i.itemcode matcode,  gen.gendesc +' '+ i.itemdesc +' '+  i.merk matlongdesc ,'' AS matqty, '' AS Harga, gen2.gendesc matunit, gen2.genoid unitoid , 'True' AS checkvalue,'True' AS enableqty   from  QL_mstitem i   INNER JOIN QL_mstgen gen ON gen.genoid= i.itemgroupoid  INNER JOIN QL_mstgen gen2 on gen2.genoid = i.satuan3   where i.has_SN = 1 and i.itemoid NOT IN (SELECT initrefoid FROM QL_initstock init WHERE init.cmpcode='" & CompnyCode & "' AND init.initbrach='" & DDLBusUnit.SelectedValue & "' AND initlocation ='" & DDLLocation.SelectedValue & "') AND i.itemoid in (" & SNExcel & ")"
            Dim dtSN As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
            If dtSN.Rows.Count > 0 Then
                Session("TblListMatView") = dtSN
            End If
        End If

        If Not Session("TblListMatView") Is Nothing Then
            Dim TabelItem As DataTable = Session("TblListMatView")
            Dim dv As DataView = TabelItem.DefaultView

            If Session("TblDtlSN") Is Nothing Then
                CreateTblDetailSN()
                Dim objTblSN As DataTable = Session("TblDtlSN")
                Dim objView As DataView = objTblSN.DefaultView
                For iSN As Integer = 0 To TabelItem.Rows.Count - 1
                    Dim rv As DataRowView = objView.AddNew()
                    rv.BeginEdit()

                    sSql = "select count(sn) from [SN_Upload$] where itemcode = '" & TabelItem.Rows(iSN).Item("matcode") & "'"
                    commecel = New OleDbCommand(sSql, connexc)
                    JumItemExcel = commecel.ExecuteScalar()

                    rv("matoid") = dv(iSN)("matoid").ToString
                    rv("matcode") = dv(iSN)("matcode").ToString
                    rv("matqty") = JumItemExcel
                    rv("Harga") = ""
                    rv("matunit") = dv(iSN)("matunit").ToString
                    rv("unitoid") = dv(iSN)("unitoid").ToString
                    rv("checkvalue") = dv(iSN)("enableqty").ToString
                    rv("enableqty") = dv(iSN)("enableqty").ToString
                    rv("matlongdesc") = dv(iSN)("matlongdesc").ToString
                    rv.EndEdit()
                Next
                objTblSN.AcceptChanges()
                Session("TblListMatView") = objTblSN
                Session("TblListMat") = objTblSN

                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            Else

                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)

            End If
            sSql = "select * from [SN_Upload$]"
            adap = New OleDbDataAdapter(sSql, connexc)
            Dim dtecelSN As New DataTable
            adap.Fill(dtecelSN)
            Session("TabelSN") = dtecelSN
        Else
            showMessage("Init Stock Sudah Ada Di Gudang or </br> Material data can't be found! ", CompnyName & " - INFORMATION", 3)
            connexc.Close()
            connexc.Dispose()
            Exit Sub
        End If

        connexc.Close()
        connexc.Dispose()

    End Sub

    Protected Sub btnLookUpMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLookUpMat.Click

        If IsFilterValid() Then
            If CheckBox1.Checked = True Then
                If FileExelInitStokSN.Text <> "" Then
                    BindMaterialSN()
                Else
                    showMessage("Serial Init Stock Belum Teruplaod ", CompnyName & " - Warning", 2)
                    Exit Sub
                End If
            Else
                FilterDDLListMat.SelectedIndex = -1
                FilterTextListMat.Text = ""
                InitDDLCat1()
                InitDDLCat2()
                'InitDDLCat3()
                'InitDDLCat4()
                cbCat01.Checked = False
                cbCat02.Checked = False
                cbCat03.Checked = False
                cbCat04.Checked = False
                has_sn = "No"
                Session("Y") = has_sn
                Session("TblListMat") = Nothing
                Session("TblListMatView") = Nothing
                gvListMat.DataSource = Session("TblListMat")
                gvListMat.DataBind()
                BindMaterial()
            End If
            btnPost.Visible = True
            btnCancel.Visible = True
            btnLookUpMat.Visible = True
        End If
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(sender As Object, e As EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLCat01.SelectedIndexChanged
        InitDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLCat02.SelectedIndexChanged
        InitDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat03_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLCat03.SelectedIndexChanged
        InitDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Sub tampildata()
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub lbAddToListMat_Click(sender As Object, e As EventArgs) Handles lbAddToListMat.Click

        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    Dim sErr As String = ""
                    For C1 As Integer = 0 To dv.Count - 1
                        If ToDouble(dv(C1)("matqty").ToString) <= 0 Then
                            sErr = "Every Qty of selected data must be more than 0!"
                            Exit For
                        End If
                        If ToDouble(dv(C1)("harga").ToString) <= 0 Then
                            sErr = "Every Init Value of selected data must be more than 0!"
                            Exit For
                        End If
                    Next
                    If sErr <> "" Then
                        Session("WarningListMat") = sErr
                        showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                        Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTbl As DataTable = Session("TblDtl")
                    Dim iSeq As Integer = objTbl.Rows.Count + 1
                    Dim objView As DataView = objTbl.DefaultView
                    For C1 As Integer = 0 To dv.Count - 1
                        objView.RowFilter = " matoid=" & dv(C1)("matoid")
                        If objView.Count > 0 Then
                            objView(0)("matqty") = ToDouble(dv(C1)("matqty").ToString)
                        Else
                            Dim StatusSn As String

                            Dim rv As DataRowView = objView.AddNew()
                            rv.BeginEdit()
                            rv("seq") = iSeq
                            rv("matwhoid") = DDLLocation.SelectedValue
                            rv("matwh") = DDLLocation.SelectedItem.Text
                            rv("matoid") = dv(C1)("matoid").ToString
                            rv("matcode") = dv(C1)("matcode").ToString
                            rv("matlongdesc") = dv(C1)("matlongdesc").ToString
                            If Session("Y") = "Yes" Then
                                StatusSn = "Yes"
                            Else
                                StatusSn = "No"
                            End If
                            rv("SN") = StatusSn
                            rv("matqty") = ToDouble(dv(C1)("matqty").ToString)
                            rv("Harga") = ToDouble(dv(C1)("Harga").ToString)
                            rv("matunit") = dv(C1)("matunit").ToString
                            rv("unitoid") = dv(C1)("unitoid").ToString
                            rv("enableqty") = dv(C1)("enableqty").ToString
                            rv.EndEdit()
                            iSeq += 1
                        End If
                        objView.RowFilter = ""
                    Next
                    objTbl.AcceptChanges()
                    Session("TblDtl") = objTbl
                    gvDtl.DataSource = Session("TblDtl")
                    gvDtl.DataBind()

                    If Not Session("TabelSN") Is Nothing Then
                        CheckBox1.Checked = False
                        CheckBox1.Enabled = False
                        FileExelInitStokSN.Text = ""
                        FileUpload1.Visible = False
                        BtnUplaodFile.Visible = False
                    Else
                        CheckBox1.Checked = False
                        CheckBox1.Enabled = True
                    End If

                    EnableHeader(Not (gvDtl.Rows.Count > 0))
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)

                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                End If
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True
        ' ButtonA_Click(sender, e)
    End Sub

    Protected Sub lbSelectAllToList_Click(sender As Object, e As EventArgs) Handles lbSelectAllToList.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblListMat")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListMat")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListMat") = objTbl
                gvListMat.DataSource = Session("TblListMat")
                gvListMat.DataBind()
                lbAddToListMat_Click(Nothing, Nothing)
                btnHideListMat.Visible = False
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            Else
                Session("WarningListMat") = "Please show material data first!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Please show material data first!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub gvDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDtl.PageIndexChanging
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.PageIndex = e.NewPageIndex
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvDtl.RowCancelingEdit
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(5).Text = "Yes" Then
                e.Row.Cells(6).Enabled = False
            Else
                e.Row.Cells(6).Enabled = True
            End If
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        'Dim country As String = gvDtl.Rows(e.RowIndex).Cells(5).Text
        'If country = "Yes" Then
        '    showMessage("Data Menggunakan SN Tidak Bisa Di Edit", CompnyCode, 3)
        '    e.Cancel = True
        'Else
        e.Cancel = True
        gvDtl.EditIndex = -1
        Dim objTable As DataTable = Session("TblDtl")
        '@@@@ SN @@@@@@@@@@@
        If Not Session("TabelSN") Is Nothing Then

            Dim objTableSN As DataTable = Session("TabelSN")
            Dim dvSN As DataView = objTableSN.DefaultView
            Dim itemid As String = objTable.Rows(e.RowIndex).Item("matcode")
            dvSN.RowFilter = "itemcode =  '" & itemid & "'"
            If Not dvSN.Count = 0 Then
                For kdsn As Integer = 0 To dvSN.Count - 1
                    objTableSN.Rows.RemoveAt(0)
                Next
            End If
            objTableSN.AcceptChanges()
            If objTableSN.Rows.Count < 0 Then
                CheckBox1.Enabled = True
            Else
                CheckBox1.Enabled = False
            End If
        End If


        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        If objTable.Rows.Count > 0 Then
            DDLBusUnit.Enabled = False
            DDLLocation.Enabled = False
            DDLBusUnit.CssClass = "inpTextDisabled"
            DDLLocation.CssClass = "inpTextDisabled"
        Else
            DDLBusUnit.Enabled = True
            DDLLocation.Enabled = True
            DDLBusUnit.CssClass = "inpText"
            DDLLocation.CssClass = "inpText"
        End If
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        'End If

    End Sub

    Protected Sub gvDtl_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvDtl.RowEditing

        Dim country As String = gvDtl.Rows(e.NewEditIndex).Cells(5).Text
        ' For this example, cancel the edit operation if the user attempts
        ' to edit the record of a customer from the United States. 
        If country = "Yes" Then
            ' Cancel the edit operation.
            'showMessage("Data Menggunakan SN Tidak Bisa Di Edit", CompnyCode, 3)
            'e.Cancel = True
            gvDtl.EditIndex = e.NewEditIndex
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
        Else

            gvDtl.EditIndex = e.NewEditIndex
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub gvDtl_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvDtl.RowUpdating
        Dim row As GridViewRow = gvDtl.Rows(e.RowIndex)
        Dim dQty As Double = ToDouble(CType(row.FindControl("tbQty"), System.Web.UI.WebControls.TextBox).Text)
        Dim dHarga As Double = ToDouble(CType(row.FindControl("tbAmount"), System.Web.UI.WebControls.TextBox).Text)
        Dim iOid As Integer = ToInteger(gvDtl.DataKeys(e.RowIndex).Value.ToString())
        Session("TblDtl").DefaultView.RowFilter = "seq=" & iOid
        Session("TblDtl").DefaultView(0)("matqty") = dQty
        Session("TblDtl").DefaultView(0)("harga") = dHarga
        Session("TblDtl").DefaultView.RowFilter = ""
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub btnFindListMat_Click(sender As Object, e As ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = ""
                If FilterDDLListMat.SelectedIndex = 0 Then
                    Dim sText() As String = FilterTextListMat.Text.Split(";")
                    For C1 As Integer = 0 To sText.Length - 1
                        If sText(C1) <> "" Then
                            sFilter &= FilterDDLListMat.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
                        End If
                    Next
                    If sFilter <> "" Then
                        sFilter = "(" & Left(sFilter, sFilter.Length - 4) & ")"
                    Else
                        sFilter = "1=1"
                    End If
                Else
                    If Tchar(FilterTextListMat.Text) = "" Then
                        If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
                            sFilter &= " merk = '" & DDLCat04.SelectedValue & "'"
                        End If
                        If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                            sFilter &= " itemtype = '" & DDLCat03.SelectedValue & "'"
                        ElseIf cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
                            sFilter &= " AND itemtype = '" & DDLCat03.SelectedValue & "'"
                        End If
                        If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                            sFilter &= " itemsubgroupoid ='" & DDLCat02.SelectedValue & "'"
                        ElseIf (cbCat03.Checked And DDLCat03.SelectedValue <> "") Or (cbCat04.Checked And DDLCat04.SelectedValue <> "") Then
                            sFilter &= " AND itemsubgroupoid ='" & DDLCat02.SelectedValue & "'"
                        End If
                        If (cbCat01.Checked And DDLCat01.SelectedValue <> "") And (cbCat02.Checked And DDLCat02.SelectedValue <> "") Then
                            sFilter &= " AND itemgroupoid ='" & DDLCat01.SelectedValue & "'"
                        ElseIf cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                            sFilter &= " itemgroupoid ='" & DDLCat01.SelectedValue & "'"
                        End If
                    Else
                        sFilter = FilterDDLListMat.SelectedValue & " like '%" & Tchar(FilterTextListMat.Text) & "%'"
                        If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
                            sFilter &= " AND merk = '" & DDLCat04.SelectedValue & "'"
                        End If
                        If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                            sFilter &= " AND itemtype = '" & DDLCat03.SelectedValue & "'"
                        End If
                        If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                            sFilter &= " AND itemsubgroupoid ='" & DDLCat02.SelectedValue & "'"
                        End If
                        If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                            sFilter &= " AND itemgroupoid ='" & DDLCat01.SelectedValue & "'"
                        End If



                    End If

                End If

                
                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(sender As Object, e As ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1
                FilterTextListMat.Text = ""
                InitDDLCat1()
                InitDDLCat2()
                'InitDDLCat3()
                'InitDDLCat4()
                cbCat01.Checked = False
                cbCat02.Checked = False
                cbCat03.Checked = False
                cbCat04.Checked = False
                Session("TblListMatView") = Session("TblListMat")
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub btnPost_Click(sender As Object, e As ImageClickEventArgs) Handles btnPost.Click
        If Session("TblDtl") Is Nothing Then
            showMessage("Please initiate some material data first!", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim objTable As DataTable = Session("TblDtl")
        If objTable.Rows.Count = 0 Then
            showMessage("Please initiate some material data first!", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim gDate As Date = "2016-12-28"
        Dim iInitStockOid As Integer = GenerateID("QL_INITSTOCK", CompnyCode)
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Dim conmtroid As Int64 = GenerateID("QL_conmtr", CompnyCode)
        Dim crdmtroid As Int64 = GenerateID("QL_crdmtr", CompnyCode)
        Try
            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                Dim sRef As String = "" : sRef = "SA" : Dim rateidr As String = ""
                rateidr = ToMaskEdit(GetStrData("select TOP 1 cast( rate2idrvalue AS INT) from ql_mstrate2 r INNER JOIN QL_mstcurr c ON c.cmpcode=r.cmpcode AND c.currencyoid=r.currencyoid WHERE c.currencycode = 'IDR' order by rate2oid desc "), 3)
                sSql = "INSERT INTO QL_initstock (cmpcode, initoid, initdate, periodacctg, initrefoid, initbrach, initlocation, initqty, initflag, upduser, updtime, initvalueidr, initvalueusd, inituser, initdatetime) VALUES ('" & CompnyCode & "', " & iInitStockOid & ", '" & sDate & "', '" & sPeriod & "', " & objTable.Rows(C1).Item("matoid") & ",'" & DDLBusUnit.SelectedValue & "', " & objTable.Rows(C1).Item("matwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("matqty").ToString) & ", 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("Harga").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("Harga").ToString) & " / " & rateidr & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP )"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                iInitStockOid += 1
                Dim lasthpp As Double = 0
                sSql = "SELECT hpp FROM QL_mstItem WHERE itemoid = " & objTable.Rows(C1).Item("matoid") & ""
                xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar
                If lasthpp = 0 Then
                    sSql = "update QL_mstItem SET HPP = " & ToDouble(objTable.Rows(C1).Item("Harga").ToString) & " WHERE itemoid = " & objTable.Rows(C1).Item("matoid") & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteScalar()
                End If
                Dim Amount As Decimal = ToMaskEdit(ToDouble(objTable.Rows(C1).Item("matqty")) * ToDouble(objTable.Rows(C1).Item("Harga").ToString), 2)
                'insert into connmtr if posting
                sSql = "INSERT INTO QL_conmtr (cmpcode,conmtroid,formoid,formname,type,trndate,periodacctg,refoid,refname, qtyin,amount, note,formaction,upduser,updtime,mtrlocoid,unitoid,hpp, branch_code) VALUES " & _
                    " ('" & CompnyCode & "'," & conmtroid & "," & iInitStockOid & ",'QL_initstock','Init Stock','" & sDate & "','" & sPeriod & "'," & objTable.Rows(C1).Item("matoid") & ",'QL_MSTITEM'," & objTable.Rows(C1).Item("matqty") & "," & Amount & ",'Data Dari Init Stock','" & sRef & "','" & Session("UserID") & "',current_timestamp," & objTable.Rows(C1).Item("matwhoid") & "," & objTable.Rows(C1).Item("unitoid") & "," & ToDouble(objTable.Rows(C1).Item("Harga").ToString) & ", '" & DDLBusUnit.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                conmtroid += 1

                sSql = "select count(*) from QL_crdmtr where periodacctg = '" & sPeriod & "' and refoid = " & objTable.Rows(C1).Item("matoid") & " and refname = 'QL_MSTITEM' and mtrlocoid = " & objTable.Rows(C1).Item("matwhoid") & " and branch_code = '" & DDLBusUnit.SelectedValue & "' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                If xCmd.ExecuteScalar() = 0 Then
                    sSql = "insert into QL_crdmtr (cmpcode, crdmatoid, lasttrans, lasttranstype, qtyin, periodacctg, refoid,saldoakhir, upduser, updtime, refname, mtrlocoid, branch_code) VALUES ('" & CompnyCode & "', '" & (crdmtroid + C1) & "','" & sDate & "', '" & sRef & "'," & ToDouble(objTable.Rows(C1).Item("matqty").ToString) & ",'" & sPeriod & "'," & objTable.Rows(C1).Item("matoid") & "," & ToDouble(objTable.Rows(C1).Item("matqty").ToString) & ", '" & Session("UserID") & "', current_timestamp, 'QL_MSTITEM'," & objTable.Rows(C1).Item("matwhoid") & ", '" & DDLBusUnit.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "update QL_crdmtr set lasttrans='" & sDate & "',lasttranstype='" & sRef & "'/*, saldoAwal = saldoAwal + " & ToDouble(objTable.Rows(C1).Item("matqty").ToString) & "*/, qtyin=" & ToDouble(objTable.Rows(C1).Item("matqty").ToString) & ", saldoakhir = saldoakhir + " & ToDouble(objTable.Rows(C1).Item("matqty").ToString) & " Where periodacctg = '" & sPeriod & "' and refoid = " & objTable.Rows(C1).Item("matoid") & " and refname = 'QL_MSTITEM' and mtrlocoid = " & objTable.Rows(C1).Item("matwhoid") & " and branch_code = '" & DDLBusUnit.SelectedValue & "' and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Next

            If Not Session("TabelSN") Is Nothing Then
                Dim objTableSN As DataTable = Session("TabelSN")
                For SNItem As Integer = 0 To objTableSN.Rows.Count - 1
                    sSql = "SELECT itemoid FROM QL_mstItem WHERE itemcode = '" & objTableSN.Rows(SNItem).Item(0) & "'"
                    xCmd.CommandText = sSql
                    Dim TampungItemID As Integer = xCmd.ExecuteScalar
                    sSql = "insert into QL_mstitemDtl (cmpcode, itemoid, itemcode, sn, status_item, branch_code, locoid, createtime, createuser, status_in_out, last_trans_type ) values ('" & CompnyCode & "', '" & TampungItemID & "','" & objTableSN.Rows(SNItem).Item(0) & "','" & objTableSN.Rows(SNItem).Item(1) & "', 'Post', '" & DDLBusUnit.SelectedValue & "', '" & DDLLocation.SelectedValue & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "','In','MATERIAL INIT' )"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "insert into QL_Mst_SN (id_sn, itemcode, status_in_out, status_approval, last_trans_type) values ('" & objTableSN.Rows(SNItem).Item(1) & "','" & objTableSN.Rows(SNItem).Item(0) & "', 'In', 'No PROCESS','MATERIAL INIT')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            sSql = "UPDATE QL_mstoid SET lastoid=" & iInitStockOid - 1 & " WHERE tablename='QL_INITSTOCK' AND cmpcode='" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update  QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + conmtroid) & " where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            sSql = "update  QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + crdmtroid) & " where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    btnPost_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.Message, CompnyName & " - ERORR", 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message, CompnyName & " - ERORR", 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, CompnyName & " - ERORR", 1)
            conn.Close()
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnInitStock.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnInitStock.aspx?awal=true")
    End Sub

    Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If Page.IsPostBack Then
            If CheckBox1.Checked = True Then
                FileUpload1.Visible = True : BtnUplaodFile.Visible = True
                TabContainer1.ActiveTabIndex = 1
            Else
                FileUpload1.Visible = False : BtnUplaodFile.Visible = False
                FileExelInitStokSN.Text = ""
                TabContainer1.ActiveTabIndex = 1
            End If
            btnPost.Visible = True : btnCancel.Visible = True
            btnLookUpMat.Visible = True
        End If

    End Sub

    Public Function getFileName(ByVal backID As String) As String
        Dim fname As String = "" : Dim ID As String = Trim(backID)
        fname = "InitStok_" & ID & ".xls"
        Return fname
    End Function

    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".xls"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function

    Public Sub uploadFileGambar(ByVal proses As String, ByVal fname As String)
        Dim savePath As String = Server.MapPath("~/FileExcel/")
        If FileUpload1.HasFile Then
            If checkTypeFile(FileUpload1.FileName) Then
                savePath &= fname
                Try
                    FileUpload1.PostedFile.SaveAs(savePath)
                    FileExelInitStokSN.Text = savePath
                    Session("LinkSaveFile") = FileExelInitStokSN.Text
                Catch ex As Exception
                End Try
            Else
                showMessage("Tidak dapat diupload. Format file harus .xls!", CompnyName & " - WARNING", 2)
            End If
        Else
        End If
    End Sub

    Protected Sub BtnUplaodFile_Click(sender As Object, e As EventArgs) Handles BtnUplaodFile.Click
        Dim fname As String = getFileName(Trim("File Exel Init Stok SN"))
        uploadFileGambar("insert", fname)
        btnPost.Visible = True : btnCancel.Visible = True
        btnLookUpMat.Visible = True
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbCloseListMat.Click
        btnHideListMat.Visible = False
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        btnPost.Visible = True : btnCancel.Visible = True
        btnLookUpMat.Visible = True
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvDtl.SelectedIndexChanged
    End Sub

    Protected Sub DDLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLocation.SelectedIndexChanged

    End Sub

    Protected Sub dd_branch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dd_branch.SelectedIndexChanged
        InitDDLLocationForm()
    End Sub

    Public Sub BindDataListItem()
        Dim sWhare As String = ""
        If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
            sWhare = "and ik.initbrach = " & dd_branch.SelectedValue
        End If

        If DDLocation.SelectedValue <> "ALL LOCATION" Then
            sWhare &= sWhare & " and ik.initlocation = " & DDLocation.SelectedValue
        End If
        sSql = "select DISTINCT m.itemoid, m.itemcode, m.itemdesc,m.itempriceunit1,m.itempriceunit2,m.itempriceunit3,m.itemoid, g.gendesc satuan1,g2.gendesc satuan2, g3.gendesc satuan3,m.konversi1_2, m.konversi2_3 , m.merk, (SELECT gendesc FROM QL_mstgen WHERE gencode =  ik.initbrach AND gengroup = 'cabang') branch_code    from ql_mstitem m  INNER JOIN QL_initstock ik ON m.itemoid = ik.initrefoid  inner join ql_mstgen g on g.genoid=m.satuan1 and m.itemflag='AKTIF'   inner join ql_mstgen g2 on g2.genoid=m.satuan2   inner join ql_mstgen g3 on g3.genoid=m.satuan3     WHERE m.cmpcode = '" & CompnyCode & "' " & sWhare & "  AND (m.itemdesc like '%" & Tchar(itemname.Text) & "%' or m.itemcode like '%" & Tchar(itemname.Text) & "%' or m.merk like '%" & Tchar(itemname.Text) & "%')"

        Dim DTItem As DataTable = cKon.ambiltabel(sSql, "listofitem")
        gvItem.DataSource = DTItem
        gvItem.DataBind()
        Session("listofitem") = DTItem
        gvItem.Visible = True
    End Sub

    Protected Sub btnSearchItem_Click(sender As Object, e As ImageClickEventArgs) Handles btnSearchItem.Click
        BindDataListItem()
    End Sub

    Protected Sub gvListMat_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvListMat.RowCommand

    End Sub

    Protected Sub gvListMat_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If has_sn = "Yes" Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(3).Enabled = True
            End If
        Else
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(3).Enabled = True
            End If
        End If
    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindData("")
        gvItem.Visible = True
        'gvItem.DataSource = Session("listofitem")
        'gvItem.DataBind()
    End Sub

    Protected Sub gvTblData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvTblData.PageIndexChanging
        gvTblData.PageIndex = e.NewPageIndex
        gvTblData.Visible = True
        gvTblData.DataSource = Session("tbldata")
        gvTblData.DataBind()
    End Sub

    Protected Sub btnfind_Click(sender As Object, e As ImageClickEventArgs) Handles btnfind.Click
        BindData("")
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)
        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub btnPreview_Click(sender As Object, e As ImageClickEventArgs) Handles btnPreview.Click
        Try
            Dim path As String = Server.MapPath("~/report/FileExel_SN.xls")
            'get file object as FileInfo
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(path)
            '-- if the file exists on the server
            Response.AddHeader("Content-Disposition", "attachment; filename=FileExel_SN.xls")
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End()
            'if file does not exist
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Protected Sub btnEraseItem_Click(sender As Object, e As ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = "" : itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub btnviewall_Click(sender As Object, e As ImageClickEventArgs) Handles btnviewall.Click
        chkPeriod.Checked = False
        dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
        dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        If Session("") = "01" Then
            dd_branch.SelectedValue = "SEMUA BRANCH"
        End If
        DDLocation.SelectedValue = "ALL LOCATION"
        itemname.Text = "" : itemoid.Text = ""
        BindData("")
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvItem.SelectedIndexChanged
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
    End Sub

    Protected Sub gvTblData_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvTblData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
        End If
    End Sub

    Protected Sub ButtonA_Click(sender As Object, e As EventArgs) Handles ButtonA.Click
        Dim objTbl As DataTable = Session("TblDtl")
        objTbl.AcceptChanges()
        Session("TblDtl") = objTbl
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvListMat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvListMat.SelectedIndexChanged
    End Sub
#End Region
End Class
