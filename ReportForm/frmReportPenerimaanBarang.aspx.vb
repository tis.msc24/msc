Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class ReportForm_frmReportPenerimaanBarang
    Inherits System.Web.UI.Page
#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)

    Dim xreader As SqlDataReader
    Dim xKon As New Koneksi
    Dim sSql As String = ""
    Dim dsData As New DataSet
    Dim dv As DataView
    Dim ckoneksi As New Koneksi
    Dim CProc As New ClassProcedure
    
    Dim report As New ReportDocument
    Dim folderReport As String = "~/report/"
    Dim param As String = ""
#End Region
#Region "Procedure"
    'Private Sub showMessage(ByVal message As String, ByVal caption As String)
    '    Validasi.Text = message
    '    lblCaption.Text = caption
    '    panelMsg.Visible = True
    '    btnMsg.Visible = True
    '    mpeMsg.Show()
    'End Sub
    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 3 Then 'Warning
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        lblCaption.Text = strCaption : Validasi.Text = sMessage
        CProc.SetModalPopUpExtender(btnMsg, panelMsg, mpeMsg, True)
    End Sub

    Sub showPrint(ByVal tipe As String)
        Dim sWhere As String = ""
        Try

            sWhere &= " and " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
            If txtTgl1.Text <> "" And txtTgl2.Text <> "" Then
                Try
                    'Dim date1 As Date = CDate(toDate(txtTgl1.Text))
                    'Dim date2 As Date = CDate(toDate(txtTgl2.Text))

                    'If date1 < CDate("01/01/1900") Then
                    '    showMessage("Tanggal Awal Tidak Valid !!", CompnyName & "- Warning") : Exit Sub
                    'End If
                    'If date2 < CDate("01/01/1900") Then
                    '    showMessage("Tanggal Akhir Tidak Valid !!", CompnyName & "- Warning") : Exit Sub
                    'End If
                    'If date1 > date2 Then
                    '    showMessage("Tanggal Awal harus lebih kecil dari Tanggal Akhir !!", CompnyName & "- Warning") : Exit Sub
                    '    txtTgl1.Text = ""
                    '    txtTgl2.Text = ""
                    '    Exit Sub
                    'End If

                    Try
                        'Dim dat1 As Date = CDate(toDate(txtTgl1.Text))
                        'Dim dat2 As Date = CDate(toDate(txtTgl2.Text))
                        Dim dat1 As Date = CDate(toDate(txtTgl1.Text))
                        Dim dat2 As Date = CDate(toDate(txtTgl2.Text))
                    Catch ex As Exception
                        showMessage("Format Tanggal Salah - Gunakan format dd/MM/yyyy !!", 2) : Exit Sub
                    End Try
                    If CDate(toDate(txtTgl1.Text)) > CDate(toDate(txtTgl2.Text)) Then
                        showMessage("Tanggal mulai tidak boleh lebih besar dari tanggal destinasi !!", 2) : Exit Sub
                    End If

                    'sSql &= " where convert(char(20),r.createtime, 101) between '" & CDate(toDate(txtTgl1.Text)) & "' " & _
                    '        "and '" & CDate(toDate(txtTgl2.Text)).AddDays(1) & "'"
                    sWhere &= " where convert(date,r.createtime, 101) between '" & CDate(toDate(txtTgl1.Text)) & "' " & _
                            "and '" & CDate(toDate(txtTgl2.Text)) & "'"

                Catch ex As Exception
                    showMessage("Format Tanggal tidak sesuai !!", 2) : Exit Sub
                End Try
            End If

            If cbstatus.SelectedValue = "Status" Then
                If StatusDDL.SelectedValue <> "All" Then
                    sWhere &= " AND r.reqstatus = '" & StatusDDL.SelectedValue & "'"
                End If
                sWhere &= " AND r.reqstatus <> 'paid' "
            Else
            End If
            If cbstatus.SelectedValue = "Paid" Then
                sWhere &= " AND r.reqstatus = 'paid' "
            End If
           
            Try
                'If conn.State = ConnectionState.Closed Then
                '    conn.Open()
                'End If

                'sSql = "select count(r.reqcode) from QL_TRNREQUEST r inner join QL_MSTCUST c ON r.reqcustoid = c.custoid inner join QL_mstgen as g ON r.reqitemtype=g.genoid inner join QL_mstgen as g2 ON r.reqitembrand = g2.genoid inner join QL_TRNREQUESTDTL t ON r.REQOID = t.REQMSTOID"

                'xCmd.CommandText = sSql & sWhere
                'Dim res As Integer = xCmd.ExecuteScalar
                'If res <= 0 Then
                '    showMessage("Tidak ada data !!", 3)
                '    Exit Sub
                'End If

                'If conn.State = ConnectionState.Open Then
                '    conn.Close()
                'End If
                report.Load(Server.MapPath(folderReport & "ReportPenerimaanBarang.rpt"))
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("dPeriode", txtTgl1.Text & " - " & txtTgl2.Text)
                report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

                CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

                If tipe = "PRINT" Then
                    CRVPenerimaanBarang.DisplayGroupTree = False
                    'CRVPenerimaanBarang.SeparatePages = False
                    CRVPenerimaanBarang.ReportSource = report
                ElseIf tipe = "EXCEL" Then
                    report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

                    Response.Buffer = False

                    Response.ClearContent()
                    Response.ClearHeaders()
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "LaporanPenerimaanService" & Format(GetServerTime(), "dd_MM_yy"))
                    report.Close() : report.Dispose()
                    Response.Redirect("~\ReportForm\frmReportPenerimaanBarang.aspx?awal=true")
                Else
                    report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

                    Response.Buffer = False

                    Response.ClearContent()
                    Response.ClearHeaders()
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "LaporanPenerimaanService" & Format(GetServerTime(), "dd_MM_yy"))
                    report.Close() : report.Dispose()
                    Response.Redirect("~\ReportForm\frmReportPenerimaanBarang.aspx?awal=true")
                End If
            Catch ex As Exception
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage(ex.ToString, 1)
            End Try
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub
    Private Sub resetReport()
        CRVPenerimaanBarang.ReportSource = Nothing
    End Sub
#End Region
#Region "Function"

#End Region
#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~\other\login.aspx")
        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Server.Transfer("~\other\NotAuthorize.aspx")
        'End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("frmReportPenerimaanBarang.aspx") ' recall lagi untuk clear
        End If
        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Laporan Penerimaan Service"

        If Not Page.IsPostBack Then
            Session("showReport") = Nothing
            'txtTgl1.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
            'txtTgl2.Text = Format(Now, "dd/MM/yyyy")
            txtTgl1.Text = Format(Now, "01/" & "MM/yyyy")
            txtTgl2.Text = Format(Now.Date, "dd/MM/yyyy ")
            
        End If
        If Session("showReport") = True Then
            showPrint("PRINT")
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        resetReport()
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbstatus.SelectedIndex = 0
        StatusDDL.SelectedIndex = 0
        txtTgl1.Text = Format(Now, "01/" & "MM/yyyy")
        txtTgl2.Text = Format(Now.Date, "dd/MM/yyyy ")
    End Sub
    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExcel.Click
        If txtTgl1.Text <> "" And txtTgl2.Text = "" Then
            showMessage("Isi Tanggal akhir !!", 2) : Exit Sub
        ElseIf txtTgl1.Text = "" And txtTgl2.Text <> "" Then
            showMessage("Isi Tanggal awal !!", 2) : Exit Sub
        End If
        showPrint("EXCEL")
    End Sub
    Protected Sub btnPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPDF.Click
        If txtTgl1.Text <> "" And txtTgl2.Text = "" Then
            showMessage("Isi Tanggal akhir !!", 2) : Exit Sub
        ElseIf txtTgl1.Text = "" And txtTgl2.Text <> "" Then
            showMessage("Isi Tanggal awal !!", 2) : Exit Sub
        End If
        showPrint("PDF")
    End Sub
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        Session("showReport") = True
        showPrint("PRINT")
    End Sub
    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        resetReport()
        Response.Redirect("frmReportPenerimaanBarang.aspx?awal=true")
        btnMsg.Visible = False
        panelMsg.Visible = False
    End Sub
	
#End Region
    
    Protected Sub Page_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
End Class
