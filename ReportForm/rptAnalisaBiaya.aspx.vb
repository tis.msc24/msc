' Last Update By 4n7JuK On 130924
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_RptAnalisaBiaya
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~\Report\"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(toDate(txtStart.Text)), CDate(toDate(txtFinish.Text))) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim iMonth As Integer = DDLMonth2.SelectedValue
            Dim iYear As Integer = DDLYear2.SelectedValue
            Dim sPeriode(11) As String
            For C1 As Integer = 0 To sPeriode.Length - 1
                sPeriode(C1) = GetDateToPeriodAcctg(New Date(iYear, iMonth, 1))
                If iMonth = 12 Then
                    iMonth = 1 : iYear += 1
                Else
                    iMonth += 1 : iYear = iYear
                End If
            Next

            Dim sCbg As String = ""
            If CbgDDL.SelectedValue <> "ALL" Then
                sCbg = CbgDDL.SelectedValue
            End If

            If ddlType.SelectedValue = "Analisa" Then
                If sType <> "Print Excel" Then
                    report.Load(Server.MapPath("~\Report\rptAnalisaBiaya.rpt"))
                Else
                    report.Load(Server.MapPath("~\Report\rptAnalisaBiayaXls.rpt"))
                End If

                Dim sSuppOid As String = ""
                Dim notaoid As String = ""
                Dim sWhere As String = " "
                Dim sDate As String = ""
                sWhere &= " Where cmpcode='" & CompnyCode & "'"
                If CbgDDL.SelectedValue <> "ALL" Then
                    sWhere &= " AND branch_code='" & CbgDDL.SelectedValue & "'"
                End If
                sDate &= " AND MONTH(jm.trnjualdate) = " & DDLMonth2.SelectedValue & " and YEAR(jm.trnjualdate) = " & DDLYear2.SelectedItem.Text & " "

                sSql = " SELECT cmpcode,Cabang,branch_code, keterangan, SUM(amtjualnetto)-SUM(amtretur)-SUM(amtcn) amtjualnetto, SUM(amtbiaya) AS amtbiaya FROM (" & _
    "/*JUAL*/" & _
    " select jm.cmpcode,g.gendesc AS Cabang,'Penj. Bersih' AS keterangan, SUM((jd.trnjualdtlprice*jd.trnjualdtlqty)-jd.amtjualdisc) amtjualnetto, 0 AS amtbiaya,0 amtretur, 0 amtcn,'1' urut, jm.branch_code from QL_trnjualdtl jd INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid = jd.trnjualmstoid and jm.branch_code = jd.branch_code INNER JOIN QL_mstgen g ON g.gencode = jm.branch_code AND g.gengroup = 'CABANG' where jm.orderno NOT IN (Select orderno from QL_trnordermst Where typeSO='Konsinyasi') and jm.trnjualdate>='12/28/2016 00:00:00' and jm.trnjualstatus = 'POST' and jm.trnjualmstoid > 0 " & sDate & " group by jm.branch_code, g.gendesc, jm.cmpcode UNION ALL " & _
    "/*RETUR*/" & _
    " select jm.cmpcode, g.gendesc AS Cabang,'Penj. Bersih' AS keterangan,0 amtjualnetto, 0 AS amtbiaya, ISNULL(SUM((jd.trnjualdtlprice*jd.trnjualdtlqty)-jd.amtjualdisc),0.00) amtretur, 0 amtcn,'1' urut, jm.branch_code From QL_trnjualreturdtl jd" & _
    " INNER JOIN QL_trnjualreturmst jm ON jm.trnjualreturmstoid=jd.trnjualreturmstoid AND jm.branch_code=jd.branch_code INNER JOIN QL_mstgen g ON g.gencode = jm.branch_code AND g.gengroup = 'CABANG' where jm.trnjualstatus = 'POST' and jm.typeSO<>'Konsinyasi' AND jm.trnjualdate>='12/28/2016 00:00:00' AND jm.trnjualstatus IN ('APPROVED','POST') " & sDate & " group by g.gendesc, jd.trnjualdtlprice, jm.branch_code, jm.cmpcode" & _
    " UNION ALL" & _
    "/*CN*/" & _
    " select jm.cmpcode,g.gendesc AS Cabang,'Penj. Bersih' AS keterangan,0 amtjualnetto, 0 AS amtbiaya,0 amtretur, SUM(cn.amount) amtcn,'1' urut, jm.branch_code" & _
    " From QL_CreditNote cn " & _
    " Inner Join QL_trnjualmst jm ON jm.trnjualmstoid=cn.refoid AND jm.trnjualmstoid>0" & _
    " INNER JOIN QL_mstgen g ON g.gencode=jm.branch_code AND g.gengroup='CABANG' " & _
    " Where cn.cnstatus IN ('APPROVED','POST') AND jm.orderno NOT IN (Select orderno from QL_trnordermst Where typeSO='Konsinyasi') AND jm.trnjualdate>='12/28/2016 00:00:00' AND jm.trnjualstatus='POST' " & sDate & " group by g.gendesc, jm.branch_code, jm.cmpcode " & _
    " UNION ALL " & _
    "/*BIAYA*/" & _
    " SELECT cmpcode, cabang, Keterangan, amtjualnetto, SUM(D)-SUM(C) amtbiaya, amtretur, amtcn, urut, branch_code FROM (SELECT gm.cmpcode, g.gendesc AS cabang, 'Biaya' Keterangan, 0 AS amtjualnetto, 0 amtretur, 0 amtcn, '2' urut, a.branch_code, CASE WHEN gd.gldbcr = 'D' THEN gd.glamt ELSE 0 END D, CASE WHEN gd.gldbcr = 'C' THEN gd.glamt * 1 ELSE 0 END C FROM QL_trngldtl gd INNER JOIN QL_trnglmst gm ON gm.glmstoid = gd.glmstoid and gm.branch_code = gd.branch_code INNER JOIN QL_mstacctg a ON a.acctgoid = gd.acctgoid and (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) INNER JOIN QL_mstgen g ON g.gencode = a.branch_code AND g.gengroup = 'CABANG' where MONTH(gm.gldate) = " & DDLMonth2.SelectedValue & " and YEAR(gm.gldate) = " & DDLYear2.SelectedItem.Text & " ) GG GROUP BY cmpcode, Cabang, keterangan, amtjualnetto, amtretur, amtcn, urut, branch_code " & _
    ") byy " & sWhere & " " & _
    "group by cabang, keterangan, branch_code, urut, cmpcode " & _
    "order by branch_code, urut "

                Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnjualmst")
                Dim dvTbl As DataView = dtTbl.DefaultView
                report.SetDataSource(dvTbl.ToTable)

                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                report.SetParameterValue("sPeriod", DDLMonth2.SelectedItem.Text & " " & DDLYear2.SelectedItem.Text)
            ElseIf ddlType.SelectedValue = "Detail" Then
                If sType <> "Print Excel" Then
                    report.Load(Server.MapPath("~\Report\rptBiayaDetail.rpt"))
                Else
                    report.Load(Server.MapPath("~\Report\rptBiayaDetailXls.rpt"))
                End If
                Dim sWhere As String = ""
                Dim sWherex As String = ""
                If CbgDDL.SelectedValue <> "ALL" Then
                    sWhere &= " AND a.branch_code = '" & CbgDDL.SelectedValue & "'"
                    sWherex &= " AND cbx.branch_code = '" & CbgDDL.SelectedValue & "'"
                End If

                sSql = " select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear,month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 01 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 01 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, 0.0 period01amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 02 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 02 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 03 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 03 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 04 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 04 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 05 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 05 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code AND cb.branch_code = gd.branch_code where month(cashbankdate) = 06 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 06 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 07 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 07 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear ,month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 08 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 08 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 09 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period09amt, 0.0 period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 09 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc,gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period10amt, 0.0 period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear,month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 10 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period10amt, 0.0 period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 10 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period11amt, 0.0 period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 11 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period11amt, 0.0 period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 11 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
" UNION ALL " & _
"select a.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, gl.cashbankglnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, (case when gl.cashbankgroup IN ('COST', 'AP', 'ARK') then cashbankglamt else cashbankglamt * -1 end) period12amt from QL_mstacctg a INNER JOIN (select year(cashbankdate) cashbankdateyear, month(cashbankdate) cashbankdatemonth, gd.cashbankglnote, cashbankglamt, cb.cashbankgroup, acctgoid from ql_cashbankgl gd INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid = gd.cashbankoid AND cb.branch_code = gd.branch_code where month(cashbankdate) = 12 ) gl on gl.acctgoid = a.acctgoid INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and cashbankdateyear = " & DDLYear2.SelectedItem.Text & " " & sWhere & " " & _
" UNION ALL " & _
" select m.cmpcode, g.gendesc AS cabang, a.acctgcode, a.branch_code, a.acctgoid, a.acctgdesc, d.glnote, 0.0 period01amt, 0.0 period02amt, 0.0 period03amt, 0.0 period04amt, 0.0 period05amt, 0.0 period06amt, 0.0 period07amt, 0.0 period08amt, 0.0 period09amt, 0.0 period10amt, 0.0 period11amt, (case when d.gldbcr = 'D' then d.glamt else d.glamt * -1 end) period12amt from QL_trngldtl d inner join QL_trnglmst m on m.glmstoid = d.glmstoid AND m.branch_code = d.branch_code inner join QL_mstacctg a on a.acctgoid=d.acctgoid inner join ql_mstgen g on g.gencode = a.branch_code and g.gengroup = 'CABANG' where (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and ( m.type IN ('ADJ', 'CN', 'DN', 'FIN', 'MU', 'NT', 'SI', 'SR', 'SRV', 'PI') or type like '%QL_trnpayhrmst%') and MONTH(gldate) = 12 and YEAR(gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & " ORDER BY a.branch_code,a.acctgcode"

                Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankmst")
                Session("QL_trncashbankmst") = dtTbl
                Dim dvTbl As DataView = dtTbl.DefaultView

                'sSql = "select '' cmpcode, '' cabang, '' branch_code, 0 acctgoid, '' acctgcode,'' acctgdesc,'' cashbankglnote,0.00 cashbankglamt, 0.00 totalglamt," & _
                '"'201301' period01,'JAN 2013' period01desc,0.00 period01amt,'201302' period02,'FEB 2013' period02desc,0.00 period02amt,'201303' period03,'MAR 2013' period03desc,0.00 period03amt," & _
                '"'201304' period04,'APR 2013' period04desc,0.00 period04amt,'201305' period05,'MEI 2013' period05desc,0.00 period05amt,'201306' period06,'JUN 2013' period06desc,0.00 period06amt," & _
                '"'201307' period07,'JUL 2013' period07desc,0.00 period07amt,'201308' period08,'AGT 2013' period08desc,0.00 period08amt,'201309' period09,'SEP 2013' period09desc,0.00 period09amt," & _
                '"'201310' period10,'OKT 2013' period10desc,0.00 period10amt,'201311' period11,'NOV 2013' period11desc,0.00 period11amt,'201312' period12,'DES 2013' period12desc,0.00 period12amt," & _
                '"0.00 totalperiodamt"
                'Dim dtlTblTemp As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankmst")
                'dtlTblTemp.Rows.Clear()

                'For R1 As Integer = 0 To dtTbl.Rows.Count - 1
                '    Dim iIndex As Integer = Array.IndexOf(sPeriode, dtTbl.Rows(R1)("periodacctg").ToString)
                '    Dim oRow As DataRow
                '    Dim dAmt() As Decimal = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
                '    Select Case iIndex + 1
                '        Case 1
                '            dAmt(0) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 2
                '            dAmt(1) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 3
                '            dAmt(2) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 4
                '            dAmt(3) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 5
                '            dAmt(4) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 6
                '            dAmt(5) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 7
                '            dAmt(6) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 8
                '            dAmt(7) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 9
                '            dAmt(8) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 10
                '            dAmt(9) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 11
                '            dAmt(10) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '        Case 12
                '            dAmt(11) = ToDouble(dtTbl.Rows(R1)("cashbankglamt").ToString)
                '    End Select

                '    If dtlTblTemp.Select("acctgoid=" & dtTbl.Rows(R1)("acctgoid").ToString, "").Length > 0 Then
                '        oRow = dtlTblTemp.Select("acctgoid=" & dtTbl.Rows(R1)("acctgoid").ToString, "")(0)
                '        oRow("period01amt") = ToDouble(oRow("period01amt").ToString) + ToDouble(dAmt(0).ToString)
                '        oRow("period02amt") = ToDouble(oRow("period02amt").ToString) + ToDouble(dAmt(1).ToString)
                '        oRow("period03amt") = ToDouble(oRow("period03amt").ToString) + ToDouble(dAmt(2).ToString)
                '        oRow("period04amt") = ToDouble(oRow("period04amt").ToString) + ToDouble(dAmt(3).ToString)
                '        oRow("period05amt") = ToDouble(oRow("period05amt").ToString) + ToDouble(dAmt(4).ToString)
                '        oRow("period06amt") = ToDouble(oRow("period06amt").ToString) + ToDouble(dAmt(5).ToString)
                '        oRow("period07amt") = ToDouble(oRow("period07amt").ToString) + ToDouble(dAmt(6).ToString)
                '        oRow("period08amt") = ToDouble(oRow("period08amt").ToString) + ToDouble(dAmt(7).ToString)
                '        oRow("period09amt") = ToDouble(oRow("period09amt").ToString) + ToDouble(dAmt(8).ToString)
                '        oRow("period10amt") = ToDouble(oRow("period10amt").ToString) + ToDouble(dAmt(9).ToString)
                '        oRow("period11amt") = ToDouble(oRow("period11amt").ToString) + ToDouble(dAmt(10).ToString)
                '        oRow("period12amt") = ToDouble(oRow("period12amt").ToString) + ToDouble(dAmt(11).ToString)

                '        oRow("totalperiodamt") = ToDouble(oRow("period01amt").ToString) + ToDouble(oRow("period02amt").ToString) + ToDouble(oRow("period03amt").ToString) + _
                '            ToDouble(oRow("period04amt").ToString) + ToDouble(oRow("period05amt").ToString) + ToDouble(oRow("period06amt").ToString) + _
                '            ToDouble(oRow("period07amt").ToString) + ToDouble(oRow("period08amt").ToString) + ToDouble(oRow("period09amt").ToString) + _
                '            ToDouble(oRow("period10amt").ToString) + ToDouble(oRow("period11amt").ToString) + ToDouble(oRow("period12amt").ToString)
                '        dtlTblTemp.Select(Nothing, Nothing)
                '        dtlTblTemp.AcceptChanges()
                '    Else
                '        oRow = dtlTblTemp.NewRow

                '        oRow("cmpcode") = CompnyCode
                '        oRow("periodreport") = DDLMonth2.SelectedItem.Text & " " & DDLYear2.SelectedItem.Text & " - " & DDLMonth2.SelectedItem.Text & " " & DDLYear2.SelectedItem.Text
                '        oRow("acctgoid") = dtTbl.Rows(R1)("acctgoid").ToString
                '        oRow("acctgcode") = dtTbl.Rows(R1)("acctgcode").ToString
                '        oRow("acctgdesc") = dtTbl.Rows(R1)("acctgdesc").ToString

                '        oRow("period01") = sPeriode(0)
                '        oRow("period01desc") = Format(New Date(Left(sPeriode(0), 4), Right(sPeriode(0), 2), 1), "MMM yyyy")
                '        oRow("period01amt") = ToDouble(oRow("period01amt").ToString) + ToDouble(dAmt(0).ToString)

                '        oRow("period02") = sPeriode(0)
                '        oRow("period02desc") = Format(New Date(Left(sPeriode(1), 4), Right(sPeriode(1), 2), 1), "MMM yyyy")
                '        oRow("period02amt") = ToDouble(oRow("period02amt").ToString) + ToDouble(dAmt(1).ToString)

                '        oRow("period03") = sPeriode(0)
                '        oRow("period03desc") = Format(New Date(Left(sPeriode(2), 4), Right(sPeriode(2), 2), 1), "MMM yyyy")
                '        oRow("period03amt") = ToDouble(oRow("period03amt").ToString) + ToDouble(dAmt(2).ToString)

                '        oRow("period04") = sPeriode(0)
                '        oRow("period04desc") = Format(New Date(Left(sPeriode(3), 4), Right(sPeriode(3), 2), 1), "MMM yyyy")
                '        oRow("period04amt") = ToDouble(oRow("period04amt").ToString) + ToDouble(dAmt(3).ToString)

                '        oRow("period05") = sPeriode(0)
                '        oRow("period05desc") = Format(New Date(Left(sPeriode(4), 4), Right(sPeriode(4), 2), 1), "MMM yyyy")
                '        oRow("period05amt") = ToDouble(oRow("period05amt").ToString) + ToDouble(dAmt(4).ToString)

                '        oRow("period06") = sPeriode(0)
                '        oRow("period06desc") = Format(New Date(Left(sPeriode(5), 4), Right(sPeriode(5), 2), 1), "MMM yyyy")
                '        oRow("period06amt") = ToDouble(oRow("period06amt").ToString) + ToDouble(dAmt(5).ToString)

                '        oRow("period07") = sPeriode(0)
                '        oRow("period07desc") = Format(New Date(Left(sPeriode(6), 4), Right(sPeriode(6), 2), 1), "MMM yyyy")
                '        oRow("period07amt") = ToDouble(oRow("period07amt").ToString) + ToDouble(dAmt(6).ToString)

                '        oRow("period08") = sPeriode(0)
                '        oRow("period08desc") = Format(New Date(Left(sPeriode(7), 4), Right(sPeriode(7), 2), 1), "MMM yyyy")
                '        oRow("period08amt") = ToDouble(oRow("period08amt").ToString) + ToDouble(dAmt(7).ToString)

                '        oRow("period09") = sPeriode(0)
                '        oRow("period09desc") = Format(New Date(Left(sPeriode(8), 4), Right(sPeriode(8), 2), 1), "MMM yyyy")
                '        oRow("period09amt") = ToDouble(oRow("period09amt").ToString) + ToDouble(dAmt(8).ToString)

                '        oRow("period10") = sPeriode(0)
                '        oRow("period10desc") = Format(New Date(Left(sPeriode(9), 4), Right(sPeriode(9), 2), 1), "MMM yyyy")
                '        oRow("period10amt") = ToDouble(oRow("period10amt").ToString) + ToDouble(dAmt(9).ToString)

                '        oRow("period11") = sPeriode(0)
                '        oRow("period11desc") = Format(New Date(Left(sPeriode(10), 4), Right(sPeriode(10), 2), 1), "MMM yyyy")
                '        oRow("period11amt") = ToDouble(oRow("period11amt").ToString) + ToDouble(dAmt(10).ToString)

                '        oRow("period12") = sPeriode(0)
                '        oRow("period12desc") = Format(New Date(Left(sPeriode(11), 4), Right(sPeriode(11), 2), 1), "MMM yyyy")
                '        oRow("period12amt") = ToDouble(oRow("period12amt").ToString) + ToDouble(dAmt(11).ToString)
                '        oRow("totalperiodamt") = ToDouble(oRow("period01amt").ToString) + ToDouble(oRow("period02amt").ToString) + ToDouble(oRow("period03amt").ToString) + _
                '            ToDouble(oRow("period04amt").ToString) + ToDouble(oRow("period05amt").ToString) + ToDouble(oRow("period06amt").ToString) + _
                '            ToDouble(oRow("period07amt").ToString) + ToDouble(oRow("period08amt").ToString) + ToDouble(oRow("period09amt").ToString) + _
                '            ToDouble(oRow("period10amt").ToString) + ToDouble(oRow("period11amt").ToString) + ToDouble(oRow("period12amt").ToString)
                '        dtlTblTemp.Rows.Add(oRow)
                '    End If
                'Next

                report.SetDataSource(dvTbl.ToTable)

                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                report.SetParameterValue("sPeriod", DDLYear2.SelectedItem.Text)
            Else
                If sType <> "Print Excel" Then
                    report.Load(Server.MapPath("~\Report\rptBiayaSum.rpt"))
                Else
                    report.Load(Server.MapPath("~\Report\rptBiayaSumXls.rpt"))
                End If
                Dim sWhere As String = ""
                Dim sWherex As String = ""
                If CbgDDL.SelectedValue <> "ALL" Then
                    sWhere &= " AND a.branch_code = '" & CbgDDL.SelectedValue & "'"
                    sWherex &= " AND ax.branch_code = '" & CbgDDL.SelectedValue & "'"
                End If

                sSql = "select cb.cmpcode, g.gendesc AS cabang, a.branch_code, a.acctgcode, a.acctgdesc" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 1 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period01amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 2 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period02amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 3 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period03amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 4 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period04amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 5 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period05amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 6 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period06amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 7 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period07amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 8 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period08amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 9 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period09amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 10 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period10amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 11 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period11amt" & _
", ISNULL((SELECT SUM(D) - SUM(C) FROM (select CASE WHEN glx.gldbcr = 'D' THEN SUM(glx.glamt) ELSE 0 END D, CASE WHEN glx.gldbcr = 'C' THEN SUM(glx.glamt) * 1 ELSE 0 END C from QL_mstacctg ax INNER JOIN QL_trngldtl glx ON glx.acctgoid = ax.acctgoid INNER JOIN QL_trnglmst cbx ON cbx.glmstoid = glx.glmstoid AND cbx.branch_code=glx.branch_code INNER JOIN QL_mstgen gx ON gx.gencode = cbx.branch_code and gx.gengroup = 'CABANG' WHERE (a.acctggrp1 = 'EXPENSE' OR a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) and MONTH(cbx.gldate) = 12 and YEAR(cbx.gldate) = " & DDLYear2.SelectedItem.Text & " and ax.acctgoid = a.acctgoid " & sWherex & " group by glx.gldbcr) gll),0.0) period12amt" & _
" from QL_mstacctg a " & _
"INNER JOIN QL_trngldtl gl ON gl.acctgoid = a.acctgoid " & _
"INNER JOIN QL_trnglmst cb ON cb.glmstoid = gl.glmstoid " & _
"INNER JOIN QL_mstgen g ON g.gencode = a.branch_code and g.gengroup = 'CABANG' " & _
"WHERE (a.acctggrp1 = 'EXPENSE'  or a.acctgoid in (select acctgoid from QL_mstacctg ax where ax.acctgcode like '502%' or ax.acctgcode like '503%' and ax.branch_code = a.branch_code)) AND YEAR(cb.gldate) = " & DDLYear2.SelectedItem.Text & " " & sWhere & "" & _
"GROUP BY cb.cmpcode, g.gendesc, a.branch_code, a.acctgcode, a.acctgdesc, a.acctgoid, a.acctggrp1 " & _
"ORDER BY a.branch_code, a.acctgcode"

                Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankmst")
                Session("QL_trncashbankmst") = dtTbl
                Dim dvTbl As DataView = dtTbl.DefaultView

                report.SetDataSource(dvTbl.ToTable)

                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                report.SetParameterValue("sPeriod", DDLYear2.SelectedItem.Text)
            End If

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ANALISABIAYAREPORT_" & ddlType.SelectedValue & "" & Format(CDate(toDate(txtStart.Text)), "yyyy"))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ANALISABIAYAREPORT_" & ddlType.SelectedValue & "" & Format(CDate(toDate(txtStart.Text)), "yyyy"))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub ddlCbg()
        Dim scb As String = ""
        sSql = "Select gencode,gendesc from QL_mstgen Where gengroup='CABANG'"

        If Session("branch_id") <> "10" Then
            CbgDDL.Enabled = False : CbgDDL.CssClass = "inpTextDisabled"
            sSql &= "And gencode='" & Session("branch_id") & "'"
            FillDDL(CbgDDL, sSql)
        Else
            FillDDL(CbgDDL, sSql)
            CbgDDL.Enabled = True : CbgDDL.CssClass = "inpText"
            CbgDDL.Items.Add(New ListItem("ALL", "ALL"))
            CbgDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' BULAN
        For C1 As Integer = 1 To 12
            Dim liMonth2 As New ListItem(MonthName(C1).ToUpper, C1)
            DDLMonth2.Items.Add(liMonth2)
        Next
        DDLMonth2.SelectedValue = GetServerTime.Month
        ' TAHUN
        For C1 As Integer = GetServerTime.Year - 5 To GetServerTime.Year + 5
            Dim liYear2 As New ListItem(C1, C1)
            DDLYear2.Items.Add(liYear2)
        Next
        DDLYear2.SelectedValue = GetServerTime.Year
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptanalisabiaya.aspx")
        End If
        Session("idPage") = Request.QueryString("idPage")
        Page.Title = CompnyName & " - Laporan Analisa Biaya"

        If Not Page.IsPostBack Then
            ddlCbg() : InitAllDDL()
            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False
            txtStart.Text = Format(GetServerTime, "01/MM/yyyy")
            txtFinish.Text = Format(GetServerTime, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("View")
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\rptanalisabiaya.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region

    Protected Sub ddlType_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.TextChanged
        If ddlType.SelectedValue = "Analisa" Then
            DDLMonth2.Visible = True
        ElseIf ddlType.SelectedValue = "Detail" Then
            DDLMonth2.Visible = False
        Else
            DDLMonth2.Visible = False
        End If
    End Sub
End Class
