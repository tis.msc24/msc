Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class ReportForm_frmpostat
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptRealisasiVoucher.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Laporan Realisasi Voucher"

        If Not Page.IsPostBack Then
            Session("showReport") = False : ddlCabang()
            ': PicDDl()
            date1.Text = "01/" & Date.Now.Month & "/" & Date.Now.Year & ""
            date2.Text = Format(Date.Now, "dd/MM/yyyy")
            'SuppBind()
        End If
        If Session("showReport") = True Then
            showPrint(dView.SelectedValue, "")
        End If
    End Sub

    Private Sub ddlCabang()
        Dim deloc As String = ""
        Deloc = "SELECT DISTINCT gencode,gendesc FROM QL_mstgen g WHERE gencode IN (SELECT fromMtrBranch FROM  ql_trntrfmtrmst tr WHERE tr.fromMtrBranch=g.gencode) AND g.gengroup='CABANG'"
        If checkPagePermission("ReportForm/rptRealisasiVoucher.aspx", Session("SpecialAccess")) = False Or Session("branch_id") <> "01" Then
            deloc &= "AND gencode = '" & Session("branch_id") & "'"
            FillDDL(BranchDDL, deloc)
        Else
            FillDDL(BranchDDL, deloc)
            BranchDDL.Items.Add("ALL BRANCH")
            BranchDDL.SelectedValue = "ALL BRANCH"
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Sub showPrint(ByVal tipe As String, ByVal ekstensi As String)
        Try
            Dim dat1 As Date = CDate(toDate(date1.Text))
            Dim dat2 As Date = CDate(toDate(date2.Text))
        Catch ex As Exception
            labelmsg.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(date1.Text)) > CDate(toDate(date2.Text)) Then
            labelmsg.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        CrystalReportViewer1.Visible = True
        Try
            Dim sWhere As String = "" : Dim d1 As String = "" : Dim d2 As String = ""
            'If ddlsumdetail.SelectedValue <> "Summary" Then
            sWhere &= "WHERE cmpcode='MSC' AND realisasidate between '" & date1.Text & "' And " & _
            "'" & date2.Text & "'"
            d1 = date1.Text : d2 = date2.Text
            'Else
            'sWhere &= "WHERE cmpcode='MSC'"
            'End If
            If ddlsumdetail.SelectedValue = "Summary" Then
                If trnPono.Text <> "" Then
                    sWhere &= " AND trnbelipono='" & trnPono.Text & "'"
                End If
            Else
                If trnPono.Text <> "" Then
                    sWhere &= " AND trnbelipono='" & trnPono.Text & "'"
                End If
            End If
            If txtSupplier.Text <> "" Then
                sWhere &= "AND suppname like '%" & txtSupplier.Text & "%'"
            End If
            If txtkdVcr.Text <> "" Then
                sWhere &= "AND voucherno='" & kdVoucher.Text & "'"
            End If

            If txtKdRealisasi.Text <> "" Then
                sWhere &= "AND realisasioid='" & KdRealisasi.Text & "'"
            End If

            vReport = New ReportDocument
            If ddlsumdetail.SelectedValue = "Summary" Then
                vReport.Load(Server.MapPath("~\Report\rptRealisasiVoucherSum.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptRealisasiVoucher.rpt"))
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)
            'CrystalReportViewer1.DisplayGroupTree = False
            vReport.SetParameterValue("d1", d1)
            vReport.SetParameterValue("d2", d2)
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            If ekstensi = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
            Else
                If ekstensi = "PDF" Then
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                    vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
                Else
                    CrystalReportViewer1.DisplayGroupTree = False
                    CrystalReportViewer1.ReportSource = vReport
                End If
            End If
        Catch ex As Exception
            labelmsg.Text = ex.ToString
        End Try
    End Sub

    Private Sub BindPO()
        sSql = "SELECT * FROM (SELECT po.trnbelimstoid,po.trnbelipono,ConVert(VarChar(20),trnbelipodate,103) AS poDate,po.trnsuppoid,s.suppname,po.upduser FROM QL_pomst po INNER JOIN QL_mstsupp s ON s.suppoid=po.trnsuppoid WHERE trnbelipono IN (SELECT DISTINCT sjm.trnbelipono FROM ql_trnsjbelimst sjm WHERE sjm.trnbelipono=po.trnbelipono) AND po.trnbelipono LIKE '%" & Tchar(trnPono.Text) & "%' and po.flagvcr=1" & _
        " UNION ALL" & _
        " SELECT po.voucheroid trnbelimstoid,po.trnbelino trnbelipono,ConVert(VarChar(20),voucherdate,103) AS poDate,0,'' suppname,'' upduser FROM QL_voucherdtl po Where voucherflag='INIT' AND po.trnbelino LIKE '%" & Tchar(trnPono.Text) & "%') AS v Order By trnbelimstoid Desc"
        FillGV(poNoGV, sSql, "QL_pomst")
        poNoGV.Visible = True
    End Sub

    Private Sub BindKdRealisasi()
        Dim sWhere As String = ""
        If trnPono.Text <> "" Then
            sWhere &= "and po.trnbelipono='" & Tchar(trnPono.Text) & "'"
        End If
        If txtkdVcr.Text <> "" Then
            sWhere &= "and vc.voucherno like '" & kdVoucher.Text & "' or vc.voucherdesc like '" & txtkdVcr.Text & "'"
        End If
        sSql = "select distinct tr.realisasioid,tr.realisasino, convert(varchar(10),tr.realisasidate, 103) realisasidate, tr.realisasidate from QL_trnrealisasi tr inner join QL_trnrealisasidtl trd on trd.trnrealisasioid = tr.realisasioid inner join QL_pomst po on po.trnbelimstoid=tr.pomstoid inner join QL_voucherdtl vc on vc.voucheroid=trd.voucheroid where tr.realisasino like '%" & Tchar(txtkdVcr.Text) & "%' " & sWhere & " order by tr.realisasidate asc"
        FillGV(KdRealisasiGV, sSql, "QL_trnrealisasi")
        KdRealisasiGV.Visible = True
    End Sub

    Private Sub BindKdVcr()
        Dim sWhere As String = ""
        If trnPono.Text <> "" Then
            sWhere &= "and po.trnbelipono='" & Tchar(trnPono.Text) & "'"
        End If
        sSql = "select distinct vc.voucherno, vc.voucherdesc/*, CONVERT(varchar(10), vc.voucherdate, 101) as voucherdate*/ from QL_voucherdtl vc inner join QL_pomst po on po.trnbelimstoid=vc.trnbelimstoid where  vc.voucherno like '%" & Tchar(txtkdVcr.Text) & "%' or voucherdesc like '%" & Tchar(txtkdVcr.Text) & "%' " & sWhere & " order by voucherno desc"
        FillGV(kdVcrGV, sSql, "QL_voucherdtl")
        kdVcrGV.Visible = True
    End Sub

    Private Sub BindSupplier()
        Dim sWhere As String = ""
        If trnPono.Text <> "" Then
            sWhere &= "and po.trnbelipono='" & Tchar(trnPono.Text) & "'"
        End If
        sSql = "select distinct s.suppoid, s.suppname, s.suppaddr from QL_mstsupp s inner join QL_pomst po on po.trnsuppoid = s.suppoid inner join QL_trnrealisasi r on r.pomstoid = po.trnbelimstoid where  s.suppcode like '%" & Tchar(txtSupplier.Text) & "%' or s.suppname like '%" & Tchar(txtSupplier.Text) & "%' " & sWhere & " order by s.suppname desc"
        FillGV(gvSupplier, sSql, "QL_mstsupp")
        gvSupplier.Visible = True
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Session("showReport") = True
        showPrint(dView.SelectedValue, "")
    End Sub

    Protected Sub dView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub detailstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ResetReport()
    End Sub

    Private Sub ResetReport()
        Session("showReport") = False
        CrystalReportViewer1.ReportSource = Nothing
        date1.Text = "01/" & Date.Now.Month & "/" & Date.Now.Year & ""
        date2.Text = Format(Date.Now, "dd/MM/yyyy")
        KdRealisasi.Text = ""
        txtKdRealisasi.Text = ""

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub imbExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint(dView.SelectedValue, "EXCEL")
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint(dView.SelectedValue, "PDF")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptRealisasiVoucher.aspx?awal=true")
    End Sub

    Protected Sub ImgPono_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgPono.Click
        BindPO()
    End Sub

    Protected Sub poNoGV_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        poNoGV.PageIndex = e.NewPageIndex
        BindPO()
    End Sub

    Protected Sub poNoGV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        trnPono.Text = poNoGV.SelectedDataKey.Item("trnbelipono")
        PoMstoid.Text = poNoGV.SelectedDataKey.Item("trnbelimstoid")
        poNoGV.Visible = False
        ResetReport()
    End Sub

    Protected Sub ImgErasePono_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trnPono.Text = "" : PoMstoid.Text = ""
        ResetReport()
        poNoGV.Visible = False
    End Sub

    Protected Sub ddlsumdetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ResetReport()
        If ddlsumdetail.SelectedValue = "Summary" Then
            'period
            'Label11.Visible = False : Label12.Visible = False
            'date1.Visible = False : search1.Visible = False
            'date2.Visible = False : search2.Visible = False
            'Label1.Visible = False : Label3.Visible = False
            'kode realisasi
            Label9.Visible = False : Label10.Visible = False
            txtKdRealisasi.Visible = False : ImgKdRealisasi.Visible = False
            EraseKdRealisasi.Visible = False

        Else
            'period
            'Label11.Visible = True : Label12.Visible = True
            'date1.Visible = True : search1.Visible = True
            'date2.Visible = True : search2.Visible = True
            'Label1.Visible = True : Label3.Visible = True
            'kode realisasi
            Label9.Visible = True : Label10.Visible = True
            txtKdRealisasi.Visible = True : ImgKdRealisasi.Visible = True
            EraseKdRealisasi.Visible = True
        End If
    End Sub

    Protected Sub ImgKdvcr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgKdvcr.Click
        BindKdVcr()
    End Sub

    Protected Sub kdVcrGV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtkdVcr.Text = kdVcrGV.SelectedDataKey.Item("voucherdesc")
        kdVoucher.Text = kdVcrGV.SelectedDataKey.Item("voucherno")
        kdVcrGV.Visible = False
        ResetReport()
    End Sub

    Protected Sub kdVcrGV_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        kdVcrGV.PageIndex = e.NewPageIndex
        BindKdVcr()
    End Sub

    Protected Sub imgerasekdvcr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtkdVcr.Text = "" : kdVoucher.Text = ""
        ResetReport()
        kdVcrGV.Visible = False
    End Sub

    Protected Sub ImgKdRealisasi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindKdRealisasi()
    End Sub

    Protected Sub KdRealisasiGV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtKdRealisasi.Text = KdRealisasiGV.SelectedDataKey.Item("realisasino")
        KdRealisasi.Text = KdRealisasiGV.SelectedDataKey.Item("realisasioid")
        KdRealisasiGV.Visible = False
        ResetReport()
    End Sub

    Protected Sub EraseKdRealisasi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtKdRealisasi.Text = "" : KdRealisasi.Text = ""
        ResetReport()
        KdRealisasiGV.Visible = False
    End Sub

    Protected Sub KdRealisasiGV_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        KdRealisasiGV.PageIndex = e.NewPageIndex
        BindKdRealisasi()
    End Sub
#End Region

    Protected Sub imgSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSupplier()
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtSupplier.Text = gvSupplier.SelectedDataKey.Item("suppname")
        IdSupplier.Text = gvSupplier.SelectedDataKey.Item("suppoid")
        gvSupplier.Visible = False
        ResetReport()
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvSupplier.PageIndex = e.NewPageIndex
        BindSupplier()
    End Sub

    Protected Sub eraseSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtSupplier.Text = "" : IdSupplier.Text = ""
        ResetReport()
        gvSupplier.Visible = False
    End Sub
End Class
