<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="GiroPaymentIn.aspx.vb" Inherits="Accounting_GiroPaymentIn" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Pencairan Giro Masuk"></asp:Label></th>
        </tr>
    </table>
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%" TabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Daftar Pencairan Giro Masuk</span></strong>
                            <strong><span style="font-size: 9pt">:. </span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel3" runat="server" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD align=left>Cabang</TD><TD align=left colSpan=4><asp:DropDownList id="fddlCabang" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w1"></asp:DropDownList></TD></TR><TR><TD align=left>Filter</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" Width="95px" CssClass="inpText" __designer:wfdid="w2"><asp:ListItem Value="gpd.nogiro">No Giro</asp:ListItem>
<asp:ListItem Value="gpm.gironote">Catatan</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="nobukti" runat="server" Width="118px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox></TD></TR><TR><TD align=left>Periode</TD><TD align=left colSpan=4><asp:TextBox id="txtPeriode1" runat="server" Width="80px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label21" runat="server" Font-Size="X-Small" Text="to"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="80px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; </TD></TR><TR><TD align=left>Status</TD><TD align=left colSpan=4><asp:DropDownList id="FilterStatus" runat="server" Width="75px" CssClass="inpText">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem Text="POST" Value="POST"></asp:ListItem>
                                                            <asp:ListItem>In Process</asp:ListItem>
                                                        </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD align=left>No.Akun&nbsp;(Bank)<ajaxToolkit:CalendarExtender id="ce4" runat="server" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy" TargetControlID="txtperiode1"></ajaxToolkit:CalendarExtender> </TD><TD align=left colSpan=4><asp:DropDownList id="coafilter" runat="server" CssClass="inpText"></asp:DropDownList>&nbsp;<asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <ajaxToolkit:MaskedEditExtender id="meece5" runat="server" TargetControlID="txtPeriode2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce5" runat="server" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy" TargetControlID="txtperiode2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meece4" runat="server" TargetControlID="txtPeriode1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="GVmst" runat="server" Width="100%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="GiroPaymentMstOid" GridLines="None" PageSize="8" AllowPaging="True" EmptyDataText="No data in database.">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="GiroPaymentMstOid" DataNavigateUrlFormatString="GiroPaymentIN.aspx?oid={0}" DataTextField="cashbankno" HeaderText="Cash/Bank No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="ExecutionDate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="perkiraan" HeaderText="Bank-Akun(COA)">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gironote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GiroAmt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GiroPaymentStatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<%--                                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick='<%# ConfirmPrint(Container.DataItem("cashbankno")) %>'>Print</asp:LinkButton>
                                                                        --%><asp:ImageButton id="imbPrintFormList" onclick="imbPrintFormList_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" __designer:wfdid="w1" ToolTip='<%# Eval("cashbankno") %>' CommandArgument='<%# Eval("GiroPaymentMstOid") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="lblmsg" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label>                                                         
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="Label13" runat="server" Font-Size="Small" Font-Bold="True" Text="Grand Total : Rp. " Visible="False"></asp:Label>&nbsp;<asp:Label id="lblgrandtotal" runat="server" Font-Size="Small" Font-Bold="True" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="GVmst"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w65" ActiveViewIndex="0"><asp:View id="View1" runat="server" __designer:wfdid="w66"><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Informasi" __designer:wfdid="w67"></asp:Label> <BR /><TABLE><TBODY><TR><TD align=left colSpan=2><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w68" TargetControlID="paymentdate" Format="dd/MM/yyyy" PopupButtonID="btnPaydate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meece8" runat="server" __designer:wfdid="w69" TargetControlID="payduedate" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD><TD align=left colSpan=2><asp:HiddenField id="HiddenField1" runat="server" __designer:wfdid="w70"></asp:HiddenField> </TD><TD align=left colSpan=2><asp:HiddenField id="HiddenField2" runat="server" __designer:wfdid="w71"></asp:HiddenField> <ajaxToolkit:MaskedEditExtender id="meece3" runat="server" __designer:wfdid="w72" TargetControlID="paymentdate" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Cabang</TD><TD style="WIDTH: 303px" align=left><asp:DropDownList id="ddlBranch" runat="server" CssClass="inpText" __designer:wfdid="w73"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD style="FONT-SIZE: x-small" align=left colSpan=3></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Tanggal</TD><TD style="WIDTH: 303px" align=left><asp:TextBox id="paymentdate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w74" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="btnPaydate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w75"></asp:ImageButton> <asp:Label id="Label8" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w76"></asp:Label> <asp:Label id="CutofDate" runat="server" __designer:wfdid="w77" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblCashBank" runat="server" Text="Bank" __designer:wfdid="w78"></asp:Label>&nbsp;Akun</TD><TD style="FONT-SIZE: x-small" align=left colSpan=3><asp:DropDownList id="cashbankacctgoid" runat="server" Width="366px" CssClass="inpText" __designer:wfdid="w79"></asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Total </TD><TD style="WIDTH: 303px" align=left><asp:TextBox id="totalreceipt" runat="server" Width="102px" CssClass="inpTextDisabled" __designer:wfdid="w80" ReadOnly="True">0.0000</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Catatan</TD><TD align=left colSpan=3><asp:TextBox id="cashbanknote" runat="server" Width="363px" Height="31px" CssClass="inpText" __designer:wfdid="w81" MaxLength="200" TextMode="MultiLine"></asp:TextBox>&nbsp;</TD></TR><TR><TD id="TD3" align=left runat="server" visible="false">Rate<asp:Label id="lblPOST" runat="server" __designer:wfdid="w82" Visible="False"></asp:Label></TD><TD style="WIDTH: 303px" id="TD5" align=left runat="server" visible="false"><asp:TextBox id="cashbankcurrate" runat="server" Width="24px" CssClass="inpText" __designer:wfdid="w83" Visible="False">1</asp:TextBox> <asp:DropDownList id="cashbankcurroid" runat="server" CssClass="inpText" __designer:wfdid="w84" OnSelectedIndexChanged="cashbankcurroid_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD id="TD4" align=left runat="server" visible="false">Tipe Giro</TD><TD style="WIDTH: 239px" id="TD6" align=left runat="server" visible="false"><asp:DropDownList id="ddlGiroType" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w85" OnSelectedIndexChanged="payflag_SelectedIndexChanged" Enabled="False">
                                                                <asp:ListItem Value="IN">IN</asp:ListItem>
                                                                <asp:ListItem>OUT</asp:ListItem>
                                                            </asp:DropDownList></TD><TD style="WIDTH: 72px" id="TD1" align=left runat="server" visible="false"><asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Data" __designer:wfdid="w86" Visible="False"></asp:Label></TD><TD id="TD2" align=left runat="server" visible="false"><asp:Label id="cashbankno" runat="server" Width="27px" __designer:wfdid="w87" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 15px" id="TD7" align=left runat="server" visible="false"></TD><TD style="WIDTH: 303px; HEIGHT: 15px" id="TD12" align=left runat="server" visible="false"><asp:Label id="bankacctgoid" runat="server" __designer:wfdid="w88" Visible="False"></asp:Label> <asp:Label id="suppoid" runat="server" __designer:wfdid="w89" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" id="TD11" align=left runat="server" visible="false"><asp:DropDownList id="payflag" runat="server" CssClass="inpText" __designer:wfdid="w90" Visible="False" OnSelectedIndexChanged="payflag_SelectedIndexChanged" Enabled="False"><asp:ListItem>NONCASH</asp:ListItem>
<asp:ListItem Value="CASH">CASH</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 239px; HEIGHT: 15px" id="TD8" align=left runat="server" visible="false"><asp:Label id="cashbankoid" runat="server" __designer:wfdid="w91" Visible="False"></asp:Label></TD><TD style="WIDTH: 72px; HEIGHT: 15px" id="TD9" align=left runat="server" visible="false"><asp:Label id="i_u2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" __designer:wfdid="w92" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" id="TD10" align=left runat="server" visible="false"><asp:Label id="giroacctgoid" runat="server" __designer:wfdid="w93" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: x-small; HEIGHT: 24px; TEXT-DECORATION: underline" align=left colSpan=2><asp:Label id="Label3" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail" __designer:wfdid="w94"></asp:Label> <asp:Label id="GiroPaymentOid" runat="server" Width="50px" __designer:wfdid="w95" Visible="False"></asp:Label></TD><TD style="HEIGHT: 24px" align=left>&nbsp;</TD><TD style="WIDTH: 239px; HEIGHT: 24px" align=left><asp:Label id="payseq" runat="server" __designer:wfdid="w96" Visible="False"></asp:Label> <asp:Label id="paymentoid" runat="server" __designer:wfdid="w97" Visible="False"></asp:Label></TD><TD style="WIDTH: 72px; HEIGHT: 24px" align=left><asp:Label id="GiroType" runat="server" __designer:wfdid="w98"></asp:Label></TD><TD style="HEIGHT: 24px" align=left><asp:Label id="paymentref_table" runat="server" __designer:wfdid="w99" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Giro&nbsp;No <asp:Label id="Label12" runat="server" CssClass="Important" Text="*" __designer:wfdid="w100"></asp:Label></TD><TD style="WIDTH: 303px" align=left><asp:TextBox id="payrefno" runat="server" Width="251px" CssClass="inpTextDisabled" __designer:wfdid="w101" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbInvoice" onclick="ImageButton8_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w102"></asp:ImageButton> <asp:ImageButton id="imbClearGiro" onclick="imbClearInv_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w103"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblDueDate" runat="server" Width="84px" Font-Size="X-Small" Text="Jatuh Tempo" __designer:wfdid="w104"></asp:Label></TD><TD style="WIDTH: 239px" align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w105" ReadOnly="True"></asp:TextBox> <asp:Label id="lblNotice" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w106" Visible="False"></asp:Label> </TD><TD style="FONT-SIZE: x-small; WIDTH: 72px" align=left><asp:Label id="Label5" runat="server" Width="84px" Font-Size="X-Small" Text="No.Dokumen" __designer:wfdid="w107"></asp:Label></TD><TD align=left><asp:TextBox id="document" runat="server" Width="127px" CssClass="inpTextDisabled" __designer:wfdid="w108" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Bank</TD><TD style="WIDTH: 303px" align=left><asp:TextBox id="bank" runat="server" Width="251px" CssClass="inpTextDisabled" __designer:wfdid="w109" Enabled="False"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Customer /<BR />Supplier(R.DP.AP)</TD><TD style="WIDTH: 239px" align=left><asp:TextBox id="custname" runat="server" Width="187px" CssClass="inpTextDisabled" __designer:wfdid="w110" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small; WIDTH: 72px" align=left>Total</TD><TD align=left><asp:TextBox id="payamt" runat="server" Width="128px" CssClass="inpTextDisabled" __designer:wfdid="w111" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Note Detail</TD><TD align=left colSpan=3><asp:TextBox id="paynote" runat="server" Width="375px" Height="31px" CssClass="inpText" __designer:wfdid="w112" MaxLength="90" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:Label id="GIRODTLOID" runat="server" Width="84px" Font-Size="X-Small" __designer:wfdid="w113" Visible="False"></asp:Label></TD><TD align=left colSpan=2><asp:ImageButton id="imbAddReceipt" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w114"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearReceipt" onclick="imbClearReceipt_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w115"></asp:ImageButton></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 54px; wifth: 990px" align=left colSpan=6><FIELDSET style="HEIGHT: 170px" id="Fieldset1"><DIV id="Div1"></DIV><TABLE id="dtltb" class="gvhdr" width="100%" runat="server" Visible="false"><TBODY><TR><TD style="WIDTH: 55px"></TD><TD style="WIDTH: 46px">No</TD><TD style="WIDTH: 123px">No. Giro</TD><TD style="WIDTH: 62px">Bank</TD><TD style="WIDTH: 90px" align=left>No. Dokumen</TD><TD style="WIDTH: 152px">Customer</TD><TD style="WIDTH: 128px">Jatuh Tempo</TD><TD style="WIDTH: 69px">Total</TD><TD colSpan=2>Catatan</TD></TR></TBODY></TABLE><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 86%; BACKGROUND-COLOR: beige"><asp:GridView id="gvReceiptDtl" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w116" GridLines="None" DataKeyNames="paymentoid,payrefno,custname,payamt,paynote,payduedate,payseq,payacctgoid,paymentref_table,document,custoid,bank,girodtloid" AutoGenerateColumns="False" CellPadding="4" OnSelectedIndexChanged="gvReceiptDtl_SelectedIndexChanged" OnRowDataBound="gvReceiptDtl_RowDataBound" ShowHeader="False" OnRowDeleting="gvReceiptDtl_RowDeleting">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payseq" HeaderText="No ">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="40px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payrefno" HeaderText="No.Giro">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bank" HeaderText="Bank">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="document" HeaderText="No.Dokumen">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Cust./Supp(R.DP)">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="70px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Total giro">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paynote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="Label8" runat="server" Text="Tidak ada detail giro !!" CssClass="Important"></asp:Label>
                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR></TBODY></TABLE></asp:View> </asp:MultiView><TABLE width="100%"><TBODY><TR><TD colSpan=4><SPAN style="FONT-SIZE: x-small; COLOR: #585858">Last update on <asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w117"></asp:Label> by <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w118"></asp:Label></SPAN></TD></TR><TR><TD colSpan=4><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:wfdid="w119"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" __designer:wfdid="w120"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w121"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:wfdid="w122"></asp:ImageButton> <asp:ImageButton id="imbPrintNota" onclick="imbPrintNota_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w123" Visible="False"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w124" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="imbPrintNota"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                Form Pencairan Giro Masuk</span></strong>&nbsp;<strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel ID="upMsgbox" runat="server">
                    <ContentTemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK" Visible="False"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                            <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px; HEIGHT: 18px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD style="HEIGHT: 18px" vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelGiro" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 495px; TEXT-ALIGN: center" align=left><asp:Label id="lblGiroData" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Akun Giro" __designer:wfdid="w12"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 495px; TEXT-ALIGN: center" align=left>Filter&nbsp;:<asp:DropDownList id="ddlgirofilter" runat="server" Width="94px" CssClass="inpText" __designer:wfdid="w13">
                                <asp:ListItem Value="payrefno">Giro No.</asp:ListItem>
                                <asp:ListItem Value="custname">Supplier</asp:ListItem>
                                <asp:ListItem Value="isnull(Bank,'')">Bank</asp:ListItem>
                                <asp:ListItem Value="document">Document</asp:ListItem>
                            </asp:DropDownList> <asp:TextBox id="tbGiro" runat="server" Width="222px" CssClass="inpText" __designer:wfdid="w14"></asp:TextBox>&nbsp; <asp:ImageButton id="btnFindInvoice" onclick="btnFindInvoice_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w15"></asp:ImageButton> <asp:ImageButton id="imbViewAllInv" onclick="imbViewAllInv_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w16" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 495px" align=center><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div5"></DIV><TABLE class="gvhdr" width="98%"><TBODY><TR><TD style="WIDTH: 53px"></TD><TD style="WIDTH: 152px">No. Giro</TD><TD style="WIDTH: 134px">Customer</TD><TD style="WIDTH: 151px">No. Dokumen</TD><TD style="WIDTH: 136px" align=right>Jatuh Tempo</TD><TD style="WIDTH: 131px" align=right>Total Giro</TD><TD style="WIDTH: 66px" align=center>Bank</TD></TR></TBODY></TABLE><DIV style="OVERFLOW-Y: scroll; WIDTH: 98%; HEIGHT: 78%; BACKGROUND-COLOR: beige"><asp:GridView style="Z-INDEX: 100; LEFT: -1px; TOP: 128px; BACKGROUND-COLOR: transparent" id="gvGiro" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w17" PageSize="6" GridLines="None" DataKeyNames="cashbankoid,NoGiro,custname,payduedate,payamt,payacctgoid,paymentref_table,suppoid,document,bankacctgoid,bank,girodtloid" AutoGenerateColumns="False" CellPadding="4" OnSelectedIndexChanged="gvGiro_SelectedIndexChanged" OnRowDataBound="gvGiro_RowDataBound" AllowSorting="True" EmptyDataRowStyle-ForeColor="Red" ShowHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="NoGiro" HeaderText="No.Giro" SortExpression="NoGiro">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer/Supp(R.DP)">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="document" HeaderText="No.Dokumen">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo" SortExpression="payduedate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Total Giro" SortExpression="payamt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payacctgoid" HeaderText="payacctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="bank" HeaderText="Bank">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada data AR !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="WIDTH: 495px; TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSales" onclick="CloseSales_Click" runat="server" CausesValidation="False" Font-Size="Small" Font-Bold="True" __designer:wfdid="w18">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenGiro" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderGiro" runat="server" TargetControlID="btnHiddenGiro" PopupDragHandleControlID="lblGiroData" PopupControlID="PanelGiro" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
