<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPinjamBarang.aspx.vb" Inherits="PinjamBarang" title="MSC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 976px; height: 1px" class="header">
        <tr>
            <td align="left" style="height: 15px; background-color: silver; width: 337px;">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Navy" Text=".: Peminjaman Barang"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel3" runat="server">
                    <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w37" DefaultButton="btnFind"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 168px">Cabang</TD><TD>:</TD><TD><asp:DropDownList id="fCabangDDL" runat="server" CssClass="inpText" __designer:wfdid="w68"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 168px">Filter&nbsp; </TD><TD>:</TD><TD><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w38"><asp:ListItem Value="trnpinjamno">No Pinjam</asp:ListItem>
<asp:ListItem Value="namapeminjam">Nama Peminjam</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" __designer:wfdid="w39"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 168px"><asp:CheckBox id="checkPeriod" runat="server" Text="Periode" __designer:wfdid="w40"></asp:CheckBox> </TD><TD>:</TD><TD style="WIDTH: 1264px"><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" __designer:wfdid="w41"></asp:TextBox> &nbsp;<asp:ImageButton id="ibPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w42"></asp:ImageButton> to&nbsp;<asp:TextBox id="txtPeriod2" runat="server" CssClass="inpText" __designer:wfdid="w43"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton id="ibPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton>&nbsp; <asp:Label id="Label2" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w45"></asp:Label></TD></TR><TR><TD style="WIDTH: 168px"><asp:CheckBox id="checkStatus" runat="server" Text="Status" __designer:wfdid="w46"></asp:CheckBox> </TD><TD>:</TD><TD style="WIDTH: 1264px"><asp:DropDownList id="StatusDDL" runat="server" CssClass="inpText" __designer:wfdid="w47"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w48"></asp:ImageButton> <asp:ImageButton id="btnView" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w49"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w50" Visible="False"></asp:ImageButton></TD></TR><TR><TD colSpan=3><asp:GridView style="max-width: 940px" id="gvPenerimaan" runat="server" Width="940px" ForeColor="#333333" __designer:wfdid="w18" OnSelectedIndexChanged="gvPenerimaan_SelectedIndexChanged" AllowPaging="True" GridLines="None" CellPadding="4" DataKeyNames="trnpinjammstoid,trnpinjamno,branch_code" AutoGenerateColumns="False" PageSize="8" AllowSorting="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnpinjamno" HeaderText="No Pinjam">
<FooterStyle BackColor="DodgerBlue" BorderColor="DodgerBlue"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndatepinjam" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="namapeminjam" HeaderText="Nama">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="lbPrint" onclick="lbPrint_Click" runat="server" ToolTip='<%# Eval("trnpinjamno") %>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Width="65px"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label10" runat="server" ForeColor="Red" Text="Data Not Found"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w52" Enabled="True" TargetControlID="FilterPeriod1" Format="dd/MM/yyyy" PopupButtonID="ibPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w53" Enabled="True" TargetControlID="txtPeriod2" Format="dd/MM/yyyy" PopupButtonID="ibPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w54" Enabled="True" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder=""></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w55" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD colSpan=3><asp:UpdatePanel id="UpdatePanelValidasi" runat="server" __designer:wfdid="w56"><ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w57"><TABLE><TBODY><TR><TD style="HEIGHT: 18px; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: center" colSpan=2><asp:Label id="Label15" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w58">Print Option</asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Label id="Label34" runat="server" Text="Printer :" __designer:wfdid="w59"></asp:Label></TD><TD style="WIDTH: 8px; TEXT-ALIGN: left" class="Label"><asp:DropDownList id="ddlprinter" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w60"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2><asp:Label id="OtherTemp" runat="server" Visible="False" __designer:wfdid="w61"></asp:Label> <asp:Label id="NoTemp" runat="server" Visible="False" __designer:wfdid="w62"></asp:Label> <asp:Label id="idTemp" runat="server" Visible="False" __designer:wfdid="w63"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btpPrintAction" onclick="btpPrintAction_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w64"></asp:ImageButton> <asp:ImageButton id="btncloseprint" onclick="btncloseprint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w65"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <BR /><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" DropShadow="True" BackgroundCssClass="modalBackground" PopupControlID="Panelvalidasi" PopupDragHandleControlID="label15" Drag="True" __designer:wfdid="w66"></ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w67"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btpPrintAction"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="gvPenerimaan"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
                &nbsp;&nbsp;
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <strong><span style="font-size: 9pt">.:</span></strong> <strong><span style="font-size: 9pt"> List Of Peminjaman Barang</span></strong>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE><TBODY><TR><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="ddlFromcabang" runat="server" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w1"></asp:DropDownList></TD><TD colSpan=3></TD><TD></TD><TD colSpan=2></TD></TR><TR><TD>No. Tanda Terima</TD><TD>:</TD><TD><asp:TextBox id="txtNoTanda" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w2" ReadOnly="True" MaxLength="15"></asp:TextBox> <asp:Label id="PinjamOid" runat="server" Text="Label" __designer:wfdid="w3" Visible="False"></asp:Label> <asp:Label id="lbloid" runat="server" __designer:wfdid="w4" Visible="False"></asp:Label> <asp:Label id="i_u" runat="server" ForeColor="Red" __designer:wfdid="w5" Visible="False"></asp:Label></TD><TD colSpan=3>Tanggal Pinjam</TD><TD>:</TD><TD colSpan=2><asp:TextBox id="txtTglTerima" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w6" ReadOnly="True"></asp:TextBox>&nbsp;<asp:Label id="Label7" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD>Peminjam<asp:Label id="Label3" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w8"></asp:Label></TD><TD style="WIDTH: 3px">:</TD><TD><asp:TextBox id="txtNamaCust" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w9" MaxLength="100"></asp:TextBox> <asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w10" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnErase" onclick="btnErase_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w11" Visible="False"></asp:ImageButton> <asp:Label id="Label4" runat="server" __designer:wfdid="w12" Visible="False"></asp:Label></TD><TD colSpan=3>Status</TD><TD style="WIDTH: 8px">:</TD><TD colSpan=2><asp:TextBox id="txtStatus" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w13" ReadOnly="True" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 28px">Keterangan</TD><TD style="WIDTH: 3px; HEIGHT: 28px">:</TD><TD style="HEIGHT: 28px" colSpan=7><asp:TextBox id="txtDetailPinjam" runat="server" Width="288px" Height="20px" CssClass="inpText" Font-Size="Medium" __designer:wfdid="w14" MaxLength="150" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label8" runat="server" Width="128px" ForeColor="Red" Text="maks. 150 karakter" __designer:wfdid="w15"></asp:Label></TD></TR><TR><TD colSpan=9><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Detail :" __designer:wfdid="w16" Font-Underline="True"></asp:Label>&nbsp; <asp:Label id="dtlseq" runat="server" __designer:wfdid="w17" Visible="False"></asp:Label>&nbsp;<asp:Label id="dtlrow" runat="server" __designer:wfdid="w18" Visible="False"></asp:Label>&nbsp;<asp:Label id="lblNo" runat="server" __designer:wfdid="w19" Visible="False"></asp:Label><asp:Label id="lblUpdNo" runat="server" __designer:wfdid="w20" Visible="False"></asp:Label>&nbsp;<asp:Label id="iuAdd" runat="server" __designer:wfdid="w21" Visible="False"></asp:Label>&nbsp;<asp:Label id="i_u2" runat="server" ForeColor="Red" __designer:wfdid="w22" Visible="False"></asp:Label>&nbsp;<asp:Label id="unitno" runat="server" __designer:wfdid="w23" Visible="False">0</asp:Label></TD></TR><TR><TD id="TD1" visible="true">Type&nbsp;Barang</TD><TD style="WIDTH: 3px" id="TD3" visible="true">:</TD><TD id="TD2" colSpan=7 visible="true"><asp:DropDownList id="JenisBarangDDL" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w24"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD><asp:Label id="Location" runat="server" __designer:wfdid="w25">From Location</asp:Label></TD><TD style="WIDTH: 3px"><asp:Label id="LblTitik" runat="server" __designer:wfdid="w26">:</asp:Label></TD><TD colSpan=7><asp:DropDownList id="DDLLoc" runat="server" CssClass="inpText" __designer:wfdid="w27"></asp:DropDownList></TD></TR><TR><TD>Katalog</TD><TD>:</TD><TD colSpan=7><asp:TextBox id="txtBarang" runat="server" Width="304px" CssClass="inpText" __designer:wfdid="w28" MaxLength="50"></asp:TextBox>&nbsp;<asp:ImageButton id="sBarang" onclick="sBarang_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton>&nbsp;<asp:ImageButton id="eBarang" onclick="btnErase_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w30"></asp:ImageButton> <asp:Label id="itemoid" runat="server" __designer:wfdid="w31" Visible="False"></asp:Label>&nbsp;<asp:Label id="periodacctg" runat="server" __designer:wfdid="w32" Visible="False"></asp:Label>&nbsp;<asp:Label id="trnpinjamdtloid" runat="server" __designer:wfdid="w33" Visible="False"></asp:Label>&nbsp;<asp:Label id="laststok" runat="server" __designer:wfdid="w34"></asp:Label>&nbsp;<asp:Label id="matcode" runat="server" __designer:wfdid="w35" Visible="False"></asp:Label>&nbsp;<asp:Label id="mtrlocoid" runat="server" __designer:wfdid="w36"></asp:Label></TD></TR><TR><TD><asp:RadioButtonList id="rbJenisBarang" runat="server" AutoPostBack="True" __designer:wfdid="w37" Visible="False" Enabled="False" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbJenisBarang_SelectedIndexChanged"><asp:ListItem Selected="True" Value="1">Inventory</asp:ListItem>
</asp:RadioButtonList></TD><TD></TD><TD colSpan=7><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvMaterial" runat="server" Width="71%" ForeColor="#333333" __designer:wfdid="w38" Visible="False" OnSelectedIndexChanged="gvMaterial_SelectedIndexChanged" EmptyDataRowStyle-ForeColor="Red" OnPageIndexChanging="gvMaterial_PageIndexChanging" AllowPaging="True" GridLines="None" CellPadding="4" DataKeyNames="matoid,matlongdesc,satuan2,saldoakhir,matcode,stockflag" AutoGenerateColumns="False" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Last Stok">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stok" HeaderText="Stok" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="safetystok" HeaderText="SafetyStock" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="JenisNya">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockflag" HeaderText="stockflag" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Item data found!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD>Quantity</TD><TD style="WIDTH: 3px">:</TD><TD colSpan=7><asp:TextBox id="trnpinjamdtlqty" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w39" MaxLength="15">0</asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ItemQtyFTE" runat="server" __designer:wfdid="w43" ValidChars="1234567890.," TargetControlID="trnpinjamdtlqty"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD colSpan=9><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton></TD></TR><TR><TD colSpan=9><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 184px; BACKGROUND-COLOR: beige"><asp:GridView id="gvPinjamBarang" runat="server" Width="100%" Height="40px" ForeColor="#333333" __designer:wfdid="w46" GridLines="None" CellPadding="4" DataKeyNames="sequence" AutoGenerateColumns="False" PageSize="2">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="10%"></HeaderStyle>

<ItemStyle BorderColor="Blue" Font-Bold="True" Width="10%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sequence" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="20%"></HeaderStyle>

<ItemStyle BorderColor="Blue" Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="60%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" BorderColor="Blue" Width="60%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="unitoid" HeaderText="unitoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="laststok" Visible="False"></asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="matcode" Visible="False"></asp:BoundField>
<asp:BoundField DataField="flagbarang" HeaderText="flagbarang" Visible="False"></asp:BoundField>
<asp:BoundField DataField="mtrlocoid" HeaderText="mtrlocoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="stockflag" HeaderText="stockflag" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp;</DIV></TD></TR><TR><TD colSpan=9><asp:Label id="lblUpd" runat="server" __designer:wfdid="w47"></asp:Label>&nbsp;On <asp:Label id="UpdTime" runat="server" Font-Bold="True" __designer:wfdid="w48"></asp:Label>&nbsp;By <asp:Label id="UpdUser" runat="server" Font-Bold="True" __designer:wfdid="w49"></asp:Label></TD></TR><TR><TD colSpan=9><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w51"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w52"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w53"></asp:ImageButton><asp:ImageButton id="printmanualpenerimaan" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w54" Visible="False"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=9><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w55" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w56"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE><DIV>&nbsp;</DIV>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="printmanualpenerimaan"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <strong><span style="font-size: 9pt">.: <span>
                Form Peminjaman Barang</span></span></strong>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel2" runat="server">
        <contenttemplate>
<asp:Panel id="panelMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeValidasi" runat="server" TargetControlID="btnValidasi" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnValidasi" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

