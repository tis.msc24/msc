<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnDPAP.aspx.vb" Inherits="Accounting_DownPaymentAR" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="Blue" Text=".: Down Payment A/P" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            &nbsp;<strong><span style="font-size: 9pt">List of Down Payment A/P :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w28" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w29"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=3><asp:DropDownList id="FilterDDL" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w30">
                                                            <asp:ListItem Value="dp.trndpapoid">Draft No.</asp:ListItem>
                                                            <asp:ListItem Value="trndpapno">DP No.</asp:ListItem>
                                                            <asp:ListItem Value="suppname">Supplier</asp:ListItem>
                                                            <asp:ListItem Value="a.acctgdesc">DP Account</asp:ListItem>
                                                            <asp:ListItem Value="trndpapnote">Note</asp:ListItem>
                                                        </asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w31"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w32"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w33"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w35"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w36"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton>&nbsp;<asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w38"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w39"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDLStatus" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w40">
                                                            <asp:ListItem>In Process</asp:ListItem>
                                                            <asp:ListItem>Post</asp:ListItem>
                                                        </asp:DropDownList></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w45" AlternateText="Find"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w46" AlternateText="View All"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w47" Visible="False" AlternateText="Print"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=5><asp:LinkButton id="lbInProcess" runat="server" __designer:wfdid="w48"></asp:LinkButton></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="gvTRN" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w23" PageSize="8" GridLines="None" DataKeyNames="trndpaPoid" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" EnableModelValidation="True" AllowSorting="True" OnSelectedIndexChanged="gvTRN_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trndpapoid" DataNavigateUrlFormatString="~\Accounting\trnDPAP.aspx?oid={0}" DataTextField="trndpapoid" HeaderText="Draft No." SortExpression="trndpapoid">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trndpapno" HeaderText="DP No." SortExpression="trndpapno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cbno" HeaderText="No. Cash/Bank">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="dpapdate" HeaderText="DP Date" SortExpression="dpapdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="DP Account" SortExpression="acctgdesc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpappaytype" HeaderText="Pay. Type" SortExpression="dpappaytype">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndpapstatus" HeaderText="Status" SortExpression="trndpapstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndpapnote" HeaderText="Note" SortExpression="trndpapnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
&nbsp; <asp:ImageButton style="HEIGHT: 16px" id="imbprintdpar" onclick="imbprintdpar_Click" runat="server" ImageUrl="~/Images/print.gif" __designer:wfdid="w1"></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!"></asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w50"></asp:Label></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w51" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN><asp:Image id="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w52"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w53" Format="dd/MM/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w54" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w55" Format="dd/MM/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w56" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="gvTRN"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w127"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w128" Visible="False"></asp:Label> <asp:Label id="custoid" runat="server" __designer:wfdid="w129" Visible="False"></asp:Label> </TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="CutofDate" runat="server" __designer:wfdid="w130" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="cashbankoid" runat="server" __designer:wfdid="w131" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Divisi" __designer:wfdid="w132" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" Width="205px" CssClass="inpTextDisabled" __designer:wfdid="w133" Visible="False" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No." __designer:wfdid="w134"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="dparoid" runat="server" __designer:wfdid="w135"></asp:Label> <asp:TextBox id="dparno" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w136" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Supplier" __designer:wfdid="w137"></asp:Label> <asp:Label id="Label17" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w138"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custname" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w139" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w140"></asp:ImageButton> <asp:ImageButton id="btnClearCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w141"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Cash/Bank No." __designer:wfdid="w142"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankno" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w143" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Width="51px" Text="Paytype" __designer:wfdid="w144"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="dparpaytype" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w145" AutoPostBack="True"><asp:ListItem Value="BKK">CASH</asp:ListItem>
<asp:ListItem Value="BBK">BANK</asp:ListItem>
<asp:ListItem Value="BGK">GIRO/CHEQUE</asp:ListItem>
<asp:ListItem Enabled="False" Value="DPP">DOWN PAYMENT</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="DP Date" __designer:wfdid="w146"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dpardate" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w147" AutoPostBack="True" ToolTip="DD/MM/YYYY" EnableTheming="True"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w148"></asp:ImageButton> <asp:Label id="Label7" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w149"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Currency" __designer:wfdid="w150"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w151"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lblRefNo" runat="server" Text="Ref. No." __designer:wfdid="w152"></asp:Label> <asp:Label id="lblWarnRefNo" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w153" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparpayrefno" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w154" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Width="97px" Text="Pay Account" __designer:wfdid="w155"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="dparpayacctgoid" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w156" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="DP Account" __designer:wfdid="w157"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="acctgoid" runat="server" Width="288px" CssClass="inpText" __designer:wfdid="w158"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblDueDate" runat="server" Text="Due Date" __designer:wfdid="w159"></asp:Label> <asp:Label id="lblWarnDueDate" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w160" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSeptDueDate" runat="server" Text=":" __designer:wfdid="w161"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="dparduedate" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w162" ToolTip="DD/MM/YYYY"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w163"></asp:ImageButton> <asp:Label id="lblInfoDueDate" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w164"></asp:Label></TD><TD class="Label" align=left><asp:Label id="lblDTG" runat="server" Width="87px" Text="Date Take Giro" __designer:wfdid="w165" Visible="False"></asp:Label> <asp:Label id="lblWarnDTG" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w166" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSeptDTG" runat="server" Text=":" __designer:wfdid="w167"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="dpartakegiro" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w168" Visible="False" ToolTip="DD/MM/YYYY"></asp:TextBox> <asp:ImageButton id="imbDTG" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w169" Visible="False"></asp:ImageButton> <asp:Label id="lblInfoDTG" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w170" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="DP Amount" __designer:wfdid="w171"></asp:Label> <asp:Label id="Label22" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w172"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparamt" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w173" AutoPostBack="True" MaxLength="12"></asp:TextBox></TD><TD id="TD21" class="Label" align=left Visible="false"><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w174"></asp:Label></TD><TD id="TD19" class="Label" align=center Visible="false">:</TD><TD id="TD20" class="Label" align=left Visible="false"><asp:TextBox id="dparstatus" runat="server" Width="80px" CssClass="inpTextDisabled" __designer:wfdid="w175" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Note" __designer:wfdid="w176"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparnote" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w177" MaxLength="100"></asp:TextBox></TD><TD id="TD24" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label20" runat="server" Width="96px" Text="Total Cash/Bank" __designer:wfdid="w178"></asp:Label></TD><TD id="TD23" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD22" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="dparnett" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w179" Enabled="False"></asp:TextBox></TD></TR><TR><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label11" runat="server" Text="Add Cost 1" __designer:wfdid="w180" Visible="False"></asp:Label></TD><TD id="TD4" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="addacctgoid1" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w181" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD id="TD16" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label21" runat="server" Text="Cost Amt 1" __designer:wfdid="w182" Visible="False"></asp:Label></TD><TD id="TD17" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD7" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="addacctgamt1" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w183" Visible="False" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD id="TD11" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label14" runat="server" Text="Add Cost 2" __designer:wfdid="w184" Visible="False"></asp:Label></TD><TD id="TD12" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD6" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="addacctgoid2" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w185" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD id="TD8" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label23" runat="server" Text="Cost Amt 2" __designer:wfdid="w186" Visible="False"></asp:Label></TD><TD id="TD10" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD18" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="addacctgamt2" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w187" Visible="False" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD id="TD15" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label18" runat="server" Text="Add Cost 3" __designer:wfdid="w188" Visible="False"></asp:Label></TD><TD id="TD14" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="addacctgoid3" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w189" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD id="TD9" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label24" runat="server" Text="Cost Amt 3" __designer:wfdid="w190" Visible="False"></asp:Label></TD><TD id="TD13" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD1" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="addacctgamt3" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w191" Visible="False" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w192" TargetControlID="dpardate" PopupButtonID="imbDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w193" TargetControlID="dpardate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDueDate" runat="server" __designer:wfdid="w194" TargetControlID="dparduedate" PopupButtonID="imbDueDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDueDate" runat="server" __designer:wfdid="w195" TargetControlID="dparduedate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbAmount" runat="server" __designer:wfdid="w196" TargetControlID="dparamt" ValidChars="1234567890,.">
                                                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:CalendarExtender id="ceDTG" runat="server" __designer:wfdid="w197" TargetControlID="dpartakegiro" PopupButtonID="imbDTG" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDTG" runat="server" __designer:wfdid="w198" TargetControlID="dpartakegiro" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear">
                                                    </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbamt1" runat="server" __designer:wfdid="w199" TargetControlID="addacctgamt1" ValidChars="1234567890,.-"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbamt2" runat="server" __designer:wfdid="w200" TargetControlID="addacctgamt2" ValidChars="1234567890,.-"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbamt3" runat="server" __designer:wfdid="w201" TargetControlID="addacctgamt3" ValidChars="1234567890,.-"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w202"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w203"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w204"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w205"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w206" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w207" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w208" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w209"></asp:ImageButton> <asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" __designer:wfdid="w210" AlternateText="Show COA"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w211" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w212"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Down Payment A/P :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="upListCust" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" __designer:wfdid="w18" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier" __designer:wfdid="w19"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" __designer:wfdid="w20" DefaultButton="btnFindListCust">
                                                Filter :
                                                <asp:DropDownList ID="FilterDDLListCust" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w21">
                                                    <asp:ListItem Value="suppcode">Code</asp:ListItem>
                                                    <asp:ListItem Value="suppname">Name</asp:ListItem>
                                                    <asp:ListItem Value="suppaddr">Address</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterTextListCust" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w22"></asp:TextBox>
                                                <asp:ImageButton ID="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w23"></asp:ImageButton>
                                                <asp:ImageButton ID="btnAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w24"></asp:ImageButton>
                                            </asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w25" EnableModelValidation="True" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="suppoid,suppname" GridLines="None" PageSize="5">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="Black"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListCust" runat="server" __designer:wfdid="w26">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" __designer:wfdid="w27" TargetControlID="btnHideListCust" PopupDragHandleControlID="lblListCust" PopupControlID="pnlListCust" Drag="True" BackgroundCssClass="modalBackground">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" __designer:wfdid="w28" Visible="False"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w29">
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                            <asp:Label ID="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True" __designer:wfdid="w30"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="HEIGHT: 10px" colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px" __designer:wfdid="w31"></asp:Image></td>
                                        <td style="TEXT-ALIGN: left" class="Label">
                                            <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w32"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td style="TEXT-ALIGN: center" colspan="2">&nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton></td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True" __designer:wfdid="w34"></ajaxToolkit:ModalPopupExtender>
                        <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w35"></asp:Button>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="upCOAPosting" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlCOAPosting" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w36" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblCOAPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting Rate :" __designer:wfdid="w37"></asp:Label> <asp:DropDownList id="DDLRateType" runat="server" Width="125px" CssClass="inpText" Font-Size="Medium" Font-Bold="True" __designer:wfdid="w38" Visible="False" AutoPostBack="True">
                                                <asp:ListItem Value="Default">Rate Default</asp:ListItem>
                                                <asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
                                                <asp:ListItem Value="USD">Rate To USD</asp:ListItem>
                                            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvCOAPosting" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w39" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Account No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdebet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Size="X-Small" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcredit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Size="X-Small" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseCOAPosting" runat="server" __designer:wfdid="w40">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideCOAPosting" runat="server" __designer:wfdid="w41" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeCOAPosting" runat="server" __designer:wfdid="w42" TargetControlID="btnHideCOAPosting" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlCOAPosting" PopupDragHandleControlID="lblCOAPosting">
                        </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

