'Prgmr:Bhie Nduuutt | LastUpdt:16.05.2013 | LastUpdt:28.02.2016
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports ClassFunction

Partial Class Accounting_trnConAR
    Inherits System.Web.UI.Page
#Region "Variabel"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public compnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cfunction As New ClassFunction
#End Region

#Region "Prosedure"

    Private Sub ReAmount()
        dpp.Text = ToMaskEdit(ToDouble(dpp.Text), 4)
        amttax.Text = ToMaskEdit(ToDouble(dpp.Text) * (ToDouble(trntaxpct.Text) / 100), 4)
        amtbeli.Text = ToMaskEdit(ToDouble(dpp.Text) + ToDouble(amttax.Text), 4)
        amtbayar.Text = ToMaskEdit(ToDouble(amtbayar.Text), 4)
        totalAP.Text = ToMaskEdit(ToDouble(amtbeli.Text) - ToDouble(amtbayar.Text), 4)
    End Sub

    Private Sub initDDL()
        ' Init Payment Term
        Dim sSql As String = "SELECT genoid, gendesc FROM ql_mstgen WHERE gengroup LIKE 'PAYTYPE' AND gendesc NOT LIKE 'Cash%'"
        FillDDL(trnpaytype, sSql)
        ' Init Currency
        sSql = "select currencyoid, currencycode  + '-' + currencydesc from QL_mstcurr where cmpcode='" & cmpcode & "'"
        FillDDL(curroid, sSql)
        FillRate(curroid.SelectedValue)
        Dim sCb As String = ""
        If Session("UserID") <> "admin" Or Session("branch_id") <> "01" Then
            sCb = "And gencode='" & Session("branch_id") & "'"
        End If
        sSql = "Select gencode,gendesc FROM QL_mstgen Where gengroup='CABANG' " & sCb & ""
        FillDDL(ddlCabang, sSql)
    End Sub

    Private Sub FillRate(ByVal iCurrOid As Integer)
        currate.Text = NewMaskEdit(ToDouble(cKon.ambilscalar("select top 1 rate2idrvalue from QL_mstrate2 where currencyoid=" & curroid.SelectedValue & " order by rate2date desc ")))

    End Sub

    Public Sub binddata(ByVal filterCompCode As String, ByVal filterName As String, ByVal filterNo As String)
        'Dim sqlSelect As String
        sSql = "SELECT trnbelimstoid, trnbelino, trnbelidate, trnsuppoid, suppname, payduedate, trnbelistatus, amttrans,trnbelinote From (SELECT B.trnjualmstoid trnbelimstoid,B.trnjualno trnbelino,B.trnjualdate trnbelidate,B.trncustoid trnsuppoid,S.custname suppname,Getdate() payduedate,B.trnjualstatus trnbelistatus,Isnull(B.trnamtjualnettoidr,0.00) amttrans,C.amtbayar,B.trnjualnote trnbelinote FROM ql_trnJUALmst B INNER JOIN QL_mstCUST S ON B.trnCUSToid = S.CUSToid LEFT JOIN ql_conaR C ON B.trnJUALmstoid = C.refoid AND C.reftype='ql_trnJUALmst' And c.conaroid < 0 LEFT JOIN ql_mstacctg a on a.acctgoid=c.acctgoid LEFT OUTER JOIN QL_trnpayaR P ON B.trnJUALmstoid = P.payrefoid AND P.payreftype = 'ql_trnJUALmst' WHERE B.cmpcode = '" & filterCompCode & "' AND S.CUSTname LIKE '%" & Tchar(filterName) & "%' AND B.trnJUALno LIKE '%" & Tchar(filterNo) & "%' AND B.trnJUALmstoid <= 0  and B.branch_code='" & Session("branch_id") & "')dt group by trnbelimstoid, trnbelino, trnbelidate, trnsuppoid, suppname, payduedate, trnbelistatus, amttrans, trnbelinote ORDER BY trnbelimstoid Asc"
        FillGV(gvTrnConap, sSql, "ql_trnjualmst")
    End Sub

    Public Sub fillTextBox(ByVal vID As String)
        Dim mySqlConn As New SqlConnection(ConnStr)
        sSql = "SELECT B.branch_code,B.trnjualmstoid,B.trncustoid,B.trnjualstatus,(Select custacctgoid FRom QL_mstcust c Where b.trncustoid=c.custoid) acctgoid,B.trnjualdate,B.trnjualno,B.trnpaytype,B.trnamtjual,B.trntaxpct,B.trnamttax,B.trnamtjualnetto,B.trnjualnote,C.conaroid,getDate() payduedate,ISNULL(P.paymentoid,0)paymentoid,ISNULL(C.payrefno,'') payrefno,ISNULL(C.amtbayar,0)amtbayar,B.upduser,B.updtime,ISNULL(C.paymentdate,'01/01/1900')paymentdate,ISNULL(C.acctgoid,0) paymentacctgoid,ISNULL(B.currencyoid,2) AS currencyoid,ISNULL(B.currencyrate,1) AS currencyrate FROM ql_trnjualmst B LEFT JOIN ql_conar C ON B.trnjualmstoid = C.refoid AND C.reftype='ql_trnjualmst' LEFT OUTER JOIN QL_trnpayar P ON B.trnjualmstoid = P.payrefoid AND P.payreftype='ql_trnjualmst' WHERE B.trnjualmstoid=" & vID & " AND B.cmpcode='" & cmpcode & "' And B.branch_code='" & Session("branch_id") & "' ORDER BY B.trnjualmstoid DESC"
        Dim mySqlDA As New SqlClient.SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet : Dim objTable As DataTable
        Dim objRow() As DataRow

        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length > 0 Then
            trnbelimstoid.Text = Trim(objRow(0)("trnjualmstoid").ToString)
            suppoid.Text = Trim(objRow(0)("trncustoid").ToString)
            lblPosting.Text = Trim(objRow(0)("trnjualstatus").ToString)
            acctgoid.Text = Trim(objRow(0)("acctgoid").ToString)
            invoicedate.Text = Format(CDate(Trim(objRow(0)("trnjualdate").ToString)), "dd/MM/yyyy")
            trnbelino.Text = (objRow(0)("trnjualno").ToString)
            curroid.SelectedValue = objRow(0)("currencyoid").ToString
            currate.Text = NewMaskEdit(ToDouble(objRow(0)("currencyrate").ToString))
            lblstatus.Visible = True
            trnpaytype.SelectedValue = Trim(objRow(0)("trnpaytype").ToString)
            dpp.Text = ToMaskEdit(Trim(objRow(0)("trnamtjual").ToString), 4)
            trntaxpct.Text = ToMaskEdit(Trim(objRow(0)("trntaxpct").ToString), 4)
            amttax.Text = ToMaskEdit(Trim(objRow(0)("trnamttax").ToString), 4)
            amtbeli.Text = ToMaskEdit(Trim(objRow(0)("trnamtjualnetto").ToString), 4)
            trnapnote.Text = Trim(objRow(0)("trnjualnote").ToString)
            conapoid.Text = Trim(objRow(0)("conaroid").ToString)
            payduedate.Text = Format(CDate(Trim(objRow(0)("payduedate").ToString)), "dd/MM/yyyy")
            paymentoid.Text = Trim(objRow(0)("paymentoid").ToString)
            'cashbankoid.Text = Trim(objRow(0)("cashbankoid").ToString)
            paymentdate.Text = Format(CDate(Trim(objRow(0)("paymentdate").ToString)), "dd/MM/yyyy")
            paymentacctgoid.Text = Trim(objRow(0)("paymentacctgoid").ToString)
            payrefno.Text = Trim(objRow(0)("payrefno").ToString)
            amtbayar.Text = ToMaskEdit(Trim(objRow(0)("amtbayar").ToString), 4)
            ddlCabang.SelectedValue = Trim(objRow(0)("branch_code").ToString)
            ReAmount()
            UpdUser.Text = Trim(objRow(0)("upduser").ToString)
            UpdTime.Text = Trim(objRow(0)("updtime").ToString)
            ' Other data
            acctgCode.Text = cKon.ambilscalar("SELECT acctgcode + '-' + acctgdesc FROM ql_mstacctg WHERE acctgoid = " & acctgoid.Text)
            payacctg.Text = cKon.ambilscalar("SELECT acctgcode + '-' + acctgdesc FROM ql_mstacctg WHERE acctgoid = '" & paymentacctgoid.Text & "'")
            suppcode.Text = cKon.ambilscalar("SELECT custname FROM ql_mstcust WHERE custoid = " & suppoid.Text)
        End If
        If lblPosting.Text = "POST" Then
            imbposting.Visible = False : btnSave.Visible = False : btnDelete.Visible = False
        Else
            imbposting.Visible = True : btnSave.Visible = True : btnDelete.Visible = True
        End If
    End Sub

    Private Sub generateID()
        If I_U.Text = "NEW" Then
            sSql = "SELECT ISNULL(MIN(trnjualmstoid),0) FROM ql_trnjualmst WHERE trnjualmstoid < 0"
            Session("vIDTrnBeli") = cKon.ambilscalar(sSql)
            trnbelimstoid.Text = cKon.ambilscalar(sSql)
        End If

        sSql = "SELECT ISNULL(MIN(paymentoid),0) FROM ql_trnpayar WHERE paymentoid < 0"
        Session("vIDPayap") = cKon.ambilscalar(sSql)
        sSql = "SELECT ISNULL(MIN(conaroid),0) FROM ql_conar WHERE conaroid < 0"
        Session("vIDConap") = cKon.ambilscalar(sSql)
        sSql = "SELECT ISNULL(MIN(cashbankoid),0) FROM ql_trncashbankmst WHERE cashbankoid < 0"
        Session("vIDCashBank") = cKon.ambilscalar(sSql)
        paymentoid.Text = Session("vIDPayap") - 1
        conapoid.Text = Session("vIDConap") - 1
        cashbankoid.Text = Session("vIDCashBank") - 1
    End Sub

    Sub bindDataAccounting(ByVal filterBranch As String, ByVal filterCode As String, ByVal filterDesc As String)
        Dim dt As New DataTable
        If lblTypeCOA.Text = "PaymentAccount" Then
            Dim varCash As String = GetVarInterface("VAR_CASH", cmpcode)
            Dim varBank As String = GetVarInterface("VAR_BANK", cmpcode)
            dt = cKon.ambiltabel("SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE (acctgcode LIKE '" & varCash & "%' OR " & _
                "acctgcode LIKE '" & varBank & "%') AND acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE " & _
                "a.acctggrp3 IS NOT NULL AND a.cmpcode=ql_mstacctg.cmpcode) AND cmpcode = '" & filterBranch & "' AND acctgcode LIKE '%" & _
                filterCode & "%' AND acctgdesc LIKE '%" & filterDesc & "%' order by acctgoid", "ql_mstacctg")
        ElseIf lblTypeCOA.Text = "SalesAccount" Then
            Dim varPuch As String = GetVarInterface("VAR_SALES", cmpcode)
            dt = cKon.ambiltabel("SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varPuch & "%' AND " & _
                "acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND " & _
                "a.cmpcode=ql_mstacctg.cmpcode) AND cmpcode = '" & filterBranch & "' AND acctgcode LIKE '%" & _
                filterCode & "%' AND acctgdesc LIKE '%" & filterDesc & "%' order by acctgoid", "ql_mstacctg")
        Else
            Dim varAP As String = GetVarInterface("VAR_AR", cmpcode)
            dt = cKon.ambiltabel("SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varAP & "%' AND " & _
                "acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND " & _
                "a.cmpcode=ql_mstacctg.cmpcode) AND cmpcode = '" & filterBranch & "' AND acctgcode LIKE '%" & _
                filterCode & "%' AND acctgdesc LIKE '%" & filterDesc & "%' order by acctgoid", "ql_mstacctg")
        End If
        gvAcc.DataSource = dt
        gvAcc.DataBind()
    End Sub

    Public Sub bindDataSupplier(ByVal filterBranch As String, ByVal filterCode As String, ByVal filterName As String)
        Dim dt As New DataTable
        dt = cKon.ambiltabel("select custoid suppoid,custcode suppcode, custname suppname, 0 coa_hutang, '' coa FROM QL_mstcust s WHERE s.cmpcode = '" & filterBranch & "' AND custcode LIKE '%" & filterCode & _
            "%' AND custname LIKE '%" & filterName & "%' and branch_code='" & ddlCabang.SelectedValue & "' ORDER BY custname ", "TblMstSupp")
        gvSupplier.DataSource = dt
        gvSupplier.DataBind()
    End Sub

    Private Sub msg(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub
#End Region

#Region "Function"
    Private Function validatingData() As String
        Dim sMsg As String = ""
        If suppoid.Text = "" Then
            sMsg &= "- Customer harus diisi !!<BR>"
        End If
        If trnbelino.Text = "" Then
            sMsg &= "-  No Nota harus diisi !!<BR>"
        End If
        If acctgCode.Text = "" Then
            sMsg &= "- COA Piutang harus diisi !!<BR>"
        End If
 
        Dim st1 As Boolean = False : Dim st2 As Boolean = False : Dim st3 As Boolean = False
        Try
            Dim duedate As Date : duedate = toDate(invoicedate.Text) : st1 = True
        Catch ex As Exception
            sMsg &= "- Format Tgl Invoice salah !!<BR>"
        End Try
        Try
            Dim duedate As Date : duedate = toDate(payduedate.Text) : st2 = True
        Catch ex As Exception
            sMsg &= "- Format Tgl Jatuh tempo salah !!<BR>"
        End Try
        Try
            Dim duedate As Date : duedate = toDate(paymentdate.Text) : st3 = True
        Catch ex As Exception
            sMsg &= "- Format Tgl Pembayaran salah !!<BR>"
        End Try
        
        If amtbeli.Text = "" Or ToDouble(amtbeli.Text) <= 0 Then
            sMsg &= "- Total Invoice harus >= 0 !!<BR>"
        End If
        If ToDouble(amtbayar.Text) >= ToDouble(amtbeli.Text) Then
            sMsg &= "- Total bayar harus < Total Invoice  !!<BR>"
        End If
        If trnapnote.Text.Trim.Length > 200 Then
            sMsg &= "- Masimal catatan pada note hanya 200 karakter !!<BR>"
        End If
        Return sMsg
    End Function
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("trnconar.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        imbposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        Page.Title = "A/R Initial Balance"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            msg("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", compnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate.AddDays(-1), "dd/MM/yyyy")
        '=============================================================
        If Not Page.IsPostBack Then
            binddata(cmpcode, "", "")
            initDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                btnDelete.Visible = True
                fillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                I_U.Text = "UPDATE"
            Else
                generateID() : I_U.Text = "NEW"
                invoicedate.Text = Format(CutOffDate.AddDays(-1), "dd/MM/yyyy")
                invoicedate_TextChanged(sender, e)
                paymentdate.Text = Format(GetServerTime, "dd/MM/yyyy")
                btnDelete.Visible = False
                UpdUser.Text = Session("UserID")
                UpdTime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0
                lblPosting.Text = "IN PROCESS"
            End If
        End If
        If lblPosting.Text = "POST" Then
            btnshowCOA.Visible = False
        Else
            btnshowCOA.Visible = False
        End If
    End Sub

    Protected Sub btnCariSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataSupplier(cmpcode, "", "")
        btnHiddenSupp.Visible = True
        PanelSupp.Visible = True
        gvSupplier.Visible = True
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub btnFindSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ddlSup.SelectedValue = "Code" Then
            bindDataSupplier(cmpcode, Tchar(filterSupp.Text), "")
        ElseIf ddlSup.SelectedValue = "Name" Then
            bindDataSupplier(cmpcode, "", Tchar(filterSupp.Text))
        End If
        gvSupplier.Visible = True
        btnHiddenSupp.Visible = True
        PanelSupp.Visible = True
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub btnListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        filterSupp.Text = ""
        bindDataSupplier(cmpcode, "", "")
        btnHiddenSupp.Visible = True
        PanelSupp.Visible = True
        gvSupplier.Visible = True
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppoid.Text = gvSupplier.SelectedDataKey(0).ToString()
        suppcode.Text = gvSupplier.SelectedDataKey(2).ToString()
        lblTypeCOA.Text = "APAccount"
        acctgoid.Text = gvSupplier.SelectedDataKey("coa_hutang").ToString().Trim
        acctgCode.Text = gvSupplier.SelectedDataKey("coa").ToString().Trim
        If ToDouble(acctgoid.Text) = 0 Then
            btnSearcAcctg.Visible = True
        Else
            btnSearcAcctg.Visible = False
        End If

        sSql = "SELECT distinct  acctgoid,acctgcode + '-' + acctgdesc as desk FROM QL_mstacctg W inner join QL_mstinterface i on W.acctgcode like i.interfacevalue + '%'   and i.interfacevar like 'VAR_AR' AND  acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=W.cmpcode) AND W.cmpcode='" & cmpcode & "' ORDER BY acctgoid  "
        Dim otable As DataTable = CreateDataTableFromSQL(sSql)
        If otable.Rows.Count > 0 Then
            acctgoid.Text = otable.Rows(0).Item("acctgoid")
            acctgCode.Text = otable.Rows(0).Item("desk")
        End If
        btnSearcAcctg.Visible = False
        btnHiddenSupp.Visible = False
        PanelSupp.Visible = False
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ClosePopUP_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHiddenSupp.Visible = False
        PanelSupp.Visible = False
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub btnSearcAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblTypeCOA.Text = "APAccount"
        bindDataAccounting(cmpcode, "", "")
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub btnFindAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ddlAcctg.SelectedValue = "Code" Then
            bindDataAccounting(cmpcode, Tchar(filterAcctg.Text), "")
        ElseIf ddlAcctg.SelectedValue = "Name" Then
            bindDataAccounting(cmpcode, "", Tchar(filterAcctg.Text))
        End If
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub btnListAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        filterAcctg.Text = ""
        bindDataAccounting(cmpcode, "", "")
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub gvAcc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If lblTypeCOA.Text = "PaymentAccount" Then
            paymentacctgoid.Text = gvAcc.SelectedDataKey(0).ToString().Trim
            payacctg.Text = gvAcc.SelectedDataKey(1).ToString().Trim & "-" & gvAcc.SelectedDataKey(2).ToString().Trim
        ElseIf lblTypeCOA.Text = "SalesAccount" Then
            purchAcctgoid.Text = gvAcc.SelectedDataKey(0).ToString().Trim
            purchAcctg.Text = gvAcc.SelectedDataKey(1).ToString().Trim & "-" & gvAcc.SelectedDataKey(2).ToString().Trim
        Else
            acctgoid.Text = gvAcc.SelectedDataKey(0).ToString().Trim
            acctgCode.Text = gvAcc.SelectedDataKey(1).ToString().Trim & "-" & gvAcc.SelectedDataKey(2).ToString().Trim
        End If
        btnHiddenAcctg.Visible = False
        PanelAcctg.Visible = False
        ModalPopupExtender2.Hide()
    End Sub

    Protected Sub CloseAcctg_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHiddenAcctg.Visible = False
        PanelAcctg.Visible = False
        ModalPopupExtender2.Hide()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sValue As String = validatingData()
        If sValue <> "" Then
            lblPosting.Text = ""
            msg(sValue, compnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        'If CDate(toDate(invoicedate.Text)) > CDate(toDate(CutofDate.Text)) Then
        '    msg("- Tanggal invoice Tidak boleh >= CutoffDate (" & CutofDate.Text & ") !! <BR> ", compnyName & "- WARNING", 2, "modalMsgBox")
        '    Exit Sub
        'End If
        generateID()
        If ToDouble(amtbayar.Text) > 0 Then
            sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctg((toDate(invoicedate.Text))) < GetStrData(sSql) Then
                msg("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", compnyName & " - Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If

        sSql = "Select COUNT(*) From QL_trnjualmst Where trncustoid=" & suppoid.Text & " AND branch_code='" & Session("branch_id") & "' AND trnjualmstoid < 0 AND trnjualstatus='POST'"

        If GetScalar(sSql) > 0 Then
            msg("Customer " & suppcode.Text & " Sudah ada saldo awal..!!", compnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        '=======================================================================================
        Dim rateidr As String = GetStrData("select top 1 rate2idrvalue from ql_mstrate2 where currencyoid='" & curroid.Text & "' order by rate2date desc")
        Dim rateusd As String = GetStrData("select top 1 rate2usdvalue from ql_mstrate2 where currencyoid='" & curroid.Text & "' order by rate2date desc")

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Connection = conn
        xCmd.Transaction = objTrans
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                ' ql_trnjualmst
                sSql = "DELETE ql_trnjualmst WHERE cmpcode='" & cmpcode & "' And trnjualmstoid=" & trnbelimstoid.Text : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO ql_trnjualmst (cmpcode ,branch_code,trnjualmstoid ,periodacctg ,trnjualtype ,trnjualno ,trnjualdate ,orderno ,trnjualref ,trncustoid ,trnpaytype ,trndiscamt ,trndisctype ,trnjualnote ,trnjualstatus ,createuser ,upduser ,updtime ,amtdischdr ,trnamtjual ,trnamtjualidr,trnamtjualusd,trnamtjualnetto ,trnamtjualnettoidr,trnamtjualnettousd,accumpayment,accumpaymentidr,accumpaymentusd ,lastpaymentdate ,amtdiscdtl ,refnotaRevisi ,salesoid ,trntaxpct ,trnamttax ,trnamttaxidr ,trnamttaxusd ,trndisctype2 ,trndiscamt2 ,amtdischdr2,amtretur ,postacctg ,currencyrate ,currencyoid, trncustname,trnamtjualnettoacctg) VALUES" & _
                " ('" & cmpcode & "','" & ddlCabang.SelectedValue & "'," & trnbelimstoid.Text & ",'" & GetDateToPeriodAcctg(toDate(invoicedate.Text)) & "','SALDO AWAL','" & Tchar(trnbelino.Text.Trim) & "','" & toDate(invoicedate.Text) & "','',''," & suppoid.Text & "," & trnpaytype.SelectedValue & ",0,'AMT','" & Tchar(trnapnote.Text) & "','" & lblPosting.Text & "','" & Session("USERID") & "','" & Session("USERID") & "',CURRENT_TIMESTAMP,0," & ToDouble(dpp.Text) & "," & ToDouble(dpp.Text) * rateidr & "," & ToDouble(dpp.Text) * rateusd & "," & ToDouble(amtbeli.Text) & "," & ToDouble(amtbeli.Text) * rateidr & "," & ToDouble(amtbeli.Text) * rateusd & "," & ToDouble(amtbayar.Text) & "," & ToDouble(amtbayar.Text) * rateidr & "," & ToDouble(amtbayar.Text) * rateusd & ",'" & toDate(paymentdate.Text) & "',0,'',0," & ToDouble(trntaxpct.Text) & "," & ToDouble(amttax.Text) & "," & ToDouble(amttax.Text) * rateidr & "," & ToDouble(amttax.Text) * rateusd & ",'AMT',0,0,0,'" & lblPosting.Text & "'," & ToDouble(currate.Text) & ", " & curroid.SelectedValue & ", '" & Tchar(suppcode.Text.Trim) & "'," & ToDouble(amtbeli.Text) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                sSql = "Update ql_trnjualmst SET periodacctg='" & GetDateToPeriodAcctg(toDate(invoicedate.Text)) & "' ,trnjualno='" & Tchar(trnbelino.Text.Trim) & "' ,trnjualdate='" & toDate(invoicedate.Text) & "',trncustoid=" & suppoid.Text & " ,trnpaytype=" & trnpaytype.SelectedValue & ",trnjualnote='" & Tchar(trnapnote.Text) & "',trnjualstatus='" & lblPosting.Text & "' ,createuser='" & Session("USERID") & "' ,upduser='" & Session("USERID") & "',updtime=CURRENT_TIMESTAMP,trnamtjual=" & ToDouble(dpp.Text) & ",trnamtjualidr=" & ToDouble(dpp.Text) * rateidr & ",trnamtjualusd=" & ToDouble(dpp.Text) * rateusd & ",trnamtjualnetto=" & ToDouble(amtbeli.Text) & " ,trnamtjualnettoidr=" & ToDouble(amtbeli.Text) * rateidr & ",trnamtjualnettousd=" & ToDouble(amtbeli.Text) * rateusd & ",accumpayment=" & ToDouble(amtbayar.Text) & ",accumpaymentidr=" & ToDouble(amtbayar.Text) * rateidr & ",accumpaymentusd=" & ToDouble(amtbayar.Text) * rateusd & " ,trntaxpct=" & ToDouble(trntaxpct.Text) & " ,trnamttax=" & ToDouble(amttax.Text) & ",trnamttaxidr=" & ToDouble(amttax.Text) * rateidr & " ,trnamttaxusd =" & ToDouble(amttax.Text) * rateusd & ",postacctg ='" & lblPosting.Text & "',currencyrate=" & ToDouble(currate.Text) & " ,currencyoid=" & curroid.SelectedValue & ", trncustname='" & Tchar(suppcode.Text.Trim) & "',branch_code='" & ddlCabang.SelectedValue & "',trnamtjualnettoacctg=" & ToDouble(amtbeli.Text) & _
                "Where trnjualmstoid=" & trnbelimstoid.Text & " And branch_code='" & ddlCabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If lblPosting.Text = "POST" Then
                'CONAP
                sSql = "INSERT INTO QL_conar (cmpcode,branch_code,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,trnarnote,trnarres1,upduser,updtime,returoid) VALUES " & _
                " ('" & cmpcode & "','" & ddlCabang.SelectedValue & "'," & Trim(conapoid.Text) & ",'ql_trnjualmst'," & trnbelimstoid.Text & ",0," & suppoid.Text & "," & Trim(acctgoid.Text) & ",'" & lblPosting.Text & "','AR','" & toDate(invoicedate.Text) & "','" & GetDateToPeriodAcctg(toDate(invoicedate.Text)) & "',0,'01/01/1900','',0,'" & toDate(payduedate.Text) & "'," & ToDouble(amtbeli.Text) & "," & ToDouble(amtbeli.Text) * rateidr & "," & ToDouble(amtbeli.Text) * rateusd & "," & ToDouble(amtbayar.Text) & "," & ToDouble(amtbayar.Text) * rateidr & "," & ToDouble(amtbayar.Text) * rateusd & ",'" & Tchar(trnapnote.Text) & "','','" & Trim(Session("UserID")) & "',CURRENT_TIMESTAMP,0)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If ToDouble(amtbayar.Text) > 0 Then
                    'PAYAP
                    sSql = "INSERT INTO QL_trnpayar (cmpcode,branch_code,paymentoid,cashbankoid,custoid,payreftype,payrefoid,payflag,payacctgoid,payrefno,paybankoid,payduedate,paynote,payamt,payamtidr,payamtusd,payrefflag,paystatus,upduser,updtime,trndparoid,DPAmt,returoid) VALUES " & _
                    " ('" & cmpcode & "','" & ddlCabang.SelectedValue & "'," & Trim(paymentoid.Text) & ",0," & suppoid.Text & ",'ql_trnjualmst'," & trnbelimstoid.Text & ",'','" & acctgoid.Text & "','" & Tchar(payrefno.Text) & "',0,'01/01/1900','" & Tchar(trnapnote.Text) & "'," & ToDouble(amtbayar.Text) & "," & ToDouble(amtbayar.Text) * rateidr & "," & ToDouble(amtbayar.Text) * rateusd & ",'','" & lblPosting.Text & "','" & Trim(Session("UserID")) & "',CURRENT_TIMESTAMP,0,0,0)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            lblPosting.Text = ""
            objTrans.Rollback() : conn.Close()
            msg(ex.ToString, compnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
        End Try
        If lblPosting.Text = "POST" Then
            msg("Data telah diposting !", compnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            msg("Data telah disimpan !", compnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        End If
        Response.Redirect("trnconar.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            'ql_trnjualmst
            sSql = "DELETE ql_trnjualmst WHERE cmpcode='" & cmpcode & "' and trnjualmstoid=" & trnbelimstoid.Text
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            'ql_conap - Invoice
            'sSql = "DELETE QL_conar WHERE cmpcode='" & cmpcode & "' and conaroid=" & conapoid.Text
            'objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            'ql_trnpayap
            'sSql = "DELETE ql_trnpayar WHERE cmpcode='" & cmpcode & "' and paymentoid=" & paymentoid.Text
            'objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            'ql_conap - Payment
            'strSQL = "DELETE QL_conar WHERE cmpcode='" & cmpcode & "' and conaroid=" & conapoid.Text - 1
            'objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            objTrans.Commit() : objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            msg(ex.Message, compnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
        End Try
        msg("Data telah dihapus !", compnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        'Response.Redirect("trnconar.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("trnconar.aspx?awal=true")
    End Sub

    Protected Sub btnFindConap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ddlFilter.SelectedValue = "name" Then
            binddata(cmpcode, FilterConap.Text, "")
        ElseIf ddlFilter.SelectedValue = "no" Then
            binddata(cmpcode, "", FilterConap.Text)
        End If
    End Sub

    Protected Sub btnViewAllConap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddata(cmpcode, "", "")
        FilterConap.Text = ""
    End Sub

    Protected Sub gvTrnConap_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnconar.aspx?oid=" & gvTrnConap.SelectedDataKey(0).ToString().Trim)
    End Sub

    Protected Sub gvTrnConap_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrnConap.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(7).Text = NewMaskEdit(e.Row.Cells(7).Text)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 4)
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
            'e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterConap.Text = ""
    End Sub

    Protected Sub trntaxpct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub amtbeli_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub imbPayAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblTypeCOA.Text = "PaymentAccount"
        bindDataAccounting(cmpcode, "", "")
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub imbposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbposting.Click
        ' POST to GL
        lblPosting.Text = "POST" : postdate.Text = Format(Today, "dd/MM/yyyy")
        btnSave_Click(sender, e)
    End Sub

    Protected Sub imbPurch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblTypeCOA.Text = "SalesAccount"
        bindDataAccounting(cmpcode, "", "")
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillRate(curroid.SelectedValue)
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA("CONAR=" & trnbelino.Text, cmpcode, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)

        End If
    End Sub

    Protected Sub gvTrnConap_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTrnConap.PageIndexChanging
        gvTrnConap.PageIndex = e.NewPageIndex
        If ddlFilter.SelectedValue = "name" Then
            binddata(cmpcode, FilterConap.Text, "")
        ElseIf ddlFilter.SelectedValue = "no" Then
            binddata(cmpcode, "", FilterConap.Text)
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub trnpaytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sSql = "select genother1 from ql_mstgen where genoid=" & trnpaytype.SelectedValue
        payduedate.Text = Format(CDate(toDate(invoicedate.Text)).AddDays(ToDouble(GetStrData(sSql))), "dd/MM/yyyy")
    End Sub

    Protected Sub invoicedate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles invoicedate.TextChanged
        Try
            sSql = "select genother1 from ql_mstgen where genoid=" & trnpaytype.SelectedValue
            payduedate.Text = Format(CDate(toDate(invoicedate.Text)).AddDays(ToDouble(GetStrData(sSql))), "dd/MM/yyyy")

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnConAR.aspx?awal=true")
        End If
    End Sub
#End Region
End Class
