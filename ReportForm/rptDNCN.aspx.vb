' Last Update By 4n7JuK On 130924
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_RptDNCN
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~\Report\"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(toDate(txtStart.Text)), CDate(toDate(txtFinish.Text))) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        If rbSupplier.SelectedValue = "SELECT" Then
            If Not (Session("QL_mstcust") Is Nothing) Then
                Dim dtSupp As DataTable : dtSupp = Session("QL_mstcust")
                If dtSupp.Rows.Count > 0 Then
                    Dim dvSupp As DataView = dtSupp.DefaultView
                    dvSupp.RowFilter = "selected='1'"
                    If dvSupp.Count < 1 Then
                        sReturn &= "- Please select some Customer first.<BR>"
                    End If
                    dvSupp.RowFilter = ""
                Else
                    sReturn &= "- Please select some Customer first.<BR>"
                End If
            Else
                sReturn &= "- Please select some Customer first.<BR>"
            End If
        End If
        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        Dim sWhere As String = ""
        If CbgDDL.SelectedValue <> "ALL" Then
            sWhere &= " AND branch_code='" & CbgDDL.SelectedValue & "'"
        End If
        If ddlDNCN.SelectedValue <> "ALL" Then
            sWhere &= " AND no LIKE '%" & ddlDNCN.SelectedValue & "%'"
        End If
        If ddlType.SelectedValue <> "ALL" Then
            sWhere &= " AND reftype = '" & ddlType.SelectedValue & "'"
        End If
        sSql = "SELECT DISTINCT 0 selected,custoid, custcode, custname from (" & _
"SELECT cmpcode,cust_supp_oid AS custoid, [Cust/Supp] AS custname, [Cust/Supp Code] AS custcode, reftype, branch_code, no from (select cmpcode, no, reftype, branch_code,cust_supp_oid, (CASE WHEN dn.reftype = 'AR' then (SELECT trnjualno FROM QL_trnjualmst where trnjualmstoid = dn.refoid) WHEN dn.reftype = 'AP' THEN (SELECT trnbelino FROM QL_trnbelimst where trnbelimstoid = dn.refoid) ELSE (SELECT trnbiayaeksno FROM ql_trnbiayaeksmst where trnbiayaeksoid = refoid) END) AS refnotano, (CASE WHEN dn.reftype = 'AP' THEN (SELECT suppname FROM QL_mstsupp WHERE suppoid = dn.cust_supp_oid) ELSE (SELECT custname FROM QL_mstcust where custoid = dn.cust_supp_oid) END) AS [Cust/Supp], (CASE WHEN dn.reftype = 'AP' THEN (SELECT suppcode FROM QL_mstsupp WHERE suppoid = dn.cust_supp_oid) ELSE (SELECT custcode FROM QL_mstcust where custoid = dn.cust_supp_oid) END) AS [Cust/Supp Code] from QL_DebitNote dn UNION ALL select cmpcode, no, reftype, branch_code,cust_supp_oid, (CASE WHEN cn.reftype = 'AR' then (SELECT trnjualno FROM QL_trnjualmst where trnjualmstoid = cn.refoid) WHEN cn.reftype = 'AP' THEN (SELECT trnbelino FROM QL_trnbelimst where trnbelimstoid = cn.refoid) ELSE (SELECT trnbiayaeksno FROM ql_trnbiayaeksmst where trnbiayaeksoid = refoid) END) AS refnotano, (CASE WHEN cn.reftype = 'AP' THEN (SELECT suppname FROM QL_mstsupp WHERE suppoid = cn.cust_supp_oid) ELSE (SELECT custname FROM QL_mstcust where custoid = cn.cust_supp_oid) END) AS [Cust/Supp], (CASE WHEN cn.reftype = 'AP' THEN (SELECT suppcode FROM QL_mstsupp WHERE suppoid = cn.cust_supp_oid) ELSE (SELECT custcode FROM QL_mstcust where custoid = cn.cust_supp_oid) END) AS [Cust/Supp Code] from QL_CreditNote cn ) as dncn WHERE dncn.cmpcode='" & CompnyCode & "' AND ([Cust/Supp] LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR [Cust/Supp Code] LIKE '%" & Tchar(FilterTextSupplier.Text) & "%') " & sWhere & " " & _
" ) DC ORDER BY custname"

        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstcust")
        gvCustomer.DataSource = dtSupp : gvCustomer.DataBind()
        Session("QL_mstcust") = dtSupp
        gvCustomer.Visible = True
        gvCustomer.SelectedIndex = -1
    End Sub

    Private Sub BindRefnoNotaData()
        Dim sSuppOid As String = ""
        Dim sWhere As String = ""
        If CbgDDL.SelectedValue <> "ALL" Then
            sWhere &= " AND branch_code='" & CbgDDL.SelectedValue & "'"
        End If
        If ddlDNCN.SelectedValue <> "ALL" Then
            sWhere &= " AND no LIKE '%" & ddlDNCN.SelectedValue & "%'"
        End If
        If ddlType.SelectedValue <> "ALL" Then
            sWhere &= " AND reftype = '" & ddlType.SelectedValue & "'"
        End If
        If rbSupplier.SelectedValue = "SELECT" Then
            Dim dtSupps As DataTable : dtSupps = Session("QL_mstcust")
            Dim dvSupps As DataView = dtSupps.DefaultView
            dvSupps.RowFilter = "selected='1'"

            For R1 As Integer = 0 To dvSupps.Count - 1
                sSuppOid &= dvSupps(R1)("custoid").ToString & ","
            Next
        End If

        If sSuppOid <> "" Then
            sWhere &= " AND cust_supp_oid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
        End If
        sSql = "SELECT DISTINCT 0 selected,refoid, refnotano, refnodate FROM (" & _
"SELECT 0 selected,refoid, refnotano, CONVERT(VARCHAR(20),refnodate,103) refnodate, branch_code, no, reftype from (select branch_code, no, reftype, refoid, (CASE WHEN dn.reftype = 'AR' then (SELECT trnjualno FROM QL_trnjualmst where trnjualmstoid = dn.refoid) WHEN dn.reftype = 'AP' THEN (SELECT trnbelino FROM QL_trnbelimst where trnbelimstoid = dn.refoid) ELSE (SELECT trnbiayaeksno FROM ql_trnbiayaeksmst where trnbiayaeksoid = refoid) END) AS refnotano, (CASE WHEN dn.reftype = 'AR' then (SELECT trnjualdate FROM QL_trnjualmst where trnjualmstoid = dn.refoid) WHEN dn.reftype = 'AP' THEN (SELECT trnbelidate FROM QL_trnbelimst where trnbelimstoid = dn.refoid) ELSE (SELECT trnbiayaeksdate FROM ql_trnbiayaeksmst where trnbiayaeksoid = refoid) END) AS refnodate, (CASE WHEN dn.reftype = 'AP' THEN (SELECT suppname FROM QL_mstsupp WHERE suppoid = dn.cust_supp_oid) ELSE (SELECT custname FROM QL_mstcust where custoid = dn.cust_supp_oid) END) AS [Cust/Supp] from QL_DebitNote dn UNION ALL select branch_code, no, reftype, refoid, (CASE WHEN cn.reftype = 'AR' then (SELECT trnjualno FROM QL_trnjualmst where trnjualmstoid = cn.refoid) WHEN cn.reftype = 'AP' THEN (SELECT trnbelino FROM QL_trnbelimst where trnbelimstoid = cn.refoid) ELSE (SELECT trnbiayaeksno FROM ql_trnbiayaeksmst where trnbiayaeksoid = refoid) END) AS refnotano, (CASE WHEN cn.reftype = 'AR' then (SELECT trnjualdate FROM QL_trnjualmst where trnjualmstoid = cn.refoid) WHEN cn.reftype = 'AP' THEN (SELECT trnbelidate FROM QL_trnbelimst where trnbelimstoid = cn.refoid) ELSE (SELECT trnbiayaeksdate FROM ql_trnbiayaeksmst where trnbiayaeksoid = cn.refoid) END) AS refnodate, (CASE WHEN cn.reftype = 'AP' THEN (SELECT suppname FROM QL_mstsupp WHERE suppoid = cn.cust_supp_oid) ELSE (SELECT custname FROM QL_mstcust where custoid = cn.cust_supp_oid) END) AS [Cust/Supp] from QL_CreditNote cn ) as dncn where refnotano like '%" & refnonota.Text & "%' " & sWhere & " " & _
" ) DC ORDER BY refnotano"

        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_debit_credit_note")
        gvRefnoNota.DataSource = dtSupp : gvRefnoNota.DataBind()
        Session("QL_debit_credit_note") = dtSupp
        gvRefnoNota.Visible = True
        gvRefnoNota.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("QL_mstcust") Is Nothing Then
            Dim dtab As DataTable = Session("QL_mstcust")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvCustomer.Rows.Count - 1
                    cb = gvCustomer.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "custoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_mstcust") = dtab
        End If

        If Not Session("QL_debit_credit_note") Is Nothing Then
            Dim dtab As DataTable = Session("QL_debit_credit_note")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvRefnoNota.Rows.Count - 1
                    cb = gvRefnoNota.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "refoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_debit_credit_note") = dtab
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim sSuppOid As String = ""
            Dim notaoid As String = ""
            Dim sWhere As String = " "
            sWhere &= " Where cmpcode='" & CompnyCode & "'"
            sWhere &= " AND tgl >= '" & CDate(toDate(txtStart.Text)) & "' AND tgl <= '" & CDate(toDate(txtFinish.Text)) & "'"
            If CbgDDL.SelectedValue <> "ALL" Then
                sWhere &= " AND branch_code='" & CbgDDL.SelectedValue & "'"
            End If
            If ddlDNCN.SelectedValue <> "ALL" Then
                sWhere &= " AND no LIKE '%" & ddlDNCN.SelectedValue & "%'"
            End If
            If ddlType.SelectedValue <> "ALL" Then
                sWhere &= " AND reftype = '" & ddlType.SelectedValue & "'"
            End If

            If rbSupplier.SelectedValue = "SELECT" Then
                Dim dtSupp As DataTable : dtSupp = Session("QL_mstcust")
                Dim dvSupp As DataView = dtSupp.DefaultView
                dvSupp.RowFilter = "selected='1'"

                For R1 As Integer = 0 To dvSupp.Count - 1
                    sSuppOid &= dvSupp(R1)("custoid").ToString & ","
                Next
            End If

            If rbNota.SelectedValue = "SELECT" Then
                Dim dtnota As DataTable : dtnota = Session("QL_debit_credit_note")
                Dim dvnota As DataView = dtnota.DefaultView
                dvnota.RowFilter = "selected='1'"

                For R1 As Integer = 0 To dvnota.Count - 1
                    notaoid &= dvnota(R1)("refoid").ToString & ","
                Next
            End If

            If sSuppOid <> "" Then
                sWhere &= " AND cust_supp_oid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            End If

            If notaoid <> "" Then
                sWhere &= " AND refoid IN (" & Left(notaoid, notaoid.Length - 1) & ")"
            End If

            If ddlstatus.SelectedValue <> "ALL" Then
                sWhere &= " AND status = '" & ddlstatus.SelectedValue & "'"
            End If

            If FilterCurrency.SelectedIndex <> "2" Then
                report.Load(Server.MapPath("~\Report\rptDNCN.rpt"))
            Else
                report.Load(Server.MapPath("~\Report\rptDNCN.rpt"))
            End If

            sSql = " SELECT * FROM ( select '" & CompnyCode & "' AS cmpcode,oid, g.gendesc AS cabang, refoid, branch_code, tgl, no,reftype, (CASE WHEN dn.reftype = 'AR' then (SELECT trnjualno FROM QL_trnjualmst where trnjualmstoid = dn.refoid) WHEN dn.reftype = 'AP' THEN (SELECT trnbelino FROM QL_trnbelimst where trnbelimstoid = dn.refoid) ELSE (SELECT trnbiayaeksno FROM ql_trnbiayaeksmst where trnbiayaeksoid = refoid) END) AS refnotano, (CASE WHEN dn.reftype = 'AP' THEN (SELECT suppname FROM QL_mstsupp WHERE suppoid = dn.cust_supp_oid) ELSE (SELECT custname FROM QL_mstcust where custoid = dn.cust_supp_oid) END) AS [Cust/Supp], dn.cust_supp_oid, amountidr, note, dnstatus AS status from QL_DebitNote dn INNER JOIN QL_mstgen g ON g.gencode = dn.branch_code and g.gengroup = 'CABANG'" & _
            " UNION ALL" & _
" select '" & CompnyCode & "' AS cmpcode,oid, g.gendesc AS cabang, refoid, branch_code, tgl, no,reftype, (CASE WHEN cn.reftype = 'AR' then (SELECT trnjualno FROM QL_trnjualmst where trnjualmstoid = cn.refoid) WHEN cn.reftype = 'AP' THEN (SELECT trnbelino FROM QL_trnbelimst where trnbelimstoid = cn.refoid) ELSE (SELECT trnbiayaeksno FROM ql_trnbiayaeksmst where trnbiayaeksoid = refoid) END) AS refnotano, (CASE WHEN cn.reftype = 'AP' THEN (SELECT suppname FROM QL_mstsupp WHERE suppoid = cn.cust_supp_oid) ELSE (SELECT custname FROM QL_mstcust where custoid = cn.cust_supp_oid) END) AS [Cust/Supp], cn.cust_supp_oid, amountidr, note, cnstatus AS status from QL_CreditNote cn INNER JOIN QL_mstgen g ON g.gencode = cn.branch_code and g.gengroup = 'CABANG'" & _
" ) dncn " & sWhere & " ORDER BY  branch_code,tgl, refnotano ASC"

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_debit_credit_note")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)

            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sPeriod", txtStart.Text & " s/d " & txtFinish.Text)


            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DNCNREPORT_" & Format(CDate(toDate(txtStart.Text)), "yyyyMMdd" & "_" & Format(CDate(toDate(txtFinish.Text)), "yyyyMMdd")))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DNCNREPORT_" & Format(CDate(toDate(txtStart.Text)), "yyyyMMdd" & "_" & Format(CDate(toDate(txtFinish.Text)), "yyyyMMdd")))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub ddlCbg()
        Dim scb As String = ""
        sSql = "Select gencode,gendesc from QL_mstgen Where gengroup='CABANG'"

        If Session("branch_id") <> "10" Then
            CbgDDL.Enabled = False : CbgDDL.CssClass = "inpTextDisabled"
            sSql &= "And gencode='" & Session("branch_id") & "'"
            FillDDL(CbgDDL, sSql)
        Else
            FillDDL(CbgDDL, sSql)
            CbgDDL.Enabled = True : CbgDDL.CssClass = "inpText"
            CbgDDL.Items.Add(New ListItem("ALL", "ALL"))
            CbgDDL.SelectedValue = "ALL"
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptDNCN.aspx")
        End If
        Session("idPage") = Request.QueryString("idPage")
        Page.Title = CompnyName & " - Debit & Credit Note Report"

        If Not Page.IsPostBack Then
            ddlCbg()
            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False
            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "01/MM/yyyy")
            txtFinish.Text = Format(GetServerTime, "dd/MM/yyyy")
            rbSupplier_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvCustomer.DataSource = Nothing : gvCustomer.DataBind()
        Session("QL_mstcust") = Nothing : gvCustomer.Visible = False
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        BindSupplierData()
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        FilterTextSupplier.Text = "" : custoid.Text = ""
        gvCustomer.DataSource = Nothing
        gvCustomer.DataBind()
        gvCustomer.Visible = False
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCustomer.PageIndexChanging
        UpdateCheckedGV()
        gvCustomer.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstcust")
        gvCustomer.DataSource = dtSupp
        gvCustomer.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("View")
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\rptDNCN.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        UpdateCheckedGV()
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        UpdateCheckedGV()
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        UpdateCheckedGV()
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region

    Protected Sub btnSearchRefnoNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchRefnoNota.Click
        BindRefnoNotaData()
    End Sub

    Protected Sub btnClearRefnoNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearRefnoNota.Click
        refnonota.Text = "" : refoid.Text = ""
        gvRefnoNota.DataSource = Nothing
        gvRefnoNota.DataBind()
        gvRefnoNota.Visible = False
    End Sub

    Protected Sub gvRefnoNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRefnoNota.PageIndexChanging
        UpdateCheckedGV()
        gvRefnoNota.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_debit_credit_note")
        gvRefnoNota.DataSource = dtSupp
        gvRefnoNota.DataBind()
    End Sub

    Protected Sub gvRefnoNota_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRefnoNota.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(2).Text = e.Row.Cells(2).Text
        'End If
    End Sub

    Protected Sub rbNota_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbNota.SelectedIndexChanged
        gvRefnoNota.DataSource = Nothing : gvRefnoNota.DataBind()
        Session("QL_debit_credit_note") = Nothing : gvRefnoNota.Visible = False
        If rbNota.SelectedValue = "ALL" Then
            refnonota.Visible = False
            btnSearchRefnoNota.Visible = False
            btnClearRefnoNota.Visible = False
        Else
            refnonota.Visible = True
            btnSearchRefnoNota.Visible = True
            btnClearRefnoNota.Visible = True
        End If
    End Sub
End Class
