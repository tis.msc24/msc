<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptNeraca.aspx.vb" Inherits="ReportForm_rptNeraca" Title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" style="text-align: left" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Neraca"></asp:Label></th>
        </tr>
    </table>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" class="Label" align=left colSpan=2><asp:TextBox id="suppname" runat="server" Width="150px" CssClass="inpText" Visible="False" __designer:wfdid="w9"></asp:TextBox> <asp:TextBox id="date1" runat="server" Width="75px" CssClass="inpText" Visible="False" __designer:wfdid="w10"></asp:TextBox> <asp:ImageButton id="search1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w11"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" Visible="False" __designer:wfdid="w12"></asp:Label> <asp:TextBox id="date2" runat="server" Width="75px" CssClass="inpText" Visible="False" __designer:wfdid="w13"></asp:TextBox> <asp:ImageButton id="search2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w14"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Visible="False" __designer:wfdid="w15"></asp:Label> </TD></TR><TR><TD style="WIDTH: 84px; TEXT-ALIGN: right" class="Label" align=left><asp:Label id="Label5" runat="server" Text="Branch :"></asp:Label> </TD><TD style="TEXT-ALIGN: left" class="Label" align=left><asp:DropDownList id="dd_branch" runat="server" Width="200px" CssClass="inpText">
                            </asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 84px; TEXT-ALIGN: right" class="Label" align=right>Tipe :</TD><TD style="TEXT-ALIGN: left" class="Label" align=left><asp:DropDownList id="DDLTipe" runat="server" CssClass="inpText" __designer:wfdid="w4">
                                <asp:ListItem>SUMMARY</asp:ListItem>
                                <asp:ListItem>DETAIL</asp:ListItem>
                            </asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 84px; TEXT-ALIGN: right" class="Label" align=left><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w18" UserDateFormat="MonthDayYear" TargetControlID="date1" MaskType="Date" Mask="99/99/9999">
                            </ajaxToolkit:MaskedEditExtender> Periode :</TD><TD style="TEXT-ALIGN: left" class="Label" align=left><asp:DropDownList id="DDLMonth" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w5" Enabled="False">
                            </asp:DropDownList> <asp:DropDownList id="DDLYear" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w6" Enabled="False">
                            </asp:DropDownList> -<asp:DropDownList id="DDLMonth2" runat="server" CssClass="inpText" __designer:wfdid="w7" AutoPostBack="True">
                            </asp:DropDownList> <asp:DropDownList id="DDLYear2" runat="server" CssClass="inpText" __designer:wfdid="w8" AutoPostBack="True">
                            </asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 84px; TEXT-ALIGN: right" class="Label" align=left>&nbsp;</TD><TD style="TEXT-ALIGN: left" class="Label" align=left><asp:CheckBox id="cbFilter" runat="server" Text="Don't Allow 0 Value" Checked="True"></asp:CheckBox> </TD></TR><TR id="trgakperlu1" runat="server" visible="false"><TD style="WIDTH: 84px; TEXT-ALIGN: center" class="Label" align=left>&nbsp;</TD><TD style="TEXT-ALIGN: center" class="Label" align=left><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w19" UserDateFormat="MonthDayYear" TargetControlID="date2" MaskType="Date" Mask="99/99/9999">
                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w16" TargetControlID="Date1" PopupButtonID="search1" Format="MM/dd/yyyy">
                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w17" TargetControlID="Date2" PopupButtonID="search2" Format="MM/dd/yyyy">
                                            </ajaxToolkit:CalendarExtender> </TD></TR><TR><TD style="TEXT-ALIGN: left" class="Label" align=left colSpan=2>&nbsp;&nbsp;<asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> &nbsp;<asp:ImageButton id="btView" onclick="btView_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w20"></asp:ImageButton> &nbsp;<asp:ImageButton id="imgExcel" onclick="imgExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w21"></asp:ImageButton> &nbsp;<asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w22"></asp:ImageButton> </TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label" align=left colSpan=2><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w23">
                                <ProgressTemplate>
                                    <asp:Image ID="Image1" runat="server" __designer:wfdid="w24" ImageAlign="AbsMiddle" ImageUrl="~/Images/loadingbar.gif" />
                                    <br />
                                    Please wait ..
                                </ProgressTemplate>
                            </asp:UpdateProgress> </TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label" align=left colSpan=2><asp:UpdatePanel id="upMsgbox" runat="server" __designer:wfdid="w25">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlPopUpMsg" runat="server" __designer:wfdid="w26" CssClass="modalMsgBox" Visible="False" Width="600px">
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td align="left" colspan="2" style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" valign="top">
                                                        <asp:Label ID="lblCaption" runat="server" __designer:wfdid="w27" Font-Bold="True" Font-Size="Small"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" style="HEIGHT: 10px" valign="top"></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="VERTICAL-ALIGN: top; WIDTH: 40px; TEXT-ALIGN: center" valign="top">
                                                        <asp:Image ID="imIcon" runat="server" __designer:wfdid="w28" Height="24px" ImageUrl="~/Images/error.jpg" Width="24px" />
                                                    </td>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblPopUpMsg" runat="server" __designer:wfdid="w29" CssClass="Important"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top">
                                                        <asp:Label ID="lblState" runat="server" __designer:wfdid="w30" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" style="TEXT-ALIGN: center" valign="top">
                                                        <asp:Button ID="btnOKPopUp" runat="server" __designer:wfdid="w31" CssClass="red" Font-Bold="True" Text="OK" Width="50px" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" __designer:wfdid="w32" BackgroundCssClass="modalBackground" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" TargetControlID="bePopUpMsg">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Button ID="bePopUpMsg" runat="server" __designer:wfdid="w33" CausesValidation="False" Visible="False" />
                                </ContentTemplate>
                            </asp:UpdatePanel> </TD></TR><TR><TD class="Label" align=left colSpan=2><asp:GridView id="GridView1" runat="server" ForeColor="#333333" __designer:wfdid="w34" GridLines="None" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" align=left colSpan=2><CR:CrystalReportViewer style="Z-INDEX: -100; POSITION: relative" id="crv" runat="server" Width="350px" Height="50px" __designer:wfdid="w35" AutoDataBind="True"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE>
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btView"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="imgExcel"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="imbClear"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

