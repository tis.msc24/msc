<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSalesRetur.aspx.vb" Inherits="ReportForm_rptSalesRetur"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" width="100%">
        <tr>
            <td colspan="3" rowspan="1" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                                Font-Size="21px" ForeColor="Navy" Text=".: Laporan Retur Penjualan"></asp:Label></th>
                    </tr>
                    <tr>
                        <th align="center" style="background-color: transparent; width: 100%; height: 100%;" valign="center">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE><TBODY><TR><TD align=left><asp:DropDownList id="DDLPeriode" runat="server" CssClass="inpText" __designer:wfdid="w3"><asp:ListItem Value="som.trnjualdate">Tanggal SR</asp:ListItem>
<asp:ListItem Value="dom.trnjualdate">Tanggal SI</asp:ListItem>
</asp:DropDownList></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="range1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w6"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD align=left>Tipe laporan</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddltype" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w9" AutoPostBack="True"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
<asp:ListItem Value="Konsinyasi">Retur Konsi</asp:ListItem>
<asp:ListItem Value="DtlKonsi">Detail Konsi</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label5" runat="server" Text="Cabang" __designer:wfdid="w10"></asp:Label></TD><TD align=left><asp:Label id="Label6" runat="server" Text=":" __designer:wfdid="w11"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLcabang" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w12"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD vAlign=top align=left>Customer</TD><TD vAlign=top align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="custcode" runat="server" Width="158px" CssClass="inpText" __designer:wfdid="w13" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w14"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w15"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=left><asp:Label id="Label4" runat="server" Width="84px" Text="No. Retur" __designer:wfdid="w16"></asp:Label></TD><TD vAlign=top align=left><asp:Label id="Label10" runat="server" Width="5px" Text=":" __designer:wfdid="w17"></asp:Label></TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="sono" runat="server" Width="158px" CssClass="inpText" __designer:wfdid="w18" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w19"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w20"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=left>No. SI</TD><TD vAlign=top align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="dono" runat="server" Width="158px" CssClass="inpText" __designer:wfdid="w21" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindDO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w22"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseDO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w23"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=left><asp:Label id="Label7" runat="server" Width="84px" Text="Sales" __designer:wfdid="w24"></asp:Label></TD><TD vAlign=top align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="regno" runat="server" Width="158px" CssClass="inpText" __designer:wfdid="w25" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindReg" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w26"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseReg" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w27"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=left><asp:Label id="Label2" runat="server" Width="84px" Text="Katalog" __designer:wfdid="w28" Visible="False"></asp:Label></TD><TD vAlign=top align=left><asp:Label id="Label3" runat="server" Text=":" __designer:wfdid="w29" Visible="False"></asp:Label></TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="itemcode" runat="server" Width="178px" CssClass="inpText" __designer:wfdid="w30" TextMode="MultiLine" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w31" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w32" Visible="False"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=left><asp:Label id="Label8" runat="server" Width="100px" Text="Amount Retur" __designer:wfdid="w33" Visible="False"></asp:Label></TD><TD vAlign=top align=left><asp:Label id="Label9" runat="server" Text=":" __designer:wfdid="w34" Visible="False"></asp:Label></TD><TD vAlign=top align=left colSpan=3><asp:DropDownList id="DDLAmtRetur" runat="server" Width="55px" CssClass="inpText" __designer:wfdid="w35" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="=0">0</asp:ListItem>
<asp:ListItem Value="&gt;0">&gt; 0</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnshowprint" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w40" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w41"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w42" Format="dd/MM/yyyy" PopupButtonID="ImageButton1" TargetControlID="range1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w43" Format="dd/MM/yyyy" PopupButtonID="ImageButton2" TargetControlID="range2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w44" TargetControlID="range1" CultureName="id-ID" MaskType="Date" ErrorTooltipEnabled="True" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w45" TargetControlID="range2" CultureName="id-ID" MaskType="Date" ErrorTooltipEnabled="True" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w46" AutoDataBind="true" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False" HasZoomFactorList="False" DisplayGroupTree="False"></CR:CrystalReportViewer> 
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListSO" runat="server">
                                <ContentTemplate>
<asp:Panel id="PanelListSO" runat="server" Width="1000px" CssClass="modalBox" __designer:wfdid="w48" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Sales Return" __designer:wfdid="w49"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" __designer:wfdid="w50" DefaultButton="btnFindListSO"><asp:Label id="Label241" runat="server" Text="Filter :" __designer:wfdid="w51"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w52"><asp:ListItem Value="trnjualno">No. SI</asp:ListItem>
<asp:ListItem Value="soitemno">No. SR</asp:ListItem>
<asp:ListItem Enabled="False" Value="soitemmstoid">No. Draft</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w53"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w54"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w55"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w56"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w57"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w58"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w59" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
                                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                            <Columns>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbLMSO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("soitemmstoid") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="soitemmstoid" HeaderText="Draft No">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Left"
                                                                        VerticalAlign="Middle" Wrap="False" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="soitemno" HeaderText="No. Retur">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="sodate" HeaderText="Tgl. Retur">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="cabang" HeaderText="Cabang">
                                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="soitemmstnote" HeaderText="Note">
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="True" Width="200px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="soitemmststatus" HeaderText="Status">
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                                HorizontalAlign="Right" />
                                                            <EmptyDataTemplate>
                                                                &nbsp;
                                                            </EmptyDataTemplate>
                                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                        </asp:GridView> </TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server" __designer:wfdid="w60">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server" __designer:wfdid="w61">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" __designer:wfdid="w62" TargetControlID="btnHiddenListSO" BackgroundCssClass="modalBackground" PopupControlID="PanelListSO" PopupDragHandleControlID="lblListSO">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" __designer:wfdid="w63" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListDO" runat="server">
                                <ContentTemplate>
<asp:Panel id="PanelListDO" runat="server" Width="900px" CssClass="modalBox" __designer:wfdid="w65" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListDO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Nota SI" __designer:wfdid="w66"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListDO" runat="server" Width="100%" __designer:wfdid="w67" DefaultButton="btnFindListDO"><asp:Label id="Label24" runat="server" Text="Filter :" __designer:wfdid="w68"></asp:Label> <asp:DropDownList id="DDLFilterListDO" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w69">
                                                                <asp:ListItem Value="doitemno">No. SI</asp:ListItem>
                                                                <asp:ListItem Value="doitemmstoid">No. Draft</asp:ListItem>
                                                            </asp:DropDownList> <asp:TextBox id="txtFilterListDO" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w70"></asp:TextBox> <asp:ImageButton id="btnFindListDO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w71"></asp:ImageButton> <asp:ImageButton id="btnViewAllListDO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w72"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllDO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w73"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneDO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w74"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedDO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w75"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListDO" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w76" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
                                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                            <Columns>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbLMDO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("doitemmstoid") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="doitemmstoid" HeaderText="No. Draft">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Center"
                                                                        VerticalAlign="Middle" Wrap="False" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="doitemno" HeaderText="No. SI">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="dodate" HeaderText="Tanggal">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="custname" HeaderText="Customer" Visible="False">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="doitemmstnote" HeaderText="Note">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="doitemmststatus" HeaderText="Status">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                                HorizontalAlign="Right" />
                                                            <EmptyDataTemplate>
                                                                &nbsp;
                                                            </EmptyDataTemplate>
                                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                        </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListDO" runat="server" __designer:wfdid="w77">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListDO" runat="server" __designer:wfdid="w78">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDO" runat="server" __designer:wfdid="w79" TargetControlID="btnHiddenListDO" BackgroundCssClass="modalBackground" PopupControlID="PanelListDO" PopupDragHandleControlID="lblListDO">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListDO" runat="server" __designer:wfdid="w80" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListMat" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" __designer:wfdid="w82" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Katalog" __designer:wfdid="w83" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" __designer:wfdid="w84" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w85">
                                                                            <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                                                                            <asp:ListItem Value="itemlongdesc">Katalog</asp:ListItem>
                                                                        </asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w86"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w87"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w88"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3>Jenis Barang : <asp:DropDownList id="JenisBarangDDL" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w89">
                                                                            <asp:ListItem>ALL</asp:ListItem>
                                                                            <asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
                                                                            <asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
                                                                            <asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
                                                                            <asp:ListItem>ASSET</asp:ListItem>
                                                                        </asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w90"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w91"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w92"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" __designer:wfdid="w93" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif" __designer:wfdid="w94"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText" __designer:wfdid="w95"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server" __designer:wfdid="w96">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" Width="96%" ForeColor="#333333" __designer:wfdid="w97" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" DataKeyNames="itemcode">
                                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                            <Columns>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Strikeout="False" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="itemlongdesc" HeaderText="Katalog">
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="unit" HeaderText="Unit">
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="JenisNya" HeaderText="Jenis">
                                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                                HorizontalAlign="Right" />
                                                            <EmptyDataTemplate>
                                                                &nbsp;
                                                            </EmptyDataTemplate>
                                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                        </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server" __designer:wfdid="w98">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server" __designer:wfdid="w99">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" __designer:wfdid="w100" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat" Drag="True">
                                        </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" __designer:wfdid="w101" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListCust" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" __designer:wfdid="w103" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer" __designer:wfdid="w104"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" __designer:wfdid="w105" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="DDLFilterListCust" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w106">
                                                            <asp:ListItem Value="custname">Customer</asp:ListItem>
                                                            <asp:ListItem Value="Custcode">Kode</asp:ListItem>
                                                        </asp:DropDownList> <asp:TextBox id="txtFilterListCust" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w107"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w108"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w109"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w110"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w111"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w112"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w113" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" DataKeyNames="custoid,custcode,custname">
                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="cbLMCust" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="custcode" HeaderText="Kode">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Center"
                                                                    VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="custname" HeaderText="Customer">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="gendesc" HeaderText="Cabang">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="custaddr" HeaderText="Address">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                            HorizontalAlign="Right" />
                                                        <EmptyDataTemplate>
                                                        </EmptyDataTemplate>
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListCust" runat="server" __designer:wfdid="w114">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListCust" runat="server" __designer:wfdid="w115">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" __designer:wfdid="w116" TargetControlID="btnHideListCust" BackgroundCssClass="modalBackground" PopupControlID="pnlListCust" PopupDragHandleControlID="lblListCust" Drag="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" __designer:wfdid="w117" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListReg" runat="server">
                                <ContentTemplate>
<asp:Panel id="PanelListReg" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w119" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListReg" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Sales " __designer:wfdid="w120"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListReg" runat="server" Width="100%" __designer:wfdid="w121" DefaultButton="btnFindListReg"><asp:Label id="Label245" runat="server" Text="Filter :" __designer:wfdid="w122"></asp:Label> <asp:DropDownList id="DDLFilterListReg" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w123">
                                                            <asp:ListItem Value="registerno">Nama</asp:ListItem>
                                                            <asp:ListItem Value="PERSONNIP">NIP</asp:ListItem>
                                                        </asp:DropDownList> <asp:TextBox id="txtFilterListReg" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w124"></asp:TextBox> <asp:ImageButton id="btnFindListReg" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w125"></asp:ImageButton> <asp:ImageButton id="btnViewAllListReg" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w126"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllReg" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w127"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneReg" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w128"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedReg" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w129"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListReg" runat="server" Width="98%" ForeColor="Black" __designer:wfdid="w130" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="cbLMPO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("registermstoid") %>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="PERSONNIP" HeaderText="NIP">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Left"
                                                                    VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="registerno" HeaderText="Nama Sales">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="PERSONNIP" HeaderText="Jabatan">
                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="STATUS" HeaderText="Status">
                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                            HorizontalAlign="Right" />
                                                        <EmptyDataTemplate>
                                                        </EmptyDataTemplate>
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToListListReg" runat="server" __designer:wfdid="w131">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListReg" runat="server" __designer:wfdid="w132">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListReg" runat="server" __designer:wfdid="w133" TargetControlID="btnHiddenListReg" BackgroundCssClass="modalBackground" PopupControlID="PanelListReg" PopupDragHandleControlID="lblListReg">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListReg" runat="server" __designer:wfdid="w134" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w136" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w137"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w138"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w139"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w140"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w141" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w142" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                        </th>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

