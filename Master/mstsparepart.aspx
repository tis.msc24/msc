<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstsparepart.aspx.vb" Inherits="Master_mstsparepart" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Maroon" Text=".: Data Sparepart" Width="232px"></asp:Label>
            </th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td>
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="ibfind" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            Filter</td>
                                        <td style="width: 1px">
                                            :</td>
                                        <td>
                                            <asp:DropDownList ID="ddlfilter" runat="server" CssClass="inpText" Width="120px">
                                                <asp:ListItem Value="code">Code Sparepart</asp:ListItem>
                                                <asp:ListItem Value="description">Deskripsi</asp:ListItem>
                                                <asp:ListItem Value="merk">Merk</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="tbfilter" runat="server" CssClass="inpText" Width="176px"></asp:TextBox>
                                            <asp:ImageButton
                                                ID="ibfind" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
                                            <asp:ImageButton
                                                    ID="ibviewall" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" />&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                            <asp:GridView ID="gvdata" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="partoid"
                                ForeColor="#333333" GridLines="None" Style="width: 100%">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:HyperLinkField DataNavigateUrlFields="partoid" DataNavigateUrlFormatString="~/Master/mstsparepart.aspx?oid={0}"
                                        DataTextField="partcode" HeaderText="Code">
                                        <HeaderStyle HorizontalAlign="Center"
                                            Width="15%" CssClass="gvhdr" ForeColor="Black" />
                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                    </asp:HyperLinkField>
                                    <asp:BoundField DataField="partdescshort" HeaderText="Deskripsi">
                                        <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="merk" HeaderText="Merk">
                                        <HeaderStyle CssClass="gvhdr" />
                                        <ItemStyle Width="80px" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Right" Font-Bold="True" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Data not found!"></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            &nbsp;<br />
                        </ContentTemplate>
                        <HeaderTemplate>
                            <span style="font-size: 12px; font-weight: bold;">
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />&nbsp; .: List Of Sparepart</span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: middle" colSpan=3><asp:Label id="lbltitle" runat="server" Font-Size="10px" ForeColor="Red" Visible="False" __designer:wfdid="w24" Font-Underline="True"></asp:Label> <asp:Label id="lblcurrentuser" runat="server" Visible="False" __designer:wfdid="w25"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 156px">Code Sparepart</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 7px">:</TD><TD><asp:TextBox id="tbcode" runat="server" Width="176px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Blue" __designer:wfdid="w26" Enabled="False"></asp:TextBox> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 156px; HEIGHT: 0px">Category Sparepart</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 7px; HEIGHT: 0px">:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 291px; HEIGHT: 0px"><asp:DropDownList id="Category" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w43"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 156px">Deskripsi Pendek&nbsp;<SPAN style="COLOR: red">*</SPAN></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 7px">:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 291px"><asp:TextBox id="tbsdesc" runat="server" Width="464px" CssClass="inpText" __designer:wfdid="w27" MaxLength="150"></asp:TextBox><SPAN style="COLOR: red"></SPAN><BR /><asp:Label id="Label2" runat="server" ForeColor="Red" Text="* Maks. 150 karakter" __designer:wfdid="w28"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 156px">Deskripsi Lengkap</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 7px">:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"><asp:TextBox id="tbldesc" runat="server" Width="464px" Height="64px" CssClass="inpText" __designer:wfdid="w29" TextMode="MultiLine"></asp:TextBox><BR /><asp:Label id="Label3" runat="server" Width="240px" ForeColor="Red" Text="* Maks. 250 karakter" __designer:wfdid="w30"></asp:Label><BR /><asp:RegularExpressionValidator id="RegularExpressionValidator2" runat="server" Width="336px" __designer:wfdid="w31" ValidationExpression="^[\s\S]{0,240}$" ControlToValidate="tbldesc" ErrorMessage="* Warning : Deskripsi lebih dari 250 karakter!"></asp:RegularExpressionValidator></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 156px">Merk</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 7px">:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"><asp:TextBox id="Merk" runat="server" Width="176px" CssClass="inpText" __designer:wfdid="w32" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 156px">Stok Aman Sparepart</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 7px">:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"><asp:TextBox style="TEXT-ALIGN: right" id="tbsafestock" runat="server" Width="56px" CssClass="inpText" __designer:wfdid="w33" MaxLength="8"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTEtbsafestock" runat="server" __designer:wfdid="w1058" TargetControlID="tbsafestock" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 156px">Unit Sparepart</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 7px">:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 180px"><asp:DropDownList id="ddlunit" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w35"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 156px">Catatan</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 7px">:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"><asp:TextBox id="tbnote" runat="server" Width="464px" Height="64px" CssClass="inpTextNr" __designer:wfdid="w36" TextMode="MultiLine"></asp:TextBox><BR /><asp:Label id="Label4" runat="server" Width="336px" ForeColor="Red" Text="* Maks. 250 karakter" __designer:wfdid="w37"></asp:Label><BR /><asp:RegularExpressionValidator id="RegularExpressionValidator3" runat="server" Width="336px" __designer:wfdid="w38" ValidationExpression="^[\s\S]{0,240}$" ControlToValidate="tbnote" ErrorMessage="* Warning : Catatan lebih dari 250 karakter!"></asp:RegularExpressionValidator></TD></TR><TR><TD colSpan=3><asp:Label id="lblcreate" runat="server" __designer:wfdid="w39"></asp:Label> </TD></TR><TR><TD colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibsave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w40"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False" __designer:wfdid="w41"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" runat="server" ImageUrl="~/Images/Delete.png" Visible="False" __designer:wfdid="w42"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <span style="font-size: 12px; font-weight: bold;">&nbsp;<asp:ImageButton ID="ImageButton1"
                                runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />&nbsp; .:
                                Form Sparepart</span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel id="uppopupmsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpepopupmsg" runat="server" Drag="True" PopupDragHandleControlID="lblcaption" BackgroundCssClass="modalBackground" PopupControlID="pnlpopupmsg" DropShadow="True" TargetControlID="bepopupmsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bepopupmsg" runat="server" BorderColor="Transparent" Visible="False" BackColor="Transparent" BorderStyle="None" CausesValidation="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

