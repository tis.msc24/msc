Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports Microsoft.Office.Interop
Imports System.Data.OleDb

Partial Class Master_mstPricelist
    Inherits System.Web.UI.Page


#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public BranchCode As String = ConfigurationSettings.AppSettings("BranchCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Private statusReport As New ReportDocument
    Private myDiskFileDestinationOptions As DiskFileDestinationOptions
    Private myExportOptions As ExportOptions
    Dim CProc As New ClassProcedure
    Public folderReport As String = "~/report/"
    Dim report As New ReportDocument
    Dim oRegex As Regex
    Dim oMatches As MatchCollection
#End Region


#Region "Procedure"

    Private Sub FillDetailPrice(ByVal iOid As String)
        'sSql = "SELECT genoid," & iOid & " promooid,left(gencode,2) gencode,gendesc,isnull((select itemavailable " & _
        '       "from QL_mstPricelistAvailable Where cmpcode=g.cmpcode and promooid= " & iOid & "),'0') itemavailable " & _
        '       "FROM QL_mstGen g WHERE gengroup='OUTLET' "
        'Dim tbDtl As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstGen")
        'If tbDtl.Rows.Count > 0 Then
        '    Session("OutletDtl") = tbDtl

        '    Dim objTable2 As DataTable
        '    objTable2 = Session("PriceDtl")
        '    objTable2.Rows.Clear() : Dim objRow As DataRow

        '    For C1 As Integer = 0 To tbDtl.Rows.Count - 1
        '        objRow = objTable2.NewRow()
        '        objRow("outletoid") = tbDtl.Rows(C1)("genoid").ToString
        '        objRow("promooid") = tbDtl.Rows(C1)("promooid").ToString
        '        objRow("outletcode") = tbDtl.Rows(C1)("gencode").ToString
        '        objRow("outletdesc") = tbDtl.Rows(C1)("gendesc").ToString
        '        objRow("itemavailable") = tbDtl.Rows(C1)("itemavailable").ToString
        '        objTable2.Rows.Add(objRow)
        '    Next

        '    Session("PriceDtl") = objTable2
        '    gvPriceDtl.DataSource = objTable2
        '    gvPriceDtl.DataBind()
        'Else
        '    Session("PriceDtl") = Nothing
        '    gvPriceDtl.DataSource = Nothing : gvPriceDtl.DataBind()
        '    showMessage("Gagal membuka data Price !!", CompnyName & " - ERROR") : Exit Sub
        'End If
        'UpdateDetail()
    End Sub

    Private Function SetTableDetailPrice(ByVal sName As String) As DataTable
        Dim nuTable As New DataTable(sName)
        nuTable.Columns.Add("outletoid", Type.GetType("System.Int32"))
        nuTable.Columns.Add("promooid", Type.GetType("System.Int32"))
        nuTable.Columns.Add("outletcode", Type.GetType("System.String"))
        nuTable.Columns.Add("outletdesc", Type.GetType("System.String"))
        nuTable.Columns.Add("itemavailable", Type.GetType("System.String"))
        Return nuTable
    End Function

    Protected Sub filledSelectedDay(ByVal promflag As String)
        For i As Integer = 0 To cbDay.Items.Count - 1
            cbDay.Items(i).Selected = False
        Next
        Dim selectedchar As String = ""
        For i As Integer = 1 To promflag.Length
            selectedchar = Mid(promflag, i, 1)
            Select Case Trim(selectedchar)
                Case "1"
                    cbDay.Items(0).Selected = True
                Case "2"
                    cbDay.Items(1).Selected = True
                Case "3"
                    cbDay.Items(2).Selected = True
                Case "4"
                    cbDay.Items(3).Selected = True
                Case "5"
                    cbDay.Items(4).Selected = True
                Case "6"
                    cbDay.Items(5).Selected = True
                Case "7"
                    cbDay.Items(6).Selected = True
            End Select
        Next
    End Sub
    Sub UpdateDetail()
        If Not Session("PriceDtl") Is Nothing Then
            Dim iError As Int16 = 0
            Dim objTable As DataTable
            objTable = Session("PriceDtl")
            Try
                For c1 As Integer = 0 To objTable.Rows.Count - 1
                    iError = c1

                    Dim row As System.Web.UI.WebControls.GridViewRow = gvPriceDtl.Rows(c1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc2 As System.Web.UI.ControlCollection = row.Cells(2).Controls
                        For Each myControl As System.Web.UI.Control In cc2
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("ITEMAVAILABLE") = "1"
                                Else
                                    Session("ITEMAVAILABLE") = "0"
                                End If
                            End If
                        Next

                        'update to session datatable
                        Dim dr As DataRow = objTable.Rows(c1)
                        dr.BeginEdit()
                        dr(4) = Session("ITEMAVAILABLE")
                        dr.EndEdit()

                    End If
                Next

                Session("PriceDtl") = objTable
                gvPriceDtl.DataSource = objTable
                gvPriceDtl.DataBind()

            Catch ex As Exception
                showMessage(ex.Message, CompnyName & " - ERROR ")
            End Try
        End If
    End Sub

    Sub removeList(ByVal sessionname As String, ByVal gvName As String, ByVal btnName As String, ByVal checkCol As Integer)
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldtlPromo")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = objRow.Length() - 1 To 0 Step -1
                If cKoneksi.getCheckBoxValue(i, checkCol, gvPromoDtl) = True Then
                    objTable.Rows.Remove(objRow(i))
                End If
            Next
            Session("tbldtlPromo") = objTable
            gvPromoDtl.DataSource = Session("tbldtlPromo")
            gvPromoDtl.DataBind()
        End If
        ibDelete.Visible = (objTable.Rows.Count > 0)
    End Sub

    Protected Sub initDDLAll()
        sSql = "SELECT genoid,gendesc FROM QL_mstgen where gengroup='itemgroup'"
        FillDDL(ddlGroup, sSql)
        ddlGroup.Items.Add(New ListItem("SEMUA GRUP", "ALL GRUP"))
        ddlGroup.SelectedValue = "ALL GRUP"
        FillDDL(ddlSubGroup, "SELECT genoid,gendesc FROM QL_mstgen where gengroup='itemsubgroup'")
        ddlSubGroup.Items.Add(New ListItem("SEMUA SUB GRUP", "ALL GRUP"))
        ddlSubGroup.SelectedValue = "ALL GRUP"
        FillDDL(ddlSubGroup2, "SELECT genoid,gendesc FROM QL_mstgen where gengroup='itemsubgroup'")
        ddlSubGroup2.Items.Add(New ListItem("SEMUA SUB GRUP", "ALL GRUP"))
        ddlSubGroup2.SelectedValue = "ALL GRUP"
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Sub ClearDtlInput()
        pergroupdisc.Text = "0"
        pergroupdisc2.Text = "0"
        pergroupdisc3.Text = "0"
        persubgroupdisc.Text = "0"
        persubgroupdisc2.Text = "0"
        persubgroupdisc3.Text = "0"
        amountdisc1.Text = "0"
        amountdisc2.Text = "0"
        amountdisc3.Text = "0"
        Subsidi.Text = "0"
        konversi1_2.Text = ""
        konversi2_3.Text = ""
        txtmerk.Text = ""
        itemdesc1.Text = ""
        itemoid.Text = ""
        itemdesc2.Text = ""
        itemoid2.Text = ""
        ddlGroup.SelectedIndex = 0
        'ddlSubGroup.SelectedIndex = 0
    End Sub

    Sub ClearMstInput()
        promoid.Text = ""
        promeventcode.Text = ""
        promeventname.Text = ""
        promstartdate.Text = ""
        promfinishdate.Text = ""
        promstarttime.Text = ""
        promfinishtime.Text = ""
        promtype.SelectedIndex = 0
        membercardrole.SelectedIndex = 0
        note.Text = ""
        autoresponse.Text = ""
        generalSMSTemplate.Text = ""
        qtysmsterkirim.Text = "0"
        lastsmssend.Text = "-"
    End Sub

    Private Sub binddataMenu()

        sSql = "SELECT top 1000 itemoid, itemdesc itemshortdesc,g.gendesc,merk,pricelist,konversi1_2,konversi2_3 FROM  QL_mstItem i inner join ql_mstgen g ON g.cmpcode=i.cmpcode AND g.genoid=i.itemgroupoid where i.cmpcode='" & CompnyCode & "'  and itemdesc like '%" & Tchar(filterMenuItem.Text) & "%' and itemflag = 'aktif'"
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        If objDs.Tables("data").Rows.Count > 0 Then
            gvMenu.Visible = True
            gvMenu.DataSource = objDs.Tables("data")
            Session("dataMenu") = objDs.Tables("data")
            gvMenu.DataBind()
            'lblmsg.Visible = False
        Else
            gvMenu.Visible = True
            gvMenu.DataSource = objDs.Tables("data")
            Session("dataMenu") = objDs.Tables("data")
            gvMenu.DataBind()
            'lblmsg.Visible = True
        End If

    End Sub

    Private Sub insertingDtlPromo(ByVal promoid As Integer, ByVal OidTemp As Integer)
        Dim objTable As DataTable
        objTable = Session("tbldtlPromo")

        Dim hargaecer As Integer = 0
        Dim hargapartai As Integer = 0
        Dim hargaqty As Integer = 0

        For c1 As Int16 = 0 To objTable.Rows.Count - 1
            sSql = "INSERT INTO [QL_mstPricelistDtl]([cmpcode], [promdtloid], [promoid], [itemoid], [amountdisc], [itemoid2], qtyitemoid1, qtyitemoid2,subsidi1,amountdisc2,amountdisc3)" & _
                    "VALUES('" & CompnyCode & "', " & OidTemp + c1 & ", " & promoid & ", " & Tnumber(objTable.Rows(c1).Item("itemoid").ToString) & ", " & ToDouble(objTable.Rows(c1).Item("amountdisc")) & ", " & Tnumber(objTable.Rows(c1).Item("itemoid2").ToString) & ", " & objTable.Rows(c1).Item("qty1") & "," & objTable.Rows(c1).Item("qty2") & "," & objTable.Rows(c1).Item("subsidi") & " , " & ToDouble(objTable.Rows(c1).Item("amountdisc2")) & ", " & ToDouble(objTable.Rows(c1).Item("amountdisc3")) & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            If status.Text = "POSTED" Then
                'hargaecer = ToDouble(objTable.Rows(c1).Item("subsidi")) - (ToDouble(objTable.Rows(c1).Item("subsidi")) * ToDouble(objTable.Rows(c1).Item("amountdisc3")) / 100)
                'hargapartai = (ToDouble(objTable.Rows(c1).Item("subsidi")) - (ToDouble(objTable.Rows(c1).Item("subsidi")) * ToDouble(objTable.Rows(c1).Item("amountdisc2")) / 100)) * ToDouble(objTable.Rows(c1).Item("qty2"))
                'hargaqty = (ToDouble(objTable.Rows(c1).Item("subsidi")) - (ToDouble(objTable.Rows(c1).Item("subsidi")) * ToDouble(objTable.Rows(c1).Item("amountdisc")) / 100)) * ToDouble(objTable.Rows(c1).Item("qty2")) * ToDouble(objTable.Rows(c1).Item("qty1"))

                'sSql = "update [QL_mstitem] set  itempriceunit1 = " & ToDouble(hargaqty) & " , itempriceunit2 = " & ToDouble(hargapartai) & " , itempriceunit3 = " & ToDouble(hargaecer) & " ,pricelist = " & ToDouble(objTable.Rows(c1).Item("subsidi")) & " ,discunit1 = " & ToDouble(objTable.Rows(c1).Item("amountdisc")) & " ,discunit2 = " & ToDouble(objTable.Rows(c1).Item("amountdisc2")) & " ,discunit3 = " & ToDouble(objTable.Rows(c1).Item("amountdisc3")) & " ,updtime = CURRENT_TIMESTAMP  ,upduser = '" & Tchar(UpdUser.Text) & "' where itemoid = " & ToDouble(objTable.Rows(c1).Item("itemoid"))
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()

            End If


        Next
        sSql = "update  QL_mstoid set lastoid=" & OidTemp + objTable.Rows.Count - 1 & " where tablename = 'QL_mstPricelistDtl' and cmpcode = '" & CompnyCode & "' "
        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()
    End Sub

    Private Sub BindData(ByVal sSqlPlus As String)
        sSql = "SELECT [promoid], [promeventcode], [promeventname],promnote,postdate , promflag " & _
            "FROM QL_mstPricelist  " & _
            "WHERE cmpcode='" & CompnyCode & "' " & sSqlPlus & " ORDER BY promeventcode ASC" 'AND m.memberstatus NOT IN ('AKTIF', 'NON AKTIF') ORDER BY m.name"
        FillGV(GVMst, sSql, "QL_mstPricelist")
    End Sub

    Sub FillTextBox(ByVal vSearchKey As String)
        Dim personid As String = ""
        sSql = "SELECT [cmpcode], [promoid], [promeventcode], [promeventname], [promflag], [promnote], [crtuser], [upduser], [updtime],postdate FROM [QL_mstPricelist] " & _
                "where promoid=" & vSearchKey
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xreader = xCmd.ExecuteReader
        BtnDelete.Enabled = True

        While xreader.Read
            promoid.Text = Trim(xreader.GetValue(1))
            promeventcode.Text = Trim(xreader.GetValue(2))
            promeventname.Text = Trim(xreader.GetValue(3))
            promstartdate.Text = Format(CDate(xreader.GetValue(9)), "dd/MM/yyyy")
            'promfinishdate.Text = Format(CDate(xreader.GetValue(5)), "dd/MM/yyyy")
            'promstarttime.Text = Trim(xreader.GetValue(6))
            'promfinishtime.Text = Trim(xreader.GetValue(7))
            'promtype.SelectedValue = Trim(xreader.GetValue(8))
            'membercardrole.SelectedValue = Trim(xreader.GetValue(9))
            note.Text = Trim(xreader.GetValue(5))
            'autoresponse.Text = Trim(xreader.GetValue(14))
            'generalSMSTemplate.Text = Trim(xreader.GetValue(15))
            'qtysmsterkirim.Text = Trim(xreader.GetValue(16))
            status.Text = Trim(xreader("promflag"))

            'If Trim(xreader.GetValue(17)) = "1/1/1900" Then
            '    lastsmssend.Text = "-"
            'Else
            '    lastsmssend.Text = Trim(xreader.GetValue(14))
            'End If

            UpdUser.Text = Trim(xreader.GetValue(7))
            Updtime.Text = Format(CDate(Trim(xreader.GetValue(8).ToString)), "MM/dd/yyyy HH:mm:ss")

        End While

        xreader.Close()
        conn.Close()

        If status.Text = "POSTED" Or status.Text = "In Approval" Or status.Text = "Approved" Or status.Text = "Updated" Then
            btnSave.Visible = False
            BtnDelete.Visible = False
            posting.Visible = False
            sendApproval.Visible = False
        Else
            btnSave.Visible = True
            BtnDelete.Visible = True
            posting.Visible = False
            sendApproval.Visible = True
        End If

    End Sub

#End Region

#Region "Functions"

    Public Function returnAvailable(ByVal sAvailable As String) As Boolean
        If sAvailable = "1" Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 15
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xoutletoid As String = Session("outletoid")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstPricelist.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If


        Page.Title = CompnyName & " - Master Pricelist"
        Session("oid") = Request.QueryString("oid")

        Me.BtnDelete.Attributes.Add("onclick", "return confirm('Are u sure want to delete ??');")

        If Not IsPostBack Then
            Dim sPlus As String = ""
            sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' and promflag " & IIf(ddlStatus.SelectedValue = "Outstanding", "<> 'Updated' ", "= 'Updated'")
            BindData(sPlus)
            initDDLAll()
            setTableDetailPromo()
            viewDetailPromo()

            If Session("PriceDtl") Is Nothing Then
                Dim dtlNew As DataTable = SetTableDetailPrice("QL_mstItemPrice")
                Session("PriceDtl") = dtlNew
            End If

            If Session("oid") Is Nothing Or Session("oid") = "" Then
                promoid.Text = ClassFunction.GenerateID("QL_mstPricelist", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
                qtysmsterkirim.Text = 0
                lastsmssend.Text = "-"

                UpdUser.Text = Session("UserID")
                Updtime.Text = GetServerTime()
                BtnDelete.Enabled = False

                ClearDtlInput()
                'FillDetailPrice("0")
                ddlInsertType.SelectedIndex = 0
                ddlInsertType_SelectedIndexChanged(sender, e)
                promtype_SelectedIndexChanged(sender, e)
                BtnDelete.Visible = False
            Else
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                'FillDetailPrice(Session("oid"))
                ddlInsertType.SelectedIndex = 0
                ddlInsertType_SelectedIndexChanged(sender, e)
                promtype_SelectedIndexChanged(sender, e)
                'PromoDtl
                Dim dttempPromoDtl As New DataTable
                If promtype.SelectedValue.Trim.ToUpper = "BUY NX GET NY" Then
                    sSql = "SELECT DISTINCT [promdtloid], [itemoid], (SELECT itemdesc FROM QL_MstItem i WHERE i.itemoid=d.itemoid) + ' (' + cast(qtyitemoid1 as varchar) + ')' itemdesc, [amountdisc],[amountdisc2],[amountdisc3], [itemoid2], (SELECT itemdesc FROM QL_MstItem i WHERE i.itemoid=d.itemoid2) + ' (' + cast(qtyitemoid2 as varchar) + ')' itemdesc2, qtyitemoid1 qty1, qtyitemoid2 qty2 ,subsidi1 subsidi ,'False' CheckValue FROM QL_mstPricelistDtl d where cmpcode='" & CompnyCode & "' and promoid=" & Session("oid")
                Else
                    sSql = "SELECT DISTINCT [promdtloid], [itemoid], (SELECT itemdesc + ' - ' + merk FROM QL_MstItem i WHERE i.itemoid=d.itemoid) itemdesc, [amountdisc],[amountdisc2],[amountdisc3], [itemoid2], (SELECT itemdesc FROM QL_MstItem i WHERE i.itemoid=d.itemoid2) itemdesc2, qtyitemoid1 qty1, qtyitemoid2 qty2 ,subsidi1 subsidi,'False' CheckValue FROM QL_mstPricelistDtl d where cmpcode='" & CompnyCode & "' and promoid=" & Session("oid")
                End If

                dttempPromoDtl = cKoneksi.ambiltabel(sSql, "QL_mstPricelistDtl")
                Session("tbldtlPromo") = dttempPromoDtl

                If Session("tbldtlPromo").rows.count > 0 Then
                    viewDetailPromo()
                End If
                ClearDtlInput()
                If promtype.SelectedValue.ToUpper.Trim = "BUY NX GET NY" Then
                    TD1.Visible = True
                    TD2.Visible = True
                    TD3.Visible = True
                    TD4.Visible = True
                Else
                    TD1.Visible = False
                    TD2.Visible = False
                    TD3.Visible = False
                    TD4.Visible = False
                    qty1.SelectedValue = 1
                    qty2.SelectedValue = 1
                End If
            End If
        End If
    End Sub

    Protected Sub ddlInsertType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInsertType.SelectedIndexChanged
        If ddlInsertType.SelectedValue = "GROUP" Then
            tblGroup.Visible = True
            tblSubGroup.Visible = False
            tblMenu.Visible = False
            lblmerk.Visible = True
            Merkfilter.Visible = True
            Merkfilter.Text = ""
            printExcel.Visible = True
        ElseIf ddlInsertType.SelectedValue = "SUBGROUP" Then
            tblGroup.Visible = False
            tblSubGroup.Visible = True
            tblMenu.Visible = False
            lblmerk.Visible = True
            Merkfilter.Visible = True
            Merkfilter.Text = ""
            printExcel.Visible = True
        ElseIf ddlInsertType.SelectedValue = "Item" Then
            tblGroup.Visible = False
            tblSubGroup.Visible = False
            tblMenu.Visible = True
            lblmerk.Visible = False
            Merkfilter.Visible = False
            Merkfilter.Text = ""
            printExcel.Visible = False
        End If
    End Sub

    Private Sub setTableDetailPromo()
        If Session("tbldtlPromo") Is Nothing Then
            Dim tblDetail As DataTable
            tblDetail = New DataTable("QL_mstPricelistDtl")
            tblDetail.Columns.Add("promdtloid", Type.GetType("System.Int32")) '0
            tblDetail.Columns.Add("itemoid", Type.GetType("System.Int32")) '1
            tblDetail.Columns.Add("itemdesc", Type.GetType("System.String")) '2
            tblDetail.Columns.Add("amountdisc", Type.GetType("System.Decimal")) '3
            tblDetail.Columns.Add("itemoid2", Type.GetType("System.Int32")) '4
            tblDetail.Columns.Add("itemdesc2", Type.GetType("System.String")) '5
            tblDetail.Columns.Add("qty1", Type.GetType("System.Int32")) '6
            tblDetail.Columns.Add("qty2", Type.GetType("System.Int32")) '7
            tblDetail.Columns.Add("subsidi", Type.GetType("System.Decimal")) '8
            tblDetail.Columns.Add("amountdisc2", Type.GetType("System.Decimal")) '9
            tblDetail.Columns.Add("amountdisc3", Type.GetType("System.Decimal")) '10
            tblDetail.Columns.Add("CheckValue", Type.GetType("System.String")) '11
            Session("tbldtlPromo") = tblDetail
        End If
    End Sub

    Private Sub viewDetailPromo()
        If Not (Session("tbldtlPromo")) Is Nothing Then
            gvPromoDtl.DataSource = Session("tbldtlPromo")
            gvPromoDtl.Visible = True
            gvPromoDtl.DataBind()
        Else
            gvPromoDtl.Visible = False
        End If
        ' btnRemoveRelative.Visible = CType(Session("tblRELATIVE"), DataTable).Rows.Count > 0
    End Sub

    Private Function listPromoDtl(ByVal itemoid As Integer, ByVal itemdesc1 As String, ByVal amountdisc1 As Double, ByVal amountdisc2 As Double, ByVal amountdisc3 As Double, ByVal itemoid2 As Integer, ByVal itemdesc2 As String, ByVal MenuSdhAda As String, ByVal subsidi As Double, ByVal konversi1_2 As Integer, ByVal konversi2_3 As Integer) As String

        Try
            Dim dv As DataView
            Dim dv2 As DataView

            Dim tbldtlPromo As DataTable = Session("tbldtlPromo")
            dv = tbldtlPromo.DefaultView
            dv.RowFilter = "itemoid=" & CStr(itemoid) & " or itemoid2= " & CStr(itemoid)

            If dv.Count > 0 Then
                '    showMessage("This item has been inputed!!", "INFORMATION", 2)
                '    Exit Sub
                MenuSdhAda += " " & itemdesc1 & ","
            End If
            dv.RowFilter = ""

            dv2 = tbldtlPromo.DefaultView
            dv2.RowFilter = "itemoid=" & CStr(itemoid2) & " or itemoid2= " & CStr(itemoid2)

            If itemoid2 <> 0 Then   'abaikan jika itemke2 tidak dipilih          
                If dv2.Count > 0 Then
                    MenuSdhAda += " " & itemdesc2 & ","
                End If
            End If

            dv2.RowFilter = ""

            If MenuSdhAda = "" Then
                Dim dr As DataRow = tbldtlPromo.NewRow
                dr(0) = 0
                dr(1) = itemoid
                If promtype.SelectedValue.ToUpper.Trim = "BUY NX GET NY" Then
                    dr(2) = Tchar(itemdesc1) & " (" & qty1.SelectedValue & ")"
                    dr(5) = Tchar(itemdesc2) & " (" & qty2.SelectedValue & ")"
                Else
                    dr(2) = Tchar(itemdesc1)
                    dr(5) = Tchar(itemdesc2)
                End If
                dr(3) = amountdisc1
                dr(4) = itemoid2
                dr(6) = konversi1_2
                dr(7) = konversi2_3
                dr(8) = subsidi
                dr(9) = amountdisc2
                dr(10) = amountdisc3
                dr(11) = "False"
                tbldtlPromo.Rows.Add(dr)
            End If
            dv.RowFilter = ""
            dv2.RowFilter = ""
            Session("tbldtlPromo") = tbldtlPromo

            'gvPromoDtl.DataSource = tbldtlPromo.DefaultView
            'gvPromoDtl.DataBind()
            'btnRemoveITEM.Visible = (GVItem.Rows.Count > 0)
        Catch ex As Exception
            showMessage(ex.ToString, "ERROR")
        End Try
        Return (MenuSdhAda)
    End Function

    Protected Sub imbFindMenu1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMenu1.Click
        filterMenuItem.Text = ""
        filterMenuItem.Text = itemdesc1.Text
        binddataMenu()
        lblstate.Text = "imbFindMenu1"
        gvMenu.Visible = True
        PanelUser.Visible = True
        BtnUser.Visible = True
        MpeUser.Show()
    End Sub

    Protected Sub imbClearMenu1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearMenu1.Click
        itemdesc1.Text = ""
        itemoid.Text = ""
    End Sub

    Protected Sub imbFindMenu2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMenu2.Click
        filterMenuItem.Text = ""
        filterMenuItem.Text = itemdesc2.Text
        binddataMenu()
        lblstate.Text = "imbFindMenu2"
        gvMenu.Visible = True
        PanelUser.Visible = True
        BtnUser.Visible = True
        MpeUser.Show()
    End Sub

    Protected Sub imbClearMenu2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearMenu2.Click
        itemdesc2.Text = ""
        itemoid2.Text = ""
    End Sub

    Protected Sub gvMenu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMenu.SelectedIndexChanged
        If lblstate.Text = "imbFindMenu1" Then
            itemoid.Text = gvMenu.SelectedDataKey.Item(0).ToString
            itemdesc1.Text = gvMenu.SelectedDataKey.Item(1).ToString
            Subsidi.Text = NewMaskEdit(gvMenu.SelectedDataKey.Item(2).ToString)
            txtmerk.Text = gvMenu.SelectedDataKey.Item(3).ToString
            konversi1_2.Text = gvMenu.SelectedDataKey.Item(4).ToString
            konversi2_3.Text = gvMenu.SelectedDataKey.Item(5).ToString
        ElseIf lblstate.Text = "imbFindMenu2" Then
            itemoid2.Text = gvMenu.SelectedDataKey.Item(0).ToString
            itemdesc2.Text = gvMenu.SelectedDataKey.Item(1).ToString
        End If
        PanelUser.Visible = False
        BtnUser.Visible = False
        MpeUser.Hide()
    End Sub

    Protected Sub LBCloseMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseMenu.Click
        PanelUser.Visible = False
        BtnUser.Visible = False
        MpeUser.Hide()
    End Sub

    Protected Sub LBAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBAddMenu.Click
        Dim MenuSdhAda As String = ""

        If ddlInsertType.SelectedValue = "GROUP" Then
            If ddlGroup.SelectedValue = "ALL GRUP" And ddlSubGroup2.SelectedValue = "ALL GRUP" And Merkfilter.Text.Trim = "" Then
                showMessage("Please choose Group or Sub Group or Merk first.", " Warning - " & CompnyCode)
                Exit Sub
            End If

            sSql = "SELECT itemoid, itemdesc + ' - ' + merk itemdesc ,pricelist , konversi1_2 , konversi2_3 FROM QL_mstItem WHERE " & _
                   " cmpcode='" & CompnyCode & "' and merk like '%" & Merkfilter.Text & "%' " & IIf(ddlGroup.SelectedValue = "ALL GRUP", "  ", " and itemgroupoid = " & ddlGroup.SelectedValue & " ") & " " & IIf(ddlSubGroup2.SelectedValue = "ALL GRUP", "  ", " and itemsubgroupoid = " & ddlSubGroup2.SelectedValue & " ") & " order by itemdesc"
        ElseIf ddlInsertType.SelectedValue = "SUBGROUP" Then
            If ddlSubGroup2.SelectedValue = "ALL GRUP" And Merkfilter.Text.Trim = "" Then
                showMessage("Please choose Sub Group or Merk first.", " Warning - " & CompnyCode)
                Exit Sub
            End If
            sSql = "SELECT itemoid, itemdesc + ' - ' + merk itemdesc ,pricelist,konversi1_2,konversi2_3 FROM QL_mstItem i inner join ql_mstgen g on i.cmpcode=g.cmpcode " & _
                   "and g.genoid=i.itemsubgroupoid WHERE i.cmpcode='" & CompnyCode & "' " & IIf(ddlSubGroup.SelectedValue = "ALL GRUP", "  ", " and itemsubgroupoid = " & ddlSubGroup.SelectedValue & " ") & " and merk like '%" & Merkfilter.Text & "%' order by itemdesc"
        ElseIf ddlInsertType.SelectedValue = "Item" Then
            Try
                MenuSdhAda = listPromoDtl(CInt(itemoid.Text), itemdesc1.Text & " - " & txtmerk.Text, ToDouble(amountdisc1.Text), ToDouble(amountdisc2.Text), ToDouble(amountdisc3.Text), CInt(IIf(itemoid2.Text = "", "0", itemoid2.Text)), itemdesc2.Text, MenuSdhAda, CInt(Subsidi.Text), ToDouble(konversi1_2.Text), ToDouble(konversi1_2.Text))
            Catch ex As Exception
            End Try
        End If

        If ddlInsertType.SelectedValue <> "Item" Then
            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDs As New DataSet
            mySqlDA.Fill(objDs, "data")
            If objDs.Tables("data").Rows.Count > 0 Then
                For i As Int16 = 0 To objDs.Tables("data").Rows.Count - 1
                    If ddlInsertType.SelectedValue = "GROUP" Then
                        MenuSdhAda = listPromoDtl(CInt(objDs.Tables("data").Rows(i).Item("itemoid").ToString), objDs.Tables("data").Rows(i).Item("itemdesc").ToString, ToDouble(pergroupdisc.Text), ToDouble(pergroupdisc2.Text), ToDouble(pergroupdisc3.Text), 0, "", MenuSdhAda, CInt(objDs.Tables("data").Rows(i).Item("pricelist").ToString), CInt(objDs.Tables("data").Rows(i).Item("konversi1_2").ToString), CInt(objDs.Tables("data").Rows(i).Item("konversi2_3").ToString))
                    ElseIf ddlInsertType.SelectedValue = "SUBGROUP" Then
                        MenuSdhAda = listPromoDtl(CInt(objDs.Tables("data").Rows(i).Item("itemoid").ToString), objDs.Tables("data").Rows(i).Item("itemdesc").ToString, ToDouble(persubgroupdisc.Text), ToDouble(persubgroupdisc2.Text), ToDouble(persubgroupdisc3.Text), 0, "", MenuSdhAda, CInt(objDs.Tables("data").Rows(i).Item("pricelist").ToString), CInt(objDs.Tables("data").Rows(i).Item("konversi1_2").ToString), CInt(objDs.Tables("data").Rows(i).Item("konversi2_3").ToString))
                    End If
                Next
            End If
        End If

        If MenuSdhAda <> "" Then
            lblmenuygada.Text = "List of menu has been added : " & MenuSdhAda.Remove(MenuSdhAda.Length - 1)
            MenuSdhAda = ""
        Else
            lblmenuygada.Text = ""
        End If

        ClearDtlInput()
        viewDetailPromo()
        ibDelete.Visible = True
    End Sub

    Protected Sub gvPromoDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPromoDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = NewMaskEdit(ToDouble(e.Row.Cells(1).Text))
            e.Row.Cells(2).Text = NewMaskEdit(ToDouble(e.Row.Cells(2).Text))
            e.Row.Cells(3).Text = NewMaskEdit(ToDouble(e.Row.Cells(3).Text))
            e.Row.Cells(4).Text = NewMaskEdit(ToDouble(e.Row.Cells(4).Text))
            'e.Row.Cells(11).Text = NewMaskEdit(ToDouble(e.Row.Cells(11).Text))
        End If
    End Sub

    Protected Sub gvPromoDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPromoDtl.RowDeleting
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldtlPromo")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

        objTable.Rows.Remove(objRow(e.RowIndex))

        Session("tbldtlPromo") = objTable
        gvPromoDtl.DataSource = Session("tbldtlPromo")
        gvPromoDtl.DataBind()

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Validasi.Text = ""
        Dim s1, s2 As Boolean
        s1 = False : s2 = False
        If promeventcode.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Code""! <br/>"
        End If
        If promeventname.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Pricelist Reason""! <br/>"
        End If

        If IsDate(toDate(promstartdate.Text)) = False Then
            'Validasi.Text = "Invalid PO Date !"
            Validasi.Text &= "Format tanggal Postdate salah ""! <br/>"

        Else
            If CDate(toDate(promstartdate.Text)) < CDate("01/01/1900") Then
                Validasi.Text &= "Format tanggal Postdate salah ""! <br/>"
            End If
            If CDate(toDate(promstartdate.Text)) > CDate(toDate("6/6/2079")) Then
                Validasi.Text &= "Tanggal Postdate harus kurang dari 06/06/2079 ""! <br/>"
            End If
            If CDate(toDate(promstartdate.Text)) < Format(Date.Now, "MM/dd/yyyy") Then
                Validasi.Text &= "Postdate tidak bisa kurang dari tanggal sekarang""! <br/>"
            End If

        End If



        If (Session("tbldtlPromo")) Is Nothing Then
            Validasi.Text &= "- No Item Data !!<br/>"
        Else
            Dim objTableCek As DataTable = (Session("tbldtlPromo"))
            If objTableCek.Rows.Count = 0 Then
                Validasi.Text &= "- No Item Data !!<br/>"
            End If
        End If

        If Validasi.Text <> "" Then
            status.Text = "In Process"
            showMessage(Validasi.Text, CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim strday As String = ""
        For i As Integer = 0 To cbDay.Items.Count - 1
            If cbDay.Items(i).Selected Then
                strday = strday & cbDay.Items(i).Value
            End If
        Next

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM QL_mstPricelist WHERE " & _
                                   "promeventcode = '" & Tchar(promeventcode.Text) & "' "
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck1 &= " AND promoid <> " & promoid.Text
        End If
        If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
            showMessage("Code already used !!", CompnyName & " - WARNING")
            status.Text = "In Process"
            Exit Sub
        End If

        If status.Text = "" Then
            status.Text = "In Process"
        ElseIf status.Text = "In Approval" Then

            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus " & _
                                              "from QL_approvalstructure " & _
                                              "WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_Pricelist' order by approvallevel"


            Dim dtData2 As DataTable = cKoneksi.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                showMessage("Tiada Approval User untuk Pricelist, Silahkan contact admin dahulu", CompnyName & " - WARNING")
                status.Text = "In Process"
                Exit Sub
            End If



            'Dim dtTempWeb As DataTable = checkApprovaldata("QL_POMST", "In Approval", Session("oid"), "", "New")

            Dim objTransApproval As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTransApproval = conn.BeginTransaction()
            xCmd.Transaction = objTransApproval

            Session("AppOid") = ClassFunction.GenerateID("QL_Approval", CompnyCode)
            Try
                If Session("oid") <> Nothing Or Session("oid") <> "" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus) VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", '" & "PL" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_Pricelist', '" & Session("oid") & "','In Approval','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1', '" & objTable.Rows(c1).Item("approvalstatus") & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Next

                        sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + _
                             objTable.Rows.Count - 1 & " " & _
                             "where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If

                    'sSql = "update QL_pomst set pomststatus='In Approval', upduser='" & Session("UserID") & "', updatetime=CURRENT_TIMESTAMP " & _
                    '        "WHERE cmpcode='" & CompnyCode & "' and  pomstoid=" & Session("oid")
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                Else
                    showMessage("Silahkan Simpan Pricelist terlebih dahulu.", CompnyName & " - Warning")
                    status.Text = "In Process"
                    Exit Sub
                End If
                objTransApproval.Commit()
                xCmd.Connection.Close()
            Catch ex As OleDb.OleDbException
                objTransApproval.Rollback()
                xCmd.Connection.Close()
                showMessage(ex.ToString, CompnyName & " - ERROR")
                status.Text = "In Process"
                Exit Sub
            End Try
            'Session("TblDtl") = Nothing
            '-------------------------------------------------------------------------------------------------------------------------------------

        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            UpdUser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)

            Dim OidTempDtl As Integer = GenerateID("QL_mstPricelistDtl", CompnyCode)

            If Session("oid") = Nothing Or Session("oid") = "" Then

                '[promstartdate], [promfinishdate], [promstarttime], [promfinishtime], [promtype], [membercardrole], [promstdpoints], [promstdamtsales], [autoresponse], [generalSMSTemplate], [qtysmsterkirim], [lastsmssend], [crtuser], [upduser], [updtime],[dayFlag], "'" & CDate(toDate(promstartdate.Text)) & " " & promstarttime.Text & "','" & CDate(toDate(promfinishdate.Text)) & " " & promfinishtime.Text & "', '" & Tchar(promstarttime.Text) & "', '" & Tchar(promfinishtime.Text) & "', '" & Tchar(promtype.SelectedValue) & "', '" & Tchar(membercardrole.SelectedValue) & "', 0,0, " & _ "'" & Tchar(autoresponse.Text) & "', " & _ "'" & Tchar(generalSMSTemplate.Text) & "', " & _"0,1/1/1900, " & _"'" & Tchar(UpdUser.Text) & "', " & _"'" & Tchar(UpdUser.Text) & "', current_timestamp,'" & strday & "')"

                promoid.Text = GenerateID("QL_mstPricelist", CompnyCode)
                sSql = "INSERT INTO [QL_mstPricelist]([cmpcode], [promoid], [promeventcode], [promeventname],  [promflag], [promnote], [crtuser], [upduser], [updtime],postdate) " & _
                        "VALUES('" & CompnyCode & "', " & _
                        "" & promoid.Text & ", " & _
                        "'" & Tchar(promeventcode.Text) & "', " & _
                        "'" & Tchar(promeventname.Text) & "', " & _
                        "'" & status.Text & "', " & _
                        "'" & Tchar(note.Text) & "','" & Tchar(UpdUser.Text) & "', " & _
                        "'" & Tchar(UpdUser.Text) & "', current_timestamp,'" & CDate(toDate(promstartdate.Text)) & "')"
            Else
                sSql = "UPDATE [QL_mstPricelist] " & _
                        "SET [cmpcode]='" & CompnyCode & "', " & _
                        "[promeventcode]='" & Tchar(promeventcode.Text) & "', " & _
                        "[promeventname]='" & Tchar(promeventname.Text) & "', " & _
                        "[promnote]='" & Tchar(note.Text) & "', " & _
                        "[promflag]='" & status.Text & "' , " & _
                        "[upduser]='" & Tchar(UpdUser.Text) & "',postdate = '" & CDate(toDate(promstartdate.Text)) & "', " & _
                        "[updtime]=current_timestamp " & _
                        "WHERE promoid=" & Session("oid")
            End If
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            If Not (Session("oid") Is Nothing Or Session("oid") = "") Then
                'Data sudah ada
                sSql = "DELETE FROM QL_mstPricelistDtl WHERE cmpcode = '" & CompnyCode & "' AND promoid = " & Session("oid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                promoid.Text = Session("oid")

                'sSql = "delete QL_mstPricelistAvailable where cmpcode = '" & CompnyCode & "' and promooid=" & Session("oid")
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()

            Else
                'Data baru
                sSql = "update  QL_mstoid set lastoid=" & promoid.Text & " where tablename = 'QL_mstPricelist' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            End If

            insertingDtlPromo(promoid.Text, OidTempDtl)

            If Session("oid") Is Nothing Then
                Session("oid") = promoid.Text
            End If
            'If Not (Session("PriceDtl") Is Nothing) Then
            '    Dim objTable As DataTable : objTable = Session("PriceDtl")
            '    For C1 As Integer = 0 To objTable.Rows.Count - 1
            '        sSql = "INSERT INTO QL_mstPricelistAvailable(cmpcode,outletoid,promooid,outletcode, " & _
            '               "itemavailable,crtuser,crttime,upduser,updtime) " & _
            '               "VALUES ('" & CompnyCode & "'," & objTable.Rows(C1)("outletoid").ToString & ", " & _
            '               "'" & Session("oid") & "','" & objTable.Rows(C1)("outletcode").ToString & "', " & _
            '               "'" & Tchar(objTable.Rows(C1)("itemavailable").ToString) & "', " & _
            '               "'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "', " & _
            '               "current_timestamp)"
            '        xCmd.CommandText = sSql
            '        xCmd.ExecuteNonQuery()
            '    Next
            'End If

            objTrans.Commit()
            conn.Close()
            ClearDtlInput()
            ClearMstInput()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING")
            status.Text = "In Process"
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstPricelist.aspx?awal=true")

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        ClearMstInput()
        ClearDtlInput()
        Response.Redirect("~\Master\mstPricelist.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "delete QL_mstPricelist WHERE promoid=" & Session("oid")

            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete QL_mstPricelistdtl WHERE promoid=" & Session("oid")

            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()


            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
            showMessage(ex.Message, CompnyName & " - WARNING")
        Finally
            Response.Redirect("~\Master\mstPricelist.aspx?awal=true")
            BtnDelete.Enabled = False
        End Try

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sPlus As String = ""
        sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' and promflag " & IIf(ddlStatus.SelectedValue = "Outstanding", "<> 'Updated' ", "= 'Updated'")
        BindData(sPlus)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterDDL.SelectedIndex = 0 : FilterText.Text = ""
        ddlStatus.SelectedIndex = 0

        Dim sPlus As String = ""
        sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' and promflag" & IIf(ddlStatus.SelectedValue = "Outstanding", "<> 'Updated' ", "= 'Updated'")
        BindData(sPlus)
    End Sub

    Protected Sub promtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If promtype.SelectedValue.ToUpper.Trim = "BUY NX GET NY" Then
            amountdisc1.CssClass = "inpTextDisabled"
            amountdisc1.Enabled = False
            Subsidi.CssClass = "inpTextDisabled"
            Subsidi.Enabled = False
            itemdesc2.CssClass = "inpText"
            itemdesc2.Enabled = True
            TD1.Visible = True
            TD2.Visible = True
            TD3.Visible = True
            TD4.Visible = True
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf promtype.SelectedValue.ToUpper.Trim = "BUY X GET Y" Then
            amountdisc1.CssClass = "inpTextDisabled"
            amountdisc1.Enabled = False
            Subsidi.CssClass = "inpTextDisabled"
            Subsidi.Enabled = False
            itemdesc2.CssClass = "inpText"
            itemdesc2.Enabled = True
            TD1.Visible = False
            TD2.Visible = False
            TD3.Visible = False
            TD4.Visible = False
            qty1.SelectedValue = 1
            qty2.SelectedValue = 1
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf promtype.SelectedValue.ToUpper.Trim = "DISC % (ITEM ONLY)" Then
            itemdesc2.CssClass = "inpTextDisabled"
            itemdesc2.Enabled = False
            amountdisc1.CssClass = "inpText"
            amountdisc1.Enabled = True
            Subsidi.CssClass = "inpText"
            Subsidi.Enabled = True
            TD1.Visible = False
            TD2.Visible = False
            TD3.Visible = False
            TD4.Visible = False
            qty1.SelectedValue = 1
            qty2.SelectedValue = 1
            ddlInsertType.CssClass = "inpText"
            ddlInsertType.Enabled = True
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf promtype.SelectedValue.ToUpper.Trim = "DISC QUANTITY" Then

            amountdisc1.CssClass = "inpText"
            amountdisc1.Enabled = True
            itemdesc2.CssClass = "inpTextDisabled"
            itemdesc2.Enabled = False
            TD1.Visible = True
            TD2.Visible = True
            TD3.Visible = False
            TD4.Visible = False
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
            '---

        ElseIf promtype.SelectedValue.ToUpper.Trim = "BUY 1 GET 1" Then
            itemdesc2.CssClass = "inpTextDisabled"
            itemdesc2.Enabled = False
            amountdisc1.CssClass = "inpTextDisabled"
            amountdisc1.Enabled = False
            Subsidi.CssClass = "inpTextDisabled"
            Subsidi.Enabled = False
            TD1.Visible = False
            TD2.Visible = False
            TD3.Visible = False
            TD4.Visible = False
            qty1.SelectedValue = 1
            qty2.SelectedValue = 1
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
        Else
            TD1.Visible = False
            TD2.Visible = False
            TD3.Visible = False
            TD4.Visible = False
            qty1.SelectedValue = 1
            qty2.SelectedValue = 1
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddataMenu()
        gvMenu.Visible = True
        PanelUser.Visible = True
        BtnUser.Visible = True
        MpeUser.Show()
    End Sub

    Protected Sub btnListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        filterMenuItem.Text = ""
        binddataMenu()
        gvMenu.Visible = True
        PanelUser.Visible = True
        BtnUser.Visible = True
        MpeUser.Show()
    End Sub

    Protected Sub ibDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelete.Click
        removeList("tbldtlPromo", "gvPromoDtl", "ibDelete", 6)
    End Sub

    Protected Sub itemavailable_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateDetail()
    End Sub

#End Region

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
    End Sub

    Protected Sub GVMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            'e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub printExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sWhere As String = ""

        If ddlInsertType.SelectedValue = "GROUP" Then
            sWhere = "WHERE  QL_mstitem.cmpcode='" & CompnyCode & "' and merk like '%" & Merkfilter.Text & "%' " & IIf(ddlGroup.SelectedValue = "ALL GRUP", "  ", " and itemgroupoid = " & ddlGroup.SelectedValue & " ") & " " & IIf(ddlSubGroup2.SelectedValue = "ALL GRUP", "  ", " and itemsubgroupoid = " & ddlSubGroup2.SelectedValue & " ") & ""
        ElseIf ddlInsertType.SelectedValue = "SUBGROUP" Then
            sWhere = "WHERE QL_mstitem.cmpcode='" & CompnyCode & "' " & IIf(ddlSubGroup.SelectedValue = "ALL GRUP", "  ", " and itemsubgroupoid = " & ddlSubGroup.SelectedValue & " ") & " and merk like '%" & Merkfilter.Text & "%'"
        End If

        sWhere &= " and itemflag = 'aktif' "

        Response.Clear()


        Response.AddHeader("content-disposition", "inline;filename=PL" & Format(Date.Now, "yyyy/MM/dd") & ".xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"

        sSql = "SELECT 	  itemoid, itemcode, itemdesc,merk, QL_mstgen.gendesc as itemgroup ,ggdtl.gendesc as itemsubgroup , pricelist, discunit1 , discunit2 , discunit3 , g1.gendesc as satuan1, g2.gendesc as satuan2, g3.gendesc as satuan3,konversi1_2,konversi2_3 FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid =  QL_mstgen.genoid  inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1  inner join ql_mstgen g2 on g2.genoid=QL_mstitem.satuan2  inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3 inner join ql_mstgen ggdtl on ggdtl.genoid = ql_mstitem.itemsubgroupoid  " & sWhere & " ORDER BY itemdesc    "

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()


        Response.End()
        'mpePrint.Show()
    End Sub
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    'Private Function createNewExcel() As DataTable
    '    createNewExcel = New DataTable
    '    createNewExcel.Columns.Add("trnbelino", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("trnbelidate", Type.GetType("System.DateTime"))
    '    createNewExcel.Columns.Add("suppname", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("suppoid", Type.GetType("System.Int32"))
    '    createNewExcel.Columns.Add("supprefno", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("paytype", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("paytypeoid", Type.GetType("System.Int32"))
    '    createNewExcel.Columns.Add("trnbeliamt", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelidiscdtl", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelidisctype", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("trnbelidiscvalue", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelidiscamt", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelidpp", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelitaxtype", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("trnbelitaxpct", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelitaxamt", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbeliamtnetto", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelinote", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("trnbelistatus", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("trnbelicost", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelidtlseq", Type.GetType("System.Int32"))
    '    createNewExcel.Columns.Add("matname", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("matoid", Type.GetType("System.Int32"))
    '    createNewExcel.Columns.Add("trnbelidtlqty", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("unit", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("trnbelidtlunit", Type.GetType("System.Int32"))
    '    createNewExcel.Columns.Add("trnbelidtlprice", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelidtlamt", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("dtldisctype", Type.GetType("System.String"))
    '    createNewExcel.Columns.Add("dtldiscvalue", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("dtldiscamt", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelidtlnetto", Type.GetType("System.Decimal"))
    '    createNewExcel.Columns.Add("trnbelidtlnote", Type.GetType("System.String"))
    '    Return createNewExcel
    'End Function

    Protected Sub btnXls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim excelconn As New OleDbConnection
        Try
            If fuXls.HasFile Then
                Dim filePath As String = fuXls.PostedFile.FileName
                Dim fileName As String = Path.GetFileName(filePath)
                Dim ext As String = Path.GetExtension(fileName)
                Dim type As String = String.Empty
                Dim uploadName As String = "PL" & Format(GetServerTime(), "MMddyyyyHHmm")

                Select Case ext
                    Case ".xls"
                        type = "application/vnd.ms-excel"
                    Case ".xlsx"
                        type = "application/vnd.ms-excel"
                End Select

                If type <> String.Empty Then
                    fuXls.SaveAs(Server.MapPath("~/Upload/" & uploadName & ext))

                    If File.Exists(Server.MapPath("~/Upload/" & uploadName & ext)) Then

                        Dim exceldtab As DataTable = Session("tbldtlPromo")
                        Dim exceldrow As DataRow

                        Dim sHeaderMsg As String = ""
                        Dim errMsg As String = ""
                        Dim errMsgTemp As String = ""
                        Dim excelconnstr As String = ""

                        If ext = ".xlsx" Then
                            excelconnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Server.MapPath("~/Upload/" & uploadName & ext) & "';Extended Properties=Excel 12.0;Persist Security Info=False"
                        ElseIf ext = ".xls" Then
                            excelconnstr = "Provider=Microsoft.JET.OLEDB.4.0;Data Source='" & Server.MapPath("~/Upload/" & uploadName & ext) & "';Extended Properties=Excel 8.0;Persist Security Info=False"
                        End If

                        excelconn.ConnectionString = excelconnstr
                        excelconn.Open()

                        Dim ds As New DataSet
                        Try
                            Dim sqlstring As String = "SELECT * FROM [PL$]"
                            Dim oda As New OleDb.OleDbDataAdapter(sqlstring, excelconnstr)
                            oda.Fill(ds, "PL")
                            excelconn.Close()
                        Catch ex As Exception
                            showMessage("Failed to read uploaded Excel file, probably because incorrect template format.<BR>Reason: " & ex.Message, " Warning - " & CompnyCode)
                            Exit Sub
                        End Try
                        Dim dtab As DataTable = ds.Tables("PL")

                        If conn.State = ConnectionState.Closed Then
                            conn.Open()
                        End If

                        Dim dtrow() As DataRow = dtab.Select("", "itemdesc ASC")
                        Dim trnbelidate As New Date
                        Dim dvExcel As DataView : dvExcel = exceldtab.DefaultView

                        For i As Integer = 0 To dtrow.Length - 1
                            exceldrow = exceldtab.NewRow
                            errMsg = ""

                            'tblDetail.Columns.Add("promdtloid", type.GetType("System.Int32")) '0
                            'tblDetail.Columns.Add("itemoid", type.GetType("System.Int32")) '1
                            'tblDetail.Columns.Add("itemdesc", type.GetType("System.String")) '2
                            'tblDetail.Columns.Add("amountdisc", type.GetType("System.Decimal")) '3
                            'tblDetail.Columns.Add("itemoid2", type.GetType("System.Int32")) '4
                            'tblDetail.Columns.Add("itemdesc2", type.GetType("System.String")) '5
                            'tblDetail.Columns.Add("qty1", type.GetType("System.Int32")) '6
                            'tblDetail.Columns.Add("qty2", type.GetType("System.Int32")) '7
                            'tblDetail.Columns.Add("subsidi", type.GetType("System.Decimal")) '8
                            'tblDetail.Columns.Add("amountdisc2", type.GetType("System.Decimal")) '9
                            'tblDetail.Columns.Add("amountdisc3", type.GetType("System.Decimal")) '10
                            'Checked='<%# eval("CheckValue") %>'


                            '== DETAIL START

                            exceldrow("promdtloid") = 0
                            exceldrow("itemoid2") = 0
                            exceldrow("qty1") = 0
                            exceldrow("qty2") = 0
                            exceldrow("itemdesc2") = ""

                            Dim iMatOid As Integer = 0
                            sSql = "SELECT ISNULL(itemoid,0) FROM ql_mstitem WHERE itemoid = '" & (dtrow(i).Item("itemoid").ToString.Trim) & "'" & _
                                "AND itemflag='Aktif' AND cmpcode = '" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            iMatOid = ToDouble(CStr(xCmd.ExecuteScalar))
                            If iMatOid = 0 Then
                                errMsg &= "- Itemoid " & dtrow(i).Item("Itemoid").ToString.Trim & " do not exist.<br />"
                            Else
                                dvExcel.RowFilter = "Itemoid=" & iMatOid
                                If dvExcel.Count > 0 Then
                                    errMsg &= "- Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " is already exist on same Pricelist.<br />"
                                    dvExcel.RowFilter = ""
                                Else
                                    dvExcel.RowFilter = ""
                                    exceldrow("Itemoid") = ToDouble(CStr(xCmd.ExecuteScalar))
                                End If
                            End If

                            exceldrow("itemdesc") = dtrow(i).Item("itemdesc").ToString.Trim & " - " & dtrow(i).Item("merk").ToString.Trim

                            If Not ValidateNumericDataToDB(ToDouble(dtrow(i).Item("discunit1").ToString), "ql_mstpricelistdtl", "amountdisc", errMsgTemp) Then
                                errMsg &= "- discunit1 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " is not valid: " & errMsgTemp & ".<br />"
                            Else
                                If ToDouble(dtrow(i).Item("discunit1").ToString) < 0 Then
                                    errMsg &= "- Invalid discunit1 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & ".<br />"
                                End If
                                If ToDouble(dtrow(i).Item("discunit1").ToString) > 100 Then
                                    errMsg &= "- discunit1 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " must be less than 100%.<br />"
                                End If
                            End If
                            exceldrow("amountdisc") = ToDouble(dtrow(i).Item("discunit1").ToString)

                            If Not ValidateNumericDataToDB(ToDouble(dtrow(i).Item("discunit2").ToString), "ql_mstpricelistdtl", "amountdisc2", errMsgTemp) Then
                                errMsg &= "- discunit2 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " is not valid: " & errMsgTemp & ".<br />"
                            Else
                                If ToDouble(dtrow(i).Item("discunit2").ToString) < 0 Then
                                    errMsg &= "- Invalid discunit2 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & ".<br />"
                                End If
                                If ToDouble(dtrow(i).Item("discunit2").ToString) > 100 Then
                                    errMsg &= "- Invalid discunit2 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " must be less than 100%.<br />"
                                End If
                            End If
                            exceldrow("amountdisc2") = ToDouble(dtrow(i).Item("discunit2").ToString)

                            If Not ValidateNumericDataToDB(ToDouble(dtrow(i).Item("discunit3").ToString), "ql_mstpricelistdtl", "amountdisc3", errMsgTemp) Then
                                errMsg &= "- discunit3 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " is not valid: " & errMsgTemp & ".<br />"
                            Else
                                If ToDouble(dtrow(i).Item("discunit3").ToString) < 0 Then
                                    errMsg &= "- Invalid discunit3 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & ".<br />"
                                End If
                                Dim str As String = ToDouble(dtrow(i).Item("discunit3").ToString)
                                If ToDouble(dtrow(i).Item("discunit3").ToString) > 100 Then
                                    errMsg &= "- Invalid discunit3 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " must be less than 100%.<br />"
                                End If
                            End If
                            exceldrow("amountdisc3") = ToDouble(dtrow(i).Item("discunit3").ToString)


                            If Not ValidateNumericDataToDB(ToDouble(dtrow(i).Item("pricelist").ToString), "ql_mstpricelistdtl", "subsidi1", errMsgTemp) Then
                                errMsg &= "- pricelist for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " is not valid: " & errMsgTemp & ".<br />"
                            Else
                                If ToDouble(dtrow(i).Item("pricelist").ToString) < 0 Then
                                    errMsg &= "- pricelist for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " must be greater than -1.<br />"
                                End If
                            End If
                            exceldrow("subsidi") = ToDouble(dtrow(i).Item("pricelist").ToString)

                            '-------------------------------
                            If Not ValidateNumericDataToDB(ToDouble(dtrow(i).Item("konversi1_2").ToString), "ql_mstpricelistdtl", "qtyitemoid1", errMsgTemp) Then
                                errMsg &= "- konversi1_2 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " is not valid: " & errMsgTemp & ".<br />"
                            Else
                                If ToDouble(dtrow(i).Item("konversi1_2").ToString) <= 0 Then
                                    errMsg &= "- konversi1_2 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " must be greater than 0.<br />"
                                End If
                            End If
                            exceldrow("qty1") = ToDouble(dtrow(i).Item("konversi1_2").ToString)

                            If Not ValidateNumericDataToDB(ToDouble(dtrow(i).Item("konversi2_3").ToString), "ql_mstpricelistdtl", "qtyitemoid2", errMsgTemp) Then
                                errMsg &= "- konversi2_3 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " is not valid: " & errMsgTemp & ".<br />"
                            Else
                                If ToDouble(dtrow(i).Item("konversi2_3").ToString) <= 0 Then
                                    errMsg &= "- konversi2_3 for Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " must be greater than 0.<br />"
                                End If
                            End If
                            exceldrow("qty2") = ToDouble(dtrow(i).Item("konversi2_3").ToString)

                            exceldrow("CheckValue") = "False"

                            '== DETAIL END

                            exceldtab.Rows.Add(exceldrow)

                            If errMsg <> "" Then
                                sHeaderMsg &= "Invalid data for PL Barang " & dtrow(i).Item("itemdesc").ToString.Trim & " : <br />" & errMsg
                            End If
                        Next

                        conn.Close()



                        If sHeaderMsg.Trim <> "" Then
                            showMessage(sHeaderMsg.Trim, " Warning - " & CompnyCode)
                            'lkbSaveExcel.Visible = False
                            Exit Sub
                        Else
                            ViewState("Excel") = exceldtab
                            gvPromoDtl.DataSource = exceldtab
                            gvPromoDtl.DataBind()
                        End If
                    Else
                        showMessage("Upload failed!", " Warning - " & CompnyCode)
                    End If
                Else
                    excelconn.Close() : conn.Close()
                    showMessage("You can only select .xls or .xlsx file!", " Warning - " & CompnyCode)
                End If
            Else
                showMessage("Please choose file to upload first.", " Warning - " & CompnyCode)
            End If

        Catch ex As Exception
            excelconn.Close() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, 1)
        End Try

    End Sub

    Protected Sub posting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles posting.Click
        status.Text = "POSTED"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub sendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sendApproval.Click
        status.Text = "In Approval"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub postedallx_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckAll()
    End Sub

    Protected Sub postednotallx_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UncheckAll()
    End Sub
    Public Sub UncheckAll()
        If Not Session("tbldtlPromo") Is Nothing Then
            Dim dtTbl As DataTable = Session("tbldtlPromo")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("tbldtlPromo")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    'objView.RowFilter = "personoid=" & dtTbl.Rows(C1)("personoid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                Session("tbldtlPromo") = objView.ToTable
                Session("tbldtlPromo") = dtTbl
                gvPromoDtl.DataSource = Session("tbldtlPromo")
                gvPromoDtl.DataBind()
            End If
        End If
    End Sub

    Public Sub CheckAll()

        If Not Session("tbldtlPromo") Is Nothing Then
            Dim dtTbl As DataTable = Session("tbldtlPromo")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("tbldtlPromo")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    'objView.RowFilter = "personoid=" & dtTbl.Rows(C1)("personoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                Session("tbldtlPromo") = objView.ToTable
                Session("tbldtlPromo") = dtTbl
                gvPromoDtl.DataSource = Session("tbldtlPromo")
                gvPromoDtl.DataBind()
            End If
        End If


    End Sub

    Protected Sub preview_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sWhere As String = ""
        lblmenuygada.Text = ""
        Response.Clear()


        Try
            Dim dt0 As DataTable = Session("tbldtlPromo")

            sSql = "select '' itemoid, '' itemcode,  '' itemdesc,   '' merk, '' itemgroup,'' itemsubgroup ,'' pricelist  , '' discunit1, '' discunit2, '' discunit3, '' satuan1,'' satuan2,  '' satuan3,   '' konversi1_2,  '' konversi2_3"
            Dim dtne As DataTable = CreateDataTableFromSQL(sSql)
            dtne.Rows.Clear()


            For c0 As Int64 = 0 To dt0.Rows.Count - 1

                sSql = " select itemcode,itemdesc,merk , g1.gendesc itemgroup ,g2.gendesc itemsubgroup,g3.gendesc  satuan1 ,g4.gendesc  satuan2 ,g5.gendesc  satuan3  from ql_mstitem i inner join QL_mstgen g1 on i.itemgroupoid = g1.genoid inner join QL_mstgen g2 on i.itemsubgroupoid = g2.genoid  inner join QL_mstgen g3 on i.satuan1 = g3.genoid inner join QL_mstgen g4 on i.satuan2 = g4.genoid inner join QL_mstgen g5 on i.satuan3 = g5.genoid  where itemoid = " & dt0.Rows(c0).Item("itemoid") & " "
                Dim dt1 As DataTable = CreateDataTableFromSQL(sSql)

                For c1 As Int16 = 0 To dt1.Rows.Count - 1
                    Dim dr2 As DataRow = dtne.NewRow
                    dr2("itemoid") = dt0.Rows(c0).Item("itemoid")
                    dr2("itemcode") = dt1.Rows(c1).Item("itemcode")
                    dr2("itemdesc") = dt1.Rows(c1).Item("itemdesc")
                    dr2("merk") = dt1.Rows(c1).Item("merk")
                    dr2("itemgroup") = dt1.Rows(c1).Item("itemgroup")
                    dr2("itemsubgroup") = dt1.Rows(c1).Item("itemsubgroup")
                    dr2("pricelist") = dt0.Rows(c0).Item("subsidi")
                    dr2("discunit1") = dt0.Rows(c0).Item("amountdisc")
                    dr2("discunit2") = dt0.Rows(c0).Item("amountdisc2")
                    dr2("discunit3") = dt0.Rows(c0).Item("amountdisc3")
                    dr2("satuan1") = dt1.Rows(c1).Item("satuan1")
                    dr2("satuan2") = dt1.Rows(c1).Item("satuan2")
                    dr2("satuan3") = dt1.Rows(c1).Item("satuan3")
                    dr2("konversi1_2") = dt0.Rows(c0).Item("qty1")
                    dr2("konversi2_3") = dt0.Rows(c0).Item("qty2")
                    dtne.Rows.Add(dr2)
                Next

                'tblDetail.Columns.Add("promdtloid", Type.GetType("System.Int32")) '0
                'tblDetail.Columns.Add("itemoid", Type.GetType("System.Int32")) '1
                'tblDetail.Columns.Add("itemdesc", Type.GetType("System.String")) '2
                'tblDetail.Columns.Add("amountdisc", Type.GetType("System.Decimal")) '3
                'tblDetail.Columns.Add("itemoid2", Type.GetType("System.Int32")) '4
                'tblDetail.Columns.Add("itemdesc2", Type.GetType("System.String")) '5
                'tblDetail.Columns.Add("qty1", Type.GetType("System.Int32")) '6
                'tblDetail.Columns.Add("qty2", Type.GetType("System.Int32")) '7
                'tblDetail.Columns.Add("subsidi", Type.GetType("System.Decimal")) '8
                'tblDetail.Columns.Add("amountdisc2", Type.GetType("System.Decimal")) '9
                'tblDetail.Columns.Add("amountdisc3", Type.GetType("System.Decimal")) '10
                'tblDetail.Columns.Add("CheckValue", Type.GetType("System.String")) '11

            Next

            'Dim angka As Integer = dt.Rows.Count
            Response.AddHeader("content-disposition", "inline;filename=PL_Prev_" & Format(Now, "dd_MM_yy") & ".xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"
            Response.Write(ConvertDtToTDF(dtne))
            Response.End()

            lblmenuygada.Text = ""
        Catch ex As Exception
            lblmenuygada.Text = ex.Message & "<br /><br />" & ex.ToString()
            Exit Sub
        End Try
    End Sub
End Class
