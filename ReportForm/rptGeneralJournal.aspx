<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptGeneralJournal.aspx.vb" Inherits="ReportForm_rptGeneralJournal" title="Untitled Page" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Laporan General Journal :."></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: transparent" valign="center">
    
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD class="Label" align=left>Periode </TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="date1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox> <asp:ImageButton id="search1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w4"></asp:Label> <asp:TextBox id="date2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w5"></asp:TextBox> <asp:ImageButton id="search2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblcbg" runat="server" Text="Cabang" __designer:wfdid="w8"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text=":" __designer:wfdid="w9"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLCabang" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w10"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="01">Pusat</asp:ListItem>
<asp:ListItem Value="02">MTCS Surabaya</asp:ListItem>
<asp:ListItem Value="03">Modern Surabaya</asp:ListItem>
<asp:ListItem Value="04">Makasar</asp:ListItem>
<asp:ListItem Value="05">Aurora Dps</asp:ListItem>
<asp:ListItem Value="06">BioTech Samarinda</asp:ListItem>
<asp:ListItem Value="07">Semarang</asp:ListItem>
</asp:DropDownList> <asp:Label id="acctgoid" runat="server" __designer:wfdid="w11" Visible="False"></asp:Label></TD></TR><TR><TD id="TD4" class="Label" align=left><asp:Label id="Label4" runat="server" Text="Type" __designer:wfdid="w12"></asp:Label></TD><TD id="TD3" class="Label" align=left>:</TD><TD id="TD2" class="Label" align=left><asp:DropDownList id="ddlType" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w13" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="d.glnote like 'A/R Payment%'">A/R PAYMENT</asp:ListItem>
<asp:ListItem Value="d.glnote like 'A/P Payment%'">A/P Payment</asp:ListItem>
<asp:ListItem Value="d.glnote like ' - Cost|%'">EXPENSE</asp:ListItem>
<asp:ListItem Value="d.glnote like ' - Receipt|%'">RECEIPT</asp:ListItem>
<asp:ListItem Value="d.glnote like ' - Mutation%'">MUTATION</asp:ListItem>
<asp:ListItem Value="noref like '%RGM/%'">MANIFEST GIRO IN</asp:ListItem>
<asp:ListItem Value="noref like '%RGK/%'">MANIFEST GIRO OUT</asp:ListItem>
<asp:ListItem Value="d.glnote like 'DN No.%'">Debit Note</asp:ListItem>
<asp:ListItem Value="d.glnote like 'CN No.%'">Credit Note</asp:ListItem>
<asp:ListItem Value="noref like 'DP%' and glother1 like 'QL_TRNDPAR%'">Down Payment A/R</asp:ListItem>
<asp:ListItem Value="noref like 'DP%' and glother1 like 'QL_TRNDPAP%'">Down Payment A/P</asp:ListItem>
<asp:ListItem Value="d.glnote like 'A/R Koreksi%'">KOREKSI A/R</asp:ListItem>
<asp:ListItem Value="d.glnote like 'A/P Koreksi%'">KOREKSI A/P</asp:ListItem>
<asp:ListItem Value="d.glnote like 'ADJUSTMENT STOCK%'">ADJUSTMENT STOCK</asp:ListItem>
<asp:ListItem Value="noref like 'EXP%'">EKSPEDISI</asp:ListItem>
<asp:ListItem Value="d.glnote like 'MATERIAL USAGE%'">MATERIAL USAGE</asp:ListItem>
<asp:ListItem Value="d.glnote like 'PURCHASE INVOICE%'">PURCHASE INVOICE</asp:ListItem>
<asp:ListItem Value="(d.glnote like 'Akun Hutang%' or d.glnote like 'Akun Persediaan%')">PURCHASE RETURN</asp:ListItem>
<asp:ListItem Value="d.glnote like 'PINJAM%'">PINJAM BARANG</asp:ListItem>
<asp:ListItem Value="d.glnote like 'KEMBALI%'">KEMBALI BARANG</asp:ListItem>
<asp:ListItem Value="d.glnote like 'SALES INVOICE%'">SALES INVOICE</asp:ListItem>
<asp:ListItem Value="(d.glnote like 'Akun Piutang%' or d.glnote like 'Akun Retur HPP%' or d.glnote like 'Akun Retur Persediaan%')">SALES RETURN</asp:ListItem>
<asp:ListItem Value="(d.glnote like 'Transfer Gudang Supp No.%' or d.glnote like 'Transfer Gudang No.%') and noref like 'TW%'">TRANSFER WAREHOUSE</asp:ListItem>
<asp:ListItem Value="d.glnote like 'Transfer Gudang No.%' and noref like 'TC%'">TRANSFER CONFIRM</asp:ListItem>
</asp:DropDownList><asp:CheckBox id="cbCheckAll" runat="server" BackColor="#FFFFC0" Text="CHECK ALL" __designer:wfdid="w14" AutoPostBack="True" BorderColor="Black"></asp:CheckBox><BR /><asp:CheckBoxList id="cbType" runat="server" ForeColor="White" BackColor="#FFFFC0" __designer:wfdid="w15" AutoPostBack="True" OnSelectedIndexChanged="cbType_SelectedIndexChanged" BorderColor="Black" RepeatDirection="Horizontal" CellPadding="5" RepeatColumns="4"><asp:ListItem Value="m.type = 'AR PAYMENT'">A/R Payment</asp:ListItem>
<asp:ListItem Value="m.type = 'AP PAYMENT'">A/P Payment</asp:ListItem>
<asp:ListItem Value="m.type = 'EXPENSE'">Expense</asp:ListItem>
<asp:ListItem Value="m.type = 'RECEIPT'">Receipt</asp:ListItem>
<asp:ListItem Value="m.type = 'MUTATION'">Mutation</asp:ListItem>
<asp:ListItem Value="m.type = 'GIROIN'">Manifest Giro In</asp:ListItem>
<asp:ListItem Value="m.type = 'GIROOUT'">Manifest Giro Out</asp:ListItem>
<asp:ListItem Value="m.type = 'DN'">Debit Note</asp:ListItem>
<asp:ListItem Value="m.type = 'CN'">Credit Note</asp:ListItem>
<asp:ListItem Value="m.type = 'DPAR'">Down Payment A/R</asp:ListItem>
<asp:ListItem Value="m.type = 'DPAP'">Down Payment A/P</asp:ListItem>
<asp:ListItem Value="m.type = 'ARK'">A/R Koreksi</asp:ListItem>
<asp:ListItem Value="m.type = 'APK'">A/P Koreksi</asp:ListItem>
<asp:ListItem Value="m.type = 'ADJ'">Adjusment Stock</asp:ListItem>
<asp:ListItem Value="m.type = 'NT'">Nota Ekspedisi</asp:ListItem>
<asp:ListItem Value="m.type = 'MU'">Material Usage</asp:ListItem>
<asp:ListItem Value="m.type = 'PI'">Purchase Invoice</asp:ListItem>
<asp:ListItem Value="m.type = 'PR'">Purchase Return</asp:ListItem>
<asp:ListItem Value="m.type = 'REQ'">Pinjam Barang</asp:ListItem>
<asp:ListItem Value="m.type = 'REF'">Kembali Barang</asp:ListItem>
<asp:ListItem Value="m.type = 'SI'">Sales Invoice</asp:ListItem>
<asp:ListItem Value="m.type = 'SR'">Sales Return</asp:ListItem>
<asp:ListItem Value="m.type = 'TW'">Transfer Warehouse</asp:ListItem>
<asp:ListItem Value="m.type = 'TC'">Transfer Confirm</asp:ListItem>
<asp:ListItem Value="m.type = 'DPR'">DP A/P Retur</asp:ListItem>
<asp:ListItem Value="m.type = 'DPR'">DP A/R Retur</asp:ListItem>
<asp:ListItem Value="m.type LIKE 'NOTAHR%'">Nota HR</asp:ListItem>
<asp:ListItem Value="m.type = 'TI'">Transform Item</asp:ListItem>
<asp:ListItem Value="m.type = 'ARSERVICE'">Payment AR Service</asp:ListItem>
<asp:ListItem Value="m.type = 'FIN'">Service Final</asp:ListItem>
<asp:ListItem Value="m.type = 'SRV'">Penerimaan Service</asp:ListItem>
<asp:ListItem Value="m.type = 'INV'">Invoice Service</asp:ListItem>
<asp:ListItem Value="m.type = 'FIT'">Final Internal</asp:ListItem>
<asp:ListItem Value="m.type = 'JURNAL'">Memo Jurnal</asp:ListItem>
<asp:ListItem Value="m.type = 'TWS'">TW Service</asp:ListItem>
<asp:ListItem Value="m.type = 'TCS'">TC Service</asp:ListItem>
<asp:ListItem Value="m.type like '%ql_trnpayhrmst%'">HR Payment</asp:ListItem>
</asp:CheckBoxList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="No. Transaksi" __designer:wfdid="w16"></asp:Label></TD><TD class="Label" align=left>:</TD><TD id="TD1" class="Label" align=left runat="server" Visible="true"><asp:TextBox id="typeGJ" runat="server" Width="168px" CssClass="inpText" __designer:wfdid="w17"></asp:TextBox>&nbsp;<asp:ImageButton id="imbClearNoTrans" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w18"></asp:ImageButton></TD></TR><TR><TD align=left>Account</TD><TD class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: baseline" align=left colSpan=1 rowSpan=1>&nbsp;<asp:RadioButtonList id="rblAccount" runat="server" __designer:wfdid="w19" AutoPostBack="True" OnSelectedIndexChanged="rblAccount_SelectedIndexChanged" RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Left"><asp:ListItem Selected="True">All</asp:ListItem>
<asp:ListItem Value="Beberapa">Select</asp:ListItem>
</asp:RadioButtonList>&nbsp;<asp:TextBox id="acctgdesc" runat="server" Width="168px" CssClass="inpText" __designer:wfdid="w20" Visible="False"></asp:TextBox> <asp:ImageButton id="btnsearchacctg" onclick="btnsearchacctg_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w21" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w22" Visible="False"></asp:ImageButton>&nbsp;</TD></TR><TR><TD class="Label" align=right></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:GridView id="gvAccounting" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w23" Visible="False" CellPadding="4" OnPageIndexChanging="gvAccounting_PageIndexChanging" AllowPaging="True" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer "></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Acctgoid" Visible="False"><ItemTemplate>
<asp:Label id="lblacctgoid" runat="server" Text='<%# Eval("id") %>' __designer:wfdid="w2"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbAccount" runat="server" __designer:wfdid="w3" Checked='<%# Eval("selected") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="code" HeaderText="Code" SortExpression="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" Font-Size="Small" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblstatusdatasupp" runat="server" Text="No data found." CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate "></AlternatingRowStyle>
</asp:GridView> <asp:DropDownList id="tipene" runat="server" CssClass="inpText" __designer:wfdid="w24" Visible="False"><asp:ListItem Value="%">ALL</asp:ListItem>
<asp:ListItem Value="SI">Sales Invoice</asp:ListItem>
<asp:ListItem Value="SR">Sales Retur</asp:ListItem>
<asp:ListItem Value="BKM">A/R Payment</asp:ListItem>
<asp:ListItem Value="CONAR">Init A/R</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnshowprint" onclick="btnshowprint_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton><asp:ImageButton id="ImageButton1" onclick="ImageButton1_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> <asp:ImageButton id="ImageButton2" onclick="ImageButton2_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton> <asp:ImageButton id="imbClearRpt" onclick="imbClearRpt_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 110px" class="Label" align=center colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w29" Format="dd/MM/yyyy" PopupButtonID="search1" TargetControlID="Date1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w30" Format="dd/MM/yyyy" PopupButtonID="search2" TargetControlID="Date2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w31" TargetControlID="date2" CultureName="id-ID" UserDateFormat="DayMonthYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w32" TargetControlID="date1" CultureName="id-ID" UserDateFormat="DayMonthYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender>&nbsp;<asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w33" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:dtid="844424930132046" __designer:wfdid="w34"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crv" runat="server" Width="350px" Height="50px" __designer:wfdid="w35" OnViewZoom="crv_ViewZoom" OnSearch="crv_Search" HasViewList="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="True" HasExportButton="False" HasPrintButton="False"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ImageButton1"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ImageButton2"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="search1"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="search2"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upMsgbox" runat="server"><contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" __designer:wfdid="w36" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow" __designer:wfdid="w37"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black" __designer:wfdid="w38"></asp:Label></asp:Panel></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w39"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black" __designer:wfdid="w40"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" __designer:wfdid="w41" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w42"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" __designer:wfdid="w43" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender><asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" __designer:wfdid="w44" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

