<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="TrnForeCastBeli.aspx.vb" Inherits="TrnForCastBeli" Title="MULTI SARANA COMPUTER - Forecast PO" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
        <table id="Table3" align="center" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th class="header" colspan="5" valign="top">
                <asp:Label ID="Label81" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Forecast PO"></asp:Label></th>
        </tr>
    </table>
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel10" runat="server" HeaderText="TabPanel10">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w23" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD align=left colSpan=4><asp:DropDownList id="dCabangNya" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w24"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Filter&nbsp;</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w25"><asp:ListItem Value="fc.trnforecastno">No. Forcast</asp:ListItem>
<asp:ListItem Value="sup.suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w26"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w27" AutoPostBack="False"></asp:CheckBox></TD><TD class="Label" align=left colSpan=4><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w28" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton>&nbsp;<asp:Label id="Label76" runat="server" Text="to" __designer:wfdid="w30"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w31"></asp:TextBox><asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w32"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w33"></asp:Label> </TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w34"></asp:CheckBox></TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w35">
                                                            <asp:ListItem Value="custname">All</asp:ListItem>
                                                            <asp:ListItem Value="In Process">In Process</asp:ListItem>
                                                            <asp:ListItem Value="In Approval">In Approval</asp:ListItem>
                                                            <asp:ListItem>Approved</asp:ListItem>
                                                            <asp:ListItem>Closed</asp:ListItem>
                                                            <asp:ListItem>Rejected</asp:ListItem>
                                                            <asp:ListItem>Closed Manual</asp:ListItem>
                                                        </asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton>&nbsp; </TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=5><asp:GridView id="tbldata" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w17" DataKeyNames="trnforecastoid,trnforecastno" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="6" GridLines="None" OnSelectedIndexChanged="tbldata_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnforecastoid" HeaderText="ForeCastOid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:HyperLinkField DataNavigateUrlFields="trnforecastoid" DataNavigateUrlFormatString="~\transaction\TrnForeCastBeli.aspx?oid={0}" DataTextField="trnforecastno" HeaderText="No. Forecast">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnforecastdate" HeaderText="Tgl Forecast">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnforecastnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnforecaststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" SelectText="" ShowSelectButton="True" ButtonType="Image" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!" __designer:wfdid="w43"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w39" TargetControlID="FilterPeriod2" PopupButtonID="imbPeriod2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w40" TargetControlID="FilterPeriod1" PopupButtonID="imbPeriod1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w41" TargetControlID="FilterPeriod1" Mask="99/99/9999" UserDateFormat="MonthDayYear" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w42" TargetControlID="FilterPeriod2" Mask="99/99/9999" UserDateFormat="MonthDayYear" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="tbldata"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> List of Forecast PO</span></strong> <strong>
                                <span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel20" runat="server" HeaderText="TabPanel20">
                        <ContentTemplate>
                            <div style="text-align: left">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
<TABLE id="Table1" cellSpacing=2 cellPadding=4 width="100%" align=center border=0><TBODY><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; WIDTH: 152px; TEXT-ALIGN: left"><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information" __designer:wfdid="w174" Font-Underline="True"></asp:Label></TD><TD><asp:Label id="i_u" runat="server" Font-Size="Small" ForeColor="Red" Text="N E W" __designer:wfdid="w175" Visible="False"></asp:Label></TD><TD><asp:Label id="trnsuppoid" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" Font-Bold="True" __designer:wfdid="w176" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD style="WIDTH: 152px">No. Forecast</TD><TD><asp:TextBox id="trnbelipono" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w177" MaxLength="20" ReadOnly="True"></asp:TextBox><asp:Label id="ForeCastoid" runat="server" __designer:wfdid="w178" Visible="False"></asp:Label></TD><TD>Cabang</TD><TD colSpan=1><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" __designer:wfdid="w179" AutoPostBack="True"></asp:DropDownList></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD>Supplier <asp:Label id="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w180"></asp:Label></TD><TD><asp:TextBox id="suppliername" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w181"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w182"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w183"></asp:ImageButton> <asp:Label id="PrefixOid" runat="server" Text="0" __designer:wfdid="w184" Visible="False"></asp:Label></TD><TD>Tanggal</TD><TD><asp:TextBox style="TEXT-ALIGN: justify" id="trnbelipodate" runat="server" Width="61px" CssClass="inpTextDisabled" __designer:wfdid="w185" MaxLength="10" AutoPostBack="True" ValidationGroup="MKE" Enabled="False"></asp:TextBox>&nbsp;<SPAN style="FONT-SIZE: 10pt"></SPAN><asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w186" Visible="False"></asp:ImageButton><SPAN><asp:Label id="Label23" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w187"></asp:Label></SPAN></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD></TD><TD colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvSupplier" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w188" Visible="False" DataKeyNames="suppoid,suppname,paymenttermoid,prefixsupp" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="6" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldsuppcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No supplier data found!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD>Id Supplier</TD><TD><asp:DropDownList id="DDLPrefix" runat="server" CssClass="inpText" __designer:wfdid="w189"></asp:DropDownList></TD><TD>Pay Type</TD><TD><asp:DropDownList id="TRNPAYTYPE" runat="server" Width="133px" CssClass="inpText" __designer:wfdid="w190" AppendDataBoundItems="True">
                                                                </asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD><asp:Label id="Label9" runat="server" Width="82px" Text="Nett. Amount" __designer:wfdid="w191"></asp:Label></TD><TD><asp:TextBox id="amtbelinetto1" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Size="X-Small" __designer:wfdid="w192" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD>Status</TD><TD><asp:TextBox id="posting" runat="server" Width="129px" CssClass="inpTextDisabled" __designer:wfdid="w193" MaxLength="15" ReadOnly="True"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD>Note</TD><TD colSpan=2><asp:TextBox id="trnbelinote" runat="server" Width="437px" CssClass="inpText" __designer:wfdid="w194" MaxLength="350"></asp:TextBox></TD><TD></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=2><asp:Label id="Label7" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Information Item" __designer:wfdid="w195" Font-Underline="True"></asp:Label>&nbsp;<asp:Label id="periodacctg" runat="server" Width="1px" ForeColor="Red" __designer:wfdid="w196" Visible="False"></asp:Label> <asp:Label id="trnbelidtlseq" runat="server" Text="1" __designer:wfdid="w197" Visible="False"></asp:Label> <asp:Label id="I_u2" runat="server" ForeColor="Red" Text="New Detail" __designer:wfdid="w198" Visible="False"></asp:Label></TD><TD><asp:Label id="matoid" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" Font-Bold="False" __designer:wfdid="w199" Visible="False"></asp:Label></TD><TD><asp:Label id="trnbelidtloid" runat="server" __designer:wfdid="w200"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="lblcat_2" runat="server" Text="Katalog" __designer:wfdid="w201"></asp:Label>&nbsp;<asp:Label id="Label10" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w202"></asp:Label></TD><TD><asp:TextBox id="txtMaterial" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w203"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchmat" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w204"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearmat" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w205"></asp:ImageButton></TD><TD></TD><TD></TD></TR><TR style="FONT-SIZE: 8pt"><TD></TD><TD colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvMaterial" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w206" Visible="False" DataKeyNames="matoid,matlongdesc,matunit1oid,merk,lastPricebuy" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="8" GridLines="None" OnRowDataBound="gvMaterial_RowDataBound" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Last Stock">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastPricebuy" HeaderText="Harga Beli" Visible="False">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="Label19" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Item data found!!"></asp:Label>

                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Quantity&nbsp;<asp:Label id="Label67" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w207"></asp:Label></TD><TD><asp:TextBox id="trnbelidtlqty" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w208" MaxLength="30" AutoPostBack="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN>Price<asp:Label id="Label72" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w209"></asp:Label></SPAN></TD><TD><asp:TextBox id="trnbelidtlprice" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w210" MaxLength="30" AutoPostBack="True">0</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Unit <asp:Label id="Label16" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w211"></asp:Label></TD><TD><asp:DropDownList id="ddlUnit" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w212" AutoPostBack="True"></asp:DropDownList><asp:Label id="trnbelidtlunit" runat="server" __designer:wfdid="w213" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><SPAN><asp:Label id="Label4" runat="server" Width="69px" Text="Nett. Detail" __designer:wfdid="w214"></asp:Label></SPAN></TD><TD><asp:TextBox id="amtbelinetto2" runat="server" Width="100px" CssClass="inpTextDisabled" Font-Size="X-Small" __designer:wfdid="w215" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Note Detail</TD><TD colSpan=2><asp:TextBox id="trnbelidtlnote" runat="server" Width="433px" CssClass="inpText" __designer:wfdid="w216" MaxLength="250"></asp:TextBox></TD><TD><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w217"></asp:ImageButton> <asp:ImageButton id="btnCancelDtl" onclick="btnCancelDtl_Click1" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w218"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 98%; HEIGHT: 68%; BACKGROUND-COLOR: beige; TEXT-ALIGN: left"><asp:GridView id="tbldtl" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w219" Visible="False" DataKeyNames="seq" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtlnetto" HeaderText="Amt Netto">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="unitoid" HeaderText="unitoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnforecastoid" HeaderText="trnforecastoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="No PO data !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="Td1" colSpan=4 runat="server" Visible="false"><asp:Label id="Label1" runat="server" Width="29px" Font-Size="1.1em" Font-Bold="True" ForeColor="Navy" Text="Total" __designer:wfdid="w224"></asp:Label>&nbsp;<asp:Label id="Label63" runat="server" Width="48px" Font-Size="1.1em" Font-Bold="True" ForeColor="Navy" Text="Rupiah" __designer:wfdid="w225"></asp:Label> <asp:Label id="totalNya" runat="server" Width="62px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="0.00" __designer:wfdid="w226"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=4>Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="1688849860263964" __designer:wfdid="w227"></asp:Label> by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="1688849860263965" __designer:wfdid="w228"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=4><asp:ImageButton id="btnsavemstr" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:dtid="1688849860263968" __designer:wfdid="w229"></asp:ImageButton> <asp:ImageButton id="btnCancelMstr" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" __designer:dtid="1688849860263969" __designer:wfdid="w230"></asp:ImageButton> <asp:ImageButton id="btnDeleteMstr" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:dtid="1688849860263970" __designer:wfdid="w231" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsBottom" __designer:dtid="1688849860263971" __designer:wfdid="w232" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:dtid="1688849860263972" __designer:wfdid="w233" Visible="False"></asp:ImageButton> <asp:ImageButton id="BtnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:dtid="1688849860263973" __designer:wfdid="w234" Visible="False"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="1688849860263976" __designer:wfdid="w235" AssociatedUpdatePanelID="UpdatePanel4"><ProgressTemplate __designer:dtid="1688849860263977">
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w236"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:FilteredTextBoxExtender id="ftetrnbelidtlprice" runat="server" __designer:wfdid="w79" TargetControlID="trnbelidtlprice" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEtrnbelidtlqty" runat="server" __designer:wfdid="w80" TargetControlID="trnbelidtlqty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w81" TargetControlID="trnbelipodate" Mask="99/99/9999" UserDateFormat="MonthDayYear" MaskType="Date" CultureName="en-US" ErrorTooltipEnabled="True" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w82" TargetControlID="trnbelipodate" PopupButtonID="ImageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Forecast PO :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>


                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="HEIGHT: 18px; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: center" colspan="2">
                                <asp:Label ID="Label15" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White">Print Option</asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label34" runat="server" Text="Printer :"></asp:Label></td>
                            <td style="WIDTH: 8px; TEXT-ALIGN: left" class="Label">
                                <asp:DropDownList ID="ddlprinter" runat="server" Width="300px" CssClass="inpText"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" colspan="2">
                                <asp:Label ID="OtherTemp" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="NoTemp" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="idTemp" runat="server" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" colspan="2">
                                <asp:ImageButton ID="btpPrintAction" OnClick="btpPrintAction_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton>
                                <asp:ImageButton ID="btncloseprint" OnClick="btncloseprint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <br />
            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" PopupControlID="Panelvalidasi" BackgroundCssClass="modalBackground" PopupDragHandleControlID="label15" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btpPrintAction"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>
