<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="WaitingAction.aspx.vb" Inherits="Other_WaitingAction" Title="MULTI SARANA COMPUTER - Waiting For My Approval" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tableutama" align="center" border="0" cellpadding="0" cellspacing="0"
        width="979">
        <tr>
            <td style="width: 250px;" valign="top">
                <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" style="width: 200px; height: 15px;" valign="middle">
                            <asp:Image ID="Image2" runat="server" Height="16px" ImageUrl="~/Images/corner.gif"
                                Width="16px" ImageAlign="AbsMiddle" />
                            <asp:Label ID="lblWelcome" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Navy">Tipe Approval</asp:Label>
                            :.</th>
                    </tr>
                </table>
                <table border="1" cellpadding="5" cellspacing="0" class="tabelhias" style="width: 100%; height: 100%">
                    <tr>
                        <td align="left" valign="top">
                            <br />
                            <asp:LinkButton ID="lkbBack" runat="server" CssClass="submenu" Font-Size="Small">� Kembali</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="RealTomb" runat="server" CssClass="submenu" Font-Size="Small">� Realisasi Voucher</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbPO" runat="server" CssClass="submenu" Font-Size="Small">� Purchase Order</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="LblLinkFC" runat="server" CssClass="submenu" Font-Size="Small">� Forecast PO</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbPDO" runat="server" CssClass="submenu" Font-Size="Small">� Purchase Delivery Order</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkb_POClose" runat="server" CssClass="submenu" Font-Size="Small">� PO Close</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbPromoSupp" runat="server" CssClass="submenu" Font-Size="Small">� Promo Supplier</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbPReturn" runat="server" CssClass="submenu" Font-Size="Small">� Purchase Return</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbSO" runat="server" CssClass="submenu" Font-Size="Small">� Sales Order</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbSDO" runat="server" CssClass="submenu" Font-Size="Small">� Delivery Order</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="LkbSalesReturn" runat="server" CssClass="submenu" Font-Size="Small"
                                Width="111px">� Sales Return</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbTW" runat="server" CssClass="submenu" Font-Size="Small">� Transfer Warehouse</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbAS" runat="server" CssClass="submenu" Font-Size="Small">� Adjusment Stock</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbCN" runat="server" CssClass="submenu" Font-Size="Small">� Credit Note</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbDN" runat="server" CssClass="submenu" Font-Size="Small">� Debit Note</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbSJ" runat="server" CssClass="submenu" Font-Size="Small">� Surat Jalan</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbTWService" runat="server" CssClass="submenu" Font-Size="Small">� TW Service Customer</asp:LinkButton>
                            <br />
                            <br />
                            <asp:LinkButton ID="lkbTWServiceSupp" runat="server" CssClass="submenu" Font-Size="Small">� Service Supplier Internal</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="LkbReturNT" runat="server" CssClass="submenu" Font-Size="Small">� NT Retur</asp:LinkButton>
                            <br />
                            <br />
                            <asp:LinkButton ID="LkbFinalNya" runat="server" CssClass="submenu" Font-Size="Small">� Service Final</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="LkbFinalInternal" runat="server" CssClass="submenu" Font-Size="Small">� Service Final Internal</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="LkbPb" runat="server" CssClass="submenu" Font-Size="Small">� Permintaan Barang</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbRnpg" runat="server" CssClass="submenu" Font-Size="Small">� Nota HR Return</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbPO_RH" runat="server" CssClass="submenu" Font-Size="Small" Visible="False" Width="233px">� Purchase Order Retail-Holding</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbPO_R" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">� Purchase Order Retail</asp:LinkButton><br />
                            <br />
                            <asp:LinkButton ID="lkbPR" runat="server" CssClass="submenu" Font-Size="Small" Visible="False"></asp:LinkButton>
                            <br />
                            <br />
                            <asp:LinkButton ID="lkbSPK" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">� SPK Rakit/Transform</asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="lkbSQ" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">� Sales Order Project</asp:LinkButton>
                            <asp:LinkButton ID="LkbFS" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">� Sales Delivery Project</asp:LinkButton><br />
                            <asp:UpdatePanel ID="upMsgbox" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="PanelMsgBox" runat="server" CssClass="modalMsgBox" Visible="False" DefaultButton="btnMsgBoxOK">
                                        <table cellspacing="1" cellpadding="1" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" valign="top" align="left" colspan="2">
                                                        <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="left">
                                                        <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                                                    </td>
                                                    <td valign="top" align="left">
                                                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                                        <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                                        <asp:ImageButton ID="btnMsgBoxOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" DropShadow="True" Drag="True">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Button ID="beMsgBox" runat="server" CausesValidation="False" Visible="False"></asp:Button>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label></td>
                    </tr>
                </table>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
<asp:Panel id="PanelPass" runat="server" CssClass="modalBox" Visible="False" DefaultButton="btnOKPass"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 120px" align=center colSpan=3><asp:Label id="Label117" runat="server" Width="172px" Font-Size="Medium" Font-Bold="True" Text="Masukkan ID & Password"></asp:Label></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD colSpan=3><asp:Image id="imIcon2" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px"></asp:Image>&nbsp; <asp:Label id="lblMessageBottom" runat="server"></asp:Label></TD></TR><TR><TD></TD><TD><asp:Label id="refSO" runat="server" Visible="False"></asp:Label></TD><TD></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD align=right><asp:Label id="lblID" runat="server" Text="UserID"></asp:Label></TD><TD align=center><asp:Label id="Label45" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="userconfim" runat="server" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD align=right><asp:Label id="Label113" runat="server" Text="Password"></asp:Label></TD><TD align=center><asp:Label id="Label114" runat="server" Text=":"></asp:Label></TD><TD><asp:TextBox id="txtPass" runat="server" CssClass="inpText" TextMode="Password"></asp:TextBox></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnOKPass" onclick="btnOKPass_Click" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton> <asp:ImageButton id="btnCancelPass" onclick="btnCancelPass_Click" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton></TD></TR></TBODY></TABLE><asp:Label id="confirmCR" runat="server" Visible="False"></asp:Label> <asp:Label id="lblpiutang" runat="server" Visible="False"></asp:Label></asp:Panel> <asp:Button id="btnPass" runat="server" Visible="False"></asp:Button><BR /><ajaxToolkit:ModalPopupExtender id="MPEPass" runat="server" Drag="True" PopupControlID="PanelPass" PopupDragHandleControlID="Label117" BackgroundCssClass="modalBackground" TargetControlID="btnPass"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnOKPass"></asp:PostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
            <td style="height: 285px" valign="top" width="9">
                <br />
                &nbsp;
                <br />
            </td>
            <td valign="top">
                <table id="Table1" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" style="height: 15px;" valign="middle">
                            <img align="absMiddle" alt="" src="../Images/corner1.gif" />&nbsp; 
                            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                                ForeColor="Navy">Data Approval</asp:Label>
                            :.</th>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <div style="text-align: left">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><STRONG></STRONG><asp:Label id="Label21" runat="server" Font-Size="Small" Font-Bold="True" Text="Transaksi berikut menunggu proses approval :"></asp:Label><BR /><BR /><TABLE style="WIDTH: 373px"><TBODY><TR style="DISPLAY: none"><TD style="WIDTH: 143px; HEIGHT: 15px" align=left><asp:Label id="Label54" runat="server" Font-Bold="False" Text="Surat Jalan"></asp:Label></TD><TD style="WIDTH: 73px; HEIGHT: 15px" align=left>: <asp:Label id="sj" runat="server" Width="8px" Text="0"></asp:Label></TD></TR><TR style="DISPLAY: none"><TD style="WIDTH: 143px" align=left>SPK</TD><TD style="WIDTH: 73px" align=left>: <asp:Label id="spk" runat="server" Width="8px" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="lblRealV" runat="server" Text="Realisasi Voucher"></asp:Label></TD><TD style="WIDTH: 73px" align=left>: <asp:Label id="RealV" runat="server" Width="8px" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="lblPO" runat="server" Text="Purchase Order"></asp:Label></TD><TD style="WIDTH: 73px" align=left>: <asp:Label id="po" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="Label51" runat="server" Text="PO Close"></asp:Label></TD><TD style="WIDTH: 73px" align=left>: <asp:Label id="poclose" runat="server" Text="0"></asp:Label></TD></TR><TR style="DISPLAY: none"><TD style="WIDTH: 143px" id="TD4" align=left runat="server" Visible="false"><asp:Label id="lblPO_r" runat="server" Text="Purchase Order Retail"></asp:Label></TD><TD style="WIDTH: 73px" id="TD6" align=left runat="server" Visible="false">:&nbsp;<asp:Label id="po_r" runat="server" Text="0"></asp:Label></TD></TR><TR style="DISPLAY: none"><TD style="WIDTH: 143px" id="TD5" align=left runat="server" Visible="false"><asp:Label id="lblPO_rh" runat="server" Width="179px" Text="Purchase Order Retail-Holding"></asp:Label></TD><TD style="WIDTH: 73px" id="TD3" align=left runat="server" Visible="false">:&nbsp;<asp:Label id="po_rh" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="Label23" runat="server" Text="Purchase Delivery Order"></asp:Label></TD><TD style="WIDTH: 73px" align=left>: <asp:Label id="pdo" runat="server" Text="0"></asp:Label> </TD></TR><TR><TD style="WIDTH: 143px" id="Td9" align=left runat="server"><asp:Label id="Label1114" runat="server" Text="Purchase Return"></asp:Label></TD><TD style="WIDTH: 73px" id="Td26" align=left runat="server">: <asp:Label id="preturn0" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left runat="server"><asp:Label id="lblAS" runat="server" Text="Adjusment Stock"></asp:Label></TD><TD style="WIDTH: 73px" align=left runat="server">: <asp:Label id="stockadj" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left runat="server"><asp:Label id="lblCN" runat="server" Text="Credit Note"></asp:Label></TD><TD style="WIDTH: 73px" align=left runat="server">: <asp:Label id="creditnote" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left runat="server"><asp:Label id="lblDN" runat="server" Text="Debit Note"></asp:Label></TD><TD style="WIDTH: 73px" align=left runat="server">: <asp:Label id="debitnote" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="lblSO" runat="server" Text="Sales Order"></asp:Label></TD><TD style="WIDTH: 73px" align=left>: <asp:Label id="so" runat="server" Text="0"></asp:Label></TD></TR><TR style="DISPLAY: none"><TD style="WIDTH: 143px" id="TD8" align=left runat="server" Visible="false"><asp:Label id="lblPR" runat="server" Font-Bold="False" Text="Purchase Requisition"></asp:Label></TD><TD style="WIDTH: 73px" id="TD7" align=left runat="server" Visible="false">: <asp:Label id="pr" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left><asp:Label id="Label15" runat="server" Text="Sales Delivery Order"></asp:Label></TD><TD style="WIDTH: 73px" align=left>:&nbsp;<asp:Label id="sdo" runat="server" Text="0"></asp:Label></TD></TR><TR style="DISPLAY: none"><TD style="WIDTH: 143px" id="TD11" align=left runat="server" visible="false"><asp:Label id="Label27" runat="server" Text="Internal Order"></asp:Label></TD><TD style="WIDTH: 73px" id="TD12" align=left runat="server" visible="false">: <asp:Label id="IO" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" id="Td10" align=left runat="server"><asp:Label id="Label37" runat="server" Text="Transfer Warehouse"></asp:Label></TD><TD style="WIDTH: 73px" id="Td17" align=left runat="server">: <asp:Label id="tw0" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" align=left runat="server"><asp:Label id="Label47" runat="server" Text="Nota HR Return" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 73px" align=left runat="server">: <asp:Label id="rnpg0" runat="server" Text="0" __designer:wfdid="w2"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" id="TD31" align=left runat="server" Visible="false"><asp:Label id="Label16" runat="server" Text="TW Service Supplier"></asp:Label></TD><TD style="WIDTH: 73px" id="TD32" align=left runat="server" Visible="false">: <asp:Label id="TwsCok" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" id="Td20" align=left runat="server" Visible="false"><asp:Label id="lblSQ" runat="server" Text="Sales Order Project"></asp:Label></TD><TD style="WIDTH: 73px" id="Td21" align=left runat="server" Visible="false">: <asp:Label id="sq" runat="server" Text="0"></asp:Label></TD></TR><TR><TD style="WIDTH: 143px" id="Td22" align=left runat="server" Visible="false"><asp:Label id="Label29" runat="server" Text="Sales Delivery Project"></asp:Label></TD><TD style="WIDTH: 73px" id="Td23" align=left runat="server" Visible="false">: <asp:Label id="FS" runat="server" Text="0"></asp:Label></TD></TR></TBODY></TABLE></asp:View>&nbsp; <asp:View id="View5" runat="server"><asp:Label id="labelPO" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Purchase Order List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvPO" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" DataKeyNames="pomstoid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." OnSelectedIndexChanged="gvPO_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="pomstoid" HeaderText="pomstoid" SortExpression="pomstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="PO No"><ItemTemplate>
<asp:LinkButton id="lkbSelectPO" onclick="lkbSelectPO_Click" runat="server" Text='<%# Eval("pono") %>' __designer:wfdid="w4" CommandArgument='<%# Eval("approvaloid") %>' ToolTip='<%# Eval("pomstoid") %>' CommandName='<%# Eval("branch_code") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="PO Date"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("podate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# FORMAT(cdate(Eval("podate")), "dd/MM/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pohdrnote" HeaderText="PO Note" SortExpression="pohdrnote">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("requestdate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")), "dd/MM/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:View><BR /><asp:View id="View6" runat="server"><BR /><asp:Label id="Label3" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Purchase Order Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE><TBODY><TR><TD style="FONT-WEIGHT: bold" align=left>PO No</TD><TD align=left>: <asp:Label id="trnbelipono" runat="server" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>PO Date</TD><TD align=left>: <asp:Label id="trnbelipodate" runat="server" __designer:wfdid="w2"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Supplier</TD><TD align=left>: <asp:Label id="suppname" runat="server" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Tujuan</TD><TD align=left>: <asp:Label id="Tujuan" runat="server" __designer:wfdid="w4"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Tax</TD><TD align=left>: <asp:Label id="taxpo" runat="server" __designer:wfdid="w5"></asp:Label> <asp:Label id="trnbelitype" runat="server" Visible="False" __designer:wfdid="w6"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; HEIGHT: 15px" align=left>PO Note</TD><TD style="HEIGHT: 15px" align=left>: <asp:Label id="trnbelinote" runat="server" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Disc. Hdr</TD><TD align=left>: <asp:Label id="dischdrpo" runat="server" Font-Bold="False" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Market</TD><TD align=left>: <asp:Label id="market" runat="server" __designer:wfdid="w9"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold" id="TD1" align=left runat="server" visible="false">Identifier</TD><TD id="TD2" align=left runat="server" visible="false">: <asp:Label id="poidentifierno" runat="server" __designer:wfdid="w10"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" id="Td33" align=left runat="server" visible="false"><asp:Label id="TypePO" runat="server" Visible="False" __designer:wfdid="w11"></asp:Label></TD><TD id="Td34" align=left runat="server" visible="false"><asp:Label id="AsCabang" runat="server" __designer:wfdid="w12"></asp:Label><asp:Label id="KeCabang" runat="server" __designer:wfdid="w13"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" id="Td37" align=left runat="server" visible="true"><asp:Label id="lblbln3" runat="server" Font-Size="Smaller" Visible="False" Text="Label" __designer:wfdid="w14"></asp:Label> <asp:Label id="lblbln1" runat="server" Font-Size="Smaller" Visible="False" Text="Label" __designer:wfdid="w15"></asp:Label> <asp:Label id="lblbln2" runat="server" Font-Size="Smaller" Visible="False" Text="Label" __designer:wfdid="w16"></asp:Label></TD><TD id="Td38" align=left runat="server" visible="true">&nbsp;<asp:Label id="StatusPO" runat="server" Visible="False" __designer:wfdid="w17"></asp:Label> <asp:Label id="pores1" runat="server" Visible="False" __designer:wfdid="w18"></asp:Label></TD></TR><TR><TD id="Td35" align=left colSpan=2 runat="server" visible="true"><asp:Label id="pocreate" runat="server" __designer:wfdid="w19"></asp:Label></TD></TR><TR><TD id="Td36" align=left colSpan=2 runat="server" visible="true"><asp:Label id="posentapproval" runat="server" __designer:wfdid="w20"></asp:Label></TD></TR></TBODY></TABLE><BR /><DIV style="OVERFLOW-Y: scroll; WIDTH: 700px; HEIGHT: 200px; BACKGROUND-COLOR: beige; TEXT-ALIGN: left"><asp:GridView id="gvPODetail" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w6" OnSelectedIndexChanged="gvPODetail_SelectedIndexChanged" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="podtlseq" HeaderText="No" SortExpression="prdtlseq">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="10px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="MATLONGDESC" HeaderText="Material" SortExpression="MATLONGDESC">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtyAkhir" HeaderText="Stock ALL">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln3" HeaderText="bln3">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="40px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln2" HeaderText="bln2">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="40px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln1" HeaderText="bln1">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="40px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlqty" HeaderText="Qty" SortExpression="prqty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="LastPrice" HeaderText="Last price">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlprice" HeaderText="Unit Price" SortExpression="unitprice">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="poamtdltdisc" HeaderText="Total Disc">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small" Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucher" HeaderText="Voucher">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="80px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalprice" HeaderText="Total Price" SortExpression="totalprice">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jBln3" HeaderText="bln3">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jBln2" HeaderText="bln2">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jBln1" HeaderText="bln1">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtyCabang">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!" __designer:wfdid="w3"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="lblTotalOrder" runat="server" Font-Size="8pt" Font-Bold="True" Visible="False" Text="Total Purchase Order = 0.00"></asp:Label><BR /><STRONG>Net Purchase Amount = <asp:Label id="amtbelinetto" runat="server"></asp:Label><BR /><BR /></STRONG><asp:Label id="lblRevisePO" runat="server" Visible="False"></asp:Label><BR /><asp:TextBox id="txtRevisePO" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine"></asp:TextBox><BR /><BR /><asp:ImageButton id="imbApprovePO" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="btnRevisePO" onclick="btnRevisePO_Click" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" AlternateText="Revised"></asp:ImageButton> <asp:ImageButton id="imbRejectPO" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton>&nbsp;<asp:Button id="btnSubRevisePO" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision"></asp:Button>&nbsp;<asp:Button id="btnCanRevisePO" onclick="btnCanRevisePO_Click" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision"></asp:Button> <asp:ImageButton id="imbBackPO" onclick="imbBackPO_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton></asp:View><asp:View id="View30" runat="server"><BR /><asp:Label id="lblas1" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Adjustment Stock List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvASMst" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" OnSelectedIndexChanged="gvPO_SelectedIndexChanged" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="draftno" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Draft No."><ItemTemplate>
<asp:LinkButton id="lkbSelectAS" onclick="lkbSelectAS_Click" runat="server" Text='<%# Eval("draftno") %>' ToolTip='<%# Eval("draftno") %>' CommandArgument='<%# Eval("approvaloid") %>' CommandName='<%# Eval("branch_code") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="adjdate" HeaderText="Adj. Date">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="type" HeaderText="Type" SortExpression="type">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("requestdate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")), "dd/MM/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View31" runat="server"><BR /><asp:Label id="lblas2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Stock Adjusment Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Draft No.</TD><TD align=left>: <asp:Label id="draftno" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Adj. Date</TD><TD align=left>: <asp:Label id="adjdate" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Type</TD><TD align=left>: <asp:Label id="Type" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Total Adj</TD><TD align=left>: <asp:Label id="totaladj" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Last Update By</TD><TD align=left>: <asp:Label id="lastupdby" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Last Update On</TD><TD align=left>: <asp:Label id="lastupdon" runat="server"></asp:Label> <asp:Label id="branch" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE> <DIV style="OVERFLOW-Y: scroll; WIDTH: 750px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvASDtl" runat="server" ForeColor="#333333" __designer:wfdid="w1" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="currqty" HeaderText="Current Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="newqty" HeaderText="Adj. Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Hpp"><ItemTemplate>
<asp:TextBox id="HppNya" runat="server" Width="100px" CssClass="inpText" Text='<%# String.Format("{0:#,##0.00}", GetHppNya()) %>' __designer:wfdid="w3" MaxLength="20" ToolTip='<%# Eval("value") %>'></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftHppNya" runat="server" TargetControlID="HppNya" __designer:wfdid="w4" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="noteas" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtrwhoid" HeaderText="mtrlocoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data not found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><BR /><asp:ImageButton id="imgAppAs" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="imgRejectAS" onclick="imgRejectAS_Click" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="imgBackAS" onclick="imgBackAS_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton> </asp:View><asp:View id="View36" runat="server"><BR /><asp:Label id="lblCN1" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Credit Note List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvCNMst" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" OnSelectedIndexChanged="gvPO_SelectedIndexChanged" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="draftno" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Draft No."><ItemTemplate>
<asp:LinkButton id="lkbSelectCN" onclick="lkbSelectCN_Click" runat="server" Text='<%# Eval("draftno") %>' __designer:wfdid="w1" ToolTip='<%# Eval("draftno") %>' CommandArgument='<%# Eval("approvaloid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="tgl" HeaderText="CN. Date">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftype" HeaderText="Type" SortExpression="type">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("requestdate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")), "dd/MM/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View><asp:View id="View37" runat="server"><BR /><asp:Label id="lblCN2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Credit Note Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Draft No.</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="oid" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>CN. Date</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="tgl" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Type</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="reftype" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Total CN</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="amount" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Last Update By</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="updusercn" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Last Update On</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="updtimecn" runat="server"></asp:Label> <asp:Label id="branch_code_cn" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="cust_supp_oid" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="trnjualbelioid" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="trnjualbelino" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="cnstatus" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="coa_credit" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="coa_debet" runat="server" Visible="False"></asp:Label>&nbsp;</TD></TR></TBODY></TABLE>&nbsp;<asp:GridView style="WIDTH: 99%" id="gvCNDtl" runat="server" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="15" OnRowDataBound="gvCNDtl_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="No. Nota">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer / Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="amount">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data not found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:ImageButton id="imgAppCN" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="imgRejectCN" onclick="imgRejectCN_Click" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="imgBackCN" onclick="imgBackCN_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton> </asp:View><asp:View id="View38" runat="server"><BR /><asp:Label id="lblDN1" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Debit Note List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvDNMst" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" OnSelectedIndexChanged="gvPO_SelectedIndexChanged" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="draftno" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Draft No."><ItemTemplate>
<asp:LinkButton id="lkbSelectDN" runat="server" Text='<%# Eval("draftno") %>' ToolTip='<%# Eval("draftno") %>' CommandArgument='<%# Eval("approvaloid") %>' OnClick="lkbSelectDN_Click"></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="tgl" HeaderText="DN. Date">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftype" HeaderText="Type" SortExpression="type">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("requestdate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")), "dd/MM/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View><asp:View id="View39" runat="server"><BR /><asp:Label id="lblDN2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Debit Note Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Draft No.</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="oiddn" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>DN. Date</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="tgldn" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Type</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="reftypedn" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Total DN</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="amountdn" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Last Update By</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="upduserdn" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 107px" align=left>Last Update On</TD><TD style="WIDTH: 688px" align=left>: <asp:Label id="updtimedn" runat="server"></asp:Label> <asp:Label id="branch_code_dn" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="cust_supp_oiddn" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="trnjualbelioiddn" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="trnjualbelinodn" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="cnstatusdn" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="coa_debetdn" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="coa_creditdn" runat="server" Visible="False"></asp:Label>&nbsp;</TD></TR></TBODY></TABLE>&nbsp;<asp:GridView style="WIDTH: 99%" id="gvDNDtl" runat="server" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="15" OnRowDataBound="gvDNDtl_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="No. Nota">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer / Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="amount">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data not found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:ImageButton id="imgAppDN" onclick="imgAppDN_Click" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="imgRejectDN" onclick="imgRejectDN_Click" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="imgBackDN" onclick="imgBackDN_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton> </asp:View><BR /><asp:View id="View9" runat="server"><BR /><asp:Label id="Label10" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Sales Order Projeck List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvSQ" runat="server" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" DataKeyNames="quotoid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="quotoid" HeaderText="quotoid" SortExpression="quotoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="Projeck. No"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("quotno") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectSQ" onclick="lkbSelectSQ_Click" runat="server" Text='<%# Eval("quotno") %>' CommandArgument='<%# Eval("approvaloid") %>' ToolTip='<%# Eval("quotoid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Projeck. Date"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("quotdate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# FORMAT(cdate(Eval("quotdate")), "MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="quotnote" HeaderText="Projeck. Note" SortExpression="quotnote">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")),"MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View10" runat="server"><BR /><asp:Label id="Label11" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Sales Order Projeck Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 112px" align=left>SO Projeck.&nbsp;No</TD><TD align=left>: <asp:Label id="quotno" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>&nbsp;Customer</TD><TD align=left>: <asp:Label id="custnamequot" runat="server"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 112px" align=left>SO Projeck. Date</TD><TD align=left>: <asp:Label id="quotdate" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>&nbsp;SO Projeck. Note</TD><TD align=left>: <asp:Label id="quotnote" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 112px" align=left>Taxable</TD><TD align=left>: <asp:Label id="quotax" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="SOPJconfimID" runat="server" Visible="False"></asp:Label></TD><TD align=left></TD></TR></TBODY></TABLE><BR /><asp:Label id="Label20" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Navy" Text="Sales Order Projeck Item :" Font-Underline="True"></asp:Label>&nbsp;<asp:Label id="lblTotalSQ" runat="server" Font-Size="8pt" Font-Bold="True" Text="Total Sales Order Projeck = 0.00"></asp:Label><asp:GridView id="gvSQDetail" runat="server" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemlongdesc" HeaderText="Item" SortExpression="itemlongdesc">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="quotqty" HeaderText="Quantity" SortExpression="quotqty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unitprice" HeaderText="Unit Price" SortExpression="unitprice">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalprice" HeaderText="Total Price" SortExpression="totalprice">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemnote" HeaderText="Note" SortExpression="itemnote">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:Label id="Label22" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Navy" Visible="False" Text="Sales Quotation Design & Color :" Font-Underline="True"></asp:Label><asp:GridView id="gvSQDesign" runat="server" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" Visible="False" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="designcolor" HeaderText="Desing &amp; Color" SortExpression="designcolor">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="600px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="600px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:ImageButton id="imbApproveSQ" onclick="imbApproveSQ_Click" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbRejectSQ" onclick="imbRejectSQ_Click" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbBackSQ" onclick="imbBackSQ_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton> </asp:View> <asp:View id="View11" runat="server"><BR /><asp:Label id="Label12" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Sales Order List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvSO" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="orderoid" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="orderid" HeaderText="orderoid" SortExpression="orderoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="Order No"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("orderno") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectSO" onclick="lkbSelectSO_Click" runat="server" Text='<%# Eval("orderno") %>' Enabled='<%# Eval("branch_code") %>' ToolTip='<%# Eval("orderoid") %>' CommandArgument='<%# Eval("approvaloid") %>' CommandName='<%# Eval("branch_code") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Order Date"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("orderdate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# FORMAT(cdate(Eval("orderdate")), "MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ordernote" HeaderText="Order Note" SortExpression="ordernote" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")),"MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View12" runat="server"><BR /><asp:Label id="Label13" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Sales Order Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE style="WIDTH: 664px" id="TABLE2" runat="server" visible="true"><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 92px" align=left>Order&nbsp;No</TD><TD style="WIDTH: 162px" align=left>: <asp:Label id="orderno" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 97px" align=left>Customer</TD><TD style="WIDTH: 175px" align=left>: <asp:Label id="custnameorder" runat="server"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 92px" align=left>Order Date</TD><TD style="WIDTH: 162px" align=left>: <asp:Label id="orderdate" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 97px" align=left>Delivery Addr.</TD><TD style="WIDTH: 175px" align=left>: <asp:Label id="SOdeliveadd" runat="server" Width="115px"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 92px" align=left>Delivery Date</TD><TD style="WIDTH: 162px" align=left>: <asp:Label id="SOdevdate" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 97px" align=left>Cabang</TD><TD style="WIDTH: 175px" align=left>: <asp:Label id="lblcabang" runat="server"></asp:Label> <asp:Label id="SOType" runat="server" Visible="False" Text="SOType"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 92px" align=left>Taxable</TD><TD style="WIDTH: 162px" align=left>: <asp:Label id="SOtax" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 97px" align=left>Currency</TD><TD style="WIDTH: 175px" align=left>:&nbsp;<asp:Label id="curr" runat="server" Visible="False" Text="lblcurr"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 92px" id="TD16" align=left runat="server" visible="false">Identifier</TD><TD style="WIDTH: 162px" id="TD15" align=left runat="server" visible="false">: <asp:Label id="soidentifierno" runat="server"></asp:Label> <asp:Label id="SOBO" runat="server" Width="48px"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 97px" id="TD13" align=left runat="server" visible="false"><asp:Label id="SOconfimID" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 175px" id="TD14" align=left runat="server" visible="false"><asp:Label id="diskon" runat="server" Visible="False"></asp:Label> <asp:Label id="currSO" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="lblflagprice" runat="server" Visible="False" Text="lblflagprice"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 92px" id="Td28" align=left runat="server" visible="true">TOP</TD><TD style="WIDTH: 162px" id="Td29" align=left runat="server" visible="true">:&nbsp;<asp:Label id="timeOP" runat="server">timeOP</asp:Label>&nbsp;<asp:Label id="Label50" runat="server" Text="Day"></asp:Label>&nbsp;<asp:Label id="lblflag" runat="server" Width="32px" ForeColor="#C00000" Text="(*)"></asp:Label><SPAN style="COLOR: #cc0000"></SPAN></TD><TD style="FONT-WEIGHT: bold; WIDTH: 97px" id="Td30" align=left runat="server" visible="true"><asp:Label id="lblrate2oid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 175px" id="Td27" align=left runat="server" visible="true"><asp:Label id="topCust" runat="server" Visible="False" Text="topCust"></asp:Label><asp:Label id="lblflagdtl" runat="server" Visible="False"></asp:Label><asp:Label id="lblrate" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><asp:Label id="lbldate" runat="server" Width="39px" Visible="False" Text="Label"></asp:Label><asp:Label id="ordernote" runat="server" Width="50px" Visible="False"></asp:Label><BR /><asp:GridView id="gvSODetail" runat="server" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" OnSelectedIndexChanged="gvSODetail_SelectedIndexChanged" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="gvSODetail_RowDataBound">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemdesc" HeaderText="Item" SortExpression="itemdesc">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderqty" HeaderText="Quantity" SortExpression="orderqty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="unitprice" HeaderText="Price SO" SortExpression="unitprice">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalprice" HeaderText="Total Price" SortExpression="totalprice">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemnote" HeaderText="Note" SortExpression="itemnote" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="free" HeaderText="Free" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="flagbottompricedtl">
<ControlStyle BackColor="White" BorderColor="White" BorderStyle="None"></ControlStyle>

<HeaderStyle BorderStyle="None" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle BorderStyle="None" ForeColor="White"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricelist" HeaderText="Item PriceList" SortExpression="pricelist">
<ControlStyle BorderColor="Red"></ControlStyle>

<HeaderStyle BackColor="#FFFFC0" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle BackColor="#FFFFC0"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="note1" runat="server" ForeColor="Red" Visible="False" Text="(*) Ada Perubahan Time Of Payment  " Font-Italic="True"></asp:Label><BR /><SPAN style="COLOR: #ff0000"></SPAN><SPAN style="COLOR: #ff0000"></SPAN><SPAN style="COLOR: #ff0000"></SPAN><asp:Label id="note2" runat="server" ForeColor="Red" Visible="False" Text="*Warna Merah Pada Kolom Price SO adalah Harga yang di bawah bottom price" Font-Italic="True"></asp:Label><BR /><asp:Label id="lblpricelist" runat="server" ForeColor="Green" Visible="False" Text="*Price SO tidak sama dengan Item Pricelist" Font-Italic="True"></asp:Label><BR /><BR /><TABLE id="Table3" width="100%" runat="server" visible="true"><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 123px" align=left><asp:Label id="Label33" runat="server" Font-Size="8pt" Font-Bold="True" Text="Total Sales Order "></asp:Label></TD><TD align=left>: <asp:Label id="netto" runat="server" Font-Size="8pt" Font-Bold="True" Text="0.00"></asp:Label><asp:Label id="currSO2" runat="server" Font-Size="8pt" Font-Bold="True" Visible="False"></asp:Label><asp:Label id="lblTotalSO" runat="server" Font-Size="8pt" Font-Bold="True" Visible="False" Text="0.00"></asp:Label><asp:Label id="Discount" runat="server" Font-Size="8pt" Font-Bold="True" Visible="False" Text="0.00"></asp:Label> <asp:Label id="lblcurr1" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 123px" align=left><asp:Label id="textdpp" runat="server" Width="88px" Font-Bold="True" Visible="False" Text="DPP Amount"></asp:Label></TD><TD align=left>&nbsp;<asp:Label id="txtdpp" runat="server" Font-Bold="True" Visible="False"></asp:Label> <asp:Label id="total1" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 123px" align=left><asp:Label id="tax" runat="server" Font-Bold="True" Visible="False" Text="Tax Amount "></asp:Label></TD><TD align=left>&nbsp;<asp:Label id="amttax" runat="server" Font-Bold="True" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 123px" align=left></TD><TD align=left></TD></TR></TBODY></TABLE><BR /><asp:GridView id="gvSODesign" runat="server" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" Visible="False" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="designcolor" HeaderText="Desing &amp; Color" SortExpression="designcolor">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="600px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="600px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:Label id="lbl_ReviseSO" runat="server" Visible="false"></asp:Label> <BR /><asp:TextBox id="txt_reviseSO" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine"></asp:TextBox> <BR /><asp:ImageButton id="imbApproveSO" onclick="imbApproveSO_Click" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbRejectSO" onclick="imbRejectSO_Click" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImbReviseSO" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" AlternateText="Revised"></asp:ImageButton>&nbsp;<asp:Button id="btnReviseSubmitSO" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision"></asp:Button> <asp:Button id="btncancelreviseSO" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision"></asp:Button> <asp:ImageButton id="imbBackSO" onclick="imbBackSO_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton> </asp:View> <asp:View id="View2" runat="server"><BR /><asp:Label id="Label17" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Delivery Order List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvSDO" runat="server" Width="100%" Height="1px" ForeColor="#333333" GridLines="None" DataKeyNames="salesdeliveryoid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="salesdeliveryoid" HeaderText="salesdeliveryoid" SortExpression="salesdeliveryoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="No"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" CssClass="inpText" Text='<%# Bind("trnsjjualno") %>' __designer:wfdid="w2"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectSDO" onclick="lkbSelectSDO_Click" runat="server" Text='<%# Eval("salesdeliveryno") %>' __designer:wfdid="w1" CommandName='<%# Eval("branch_code") %>' CommandArgument='<%# Eval("approvaloid") %>' ToolTip='<%# Eval("salesdeliveryoid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Delv. Date"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("trnsjjualsendate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# FORMAT(cdate(Eval("salesdeliverydate")), "MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")),"MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" HeaderText="Cabang Code" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View13" runat="server"><asp:Label id="Label18" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Delivery Order Data :" Font-Underline="True"></asp:Label> <asp:DropDownList id="TOP" runat="server" CssClass="inpTextDisabled" Visible="False"></asp:DropDownList><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold; HEIGHT: 15px" align=left>Seles Order No</TD><TD style="WIDTH: 240px; HEIGHT: 15px" align=left>:&nbsp;<asp:Label id="sjorderno" runat="server"></asp:Label> <asp:Label id="zCabang" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-WEIGHT: bold; HEIGHT: 15px" align=left>Delivery Order&nbsp;No</TD><TD style="WIDTH: 219px; HEIGHT: 15px" align=left>:&nbsp;<asp:Label id="trnsjjualno" runat="server"></asp:Label>&nbsp;<asp:Label id="sjjualmstoid" runat="server" Visible="False"></asp:Label>&nbsp;</TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Delivery&nbsp;Date</TD><TD style="WIDTH: 240px" align=left>: <asp:Label id="trnsjjualsenddate" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>Police No</TD><TD style="WIDTH: 219px" align=left>: <asp:Label id="SjPolisce" runat="server"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Ekspedisi Name</TD><TD style="WIDTH: 240px" align=left>: <asp:Label id="EkspedisiSj" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>Customer</TD><TD style="WIDTH: 219px" align=left>: <asp:Label id="sjcustname" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Address</TD><TD style="WIDTH: 240px" align=left>: <asp:Label id="CustAddress" runat="server"></asp:Label> </TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="SeTipe" runat="server"></asp:Label><asp:Label id="OidCust" runat="server" Visible="False"></asp:Label><asp:Label id="OidSO" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 219px" align=left><asp:Label id="ToBranchSO" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left></TD><TD align=left colSpan=3>&nbsp;<asp:Label id="sjidentifierno" runat="server" Visible="False"></asp:Label> <asp:Label id="note" runat="server" Visible="False"></asp:Label> <asp:Label id="posting" runat="server" Visible="False"></asp:Label> <asp:Label id="nopolisi" runat="server" Visible="False"></asp:Label> <asp:Label id="Ekspedisi" runat="server" Visible="False"></asp:Label> <asp:Label id="trnSjtype" runat="server" Visible="False"></asp:Label> <asp:Label id="SalesOid" runat="server" Visible="False"></asp:Label> <asp:Label id="SpgOid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left></TD><TD align=left colSpan=3>&nbsp;<asp:Label id="trnjualno" runat="server" Visible="False"></asp:Label> <asp:Label id="trncustoid" runat="server" Visible="False"></asp:Label> <asp:Label id="trntaxpct" runat="server" Visible="False"></asp:Label> <asp:Label id="currencyoidSo" runat="server" Visible="False"></asp:Label> <asp:Label id="salesdeliveryshipdate" runat="server" Visible="False"></asp:Label> <asp:Label id="ApprovedOid" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=4><asp:Label id="Label19" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Delivery Order Item :" Font-Underline="True"></asp:Label> <asp:Label id="MsgNya" runat="server" Visible="False" Text="MsgNya"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=4><asp:GridView id="gvSDODetail" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnsjjualdtlseq,salesdeliverydtloid,itemlongdesc,itemoid" GridLines="None" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnsjjualdtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Item">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="salesdeliveryqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderprice" HeaderText="Order Price" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qtypak" HeaderText="Qty Coly" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
                                                                <asp:LinkButton ID="LinkButton9" runat="server" CommandName='<%# Eval("salesdeliveryqty") %>' CommandArgument='<%# Eval("itemlongdesc") %>' ToolTip='<%# Eval("itemoid") %>'>Detail No. Roll</asp:LinkButton>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data!!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lbl_revisedo" runat="server" Visible="false"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=4><asp:TextBox id="txt_revisedo" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=4><asp:ImageButton id="imbApproveSDO" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbRejectSDO" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbReviseDO" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" AlternateText="Revised"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbBackSDO" onclick="imbBackSDO_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Button id="btnReviseDO" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision"></asp:Button>&nbsp;<asp:Button id="btnCancelReviseDO" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision"></asp:Button>&nbsp;<asp:ImageButton id="PostBtn" onclick="PostBtn_Click" runat="server" ImageUrl="~/Images/Approval.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE>&nbsp;&nbsp;&nbsp;&nbsp; </asp:View> <asp:View id="View14" runat="server"><asp:Label id="Label24" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Purchase Delivery Order List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvPDO" runat="server" Width="100%" Height="1px" ForeColor="#333333" GridLines="None" DataKeyNames="trnsjbelioid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnsjbelioid" HeaderText="trnsjbelioid" SortExpression="trnsjbelioid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="No"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("trnsjbelino") %>' __designer:wfdid="w2"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectPDO" onclick="lkbSelectPDO_Click" runat="server" Text='<%# Eval("trnsjbelino") %>' __designer:wfdid="w1" ToolTip='<%# Eval("trnsjbelioid") %>' CommandArgument='<%# Eval("approvaloid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Recv. Date"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("trnsjreceivedate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# FORMAT(cdate(Eval("trnsjreceivedate")), "dd/MM/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")),"dd/MM/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View15" runat="server"><BR /><asp:Label id="Label25" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Purchase Delivery Order Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 98px" align=left>Delivery No</TD><TD align=left>: <asp:Label id="trnsjbelino" runat="server"></asp:Label>&nbsp;<asp:Label id="trnsjbelioid" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>&nbsp;PO No</TD><TD align=left>:&nbsp;<asp:Label id="trnbeliponosj" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 98px" align=left>Delivery&nbsp;Date</TD><TD align=left>: <asp:Label id="trnsjreceivedate" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left></TD><TD align=left>&nbsp;<asp:Label id="identifierpo" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 98px" align=left>Supplier</TD><TD align=left colSpan=3>: <asp:Label id="suppnamepdo" runat="server"></asp:Label>&nbsp;<asp:Label id="trnsjbelitype" runat="server" Visible="False"></asp:Label> <asp:Label id="sCbg" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><BR /><asp:GridView id="gvPDODetail" runat="server" Width="100%" Height="1px" ForeColor="#333333" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="item" HeaderText="Item" SortExpression="item">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelidtlqty" HeaderText="Qty 1" SortExpression="trnsjbelidtlqty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit" SortExpression="unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelidtlnote" HeaderText="Note" SortExpression="trnsjbelidtlnote">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 125px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset7" runat="server" Visible="false"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 120px"><asp:GridView id="gvMst" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="No" HeaderText="No">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="SO No.">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="delivery" HeaderText="Delivery">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="needbooking" HeaderText="Qty BO">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty Booking">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note" Visible="False"><ItemTemplate>
                                                                        <asp:Label ID="labelitemoid" runat="server" Text='<%# Eval("itemoid") %>'></asp:Label>
                                                                    
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
                                                                        <asp:TextBox ID="newqty" runat="server" Width="70px" CssClass="inpText" Text='<%# String.Format("{0:#,##0.00}", Eval("saldoawal")) %>' MaxLength="7" AutoPostBack="True" BackColor="#C0FFC0" OnTextChanged="newprice_TextChanged"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="newqty" ValidChars="1234567890.,-">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    
</ItemTemplate>

<ItemStyle Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
                                                                        <asp:TextBox ID="note" runat="server" CssClass="inpText" MaxLength="200" Text='<%# Eval("note") %>' Width="250px"></asp:TextBox>
                                                                    
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
                                                                        <asp:Label ID="labelordermstoid" runat="server" Text='<%# Eval("ordermstoid") %>'></asp:Label>
                                                                    
</ItemTemplate>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="No Allocation Data !!!"></asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" Font-Size="X-Small"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> <asp:Label id="lblwarningBO" runat="server" CssClass="Important"></asp:Label><BR /><BR /><asp:ImageButton id="imbApprovePDO" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbRejectPDO" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbBackPDO" onclick="imbBackPDO_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:View>&nbsp;&nbsp; <asp:View id="View18" runat="server"><BR /><asp:Label id="Label30" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Sales Delivery Order Projeck List :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GvFS" runat="server" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" DataKeyNames="orderoid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="orderid" HeaderText="orderoid" SortExpression="orderoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="Order No"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("orderno") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:LinkButton ID="lkbSelectFS" OnClick="lkbSelectFS_Click" runat="server" Text='<%# Eval("orderno") %>' CommandArgument='<%# Eval("approvaloid") %>' ToolTip='<%# Eval("orderoid") %>'></asp:LinkButton>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Order Date"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("orderdate") %>'></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# FORMAT(cdate(Eval("orderdate")), "MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="username" HeaderText="Customer" SortExpression="username">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ordernote" HeaderText="Order Note" SortExpression="ordernote">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")),"MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View19" runat="server"><BR /><asp:Label id="Label31" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Sales Delivery Order Projeck Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE><TBODY><TR><TD style="FONT-WEIGHT: bold; HEIGHT: 18px" align=left>Delivery Order&nbsp;No</TD><TD style="HEIGHT: 18px" align=left>: <asp:Label id="OrdernoFS" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold; HEIGHT: 18px" align=left>&nbsp;Customer</TD><TD style="HEIGHT: 18px" align=left>: <asp:Label id="custnameorderFS" runat="server"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Delivery Order Date</TD><TD align=left>: <asp:Label id="OrderDateFS" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>&nbsp;Order No</TD><TD align=left>: <asp:Label id="ordernoteFS" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left></TD><TD align=left>&nbsp;<asp:Label id="soidentifiernoFS" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left></TD><TD align=left></TD></TR></TBODY></TABLE><BR /><asp:Label id="Label39" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Navy" Text="Sales Delivery Projeck Item :" Font-Underline="True"></asp:Label>&nbsp;<asp:Label id="lblTotalFS" runat="server" Font-Size="8pt" Font-Bold="True" Visible="False" Text="Total Free Stock = 0.00"></asp:Label><BR /><BR /><asp:GridView id="GVfsDetail" runat="server" Width="99%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnsjjualdtlseq,salesdeliverydtloid,itemlongdesc,itemoid" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnsjjualdtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Item">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="salesdeliveryqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtypak" HeaderText="Qty Coly">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data!!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:Label id="Label41" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Navy" Visible="False" Text="Free Stock Design & Color :" Font-Underline="True"></asp:Label><asp:GridView id="GVfsDesign" runat="server" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" Visible="False" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="designcolor" HeaderText="Desing &amp; Color" SortExpression="designcolor">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="600px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="600px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:ImageButton id="imbApproveFS" onclick="imbApproveFS_Click" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbRejectFS" onclick="imbRejectFS_Click" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbBackFS" onclick="imbBackFS_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton> </asp:View> <asp:View id="View20" runat="server"><BR /><asp:Label id="Label55" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="List Surat Jalan :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvSJ" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." DataKeyNames="sjloid,approvaloid,sjlperson" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="sjloid" HeaderText="sjloid" SortExpression="sjloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="sjlperson" HeaderText="sjlperson" SortExpression="sjlperson" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="No. Surat Jalan"><EditItemTemplate>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:LinkButton ID="lkbSelectSJ" runat="server" Text='<%# Eval("sjlno") %>' OnClick="lkbSelectSJ_Click"></asp:LinkButton>
                                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="sjlsend" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal Pengiriman">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="PIC" SortExpression="personname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sjlnopol" HeaderText="No. Expedisi" SortExpression="sjlnopol">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sjldriver" HeaderText="Driver">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date" SortExpression="requestdate">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View21" runat="server"><BR /><asp:Label id="Label61" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Data Surat Jalan :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE><TBODY><TR><TD style="FONT-WEIGHT: bold" align=left>No Surat Jalan</TD><TD align=left>: <asp:Label id="lblsjno" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>&nbsp;Tanggal Pengiriman</TD><TD align=left>: <asp:Label id="lblsjsend" runat="server"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>PIC</TD><TD align=left>: <asp:Label id="lblsjpic" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>&nbsp;Driver</TD><TD align=left>: <asp:Label id="lblsjdriver" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>No. Expedisi</TD><TD align=left>: <asp:Label id="lblsjexpedisi" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>&nbsp;Supplier</TD><TD align=left>: <asp:Label id="lblsjsupplier" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="lblpersonnip" runat="server" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="lblapprovaloid" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left></TD><TD align=left><asp:Label id="lblsjloid" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><BR /><asp:Label id="Label62" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Navy" Text="Detail Request :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvSJDetail" runat="server" Width="100%" Height="1px" ForeColor="#333333" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="barcode" HeaderText="Barcode" SortExpression="barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqitemname" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="type" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="brand" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemqty" HeaderText="Jumlah">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Kerusakan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibapprovesj" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibrejectsj" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibbacksj" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:View> <asp:View id="View22" runat="server"><BR /><asp:Label id="Label56" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="List SPK :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvSPK" runat="server" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" DataKeyNames="spkmainoid,approvaloid,spkpic" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="No. SPK"><EditItemTemplate>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:LinkButton ID="lkbSelectSPK" runat="server" Text='<%# Eval("spkno") %>' OnClick="lkbSelectSPK_Click"></asp:LinkButton>
                                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="spkopendate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Open Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="spkplanstart" HeaderText="Plan Start">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkplanend" HeaderText="Plan End">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkaddnote" HeaderText="Note SPK">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date" SortExpression="requestdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View23" runat="server"><BR /><asp:Label id="Label57" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Data SPK :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE><TBODY><TR><TD style="FONT-WEIGHT: bold; HEIGHT: 18px" align=left>No. SPK</TD><TD style="HEIGHT: 18px" align=left>: <asp:Label id="lblspkno" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold; HEIGHT: 18px" align=left>&nbsp;Rencana Mulai</TD><TD style="HEIGHT: 18px" align=left>: <asp:Label id="lblspkplanstart" runat="server"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Open Date</TD><TD align=left>: <asp:Label id="lblspkdate" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>&nbsp;Rencana Selesai</TD><TD align=left>: <asp:Label id="lblspkplanend" runat="server"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; HEIGHT: 18px" align=left><asp:Label id="lblspkperson" runat="server" Visible="False"></asp:Label></TD><TD style="HEIGHT: 18px" align=left><asp:Label id="lblspkapprovaloid" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-WEIGHT: bold; HEIGHT: 18px" align=left></TD><TD style="HEIGHT: 18px" align=left><asp:Label id="lblspkmainoid" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><BR /><asp:Label id="Label69" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Navy" Text="OutPut :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvSPKDetail" runat="server" Height="1px" ForeColor="#333333" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="barcode" HeaderText="FG Code" SortExpression="barcode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqitemname" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="type" HeaderText="Jenis Barang" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyproduksi" HeaderText="Qty">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="brand" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR /><asp:Label id="Label58" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Navy" Visible="False" Text="Detail Job :" Font-Underline="True"></asp:Label><BR /><asp:GridView id="gvprocessitemresult" runat="server" Width="98%" Font-Size="X-Small" ForeColor="#333333" Visible="False" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="spkdtloid,spkprocessname,spkprocessoid,outputname,outputqty1,outputunit1,outputqty2,outputunit2,typejob" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="spkdtloid" HeaderText="BarangNo" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessname" HeaderText="From Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="todept" HeaderText="To Process">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="operator1" HeaderText="OP 1">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="operator2" HeaderText="OP 2">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="quality" HeaderText="QC">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="typejob" HeaderText="Type" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="outputname" HeaderText="Nama Job" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputtype" HeaderText="Typeoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputqty1" HeaderText="Tarif" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputunit1" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputqty2" HeaderText="Qty2" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="6%"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="6%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputunit2" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="3%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Overline="False" Font-Size="X-Small" Font-Strikeout="False" Width="3%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessoid" Visible="False">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="No Route on List"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR /><asp:Label id="Label59" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Navy" Text="Input:" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GVprocessmaterial" runat="server" Width="98%" Font-Size="X-Small" ForeColor="#333333" EmptyDataText="Data Not Found" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="spkdtloid,spkprocessmatoid,matlongdesc,qty1,unit1,qty2,unit2,wastestd" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="spkdtloid" HeaderText="BarangNo" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessname" HeaderText="NamaProcess">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Sparepart">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qty1" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit1" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty2" HeaderText="Qty2" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="6%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit2" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="2%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wastestd" HeaderText="Harga" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessmatoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="No Sparepart on List"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR /><asp:Label id="lbl_revisespk" runat="server" Visible="False"></asp:Label>&nbsp;<BR /><asp:TextBox id="txtrevise" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine"></asp:TextBox><BR /><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibapprovespk" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibrejectspk" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="imb_revise" onclick="imb_revise_Click" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" AlternateText="Revised"></asp:ImageButton>&nbsp;<asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibbackspk" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Button id="imbsubmitrevisi" onclick="imbsubmitrevisi_Click" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision"></asp:Button>&nbsp;<asp:Button id="imbcancelrevisi" onclick="imbcancelrevisi_Click" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision"></asp:Button><BR /></asp:View> <asp:View id="View24" runat="server" __designer:wfdid="w1"><BR /><asp:Label id="Label32" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Purchase Return List :" __designer:wfdid="w2" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GVPreturn" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w3" OnSelectedIndexChanged="gvPR_SelectedIndexChanged" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnbeliReturmstoid" GridLines="None" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnbeliReturmstoid" HeaderText="trnbeliReturmstoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="PR No."><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" CssClass="inpText" Text='<%# Bind("trnbelireturno") %>' __designer:wfdid="w2"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectPReturn" onclick="lkbSelectPReturn_Click" runat="server" Text='<%# Eval("trnbelireturno") %>' __designer:wfdid="w1" CommandArgument='<%# Eval("approvaloid") %>' ToolTip='<%# Eval("trnbeliReturmstoid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnbelidate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="PR Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnTrfToReturNo" HeaderText="TR No." Visible="False">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelinote" HeaderText="PR Note" SortExpression="prnote">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR />&nbsp;</asp:View> <asp:View id="View25" runat="server" __designer:wfdid="w4"><asp:Label id="Label35" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Purchase Return Data :" __designer:wfdid="w5" Font-Underline="True"></asp:Label><BR /><BR /><TABLE><TBODY><TR><TD style="FONT-WEIGHT: bold" align=left>PR No.</TD><TD align=left>: <asp:Label id="preturnno" runat="server" __designer:wfdid="w6"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>PR Date</TD><TD align=left>: <asp:Label id="preturndate" runat="server" __designer:wfdid="w7"></asp:Label></TD></TR><TR id="trtr" runat="server" visible="false"><TD style="FONT-WEIGHT: bold" align=left>TR No.</TD><TD align=left>: <asp:Label id="preturntrno" runat="server" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Invoice No.</TD><TD align=left>: <asp:Label id="preturninvoice" runat="server" __designer:wfdid="w9"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Supplier</TD><TD align=left>: <asp:Label id="NamaSupp" runat="server" __designer:wfdid="w10"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Total Amount</TD><TD align=left>: <asp:Label id="preturnnetto" runat="server" __designer:wfdid="w11"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WHITE-SPACE: nowrap" align=left>PR Note</TD><TD align=left>: <asp:Label id="preturnnote" runat="server" __designer:wfdid="w12"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WHITE-SPACE: nowrap" align=left><asp:Label id="typeret" runat="server" Visible="False" __designer:wfdid="w13"></asp:Label></TD><TD align=left><asp:Label id="CabangNya" runat="server" Visible="False" __designer:wfdid="w14"></asp:Label> <asp:Label id="cBngPusat" runat="server" Visible="False" __designer:wfdid="w15"></asp:Label></TD></TR></TBODY></TABLE><BR /><asp:DropDownList id="paytype" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w16"></asp:DropDownList><BR />&nbsp;<DIV style="OVERFLOW-Y: scroll; WIDTH: 700px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView id="GVPreturnDtl" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w57" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="hargabeli" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Harga Beli"><ItemTemplate>
<asp:TextBox id="tbHarga" runat="server" Width="100px" CssClass="inpText" Text='<%# String.Format("{0:#,##0.0000}", GetHargaBeli()) %>' MaxLength="20" ToolTip='<%# Eval("hargabeli") %>'></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeHargaLM" runat="server" TargetControlID="tbHarga" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelireturdtlqtydisc" DataFormatString="{0:#,##0.00}" HeaderText="Diskon/Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="DiscAmt" DataFormatString="{0:#,##0.00}" HeaderText="Total Diskon">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbelinetto" DataFormatString="{0:#,##0.00}" HeaderText="Amtt. Netto">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdisc" HeaderText="Total Disc" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelireturdtlqtydisc" HeaderText="Disc. Dtl" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><BR /><asp:Label id="cashamount" runat="server" Visible="False" __designer:wfdid="w18">0</asp:Label><asp:Label id="returamount" runat="server" Visible="False" __designer:wfdid="w19"></asp:Label> <asp:Label id="ppnamount" runat="server" Visible="False" __designer:wfdid="w20"></asp:Label> <asp:Label id="hutangamount" runat="server" Visible="False" __designer:wfdid="w21"></asp:Label> <asp:Label id="amtpotonganpembelian" runat="server" Visible="False" __designer:wfdid="w22"></asp:Label> <asp:Label id="costamount" runat="server" Visible="False" __designer:wfdid="w23">0</asp:Label> <asp:Label id="returhutangamount" runat="server" Visible="False" __designer:wfdid="w24"></asp:Label> <asp:Label id="suppoid" runat="server" Visible="False" __designer:wfdid="w25"></asp:Label> <asp:Label id="amtvoucher" runat="server" Visible="False" __designer:wfdid="w26"></asp:Label><asp:Label id="amtexpedisi" runat="server" Visible="False" __designer:wfdid="w27"></asp:Label><BR /><asp:ImageButton style="HEIGHT: 23px" id="ibapprovepreturn" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" Height="24px" __designer:wfdid="w28" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="ibrejectpreturn" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" __designer:wfdid="w29" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="ibbackpreturn" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" __designer:wfdid="w30" AlternateText="Back"></asp:ImageButton> </asp:View> <asp:View id="View26" runat="server"><BR /><asp:Label id="Label38" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Transfer Warehouse" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GVTw" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" DataKeyNames="trfmtrmstoid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Transfer No."><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("transferno") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectTW" onclick="lkbSelectTW_Click" runat="server" Text='<%# Eval("transferno") %>' ToolTip='<%# Eval("trfmtrmstoid") %>' CommandArgument='<%# Eval("approvaloid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trfmtrdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Transfer User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View27" runat="server"><asp:Label id="Label40" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Transfer Warehouse Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width=800><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Transfer&nbsp;No.</TD><TD style="WIDTH: 212px" align=left>: <asp:Label id="twno" runat="server"></asp:Label></TD><TD style="WIDTH: 3px" align=left>&nbsp;</TD><TD style="WIDTH: 98px" align=left>&nbsp;</TD><TD style="WIDTH: 2px" align=left>&nbsp;</TD><TD style="WIDTH: 251px" align=left>&nbsp;</TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Transfer&nbsp;Date</TD><TD style="WIDTH: 212px" align=left>: <asp:Label id="twdate" runat="server"></asp:Label></TD><TD style="WIDTH: 3px" align=left>&nbsp;</TD><TD style="WIDTH: 98px" align=left>&nbsp;</TD><TD style="WIDTH: 2px" align=left>&nbsp;</TD><TD style="WIDTH: 251px" align=left>&nbsp;</TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Transfer User</TD><TD style="WIDTH: 212px" align=left>: <asp:Label id="twuser" runat="server"></asp:Label></TD><TD style="WIDTH: 3px" align=left>&nbsp;</TD><TD style="WIDTH: 98px" align=left>&nbsp;</TD><TD style="WIDTH: 2px" align=left>&nbsp;</TD><TD style="WIDTH: 251px" align=left>&nbsp;</TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px; HEIGHT: 10px" align=left>From Branch</TD><TD style="WIDTH: 212px; HEIGHT: 10px" align=left>: <asp:Label id="FromBranch" runat="server"></asp:Label> </TD><TD style="WIDTH: 3px; HEIGHT: 10px" align=left></TD><TD style="FONT-WEIGHT: bold; WIDTH: 98px; HEIGHT: 10px" align=left>To Branch</TD><TD style="WIDTH: 2px; HEIGHT: 10px" align=left>:</TD><TD style="WIDTH: 251px; HEIGHT: 10px" align=left><asp:Label id="ToBranch" runat="server"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>From Location</TD><TD style="WIDTH: 212px" align=left>: <asp:Label id="twfrom" runat="server"></asp:Label> </TD><TD style="WIDTH: 3px" align=left>&nbsp;</TD><TD style="FONT-WEIGHT: bold; WIDTH: 98px" align=left>To Location</TD><TD style="WIDTH: 2px" align=left>:</TD><TD style="WIDTH: 251px" align=left><asp:Label id="twto" runat="server"></asp:Label> </TD></TR></TBODY></TABLE><BR /><asp:GridView id="GVTwDtl" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <BR /><asp:Label id="lbl_reviseTW" runat="server" Visible="False"></asp:Label> <BR /><asp:TextBox id="txt_reviseTW" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine" MaxLength="250"></asp:TextBox> <BR /><BR /><asp:ImageButton id="btnTwApprove" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="btnTwReject" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="imbReviseTW" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" AlternateText="Revised"></asp:ImageButton> <asp:ImageButton id="btnTwBack" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton> &nbsp;<asp:Button id="btnReviseTW" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision"></asp:Button> &nbsp;<asp:Button id="btnCancelReviseTW" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision"></asp:Button> </asp:View><BR /><asp:View id="View7" runat="server" __designer:wfdid="w2"><BR /><asp:Label id="Label9" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Service Supplier Internal :" __designer:wfdid="w3" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GVTws" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w4" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="Trfwhserviceoid" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Transfer No."><ItemTemplate>
<asp:LinkButton id="lkbSelectTWS" onclick="lkbSelectTWS_Click" runat="server" Text='<%# Eval("TrfwhserviceNo") %>' __designer:wfdid="w1" CommandArgument='<%# Eval("approvaloid") %>' ToolTip='<%# Eval("Trfwhserviceoid") %>' CommandName='<%# Eval("FromBranch") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppName" HeaderText="Tujuan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Trfwhservicedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Transfer User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:View> <asp:View id="View8" runat="server" __designer:wfdid="w5"><asp:Label id="Label14" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Service Supplier Internal Data :" __designer:wfdid="w6" Font-Underline="True"></asp:Label><BR /><TABLE width="100%"><TBODY><TR><TD align=left>Transfer&nbsp;No.</TD><TD align=left colSpan=2>: <asp:Label id="TwsSupNo" runat="server" __designer:wfdid="w7"></asp:Label>&nbsp;</TD><TD id="TD24" align=left>Cabang Kirim</TD><TD id="TD18" align=left runat="server">:&nbsp;</TD><TD id="TD25" align=left runat="server"><asp:Label id="DariCabang" runat="server" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD align=left>Transfer&nbsp;Date</TD><TD align=left colSpan=2>: <asp:Label id="TwsSupDate" runat="server" __designer:wfdid="w9"></asp:Label>&nbsp;</TD><TD align=left>Gudang Kirim</TD><TD align=left>:&nbsp;</TD><TD align=left><asp:Label id="GudangLoc" runat="server" __designer:wfdid="w10"></asp:Label></TD></TR><TR><TD align=left>Transfer User</TD><TD align=left colSpan=2>: <asp:Label id="TwsSupUsr" runat="server" __designer:wfdid="w11"></asp:Label>&nbsp;</TD><TD align=left>Cabang Tujuan</TD><TD align=left>:</TD><TD align=left><asp:Label id="TujuanNya" runat="server" __designer:wfdid="w12"></asp:Label> </TD></TR><TR><TD align=left colSpan=6><asp:Label id="kodeCab" runat="server" Visible="False" __designer:wfdid="w13"></asp:Label><asp:Label id="OidTws" runat="server" Visible="False" __designer:wfdid="w14"></asp:Label><asp:Label id="OidTujuan" runat="server" Visible="False" __designer:wfdid="w15"></asp:Label><asp:Label id="KodeTujuan" runat="server" Visible="False" __designer:wfdid="w16"></asp:Label></TD></TR><TR><TD align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 700px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView id="GVTwsDtl" runat="server" Width="100%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w17" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="30px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR></TBODY></TABLE><asp:Label id="Label44" runat="server" Visible="False" __designer:wfdid="w18"></asp:Label><BR /><asp:TextBox id="TxtRevNoteTWsupp" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine" __designer:wfdid="w19" MaxLength="250"></asp:TextBox> <BR /><asp:ImageButton id="BtnAppSupTws" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" __designer:wfdid="w20" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="BtnTwsReject" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" __designer:wfdid="w21" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="ImgRevTws" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w22" AlternateText="Revised"></asp:ImageButton> <asp:ImageButton id="btnBackTws" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" __designer:wfdid="w23" AlternateText="Back"></asp:ImageButton> &nbsp;<asp:Button id="BtnSubMitRev" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision" __designer:wfdid="w24"></asp:Button> &nbsp;<asp:Button id="BtnBatalRev" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision" __designer:wfdid="w25"></asp:Button> </asp:View> <asp:View id="View3" runat="server"><BR /><asp:Label id="Label4" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Transfer Warehouse Service" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="gvTWService" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trfwhserviceoid" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Transfer No."><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("transferno") %>' __designer:wfdid="w2"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectTWService" onclick="lkbSelectTWService_Click" runat="server" Text='<%# Eval("trfwhserviceno") %>' __designer:wfdid="w1" ToolTip='<%# Eval("trfwhserviceoid") %>' CommandArgument='<%# Eval("approvaloid") %>' CommandName='<%# Eval("frombranch") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trfwhservicedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Transfer User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View><asp:View id="View4" runat="server"><asp:Label id="Label6" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Transfer Warehouse Service Data :" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width=800><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Transfer&nbsp;No.</TD><TD style="WIDTH: 212px" align=left>: <asp:Label id="twsno" runat="server" __designer:wfdid="w3"></asp:Label></TD><TD style="WIDTH: 3px" align=left>&nbsp;</TD><TD style="WIDTH: 98px" align=left>&nbsp;</TD><TD style="WIDTH: 2px" align=left>&nbsp;</TD><TD align=left>&nbsp;</TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Transfer&nbsp;Date</TD><TD style="WIDTH: 212px" align=left>: <asp:Label id="twsdate" runat="server" __designer:wfdid="w4"></asp:Label></TD><TD style="WIDTH: 3px" align=left>&nbsp;</TD><TD style="WIDTH: 98px" align=left>&nbsp;</TD><TD style="WIDTH: 2px" align=left>&nbsp;</TD><TD align=left>&nbsp;</TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Transfer User</TD><TD style="WIDTH: 212px" align=left>: <asp:Label id="twsuser" runat="server" __designer:wfdid="w5"></asp:Label></TD><TD style="WIDTH: 3px" align=left>&nbsp;</TD><TD style="WIDTH: 98px" align=left>&nbsp;</TD><TD style="WIDTH: 2px" align=left>&nbsp;</TD><TD align=left>&nbsp;</TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px; HEIGHT: 10px" align=left>From Branch</TD><TD style="WIDTH: 212px; HEIGHT: 10px" align=left>: <asp:Label id="twsfrombranch" runat="server" __designer:wfdid="w6"></asp:Label> </TD><TD style="WIDTH: 3px; HEIGHT: 10px" align=left></TD><TD style="FONT-WEIGHT: bold; WIDTH: 98px; HEIGHT: 10px" align=left>To Branch</TD><TD style="WIDTH: 2px; HEIGHT: 10px" align=left>:</TD><TD style="HEIGHT: 10px" align=left><asp:Label id="twstobranch" runat="server" __designer:wfdid="w7"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>From Location</TD><TD style="WIDTH: 212px" align=left>: <asp:Label id="twsfromloc" runat="server" __designer:wfdid="w8"></asp:Label> </TD><TD style="WIDTH: 3px" align=left>&nbsp;</TD><TD style="FONT-WEIGHT: bold; WIDTH: 98px" align=left>To Location</TD><TD style="WIDTH: 2px" align=left>:</TD><TD align=left><asp:Label id="twstoloc" runat="server" __designer:wfdid="w9"></asp:Label> </TD></TR></TBODY></TABLE><BR /><asp:GridView id="gvTWServicedtl" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w10" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="reqoid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR /><asp:Label id="lbl_reviseTWService" runat="server" Visible="False"></asp:Label><BR /><asp:TextBox id="txt_reviseTWService" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine" MaxLength="250"></asp:TextBox> <BR /><BR /><asp:ImageButton id="btnTwServiceApp" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="btnTwServiceRej" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="imbTWServiceRev" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Revised"></asp:ImageButton> <asp:ImageButton id="btnTWServiceBack" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton> &nbsp;<asp:Button id="btnReviseTWService" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision"></asp:Button> &nbsp;<asp:Button id="btnCancelTWService" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision"></asp:Button> </asp:View>&nbsp;&nbsp;&nbsp;&nbsp; <BR /><asp:View id="View32" runat="server"><asp:Label id="Label52" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="List Data Realisasi Voucher :" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="RealGV" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="realisasioid" HeaderText="realisasioid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="No. Realisasi"><EditItemTemplate>
                                                            
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkSelectReal" onclick="lkSelectReal_Click" runat="server" Text='<%# Eval("realisasino") %>' ToolTip='<%# Eval("realisasioid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="realisasidate" HeaderText="Real Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No. PI">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasitype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Realisasinote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View> <asp:View id="View33" runat="server"><asp:Label id="Label53" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Realisasi Voucher Data" Font-Underline="True"></asp:Label><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>No. Realisasi</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:Label id="RealNo" runat="server" Text="RealNo"></asp:Label>&nbsp;<asp:Label id="CodeCb" runat="server" Visible="False" Text="CodeCbg"></asp:Label>&nbsp;<asp:Label id="RealisasiOid" runat="server" Visible="False" Text="RealisasiOid"></asp:Label>&nbsp;<asp:Label id="ApprovalOid" runat="server" Visible="False" Text="ApprovalOid"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>Tanggal</TD><TD align=left>:</TD><TD id="TD19" align=left colSpan=4 runat="server"><asp:Label id="RealDate" runat="server" Text="RealDate"></asp:Label>&nbsp; <asp:Label id="acctgoidreal" runat="server" Visible="False" Text="acctgidreal"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>Type</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="payflag" runat="server" Width="200px" CssClass="inpText" Font-Bold="False" OnSelectedIndexChanged="payflag_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Value="HUTANG">PELUNASAN HUTANG</asp:ListItem>
<asp:ListItem Text="CASH" Value="CASH"></asp:ListItem>
<asp:ListItem>BANK</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left><asp:Label id="LblAkun" runat="server" Visible="False" Text="Akun COA"></asp:Label></TD><TD align=left><asp:Label id="Titik2" runat="server" Visible="False" Text=":"></asp:Label></TD><TD align=left colSpan=4><asp:DropDownList id="DdlAkun" runat="server" Width="250px" CssClass="inpText" Font-Bold="False" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>No. PO</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:Label id="NoPi" runat="server" Text="NoPi"></asp:Label> <asp:Label id="OidPO" runat="server" Visible="False" Text="OidPO"></asp:Label> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>Supplier</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:Label id="SuppReal" runat="server" Text="SuppReal"></asp:Label>&nbsp; </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>Amount PO</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:Label id="AmtReal" runat="server" Text="AmtReal"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>Amount Realisasi</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:Label id="LblTampung" runat="server" Text="LblTampung"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" id="TD45" align=left runat="server" Visible="false">Sisa Amount</TD><TD id="TD44" align=left runat="server" Visible="false">:</TD><TD id="TD46" align=left colSpan=4 runat="server" Visible="false"><asp:Label id="amtsisa" runat="server" Text="amtsisa"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px; HEIGHT: 15px" align=left>Note</TD><TD style="HEIGHT: 15px" align=left>:</TD><TD style="HEIGHT: 15px" align=left colSpan=4><asp:TextBox id="RealNote" runat="server" Width="300px" CssClass="inpText" MaxLength="100"></asp:TextBox> </TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left><asp:Label id="I_Udtl" runat="server" Font-Bold="True" ForeColor="Red" Text="Data Detail"></asp:Label></TD><TD align=left></TD><TD align=left colSpan=4><asp:Label id="labelseq" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left><asp:Label id="Label75" runat="server" Width="5px" Text="Voucher"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="txtVoucher" runat="server" Width="150px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="VoucherOid" runat="server" Visible="False" Text="0"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>Amount Detail</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="AmtVdtl" runat="server" Width="150px" CssClass="inpText " AutoPostBack="True" OnTextChanged="AmtVdtl_TextChanged"></asp:TextBox>&nbsp;<asp:Label id="vDtlAmt" runat="server" Visible="False" Text="0.00"></asp:Label>&nbsp;<asp:Label id="AmtSisaDtl" runat="server" Visible="False" Text="0.00"></asp:Label>&nbsp;<asp:TextBox id="amtdtltemp" runat="server" CssClass="inpText " Visible="False" AutoPostBack="True" Enabled="False" OnTextChanged="AmtVdtl_TextChanged" Wrap="False">0.00</asp:TextBox>&nbsp;<asp:TextBox id="amtdtltempsisa" runat="server" CssClass="inpText " Visible="False" AutoPostBack="True" Enabled="False" OnTextChanged="AmtVdtl_TextChanged" Wrap="False">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 120px" align=left>Note</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="cashbanknote" runat="server" Width="300px" CssClass="inpText" MaxLength="100"></asp:TextBox></TD></TR><TR><TD align=center colSpan=6><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click" runat="server" ImageUrl="~/Images/addtolist.png" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="FONT-WEIGHT: bold; HEIGHT: 145px" align=left colSpan=6><asp:GridView id="GVvOucherDtl" runat="server" Width="100%" ForeColor="#333333" OnSelectedIndexChanged="GVvOucherDtl_SelectedIndexChanged" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="seqdtl,voucheroid,voucherdesc,amtrealdtl,amtvoucherdtl,amtsisadtl,notedtl,amtdtltemp,amtdtltempsisa" GridLines="None" OnRowDataBound="GVvOucherDtl_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectText="Edit " ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="seqdtl" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucheroid" HeaderText="VoucherOid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="voucherdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtrealdtl" HeaderText="Realisasi Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="120px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtvoucherdtl" HeaderText="Amt Voucher">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtsisadtl" HeaderText="Amt Sisa">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtltemp" HeaderText="amtdtltemp" Visible="False"></asp:BoundField>
<asp:BoundField DataField="amtdtltempsisa" HeaderText="amtdtltempsisa" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" CssClass="Important" Font-Size="Small" Text="No Data Detail"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="ApRealBtn" onclick="ApRealBtn_Click" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton>&nbsp;<asp:ImageButton id="RejectBtn" onclick="RejectBtn_Click" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton>&nbsp;<asp:ImageButton id="ReviseBtn" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Revised"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImageButton9" onclick="ImageButton9_Click" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:View><BR /><asp:View id="View34" runat="server"><BR /><asp:Label id="Label74" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Sales Return List" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="sRetGV" runat="server" Width="100%" Height="1px" ForeColor="#333333" __designer:wfdid="w1" GridLines="None" DataKeyNames="trnjualreturmstoid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnjualreturmstoid" HeaderText="trnjualreturmstoid" SortExpression="trnjualreturmstoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="approvaloid" HeaderText="approvaloid" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="No. Return"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("trnsjjualno") %>' __designer:wfdid="w2"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectSR" onclick="lkbSelectSR_Click" runat="server" Text='<%# Eval("trnjualreturno") %>' __designer:wfdid="w1" CommandArgument='<%# Eval("approvaloid") %>' ToolTip='<%# Eval("trnjualreturmstoid") %>' CommandName='<%# Eval("branch_code") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User" SortExpression="requestuser">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Request Date" SortExpression="requestdate"><EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# format(cdate(eval("requestdate")),"MM/dd/yyyy") %>'></asp:Label>
                                                            
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:View><BR /><asp:View id="View35" runat="server"><BR /><asp:Label id="Label76" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Sales Return Data" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width="100%" __designer:dtid="1407374883555877"><TBODY __designer:dtid="1407374883555878"><TR __designer:dtid="1407374883555879"><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555880">No. Return</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555881">:</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555882"><asp:Label id="sRetNo" runat="server" __designer:dtid="1407374883555883" __designer:wfdid="w3"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555884">Tanggal Retur</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555885">:</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555886"><asp:Label id="sRetDate" runat="server" __designer:dtid="1407374883555887" __designer:wfdid="w4"></asp:Label></TD></TR><TR __designer:dtid="1407374883555888"><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555889">No. Invoice</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555890">:</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555891"><asp:Label id="InvNoSI" runat="server" __designer:dtid="1407374883555892" __designer:wfdid="w5"></asp:Label> </TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555893">Customer</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555894">:</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555895"><asp:Label id="sCustName" runat="server" __designer:dtid="1407374883555896" __designer:wfdid="w6"></asp:Label><asp:Label id="sCustoid" runat="server" __designer:dtid="1407374883555897" Visible="False" __designer:wfdid="w7"></asp:Label></TD></TR><TR __designer:dtid="1407374883555898"><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555899">Total Amount</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555900">:</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555901"><asp:Label id="sTotAmt" runat="server" __designer:dtid="1407374883555902" __designer:wfdid="w8"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555903">Type</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555904">:</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555905"><asp:DropDownList id="DDLtype" runat="server" CssClass="inpTextDisabled" __designer:dtid="1407374883555906" __designer:wfdid="w9" Enabled="False"><asp:ListItem __designer:dtid="1407374883555907" Value="1">Potong Invoice</asp:ListItem>
<asp:ListItem __designer:dtid="1407374883555908" Value="2">Tukar Barang Sama</asp:ListItem>
<asp:ListItem __designer:dtid="1407374883555909" Value="3">Tukar Beda Barang</asp:ListItem>
<asp:ListItem __designer:dtid="1407374883555910" Value="4">Jadikan DP</asp:ListItem>
</asp:DropDownList> </TD></TR><TR __designer:dtid="1407374883555911"><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555912">Tax Amount</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555913">:</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555914"><asp:Label id="sTaxAmt" runat="server" __designer:dtid="1407374883555915" __designer:wfdid="w10"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555916">Total Netto</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555917">:</TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555918"><asp:Label id="sTotNetto" runat="server" __designer:dtid="1407374883555919" __designer:wfdid="w11"></asp:Label></TD></TR><TR __designer:dtid="1407374883555920"><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555921"><asp:Label id="returpiutangamount" runat="server" __designer:dtid="1407374883555922" Visible="False" __designer:wfdid="w12">0.0</asp:Label>&nbsp;<asp:Label id="TypeSoRet" runat="server" __designer:dtid="1407374883555923" Visible="False" __designer:wfdid="w13"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555924"></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555925"><asp:Label id="returamounts" runat="server" __designer:dtid="1407374883555926" Visible="False" __designer:wfdid="w14"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555927"><asp:Label id="amtdiscdtl" runat="server" __designer:dtid="1407374883555928" Visible="False" __designer:wfdid="w15"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555929"></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555930"><asp:Label id="sCodeCbg" runat="server" __designer:dtid="1407374883555931" Visible="False" __designer:wfdid="w16"></asp:Label></TD></TR><TR __designer:dtid="1407374883555932"><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555933"><asp:Label id="sTypeRet" runat="server" __designer:dtid="1407374883555934" Visible="False" __designer:wfdid="w17"></asp:Label> <asp:Label id="AmtPiutang" runat="server" __designer:dtid="1407374883555935" Visible="False" __designer:wfdid="w18"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555936"></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555937"><asp:Label id="trnjualreturmstoid" runat="server" __designer:dtid="1407374883555938" Visible="False" __designer:wfdid="w19"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555939"><asp:Label id="sAppOid" runat="server" __designer:dtid="1407374883555940" Visible="False" __designer:wfdid="w20"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555941"></TD><TD style="FONT-WEIGHT: bold" align=left __designer:dtid="1407374883555942"><asp:Label id="status" runat="server" __designer:dtid="1407374883555943" Visible="False" __designer:wfdid="w21"></asp:Label> <asp:Label id="TrnJualmstoid" runat="server" __designer:dtid="1407374883555944" Visible="False" __designer:wfdid="w22"></asp:Label></TD></TR><TR __designer:dtid="1407374883555945"><TD style="FONT-WEIGHT: bold" align=left colSpan=6 __designer:dtid="1407374883555946"><asp:GridView id="GVDtl" runat="server" Width="100%" ForeColor="#333333" __designer:dtid="1407374883555947" __designer:wfdid="w23" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemoid,netamt,unitoid,maxqty,itemcode,unitseq,trnjualdtloid,locationoid,promodisc,promodiscamt" GridLines="None">
<RowStyle __designer:dtid="1407374883555948" BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns __designer:dtid="1407374883555949">
<asp:BoundField __designer:dtid="1407374883555950" DataField="itemname" HeaderText="Item">
<HeaderStyle __designer:dtid="1407374883555951" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle __designer:dtid="1407374883555952" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField __designer:dtid="1407374883555953" DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle __designer:dtid="1407374883555954" HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle __designer:dtid="1407374883555955" HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField __designer:dtid="1407374883555956" DataField="unit" HeaderText="Unit">
<HeaderStyle __designer:dtid="1407374883555957" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle __designer:dtid="1407374883555958" Wrap="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField __designer:dtid="1407374883555959" DataField="price" DataFormatString="{0:#,##0.00}" HeaderText="Price">
<HeaderStyle __designer:dtid="1407374883555960" HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle __designer:dtid="1407374883555961" HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField __designer:dtid="1407374883555962" DataField="promodiscamt" DataFormatString="{0:#,##0.00}" HeaderText="Promo Disc.">
<HeaderStyle __designer:dtid="1407374883555963" HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle __designer:dtid="1407374883555964" HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField __designer:dtid="1407374883555965" DataField="netamt" DataFormatString="{0:#,##0.00}" HeaderText="Amount">
<HeaderStyle __designer:dtid="1407374883555966" HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle __designer:dtid="1407374883555967" HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField __designer:dtid="1407374883555968" DataField="note" HeaderText="Note">
<HeaderStyle __designer:dtid="1407374883555969" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle __designer:dtid="1407374883555970" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField __designer:dtid="1407374883555971" DataField="location" HeaderText="Location">
<HeaderStyle __designer:dtid="1407374883555972" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle __designer:dtid="1407374883555973" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle __designer:dtid="1407374883555974" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle __designer:dtid="1407374883555975" HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate __designer:dtid="1407374883555976">
                                                                                    <asp:Label __designer:dtid="1407374883555977" ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
                                                                                        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle __designer:dtid="1407374883555978" BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle __designer:dtid="1407374883555979" BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle __designer:dtid="1407374883555980" BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR __designer:dtid="1407374883555981"><TD style="FONT-WEIGHT: bold" align=left colSpan=6 __designer:dtid="1407374883555982"><asp:Label id="LblSretNo" runat="server" __designer:dtid="1407374883555983" Visible="False" __designer:wfdid="w24"></asp:Label> <asp:TextBox id="TxtRevSret" runat="server" Width="342px" CssClass="inpText" __designer:dtid="1407374883555984" Visible="false" TextMode="MultiLine" __designer:wfdid="w25" MaxLength="250"></asp:TextBox></TD></TR><TR __designer:dtid="1407374883555985"><TD style="FONT-WEIGHT: bold" align=left colSpan=6 __designer:dtid="1407374883555986"><asp:ImageButton id="BtnAppSret" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" __designer:dtid="1407374883555987" __designer:wfdid="w26" AlternateText="Approve"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnRejectSret" onclick="BtnRejectSret_Click" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" __designer:dtid="1407374883555988" __designer:wfdid="w27" AlternateText="Reject"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnReviseSret" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" __designer:dtid="1407374883555989" Visible="False" __designer:wfdid="w28" AlternateText="Revised"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnBackSret" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" __designer:dtid="1407374883555990" __designer:wfdid="w29" AlternateText="Back"></asp:ImageButton> <asp:Button id="SubRevisiBtn" onclick="SubRevisiBtn_Click" runat="server" CssClass="btn red" __designer:dtid="1407374883555991" Visible="false" Text="Submit Revision" __designer:wfdid="w30"></asp:Button> <asp:Button id="CancelRevBtn" runat="server" CssClass="btn gray" __designer:dtid="1407374883555992" Visible="false" Text="Cancel Revision" __designer:wfdid="w31"></asp:Button></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View16" runat="server" __designer:wfdid="w1"><BR /><asp:Label id="Label26" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Promo Supplier" __designer:wfdid="w2" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GVPromSupp" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w3" GridLines="None" DataKeyNames="promooid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Kode Promo"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("transferno") %>' __designer:wfdid="w28"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectPromSupp" onclick="lkbSelectPromSupp_Click" runat="server" Text='<%# Eval("promocode") %>' __designer:wfdid="w27" CommandArgument='<%# Eval("approvaloid") %>' ToolTip='<%# Eval("promooid") %>' CommandName='<%# Eval("branch_code") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="promoname" HeaderText="Nama Promo">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="crttime" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="User Input">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:View><BR /><asp:View id="View17" runat="server" __designer:wfdid="w4"><asp:Label id="Label28" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Promo Supplier Data :" __designer:wfdid="w5" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width=800><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Kode Promo</TD><TD align=left>: <asp:Label id="KodePromo" runat="server" __designer:wfdid="w6"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Type Promo</TD><TD align=left>: <asp:Label id="TypePromo" runat="server" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Start Date</TD><TD align=left>: <asp:Label id="sTartTgl" runat="server" __designer:wfdid="w8"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Nama Promo&nbsp;</TD><TD align=left>: <asp:Label id="PromName" runat="server" __designer:wfdid="w9"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>End Date</TD><TD align=left>:&nbsp;<asp:Label id="FinishTgl" runat="server" __designer:wfdid="w10"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left><asp:Label id="OidProm" runat="server" Visible="False" __designer:wfdid="w11"></asp:Label></TD><TD align=left><asp:Label id="TypeNya" runat="server" Visible="False" __designer:wfdid="w12"></asp:Label></TD></TR></TBODY></TABLE><BR /><asp:Label id="Label46" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Supplier : " __designer:wfdid="w13" Font-Underline="True"></asp:Label><BR /><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="GVdtlSupp" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w14" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="suppoid" HeaderText="suppoid" Visible="False"></asp:BoundField>
<asp:BoundField HeaderText="seq" Visible="False"></asp:BoundField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supp">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No supplier data found!!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR /><asp:Label id="Label34" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang : " __designer:wfdid="w15" Font-Underline="True"></asp:Label><BR /><asp:GridView id="gvPromoDtl" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w16" EmptyDataText="No Data" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item. Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Point" HeaderText="Nilai Point">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="targetqty" HeaderText="Target Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aspend" HeaderText="Nilai Aspend">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceitem" HeaderText="Pricelist">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Bold="True" ForeColor="Red" Width="2px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="2px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="promdtloid" HeaderText="promdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="lblmsg3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR /><asp:Label id="Label36" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Reward : " __designer:wfdid="w17" Font-Underline="True"></asp:Label><BR /><asp:GridView id="GvRewardDtl" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w18" EmptyDataText="No Data" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item. Reward">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TargetPoint" HeaderText="Target Point">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TargetAmt" HeaderText="Target Amt">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rQty" HeaderText="Qty Reward">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rAmount" HeaderText="Amt. Reward">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="targetqty" HeaderText="Target Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="persenreward" HeaderText="Reward1(%)">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="persenreward2" HeaderText="Reward2(%)">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TypeNya" HeaderText="Type Reward">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Bold="True" ForeColor="Red" Width="2px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="2px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="RewardOid" HeaderText="RewardOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="lblmsg3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR /><asp:Label id="LblRevisi" runat="server" Visible="False" __designer:wfdid="w19"></asp:Label> <BR /><asp:TextBox id="TextBox4" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine" __designer:wfdid="w20" MaxLength="250"></asp:TextBox><BR /><BR /><asp:ImageButton id="BtnAppPromo" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" __designer:wfdid="w21" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="BtnRejectProm" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" __designer:wfdid="w22" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="ImageButton3" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w23" AlternateText="Revised"></asp:ImageButton> <asp:ImageButton id="BtnBackProm" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" __designer:wfdid="w24" AlternateText="Back"></asp:ImageButton>&nbsp;<asp:Button id="Button1" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision" __designer:wfdid="w25"></asp:Button> <asp:Button id="Button2" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision" __designer:wfdid="w26"></asp:Button> </asp:View><BR /><asp:View id="View28" runat="server" __designer:wfdid="w2"><BR /><asp:Label id="Label42" runat="server" Width="78px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Retur NT" __designer:wfdid="w3" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GVReturNT" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w1" GridLines="None" DataKeyNames="trnbiayaeksoid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="No. Nota"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("transferno") %>' __designer:wfdid="w3"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lkbSelectNTR" onclick="lkbSelectNTR_Click" runat="server" Text='<%# Eval("trnbiayaeksno") %>' __designer:wfdid="w1" CommandName='<%# Eval("branch_code") %>' ToolTip='<%# Eval("trnbiayaeksoid") %>' CommandArgument='<%# Eval("approvaloid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppName" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbiayaeksdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtekspedisi" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="User Input">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:View><BR /><asp:View id="View29" runat="server" __designer:wfdid="w5"><BR /><asp:Label id="Label43" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="NT Return Data" __designer:wfdid="w6" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold" align=left>Cabang</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="CbgExp" runat="server" __designer:wfdid="w4"></asp:Label><asp:Label id="ExpOid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w5"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>Type Nota</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="TypeNota" runat="server" __designer:wfdid="w6"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>No. Biaya</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="biayaeksno" runat="server" __designer:wfdid="w7"></asp:Label> </TD><TD style="FONT-WEIGHT: bold" align=left>Tanggal</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="PaymentDate" runat="server" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Customer</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="ntCustNames" runat="server" __designer:wfdid="w9"></asp:Label> <asp:Label id="NtCustoid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w10"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>No. Bank</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="CashBankNo" runat="server" __designer:wfdid="w11"></asp:Label><asp:Label id="cashbankoid" runat="server" Width="50px" Visible="False" __designer:wfdid="w12"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Amount</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="amtekspedisi" runat="server" __designer:wfdid="w13"></asp:Label></TD><TD style="FONT-WEIGHT: bold" align=left>Coa</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left><asp:DropDownList id="cashbankacctgoid" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w14" Enabled="False"></asp:DropDownList><asp:Label id="CabangDr" runat="server" Width="50px" Visible="False" __designer:wfdid="w15"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left>Reason (Alasan)</TD><TD style="FONT-WEIGHT: bold" align=left>:</TD><TD style="FONT-WEIGHT: bold" align=left colSpan=4><asp:TextBox id="ReasonRetur" runat="server" Width="342px" CssClass="inpTextDisabled" TextMode="MultiLine" __designer:wfdid="w16" MaxLength="300" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=6><asp:Label id="LblCustOid" runat="server" Width="50px" Visible="False" __designer:wfdid="w17"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=6><asp:GridView id="GVDtlEkp" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w18" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="payseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="DescNya" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Overline="False" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualoid" HeaderText="trnbelimstoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label6" runat="server" Font-Size="X-Small" 
        ForeColor="Red" Text="Data detail belum ada..!!!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=6><asp:ImageButton id="BtnAppNT" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" __designer:wfdid="w19" AlternateText="Approve"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnRejectNT" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" __designer:wfdid="w20" AlternateText="Reject"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnBackNT" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" __designer:wfdid="w21" AlternateText="Back"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View42" runat="server" __designer:wfdid="w60"><asp:Label id="Label48" runat="server" Width="95px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Servis Final" __designer:wfdid="w61" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GvFinalData" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w62" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trfwhserviceoid" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="No. Final"><ItemTemplate>
<asp:LinkButton id="lkbSelectFinal" runat="server" Text='<%# Eval("trfwhserviceno") %>' __designer:wfdid="w93" CommandName='<%# Eval("TrfwhserviceNo") %>' ToolTip='<%# Eval("trfwhserviceoid") %>' CommandArgument='<%# Eval("approvaloid") %>' OnClick="lkbSelectFinal_Click"></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="SuppName" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfwhservicedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl Final">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppName" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:View> <asp:View id="View43" runat="server" __designer:wfdid="w63"><asp:Label id="Label49" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Service Final Data :" __designer:wfdid="w64" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width=800><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Nomer Final</TD><TD align=left>: <asp:Label id="FinalNo" runat="server" __designer:wfdid="w65"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Type Final</TD><TD align=left>:&nbsp;<asp:Label id="TypeFInal" runat="server" __designer:wfdid="w66"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Tanggal</TD><TD align=left>: <asp:Label id="FinalDate" runat="server" __designer:wfdid="w67"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Customer</TD><TD align=left>: <asp:Label id="CustNameNya" runat="server" __designer:wfdid="w68"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Cabang </TD><TD align=left>:&nbsp;<asp:Label id="CabangFinal" runat="server" __designer:wfdid="w69"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>No. TTS</TD><TD align=left>: <asp:Label id="NoTTS" runat="server" __designer:wfdid="w70"></asp:Label><asp:Label id="FinOid" runat="server" Visible="False" __designer:wfdid="w71"></asp:Label><asp:Label id="OidReq" runat="server" Visible="False" __designer:wfdid="w72"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Note Final</TD><TD align=left colSpan=3>: <asp:Label id="FinalNote" runat="server" __designer:wfdid="w73"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=4><asp:Label id="CodeCabangAsal" runat="server" Visible="False" __designer:wfdid="w74"></asp:Label><asp:Label id="CabangFinalCode" runat="server" Visible="False" __designer:wfdid="w75"></asp:Label><asp:Label id="OidGudangBagus" runat="server" Visible="False" __designer:wfdid="w76"></asp:Label><asp:Label id="OidGudangRusak" runat="server" Visible="False" __designer:wfdid="w77"></asp:Label> <asp:Label id="CustOid" runat="server" Visible="False" __designer:wfdid="w78"></asp:Label><asp:Label id="OidGudangTitipan" runat="server" Visible="False" __designer:wfdid="w79"></asp:Label> <asp:Label id="ReqCode" runat="server" Visible="False" __designer:wfdid="w79"></asp:Label></TD></TR></TBODY></TABLE><BR /><asp:Label id="Label63" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang : " __designer:wfdid="w80" Font-Underline="True"></asp:Label><BR /><DIV style="OVERFLOW-Y: scroll; WIDTH: 800px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvrequest" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w81" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemOid" HeaderText="ItemOid" Visible="False">
<HeaderStyle HorizontalAlign="Left" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="snno" HeaderText="Serial No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqqty" DataFormatString="{0:#,###.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Kelengkapan" HeaderText="Kelengkapan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtljob" HeaderText="Kerusakan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="satuan1" Visible="False"></asp:BoundField>
<asp:BoundField DataField="typegaransi" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqmstoid" HeaderText="lblreqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="PARTOID" HeaderText="PARTOID" Visible="False"></asp:BoundField>
<asp:BoundField DataField="partdescshort" HeaderText="Spare Parts">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PartUnitOid" HeaderText="PartUnitOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="partsqty" HeaderText="Parts Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PartsUnit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtrloc" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="spartprice" HeaderText="Parts Price">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalprice" HeaderText="Total Nett Parts">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="flagbarang" HeaderText="Flag Barang" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red" __designer:wfdid="w335">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><BR /><asp:Label id="Label64" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Sparepart : " __designer:wfdid="w82" Font-Underline="True"></asp:Label><BR /><DIV style="OVERFLOW-Y: scroll; WIDTH: 800px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GvRusak" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w83" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnfinalpartoid,trnfinaloid,reqoid,reqdtloid,itemrusakoid,itemreqoid,itempartoid,qtyrusak,qtyreq,qtypart,seq,locoidrusak,itemdesc" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" Width="70px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyrusak" HeaderText="Qty Rusak">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtypart" HeaderText="Qty Part" Visible="False"></asp:BoundField>
<asp:BoundField DataField="qtyreq" HeaderText="Qty Req" Visible="False">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="locoidrusak" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reqmstoid" HeaderText="reqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemrusakoid" HeaderText="itemrusakoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itempartoid" HeaderText="itempartoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemreqoid" HeaderText="itemreqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnfinaloid" HeaderText="trnfinaloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnfinalpartoid" HeaderText="trnfinalpartoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red" __designer:wfdid="w335">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="KetRev" runat="server" Visible="False" __designer:wfdid="w84"></asp:Label><BR /><asp:TextBox id="KetRevisi" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine" __designer:wfdid="w85" MaxLength="250"></asp:TextBox><BR /><asp:ImageButton id="BtnAppFinal" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" __designer:wfdid="w86" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="BtnRejectFinal" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" __designer:wfdid="w87" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="BtnRevFinal" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w88" AlternateText="Revised"></asp:ImageButton> <asp:ImageButton id="BtnBackFinal" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" __designer:wfdid="w89" AlternateText="Back"></asp:ImageButton>&nbsp;<asp:Button id="BtnSubmitFinal" runat="server" CssClass="btn red" Visible="false" Text="Submit Revision" __designer:wfdid="w90"></asp:Button> <asp:Button id="BtnCancelFinal" runat="server" CssClass="btn gray" Visible="false" Text="Cancel Revision" __designer:wfdid="w91"></asp:Button> </asp:View> <asp:View id="View44" runat="server" __designer:wfdid="w96"><asp:Label id="Label60" runat="server" Width="155px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Servis Final Internal" __designer:wfdid="w97" Font-Underline="True"></asp:Label><BR /><BR /><asp:GridView id="GvFinalInt" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w98" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trfwhserviceoid" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="No. Final"><ItemTemplate>
<asp:LinkButton id="lkbSelectFinalInt" onclick="lkbSelectFinalInt_Click" runat="server" Text='<%# Eval("trfwhserviceno") %>' __designer:wfdid="w150" CommandName='<%# Eval("TrfwhserviceNo") %>' ToolTip='<%# Eval("trfwhserviceoid") %>' CommandArgument='<%# Eval("approvaloid") %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="SuppName" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfwhservicedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl Final">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppName" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestuser" HeaderText="Request User">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="requestdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Request Date">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="lblmsg2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:View> <asp:View id="View45" runat="server" __designer:wfdid="w124"><asp:Label id="Label65" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Service Final Internal Data :" __designer:wfdid="w125" Font-Underline="True"></asp:Label><BR /><BR /><TABLE width="100%"><TBODY><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Cabang</TD><TD style="WIDTH: 257px" align=left>: <asp:Label id="FinalCabang" runat="server" __designer:wfdid="w126"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Customer</TD><TD align=left>:&nbsp;<asp:Label id="FinalCust" runat="server" __designer:wfdid="w127"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Tanggal</TD><TD style="WIDTH: 257px" align=left>: <asp:Label id="TglFinalInt" runat="server" __designer:wfdid="w128"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>No. TTS</TD><TD align=left>: <asp:Label id="ReqNoFinal" runat="server" __designer:wfdid="w129"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>No. Final</TD><TD style="WIDTH: 257px" align=left>:&nbsp;<asp:Label id="NoFinalInt" runat="server" __designer:wfdid="w130"></asp:Label></TD><TD style="FONT-WEIGHT: bold; WIDTH: 108px" align=left>Keterangan</TD><TD align=left>: <asp:Label id="FinalIntNote" runat="server" __designer:wfdid="w131"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold" align=left colSpan=4><asp:Label id="BranchCodeFinal" runat="server" Visible="False" __designer:wfdid="w132"></asp:Label><asp:Label id="FinoidInt" runat="server" Visible="False" __designer:wfdid="w133"></asp:Label><asp:Label id="CustOidIntFin" runat="server" Visible="False" __designer:wfdid="w134"></asp:Label></TD></TR></TBODY></TABLE><BR /><asp:Label id="Label700" runat="server" Width="165px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang Rusak :" __designer:wfdid="w135" Font-Underline="True"></asp:Label><BR /><DIV style="OVERFLOW-Y: scroll; WIDTH: 750px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GVFinIntRusak" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w136" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemOid" HeaderText="ItemOid" Visible="False">
<HeaderStyle HorizontalAlign="Left" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="snno" HeaderText="Serial No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl1" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqmstoid" HeaderText="lblreqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="mtrloc" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red" __designer:wfdid="w335">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><BR /><asp:Label id="Label77" runat="server" Width="132px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Sparepart : " __designer:wfdid="w137" Font-Underline="True"></asp:Label><BR /><DIV style="OVERFLOW-Y: scroll; WIDTH: 750px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GvParts" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w138" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="seq,finoid,itemcode,itemdesc,qty,notedtl1,mtrlocoid,reqoid,reqdtloid,hpp,itemrusakoid,PartOid" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PartOid" HeaderText="PartOid" Visible="False">
<HeaderStyle HorizontalAlign="Left" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl1" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="lblreqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="mtrlocoid" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="hpp" HeaderText="HPP" Visible="False"></asp:BoundField>
<asp:BoundField DataField="finoid" HeaderText="finoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemrusakoid" HeaderText="itemrusakoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red" __designer:wfdid="w335">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><BR /><asp:Label id="Label66" runat="server" Width="193px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang Rombeng :" __designer:wfdid="w139" Font-Underline="True"></asp:Label> <DIV style="OVERFLOW-Y: scroll; WIDTH: 750px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GvRombeng" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w140" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="seq,itemrombengoid,itemcode,itemdesc,qty,mtrlocoid,hpp,notedtl3" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemrombengoid" HeaderText="itemrombengoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl3" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="lblreqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="mtrlocoid" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="hpp" HeaderText="HPP" Visible="False"></asp:BoundField>
<asp:BoundField DataField="finoid" HeaderText="finoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemrusakoid" HeaderText="itemrusakoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red" __designer:wfdid="w335">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><BR /><asp:Label id="Label67" runat="server" Width="163px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang Bagus :" __designer:wfdid="w141" Font-Underline="True"></asp:Label><BR /><DIV style="OVERFLOW-Y: scroll; WIDTH: 750px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GvBagus" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w142" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="seq,itembagusoid,itemcode,itemdesc,qty,mtrlocoid,hpp,notedtl4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itembagusoid" HeaderText="itembagusoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl4" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gudang" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqmstoid" HeaderText="lblreqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="mtrlocoid" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="hpp" HeaderText="HPP" Visible="False"></asp:BoundField>
<asp:BoundField DataField="finoid" HeaderText="finoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemrusakoid" HeaderText="itemrusakoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red" __designer:wfdid="w335">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="Label78" runat="server" Visible="False" __designer:wfdid="w143"></asp:Label><BR /><asp:TextBox id="TextBox5" runat="server" Width="342px" CssClass="inpText" Visible="false" TextMode="MultiLine" __designer:wfdid="w144" MaxLength="250"></asp:TextBox><BR /><asp:ImageButton id="BtnAppFinInt" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" __designer:wfdid="w145" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="BtnRejectFinInt" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" __designer:wfdid="w146" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="BtnRevFinInt" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w147" AlternateText="Revised"></asp:ImageButton> <asp:ImageButton id="BtnBackFinInt" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" __designer:wfdid="w148" AlternateText="Back"></asp:ImageButton></asp:View></asp:MultiView><TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                </table>
    <asp:UpdatePanel id="upPreview" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPreview" runat="server" Width="750px" CssClass="modalBox" Visible="False"><TABLE width="100%" align=center><TBODY><TR><TD align=center><asp:Label id="lblCaptPreview" runat="server" Font-Size="Medium" Font-Bold="True" Text="Preview Jurnal"></asp:Label> </TD></TR><TR><TD><asp:Panel id="pnlScrollPreview" runat="server" Width="100%" Height="200px" ScrollBars="Vertical"><asp:GridView id="gvPreview" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None" EmptyDataRowStyle-ForeColor="Red" AllowSorting="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="code" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="desc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="credit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No Data Found !!"></asp:Label>
                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 15px" align=center>&nbsp;<asp:LinkButton id="lkbClosePreview" runat="server" Font-Bold="True">[ CLOSE ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="beHidePreview" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpePreview" runat="server" Drag="True" PopupControlID="pnlPreview" PopupDragHandleControlID="lblCaptPreview" BackgroundCssClass="modalBackground" TargetControlID="beHidePreview">
                                                            </ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>

</asp:Content>

