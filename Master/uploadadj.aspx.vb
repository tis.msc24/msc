Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports System.Data.OleDb

Partial Class Master_uploadadj
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public BranchCode As String = ConfigurationSettings.AppSettings("BranchCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim oRegex As Regex
    Dim oMatches As MatchCollection
#End Region

#Region "Functions"
   
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub setTableDetailPromo()
        If Session("viewTarget") Is Nothing Then
            Dim tblDetail As DataTable
            tblDetail = New DataTable("QL_trnUpload")
            tblDetail.Columns.Add("nomor", Type.GetType("System.Int32")) '0
            tblDetail.Columns.Add("itemoid", Type.GetType("System.Int32")) '0
            tblDetail.Columns.Add("gudang", Type.GetType("System.Int32")) '1
            tblDetail.Columns.Add("hpp", Type.GetType("System.Decimal")) '2
            tblDetail.Columns.Add("saldo", Type.GetType("System.Decimal")) '3
            tblDetail.Columns.Add("opname", Type.GetType("System.Decimal")) '4
            Session("viewTarget") = tblDetail
        End If
    End Sub

    Private Sub GenerateDraftNo()
        sSql = "SELECT ISNULL(MAX(CAST(ISNULL(resfield1,0) AS INTEGER)) + 1, 1) AS IDNEW FROM ql_trnstockadj"
        resfield1.Text = ToDouble(cKoneksi.ambilscalar(sSql).ToString)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~/Other/login.aspx")
        ' THROW TO NOT AUTHORIZED PAGE IF NO ROLE EXIST
        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Response.Redirect("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("uploadadj.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Upload Adjusment" ' CHANGE FORM NAME HERE
        Session("oid") = Request.QueryString("oid")
        btnSave.Attributes.Add("OnClick", "javascript:return confirm('Are u sure want to SAVE this data?');")

        If Not Page.IsPostBack Then
            setTableDetailPromo()
            upduser.Text = Session("UserID")
            updtime.Text = GetServerTime()
        End If
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("uploadadj.aspx?awal=true")
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Master\uploadadj.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnSave_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim Validasi As String = ""
        If (Session("viewTarget")) Is Nothing Then
            Validasi &= "- No Item Data !!<br/>"
        Else
            Dim objTran As DataTable = Session("viewTarget")
            If (objTran.Rows.Count) <= 0 Then
                showMessage("No Item Detail Data", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If

        If Validasi <> "" Then
            showMessage(Validasi, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Dim iStockAdjOid As Integer = GenerateID("QL_TRNSTOCKADJ", CompnyCode)
        Dim iConMtrOid As Integer = GenerateID("ql_conmtr", CompnyCode)
        Dim iCrdMatOid As Integer = GenerateID("ql_crdmtr", CompnyCode)
        Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim sDate As Date = "12/28/2016"
        Dim di As Date = GetServerTime()
        Dim iStockAcctgGMOid As String = "" : Dim bc As String = ""
        Dim iGlSeq As Integer = 1 : Dim glValue As Double = 0
        Dim adjin As Double = 0 : Dim adjout As Double = 0 : Dim qty As Double = 0
        Dim sDebetCode As String = "" : Dim sDebetOid As Integer = 0
        Dim sCreditCode As String = "" : Dim sCreditOid As Integer = 0
        If Not Session("viewTarget") Is Nothing Then
            Dim obj2 As DataTable = Session("viewTarget")
            For c1 As Int16 = 0 To obj2.Rows.Count - 1
                sSql = "Select b.gencode as branchcode from QL_mstgen a inner join QL_mstgen b on b.genoid=CAST(a.genother2 as int) where a.gengroup='LOCATION'and a.genoid=" & obj2.Rows(c1).Item("gudang") & ""
                bc = GetStrData(sSql)
                iStockAcctgGMOid = GetAcctgOID(GetVarInterface("VAR_GUDANG", bc), CompnyCode)
            Next
        End If
        Dim BranchDesc As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & bc & "' And gengroup='CABANG'")
        Dim code As String = "ADJ/" & BranchDesc & "/" & Format(sDate, "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(stockadjno,1) AS INT)),0) FROM ql_trnstockadj WHERE stockadjno LIKE '" & code & "%'"
        Dim sequence As String = Format((ToDouble(GetStrData(sSql)) + 1), "0000")
        stockadjno.Text = code & sequence
        GenerateDraftNo()

        Dim objTransApproval As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTransApproval = conn.BeginTransaction()
        xCmd.Transaction = objTransApproval

        Try
            If Not Session("viewTarget") Is Nothing Then
                Dim obj As DataTable = Session("viewTarget")
                For c1 As Int16 = 0 To obj.Rows.Count - 1
                    sSql = "select b.gencode as branchcode from QL_mstgen a inner join QL_mstgen b on b.genoid=CAST(a.genother2 as int) where a.gengroup='LOCATION'and a.genoid=" & obj.Rows(c1).Item("gudang") & ""
                    xCmd.CommandText = sSql
                    bc = CStr(xCmd.ExecuteScalar)

                    'Insert Ql_trnstockadj
                    sSql = "INSERT INTO QL_trnstockadj (cmpcode,branch_code, stockadjoid, stockadjno, stockadjdate, periodacctg, refoid, mtrwhoid, stockadjqtybefore, stockadjqty, stockadjunit, stockadjamtidr, stockadjamtusd, stockadjnote, stockadjstatus, resfield1, resfield2, createuser, createdate, upduser, updtime, adjtype) " & _
                    " VALUES ('" & CompnyCode & "','" & bc & "', " & iStockAdjOid & ", '" & stockadjno.Text & "', '" & sDate & "', '" & GetDateToPeriodAcctg(sDate) & "', " & obj.Rows(c1)("itemoid").ToString & ", " & obj.Rows(c1)("gudang").ToString & ", " & ToDouble(obj.Rows(c1)("saldo").ToString) & ", " & ToDouble(obj.Rows(c1)("opname").ToString) & ", 945, " & ToDouble(obj.Rows(c1)("hpp").ToString) & ", 0, 'Penyesuaian stok', 'Approved', '" & resfield1.Text & "', '" & iStockAcctgGMOid & "','admin', '" & sDate & "', 'admin', '" & sDate & "','ADJUSMENT')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iStockAdjOid += 1

                    qty = ToDouble(obj.Rows(c1)("opname").ToString) - ToDouble(obj.Rows(c1)("saldo").ToString) 

                    If ToDouble(obj.Rows(c1)("saldo").ToString) < ToDouble(obj.Rows(c1)("opname").ToString) Then
                        adjin = (ToDouble(obj.Rows(c1)("saldo").ToString) - ToDouble(obj.Rows(c1)("opname").ToString)) * (-1)
                        adjout = 0
                        glValue = ToDouble(adjin) * (ToDouble(obj.Rows(c1)("hpp").ToString))
                    Else
                        adjout = ToDouble(obj.Rows(c1)("saldo").ToString) - ToDouble(obj.Rows(c1)("opname").ToString)
                        adjin = 0
                        glValue = ToDouble(adjout) * (ToDouble(obj.Rows(c1)("hpp").ToString))
                    End If
                    'insert&update stock
                    sSql = "UPDATE QL_crdmtr SET qtyadjin=qtyadjin + " & IIf(ToDouble(qty) >= 0, ToDouble(qty), 0) & ", qtyadjout = qtyadjout + " & IIf(ToDouble(qty) < 0, -1 * ToDouble(qty), 0) & ", saldoakhir = saldoakhir + " & ToDouble(qty) & ", lasttranstype='QL_trnstockadj', lastTrans='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND refoid=" & obj.Rows(c1)("itemoid").ToString & " AND periodacctg='" & GetPeriodAcctg(sDate) & "' AND mtrlocoid=" & obj.Rows(c1)("gudang").ToString & " And branch_code = '" & bc & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "INSERT INTO ql_conmtr (cmpcode,conmtroid, type, trndate, periodacctg, formaction, formoid, Formname, refoid, refname,unitoid, mtrlocoid,branch_code, qtyin, qtyout, reason, upduser, updtime, typemin, personoid,amount,note,HPP) VALUES ('" & CompnyCode & "'," & iConMtrOid & ",'AS','" & sDate & "','" & GetPeriodAcctg(sDate) & "','" & stockadjno.Text & "'," & iStockAdjOid & ",'QL_trnstockadj'," & obj.Rows(c1)("itemoid").ToString & ",'QL_MSTITEM',945,'" & obj.Rows(c1)("gudang").ToString & "','" & bc & "'," & adjin & "," & adjout & ",'','admin','" & sDate & "',0,0," & ToDouble(glValue) & ",'Penyesuaian stock','" & ToDouble(obj.Rows(c1)("hpp").ToString) & "')"

                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iConMtrOid += 1
                    sSql = "update ql_mstitem set hpp=" & ToDouble(obj.Rows(c1)("hpp").ToString) & " Where itemoid=" & obj.Rows(c1)("itemoid").ToString & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If ToDouble(obj.Rows(c1)("saldo").ToString) < ToDouble(obj.Rows(c1)("opname").ToString) Then
                        sDebetCode = GetVarInterface("VAR_ADJUST_STOCK+", bc)
                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & sDebetCode & "'"
                        xCmd.CommandText = sSql : sDebetOid = xCmd.ExecuteScalar
                    Else
                        sDebetCode = GetVarInterface("VAR_ADJUST_STOCK-", bc)
                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & sDebetCode & "'"
                        xCmd.CommandText = sSql : sDebetOid = xCmd.ExecuteScalar
                    End If

                    sCreditCode = GetVarInterface("VAR_GUDANG", bc)
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & sCreditCode & "'"
                    xCmd.CommandText = sSql : sCreditOid = xCmd.ExecuteScalar

                    'jurnal detail
                    If ToDouble(glValue) <> 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode,branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "','" & bc & "', " & iGLDtlOid & ", " & iGlSeq & ", " & iGLMstOid & ", '" & sDebetOid & "','D', " & glValue & "," & glValue & ",0,'" & stockadjno.Text & "', 'ADJUSTMENT STOCK', '', '','POST','" & sDate & "', 'admin', '" & sDate & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGLDtlOid += 1 : iGlSeq += 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "','" & bc & "', " & iGLDtlOid & ", " & iGlSeq & ", " & iGLMstOid & ", " & sCreditOid & ",'C', " & glValue & "," & glValue & " , 0, '" & stockadjno.Text & "', 'ADJUSTMENT STOCK', '', '', 'POST', '" & sDate & "', 'admin', '" & sDate & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGLDtlOid += 1 : iGlSeq += 1
                    End If
                Next
                'jurnal Mst
                ' Insert GL MST
                sSql = "INSERT INTO QL_trnglmst (cmpcode,branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime) VALUES ('" & CompnyCode & "','" & bc & "', " & iGLMstOid & ",'" & sDate & "', '" & GetPeriodAcctg(sDate) & "', 'AS', 'POST', '" & sDate & "', 'admin', '" & sDate & "', 'admin', '" & sDate & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iStockAdjOid - 1 & " WHERE tablename='QL_TRNSTOCKADJ' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iConMtrOid - 1 & " WHERE tablename='ql_conmtr' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdMatOid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTransApproval.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTransApproval.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBoxWarn")
            Exit Sub
        End Try
        showMessage("Data telah disimpan !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub

    Protected Sub btnXls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnXls.Click
        Dim objTran As DataTable = Session("viewTarget")
        If (objTran.Rows.Count) > 0 Then
            showMessage("Data Excel TO Exist From List", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Dim excelconn As New OleDbConnection
        Try
            If fuXls.HasFile Then
                Dim filePath As String = fuXls.PostedFile.FileName
                Dim fileName As String = Path.GetFileName(filePath)
                Dim ext As String = Path.GetExtension(fileName)
                Dim type As String = String.Empty
                Dim uploadName As String = "ADJ" & Format("12/28/2016", "MMddyyyyHHmm")

                Select Case ext
                    Case ".xls"
                        type = "application/vnd.ms-excel"
                    Case ".xlsx"
                        type = "application/vnd.ms-excel"
                End Select

                If type <> String.Empty Then
                    fuXls.SaveAs(Server.MapPath("~/Upload/" & uploadName & ext))

                    If File.Exists(Server.MapPath("~/Upload/" & uploadName & ext)) Then
                        Dim exceldtab As DataTable = Session("viewTarget")
                        Dim exceldrow As DataRow

                        Dim sHeaderMsg As String = "" : Dim errMsg As String = ""
                        Dim excelconnstr As String = "" : Dim iMatCode As String = ""
                        Dim iMatOid As Integer = 0 : Dim iMatDesc As String = ""

                        If ext = ".xlsx" Then
                            excelconnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Server.MapPath("~/Upload/" & uploadName & ext) & "';Extended Properties=Excel 12.0;Persist Security Info=False"
                        ElseIf ext = ".xls" Then
                            excelconnstr = "Provider=Microsoft.JET.OLEDB.4.0;Data Source='" & Server.MapPath("~/Upload/" & uploadName & ext) & "';Extended Properties=Excel 8.0;Persist Security Info=False"
                        End If

                        excelconn.ConnectionString = excelconnstr
                        excelconn.Open()

                        Dim ds As New DataSet
                        Try
                            Dim sqlstring As String = "SELECT * FROM [ADJ$]"
                            Dim oda As New OleDb.OleDbDataAdapter(sqlstring, excelconnstr)
                            oda.Fill(ds, "ADJ")
                            excelconn.Close()
                        Catch ex As Exception
                            showMessage("Format Template Salah, Ganti Nama Sheet Menjadi ADJ ..!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                            Exit Sub
                        End Try
                        Dim dtab As DataTable = ds.Tables("ADJ")

                        If conn.State = ConnectionState.Closed Then
                            conn.Open()
                        End If

                        Dim dtrow() As DataRow = dtab.Select("", "")
                        Dim trnbelidate As New Date
                        Dim dvExcel As DataView
                        dvExcel = exceldtab.DefaultView

                        'Validasi Excel
                        For i As Integer = 0 To dtrow.Length - 1
                            If dtrow(i).Item("itemoid").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum itemoid " & dtrow(i).Item("itemoid").ToString.Trim & " Harus Angka<br />"
                            End If
                            If dtrow(i).Item("gudang").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum gudang " & dtrow(i).Item("gudang").ToString.Trim & " Harus Angka<br />"
                            End If
                            If dtrow(i).Item("hpp").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum hpp " & dtrow(i).Item("hpp").ToString.Trim & " Harus Angka<br />"
                            End If
                            If dtrow(i).Item("saldo").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum saldo " & dtrow(i).Item("saldo").ToString.Trim & " Harus Angka<br />"
                            End If
                            If dtrow(i).Item("opname").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum opname " & dtrow(i).Item("opname").ToString.Trim & " Harus Angka<br />"
                            End If
                        Next
                        If sHeaderMsg.Trim <> "" Then
                            showMessage(sHeaderMsg.Trim, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                            Exit Sub
                        End If

                        For i As Integer = 0 To dtrow.Length - 1
                            sSql = "select b.gencode as branchcode from QL_mstgen a inner join QL_mstgen b on b.genoid=CAST(a.genother2 as int) where a.gengroup='LOCATION'and a.genoid=" & dtrow(i).Item("gudang").ToString.Trim & ""
                            xCmd.CommandText = sSql
                            Dim bc As String = xCmd.ExecuteScalar()

                            sSql = "select saldoAkhir from QL_crdmtr Where refoid=" & dtrow(i).Item("itemoid").ToString & " And periodacctg='2016-12' and mtrlocoid=" & dtrow(i).Item("gudang").ToString & " AND branch_code='" & bc & "'"
                            xCmd.CommandText = sSql
                            Dim sa As Integer = xCmd.ExecuteScalar()

                            exceldrow = exceldtab.NewRow
                            exceldrow("nomor") = i + 1
                            exceldrow("itemoid") = dtrow(i).Item("itemoid").ToString
                            exceldrow("gudang") = dtrow(i).Item("gudang").ToString
                            exceldrow("hpp") = ToDouble(dtrow(i).Item("hpp").ToString)
                            exceldrow("saldo") = ToDouble(dtrow(i).Item("saldo").ToString)
                            exceldrow("opname") = ToDouble(dtrow(i).Item("opname").ToString)
                            exceldtab.Rows.Add(exceldrow)
                        Next

                        conn.Close()
                        gvDtl.DataSource = exceldtab
                        gvDtl.DataBind()
                    Else
                        showMessage("Upload failed!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    End If
                Else
                    excelconn.Close() : conn.Close()
                    showMessage("You can only select .xls or .xlsx file!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                End If
            Else
                showMessage("Please choose file to upload first.", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            End If

        Catch ex As Exception
            excelconn.Close() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End Try
    End Sub
#End Region

End Class
