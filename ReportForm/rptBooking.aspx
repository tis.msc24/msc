<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptBooking.aspx.vb" Inherits="ReportForm_rptBooking" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" style="width: 976px; height: 168px">
        <tr>
            <td colspan="3" rowspan="3" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                                Text=".: Actual Booking Stock Report" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                </table>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 560px; HEIGHT: 72px">
<TBODY>
<TR>
<TD align=right></TD>
<TD style="WIDTH: 345px" align=left colSpan=3>
<asp:TextBox id="range1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w1" Visible="False"></asp:TextBox>&nbsp;
<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w2" Visible="False"></asp:ImageButton>&nbsp; 
<asp:TextBox id="range2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w3" Visible="False"></asp:TextBox>&nbsp;
<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4" Visible="False"></asp:ImageButton>&nbsp;
<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w5" Visible="False"></asp:Label></TD>
</TR>
<TR>
<TD align=right>Filter&nbsp;:</TD>
<TD style="WIDTH: 345px" align=left colSpan=3>
<asp:DropDownList id="ddltype" runat="server" Width="69px" CssClass="inpText" __designer:wfdid="w6" AutoPostBack="True" OnSelectedIndexChanged="ddltype_SelectedIndexChanged">
<asp:ListItem Value="Summary">SO</asp:ListItem>
<asp:ListItem Value="Detail">Item</asp:ListItem>
</asp:DropDownList>
</TD>
</TR>
<TR>
<TD align=right>
<asp:Label id="Label4" runat="server" Width="98px" Font-Size="Small" Text="No SO :" __designer:wfdid="w7"></asp:Label>
</TD>
<TD style="WIDTH: 345px" align=left colSpan=3>
<asp:TextBox id="sono" runat="server" Width="159px" CssClass="inpText" __designer:wfdid="w8"></asp:TextBox>&nbsp;
<asp:ImageButton id="posearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;
<asp:ImageButton id="cancelpo" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton>&nbsp;
<asp:Label id="sooid" runat="server" __designer:wfdid="w11" Visible="False"></asp:Label>&nbsp;
<asp:DropDownList id="type_so" runat="server" Width="64px" CssClass="inpText" __designer:wfdid="w12" AutoPostBack="True" Visible="False">
<asp:ListItem Value="'%'">All</asp:ListItem>
<asp:ListItem Value="'SO/%'">SO</asp:ListItem>
<asp:ListItem Value="'SOT/%'">SOT</asp:ListItem>
<asp:ListItem Value="'SJ/%'">SJ</asp:ListItem>
</asp:DropDownList>
</TD>
</TR>
<TR>
<TD align=right></TD>
<TD style="WIDTH: 345px" align=left colSpan=3>
<asp:GridView style="Z-INDEX: 100; LEFT: 0px; POSITION: relative; TOP: 2px; BACKGROUND-COLOR: transparent" id="gvListpo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w13" Visible="False" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="ordermstoid,orderno,trnorderdate,trncustname,trncustoid">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="Order No">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderdate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=right><asp:Label id="Label5" runat="server" Width="151px" Font-Size="Small" Text="Customer Name" __designer:wfdid="w14"></asp:Label>&nbsp;:</TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:TextBox id="custname" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w15"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w17"></asp:ImageButton>&nbsp;<asp:Label id="custoid" runat="server" __designer:wfdid="w18" Visible="False"></asp:Label></TD></TR><TR><TD align=right></TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:GridView id="gvCustomer" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w19" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="Id,Name,code" BorderWidth="1px" BorderStyle="Solid">
<RowStyle BackColor="#F7F7DE" Font-Size="X-Small"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True"></asp:CommandField>
<asp:BoundField DataField="code" HeaderText="Code"></asp:BoundField>
<asp:BoundField DataField="Name" HeaderText="Nama"></asp:BoundField>
<asp:BoundField DataField="Id" HeaderText="Id" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#F25407"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#999999" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblstatusdataCust" runat="server" Text="No data found !!" CssClass="Important" __designer:wfdid="w74"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=right><asp:Label id="Label3" runat="server" Width="98px" Font-Size="Small" Text="Item Name :" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:TextBox id="itemdesc" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" onclick="btnEraseItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" __designer:wfdid="w5" Visible="False"></asp:Label></TD></TR><TR><TD align=right></TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="gvListForecast" runat="server" Width="98%" __designer:wfdid="w7" OnSelectedIndexChanged="gvListForecast_SelectedIndexChanged1" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemcode,itemdesc,itemoid">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Name">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD3" align=right runat="server" Visible="false"><asp:Label id="Label6" runat="server" Width="98px" Font-Size="Small" Text="Invoice No" __designer:wfdid="w21"></asp:Label>&nbsp;: </TD><TD style="WIDTH: 345px" id="TD2" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="invoiceno" runat="server" CssClass="inpText" __designer:wfdid="w22"></asp:TextBox> <asp:ImageButton id="ImageButton3" onclick="ImageButton3_Click1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w23"></asp:ImageButton> <asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w24"></asp:ImageButton> <asp:Label id="trnjualmstoid" runat="server" __designer:wfdid="w25"></asp:Label></TD></TR><TR><TD id="TD5" align=right Visible="true"><asp:Label id="lbltype" runat="server" Width="98px" Font-Size="Small" Text="Item Group :" __designer:wfdid="w26" Visible="False"></asp:Label></TD><TD style="WIDTH: 345px" id="TD4" align=left colSpan=3 Visible="true"><asp:DropDownList id="typeM" runat="server" CssClass="inpText" __designer:wfdid="w27" AutoPostBack="True" Visible="False"></asp:DropDownList></TD></TR><TR><TD align=right><asp:Label id="lbltypeM" runat="server" Width="162px" Font-Size="Small" Text="Item Sub Group :" __designer:wfdid="w28" Visible="False"></asp:Label></TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="mattype" runat="server" CssClass="inpText" __designer:wfdid="w29" AutoPostBack="True" Visible="False"></asp:DropDownList></TD></TR><TR><TD align=right></TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="sales" runat="server" Width="215px" CssClass="inpText" __designer:wfdid="w6" AutoPostBack="True" Visible="False"></asp:DropDownList></TD></TR><TR><TD align=right></TD><TD style="WIDTH: 345px" align=left colSpan=3><asp:DropDownList id="paytype" runat="server" Width="67px" CssClass="inpText" __designer:wfdid="w36" AutoPostBack="True" Visible="False"><asp:ListItem Value=" and 1=1 ">All</asp:ListItem>
<asp:ListItem Value="and  (mst.trnpaytype in (select g.genoid from QL_mstGen g where abs(replace(replace(g.gencode,'CRD',''),'CSH',''))=0    and gengroup='PAYTYPE'  ) or mst.trnpaytype=0)">Cash</asp:ListItem>
<asp:ListItem Value="and   mst.trnpaytype in (select g.genoid from QL_mstGen g where abs(replace(replace(g.gencode,'CRD',''),'CSH',''))&gt;0    and gengroup='PAYTYPE'  )  ">Credit</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 38px" align=center colSpan=4><BR /><asp:ImageButton id="btnView" onclick="btnView_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton> <asp:ImageButton id="BTNpRINT" onclick="BTNpRINT_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton> <asp:ImageButton id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton></TD></TR><TR><TD colSpan=4><ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w41" PopupButtonID="ImageButton1" TargetControlID="range1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w42" PopupButtonID="ImageButton2" TargetControlID="range2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w43" TargetControlID="range1" CultureName="en-US" MaskType="Date" ErrorTooltipEnabled="True" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w44" TargetControlID="range2" CultureName="en-US" MaskType="Date" ErrorTooltipEnabled="True" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w45"></asp:Label></TD></TR><TR><TD colSpan=4><asp:UpdatePanel id="UpdatePanel2" runat="server" __designer:dtid="844424930132012" __designer:wfdid="w46"><ContentTemplate __designer:dtid="844424930132013">
<asp:Panel id="panelDso" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w47" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="lblDSO" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List of Nota Jual" __designer:wfdid="w48"></asp:Label></TD></TR><TR><TD><asp:GridView style="Z-INDEX: 100; LEFT: 0px; POSITION: relative; TOP: 2px; BACKGROUND-COLOR: transparent" id="gvListCust" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w49" OnSelectedIndexChanged="gvListCust_SelectedIndexChanged1" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnjualmstoid,trnjualno,trnjualdate">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle ForeColor="Red" Font-Size="XX-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="No. Nota">
<ItemStyle HorizontalAlign="Left"></ItemStyle>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Date">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>

<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"></HeaderStyle>
</asp:BoundField>
</Columns>

<RowStyle BackColor="#F7F7DE"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="Yellow" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle BackColor="#507CD1" ForeColor="White" BorderColor="White" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="closeDSO" onclick="closeDSO_Click" runat="server" __designer:wfdid="w50">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderdso" runat="server" __designer:wfdid="w51" TargetControlID="btnHideDSO" BackgroundCssClass="modalBackground" PopupControlID="panelDso"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideDSO" runat="server" __designer:wfdid="w52" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w53"><ContentTemplate>
<asp:Panel id="PanelForecast" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w54" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 526px; TEXT-ALIGN: center"><asp:Label id="labelForecast" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Item" __designer:wfdid="w55"></asp:Label></TD></TR><TR><TD style="WIDTH: 526px"><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="FIELDSET2"><DIV id="div2"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"></DIV></FIELDSET> </TD></TR><TR><TD style="WIDTH: 526px; TEXT-ALIGN: center"><asp:LinkButton id="btnCloseForecast" onclick="btnCloseForecast_Click" runat="server" __designer:wfdid="w56">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderForecast" runat="server" __designer:wfdid="w57" TargetControlID="btnHideForecast" BackgroundCssClass="modalBackground" PopupControlID="PanelForecast"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideForecast" runat="server" Text="Button" __designer:wfdid="w58" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel5" runat="server" __designer:wfdid="w59"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w60" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center" colSpan=3><asp:Label id="Label13" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer" __designer:wfdid="w61"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; TEXT-ALIGN: center" id="TD1" class="Label" colSpan=3 runat="server" Visible="false"><SPAN style="FONT-SIZE: x-small"></SPAN>Filter : <asp:DropDownList id="DDLCustID" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w62"><asp:ListItem Selected="True" Value="Name">Name</asp:ListItem>
<asp:ListItem Value="Code">Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindCustID" runat="server" Width="129px" CssClass="inpText" __designer:wfdid="w63"></asp:TextBox> <asp:ImageButton id="ibtnCustID" onclick="ibtnCustID_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w64"></asp:ImageButton> <asp:ImageButton id="imbAllCust" onclick="imbAllCust_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w65"></asp:ImageButton></TD></TR><TR><TD colSpan=3><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="FIELDSET4"><DIV id="divgvCustomer"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"></DIV></FIELDSET> <asp:HiddenField id="HiddenFieldCust" runat="server" __designer:wfdid="w66"></asp:HiddenField> <asp:SqlDataSource id="SDSCust" runat="server" __designer:wfdid="w67" SelectCommand="SELECT c.custoid as Id,c.custcode as code,c.custname as Name,c.custaddr as Address from QL_mstcust c WHERE c.cmpcode =@cmpcode AND c.custname like @Name and c.custcode like @code  AND c.custflag='Active'  ORDER BY c.custoid DESC" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>"><SelectParameters>
<asp:Parameter DefaultValue="SDC" Name="cmpcode"></asp:Parameter>
<asp:Parameter DefaultValue="" Name="Name"></asp:Parameter>
<asp:Parameter Name="code"></asp:Parameter>
</SelectParameters>
</asp:SqlDataSource></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=3><asp:LinkButton id="LBCust" onclick="LBCust_Click" runat="server" __designer:wfdid="w68">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><asp:Button id="btnhiddenCust" runat="server" __designer:wfdid="w69" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w70" TargetControlID="btnhiddenCust" BackgroundCssClass="modalBackground" PopupControlID="panel1" Drag="True" PopupDragHandleControlID="lblCustdata"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel21" runat="server" __designer:wfdid="w71"><ContentTemplate>
<asp:Panel id="panelDsopo" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w72" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="lblDSOpo" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List of Order" __designer:wfdid="w73"></asp:Label></TD></TR><TR><TD></TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="closeDSOpo" runat="server" __designer:wfdid="w74">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderdsopo" runat="server" __designer:wfdid="w75" TargetControlID="btnHideDSOpo" BackgroundCssClass="modalBackground" PopupControlID="panelDsopo"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideDSOpo" runat="server" __designer:wfdid="w76" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <TABLE><TBODY><TR><TD style="WIDTH: 100px" vAlign=top align=left><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w77" AutoDataBind="True" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="BTNpRINT"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnView"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>

