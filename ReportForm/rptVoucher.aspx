<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptVoucher.aspx.vb" Inherits="ReportForm_rptVoucher" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="width: 975px;">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Voucher"></asp:Label></th>
        </tr>
<tr>
<th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD id="tdTipeLap1" align=left Visible="false"><asp:Label id="Label2" runat="server" Width="78px" Text="Tipe Laporan"></asp:Label></TD><TD align=left runat="server">:</TD><TD id="tdTipeLap2" align=left colSpan=3 Visible="false"><asp:DropDownList id="TypeLap" runat="server" Width="100px" CssClass="inpText" OnSelectedIndexChanged="type_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Value="PI">SUMMARY</asp:ListItem>
<asp:ListItem Value="PO">DETAIL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Tanggal"></asp:Label></TD><TD align=left runat="server" Visible="true">:</TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label></TD></TR><TR><TD id="Td9" align=right runat="server" Visible="true"></TD><TD align=right runat="server" Visible="true"></TD><TD id="Td10" align=left colSpan=3 runat="server" Visible="true"><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" PopupButtonID="imageButton1" TargetControlID="dateAwal" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" PopupButtonID="imageButton2" TargetControlID="dateAkhir" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD id="Td3" align=left runat="server" Visible="true">No. PO</TD><TD align=left runat="server" Visible="true">:</TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="nota" runat="server" Width="151px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" onclick="btnSearchNota_Click1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:Label id="oid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="Td1" align=right runat="server" Visible="true"></TD><TD align=right runat="server" Visible="true"></TD><TD id="Td2" align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="GVNota" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" OnSelectedIndexChanged="GVNota_SelectedIndexChanged1" CellPadding="4" UseAccessibleHeader="False" DataKeyNames="trnbelimstoid,trnbelipono,suppname,suppoid" AutoGenerateColumns="False" AllowPaging="True" PageSize="7" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelipono" HeaderText="No Nota">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="NoPI" runat="server" Text="No. PI"></asp:Label></TD><TD align=left runat="server" Visible="true"><asp:Label id="Titi2PI" runat="server" Text=":"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="txtInv" runat="server" Width="151px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchPI" onclick="btnSearchPI_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnErsaePI" onclick="BtnErsaePI_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:Label id="MstoidPI" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=right runat="server"></TD><TD align=right runat="server"></TD><TD align=left colSpan=3 runat="server"><asp:GridView id="GvPI" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" OnSelectedIndexChanged="GvPI_SelectedIndexChanged" CellPadding="4" UseAccessibleHeader="False" DataKeyNames="trnbelino,trnbelimstoid" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" GridLines="None" OnPageIndexChanging="GvPI_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="No PI">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal PI">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left runat="server">Supplier</TD><TD align=left runat="server">:</TD><TD align=left colSpan=3 runat="server"><asp:TextBox id="suppname" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupplier" onclick="btnSearchSupplier_click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseSupplier" onclick="btnEraseSupplier_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton><asp:Label id="suppoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="Td5" align=left Visible="false"></TD><TD align=left runat="server" Visible="false"></TD><TD id="Td6" align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="gvSupp" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged1" CellPadding="4" UseAccessibleHeader="False" DataKeyNames="suppcode,suppoid,suppname,supptype" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:DropDownList id="FilterDDLGrup" runat="server" Width="254px" CssClass="inpTextDisabled" Visible="False" Enabled="False"><asp:ListItem>Voucher</asp:ListItem>
</asp:DropDownList> <asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="Td7" align=left runat="server" Visible="false">Sub Group</TD><TD align=left runat="server" Visible="false"></TD><TD id="Td8" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="254px" CssClass="inpTextDisabled" Enabled="False"><asp:ListItem>Voucher</asp:ListItem>
</asp:DropDownList> <asp:Label id="itemcode" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="TD14" align=left runat="server" Visible="false"><asp:Label id="Label3" runat="server" Text="Voucher"></asp:Label></TD><TD align=left runat="server" Visible="false"></TD><TD id="TD13" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="itemname" runat="server" Width="151px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px"></asp:ImageButton><asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR><TR><TD id="TD15" align=right runat="server" Visible="false"></TD><TD align=right runat="server" Visible="false"></TD><TD id="TD16" align=left colSpan=3 runat="server" Visible="false"><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" CellPadding="4" UseAccessibleHeader="False" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3,merk" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Voucher">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:TextBox id="merk" runat="server" Width="250px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD id="TD11" align=left runat="server" Visible="false">Status PO</TD><TD align=left runat="server" Visible="false">:</TD><TD id="TD12" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="ddlstatusPO" runat="server" Width="78px" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=center colSpan=5>&nbsp; <asp:Label id="labelEx" runat="server" Width="29px"></asp:Label></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasExportButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="true" ShowAllPageIds="True" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</th>
</tr>
    </table>
</asp:Content>
