Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnTwSvcSupp
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure

#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        
        Return True
    End Function

    Private Function setTabelDetail() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("reqqty", Type.GetType("System.Decimal"))
        dtab.Columns.Add("reqdtljob", Type.GetType("System.String"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqcode", Type.GetType("System.String"))
        dtab.Columns.Add("snno", Type.GetType("System.String"))
        dtab.Columns.Add("qty", Type.GetType("System.Decimal"))
        Session("itemdetail") = dtab
        Return dtab
    End Function

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub ClearDtl()
        I_u2.Text = "new"
        notedtl.Text = "" : labelitemoid.Text = ""
        item.Text = "" : qty.Text = ""
        reqoid.Text = "" : reqdtloid.Text = ""
        txtPenerimaan.Text = "" : reqqty.Text = ""
        FromBranch.Enabled = False : FromBranch.CssClass = "inpTextDisabled"
        fromlocation.Enabled = False : fromlocation.CssClass = "inpTextDisabled"
    End Sub

    Private Sub FillTextbox(ByVal fCabang As String, ByVal OidTw As Integer)
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim transdate As New Date
            Dim fromlocationoid As Integer = 0 : Dim tolocationoid As Integer = 0
            sSql = "Select trfwhserviceoid,trfwhserviceNo,trfwhservicedate, trfwhservicenote,s.createtime, s.createuser, s.updtime,s.upduser, trfwhservicerefno, trfwhservicestatus, frombranch, tobranch, frommtrlocoid,s.suppoid,sp.suppname,Trfwhservicetype,ToBranch From ql_trfwhservicemst s Inner Join QL_mstsupp sp ON sp.suppoid=s.suppoid WHERE s.cmpcode = '" & cmpcode & "' AND Trfwhservicetype IN ('EXTERNAL') AND s.trfwhserviceoid = " & OidTw & " AND s.frombranch='" & fCabang & "' UNION ALL Select trfwhserviceoid,trfwhserviceNo,trfwhservicedate, trfwhservicenote,s.createtime, s.createuser, s.updtime,s.upduser, trfwhservicerefno, trfwhservicestatus, frombranch, tobranch, frommtrlocoid,ToMtrLocOId suppoid,gc.gendesc suppname,Trfwhservicetype,ToBranch From ql_trfwhservicemst s Inner Join QL_mstgen gc ON gc.gencode=s.ToBranch AND gc.gengroup='CABANG' Inner Join QL_mstgen gd ON gd.genoid=s.ToMtrLocOId AND gd.gengroup='LOCATION' WHERE s.cmpcode='" & cmpcode & "' AND Trfwhservicetype IN ('INTERNSUPP') AND s.trfwhserviceoid = " & OidTw & " AND s.frombranch='" & fCabang & "'"
            xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader

            If xreader.HasRows Then
                While xreader.Read
                    transferno.Text = xreader("trfwhserviceNo")
                    transdate = xreader("trfwhservicedate")
                    transferdate.Text = Format(xreader("trfwhservicedate"), "dd/MM/yyyy")
                    FromBranch.SelectedValue = xreader("fromBranch").ToString
                    TypeService.SelectedValue = xreader("Trfwhservicetype").ToString
                    Session("OidGudang") = xreader("frommtrlocoid")

                    If TypeService.SelectedValue = "EXTERNAL" Then
                        SuppOid.Text = xreader("suppoid")
                        SuppName.Text = xreader("suppname").ToString
                    Else
                        InitDDLBranchTujuan()
                        DDLTujuan.SelectedValue = xreader("ToBranch").ToString
                        InitDDLLocTo()
                        DDLTujuanGudang.SelectedValue = xreader("suppoid")
                        SuppOid.Text = 0 : SuppName.Text = ""
                    End If

                    note.Text = xreader("trfwhserviceNote").ToString
                    trnstatus.Text = xreader("trfwhservicestatus").ToString.Trim
                    upduser.Text = xreader("upduser").ToString
                    noref.Text = xreader("trfwhservicerefno").ToString
                    updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
                    createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss.fff")
                End While
            Else
                showMessage("Maaf, ini adalah error aplikasi, segera hubungi admin atau programmer..!!", 2)
            End If
            xreader.Close() : conn.Close()
            TypeService_SelectedIndexChanged(Nothing, Nothing)

            InitDDLLocation()
            fromlocation.SelectedValue = Session("OidGudang")
            sSql = "Select trfwhservicedtlseq seq, r.reqcode, r.reqoid, twd.reqdtloid, i.itemdesc, twd.trfwhservicedtlnote reqdtljob, rd.reqdtljob jobnya, twd.trfwhserviceqty reqqty, twd.itemoid, reqqty qty, snno From ql_trfwhservicedtl twd INNER JOIN QL_TRNREQUEST r on r.reqoid = twd.reqoid INNER JOIN QL_TRNREQUESTDTL rd ON rd.reqmstoid=r.reqoid AND rd.reqdtloid=twd.reqdtloid INNER JOIN ql_mstitem i ON i.itemoid=twd.itemoid WHERE twd.cmpcode = '" & cmpcode & "' and trfwhserviceoid = " & OidTw & " Group by trfwhservicedtlseq, r.reqcode, r.reqoid, twd.reqdtloid, i.itemdesc, rd.reqdtljob, twd.trfwhserviceqty, twd.itemoid, reqqty, snno, twd.trfwhservicedtlnote Order by trfwhservicedtlseq"
            Dim dtabs As DataTable = ckon.ambiltabel(sSql, "detailts")
            GVItemDetail.DataSource = dtabs
            GVItemDetail.DataBind() : Session("itemdetail") = dtabs

            If trnstatus.Text = "In Process" Then
                If TypeService.SelectedValue = "EXTERNAL" Then
                    btnPosting.Visible = True
                    BtnSendApp.Visible = False
                Else
                    btnPosting.Visible = False
                    BtnSendApp.Visible = True
                End If
                btnSave.Visible = True
                btnDelete.Visible = True : BtnCancel.Visible = True
                FromBranch.Enabled = False : fromlocation.Enabled = False
                FromBranch.CssClass = "inpTextDisabled" : fromlocation.CssClass = "inpTextDisabled"
            ElseIf trnstatus.Text = "IN APPROVAL" Or trnstatus.Text = "POST" Then
                BtnSendApp.Visible = False : btnDelete.Visible = False
                btnSave.Visible = False : btnDelete.Visible = False
                BtnCancel.Visible = True : btnPosting.Visible = False
                FromBranch.Enabled = False : fromlocation.Enabled = False
                FromBranch.CssClass = "inpTextDisabled" : fromlocation.CssClass = "inpTextDisabled"
            ElseIf trnstatus.Text = "Approved" Or trnstatus.Text = "Rejected" Then
                BtnSendApp.Visible = False : btnPosting.Visible = False
                btnSave.Visible = False : FromBranch.Enabled = False : fromlocation.Enabled = False
                FromBranch.CssClass = "inpTextDisabled" : fromlocation.CssClass = "inpTextDisabled"
                btnDelete.Visible = False
            End If
        Catch ex As Exception
            xreader.Close() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindData(ByVal sWhere As String)
        Try
            If CBTanggal.Checked = True Then
                Dim date1, date2 As New Date
                date1 = Date.ParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing)
                date2 = Date.ParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing)
                sWhere &= " AND CONVERT(DATE, s.trfwhservicedate, 101) between '" & date1 & "' AND '" & date2 & "'"
            End If

            If ddlStatus.SelectedValue.ToUpper <> "ALL" Then
                sWhere &= " AND s.Trfwhservicestatus='" & ddlStatus.SelectedValue.ToUpper & "'"
            End If

            If drCabang.SelectedValue <> "SEMUA BRANCH" Then
                sWhere &= " AND s.FromBranch='" & drCabang.SelectedValue & "'"
            End If

            sSql = "Select * From (" & _
            "Select trfwhserviceoid, trfwhserviceNo, trfwhservicedate, (SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=s.fromBranch AND gengroup='CABANG') FromBranch, sp.suppname, sp.suppoid, trfwhservicestatus, s.fromBranch branch_code, Trfwhservicetype From ql_trfwhservicemst s Inner Join QL_mstsupp sp ON sp.suppoid=s.suppoid WHERE s.cmpcode='" & cmpcode & "' AND Trfwhservicetype IN ('EXTERNAL') AND " & DDLFilter.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%' " & sWhere & " UNION ALL Select trfwhserviceoid, trfwhserviceNo, trfwhservicedate, (SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=s.fromBranch AND gengroup='CABANG') fromBranch, gc.gendesc suppname, ToMtrLocOId suppoid, trfwhservicestatus, s.fromBranch branch_code, Trfwhservicetype From ql_trfwhservicemst s Inner Join QL_mstgen gc ON gc.gencode=s.ToBranch AND gc.gengroup='CABANG' Inner Join QL_mstgen gd ON gd.genoid=s.ToMtrLocOId AND gd.gengroup='LOCATION' WHERE s.cmpcode='" & cmpcode & "' AND Trfwhservicetype IN ('INTERNSUPP') AND " & DDLFilter.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%'" & sWhere & ") Tws "
            If TypeDDL.SelectedValue <> "ALL" Then
                sSql &= " Where Trfwhservicetype='" & TypeDDL.SelectedValue & "'"
            End If

            sSql &= " Order By trfwhservicedate DESC"
            Dim dtab As DataTable = ckon.ambiltabel(sSql, "ts")
            gvMaster.DataSource = dtab
            gvMaster.DataBind()
            Session("ts") = dtab
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindSuppData()
        sSql = "Select sup.suppoid,sup.suppcode,sup.suppname,sup.suppaddr from QL_mstsupp sup Where cmpcode='" & cmpcode & "' AND (sup.suppcode LIKE '%" & Tchar(SuppName.Text) & "%' OR suppname LIKE '%" & Tchar(SuppName.Text) & "%')"
        Dim dts As DataTable = ckon.ambiltabel(sSql, "Ql_supplier")
        GVSupp.DataSource = dts : GVSupp.DataBind()
        GVSupp.SelectedIndex = -1 : GVSupp.Visible = True
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "reqdtloid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("QtyOs") = ToDouble(GetTextValue(row.Cells(5).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If

        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "reqdtloid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("qtyNya") = ToDouble(GetTextValue(row.Cells(5).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                            dv(0)("qtyNya") = ToDouble(GetTextValue(row.Cells(5).Controls))
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub BindItem(ByVal OidReq As String)
        Try
            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            sSql = "Select COUNT(conrefoid) from QL_conmtr Where conrefoid IN (Select re.reqdtloid From QL_TRNREQUESTDTL re WHere re.reqmstoid=" & Integer.Parse(reqoid.Text) & ")"

            If ToDouble(GetScalar(sSql)) > 0 Then
                sSql = "Select 'False' AS checkvalue, i.itemoid, i.itemcode, i.itemdesc, rq.reqdtljob, rq.reqqty, rq.reqdtloid, rq.reqmstoid reqoid, (Select rqm.reqcode from QL_TRNREQUEST rqm Where rqm.reqoid=rq.reqmstoid AND rq.branch_code=rqm.branch_code) reqcode, rq.snno, ISNULL(SUM(con.qtyIn)-SUM(qtyOut),0.00) QtyNya, ISNULL(SUM(con.qtyIn)-SUM(qtyOut),0.00) QtyOs, ISNULL(SUM(con.qtyIn)-SUM(qtyOut),0.00) OsQtyNya From QL_TRNREQUESTDTL rq Inner Join QL_conmtr con ON con.conrefoid=rq.reqdtloid Inner Join QL_mstitem i ON i.itemoid=con.refoid Where con.cmpcode='" & cmpcode & "' AND con.branch_code='" & FromBranch.SelectedValue & "' " & OidReq & " AND con.mtrlocoid=" & fromlocation.SelectedValue & " Group By rq.reqmstoid, rq.reqdtloid, i.itemoid, i.itemcode, i.itemdesc, rq.reqdtljob, rq.reqqty, rq.branch_code, rq.snno Having Isnull(SUM(con.qtyIn)-SUM(qtyOut),0.00)>0.00"
            Else
                sSql = "Select * from (Select 'False' AS checkvalue, i.itemoid, i.itemcode, i.itemdesc, r.reqdtljob, r.reqqty, r.reqdtloid, r.reqmstoid reqoid, rq.reqcode, r.snno, ISNULL((Select twd.trfwhserviceqty from ql_trfwhservicedtl twd INNER JOIN ql_trfwhservicemst twm ON twm.Trfwhserviceoid=twd.trfwhserviceoid Where twd.reqoid=rq.reqoid AND twd.reqdtloid=r.reqdtloid AND r.itemoid=twd.itemoid AND twm.Trfwhservicestatus='APPROVED' AND twm.Trfwhservicetype='INTERNAL'),0.00) qtyNya, r.reqqty-ISNULL((Select twd.trfwhserviceqty from ql_trfwhservicedtl twd INNER JOIN ql_trfwhservicemst twm ON twm.Trfwhserviceoid=twd.trfwhserviceoid Where twd.reqoid=rq.reqoid AND twd.reqdtloid=r.reqdtloid AND r.itemoid=twd.itemoid AND twm.Trfwhservicetype='INTERNAL'),0.00) QtyOs, r.reqqty-ISNULL((Select twd.trfwhserviceqty from ql_trfwhservicedtl twd INNER JOIN ql_trfwhservicemst twm ON twm.Trfwhserviceoid=twd.trfwhserviceoid Where twd.reqoid=rq.reqoid AND twd.reqdtloid=r.reqdtloid AND r.itemoid=twd.itemoid AND twm.Trfwhservicetype='INTERNAL'),0.00) OsQtyNya from ql_mstitem i inner join QL_TRNREQUESTDTL r on r.itemoid = i.itemoid Inner Join QL_TRNREQUEST rq ON rq.reqoid=r.reqmstoid AND rq.branch_code=r.branch_code Where r.branch_code='" & FromBranch.SelectedValue & "' " & OidReq & _
                " UNION ALL " & _
                "Select 'False' AS checkvalue,i.itemoid, i.itemcode, i.itemdesc, r.reqdtljob, r.reqqty, r.reqdtloid, r.reqmstoid reqoid, rq.reqcode, r.snno, ISNULL((Select twd.trfwhserviceqty from ql_trfwhservicedtl twd INNER JOIN ql_trfwhservicemst twm ON twm.Trfwhserviceoid=twd.trfwhserviceoid Where(twd.reqoid = rq.reqoid And twd.reqdtloid = r.reqdtloid And r.itemoid = twd.itemoid) AND twm.Trfwhservicetype='EXTERNAL'),0.00) qtyNya, ISNULL((SELECT SUM(tcd.trntcservicedtlqty) FROM ql_trntcservicedtl tcd WHERE cmpcode = '" & cmpcode & "' AND tcd.tomtrlocoid = " & fromlocation.SelectedValue & " AND tcd.refoid = r.itemoid AND tcd.reqoid=rq.reqoid),0.00)-ISNULL((Select twd.trfwhserviceqty from ql_trfwhservicedtl twd INNER JOIN ql_trfwhservicemst twm ON twm.Trfwhserviceoid=twd.trfwhserviceoid Where(twd.reqoid = rq.reqoid And twd.reqdtloid = r.reqdtloid And r.itemoid = twd.itemoid) AND twm.Trfwhservicetype='EXTERNAL'),0.00) QtyOs, ISNULL((SELECT SUM(tcd.trntcservicedtlqty) FROM ql_trntcservicedtl tcd WHERE cmpcode = '" & cmpcode & "' AND tcd.tomtrlocoid = " & fromlocation.SelectedValue & " AND tcd.refoid = r.itemoid AND tcd.reqoid=rq.reqoid),0.00)-ISNULL((Select twd.trfwhserviceqty from ql_trfwhservicedtl twd INNER JOIN ql_trfwhservicemst twm ON twm.Trfwhserviceoid=twd.trfwhserviceoid Where (twd.reqoid = rq.reqoid And twd.reqdtloid = r.reqdtloid And r.itemoid = twd.itemoid) AND twm.Trfwhservicetype='EXTERNAL'),0.00) OsQtyNya From ql_mstitem i inner join QL_TRNREQUESTDTL r on r.itemoid = i.itemoid Inner Join QL_TRNREQUEST rq ON rq.reqoid=r.reqmstoid AND rq.branch_code=r.branch_code Where r.cmpcode='" & cmpcode & "' " & OidReq & ") req Where QtyOs>0.00"
            End If
            Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstmat")
            If dt.Rows.Count > 0 Then
                Session("TblListMat") = dt : Session("itemlistwhretur") = dt
                Session("TblListMatView") = Session("TblListMat")
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind() : GVItemList.SelectedIndex = -1
                GVItemList.Visible = True
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindPenerimaan(ByVal sWhere As String)
        Try
            sSql = "Select * from (Select rq.reqoid, rq.reqcode, cu.custoid, cu.custname, con.mtrlocoid, Convert(varchar(10),rq.reqdate, 103) reqdate, SUM(con.qtyIn)-SUM(con.qtyOut) QtyTw, con.branch_code From QL_TRNREQUEST rq Inner Join QL_TRNREQUESTDTL rqd ON rqd.reqmstoid=rq.reqoid Inner Join QL_mstcust cu ON cu.custoid=rq.reqcustoid Inner Join (SELECT con.branch_code, con.mtrlocoid, qtyIn, qtyOut, conrefoid FROM QL_conmtr con) con ON con.conrefoid=reqdtloid Where rq.reqstatus IN ('POST','APPROVED') AND rq.reqservicecat IN ('N','INTERNAL') Group BY rq.reqoid, rq.reqcode, cu.custoid, reqservicecat, cu.custname, con.mtrlocoid, rq.reqdate,con.branch_code Having Isnull(SUM(qtyIn),0.00)-Isnull(Sum(qtyOut),0.00)>0.00) req Where branch_code='" & FromBranch.SelectedValue & "' AND req.mtrlocoid=" & fromlocation.SelectedValue & " " & sWhere & " Order by req.reqcode"
            Dim objTable As DataTable = ckon.ambiltabel(sSql, "penerimaan")
            gvPenerimaan.DataSource = objTable : gvPenerimaan.DataBind()
            gvPenerimaan.SelectedIndex = -1 : gvPenerimaan.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FromBranch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FromBranch, sSql)
            Else
                FillDDL(FromBranch, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(FromBranch, sSql)
        End If
    End Sub

    Private Sub InitDDLBranchTujuan()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang' AND gencode<>'" & FromBranch.SelectedValue & "'"
        FillDDL(DDLTujuan, sSql)
    End Sub

    Private Sub fInitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(drCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(drCabang, sSql)
            Else
                FillDDL(drCabang, sSql)
                drCabang.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
                drCabang.SelectedValue = "SEMUA BRANCH"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(drCabang, sSql)
            drCabang.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            drCabang.SelectedValue = "SEMUA BRANCH"
        End If
    End Sub

    Private Sub InitDDLLocation()
        Try
            sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & FromBranch.SelectedValue & "' and gengroup = 'cabang') "
            If TypeService.SelectedValue = "INTERNSUPP" Then
                sSql &= "And genother5 IN ('RUSAK')"
            Else
                sSql &= "And genother5 IN ('TITIPAN','RUSAK')"
            End If
            FillDDL(fromlocation, sSql)

            If fromlocation.Items.Count = 0 Then
                showMessage("- Maaf, Gudang asal belum Disetting..!!", 3)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub InitDDLLocTo()
        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & DDLTujuan.SelectedValue & "' and gengroup = 'cabang') AND a.genoid <> '" & fromlocation.SelectedValue & "' and genother5 IN ('RUSAK')"
        FillDDL(DDLTujuanGudang, sSql)

        If DDLTujuanGudang.Items.Count = 0 Then
            showMessage("- Maaf, Gudang Tujuan belum Disetting..!!", 3)
            Exit Sub
        End If
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\trnTWServiceSup.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Post this data?')")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Transfer Service Supplier"

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
            fInitDDLBranch() : BindData("")
            transferno.Text = GenerateID("ql_trfwhservicemst", cmpcode)
            transferdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitDDLBranch() : InitDDLLocation()
            InitDDLBranchTujuan() : InitDDLLocTo()
            createuser.Text = Session("UserID")
            createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            TypeService_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox(Session("branch_code"), Session("oid"))
            Else
                tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
                tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 0
                GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                labelseq.Text = "1" : sequence.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        CBTanggal.Checked = False
        drCabang.SelectedValue = "SEMUA BRANCH"
        ddlStatus.SelectedValue = "ALL"
        tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
        tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        FilterText.Text = "" : BindData("")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Tanggal period harus diisi !", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Tanggal period 1 tidak valid !", 2)
            Exit Sub
        End If

        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Tanggal period 2 tidak valid !", 2)
            Exit Sub
        End If

        If date1 > date2 Then
            showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
            Exit Sub
        End If
        BindData("")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If sValidasi.Text <> "" Then
            UpdateCheckedMat()
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.Visible = True
            sValidasi.Text = ""
            mpeItem.Show()
        End If
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Try
            If reqoid.Text = "" Then
                showMessage("Pilih No. Penerimaan terlebih dahulu!", 2)
            Else
                I_u2.Text = "new"
                Session("TblListMat") = Nothing
                Session("TblListMatView") = Nothing
                GVItemList.DataSource = Session("TblListMat")
                Dim OidReq As String = " AND rq.reqmstoid = '" & reqoid.Text & "'"
                GVItemList.DataBind() : BindItem(OidReq)
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            End If
            GVItemList.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = "" : labelitemoid.Text = ""
        notedtl.Text = "" : qty.Text = ""
        reqdtloid.Text = "" : reqoid.Text = ""
        txtPenerimaan.Text = ""
        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        UpdateCheckedMat()
        GVItemList.PageIndex = e.NewPageIndex
        GVItemList.DataSource = Session("TblListMatView")
        GVItemList.DataBind() : GVItemList.Visible = True
        mpeItem.Show()
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        btnHideItem.Visible = False : panelItem.Visible = False
        mpeItem.Show()
        item.Text = GVItemList.SelectedDataKey("itemdesc").ToString
        labelitemoid.Text = GVItemList.SelectedDataKey("itemoid")
        notedtl.Text = GVItemList.SelectedDataKey("reqdtljob")
        qty.Text = GVItemList.SelectedDataKey("reqqty")
        reqqty.Text = GVItemList.SelectedDataKey("reqqty")
        reqdtloid.Text = GVItemList.SelectedDataKey("reqdtloid")
        CProc.DisposeGridView(sender)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnTWServiceSup.aspx?awal=true")
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        Try
            UpdateCheckedMat()
            If Session("TblListMat") IsNot Nothing Then
                Dim dt As DataTable = Session("TblListMat")

                If dt.Rows.Count > 0 Then
                    Dim dv As DataView = dt.DefaultView
                    dv.RowFilter = "checkvalue='True'"
                    If dv.Count > 0 Then
                        Dim sErr As String = ""
                        For C1 As Integer = 0 To dv.Count - 1
                            If ToDouble(dv(C1)("qtyNya")) <= 0 Or ToDouble(dv(C1)("qtyNya")) = 0 Then
                                sValidasi.Text &= "- Maaf," & dv(C1)("itemdesc") & " belum di input qty received..!!<br />"
                            End If
                            If ToDouble(dv(C1)("qtyNya")) > ToDouble(dv(C1)("OsQtyNya")) Then
                                sValidasi.Text &= "- Maaf," & dv(C1)("itemdesc") & " belum di input qty received melebihi qty penerimaan..!!<br />"
                            End If
                        Next

                        If sValidasi.Text <> "" Then
                            showMessage(sValidasi.Text, 2)
                            Exit Sub
                        End If

                        Dim dtab As DataTable
                        If Session("itemdetail") Is Nothing Then
                            dtab = setTabelDetail() : Session("itemdetail") = dtab
                            labelseq.Text = 0 + 1
                        Else
                            dtab = Session("itemdetail")
                            labelseq.Text = dtab.Rows.Count + 1
                        End If

                        Dim dtView As DataView = dtab.DefaultView
                        For C1 As Integer = 0 To dv.Count - 1
                            dtView.RowFilter = "itemoid=" & Integer.Parse(dv(C1)("itemoid")) & " And reqdtloid=" & Integer.Parse(dv(C1)("reqdtloid")) & ""
                            Dim drow As DataRow : Dim drowedit() As DataRow
                            If dtView.Count <= 0 Then
                                drow = dtab.NewRow
                                drow("seq") = labelseq.Text
                                drow("reqoid") = Integer.Parse(dv(C1)("reqoid"))
                                drow("itemoid") = Integer.Parse(dv(C1)("itemoid"))
                                drow("itemdesc") = dv(C1)("itemdesc").ToString
                                drow("reqqty") = ToDouble(dv(C1)("qtyNya"))
                                drow("qty") = ToDouble(dv(C1)("QtyOs"))
                                drow("reqdtljob") = dv(C1)("reqdtljob").ToString
                                drow("reqdtloid") = Integer.Parse(dv(C1)("reqdtloid"))
                                drow("reqcode") = dv(C1)("reqcode").ToString
                                drow("snno") = dv(C1)("snno").ToString
                                dtab.Rows.Add(drow) : dtab.AcceptChanges()
                            Else
                                drowedit = dtab.Select("itemoid = " & Integer.Parse(dv(C1)("itemoid")) & "", "")
                                drow = drowedit(0)
                                drowedit(0).BeginEdit()
                                drow("seq") = labelseq.Text
                                drow("reqoid") = Integer.Parse(dv(C1)("reqoid"))
                                drow("itemoid") = Integer.Parse(dv(C1)("itemoid"))
                                drow("itemdesc") = dv(C1)("itemdesc").ToString
                                drow("reqqty") = ToDouble(dv(C1)("qtyNya"))
                                drow("qty") = ToDouble(dv(C1)("QtyOs"))
                                drow("reqdtljob") = dv(C1)("reqdtljob").ToString
                                drow("reqdtloid") = Integer.Parse(dv(C1)("reqdtloid"))
                                drow("reqcode") = dv(C1)("reqcode").ToString
                                drow("snno") = dv(C1)("snno").ToString
                                drowedit(0).EndEdit()
                                dtab.Select(Nothing, Nothing)
                                dtab.AcceptChanges()
                            End If
                            drow.EndEdit()
                            labelseq.Text += 1
                            dtView.RowFilter = ""
                        Next

                        btnHideItem.Visible = False : panelItem.Visible = False
                        mpeItem.Show()
                        GVItemDetail.DataSource = dtab
                        GVItemDetail.DataBind()
                        Session("itemdetail") = dtab
                        ClearDtl()
                    Else
                        sValidasi.Text &= "- Maaf, Anda belum memilih katalog sama sekali, pilih dan isi jumlah Qty nya...!!<br />"
                        If sValidasi.Text <> "" Then
                            showMessage(sValidasi.Text, 2)
                            Exit Sub
                        End If
                    End If
                End If
            Else
                showMessage("Maaf, Silahkan klik add to list dulu..!!", 2)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Try
            Dim sErr As String = ""
            If item.Text.Trim = "" Or labelitemoid.Text = "" Then
                sErr &= "- Maaf, Item tidak boleh kosong..!!<br />"
            End If

            If qty.Text = "" Then : qty.Text = 0
                If ToDouble(qty.Text) = 0 Then
                    sErr &= "- Maaf, Qty tidak boleh nol..!!<br />"
                End If
            End If

            If ToDouble(qty.Text) > ToDouble(reqqty.Text) Then
                sErr &= "- Maaf, Qty transfer tidak boleh melebihi Qty Penerimaan barang..!!<br />"
            End If

            If sErr <> "" Then
                showMessage(sErr, 2)
                Exit Sub
            End If

            Dim dtab As DataTable = Session("itemdetail")
            If I_u2.Text = "new" Then
                If Session("itemdetail") Is Nothing Then
                    setTabelDetail()
                    labelseq.Text = "1"
                Else
                    dtab = Session("itemdetail")
                    labelseq.Text = (dtab.Rows.Count + 1).ToString
                End If
            Else
                dtab = Session("itemdetail")
            End If

            Dim objTable As DataTable
            objTable = Session("itemdetail")
            Dim dv As DataView = objTable.DefaultView
            If I_u2.Text = "new" Then
                dv.RowFilter = "reqoid = " & reqoid.Text & " AND reqdtloid=" & reqdtloid.Text
            Else
                dv.RowFilter = "itemoid = " & Integer.Parse(labelitemoid.Text) & " AND reqoid = " & reqoid.Text & " AND seq<>" & Integer.Parse(labelseq.Text) & " AND reqdtloid=" & reqdtloid.Text
            End If

            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Maaf, Item ini sudah ada di dalam list !", 2)
                Exit Sub
            End If

            dv.RowFilter = ""
            Dim drow As DataRow
            Dim drowedit() As DataRow
            If I_u2.Text = "new" Then
                drow = dtab.NewRow
                drow("seq") = Integer.Parse(labelseq.Text)
                drow("reqoid") = Integer.Parse(reqoid.Text)
                drow("itemoid") = Integer.Parse(labelitemoid.Text)
                drow("itemdesc") = item.Text.Trim
                drow("reqqty") = ToDouble(qty.Text)
                drow("qty") = ToDouble(reqqty.Text)
                drow("reqdtljob") = notedtl.Text
                drow("reqdtloid") = reqdtloid.Text
                drow("reqcode") = txtPenerimaan.Text
                dtab.Rows.Add(drow)
                dtab.AcceptChanges()
            Else
                drowedit = dtab.Select("reqdtloid=" & Integer.Parse(reqdtloid.Text) & "", "")
                drow = drowedit(0)
                drowedit(0).BeginEdit()
                drow("reqoid") = Integer.Parse(reqoid.Text)
                drow("itemoid") = Integer.Parse(labelitemoid.Text)
                drow("itemdesc") = item.Text.Trim
                drow("reqqty") = ToDouble(qty.Text)
                drow("qty") = ToDouble(reqqty.Text)
                drow("reqdtljob") = notedtl.Text
                drow("reqdtloid") = reqdtloid.Text
                drow("reqcode") = txtPenerimaan.Text
                drowedit(0).EndEdit()
                dtab.Select(Nothing, Nothing)
                dtab.AcceptChanges()
            End If

            GVItemDetail.DataSource = dtab
            GVItemDetail.DataBind()
            Session("itemdetail") = dtab
            ClearDtl()
            labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
            GVItemDetail.SelectedIndex = -1
            GVItemDetail.Columns(8).Visible = True

            If GVItemList.Visible = True Then
                GVItemList.DataSource = Nothing
                GVItemList.DataBind()
                GVItemList.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click 
        I_u2.Text = "new" : labelitemoid.Text = ""
        item.Text = "" : notedtl.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(8).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            I_u2.Text = "new"
            Dim lbutton As System.Web.UI.WebControls.LinkButton
            Dim gvr As GridViewRow

            lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            gvr = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim dtab As DataTable
            If Not Session("itemdetail") Is Nothing Then
                dtab = Session("itemdetail")
            Else
                showMessage("Missing detail list session !", 2)
                Exit Sub
            End If
            Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
            drow(0).Delete()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
            Dim dtrow As DataRow
            For i As Integer = 0 To dtab.Rows.Count - 1
                dtrow = dtab.Rows(i)
                dtrow.BeginEdit()
                dtrow("seq") = i + 1
                dtrow.EndEdit()
            Next
            dtab.AcceptChanges()
            GVItemDetail.DataSource = dtab
            GVItemDetail.DataBind()
            Session("itemdetail") = dtab
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        Try
            I_u2.Text = "edit"
            labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text
            reqoid.Text = GVItemDetail.SelectedDataKey("reqoid")
            reqdtloid.Text = GVItemDetail.SelectedDataKey("reqdtloid")
            labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
            item.Text = GVItemDetail.SelectedDataKey("itemdesc")
            qty.Text = GVItemDetail.SelectedDataKey("reqqty")
            snno.Text = GVItemDetail.SelectedDataKey("snno")
            sSql = "Select rd.reqqty from QL_TRNREQUESTDTL rd Inner Join QL_TRNREQUEST rm ON rd.reqmstoid=rm.reqoid AND rd.branch_code=rm.branch_code Where rm.reqoid=" & Integer.Parse(GVItemDetail.SelectedDataKey("reqoid")) & " AND rd.reqdtloid=" & Integer.Parse(GVItemDetail.SelectedDataKey("reqdtloid")) & ""
            reqqty.Text = GVItemDetail.SelectedDataKey("qty")
            txtPenerimaan.Text = GVItemDetail.SelectedDataKey("reqcode")
            notedtl.Text = GVItemDetail.SelectedDataKey("reqdtljob").Replace("&nbsp;", "")

            If GVItemList.Visible = True Then
                GVItemList.DataSource = Nothing
                GVItemList.DataBind()
                GVItemList.Visible = False
            End If
            GVItemDetail.Columns(8).Visible = False
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            'Dim transdate As Date = Format(GetServerTime(), "dd/MM/yyyy")
            Dim period As String = GetDateToPeriodAcctg(GetServerTime())
            Dim ErrMsg As String = "" : Dim TwSNo As String = ""

            If noref.Text.Length > 20 Then
                ErrMsg &= "- No. ref Terlalu Panjang..!!<br />"
            End If

            If fromlocation.Items.Count <= 0 Then
                ErrMsg &= "- Maaf, Lokasi asal transfer tidak boleh kosong..!!<br />"
            End If

            If GVItemDetail.Rows.Count <= 0 Then
                ErrMsg &= "- Maaf, Detail Item tidak boleh kosong..!!<br />"
            End If

            'Cek saldoakhir
            If Not Session("itemdetail") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("itemdetail")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "SELECT isnull(SUM(qtyin)-SUM(qtyout),0.00) FROM QL_conmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND refoid = " & dtab.Rows(j).Item("itemoid") & " And branch_code = '" & FromBranch.SelectedValue & "'"
                    saldoakhire = GetScalar(sSql)
                    'If saldoakhire = Nothing Or saldoakhire = 0 Then
                    If ToDouble(dtab.Rows(j).Item("reqqty")) > ToDouble(saldoakhire) Then
                        ErrMsg &= "- Maaf, Transfer barang " & dtab.Rows(j).Item("itemdesc") & " tidak bisa dilakukan untuk barang karena saldo akhir tidak memenuhi..!!<br />"
                    End If
                    'End If
                Next
            End If

            If trnstatus.Text = "IN APPROVAL" Then
                sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus From QL_approvalstructure WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trfwhservicemst' And branch_code LIKE '%" & DDLTujuan.SelectedValue & "%' order by approvallevel"
                Dim dtData2 As DataTable = ckon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                Else
                    ErrMsg &= "- Maaf, User untuk Cabang " & DDLTujuan.SelectedItem.Text & " belum disetting, Silahkan hubungi admin..!!<br>"
                End If

                If checkApproval("ql_trfwhservicemst", "IN APPROVAL", Session("oid"), "New", "FINAL", DDLTujuan.SelectedValue) > 0 Then
                    ErrMsg &= "- Maaf, data ini sudah send Approval..!!<br>"
                    trnstatus.Text = "IN APPROVAL"
                End If

            ElseIf trnstatus.Text = "Rejected" Then
                trnstatus.Text = "IN PROCESS"
            End If

            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
            If GetStrData(sSql) = 0 Then
                ErrMsg &= "- Maaf, Tanggal ini tidak dalam periode Open Stock..!!<br />"
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                If ToDouble(GetStrData("SELECT COUNT(*) FROM ql_trfwhservicemst WHERE createtime='" & createtime.Text & "'")) > 0 Then
                    ErrMsg &= "- Maaf, Data sudah tersimpan, Silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!<BR />"
                End If
            Else

                sSql = "SELECT trfwhservicestatus FROM ql_trfwhservicemst WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                Dim srest As String = GetStrData(sSql)
                If srest Is Nothing Or srest = "" Then
                    ErrMsg &= "- Maaf, Data transfer service tidak ditemukan..!!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
                Else
                    If srest.ToLower = "post" Then
                        ErrMsg &= "- Maaf, Data sudah terinput, klik tombol cancel..!!<br />Periksa bila data telah dalam status 'Post'..!!<br>"
                    End If
                End If
            End If

            Dim ConMtrOid, trfmtrmstoid, trfmtrdtloid, iCurID, AppOid As Int32
            ConMtrOid = GenerateID("QL_conmtr", cmpcode)
            trfmtrdtloid = GenerateID("ql_trfwhservicedtl", cmpcode)
            AppOid = GenerateID("QL_Approval", cmpcode)

            If i_u.Text = "new" Then
                trfmtrmstoid = GenerateID("ql_trfwhservicemst", cmpcode)
                transferno.Text = GenerateID("ql_trfwhservicemst", cmpcode)
            Else
                trfmtrmstoid = Integer.Parse(Session("oid"))
                transferno.Text = Integer.Parse(Session("oid"))
            End If

            'Generate transfer number
            If trnstatus.Text = "POST" Then
                Dim Cabang As String = GetStrData("Select genother1 From ql_mstgen Where gencode='" & FromBranch.SelectedValue & "' And gengroup='CABANG'")
                TwSNo = "TSS/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trfwhserviceno,4) AS INT)),0) FROM QL_trfwhservicemst WHERE trfwhserviceno LIKE '" & TwSNo & "%'" : iCurID = GetScalar(sSql) + 1
                transferno.Text = GenNumberString(TwSNo, "", iCurID, 4)
            End If

            If ErrMsg.Trim <> "" Then
                trnstatus.Text = "In Process"
                showMessage(ErrMsg, 2)
                Exit Sub
            End If

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim objTrans As SqlClient.SqlTransaction
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try
                'Insert new record
                If i_u.Text = "new" Then
                    'Insert to master
                    sSql = "INSERT INTO ql_trfwhservicemst (cmpcode, Trfwhserviceoid, TrfwhserviceNo, Trfwhservicedate, Trfwhservicenote, createtime, createuser, updtime, upduser, finalappovaldatetime, finalapprovaluser, finalapprovalcode, canceluser, canceltime, cancelnote, Trfwhserviceres1, Trfwhserviceres2, Trfwhservicestatus, Trfwhservicerefno, Frombranch, ToBranch, FromMtrLocOid, ToMtrLocOid, Trfwhservicetype, suppoid) VALUES " & _
                    " ('" & cmpcode & "', " & Integer.Parse(trfmtrmstoid) & ", '" & transferno.Text & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Tchar(note.Text.Trim) & "', '" & createtime.Text & "', '" & createuser.Text & "', CURRENT_TIMESTAMP, '" & createuser.Text & "', CURRENT_TIMESTAMP, '', '', '', '', '', '', '', '" & trnstatus.Text & "', '" & Tchar(noref.Text) & "', '" & FromBranch.SelectedValue & "','" & DDLTujuan.SelectedValue & "', '" & fromlocation.SelectedValue & "', '" & DDLTujuanGudang.SelectedValue & "', '" & TypeService.SelectedValue & "', " & SuppOid.Text & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'Update lastoid QL_trntrfmtrmst
                    sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(trfmtrmstoid) & " WHERE tablename = 'ql_trfwhservicemst' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else

                    'Update master record
                    sSql = "UPDATE ql_trfwhservicemst SET TrfwhserviceNo = '" & transferno.Text & "', TrfwhserviceNote = '" & Tchar(note.Text.Trim) & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP, finalapprovaluser = '" & Session("UserID") & "', Trfwhservicestatus = '" & trnstatus.Text & "', frombranch = '" & FromBranch.SelectedValue & "', Frommtrlocoid = '" & fromlocation.SelectedValue & "', trfwhservicerefno = '" & Tchar(noref.Text) & "', suppoid=" & SuppOid.Text & ", Trfwhservicetype='" & TypeService.SelectedValue & "', ToMtrLocOid='" & DDLTujuanGudang.SelectedValue & "', ToBranch='" & DDLTujuan.SelectedValue & "' WHERE Trfwhserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnrequestdtl SET reqdtlres1='' WHERE cmpcode='" & cmpcode & "' AND reqdtloid IN (SELECT reqdtloid FROM QL_trfwhservicedtl WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid=" & Integer.Parse(Session("oid")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnrequest SET reqres1='' WHERE cmpcode='" & cmpcode & "' AND reqoid IN (SELECT reqoid FROM QL_trfwhservicedtl WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid=" & Integer.Parse(Session("oid")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'Delete detail
                    Dim SmBrg As DataTable = Session("penerimaandetail")
                End If

                'Insert to detail
                If Not Session("itemdetail") Is Nothing Then
                    If i_u.Text <> "new" Then
                        sSql = "DELETE FROM ql_trfwhservicedtl WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    Dim objTable As DataTable = Session("itemdetail")
                    For i As Integer = 0 To objTable.Rows.Count - 1
                        sSql = " INSERT INTO ql_trfwhservicedtl (cmpcode, trfwhserviceoid, trfwhservicedtloid, trfwhservicedtlseq, reqoid, reqdtloid, itemoid, trfwhserviceqty, trfwhservicedtlnote, trfwhservicedtlres1) " & _
                       "VALUES ('" & cmpcode & "', " & Integer.Parse(trfmtrmstoid) & ", " & Integer.Parse(trfmtrdtloid) & ", " & objTable.Rows(i).Item("seq") & ", " & Integer.Parse(objTable.Rows(i).Item("reqoid")) & ", " & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & ", " & Integer.Parse(objTable.Rows(i).Item("itemoid")) & ", " & ToDouble(objTable.Rows(i).Item("reqqty")) & ", '" & Tchar(objTable.Rows(i).Item("reqdtljob")) & "', '')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Dim twdtloid As Integer = Integer.Parse(trfmtrmstoid)
                        trfmtrdtloid += 1

                        '--- Proses Stok Keluar ---
                        If trnstatus.Text = "POST" Then
                            Dim lasthpp As Double = 0, cQty As Double = 0, sTatusDtl As String = ""
                            sSql = "Select hpp from ql_mstitem Where itemoid=" & Integer.Parse(objTable.Rows(i).Item("itemoid")) & ""
                            xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar

                            sSql = "Select rd.reqqty-ISNULL((Select SUM(trfwhserviceqty) From ql_trfwhservicedtl twd Where twd.reqoid=rd.reqmstoid AND rd.itemoid=twd.itemoid AND twd.reqdtloid=rd.reqdtloid),0.00) From QL_TRNREQUESTDTL rd Where rd.reqmstoid=" & Integer.Parse(objTable.Rows(i).Item("reqoid")) & " AND rd.itemoid=" & Integer.Parse(objTable.Rows(i).Item("itemoid")) & " AND rd.reqdtloid=" & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & ""
                            xCmd.CommandText = sSql : cQty = xCmd.ExecuteScalar

                            If ToDouble(cQty) = 0.0 Then
                                sTatusDtl = "Complete"
                            Else
                                sTatusDtl = "In Complete"
                            End If

                            sSql = "Update QL_TRNREQUESTDTL set reqdtlres1 ='" & sTatusDtl & "' Where reqdtloid=" & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & " AND reqmstoid=" & Integer.Parse(objTable.Rows(i).Item("reqoid")) & " AND itemoid=" & Integer.Parse(objTable.Rows(i).Item("itemoid")) & ""
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code, conrefoid) VALUES " & _
                            "('" & cmpcode & "', " & Integer.Parse(ConMtrOid) & ", 'TSS', CURRENT_TIMESTAMP, '" & GetDateToPeriodAcctg(GetServerTime()) & "', '" & transferno.Text & "', " & Integer.Parse(twdtloid) & ", 'QL_trfwhservicemst', " & Integer.Parse(objTable.Rows(i).Item("itemoid")) & ", 'QL_MSTITEM', 945, " & fromlocation.SelectedValue & ", 0, " & ToDouble(objTable.Rows(i).Item("reqqty")) & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("reqdtljob")) & "', (Select hpp from ql_mstitem Where itemoid=" & Integer.Parse(objTable.Rows(i).Item("itemoid")) & "), '" & FromBranch.SelectedValue & "', " & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtrOid = ConMtrOid + 1
                        End If
                        '--- End Proses Stok Keluar ---
                    Next

                    If trnstatus.Text = "POST" Then
                        sSql = "UPDATE QL_mstoid SET lastoid =" & ConMtrOid - 1 & " WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    'Update lastoid ql_InTransferDtl
                    sSql = "UPDATE QL_mstoid SET lastoid =" & trfmtrdtloid - 1 & " WHERE tablename = 'ql_trfwhservicedtl' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If trnstatus.Text = "IN APPROVAL" Then
                    Session("AppOid") = GenerateID("QL_Approval", cmpcode)
                    If Session("oid") <> Nothing Or Session("oid") <> "" Then
                        If Not Session("TblApproval") Is Nothing Then
                            Dim obj As DataTable : obj = Session("TblApproval")
                            For c1 As Int16 = 0 To obj.Rows.Count - 1
                                sSql = "INSERT INTO QL_APPROVAL (cmpcode, approvaloid, branch_code, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus) VALUES" & _
                                " ('" & cmpcode & "', " & AppOid + c1 & ", '" & DDLTujuan.SelectedValue & "', '" & "TSI" & AppOid & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'ql_trfwhservicemst', '" & Session("oid") & "', 'IN APPROVAL', '0', '" & obj.Rows(c1).Item("approvaluser") & "', '1/1/1900', '" & obj.Rows(c1).Item("approvaltype") & "', '1', '" & obj.Rows(c1).Item("approvalstatus") & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            Next
                            sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + obj.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & cmpcode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    Else
                        trnstatus.Text = "In Process"
                        objTrans.Commit() : conn.Close()
                        showMessage("- Silahkan Input Data dulu", 2)
                        Exit Sub
                    End If
                End If

                objTrans.Commit() : conn.Close()
            Catch ex As Exception
                trnstatus.Text = "In Process"
                objTrans.Rollback() : conn.Close()
                showMessage(ex.ToString & ";<br />" & sSql, 1)
                Exit Sub
            End Try
            Response.Redirect("trnTWServiceSup.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim sMsg As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "UPDATE QL_TRNREQUESTDTL SET reqdtlres1 ='' WHERE cmpcode='" & cmpcode & "' AND reqdtloid IN (SELECT reqdtloid FROM QL_trfwhservicemst WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid =" & Integer.Parse(Session("oid")) & ")"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_TRNREQUEST SET reqres1='' WHERE cmpcode='" & cmpcode & "' AND reqoid IN (SELECT reqoid FROM QL_trfwhservicedtl WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid=" & Integer.Parse(Session("oid")) & ")"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "SELECT trfwhservicestatus FROM ql_trfwhservicemst WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                sMsg &= "Data Service tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain"
            Else
                If srest = "In Approval" Then
                    sMsg &= "Data Service tidak dapat dihapus !<br />Periksa bila data telah diposting oleh user lain"
                End If
            End If

            If sMsg <> "" Then
                objTrans.Rollback() : conn.Close()
                showMessage(sMsg, 2)
                Exit Sub
            End If

            sSql = "DELETE FROM ql_trfwhservicemst WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trfwhservicedtl WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()

        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("trnTWServiceSup.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        trnstatus.Text = "POST" : btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub fromlocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromlocation.SelectedIndexChanged
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        reqoid.Text = "" : sequence.Text = ""

        'item
        I_u2.Text = "new" : labelitemoid.Text = ""
        item.Text = "" : notedtl.Text = ""
        labelseq.Text = 1

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        If gvPenerimaan.Visible = True Then
            gvPenerimaan.DataSource = Nothing
            gvPenerimaan.DataBind()
            gvPenerimaan.Visible = False
        End If
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            'Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            'Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")
            Dim OidTwNya As Integer = sender.ToolTip
            Dim sWhere As String = " WHERE twm.Trfwhserviceoid = " & Integer.Parse(OidTwNya) & ""
            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "PrintOutTWS.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", Integer.Parse(OidTwNya))
            'report.PrintOptions.PaperSize = PaperSize.PaperA5

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TWSupp_" & OidTwNya & "")
            report.Close() : report.Dispose()
            Response.Redirect("trnTWServiceSup.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        gvMaster.PageIndex = e.NewPageIndex
        BindData("")
    End Sub

    Protected Sub imbFindPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPenerimaan.Click
        panelPenerimaan.Visible = True
        btnHidePenerimaan.Visible = True
        mpePenerimaan.Show()
        'Dim sWhere As String = " AND " & DDLFilterPenerimaan.SelectedValue & " LIKE '%" & Tchar(txtFilterPenerimaan.Text) & "%'"
        BindPenerimaan(" AND " & DDLFilterPenerimaan.SelectedValue & " LIKE '%" & Tchar(txtFilterPenerimaan.Text) & "%'")
    End Sub

    Protected Sub imbViewPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewPenerimaan.Click
        panelPenerimaan.Visible = True
        btnHidePenerimaan.Visible = True
        mpePenerimaan.Show() : BindPenerimaan("")
        txtFilterPenerimaan.Text = "" : DDLFilterPenerimaan.SelectedIndex = 0
    End Sub

    Protected Sub lkbClosePenerimaan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbClosePenerimaan.Click
        panelPenerimaan.Visible = False : btnHidePenerimaan.Visible = False
        CProc.DisposeGridView(gvPenerimaan)
    End Sub

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindItem.Click
        Try
            UpdateCheckedMat()
            If Session("TblListMat") IsNot Nothing Then
                Dim dt As DataTable = Session("TblListMat")
                If dt.Rows.Count > 0 Then
                    Dim dv As DataView = dt.DefaultView
                    Dim sFilter As String = " AND " & DDLFilterItem.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterItem.Text) & "%'"

                    dv.RowFilter = sFilter
                    Session("TblListMatView") = dv.ToTable
                    GVItemList.DataSource = Session("TblListMatView")
                    GVItemList.DataBind() : dv.RowFilter = ""
                    panelItem.Visible = True : btnHideItem.Visible = True
                    mpeItem.Show()
                Else
                    Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
                    showMessage(Session("WarningListMat"), 2)
                End If
            Else
                Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub FromBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FromBranch.SelectedIndexChanged
        If Page.IsPostBack Then
            InitDDLLocation()
            InitDDLBranchTujuan()
        End If
    End Sub

    Protected Sub ImgBtnSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSupp.Click
        BindSuppData()
        GVSupp.Visible = True
    End Sub

    Protected Sub GVSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVSupp.PageIndexChanging
        GVSupp.PageIndex = e.NewPageIndex
        BindSuppData()
    End Sub

    Protected Sub GVSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVSupp.SelectedIndexChanged
        SuppOid.Text = GVSupp.SelectedDataKey("suppoid")
        SuppName.Text = GVSupp.SelectedDataKey("suppname")
        GVSupp.Visible = False
    End Sub

    Protected Sub ImgBtnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnErase.Click
        SuppOid.Text = "" : SuppName.Text = ""
        GVSupp.Visible = False
    End Sub

    Protected Sub imbViewItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewItem.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                DDLFilterItem.SelectedIndex = -1
                txtFilterItem.Text = ""
                Session("TblListMatView") = Session("TblListMat")
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind()
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lkbCloseItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseItem.Click
        txtFilterItem.Text = ""
        btnHideItem.Visible = False : panelItem.Visible = False
    End Sub

    Protected Sub gvMaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaster.SelectedIndexChanged
        Response.Redirect("trnTWServiceSup.aspx?branch_code=" & gvMaster.SelectedDataKey("branch_code").ToString & "&oid=" & gvMaster.SelectedDataKey("trfwhserviceoid") & "")
    End Sub

    Protected Sub gvPenerimaan_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPenerimaan.PageIndexChanging
        gvPenerimaan.PageIndex = e.NewPageIndex
        panelPenerimaan.Visible = True : btnHidePenerimaan.Visible = True
        mpePenerimaan.Show()
        Dim sWhere As String = " AND " & DDLFilterPenerimaan.SelectedValue & " LIKE '%" & Tchar(txtFilterPenerimaan.Text) & "%'"
        BindPenerimaan(sWhere)
    End Sub

    Protected Sub TypeService_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TypeService.SelectedIndexChanged
        InitDDLLocation()
        If TypeService.SelectedValue = "INTERNSUPP" Then
            Label6.Text = "Tujuan"
            SuppName.Visible = False : ImgBtnSupp.Visible = False
            ImgBtnErase.Visible = False : DDLTujuan.Visible = True
            Label7.Visible = True : Label8.Visible = True
            DDLTujuanGudang.Visible = True : GVSupp.Visible = False
        Else
            Label6.Text = "Supplier"
            SuppName.Visible = True : ImgBtnSupp.Visible = True
            ImgBtnErase.Visible = True : DDLTujuan.Visible = False
            Label7.Visible = False : Label8.Visible = False
            DDLTujuanGudang.Visible = False
        End If
    End Sub

    Protected Sub DDLTujuan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLTujuan.SelectedIndexChanged
        InitDDLLocTo()
    End Sub

    Protected Sub BtnSendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSendApp.Click
        trnstatus.Text = "IN APPROVAL" : btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbSeachPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSeachPenerimaan.Click
        If TypeService.SelectedValue = "INTERNSUPP" Then
            SuppOid.Text = 0
            panelPenerimaan.Visible = True : btnHidePenerimaan.Visible = True
            mpePenerimaan.Show() : BindPenerimaan("")
        Else
            If SuppOid.Text <> "" Then
                panelPenerimaan.Visible = True : btnHidePenerimaan.Visible = True
                mpePenerimaan.Show() : BindPenerimaan("")
            Else
                showMessage("- Maaf, Silahkan pilih Supplier dulu..!!", 2)
            End If
        End If
    End Sub

    Protected Sub gvPenerimaan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHidePenerimaan.Visible = False
        panelPenerimaan.Visible = False
        mpePenerimaan.Show()
        reqoid.Text = gvPenerimaan.SelectedDataKey("reqoid").ToString
        txtPenerimaan.Text = gvPenerimaan.SelectedDataKey("reqcode")
        CProc.DisposeGridView(sender)
        Dim OidReq As String = " AND rq.reqmstoid = '" & reqoid.Text & "'"
        panelItem.Visible = True : btnHideItem.Visible = True
        mpeItem.Show() : BindItem(OidReq)
    End Sub

    Protected Sub imbAddToListPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If txtPenerimaan.Text.Trim = "" Or penerimaanoid.Text = "" Then
            showMessage("No. Penerimaan tidak boleh kosong!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable

        If I_u2.Text = "new" Then
            If Session("penerimaandetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("reqcode", Type.GetType("System.String"))
                dtab.Columns.Add("custoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("custname", Type.GetType("System.String"))
                dtab.Columns.Add("reqdate", Type.GetType("System.String"))

                Session("penerimaandetail") = dtab
                sequence.Text = "1"
            Else
                dtab = Session("penerimaandetail")
                sequence.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("penerimaandetail")
        End If

        Dim objTable As DataTable
        objTable = Session("penerimaandetail")
        Dim dv As DataView = objTable.DefaultView

        If I_u2.Text = "new" Then
            dv.RowFilter = "reqoid = " & Integer.Parse(penerimaanoid.Text) & " "
        Else
            dv.RowFilter = "reqoid = " & Integer.Parse(penerimaanoid.Text) & " AND seq <> " & Integer.Parse(sequence.Text) & ""
        End If
        If dv.Count > 0 Then
            dv.RowFilter = ""
            showMessage("No. Penerimaan tidak bisa ditambahkan ke dalam list..!! No. Penerimaan ini telah ada di dalam list !", 2)
            Exit Sub
        End If

        dv.RowFilter = "" : Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow

            drow("seq") = Integer.Parse(sequence.Text)
            drow("reqoid") = Integer.Parse(penerimaanoid.Text)
            drow("reqcode") = txtPenerimaan.Text

            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(sequence.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()
            drow("reqoid") = Integer.Parse(penerimaanoid.Text)
            drow("reqcode") = txtPenerimaan.Text

            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If
        I_u2.Text = "new" : txtPenerimaan.Text = ""

        If gvPenerimaan.Visible = True Then
            gvPenerimaan.DataSource = Nothing
            gvPenerimaan.DataBind()
            gvPenerimaan.Visible = False
        End If

        GVItemList.Visible = True
        BindItem(2)
    End Sub

    Protected Sub imbClearPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtPenerimaan.Text = "" : gvPenerimaan.Visible = False
    End Sub

    Protected Sub imbClearPenerimaanDetail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearPenerimaanDetail.Click
        I_u2.Text = "new" : txtPenerimaan.Text = ""
        txtPenerimaan.Text = ""


        If Session("penerimaandetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("penerimaandetail")
            sequence.Text = objTable.Rows.Count + 1
        Else
            sequence.Text = 1
        End If

        If gvPenerimaan.Visible = True Then
            gvPenerimaan.DataSource = Nothing
            gvPenerimaan.DataBind()
            gvPenerimaan.Visible = False
        End If
    End Sub

    Protected Sub lbldelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)
        Dim dtab As DataTable
        If Not Session("penerimaandetail") Is Nothing Then
            dtab = Session("penerimaandetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()
        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        lbldeleteitem_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbldeleteitem_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton1 As System.Web.UI.WebControls.LinkButton
        Dim gvr1 As GridViewRow

        lbutton1 = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr1 = TryCast(lbutton1.NamingContainer, GridViewRow)
        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("reqoid = " & Integer.Parse(gvr1.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()
        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objTable As DataTable = Session("itemdetail")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit() : dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("itemdetail") = objTable
        GVItemDetail.DataSource = objTable
        GVItemDetail.DataBind()

        If GVItemDetail.Rows.Count = 0 Then
            FromBranch.Enabled = True : FromBranch.CssClass = "inpText"
            fromlocation.Enabled = True : fromlocation.CssClass = "inpText"
        End If
    End Sub
#End Region
End Class
