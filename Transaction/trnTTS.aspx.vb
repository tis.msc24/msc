Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnTTS
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

    Private Sub FillTextbox()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "SELECT a.TTSNo, a.TTSDate, a.TTSnote, a.custoid, b.custname, a.trnjualno, a.trnsjjualno, a.updtime, a.upduser, a.TTSstatus, c.trnsjjualmstoid FROM QL_trnttsmst a inner join ql_mstcust b on a.cmpcode = b.cmpcode and a.custoid = b.custoid inner join ql_trnsjjualmst c on a.cmpcode = c.cmpcode and a.trnsjjualno = c.trnsjjualno WHERE a.cmpcode = '" & cmpcode & "' AND a.ttsoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                ttsno.Text = xreader("TTSNo")
                ttsdate.Text = Format(xreader("TTSDate"), "dd/MM/yyyy")
                ttsso.Text = xreader("trnjualno")
                sono.Text = xreader("trnjualno")
                ttscustname.Text = xreader("custname")
                ttscustoid.Text = xreader("custoid")
                ttsDO.Text = xreader("trnsjjualno")
                dono.Text = xreader("trnsjjualno")
                dooid.Text = xreader("trnsjjualmstoid")
                ttsstatus.Text = xreader("TTSstatus")
                ttsnote.Text = xreader("TTSnote")
                upduser.Text = xreader("upduser")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
            End While
        Else
            showMessage("Missing tts data >.<", 2)
        End If
        xreader.Close()
        conn.Close()

        sSql = "select a.seq, a.refoid itemoid, b.itemdesc, b.Merk, a.qty, a.unitoid satuan, b.satuan1, b.satuan2, b.satuan3, cast(b.konversi1_2 as decimal(18,2)) konversi1_2, cast(b.konversi2_3 as decimal(18,2)) konversi2_3, c.gendesc unit, d.gendesc unit1, e.gendesc unit2, f.gendesc unit3, ((select sum(case n.unitseq when 1 then (m.qty*cast(o.konversi1_2 as decimal(18,2))*cast(o.konversi2_3 as decimal(18,2))) when 2 then (m.qty*cast(o.konversi2_3 as decimal(18,2))) when 3 then (m.qty) end) from ql_trnsjjualdtl m inner join QL_trnorderdtl n on m.cmpcode = n.cmpcode and m.trnorderdtloid = n.trnorderdtloid inner join ql_mstitem o on n.cmpcode = o.cmpcode and n.itemoid = o.itemoid where m.trnsjjualmstoid = " & dooid.Text & " and m.refoid = a.refoid)-(select isnull(sum(case j.unitseq when 1 then (j.qty*cast(l.konversi1_2 as decimal(18,2))*cast(l.konversi2_3 as decimal(18,2))) when 2 then (j.qty*cast(l.konversi2_3 as decimal(18,2))) when 3 then (j.qty) end), 0) from ql_TrnTTSDtl j inner join ql_trnTTSmst k on j.cmpcode = k.cmpcode and j.ttsoid = k.TTSoid inner join ql_mstitem l on j.cmpcode = l.cmpcode and j.refoid = l.itemoid where j.ttsoid <> " & Integer.Parse(Session("oid")) & " and k.trnsjjualno = '" & Tchar(dono.Text) & "' and j.refoid = a.refoid)) maxqty from ql_TrnTTSDtl a inner join ql_mstitem b on a.cmpcode = b.cmpcode and a.refoid = b.itemoid and a.refname = 'ql_mstitem' inner join QL_mstgen c on a.cmpcode = c.cmpcode and a.unitoid = c.genoid inner join QL_mstgen d on b.cmpcode = d.cmpcode and b.satuan1 = d.genoid inner join QL_mstgen e on b.cmpcode = e.cmpcode and b.satuan2 = e.genoid inner join QL_mstgen f on b.cmpcode = f.cmpcode and b.satuan3 = f.genoid where a.cmpcode = '" & cmpcode & "' and a.ttsoid = " & Integer.Parse(Session("oid")) & ""
        Session("itemdetail") = ckon.ambiltabel(sSql, "detailtw")
        GVItemDetail.DataSource = Session("itemdetail")
        GVItemDetail.DataBind()

        If ttsstatus.Text = "POST" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            BtnCancel.Visible = True
            btnPosting.Visible = False
            btnApproval.Visible = False
            batal.Visible = True
        ElseIf ttsstatus.Text = "BATAL" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            BtnCancel.Visible = True
            btnPosting.Visible = False
            btnApproval.Visible = False
            batal.Visible = False
        Else
            btnSave.Visible = True
            btnDelete.Visible = True
            BtnCancel.Visible = True
            btnPosting.Visible = True
            btnApproval.Visible = False
            batal.Visible = False
        End If

    End Sub

    Private Sub BindData(ByVal swhere As String)
        sSql = "select  a.ttsoid, a.TTSNo, a.TTSDate, a.custoid, b.custname, a.trnjualno,a.trnsjjualno, a.TTSnote, a.TTSstatus from ql_trnTTSmst a inner join QL_mstcust b on a.cmpcode = b.cmpcode and a.custoid = b.custoid where a.cmpcode = '" & cmpcode & "'" & swhere & " order by case TTSstatus when 'In Process' then 1 else 2 end , TTSNo desc"
        Session("tts") = ckon.ambiltabel(sSql, "tts")
        gvMaster.DataSource = Session("tts")
        gvMaster.DataBind()

    End Sub

    Private Sub bindSO()
        sSql = "select distinct top 100 a.orderno, a.trnorderdate, c.custoid, c.custname from ql_trnordermst a inner join QL_trnorderdtl b on a.cmpcode = b.cmpcode and a.ordermstoid = b.trnordermstoid inner join QL_mstcust c on a.cmpcode = c.cmpcode and a.trncustoid = c.custoid inner join ql_trnsjjualmst jm on jm.orderno = a.orderno   inner join QL_trnsjjualdtl jd on jd.trnsjjualmstoid = jm.trnsjjualmstoid and jd.trnorderdtloid = b.trnorderdtloid inner join QL_MSTITEM i on i.itemoid = b.itemoid where a.cmpcode = '" & cmpcode & "' and (a.orderno like '%" & Tchar(ttsso.Text) & "%' or c.custname like '%" & Tchar(ttsso.Text) & "%') and a.trnorderstatus in ('approved','Closed') group by a.orderno, a.trnorderdate, c.custoid, c.custname having ((sum(case b.unitseq when 1 then (jd.qty*cast(i.konversi1_2 as decimal(18,5))*cast(i.konversi2_3 as decimal(18,5))) when 2 then (jd.qty*cast(i.konversi2_3 as decimal(18,5))) when 3 then (jd.qty) end))-(select isnull(sum(case m.unitseq when 1 then (m.qty*cast(n.konversi1_2 as decimal(18,5))*cast(n.konversi2_3 as decimal(18,5))) when 2 then (m.qty*cast(n.konversi2_3 as decimal(18,5))) when 3 then m.qty end), 0) from ql_TrnTTSDtl m inner join QL_mstItem n on m.cmpcode = n.cmpcode and m.refoid = n.itemoid where m.ttsoid in (select ttsoid from ql_trnTTSmst where trnjualno = a.orderno and TTSstatus <> 'BATAL' ))) > 0  order by a.trnorderdate desc"
        Session("ttsso") = ckon.ambiltabel(sSql, "ttssolist")
        GVSo.DataSource = Session("ttsso")
        GVSo.DataBind()
    End Sub

    Private Sub bindDO()
        sSql = "select a.trnsjjualmstoid, a.trnsjjualno, a.orderno, a.trnsjjualdate, f.custname, f.custoid from ql_trnsjjualmst a inner join ql_trnsjjualdtl b on a.cmpcode = b.cmpcode and a.trnsjjualmstoid = b.trnsjjualmstoid inner join QL_trnorderdtl c on b.cmpcode = c.cmpcode and b.trnorderdtloid = c.trnorderdtloid inner join QL_mstItem d on c.cmpcode = d.cmpcode and c.itemoid = d.itemoid inner join QL_trnordermst e on c.cmpcode = e.cmpcode and c.trnordermstoid = e.ordermstoid inner join QL_mstcust f on e.cmpcode = f.cmpcode and e.trncustoid = f.custoid where a.cmpcode = '" & cmpcode & "' and a.orderno like '%" & Tchar(ttsso.Text) & "%' and a.trnsjjualstatus in ('Approved','INVOICED') group by a.trnsjjualmstoid, a.trnsjjualno, a.orderno, a.trnsjjualdate, f.custname, f.custoid having ((sum(case c.unitseq when 1 then (b.qty*cast(d.konversi1_2 as decimal(18,5))*cast(d.konversi2_3 as decimal(18,5))) when 2 then (b.qty*cast(d.konversi2_3 as decimal(18,5))) when 3 then (b.qty) end))-(select isnull(sum(case m.unitseq when 1 then (m.qty*cast(n.konversi1_2 as decimal(18,5))*cast(n.konversi2_3 as decimal(18,5))) when 2 then (m.qty*cast(n.konversi2_3 as decimal(18,5))) when 3 then m.qty end), 0) from ql_TrnTTSDtl m inner join QL_mstItem n on m.cmpcode = n.cmpcode and m.refoid = n.itemoid where m.ttsoid in (select ttsoid from ql_trnTTSmst where trnsjjualno = a.trnsjjualno and TTSstatus <> 'BATAL' ))) > 0"
        Session("ttsdo") = ckon.ambiltabel(sSql, "ttsdolist")
        GVDo.DataSource = Session("ttsdo")
        GVDo.DataBind()
    End Sub

    Private Sub bindItem()
        sSql = "select a.refoid, c.itemcode, c.Merk, c.itemdesc, c.konversi1_2, c.konversi2_3, c.satuan1, c.satuan2, c.satuan3, d.gendesc unit1, e.gendesc unit2, f.gendesc unit3, (sum(case b.unitseq when 1 then (a.qty*cast(c.konversi1_2 as decimal(18,5))*cast(c.konversi2_3 as decimal(18,5))) when 2 then (a.qty*cast(c.konversi2_3 as decimal(18,5))) when 3 then (a.qty) end)-(select isnull(sum(case m.unitseq when 1 then (m.qty*cast(o.konversi1_2 as decimal(18,5))*cast(o.konversi2_3 as decimal(18,5))) when 2 then (m.qty*cast(o.konversi2_3 as decimal(18,5))) when 3 then (m.qty) end), 0) from ql_TrnTTSDtl m inner join ql_trnTTSmst n on m.cmpcode = n.cmpcode and m.ttsoid = n.TTSoid inner join QL_mstItem o on m.cmpcode = o.cmpcode and m.refoid = o.itemoid where m.refoid = a.refoid and n.trnsjjualno like '%" & Tchar(dono.Text) & "%' and n.TTSstatus <> 'BATAL' )) qtysisa, a.unitoid from ql_trnsjjualdtl a inner join QL_trnorderdtl b on a.cmpcode = b.cmpcode and a.trnorderdtloid = b.trnorderdtloid inner join QL_mstItem c on a.cmpcode = c.cmpcode and a.refoid = c.itemoid and a.refname = 'ql_mstitem' inner join QL_mstgen d on c.cmpcode = d.cmpcode and c.satuan1 = d.genoid inner join QL_mstgen e on c.cmpcode = e.cmpcode and c.satuan2 = e.genoid inner join QL_mstgen f on c.cmpcode = f.cmpcode and c.satuan3 = f.genoid where a.cmpcode = '" & cmpcode & "' and a.trnsjjualmstoid = " & Integer.Parse(dooid.Text) & " group by a.refoid, c.itemcode, c.Merk, c.itemdesc, a.trnsjjualmstoid, a.trnorderdtloid, c.konversi1_2, c.konversi2_3, c.satuan1, c.satuan2, c.satuan3, d.gendesc, e.gendesc, f.gendesc, a.unitoid having (sum(case b.unitseq when 1 then (a.qty*cast(c.konversi1_2 as decimal(18,5))*cast(c.konversi2_3 as decimal(18,5))) when 2 then (a.qty*cast(c.konversi2_3 as decimal(18,5))) when 3 then (a.qty) end)-(select isnull(sum(case m.unitseq when 1 then (m.qty*cast(o.konversi1_2 as decimal(18,5))*cast(o.konversi2_3 as decimal(18,5))) when 2 then (m.qty*cast(o.konversi2_3 as decimal(18,5))) when 3 then (m.qty) end), 0) from ql_TrnTTSDtl m inner join ql_trnTTSmst n on m.cmpcode = n.cmpcode and m.ttsoid = n.TTSoid inner join QL_mstItem o on m.cmpcode = o.cmpcode and m.refoid = o.itemoid where m.refoid = a.refoid and n.trnsjjualno like '%" & Tchar(dono.Text) & "%' and n.TTSstatus <> 'BATAL')) > 0"
        Session("ttsitem") = ckon.ambiltabel(sSql, "ttsitemlist")
        GVItemList.DataSource = Session("ttsitem")
        GVItemList.DataBind()
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")

            Session.Clear()

            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("~\Transaction\trnTTS.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")
        btnApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data to approval?')")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Tanda Terima Sementara"

        If Not Page.IsPostBack Then
            BindData("")
            ttsno.Text = GenerateID("ql_trnTTSmst", cmpcode)
            ttsdate.Text = Format(Date.Now, "dd/MM/yyyy")

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(date1, "dd/MM/yyyy")
                tgl2.Text = Format(date2, "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0

                GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        BindData("")

        filterddl.SelectedIndex = 0
        filtertext.Text = ""
        filterstatus.SelectedValue = "All"

        Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
        Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
        tgl1.Text = Format(date1, "dd/MM/yyyy")
        tgl2.Text = Format(date2, "dd/MM/yyyy")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Period cannot empty !", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Period 1 is not valid !", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Period 2 is not valid !", 2)
            Exit Sub
        End If

        If date1 > date2 Then
            showMessage("Date for period 1 cannot more than period 2 !", 2)
            Exit Sub
        End If

        BindData(" and convert(date, a.TTSDate, 103) between '" & date1 & "' and '" & date2 & "' and " & filterddl.SelectedValue.ToString & " like '%" & Tchar(filtertext.Text) & "%'" & IIf(filterstatus.SelectedValue.ToString <> "All", " and a.TTSstatus = '" & filterstatus.SelectedValue.ToString & "'", "") & "")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""
        If ttsdate.Text = "" Then
            errmsg &= "-TTS date cannot empty !</br>"
        Else
            If IsDate(toDate(ttsdate.Text)) = False Then
                errmsg &= "-Invalid TTS date!</br>"
            End If
        End If

        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "-Transfer detail cannot empty !"
        End If
        If errmsg.Trim <> "" Then
            showMessage(errmsg, 2)
            ttsstatus.Text = "In Process"
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Dim transdate As Date = Date.ParseExact(ttsdate.Text, "dd/MM/yyyy", Nothing)

            Dim ttsoid As Integer = 0
            If i_u.Text = "new" Then
                'Generate transfer master ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_trnttsmst' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                ttsoid = xCmd.ExecuteScalar + 1
            End If

            'Generate tts detail ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_trnttsdtl' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim ttsdtloid As Int32 = xCmd.ExecuteScalar + 1

            'Generate tts number
            If ttsstatus.Text = "POST" Then
                Dim ttsnotemp As String = "TTS/" & Format(transdate, "yy") & "/" & Format(transdate, "MM") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(ttsno, 4) AS INT)), 0) FROM QL_trnttsmst WHERE cmpcode = '" & cmpcode & "' AND ttsno LIKE '" & ttsnotemp & "%'"
                xCmd.CommandText = sSql
                ttsnotemp = ttsnotemp & Format(xCmd.ExecuteScalar + 1, "0000")
                ttsno.Text = ttsnotemp
            Else
                If i_u.Text = "new" Then
                    ttsno.Text = ttsoid.ToString
                End If
            End If

            If i_u.Text = "new" Then

                'Insert to master
                sSql = "INSERT INTO QL_trnttsmst (cmpcode, TTSoid, TTSNo, TTSDate, TTSnote, custoid, trnjualno, trnsjjualno, updtime, upduser, TTSstatus) VALUES ('" & cmpcode & "', " & ttsoid & ", '" & ttsno.Text & "', '" & transdate & "', '" & Tchar(ttsnote.Text.Trim) & "', " & Integer.Parse(ttscustoid.Text) & ", '" & Tchar(sono.Text) & "', '" & Tchar(dono.Text) & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', '" & ttsstatus.Text & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Update lastoid QL_ttsmst
                sSql = "UPDATE QL_mstoid SET lastoid = " & ttsoid & " WHERE tablename = 'QL_trnttsmst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            Else

                sSql = "SELECT ttsstatus FROM QL_trnttsmst WHERE ttsoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                Dim srest As String = xCmd.ExecuteScalar
                If srest Is Nothing Or srest = "" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("TTS data cannot be found !<br />Check if data has been deleted by another user!", 2)
                    ttsstatus.Text = "In Process"
                    Exit Sub
                Else
                    If srest = "POST" Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("TTS data cannot be modified !<br />Check if data has been posted!", 2)
                        ttsstatus.Text = "In Process"
                        Exit Sub
                    End If
                End If

                'Update master record
                sSql = "UPDATE QL_trnttsmst SET TTSNo = '" & ttsno.Text & "', TTSDate = '" & transdate & "', TTSnote = '" & Tchar(ttsnote.Text) & "', custoid = " & Integer.Parse(ttscustoid.Text) & ", trnjualno = '" & Tchar(sono.Text) & "', trnsjjualno = '" & Tchar(dono.Text) & "', ttsstatus = '" & ttsstatus.Text & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "' WHERE ttsoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM QL_trnttsdtl WHERE ttsoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0

                For i As Integer = 0 To objTable.Rows.Count - 1

                    If i_u.Text = "edit" Then
                        ttsoid = Integer.Parse(Session("oid"))
                    End If

                    If objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan1") Then
                        unitseq = 1
                    ElseIf objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan2") Then
                        unitseq = 2
                    ElseIf objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan3") Then
                        unitseq = 3
                    End If

                    sSql = "INSERT INTO QL_trnttsdtl (cmpcode, ttsDtlOid, ttsoid, seq, refname, refoid, qty, unitoid, unitseq) VALUES ('" & cmpcode & "', " & ttsdtloid & ", " & ttsoid & ", " & i + 1 & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("itemoid") & ", " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan") & ", " & unitseq & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ttsdtloid += 1

                Next

                'Update lastoid QL_trntrfmtrdtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & ttsdtloid - 1 & "  WHERE tablename = 'QL_trnttsdtl' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            End If

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            ttsstatus.Text = "In Process"
            Exit Sub
        End Try

        Response.Redirect("trnTTS.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT TTSstatus FROM ql_trnTTSmst WHERE TTSoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("TTS data cannot be found !<br />Check if data has been deleted by another user", 2)
                Exit Sub
            Else
                If srest = "POST" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("TTS data cannot be deleted !<br />Check if data has been posted!", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM ql_trnTTSmst WHERE TTSoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trnTTSdtl WHERE TTSoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnTTS.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        ttsstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            showMessage("Print out not available yet !", 2)
            Exit Sub

            'Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            'Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            'Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")

            'Dim sWhere As String = " WHERE a.trfmtrmstoid = " & Integer.Parse(labeloid.Text) & ""

            'report = New ReportDocument
            'report.Load(Server.MapPath(folderReport & "TWprintout.rpt"))
            'CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'report.SetParameterValue("sWhere", sWhere)
            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

            'Response.Buffer = False
            'Response.ClearContent()
            'Response.ClearHeaders()
            'report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TW_" & labeloid.Text & "")

            'report.Close() : report.Dispose()

            'Response.Redirect("trnWHTransfer.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        If Not Session("tts") Is Nothing Then
            gvMaster.DataSource = Session("tts")
            gvMaster.PageIndex = e.NewPageIndex
            gvMaster.DataBind()
        Else
            showMessage("Missing something !", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnApproval.Click
        ttsstatus.Text = "In Approval"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnTTS.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        Dim qtyval As Double = 0.0
        If qty.Text.Trim <> "" Then
            If Double.TryParse(qty.Text.Trim, qtyval) Then
                qty.Text = Format(qtyval, "#,##0.00")
            Else
                qty.Text = Format(0.0, "#,##0.00")
            End If
        Else
            qty.Text = Format(0.0, "#,##0.00")
        End If
    End Sub

    Protected Sub btnsearchso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearchso.Click
        bindSO()
        GVSo.Visible = True

        GVDo.Visible = False

        GVItemList.Visible = False
    End Sub

    Protected Sub GVSo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVSo.PageIndexChanging
        GVSo.DataSource = Session("ttsso")
        GVSo.PageIndex = e.NewPageIndex
        GVSo.DataBind()
    End Sub

    Protected Sub GVSo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVSo.SelectedIndexChanged
        ttsso.Text = GVSo.Rows(GVSo.SelectedIndex).Cells(1).Text
        sono.Text = GVSo.Rows(GVSo.SelectedIndex).Cells(1).Text
        ttscustname.Text = GVSo.Rows(GVSo.SelectedIndex).Cells(3).Text
        ttscustoid.Text = GVSo.SelectedDataKey("custoid")
        GVSo.Visible = False

        ttsDO.Text = ""
        dono.Text = ""

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
    End Sub

    Protected Sub btneraseso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btneraseso.Click
        ttsso.Text = ""
        sono.Text = ""
        ttscustname.Text = ""
        ttscustoid.Text = ""
        GVSo.Visible = False

        ttsDO.Text = ""
        dono.Text = ""
        GVDo.Visible = False

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        GVItemList.Visible = False
        btnClear_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnsearchdo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearchdo.Click
        If sono.Text = "" Then
            showMessage("Please choose SO number!", 2)
            Exit Sub
        End If
        bindDO()
        GVDo.Visible = True

        GVSo.Visible = False

        GVItemList.Visible = False
    End Sub

    Protected Sub btnerasedo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnerasedo.Click
        ttsDO.Text = ""
        dono.Text = ""
        dooid.Text = ""
        GVDo.Visible = False

        GVSo.Visible = False

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        GVItemList.Visible = False
        btnClear_Click(Nothing, Nothing)
    End Sub

    Protected Sub GVDo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVDo.PageIndexChanging
        GVDo.DataSource = Session("ttsdo")
        GVDo.PageIndex = e.NewPageIndex
        GVDo.DataBind()
    End Sub

    Protected Sub GVDo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDo.SelectedIndexChanged
        ttsDO.Text = GVDo.Rows(GVDo.SelectedIndex).Cells(1).Text
        dono.Text = GVDo.Rows(GVDo.SelectedIndex).Cells(1).Text
        dooid.Text = GVDo.SelectedDataKey("trnsjjualmstoid")
        ttsso.Text = GVDo.Rows(GVDo.SelectedIndex).Cells(2).Text
        sono.Text = GVDo.Rows(GVDo.SelectedIndex).Cells(2).Text
        ttscustname.Text = GVDo.Rows(GVDo.SelectedIndex).Cells(4).Text
        ttscustoid.Text = GVDo.SelectedDataKey("custoid")

        GVDo.Visible = False

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        If dono.Text.Trim = "" And dooid.Text.Trim = "" Then
            showMessage("Please choose DO number!", 2)
            Exit Sub
        End If
        bindItem()
        GVItemList.Visible = True
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        GVItemList.DataSource = Session("ttsitem")
        GVItemList.PageIndex = e.NewPageIndex
        GVItemList.DataBind()
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = ""
        labelitemoid.Text = ""
        merk.Text = ""
        qty.Text = "0.00"
        labelmaxqty.Text = "0.00"
        maxqtytemp.Text = "0.00"

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        unit.Items.Clear()

        GVItemList.Visible = False
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        item.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(2).Text
        labelitemoid.Text = GVItemList.SelectedDataKey("refoid")
        merk.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(3).Text
        'qty.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text
        qty.Text = "0.00"
        labelmaxqty.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text
        maxqtytemp.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text

        unit.Items.Clear()
        unit.Items.Add(GVItemList.Rows(GVItemList.SelectedIndex).Cells(5).Text)
        unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan3")
        If GVItemList.SelectedDataKey("satuan2") <> GVItemList.SelectedDataKey("satuan3") Then
            unit.Items.Add(GVItemList.SelectedDataKey("unit2"))
            unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan2")
        End If
        If GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan3") And GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan2") Then
            unit.Items.Add(GVItemList.SelectedDataKey("unit1"))
            unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan1")
        End If
        unit.SelectedValue = GVItemList.SelectedDataKey("unitoid")

        labelsatuan1.Text = GVItemList.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemList.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemList.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemList.SelectedDataKey("unit1").ToString
        labelunit2.Text = GVItemList.SelectedDataKey("unit2").ToString
        labelunit3.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(5).Text

        labelkonversi1_2.Text = GVItemList.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemList.SelectedDataKey("konversi2_3")

        GVItemList.Visible = False

        unit_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unit.SelectedIndexChanged
        Dim maxqty As Double = 0.0

        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = ToDouble(maxqtytemp.Text) / ToDouble(labelkonversi2_3.Text) / ToDouble(labelkonversi1_2.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = ToDouble(maxqtytemp.Text) / ToDouble(labelkonversi2_3.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan3.Text) Then
            maxqty = ToDouble(maxqtytemp.Text)
        End If

        labelmaxqty.Text = Format(maxqty, "#,##0.00")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new"
        labelitemoid.Text = ""
        qty.Text = "0.00"
        item.Text = ""
        merk.Text = ""
        labelmaxqty.Text = "0.00"
        maxqtytemp.Text = "0.00"

        unit.Items.Clear()

        labelseq.Text = GVItemDetail.Rows.Count + 1

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        GVItemList.Visible = False

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(6).Visible = True
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            showMessage("Item cannot empty!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) = 0.0 Then
            showMessage("Qty must be more than 0!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) > Double.Parse(labelmaxqty.Text.Trim) Then
            showMessage("Qty cannot more than maximum qty!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable = Nothing

        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("unit1", Type.GetType("System.String"))
                dtab.Columns.Add("unit2", Type.GetType("System.String"))
                dtab.Columns.Add("unit3", Type.GetType("System.String"))
                dtab.Columns.Add("maxqty", Type.GetType("System.Double"))
                Session("itemdetail") = dtab
            Else
                dtab = Session("itemdetail")
            End If
            labelseq.Text = GVItemDetail.Rows.Count + 1
        Else
            dtab = Session("itemdetail")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "' AND seq <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Item is already exist in the list !<br />It can't be added!", 2)
                Exit Sub
            End If
        End If

        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow
            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = Unit.SelectedValue
            drow("unit") = Unit.SelectedItem.Text
            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text
            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("maxqty") = Double.Parse(maxqtytemp.Text)
            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drow.BeginEdit()
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = Unit.SelectedValue
            drow("unit") = Unit.SelectedItem.Text
            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text
            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("maxqty") = Double.Parse(maxqtytemp.Text)
            drow.EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        labelseq.Text = GVItemDetail.Rows.Count + 1
        labelitemoid.Text = ""
        item.Text = ""
        I_u2.Text = "new"
        merk.Text = ""
        qty.Text = "0.00"
        labelmaxqty.Text = "0.00"
        maxqtytemp.Text = "0.00"
        Unit.Items.Clear()

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(6).Visible = True

        GVItemList.Visible = False
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        item.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(2).Text
        merk.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(3).Text
        qty.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(4).Text
        maxqtytemp.Text = Format(GVItemDetail.SelectedDataKey("maxqty"), "#,##0.00")
        labelmaxqty.Text = Format(GVItemDetail.SelectedDataKey("maxqty"), "#,##0.00")

        unit.Items.Clear()
        Unit.Items.Add(GVItemDetail.SelectedDataKey("unit3"))
        Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan3")
        If GVItemDetail.SelectedDataKey("satuan2") <> GVItemDetail.SelectedDataKey("satuan3") Then
            Unit.Items.Add(GVItemDetail.SelectedDataKey("unit2"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan2")
        End If
        If GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan3") And GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan2") Then
            Unit.Items.Add(GVItemDetail.SelectedDataKey("unit1"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan1")
        End If
        Unit.SelectedValue = GVItemDetail.SelectedDataKey("satuan")

        labelsatuan1.Text = GVItemDetail.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemDetail.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemDetail.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemDetail.SelectedDataKey("unit1")
        labelunit2.Text = GVItemDetail.SelectedDataKey("unit2")
        labelunit3.Text = GVItemDetail.SelectedDataKey("unit3")
        labelkonversi1_2.Text = GVItemDetail.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemDetail.SelectedDataKey("konversi2_3")

        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text

        GVItemList.Visible = False
        GVItemDetail.Columns(6).Visible = False

        unit_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub batal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ttsstatus.Text = "BATAL"
        'btnSave_Click(Nothing, Nothing)

        Dim CekRetur As Integer = 0
        CekRetur = GetStrData(" select count(*) from QL_trnjualreturmst where ttsoid = " & Integer.Parse(Session("oid")) & " ")
        If CekRetur > 0 Then
            showMessage(" TTS ini sudah dibuatkan Retur, silahkan cek lagi ", 2)
            Exit Sub
        End If


        If conn.State = ConnectionState.Closed Then
            conn.Open()
            End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try


                'Update master record
            sSql = "UPDATE QL_trnttsmst SET  ttsstatus = '" & ttsstatus.Text & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "' WHERE ttsoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            ttsstatus.Text = "In Process"
            Exit Sub
            End Try

        Response.Redirect("trnTTS.aspx?awal=true")
    End Sub
End Class
