Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnWhRetur
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub FillTextbox()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim tolocationoid As Integer = 0

        sSql = "SELECT TOP 1 a.trnTrfToReturNo, a.trntrfdate, a.trnbelipono, a.trnsjbelino, a.flag, b.mtrlocoid_from, b.mtrlocoid_to, d.suppname, a.Trntrfnote, a.status, a.updtime, a.upduser FROM ql_trnTrfToReturmst a INNER JOIN ql_trnTrfToReturDtl b ON a.cmpcode = b.cmpcode AND a.trnTrfToReturoid = b.trnTrfToReturoid INNER JOIN QL_pomst c ON a.cmpcode = c.cmpcode AND a.trnbelipono = c.trnbelipono INNER JOIN QL_mstsupp d ON c.cmpcode = d.cmpcode AND c.trnsuppoid = d.suppoid WHERE a.cmpcode = '" & cmpcode & "' AND a.trnTrfToReturoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                transferno.Text = xreader("trnTrfToReturNo")
                transferdate.Text = Format(xreader("trntrfdate"), "dd/MM/yyyy")
                noref.Text = xreader("trnbelipono")
                pooid.Text = xreader("trnbelipono")
                nosj.Text = xreader("trnsjbelino")
                sjoid.Text = xreader("trnsjbelino")
                rblflag.SelectedValue = xreader("flag")
                fromlocation.SelectedValue = xreader("mtrlocoid_from")
                tolocationoid = xreader("mtrlocoid_to")
                tosupplier.Text = xreader("suppname")
                note.Text = xreader("Trntrfnote")
                trnstatus.Text = xreader("status").ToString.Trim
                upduser.Text = xreader("upduser")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
            End While
        Else
            showMessage("Missing transfer data >.<", 2)
        End If
        xreader.Close()

        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)

        sSql = "SELECT a.seq, a.refoid AS itemoid, b.itemdesc, b.merk, a.qty, a.unitoid AS satuan, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3, c.gendesc AS unit, d.gendesc AS unit1, e.gendesc AS unit2, f.gendesc AS unit3, a.note AS note, ((SELECT SUM(CASE WHEN n.unitoid = o.satuan1 then n.qty*o.konversi1_2*o.konversi2_3 when n.unitoid = o.satuan2 then n.qty*o.konversi2_3 else n.qty end) FROM ql_trnsjbelimst m INNER JOIN ql_trnsjbelidtl n ON m.cmpcode = n.cmpcode AND m.trnsjbelioid = n.trnsjbelioid INNER JOIN ql_mstitem o ON n.cmpcode = o.cmpcode AND n.refoid = o.itemoid AND n.refname = 'ql_mstitem' WHERE m.trnsjbelino = '" & Tchar(sjoid.Text) & "' AND o.itemoid = b.itemoid)-(SELECT ISNULL(SUM(qty_to), 0) FROM ql_trnTrfToReturDtl s INNER JOIN ql_trnTrfToReturmst t ON s.cmpcode = t.cmpcode AND s.trnTrfToReturoid = t.trnTrfToReturoid WHERE t.trnsjbelino = '" & Tchar(sjoid.Text) & "' AND s.refoid = b.itemoid AND s.refname = 'ql_mstitem' and s.trnTrfToReturoid <> a.trnTrfToReturoid)+(select isnull(sum(x.qty_to), 0) from ql_trnTrffromReturDtl x inner join ql_trnTrffromReturmst y on x.cmpcode = y.cmpcode and x.trnTrfFromReturoid = y.trnTrfFromReturoid where y.cmpcode = a.cmpcode and y.trnTrfToReturNo in (select trnTrfToReturNo from ql_trnTrfToReturmst where trnsjbelino = '" & Tchar(nosj.Text) & "') and y.status='POST')) as sisaretur, ((select saldoakhir-qtybooking from QL_crdmtr where refoid = a.refoid and mtrlocoid = a.mtrlocoid_from and periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "')) stock,a.trnbelidtloid FROM ql_trnTrfToReturDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.unitoid = c.genoid INNER JOIN QL_mstgen d ON b.cmpcode = d.cmpcode AND b.satuan1 = d.genoid INNER JOIN QL_mstgen e ON b.cmpcode = e.cmpcode AND b.satuan2 = e.genoid INNER JOIN QL_mstgen f ON b.cmpcode = f.cmpcode AND b.satuan2 = f.genoid WHERE a.cmpcode = '" & cmpcode & "' AND a.trnTrfToReturoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "detailtr")
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        If trnstatus.Text = "IN PROCESS" Then
            btnSave.Visible = True
            btnDelete.Visible = True
            BtnCancel.Visible = True
            btnPosting.Visible = True
        ElseIf trnstatus.Text = "POST" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            BtnCancel.Visible = True
            btnPosting.Visible = False
        End If

        conn.Close()

        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'RETUR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND a.genoid <> " & fromlocation.SelectedValue & " AND b.gendesc LIKE '%retur%' ORDER BY a.gendesc"
        FillDDL(tolocation, sSql)

        tolocation.SelectedValue = tolocationoid
    End Sub

    Private Sub BindData(ByVal state As String)
        Dim date1, date2 As New Date

        If state = 0 Then

            date1 = New Date(Date.Now.Year, Date.Now.Month, 1)
            date2 = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)

            sSql = "SELECT trnTrfToReturoid, trnTrfToReturNo, trntrfdate, upduser, status FROM ql_trnTrfToReturmst WHERE cmpcode = '" & cmpcode & "' AND CONVERT(DATE, trntrfdate, 101) BETWEEN '" & date1 & "' AND '" & date2 & "' ORDER BY status,trnTrfToReturNo desc "

        Else

            date1 = Date.ParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing)
            date2 = Date.ParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing)

            Dim swhere As String = ""
            Dim swhere1 As String = ""

            If Group.SelectedValue <> "ALL" Then
                swhere &= " AND b.itemgroupoid = " & Group.SelectedValue & ""
            End If

            If subgroup.SelectedValue <> "ALL" Then
                swhere &= " AND b.itemsubgroupoid = " & subgroup.SelectedValue & ""
            End If

            Dim searchitemoid As Integer = 0
            If itemoid.Text <> "" Then
                If Integer.TryParse(itemoid.Text, searchitemoid) = False Then
                    showMessage("Item tidak valid !", 2)
                    Exit Sub
                Else
                    swhere &= " AND a.refoid = " & Integer.Parse(itemoid.Text) & ""
                End If
            End If

            If FilterText.Text.Trim <> "" Then
                swhere1 &= " AND c.trnTrfToReturNo LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
            End If

            sSql = "SELECT DISTINCT c.trnTrfToReturoid, c.trnTrfToReturNo, c.trntrfdate, c.upduser, c.status FROM ql_trnTrfToReturDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM'" & swhere & " INNER JOIN ql_trnTrfToReturmst c ON a.cmpcode = c.cmpcode AND a.trnTrfToReturoid = c.trnTrfToReturoid" & swhere1 & " WHERE CONVERT(DATE, c.trntrfdate, 101) BETWEEN '" & date1 & "' AND '" & date2 & "' ORDER BY c.status, c.trnTrfToReturNo desc"

        End If

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "tr")
        gvMaster.DataSource = dtab
        gvMaster.DataBind()
        Session("tr") = dtab
    End Sub

    Private Sub BindItem()
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)

        sSql = "select a.refoid itemoid, c.itemcode, c.itemdesc, c.merk, (d.saldoakhir-d.qtybooking) as sisa, c.satuan1, e.gendesc AS unit1, c.satuan2, f.gendesc AS unit2, c.satuan3, g.gendesc AS unit3, c.konversi1_2, c.konversi2_3, ((case when a.unitoid = c.satuan1 then (a.qty*cast(c.konversi1_2 as decimal(18,2))*cast(c.konversi2_3 as decimal(18,2))) when a.unitoid = c.satuan2 then (a.qty*cast(c.konversi2_3 as decimal(18,2))) when a.unitoid = c.satuan3 then (a.qty) end)-(select isnull(sum(j.qty_to), 0) FROM ql_trnTrfToReturDtl j inner join ql_trnTrfToReturmst k on j.cmpcode = k.cmpcode and j.trnTrfToReturoid = k.trnTrfToReturoid where k.trnsjbelino like '%" & Tchar(nosj.Text) & "%' and j.cmpcode = a.cmpcode and j.refoid = a.refoid and j.refname = a.refname and j.refname = 'ql_mstitem'" & IIf(i_u.Text <> "new", " and j.trnTrfToReturoid <> " & Session("oid") & "", "") & ")+(select isnull(sum(x.qty_to), 0) FROM ql_trnTrfFromReturDtl x inner join ql_trnTrfFromReturmst y on x.cmpcode = y.cmpcode and x.trnTrfFromReturoid = y.trnTrfFromReturoid where y.trnTrfToReturNo in (select trnTrfToReturNo from ql_trnTrfToReturmst where trnsjbelino like '%" & Tchar(nosj.Text) & "%') and x.cmpcode = a.cmpcode and x.refoid = a.refoid and x.refname = a.refname and x.refname = 'ql_mstitem' and y.status='POST')) as sisaretur, a.unitoid,a.trnbelidtloid from ql_trnsjbelidtl a inner join ql_trnsjbelimst b on a.cmpcode = b.cmpcode and a.trnsjbelioid = b.trnsjbelioid inner join ql_mstitem c on a.cmpcode = c.cmpcode and a.refoid = c.itemoid and a.refname = 'ql_mstitem' inner join QL_crdmtr d on a.cmpcode = d.cmpcode and a.refoid = d.refoid and a.refname = d.refname and a.refname = 'ql_mstitem' and d.periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "' and d.mtrlocoid = " & fromlocation.SelectedValue & " inner join QL_mstgen e on c.cmpcode = e.cmpcode and c.satuan1 = e.genoid inner join QL_mstgen f on c.cmpcode = f.cmpcode and c.satuan2 = f.genoid inner join QL_mstgen g on c.cmpcode = g.cmpcode and c.satuan3 = g.genoid where a.cmpcode = '" & cmpcode & "' and b.trnsjbelino like '%" & Tchar(nosj.Text) & "%' and ((case when a.unitoid = c.satuan1 then (a.qty*cast(c.konversi1_2 as decimal(18,2))*cast(c.konversi2_3 as decimal(18,2))) when a.unitoid = c.satuan2 then (a.qty*cast(c.konversi2_3 as decimal(18,2))) when a.unitoid = c.satuan3 then (a.qty) end)-(select isnull(sum(j.qty_to), 0) FROM ql_trnTrfToReturDtl j inner join ql_trnTrfToReturmst k on j.cmpcode = k.cmpcode and j.trnTrfToReturoid = k.trnTrfToReturoid where k.trnsjbelino like '%" & Tchar(nosj.Text) & "%' and j.cmpcode = a.cmpcode and j.refoid = a.refoid and j.refname = a.refname and j.refname = 'ql_mstitem'" & IIf(i_u.Text <> "new", " and j.trnTrfToReturoid <> " & Session("oid") & "", "") & ")+(select isnull(sum(x.qty_to), 0) FROM ql_trnTrfFromReturDtl x inner join ql_trnTrfFromReturmst y on x.cmpcode = y.cmpcode and x.trnTrfFromReturoid = y.trnTrfFromReturoid where y.trnTrfToReturNo in (select trnTrfToReturNo from ql_trnTrfToReturmst where trnsjbelino like '%" & Tchar(nosj.Text) & "%') and x.cmpcode = a.cmpcode and x.refoid = a.refoid and x.refname = a.refname and x.refname = 'ql_mstitem' and y.status='POST')) > 0 and (c.itemcode like '%" & Tchar(item.Text.Trim) & "%' or c.itemdesc like '%" & Tchar(item.Text.Trim) & "%' or c.Merk like '%" & Tchar(item.Text.Trim) & "%')"

        Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
        GVItemList.DataSource = objTable
        GVItemList.DataBind()

        Session("itemlistwhretur") = objTable
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Sub InitAllDDL()
        FillDDLGen("ITEMGROUP", Group)
        Group.Items.Add("ALL")
        Group.SelectedValue = "ALL"
        FillDDLGen("ITEMSUBGROUP", subgroup)
        subgroup.Items.Add("ALL")
        subgroup.SelectedValue = "ALL"

        'FillDDLGen("LOCATION", fromlocation)
        FillDDL(fromlocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND b.gendesc NOT LIKE '%retur%' ORDER BY a.gendesc")
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If
        fromlocation.SelectedIndex = 0

        'FillDDLGen("LOCATION", tolocation, " genoid <> " & fromlocation.SelectedValue)
        FillDDL(tolocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND a.genoid <> " & fromlocation.SelectedValue & " AND b.gendesc LIKE '%%' ORDER BY a.gendesc")
        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If
        tolocation.SelectedIndex = 0
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            'simpan session k variabel spy tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")

            'clear all session 
            Session.Clear()

            'insert lagi session yg disimpan dan create session 
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            'di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
            Response.Redirect("~\Transaction\trnWhRetur.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Transfer Gudang Retur"

        If Not Page.IsPostBack Then
            BindData(0)
            transferno.Text = GenerateID("ql_trnTrfToReturmst", cmpcode)
            transferdate.Text = Format(Date.Now, "dd/MM/yyyy")
            InitAllDDL()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(date1, "dd/MM/yyyy")
                tgl2.Text = Format(date2, "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0

                qty.Text = "0.00"
                GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                labelseq.Text = "1"
                labelkonversi1_2.Text = "1"
                labelkonversi2_3.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        BindData(0)

        FilterText.Text = ""
        Group.SelectedValue = "ALL"
        subgroup.SelectedValue = "ALL"
        Filteritem.Text = ""

        Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
        Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
        tgl1.Text = Format(date1, "dd/MM/yyyy")
        tgl2.Text = Format(date2, "dd/MM/yyyy")

        itemoid.Text = ""
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Tanggal period harus diisi !", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Tanggal period 1 tidak valid !", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Tanggal period 2 tidak valid !", 2)
            Exit Sub
        End If

        If date1 > date2 Then
            showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
            Exit Sub
        End If

        BindData(1)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        If nosj.Text.Trim = "" Or sjoid.Text = "" Then
            showMessage("Pilihlah no sj terlebih dahulu!", 2)
            Exit Sub
        End If
        If nosj.Text.Trim <> sjoid.Text Then
            showMessage("Pilihlah no sj terlebih dahulu!", 2)
            Exit Sub
        End If
        Try
            BindItem()
            GVItemList.Visible = True
            GVsj.Visible = False
            GVpo.Visible = False
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = ""
        labelitemoid.Text = ""
        qty.Text = "0.00"
        labelmaxqty.Text = "0.00"
        sisado.Text = "0.00"
        merk.Text = ""
        notedtl.Text = ""

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        unit.Items.Clear()

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        If Not Session("itemlistwhretur") Is Nothing Then
            GVItemList.DataSource = Session("itemlistwhretur")
            GVItemList.PageIndex = e.NewPageIndex
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        item.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(2).Text
        labelitemoid.Text = GVItemList.SelectedDataKey("itemoid")
        merk.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(3).Text
        'qty.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text
        qty.Text = "0.00"
        labelmaxqty.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text
        labeltempstock.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text
        labeltempsisaretur.Text = Format(GVItemList.SelectedDataKey("sisaretur"), "#,##0.00")
        sisado.Text = Format(GVItemList.SelectedDataKey("sisaretur"), "#,##0.00")

        Unit.Items.Clear()
        Unit.Items.Add(GVItemList.Rows(GVItemList.SelectedIndex).Cells(5).Text)
        Unit.Items(Unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan3")
        If GVItemList.SelectedDataKey("satuan2") <> GVItemList.SelectedDataKey("satuan3") Then
            Unit.Items.Add(GVItemList.SelectedDataKey("unit2"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan2")
        End If
        If GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan3") And GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan2") Then
            Unit.Items.Add(GVItemList.SelectedDataKey("unit1"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan1")
        End If
        unit.SelectedValue = GVItemList.SelectedDataKey("unitoid")

        labelsatuan1.Text = GVItemList.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemList.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemList.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemList.SelectedDataKey("unit1").ToString
        labelunit2.Text = GVItemList.SelectedDataKey("unit2").ToString
        trnbelidtloid.Text = GVItemList.SelectedDataKey("trnbelidtloid").ToString
        labelunit3.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(5).Text

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelkonversi1_2.Text = GVItemList.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemList.SelectedDataKey("konversi2_3")

        GVItemList.Visible = False
        GVItemList.DataSource = Nothing
        GVItemList.DataBind()

        unit_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnWhRetur.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        Dim qtyval As Double = 0.0
        If qty.Text.Trim <> "" Then
            If Double.TryParse(qty.Text.Trim, qtyval) Then
                qty.Text = Format(qtyval, "#,##0.00")
            Else
                qty.Text = Format(0.0, "#,##0.00")
            End If
        Else
            qty.Text = Format(0.0, "#,##0.00")
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            showMessage("Item tidak boleh kosong!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) = 0.0 Then
            showMessage("Qty harus lebih dari 0!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) > Double.Parse(labelmaxqty.Text.Trim) Then
            showMessage("Qty tidak boleh lebih dari stock!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) > Double.Parse(sisado.Text.Trim) Then
            showMessage("Qty tidak boleh lebih dari sisa!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable

        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("unit1", Type.GetType("System.String"))
                dtab.Columns.Add("unit2", Type.GetType("System.String"))
                dtab.Columns.Add("unit3", Type.GetType("System.String"))
                dtab.Columns.Add("note", Type.GetType("System.String"))
                dtab.Columns.Add("sisaretur", Type.GetType("System.Double"))
                dtab.Columns.Add("stock", Type.GetType("System.Double"))
                dtab.Columns.Add("trnbelidtloid", Type.GetType("System.Int32"))
                Session("itemdetail") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetail")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "' AND seq <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list !", 2)
                Exit Sub
            End If
        End If

        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow

            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = Unit.SelectedValue
            drow("unit") = Unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim
            drow("sisaretur") = Double.Parse(labeltempsisaretur.Text)
            drow("stock") = Double.Parse(labeltempstock.Text)
            drow("trnbelidtloid") = trnbelidtloid.Text

            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drow.BeginEdit()

            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = Unit.SelectedValue
            drow("unit") = Unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim
            drow("sisaretur") = Double.Parse(labeltempsisaretur.Text)
            drow("stock") = Double.Parse(labeltempstock.Text)
            drow("trnbelidtloid") = trnbelidtloid.Text

            drow.EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
        merk.Text = ""
        qty.Text = "0.00"
        labelmaxqty.Text = "0.00"
        sisado.Text = "0.00"
        Unit.Items.Clear()
        notedtl.Text = ""
        labelitemoid.Text = ""
        item.Text = ""
        I_u2.Text = "new"

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"
        trnbelidtloid.Text = ""

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new"
        labelitemoid.Text = ""
        qty.Text = "0.00"
        item.Text = ""
        labelmaxqty.Text = "0.00"
        sisado.Text = "0.00"
        Unit.Items.Clear()
        notedtl.Text = ""
        merk.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        item.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(2).Text
        qty.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(4).Text

        notedtl.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(6).Text.Replace("&nbsp;", "")
        merk.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(3).Text

        Unit.Items.Clear()
        Unit.Items.Add(GVItemDetail.SelectedDataKey("unit3"))
        Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan3")
        If GVItemDetail.SelectedDataKey("satuan2") <> GVItemDetail.SelectedDataKey("satuan3") Then
            Unit.Items.Add(GVItemDetail.SelectedDataKey("unit2"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan2")
        End If
        If GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan3") And GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan2") Then
            Unit.Items.Add(GVItemDetail.SelectedDataKey("unit1"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan1")
        End If
        Unit.SelectedValue = GVItemDetail.SelectedDataKey("satuan")

        labelsatuan1.Text = GVItemDetail.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemDetail.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemDetail.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemDetail.SelectedDataKey("unit1")
        labelunit2.Text = GVItemDetail.SelectedDataKey("unit2")
        labelunit3.Text = GVItemDetail.SelectedDataKey("unit3")

        labelkonversi1_2.Text = GVItemDetail.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemDetail.SelectedDataKey("konversi2_3")

        trnbelidtloid.Text = GVItemDetail.SelectedDataKey("trnbelidtloid")

        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text

        labeltempsisaretur.Text = Format(GVItemDetail.SelectedDataKey("sisaretur"), "#,##0.00")
        labeltempstock.Text = Format(GVItemDetail.SelectedDataKey("stock"), "#,##0.00")

        Dim maxqty As Double = 0.0
        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = Double.Parse(labeltempstock.Text) / Double.Parse(labelkonversi1_2.Text) / Double.Parse(labelkonversi2_3.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = Double.Parse(labeltempstock.Text) / Double.Parse(labelkonversi2_3.Text)
        Else
            maxqty = Double.Parse(labeltempstock.Text)
        End If
        labelmaxqty.Text = Format(maxqty, "#,##0.00")

        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi1_2.Text) / Double.Parse(labelkonversi2_3.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text)
        Else
            maxqty = Double.Parse(labeltempsisaretur.Text)
        End If
        sisado.Text = Format(maxqty, "#,##0.00")

        'Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        'conn.Open()
        'sSql = "SELECT saldoakhir from QL_crdmtr WHERE refname = 'QL_MSTITEM' AND refoid = " & Integer.Parse(labelitemoid.Text) & " AND periodacctg = '" & GetDateToPeriodAcctg(Date.Now).Trim & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND cmpcode = '" & cmpcode & "'"
        'xCmd.CommandText = sSql
        'Dim maxqty As Double = xCmd.ExecuteScalar
        'If Not maxqty = Nothing Then
        '    If GVItemDetail.SelectedDataKey("satuan") = GVItemDetail.SelectedDataKey("satuan1") Then
        '        maxqty = maxqty / GVItemDetail.SelectedDataKey("konversi1_2")
        '    End If
        '    labelmaxqty.Text = Format(maxqty, "#,##0.00")
        'Else
        '    labelmaxqty.Text = "0.00"
        'End If
        'conn.Close()

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        GVItemDetail.Columns(7).Visible = False

        unit_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""

        If fromlocation.Items.Count <= 0 Then
            errmsg &= "Lokasi asal transfer tidak boleh kosong !"
        End If
        If tolocation.Items.Count <= 0 Then
            errmsg &= "Lokasi tujuan transfer tidak boleh kosong !"
        End If
        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "Detail transfer tidak boleh kosong !"
        End If

        If errmsg.Trim <> "" Then
            showMessage(errmsg, 2)
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            Dim period As String = GetDateToPeriodAcctg(transdate).Trim

            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = 0 Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Tanggal ini tidak dalam periode Open Stock !", 2)
                Exit Sub
            End If

            'Cek saldoakhir
            If Not Session("itemdetail") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("itemdetail")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "SELECT saldoAkhir-qtybooking FROM QL_crdmtr WHERE periodacctg = '" & period & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND refoid = " & dtab.Rows(j).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql
                    saldoakhire = xCmd.ExecuteScalar
                    If saldoakhire = Nothing Or saldoakhire = 0 Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir satuan std hanya tersedia 0.00 !", 2)
                        Exit Sub
                    Else
                        If dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan3") Then
                            If saldoakhire - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                Exit Sub
                            End If
                        ElseIf dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan2") Then
                            If (saldoakhire / dtab.Rows(j).Item("konversi2_3")) - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                Exit Sub
                            End If
                        ElseIf dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan1") Then
                            If (saldoakhire / dtab.Rows(j).Item("konversi2_3") / dtab.Rows(j).Item("konversi1_2")) - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                Exit Sub
                            End If
                        End If
                    End If
                Next

                If trnstatus.Text = "POST" Then
                    'Cek quantity
                    For j As Integer = 0 To dtab.Rows.Count - 1
                        sSql = "select (case when a.unitoid = c.satuan1 then (a.qty*cast(c.konversi1_2 as decimal(18,2))*cast(c.konversi2_3 as decimal(18,2))) when a.unitoid = c.satuan2 then (a.qty*cast(c.konversi2_3 as decimal(18,2))) when a.unitoid = c.satuan3 then (a.qty) end)-(select isnull(sum(j.qty_to), 0) FROM ql_trnTrfToReturDtl j inner join ql_trnTrfToReturmst k on j.cmpcode = k.cmpcode and j.trnTrfToReturoid = k.trnTrfToReturoid where k.trnsjbelino like '%" & Tchar(nosj.Text) & "%' and j.cmpcode = a.cmpcode and j.refoid = a.refoid and j.refname = a.refname and j.refname = 'ql_mstitem')+(select isnull(sum(x.qty_to), 0) FROM ql_trnTrfFromReturDtl x inner join ql_trnTrfFromReturmst y on x.cmpcode = y.cmpcode and x.trnTrfFromReturoid = y.trnTrfFromReturoid where y.trnTrfToReturNo in (select trnTrfToReturNo from ql_trnTrfToReturmst where trnsjbelino like '%" & Tchar(nosj.Text) & "%') and x.cmpcode = a.cmpcode and x.refoid = a.refoid and x.refname = a.refname and x.refname = 'ql_mstitem' and y.status='POST') from ql_trnsjbelidtl a inner join ql_trnsjbelimst b on a.cmpcode = b.cmpcode and a.trnsjbelioid = b.trnsjbelioid inner join ql_mstitem c on a.cmpcode = c.cmpcode and a.refoid = c.itemoid and a.refname = 'ql_mstitem' where b.trnsjbelino = '" & Tchar(nosj.Text) & "' and a.refoid = " & dtab.Rows(j).Item("itemoid") & ""
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteScalar < 0 Then
                            objTrans.Rollback()
                            conn.Close()
                            showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena jumlah transfer melebihi jumlah delivery order!", 2)
                            Exit Sub
                        End If
                    Next
                End If
            End If

            'Generate crdmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

            'Generate conmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim crdmtroid As Int32 = xCmd.ExecuteScalar + 1
            Dim tempoid As Int32 = crdmtroid

            Dim trfmtrmstoid As Integer = 0
            If i_u.Text = "new" Then
                'Generate transfer master ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'ql_trnTrfToReturmst' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                trfmtrmstoid = xCmd.ExecuteScalar + 1
            End If

            'Generate transfer number
            If trnstatus.Text = "POST" Then
                Dim trnno As String = "TR/" & Format(transdate, "yy") & "/" & Format(transdate, "MM") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnTrfToReturNo, 4) AS INT)), 0) FROM ql_trnTrfToReturmst WHERE cmpcode = '" & cmpcode & "' AND trnTrfToReturNo LIKE '" & trnno & "%'"
                xCmd.CommandText = sSql
                trnno = trnno & Format(xCmd.ExecuteScalar + 1, "0000")
                transferno.Text = trnno
            Else
                If i_u.Text = "new" Then
                    transferno.Text = trfmtrmstoid.ToString
                End If
            End If

            'Generate transfer detail ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'ql_trnTrfToReturDtl' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim trfmtrdtloid As Int32 = xCmd.ExecuteScalar + 1

            'Insert new record
            If i_u.Text = "new" Then

                'Insert to master
                sSql = "INSERT INTO ql_trnTrfToReturmst (cmpcode, trnTrfToReturoid, trnTrfToReturNo, trnsjbelino, trnbelipono, trntrfdate, personoid, Trntrfnote, updtime, upduser, finalappovaldatetime, finalapprovaluser, finalapprovalcode, canceluser, canceltime, cancelnote, flag, status) VALUES ('" & cmpcode & "', " & trfmtrmstoid & ", '" & transferno.Text & "', '" & Tchar(sjoid.Text) & "', '" & Tchar(pooid.Text) & "', '" & transdate & "', 0, '" & Tchar(note.Text.Trim) & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', '', '', '1/1/1900', '', '" & rblflag.SelectedValue & "', '" & trnstatus.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_trntrfmtrmst
                sSql = "UPDATE QL_mstoid SET lastoid = " & trfmtrmstoid & " WHERE tablename = 'ql_trnTrfToReturmst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else

                sSql = "SELECT status FROM ql_trnTrfToReturmst WHERE trnTrfToReturoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                Dim srest As String = xCmd.ExecuteScalar
                If srest Is Nothing Or srest = "" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer gudang retur tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                    Exit Sub
                Else
                    If srest = "POST" Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Data transfer gudang retur tidak dapat diposting !<br />Periksa bila data telah diposting oleh user lain", 2)
                        Exit Sub
                    End If
                End If

                'Update master record
                sSql = "UPDATE ql_trnTrfToReturmst SET trnTrfToReturNo = '" & transferno.Text & "', trnsjbelino = '" & Tchar(sjoid.Text) & "', trnbelipono = '" & Tchar(pooid.Text) & "', Trntrfnote = '" & Tchar(note.Text.Trim) & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP, finalapprovaluser = '" & Session("UserID") & "', flag = '" & rblflag.SelectedValue & "', status = '" & trnstatus.Text & "' WHERE trnTrfToReturoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM ql_trnTrfToReturDtl WHERE trnTrfToReturoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0
                Dim qty_to As Double = 0.0

                For i As Integer = 0 To objTable.Rows.Count - 1

                    If i_u.Text = "edit" Then
                        trfmtrmstoid = Integer.Parse(Session("oid"))
                    End If

                    If objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan1") Then
                        unitseq = 1
                        qty_to = objTable.Rows(i).Item("qty") * objTable.Rows(i).Item("konversi1_2") * objTable.Rows(i).Item("konversi2_3")
                    ElseIf objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan2") Then
                        unitseq = 2
                        qty_to = objTable.Rows(i).Item("qty") * objTable.Rows(i).Item("konversi2_3")
                    ElseIf objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan3") Then
                        unitseq = 3
                        qty_to = objTable.Rows(i).Item("qty")
                    End If

                    sSql = "INSERT INTO ql_trnTrfToReturDtl (cmpcode, trnTrfToReturDtloid, trnTrfToReturoid, seq, refname, refoid, destination, note, mtrlocoid_from, mtrlocoid_to, unitseq, qty, unitoid, unitseq_to, qty_to, unitoid_to,trnbelidtloid) VALUES ('" & cmpcode & "', " & trfmtrdtloid & ", " & trfmtrmstoid & ", " & i + 1 & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("itemoid") & ", '', '" & Tchar(objTable.Rows(i).Item("note")) & "', " & fromlocation.SelectedValue & ", " & tolocation.SelectedValue & ", " & unitseq & ", " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan") & ", 3, " & qty_to & ", " & objTable.Rows(i).Item("satuan3") & "," & objTable.Rows(i).Item("trnbelidtloid") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If trnstatus.Text = "POST" Then

                        '--------------------------
                        Dim lasthpp As Double = 0
                        sSql = "select hpp  from ql_mstitem where itemoid=" & objTable.Rows(i).Item("itemoid") & " "
                        xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar
                        '---------------------------

                        'Update crdmtr for item out
                        sSql = "UPDATE QL_crdmtr SET qtyout = qtyout + " & qty_to & ", saldoakhir = saldoakhir - " & qty_to & ", LastTransType = 'TR', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & cmpcode & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND refoid = " & objTable.Rows(i).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & cmpcode & "', " & conmtroid & ", 'TR', '" & transdate & "', '" & period & "', '" & transferno.Text & "', " & trfmtrdtloid & ", 'ql_trnTrfToReturDtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & fromlocation.SelectedValue & ", 0, " & objTable.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & ToDouble(lasthpp) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1

                        'Update crdmtr for item in
                        sSql = "UPDATE QL_crdmtr SET qtyin = qtyin + " & qty_to & ", saldoakhir = saldoakhir + " & qty_to & ", LastTransType = 'TR', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & cmpcode & "' AND mtrlocoid = " & tolocation.SelectedValue & " AND refoid = " & objTable.Rows(i).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() <= 0 Then

                            'Insert crdmtr if no record found
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) VALUES ('" & cmpcode & "', " & crdmtroid & ", '" & period & "', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & tolocation.SelectedValue & ", " & qty_to & ", 0, 0, 0, 0, " & qty_to & ", 0, 'TR', '" & transdate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', '')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            crdmtroid += 1

                        End If

                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & cmpcode & "', " & conmtroid & ", 'TR', '" & transdate & "', '" & period & "', '" & transferno.Text & "', " & trfmtrdtloid & ", 'ql_trnTrfToReturDtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & tolocation.SelectedValue & ", " & objTable.Rows(i).Item("qty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & ToDouble(lasthpp) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1

                    End If
                    trfmtrdtloid += 1
                Next

                'Update lastoid ql_trnTrfToReturDtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & trfmtrdtloid - 1 & "  WHERE tablename = 'ql_trnTrfToReturDtl' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_crdmtr
                If tempoid <> crdmtroid Then
                    sSql = "UPDATE QL_mstoid SET lastoid =" & crdmtroid - 1 & "  WHERE tablename = 'QL_crdmtr' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnWhRetur.aspx?awal=true")
    End Sub

    Protected Sub unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unit.SelectedIndexChanged

        Dim maxqty As Double = 0.0
        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = Double.Parse(labeltempstock.Text) / Double.Parse(labelkonversi1_2.Text) / Double.Parse(labelkonversi2_3.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = Double.Parse(labeltempstock.Text) / Double.Parse(labelkonversi2_3.Text)
        Else
            maxqty = Double.Parse(labeltempstock.Text)
        End If
        labelmaxqty.Text = Format(maxqty, "#,##0.00")

        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi1_2.Text) / Double.Parse(labelkonversi2_3.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text)
        Else
            maxqty = Double.Parse(labeltempsisaretur.Text)
        End If
        sisado.Text = Format(maxqty, "#,##0.00")

        'Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        'conn.Open()

        'sSql = "SELECT a.saldoakhir, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3 from QL_crdmtr a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' WHERE a.refoid = " & Integer.Parse(labelitemoid.Text) & " AND a.periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "' AND a.mtrlocoid = " & fromlocation.SelectedValue & " AND a.cmpcode = '" & cmpcode & "'"
        'xCmd.CommandText = sSql
        'xreader = xCmd.ExecuteReader
        'Dim maxqty As Double = 0.0
        'If xreader.HasRows Then
        '    While xreader.Read
        '        If Unit.SelectedValue = xreader("satuan2") Then
        '            maxqty = xreader("saldoakhir") / xreader("konversi2_3")
        '        ElseIf Unit.SelectedValue = xreader("satuan1") Then
        '            maxqty = xreader("saldoakhir") / xreader("konversi1_2") / xreader("konversi2_3")
        '        End If
        '    End While
        'End If
        'labelmaxqty.Text = Format(maxqty, "#,##0.00")

        'conn.Close()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT status FROM ql_trnTrfToReturmst WHERE trnTrfToReturoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transfer gudang retur tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                Exit Sub
            Else
                If srest = "POST" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer gudang retur tidak dapat dihapus !<br />Periksa bila data telah diposting oleh user lain", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM ql_trnTrfToReturmst WHERE trnTrfToReturoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trnTrfToReturDtl WHERE trnTrfToReturoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnWhRetur.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        trnstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub fromlocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromlocation.SelectedIndexChanged
        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND a.genoid <> " & fromlocation.SelectedValue & " AND b.gendesc NOT LIKE '%retur%' ORDER BY a.gendesc"
        FillDDL(tolocation, sSql)

        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        I_u2.Text = "new" : labelitemoid.Text = "" : qty.Text = "0.00" : item.Text = ""
        labelmaxqty.Text = "0.00" : unit.Items.Clear()
        notedtl.Text = "" : merk.Text = "" : labelsatuan1.Text = "" : labelsatuan2.Text = ""
        labelsatuan3.Text = "" : labelunit1.Text = "" : labelunit2.Text = ""
        labelunit3.Text = "" : labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
        labelseq.Text = 1

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing

        btnClear_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        sSql = "SELECT itemoid, itemcode, itemdesc FROM QL_mstitem WHERE cmpcode = '" & cmpcode & "' AND (itemcode LIKE '%" & Tchar(Filteritem.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(Filteritem.Text.Trim) & "%') AND itemflag = 'Aktif'"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "mstitem")
        GVItemSearch.DataSource = dtab
        GVItemSearch.DataBind()

        GVItemSearch.Visible = True
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        Filteritem.Text = ""
        itemoid.Text = ""

        GVItemSearch.DataSource = Nothing
        GVItemSearch.DataBind()

        GVItemSearch.Visible = False
    End Sub

    Protected Sub GVItemSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemSearch.PageIndexChanging
        sSql = "SELECT itemoid, itemcode, itemdesc FROM QL_mstitem WHERE cmpcode = '" & cmpcode & "' AND (itemcode LIKE '%" & Tchar(Filteritem.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(Filteritem.Text.Trim) & "%') AND itemflag = 'Aktif'"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "mstitem")
        GVItemSearch.DataSource = dtab
        GVItemSearch.PageIndex = e.NewPageIndex
        GVItemSearch.DataBind()
    End Sub

    Protected Sub GVItemSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemSearch.SelectedIndexChanged
        Filteritem.Text = GVItemSearch.Rows(GVItemSearch.SelectedIndex).Cells(2).Text
        itemoid.Text = GVItemSearch.SelectedDataKey("itemoid")

        GVItemSearch.DataSource = Nothing
        GVItemSearch.DataBind()

        GVItemSearch.Visible = False
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'showMessage("Print out not available yet !", 2)
            'Exit Sub

            Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")

            Dim sWhere As String = " WHERE a.trnTrfToReturoid = " & Integer.Parse(labeloid.Text) & ""

            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "TWprintout.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", sWhere)
            report.PrintOptions.PaperSize = PaperSize.PaperA5

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TW_" & labeloid.Text & "")

            report.Close() : report.Dispose()

            Response.Redirect("trnWHTransfer.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        If Not Session("tr") Is Nothing Then
            Dim dtab As DataTable = Session("tr")
            gvMaster.DataSource = dtab
            gvMaster.PageIndex = e.NewPageIndex
            gvMaster.DataBind()
        Else
            showMessage("Missing something !", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub ibposearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposearch.Click
        binddatapo()
        GVpo.Visible = True
        GVsj.Visible = False
        GVItemList.Visible = False
    End Sub

    Protected Sub ibpoerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoerase.Click
        noref.Text = "" : pooid.Text = "" : tosupplier.Text = ""
        nosj.Text = "" : sjoid.Text = ""

        If GVpo.Visible = True Then
            GVpo.Visible = False
            GVpo.DataSource = Nothing
            GVpo.DataBind()
            Session("polistwhretur") = Nothing
        End If

        If GVsj.Visible = True Then
            GVsj.Visible = False
            GVsj.DataSource = Nothing
            GVsj.DataBind()
            Session("sjlistwhretur") = Nothing
        End If

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing

        btnClear_Click(Nothing, Nothing)
    End Sub

    Private Sub bindDataPO()
        sSql = "select TOP 100 a.trnbelipono, a.trnbelipodate, b.suppname from QL_pomst a inner join QL_mstsupp b on a.cmpcode = b.cmpcode and trnsuppoid = b.suppoid inner join ql_trnsjbelimst c on a.cmpcode = c.cmpcode and a.trnbelipono = c.trnbelipono where a.cmpcode = '" & cmpcode & "' and a.trnbelistatus = 'approved' and a.trnbelitype = 'grosir' and (a.trnbelipono like '%" & Tchar(noref.Text) & "%' or b.suppname like '%" & Tchar(noref.Text) & "%') group by a.trnbelipono, a.trnbelipodate, b.suppname"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "polist")
        GVpo.DataSource = dtab
        GVpo.DataBind()

        Session("polistwhretur") = dtab
    End Sub

    Protected Sub GVpo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVpo.PageIndexChanging
        If Not Session("polistwhretur") Is Nothing Then
            GVpo.DataSource = Session("polistwhretur")
            GVpo.PageIndex = e.NewPageIndex
            GVpo.DataBind()
        End If
    End Sub

    Protected Sub GVpo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVpo.SelectedIndexChanged
        noref.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        pooid.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        tosupplier.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(3).Text

        GVpo.Visible = False
        GVpo.DataSource = Nothing
        GVpo.DataBind()
        Session("polistwhretur") = Nothing

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub ibsjsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsjsearch.Click
        noref.Text = pooid.Text
        bindDataSJ()
        GVsj.Visible = True

        GVpo.Visible = False
        GVItemList.Visible = False
    End Sub

    Protected Sub ibsjerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsjerase.Click
        nosj.Text = ""
        sjoid.Text = ""

        If GVsj.Visible = True Then
            GVsj.Visible = False
            GVsj.DataSource = Nothing
            GVsj.DataBind()
            Session("sjlistwhretur") = Nothing
        End If

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing

        btnClear_Click(Nothing, Nothing)
    End Sub

    Private Sub bindDataSJ()
        sSql = "select distinct a.trnbelipono, a.trnsjbelino, a.trnsjbelidate, a.trnsjbelioid, c.suppname from ql_trnsjbelimst a inner join QL_pomst b on a.cmpcode = b.cmpcode and a.trnbelipono = b.trnbelipono inner join QL_mstsupp c on b.cmpcode = c.cmpcode and b.trnsuppoid = c.suppoid where a.cmpcode = '" & cmpcode & "' and a.trnsjbelistatus in ('approved','invoiced') and a.trnsjbelitype in ('po','bo') and a.trnsjbelino like '%" & Tchar(nosj.Text) & "%' and a.trnbelipono like '%" & Tchar(pooid.Text) & "%' and ((select sum(case when j.unitoid = k.satuan1 then (j.qty*cast(k.konversi1_2 as decimal(18,2))*cast(k.konversi2_3 as decimal(18,2))) when j.unitoid = k.satuan2 then (j.qty*cast(k.konversi2_3 as decimal(18,2))) when j.unitoid = k.satuan3 then (j.qty) end) from ql_trnsjbelidtl j inner join ql_mstitem k on j.cmpcode = k.cmpcode and j.refoid = k.itemoid and j.refname = 'ql_mstitem' where j.cmpcode = a.cmpcode and j.trnsjbelioid = a.trnsjbelioid)-(select isnull(sum(m.qty_to), 0) from ql_trnTrfToReturDtl m inner join ql_trnTrfToReturmst n on m.cmpcode = n.cmpcode and m.trnTrfToReturoid = n.trnTrfToReturoid where n.cmpcode = a.cmpcode and n.trnsjbelino = a.trnsjbelino)+(select isnull(sum(x.qty_to), 0) from ql_trnTrffromReturDtl x inner join ql_trnTrffromReturmst y on x.cmpcode = y.cmpcode and x.trnTrfFromReturoid = y.trnTrfFromReturoid where y.cmpcode = a.cmpcode and y.trnTrfToReturNo in (select trnTrfToReturNo from ql_trnTrfToReturmst where trnsjbelino = a.trnsjbelino) and y.status='POST')) > 0  order by a.trnsjbelino"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "sjlist")
        GVsj.DataSource = dtab
        GVsj.DataBind()

        Session("sjlistwhretur") = dtab
    End Sub

    Protected Sub GVsj_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVsj.PageIndexChanging
        If Not Session("sjlistwhretur") Is Nothing Then
            GVsj.DataSource = Session("sjlistwhretur")
            GVsj.PageIndex = e.NewPageIndex
            GVsj.DataBind()
        End If
    End Sub

    Protected Sub GVsj_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsj.SelectedIndexChanged
        nosj.Text = GVsj.Rows(GVsj.SelectedIndex).Cells(1).Text
        sjoid.Text = GVsj.Rows(GVsj.SelectedIndex).Cells(1).Text
        noref.Text = GVsj.Rows(GVsj.SelectedIndex).Cells(2).Text
        pooid.Text = GVsj.Rows(GVsj.SelectedIndex).Cells(2).Text
        tosupplier.Text = GVsj.SelectedDataKey("suppname")

        GVsj.Visible = False
        GVsj.DataSource = Nothing
        GVsj.DataBind()
        Session("sjlistwhretur") = Nothing

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub
#End Region
End Class
