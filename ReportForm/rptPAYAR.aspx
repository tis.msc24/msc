<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPAYAR.aspx.vb" Inherits="RptPiutang" Title="PT. Multi Sarana Computer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" colspan="2" valign="center">
                <asp:Label ID="lblHeader" runat="server" CssClass="Title" Font-Bold="True" Font-Size="X-Large"
                    ForeColor="Navy" Text=".: Laporan Piutang" Width="262px"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" colspan="2" style="background-image: none; background-color: #ffffff"
                valign="center">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" DefaultButton="btnSearchSupplier" __designer:wfdid="w78"><TABLE><TBODY><TR><TD align=left><asp:Label id="Label7" runat="server" Text="Type" __designer:wfdid="w79"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterType" runat="server" CssClass="inpText" __designer:wfdid="w80" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>KARTU PIUTANG</asp:ListItem>
<asp:ListItem>DETAIL PIUTANG</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td1" align=left runat="server" visible="true"><asp:Label id="Label9" runat="server" Text="Branch" __designer:wfdid="w81"></asp:Label> </TD><TD id="td2" align=left runat="server" visible="true">:</TD><TD align=left><asp:DropDownList id="dd_branch" runat="server" Width="180px" CssClass="inpText" __designer:wfdid="w82" AutoPostBack="True"></asp:DropDownList> </TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w83"></asp:Label></TD><TD class="Label" align=center><asp:Label id="Label11" runat="server" Text=":" __designer:wfdid="w84"></asp:Label></TD><TD align=left><asp:TextBox id="txtStart" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w85"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w86"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w87"></asp:Label> <asp:TextBox id="txtFinish" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w88"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w89"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(DD/MM/yyyy)" __designer:wfdid="w90"></asp:Label>&nbsp;<asp:DropDownList id="FilterDDLYear3" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w91" Visible="False"></asp:DropDownList><asp:DropDownList id="FilterDDLMonth3" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w92" Visible="False"></asp:DropDownList>&nbsp;&nbsp;</TD></TR><TR><TD style="HEIGHT: 15px" align=left><asp:Label id="Label8" runat="server" Text="Period" __designer:wfdid="w93" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" class="Label" align=center><asp:Label id="Label10" runat="server" Text=":" __designer:wfdid="w94" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><asp:DropDownList id="FilterDDLMonth" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w95" Visible="False"></asp:DropDownList>&nbsp;<asp:DropDownList id="FilterDDLYear" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w96" Visible="False"></asp:DropDownList>&nbsp;<asp:Label id="Label5" runat="server" Text="-" __designer:wfdid="w97" Visible="False"></asp:Label>&nbsp;<asp:DropDownList id="FilterDDLMonth2" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w98" Visible="False"></asp:DropDownList>&nbsp;<asp:DropDownList id="FilterDDLYear2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w99" Visible="False"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="Customer" __designer:wfdid="w100"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><asp:RadioButtonList id="rbSupplier" runat="server" __designer:wfdid="w101" AutoPostBack="True" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Selected="True">ALL</asp:ListItem>
                                                                                                    <asp:ListItem>SELECT</asp:ListItem>
                                                                                                </asp:RadioButtonList></TD><TD align=left><asp:TextBox id="FilterTextSupplier" runat="server" Width="175px" CssClass="inpText" __designer:wfdid="w102" Visible="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupplier" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w103" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w104" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE> </TD></TR><TR><TD id="TD6" align=left runat="server" Visible="false"><asp:Label id="Label1" runat="server" Text="Sort By" __designer:wfdid="w106"></asp:Label></TD><TD id="TD8" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD7" align=left runat="server" Visible="false"><asp:DropDownList id="DDLFilter" runat="server" CssClass="inpText" __designer:wfdid="w107" Visible="False"><asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="periodacctg,transdate">Tanggal</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="DDLFilterSum" runat="server" CssClass="inpText" __designer:wfdid="w108"><asp:ListItem Value="custname">Customer</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="DDLFilter2" runat="server" CssClass="inpText" __designer:wfdid="w109"><asp:ListItem Value="Asc">Ascending</asp:ListItem>
<asp:ListItem Value="Desc">Descending</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="custoid" runat="server" __designer:wfdid="w110" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD align=left><asp:Panel id="pnlSupplier" runat="server" Width="100%" __designer:wfdid="w111"><asp:GridView id="gvSupplier" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w112" Visible="False" OnPageIndexChanging="gvSupplier_PageIndexChanging" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><EditItemTemplate>
                                                                                                    <asp:CheckBox ID="CheckBox1" runat="server" __designer:wfdid="w3"></asp:CheckBox>
                                                                                                
</EditItemTemplate>
<ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkSelect" runat="server" ToolTip='<%# Eval("custoid") %>' Checked='<%# Eval("selected") %>' __designer:wfdid="w2"></asp:CheckBox>
                                                                                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                            <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label>
                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD id="TD3" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Width="93px" Text="Currency" __designer:wfdid="w113"></asp:Label></TD><TD id="TD4" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD5" align=left runat="server" Visible="false"><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText" __designer:wfdid="w114"></asp:DropDownList> <asp:CheckBox id="chkEmpty" runat="server" Text="Hide Customer with 0 A/R amount" __designer:wfdid="w105" Visible="False" Checked="True"></asp:CheckBox></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w115"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w116"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w117"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w118"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w119" TargetControlID="txtStart" Format="dd/MM/yyyy" PopupButtonID="imbStart"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" __designer:wfdid="w120" TargetControlID="txtStart" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceFin" runat="server" __designer:wfdid="w121" TargetControlID="txtFinish" Format="dd/MM/yyyy" PopupButtonID="imbFinish"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" __designer:wfdid="w122" TargetControlID="txtFinish" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="HEIGHT: 75px" vAlign=top align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w4" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="imgReportForm" runat="server" Width="80px" ImageUrl="~/Images/loading_animates.gif" __designer:dtid="844424930132046" __designer:wfdid="w6"></asp:Image><BR __designer:dtid="844424930132047" /></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" __designer:dtid="1125899906842646" __designer:wfdid="w125" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</ContentTemplate>
                                                        <Triggers>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</Triggers>
                                                    </asp:UpdatePanel>
                <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w110" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w111"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w112"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w113"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w114"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w115" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w116" Visible="False"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

