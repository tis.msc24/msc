<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trntcSupplier.aspx.vb" Inherits="trnTCSupplier" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".:Kembali Supplier" CssClass="Title" ForeColor="Navy" Font-Names="Verdana" Font-Size="21px"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
               <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                 <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Kembali Supplier :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 100px" class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="drCabang" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w24"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 100px" class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="sFilteNo" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w25"><asp:ListItem Value="c.trftcserviceno">No. TC</asp:ListItem>
<asp:ListItem Value="tw.TrfwhserviceNo">No. TW</asp:ListItem>
<asp:ListItem Value="sp.suppname">Supplier</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w26" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:CheckBox id="CbTanggal" runat="server" Width="77px" Text="Tanggal" __designer:wfdid="w27"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w28"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w30"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w32"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=left>Status</TD><TD align=left>: </TD><TD align=left colSpan=4><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" __designer:wfdid="w33"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w34">
                                                            </asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w35">
                                                            </asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w36" DefaultButton="btnSearch"><asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w37" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8" DataKeyNames="trntcserviceoid,branch_code" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trftcserviceno" HeaderText="No. TC">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntcservicedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal TC">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TrfwhserviceNo" HeaderText="No. TW">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FromBranch" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntcstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
&nbsp;<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" Width="53px" __designer:wfdid="w1" ToolTip='<%# eval("trntcserviceoid") %>'>Print Out</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="trntcserviceoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w38" TargetControlID="tgl1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w39" TargetControlID="tgl2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w40" TargetControlID="tgl1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w41" TargetControlID="tgl2" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Width="82px" Font-Bold="True" Text="Information :" __designer:wfdid="w118" Font-Underline="True"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="TujuanCb" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w119" AutoPostBack="True"></asp:DropDownList> <asp:Label id="OidTC" runat="server" __designer:wfdid="w120" Visible="False"></asp:Label> <asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" __designer:wfdid="w121" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Width="93px" Text="No. TC Supplier" __designer:wfdid="w122"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" __designer:wfdid="w123" MaxLength="20" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left>Tanggal</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" __designer:wfdid="w124" Enabled="False" ValidationGroup="MKE"></asp:TextBox> <asp:ImageButton id="imbTwDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w125" Enabled="False"></asp:ImageButton> <asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w126"></asp:Label></TD></TR><TR><TD class="Label" align=left>Supplier</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="SuppName" runat="server" Width="200px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w127"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgBtnSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w128"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImgBtnErase" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w129"></asp:ImageButton>&nbsp;<asp:Label id="SuppOid" runat="server" __designer:wfdid="w130" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="labelnotr" runat="server" Width="75px" Text="No.  Transfer" __designer:wfdid="w131"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="noref" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w132" MaxLength="200"></asp:TextBox>&nbsp;<asp:ImageButton id="ibposearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w133"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibpoerase" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w134"></asp:ImageButton><asp:Label id="pooid" runat="server" __designer:wfdid="w135" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="trfmtrmstoid" runat="server" __designer:wfdid="w136" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVSupp" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w137" Visible="False" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="6" DataKeyNames="suppoid,suppcode,suppname,suppaddr" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Supp. Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp. Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Alamat" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD class="Label" align=left><asp:Label id="TypeGudang" runat="server" __designer:wfdid="w138" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVpo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w139" Visible="False" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="6" DataKeyNames="Trfwhserviceoid,Trfwhserviceno,FromMtrLocOid,FromBranch,Trfwhservicedate,TypeGudang" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="Trfwhserviceno" HeaderText="Transfer No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Trfwhservicedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Trfwhservicenote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="TypeGudang" HeaderText="Type Gudang" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                    <asp:Label ID="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left><asp:Label id="labelfromloc" runat="server" Text="Location" __designer:wfdid="w140"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="fromlocation" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w141" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="trnstatus" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w142" MaxLength="20" Enabled="False">IN PROCESS</asp:TextBox></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="note" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w143" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w144" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD id="TD1" class="Label" align=left runat="server" Visible="false"><asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" __designer:wfdid="w145" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Data Detail :" __designer:wfdid="w146" Font-Underline="True"></asp:Label>&nbsp;<asp:Label id="ReqDtlOid" runat="server" __designer:wfdid="w147" Visible="False"></asp:Label> <asp:Label id="ReqOid" runat="server" __designer:wfdid="w148" Visible="False"></asp:Label> <asp:Label id="TrfWhDtlOid" runat="server" __designer:wfdid="w149" Visible="False"></asp:Label> <asp:Label id="UnitOid" runat="server" __designer:wfdid="w150" Visible="False"></asp:Label> <asp:Label id="labelseq" runat="server" __designer:wfdid="w151" Visible="False"></asp:Label><asp:Label id="labelitemoid" runat="server" __designer:wfdid="w152" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Katalog" __designer:wfdid="w153"></asp:Label> <asp:Label id="Label26" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w154"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="300px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" __designer:wfdid="w155" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w156"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w157"></asp:ImageButton> </TD></TR><TR><TD class="Label" align=left>Qty <asp:Label id="Label28" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w158"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox style="TEXT-ALIGN: justify" id="qty" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w159" AutoPostBack="True" MaxLength="10" ValidationGroup="MKE"></asp:TextBox>&nbsp;<asp:Label id="Label30" runat="server" CssClass="label" Text="Max :" __designer:wfdid="w160"></asp:Label>&nbsp;<asp:Label id="MaxQty" runat="server" ForeColor="Red" Text="0.00" __designer:wfdid="w161"></asp:Label>&nbsp;<asp:DropDownList id="unit" runat="server" Width="95px" CssClass="inpText" __designer:wfdid="w162" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Keterangan</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" __designer:wfdid="w163" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left colSpan=2></TD><TD class="Label" align=left><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w164"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w165"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w166" CellPadding="4" AutoGenerateColumns="False" GridLines="None" DataKeyNames="seq,itemoid,itemdesc,trntcserviceqty,reqdtloid,reqoid,satuan,trntcnotedtl,trfwhserviceoid,trfwhservicedtloid,trntcservicedtloid">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntcserviceqty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntcnotedtl" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="True" CssClass="gvhdr" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server" __designer:wfdid="w1"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trfwhserviceoid" HeaderText="trfwhserviceoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trntcservicedtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan" HeaderText="satuan" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w167"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w168"></asp:Label></TD></TR><TR><TD style="HEIGHT: 25px" class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w169"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w170"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w171" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w172" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w173" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w174"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="ceTwDate" runat="server" __designer:wfdid="w175" TargetControlID="transferdate" Format="dd/MM/yyyy" PopupButtonID="imbTwDate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="TwDate" runat="server" __designer:wfdid="w176" TargetControlID="transferdate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" __designer:wfdid="w177" TargetControlID="qty" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel8" runat="server">
        <contenttemplate>
<asp:Panel id="panelItem" runat="server" CssClass="modalBox" __designer:wfdid="w179" Visible="False" DefaultButton="imbFindItem"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptItem" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Barang" __designer:wfdid="w180"></asp:Label></TD></TR><TR><TD align=center>Filter : <asp:DropDownList id="DDLFilterItem" runat="server" CssClass="inpText" __designer:wfdid="w181"><asp:ListItem Value="itemdesc">Deskripsi</asp:ListItem>
<asp:ListItem>Itemcode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterItem" runat="server" CssClass="inpText" __designer:wfdid="w182"></asp:TextBox> <asp:ImageButton id="imbFindItem" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w183"></asp:ImageButton> <asp:ImageButton id="imbViewItem" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w184"></asp:ImageButton></TD></TR><TR><TD align=center><DIV style="OVERFLOW-Y: scroll; WIDTH: 800px; HEIGHT: 184px; BACKGROUND-COLOR: beige"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w3" Visible="False" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" __designer:wfdid="w30" ToolTip='<%# eval("trfwhservicedtloid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtyTW" DataFormatString="{0:#,##0.00}" HeaderText="Qty TW">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("Qty") %>' __designer:wfdid="w28" MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" __designer:wfdid="w32" TargetControlID="tbQty" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="QtyTc" DataFormatString="{0:#,##0.00}" HeaderText="Qty TC">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trfwhservicedtlnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center><asp:LinkButton id="lbAddToListMat" runat="server" __designer:wfdid="w186">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseItem" runat="server" __designer:wfdid="w187">[ Close ]</asp:LinkButton></TD></TR><TR><TD align=center><asp:Label id="sValidasi" runat="server" __designer:wfdid="w188" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" __designer:wfdid="w189" TargetControlID="btnHideItem" PopupDragHandleControlID="lblCaptItem" BackgroundCssClass="modalBackground" PopupControlID="panelItem"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideItem" runat="server" __designer:wfdid="w190" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Kembali Supplier :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
        <tr>
            <td align="center">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w13" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w14"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w15"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w16"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w17"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w18" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w19" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

