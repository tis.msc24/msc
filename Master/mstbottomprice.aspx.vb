Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_bottomprice
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Private cRate As New ClassRate
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2, "")
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2, "")
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2, "")
            Return False
        End If
        Return True
    End Function

    Public Function GetSelected() As String
        Return Eval("selected")
    End Function

    Public Function GetTransID() As String
        Return (Eval("cmpcode") & "," & Eval("resfield1"))
    End Function

    Public Function GetAdjQty() As String
        Return ToMaskEdit(ToDouble(Eval("presentase")), 3)
    End Function

    Private Function GetCheckBoxValue(ByVal gvTarget As GridView, ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim bReturn As Boolean = False
        Dim row As System.Web.UI.WebControls.GridViewRow = gvTarget.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    bReturn = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                End If
            Next
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function GetDDLItem(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    If CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Count > 0 Then
                        sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                    End If
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function UpdateDetailData() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCrd") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCrd")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvFindCrd.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(C1)
                    dtView.RowFilter = "seq=" & row.Cells(0).Text
                    Dim presentase As Double = ToDouble(GetTextBoxValue(C1, 5))
                    If presentase <> ToDouble(dtView(0)("hpp")) Or presentase <> 0.0 Then
                        dtView(0)("presentase") = ToDouble(GetTextBoxValue(C1, 5))
                        dtView(0)("note") = GetTextBoxValue(C1, 6)
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl = dtView.ToTable
                Session("TblCrd") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Sub bindkatalog(ByVal sWhere As String)
        Try
            Dim periodeacctg As String = GetPeriodAcctg(Format(GetServerTime, "MM/dd/yyyy"))

            sSql = "SELECT DISTINCT 0 as mtrwhoid,m.satuan2, 0.0 stockvalueidr, 0.0 stockvalueusd,0 seq, m.itemoid refoid, itemcode matcode, itemdesc matlongdesc, m.hpp, 0.0 AS presentase, '' AS location, '' note ,'False' selected,0 seq, bottomprice FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.genoid = m.satuan1 AND g.cmpcode = m.cmpcode WHERE m.cmpcode = 'MSC' " & sWhere & " ORDER BY m.itemoid"
            'End If
            Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "bottomprice")
            For C1 As Integer = 0 To tbDtl.Rows.Count - 1
                tbDtl.Rows.Item(C1)("seq") = C1 + 1
            Next
            Session("TblCrd") = tbDtl
            gvFindCrd.DataSource = tbDtl
            gvFindCrd.DataBind()
            lblCount.Text = tbDtl.Rows.Count & " data found."
            imbAddToList.Visible = (tbDtl.Rows.Count > 0)
        Catch ex As Exception
            showMessage(ex.ToString, 2, "")
        End Try
    End Sub

#End Region

#Region "Procedures"
    Private Sub InitAllDDL()

    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnmrrawdtl")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("reflongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("hpp", Type.GetType("System.Double"))
        dtlTable.Columns.Add("presentase", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bottomprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("newbottomprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("note", Type.GetType("System.String"))
        dtlTable.Columns.Add("createuser", Type.GetType("System.String"))
        dtlTable.Columns.Add("createtime", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("stockvalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockvalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("unitoid", Type.GetType("System.Int32"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ResetFind()
        Session("TblCrd") = Nothing
        gvFindCrd.DataSource = Nothing
        gvFindCrd.DataBind()
        lblCount.Text = "" : imbAddToList.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvList.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next

                    Dim sID() As String = sOid.Split(",")
                    dv.RowFilter = "cmpcode='" & sID(0) & "' AND resfield1='" & sID(1) & "'"
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindData(ByVal sSQLPlus As String)
        sSql = "SELECT DISTINCT '' locdesc,s.cmpcode,CAST(resfield1 AS INTEGER) resfield1,'' AS bottompriceno,CONVERT(VARCHAR(10),bottompricedate,101) AS bottompricedate,COUNT(-1) AS bottompricecount,bottompricestatus,'False' AS checkvalue FROM QL_mstbottomprice s Where s.cmpcode = '" & CompnyCode & "' " & sSQLPlus & "  group by s.cmpcode, resfield1, bottompriceno, bottompricedate, bottompricestatus order by resfield1 desc"
        Session("TblMst") = cKon.ambiltabel(sSql, "ql_mstbottomprice")
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox(ByVal sNo As String)
        Dim sID() As String = sNo.Split(",")
        If sID.Length < 2 Then
            showMessage("Invalid Transaction ID selection.", 3, "REDIR")
        Else
            Dim sMsg As String = ""
            sSql = "select branch_code,bottompriceoid, bottompricestatus, bottompricedate, resfield1, createuser, createtime,upduser, updtime from ql_mstbottomprice WHERE cmpcode='" & sID(0) & "' AND resfield1='" & sID(1) & "'"
            Dim tbHdr As DataTable = cKon.ambiltabel(sSql, "Mst")
            If tbHdr.Rows.Count < 1 Then
                sMsg &= "- Can't load transaction header data.<BR>"
            Else
                'busunit.SelectedValue = tbHdr.Rows(0)("cmpcode").ToString
                'InitWH(ddlType.SelectedValue)
                resfield1.Text = tbHdr.Rows(0)("resfield1").ToString
                bottompricedate.Text = Format(CDate(tbHdr.Rows(0)("bottompricedate").ToString), "MM/dd/yyyy")
                createuser.Text = tbHdr.Rows(0)("createuser").ToString
                createtime.Text = tbHdr.Rows(0)("createtime").ToString
                upduser.Text = tbHdr.Rows(0)("upduser").ToString
                updtime.Text = tbHdr.Rows(0)("updtime").ToString
                bottompricestatus.Text = tbHdr.Rows(0)("bottompricestatus").ToString
            End If

            sSql = "SELECT 0 seq, a.refoid, 'refcode'=(SELECT m.itemCode FROM QL_mstitem m WHERE m.itemoid=a.refoid), 'reflongdesc'=(SELECT m.itemdesc FROM QL_mstitem m WHERE m.itemoid=a.refoid), a.hpp, a.presentase,newbottomprice AS bottomprice, newbottomprice, a.bottompricenote note, a.createuser, a.createtime FROM ql_mstbottomprice a WHERE a.cmpcode='" & sID(0) & "' AND a.resfield1='" & sID(1) & "' ORDER BY a.bottompriceoid"
            Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "ql_mstbottomprice")
            If tbDtl.Rows.Count < 1 Then
                sMsg &= "- Can't load transaction detail data.<BR>"
            Else
                For R1 As Integer = 0 To tbDtl.Rows.Count - 1
                    tbDtl.Rows(R1)("seq") = R1 + 1
                Next
                tbDtl.AcceptChanges()
                Session("TblDtl") = tbDtl : gvDtl.DataSource = Session("TblDtl") : gvDtl.DataBind()
            End If

            If sMsg <> "" Then
                showMessage(sMsg, 2, "")
            Else
                If bottompricestatus.Text <> "In Process" Then
                    lblNo.Text = "Draft No"
                    resfield1.Visible = True : bottompriceno.Visible = False
                    imbFind.Visible = False : imbSave.Visible = False
                    imbPosting.Visible = False : imbDelete.Visible = False
                    gvDtl.Columns(7).Visible = False
                Else
                    lblNo.Text = "Draft No"
                    resfield1.Visible = True : bottompriceno.Visible = False
                    imbFind.Visible = True : imbSave.Visible = True
                    imbPosting.Visible = True : imbDelete.Visible = True
                    gvDtl.Columns(7).Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer, ByVal sState As String)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        Dim sAsliTemp As String = sMessage.Replace("<br />", vbCrLf).Replace("<BR>", vbCrLf)
        Dim sTemp() As String = sAsliTemp.Split(vbCrLf)
        If sTemp.Length > 25 Then
            lblPopUpMsg.Text = "<textarea class='inpText' readonly='true' style='height:250px;width:99%;'>" & sAsliTemp & "</textarea>"
        Else
            lblPopUpMsg.Text = sMessage
        End If
        lblCaption.Text = strCaption : lblState.Text = sState
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub GenerateDraftNo()
        sSql = "SELECT ISNULL(MAX(CAST(ISNULL(resfield1,0) AS INTEGER)) + 1, 1) AS IDNEW FROM ql_mstbottomprice"
        resfield1.Text = ToDouble(cKon.ambilscalar(sSql).ToString)
    End Sub

    Private Sub GenerateNo()
        Dim sNo As String = "BP-" & Format(GetServerTime, "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(bottompriceno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_bottomprice WHERE cmpcode='" & CompnyCode & "' AND bottompriceno LIKE '" & sNo & "%'"
        bottompriceno.Text = GenNumberString(sNo, "", ToDouble(cKon.ambilscalar(sSql).ToString), DefaultFormatCounter)
    End Sub

    Private Sub PrintReport()
        Try
            Dim sWhere As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sWhere &= IIf(sWhere = "", " WHERE ", " OR ") & " (a.cmpcode='" & dv(C1)("cmpcode").ToString & "' AND a.resfield1='" & dv(C1)("resfield1").ToString & "') "
                Next
                dv.RowFilter = ""
            End If

            report.Load(Server.MapPath(folderReport & "crPrintAdj.rpt"))

            report.SetParameterValue("swhere", sWhere)
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
             System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "stockadjustment_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            Catch ex As Exception
                showMessage(ex.Message, 1, "")
                report.Close()
                report.Dispose()
            End Try
        Catch ex As Exception
            showMessage(ex.Message, 1, "")
        End Try
    End Sub

    Private Sub InitWH(ByVal Type As String)
        sSql = "SELECT genoid,gendesc FROM QL_mstgen WHERE activeflag='ACTIVE' AND genoid>0 AND gengroup='WAREHOUSE' "
        sSql &= " ORDER BY gendesc"
        'FillDDL(FilterWarehouse, sSql)
    End Sub

    Private Sub InitDDLReason()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='ADJUSMENT REASON' AND activeflag='ACTIVE' AND cmpcode='" & CompnyCode & "'"
        For i As Integer = 1 To gvFindCrd.Rows.Count
            Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(i - 1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(6).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                        FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), sSql)
                        If CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Count = 0 Then
                            showMessage("Please input ADJUSTMENT REASON from MASTER GENERAL first xxx", 2, "")
                            Exit Sub
                        End If
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub AddToJurnal(ByVal dtJurnal As DataTable, ByVal iAcctgOid As Integer, ByVal sDbCr As String, ByVal dAmt As Decimal, ByVal dAmtIDR As Decimal, ByVal dAmtUSD As Decimal, ByVal iRefOid As String)
        Dim nuRow As DataRow = dtJurnal.NewRow
        nuRow("acctgoid") = iAcctgOid
        nuRow("gldbcr") = sDbCr
        nuRow("glamt") = dAmt
        nuRow("glamtidr") = dAmtIDR
        nuRow("glamtusd") = dAmtUSD
        nuRow("refoid") = iRefOid
        dtJurnal.Rows.Add(nuRow)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~/Other/login.aspx")
        If Request.QueryString("awal") = "true" Then
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            ' Simpan session ke variabel temporary supaya tidak hilang

            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstbottomprice.aspx")
        End If
        If checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Bottom Price"
        Session("oid") = Request.QueryString("oid")

        imbPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to posting this data?');")
        imbDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            InitAllDDL() : BindData("")
            If (Session("oid") <> Nothing And Session("oid") <> "") Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
                FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
                createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
                bottompricedate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                GenerateDraftNo()
                TabContainer1.ActiveTabIndex = 0
            End If
            Dim dt As New DataTable : dt = Session("TblDtl")
            gvDtl.DataSource = dt : gvDtl.DataBind()
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        If lblState.Text = "REDIR" Then
            Response.Redirect("~\Master\mstbottomprice.aspx?awal=true")
        Else
            cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        End If
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvFindCrd.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(8).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        'UpdateDetailData()
    End Sub

    Protected Sub imbAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAddToList.Click
        Try
            UpdateDetailData()
            Dim sMsg As String = ""
            If Session("TblDtl") Is Nothing Then CreateTblDetail()

            If Session("TblCrd") Is Nothing Then
                sMsg &= "- No data to be added.<BR>"
            Else
                Dim dtCek As DataTable = Session("TblCrd")
                If dtCek.Rows.Count < 1 Then
                    sMsg &= "- No data to be added.<BR>"
                Else

                    Dim dvCek As DataView = dtCek.DefaultView
                   
                    dvCek.RowFilter = ""
                    dvCek.RowFilter = "presentase <> 0"

                    'If dvCek(0)("presentase") > 100 Then
                    '    sMsg &= "Presentase Tidak Boleh Lebih dari 100 %..!!<br>"
                    'End If

                    If dvCek.Count < 1 Then
                        sMsg &= "- Failed to Add to List data: "
                        dvCek.RowFilter = ""
                    Else
                        Dim dtDtlCek As DataTable = Session("TblDtl")
                        Dim dvDtlCek As DataView = dtDtlCek.DefaultView

                        Dim sItem As String = ""
                        For R1 As Integer = 0 To dvCek.Count - 1
                            dvDtlCek.RowFilter = "refoid=" & dvCek(R1)("refoid").ToString & ""
                            If dvDtlCek.Count > 0 Then
                                sItem &= "# Code = " & dvCek(R1)("matcode").ToString & " <BR>"
                            End If
                            dvDtlCek.RowFilter = ""
                        Next
                        If sItem <> "" Then
                            sMsg &= "- Following " & dvCek(0)("refoid") & "-" & dvCek(0)("matlongdesc") & " has been added before:<BR>" & sItem
                        End If
                    End If
                End If
            End If

            Dim dtCrd As DataTable = Session("TblCrd")
            Dim dvCrd As DataView = dtCrd.DefaultView
            dvCrd.RowFilter = "presentase <> 0"
            If dvCrd.Count < 1 Then
                sMsg &= "Failed to Add to List data: " & dvCrd.Count
                dvCrd.RowFilter = ""
                Exit Sub
            End If

            If sMsg <> "" Then
                showMessage(sMsg, 2, "") : Exit Sub
            End If

            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            For R1 As Integer = 0 To dvCrd.Count - 1
                Dim nuRow As DataRow = dtDtl.NewRow
                nuRow("seq") = dtDtl.Rows.Count + 1
                nuRow("refoid") = dvCrd(R1)("refoid").ToString
                nuRow("refcode") = dvCrd(R1)("matcode").ToString
                nuRow("reflongdesc") = dvCrd(R1)("matlongdesc").ToString
                nuRow("hpp") = ToDouble(dvCrd(R1)("hpp"))
                nuRow("presentase") = ToDouble(dvCrd(R1)("presentase"))
                nuRow("bottomprice") = ToDouble(dvCrd(R1)("bottomprice"))
                nuRow("newbottomprice") = ToDouble(dvCrd(R1)("hpp")) + (ToDouble(dvCrd(R1)("hpp")) * (ToDouble(dvCrd(R1)("presentase")) / 100))
                nuRow("note") = dvCrd(R1)("note").ToString
                nuRow("createuser") = Session("UserID")
                nuRow("createtime") = GetServerTime()
                dtDtl.Rows.Add(nuRow) 
            Next
            dvCrd.RowFilter = ""

            ResetFind()
            Session("TblDtl") = dtDtl
            gvDtl.DataSource = dtDtl : gvDtl.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString, 2, "")
        End Try

    End Sub 

    Protected Sub lkbDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
        Dim dvDel As DataView = dtDtl.DefaultView
        dvDel.RowFilter = "seq=" & sender.ToolTip
        If dvDel.Count > 0 Then
            dvDel(0).Delete()
        End If
        dvDel.RowFilter = ""
        dtDtl.AcceptChanges()

        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
            dtDtl.Rows.Item(C1)("seq") = C1 + 1
        Next

        Session("TblDtl") = dtDtl
        gvDtl.DataSource = dtDtl : gvDtl.DataBind()
    End Sub

    Protected Sub imbPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPosting.Click
        bottompricestatus.Text = "Post"
        imbSave_Click(sender, e)
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim sMsg As String = "" : Dim sErr As String = ""
        If Not IsValidDate(bottompricedate.Text, "MM/dd/yyyy", sErr) Then
            sMsg &= "- Invalid Bottom Price Date: " & sErr & "<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- No detail Bottom Price data.<BR>"
        Else
            Dim dtCek As DataTable = Session("TblDtl")
            If dtCek.Rows.Count < 1 Then
                sMsg &= "- No detail Bottom Price data.<BR>"
            End If
        End If

        If sMsg <> "" Then
            bottompricestatus.Text = "In Process"
            showMessage(sMsg, 2, "") : Exit Sub
        End If

        Dim sDraftNo As String = resfield1.Text : Dim sPostNo As String = bottompriceno.Text
        If Session("oid") = Nothing Or Session("oid") = "" Then
            sSql = "SELECT COUNT(*) FROM ql_mstbottomprice WHERE resfield1='" & resfield1.Text & "'"
            If CheckDataExists(sSql) Then
                GenerateDraftNo()
            End If
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("ql_mstbottomprice", "resfield1", resfield1.Text, "bottompricestatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                'showMessage(sStatusInfo, 2, "")
                'bottompricestatus.Text = "In Process" : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Dim ibottompriceOid As Integer = GenerateID("ql_mstbottomprice", CompnyCode)
        Dim sType As String = ""
        Dim periodacctg As String = GetDateToPeriodAcctg(CDate(bottompricedate.Text))

        Dim objTable As DataTable = Session("TblDtl")
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                ' Do Nothing
            Else
                sSql = "DELETE FROM ql_mstbottomprice WHERE cmpcode='" & CompnyCode & "' AND resfield1='" & resfield1.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            For C1 As Integer = 0 To objTable.Rows.Count - 1

                sSql = "INSERT INTO ql_mstbottomprice (cmpcode,branch_code,bottompriceoid,bottompricedate,periodacctg,refoid,hpp,presentase,newbottomprice, lastbottomprice,bottompricenote,bottompricestatus,resfield1,resfield2,createuser,createtime,upduser,updtime) VALUES" & _
                " ('" & CompnyCode & "','" & Session("branch_id") & "', " & ibottompriceOid & ", '" & CDate(bottompricedate.Text) & "', '" & GetDateToPeriodAcctg(CDate(bottompricedate.Text)) & "', " & Integer.Parse(objTable.Rows(C1)("refoid")) & ", " & ToDouble(objTable.Rows(C1)("hpp")) & ", " & ToDouble(objTable.Rows(C1)("presentase")) & ", " & ToDouble(objTable.Rows(C1)("newbottomprice")) & ", " & ToDouble(objTable.Rows(C1)("bottomprice")) & ", '" & Tchar(objTable.Rows(C1)("note").ToString) & "', '" & bottompricestatus.Text & "', '" & resfield1.Text & "', '', '" & objTable.Rows(C1)("createuser").ToString & "', '" & CDate(objTable.Rows(C1)("createtime").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ibottompriceOid += 1

                If bottompricestatus.Text = "Post" Then
                    sSql = "update ql_mstitem set bottomprice = " & ToDouble(objTable.Rows(C1)("newbottomprice").ToString) & ", persentobtmprice = " & ToDouble(objTable.Rows(C1)("presentase")) & " Where itemoid = " & objTable.Rows(C1)("refoid").ToString & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Next

            sSql = "UPDATE QL_mstoid SET lastoid=" & ibottompriceOid - 1 & " WHERE tablename='ql_mstbottomprice' AND cmpcode='" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    imbSave_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.Message & sSql, 1, "")
                    bottompricestatus.Text = "In Process"
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message & sSql, 1, "")
                bottompricestatus.Text = "In Process"
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message & sSql, 1, "") : Exit Sub
        End Try

        If sDraftNo <> resfield1.Text Then
            showMessage("Draft No. have been regenerated because have been used by another data. Your new Draft No. is " & resfield1.Text & ".<BR>", 3, "REDIR")
        Else
            If sPostNo <> bottompriceno.Text Then
                showMessage("Data have been posted with Bottom Price. No. " & bottompriceno.Text & ".", 3, "REDIR")
            Else
                Response.Redirect("~\Master\mstbottomprice.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub imbDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbDelete.Click
        If ToDouble(resfield1.Text) = 0 Then
            showMessage("Invalid Bottom Price data.<BR>", 2, "")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try

            sSql = "DELETE FROM ql_mstbottomprice WHERE resfield1='" & resfield1.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, 1, "") : Exit Sub
        End Try
        Response.Redirect("~\Master\mstbottomprice.aspx?awal=true")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("~\Master\mstbottomprice.aspx?awal=true")
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        'If Session("TblDtl") Is Nothing Then
        '    showMessage("Please define detail data first!", 2, "")
        '    Exit Sub
        'End If
        'If UpdateDetailData() Then
        '    ShowReport()
        'End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = ""
        If FilterTextMst.Text <> "" Then
            sSqlPlus = " AND " & FilterDDLMst.SelectedValue & " LIKE '%" & Tchar(FilterTextMst.Text) & "%'"
        End If
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND bottompricedate>='" & FilterPeriod1.Text & " 00:00:00' AND bottompricedate<='" & FilterPeriod2.Text & "'"
            Else
                Exit Sub
            End If
        End If
        If FilterDDLStatus.SelectedValue <> "ALL" Then
            sSqlPlus &= "and bottompricestatus = '" & FilterDDLStatus.SelectedValue & "'"
        End If
 
        BindData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sSqlPlus As String = ""
        'FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = 0
        If checkPagePermission("~\Master\mstbottomprice.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND a.createuser='" & Session("UserID") & "'"
        End If
        BindData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dv As DataView = Session("TblMst").DefaultView
            dv.RowFilter = "checkvalue='True'"
            If dv.Count < 1 Then
                dv.RowFilter = ""
                showMessage("Please Select data to Print first.", 2, "")
            Else
                dv.RowFilter = ""
                PrintReport()
            End If
        Else
            showMessage("Please Select data to Print first.", 2, "")
        End If
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        UpdateCheckedValue()
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt As DataTable = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub lkbSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\Master\mstbottomprice.aspx?oid=" & sender.ToolTip)
    End Sub

    Protected Sub imbFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateDetailData()
        Dim sWhere As String = ""
        sWhere &= "AND (m.itemdesc LIKE '%" & Tchar(FilterText.Text) & "%' OR m.itemcode LIKE '%" & Tchar(FilterText.Text) & "%' OR m.merk LIKE '%" & Tchar(FilterText.Text) & "%') AND m.hpp > 0 AND m.itemflag = '" & status.SelectedValue & "'"

        bindkatalog(sWhere)
    End Sub

    Protected Sub qtyin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("bottomprice") Is Nothing Then
            Dim iError As Int16 = 0
            Dim objTable As DataTable = Session("bottomprice")

            Dim tbox As System.Web.UI.WebControls.TextBox = TryCast(sender, System.Web.UI.WebControls.TextBox)
            tbox.Text = ToMaskEdit(ToDouble(tbox.Text), 4)
            Dim tboxval As Double = 0.0
            If Double.TryParse(tbox.Text, tboxval) = True Then
                tbox.Text = Format(tboxval, "#,##0.00")
            Else
                tbox.Text = Format(0, "#,##0.00")
            End If
        End If
    End Sub

    Protected Sub gvFindCrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        UpdateDetailData()
        gvFindCrd.PageIndex = e.NewPageIndex
        Dim dtDtl As DataTable = Session("TblCrd")
        gvFindCrd.DataSource = dtDtl : gvFindCrd.DataBind()
    End Sub

    Protected Sub btnAll_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sSqlPlus As String = "" : FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = 0 
        Dim dCb As String = "" : BindData("")
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateDetailData()
        bindkatalog("")
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 3)
        End If
    End Sub
#End Region

    Protected Sub gvFindCrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFindCrd.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
        End If
    End Sub
End Class