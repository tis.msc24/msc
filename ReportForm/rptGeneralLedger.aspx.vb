Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class reportForm_rptGeneralLedger
    Inherits System.Web.UI.Page
#Region "Variables"
    Dim Report As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure

#End Region

#Region "Functions"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function
#End Region

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption
        lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function IsValidPeriod() As Boolean
        Try
            If Format(CDate(toDate(gldate1.Text)), "dd/MM/yyyy") > Format(CDate(toDate(Gldate2.Text)), "dd/MM/yyyy") Then
                showMessage("Tanggal Terakhir tidak boleh lebih kecil dari Tanggal Awal!!", 2)
                Return False
            End If
        Catch
            showMessage("Format tanggal salah", 2)
            Return False
        End Try
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "acctgoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function

#Region "Procedures"
    Private Sub InitAllDDL()
        If Session("branch_id") = "10" Then
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
            FillDDL(ddlBranch, sSql)
            ddlBranch.Enabled = True : ddlBranch.CssClass = "inpText"
            ddlBranch.Items.Add(New ListItem("ALL", "ALL"))
            ddlBranch.SelectedValue = "ALL"
        Else
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and gencode='" & Session("branch_id") & "'"
            FillDDL(ddlBranch, sSql)
            ddlBranch.Enabled = False
            ddlBranch.CssClass = "inpTextDisabled"
        End If
    End Sub

    Public Sub bindDataAcct()
        Dim cBrn As String = ""
        If ddlBranch.SelectedValue.ToUpper <> "ALL" Then
            cBrn = "AND gl.branch_code='" & ddlBranch.SelectedValue & "'"
        End If
        sSql = "SELECT DISTINCT 'False' AS selected, a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.acctgflag='A' AND (a.acctgcode LIKE '%" & Tchar(acctgdesc.Text.Trim) & "%' OR a.acctgdesc LIKE '%" & Tchar(acctgdesc.Text.Trim) & "%') AND a.acctgoid IN (SELECT gl.acctgoid FROM QL_trngldtl gl WHERE a.acctgoid=gl.acctgoid AND a.cmpcode=gl.cmpcode " & cBrn & ") ORDER BY a.acctgcode"
        FillGV(gvAccounting, sSql, "QL_mstacctg")
        ViewState("listofCOA") = gvAccounting.DataSource
        gvAccounting.Visible = True
    End Sub

    Sub clearForm()
        CrystalReportViewer1.ReportSource = Nothing
        gvAccounting.Visible = False
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        'If Not IsValidPeriod() Then
        '    Exit Sub
        'End If
        If CDate(toDate(gldate1.Text.Trim)) > CDate(toDate(Gldate2.Text.Trim)) Then
            showMessage("-Period 2 must be more than Period 1!.<BR>", 2)
            Exit Sub
        End If
        Dim sBranch As String = ""
        Try
            Dim oid As String = "" : Dim sCoa As String = "" : Dim rptName As String = ""
            Dim awaldate As Date : Dim akhirdate As Date
            'If DDLType.SelectedValue = "Summary" Then
            gvAccounting_PageIndexChanging(Nothing, Nothing)
            'If ViewState("listofCOA") IsNot Nothing Then
            '    Dim dtab As DataTable = ViewState("listofCOA")
            '    If dtab.Rows.Count > 0 Then
            '        For i As Integer = 0 To dtab.Rows.Count - 1
            '            If dtab.Rows(i).Item("selected") = True Then
            '                oid = oid & dtab.Rows(i).Item("acctgoid").ToString() & ","
            '            End If
            '        Next
            '    End If
            'End If
            'If (oid <> "") Then
            '    oid = oid.Substring(0, oid.Length - 1)
            'End If
            ''End If

            'If oid = "" And rbSupplier.SelectedValue <> "ALL" Then
            '    showMessage("Pilih COA Dulu", 2)
            '    Exit Sub
            'End If

            Dim arCode() As String = AcctgName.Text.Split(";")
            Dim sCodeIn As String = "" : Dim sQel As String = ""
            Dim adr As String = ""
            For C1 As Integer = 0 To arCode.Length - 1
                If arCode(C1) <> "" Then
                    sCodeIn &= "'" & arCode(C1) & "',"
                End If
            Next

            If sCodeIn <> "" Then
                sCoa &= " AND acctgcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
            End If

            If ddlBranch.SelectedValue <> "ALL" Then
                sBranch = "And branch_code = '" & ddlBranch.SelectedValue & "'"
            End If

            awaldate = CDate(toDate(gldate1.Text))
            akhirdate = CDate(toDate(Gldate2.Text))
            Report = New ReportDocument
            If DDLType.SelectedValue = "Summary" Then
                Report.Load(Server.MapPath("~\Report\rptGLSummary.rpt"))
                rptName = "General_Report_(Sum)_"
                'If rbSupplier.SelectedValue <> "ALL" Then
                '    sCoa = " AND acctgoid IN (" & oid & ") "
                'End If
                Report.SetParameterValue("awaldate", CDate(toDate(gldate1.Text)))
                Report.SetParameterValue("akhirdate", CDate(toDate(Gldate2.Text)))
                Report.SetParameterValue("branchcode", sBranch)
                Report.SetParameterValue("sCoa", sCoa)
            Else
                Dim sWhere As String = ""
                Report.Load(Server.MapPath("~\Report\rptGLdetail.rpt"))
                rptName = "General_Report_(Dtl)_"
                'If rbSupplier.SelectedValue <> "ALL" Then
                '    sCoa = " AND acctgoid IN (" & oid & ") "
                'End If
                sWhere = "WHERE cmpcode='" & CompnyCode & "' " & sBranch & " " & sCoa & ""
                Report.SetParameterValue("sWhere", sWhere)
            End If

            Report.SetParameterValue("awaldate", awaldate)
            Report.SetParameterValue("akhirdate", akhirdate)
            Report.SetParameterValue("cmpcode", CompnyCode)
            Report.SetParameterValue("BranchName", ddlBranch.SelectedItem.ToString)
            Report.SetParameterValue("sDatePeriod", Format(CDate(toDate(gldate1.Text)), "dd/MM/yyyy") & " - " & Format(CDate(toDate(Gldate2.Text)), "dd/MM/yyyy"))
            Report.SetParameterValue("reportName", Report.FileName.Substring(Report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            'Report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(Report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            If sType = "crv" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = Report
            ElseIf sType = "xls" Then
                Report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "General_Ledger_Report" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close()
                Report.Dispose()
                Response.Redirect("~\ReportForm\rptGeneralLedger.aspx?awal=true")
            ElseIf sType = "pdf" Then
                Report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "General_ledger_Report" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close()
                Report.Dispose()
                Response.Redirect("~\ReportForm\rptGeneralLedger.aspx?awal=true")
            Else
            End If
        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Public Sub BindDataListAcctg()
        Try
            Dim cBrn As String = ""
            If ddlBranch.SelectedValue.ToUpper <> "ALL" Then
                cBrn = "AND gl.branch_code='" & ddlBranch.SelectedValue & "'"
            End If
            sSql = "SELECT DISTINCT 'False' AS Checkvalue, a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.acctgflag='A' AND (a.acctgcode LIKE '%" & Tchar(acctgdesc.Text.Trim) & "%' OR a.acctgdesc LIKE '%" & Tchar(acctgdesc.Text.Trim) & "%') AND a.acctgoid IN (SELECT gl.acctgoid FROM QL_trngldtl gl WHERE a.acctgoid=gl.acctgoid AND a.cmpcode=gl.cmpcode " & cBrn & ") ORDER BY a.acctgcode"
            Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstitem")
            Session("TblMat") = dt
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind() : gvItem.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Session("branch") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("UserName") = userName : Session("Role") = xsetRole
            Session("UserID") = userId : Session("Access") = access
            Session("SpecialAccess") = xsetAcc : Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\ReportForm\rptGeneralLedger.aspx")
        End If
        Session("sCmpcode") = Request.QueryString("cmpcode")
        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - General Ledger Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            gldate1.Text = Format(GetServerTime, "01/MM/yyyy")
            Gldate2.Text = Format(GetServerTime, "dd/MM/yyyy")
        Else
        End If
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rbSupplier.SelectedValue.ToUpper = "SELECT" Then
            If DDLType.SelectedValue = "Detail" Then
                Label25.Visible = False : acctgdesc.Visible = False 'coa.Visible = False
                btnSearch.Visible = False : Label14.Visible = True
                imbClearAcctg.Visible = False : acctgdesc.Text = ""
                gvAccounting.Visible = False : acctgoid.Text = 0
                rbSupplier.SelectedValue = "ALL"
            Else
                Label25.Visible = False : acctgdesc.Visible = False 'coa.Visible = False
                btnSearch.Visible = False : Label14.Visible = True
                imbClearAcctg.Visible = False : acctgdesc.Text = ""
                gvAccounting.Visible = False : acctgoid.Text = 0
                rbSupplier.SelectedValue = "ALL"
            End If
        End If
    End Sub

    Protected Sub gvAccounting_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gvAccounting.PageIndexChanging
        If Not (ViewState("listofCOA") Is Nothing) Then
            Dim dtab As DataTable = ViewState("listofCOA")
            If dtab.Rows.Count > 0 Then
                For i As Integer = 0 To gvAccounting.Rows.Count - 1
                    Dim gvr As GridViewRow
                    Dim cb As CheckBox
                    Dim lbl As Label
                    Dim CoaOid As Integer
                    Dim dView As DataView = dtab.DefaultView
                    Dim drView As DataRowView

                    For j As Integer = 0 To gvAccounting.Rows.Count - 1
                        gvr = gvAccounting.Rows(j)
                        cb = gvAccounting.Rows(j).FindControl("cblist")
                        If cb.Checked = True Then
                            lbl = gvAccounting.Rows(j).FindControl("acctgoid")
                            CoaOid = Integer.Parse(lbl.Text)
                            dView.RowFilter = "acctgoid = " & CoaOid
                            drView = dView.Item(0)
                            drView.BeginEdit()
                            drView("selected") = 1
                            drView.EndEdit()
                            dView.RowFilter = ""
                        Else
                            lbl = gvAccounting.Rows(j).FindControl("acctgoid")
                            CoaOid = Integer.Parse(lbl.Text)
                            dView.RowFilter = "acctgoid = " & CoaOid
                            drView = dView.Item(0)
                            drView.BeginEdit()
                            drView("selected") = 0
                            drView.EndEdit()
                            dView.RowFilter = ""
                        End If
                    Next
                Next
            End If
            If Not (e Is Nothing) Then
                gvAccounting.PageIndex = e.NewPageIndex
            End If
            gvAccounting.DataSource = dtab
            gvAccounting.DataBind()
            ViewState("listofCOA") = dtab

        End If

    End Sub

    Protected Sub gvAccounting_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAccounting.SelectedIndexChanged
        'acctgdesc.Text = gvAccounting.SelectedDataKey("acctgdesc").ToString().Trim
        'acctgoid.Text = gvAccounting.SelectedDataKey("acctgoid").ToString().Trim
        'gvAccounting.Visible = False
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not Report Is Nothing Then
                If Report.IsLoaded Then
                    Report.Dispose()
                    Report.Close()
                End If
            End If
        Catch ex As Exception
            Report.Dispose()
            Report.Close()
        End Try
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        ShowReport("crv")
    End Sub

    Protected Sub imbClearAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        acctgoid.Text = ""
        acctgdesc.Text = ""
        'sjjualmstoid.Text = ""
        gvAccounting.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        bindDataAcct()
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvAccounting.DataSource = Nothing : gvAccounting.DataBind()
        Session("listofCOA") = Nothing : gvAccounting.Visible = False
        If rbSupplier.SelectedValue = "ALL" Then
            acctgdesc.Visible = False : btnSearch.Visible = False : imbClearAcctg.Visible = False
        Else
            acctgdesc.Visible = True : btnSearch.Visible = True : imbClearAcctg.Visible = True
        End If
    End Sub

    Protected Sub acctgdesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        acctgoid.Text = ""
        bindDataAcct()
    End Sub

    Protected Sub btnViewReport_Click(sender As Object, e As ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("crv")
    End Sub

    Protected Sub btnExportToPdf0_Click(sender As Object, e As ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("pdf")
    End Sub

    Protected Sub btnExportToExcel_Click(sender As Object, e As ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("xls")
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\reportForm\rptGeneralLedger.aspx")
    End Sub

    Protected Sub btnErrOK_Click(sender As Object, e As ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        Response.Redirect("~\reportForm\rptGeneralLedger.aspx")
    End Sub
#End Region

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvItem.DataSource = Nothing : gvItem.DataBind()
        BindDataListAcctg()
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        AcctgName.Text = "" : acctgoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                showMessage("Data Account tidak ditemukan!", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "Checkvalue=True"
        If dv.Count > 0 Then

            If AcctgName.Text <> "" Then
                For C1 As Integer = 0 To dv.Count - 1
                    AcctgName.Text &= ";" & dv(C1)("acctgcode").ToString & ";"
                Next
            Else
                For C1 As Integer = 0 To dv.Count - 1
                    AcctgName.Text &= dv(C1)("acctgcode").ToString & ";"
                Next
            End If


            If AcctgName.Text <> "" Then
                AcctgName.Text = Left(AcctgName.Text, AcctgName.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            showMessage("- Maaf, Silahkan pilih Account, dengan beri centang pada kolom pilih!<BR>", 2)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next

            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage("Maaf, Data barang yang anda cari tidak ada..!!", 2)
                mpeListMat.Show()
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            BindDataListAcctg()
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind()
        End If
        mpeListMat.Show()
    End Sub
End Class
