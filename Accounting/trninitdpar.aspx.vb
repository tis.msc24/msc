'Created By Vriska On 4 October 2014
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class Accounting_trninitdpar
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cRate As New ClassRate
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"

    Public Function GetIDCB() As String
        Return Eval("cmpcode") & "," & Eval("trndparoid")
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Private Function IsInputValid() As Boolean
        Try
            Dim sMsg As String = ""
            Dim sErr As String = "" : Dim offDate As String = ""
            '======================
            'Cek Peride CutOffDate
            '======================
            Dim CutOffDate As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

            If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName)
                Exit Function
            Else
                CutOffDate = toDate(GetStrData(sSql))
            End If
            CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
            '=============================================================
            If sMsg <> "" Then

                showMessage(sMsg, CompnyName & " - Warning")
                Exit Function
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Warning")
            Return False
            Exit Function
        End Try
        Return True
    End Function

#End Region

#Region "Procedures"
    Private Sub Validasi()
        Dim sMsg As String = ""

        If trndparno.Text = "" Then
            sMsg &= "-  DP No. harus diisi !!<BR>"
        End If

        If custoid.Text = "" Then
            sMsg &= "- Customer harus diisi !!<BR>"
        End If

        If Not IsDate(toDate(trndpardate.Text)) Then
            sMsg &= "- Tanggal harus diisi !!<BR>"
        End If

        If IsDate(toDate(trndpardate.Text)) = False Then
            sMsg &= "- Tanggal salah !!<BR>"
        End If

        If CDate(toDate(trndpardate.Text)) >= CDate(toDate(CutofDate.Text)) Then
            sMsg &= "- Tanggal DP Tidak boleh >= CutoffDate (" & CutofDate.Text & ") !! <BR> "
        End If
        If payduedate.Visible Then
            If Not IsDate(toDate(payduedate.Text)) Then
                sMsg &= "- Tanggal Jatuh Tempo harus diisi !!<BR>"
            End If
            If IsDate(toDate(payduedate.Text)) = False Then
                sMsg &= "- Tanggal Jatuh Tempo salah !!<BR>"
            End If
        End If
        If payduedate.Visible And (IsDate(toDate(payduedate.Text))) And (IsDate(toDate(trndpardate.Text))) Then
            If CDate(toDate(payduedate.Text)) < CDate(toDate(trndpardate.Text)) Then
                sMsg &= "- Tanggal jatuh tempo harus >= tanggal transaksi !!<BR>"
            End If
        End If

        If payrefno.Text = "" And payreftype.SelectedValue <> "CASH" Then
            sMsg &= "-  Ref No. harus diisi !!<BR>"
        End If

        If ToDouble(trndparamt.Text) <= 0 Then
            sMsg &= "- Total DP harus >  0 !!<BR>"
        End If
        If trndparnote.Text.Trim.Length > 200 Then
            sMsg &= "- Note, maksimal 200 karakter !!<BR>"
        End If

        'If CheckDataExists("SELECT COUNT(*) FROM QL_trndpar WHERE cmpcode='" & cabangDDL.SelectedValue & "' AND dparno='" & Tchar(trndparno.Text) & "' AND dparoid<>'" & Session("oid") & "'") = True Then
        '    sMsg &= "-  DP No Outlet sudah ada !!<BR>"
        'End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            trndparstatus.Text = "In Process"
            Exit Sub
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub DDLcaBang()
        Dim sCb As String = ""
        If Session("branch_id") <> "10" Then
            sCb = "And gencode='" & Session("branch_id") & "'"
        Else
            cabangDDL.Enabled = True : cabangDDL.CssClass = "inpText"
        End If
        sSql = "Select gencode,gendesc from QL_mstgen Where gengroup='CABANG'" & sCb & ""
        FillDDL(cabangDDL, sSql)
    End Sub

    Private Sub InitAllDDL()
        FillDDLAcctg(trndparacctgoid, "VAR_DPAR", cabangDDL.SelectedValue)
        If trndparacctgoid.Items.Count < 1 Then
            showMessage("Isi/Buat account VAR_DPAR di master accounting!!", 2)
        End If
    End Sub

    Private Sub BindSupplier(ByVal sPlus As String)
        sSql = "SELECT custoid,custcode, custname FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' " & sPlus & " and branch_code = '" & cabangDDL.SelectedValue & "' ORDER BY custcode"
        FillGV(gvCust, sSql, "QL_mstcreditcard")
    End Sub

    Private Sub BindData(ByVal sPLus As String)
        If IsValidPeriod() Then
            sSql = "SELECT ar.trndparoid,ar.trndparno,ar.trndpardate,ar.trndparamt, ar.trndparstatus,ar.trndparnote,payreftype giroNo,ar.cmpcode,custname FROM QL_trndpar ar INNER JOIN QL_mstcust s ON ar.custoid=s.custoid WHERE trndparoid <=0 AND trndparstatus <> 'DELETE'" & sPLus & " " & IIf(statuse.SelectedValue = "ALL", "", " And ar.trndparstatus='" & statuse.SelectedValue & "'") & " ORDER BY trndparno"

            Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_trndpar")
            Session("TblDPARInit") = objTable
            gvMst.DataSource = Session("TblDPARInit")
            gvMst.DataBind()
            UnabledCheckBox()
        End If
    End Sub

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(dateAwal.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(dateAkhir.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            'showMessage("Please select Down Payment A/R data first!", 2)
            Return False
        End If
        Return True
    End Function

    Private Sub DDLPayreftypeChange()
        If payreftype.SelectedValue = "CASH" Then
            tr1.Visible = False
            tr2.Visible = False
        Else
            tr1.Visible = True
            tr2.Visible = True
            payduedate.Text = Format(GetServerTime, "dd/MM/yyyy")
        End If
    End Sub

    Private Sub FillTextBox(ByVal iOid As Integer, ByVal compnycode As String)
       
        sSql = "SELECT ar.trndparoid,ar.cashbankoid,ar.trndparno,ar.trndpardate,ar.custoid,custname, " & _
               "ar.payreftype,0,ar.payduedate,ar.payrefno,ar.trndparacctgoid," & _
               "ar.trndparamt,ar.trndparnote,ar.trndparstatus,ar.upduser, " & _
               "ar.updtime,ar.createuser,ar.createtime,ar.cmpcode, ar.branch_code " & _
               "FROM QL_trndpar ar INNER JOIN ql_mstcust s ON ar.custoid=s.custoid " & _
               "WHERE ar.cmpcode='" & compnycode & "' AND ar.trndparoid=" & iOid
        Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_trndpar")
        If dtMst.Rows.Count < 1 Then
             showMessage("Can't load data DP AR Balance !!", 2)
            Exit Sub
        Else
            trndparoid.Text = dtMst.Rows(0)("trndparoid").ToString
            cabangDDL.SelectedValue = dtMst.Rows(0)("branch_code").ToString
            trndparno.Text = dtMst.Rows(0)("trndparno").ToString
            trndpardate.Text = Format(CDate(dtMst.Rows(0)("trndpardate").ToString), "dd/MM/yyyy")
            custname.Text = dtMst.Rows(0)("custname").ToString
            custoid.Text = dtMst.Rows(0)("custoid").ToString
            payreftype.Text = dtMst.Rows(0)("payreftype").ToString
            payduedate.Text = Format(CDate(dtMst.Rows(0)("payduedate").ToString), "dd/MM/yyyy")
            payrefno.Text = dtMst.Rows(0)("payrefno").ToString
            InitAllDDL()
            trndparacctgoid.SelectedValue = dtMst.Rows(0)("trndparacctgoid").ToString
            trndparamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndparamt").ToString), 4) * 100
            trndparnote.Text = dtMst.Rows(0)("trndparnote").ToString
            trndparstatus.Text = dtMst.Rows(0)("trndparstatus").ToString

            If payreftype.SelectedValue = "CASH" Then
                tr1.Visible = False : tr2.Visible = False
            Else
                tr1.Visible = True : tr2.Visible = True
            End If

            create.Text = "Created By <B>" & dtMst.Rows(0)("createuser").ToString & "</B> On <B>" & dtMst.Rows(0)("createtime").ToString & "</B> "
            update.Text = "Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "

            If trndparstatus.Text <> "POST" Then
                btnSave1.Visible = True : btnDelete1.Visible = True : btnposting1.Visible = True
            Else
                btnSave1.Visible = False : btnDelete1.Visible = False : btnposting1.Visible = False
            End If
        End If
        cabangDDL.Enabled = False
        cabangDDL.CssClass = "inpTextDisabled"

        If trndparstatus.Text <> "In Process" Then
            btnSave.Visible = False
            btnDelete.Visible = False : btnPosting2.Visible = False
            trndpardate.CssClass = "inpTextDisabled" : trndpardate.ReadOnly = True
            imbDPARDate.Visible = False : trndparno.Enabled = False
            cabangDDL.Enabled = False : custname.Enabled = False
            imbSearchCust.Visible = False : imbClearCust.Visible = False
            trndparacctgoid.Enabled = False : payreftype.Enabled = False
            payduedate.CssClass = "inpTextDisabled" : payduedate.ReadOnly = True
            imbDueDate.Visible = False : payrefno.Enabled = False
            trndparamt.Enabled = False : trndparnote.Enabled = False
        End If

    End Sub

    Public Sub UnabledCheckBox()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPARInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (gvMst.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("trndparstatus".ToString)) = "POST" Or Trim(objRow(C1)("trndparstatus".ToString) = "DELETE") Then
                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPARInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("trndparstatus").ToString) <> "POST" And Trim(objRow(C1)("trndparstatus").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPARInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("trndparstatus").ToString) <> "POST" And Trim(objRow(C1)("trndparstatus").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub UpdateCheckedPost()
        'Untuk mendapat cek box value multi select
        Dim dv As DataView = Session("TblDPARInit").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvMst.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        Dim a As String() = sOid.Split(",")
                        dv.RowFilter = "trndparoid=" & a(1) & " AND cmpcode='" & a(0) & "'"
                        If cbcheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        'Dim cP As New ClassProcedure
        'cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("~\Accounting\trninitdpar.aspx")
        End If
        Page.Title = CompnyName & " - DP AR Inittial Balance"
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan menghapus data ini ?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan mem-posting data ini ?');")
        btnPosting2.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan mem-posting data ini ?');")
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName)
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        If Not Page.IsPostBack Then
            dateAwal.Text = Format(GetServerTime, "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime, "dd/MM/yyyy")
            BindData("") : DDLcaBang() : InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"), CompnyCode)
                TabContainer1.ActiveTabIndex = 1
            Else
                trndpardate.Text = Format(CutOffDate.AddDays(-1), "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime, "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 0
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
                update.Text = "" : trndparstatus.Text = "In Process" : btnDelete1.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSupplier(" and (custname like '%" & Tchar(custname.Text) & "%' or custcode like '%" & Tchar(custname.Text) & "%')")
        gvCust.Visible = True
    End Sub

    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custoid.Text = "" : custname.Text = ""
        gvCust.Visible = False
    End Sub

    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Validasi()
    '    Session("DPARInit") = GetStrData("SELECT isnull(MIN(dparoid)-1,-1) FROM QL_trndpar /*WHERE cmpcode='" & cabangDDL.SelectedValue & "'*/")

    '    'sSql = "select count(-1) From QL_trndpar Where dparno='" & trndparno.Text & "' and dparstatus='POST' and  cmpcode='" & cabangDDL.SelectedValue & "'"
    '    'If GetStrData(sSql) > 0 Then
    '    '    showMessage("Data ini sudah di Posting !, Tekan batal untuk lihat Data di List Information", 2)
    '    '    trndparstatus.Text = "In Process"
    '    '    Exit Sub
    '    'End If
    '    '==============================
    '    'Cek Peroide Bulanan Dari Crdgl
    '    '==============================
    '    If ToDouble(trndparamt.Text) > 0 Then
    '        'CEK PERIODE AKTIF BULANAN
    '        'sSql = "select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
    '        'If GetStrData(sSql) <> "" Then
    '        sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
    '        If GetPeriodAcctg(CDate(toDate(trndpardate.Text))) < GetStrData(sSql) Then
    '            showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>", 2)
    '            Exit Sub
    '        End If
    '    End If
    '    'End If

    '    Dim objTrans As SqlClient.SqlTransaction
    '    If conn.State = ConnectionState.Closed Then
    '        conn.Open()
    '    End If
    '    objTrans = conn.BeginTransaction()
    '    xCmd.Transaction = objTrans

    '    Try
    '        If payreftype.SelectedValue = "CASH" Then
    '            payduedate.Text = "01/01/1900"
    '        End If

    '        If Session("oid") = Nothing Or Session("oid") = "" Then
    '            Dim lastrate As Integer
    '            lastrate = GetStrData("select top 1 rateoid from ql_mstrate where curroid=1 order by rateoid desc")
    '            sSql = "INSERT INTO QL_trndpar(cmpcode,dparoid,dparno,dpardate,custoid,cashbankoid,dparpayacctgoid,dparpaytype,dparduedate,dparpayrefno,dparamt,dparnote,dparaccumamt,dparstatus,createuser,createtime,upduser,updtime,acctgoid, curroid, rateoid,rate2oid,giroacctgoid,branch_code, flagdp, taxtype) VALUES " & _
    '            " ('" & CompnyCode & "'," & Session("DPARInit") & ",'" & Tchar(trndparno.Text) & "', '" & CDate(toDate(trndpardate.Text)) & "','" & custoid.Text & "',0, " & trndparacctgoid.SelectedValue & ",'','" & CDate(toDate(payduedate.Text)) & "','" & Tchar(payrefno.Text) & "'," & ToDouble(trndparamt.Text) & ", '" & Tchar(trndparnote.Text) & "',0,'" & trndparstatus.Text & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "', CURRENT_TIMESTAMP,150,1, " & lastrate & ",60,0,'" & cabangDDL.SelectedValue & "', 'NORMAL', 'NON TAX')"
    '            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
    '        Else
    '            sSql = "UPDATE QL_trndpar SET dparno='" & Tchar(trndparno.Text) & "',dpardate='" & CDate(toDate(trndpardate.Text)) & "', dparpayacctgoid=" & trndparacctgoid.SelectedValue & ",dparamt=" & ToDouble(trndparamt.Text) & ", dparpaytype='" & payreftype.SelectedValue & "',dparduedate='" & CDate(toDate(payduedate.Text)) & "', dparpayrefno='" & Tchar(payrefno.Text) & "', dparnote='" & Tchar(trndparnote.Text) & "',custoid='" & custoid.Text & "', dparaccumamt=0,dparstatus='" & trndparstatus.Text & "',upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='MSC' AND dparoid=" & trndparoid.Text & " AND branch_code='" & cabangDDL.SelectedValue & "'"
    '            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
    '        End If
    '        objTrans.Commit() : conn.Close()
    '    Catch ex As Exception
    '        objTrans.Rollback() : conn.Close() : trndparstatus.Text = "In Process"
    '        showMessage(ex.ToString, 2)
    '        Exit Sub
    '    End Try

    '    If Session("TblDPARInitdtl") Is Nothing Then
    '        If trndparstatus.Text = "POST" Then
    '            Session("SavedInfo") &= "Data have been posted with DP AR No. = " & trndparno.Text & " !"
    '        End If
    '        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
    '            showMessage(Session("SavedInfo"), 3)
    '        Else
    '            Response.Redirect("trninitdpar.aspx?awal=true")
    '        End If
    '    End If
    'End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~\accounting\trninitdpar.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trndpar SET trndparstatus='DELETE',syncflag='' WHERE cmpcode='MSC' AND trndparoid=" & trndparoid.Text & " AND branch_code='" & cabangDDL.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            'QLMsgBox1.ShowMessage(ex.Message, 2, "")
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
        Response.Redirect("trninitdpar.aspx?awal=true")
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckAll.Click
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUncheckAll.Click
        UncheckAll()
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosting.Click
        Dim sErrorku As String = ""
        If Not Session("TblDPARInit") Is Nothing Then
            Dim dt As DataTable = Session("TblDPARInit")
            If dt.Rows.Count > 0 Then
                UpdateCheckedPost()
                Dim dv As DataView = dt.DefaultView

                dv.RowFilter = "selected=1"
                If dv.Count = 0 Then
                    sErrorku &= "Please select DP AR first!"
                End If
                dv.RowFilter = ""
                dv.RowFilter = "selected=1 AND trndparstatus='POST'"
                If dv.Count > 0 Then
                    sErrorku &= "DP AR have been POSTED before!"
                End If
                dv.RowFilter = ""

                If sErrorku <> "" Then
                    'QLMsgBox1.ShowMessage(sErrorku, 2, "")
                    showMessage(sErrorku, 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If

                Dim parsingOID As Integer = 0
                Dim sCmpcode As String = ""
                Dim dtAwal As DataTable = Session("TblDPARInit")
                Dim dvAwal As DataView = dtAwal.DefaultView
                Dim iSeq As Integer = dvAwal.Count + 1
                dvAwal.AllowEdit = True
                dvAwal.AllowNew = True
                dv.RowFilter = "selected=1 AND trndparstatus='In Process'"
                'poststatx.Text = "SPECIAL"

                Dim seq As Integer = 0
                For C1 As Integer = 0 To dv.Count - 1
                    parsingOID = dv(C1)("trndparoid").ToString
                    sCmpcode = dv(C1)("cmpcode").ToString
                    filltextboxSELECTED(sCmpcode, parsingOID)
                    If Not (Session("errmsg") Is Nothing Or Session("errmsg") = "") Then
                        sErrorku &= "Can't POST DP AP No " & dv(C1)("trndparno").ToString & " with reason:<BR>" & Session("errmsg")
                    End If
                    seq = C1
                Next

                If seq = dv.Count - 1 Then
                    showMessage("Data Telah diPosting <BR>", 2)
                    'QLMsgBox1.ShowMessage("Data Telah di Posting <BR> ", 3, "REDIR")
                    'Session("QLMsgBox") = QLMsgBox1
                    Response.Redirect("trninitdpar.aspx?awal=true")
                End If

            End If
        End If
    End Sub

    Private Sub filltextboxSELECTED(ByVal sCmpcode As String, ByVal selectedoid As Integer)
        If Not selectedoid = Nothing Or selectedoid <> 0 Then
            Dim sMsg As String = ""
            Dim vpayid As Integer = selectedoid
            Session("oid") = vpayid.ToString
            Try
                sSql = "SELECT ar.trndparoid,ar.cashbankoid,ar.trndparno,ar.trndpardate,ar.custoid,custname,ar.payreftype,ar.cashbankacctgoid,ar.payduedate,ar.payrefno,ar.trndparacctgoid,ar.trndparamt,ar.trndparnote,ar.trndparstatus,ar.upduser,ar.updtime,ar.createuser,ar.createtime,ar.cmpcode,ar.branch_code FROM QL_trndpar ar INNER JOIN ql_mstcust s ON ar.custoid=s.custoid WHERE ar.cmpcode='" & sCmpcode & "' AND ar.trndparoid=" & selectedoid
                Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_trndpar")
                If dtMst.Rows.Count < 1 Then
                    showMessage("Can't load data DP AR Balance !!", 2)
                    'QLMsgBox1.ShowMessage("can't load data DP AR Balance !!<BR>Info :<BR>" & sSql, 2, "")
                    Exit Sub
                Else
                    trndparoid.Text = dtMst.Rows(0)("trndparoid").ToString
                    cabangDDL.SelectedValue = dtMst.Rows(0)("branch_code").ToString
                    trndparno.Text = dtMst.Rows(0)("trndparno").ToString
                    trndpardate.Text = Format(CDate(dtMst.Rows(0)("trndpardate").ToString), "dd/MM/yyyy")
                    custname.Text = dtMst.Rows(0)("custname").ToString
                    custoid.Text = dtMst.Rows(0)("custoid").ToString
                    payreftype.Text = dtMst.Rows(0)("payreftype").ToString
                    payduedate.Text = Format(CDate(dtMst.Rows(0)("payduedate").ToString), "dd/MM/yyyy")
                    payrefno.Text = dtMst.Rows(0)("payrefno").ToString
                    DDLPayreftypeChange()
                    trndparacctgoid.SelectedValue = dtMst.Rows(0)("trndparacctgoid").ToString
                    trndparamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndparamt").ToString), 4)
                    trndparnote.Text = dtMst.Rows(0)("trndparnote").ToString
                    trndparstatus.Text = dtMst.Rows(0)("trndparstatus").ToString
                    create.Text = "Created By <B>" & dtMst.Rows(0)("createuser").ToString & "</B> On <B>" & dtMst.Rows(0)("createtime").ToString & "</B> "
                    update.Text = "Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "
                    If trndparstatus.Text <> "POST" Then
                        btnSave1.Visible = True : btnDelete1.Visible = True : btnposting1.Visible = True
                    Else
                        btnSave1.Visible = False : btnDelete1.Visible = False : btnposting1.Visible = False
                    End If
                End If
                cabangDDL.Enabled = False
                cabangDDL.CssClass = "inpTextDisabled"

                Session("TblDPARInitdtl") = dtMst

            Catch ex As Exception
                xreader.Close() : conn.Close()
                sMsg &= "Gagal menampilkan data DP AR Balance.<BR>Info:" & ex.Message & "<BR>" & sSql & "<BR><BR>"
            End Try

        Else
            showMessage("DP AR Balance salah !", 2)
            'QLMsgBox1.ShowMessage("DP AR Balance salah !", 2, "")
            Exit Sub
        End If
        TabContainer1.ActiveTabIndex = 0
        btnPosting2_Click(Nothing, Nothing)
        TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub btnPosting2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosting2.Click
        trndparstatus.Text = "POST"
        'btnSave_Click(sender, e)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAll.Click
        dateAwal.Text = Format(Now, "01/MM/yyyy")
        dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        FilterDDL.SelectedIndex = 0 : FilterText.Text = "" : statuse.SelectedIndex = 0
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custoid.Text = gvCust.SelectedDataKey("custoid").ToString
        custname.Text = gvCust.SelectedDataKey("custname").ToString
        gvCust.Visible = False
    End Sub

    Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCust.PageIndexChanging
        gvCust.PageIndex = e.NewPageIndex
        BindSupplier(" and (custname like '%" & Tchar(custname.Text) & "%' or custcode like '%" & Tchar(custname.Text) & "%')")
        gvCust.Visible = True
    End Sub

    Protected Sub payreftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payreftype.SelectedIndexChanged
        DDLPayreftypeChange()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\accounting\trninitdpar.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' And ar.trndpardate Between '" & toDate(dateAwal.Text) & "' And '" & toDate(dateAkhir.Text) & "' ")
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'dateAwal.Text = Format(Now, "01/MM/yyyy")
        'dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        FilterDDL.SelectedIndex = 0 : FilterText.Text = "" : statuse.SelectedIndex = 0
        BindData("")
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsInputValid() Then
            Dim sMsg As String = ""

            If trndparno.Text = "" Then
                sMsg &= "-  DP No. harus diisi !!<BR>"
            End If

            If custoid.Text = "" Then
                sMsg &= "- Customer harus diisi !!<BR>"
            End If

            If Not IsDate(toDate(trndpardate.Text)) Then
                sMsg &= "- Tanggal harus diisi !!<BR>"
            End If
            If IsDate(toDate(trndpardate.Text)) = False Then
                sMsg &= "- Tanggal salah !!<BR>"
            End If

            If payduedate.Visible Then
                If Not IsDate(toDate(payduedate.Text)) Then
                    sMsg &= "- Tanggal Jatuh Tempo harus diisi !!<BR>"
                End If
                If IsDate(toDate(payduedate.Text)) = False Then
                    sMsg &= "- Tanggal Jatuh Tempo salah !!<BR>"
                End If
            End If
            If payduedate.Visible And (IsDate(toDate(payduedate.Text))) And (IsDate(toDate(trndpardate.Text))) Then
                If CDate(toDate(payduedate.Text)) < CDate(toDate(trndpardate.Text)) Then
                    sMsg &= "- Tanggal jatuh tempo harus >= tanggal transaksi !!<BR>"
                End If
            End If
            '==============================
            'Cek Peroide Bulanan Dari Crdgl
            '==============================
            If ToDouble(trndparamt.Text) > 0 Then
                'CEK PERIODE AKTIF BULANAN
                'sSql = "select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag='OPEN'"
                'If GetStrData(sSql) <> "" Then
                sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
                'Dim sd As String = toDate(trndpardate.Text)
                If GetPeriodAcctg(CDate(toDate(trndpardate.Text))) < GetStrData(sSql) Then
                    sMsg &= "Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>"
                    Exit Sub
                End If
            End If
            'End If

            If payrefno.Text = "" And payreftype.SelectedValue <> "CASH" Then
                sMsg &= "-  Ref No. harus diisi !!<BR>"
            End If

            If ToDouble(trndparamt.Text) <= 0 Then
                sMsg &= "- Total DP harus >  0 !!<BR>"
            End If
            If trndparnote.Text.Trim.Length > 200 Then
                sMsg &= "- Note, maksimal 200 karakter !!<BR>"
            End If

            'If CheckDataExists("SELECT COUNT(*) FROM QL_trndpar WHERE cmpcode='MSC' AND trndparno='" & Tchar(trndparno.Text) & "' AND trndparoid <> '" & Session("oid") & "' AND branch_code='" & cabangDDL.SelectedValue & "'") = True Then
            '    sMsg &= "-  DP No Outlet sudah ada !!<BR>"
            'End If

            If sMsg <> "" Then
                showMessage(sMsg, 2)
                trndparstatus.Text = "In Process"
                Exit Sub
            End If

            cRate.SetRateValue(CInt(1), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2)
                Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2)
                Exit Sub
            End If

            Session("DPARInit") = GetStrData("SELECT isnull(MIN(trndparoid)-1,-1) FROM QL_trndpar WHERE cmpcode='" & CompnyCode & "'")

            'sSql = "select count(-1) from QL_trndpar where trndparno='" & trndparno.Text & "' and trndparstatus='POST' And cmpcode='" & CompnyCode & "'"
            'If GetStrData(sSql) > 0 Then
            '    showMessage("Data ini sudah di Posting !, Tekan batal untuk lihat Data di List Information", 2)
            '    trndparstatus.Text = "In Process" : Exit Sub
            'End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try
                If payreftype.SelectedValue = "CASH" Then
                    payduedate.Text = "01/01/1900"
                End If

                If Session("oid") = Nothing Or Session("oid") = "" Then
                    Dim lastrate As Integer
                    lastrate = GetStrData("select top 1 rate2idrvalue from ql_mstrate2 where currencyoid =1 order by rate2oid desc")

                    sSql = "INSERT INTO QL_trndpar(cmpcode,branch_code,trndparoid,trndparno,trndpardate,custoid,cashbankoid,trndparacctgoid,payreftype,payduedate,payrefno,trndparamt,trndparamtidr,trndparamtusd,trndparnote,trndparacumamt,trndparstatus,createuser,createtime,upduser,updtime, currencyoid, currencyrate,girodtloid, flagdp, taxtype) VALUES ('" & CompnyCode & "','" & cabangDDL.SelectedValue & "'," & Session("DPARInit") & ",'" & Tchar(trndparno.Text) & "', '" & CDate(toDate(trndpardate.Text)) & "','" & custoid.Text & "',0, " & trndparacctgoid.SelectedValue & ",'','" & CDate(toDate(payduedate.Text)) & "','" & Tchar(payrefno.Text) & "'," & ToDouble(trndparamt.Text) & "," & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyUSDValue & ", '" & Tchar(trndparnote.Text) & "',0,'" & trndparstatus.Text & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "', CURRENT_TIMESTAMP,1, " & lastrate & ",0, 'NORMAL', 'NON TAX')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trndpar SET trndparno='" & Tchar(trndparno.Text) & "',trndpardate='" & CDate(toDate(trndpardate.Text)) & "', trndparacctgoid=" & trndparacctgoid.SelectedValue & ",trndparamt=" & ToDouble(trndparamt.Text) & ",trndparamtidr=" & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyIDRValue & ",trndparamtusd=" & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyUSDValue & ", payreftype='" & payreftype.SelectedValue & "',payduedate='" & CDate(toDate(payduedate.Text)) & "', payrefno='" & Tchar(payrefno.Text) & "', trndparnote='" & Tchar(trndparnote.Text) & "',custoid='" & custoid.Text & "', trndparacumamt=0,trndparstatus='" & trndparstatus.Text & "',upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND trndparoid=" & trndparoid.Text & " and branch_code='" & cabangDDL.SelectedValue & "'"

                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit() : conn.Close()
            Catch ex As Exception
                objTrans.Rollback() : conn.Close() : trndparstatus.Text = "In Process"
                showMessage(ex.ToString, 2)
                Exit Sub
            End Try

            If Session("TblDPARInitdtl") Is Nothing Then
                If trndparstatus.Text = "POST" Then
                    Response.Redirect("trninitdpar.aspx?awal=true")
                Else
                    Response.Redirect("trninitdpar.aspx?awal=true")
                End If
            End If
        End If
    End Sub

    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~\accounting\trninitdpar.aspx?awal=true")
    End Sub

    Protected Sub btnposting1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trndparstatus.Text = "Post"
        ImageButton3_Click(sender, e)
    End Sub

    Protected Sub btnDelete1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_trndpar WHERE cmpcode='" & CompnyCode & "' AND trndparoid=" & trndparoid.Text & " and branch_code = '" & cabangDDL.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
        Response.Redirect("trninitdpar.aspx?awal=true")
    End Sub
#End Region

    Protected Sub cabangDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitAllDDL()
    End Sub

    Protected Sub trndparamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndparamt.TextChanged
        trndparamt.Text = ToMaskEdit(trndparamt.Text, 4)
    End Sub
End Class
