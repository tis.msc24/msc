<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptStock2.aspx.vb" Inherits="rptStock2" Title="MSC - Laporan Stock Inc. Value" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Laporan Stock Inc. Value" Width="328px"></asp:Label></th>
        </tr>
<tr>
<th align="center" style="background-color: transparent" valign="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE id="tb"><TBODY><TR><TD id="TD28" align=left runat="server"><asp:Label id="Label2" runat="server" Width="102px" Text="Tipe Laporan "></asp:Label></TD><TD id="td29" align=center runat="server">:</TD><TD id="tdtipe3" align=left colSpan=3 runat="server"><asp:DropDownList id="type" runat="server" Width="78px" CssClass="inpText" AutoPostBack="True"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD id="Td1" align=left runat="server"><asp:Label id="Label12" runat="server" Text="Periode "></asp:Label></TD><TD id="Td2" align=center runat="server">:</TD><TD id="TD44" align=left colSpan=3 runat="server"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgDateAwal" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgDateAkhir" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/MM/yyyy)</asp:Label></TD></TR><TR><TD id="Td3" align=left runat="server" visible="false"><asp:Label id="Label9" runat="server" Text="Cabang"></asp:Label></TD><TD id="Td4" align=left runat="server" visible="false">:</TD><TD id="Td5" align=left runat="server" visible="false"><asp:DropDownList id="dd_branch" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD id="tdperiod3" align=left runat="server" visible="false"><asp:Label id="Label1" runat="server" Text="Lokasi "></asp:Label></TD><TD id="Td6" align=left runat="server" visible="false">:</TD><TD id="tdperiod4" align=left runat="server" visible="false"><asp:DropDownList id="ddlLocation" runat="server" Width="250px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD id="TD31" align=left runat="server"><asp:CheckBox id="PIC" runat="server" Text="Filter PIC"></asp:CheckBox></TD><TD id="TD32" align=left runat="server" Visible="true">:</TD><TD id="TD30" align=left runat="server"><asp:DropDownList id="ddlPIC" runat="server" Width="150px" CssClass="inpText"></asp:DropDownList> <asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left runat="server">Jenis Barang</TD><TD align=left runat="server" Visible="true">:</TD><TD align=left runat="server"><asp:DropDownList id="JenisBarangDDL" runat="server" CssClass="inpText" Font-Size="Small"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Katalog"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="ItemName" runat="server" Width="255px" CssClass="inpTextDisabled" TextMode="MultiLine" Rows="2"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton></TD></TR><TR><TD id="Td10" align=left colSpan=5 runat="server" visible="true"><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR></TBODY></TABLE><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <CR:CrystalReportViewer id="crv" runat="server" AutoDataBind="true" HasCrystalLogo="False" HasPrintButton="False" DisplayGroupTree="False" HasExportButton="False" HasSearchButton="False" HasViewList="False" HasDrillUpButton="False" HasToggleGroupTreeButton="False" HasZoomFactorList="False"></CR:CrystalReportViewer> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="dateAwal" Format="dd/MM/yyyy" PopupButtonID="ImgDateAwal"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="dateAkhir" Format="dd/MM/yyyy" PopupButtonID="ImgDateAkhir"></ajaxToolkit:CalendarExtender> 
</ContentTemplate>
        <Triggers>
<asp:PostBackTrigger ControlID="ibPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
</Triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=4><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Katalog</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4>Filter :&nbsp;&nbsp;&nbsp; <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Kode Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=center colSpan=4>Jenis Barang :&nbsp;&nbsp; <asp:DropDownList id="dd_stock" runat="server" CssClass="inpText" Font-Size="Small"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR>
    <tr>
        <td align="center" class="Label" colspan="4">
            <asp:ImageButton ID="btnSelectAll" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectall.png" />&nbsp;<asp:ImageButton
                ID="btnSelectNone" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectnone.png" />&nbsp;<asp:ImageButton
                    ID="btnViewChecked" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewselected.png" /></td>
    </tr>
    <TR><TD class="Label" align=center colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 740px; HEIGHT: 325px; BACKGROUND-COLOR: beige"><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" CellPadding="4" DataKeyNames="itemcode,itemdesc,itemoid,satuan3" AutoGenerateColumns="False" GridLines="None" PageSize="100" UseAccessibleHeader="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=4><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblListMat" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
</asp:UpdatePanel><asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></th>
</tr>
    </table>
</asp:Content>

