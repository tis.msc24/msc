<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="Master_Job.aspx.vb" Inherits="Master_Master_Job" title="MSC - Data Job" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table>
        <tr>
            <td align="left" colspan="3">
                <table width="960">
                    <tr>
                        <td align="left" char="header" colspan="3" style="background-color: silver">
                            <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                                ForeColor="Maroon" Text=".: Data Job"></asp:Label></td>
                    </tr>
                </table>
                &nbsp;<ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<TABLE width=600><TBODY><TR><TD style="WIDTH: 101px"><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w80"></asp:Label></TD><TD style="WIDTH: 11px"><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w81"></asp:Label></TD><TD style="WIDTH: 151px"><asp:DropDownList id="FilterDDL" runat="server" Width="137px" CssClass="inpText" __designer:wfdid="w82"><asp:ListItem Value="a.itservcode">Item Service Code</asp:ListItem>
<asp:ListItem Value="a.itservdesc">Deskripsi</asp:ListItem>
<asp:ListItem Value="c.gendesc">Jenis Barang</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 210px"><asp:TextBox id="FilterText" runat="server" Width="189px" CssClass="inpText" __designer:wfdid="w83"></asp:TextBox></TD><TD style="WIDTH: 56px"><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w84"></asp:ImageButton></TD><TD style="WIDTH: 14px"><asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w85"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 101px; HEIGHT: 15px"></TD><TD style="WIDTH: 11px; HEIGHT: 15px"></TD><TD style="WIDTH: 151px; HEIGHT: 15px"></TD><TD style="WIDTH: 210px; HEIGHT: 15px"></TD><TD style="WIDTH: 56px; HEIGHT: 15px"></TD><TD style="WIDTH: 14px; HEIGHT: 15px"></TD></TR></TBODY></TABLE><asp:GridView id="GVmstjob" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w78" PageSize="15" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="Data Not Found" GridLines="None">
<RowStyle BackColor="#EFF3FB"></RowStyle>

<EmptyDataRowStyle BackColor="White" BorderColor="White" ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="itservoid" DataNavigateUrlFormatString="~\master\Master_Job.aspx?idPage={0}" DataTextField="itservcode" HeaderText="Item Service Code">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="itservtype" HeaderText="Tipe Pengerjaan">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Jenis Barang"></asp:BoundField>
<asp:BoundField DataField="itservdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itservtarif" HeaderText="Tarif Pengerjaan">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itservpoint" HeaderText="Poin Pengerjaan">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itservnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp; 
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <strong>
                    <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                    ::List Job </strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width=500><TBODY><TR><TD style="WIDTH: 160px"><asp:Label id="Label8" runat="server" Width="112px" Text="Item Service Code" __designer:wfdid="w39"></asp:Label><asp:Label id="Label3" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w2"></asp:Label></TD><TD><asp:Label id="Label11" runat="server" Text=":" __designer:wfdid="w40"></asp:Label></TD><TD style="WIDTH: 323px"><asp:TextBox id="itservcode" runat="server" CssClass="inpText" __designer:wfdid="w41" ReadOnly="True"></asp:TextBox>&nbsp; <asp:Label id="i_u" runat="server" ForeColor="Red" Text="New" __designer:wfdid="w43" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 160px"><asp:Label id="Label21" runat="server" Width="112px" Text="Jenis Barang" __designer:wfdid="w57"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 323px"><asp:DropDownList id="Jenisddl" runat="server" Width="184px" CssClass="inpText" __designer:wfdid="w16"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 160px"><asp:Label id="Label10" runat="server" Width="96px" Text="Tipe Pengerjaan" __designer:wfdid="w44"></asp:Label><asp:Label id="Label4" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w3"></asp:Label></TD><TD><asp:Label id="Label6" runat="server" Text=":" __designer:wfdid="w45"></asp:Label></TD><TD style="WIDTH: 323px"><asp:DropDownList id="itservtypeoid" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w46"></asp:DropDownList>&nbsp;</TD></TR><TR><TD style="WIDTH: 160px"><asp:Label id="Label9" runat="server" Width="56px" Text="Deskripsi" __designer:wfdid="w48"></asp:Label><asp:Label id="Label5" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w4"></asp:Label></TD><TD><asp:Label id="Label12" runat="server" Text=":" __designer:wfdid="w49"></asp:Label></TD><TD style="WIDTH: 323px"><asp:DropDownList id="itservdesc" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem>Ringan</asp:ListItem>
<asp:ListItem>Sedang</asp:ListItem>
<asp:ListItem>Berat</asp:ListItem>
<asp:ListItem>Other</asp:ListItem>
</asp:DropDownList> <asp:Label id="Label20" runat="server" CssClass="Important" Text="Maks. 70 Karakter" __designer:wfdid="w52" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 160px"><asp:Label id="Label14" runat="server" Width="96px" Text="Tarif Pengerjaan" __designer:wfdid="w20"></asp:Label><asp:Label id="Label7" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w5"></asp:Label></TD><TD><asp:Label id="Label17" runat="server" Text=":" __designer:wfdid="w54"></asp:Label></TD><TD style="WIDTH: 323px"><asp:TextBox id="itservtarif" runat="server" CssClass="inpText" __designer:wfdid="w55" AutoPostBack="True" MaxLength="14"></asp:TextBox>&nbsp; <ajaxToolkit:MaskedEditExtender id="meepChange" runat="server" __designer:wfdid="w56" TargetControlID="itservtarif" Mask="9,999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="WIDTH: 160px"><asp:Label id="Label15" runat="server" Width="112px" Text="Poin Pengerjaan" __designer:wfdid="w57"></asp:Label></TD><TD><asp:Label id="Label18" runat="server" Text=":" __designer:wfdid="w58"></asp:Label></TD><TD style="WIDTH: 323px"><asp:TextBox id="itservpoint" runat="server" CssClass="inpText" __designer:wfdid="w59" AutoPostBack="True" MaxLength="14"></asp:TextBox> <ajaxToolkit:MaskedEditExtender id="meepPoint" runat="server" __designer:wfdid="w60" TargetControlID="itservpoint" Mask="9,999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="WIDTH: 160px"><asp:Label id="Label16" runat="server" Text="Keterangan" __designer:wfdid="w61"></asp:Label></TD><TD><asp:Label id="Label19" runat="server" Text=":" __designer:wfdid="w62"></asp:Label></TD><TD style="WIDTH: 323px"><asp:TextBox id="itservnote" runat="server" CssClass="inpText" __designer:wfdid="w63" AutoPostBack="True" MaxLength="50" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:Label id="Label13" runat="server" CssClass="Important" Text="Maks. 70 Karakter" __designer:wfdid="w2"></asp:Label> <asp:TextBox id="itservoid" runat="server" Width="22px" CssClass="inpText" __designer:wfdid="w64" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="hiddencode" runat="server" __designer:wfdid="w65" Visible="False"></asp:TextBox></TD></TR></TBODY></TABLE><TABLE width=500><TBODY><TR><TD style="WIDTH: 333px; HEIGHT: 15px" colSpan=3>Created On <asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w66"></asp:Label>&nbsp;By&nbsp;<asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w67"></asp:Label></TD></TR><TR><TD style="WIDTH: 333px" colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w68"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w69"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w70" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <strong>
                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                    ::Form Job</strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <br />
    &nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
        SelectCommand="SELECT QL_mstitemserv.* FROM QL_mstitemserv"></asp:SqlDataSource>
    <br />
    <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w255" Visible="False"><TABLE width=350><TBODY><TR><TD style="BACKGROUND-COLOR: red"><asp:Label id="lblCaption" runat="server" Width="342px" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w256"></asp:Label></TD></TR><TR><TD align=center><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/warn.png" ImageAlign="AbsMiddle" __designer:wfdid="w257"></asp:Image> <asp:Label id="lblPopUpMsg" runat="server" Width="211px" ForeColor="Red" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD></TD></TR><TR><TD align=center colSpan=1><asp:ImageButton id="imbPopUpOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w259"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w260" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w261" TargetControlID="bePopUpMsg" DropShadow="True" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

