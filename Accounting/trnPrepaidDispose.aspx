<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPrepaidDispose.aspx.vb" Inherits="Accounting_trnPrePaidDispose" title="Untitled Page" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Pre - Paid Disposal"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel2" runat="server"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 118px" align=left>Cabang</TD><TD align=left colSpan=4><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" __designer:wfdid="w66"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 118px" align=left><asp:CheckBox id="cbPeriode" runat="server" Width="67px" Text="Periode" __designer:wfdid="w67"></asp:CheckBox></TD><TD align=left colSpan=4><asp:TextBox id="txtPeriode1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w68"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w69"></asp:ImageButton> <asp:Label id="Label160" runat="server" Text="to" __designer:wfdid="w70"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w71"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w72"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 118px" align=left><asp:CheckBox id="cbDesc" runat="server" Width="98px" Text="Dispose No" __designer:wfdid="w73"></asp:CheckBox></TD><TD align=left colSpan=4><asp:TextBox id="txtFilter" runat="server" Width="237px" CssClass="inpText" __designer:wfdid="w74"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 118px" align=left><asp:CheckBox id="cbBlmPosting" runat="server" Width="116px" Text="Not Posted Yet" __designer:wfdid="w75"></asp:CheckBox></TD><TD align=left colSpan=4><asp:ImageButton id="BtnFind" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w76"></asp:ImageButton> <asp:ImageButton id="BtnViewALL" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w77"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="GVPrePaid" runat="server" Width="100%" Font-Bold="False" ForeColor="Red" __designer:wfdid="w60" OnSelectedIndexChanged="GVPrePaid_SelectedIndexChanged" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="branch_code,prepaiddisposeoid" AllowPaging="True" GridLines="None" OnPageIndexChanging="GVPrePaid_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="prepaiddisposeoid" HeaderText="Dis.oid" Visible="False">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddisposeno" HeaderText="Disposal No">
<HeaderStyle CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outlet" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstcode" HeaderText="Pre-Paid Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstdesc" HeaderText="Pre-Paid Desc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddisposedate" HeaderText="Dispose Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="85px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddisposestatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="90px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label38" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 118px" align=left><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w79" TargetControlID="txtPeriode1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w80" TargetControlID="txtPeriode2" Format="MM/dd/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" __designer:wfdid="w81" TargetControlID="txtPeriode1" CultureName="en-US" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" __designer:wfdid="w82" TargetControlID="txtPeriode2" CultureName="en-US" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=5><asp:Label id="lblViewInfo" runat="server" CssClass="Important" Text="Click button Find or View All to view data" __designer:wfdid="w83"></asp:Label></TD></TR></TBODY></TABLE>&nbsp;
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
                        <HeaderTemplate>
<STRONG><SPAN style="FONT-SIZE: 9pt"><asp:Image id="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/../Images/corner.gif"></asp:Image> List of Pre-Paid Disposal :.</SPAN></STRONG>&nbsp; 
</HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<DIV style="HEIGHT: 100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 5px" align=left><asp:Label id="Outlet" runat="server" Text="Cabang" __designer:wfdid="w181"></asp:Label></TD><TD style="WIDTH: 1048px" align=left><asp:DropDownList id="DDLCabang" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w248" AutoPostBack="True" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged"></asp:DropDownList></TD><TD align=left><asp:Label id="Label4" runat="server" Width="117px" Text="Prepaid Dispose No" __designer:wfdid="w249"></asp:Label></TD><TD style="WIDTH: 1621px" align=left><asp:TextBox id="PrePaidDisposeNo" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w250" ReadOnly="True"></asp:TextBox></TD><TD align=left><asp:Label id="Label6" runat="server" Text="Description" __designer:wfdid="w251"></asp:Label></TD><TD style="WIDTH: 225px" align=left><asp:TextBox id="PrePaiddesc" runat="server" Width="250px" CssClass="inpTextDisabled" __designer:wfdid="w252" MaxLength="200" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 5px; HEIGHT: 15px" align=left><asp:Label id="lblFaDtl" runat="server" Width="81px" Text="Akun Prepaid" __designer:wfdid="w253"></asp:Label></TD><TD style="WIDTH: 1048px; HEIGHT: 15px" align=left><asp:TextBox id="PrepaidCode" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w254"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCari2" onclick="btnCari2_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w255"></asp:ImageButton> <asp:ImageButton id="btnHapus2" onclick="btnHapus2_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w256"></asp:ImageButton></TD><TD style="HEIGHT: 15px" align=left><asp:Label id="Label5" runat="server" Width="82px" Text="Prepaid Date" __designer:wfdid="w257"></asp:Label></TD><TD style="WIDTH: 1621px; HEIGHT: 15px" align=left><asp:TextBox id="PrePaidDate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w258" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w259" Visible="False"></asp:ImageButton> </TD><TD style="HEIGHT: 15px" align=left><asp:Label id="lblFaMst" runat="server" Text="Akun Cash/Bank" __designer:wfdid="w261" Visible="False"></asp:Label></TD><TD style="WIDTH: 225px; HEIGHT: 15px" align=left><asp:DropDownList id="cashbankacctgoid" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w262" AutoPostBack="True" Enabled="False" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 5px" align=left><asp:Label id="Label14" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w194" Visible="False"></asp:Label><asp:Label id="PrePaidmstoid" runat="server" __designer:wfdid="w263" Visible="False"></asp:Label><asp:Label id="lblPOST" runat="server" Text="lblPOST" __designer:wfdid="w264" Visible="False"></asp:Label><asp:Label id="PrepaidGVoid" runat="server" Text="0" __designer:wfdid="w265" Visible="False"></asp:Label></TD><TD align=left colSpan=5><asp:GridView id="GvPrePaidmst" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w268" OnSelectedIndexChanged="GvPrePaidmst_SelectedIndexChanged" Visible="False" PageSize="5" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="prepaidmstoid,prepaidmstcode,prepaidmstdesc,prepaidmstdate,PrepaidAmt,BookValue,Accumulation,prepaidmstlengthvalue,prepaidmstacctgoid,prepaidmstpayacctgoid,mstacctg,payacctg" AllowPaging="True" GridLines="None" OnPageIndexChanging="GvPrePaidmst_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="outletname" HeaderText="Cabang">
<FooterStyle HorizontalAlign="Center"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstoid" HeaderText="PrePaidOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="prepaidmstcode" HeaderText="PrePaid No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstdesc" HeaderText="PrePaid Desc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstdate" HeaderText="PrePaid Date">
<FooterStyle HorizontalAlign="Center"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="85px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="85px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="BookValue" HeaderText="Book Value" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="85px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PrepaidAmt" HeaderText="PrePaid Amt" Visible="False"></asp:BoundField>
<asp:BoundField DataField="prepaidmstlengthvalue" HeaderText="Dept Month" Visible="False"></asp:BoundField>
<asp:BoundField DataField="prepaidmstpayacctgoid" HeaderText="prepaidmstpayacctgoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="prepaidmstacctgoid" HeaderText="prepaidmstacctgoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 5px; HEIGHT: 22px" align=left><asp:Label id="Label2" runat="server" Width="107px" Text="Expense Account" __designer:wfdid="w203"></asp:Label></TD><TD style="HEIGHT: 22px" align=left colSpan=2><asp:DropDownList id="prepaidmstacctgoid" runat="server" Width="340px" CssClass="inpTextDisabled" __designer:wfdid="w270" AutoPostBack="True" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD><TD style="WIDTH: 1621px; HEIGHT: 22px" align=left></TD><TD style="HEIGHT: 22px" align=left><asp:Label id="Label13" runat="server" Width="98px" Text="PrePaid DueDate" __designer:wfdid="w272" Visible="False"></asp:Label></TD><TD style="WIDTH: 225px; HEIGHT: 22px" align=left><asp:TextBox id="PrePaidDueDate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w274" Enabled="False" Visible="False"></asp:TextBox> <asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w275" Visible="False"></asp:ImageButton> <asp:Label id="Label19" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w276" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 5px; HEIGHT: 22px" align=left><asp:Label id="Label16" runat="server" Width="110px" Text="Accum Account" __designer:wfdid="w211"></asp:Label></TD><TD style="HEIGHT: 22px" align=left colSpan=2><asp:DropDownList id="prepaidmstpayacctgoid" runat="server" Width="340px" CssClass="inpTextDisabled" __designer:wfdid="w278" AutoPostBack="True" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD><TD style="WIDTH: 1621px; HEIGHT: 22px" align=left></TD><TD style="HEIGHT: 22px" align=left><asp:Label id="Label1" runat="server" Text="Pembayaran" __designer:wfdid="w279" Visible="False"></asp:Label></TD><TD style="WIDTH: 225px; HEIGHT: 22px" align=left><asp:DropDownList id="payflag" runat="server" Width="105px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w280" AutoPostBack="True" Enabled="False" Visible="False"><asp:ListItem Text="CASH" Value="BKM"></asp:ListItem>
<asp:ListItem Text="BANK" Value="BBM"></asp:ListItem>
</asp:DropDownList> <asp:Label id="CutOffDatePaid" runat="server" __designer:wfdid="w281" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 5px; HEIGHT: 22px" align=left><asp:Label id="Label12" runat="server" Width="76px" Text="Pre-Paid Amt" __designer:wfdid="w282"></asp:Label></TD><TD style="WIDTH: 1048px; HEIGHT: 22px" align=left><asp:TextBox id="PrePaidAmt" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w283" ReadOnly="True" Enabled="False">0</asp:TextBox></TD><TD style="HEIGHT: 22px" align=left><asp:Label id="Label29" runat="server" Width="72px" Text="Book Value" __designer:wfdid="w284"></asp:Label></TD><TD style="WIDTH: 1621px; HEIGHT: 22px" align=left><asp:TextBox id="BookValue" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w285" AutoPostBack="True" MaxLength="16" Enabled="False">0</asp:TextBox></TD><TD style="HEIGHT: 22px" align=left><asp:Label id="Label10" runat="server" Text="Accumulation" __designer:wfdid="w286"></asp:Label></TD><TD style="WIDTH: 225px; HEIGHT: 22px" align=left><asp:TextBox id="fixdepval" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w287" ReadOnly="True" Enabled="False">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 5px" align=left><asp:Label id="Label8" runat="server" Width="102px" Text="Sisa Depreciation" __designer:wfdid="w288"></asp:Label></TD><TD style="WIDTH: 1048px" align=left><asp:TextBox id="PrePaiddepmonth" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w289" AutoPostBack="True" MaxLength="2" Enabled="False">0</asp:TextBox> <asp:Label id="Label9" runat="server" Font-Size="X-Small" ForeColor="Red" Text="(month)" __designer:wfdid="w290"></asp:Label> </TD><TD align=left><asp:Label id="Label240" runat="server" Width="110px" ForeColor="DarkBlue" Text="Prepaid Diff Value" __designer:wfdid="w291" Visible="False"></asp:Label></TD><TD style="WIDTH: 1621px" align=left><asp:TextBox id="DiffValue" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w292" ReadOnly="True" Enabled="False" Visible="False">0</asp:TextBox></TD><TD align=left><asp:Label id="Label3" runat="server" Width="106px" Text="Prepaid Akun Date" __designer:wfdid="w293" Visible="False"></asp:Label></TD><TD style="WIDTH: 225px" align=left><asp:TextBox id="AkunPrePaidDate" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w294" ReadOnly="True" Enabled="False" Visible="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 5px" align=left><asp:Label id="Label28" runat="server" Width="75px" Text="Type Prepaid" __designer:wfdid="w295" Visible="False"></asp:Label></TD><TD style="WIDTH: 1048px" align=left><asp:DropDownList id="PrePaidGroup" runat="server" Width="170px" CssClass="inpTextDisabled" __designer:wfdid="w296" AutoPostBack="True" Enabled="False" Visible="False"></asp:DropDownList></TD><TD align=left><asp:Label id="Label7" runat="server" Text="Nilai Jual" __designer:wfdid="w297" Visible="False"></asp:Label></TD><TD style="WIDTH: 1621px" align=left><asp:TextBox id="NilaiJual" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w298" AutoPostBack="True" MaxLength="16" Enabled="False" Visible="False" OnTextChanged="NilaiJual_TextChanged"></asp:TextBox></TD><TD align=left><asp:Label id="Label27" runat="server" Text="Ref No" __designer:wfdid="w299" Visible="False"></asp:Label></TD><TD style="WIDTH: 225px" align=left><asp:TextBox id="PrePaidRefno" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w300" MaxLength="20" Enabled="False" Visible="False"></asp:TextBox>&nbsp;<asp:Label id="Label15" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w301" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="GVPrePaiddtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w302" OnSelectedIndexChanged="GVPrePaiddtl_SelectedIndexChanged" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="prepaiddtldate,prepaiddtlstatus" AllowPaging="True" GridLines="None" OnPageIndexChanging="GVPrePaiddtl_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField>
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="75px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="prepaiddtldate" HeaderText="Payment Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlaccumamt" DataFormatString="{0:#,##0.00}" HeaderText="PrePaid Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlamount" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlaccum" DataFormatString="{0:#,##0.00}" HeaderText="Accumulation">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label11" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 20px" align=left colSpan=6>Created By&nbsp;<asp:Label id="lblUser" runat="server" Font-Bold="True" __designer:wfdid="w317">User</asp:Label>&nbsp; On <asp:Label id="lblTime" runat="server" Font-Bold="True" __designer:wfdid="w318">Time</asp:Label>,&nbsp; Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w319"></asp:Label>&nbsp; By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w320"></asp:Label></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="BtnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w1"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w2"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w3"></asp:ImageButton> <asp:ImageButton id="BtnPosting" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w4"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w311" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w312"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w313" TargetControlID="PrePaidDate" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" ErrorTooltipEnabled="True" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w314" TargetControlID="PrePaidDate" Format="dd/MM/yyyy" PopupButtonID="btnDate"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ce4" runat="server" __designer:wfdid="w315" TargetControlID="PrePaidDueDate" Format="dd/MM/yyyy" PopupButtonID="btnDueDate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meedate" runat="server" __designer:wfdid="w316" TargetControlID="PrePaidDueDate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date" InputDirection="RightToLeft" ClearMaskOnLostFocus="true"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></DIV>
</ContentTemplate>
</asp:UpdatePanel> 
                            <asp:UpdatePanel id="UpdatePanel78" runat="server">
                                <contenttemplate>
<asp:Panel id="panelMsg" runat="server" Width="264px" CssClass="modalMsgBox" Visible="False"><TABLE style="WIDTH: 99%; HEIGHT: 109%"><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeValidasi" runat="server" TargetControlID="btnValidasi" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnValidasi" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
                            </asp:UpdatePanel>
</ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/../Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Pre-Paid Disposal :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
</asp:Content>