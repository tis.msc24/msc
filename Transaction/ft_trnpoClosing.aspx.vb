'Prgmr : zipi ; Update : zipi on 15.03.13
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnpo
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Private cKon As New Koneksi
    Private cProc As New ClassProcedure
#End Region

#Region "Function"
    Function checkApprovaldata(ByVal sTableName As String, ByVal sEvent As String, ByVal sOid As String, _
     ByVal sRequestUser As String, ByVal sStatus As String) As DataTable
        Dim ssql As String = "SELECT CMPCODE,APPROVALOID,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME," & _
             "OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval WHERE CmpCode='" & CompnyCode & _
             "' and RequestUser LIKE '" & Tchar(sRequestUser) & "%' and STATUSREQUEST = '" & sStatus & "' " & _
             " AND TABLENAME='" & sTableName & "' and OID=" & sOid & " and EVENT='" & sEvent & "' ORDER BY REQUESTDATE DESC"
        Return cKon.ambiltabel(ssql, "QL_APPROVAL")
    End Function

#End Region

#Region "Procedure"
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub BindData(ByVal filterCompCode As String)
        Try
            Dim swhere As String = ""
            If chkPeriod.Checked Then
                If FilterPeriod1.Text = "" And FilterPeriod2.Text = "" Then
                    showMessage("Please fill periode!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                Else
                    swhere = swhere & "AND a.closetime BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND '" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
                End If
            Else
                'swhere = swhere & "AND  (a.updtime BETWEEN '" & Format(GetServerTime, "MM/01/yyyy") & "' AND '" & Format(GetServerTime, "MM/dd/yyyy") & "' ) "
            End If
            If FilterText.Text <> "" Then
                If ddlFilter.SelectedValue = "pono" Then
                    swhere = swhere & "AND  (a.trnbelipono LIKE '%" & Tchar(FilterText.Text) & "%') "
                ElseIf ddlFilter.SelectedValue = "suppname" Then
                    swhere = swhere & "AND  (s.suppname LIKE '%" & Tchar(FilterText.Text) & "%') "
                End If
            End If
            sSql = "SELECT a.trnbelimstoid pomstoid,a.trnbelipono pono,a.trnbelipodate  podate,a.trnbelistatus postatus, s.suppname supplier, a.trnbelitype potype, a.trnbelistatus postatus, a.updtime, a.closetime podateclosed, a.closenote ponoteclosed FROM QL_pomst a INNER JOIN QL_mstsupp s ON a.trnsuppoid=s.suppoid and a.trnbelistatus='CLOSED MANUAL' WHERE a.cmpcode = '" & CompnyCode & "' " & swhere & " ORDER BY periodacctg desc, right(a.trnbelipono,4) DESC "
            FillGV(tbldata, sSql, "QL_pomst")

        Catch ex As Exception
            showMessage("Input Invalid!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End Try
    End Sub

    Private Sub dtlPOBind()
        sSql = "SELECT d.trnbelidtlqty qtyPO,m.itemdesc material,g2.gendesc as unit,trnbelidtlseq seq, ISNULL((select sum(s.qty) from QL_trnsjbelidtl s inner join ql_trnsjbelimst a on a.trnsjbelioid=s.trnsjbelioid where s.trnbelidtloid=d.trnbelidtloid and trnsjbelistatus in ('Approved','INVOICED')),0.00 ) qtyDelivery,trnbelidtlqty-ISNULL((select sum(s.qty) from QL_trnsjbelidtl s inner join ql_trnsjbelimst a on a.trnsjbelioid=s.trnsjbelioid where s.trnbelidtloid=d.trnbelidtloid and trnsjbelistatus in ('Approved','INVOICED')),0.00 ) osQty " & _
              "FROM QL_podtl d " & _
              "inner join QL_mstitem m on m.itemoid=d.itemoid " & _
              "inner join QL_mstgen g2 on g2.genoid=d.trnbelidtlunit where d.trnbelimstoid=" & IOID.Text & " And d.trnbelidtlstatus <> 'completed' Order By d.trnbelidtlseq"
        FillGV(tbldtl, sSql, "ql_podtl")
    End Sub

    Private Sub FillTextBox(ByVal iID As Int64)

    End Sub

    Public Sub binddataSupp(ByVal sqlPlus As String)
        sSql = "SELECT po.trnbelimstoid pomstoid, po.trnbelipono pono,case po.statusPOBackorder when 'T' then 'BO' else 'PO' end as Type ,po.trnbelipodate podate,po.trnbelideliverydate podeliv,po.trnsuppoid suppoid,s.suppname FROM ql_pomst po INNER JOIN ql_mstsupp s ON po.trnsuppoid=s.suppoid AND po.cmpcode=s.cmpcode AND po.trnbelistatus<>'REJECTED' WHERE po.trnbelimstoid IN (Select dt.trnbelimstoid From (SELECT D.trnbelimstoid, D.trnbelidtlqty - ISNULL((	Select SUM(s.qty) FROM QL_trnsjbelidtl s INNER JOIN ql_trnsjbelimst sj ON sj.trnsjbelioid=s.trnsjbelioid WHERE s.trnbelidtloid=d.trnbelidtloid AND sj.trnsjbelistatus <>'REJECTED'),0) QTY FROM QL_podtl D)DT WHERE DT.QTY>0) AND po.TRNBELItype = 'GROSIR' AND (po.trnbelipono LIKE '%" & Tchar(suppliername.Text) & "%' OR s.suppname LIKE '%" & Tchar(suppliername.Text) & "%') /*AND po.upduser='" & Session("UserID") & "'*/ ORDER BY po.trnbelipono DESC"
        FillGV(gvSupplier, sSql, "QL_mstsupp")
        tbldtl.Visible = True
    End Sub
#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'initcat(DDlmatCatmst.SelectedValue)
        'matcab.Text = "13"

        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\ft_trnpoClosing.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Purchase Order Closing"
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

        Session("oid") = Request.QueryString("oid")
        'btnsavemstr.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Close this data?');")

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime, "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
            BindData(CompnyCode)

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        Dim objTable As New DataTable : objTable = Session("TblDtl") ': MsgBox(objTable.Rows.Count)
        tbldtl.DataSource = objTable : tbldtl.DataBind() : tbldtl.Visible = True
        'fillTot() ': ReAmount()
    End Sub

    Protected Sub TblData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles tbldata.PageIndexChanging
        tbldata.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If chkPeriod.Checked Then
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text))
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text))
                If dat1 > dat2 Then
                    showMessage("Start Date must be less than End Date!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                End If
            Catch ex As Exception
                showMessage("Invalid date format!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End Try
        End If

        Session("FilterText") = Tchar(FilterText.Text)
        Session("FilterDDL") = ddlFilter.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub TblDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles tbldtl.PageIndexChanging
        tbldtl.PageIndex = e.NewPageIndex
        tbldtl.Visible = True
        tbldtl.DataSource = Session("TblDtl")
        tbldtl.DataBind()
    End Sub

    Protected Sub tbldtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldtl.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(3).Text = NewMaskEdit(ToDouble(e.Row.Cells(3).Text))
                e.Row.Cells(5).Text = NewMaskEdit(ToDouble(e.Row.Cells(5).Text))
                e.Row.Cells(4).Text = NewMaskEdit(ToDouble(e.Row.Cells(4).Text))
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
        End Try
    End Sub

    Protected Sub btnsavemstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsavemstr.Click
        If IOID.Text.Trim = "" Then
            showMessage("Please select PO No first !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If txtNote.Text = "" Then
            showMessage("Please Fill Reason !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus from QL_approvalstructure  WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNPOCLOSE' order by approvallevel"
        Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
        If dtData2.Rows.Count > 0 Then
            Session("TblApproval") = dtData2
        Else
            showMessage("Tidak ada User untuk Approved SO, Silahkan hubungi admin dahulu", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Dim sMsg As String = ""
        ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
        sSql = "select trnbelistatus from QL_pomst Where trnbelimstoid = " & IOID.Text
        Dim stat As String = GetStrData(sSql)
        If stat.ToLower = "in process" Or stat.ToLower = "rejected" Then
            sMsg &= "- Maaf, data masih berstatus '" & stat & "' dan tidak boleh diproses, tekan Cancel untuk kembali ke awal..!!<br />"
        ElseIf stat.ToLower = "in approval" Or stat.ToLower = "closed manual" Then
            sMsg &= "- Maaf, status transaksi sudah '" & stat & "', tekan Cancel dan cek kembali status transaksi ini..!!<br />"
        ElseIf stat Is Nothing Or stat = "" Then
            sMsg &= "- Maaf, data Purchase Order tidak valid..!!<br>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & "  - WARNING", 2, "modalMsgBox")
            Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            ' insert table master
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Session("AppOid") = ClassFunction.GenerateID("QL_Approval", CompnyCode)
            Try
                If Not Session("TblApproval") Is Nothing Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objApp As DataTable = Session("TblApproval")
                        For Ap As Int16 = 0 To objApp.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,branch_code,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus) VALUES ('" & CompnyCode & "'," & Session("AppOid") + Ap & ", '" & Session("branch_id") & "','" & "PO" & IOID.Text & "_" & Session("AppOid") + Ap & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_TRNPOCLOSE', '" & IOID.Text & "','In Approval','0', '" & objApp.Rows(Ap).Item("approvaluser") & "','1/1/1900','" & objApp.Rows(Ap).Item("approvaltype") & "','1', '" & objApp.Rows(Ap).Item("approvalstatus") & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objApp.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If
                'Cek di PDo jika ada status PDO yg blm approved maka harus di selesaikan dl
                If GetStrData("SELECT COUNT(-1) FROM ql_trnsjbelimst WHERE trnbelipono='" & suppliername.Text & "' AND trnsjbelistatus not in ('Approved','INVOICED','Rejected')") > 0 Then
                    showMessage("Ada Purchase Delivery Order yang belum di approved/rejected !", CompnyName & " - Warning", 2, "modalMsgBoxWarn") : Exit Sub
                End If
                sSql = "UPDATE QL_pomst SET trnbelistatus='In Approval',trnbelires1='POCLOSE', closenote='" & Tchar(txtNote.Text) & "',closetime=CURRENT_TIMESTAMP WHERE trnbelimstoid='" & IOID.Text & "' and cmpcode='" & CompnyCode & "' AND branch_code ='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'sSql = "UPDATE QL_podtl SET trnbelidtlstatus='Completed' WHERE trnbelimstoid='" & IOID.Text & "' and cmpcode='" & CompnyCode & "' and branch_code = '" & Session("branch_id") & "'"
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, CompnyName & "  - ERROR", 1, "modalMsgBox")
                xCmd.Connection.Close() : Exit Sub
            End Try
        End If
        Session("TblDtl") = Nothing
        Session("oid") = Nothing
        'showMessage("Data telah diClose !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Response.Redirect("~\Transaction\ft_trnpoClosing.aspx?awal=true")
    End Sub

    Protected Sub btnCancelMstr1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelMstr.Click
        Session("oid") = Nothing
        Session("TblDtl") = Nothing
        Response.Redirect("~\Transaction\ft_trnpoClosing.aspx")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldata.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(6).Text = NewMaskEdit(ToDouble(e.Row.Cells(6).Text))
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ddlFilter.SelectedIndex = 0 : FilterText.Text = ""
        'chkPeriod.Checked = False :
        chkStatus.Checked = False

        FilterPeriod1.Text = Format(Now, "01/MM/yyyy")
        FilterPeriod2.Text = Format(Now, "dd/MM/yyyy")
        DDLStatus.SelectedIndex = 0
        Session("FilterText") = Nothing
        Session("FilterDDL") = Nothing
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvSupplier.Visible = True
        binddataSupp("")
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        gvSupplier.Visible = True
        binddataSupp("")
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        'ButtonSuplier.Visible = False : PanelSuplier.Visible = False : ModalPopupExtender1.Hide()
        IOID.Text = gvSupplier.SelectedDataKey("pomstoid")
        suppliername.Text = gvSupplier.SelectedDataKey(1).ToString().Trim
        suppname.Text = gvSupplier.SelectedDataKey(2).ToString().Trim
        cProc.DisposeGridView(sender)
        gvSupplier.Visible = False

        osPO.Text = GetStrData("SELECT trnbelidtlqty-ISNULL((select sum(s.qty) from QL_trnsjbelidtl s inner join ql_trnsjbelimst a on a.trnsjbelioid=s.trnsjbelioid where s.trnbelidtloid=d.trnbelidtloid and trnsjbelistatus in ('Approved','INVOICED')),0 ) osQty FROM QL_podtl d inner join QL_mstitem m on m.itemoid=d.itemoid inner join QL_mstgen g2 on g2.genoid=d.trnbelidtlunit where d.trnbelimstoid=" & IOID.Text & " order by d.trnbelidtlseq")
        'Isikan detail informasinya
        dtlPOBind()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If lblMessage.Text = "Data telah diClose !" Then
            Response.Redirect("~\Transaction\ft_trnpoClosing.aspx?awal=true")
        Else
            PanelMsgBox.Visible = False
            beMsgBox.Visible = False
            mpeMsgbox.Hide()
            dtlPOBind()
        End If
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        IOID.Text = ""
        suppliername.Text = ""
        osPO.Text = ""
        tbldtl.DataSource = Nothing
        tbldtl.DataBind()
    End Sub
#End Region 
 
End Class

