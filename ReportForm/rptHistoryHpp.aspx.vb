Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptstock
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Public Sub initddl()
        FillDDL(person, "select personoid, personname from ql_mstperson where status = 'Aktif' AND cmpcode = '" & CompnyCode & "' AND PERSONPOST = (SELECT genoid FROM QL_mstgen WHERE gengroup='JOBPOSITION' AND gendesc = 'SALES PERSON')")
        person.Items.Add(New ListItem("ALL", "ALL"))
        person.SelectedValue = "ALL"

        sSql = "select distinct periodacctg,periodacctg from ql_crdmtr c order by c.periodacctg desc"
        FillDDL(period, sSql)

        '--------------------------------------------------------------------------------------
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP' AND cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("SEMUA GRUP", "ALL GRUP"))
        FilterDDLGrup.SelectedValue = "ALL GRUP"

        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMTYPE' AND cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("SEMUA TYPE", "ALL TYPE"))
        FilterDDLSubGrup.SelectedValue = "ALL TYPE"

        If Session("UserID") = "admin" Or Session("UserID") = "popy" Or Session("UserID") = "alchu" Or Session("UserID") = "endry" Then
            sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "' And PERSONSTATUS in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC like '%AMP%')"
            FillDDL(DDLpic, sSql)
            DDLpic.Items.Add(New ListItem("ALL", "ALL"))
            DDLpic.SelectedValue = "ALL"
        Else
            sSql = "SELECT personoid, personname FROM QL_mstperson prsn INNER JOIN QL_MSTPROF prof ON prof.USERNAME = prsn.PERSONNAME WHERE prof.cmpcode = 'MSC' And prsn.PERSONSTATUS in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC like '%AMP%') /*AND prof.userid='" & Session("UserID") & "'*/"
            FillDDL(DDLpic, sSql)
            'DDLpic.Items.Add(New ListItem("ALL", "ALL"))
            'DDLpic.SelectedValue = "ALL"
        End If
    End Sub

    Public Sub showPrint(ByVal showtype As String)
        Dim sWhere As String = "  "
        lblkonfirmasi.Text = ""

        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Try
            Dim namaPDF As String = "" : Dim sWherePIC As String = ""
            Dim arCode() As String = ItemName.Text.Split(";")
            Dim sCodeIn As String = "" : Dim sQel As String = ""
            Dim adr As String = ""

            Dim sWhereitem As String = " Where tgltransaksi between '" & CDate(toDate(dateAwal.Text)) & " 0:0:0' and '" & CDate(toDate(dateAkhir.Text)) & " 23:59:59'"

            For C1 As Integer = 0 To arCode.Length - 1
                If arCode(C1) <> "" Then
                    sCodeIn &= "'" & arCode(C1) & "',"
                End If
            Next

            If sCodeIn <> "" Then
                sWhereitem &= " AND itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
            End If
            'sWhereitem &= IIf(ToDouble(itemoid.Text) > 0, " AND itemoid=" & itemoid.Text, "")

            If FilterDDLGrup.SelectedValue <> "ALL GRUP" Then
                sWhereitem &= " And itemgroupoid=" & FilterDDLGrup.SelectedValue & ""
            End If

            If FilterDDLSubGrup.SelectedValue <> "ALL TYPE" Then
                sWhereitem &= " And itemtype=" & FilterDDLSubGrup.SelectedValue & ""
            End If

            If DDLpic.SelectedValue <> "ALL" Then
                sWherePIC &= " AND personoid = " & DDLpic.SelectedValue & ""
            End If

            vReport = New ReportDocument
            If showtype = "excel" Then
                vReport.Load(Server.MapPath("~\Report\rptHistHPPExl.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptHistHPP.rpt"))
            End If
            vReport.SetParameterValue("sWhere", sWhereitem)
            'vReport.SetParameterValue("sWherePic", sWherePIC)
            namaPDF = "HistoryHPP_"

            'vReport.PrintOptions.PaperSize = PaperSize.PaperTabloid
            'Dim paperMargin As CrystalDecisions.Shared.PageMargins
            ' 1 centimeter = 567 twips
            ' 1 inch = 1440 twips
            'paperMargin.leftMargin = 1 * 567
            'paperMargin.rightMargin = 1 * 567
            'paperMargin.topMargin = 1 * 567
            'paperMargin.bottomMargin = 1 * 567
            'vReport.PrintOptions.ApplyPageMargins(paperMargin)
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'crvMutasiStock.DisplayGroupTree = False
            'crvMutasiStock.ReportSource = vReport
            vReport.SetParameterValue("CompanyName", System.Configuration.ConfigurationManager.AppSettings("CompanyName"))
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If showtype = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "view" Then
                Session("diprint") = "True"
                crv.DisplayGroupTree = False
                crv.ReportSource = vReport
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub  

    Public Sub BindDataListItem()

        sSql = "Select * from (Select 'False' Checkvalue,i.itemcode, i.itemdesc,i.itemoid, g.gendesc satuan3, i.merk from ql_mstitem i inner join ql_mstgen g on g.genoid=satuan1 and g.gengroup='ITEMUNIT' and itemflag='AKTIF' Where " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text.Trim) & "%' ) dt"
        Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstitem")
        Session("TblMat") = dt
        Session("TblMatView") = Session("TblMat")
        gvItem.DataSource = Session("TblMatView")
        gvItem.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub
#End Region

#Region "Event"
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        If Not Session("listofitem") Is Nothing Then
            gvItem.DataSource = Session("listofitem")
        Else
            gvItem.DataSource = Nothing
        End If
        gvItem.DataBind()
        gvItem.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        crv.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = "" : itemoid.Text = ""
        gvItem.Visible = False
        Response.Redirect("rptHistoryHpp.aspx?awal=true")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch

            Response.Redirect("rptHistoryHpp.aspx")
        End If

        Page.Title = CompnyName & " - Laporan History HPP Item"
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrint("view")
            End If
        Else
            initddl()
            Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
            dateAwal.Text = "01/" & lastPeriodene.Substring(5).Trim & "/" & lastPeriodene.Substring(0, 4).Trim
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")
            type_SelectedIndexChanged(sender, e)
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If vReport Is Nothing = False Then
            If vReport.IsLoaded = True Then
                vReport.Dispose()
                vReport.Close()
            End If
        End If
    End Sub

    Protected Sub type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles type.SelectedIndexChanged
        Session("diprint") = "False"
        If type.SelectedValue = "Summary" Then
            lblfilterqty.Visible = True
            filterqty.Visible = True
            ddlfilterqty.Visible = True
            tdperiod2.Visible = False
            tdPeriod1.Visible = False

            Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
            dateAwal.Text = "01/" & lastPeriodene.Substring(5).Trim & "/" & lastPeriodene.Substring(0, 4).Trim
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")

        Else ' detail
            lblfilterqty.Visible = False
            filterqty.Visible = False
            ddlfilterqty.Visible = False
            tdperiod2.Visible = True
            tdPeriod1.Visible = True
        End If
    End Sub

    'Protected Sub ibkonversi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    showPrintExcelKonversi("ListofStock")
    'End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvItem.DataSource = Nothing : gvItem.DataBind()
        BindDataListItem()
     End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("rptHistoryHpp.aspx?awal=true")
    End Sub

    Protected Sub filterqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles filterqty.TextChanged
        filterqty.Text = ToMaskEdit(ToDouble(filterqty.Text), 3)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("view")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        'showPrintExcel("ListofStock")
        showPrint("excel")
    End Sub

    Protected Sub ibPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibPDF.Click
        showPrint("pdf")
    End Sub
#End Region

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Maaf, data tidak ditemukan..!!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Maaf, data tidak ditemukan..!!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "Checkvalue=True"
        If dv.Count > 0 Then

            If itemname.Text <> "" Then
                For C1 As Integer = 0 To dv.Count - 1
                    itemname.Text &= ";" & dv(C1)("itemcode").ToString & ""
                Next
            Else
                For C1 As Integer = 0 To dv.Count - 1
                    itemname.Text &= dv(C1)("itemcode").ToString & ";"
                Next
            End If


            If itemname.Text <> "" Then
                itemname.Text = Left(itemname.Text, itemname.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            showMessage("- Maaf, Silahkan pilih katalog, dengan beri centang pada kolom pilih..<BR>", "INFORMASI", 2)
        End If
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub
End Class
