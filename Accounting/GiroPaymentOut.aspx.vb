'Prgmr:Ocid Nduuutt | LastUpdt:07.05.2015
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_GiroPaymentOut
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompanyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim objCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dv As DataView
    Dim cKoneksi As New Koneksi
    Dim cProc As ClassProcedure
    Dim dtStaticItem As DataTable
    Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
    Dim objDs As New DataSet
    Private strSearch As String = ""
    Private reportAR As New ReportDocument
#End Region

#Region "Procedures"
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Protected Sub ClearDtlAR()
        bank.Text = "" : payrefno.Text = "" : custname.Text = ""
        paymentoid.Text = "" : payduedate.Text = "" : payamt.Text = "0.00"
        paynote.Text = "" : document.Text = "" : custname.Text = ""
        Dim tbDtl As DataTable
        If Session("tbldtl") Is Nothing Then
            payseq.Text = 1
        Else
            tbDtl = Session("tbldtl")
            payseq.Text = tbDtl.Rows.Count + 1
        End If
        Session("ItemLinePayment") = payseq.Text
        i_u2.Text = "New Detail"
        gvReceiptDtl.Columns(7).Visible = True
        gvReceiptDtl.SelectedIndex = -1
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssclass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Protected Sub calcTotalInGrid()
        Dim countrow As Integer = gvmst.Rows.Count
        Dim gtotal As Double = 0
        For i As Integer = 0 To countrow - 1
            gtotal += ToDouble(gvmst.Rows(i).Cells(6).Text)
        Next
        lblgrandtotal.Text = ToMaskEdit(gtotal, 4)
    End Sub

    Public Sub UnabledCheckBox()
        'Dim objTable As DataTable
        'Dim objRow() As DataRow
        'objTable = Session("tbldata")
        'objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        'If objRow.Length() > 0 Then
        '    For i As Integer = 0 To objRow.Length() - 1
        '        If Trim(objRow(i)("GiroStatus").ToString) = "POST" Then
        '            Dim row As System.Web.UI.WebControls.GridViewRow = gvmst.Rows(i)
        '            If (row.RowType = DataControlRowType.DataRow) Then
        '                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
        '                For Each myControl As System.Web.UI.Control In cc
        '                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
        '                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
        '                    End If
        '                Next
        '            End If
        '        End If
        '    Next
        'End If
    End Sub

    Protected Sub binddata()
        Dim st1, st2 As Boolean : Dim sMsg As String = ""
        Try
            Dim dt1 As Date = CDate(toDate(txtPeriode1.Text)) : st1 = True
        Catch ex As Exception
            sMsg &= "- Tanggal awal salah!!<BR>" : st1 = False
        End Try
        Try
            Dim dt2 As Date = CDate(toDate(txtPeriode2.Text)) : st2 = True
        Catch ex As Exception
            sMsg &= "- Tanggal Akhir salah!!<BR>" : st2 = False
        End Try
        If st1 And st2 Then
            If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
                sMsg &= "- Tanggal awal harus <= tanggal akhir !!<BR>"
            End If
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompanyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If
        Dim mySqlConn As New SqlConnection(ConnStr)
        Dim sqlSelect As String = "SELECT gpm.GiroPaymentMstOid, gpm.GiroAmt, cb.cashbankno, gpm.Tipe,gpm.ExecutionDate, gpm.BiayaAmt, gpm.GiroStatus, gpm.GiroPaymentStatus, gironote, a.acctgdesc perkiraan FROM QL_GiroPaymentMst gpm INNER JOIN QL_trncashbankmst cb ON gpm.cashbankoid=cb.cashbankoid AND gpm.cmpcode=cb.cmpcode inner join ql_mstacctg a on a.acctgoid=cb.cashbankacctgoid WHERE gpm.cmpcode = '" & CompnyCode & "' AND cb.cashbankgroup='GIRO OUT' and cb.cashbankno like '%" & Tchar(nobukti.Text.Trim) & "%'  " & IIf(coafilter.SelectedValue = "ALL", "", " and cb.cashbankacctgoid=" & coafilter.SelectedValue)
        Dim sqlWhere As String = "  AND gpm.Tipe = 'OUT'"
        If txtPeriode1.Text = "" Or txtPeriode2.Text = "" Then
            showMessage("Isi Tanggal Periode !", CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If txtPeriode1.Text <> "" And txtPeriode2.Text <> "" Then
            sqlWhere += " AND gpm.ExecutionDate>='" & CDate(toDate(txtPeriode1.Text)) & "' and gpm.ExecutionDate<='" & CDate(toDate(txtPeriode2.Text)) & "'"
        End If

        If FilterStatus.SelectedValue <> "ALL" Then
            If FilterStatus.SelectedValue = "POST" Then
                sqlWhere += " AND gpm.GiroPaymentStatus = 'POST'"
            Else
                sqlWhere += " AND gpm.GiroPaymentStatus <> 'POST'"
            End If
        End If

        sqlSelect = sqlSelect & sqlWhere & " ORDER BY cashbankdate desc,cashbankno desc"
        strSearch = sqlSelect

        Dim mySqlDA As New SqlDataAdapter(sqlSelect, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        Session("tbldata") = objTable
        gvmst.DataSource = objDs.Tables(0)
        gvmst.DataBind()
        calcTotalInGrid()
        UnabledCheckBox()

    End Sub

    Private Sub BindLastSearch(ByVal sSearchString As String)
        Dim mySqlDA As New SqlDataAdapter(sSearchString, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        Session("tbldata") = objTable
        gvmst.DataSource = objDs.Tables(0)
        gvmst.DataBind()
        calcTotalInGrid()
        UnabledCheckBox()
    End Sub

    Public Sub CheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("GiroPaymentStatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvmst.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub UncheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("GiroPaymentStatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvmst.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub bindDataGiro(ByVal noGiro As String, ByVal cmpcode As String)
        sSql = " select * from (SELECT p.cashbankoid, p.payREFno, s.suppname as custname, ISNULL(p.payduedate,'1/1/1900') as payduedate, sum(ISNULL(p.payamt,0)) as payamt, p.paybankoid as payacctgoid, 'QL_CASHBANKMST-PAYAP' paymentref_table, s.suppoid, cbm.cashbankno document, 0 bankacctgoid, gb.gendesc bank FROM QL_trnpayap p inner join ql_trncashbankmst cbm on cbm.cashbankoid=p.cashbankoid AND p.cmpcode=cbm.cmpcode AND Cbm.cashbanktype in ('BGK','BCK') and girodtloid=0 INNER JOIN QL_trnbelimst bm ON p.payrefoid=bm.trnbelimstoid AND p.cmpcode=bm.cmpcode AND p.payreftype='QL_trnbelimst' INNER JOIN QL_mstsupp s ON p.suppoid=s.suppoid AND p.cmpcode=s.cmpcode inner join ql_mstgen gb on gb.genoid=cbm.bankoid inner join ql_mstacctg a on a.acctgoid=cbm.cashbankacctgoid WHERE p.cmpcode ='" & CompnyCode & "' AND p.cashbankoid NOT IN (SELECT G.paymentoid FROM QL_GiroPaymentDtl G WHERE G.paymentref_table='QL_CASHBANKMST-PAYAP' AND G.CustOid=S.SUPPOID) group by p.cashbankoid, p.payREFno, s.suppname, ISNULL(p.payduedate,'1/1/1900') , p.paybankoid, s.suppoid, cbm.cashbankno, gb.gendesc UNION SELECT p.cashbankoid, p.payREFno, s.suppname as custname, ISNULL(p.payduedate,'1/1/1900') as payduedate, sum(ISNULL(p.payamt,0)) as payamt, p.paybankoid as payacctgoid, 'QL_CASHBANKMST-PAYAP' paymentref_table, s.suppoid, cbm.cashbankno document, 0 bankacctgoid, gb.gendesc bank FROM QL_trnpayap p inner join ql_trncashbankmst cbm on cbm.cashbankoid=p.cashbankoid AND p.cmpcode=cbm.cmpcode AND Cbm.cashbanktype in ('BGK','BCK') INNER JOIN QL_trnbelimst bm ON p.payrefoid=bm.trnbelimstoid AND p.cmpcode=bm.cmpcode AND p.payreftype='QL_trnbelimst' INNER JOIN QL_mstsupp s ON p.suppoid=s.suppoid AND p.cmpcode=s.cmpcode inner join ql_mstgen gb on gb.genoid=cbm.bankoid inner join ql_mstacctg a on a.acctgoid=cbm.cashbankacctgoid WHERE p.cmpcode ='" & CompnyCode & "' AND p.cashbankoid NOT IN (SELECT G.paymentoid FROM QL_GiroPaymentDtl G WHERE G.paymentref_table='QL_CASHBANKMST-PAYAP' AND G.CustOid=S.SUPPOID) group by p.cashbankoid, p.payREFno, s.suppname, ISNULL(p.payduedate,'1/1/1900') , p.paybankoid, s.suppoid, cbm.cashbankno, gb.gendesc UNION SELECT trndpapoid, dp.payrefno, s.suppname,dp.payduedate, trndpapamt, dp.cashbankacctgoid, 'QL_TRNDPAP', s.suppoid, dp.trndpapno , isnull(c.bankacctgoid,dp.cashbankacctgoid)bankacctgoid, gb.gendesc bank FROM QL_TRNDPAP dp inner join ql_mstsupp s on s.suppoid=dp.suppoid and Dp.cmpcode=s.cmpcode inner join ql_trncashbankmst cbm on cbm.cashbankoid=dp.cashbankoid and girodtloid=0 left join ql_mstgen gb on gb.genoid=cbm.bankoid left outer join ql_mstcreditcard c on c.nokartu=dp.payrefno left outer join ql_mstacctg g on g.acctgoid=c.bankacctgoid where Dp.trndpapoid NOT IN (SELECT G.paymentoid FROM QL_GiroPaymentDtl G WHERE G.paymentref_table='QL_TRNDPAP' AND G.CustOid=S.SUPPOID) and (dp.trndpapno like 'DPAP%' or dp.trndpapno like 'DPAP%') UNION SELECT trndpaRoid, dp.payrefno, s.CUSTNAME ,dp.payduedate, trndpaRamt, dp.cashbankacctgoid, 'QL_TRNDPAP_BACK', s.CUSTOID, dp.trndpaRno , dp.cashbankacctgoid bankacctgoid , gb.gendesc bank FROM QL_TRNDPAR_BACK dp inner join ql_mstCUST s on s.CUSToid=dp.CUSToid and Dp.cmpcode=s.cmpcode inner join ql_trncashbankmst cbm on cbm.cashbankoid=dp.cashbankoid and girodtloid=0  inner join ql_mstgen gb on gb.genoid=cbm.bankoid where Dp.trndpaRoid NOT IN (SELECT G.paymentoid FROM QL_GiroPaymentDtl G WHERE G.paymentref_table='QL_TRNDPAP_BACK' AND G.CustOid=S.CUSTOID)   and   dp.trndpaRno  like 'R.DP.AP.G%' ) dt where " & ddlgirofilter.SelectedValue & " like '%" & Tchar(tbGiro.Text.Trim) & "%' and payacctgoid <> 185  "
        FillGV(gvGiro, sSql, "ql_payar")
    End Sub

    Private Function SetTabelDetail() As DataTable
        Dim nuDT As New DataTable("ql_trnpayar")
        nuDT.Columns.Add("paymentoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payrefno", Type.GetType("System.String"))
        nuDT.Columns.Add("custname", Type.GetType("System.String"))
        nuDT.Columns.Add("payduedate", Type.GetType("System.String"))
        nuDT.Columns.Add("payamt", Type.GetType("System.Double"))
        nuDT.Columns.Add("paynote", Type.GetType("System.String"))
        nuDT.Columns.Add("paymentref_table", Type.GetType("System.String"))
        nuDT.Columns.Add("document", Type.GetType("System.String"))
        nuDT.Columns.Add("bank", Type.GetType("System.String"))
        nuDT.Columns.Add("payseq", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payacctgoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("custoid", Type.GetType("System.Int32"))
        Return nuDT
    End Function

    Protected Sub ClearDtlGiro()
        paymentoid.Text = "" : payrefno.Text = ""
        payamt.Text = "0.00" : paynote.Text = ""
        custname.Text = "" : cashbankacctgoid.Enabled = True
        cashbankacctgoid.CssClass = "inpText"
        Dim tbDtl As DataTable
        If Session("tbldtl") Is Nothing Then
            payseq.Text = 1
        Else
            tbDtl = Session("tbldtl")
            payseq.Text = tbDtl.Rows.Count + 1
            If payseq.Text > 1 Then
                cashbankacctgoid.Enabled = False
                cashbankacctgoid.CssClass = "inpTextDisabled"
            End If
        End If

        Session("ItemLinePayment") = payseq.Text
        i_u2.Text = "New Detail"
        gvReceiptDtl.Columns(7).Visible = True
        gvReceiptDtl.SelectedIndex = -1
        bankacctgoid.Text = 0
    End Sub

    Protected Sub calcTotalInGridDtl()
        Dim countrow As Integer = gvReceiptDtl.Rows.Count
        Dim gtotal As Decimal = 0
        For i As Integer = 0 To countrow - 1
            gtotal += ToDouble(gvReceiptDtl.Rows(i).Cells(7).Text)
        Next
        totalreceipt.Text = ToMaskEdit(gtotal, 4)
    End Sub

    Private Sub FillCurrencyRate(ByVal currencyoid As Integer)
        sSql = "select top 1 D.curratestoIDRbeli from QL_mstcurrhist D INNER JOIN ql_mstcurr m on M.CMPCODE=D.cmpcode AND m.currencyoid=d.curroid and  m.cmpcode='" & CompnyCode & "' and currencyoid=" & currencyoid & " order by d.currdate desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        cashbankcurrate.Text = NewMaskEdit(ToDouble(objCmd.ExecuteScalar))
        conn.Close()
    End Sub

    Sub initallddl()
        ' Init Currency
        sSql = "select currencyoid,currencycode from QL_mstcurr where cmpcode='" & CompnyCode & "' order by currencycode"
        FillDDL(cashbankcurroid, sSql) : FillCurrencyRate(cashbankcurroid.SelectedValue)
    End Sub

    Sub initddlcasbank(ByVal cashbank As String)
        cashbankacctgoid.Items.Clear()
        If Trim(cashbank) = "CASH" Then
            'Dim varCash As String = GetVarInterface("VAR_CASH", CompnyCode)
            'sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varCash & "%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode+'-'+acctgdesc"
            'FillDDL(cashbankacctgoid, sSql)
            FillDDLAcctg(cashbankacctgoid, "VAR_CASH", Session("branch_id"))
            If cashbankacctgoid.Items.Count < 1 Then
                showMessage("Isi/Buat account CASH di master accounting!!", _
                    CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
            End If
        ElseIf Trim(cashbank) = "NONCASH" Then
            'Dim varBank As String = GetVarInterface("VAR_BANK", CompnyCode)
            'sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE (acctgcode LIKE '" & varBank & "%') AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode ) ORDER BY acctgcode+'-'+acctgdesc "
            'FillDDL(cashbankacctgoid, sSql)
            FillDDLAcctg(cashbankacctgoid, "VAR_BANK", Session("branch_id"))
            If cashbankacctgoid.Items.Count < 0 Then
                showMessage("Isi/Buat account CASH di master accounting!!", _
                    CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
            End If
        End If
    End Sub

    Private Sub PrintNota(ByVal oid As String, ByVal no As String)
        'untuk print
        reportAR.Load(Server.MapPath("~/report/printPencairanGiroAP.rpt"))
        reportAR.SetParameterValue("oid", oid)
        reportAR.SetParameterValue("companyname", CompanyName)
        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("DBServer")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DBName")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, reportAR)
        reportAR.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        reportAR.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        reportAR.Close() : reportAR.Dispose()
    End Sub

    Sub FillTextBox(ByVal id As String)
        dtltb.Visible = True
        sSql = "SELECT DISTINCT gm.GiroPaymentStatus, gm.ExecutionDate, gm.GiroNote, gm.CashBankOid, cb.cashbankno, gm.GiroFlag, gm.CurrencyOid, gm.CurrencyRate, cb.cashbankacctgoid, gm.upduser, gm.updtime, gm.Tipe FROM QL_GiroPaymentMst gm INNER JOIN QL_trncashbankmst cb ON gm.CashBankOid=cb.cashbankoid AND gm.cmpcode=cb.cmpcode WHERE gm.GiroPaymentMstOid=" & id
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objCmd.CommandText = sSql
        xreader = objCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                cashbankoid.Text = Trim(xreader.Item("cashbankoid"))
                cashbankno.Text = Trim(xreader.Item("cashbankno"))
                payflag.SelectedValue = Trim(xreader.Item("GiroFlag").ToString())
                HiddenField1.Value = Trim(xreader.Item("GiroFlag").ToString())
                cashbankcurroid.Text = xreader("CurrencyOid").ToString
                cashbankcurrate.Text = ToMaskEdit(ToDouble(xreader("CurrencyRate").ToString), 4)
                initddlcasbank(payflag.SelectedValue)
                If payflag.SelectedValue = "CASH" Then
                    lblCashBank.Text = "Cash"
                ElseIf payflag.SelectedValue = "NONCASH" Then
                    lblCashBank.Text = "Bank"
                End If
                cashbankacctgoid.SelectedValue = Trim(xreader.Item("cashbankacctgoid"))
                HiddenField2.Value = Trim(xreader.Item("cashbankacctgoid"))
                cashbanknote.Text = Trim(xreader("GiroNote"))
                UpdUser.Text = xreader("upduser")
                UpdTime.Text = xreader("updtime")
                ddlGiroType.SelectedValue() = xreader("Tipe").ToString()
                paymentdate.Text = Format(CDate(Trim(xreader("ExecutionDate").ToString)), "dd/MM/yyyy")
                If Trim(xreader("GiroPaymentStatus").ToString) = "POST" Then
                    btnPosting.Visible = False : btnSave.Visible = False
                    btnDelete.Visible = False : lblPOST.Text = "POST"
                Else
                    btnPosting.Visible = True : btnSave.Visible = True
                    btnDelete.Visible = True : lblPOST.Text = ""
                End If
            End While
        End If
        xreader.Close() : conn.Close()

        sSql = "SELECT gd.paymentoid, gd.NoGiro as payrefno, case gd.paymentref_table when 'QL_TRNDPAR_BACK' then s.custname else c.suppname end custname, gd.custoid, gd.GiroAmt as payamt, gd.GiroNote as paynote, gd.DueDate as payduedate, gd.GiroAcctgOid as payacctgoid, '1' AS payseq, gd.paymentref_table, case gd.paymentref_table when 'QL_TRNPAYAP' then  (select cb.cashbankno from QL_trncashbankmst cb inner join QL_trnpayap p on p.cashbankoid=cb.cashbankoid and p.cmpcode=cb.cmpcode and p.paymentoid=gd.paymentoid) when 'QL_CASHBANKMST-PAYAP' then (select cb.cashbankno from QL_trncashbankmst cb WHERE gd.paymentoid=cb.cashbankoid) when 'QL_TRNDPAP' then (select dp.trndpapno from QL_trndpap dp where dp.cmpcode=gm.cmpcode and dp.trndpapoid=gd.paymentoid) when 'QL_TRNDPAR_BACK' then (select dp.trndpaRno from QL_trndpaR_BACK dp where dp.cmpcode=gm.cmpcode and dp.trndpaRoid=gd.paymentoid ) end document, COALESCE((select g.acctgdesc from QL_mstcreditcard cr inner join  ql_mstacctg g on g.acctgoid=cr.bankacctgoid  where cr.nokartu=gd.NoGiro),(SELECT AC.acctgdesc FROM QL_MSTACCTG AC WHERE AC.ACCTGOID=GD.GIROACCTGOID))bank FROM QL_GiroPaymentDtl gd INNER JOIN QL_GiroPaymentMst gm ON gd.GiroPaymentMstoid=gm.GiroPaymentMstOid AND gd.cmpcode=gm.cmpcode left outer JOIN QL_mstsupp c ON gd.custoid=c.suppoid and gd.cmpcode=c.cmpcode left outer JOIN QL_mstcust s ON gd.custoid=s.custoid and gd.cmpcode=S.cmpcode WHERE gm.GiroPaymentMstoid=" & id

        Dim dtPayment As DataTable : dtPayment = cKoneksi.ambiltabel(sSql, "QL_payar")
        If dtPayment.Rows.Count > 0 Then
            For C2 As Integer = 0 To dtPayment.Rows.Count - 1
                Dim ed As DataRow = dtPayment.Rows(C2)
                ed.BeginEdit()
                ed("payseq") += C2
                ed.EndEdit()
            Next
            payseq.Text = dtPayment.Rows.Count + 1
            Session("ItemLinePayment") = payseq.Text
            Session("tbldtl") = dtPayment : gvReceiptDtl.DataSource = dtPayment
            gvReceiptDtl.DataBind()
            calcTotalInGridDtl()
        Else
            showMessage("Tidak dapat menampilkan data detail pembayaran!!", CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
        End If
        ClearDtlGiro()
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlTemp As String = Session("SearchARR")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchARR") = sqlTemp
            Response.Redirect(Page.AppRelativeVirtualPath) '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        imbPrintNota.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to PRINT this data?');")
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================

        I_U.Text = "New Data"
        Page.Title = "Manifested Giro Out (A/P)"
        imbPrintNota.Visible = False
        If Not IsPostBack Then
            Dim varBank As String = GetVarInterface("VAR_BANK", Session("branch_id"))
            sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE (acctgcode LIKE '" & varBank & "%') AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)    order by 2"
            FillDDL(coafilter, sSql)
            coafilter.Items.Add("ALL")
            coafilter.SelectedValue = "ALL"
            paymentdate.Text = Format(CDate(GetServerTime()), "dd/MM/yyyy")
            initddlcasbank("CASH")
            initallddl()
            payflag.SelectedIndex = 0
            txtPeriode1.Text = Format(GetServerTime().AddDays(-1), "01/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            UnabledCheckBox()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                I_U.Text = "Update Data"
                FillTextBox(Session("oid"))
                If payflag.SelectedValue = "CASH" Then
                    payrefno.Text = ""
                End If
                imbPrintNota.Visible = True
                TabContainer1.ActiveTabIndex = 1
            Else
                I_U.Text = "New Data"
                btnDelete.Visible = False
                initddlcasbank(payflag.SelectedValue)
                UpdTime.Text = GetServerTime()
                UpdUser.Text = Session("UserID")
                payseq.Text = 1
                Session("ItemLinePayment") = payseq.Text
                TabContainer1.ActiveTabIndex = 0
            End If
            binddata()
        End If
        btnshowCOA.Visible = False : imbPrintNota.Visible = False
        If lblPOST.Text = "POST" Then
            I_U.Text = "Update Data"
            btnshowCOA.Visible = True
        Else
            btnSave.Visible = True
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GVmst.PageIndex = 0 : binddata()
        Session("SearchARR") = strSearch
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtPeriode1.Text = Format(GetServerTime().AddDays(-1), "01/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        FilterStatus.SelectedValue = "ALL" : coafilter.SelectedValue = "ALL"
        coafilter.SelectedIndex = 0 : nobukti.Text = ""
        binddata()
    End Sub

    Protected Sub imbLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("SearchARR") Is Nothing = False Then
            BindLastSearch(Session("SearchARR").ToString)
        End If
    End Sub

    Protected Sub imbSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub imbSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub imbPostSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub ImageButton8_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbInvoice.Click
        PanelGiro.Visible = True
        btnHiddenGiro.Visible = True
        ModalPopupExtenderGiro.Show()
        bindDataGiro(payrefno.Text.Trim, CompnyCode)
    End Sub

    Protected Sub btnFindInvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataGiro(Tchar(tbGiro.Text), CompnyCode)
        ModalPopupExtenderGiro.Show()
    End Sub

    Protected Sub imbViewAllInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        tbGiro.Text = ""
        bindDataGiro("", CompnyCode)
        ModalPopupExtenderGiro.Show()
    End Sub

    Protected Sub CloseSales_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelGiro.Visible = False
        btnHiddenGiro.Visible = False
        ModalPopupExtenderGiro.Hide()
    End Sub

    Protected Sub lkbInfo1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub gvGiro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvGiro.SelectedIndexChanged
        paymentoid.Text = gvGiro.SelectedDataKey("cashbankoid").ToString()
        payrefno.Text = gvGiro.SelectedDataKey(1).ToString()
        paymentref_table.Text = gvGiro.SelectedDataKey("paymentref_table").ToString()
        suppoid.Text = gvGiro.SelectedDataKey("suppoid").ToString()
        custname.Text = gvGiro.SelectedDataKey(2).ToString()
        payduedate.Text = Format(CDate(gvGiro.SelectedDataKey(3).ToString()), "dd/MM/yyyy")
        payamt.Text = ToMaskEdit(ToDouble(gvGiro.SelectedDataKey(4).ToString()), 4)
        giroacctgoid.Text = gvGiro.SelectedDataKey(5).ToString()
        document.Text = gvGiro.SelectedDataKey("document").ToString()
        PanelGiro.Visible = False : btnHiddenGiro.Visible = False : ModalPopupExtenderGiro.Hide()
    End Sub

    Protected Sub imbAddReceipt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAddReceipt.Click
        Dim sMsg As String = "" : dtltb.Visible = True
        If paymentoid.Text = "" Then : sMsg &= "- Pilih Giro terlebih dahulu !!<BR>" : End If
        If ToDouble(payamt.Text) <= 0 Then : sMsg &= "- Jumlah Giro > 0!!<BR>" : End If
        If paynote.Text.Trim.Length > 200 Then : sMsg &= "- Catatan  detail maksimal 200 karakter!!<BR>" : End If
        If sMsg <> "" Then
            showMessage(sMsg, CompanyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If
        If Session("tbldtl") Is Nothing Then
            Dim dtlTable As DataTable = SetTabelDetail()
            Session("tbldtl") = dtlTable
        End If

        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        If i_u2.Text = "New Detail" Then
            dv.RowFilter = "paymentoid='" & Trim(paymentoid.Text) & "'  and paymentref_table ='" & paymentref_table.Text & "'  "
        Else
            dv.RowFilter = "paymentoid='" & Trim(paymentoid.Text) & "'   and paymentref_table ='" & paymentref_table.Text & "'   And payseq <> " & payseq.Text
        End If
        If dv.Count > 0 Then
            showMessage("Data ini sudah di tambahkan sebelumnya!!", CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
            dv.RowFilter = "" : Exit Sub
        End If
        dv.RowFilter = ""
        'If toDate(payduedate.Text) > GetServerTime() Then
        '    showMessage("Can't Add data (Duedate must be <= '" & Format(GetServerTime, "dd/MM/yyyy") & "') !!", CompnyName & " - Information", 2)
        '    Exit Sub
        'End If

        If ToDouble(bankacctgoid.Text) > 0 And objTable.Rows.Count > 0 And cashbankacctgoid.SelectedValue <> ToDouble(bankacctgoid.Text) Then
            showMessage("Data ini punya bank yang berbeda !!", CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If ToDouble(bankacctgoid.Text) > 0 Then
            cashbankacctgoid.SelectedValue = ToDouble(bankacctgoid.Text)
        End If

        Dim oRow As DataRow
        If i_u2.Text = "New Detail" Then
            oRow = objTable.NewRow
            oRow("payseq") = payseq.Text
        Else
            oRow = objTable.Rows(payseq.Text - 1)
            oRow.BeginEdit()
        End If
        oRow("bank") = bank.Text
        oRow("paymentoid") = paymentoid.Text
        oRow("payrefno") = payrefno.Text
        oRow("custname") = custname.Text
        oRow("custoid") = suppoid.Text
        oRow("payduedate") = toDate(payduedate.Text)
        oRow("payamt") = ToMaskEdit(ToDouble(payamt.Text), 4)
        oRow("paynote") = paynote.Text
        oRow("paymentref_table") = paymentref_table.Text
        oRow("payacctgoid") = giroacctgoid.Text
        oRow("document") = document.Text
        If i_u2.Text = "New Detail" Then
            objTable.Rows.Add(oRow)
        Else
            oRow.EndEdit()
        End If
        Session("tbldtl") = objTable
        gvReceiptDtl.DataSource = Session("tbldtl")
        gvReceiptDtl.DataBind()
        ClearDtlGiro()
        calcTotalInGridDtl()
        ClearDtlAR()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If cashbankacctgoid.SelectedValue = "" Then
            sMsg &= "- COA Cash/Bank/Titipan/Giro Tidak di temukan !!"
        End If

        Try
            Dim dt As Date = CDate(toDate(paymentdate.Text))
        Catch ex As Exception
            sMsg &= "- Tanggal Salah!!<BR>"
        End Try
        If CDate(toDate(paymentdate.Text)) <= CDate(toDate(CutofDate.Text)) Then
            sMsg &= "- Tanggal Giro Tidak boleh <= CutoffDate (" & CutofDate.Text & ") !! <BR> "
        End If
        If Session("tbldtl") Is Nothing Then
            sMsg &= "- Isi detail giro !!<BR>"
            gvReceiptDtl.DataSource = Nothing
            gvReceiptDtl.DataBind()
        Else
            Dim objTableCek As DataTable : Dim objRowCek() As DataRow : objTableCek = Session("tbldtl")
            objRowCek = objTableCek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowCek.Length = 0 Then
                gvReceiptDtl.DataSource = objTableCek
                gvReceiptDtl.DataBind()
                sMsg &= "- Isi Detail Giro !!<BR>"
            End If

            For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
                sSql = "select COUNT(-1) from QL_GiroPaymentMst m inner join QL_GiroPaymentDtl d on m.cmpcode=d.cmpcode  and m.GiroPaymentMstOid=d.GiroPaymentMstOid and d.NoGiro='" & objTableCek.Rows(c1).Item("payrefno").ToString.Trim & "'  and m.GiroPaymentStatus='POST' AND D.PAYMENTOID =" & objTableCek.Rows(c1)("paymentoid") & "  AND PAYMENTREF_TABLE='" & objTableCek.Rows(c1)("paymentref_table") & "' "
                If ToDouble(GetStrData(sSql)) Then
                    sMsg &= "- No Giro (" & objTableCek.Rows(c1).Item("payrefno").ToString.Trim & ") dengan documen " & objTableCek.Rows(c1).Item("document").ToString.Trim & " sudah dicairkan !!<BR>"
                End If
            Next
        End If
        If cashbanknote.Text.Trim.Length > 200 Then
            sMsg &= "- Catatan maksimal 200 Karakter !!<BR>"
        End If
        If ToDouble(cashbankcurrate.Text) <= 0 Then
            cashbankcurrate.Text = ToMaskEdit(1, 4)
        End If
        If sMsg <> "" Then
            lblPOST.Text = ""
            showMessage(sMsg, CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & Session("branch_id") & "' AND gengroup='CABANG'")
        '==============================
        'Cek Peroide Bulanan Dari Crdgl
        '==============================
        If ToDouble(payamt.Text) > 0 Then
            'CEK PERIODE AKTIF BULANAN
            'sSql = "select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag ='OPEN'"
            'If GetStrData(sSql) <> "" Then
            sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctg(CDate(toDate(paymentdate.Text))) < GetStrData(sSql) Then
                showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>", CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
        'End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                Dim sCashBank As Integer = 0
                Dim sCBType As String = "" : Dim iCurID As Integer = 0
                ' Cashbank No
                'generat id cashbank
                sCashBank = GenerateID("QL_trncashbankmst", CompnyCode)
                Select Case payflag.SelectedValue
                    Case "CASH" : sCBType = "BKK"
                    Case "NONCASH" : sCBType = "RGK" '"BBK"
                    Case Else : sCBType = "BLK"
                End Select
                Dim sNo As String = sCBType & "/" & Format(CDate(toDate(paymentdate.Text)), "yy/MM/dd") & "/"
                sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1  FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%'"
                objCmd.CommandText = sSql
                iCurID = CInt(objCmd.ExecuteScalar)
                Session("vNoCashBank") = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), DefCounter)

                sSql = "INSERT into QL_trncashbankmst(cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime,cashbankcurroid,cashbankcurrate, PIC, PIC_REFNAME,branch_code) " & _
                    " VALUES ('" & CompnyCode & "'," & sCashBank & ",'" & Session("vNoCashBank") & "','" & lblPOST.Text & "','" & sCBType & "','GIRO OUT'," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(paymentdate.Text)) & "','" & Tchar(cashbanknote.Text) & "','" & Session("UserID") & "',current_timestamp," & cashbankcurroid.Text & "," & ToDouble(cashbankcurrate.Text) & ", 0,'','" & Session("branch_id") & "')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                ' Save Last cashbankoid for later use!!
                Session("LastCBMst") = sCashBank
                Dim sGiroMst As Integer = GenerateID("QL_GiroPaymentMst", CompnyCode)
                If ddlGiroType.SelectedValue() = "IN" Then
                    sSql = "INSERT INTO QL_GiroPaymentMst(cmpcode,GiroPaymentMstOid,Tipe,GiroAmt,InAcctgOid,OutAcctgOid,ExecutionDate,GiroStatus,CashBankOid,CurrencyOid,CurrencyRate,GiroNote,upduser,updtime,GiroPaymentStatus,GiroFlag,branch_code) VALUES ('" & CompnyCode & "'," & sGiroMst & ",'" & ddlGiroType.SelectedValue() & "'," & ToDouble(totalreceipt.Text) & "," & cashbankacctgoid.SelectedValue & ",0,'" & CDate(toDate(paymentdate.Text)) & "','CLOSE'," & Session("LastCBMst") & "," & cashbankcurroid.SelectedValue() & "," & ToDouble(cashbankcurrate.Text) & ",'" & Tchar(cashbanknote.Text) & "','" & Session("UserID") & "',current_timestamp,'" & Trim(lblPOST.Text) & "','" & payflag.SelectedValue() & "','" & Session("branch_id") & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                ElseIf ddlGiroType.SelectedValue() = "OUT" Then
                    sSql = "INSERT INTO QL_GiroPaymentMst(cmpcode,GiroPaymentMstOid,Tipe,GiroAmt,InAcctgOid,OutAcctgOid,ExecutionDate,GiroStatus,CashBankOid,CurrencyOid,CurrencyRate,GiroNote,upduser,updtime,GiroPaymentStatus,GiroFlag,branch_code) VALUES ('" & CompnyCode & "'," & sGiroMst & ",'" & ddlGiroType.SelectedValue() & "'," & ToDouble(totalreceipt.Text) & ",0," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(paymentdate.Text)) & "','CLOSE'," & Session("LastCBMst") & "," & cashbankcurroid.SelectedValue() & "," & ToDouble(cashbankcurrate.Text) & ",'" & Tchar(cashbanknote.Text) & "','" & Session("UserID") & "',current_timestamp,'" & Trim(lblPOST.Text) & "','" & payflag.SelectedValue() & "','" & Session("branch_id") & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                Session("LastGiroMst") = sGiroMst
                If Not Session("tbldtl") Is Nothing Then
                    Dim objTable As DataTable
                    Dim objRow() As DataRow
                    objTable = Session("tbldtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                    Dim sTemp As Integer = GenerateID("QL_GiroPaymentDtl", CompnyCode)

                    For C1 As Int16 = 0 To objRow.Length - 1
                        sSql = "INSERT into QL_GiroPaymentDtl(cmpcode,GiroPaymentDtlOid,GiroPaymentMstOid,CustOid,NoGiro,DueDate,GiroAmt,paymentoid,GiroNote,GiroAcctgOid, paymentref_table,branch_id) VALUES ('" & CompnyCode & "'," & sTemp + C1 & "," & Session("LastGiroMst") & "," & objRow(C1)("custoid") & ",'" & objRow(C1)("payrefno").ToString & "','" & objRow(C1)("payduedate").ToString & "'," & ToDouble(objRow(C1)("payamt")) & "," & objRow(C1)("paymentoid").ToString & ",'" & Tchar(objRow(C1)("paynote").ToString) & "'," & objRow(C1)("payacctgoid").ToString & ",'" & objRow(C1)("paymentref_table").ToString & "','" & Session("branch_id") & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Next

                    'update GiroPaymentDtl
                    sSql = "UPDATE QL_mstoid SET lastoid=" & sTemp + objRow.Length - 1 & " WHERE tablename='QL_GiroPaymentDtl' and cmpcode = '" & CompnyCode & "' "
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                'update GiroPaymentMst
                sSql = "update  QL_mstoid set lastoid=" & Session("LastGiroMst") & " where tablename = 'QL_GiroPaymentMst' and cmpcode = '" & CompnyCode & "' "
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                'Update lastoid dari QL_mstoid table QL_trncashbankmst 
                sSql = "update  QL_mstoid set lastoid=" & Session("LastCBMst") & " where tablename = 'QL_trncashbankmst' and cmpcode = '" & CompnyCode & "' "
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            Else
                Dim bUpdPayType As Boolean = False
                Dim sCBType As String = "" : Dim sCBCode As String = ""
                If payflag.SelectedValue <> HiddenField1.Value Or cashbankacctgoid.SelectedValue <> HiddenField2.Value Then ' jika ada perubahan pada payment type
                    Dim iCurID As Integer = 0
                    Select Case payflag.SelectedValue
                        Case "CASH" : sCBType = "BKK"
                        Case "NONCASH" : sCBType = "RGK" '"BBK"
                        Case Else : sCBType = "BLK"
                    End Select
                    Dim sNo As String = sCBType & "/" & Format(CDate(toDate(paymentdate.Text)), "yy/MM/dd") & "/"
                    sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1  FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%'"
                    objCmd.CommandText = sSql
                    iCurID = CInt(objCmd.ExecuteScalar)
                    Session("vNoCashBank") = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), DefCounter)
                    bUpdPayType = True
                Else
                    Session("vNoCashBank") = cashbankno.Text
                End If

                ' Update QL_trncashbankmst
                sSql = "UPDATE QL_trncashbankmst SET cashbankacctgoid=" & cashbankacctgoid.SelectedValue & ",cashbankdate='" & CDate(toDate(paymentdate.Text)) & "',cashbanknote='" & Tchar(cashbanknote.Text) & "',upduser = '" & Session("UserID") & "',updtime=current_timestamp,cashbankstatus='" & lblPOST.Text & "',cashbankcurroid=" & cashbankcurroid.Text & ",cashbankcurrate=" & ToDouble(cashbankcurrate.Text)
                If bUpdPayType Then
                    sSql &= ",cashbankno='" & Session("vNoCashBank") & "', cashbanktype='" & sCBType & "' "
                End If
                sSql &= " WHERE cmpcode = '" & CompnyCode & "' and cashbankoid = " & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                If ddlGiroType.SelectedValue() = "IN" Then
                    sSql = "UPDATE QL_GiroPaymentMst  SET Tipe='" & ddlGiroType.SelectedValue() & "', GiroAmt=" & ToDouble(totalreceipt.Text) & ", InAcctgOid=" & cashbankacctgoid.SelectedValue & ", ExecutionDate='" & CDate(toDate(paymentdate.Text)) & "', GiroStatus='CLOSE', CashBankOid=" & Trim(cashbankoid.Text) & ", CurrencyOid=" & cashbankcurroid.SelectedValue() & ", CurrencyRate=" & ToDouble(cashbankcurrate.Text) & ", GiroNote='" & Tchar(cashbanknote.Text) & "', upduser='" & Session("UserID") & "', updtime=current_timestamp, GiroPaymentStatus='" & Trim(lblPOST.Text) & "', GiroFlag='" & payflag.SelectedValue() & "', OutAcctgOid=0 WHERE GiroPaymentMstOid=" & Session("oid")
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                ElseIf ddlGiroType.SelectedValue() = "OUT" Then
                    sSql = "UPDATE QL_GiroPaymentMst SET Tipe='" & ddlGiroType.SelectedValue() & "', GiroAmt=" & ToDouble(totalreceipt.Text) & ", OutAcctgOid=" & cashbankacctgoid.SelectedValue & ", ExecutionDate='" & CDate(toDate(paymentdate.Text)) & "', GiroStatus='CLOSE', CashBankOid=" & Trim(cashbankoid.Text) & ", CurrencyOid=" & cashbankcurroid.SelectedValue() & ", CurrencyRate=" & ToDouble(cashbankcurrate.Text) & ", GiroNote='" & Tchar(cashbanknote.Text) & "', upduser='" & Session("UserID") & "', updtime=current_timestamp, GiroPaymentStatus='" & Trim(lblPOST.Text) & "', GiroFlag='" & payflag.SelectedValue() & "', InAcctgOid=0 WHERE GiroPaymentMstOid=" & Session("oid")
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
                'delete tabel detail
                sSql = "DELETE FROM QL_GiroPaymentDtl WHERE GiroPaymentMstOid=" & Session("oid") & " AND cmpcode='" & CompnyCode & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                If Not Session("tbldtl") Is Nothing Then
                    Dim objTable As DataTable
                    Dim objRow() As DataRow
                    objTable = Session("tbldtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                    Dim sTemp As Integer = GenerateID("QL_GiroPaymentDtl", CompnyCode)
                    For C1 As Int16 = 0 To objRow.Length - 1
                        sSql = "INSERT into QL_GiroPaymentDtl(cmpcode,GiroPaymentDtlOid,GiroPaymentMstOid,CustOid,NoGiro,DueDate,GiroAmt,paymentoid,GiroNote,GiroAcctgOid,paymentref_table,branch_id) VALUES ('" & CompnyCode & "'," & sTemp + C1 & "," & Session("oid") & "," & objRow(C1)("custoid") & ",'" & objRow(C1)("payrefno").ToString & "','" & objRow(C1)("payduedate").ToString & "'," & ToDouble(objRow(C1)("payamt")) & "," & objRow(C1)("paymentoid").ToString & ",'" & Tchar(objRow(C1)("paynote").ToString) & "'," & objRow(C1)("payacctgoid").ToString & ",'" & objRow(C1)("paymentref_table").ToString & "','" & Session("branch_id") & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Next

                    'update GiroPaymentDtl
                    sSql = "UPDATE QL_mstoid SET lastoid=" & sTemp + objRow.Length - 1 & " WHERE tablename='QL_GiroPaymentDtl' and cmpcode = '" & CompnyCode & "' "
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
            End If
            If lblPOST.Text = "POST" Then
                Session("vJurnalDtl") = ClassFunction.GenerateID("ql_trngldtl", CompnyCode)
                Session("vJurnalMst") = ClassFunction.GenerateID("ql_trnglmst", CompnyCode)

                '======POSTING TO GL 
                'GL MST
                sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & Session("vJurnalMst") & ",'" & CDate(toDate(paymentdate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(paymentdate.Text))) & "','Giro Payment In=" & cashbankno.Text & "','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("branch_id") & "')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                'GL DTL
                'CashBank
                Session("vJurnalDtl") = Session("vJurnalDtl") + 1
                If ddlGiroType.SelectedValue() = "IN" Then
                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glpostdate,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & Session("vJurnalDtl") & ",1," & Session("vJurnalMst") & "," & cashbankacctgoid.SelectedValue & ",'D'," & ToDouble(totalreceipt.Text) * ToDouble(cashbankcurrate.Text) & ",'" & Session("vNoCashBank") & "','" & Tchar(cashbanknote.Text) & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("branch_id") & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                ElseIf ddlGiroType.SelectedValue() = "OUT" Then
                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glpostdate,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & Session("vJurnalDtl") & ",1," & Session("vJurnalMst") & "," & cashbankacctgoid.SelectedValue & ",'C'," & ToDouble(totalreceipt.Text) * ToDouble(cashbankcurrate.Text) & ",'" & Session("vNoCashBank") & "','" & Tchar(cashbanknote.Text) & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("branch_id") & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                Dim objTableAR As DataTable : objTableAR = Session("tbldtl")
                For C2 As Integer = 0 To objTableAR.Rows.Count - 1
                    Session("vJurnalDtl") = Session("vJurnalDtl") + 1
                    If ddlGiroType.SelectedValue() = "IN" Then
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glpostdate,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & (Session("vJurnalDtl")) & "," & 2 + C2 & "," & Session("vJurnalMst") & "," & objTableAR.Rows(C2).Item("payacctgoid") & ",'C'," & ToDouble(objTableAR.Rows(C2).Item("payamt")) * ToDouble(cashbankcurrate.Text) & ",'" & Session("vNoCashBank") & "','" & Tchar(cashbanknote.Text) & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("branch_id") & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    ElseIf ddlGiroType.SelectedValue() = "OUT" Then
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glpostdate,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & (Session("vJurnalDtl")) & "," & 2 + C2 & "," & Session("vJurnalMst") & "," & objTableAR.Rows(C2).Item("payacctgoid") & ",'D'," & ToDouble(objTableAR.Rows(C2).Item("payamt")) * ToDouble(cashbankcurrate.Text) & ",'" & Session("vNoCashBank") & "','" & Tchar(cashbanknote.Text) & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("branch_id") & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                Next

                sSql = "update QL_mstoid set lastoid=" & Session("vJurnalDtl") + objTableAR.Rows.Count & " where tablename ='QL_trngldtl' and cmpcode = '" & CompnyCode & "' "
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                ' Update lastoid GLMST
                sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalMst") & " WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnglmst'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            objCmd.Connection.Close()
        Catch ex As Exception
            btnshowCOA.Visible = False
            objTrans.Rollback() : objCmd.Connection.Close() : lblPOST.Text = ""
            showMessage(ex.ToString, CompanyName & " - ERROR", 1, "modalMsgBox") : objTrans.Dispose() : Exit Sub
        End Try
        Session("tbldtl") = Nothing : Session("oid") = Nothing
        If lblPOST.Text = "POST" Then
            showMessage("Data telah diposting !", CompanyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            showMessage("Data telah disimpan !", CompanyName & " - INFORMASI", 3, "modalMsgBoxOK")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("GiroPaymentout.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        lblPOST.Text = "POST"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            Dim strSQL As String
            Dim objConn As New SqlClient.SqlConnection(ConnStr)
            Dim objTrans As SqlClient.SqlTransaction
            Dim objCmd As New SqlClient.SqlCommand
            objConn.Open()
            objTrans = objConn.BeginTransaction()
            objCmd.Connection = objConn
            objCmd.Transaction = objTrans

            Try
                strSQL = "DELETE FROM QL_GiroPaymentMst WHERE QL_GiroPaymentMst.cmpcode='" & CompnyCode & "' AND QL_GiroPaymentMst.GiroPaymentMstOid=" & Session("oid")
                objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                strSQL = "DELETE FROM QL_GiroPaymentDtl WHERE GiroPaymentMstOid=" & Session("oid") & " AND cmpcode='" & CompnyCode & "'"
                objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                strSQL = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & CompnyCode & "' and cashbankoid = " & Trim(cashbankoid.Text)
                objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                strSQL = "DELETE FROM QL_cashbankgl WHERE cmpcode='" & CompnyCode & "' and cashbankoid = " & Trim(cashbankoid.Text)
                objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()
                objTrans.Commit() : objCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : objCmd.Connection.Close()
                showMessage(ex.Message, CompanyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
            End Try
            Session("oid") = Nothing : Session("tbldtl") = Nothing
            showMessage("Data telah dihapus !", CompanyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            showMessage("Tidak dapat menghapus karena tidak ada data yang di pilih", CompanyName & " - WARNING", 2, "modalMsgBoxWarn")
        End If
    End Sub

    Protected Sub imbPrintNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrintNota.Click

    End Sub

    Protected Sub gvReceiptDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReceiptDtl.SelectedIndexChanged
        payseq.Text = gvReceiptDtl.SelectedDataKey.Item("payseq").ToString
        paymentoid.Text = gvReceiptDtl.SelectedDataKey.Item("paymentoid").ToString
        payrefno.Text = gvReceiptDtl.SelectedDataKey.Item("payrefno").ToString
        paymentref_table.Text = gvReceiptDtl.SelectedDataKey.Item("paymentref_table").ToString
        custname.Text = gvReceiptDtl.SelectedDataKey.Item("custname").ToString
        payduedate.Text = toDate(gvReceiptDtl.SelectedDataKey.Item("payduedate").ToString)
        payamt.Text = ToMaskEdit(ToDouble(gvReceiptDtl.SelectedDataKey.Item("payamt").ToString), 4)
        paynote.Text = gvReceiptDtl.SelectedDataKey.Item("paynote").ToString
        giroacctgoid.Text = gvReceiptDtl.SelectedDataKey.Item("payacctgoid").ToString
        document.Text = gvReceiptDtl.SelectedDataKey.Item("document").ToString
        suppoid.Text = gvReceiptDtl.SelectedDataKey.Item("custoid").ToString()
        bank.Text = gvReceiptDtl.SelectedDataKey.Item("bank").ToString()
        i_u2.Text = "Update Data" : MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub imbClearReceipt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAR() : payseq.Text = Session("ItemLinePayment")
    End Sub

    Protected Sub cashbankcurroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillCurrencyRate(cashbankcurroid.SelectedValue)
        Session("tbldtl") = Nothing : gvReceiptDtl.DataSource = Nothing : gvReceiptDtl.DataBind()
    End Sub

    Protected Sub imbClearInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlGiro()
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initddlcasbank(payflag.SelectedValue)
    End Sub

    Protected Sub imbPrintFormList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            PrintNota(sender.CommandArgument, "")
        Catch ex As Exception
            showMessage(ex.ToString, CompanyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
        End Try
        Response.Redirect("~\Accounting\GiroPaymentOut.aspx?awal=true")
    End Sub

    Protected Sub gvReceiptDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        If lblPOST.Text = "" Then
            Dim idx As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("tbldtl")
            objTable.Rows.RemoveAt(idx)

            'resequence Detial 
            For C2 As Int16 = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C2)
                dr.BeginEdit() : dr("payseq") = C2 + 1 : dr.EndEdit()
            Next

            Session("tbldtl") = objTable
            gvReceiptDtl.DataSource = Session("tbldtl")
            gvReceiptDtl.DataBind()
            calcTotalInGridDtl()
            payseq.Text = objTable.Rows.Count + 1
            Session("ItemLinePayment") = payseq.Text
            ClearDtlGiro()
        Else
            e.Cancel = True
        End If
    End Sub

    Protected Sub gvReceiptDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = Format(CDate(e.Row.Cells(6).Text), "dd/MM/yyyy")
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub gvGiro_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub GVmst_PageIndexChanging1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmst.PageIndexChanging
        GVmst.PageIndex = e.NewPageIndex
        binddata()
        Session("SearchARR") = strSearch
    End Sub

    Protected Sub gvmst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            If e.Row.Cells(5).Text = "&nbsp;" Then
                e.Row.Cells(5).Text = "In Process"
            End If
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA(cashbankno.Text, Session("branch_id"), gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
        End If
    End Sub

    Protected Sub gvmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVmst.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\GiroPaymentOut.aspx?awal=true")
        End If
    End Sub
#End Region

End Class