'Prgmr:Shutup_M | LastUpdt:28.02.2016
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports Koneksi
Imports ClassFunctionAccounting
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Accounting_trncbreceipt
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim cKon As New Koneksi
    'Dim cfunction As New ClassFunction
    Public sql_temp As String
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate
    Public folderReport As String = "~/Report/"
    Dim tempPayFlag As String = ""
#End Region

#Region "Prosedure"

    Private Sub bindDataPerson()
        Dim picc As String = GetStrData("select p.personname from ql_mstperson p inner join QL_MSTPROF pr on pr.USERNAME = p.PERSONNAME and pr.STATUSPROF = 'active' and pr.userid = '" & Session("UserID") & "'")
        If pic.Text <> "" Then
            sSql = "select personoid,personnip,personname from ql_mstperson where personname like '%" & Tchar(pic.Text) & "%'"
        Else
            sSql = "select personoid,personnip,personname from ql_mstperson where personname like '%" & picc & "%'"
        End If
        FillGV(gvPerson, sSql, "ql_mstperson")
        gvPerson.Visible = True
    End Sub

    Private Sub bindAccounting(ByVal sFind As String)
        FillGVAcctg(GvAccCost, "VAR_RECEIPT1", deptoid.SelectedValue, "AND " & ddlFindAcctg.SelectedValue & " LIKE '%" & Tchar(txtFind.Text) & "%'")
        btnHidden.Visible = True : Panel1.Visible = True
        ModalPopupExtender1.Show()
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub binddata()
        Try
            If IsDate(toDate(txtPeriode1.Text)) = False Or IsDate(toDate(txtPeriode1.Text)) = False Then
                showMessage("Periode awal salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If IsDate(toDate(txtPeriode2.Text)) = False Or IsDate(toDate(txtPeriode2.Text)) = False Then
                showMessage("Periode akhir salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            'Dim mySqlConn As New SqlConnection(ConnStr)
            sSql = "Select * From (SELECT cb.branch_code, 0 AS selected, cashbankno, gendesc, cashbankoid, cashbankdate, (SELECT SUM(cashbankglamt) FROM QL_cashbankgl p WHERE p.cashbankoid=cb.cashbankoid AND p.cmpcode=cb.cmpcode and cb.branch_code = p.branch_code) cashbankglamt, cashbankstatus, cashbanknote, cashbankacctgoid, g.currencycode rate, cb.cmpcode FROM QL_trncashbankmst cb inner join ql_mstcurr g on g.currencyoid=cb.cashbankcurroid inner join ql_mstgen gen on gen.gengroup='cabang' And gen.gencode = cb.branch_code WHERE cb.cashbankno like '%" & TcharNoTrim(no.Text) & "%' and cb.cashbankacctgoid in (select ao.acctgoid from ql_mstacctg ao) and cb.cmpcode = '" & cmpcode & "' AND cashbankgroup='RECEIPT' UNION ALL SELECT cb.branch_code, 0 AS selected, cashbankno, gendesc, cashbankoid, cashbankdate, (SELECT SUM(cashbankglamt) FROM QL_cashbankgl p WHERE p.cashbankoid=cb.cashbankoid AND p.cmpcode=cb.cmpcode and cb.branch_code = p.branch_code) cashbankglamt, cashbankstatus, cashbanknote, cashbankacctgoid, g.currencycode rate, cb.cmpcode FROM QL_trncbclosedmst cb inner join ql_mstcurr g on g.currencyoid=cb.cashbankcurroid inner join ql_mstgen gen on gen.gengroup='cabang' And gen.gencode = cb.branch_code WHERE cb.cashbankno like '%" & TcharNoTrim(no.Text) & "%' and cb.cashbankacctgoid in (select ao.acctgoid from ql_mstacctg ao) and cb.cmpcode = '" & cmpcode & "' AND cashbankgroup='RECEIPT') cb Where cb.cmpcode = '" & cmpcode & "'"

            If cbPeriode.Checked Then
                If txtPeriode1.Text = "" And txtPeriode2.Text = "" Then
                    showMessage("Tolong isi Periode!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
                If CDate(toDate(txtPeriode1.Text)) <= CDate(toDate(txtPeriode2.Text)) Then
                    sSql &= " AND cashbankdate>='" & CDate(toDate(txtPeriode1.Text)) & "' and cashbankdate<dateadd(day,1,'" & CDate(toDate(txtPeriode2.Text)) & "')"
                Else
                    showMessage("Tanggal akhir harus sama atau lebih dari tanggal mulai !", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
                'If txtPeriode1.Text <> "" And txtPeriode2.Text <> "" Then

                'ElseIf txtPeriode1.Text <> "" And txtPeriode2.Text = "" Then
                '    sSql &= " AND cashbankdate >='" & CDate(toDate(txtPeriode1.Text)) & "'"
                'ElseIf txtPeriode1.Text = "" And txtPeriode2.Text <> "" Then
                '    sSql &= " AND cashbankdate <='" & CDate(toDate(txtPeriode2.Text)) & "'"
                'End If
            End If

            If statuse.SelectedValue <> "ALL" Then
                sSql &= " AND cashbankstatus='" & statuse.SelectedValue & "'"
            End If

            If statuse.SelectedValue <> "ALL" Then
                If statuse.SelectedValue.ToUpper = "IN PROCESS" Then
                    sSql &= " AND cashbankstatus=''"
                Else
                    sSql &= " AND cashbankstatus='" & statuse.SelectedValue.Trim & "'"
                End If
            End If

            If dd_cabang.SelectedValue <> "ALL" Then
                sSql &= " AND cb.branch_code = '" & dd_cabang.SelectedValue & "'"
            End If

            sSql &= " ORDER BY cashbankdate desc"
            Dim objTable As DataTable = cKon.ambiltabel(sSql, "Receipt")
            Session("tbldata") = objTable : gvmstcost.DataSource = objTable
            gvmstcost.DataBind()
            UnabledCheckBox() : calcTotalInGrid()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Public Sub bindLastSearched()
        If (Not Session("SearchExpense") Is Nothing) Or (Not Session("SearchExpense") = "") Then
            Dim mySqlConn As New SqlConnection(ConnStr)
            Dim sqlSelect As String = Session("SearchExpense")

            Dim objTable As DataTable = cKoneksi.ambiltabel(sqlSelect, "cost")
            Session("tbldata") = objTable
            gvmstcost.DataSource = objTable
            gvmstcost.DataBind()
            UnabledCheckBox()
            mySqlConn.Close()
            calcTotalInGrid()
        End If

    End Sub

    Private Sub InitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dd_cabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_cabang, sSql)
            Else
                FillDDL(dd_cabang, sSql)
                dd_cabang.Items.Add(New ListItem("ALL", "ALL"))
                dd_cabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dd_cabang, sSql)
            dd_cabang.Items.Add(New ListItem("ALL", "ALL"))
            dd_cabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(deptoid, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(deptoid, sSql)
            Else
                FillDDL(deptoid, sSql) 
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(deptoid, sSql) 
        End If
    End Sub

    Public Sub initDDLAll(ByVal cashbank As String)
        sSql = "select currencyoid,currencycode from QL_mstcurr order by currencyoid"
        FillDDL(currate, sSql)
        FillCurrencyRate(currate.SelectedValue)

        sSql = "select genoid,gendesc from ql_mstgen where gengroup='BANK NAME'"
        If cKoneksi.ambilscalar(sSql) > 0 Then
            FillDDLGen("BANK NAME", bank)
        Else
            showMessage("Isi/Buat BANK NAME di master general!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet
        If cashbank = "" Then
            ' mengisi data pada DDL cmpcode
            sSql = "SELECT gendesc,genother1 FROM QL_mstgen WHERE gengroup='CABANG' AND cmpcode='" & cmpcode & "'"

            mySqlDA = New SqlDataAdapter(sSql, ConnStr)
            objDs = New DataSet
            mySqlDA.Fill(objDs, "dataA")
        Else
            cashbankacctgoid.Items.Clear()
            ' untuk mengisi data pada DDL CashBank
            If Trim(cashbank) = "CASH" Then
                sSql = "SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '111%' "
                mySqlDA = New SqlDataAdapter(sSql, ConnStr)
                objDs = New DataSet
                mySqlDA.Fill(objDs, "dataA")

                If objDs.Tables("dataA").Rows.Count > 0 Then
                    For C1 As Int16 = 0 To objDs.Tables("dataA").Rows.Count - 1
                        cashbankacctgoid.Items.Add(Trim(objDs.Tables("dataA").Rows(C1).Item(1).ToString & "-" & objDs.Tables("dataA").Rows(C1).Item(2).ToString))
                        cashbankacctgoid.Items.Item(C1).Value = Trim(objDs.Tables("dataA").Rows(C1).Item(0).ToString)
                    Next
                Else
                    showMessage("Isi/Buat account CASH di master accounting !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                End If
            ElseIf Trim(cashbank) = "NON CASH" Then
                sSql = "SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '112%'"
                mySqlDA = New SqlDataAdapter(sSql, ConnStr)
                objDs = New DataSet
                mySqlDA.Fill(objDs, "dataA")

                If objDs.Tables("dataA").Rows.Count > 0 Then
                    For C1 As Int16 = 0 To objDs.Tables("dataA").Rows.Count - 1
                        cashbankacctgoid.Items.Add(Trim(objDs.Tables("dataA").Rows(C1).Item(1).ToString & "-" & objDs.Tables("dataA").Rows(C1).Item(2).ToString))
                        cashbankacctgoid.Items.Item(C1).Value = Trim(objDs.Tables("dataA").Rows(C1).Item(0).ToString)
                    Next
                Else
                    showMessage("Isi/Buat account Bank di master accounting!!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                End If
            End If
        End If
    End Sub

    Private Sub initPayactg()
        Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet

        sSql = "SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode like '1103%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"

        mySqlDA = New SqlDataAdapter(sSql, ConnStr)
        objDs = New DataSet
        mySqlDA.Fill(objDs, "dataB")

        If objDs.Tables("dataB").Rows.Count <= 0 Then
            showMessage("Isi/Buat account pembayaran di master accounting !", "WARNING", 2, "modalMsgBoxWarn")
        End If
    End Sub

    Private Sub initddlcasbank(ByVal cashbank As String)
        Dim scbtype As String = "" : Dim sVar As String = ""
        If payflag.SelectedValue = "CASH" Then
            scbtype = "BKM" : sVar = "VAR_CASH"
        ElseIf payflag.SelectedValue = "NON CASH" Then
            scbtype = "BBM" : sVar = "VAR_BANK"
        End If

        cashbankacctgoid.Items.Clear()
        lblBankname.Visible = False : bank.Visible = False
        sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & deptoid.SelectedValue & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "?" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & cmpcode & "'"
            sCode = GetStrData(sSql)
        End If

        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillDDL(cashbankacctgoid, sSql)
        End If

        If sCode = "" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='All'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND (acctgdesc LIKE '%" & Tchar(txtCoa.Text.Trim) & "%' OR acctgcode LIKE '%" & Tchar(txtCoa.Text.Trim) & "%') AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(GvCoa, sSql, "QL_mstacctg")
        End If
        GvCoa.Visible = True
    End Sub

    Public Sub fillTextBox(ByVal vpayid As Integer, ByVal status As String, ByVal branch_code As String)
        'tbladd.Visible = True
        sSql = "SELECT cb.cashbankoid,cb.upduser,cb.updtime, case when cashbanktype='BKM' then 'CASH' else 'NON CASH' end payflag, cashbankacctgoid,cashbankstatus,cb.cmpcode,(Select SUM(p.cashbankglamt) From QL_cashbankgl p Where p.cashbankoid=cb.cashbankoid AND p.branch_code = cb.branch_code) cashbankglamt,cb.cashbanknote,convert(char(10),cashbankdate, 103)cashbankdate,convert(char(10),cashbankdate, 103) duedate,cb.cashbankrefno refno,cb.cashbankno,cb.cashbankcurroid,cb.cashbankcurrate, '' pic ,cb.deptoid,cb.bankoid,0 personoid,cb.branch_code,cb.createtime,cb.upduser,cb.updtime FROM QL_trncashbankmst cb Where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid=" & vpayid & " AND cb.cashbankstatus='" & status & "' AND cb.branch_code = '" & branch_code & "' UNION ALL SELECT cb.cashbankoid,cb.upduser,cb.updtime, case when cashbanktype='BKM' then 'CASH' else 'NON CASH' end payflag, cashbankacctgoid,cashbankstatus,cb.cmpcode,(Select SUM(p.cashbankglamt) From QL_cashbankgl p Where p.cashbankoid=cb.cashbankoid AND cb.branch_code = p.branch_code) cashbankglamt,cb.cashbanknote,convert(char(10),cashbankdate, 103)cashbankdate,convert(char(10),cashbankdate, 103) duedate,cb.cashbankrefno refno,cb.cashbankno,cb.cashbankcurroid,cb.cashbankcurrate, '' pic ,cb.deptoid,cb.bankoid,0 personoid,cb.branch_code,cb.createtime,cb.upduser,cb.updtime FROM QL_trncbclosedmst cb Where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid=" & vpayid & " AND cb.cashbankstatus='" & status & "' AND cb.branch_code = '" & branch_code & "'"

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                Session("vNoCashBank") = Trim(xreader("cashbankno"))
                cashbankoid.Text = xreader("cashbankoid")
                payflag.SelectedValue = Trim(xreader("payflag"))
                HiddenField1.Value = Trim(xreader.GetValue(3))

                If payflag.SelectedValue = "CASH" Then
                    payrefno.Text = ""
                    payduedate.Text = ""
                Else
                    If IsDBNull(xreader("refno")) Then payrefno.Text = "" Else payrefno.Text = Trim(xreader("refno"))
                    If IsDBNull(xreader("duedate")) Then payduedate.Text = "" Else payduedate.Text = xreader("duedate")
                End If

                CashBankNo.Text = xreader("cashbankno")
                cashbankdate.Text = xreader("cashbankdate")
                cashbanknote.Text = xreader("cashbanknote")
                currate.SelectedValue = (xreader.Item("cashbankcurroid"))
                deptoid.SelectedValue = Trim(xreader("branch_code").ToString)
                currencyrate.Text = Trim(xreader.Item("cashbankcurrate"))
                pic.Text = Trim(xreader.Item("pic"))

                initddlcasbank(payflag.SelectedValue)
                If payflag.SelectedValue = "CASH" Then
                    setcontrolCash(False)
                    payrefno.Text = ""
                    payduedate.Text = ""
                Else
                    setcontrolCash(True)
                End If

                'cashbankacctgoid.SelectedValue = xreader("cashbankacctgoid")
                lblCoa.Text = xreader("cashbankacctgoid")
                txtCoa.Text = GetStrData("SELECT a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND acctgoid=" & xreader("cashbankacctgoid") & "")

                HiddenField2.Value = Trim(xreader.GetValue(4))
                upduser.Text = Trim(xreader("upduser"))
                updtime.Text = Trim(xreader("updtime"))
                If Trim(xreader.Item("cashbankstatus")) = "POST" Or Trim(xreader.Item("cashbankstatus")) = "CLOSED" Then
                    btnPosting2.Visible = False : btnsave.Visible = False
                    btnDelete.Visible = False : btnAddToList.Visible = False
                    lblPOST.Text = Trim(xreader.Item("cashbankstatus"))
                    btnClear.Visible = False
                Else
                    btnPosting2.Visible = True : btnsave.Visible = True
                    btnDelete.Visible = True : btnAddToList.Visible = True
                    lblPOST.Text = ""
                End If

                payflag.Enabled = False
                bank.SelectedValue = (xreader.Item("bankoid"))
                deptoid.SelectedValue = (xreader.Item("branch_code"))
                picoid.Text = xreader.Item("personoid")
                createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss tt")
            End While
        End If

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If

        sSql = "SELECT gl.cashbankgloid seq,gl.acctgoid,gl.cashbankglamt,gl.cashbankglnote,acc.acctgcode,acc.acctgdesc FROM QL_cashbankgl gl INNER JOIN QL_trncashbankmst cb ON gl.cmpcode=cb.cmpcode and cb.branch_code = gl.branch_code AND gl.cashbankoid=cb.cashbankoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=gl.acctgoid WHERE cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid='" & vpayid & "' AND cb.cashbankstatus='" & status & "' AND cb.branch_code = '" & deptoid.SelectedValue & "' UNION ALL SELECT gl.cashbankgloid seq,gl.acctgoid,gl.cashbankglamt,gl.cashbankglnote,acc.acctgcode,acc.acctgdesc FROM QL_trncbcloseddtl gl INNER JOIN QL_trncbclosedmst cb ON gl.cmpcode=cb.cmpcode and cb.branch_code = gl.branch_code AND gl.cashbankoid=cb.cashbankoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=gl.acctgoid WHERE cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid='" & vpayid & "' AND cb.cashbankstatus='" & status & "' AND cb.branch_code = '" & deptoid.SelectedValue & "'"

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet

        mySqlDA.Fill(objDs, "dataCost")
        GVDtlCost.Visible = True

        Dim dv As DataView = objDs.Tables("dataCost").DefaultView
        dv.Sort = "seq desc"
        GVDtlCost.DataSource = objDs.Tables("dataCost")
        GVDtlCost.DataBind()
        Session("dtlTable") = objDs.Tables("dataCost")

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If

        calcTotalInGridDtl() : generateCostID()
        TabContainer1.ActiveTabIndex = 1
        imbCBDate.Visible = False : cashbankdate.Enabled = False
        cashbankdate.CssClass = "inpTextDisabled"
    End Sub

    Private Sub generateCashBankID()
        cashbankoid.Text = GenerateID("QL_trncashbankmst", cmpcode)
        CashBankNo.Text = GenerateID("QL_trncashbankmst", cmpcode)
    End Sub

    Private Sub generateCostID()
        cashbankgloid.Text = GenerateID("QL_cashbankgl", cmpcode)
    End Sub

    Private Sub generateCashBankNo(ByVal cashbanktype As String)
        Dim sCBType As String = ""
        Dim iCurID As Integer = 0

        If cashbanktype = "CASH" Then sCBType = "BKM" Else sCBType = "BBM"
        sSql = "SELECT MAX(ABS(Right(RTRIM(cashbankno),LEN(cashbankno)-3))) cashbankno FROM QL_trncashbankmst WHERE LEFT(cashbankno,3)='" & sCBType & "'"
        xCmd.CommandText = sSql

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        If Not IsDBNull(xCmd.ExecuteScalar) Then
            iCurID = xCmd.ExecuteScalar + 1
        Else
            iCurID = 1
        End If
        conn.Close()

        Session("vNoCashBank") = GenNumberString(sCBType, "", iCurID, DefCounter)

    End Sub

    Protected Sub calcTotalInGrid()
        Dim iCountrow As Integer = gvmstcost.Rows.Count
        Dim iGtotal As Double = 0

        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(gvmstcost.Rows(i).Cells(3).Text)
        Next
        lblgrandtotal.Text = ToMaskEdit(ToDouble(iGtotal), 2)
    End Sub

    Protected Sub calcTotalInGridDtl()
        Dim iCountrow As Integer = GVDtlCost.Rows.Count
        Dim iGtotal As Double = 0
        Dim dt_temp As New DataTable
        dt_temp = Session("dtlTable")
        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(GVDtlCost.Rows(i).Cells(4).Text)

            dt_temp.Rows(i).BeginEdit()
            dt_temp.Rows(i).Item(0) = i + 1
            dt_temp.Rows(i).EndEdit()
        Next
        Session("dtlTable") = dt_temp
        GVDtlCost.DataSource = dt_temp
        GVDtlCost.DataBind()

        Dim sCurr() As String = currate.SelectedItem.Text.Split("-")
        lblTotExpense.Text = "Total Receipt : " & sCurr(0)
        amtcost.Text = ToMaskEdit(ToDouble(iGtotal), 3)
    End Sub

    Public Sub CheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("cashbankstatus").ToString) <> "POST" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UnabledCheckBox()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (gvmstcost.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("cashbankstatus").ToString) = "POST" Or Trim(objRow(C1)("cashbankstatus").ToString) = "DELETE" Then
                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("cashbankstatus").ToString) <> "POST" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ClearDtlAP()
        acctgoid.Text = "" : lblAcctgOid.Text = ""
        cashbankglnote.Text = "" : cashbankglamt.Text = "0"
        no.Text = "" : AddToList.Text = "Add To List"
        i_u2.Text = "New Detail" : cashbankglseq.Text = "1"
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select rate2idrvalue from ql_mstrate2 where rate2year =YEAR(getdate()) and rate2month = MONTH(getdate()) and currencyoid = " & iOid & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyrate.Text = ToMaskEdit(xCmd.ExecuteScalar, 2)
        conn.Close()
        'If currate.Items(currate.SelectedIndex).Text.Trim.ToUpper = "IDR" Then
        '    currencyrate.Text = "1.00"
        'End If
    End Sub 

    Private Sub BindAkun()
        Dim sVar As String = "" : Dim dVal As Boolean = False
        If payflag.SelectedValue = "CASH" Then
            sVar = "VAR_CASH"
            dVal = True
        ElseIf payflag.SelectedValue = "NON CASH" Then
            sVar = "VAR_BANK"
            dVal = True
        End If

        sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & deptoid.SelectedValue & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "?" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & cmpcode & "'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND (acctgdesc LIKE '%" & Tchar(txtCoa.Text.Trim) & "%' OR acctgcode LIKE '%" & Tchar(txtCoa.Text.Trim) & "%') AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(GvCoa, sSql, "QL_mstacctg")
        End If
    End Sub

    Private Sub BindDataClosed()
        sSql = "Select cmpcode,cb.cashbankoid,cb.cashbankno,cb.cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, PIC_REFNAME, deptoid, createuser,createtime, bankoid,branch_code from ql_trncashbankmst cb Where cashbankgroup='RECEIPT' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Order by cashbankoid ASC"
        Dim cbMst As DataTable = cKoneksi.ambiltabel(sSql, "Ql_CbClosed")
        Session("ClosedMst") = cbMst

        sSql = "Select cb.cmpcode,cashbankgloid,cb.cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,cb.upduser,cb.updtime,duedate,refno,cashbankglstatus,cb.branch_code from QL_cashbankgl gl Inner Join QL_trncashbankmst cb ON cb.cashbankoid=gl.cashbankoid AND cb.branch_code = gl.branch_code Where cb.cashbankgroup='RECEIPT' AND cb.cashbankstatus='' AND cb.cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Order by cb.cashbankoid ASC"
        Dim cbgl As DataTable = cKoneksi.ambiltabel2(sSql, "Ql_GlClosed")
        Session("ClosedDtl") = cbgl

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'insert mst
            For c1 As Int32 = 0 To cbMst.Rows.Count - 1
                sSql = "INSERT into QL_trncbclosedmst (cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, PIC_REFNAME, deptoid, createuser,createtime, bankoid,branch_code,flagexpedisi) VALUES " & _
                "('" & cmpcode & "'," & Integer.Parse(cbMst.Rows(c1)("cashbankoid")) & ",'" & cbMst.Rows(c1)("cashbankno") & "','CLOSED','" & cbMst.Rows(c1)("cashbanktype") & "','RECEIPT'," & Integer.Parse(cbMst.Rows(c1)("cashbankacctgoid")) & ",'" & cbMst.Rows(c1)("cashbankdate") & "','" & Tchar(cbMst.Rows(c1)("cashbanknote")) & "','" & cbMst.Rows(c1)("upduser") & "',current_timestamp," & cbMst.Rows(c1)("cashbankcurroid") & "," & ToDouble(cbMst.Rows(c1)("cashbankcurrate")) & ", '" & Tchar(cbMst.Rows(c1)("pic")) & "', 'QL_mstperson', '" & Integer.Parse(cbMst.Rows(c1)("deptoid")) & "', '" & cbMst.Rows(c1)("createuser") & "','" & cbMst.Rows(c1)("createtime") & "'," & Integer.Parse(cbMst.Rows(c1)("bankoid")) & ",'" & cbMst.Rows(c1)("branch_code") & "','')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next
            'insert dtl
            For c1 As Int32 = 0 To cbgl.Rows.Count - 1
                sSql = "INSERT into QL_trncbcloseddtl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,upduser,updtime,duedate,refno,cashbankglstatus,branch_code) VALUES " & _
                "('" & cmpcode & "'," & Integer.Parse(cbgl.Rows(c1)("cashbankgloid")) & "," & Integer.Parse(cbgl.Rows(c1)("cashbankoid")) & "," & Integer.Parse(cbgl.Rows(c1)("acctgoid")) & "," & cbgl.Rows(c1)("cashbankglamt") & "," & cbgl.Rows(c1)("cashbankglamtidr") & "," & cbgl.Rows(c1)("cashbankglamtusd") & ",'" & Tchar(cbgl.Rows(c1)("cashbankglnote")) & "','" & Tchar(cbgl.Rows(c1)("upduser")) & "','" & cbgl.Rows(c1)("updtime") & "','" & cbgl.Rows(c1)("duedate") & "','" & Tchar(cbgl.Rows(c1)("refno")) & "','" & Tchar(cbgl.Rows(c1)("cashbankglstatus")) & "','" & Tchar(cbgl.Rows(c1)("branch_code")) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            sSql = "DELETE FROM QL_cashbankgl WHERE cashbankoid IN (Select cashbankoid from ql_trncashbankmst Where cashbankgroup='RECEIPT' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)))"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trncashbankmst WHERE cashbankoid IN (Select cashbankoid from ql_trncashbankmst Where cashbankgroup='RECEIPT' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)))"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & " <br />" & sSql, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal no As String, ByVal cbOid As String)
        'untuk print
        Dim type As String = payflag.SelectedValue
        Dim cbNo As String = "RECEIPT-" & GetStrData(sSql)
        Dim sWhere As String = "AND cashbankgroup='RECEIPT'"
        If type <> "" Then
            report.Load(Server.MapPath(folderReport & "printBKK.rpt"))
            'Report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("cmpCode", cmpcode)
            report.SetParameterValue("cbOid", cbOid)
            report.SetParameterValue("companyname", "MULTI SARANA COMPUTER")

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no)
            report.Close() : report.Dispose() : Session("no") = Nothing
        End If
    End Sub

#End Region

#Region "Function"
    'Public Function GetEnabled() As Boolean
    '    Return Eval("cashbankstatus").ToString.ToUpper = "IN PROCESS"
    'End Function
    Function getCurrency() As Double
        Dim curr As Double
        sSql = "select rate2idrvalue from ql_mstrate2 where rate2year =YEAR(getdate()) and rate2month = MONTH(getdate()) and currencyoid = 2"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curr = ToMaskEdit(xCmd.ExecuteScalar, 2)
        Return curr
        conn.Close()
    End Function

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("seq", Type.GetType("System.Int32"))
        dt.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dt.Columns.Add("cashbankglamt", Type.GetType("System.Decimal"))
        dt.Columns.Add("cashbankglnote", Type.GetType("System.String"))
        dt.Columns.Add("acctgcode", Type.GetType("System.String"))
        dt.Columns.Add("acctgdesc", Type.GetType("System.String"))

        Return dt
    End Function

    Private Function getNewDT(ByVal xdt As DataTable) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("cmpcode")
        dt.Columns.Add("cashbankgloid")
        dt.Columns.Add("cashbankoid")
        dt.Columns.Add("acctgdesc")
        dt.Columns.Add("acctgoid")
        dt.Columns.Add("cashbankglamt")
        dt.Columns.Add("cashbankglnote")
        dt.Columns.Add("upduser")
        dt.Columns.Add("updtime")
        dt.Columns.Add("gendesc")
        dt.Columns.Add("acctgcode")

        For i As Integer = 0 To xdt.Rows.Count - 1
            Dim xrow As DataRow
            xrow = dt.NewRow
            xrow(0) = xdt.Rows(i).Item(0)
            xrow(1) = xdt.Rows(i).Item(1)
            xrow(2) = xdt.Rows(i).Item(2)
            xrow(3) = xdt.Rows(i).Item(3)
            xrow(4) = xdt.Rows(i).Item(4)
            xrow(5) = xdt.Rows(i).Item(5)
            xrow(6) = xdt.Rows(i).Item(6)
            xrow(7) = xdt.Rows(i).Item(7)
            xrow(9) = xdt.Rows(i).Item(9)
            xrow(10) = xdt.Rows(i).Item(10)
            dt.Rows.Add(xrow)
        Next

        Return dt
    End Function

    Private Sub UpdateCheckedPost()
        If Session("tbldata") IsNot Nothing Then
            Dim dt As DataTable = Session("tbldata")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvmstcost.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("tbldata") = dt
        End If
    End Sub

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvmstcost.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        'Proses Auto closed Jika transaksi tanggal kemarin
        sSql = "Select Count(cashbankoid) from ql_trncashbankmst Where cashbankgroup='RECEIPT' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))"
        Dim cOs As Double = GetStrData(sSql)
        If ToDouble(cOs) > 0 Then
            BindDataClosed()
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim lokasioid As Int32 = Session("lokasioid")
            Dim lokasi As String = Session("lokasi")
            Dim lokasino As String = Session("lokasino")
            Dim sqlExpense As String = Session("SearchExpense")
            Dim cbType As String = Session("CashBankType")

            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole : Session("lokasioid") = lokasioid
            Session("lokasi") = lokasi : Session("lokasino") = lokasino
            Session("SearchExpense") = sqlExpense

            Response.Redirect("trncbreceipt.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid") = Request.QueryString("cashbankoid")
        Session("status") = Request.QueryString("cashbankstatus")
        Session("code_branch") = Request.QueryString("branch_code")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting2.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        BtnPostSelected.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST selected data?');")
        Page.Title = CompnyName & " - Receipt"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date

        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        '=============================================================
        If Not IsPostBack Then
            initDDLAll("") : InitCabang() : InitDDLBranch()
            AddToList.Text = "Add To List"
            txtPeriode1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            binddata()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                fillTextBox(Session("oid"), Session("status"), Session("code_branch"))
                lblIO.Text = "U P D A T E"
                gvmstcost.Visible = True
                gvmstcost.DataSource = Session("tbldata")
                gvmstcost.DataBind()
                'UnabledCheckBox()
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                setcontrolCash(False)
                cashbankdate.Text = Format(GetServerTime, "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime, "dd/MM/yyyy")
                generateCostID() : generateCashBankID()
                btnDelete.Visible = False
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                amtcost.Text = "0.00" : lblIO.Text = "N E W"
                TabContainer1.ActiveTabIndex = 0
                initddlcasbank(payflag.SelectedValue)
                currate_SelectedIndexChanged(sender, e)
                'bindDataPerson()
            End If

            Dim dtlTable As DataTable : dtlTable = Session("dtlTable")
            GVDtlCost.DataSource = dtlTable : GVDtlCost.DataBind()
            calcTotalInGridDtl()
        End If

        If lblPOST.Text = "POST" Or lblPOST.Text = "CLOSED" Then
            btnshowCOA.Visible = True : btnPrint.Visible = True
        Else
            btnPrint.Visible = False : btnshowCOA.Visible = False
        End If
    End Sub

    Private Sub setcontrolCash(ByVal status As Boolean)
        Label17.Visible = status : payrefno.Visible = status
        payduedate.Visible = status : Label10.Visible = status
        btnDueDate.Visible = status : Label8.Visible = status
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtCoa.Text = "" : lblCoa.Text = ""
        initddlcasbank(payflag.SelectedValue)
        If Trim(payflag.SelectedValue) = "CASH" Then
            'PanelNonCash.Visible = True
            setcontrolCash(False)
        Else
            'PanelNonCash.Visible = False
            setcontrolCash(True)
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindAccounting("")
    End Sub

    Protected Sub GvAccCost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlFindAcctg.SelectedIndex = 0 : txtFind.Text = ""
        acctgoid.Text = GvAccCost.SelectedDataKey(1).ToString().Trim & " - " & GvAccCost.SelectedDataKey(2S).ToString().Trim
        lblAcctgOid.Text = GvAccCost.SelectedDataKey(0).ToString().Trim
        btnHidden.Visible = False : Panel1.Visible = False
        ModalPopupExtender1.Hide()
        GvAccCost.DataSource = Nothing
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sTempSql As String = " AND " & ddlFindAcctg.SelectedValue & " LIKE '%" & Tchar(txtFind.Text) & "%' "
        bindAccounting(sTempSql)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ddlFindAcctg.SelectedIndex = 0 : txtFind.Text = ""
        bindAccounting("")
    End Sub

    Protected Sub ClosePopUP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnHidden.Visible = False : Panel1.Visible = False : ModalPopupExtender1.Hide()
    End Sub

    Protected Sub GVDtlCost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            cashbankglseq.Text = GVDtlCost.SelectedDataKey.Item("seq").ToString
            If Session("dtlTable") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("dtlTable")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "seq=" & cashbankglseq.Text
                lblAcctgOid.Text = dv.Item(0).Item("acctgoid").ToString
                acctgoid.Text = dv.Item(0).Item("acctgcode").ToString & " - " & dv.Item(0).Item("acctgdesc").ToString
                cashbankglamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("cashbankglamt").ToString), 4)
                cashbankglnote.Text = dv.Item(0).Item("cashbankglnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR !!", 1, "modalMsgBox")
        End Try
        AddToList.Text = "EDIT"
    End Sub

    Protected Sub GVDtlCost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlCost.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
        End If
    End Sub

    Protected Sub GVDtlPayAP_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlCost.RowDeleting
        Dim objTable As DataTable = Session("dtlTable")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        GVDtlCost.DataSource = objTable
        GVDtlCost.DataBind()
        ClearDtlAP()
        calcTotalInGridDtl()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("dtlTable") = Nothing
        gvmstcost.PageIndex = 0 : binddata()
        Session("SearchExpense") = sql_temp
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing
        cbPeriode.Checked = True
        statuse.SelectedIndex = 0
        no.Text = ""
        If Session("branch_id") = "10" Then
            dd_cabang.SelectedValue = "ALL"
        End If
        txtPeriode1.Text = Format(GetServerTime(), "01/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        binddata():Session("SearchExpense") = sql_temp
    End Sub

    Protected Sub gvmstcost_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvmstcost.PageIndexChanging
        UpdateCheckedPost()
        gvmstcost.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UncheckAll()
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP()
        cashbankgloid.Text = Session("vIDCost")
    End Sub 

    Protected Sub btnClear_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP() : GVDtlCost.SelectedIndex = -1
        cashbankgloid.Text = Session("vIDCost")
    End Sub

    Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing : Session("dtlTable") = Nothing
        Response.Redirect("trncbreceipt.aspx?awal=true")
    End Sub

    Protected Sub LinkButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlFindAcctg.SelectedIndex = 0 : txtFind.Text = ""
        btnHidden.Visible = False : Panel1.Visible = False
        ModalPopupExtender1.Hide()
        GvAccCost.DataSource = Nothing
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub BtnPostSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sErrorku As String = ""
        If Not Session("tbldata") Is Nothing Then
            Dim dt As DataTable = Session("tbldata")
            If dt.Rows.Count > 0 Then
                UpdateCheckedPost()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "selected=1"
                If dv.Count = 0 Then
                    sErrorku &= "Please select C/B Receipt first!"
                End If
                dv.RowFilter = ""
                dv.RowFilter = "selected=1 AND cashbankstatus='POST'"
                If dv.Count > 0 Then
                    sErrorku &= "C/B Receipt have been POSTED before!"
                End If
                dv.RowFilter = ""
                If sErrorku <> "" Then
                    showMessage(sErrorku, CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                    dv.RowFilter = ""
                    Exit Sub
                End If

                Dim objTable As DataTable
                Dim objRow() As DataRow
                objTable = Session("tbldata")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                If objRow.Length() > 0 Then
                    Dim strSQL As String = ""
                    Dim objConn As New SqlClient.SqlConnection(ConnStr)
                    Dim objTrans As SqlClient.SqlTransaction
                    Dim objCmd As New SqlClient.SqlCommand
                    Dim dtCBGL As DataTable

                    Dim irowne As Int16 = 0
                    For i As Integer = 0 + (gvmstcost.PageIndex * 10) To objRow.Length() - 1
                        Dim ambilPayFlag As String = GetStrData("select case when cashbanktype='BKM' then 'CASH'  else 'NON CASH' end payflag FROM QL_trncashbankmst cb where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid='" & objTable.Rows(i)("cashbankoid") & "'")
                        If ambilPayFlag = "CASH" Then
                            tempPayFlag = "CASH"
                        ElseIf ambilPayFlag = "NON CASH" Then
                            tempPayFlag = "BANK"
                        End If
                        If irowne < 10 Then
                            If getCheckBoxValue(irowne, 0) = True Then
                                dtCBGL = cKon.ambiltabel("SELECT * FROM ql_cashbankgl WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & objTable.Rows(i)("cashbankoid") & " AND branch_code = '" & deptoid.SelectedValue & "' ", "ql_cashbankgl")
                                objConn.Open()
                                objTrans = objConn.BeginTransaction()
                                objCmd.Connection = objConn
                                objCmd.Transaction = objTrans
                                Try
                                    Dim vIDMst As Integer = ClassFunction.GenerateID("QL_trnglmst", cmpcode)
                                    Dim vIDDtl As Integer = ClassFunction.GenerateID("QL_trngldtl", cmpcode)

                                    If objTable.Rows(i)("cashbankno").ToString.IndexOf("BGK") >= 0 Then
                                        'Cek no giro /cek yg digunakan
                                        sSql = "Select isnull((select count(-1) from ql_trnpayaP where payrefno = '" & Tchar(dtCBGL.Rows(0)("refno").ToString) & "' and  payrefno not like 'DP.AP.%'  and  payrefno not like 'RB/%' and paystatus='POST'  and  RTRIM(payrefno) <>''),0) + isnull((select count(-1) from ql_trndpaP where payrefno = '" & Tchar(dtCBGL.Rows(0)("refno").ToString) & "' and  rtrim(payrefno) <> '' AND TRNDPAPSTATUS='POST') ,0)  + isnull((select count(-1) from QL_cashbankgl where refno = '" & Tchar(dtCBGL.Rows(0)("refno").ToString) & "' and  rtrim(refno) <> ''  AND CASHBANKGLSTATUS='POST') ,0) "
                                        objCmd.CommandText = sSql
                                        If objCmd.ExecuteScalar > 0 Then
                                            showMessage("- Giro (" & dtCBGL.Rows(0)("refno") & ") pada '" & objTable.Rows(i)("cashbankno") & "' sudah pernah dipakai !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                                            objTrans.Rollback() : objCmd.Connection.Close()
                                            Exit Sub
                                        End If
                                    End If

                                    strSQL = "Select cashbankcurrate from QL_trncashbankmst  WHERE cmpcode='" & _
                                        cmpcode & "' and cashbankoid=" & objTable.Rows(i)("cashbankoid")
                                    objCmd.CommandText = strSQL
                                    Dim crate As Decimal = objCmd.ExecuteScalar

                                    strSQL = "UPDATE QL_trncashbankmst SET cashbankstatus='POST' WHERE cmpcode='" & cmpcode & "' and cashbankoid=" & objTable.Rows(i)("cashbankoid")
                                    objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                                    strSQL = "UPDATE QL_cashbankgl SET cashbankglstatus='POST' WHERE cashbankoid=" & objTable.Rows(i)("cashbankoid") & " AND cmpcode='" & cmpcode & "' AND branch_code = '" & deptoid.SelectedValue & "'"
                                    objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                                    '//INSERT CASHBANK INTO GL MST
                                    strSQL = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,type) VALUES ('" & cmpcode & "'," & vIDMst & ",'" & objTable.Rows(i)("cashbankdate") & "','" & GetDateToPeriodAcctg3(objTable.Rows(i)("cashbankdate")) & "','" & tempPayFlag & " - Cost|No=" & objTable.Rows(i)("cashbankno") & "','POST','" & Now & "','" & Session("UserID") & "',current_timestamp,'Receipt')"
                                    objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                                    '//INSERT CASHBANK INTO GL DTL
                                    'VARCASHBANK
                                    strSQL = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr," & "glamt,noref,glnote,upduser,updtime, GLOTHER2) VALUES ('" & cmpcode & "'," & vIDDtl & ",1," & vIDMst & "," & objTable.Rows(i)("cashbankacctgoid") & ",'C'," & objTable.Rows(i)("cashbankglamt") * crate & ",'" & objTable.Rows(i)("cashbankno") & "','" & tempPayFlag & " - Cost|No=" & objTable.Rows(i)("cashbankno") & "','" & Session("UserID") & "',current_timestamp,'" & objTable.Rows(i)("cashbankoid") & "')"
                                    objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                                    For C1 As Integer = 0 To dtCBGL.Rows.Count - 1
                                        '//INSERT CASHBANKGL INTO GL DTL
                                        'VARCOST
                                        strSQL = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr," & "glamt,noref,glnote,upduser,updtime,GLOTHER2) VALUES ('" & cmpcode & "'," & vIDDtl + 1 + C1 & ",2," & vIDMst & "," & dtCBGL.Rows(C1)("acctgoid") & ",'D'," & dtCBGL.Rows(C1)("cashbankglamt") * crate & ",'" & objTable.Rows(i)("cashbankno") & "','" & tempPayFlag & " - Cost|No=" & objTable.Rows(i)("cashbankno") & "','" & Session("UserID") & "',current_timestamp,'" & objTable.Rows(i)("cashbankoid") & "')"
                                        objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()
                                    Next
                                    strSQL = "UPDATE QL_mstoid SET lastoid=" & vIDMst & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trnglmst'"
                                    objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

                                    strSQL = "UPDATE QL_mstoid SET lastoid=" & vIDDtl + dtCBGL.Rows.Count & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trngldtl'"
                                    objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()
                                    objTrans.Commit() : objCmd.Connection.Close()
                                Catch ex As Exception
                                    objTrans.Rollback() : objCmd.Connection.Close()
                                    showMessage(ex.Message, CompnyName & " - ERROR !!", 1, "modalMsgBox")
                                    Exit Sub
                                End Try
                            End If
                        End If
                        irowne += 1
                    Next
                End If
            End If
        End If
        Response.Redirect("trncbreceipt.aspx?type=awal=true")
    End Sub

    Protected Sub gvmstcost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvmstcost.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
            If e.Row.Cells(7).Text = "&nbsp;" Then
                e.Row.Cells(7).Text = "In Process"
            End If
        End If
    End Sub

    Protected Sub currate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles currate.SelectedIndexChanged
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        FillCurrencyRate(currate.SelectedValue)
        If currate.SelectedValue = 2 Then
            lblCurr.Text = "USD"
        Else
            lblCurr.Text = "IDR"
        End If
        initddlcasbank(payflag.SelectedValue)
        If Trim(payflag.SelectedValue) = "CASH" Then
            'PanelNonCash.Visible = True
            setcontrolCash(False)
        Else
            'PanelNonCash.Visible = False
            setcontrolCash(True)
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA(Session("vNoCashBank"), cmpcode, gvakun, cashbankoid.Text)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
            e.Row.Cells(3).Text = NewMaskEdit(e.Row.Cells(3).Text)
        End If
    End Sub

    Protected Sub cashbankglamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankglamt.TextChanged
        If cashbankglamt.Text = "" Then
            cashbankglamt.Text = 0
        End If
        cashbankglamt.Text = ToMaskEdit(cashbankglamt.Text, 4)
    End Sub

    Protected Sub pic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub cashbankdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(toDate(cashbankdate.Text)) = False Or IsDate(toDate(cashbankdate.Text)) = False Then
            showMessage("Tanggal salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If CDate(toDate(cashbankdate.Text)) < GetServerTime() Then
            showMessage("Tanggal Tidak bisa kurang dari tanggal sekarang !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        FillCurrencyRate(currate.SelectedValue)
    End Sub

    Protected Sub deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deptoid.SelectedIndexChanged
        initddlcasbank(payflag.SelectedValue)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & Tchar(CashBankNo.Text) & "'"
        PrintReport(CashBankNo.Text, Integer.Parse(Session("oid")))
    End Sub

    Protected Sub gvmstcost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvmstcost.SelectedIndexChanged
        Response.Redirect("~\Accounting\trncbreceipt.aspx?cashbankoid=" & gvmstcost.SelectedDataKey("cashbankoid").ToString & "&cashbankstatus=" & gvmstcost.SelectedDataKey("cashbankstatus").ToString & "&branch_code=" & gvmstcost.SelectedDataKey("branch_code").ToString & "")
    End Sub

    Protected Sub ibSearchPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchPerson.Click
        'tdPerson.Visible = True
        bindDataPerson()
    End Sub

    Protected Sub gvPerson_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPerson.PageIndexChanging
        gvPerson.PageIndex = e.NewPageIndex
        bindDataPerson()
    End Sub

    Protected Sub gvPerson_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPerson.SelectedIndexChanged
        picoid.Text = gvPerson.SelectedDataKey(0).ToString
        pic.Text = gvPerson.SelectedDataKey(2).ToString
        gvPerson.SelectedIndex = -1 : gvPerson.Visible = False
        'tdPerson.Visible = False
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trncbreceipt.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trncbreceipt.aspx?awal=true")
    End Sub

    Protected Sub ImageButton1_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        pic.Text = "" : picoid.Text = 0
    End Sub

    Protected Sub BtnCariCoa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindAkun()
        GvCoa.Visible = True
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtCoa.Text = ""
        GvCoa.Visible = False
    End Sub

    Protected Sub GvCoa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtCoa.Text = GvCoa.SelectedDataKey(1).ToString().Trim & " - " & GvCoa.SelectedDataKey(2S).ToString().Trim
        lblCoa.Text = GvCoa.SelectedDataKey(0).ToString().Trim
        GvCoa.Visible = False
        initddlcasbank(payflag.SelectedValue)
        'cashbankacctgoid.SelectedValue = lblCoa.Text
    End Sub

    Protected Sub GvCoa_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvCoa.PageIndex = e.NewPageIndex
        BindAkun()
        GvCoa.Visible = True
    End Sub

    Protected Sub btnViewLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewLast.Click
        Session("dtlTable") = Nothing
        If Session("SearchExpense") Is Nothing = False Then
            bindLastSearched()
        End If
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim cashbankno As String = sender.tooltip
        sSql = "SELECT cashbankoid FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & cashbankno & "'"
        PrintReport(cashbankno.ToString, Integer.Parse(GetStrData(sSql))) 
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        Dim sMsg As String = "" : Dim pDate As Date = Format(GetServerTime(), "MM/dd/yyyy")
        Dim mDate As Date = CDate(toDate(cashbankdate.Text))

        'If IsDate(toDate(cashbankdate.Text)) = False Or IsDate(toDate(cashbankdate.Text)) = False Then
        '    sMsg &= "Invalid Date !!<BR>"
        'Else
        '    If CheckYearIsValid(toDate(cashbankdate.Text)) Then
        '        sMsg &= "Year of Date can't Less than 2000 and can't More than 2072 <br />"
        '    End If
        'End If

        'If CDate(toDate(cashbankdate.Text)) < CDate(toDate(CutofDate.Text)) Then
        '    sMsg &= "- Tanggal Receipt Tidak boleh < CutoffDate (" & CutofDate.Text & ") !! <BR> "
        'End If

        'If lblCoa.Text = "0" Then
        '    sMsg &= "Maaf, Cash/Akun Bank belum dipilih..!!<br />"
        'End If

        'If ToDouble(currencyrate.Text) = 0 Then
        '    sMsg &= "Maaf, isi rate di form master rate standart..!!<BR>"
        'End If

        'If bank.Items.Count <= 0 Then
        '    sMsg &= "Maaf, Isi/Buat BANK NAME di master general..!!<BR>"
        'End If

        'If payflag.SelectedValue <> "CASH" Then
        '    If IsDate(toDate(payduedate.Text)) = False Or IsDate(toDate(payduedate.Text)) = False Then
        '        sMsg &= "Invalid DueDate !!<BR>"
        '    Else
        '        If CheckYearIsValid(toDate(payduedate.Text)) Then
        '            sMsg &= "Year of DueDate can't Less than 2000 and can't More than 2072 <br />"
        '        End If
        '    End If
        'End If

        'If payflag.SelectedValue <> "CASH" Then
        '    If IsDate(toDate(payduedate.Text)) = False Or IsDate(toDate(payduedate.Text)) = False Then
        '        sMsg &= "Invalid DueDate !!<BR>"
        '    End If
        'End If

        'If picoid.Text.Trim = "" Then
        '    sMsg &= "Please select Person List first, becouse id PIC still empty !!<BR>"
        'End If

        'Dim dateStat1, dateStat2 As Boolean
        'Try
        '    Dim dt1 As Date = CDate(toDate(cashbankdate.Text)) : dateStat1 = True
        'Catch ex As Exception
        '    dateStat1 = False : sMsg &= "Tgl Cost salah !!<BR>"
        'End Try

        'Try
        '    If payduedate.Visible Then
        '        Dim dt1 As Date = CDate(toDate(payduedate.Text))
        '        dateStat2 = True
        '    Else
        '        dateStat2 = True
        '    End If
        'Catch ex As Exception
        '    dateStat2 = False : sMsg &= "Tgl Jatuh tempo salah!!<BR>"
        'End Try

        'If payduedate.Visible And (dateStat1 And dateStat2) Then
        '    If CDate(toDate(cashbankdate.Text)) > CDate(toDate(payduedate.Text)) Then
        '        sMsg &= "Tgl jatuh tempo harus >= Tgl Transaksi Cost !!<BR>"
        '    End If
        'End If

        '=========================
        'CEK PERIODE AKTIF BULANAN
        '========================= 
        sSql = "select distinct left(isnull(periodacctg,''),4)+'-'+substring(isnull(periodacctg,''),6,2) FROM QL_crdgl Where glflag='OPEN'"
        If GetPeriodAcctgHJ(CDate(toDate(cashbankdate.Text))) < GetStrData(sSql) Then
            sMsg &= "Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !"
        End If

        If Not Session("dtlTable") Is Nothing Then
            Dim objTblDetail As DataTable
            Dim objRowDetail() As DataRow
            objTblDetail = Session("dtlTable")
            objRowDetail = objTblDetail.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowDetail.Length = 0 Then
                sMsg &= "Maaf, Detail data harus di isi!!<BR>"
            End If
        Else
            sMsg &= "Maaf, Detail data harus di isi!!<BR>"
        End If

        If payrefno.Text.Trim = "" And payflag.SelectedValue <> "CASH" Then
            sMsg &= "No cek(Payrefno) harus di isi !!<BR>"
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trncashbankmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!<br />"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT cashbankstatus FROM QL_trncashbankmst WHERE cashbankoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"

            Dim srest As String = GetStrData(sSql)
            If srest.ToLower = "post" Then
                sMsg &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            lblPOST.Text = ""
            Exit Sub
        End If

        Dim scbtype As String = "" : Dim sVar As String = ""
        Dim iCurID As Integer = 0 : Dim sCBCode As String = ""
        If Session("oid") = Nothing Or Session("oid") = "" Then
            cashbankoid.Text = GenerateID("QL_trncashbankmst", cmpcode)
        Else
            cashbankoid.Text = Session("oid")
            CashBankNo.Text = Session("oid")
        End If

        Session("vIDCost") = GenerateID("QL_cashbankgl", cmpcode)
        Dim vIDMst As Integer = GenerateID("QL_trnglmst", cmpcode)
        Dim vIDDtl As Integer = GenerateID("QL_trngldtl", cmpcode)

        If payflag.SelectedValue = "CASH" Then
            scbtype = "BKM" : sVar = "VAR_CASH"
        ElseIf payflag.SelectedValue = "NON CASH" Then
            scbtype = "BBM" : sVar = "VAR_BANK"
        End If

        Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & deptoid.SelectedValue & "' AND gengroup='CABANG'")

        If lblPOST.Text = "POST" Then
            Dim sNo As String = scbtype & "/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1 FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%' and branch_code='" & deptoid.SelectedValue & "'"
            CashBankNo.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT into QL_trncashbankmst (cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, PIC_REFNAME, deptoid, createuser,createtime, bankoid,branch_code, cashbankamount, cashbankamountidr, cashbankamountusd, cashbankduedate) VALUES " & _
                "('" & cmpcode & "'," & cashbankoid.Text & ",'" & CashBankNo.Text & "','" & lblPOST.Text & "','" & scbtype & "','RECEIPT'," & lblCoa.Text & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & Tchar(cashbanknote.Text) & "','" & Session("UserID") & "',current_timestamp," & currate.SelectedValue & "," & ToDouble(currencyrate.Text) & ", '" & Tchar(picoid.Text) & "', '', '" & deptoid.SelectedValue & "', '" & Session("userid") & "', '" & CDate(toDate(createtime.Text)) & "'," & bank.SelectedValue & ",'" & deptoid.SelectedValue & "', '" & ToDouble(amtcost.Text) & "', '" & ToDouble(amtcost.Text) & "', '1', '" & CDate(toDate(payduedate.Text)) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim glamt, glamtidr, glamtusd As Double
                Dim rateusd As Double = getCurrency()
                If Not Session("dtlTable") Is Nothing Then
                    Dim objTable As DataTable
                    Dim objRow() As DataRow
                    objTable = Session("dtlTable")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                    Dim dateStr As String = "'" & toDate(cashbankdate.Text) & "'"
                    If payduedate.Visible Then
                        dateStr = "'" & toDate(payduedate.Text) & "'"
                    End If
                    For C1 As Int16 = 0 To objRow.Length - 1
                        glamt = ToDouble(objRow(C1)("cashbankglamt"))
                        glamtidr = ToDouble(objRow(C1)("cashbankglamt"))
                        glamtusd = ToDouble(objRow(C1)("cashbankglamt"))

                        sSql = "INSERT into QL_cashbankgl(cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,upduser,updtime,duedate,refno,cashbankglstatus,branch_code) VALUES " & _
                        " ('" & cmpcode & "'," & (Session("vIDCost") + C1) & "," & Integer.Parse(cashbankoid.Text) & "," & objRow(C1)("acctgoid") & "," & glamt & "," & glamtidr & "," & glamtusd & ",'" & Tchar(objRow(C1)("cashbankglnote")) & "','" & Session("UserID") & "',current_timestamp," & dateStr & "," & IIf(payrefno.Visible, "'" & Tchar(payrefno.Text) & "'", "''") & ",'" & lblPOST.Text & "','" & deptoid.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (Session("vIDCost") + objRow.Length - 1) & " WHERE tablename='QL_cashbankgl' AND cmpcode='" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                sSql = "UPDATE QL_mstoid SET lastoid=" & Integer.Parse(cashbankoid.Text) & " WHERE tablename='QL_trncashbankmst' AND cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                Dim bUpdPayType As Boolean = False
                'jika ada perubahan pada payment type
                If payflag.SelectedValue <> HiddenField1.Value Then
                    ' Cashbank No
                    Session("vNoCashBank") = scbtype & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                    sSql = "SELECT MAX(ABS(replace(cashbankno,'" & Session("vNoCashBank") & "',''))) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & Session("vNoCashBank") & "%'"
                    xCmd.CommandText = sSql
                    If Not IsDBNull(xCmd.ExecuteScalar) Then
                        iCurID = xCmd.ExecuteScalar + 1
                    Else
                        iCurID = 1
                    End If
                    Session("vNoCashBank") = GenNumberString(Session("vNoCashBank"), "", iCurID, DefCounter)
                    bUpdPayType = True
                End If

                sSql = "UPDATE QL_trncashbankmst SET cashbankno='" & CashBankNo.Text & "', cashbankacctgoid=" & lblCoa.Text & ", cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),cashbankstatus='" & lblPOST.Text & "',upduser = '" & Session("UserID") & "', updtime=current_timestamp, pic='" & Tchar(picoid.Text) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', deptoid='" & deptoid.SelectedValue & "' ,cashbankcurroid=" & currate.SelectedValue & ",cashbankcurrate=" & ToDouble(currencyrate.Text)
                If bUpdPayType Then
                    sSql &= ",cashbankoid='" & cashbankoid.Text & "', bankoid=" & bank.SelectedValue & ", cashbanktype='" & scbtype & "'"
                End If

                sSql &= " WHERE cmpcode = '" & cmpcode & "' and cashbankoid = " & Integer.Parse(cashbankoid.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim glamt, glamtidr, glamtusd As Double
                Dim rateusd As Double = getCurrency()

                If Not Session("dtlTable") Is Nothing Then
                    Dim objTable As DataTable : Dim objRow() As DataRow
                    objTable = Session("dtlTable")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                    sSql = "DELETE FROM QL_cashbankgl WHERE cashbankoid = " & Integer.Parse(cashbankoid.Text) & " AND branch_code = '" & deptoid.SelectedValue & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    Session("vIDCost") = GenerateID("QL_cashbankgl", cmpcode)
                    Dim dateStr As String = "'" & toDate(cashbankdate.Text) & "'"
                    If payduedate.Visible Then
                        dateStr = "'" & toDate(payduedate.Text) & "'"
                    End If

                    For C1 As Int16 = 0 To objRow.Length - 1
                        glamt = ToDouble(objRow(C1)("cashbankglamt"))
                        glamtidr = ToDouble(objRow(C1)("cashbankglamt"))
                        glamtusd = ToDouble(objRow(C1)("cashbankglamt"))

                        sSql = "INSERT into QL_cashbankgl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,upduser,updtime,duedate,refno,cashbankglstatus,branch_code) VALUES " & _
                        "('" & cmpcode & "'," & (Session("vIDCost") + C1) & "," & Integer.Parse(cashbankoid.Text) & "," & objRow(C1)("acctgoid") & "," & glamt & "," & glamtidr & "," & glamtusd & ",'" & Tchar(objRow(C1)("cashbankglnote")) & "','" & Session("UserID") & "',current_timestamp," & dateStr & ",'" & Tchar(payrefno.Text) & "','" & lblPOST.Text & "','" & deptoid.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        ' GL Detail
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & (Session("vIDCost") + objRow.Length) & " WHERE tablename='QL_cashbankgl'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            If lblPOST.Text = "POST" Then
                'update status cashbankmst
                sSql = "update ql_trncashbankmst set cashbankno='" & CashBankNo.Text & "', posttime=current_timestamp, cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), postuser='" & Session("userid") & "' Where cashbankoid=" & Integer.Parse(cashbankoid.Text) & " And cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '//////INSERT INTO TRN GL MST
                sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,type,branch_code) VALUES " & _
                " ('" & cmpcode & "'," & vIDMst & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg3(GetServerTime()) & "','" & tempPayFlag & " - Receipt|No=" & CashBankNo.Text & "','POST','" & GetServerTime() & "','" & Session("UserID") & "',current_timestamp,'RECEIPT','" & deptoid.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim glamt, glamtidr, glamtusd As Double
                Dim rateusd As Double = getCurrency()
                'insert data detail
                glamt = ToDouble(amtcost.Text)
                glamtidr = ToDouble(amtcost.Text)
                glamtusd = ToDouble(amtcost.Text)

                'VARCOST
                Dim iSeq As Integer = 1
                Dim objTable As DataTable : objTable = Session("dtlTable")
                If Not Session("dtlTable") Is Nothing Then
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        glamt = ToDouble(objTable.Rows(C1).Item("cashbankglamt"))
                        glamtidr = ToDouble(objTable.Rows(C1).Item("cashbankglamt"))
                        glamtusd = ToDouble(objTable.Rows(C1).Item("cashbankglamt"))
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,upduser,updtime,GLOTHER2,branch_code,glflag) VALUES " & _
                        " ('" & cmpcode & "'," & vIDDtl & "," & iSeq & "," & vIDMst & "," & objTable.Rows(C1).Item("acctgoid") & ",'C'," & glamt & "," & glamtidr & "," & glamtusd & ", '" & CashBankNo.Text & "','" & tempPayFlag & " - Receipt=" & Tchar(objTable.Rows(C1)("cashbankglnote")) & "','" & Session("UserID") & "',current_timestamp, '" & cashbankoid.Text & "','" & deptoid.SelectedValue & "','" & lblPOST.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iSeq += 1 : vIDDtl += 1
                    Next
                    'VARCASHBANK
                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,upduser,updtime,GLOTHER2,branch_code,glflag) " & _
                        "VALUES ('" & cmpcode & "'," & vIDDtl & "," & iSeq & "," & vIDMst & "," & lblCoa.Text & ",'D','" & ToDouble(amtcost.Text) & "','" & ToDouble(amtcost.Text) & "','1','" & Tchar(CashBankNo.Text) & "','" & tempPayFlag & " - Receipt|No=" & CashBankNo.Text & " | Note. " & Tchar(cashbanknote.Text) & "','" & Session("UserID") & "',current_timestamp, '" & cashbankoid.Text & "','" & deptoid.SelectedValue & "','" & lblPOST.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & vIDMst & " WHERE tablename='QL_trnglmst'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & vIDDtl & " WHERE tablename='QL_trngldtl'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update g set trnid = (select trnid from QL_trncashbankmst where cashbankoid = g.cashbankoid and branch_code = g.branch_code) from QL_cashbankgl g inner join QL_trncashbankmst cb on cb.branch_code=g.branch_code AND cb.cashbankoid=g.cashbankoid AND g.trnid=0"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & "<br/>" & sSql, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trncbreceipt.aspx?awal=true")
    End Sub
 
    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Dim sMsg As String = "" ': tbladd.Visible = False
        If acctgoid.Text = "" Then
            sMsg &= "COA Cost harus diisi !!<BR>"
        End If

        If ToDouble(cashbankglamt.Text) <= 0 Then
            sMsg &= "Amount harus >= 0!!"
        End If

        If cashbankglnote.Text.Trim.Length >= 150 Then
            sMsg &= "Note Detail harus <= 150!!"
        End If

        If lblCoa.Text = "0" Then
            sMsg &= "Cash/Akun Bank belum dipilih!!"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If Session("dtlTable") Is Nothing Then
            Dim dt As New DataTable
            dt.Columns.Add("seq", Type.GetType("System.Int32"))
            dt.Columns.Add("acctgoid", Type.GetType("System.Int32"))
            dt.Columns.Add("cashbankglamt", Type.GetType("System.Decimal"))
            dt.Columns.Add("cashbankglnote", Type.GetType("System.String"))
            dt.Columns.Add("acctgcode", Type.GetType("System.String"))
            dt.Columns.Add("acctgdesc", Type.GetType("System.String"))
            Session("dtlTable") = dt
        End If
        Dim objTable As DataTable = Session("dtlTable")
        Dim dv As DataView = objTable.DefaultView
        If i_u2.Text = "New Detail" Then
            dv.RowFilter = "acctgoid=" & lblAcctgOid.Text & " AND cashbankglnote='" & Tchar(cashbankglnote.Text) & "' "
        Else
            dv.RowFilter = "acctgoid=" & lblAcctgOid.Text & " AND cashbankglnote='" & Tchar(cashbankglnote.Text) & "' AND seq <> " & cashbankglseq.Text & " "
        End If
        If dv.Count > 0 Then
            dv.RowFilter = ""
            showMessage("This Data has been added before, please check!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        dv.RowFilter = ""
        Dim code As String = cKon.ambilscalar("SELECT acctgcode FROM ql_mstacctg WHERE acctgoid=" & lblAcctgOid.Text)
        Dim desc As String = cKon.ambilscalar("SELECT acctgdesc FROM ql_mstacctg WHERE acctgoid=" & lblAcctgOid.Text)
        Dim objRow As DataRow
        If i_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            cashbankglseq.Text = objTable.Rows.Count + 1
            objRow("seq") = cashbankglseq.Text
        Else
            objRow = objTable.Rows(cashbankglseq.Text - 1)
            objRow.BeginEdit()
        End If
        objRow("acctgoid") = lblAcctgOid.Text
        objRow("acctgcode") = code
        objRow("acctgdesc") = desc
        objRow("cashbankglamt") = ToDouble(cashbankglamt.Text)
        objRow("cashbankglnote") = cashbankglnote.Text
        Session("dtlTable") = objTable
        If i_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If
        GVDtlCost.DataSource = Session("dtlTable")
        GVDtlCost.DataBind()
        ClearDtlAP() : calcTotalInGridDtl()
        currate.Enabled = False : payflag.Enabled = True : currate.Enabled = False
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim strSQL As String
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            strSQL = "DELETE FROM QL_cashbankgl " & _
                    "WHERE cashbankoid = " & Trim(cashbankoid.Text) & " AND branch_code = '" & deptoid.SelectedValue & "' "
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            strSQL = "DELETE FROM QL_trncashbankmst " & _
                    "WHERE cmpcode='" & cmpcode & "' and cashbankoid = " & Trim(cashbankoid.Text) & " AND branch_code = '" & deptoid.SelectedValue & "' "
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            objTrans.Commit() : objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
        Session("oid") = Nothing : Session("dtlTable") = Nothing
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        'Response.Redirect("Expense.aspx?awal=true")
    End Sub

    Protected Sub btnPosting2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting2.Click
        lblPOST.Text = "POST" : btnsave_Click(sender, e)
    End Sub
#End Region
End Class
