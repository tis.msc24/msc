'UpdtBy:Els | LastUpdt:14.4.08

Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Default
    Inherits System.Web.UI.Page
    
#Region "variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
#End Region

#Region "Function"

#End Region

#Region "Procedure"
    Private Sub CabangDDL()
        sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
        FillDDL(dd_branch, sSql)
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        'untuk print
        rptReport.Load(Server.MapPath("~/report/rptChartOfAccountList.rpt"))
        rptReport.SetParameterValue("cmpcode", CompnyCode)
        Dim swhere As String = " AND " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%' "
        rptReport.SetParameterValue("sWhere", swhere)
        'cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report_DB_Server"), _
        '    System.Configuration.ConfigurationManager.AppSettings("Report_DB_Name"))

        'Dim crConnInfo As New ConnectionInfo
        'With crConnInfo
        '    .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report_DB_Server")
        '    .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report_DB_Name")
        '    .UserID = System.Configuration.ConfigurationManager.AppSettings("Report_DB_UID")
        '    .Password = System.Configuration.ConfigurationManager.AppSettings("Report_DB_Pwd")
        'End With
        cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        'Dim crConnInfo As New ConnectionInfo()
        'With crConnInfo
        '    .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report_DB_Server")
        '    .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report_DB_Name")
        '    .IntegratedSecurity = True
        'End With
        'SetDBLogonForReport(crConnInfo, report)

        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        rptReport.ExportToHttpResponse(formatReport, Response, True, sFileName)
        'Response.Redirect(Page.AppRelativeVirtualPath.ToString)
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""
        If Session("FilterDDL") Is Nothing = False Then
            FilterText.Text = Session("FilterText")
            FilterDDL.SelectedIndex = Session("FilterDDL")
            If Session("FilterText").ToString.Trim <> "" Then
                sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & FilterText.Text & "%' "
            End If
        End If

        If sCompCode <> "" Then
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, acctgdbcr, acctggrp1, acctggrp2, acctggrp3, acctgflag=case acctgflag when 'A' then 'Active' when 'P' then 'Passive' else 'Suspended' end, upduser, updtime FROM QL_mstacctg c " & sWhere & IIf(sWhere <> "", " and ", " where ") & "upper(cmpcode) LIKE '%" & sCompCode.ToUpper & "%' ORDER BY acctgoid desc"
        Else
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, acctgdbcr, acctggrp1, acctggrp2, acctggrp3, acctgflag=case acctgflag when 'A' then 'Active' when 'P' then 'Passive' else 'Suspended' end, upduser, updtime FROM QL_mstacctg c " & sWhere & "%' ORDER BY acctgoid desc"
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        If objDs.Tables("data").Rows.Count > 0 Then
            GVmst.Visible = True
            GVmst.DataSource = objDs.Tables("data")
            GVmst.DataBind()
            'lblmsg.Visible = False
        Else
            GVmst.Visible = False
            'lblmsg.Visible = True
        End If
    End Sub

    Private Sub BindDataCoa()
        sSql = "SELECT [cmpcode], [acctgoid], [acctgcode], [acctgdesc], Case [acctgdbcr] When 'D' then 'Debet' Else 'Credit' End [acctgdbcr], [acctggrp1], [acctggrp2], acctgflag FROM [QL_mstacctg] WHERE [cmpcode] like '%%' and [acctgoid] like '%%' and " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%' and [acctgdbcr] like '%%' and  [acctggrp1] like '%%' and  [acctggrp2] like '%%' ORDER BY ACCTGCODE"
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet : mySqlDA.Fill(objDs, "data")
        GridView1.DataSource = objDs.Tables("data")
        GridView1.DataBind() : GridView1.PageIndex = 0
        GridView1.Visible = True
    End Sub

    Sub FilterGVMst(ByVal id As String, ByVal code As String, ByVal name As String, ByVal dbcr As String, ByVal category As String, ByVal level As String)
        With SQLMst
            .SelectParameters("cmpcode").DefaultValue = "%" & CompnyCode & "%"
            .SelectParameters("acctgoid").DefaultValue = "%" & id & "%"
            .SelectParameters("acctgcode").DefaultValue = "%" & code & "%"
            .SelectParameters("acctgdesc").DefaultValue = "%" & name & "%"
            .SelectParameters("acctgdbcr").DefaultValue = "%" & dbcr & "%"
            .SelectParameters("acctggrp1").DefaultValue = "%" & category & "%"
            .SelectParameters("acctggrp2").DefaultValue = "%" & level & "%"
        End With
        GridView1.DataBind()
    End Sub

    Public Sub generateAcctgID()
        oid.Text = GenerateID("QL_mstacctg", CompnyCode)
    End Sub

    Public Sub FillTextBox(ByVal vAcctgoid As String)
        'Dim mySqlConn As New SqlConnection(ConnStr)
        Try
            Dim sqlSelect As String = "select cmpcode, acctgoid, acctgcode, acctgdesc, acctgdbcr, acctggrp1, acctggrp2,acctggrp3, acctgflag, upduser, updtime, ISNULL(branch_code,'10') branch_code From QL_mstacctg where cmpcode='" & CompnyCode & "' and acctgoid = '" & vAcctgoid & "'"
            Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, conn)
            Dim objDs As New DataSet
            Dim objTable As DataTable
            Dim objRow() As DataRow
            mySqlDA.Fill(objDs)
            objTable = objDs.Tables(0)
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length > 0 Then
                oid.Text = Trim(objRow(0)("acctgoid").ToString)
                ddlAccType.SelectedValue = Trim(objRow(0)("acctggrp1").ToString)

                'Dim maxlevel As Integer = 0

                'Dim temp As Object = ckon.ambilscalar("Select max(acctggrp2) from QL_mstacctg where acctggrp1 = '" & ddlAccType.SelectedItem.Text & "'")

                'If IsDBNull(temp) Then
                '    ddlLevelCOA.Items.Clear()
                '    ddlLevelCOA.Items.Add("1")
                'Else
                '    maxlevel = CInt(Trim(temp))
                '    ddlLevelCOA.Items.Clear()
                '    For i As Int32 = 1 To maxlevel + 1
                '        ddlLevelCOA.Items.Add(i)
                '    Next
                'End If
                'ddlLevelCOA.SelectedValue = Trim(objRow(0)("acctggrp2").ToString)
                txtAccName.Text = Trim(objRow(0)("acctgdesc").ToString)
                AcctgDbCr.SelectedValue = Trim(objRow(0)("acctgdbcr").ToString)
                ddlAccType.SelectedValue = Trim(objRow(0)("acctggrp1").ToString)
                lblParentID.Text = Trim(objRow(0)("acctggrp3").ToString)
                dd_branch.SelectedValue = Trim(objRow(0)("branch_code").ToString)
                If Trim(objRow(0)("acctggrp2").ToString) = "0" Then
                    chkParent.Checked = False : imbChoose.Visible = False
                    lblCode1.Visible = False : lbl.Visible = False
                Else
                    chkParent.Checked = True : imbChoose.Visible = True
                    lblCode1.Visible = True : lbl.Visible = True
                End If
                txtParentCode.Text = Trim(ckon.ambilscalar("Select acctgcode From QL_mstacctg where acctgoid = '" & lblParentID.Text & "'"))
                txtParentName.Text = Trim(ckon.ambilscalar("Select acctgdesc From QL_mstacctg where acctgoid = '" & lblParentID.Text & "'"))
                lblCode1.Text = txtParentCode.Text : Acctgcode.MaxLength = 20 - lblCode1.Text.Length
                Dim sCode As String = Trim(objRow(0)("acctgcode").ToString)
                Acctgcode.Text = Right(sCode, sCode.Length - lblCode1.Text.Length)
                acctgflag.SelectedValue = Trim(objRow(0)(8).ToString)
                UpdUser.Text = objRow(0)(9).ToString
                UpdTime.Text = objRow(0)(10).ToString
                btnSave.Enabled = True
                btnDelete.Enabled = True
            End If
            conn.Close()
        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Sub ClearItem()
        AcctgDesc.Text = ""
        AcctgDbCr.SelectedIndex = 0
        AcctgGrp1.Text = ""
        AcctgGrp2.Text = ""
        AcctgGrp3.Text = ""
        UpdUser.Text = Session("UserID")
        UpdTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstacctg.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "Update Data"
        Else
            i_u.Text = "New Data"
        End If

        If Not Page.IsPostBack Then
            ' Me.btnSave.Attributes.Add("onclick", "alert('Data is saved.');")
            'If Session("FilterText") <> "" Then
            '    Session("FilterDDL") = 0
            '    Session("FilterText") = ""
            'End If
            BindDataCoa()
            CabangDDL()
            'BindData(CompnyCode)
            'FilterGVMst("", "", "", "", "", "")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                lblupd.Text = "Last Update"
            Else
                generateAcctgID()
                btnDelete.Enabled = False
                UpdUser.Text = Session("UserID")
                UpdTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
                TabContainer1.ActiveTabIndex = 0
                lblupd.Text = "Create"
            End If
        End If
    End Sub

    Protected Sub GVmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmst.PageIndexChanging
        GVmst.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterDDL.SelectedIndex = 0 : FilterText.Text = ""
        BindDataCoa()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindDataCoa()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If oid.Text = "" Then
            lblKonfirmasi.Text = "Please fill Account ID!"
            Exit Sub
        ElseIf Acctgcode.Text.Trim = "" Then
            lblKonfirmasi.Text = "Please fill Account Number!"
            Exit Sub
        ElseIf txtAccName.Text.Trim = "" Then
            lblKonfirmasi.Text = "Please fill Account Name!"
            Exit Sub
        End If
        If chkParent.Checked And lblParentID.Text = "" Then
            lblKonfirmasi.Text = "Please select a Parent first!"
            Exit Sub
        End If

        'cek kode accounting   yang kembar
        Dim sSqlCheck1 As String = ""
        If chkParent.Checked Then
            sSqlCheck1 = "SELECT COUNT(-1) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' and acctgcode = '" & lblCode1.Text & "" & Acctgcode.Text & "'  "
        Else
            sSqlCheck1 = "SELECT COUNT(-1) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' and acctgcode = '" & Acctgcode.Text & "'  "
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck1 &= " AND acctgoid <> " & oid.Text
        End If
        If ckon.ambilscalar(sSqlCheck1) > 0 Then
            lblKonfirmasi.Text = "This Code Number is already used!!"
            Exit Sub
        End If

        'cek kode accounting   yang kembar
        sSqlCheck1 = "SELECT COUNT(-1) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' and " & _
                                 "acctgdesc = '" & Tchar(txtAccName.Text) & "' "
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck1 &= " AND acctgoid <> " & oid.Text
        End If
        If ckon.ambilscalar(sSqlCheck1) > 0 Then
            lblKonfirmasi.Text = "This Description is already used!!"
            Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            'Dim xada As String = ""
            'xada = "Select count(*) from QL_mstacctg where acctgcode = '" & lblCode1.Text & Tchar(Acctgcode.Text) & "'"
            'If (ckon.ambilscalar(xada)) > 0 Then
            '    lblKonfirmasi.Text = "This code number is already used!"
            '    Exit Sub
            'End If
            generateAcctgID()
        Else
            'Dim xada As String = ""
            'xada = "Select count(*) from QL_mstacctg where acctgcode = '" & lblCode1.Text & Tchar(Acctgcode.Text) & "' AND acctgoid<>" & oid.Text
            'If (ckon.ambilscalar(xada)) > 0 Then
            '    lblKonfirmasi.Text = "This code number is already used!"
            '    Exit Sub
            'End If
        End If
        Dim A As String = ckon.ambilscalar("Select isnull(acctggrp3,0) From QL_mstacctg where acctgoid = '" & lblParentID.Text & "'")
        Dim strSQL As String
        'Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Connection = conn
        objCmd.Transaction = objTrans
        'Try
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                strSQL = "INSERT into QL_mstacctg(cmpcode,acctgoid,acctgcode,acctgdesc,acctgdbcr,acctggrp1,acctggrp2,acctggrp3,acctgflag,upduser,updtime,createuser,branch_code) VALUES ('" & CompnyCode & "'," & oid.Text & ",'" & lblCode1.Text & Tchar(Acctgcode.Text) & "','" & Tchar(txtAccName.Text) & "','" & AcctgDbCr.SelectedValue & "','" & ddlAccType.SelectedItem.Text & "','" & IIf(chkParent.Checked = False, "0", ToDouble(ckon.ambilscalar("Select acctggrp3 From QL_mstacctg where acctgoid= '" & lblParentID.Text & "'")) + 1) & "','" & lblParentID.Text & "','" & acctgflag.SelectedValue & "','" & Session("userId") & "',current_timestamp,'" & UpdUser.Text & "','" & dd_branch.SelectedValue & "')"
                objCmd.CommandText = strSQL
                objCmd.ExecuteNonQuery()
            Else
                strSQL = "UPDATE QL_mstacctg " & _
                    "SET cmpcode='" & CompnyCode & "', acctgoid=" & oid.Text & ", acctgcode='" & lblCode1.Text & Tchar(Acctgcode.Text) & "', acctgdesc='" & Tchar(txtAccName.Text) & "', acctgdbcr='" & AcctgDbCr.SelectedValue & "', acctggrp1='" & ddlAccType.SelectedItem.Text & "', acctggrp2='" & IIf(chkParent.Checked = False, "0", ToDouble(ckon.ambilscalar("Select isnull(acctggrp3,0) From QL_mstacctg where acctgoid = '" & lblParentID.Text & "'")) + 1) & "', acctggrp3='" & lblParentID.Text & "', acctgflag='" & acctgflag.SelectedValue & "', upduser='" & Session("userId") & "', updtime=current_timestamp,branch_code='" & dd_branch.SelectedValue & "' " & _
                        "WHERE cmpcode='" & CompnyCode & "' and acctgoid='" & oid.Text & "'"
                objCmd.CommandText = strSQL
                objCmd.ExecuteNonQuery()
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "update  QL_mstoid set lastoid=" & oid.Text & " where tablename like '%QL_mstacctg%' and cmpcode like '%" & CompnyCode & "%' "
                objCmd.CommandText = sSql
                objCmd.ExecuteNonQuery()
            End If
            lblKonfirmasi.Text = "Data has been saved"

        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            lblKonfirmasi.Text = ex.ToString
            Exit Sub
        End Try
        objTrans.Commit()
        generateAcctgID()
        ClearItem()
        conn.Close()
        btnDelete.Enabled = False
        Session("oid") = Nothing
        FilterGVMst("", "", "", "", "", "")
        'BindData(CompnyCode)
        Response.Redirect("~\Master\mstacctg.aspx?awal=true")
        'End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If oid.Text = "" Then
            lblKonfirmasi.Text = "Please fill Accounting ID!"
            Exit Sub
        End If
        'cek relationship database to table
        Dim sColomnName() As String = {"acctgoid", _
            "acctgoid", "acctgoid"}
        Dim sTable() As String = {"QL_trngldtl", _
            "QL_conap", "QL_conar"}
        If CheckDataExists(oid.Text, sColomnName, sTable) = True Then
            lblKonfirmasi.Text = "Cannot delete because this data already used by another table!"
            Exit Sub
        End If

        Dim strSQL As String
        'Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        conn.Open()
        objTrans = conn.BeginTransaction()
        objCmd.Connection = conn
        objCmd.Transaction = objTrans

        Try
            ' Dim iHistOid As Integer = GenerateID("QL_HistoryLog", CompnyCode)
            strSQL = "DELETE QL_mstacctg " & _
            "WHERE cmpcode='" & CompnyCode & "' and acctgoid='" & oid.Text & "'"
            objCmd.CommandText = strSQL
            objCmd.ExecuteNonQuery()

            'sSql = "INSERT INTO QL_HistoryLog (cmpcode,historyoid,userid,activity,tablename,oid,activitydate,note) VALUES " & _
            '    "('" & CompnyCode & "'," & iHistOid & ",'" & Session("UserID") & "','DELETE','QL_mstacctg'," & oid.Text & _
            '    ",CURRENT_TIMESTAMP,'From=" & Request.UserHostAddress & "|Code=" & Acctgcode.Text & "|Desc=" & Tchar(txtAccName.Text) & "')"
            'objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            'sSql = "UPDATE QL_mstoid SET lastoid=" & iHistOid & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_HistoryLog'"
            'objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
            ClearItem()
            lblKonfirmasi.Text = "Data has been deleted."
            Session("oid") = ""
        Catch ex As Exception
            objTrans.Rollback()
            objCmd.Connection.Close()
            conn.Close()
            lblKonfirmasi.Text = ex.Message
            Exit Sub
        End Try
        objCmd.Connection.Close() : conn.Close()
        btnDelete.Enabled = True
        Response.Redirect("~\Master\mstacctg.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        ClearItem()
        Response.Redirect("~\master\mstacctg.aspx?awal=true")
    End Sub

#End Region

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Panel1.Visible = False
        'btnHidden.Visible = False
        'ModalPopupExtender1.Hide()
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("UpperID") = sender.CommandArgument
        'Panel1.Visible = False
        'btnHidden.Visible = False
        'ModalPopupExtender1.Hide()
    End Sub

    Protected Sub Acctgcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'MsgBox(Acctgcode.MaxLength)
    End Sub

    Protected Sub txtLevCOA_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ddlAccType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If gvParent.Visible = True Then
            gvParent.Visible = False
            txtParentCode.Text = ""
            txtParentName.Text = ""
            lblCode1.Text = ""
        Else
            txtParentCode.Text = ""
            txtParentName.Text = ""
            lblCode1.Text = ""
        End If
        'Dim maxlevel As Integer = 0

        'Dim temp As Object = ckon.ambilscalar("Select max(acctggrp2) from QL_mstacctg where acctggrp1 = '" & ddlAccType.SelectedItem.Text & "'")

        'If IsDBNull(temp) Then
        '    ddlLevelCOA.Items.Clear()
        '    ddlLevelCOA.Items.Add("1")
        'Else
        '    maxlevel = temp
        '    ddlLevelCOA.Items.Clear()
        '    For i As Int32 = 1 To maxlevel + 1
        '        ddlLevelCOA.Items.Add(i)
        '    Next
        'End If
        'ddlLevelCOA_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub ddlLevelCOA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If ddlLevelCOA.SelectedItem.Text <> "1" Then
        '    btnPilihParent.Visible = True
        '    'Acctgcode.MaxLength = CInt(ckon.ambilscalar("Select gengroup from QL_mstgen where gendesc = '" & ddlLevelCOA.SelectedItem.Text & "'"))
        'Else
        '    btnPilihParent.Visible = False
        '    'Acctgcode.MaxLength = 1
        'End If
        'txtParentCode.Text = ""
        'txtParentName.Text = ""
        'Acctgcode.Text = ""
        'lblAccChildCode.Text = ""
    End Sub

    Protected Sub gvParent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblParentID.Text = Trim(gvParent.SelectedDataKey.Item(0))
        txtParentCode.Text = Trim(gvParent.SelectedDataKey.Item(1))
        lblCode1.Text = Trim(gvParent.SelectedDataKey.Item(1))
        txtParentName.Text = Trim(gvParent.SelectedDataKey.Item(2))
        Acctgcode.MaxLength = 20 - lblCode1.Text.Length
        gvParent.Visible = False
        'btnHidden.Visible = False : Panel1.Visible = False
        'ModalPopupExtender1.Hide() : Acctgcode.Text = "" : Acctgcode.Focus()
    End Sub

    Protected Sub GridView1_Sorted(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.Sorted
        TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub lbModParClo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'btnHidden.Visible = False
        'Panel1.Visible = False
        'ModalPopupExtender1.Hide()
    End Sub

    Private Sub BindDataParent()
        Dim xdt As New DataTable
        xdt = ckon.ambiltabel("Select acctgoid, acctgcode, acctgdesc from QL_mstacctg Where acctggrp1 = '" & ddlAccType.SelectedItem.Text & "' AND acctgoid <> '" & Session("oid") & "' AND (acctgcode LIKE '%" & Tchar(sFilter.Text) & "%' OR acctgdesc LIKE '%" & Tchar(sFilter.Text) & "%') ORDER BY acctgcode ", "mstacc")
        gvParent.DataSource = xdt
        gvParent.DataBind()
        gvParent.SelectedIndex = -1
        gvParent.Visible = True
    End Sub

    Protected Sub imbChoose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbChoose.Click
        BindDataParent()
    End Sub

    Protected Sub chkParent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        imbChoose.Visible = chkParent.Checked
        imbErase.Visible = chkParent.Checked
        gvParent.Visible = chkParent.Checked
        txtParentCode.Visible = chkParent.Checked
        txtParentName.Visible = chkParent.Checked
        lblCode1.Visible = chkParent.Checked : lbl.Visible = chkParent.Checked
        If Not chkParent.Checked Then
            lblParentID.Text = "" : txtParentCode.Text = "" : txtParentName.Text = ""
            lblCode1.Text = ""
        End If
    End Sub

    Protected Sub imbErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If gvParent.Visible = True Then
            gvParent.Visible = False
        Else
            txtParentCode.Text = ""
            txtParentName.Text = ""
            lblCode1.Text = ""
        End If
    End Sub

    Protected Sub gvParent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParent.PageIndex = e.NewPageIndex
        BindDataParent()
        gvParent.Visible = True
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintReport("", "COA_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintReport("", "COA_PrintOut", ExportFormatType.Excel)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub imbPrintCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrintCOA.Click
        printType.Text = "CAO"
        imbPrintPDF.Visible = True
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("oid") = ""
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        rptReport.Close() : rptReport.Dispose()
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintContract(printType.Text, orderIDForReport.Text, orderNoForReport.Text, ExportFormatType.Excel)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)
        Dim sWhere As String = "" 
        If FilterText.Text.Trim <> "" Then
            If FilterDDL.SelectedItem.Text.ToUpper = "acctgcode" Or FilterDDL.SelectedItem.Text.ToUpper = "acctgdesc" Then
                sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
            Else
                sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If
        End If
        Response.Clear()
        If printType = "CAO" Then
            Response.AddHeader("content-disposition", "inline;filename=ListCOA.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"

            sSql = "select ISNULL((Select gendesc from QL_mstgen g Where gencode=branch_code AND gengroup='CABANG'),'-') Cabang, acctgcode, acctgdesc, case acctgdbcr when 'D' then 'Debet' when 'C' then 'Credit' end as stat, acctggrp1, acctgflag from ql_mstacctg " & sWhere & " order by acctgcode"
        End If


        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
        mpePrint.Show()
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintReport("", "ListCOA", ExportFormatType.PortableDocFormat)
            'PrintContract(printType.Text, orderIDForReport.Text, orderNoForReport.Text, ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridView1.PageIndex = e.NewPageIndex
        BindDataCoa() : GridView1.Visible = True
    End Sub
End Class