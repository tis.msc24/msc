Imports System.Data
Imports System.Data.SqlClient
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure

Partial Class TTS_Close
    Inherits System.Web.UI.Page
    Dim sSql As String = ""
    Dim CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cmd As New SqlCommand("", conn)
    Dim cKon As New Koneksi
    Dim cFungsi As New ClassFunction
    Dim cProc As New ClassProcedure

#Region "Procedure"

    Private Sub showMessage(ByVal isi As String, ByVal judul As String)
        isiPesan.Text = isi
        captionPesan.Text = judul
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, True)
    End Sub

    Private Sub CabangInit()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(cBangFilter, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(cBangFilter, sSql)
            Else
                FillDDL(fCabang, sSql)
                cBangFilter.Items.Add(New ListItem("ALL", "ALL"))
                cBangFilter.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(cBangFilter, sSql)
            cBangFilter.Items.Add(New ListItem("ALL", "ALL"))
            cBangFilter.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub fCabangInit()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCabang, sSql)
            Else
                FillDDL(fCabang, sSql)
                fCabang.Items.Add(New ListItem("ALL", "ALL"))
                fCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fCabang, sSql)
            fCabang.Items.Add(New ListItem("ALL", "ALL"))
            fCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub BinddataTrn()
        Try
            Dim dat1 As Date = CDate(toDate(tglAwal.Text))
            Dim dat2 As Date = CDate(toDate(tglAkhir.Text))
        Catch ex As Exception
            showMessage("Invalid filter date!!", CompnyName & " - WARNING") : Exit Sub
        End Try

        If CDate(toDate(tglAwal.Text)) > CDate(toDate(tglAkhir.Text)) Then
            showMessage("Start date must be less or equal than End date!!", CompnyName & " - WARNING")
            Exit Sub
        End If

        sSql = "SELECT rd.reqmstoid, rm.branch_code, (Select gendesc from QL_mstgen cb WHERE cb.gencode=rm.branch_code AND cb.gengroup='CABANG') cabang, Convert(Char(20),rm.reqdate,103) reqdate, rm.reqcode, c.custname, c.custoid, rm.reqstatus FROM QL_TRNREQUESTDTL rd INNER JOIN QL_TRNREQUEST rm ON reqmstoid=reqoid INNER JOIN QL_mstcust c ON c.custoid=rm.reqcustoid WHERE reqdtloid NOT IN (SELECT DISTINCT conrefoid FROM QL_conmtr WHERE conrefoid>1 AND Formname<>'ql_trnrequestdtl') AND rm.reqstatus IN ('Closed','In Approval') AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"

        If CbPeriode.Checked = True Then
            sSql &= "AND convert(char(10),rm.reqdate,121) Between '" & Format(CDate(toDate(tglAwal.Text)), "yyyy-MM-dd") & "' AND '" & Format(CDate(toDate(tglAkhir.Text)), "yyyy-MM-dd") & "'"
        End If

        If cBangFilter.SelectedValue <> "ALL" Then
            sSql &= "And rm.branch_code='" & cBangFilter.SelectedValue & "'"
        End If

        sSql &= " Group By reqmstoid, rd.reqmstoid, rm.reqdate, rm.reqcode, c.custname, rm.branch_code, c.custoid, rm.reqstatus ORDER BY reqcode"

        Dim objtrn As DataTable = cKon.ambiltabel(sSql, "itemlist")
        gvClosedBPM.DataSource = objtrn
        gvClosedBPM.DataBind()
        Session("gvtrn") = objtrn
        gvClosedBPM.Visible = True
    End Sub

    Private Sub BindData()
        Try
            sSql = "SELECT rd.reqmstoid, rm.branch_code,(Select gendesc from QL_mstgen cb WHERE cb.gencode=rm.branch_code AND cb.gengroup='CABANG') cabang, Convert(Char(20),rm.reqdate,103) reqdate, rm.reqcode, c.custname, c.custoid FROM QL_TRNREQUESTDTL rd INNER JOIN QL_TRNREQUEST rm ON reqmstoid=reqoid INNER JOIN QL_mstcust c ON c.custoid=rm.reqcustoid WHERE reqdtloid NOT IN (SELECT DISTINCT conrefoid FROM QL_conmtr WHERE conrefoid>1 AND Formname<>'ql_trnrequestdtl') AND rm.reqstatus IN ('APPROVED','POST') AND (rm.reqcode LIKE '%" & Tchar(txtorderNo.Text) & "%' OR c.custname LIKE '%" & Tchar(txtorderNo.Text) & "%')"

            If fCabang.SelectedValue <> "ALL" Then
                sSql &= "And rm.branch_code='" & fCabang.SelectedValue & "'"
            End If

            sSql &= " Group By reqmstoid, rd.reqmstoid, rm.reqdate, rm.reqcode, c.custname, rm.branch_code, c.custoid ORDER BY reqcode"
            FillGV(gvBPM2, sSql, "QL_TRNREQUEST")
            gvBPM2.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br /> " & sSql, CompnyName & " - WARNING")
            Exit Sub
        End Try
    End Sub

    Private Sub BindItem(ByVal sCabang As String)
        sSql = "SELECT rd.reqmstoid, reqdtloid, m.itemoid, m.itemcode, m.itemdesc, rd.reqqty, typegaransi, reqdtljob, kelengkapan, snno, ISNULL((Select trnjualno From QL_trnjualmst jm Where jm.trnjualmstoid=rd.trnjualoid AND rd.branch_code=jm.branch_code),'NON SI') NoSI FROM QL_TRNREQUESTDTL rd INNER join QL_mstitem m ON m.itemoid=rd.itemoid WHERE reqdtloid NOT IN (SELECT DISTINCT conrefoid FROM QL_conmtr WHERE conrefoid>1 AND Formname<>'ql_trnrequestdtl') AND rd.reqmstoid=" & Integer.Parse(ordermstoid.Text) & " And branch_code='" & sCabang & "' ORDER BY rd.reqdtloid"
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "itemlist")
        gv_item.DataSource = objTable
        gv_item.DataBind()
        Session("gvitem") = objTable
        gv_item.Visible = True
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'jangan lupa cek hak akses
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSDO")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSDO") = sqlSearch
            Response.Redirect("~/Transaction/TrnClosedTTS.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Sales Order Closing"

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            CabangInit() : fCabangInit()
            tglAwal.Text = Format(GetServerTime(), "dd/MM/yyyy")
            tglAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            ddlFilter.SelectedIndex = 1 : TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'btnExtenderValidasi.Visible = False
        'PanelValidasi.Visible = False
        'mpeMsg.Hide()
        Dim x As Integer = 123
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, False)
        'isiPesan.Text = "tes"
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlFilter.SelectedValue <> "spmno" Then
            tglAwal.Enabled = True
            tglAwal.CssClass = "inpText"
            tglAkhir.Enabled = True
            tglAkhir.CssClass = "inpText"
        Else
            tglAwal.Enabled = False
            tglAwal.CssClass = "inpTextDisabled"
            tglAkhir.Enabled = False
            tglAkhir.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub imbSearchBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchBPM.Click 
        BindData()
    End Sub

    Protected Sub imbEraseBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseBPM.Click
        txtorderNo.Text = "" : ordermstoid.Text = ""
        custname.Text = "" : gvBPM2.Visible = False
        gv_item.Visible = False : gv_item.SelectedIndex = -1
        gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBPM2.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy") 
        'End If
    End Sub

    Protected Sub gvBPM2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBPM2.SelectedIndexChanged
        ordermstoid.Text = gvBPM2.SelectedDataKey.Item("reqmstoid")
        txtorderNo.Text = gvBPM2.SelectedDataKey.Item("reqcode")
        custoid.Text = gvBPM2.SelectedDataKey.Item("custoid")
        custname.Text = gvBPM2.SelectedDataKey.Item("custname")
        fCabang.SelectedValue = gvBPM2.SelectedDataKey.Item("branch_code")
        bindItem(gvBPM2.SelectedDataKey.Item("branch_code"))
        gvBPM2.Visible = False : gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBPM2.PageIndexChanging
        gvBPM2.PageIndex = e.NewPageIndex 
        BindData()
    End Sub

    Protected Sub btnCloseBPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseBPM.Click
        Dim sErr As String = ""
        If ordermstoid.Text.Trim = "" Or txtorderNo.Text = "" Then
            sErr &= "Silahkan pilih No. TTS !<br />"
            Exit Sub
        End If

        If txtorderNote.Text.Trim = "" Then
            sErr &= "Maaf, Silahkan Isi keterangan Closed dahulu !<br />"
        End If

        sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNCLOSEDTTS' And branch_code LIKE '%" & fCabang.SelectedValue & "%' order by approvallevel"
        Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
        If dtData2.Rows.Count > 0 Then
            Session("TblApproval") = dtData2
        Else
            sErr &= "Tidak ada User untuk Approved Closed Penerimaan, Silahkan hubungi admin dahulu..!!<br>"
        End If

        If sErr <> "" Then
            showMessage(sErr, CompnyName & " - WARNING")
            Exit Sub
        End If

        Session("AppOid") = GenerateID("QL_Approval", CompnyCode)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        cmd.Transaction = objTrans

        Try
            sSql = "Update QL_TRNREQUEST Set noteclosed='" & Tchar(txtorderNote.Text) & "', reqstatus='" & lblStatus.Text & "' Where reqoid=" & ordermstoid.Text
            cmd.CommandText = sSql : cmd.ExecuteNonQuery()

            If Not Session("TblApproval") Is Nothing Then
                Dim objTable As DataTable : objTable = Session("TblApproval")
                For c1 As Int16 = 0 To objTable.Rows.Count - 1
                    sSql = "INSERT INTO QL_APPROVAL (cmpcode, approvaloid, branch_code, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus) VALUES" & _
                    " ('" & CompnyCode & "', " & Session("AppOid") + c1 & ", '" & fCabang.SelectedValue & "', '" & "TTS" & ordermstoid.Text & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'QL_TRNCLOSEDTTS', '" & Integer.Parse(ordermstoid.Text) & "', 'In Approval', '0', '" & objTable.Rows(c1).Item("approvaluser") & "', '1/1/1900', '" & objTable.Rows(c1).Item("approvaltype") & "', '1', '" & objTable.Rows(c1).Item("approvalstatus") & "')"
                    cmd.CommandText = sSql : cmd.ExecuteNonQuery()
                Next
                sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                cmd.CommandText = sSql : cmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            cmd.Connection.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR")
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\TrnClosedTTS.aspx?awal=true")
    End Sub

    Protected Sub gvClosedBPM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvClosedBPM.PageIndex = e.NewPageIndex
        'btnFind_Click(sender, e)
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Redirect("~\Transaction\TrnClosedTTS.aspx")
    End Sub

    Protected Sub gvClosedBPM_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy HH:mm:ss")
        End If
    End Sub

    Protected Sub txtorderNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtorderNo.TextChanged
        'bindItem()
    End Sub

    Protected Sub ImgFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgFind.Click 
        BinddataTrn()
    End Sub

    Protected Sub ImgAllFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAllFind.Click
        tglAwal.Text = Format(GetServerTime().AddDays(-1), "dd/MM/yyyy")
        tglAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        txtFilter.Text = "" : CbPeriode.Checked = False
        BinddataTrn()
    End Sub

    Protected Sub fCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fCabang.SelectedIndexChanged
        Session("gvitem") = Nothing : gv_item.Visible = False
        ordermstoid.Text = "" : txtorderNo.Text = ""
        custoid.Text = "" : custname.Text = ""
    End Sub
#End Region
End Class
