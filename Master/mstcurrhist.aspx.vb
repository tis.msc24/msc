'Prgmr:DW | UpdtBy:4n7JuK | LastUpdt:09.04.25
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class mstcurrhist
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim xCmd As New SqlCommand("", conn)
    Dim sSql As String
#End Region

    Sub FilterGV(ByVal filterCode As String, ByVal filterDesc As String, ByVal curroid As String, ByVal date1 As String, ByVal date2 As String, ByVal filteruser As String)
        With SDSDataView
            .SelectParameters("curroid").DefaultValue = curroid
            .SelectParameters("currdate").DefaultValue = date1
            .SelectParameters("currdate2").DefaultValue = date2
            .SelectParameters("upduser").DefaultValue = "%%"
            .SelectParameters("CmpCode").DefaultValue = "%" & CompnyCode & "%"
        End With
        GVmst.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        Session("Oid") = Request.QueryString("Oid")

        'CType(FrmViewCurrHis.FindControl("btnDelete"), Button).Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim oid As Integer = Session("oid")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("oid") = oid

            Response.Redirect("~/master/mstcurrhist.aspx?oid=" & Session("Oid")) '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        ' Session("Oid2") = Request.QueryString("Oid2")
        'Session("Oid") = Request.QueryString("Oid")
        ' MsgBox(Session("oid2"))

        Page.Title = CompnyName & " - Currency History"

        If Not IsPostBack Then
            If cKoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("SpecialAccess")) = True Then
                FilterGV("", "", Session("Oid"), "1-1-1900", Now.Date, "")
            Else
                FilterGV("", "", Session("Oid"), "1-1-1900", Now.Date, Session("UserID"))
            End If
            '   If Session("oid2") Is Nothing Or Session("oid") = "" Then
            FrmViewCurrHis.ChangeMode(FormViewMode.Insert)
            'CType(FrmViewCurrHis.FindControl("currdate"), Label).Text = Now.ToShortDateString
            CType(FrmViewCurrHis.FindControl("oid"), TextBox).Text = ClassFunction.GenerateID("QL_mstCurrhist", CompnyCode)
            CType(FrmViewCurrHis.FindControl("UpdUser"), Label).Text = Session("UserID")
            'CType(FrmViewCurrHis.FindControl("UpdTime"), Label).Text = Now
            'Else
            '   With SDSData
            '.SelectParameters("Currhistoid").DefaultValue = Session("oid")
            '.SelectParameters("Curroid").DefaultValue = Session("oid")
            '.SelectParameters("cmpcode").DefaultValue = CompnyCode
            'End With
            '   FrmViewCurrHis.ChangeMode(FormViewMode.Edit)
            FrmViewCurrHis.DataBind()
            'CType(FrmViewCurrHis.FindControl("UpdUser"), Label).Text = Session("UserID")
            'CType(FrmViewCurrHis.FindControl("UpdTime"), Label).Text = Now
            If Session("kode") = "" Or IsDBNull(Session("kode")) Then
                TabContainer1.ActiveTabIndex = 0
            Else
                TabContainer1.ActiveTabIndex = 1
            End If
            'If Session("tab") = "0" Then
            '    TabContainer1.ActiveTabIndex = 0
            'End If
            'End If
            CType(FrmViewCurrHis.FindControl("currdate"), TextBox).Text = Format(Date.Now, "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub LBOid_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        TabContainer1.ActiveTabIndex = 1

        Dim lnkEdit As LinkButton = sender
        Dim row As GridViewRow = lnkEdit.NamingContainer
        Session.Item("kode") = lnkEdit.CommandArgument
        With SDSData
            .SelectParameters("Currhistoid").DefaultValue = Session.Item("kode")
            .SelectParameters("Curroid").DefaultValue = Session.Item("oid")
            .SelectParameters("cmpcode").DefaultValue = CompnyCode
        End With
        FrmViewCurrHis.ChangeMode(FormViewMode.Edit)
        FrmViewCurrHis.DataBind()
    End Sub

    Protected Sub FrmViewCurrHis_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewDeletedEventArgs) Handles FrmViewCurrHis.ItemDeleted
        GVmst.DataBind()
        CType(FrmViewCurrHis.FindControl("oid"), TextBox).Text = ClassFunction.GenerateID("QL_mstcurrhist", CompnyCode)
        Response.Redirect("~/Master/mstcurrhist.aspx?oid=" & Session("oid") & "&awal=true")
    End Sub

    Protected Sub FrmViewCurrHis_ItemDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewDeleteEventArgs) Handles FrmViewCurrHis.ItemDeleting
        With SDSData
            .DeleteParameters("CurrhistOID").DefaultValue = CType(FrmViewCurrHis.FindControl("oid"), TextBox).Text
            .DeleteParameters("CurrOid").DefaultValue = Session("oid") 'CType(FrmViewCurrHis.FindControl("oid"), TextBox).Text
            .DeleteParameters("cmpcode").DefaultValue = CompnyCode
        End With
    End Sub

    Protected Sub FrmViewCurrHis_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertedEventArgs) Handles FrmViewCurrHis.ItemInserted
        SDSOid.Update()
        GVmst.DataBind()
        'FrmViewCurrHis.ChangeMode(FormViewMode.Edit)
        CType(FrmViewCurrHis.FindControl("oid"), TextBox).Text = ClassFunction.GenerateID("QL_mstcurrhist", CompnyCode)
        Response.Redirect("~/Master/mstcurrhist.aspx?oid=" & Session("oid") & "&awal=true")
    End Sub

    Protected Sub FrmViewCurrHis_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertEventArgs) Handles FrmViewCurrHis.ItemInserting
        Try
            If CType(FrmViewCurrHis.FindControl("currdate"), TextBox).Text = "" Then
                lblWarning.Text = "Silahkan mengisi kolom Currency Date !!"
                e.Cancel = True : Exit Sub
            End If


            If validateCurrencyDate(CType(FrmViewCurrHis.FindControl("currdate"), TextBox).Text) = False Then
                lblWarning.Text = "Currency date tidak boleh melebihi tanggal hari ini !!"
                e.Cancel = True : Exit Sub
            End If
        Catch ex As Exception
            lblWarning.Text = ex.Message : e.Cancel = True : Exit Sub
        End Try
        CType(FrmViewCurrHis.FindControl("oid"), TextBox).Text = ClassFunction.GenerateID("QL_mstCurrHist", CompnyCode)
        CType(FrmViewCurrHis.FindControl("UpdUser"), Label).Text = Session("UserID")
        CType(FrmViewCurrHis.FindControl("UpdTime"), Label).Text = Format(CDate(Now().ToString), "MM/dd/yyyy")  'FormatDateTime(Now(), "MM/DD/YYYY")

        'Format(CDate(tblMst.Rows(0)("spkopendate").ToString), "MM/dd/yyyy")

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        conn.Open()
        Dim sMsg As String = ""
        If CType(FrmViewCurrHis.FindControl("curratestoIDRbeli"), TextBox).Text = "" Or ToDouble(CType(FrmViewCurrHis.FindControl("curratestoIDRbeli"), TextBox).Text) = 0 Then
            CType(FrmViewCurrHis.FindControl("curratestoIDRbeli"), TextBox).Text = 0
            sMsg &= "- Silahkan mengisi kolom Sell Rupiah !!!<br>"
        End If

        If CType(FrmViewCurrHis.FindControl("curratestoIDRjual"), TextBox).Text = "" Or ToDouble(CType(FrmViewCurrHis.FindControl("curratestoIDRjual"), TextBox).Text) = 0 Then
            CType(FrmViewCurrHis.FindControl("curratestoIDRjual"), TextBox).Text = 0
            sMsg &= "- Silahkan mengisi kolom Buy Rupiah !!!<br>"
        End If

        Dim midIDR As Integer = ToDouble((CType(FrmViewCurrHis.FindControl("curratestoIDRbeli"), TextBox).Text) + ToDouble(CType(FrmViewCurrHis.FindControl("curratestoIDRjual"), TextBox).Text)) / 2

        If CType(FrmViewCurrHis.FindControl("curratestoUSDbeli"), TextBox).Text = "" Or ToDouble(CType(FrmViewCurrHis.FindControl("curratestoUSDbeli"), TextBox).Text) = 0 Then
            CType(FrmViewCurrHis.FindControl("curratestoUSDbeli"), TextBox).Text = 0
            sMsg &= "- Silahkan mengisi kolom Sell USD !!!<br>"
        End If

        If CType(FrmViewCurrHis.FindControl("curratestoUSDjual"), TextBox).Text = "" Or ToDouble(CType(FrmViewCurrHis.FindControl("curratestoUSDjual"), TextBox).Text) = 0 Then
            CType(FrmViewCurrHis.FindControl("curratestoUSDjual"), TextBox).Text = 0
            sMsg &= "- Silahkan mengisi kolom Buy USD !!!<br>"
        End If

        Dim midUSD As Integer = ToDouble((CType(FrmViewCurrHis.FindControl("curratestoUSDbeli"), TextBox).Text) + ToDouble(CType(FrmViewCurrHis.FindControl("curratestoUSDjual"), TextBox).Text)) / 2

        If sMsg <> "" Then
            lblWarning.Text = sMsg
            e.Cancel = True
            conn.Close()
            Exit Sub
        End If

        sSql = "INSERT INTO [QL_mstcurrhist] ([cmpcode], [currhistoid], [curroid], [currdate], [curratestoIDRbeli], [curratestoIDRjual], [curratestoUSDbeli], [curratestoUSDjual], [currnotes], [createuser], [upduser], [updtime], currmidtoIDR, currmidtoUSD) VALUES ( '" & CompnyCode & "', " & CType(FrmViewCurrHis.FindControl("oid"), TextBox).Text & ", '" & Session("oid") & "', '" & CType(FrmViewCurrHis.FindControl("currdate"), TextBox).Text & "', " & ToDouble(CType(FrmViewCurrHis.FindControl("curratestoIDRbeli"), TextBox).Text) & ", " & ToDouble(CType(FrmViewCurrHis.FindControl("curratestoIDRjual"), TextBox).Text) & ", " & ToDouble(CType(FrmViewCurrHis.FindControl("curratestoUSDbeli"), TextBox).Text) & ", " & ToDouble(CType(FrmViewCurrHis.FindControl("curratestoUSDjual"), TextBox).Text) & ", '" & Tchar(CType(FrmViewCurrHis.FindControl("currnotes"), TextBox).Text) & "', '" & CType(FrmViewCurrHis.FindControl("UpdUser"), Label).Text & "', '" & CType(FrmViewCurrHis.FindControl("UpdUser"), Label).Text & "', '" & CType(FrmViewCurrHis.FindControl("UpdTime"), Label).Text & "' , " & ToDouble(midIDR) & ", " & ToDouble(midUSD) & ") "

        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()

        ' ''With SDSData
        ' ''    .InsertParameters("CurrhistOid").DefaultValue = ClassFunction.GenerateID("QL_mstCurrHist", CompnyCode) 'id
        ' ''    .InsertParameters("cmpcode").DefaultValue = CompnyCode
        ' ''    .InsertParameters("CurrOid").DefaultValue = Session("oid") 'id
        ' ''    .InsertParameters("upduser").DefaultValue = Session("Userid") 'cFunction.GenerateID("QL_mstCurrHist", CompnyCode) 'id
        ' ''    .InsertParameters("updtime").DefaultValue = Now
        ' ''End With
        ' ''With SDSOid
        ' ''    .UpdateParameters("lastoid").DefaultValue = ClassFunction.GenerateID("QL_mstCurrHist", CompnyCode) 'id
        ' ''    .UpdateParameters("cmpcode").DefaultValue = CompnyCode
        ' ''    .UpdateParameters("tablename").DefaultValue = "QL_mstCurrHist"
        ' ''End With

        sSql = "UPDATE QL_mstoid SET lastoid = " & ClassFunction.GenerateID("QL_mstCurrHist", CompnyCode) & " WHERE (tablename = 'QL_mstCurrHist') AND (cmpcode = '" & CompnyCode & "')"

        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()

        conn.Close()
        Response.Redirect("~/Master/mstcurrhist.aspx?oid=" & Session("oid") & "&awal=true")
    End Sub

    Protected Sub FrmViewCurrHis_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdatedEventArgs) Handles FrmViewCurrHis.ItemUpdated
        GVmst.DataBind()
        Response.Redirect("~/Master/mstcurrhist.aspx?oid=" & Session("oid") & "&awal=true")
    End Sub

    Protected Sub FrmViewCurrHis_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdateEventArgs) Handles FrmViewCurrHis.ItemUpdating
        Try
            If validateCurrencyDate(CType(FrmViewCurrHis.FindControl("currdate"), TextBox).Text) = False Then
                lblWarning.Text = "Currency Date tidak boleh melebihi tanggal hari ini!!"
                e.Cancel = True : Exit Sub
            End If
        Catch ex As Exception
            lblWarning.Text = ex.Message : e.Cancel = True : Exit Sub
        End Try
        CType(FrmViewCurrHis.FindControl("UpdUser"), Label).Text = Session("UserID")
        CType(FrmViewCurrHis.FindControl("UpdTime"), Label).Text = Now

        Dim sMsg As String = ""
        If CType(FrmViewCurrHis.FindControl("curratestoIDRbeli"), TextBox).Text = "" Or ToDouble(CType(FrmViewCurrHis.FindControl("curratestoIDRbeli"), TextBox).Text) = 0 Then
            CType(FrmViewCurrHis.FindControl("curratestoIDRbeli"), TextBox).Text = 0
            sMsg &= "- Silahkan mengisi kolom Sell Rupiah !!!<br>"
        End If

        If CType(FrmViewCurrHis.FindControl("curratestoIDRjual"), TextBox).Text = "" Or ToDouble(CType(FrmViewCurrHis.FindControl("curratestoIDRjual"), TextBox).Text) = 0 Then
            CType(FrmViewCurrHis.FindControl("curratestoIDRjual"), TextBox).Text = 0
            sMsg &= "- Silahkan mengisi kolom Buy Rupiah !!!<br>"
        End If

        If CType(FrmViewCurrHis.FindControl("curratestoUSDbeli"), TextBox).Text = "" Or ToDouble(CType(FrmViewCurrHis.FindControl("curratestoUSDbeli"), TextBox).Text) = 0 Then
            CType(FrmViewCurrHis.FindControl("curratestoUSDbeli"), TextBox).Text = 0
            sMsg &= "- Silahkan mengisi kolom Sell USD !!!<br>"
        End If

        If CType(FrmViewCurrHis.FindControl("curratestoUSDjual"), TextBox).Text = "" Or ToDouble(CType(FrmViewCurrHis.FindControl("curratestoUSDjual"), TextBox).Text) = 0 Then
            CType(FrmViewCurrHis.FindControl("curratestoUSDjual"), TextBox).Text = 0
            sMsg &= "- Silahkan mengisi kolom Buy USD !!!<br>"
        End If

        If sMsg <> "" Then
            lblWarning.Text = sMsg
            e.Cancel = True
            Exit Sub
        End If

        With SDSData
            .UpdateParameters("CurrhistOID").DefaultValue = CType(FrmViewCurrHis.FindControl("oid"), TextBox).Text
            .UpdateParameters("Curroid").DefaultValue = Session("oid")
            .UpdateParameters("CmpCode").DefaultValue = CompnyCode
            .UpdateParameters("UpdUser").DefaultValue = Session("UserID")
            .UpdateParameters("Updtime").DefaultValue = Now
        End With
    End Sub

    Private Function validateCurrencyDate(ByVal currDate As Date) As Boolean
        Return currDate <= Today
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("kode") = Nothing
        Session("oid") = Nothing
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        If cKoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("SpecialAccess")) = True Then
            FilterGV("", "", Session("Oid"), "1-1-1900", Now.Date, "")
        Else
            FilterGV("", "", Session("Oid"), "1-1-1900", Now.Date, Session("UserID"))
        End If
        txtPeriode1.Text = "" : txtPeriode2.Text = "" : lblWarn.Text = ""
        GVmst.PageIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim st1, st2 As Boolean
        lblWarn.Text = ""
        Try
            Dim dt1 As Date = CDate(txtPeriode1.Text) : st1 = True
        Catch ex As Exception
            st1 = False
        End Try
        Try
            Dim dt2 As Date = CDate(txtPeriode2.Text) : st2 = True
        Catch ex As Exception
            st2 = False
        End Try
        If st1 And st2 Then
            If CDate(txtPeriode2.Text) < CDate(txtPeriode1.Text) Then
                lblWarn.Text = "End date can't be less than start date !!"
                Exit Sub
            End If
        Else
            lblWarn.Text = "Invalid date for period !!"
            Exit Sub
        End If

        If cKoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("SpecialAccess")) = True Then
            FilterGV("", "", Session("Oid"), txtPeriode1.Text, txtPeriode2.Text, "")
        Else
            FilterGV("", "", Session("Oid"), txtPeriode1.Text, txtPeriode2.Text, Session("UserID"))
        End If
        GVmst.DataBind()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Response.Redirect("~/master/mstcurr.aspx?awal=true")
    End Sub

    Protected Sub GVmst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = ToMaskEdit(ToDouble(e.Row.Cells(1).Text), 3)
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~/Master/mstcurrhist.aspx?oid=" & Session("oid") & "&awal=true")
        Session("tab") = "0"
    End Sub

    Protected Sub FrmViewCurrHis_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewPageEventArgs) Handles FrmViewCurrHis.PageIndexChanging

    End Sub
End Class