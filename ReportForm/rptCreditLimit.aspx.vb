Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports ClassFunction
Imports System.Data.SqlClient
Partial Class ReportForm_rptCreditLimit
    Inherits System.Web.UI.Page
#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Dim conn As New SqlConnection(ConnStr)
    Dim CProc As New ClassProcedure
    Dim sSql As String
    Dim sSql2 As String
    Dim ckon As New Koneksi
#End Region

#Region "Procedure"
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssclass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub
    Sub initAllDDL()
        initGroupItem()
        initSubGroupItem()
    End Sub
    Sub initGroupItem()
        'FillDDL2(itemcat, "select distinct genoid, gendesc, gengroup from QL_mstgen g inner join ql_mstitem m on g.genoid=m.itemgroupoid where gengroup IN ('ITEMGROUP') order by gendesc", "ALL")
    End Sub
    Sub initSubGroupItem()
        Dim sWhere As String = ""
        'If itemcat.SelectedItem.Text <> "ALL" Then
        '    sWhere = " and m.itemcategoryoid=" & itemcat.SelectedValue & ""
        'End If
        'FillDDL2(itemsubcat, "select distinct genoid, gendesc, gengroup from QL_mstgen g inner join ql_mstitem m on g.genoid=m.itemsubgroupoid where gengroup IN ('ITEMSUBGROUP') " & sWhere & " order by gendesc", "ALL")
    End Sub
    Sub bindDataCustomer()
        Dim sWhereTemp As String = ""
        'sWhereTemp = sWhere()
        sSql = "select custoid,custcode,custname from QL_mstcust where cmpcode='" & CompnyCode & "' and (custcode like '%" & Tchar(custName.Text) & "%' or custname like '%" & Tchar(custName.Text) & "%') "
        FillGV(gvCust, sSql, "ql_mstsupp")
        gvCust.Visible = True
    End Sub
    Sub bindDataItem()
        'Dim filterItem As String = ""
        'filterItem &= _
        '       IIf(itemcat.SelectedItem.Text <> "ALL", " and itemgroupoid=" & itemcat.SelectedValue, " ") & _
        '       IIf(itemsubcat.SelectedItem.Text <> "ALL", " and itemsubgroupoid=" & itemsubcat.SelectedValue, " ")
        'sSql = "SELECT itemoid,itemcode,itemdesc itemdesc FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' " & filterItem & " and itemdesc like '%" & Tchar(item.Text) & "%' ORDER BY itemdesc"
        'FillGV(gvItem, sSql, "QL_mstitem")
    End Sub
    'showReport
    Sub showPrint(ByVal ekstensi As String)
        vReport = New ReportDocument
        Dim sWhere As String = ""

        Dim d1 As String = ""
        Dim d2 As String = ""

        sWhere = "  "
        Try
            Dim dat1 As Date = CDate(toDate(sodate1.Text))
            Dim dat2 As Date = CDate(toDate(sodate2.Text))
        Catch ex As Exception
            showMessage("Invalid filter date!!", CompnyName & " - Warning", 2, "modalMsgBoxWarn") : Exit Sub
        End Try
        If CDate(toDate(sodate1.Text)) > CDate(toDate(sodate2.Text)) Then
            showMessage("Start date must be less or equal than End date!!", CompnyName & " - Warning", 2, "modalMsgBoxWarn") : Exit Sub
        End If


        'CUSTOMER
        If (custoid.Text.Trim <> "") Then
            sWhere &= " and c.custoid=" & custoid.Text & ""
        End If
        ''Taxable
        'If ddlTax.SelectedValue <> "ALL" Then
        '    sWhere &= " and so.taxable LIKE '%" & ddlTax.SelectedValue.ToUpper & "%' "
        'End If
        ''Item Category
        'If itemcat.SelectedItem.Text <> "ALL" Then
        '    sWhere &= " and i.itemgroupoid = " & itemcat.SelectedValue & " "
        'End If
        ''Item Sub Category
        'If itemsubcat.SelectedItem.Text <> "ALL" Then
        '    sWhere &= " and i.itemsubgroupoid = " & itemsubcat.SelectedValue & " "
        'End If
       
        If cb1.Checked = True Then
            Try
                Dim dat1 As Date = CDate(toDate(optionDate1.Text))
                Dim dat2 As Date = CDate(toDate(optionDate2.Text))
                sWhere &= " " & option1.SelectedValue & " convert(char(10)," & ddlOption1.SelectedValue & ",121) between '" & Format(CDate(toDate(optionDate1.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(optionDate2.Text)), "yyyy-MM-dd") & "'"
            Catch ex As Exception
                showMessage("Invalid filter date 2 !!", CompnyName & " - Warning", 2, "modalMsgBoxWarn") : Exit Sub
            End Try
        End If

      
        Dim rangeBulan As Integer = (DateDiff(DateInterval.Month, CDate(toDate(sodate1.Text)), CDate(toDate(sodate2.Text)))) + 1
        sSql = "select custoid,custcode,custgroup,custname,CP,phone1,fax,email,CL_IDR,'' location, " & _
               "sum(CL_Usage_Rupiah) CL_Usage_Rupiah,currencycode, " & _
               "CL_default,SUM(CL_Usage_Default) CL_Usage_Default,'' filter1,'' filterdate1,'' filter2,'' filterDate2, " & _
               "'' filterCustomer,'' FiltercustGroup,'' filterTaxable, " & _
               "'' filterCountry,'' filterProvince,'' filterCity,0.0 avgOrder_IDR,0.0 avgOrder_Default,'' reportName " & _
               "from ( " & _
               "    select distinct c.custoid,custcode,custgroup,custname,case when ISNULL(contactperson1,'')='' then '' " & _
               "    else g.gendesc+' '+ contactperson1 end CP,phone1,ISNULL(custfax1,'') fax,ISNULL(custemail,'') email, " & _
               "    custcreditlimitrupiah CL_IDR, " & _
               "    (so.amtjualnetto*so.currate) CL_Usage_Rupiah,cr.currencycode, " & _
               "    custcreditlimit CL_default,(so.amtjualnetto) CL_Usage_Default, " & _
               "    '' filter1,'' filterdate1,'' filter2,'' filterDate2,'' filterCustomer, " & _
               "    '' FiltercustGroup,'' filterTaxable,'' filterCountry,'' filterProvince,'' filterCity,0.0 avgOrder " & _
               "    from QL_mstcust c " & _
               "    inner join QL_mstgen g on g.genoid=c.prefixcp1 " & _
               "    inner join QL_trnordermst so on so.trncustoid=c.custoid " & _
               "    inner join QL_trnorderdtl sodtl on sodtl.trnordermstoid=so.ordermstoid  " & _
               "    inner join QL_mstcurr cr on cr.currencyoid=so.curroid   " & _
               "    inner join QL_mstitem i on i.itemoid=sodtl.itemoid " & _
               "    inner join QL_mstgen cat on cat.genoid=i.itemgroupoid  " & _
               "    inner join QL_mstgen subcat on subcat.genoid=i.itemsubgroupoid " & _
               "    where c.cmpcode='JPT' and upper(so.trnorderstatus) in ('POST','APPROVED') " & _
               "    and convert(char(10),so.trnorderdate,121) between '" & Format(CDate(toDate(sodate1.Text)), "yyyy-MM-dd") & "' " & _
               "    and '" & Format(CDate(toDate(sodate2.Text)), "yyyy-MM-dd") & "' " & sWhere & " " & _
               "    )dt " & _
               "group by custoid,custcode,custgroup,custname,CP,phone1,fax,email,CL_IDR,currencycode,CL_default " & _
               "order by custname"
        'showMessage(sSql, CompnyName & " - Warning", 2, "modalMsgBoxWarn") : Exit Sub
        Dim dt As New DataTable
        dt = ckon.ambiltabel(sSql, "QL_Hist_Cust")
        Session("TblDtl") = dt
        ckon.xkoneksi.Close()

        sSql2 = "select custoid,custcode,custgroup,custname,CP,phone1,fax,email,currencycode, " & _
                "SUM(invoice_IDR) invoice_IDR,sum(invoice_Default) invoice_Default  " & _
                "from ( " & _
                "	select distinct c.custoid,custcode,custgroup,custname,case when ISNULL(contactperson1,'')='' then ''  " & _
                "	else g.gendesc+' '+ contactperson1 end CP,phone1,ISNULL(custfax1,'') fax,ISNULL(custemail,'') email,  " & _
                "	cr.currencycode,(jualmst.trnamtjualnetto*jualmst.currencyrate) invoice_IDR,  " & _
                "	(jualmst.trnamtjualnetto) invoice_Default  " & _
                "	from QL_mstcust c  " & _
                "	inner join QL_mstgen g on g.genoid=c.prefixcp1  " & _
                "	inner join QL_conar ar on ar.custoid=c.custoid  " & _
                "	inner join QL_trnjualmst jualmst on jualmst.trnjualmstoid=ar.refoid  " & _
                "	inner join QL_mstcurr cr on cr.currencyoid=jualmst.currencyoid  " & _
                "	inner join QL_trnjualdtl jualdtl on jualdtl.trnjualmstoid=jualmst.trnjualmstoid  " & _
                "	inner join ql_trnsjjualdtl sjdtl on sjdtl.trnsjjualdtloid=jualdtl.trnsjjualdtloid " & _
                "	inner join QL_trnorderdtl sodtl on sjdtl.trnorderdtloid=sodtl.trnorderdtloid  " & _
                "	inner join QL_trnordermst so on sodtl.trnordermstoid=so.ordermstoid  " & _
                "	inner join QL_mstitem i on i.itemoid=sodtl.itemoid  " & _
                "	inner join QL_mstgen cat on cat.genoid=i.itemgroupoid   " & _
                "	inner join QL_mstgen subcat on subcat.genoid=i.itemsubgroupoid " & _
                "	where c.cmpcode='JPT' and upper(so.trnorderstatus) in ('POST','APPROVED') " & _
                "	and convert(char(10),so.trnorderdate,121) between '" & Format(CDate(toDate(sodate1.Text)), "yyyy-MM-dd") & "' " & _
               "    and '" & Format(CDate(toDate(sodate2.Text)), "yyyy-MM-dd") & "' " & sWhere & " " & _
                ") dt  " & _
                "group by custoid,custcode,custgroup,custname,CP,phone1,fax,email,currencycode"
        Dim routeTbl As New DataTable '= cKoneksi.ambiltabel(sSql, "ql_trnspkroutedtl")

        Dim ss As New SqlDataAdapter(sSql2, conn)
        ss.Fill(routeTbl)
        Session("TblDtl_Invoice") = routeTbl


        Dim oTable As DataTable = Session("TblDtl")
        Dim oTable_Invoice As DataTable = Session("TblDtl_Invoice")



        If oTable.Rows.Count = 0 Then
            Dim objrow As DataRow = oTable.NewRow
            objrow("filter1") = "Periode"
            objrow("filterdate1") = sodate1.Text & " s/d " & sodate2.Text
            objrow("filter2") = IIf(cb1.Checked = True, option1.SelectedValue & ddlOption1.SelectedItem.Text, "Periode 2")
            objrow("filterDate2") = IIf(cb1.Checked = True, optionDate1.Text & " s/d " & optionDate2.Text, "ALL")
            objrow("filterCustomer") = IIf(custoid.Text.Trim <> "", Tchar(custName.Text), "ALL")
            objrow("avgOrder_IDR") = 0
            objrow("avgOrder_Default") = 0
            objrow("reportName") = "rptDataCustomer.rpt" & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", "")
            oTable.Rows.Add(objrow)
        Else
            For c As Int64 = 0 To oTable.Rows.Count - 1
                Dim dr As DataRow = oTable.Rows(c)
                dr.BeginEdit()
                dr("filter1") = "Periode"
                dr("filterdate1") = sodate1.Text & " s/d " & sodate2.Text
                dr("filter2") = IIf(cb1.Checked = True, option1.SelectedValue & ddlOption1.SelectedItem.Text, "Periode 2")
                dr("filterDate2") = IIf(cb1.Checked = True, optionDate1.Text & " s/d " & optionDate2.Text, "ALL")
                dr("filterCustomer") = IIf(custoid.Text.Trim <> "", Tchar(custName.Text), "ALL")
                dr("reportName") = "rptDataCustomer.rpt" & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", "")

                For c2 As Int64 = 0 To oTable_Invoice.Rows.Count - 1
                    If oTable_Invoice.Rows(c2).Item("custoid") = oTable.Rows(c).Item("custoid") And _
                    oTable_Invoice.Rows(c2).Item("currencycode") = oTable.Rows(c).Item("currencycode") Then
                        dr("avgOrder_IDR") = ToDouble(oTable_Invoice.Rows(c2).Item("invoice_IDR").ToString) / ToDouble(rangeBulan)
                        dr("avgOrder_Default") = ToDouble(oTable_Invoice.Rows(c2).Item("invoice_Default").ToString) / ToDouble(rangeBulan)
                    End If
                Next
                dr.EndEdit()
            Next
        End If
        Session("TblDtl") = oTable

        vReport.Load(Server.MapPath("~\Report\rptDataCustomer.rpt"))
        vReport.SetDataSource(Session("TblDtl"))



        Dim crConnInfo As New ConnectionInfo
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, vReport)
        If ekstensi = "EXCEL" Then
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
            vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
        ElseIf ekstensi = "PDF" Then
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
            vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
        End If
    End Sub
#End Region

#Region "Function"
    Function sWhere()
        Dim where As String = ""
        Return where
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    ' Server.Transfer("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("rptCreditLimit.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Report Data Customer"

        If Not Page.IsPostBack Then '
            initAllDDL()
            Session("showReport") = False
            sodate1.Text = "01/" & Date.Now.Month & "/" & Date.Now.Year & ""
            sodate2.Text = Format(Date.Now, "dd/MM/yyyy")
            optionDate1.Text = "01/" & Date.Now.Month & "/" & Date.Now.Year & ""
            optionDate2.Text = Format(Date.Now, "dd/MM/yyyy")
        End If
        If Session("showReport") = True Then
        End If
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToPDF.Click
        showPrint("PDF")
    End Sub

    Protected Sub imbExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbExport.Click
        showPrint("EXCEL")
    End Sub
    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("rptCreditLimit.aspx")
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub
    Protected Sub ibSearchcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataCustomer()
        gvCust.SelectedIndex = -1
        TDCustomer.Visible = True
    End Sub
    Protected Sub ibDelCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custName.Text = ""
        custoid.Text = ""
        CProc.DisposeGridView(gvCust)
        TDCustomer.Visible = False
    End Sub
    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custName.Text = gvCust.SelectedDataKey("custname")
        custoid.Text = gvCust.SelectedDataKey("custoid")
        TDCustomer.Visible = False
    End Sub
    Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCust.PageIndex = e.NewPageIndex
        gvCust.Visible = True
        bindDataCustomer()
    End Sub
    Protected Sub ibSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataItem()
        'tdMaterial.Visible = True
    End Sub
    Protected Sub ibClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'itemoid.Text = ""
        'item.Text = ""
        'CProc.DisposeGridView(gvItem)
        'tdMaterial.Visible = False
    End Sub
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'gvItem.PageIndex = e.NewPageIndex
        'tdMaterial.Visible = True
        bindDataItem()
    End Sub
    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'itemoid.Text = gvItem.SelectedDataKey("itemoid")
        'item.Text = gvItem.SelectedDataKey("itemdesc")
        'TDMaterial.Visible = False
    End Sub
    Protected Sub itemcat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initGroupItem()
        initSubGroupItem()
    End Sub
    Protected Sub itemsubcat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initGroupItem()
        initSubGroupItem()
    End Sub
    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah di send for approval !" Then
            Response.Redirect("~\Transaction\trnCollection.aspx?awal=true")
        End If
    End Sub
#End Region
End Class
