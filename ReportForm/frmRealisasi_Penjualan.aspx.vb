'Create by Widi on 24 oktober 2013
'Update by HJ on 31 Mei 2014
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports ClassFunction
Partial Class ReportForm_frmRealisasi_Penjualan
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim sSql As String
    Dim CKon As New Koneksi
#End Region

#Region "Procedure"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Sub initAllDDL()
        Dim sSql As String = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'"
        FillDDL(ddlcabang, sSql)
        ddlcabang.Items.Add(New ListItem("ALL", "ALL"))
        If Session("branch_id") <> "01" Then
            ddlcabang.SelectedValue = Session("branch_id")
        Else
            ddlcabang.SelectedIndex = ddlcabang.Items.Count - 1
        End If

        initGroup()
        initSubGroup()
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = 'MSC' AND PERSONOID IN (SELECT spgoid FROM QL_trnordermst od WHERE PERSONOID=od.spgoid AND od.branch_code='" & Session("branch_id") & "')"
        FillDDL(SalesPerson, sSql)
        SalesPerson.Items.Add(New ListItem("ALL SALES", "ALL SALES"))
        SalesPerson.SelectedValue = "ALL SALES"
    End Sub

    Public Sub initGroup()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("ALL GROUP", "ALL GROUP"))
        FilterDDLGrup.SelectedValue = "ALL GROUP"
    End Sub

    Public Sub initSubGroup()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GROUP", "ALL SUB GROUP"))
        FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP"
    End Sub

    Sub bindDataCustomer()
        Dim sWhereTemp As String = ""
        'sWhereTemp = sWhere()
        sSql = "select custoid,custcode,custname from QL_mstcust where cmpcode='" & CompnyCode & "' and (custcode like '%" & Tchar(custName.Text) & "%' or custname like '%" & Tchar(custName.Text) & "%') " & sWhereTemp & " "
        FillGV(gvCustomer, sSql, "QL_mstcust")
    End Sub

    Sub bindDataMaterial()
        sSql = "select ql_mstitem.itemcode, ql_mstitem.itemdesc, " & _
        "ql_mstitem.itempriceunit1,ql_mstitem.itempriceunit2," & _
        "ql_mstitem.itempriceunit3,ql_mstitem.itemoid, g.gendesc satuan1," & _
        "g2.gendesc satuan2, g3.gendesc satuan3,konversi1_2, " & _
        "konversi2_3,ql_mstitem.merk from ql_mstitem inner join ql_mstgen g on g.genoid=satuan1 " & _
        "and itemflag='AKTIF' inner join ql_mstgen g2 on g2.genoid=satuan2  " & _
        "inner join ql_mstgen g3 on g3.genoid=satuan3  " & _
        "where itemdesc like '%" & Tchar(material.Text) & "%' or ql_mstitem.itemcode like '%" & Tchar(material.Text) & "%' or ql_mstitem.merk like '%" & Tchar(material.Text) & "%'"
        FillGV(gvItem, sSql, "ql_mstmat")
    End Sub

    Sub bindDataSO()
        Dim sWhereTemp As String = ""
        sSql = "select pom.ordermstoid, pom.orderno, c.custoid, c.custname,pom.trnorderdate from QL_trnordermst pom inner join QL_mstcust c on c.custoid=pom.trncustoid where pom.trnorderstatus in ('Approved','INVOICED', 'CLOSED') and orderno like '%" & Tchar(orderno.Text.Trim) & "%' order by orderno desc"
        FillGV(gvSO, sSql, "QL_trnordermst")
    End Sub

    Sub bindDataSDO()
        Dim sWhereTemp As String = ""
        sSql = "select sjmst.trnsjjualmstoid ,sjmst.trnsjjualno,sjmst.trnsjjualdate from QL_trnsjjualmst sjmst where sjmst.cmpcode='" & CompnyCode & "' and sjmst.trnsjjualno like '%" & Tchar(trnsjjualno.Text) & "%' and isnull(sjmst.trnsjjualstatus,'') in ('Approved','INVOICED', 'CLOSED')   " & sWhereTemp & " order by trnsjjualno,sjmst.trnsjjualdate  desc"
        FillGV(gvSDO, sSql, "QL_trnsjjualmst")
    End Sub

    Sub bindDataReturn()
        Dim sWhereTemp As String = ""
        sSql = "select trnjualreturmstoid,trnjualreturno,convert(char(10),trnjualdate,103) trnjualdate from QL_trnjualreturmst where cmpcode='" & CompnyCode & "' and trnjualreturno like '%" & Tchar(trnjualreturno.Text) & "%' and trnjualstatus='POST' " & sWhereTemp & " order by trnjualreturno desc"
        FillGV(gvSalesReturn, sSql, "QL_trnjualreturmst")
    End Sub

    Sub showPrint(ByVal ekstensi As String)

        vReport = New ReportDocument
        If ddlType.SelectedValue = "Summary" Then
            vReport.Load(Server.MapPath("~\Report\rptRealisasi_Penjualan_Summary.rpt"))
        ElseIf ddlType.SelectedValue = "Detail" Then
            vReport.Load(Server.MapPath("~\Report\rptRealisasi_Penjualan_Detail.rpt"))
        ElseIf ddlType.SelectedValue = "ALL" Then
            If ekstensi = "EXCEL" Then
                vReport.Load(Server.MapPath("~\Report\rptRealisasi_Penjualan_DetailWoGroupExcel.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptRealisasi_Penjualan_DetailWoGroup.rpt"))
            End If
        End If

        Try
            Dim dat1 As Date = CDate(toDate(podate1.Text))
            Dim dat2 As Date = CDate(toDate(podate2.Text))

            If ddlTanggal.SelectedValue <> "NOT" Then
                Dim dat3 As Date = CDate(toDate(POdate_2_1.Text))
                Dim dat4 As Date = CDate(toDate(POdate_2_2.Text))
            End If

        Catch ex As Exception
            showMessage("Format tanggal periode salah", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End Try
        If CDate(toDate(podate1.Text)) > CDate(toDate(podate2.Text)) Then
            showMessage("Tanggal Awal tidak boleh lebih besar dari tanggal akhir !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If
        If ddlTanggal.SelectedValue <> "NOT" Then
            If CDate(toDate(POdate_2_1.Text)) > CDate(toDate(POdate_2_2.Text)) Then
                showMessage("Tanggal Awal Filter 2 tidak boleh lebih besar dari tanggal akhir Filter 2 !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End If
        End If

        Try
            Dim sWhere As String = ""
            Dim sWhereInv As String = ""
            Dim sWhereMerk As String = ""
            Dim d1 As String = ""
            Dim d2 As String = ""

            If ddlTanggal.SelectedValue <> "NOT" Then
                sWhere = "  and ( convert(char(10)," & ddlOption1.SelectedValue & ",121) between '" & Format(CDate(toDate(podate1.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(podate2.Text)), "yyyy-MM-dd") & "' "


                sWhere &= " " & ddlTanggal.SelectedValue & " convert(char(10)," & filterdate2.SelectedValue & ",121) between '" & Format(CDate(toDate(POdate_2_1.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(POdate_2_2.Text)), "yyyy-MM-dd") & "' ) "
            Else
                sWhere = " and convert(char(10)," & ddlOption1.SelectedValue & ",121) between '" & Format(CDate(toDate(podate1.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(podate2.Text)), "yyyy-MM-dd") & "' "

            End If

            sWhere &= IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", "  ", "  and  m.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
            sWhere &= IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", "  ", "  and  m.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")
            sWhere &= IIf(SalesPerson.SelectedValue = "ALL SALES", " ", " and pomst.salesoid = " & SalesPerson.SelectedValue & " ")

            If custoid.Text.Trim <> "" Then
                sWhere &= " and s.custoid=" & custoid.Text & ""
            End If
            If ddlTax.SelectedValue <> "ALL" Then
                If ddlTax.SelectedValue = "Yes" Then
                    sWhere &= " and pomst.trntaxpct>0"
                Else
                    sWhere &= " and pomst.trntaxpct=0"
                End If
            End If

            If Session("branch_id") = "01" Then
                sWhere &= IIf(ddlcabang.SelectedValue = "ALL", " ", " and pomst.branch_code= '" & ddlcabang.SelectedValue & "'")
            Else
                sWhere &= "and pomst.branch_code='" & Session("branch_id") & "'"
            End If


            If matoid.Text.Trim <> "" Then
                sWhere &= " and m.itemoid=" & matoid.Text & ""
            Else
                If material.Text.Trim <> "" Then
                    sWhere &= " and m.itemdesc like '%" & Tchar(material.Text) & "%'"
                End If
            End If
            If ordermstoid.Text.Trim <> "" Then
                sWhere &= " and pomst.ordermstoid=" & ordermstoid.Text & ""
            End If
            If trnsjjualmstoid.Text.Trim <> "" Then
                sWhere &= " and sjmst.trnsjjualmstoid=" & trnsjjualmstoid.Text & ""
            End If
            If postatus.SelectedItem.Text <> "ALL" Then
                sWhere &= " and pomst.trnorderstatus='" & postatus.SelectedValue & "'"
            End If
            If ddlSDOStatus.SelectedItem.Text <> "ALL" Then
                sWhere &= " and sjmst.trnsjjualstatus='" & ddlSDOStatus.SelectedValue & "'"
            End If
            If ddlType.SelectedValue = "Detail" Or ddlType.SelectedValue = "ALL" Then
                If trnjualreturmstoid.Text <> "" Then
                    sWhere &= " and retmst.trnjualreturmstoid=" & trnjualreturmstoid.Text & ""
                End If
                If Merk.Text.Trim <> "" Then
                    sWhereMerk = " where dt.Merk like '%" & Tchar(Merk.Text) & "%' "
                End If
                vReport.SetParameterValue("pretno", IIf(Tchar(trnjualreturno.Text) = "", "ALL", Tchar(trnjualreturno.Text)))
                vReport.SetParameterValue("sWhereMerk", sWhereMerk)
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)

            vReport.SetParameterValue("cmpcode", CompnyCode)
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("material", IIf(Tchar(material.Text.Trim) = "", "ALL", material.Text))
            vReport.SetParameterValue("lpbno", IIf(Tchar(trnsjjualno.Text.Trim) = "", "ALL", trnsjjualno.Text))
            vReport.SetParameterValue("postatus", postatus.SelectedItem.Text)
            If ddlTanggal.SelectedValue <> "NOT" Then
                vReport.SetParameterValue("poDate1", podate1.Text & " s.d. " & podate1.Text & " ")
                vReport.SetParameterValue("poDate2", " " & ddlTanggal.SelectedValue & " ) - " & filterdate2.SelectedItem.Text & ": (" & podate2.Text & " s.d. " & podate1.Text & " ")
            Else
                vReport.SetParameterValue("poDate1", podate1.Text)
                vReport.SetParameterValue("poDate2", podate2.Text)
            End If
            vReport.SetParameterValue("pono", IIf(Tchar(orderno.Text.Trim) = "", "ALL", orderno.Text))
            vReport.SetParameterValue("taxable", ddlTax.SelectedValue)
            vReport.SetParameterValue("filter1", ddlOption1.SelectedItem.Text)
            vReport.SetParameterValue("matcat", FilterDDLGrup.SelectedItem.Text)
            vReport.SetParameterValue("matsubcat", FilterDDLSubGrup.SelectedItem.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If ekstensi = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
            ElseIf ekstensi = "PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
            Else
                crv_penjualan.DisplayGroupTree = False
                crv_penjualan.ReportSource = vReport
            End If
        Catch ex As Exception
            ' showMessage(ex.ToString, CompnyName & " - Error")
        End Try
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    ' Server.Transfer("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim cmpcode As String = Session("CompnyCode")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("frmRealisasi_Penjualan.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Report Realisasi Penjualan"

        If kacabCek("ReportForm/frmRealisasi_Penjualan.aspx", Session("UserID")) = True Or Session("UserID") = "POPY" Then
            imbExport.Visible = True : ToPDF.Visible = True
        Else
            If Session("branch_id") <> "01" Then
                imbExport.Visible = False : ToPDF.Visible = False
            Else
                imbExport.Visible = True : ToPDF.Visible = True

            End If
        End If
        If Not Page.IsPostBack Then '
            initAllDDL()
            Session("showReport") = False
            podate1.Text = "01/" & Date.Now.Month & "/" & Date.Now.Year & ""
            podate2.Text = Format(Date.Now, "dd/MM/yyyy")

            POdate_2_1.Text = "01/" & Date.Now.Month & "/" & Date.Now.Year & ""
            POdate_2_2.Text = Format(Date.Now, "dd/MM/yyyy")
        End If
        If Session("showReport") = "True" Then
            showPrint("")
        End If
    End Sub

    Protected Sub ibSearchLPB_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchLPB.Click
        If trnsjjualno.Text.Trim = "" Then
            'showMessage("Please fill LPB No first for filter !", CompnyName & " - Warning") : Exit Sub
        End If
        tdLPB.Visible = True
        bindDataSDO()
    End Sub

    Protected Sub ibSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchMat.Click
        If material.Text.Trim = "" Then
            'showMessage("Please fill Material Code/Material Name first for filter !", CompnyName & " - Warning") : Exit Sub
        End If
        tdMaterial.Visible = True
        bindDataMaterial()
    End Sub

    Protected Sub ibSearchPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchPO.Click
        If orderno.Text.Trim = "" Then
            'showMessage("Please fill PO No first for filter !", CompnyName & " - Warning") : Exit Sub
        End If
        tdPO.Visible = True
        bindDataSO()
    End Sub

    Protected Sub ibClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClearMat.Click
        material.Text = ""
        matoid.Text = ""
        tdMaterial.Visible = False
    End Sub

    Protected Sub ibDelLPB_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelLPB.Click
        trnsjjualno.Text = ""
        trnsjjualmstoid.Text = ""
        tdLPB.Visible = False
    End Sub

    Protected Sub ibDelPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelPO.Click
        orderno.Text = ""
        ordermstoid.Text = ""
        tdPO.Visible = False
    End Sub

    Protected Sub gvSDO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSDO.PageIndexChanging
        gvSDO.PageIndex = e.NewPageIndex
        tdLPB.Visible = True
        bindDataSDO()
    End Sub

    Protected Sub gvSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSO.PageIndexChanging
        gvSO.PageIndex = e.NewPageIndex
        tdPO.Visible = True
        bindDataSO()
    End Sub

    Protected Sub gvSO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub gvSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSO.SelectedIndexChanged
        ordermstoid.Text = gvSO.SelectedDataKey("ordermstoid").ToString
        orderno.Text = gvSO.SelectedDataKey("orderno").ToString
        tdPO.Visible = False
    End Sub

    Protected Sub gvSDO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSDO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub gvSDO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSDO.SelectedIndexChanged
        trnsjjualmstoid.Text = gvSDO.SelectedDataKey("trnsjjualmstoid").ToString
        trnsjjualno.Text = gvSDO.SelectedDataKey("trnsjjualno").ToString
        tdLPB.Visible = False
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToPDF.Click
        showPrint("PDF")
    End Sub

    Protected Sub imbExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbExport.Click
        showPrint("EXCEL")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("frmRealisasi_Penjualan.aspx")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub ibSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchSupp.Click
        TDSupplier.Visible = True
        bindDataCustomer()
    End Sub

    Protected Sub ibDelSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelSupp.Click
        custoid.Text = ""
        custName.Text = ""
        TDSupplier.Visible = False
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCustomer.PageIndexChanging
        gvCustomer.PageIndex = e.NewPageIndex
        TDSupplier.Visible = True
        bindDataCustomer()
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCustomer.SelectedIndexChanged
        custoid.Text = gvCustomer.SelectedDataKey("custoid").ToString
        custName.Text = gvCustomer.SelectedDataKey("custname").ToString
        TDSupplier.Visible = False
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "Summary" Then
            TD1.Visible = False
            TD2.Visible = False
            TD3.Visible = False
            tdPurchaseReturn.Visible = False
            RD1.Visible = False
            RD2.Visible = False
            RD3.Visible = False

        Else
            TD1.Visible = True
            TD2.Visible = True
            TD3.Visible = True
            tdPurchaseReturn.Visible = True
            RD1.Visible = True
            RD2.Visible = True
            RD3.Visible = True

        End If
    End Sub

    Protected Sub ibSearchPurchaseReturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchPurchaseReturn.Click
        If trnjualreturno.Text.Trim = "" Then
            'showMessage("Please fill Purchase Return No first for filter !", CompnyName & " - Warning") : Exit Sub
        End If
        tdPurchaseReturn.Visible = True
        bindDataReturn()
    End Sub

    Protected Sub ibDelPurchaseReturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelPurchaseReturn.Click
        trnjualreturno.Text = ""
        trnjualreturmstoid.Text = ""
        tdPurchaseReturn.Visible = False
    End Sub

    Protected Sub gvSalesReturn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSalesReturn.PageIndexChanging
        gvSalesReturn.PageIndex = e.NewPageIndex
        tdPurchaseReturn.Visible = True
        bindDataReturn()
    End Sub

    Protected Sub gvSalesReturn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSalesReturn.SelectedIndexChanged
        trnjualreturno.Text = gvSalesReturn.SelectedDataKey("trnjualreturno").ToString
        trnjualreturmstoid.Text = gvSalesReturn.SelectedDataKey("trnjualreturmstoid").ToString
        tdPurchaseReturn.Visible = False
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\ReportForm\frmRealisasi_Pembelian.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        tdMaterial.Visible = True
        bindDataMaterial()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        material.Text = gvItem.SelectedDataKey.Item("itemdesc")
        matoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.DataSource = Nothing
        gvItem.DataBind()
        tdMaterial.Visible = False
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        Session("showReport") = "True"
        showPrint("")
    End Sub
#End Region
End Class
