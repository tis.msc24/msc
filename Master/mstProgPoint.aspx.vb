Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports Microsoft.Office.Interop

Partial Class Master_mstProgPoint
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public BranchCode As String = ConfigurationSettings.AppSettings("BranchCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Private statusReport As New ReportDocument
    Private myDiskFileDestinationOptions As DiskFileDestinationOptions
    Private myExportOptions As ExportOptions
    Dim CProc As New ClassProcedure
    Public folderReport As String = "~/report/"
    Dim report As New ReportDocument
    Dim oRegex As Regex
    Dim oMatches As MatchCollection
#End Region

#Region "Functions"
    Public Function returnAvailable(ByVal sAvailable As String) As Boolean
        If sAvailable = "1" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateDetailData() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("tblPoint") Is Nothing Then
            Dim dtTbl As DataTable = Session("tblPoint")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvList.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvList.Rows(C1)
                    dtView.RowFilter = "seq=" & row.Cells(0).Text
                    Dim sPoint As Double
                    dtView(0)("sumpoint") = 0
                    For d1 As Integer = 0 To cbCabang.Items.Count - 1
                        dtView(0)("sumpoint") += ToDouble(GetTextBoxValue(C1, d1 + 4))
                    Next
                    sPoint = dtView(0)("sumpoint")
                    If sPoint > 0 Then
                        dtView(0)("point01") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 4)), 3)
                        dtView(0)("point02") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 5)), 3)
                        dtView(0)("point03") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 6)), 3)
                        dtView(0)("point04") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 7)), 3)
                        dtView(0)("point05") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 8)), 3)
                        dtView(0)("point06") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 9)), 3)
                        dtView(0)("point07") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 10)), 3)
                        dtView(0)("point08") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 11)), 3)
                        dtView(0)("point09") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 12)), 3)
                        dtView(0)("point10") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 13)), 3)
                        dtView(0)("point11") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 14)), 3)
                        'dtView(0)("point12") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 15)), 3)
                        dtView(0)("point13") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 15)), 3)
                        dtView(0)("point14") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 16)), 3)
                        dtView(0)("point15") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 17)), 3)
                        dtView(0)("point16") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 18)), 3)
                        dtView(0)("point17") = ToMaskEdit(ToDouble(GetTextBoxValue(C1, 19)), 3)
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl = dtView.ToTable
                Session("tblPoint") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvList.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function setTableDetailPoint() As DataTable
        If Session("tbldtlPoint") Is Nothing Then
            'Dim tblDetail As DataTable
            'tblDetail = New DataTable("QL_MstPromoDtl")
            Dim tblDetail As New DataTable
            tblDetail.Columns.Add("itemoid", Type.GetType("System.Int32")) '1
            tblDetail.Columns.Add("itemdesc", Type.GetType("System.String")) '2
            tblDetail.Columns.Add("itemcode", Type.GetType("System.String")) '3
            tblDetail.Columns.Add("point01", Type.GetType("System.Decimal")) '4
            tblDetail.Columns.Add("point02", Type.GetType("System.Decimal")) '5
            tblDetail.Columns.Add("point03", Type.GetType("System.Decimal")) '8
            tblDetail.Columns.Add("point04", Type.GetType("System.Decimal")) '9
            tblDetail.Columns.Add("point05", Type.GetType("System.Decimal")) '10
            tblDetail.Columns.Add("point06", Type.GetType("System.Decimal")) '11
            tblDetail.Columns.Add("point07", Type.GetType("System.Decimal")) '12
            tblDetail.Columns.Add("point08", Type.GetType("System.Decimal")) '13
            tblDetail.Columns.Add("point09", Type.GetType("System.Decimal")) '14
            tblDetail.Columns.Add("point10", Type.GetType("System.Decimal")) '15
            tblDetail.Columns.Add("point11", Type.GetType("System.Decimal")) '16
            'tblDetail.Columns.Add("point12", Type.GetType("System.Decimal")) '17
            tblDetail.Columns.Add("point13", Type.GetType("System.Decimal")) '18
            tblDetail.Columns.Add("point14", Type.GetType("System.Decimal")) '19
            tblDetail.Columns.Add("point15", Type.GetType("System.Decimal")) '20
            tblDetail.Columns.Add("point16", Type.GetType("System.Decimal")) '21
            tblDetail.Columns.Add("point17", Type.GetType("System.Decimal")) '21
            tblDetail.Columns.Add("seq", Type.GetType("System.Int32")) '22
            tblDetail.Columns.Add("sumpoint", Type.GetType("System.Decimal")) '23
            Return tblDetail
        End If
    End Function

    Private Function setTableDetailReward() As DataTable
        If Session("tbldtlRwd") Is Nothing Then
            'Dim tblDetail As DataTable
            'tblDetail = New DataTable("QL_MstPromoDtl")
            Dim tblDetail As New DataTable
            tblDetail.Columns.Add("itemoid", Type.GetType("System.Int32")) '1
            tblDetail.Columns.Add("itemdesc", Type.GetType("System.String")) '2
            tblDetail.Columns.Add("itemcode", Type.GetType("System.String")) '3
            tblDetail.Columns.Add("qty", Type.GetType("System.Int32")) '4
            tblDetail.Columns.Add("amount", Type.GetType("System.Int32")) '5
            tblDetail.Columns.Add("point", Type.GetType("System.Int32")) '6
            tblDetail.Columns.Add("targetamount", Type.GetType("System.Int32")) '7
            tblDetail.Columns.Add("typebarang", Type.GetType("System.String")) '8
            tblDetail.Columns.Add("seq", Type.GetType("System.Int32")) '9
            Return tblDetail
        End If
    End Function

    Private Function listPromoDtl(ByVal itemoid As Integer, ByVal itemdesc1 As String, ByVal amountdisc1 As Double, ByVal itemoid2 As Integer, ByVal itemdesc2 As String, ByVal MenuSdhAda As String, ByVal subsidi As Double) As String

        Try
            Dim dv As DataView
            Dim dv2 As DataView

            Dim tbldtlPoint As DataTable = Session("tbldtlPoint")
            dv = tbldtlPoint.DefaultView
            dv.RowFilter = "itemoid=" & CStr(itemoid) & " or itemoid2= " & CStr(itemoid)

            If dv.Count > 0 Then
                '    showMessage("This item has been inputed!!", "INFORMATION", 2)
                '    Exit Sub
                MenuSdhAda += " " & itemdesc1 & ","
            End If
            dv.RowFilter = ""

            dv2 = tbldtlPoint.DefaultView
            dv2.RowFilter = "itemoid=" & CStr(itemoid2) & " or itemoid2= " & CStr(itemoid2)

            If itemoid2 <> 0 Then   'abaikan jika itemke2 tidak dipilih          
                If dv2.Count > 0 Then
                    MenuSdhAda += " " & itemdesc2 & ","
                End If
            End If

            dv2.RowFilter = ""

            If MenuSdhAda = "" Then
                Dim dr As DataRow = tbldtlPoint.NewRow
                dr(0) = 0
                dr(1) = itemoid
                dr(3) = amountdisc1
                dr(4) = itemoid2
                dr(8) = subsidi
                tbldtlPoint.Rows.Add(dr)
            End If
            dv.RowFilter = ""
            dv2.RowFilter = ""
            Session("tbldtlPoint") = tbldtlPoint
        Catch ex As Exception
            showMessage(ex.ToString, "ERROR")
        End Try
        Return (MenuSdhAda)
    End Function
#End Region

#Region "Procedure"
    Private Sub viewDetailPromo(ByVal oidpro As Integer)
        If Not (Session("tbldtlPoint")) Is Nothing Then
            sSql = "SELECT pd.promoid, pd.promdtloid, i.itemoid, i.itemdesc, qtyitemoid1 qty1, 0 qty2, priceitem, subsidi1 subsidi, amountdisc, typebarang, seq, 0 itemoid2, '' itemdesc2 FROM QL_mstpromodtl pd INNER JOIN ql_mstitem i ON i.itemoid=pd.itemoid Where pd.promoid=" & oidpro & ""
            Dim dtv As DataTable = cKoneksi.ambiltabel(sSql, "ql_promodtl")
            gvPointDtl.Visible = True
            gvPointDtl.DataSource = dtv
            Session("tbldtlPoint") = dtv
            gvPointDtl.Visible = True
            gvPointDtl.DataBind()
        Else
            gvPointDtl.Visible = False
        End If
        ' btnRemoveRelative.Visible = CType(Session("tblRELATIVE"), DataTable).Rows.Count > 0
    End Sub

    Private Sub BindDataPersonPopup(ByVal sqltext As String)
        sSql = "select * from(SELECT p.personnip nip, p.personname, (SELECT GENDESC FROM QL_MSTGEN WHERE GENOID = P.PERSONSTATUS  ) divisi, personoid FROM QL_mstperson p where  p.cmpcode='" & CompnyCode & "' and p.PERSONNIP  not in (select userid  from QL_mstprof) " & sqltext & " ) t where divisi like '%AMP%' ORDER BY t.divisi, personname"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "MstPerson")
        GVPerson.Visible = True
        GVPerson.DataSource = xreader
        GVPerson.DataBind()
        GVPerson.SelectedIndex = -1
    End Sub

    Private Sub HitungAmt()
        Dim TitsNya As Double = 0.0
        Dim bRa As DataTable = Session("tbldtlPoint")
        Try
            For c1 As Integer = 0 To bRa.Rows.Count - 1
                TitsNya += bRa.Rows(c1)("totalamt")
            Next
        Catch ex As Exception : End Try
        totKotor.Text = ToMaskEdit(ToDouble(TitsNya), 4)
    End Sub

    Private Sub InitCBL()
        sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' and gengroup='CABANG' ORDER BY gencode ASC"
        cbCabang.Items.Clear()
        'cbCabang.Items.Add("All")
        'cbCabang.Items(cbCabang.Items.Count - 1).Value = "All"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            cbCabang.Items.Add(xreader.GetValue(1).ToString)
            cbCabang.Items(cbCabang.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
        Session("LastAllCheck") = False
    End Sub

    Private Sub FillDetailPrice(ByVal iOid As String)
        sSql = "SELECT genoid," & iOid & " promooid,left(gencode,2) gencode,gendesc,isnull((select itemavailable " & _
               "from QL_mstPromoAvailable Where cmpcode=g.cmpcode and promooid= " & iOid & "),'0') itemavailable " & _
               "FROM QL_mstGen g WHERE gengroup='OUTLET' "
        Dim tbDtl As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstGen")
        If tbDtl.Rows.Count > 0 Then
            Session("OutletDtl") = tbDtl

            Dim objTable2 As DataTable
            objTable2 = Session("PriceDtl")
            objTable2.Rows.Clear() : Dim objRow As DataRow

            For C1 As Integer = 0 To tbDtl.Rows.Count - 1
                objRow = objTable2.NewRow()
                objRow("outletoid") = tbDtl.Rows(C1)("genoid").ToString
                objRow("promooid") = tbDtl.Rows(C1)("promooid").ToString
                objRow("outletcode") = tbDtl.Rows(C1)("gencode").ToString
                objRow("outletdesc") = tbDtl.Rows(C1)("gendesc").ToString
                objRow("itemavailable") = tbDtl.Rows(C1)("itemavailable").ToString
                objTable2.Rows.Add(objRow)
            Next

            Session("PriceDtl") = objTable2
            'gvPriceDtl.DataSource = objTable2
            'gvPriceDtl.DataBind()
        Else
            Session("PriceDtl") = Nothing
            'gvPriceDtl.DataSource = Nothing : gvPriceDtl.DataBind()
            showMessage("Gagal membuka data Price !!", CompnyName & " - ERROR") : Exit Sub
        End If
        UpdateDetail()
    End Sub

    Private Function SetTableDetailPrice(ByVal sName As String) As DataTable
        Dim nuTable As New DataTable(sName)
        nuTable.Columns.Add("outletoid", Type.GetType("System.Int32"))
        nuTable.Columns.Add("promooid", Type.GetType("System.Int32"))
        nuTable.Columns.Add("outletcode", Type.GetType("System.String"))
        nuTable.Columns.Add("outletdesc", Type.GetType("System.String"))
        nuTable.Columns.Add("itemavailable", Type.GetType("System.String"))
        Return nuTable
    End Function

    Private Sub UpdateDetail()
        If Not Session("PriceDtl") Is Nothing Then
            Dim iError As Int16 = 0
            Dim objTable As DataTable
            objTable = Session("PriceDtl")
            Try
                Session("PriceDtl") = objTable
            Catch ex As Exception
                showMessage(ex.Message, CompnyName & " - ERROR ")
            End Try
        End If
    End Sub

    Private Sub initDDLAll()
        sSql = "SELECT genoid,gendesc FROM QL_mstgen where gengroup='itemgroup'"
        FillDDL(ddlGroup, sSql)
        FillDDL(ddlSubGroup, "SELECT genoid,gendesc FROM QL_mstgen where gengroup='itemsubgroup'")
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub ClearDtlInput()
        itemdesc.Text = "" : itemoid.Text = ""
        point.Text = "" : qty.Text = ""
        amount.Text = "" : targetamount.Text = ""
    End Sub

    Private Sub ClearMstInput()
        promoid.Text = ""
        promeventcode.Text = ""
        promeventname.Text = ""
        promstartdate.Text = ""
        promfinishdate.Text = ""
        promstarttime.Text = ""
        promfinishtime.Text = ""
        promtype.SelectedIndex = 0
        membercardrole.SelectedIndex = 0
        note.Text = ""

    End Sub

    Private Sub binddataMenu()
        sSql = "SELECT top 1000 itemoid, itemdesc itemshortdesc,g.gendesc,merk,itempriceunit1 pricelist FROM QL_mstItem i inner join ql_mstgen g ON g.cmpcode=i.cmpcode AND g.genoid=i.itemgroupoid Where i.cmpcode='" & CompnyCode & "' and itemdesc like '%" & Tchar(filterMenuItem.Text) & "%'"
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        If objDs.Tables("data").Rows.Count > 0 Then
            gvMenu.Visible = True
            gvMenu.DataSource = objDs.Tables("data")
            Session("dataMenu") = objDs.Tables("data")
            gvMenu.DataBind()
            'lblmsg.Visible = False
        Else
            gvMenu.Visible = True
            gvMenu.DataSource = objDs.Tables("data")
            Session("dataMenu") = objDs.Tables("data")
            gvMenu.DataBind()
            'lblmsg.Visible = True
        End If

    End Sub

    Private Sub insertingDtlPromo(ByVal promoid As Integer, ByVal OidTemp As Integer)
        Dim objTable As DataTable
        objTable = Session("tbldtlPoint")
        Dim seq_bc As Integer = 1
        For c1 As Int16 = 0 To objTable.Rows.Count - 1
            For d1 As Integer = 0 To cbCabang.Items.Count - 1
                If cbCabang.Items(d1).Selected = True Then
                    sSql = "INSERT INTO [QL_mstprogpointDtl]([cmpcode], [progpointdtloid], [progpointmstoid], [seq], [seq_branch_code], [branch_code], [itemoid], [point], [upduser], [updtime])" & _
                                        "VALUES('" & CompnyCode & "', " & OidTemp & ", " & promoid & ", " & Tnumber(objTable.Rows(c1).Item("seq").ToString) & ", " & seq_bc & ", '" & cbCabang.Items(d1).Value & "', " & Tnumber(objTable.Rows(c1).Item("itemoid").ToString) & ", " & ToDouble(objTable.Rows(c1).Item("point" & cbCabang.Items(d1).Value & "")) & ", '" & Session("UserID") & "', current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    OidTemp += 1 : seq_bc += 1
                End If
            Next
            seq_bc = 1
        Next
        sSql = "update QL_mstoid set lastoid=" & OidTemp - 1 & " where tablename = 'QL_mstprogpointDtl' and cmpcode = '" & CompnyCode & "' "
        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
    End Sub

    Private Sub insertingDtlPromo2(ByVal promoid As Integer, ByVal OidTemp As Integer)
        Dim objTable As DataTable
        objTable = Session("tbldtlRwd")
        If (Session("tbldtlRwd")) IsNot Nothing Then
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT INTO [QL_mstprogpointDtl2]([cmpcode], [progpointdtl2oid], [progpointmstoid], [seq], [branch_code], [itemoid], [point], [upduser], [updtime], [qty], [amount], [targetamount], itemdesc, typebarang)" & _
                                    "VALUES('" & CompnyCode & "', " & OidTemp & ", " & promoid & ", " & Tnumber(objTable.Rows(c1).Item("seq")) & ", '', " & Tnumber(objTable.Rows(c1).Item("itemoid")) & ", " & ToDouble(objTable.Rows(c1).Item("point")) & ", '" & Session("UserID") & "', current_timestamp, " & ToDouble(objTable.Rows(c1).Item("qty")) & ", " & ToDouble(objTable.Rows(c1).Item("amount")) & ", " & ToDouble(objTable.Rows(c1).Item("targetamount")) & ", '" & objTable.Rows(c1).Item("itemdesc").ToString & "', '" & objTable.Rows(c1).Item("typebarang").ToString & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                OidTemp += 1
            Next
            sSql = "update QL_mstoid set lastoid=" & OidTemp - 1 & " where tablename = 'QL_mstprogpointDtl2' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
        End If
    End Sub

    Private Sub BindData(ByVal sSqlPlus As String)
        sSql = "SELECT [progpointmstoid], [progpointcode], [progpointdesc], convert(varchar(10),[progpointstartdate],101) progpointstartdate, convert(varchar(10),[progpointfinishdate],101) progpointfinishdate,[progpointtype], progpointflag " & _
            "FROM QL_MstProgpoint  " & _
            "WHERE cmpcode='" & CompnyCode & "' " & sSqlPlus & " ORDER BY progpointmstoid DESC" 'AND m.memberstatus NOT IN ('AKTIF', 'NON AKTIF') ORDER BY m.name"
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "ql_mstprogpoint")
        Session("tbldata") = objTable
        GVMst.DataSource = objTable
        GVMst.DataBind()
    End Sub

    Private Sub FillTextBox(ByVal vSearchKey As String)
        Dim personid As String = ""
        sSql = "SELECT [cmpcode], [progpointmstoid], [progpointcode], [progpointdesc], [progpointstartdate], [progpointfinishdate], CONVERT(time, progpointstartdate) AS [progpointstarttime], CONVERT(time, progpointfinishdate) AS [progpointfinsihtime], [progpointtype], [progpointnote], [crtuser], [upduser], [updtime], branch_code, opsipiutanglunas, opsipiutangontime, progpointflag FROM [QL_mstprogpoint] Where progpointmstoid=" & vSearchKey
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xreader = xCmd.ExecuteReader
        BtnDelete.Enabled = True

        While xreader.Read
            promoid.Text = Trim(xreader("progpointmstoid"))
            promeventcode.Text = Trim(xreader("progpointcode"))
            promeventname.Text = Trim(xreader("progpointdesc"))
            promstartdate.Text = Format(CDate(xreader("progpointstartdate")), "dd/MM/yyyy")
            promfinishdate.Text = Format(CDate(xreader("progpointfinishdate")), "dd/MM/yyyy")
            'promstarttime.Text = Trim(xreader.GetValue(6))
            'promfinishtime.Text = Trim(xreader.GetValue(7))
            promtype.SelectedValue = Trim(xreader("progpointtype"))
            'membercardrole.SelectedValue = Trim(xreader.GetValue(9))
            cbPiutangLunas.Checked = Trim(xreader("opsipiutanglunas"))
            cbPiutangOntime.Checked = Trim(xreader("opsipiutangontime"))
            note.Text = Trim(xreader("progpointnote"))
            status.Text = Trim(xreader("progpointflag"))
            Dim sValue() As String = xreader("branch_code").ToString.Split(",")
            If sValue.Length > 0 Then
                If cbCabang.Items.Count > 0 Then
                    For C1 As Integer = 0 To sValue.Length - 1
                        Dim li As ListItem = cbCabang.Items.FindByValue(sValue(C1).Trim)
                        Dim i As Integer = cbCabang.Items.IndexOf(li)
                        If i >= 0 Then
                            cbCabang.Items(cbCabang.Items.IndexOf(li)).Selected = True
                        End If
                    Next
                End If
            End If
            cbCabang.Enabled = False
            cbCheckAll.Enabled = False
            UpdUser.Text = Trim(xreader("upduser"))
            Updtime.Text = Format(CDate(Trim(xreader("updtime").ToString)), "MM/dd/yyyy HH:mm:ss")
        End While
        conn.Close() : xreader.Close()
        sSql = "Select distinct seq, pd.itemoid, i.itemdesc, i.itemcode, 0.00 AS point01, 0.00 AS point02, 0.00 AS point03, 0.00 AS point04, 0.00 AS point05, 0.00 AS point06, 0.00 AS point07, 0.00 AS point08, 0.00 AS point09, 0.00 AS point10, 0.00 AS point11/*, 0.00 AS point12*/, 0.00 AS point13, 0.00 AS point14, 0.00 AS point15, 0.00 AS point16, 0.00 AS point17 from QL_mstprogpointdtl pd INNER JOIN QL_MSTITEM i ON i.itemoid = pd.itemoid where pd.progpointmstoid=" & vSearchKey & ""
        Dim dtv As DataTable = cKoneksi.ambiltabel(sSql, "ql_progpointdtl")
        For C1 As Integer = 0 To dtv.Rows.Count - 1
            For D1 As Integer = 0 To cbCabang.Items.Count - 1
                If cbCabang.Items(D1).Selected = True Then
                    Dim cb As String = cbCabang.Items(D1).Value & ""
                    Dim point As Double = GetStrData("select isnull(point,0.00) from ql_mstprogpointdtl where seq = " & dtv.Rows(C1).Item("seq") & " and branch_code = '" & cbCabang.Items(D1).Value & "'") & ""
                    dtv.Rows(C1).Item("point" & cbCabang.Items(D1).Value & "") = point
                    gvPointDtl.Columns(D1 + 4).Visible = True
                End If
            Next
        Next
        dtv.AcceptChanges()
        gvPointDtl.Visible = True : gvPointDtl.DataSource = dtv
        Session("tbldtlPoint") = dtv : gvPointDtl.Visible = True
        gvPointDtl.DataBind()

        sSql = "Select seq, pd.itemoid, pd.itemdesc, pd.qty, pd.amount, pd.targetamount, i.itemcode, pd.point, pd.typebarang from QL_mstprogpointdtl2 pd LEFT JOIN QL_MSTITEM i ON i.itemoid = pd.itemoid where pd.progpointmstoid = " & vSearchKey & " order by seq"
        Dim dtr As DataTable = cKoneksi.ambiltabel2(sSql, "ql_progpointdtl2")
        gvReward.Visible = True : gvReward.DataSource = dtr
        Session("tbldtlRwd") = dtr : gvReward.Visible = True
        gvReward.DataBind()

        promtype.Enabled = False
        promtype.CssClass = "inpTextDisabled"
        btnGenerate.Visible = True : btnClearDtl.Visible = True
        BtnAddList.Visible = False
        If status.Text.ToUpper = "INACTIVE" Then
            btnSave.Visible = False : BtnDelete.Visible = False
        Else
            btnSave.Visible = True : BtnDelete.Visible = True
        End If
    End Sub

    Public Sub BindDataListItem()
        Try
            sSql = "Select * from (Select 'False' Checkvalue,i.itemcode, i.itemdesc,i.itemoid, g.gendesc satuan3, i.merk,i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya from ql_mstitem i inner join ql_mstgen g on g.genoid=satuan1 and g.gengroup='ITEMUNIT' and itemflag='AKTIF') dt"
            Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstitem")
            Session("TblMat") = dt
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind() : gvItem.Visible = True
            CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

    End Sub

    Private Sub ResetGenerate()
        Session("tblPoint") = Nothing
        gvList.DataSource = Nothing
        gvList.DataBind()
        'BtnAddList.Visible = False
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 15
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xoutletoid As String = Session("outletoid")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstProgPoint.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Master Program Point"
        Session("oid") = Request.QueryString("oid")

        Me.BtnDelete.Attributes.Add("onclick", "return confirm('Are u sure want to delete ??');")

        If Not IsPostBack Then
            Dim sPlus As String = ""
            point.Text = "0.00"
            sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' "
            BindData(sPlus) : initDDLAll() : InitCBL()
            If Session("PriceDtl") Is Nothing Then
                Dim dtlNew As DataTable = SetTableDetailPrice("QL_mstItemPrice")
                Session("PriceDtl") = dtlNew
            End If

            If Session("oid") Is Nothing Or Session("oid") = "" Then
                promoid.Text = GenerateID("QL_mstprogpoint", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
                UpdUser.Text = Session("UserID")
                Updtime.Text = GetServerTime()
                BtnDelete.Enabled = False
                ClearDtlInput()
                'FillDetailPrice("0")
                ddlInsertType.SelectedIndex = 0
                ddlInsertType_SelectedIndexChanged(sender, e)
                promtype_SelectedIndexChanged(sender, e)
                BtnDelete.Visible = False
                status.Text = "Active"
                Dim sNo As String = "PP/G24/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                sSql = "SELECT isnull(max(abs(replace(progpointcode,'" & sNo & "',''))),0)+1 FROM QL_mstprogpoint WHERE progpointcode LIKE '" & sNo & "%' "
                promeventcode.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
            Else
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                'FillDetailPrice(Session("oid"))
                ddlInsertType.SelectedIndex = 0
                ddlInsertType_SelectedIndexChanged(sender, e)
                promtype_SelectedIndexChanged(sender, e)
                ClearDtlInput()
            End If
        End If
    End Sub

    Private Sub CreateDtlTbl()
        Dim TblDtNya As DataTable = New DataTable("QL_Item")
        TblDtNya.Columns.Add("seq", Type.GetType("System.String"))
        TblDtNya.Columns.Add("progpointcode", Type.GetType("System.String"))
        TblDtNya.Columns.Add("progpointdesc", Type.GetType("System.String"))
        TblDtNya.Columns.Add("progpointstartdate", Type.GetType("System.DateTime"))
        TblDtNya.Columns.Add("progpointfinishdate", Type.GetType("System.DateTime"))
        TblDtNya.Columns.Add("progpointtype", Type.GetType("System.String"))
        TblDtNya.Columns.Add("crtuser", Type.GetType("System.String"))
        TblDtNya.Columns.Add("upduser", Type.GetType("System.String"))
        TblDtNya.Columns.Add("updtime", Type.GetType("System.DateTime"))
        TblDtNya.Columns.Add("progpointnote", Type.GetType("System.String"))
        TblDtNya.Columns.Add("progpointflag", Type.GetType("System.String"))
        TblDtNya.Columns.Add("itemoid", Type.GetType("System.Int32")) 'promooid
        TblDtNya.Columns.Add("itemcode", Type.GetType("System.String"))
        TblDtNya.Columns.Add("itemdesc", Type.GetType("System.String"))
        TblDtNya.Columns.Add("point01", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point02", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point03", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point04", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point05", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point06", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point07", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point08", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point09", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point10", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point11", Type.GetType("System.Decimal"))
        'TblDtNya.Columns.Add("point12", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point13", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point14", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point15", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point16", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point17", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("qty", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("amount", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("targetamount", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("point", Type.GetType("System.Decimal"))
        TblDtNya.Columns.Add("typebarang", Type.GetType("System.String"))
        TblDtNya.Columns.Add("GroupNya", Type.GetType("System.String"))
        Session("TabelDtl") = TblDtNya
    End Sub

    Protected Sub ddlInsertType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInsertType.SelectedIndexChanged
        If ddlInsertType.SelectedValue = "GROUP" Then
            tblGroup.Visible = True
            tblSubGroup.Visible = False
            tblMenu.Visible = False
        ElseIf ddlInsertType.SelectedValue = "SUBGROUP" Then
            tblGroup.Visible = False
            tblSubGroup.Visible = True
            tblMenu.Visible = False
        ElseIf ddlInsertType.SelectedValue = "Item" Then
            tblGroup.Visible = False
            tblSubGroup.Visible = False
            tblMenu.Visible = True
        End If
    End Sub

    Protected Sub imbFindMenu1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMenu1.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvItem.DataSource = Nothing : gvItem.DataBind()
        BindDataListItem()
    End Sub

    Protected Sub imbClearMenu1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearMenu1.Click
        itemdesc1.Text = ""
        itemoid.Text = ""
    End Sub

    Protected Sub gvMenu_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMenu.PageIndexChanging
        gvMenu.PageIndex = e.NewPageIndex
        Dim dtDtl As DataTable = Session("dataMenu")
        gvMenu.DataSource = dtDtl : gvMenu.DataBind()
        MpeUser.Show()
    End Sub

    Protected Sub gvMenu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMenu.SelectedIndexChanged
        itemoid.Text = gvMenu.SelectedDataKey.Item(0).ToString
        itemdesc.Text = gvMenu.SelectedDataKey.Item(1).ToString
        PanelUser.Visible = False
        BtnUser.Visible = False
        MpeUser.Hide()
    End Sub

    Protected Sub LBCloseMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseMenu.Click
        PanelUser.Visible = False
        BtnUser.Visible = False
        MpeUser.Hide()
    End Sub

    Protected Sub gvPointdtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPointDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
            e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 3)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 3)
            e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 3)
            e.Row.Cells(14).Text = ToMaskEdit(ToDouble(e.Row.Cells(14).Text), 3)
            e.Row.Cells(15).Text = ToMaskEdit(ToDouble(e.Row.Cells(15).Text), 3)
            e.Row.Cells(16).Text = ToMaskEdit(ToDouble(e.Row.Cells(16).Text), 3)
            e.Row.Cells(17).Text = ToMaskEdit(ToDouble(e.Row.Cells(17).Text), 3)
            e.Row.Cells(18).Text = ToMaskEdit(ToDouble(e.Row.Cells(18).Text), 3)
        End If
    End Sub

    Protected Sub gvPointdtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPointDtl.RowDeleting
        Dim dtab As DataTable
        If Not Session("tbldtlPoint") Is Nothing Then
            dtab = Session("tbldtlPoint")
        Else
            showMessage("Maaf, Ada error detail list session !", 1)
            Exit Sub
        End If

        Dim df As DataView = dtab.DefaultView
        df.RowFilter = "seq=" & e.RowIndex + 1 & ""
        df.RowFilter = ""

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(e.RowIndex + 1) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        gvPointDtl.DataSource = dtab
        gvPointDtl.DataBind()
        Session("tbldtlPoint") = dtab
        cbCheckAll.Checked = False
        If gvPointDtl.Rows.Count = 0 Then
            For C1 As Integer = 0 To cbCabang.Items.Count - 1
                cbCabang.Items(C1).Selected = False
            Next
            btnClearDtl.Visible = False : cbCabang.Enabled = True : cbCheckAll.Enabled = True
            btnGenerate.Visible = True
            promtype.Enabled = True : promtype.CssClass = "inpText"
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Validasi.Text = ""
        Dim s1, s2 As Boolean
        s1 = False : s2 = False
        Dim strcabang As String = ""

        For C1 As Integer = 0 To cbCabang.Items.Count - 1
            If cbCabang.Items(C1).Value <> "All" And cbCabang.Items(C1).Selected = True Then
                strcabang &= cbCabang.Items(C1).Value & ", "
            End If
        Next

        If strcabang <> "" Then
            strcabang = Left(strcabang, strcabang.Length - 2)
        End If

        If promeventcode.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Code""! <br/>"
        End If
        If promeventname.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Program Point Name""! <br/>"
        End If

        If promstartdate.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Start Date""! <br/>"
        Else
            Try
                Dim tgle As Date = CDate(toDate(promstartdate.Text))
                tgle = CDate(toDate(promstartdate.Text))
                s1 = True
            Catch ex As Exception
                Validasi.Text &= "Incorrect format date ""dd/MM/yyyy"" !" & "<br>"
                s1 = False
            End Try
        End If

        If promfinishdate.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""End Date""! <br/>"
        Else
            Try
                Dim tgle As Date = CDate(toDate(promfinishdate.Text))
                tgle = CDate(toDate(promfinishdate.Text))
                s2 = True
            Catch ex As Exception
                Validasi.Text &= "Incorrect format date ""dd/MM/yyyy"" !" & "<br>"
                s2 = False
            End Try
        End If

        'If promstarttime.Text.Trim = "" Then
        '    Validasi.Text &= "Please Fill ""Start Time""! <br/>"
        'Else
        '    Try
        '        Dim TEST As DateTime = CDate("01/01/1900 " & promstarttime.Text)
        '    Catch ex As Exception
        '        Validasi.Text &= "Incorrect format ""Start Time"" ! <br/>"
        '    End Try
        'End If
        'If promfinishtime.Text.Trim = "" Then
        '    Validasi.Text &= "Please Fill ""End Time""! <br/>"
        'Else
        '    Try
        '        Dim TEST As DateTime = CDate("01/01/1900 " & promfinishtime.Text)
        '    Catch ex As Exception
        '        Validasi.Text &= "Incorrect format ""End Time"" ! <br/>"
        '    End Try
        'End If

        If Validasi.Text.Trim = "" Then
            If CDate("01/01/1900 " & promfinishtime.Text) < CDate("01/01/1900 " & promstarttime.Text) Then
                Validasi.Text &= "End Time must >= Start Time ! <br/>"
            End If
        End If

        If s1 And s2 Then
            If CDate(toDate(promstartdate.Text)) > CDate(toDate(promfinishdate.Text)) Then
                Validasi.Text &= "Finish Date must >= Start Date ! <br/>"
            End If
        End If

        If (Session("tbldtlPoint")) Is Nothing Then
            Validasi.Text &= "- Tidak ada data point!!<br/>"
        Else
            Dim objTableCek As DataTable = (Session("tbldtlPoint"))
            If objTableCek.Rows.Count = 0 Then
                Validasi.Text &= "- No Point Data !!<br/>"
            End If
        End If

        'If (Session("tbldtlRwd")) Is Nothing Then
        '    Validasi.Text &= "- No Reward Data !!<br/>"
        'Else
        '    Dim objTableCek As DataTable = (Session("tbldtlRwd"))
        '    If objTableCek.Rows.Count = 0 Then
        '        Validasi.Text &= "- No Reward Data !!<br/>"
        '    End If
        'End If

        If Validasi.Text <> "" Then
            showMessage(Validasi.Text, CompnyName & " - WARNING")
            Exit Sub
        End If

        'For i As Integer = 0 To cbCabang.Items.Count - 1
        '    If cbCabang.Items(i).Selected Then
        '        strcabang = strcabang & cbCabang.Items(i).Value
        '    End If
        'Next

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM QL_mstprogpoint WHERE progpointcode = '" & Tchar(promeventcode.Text) & "' "
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck1 &= " AND progpointmstoid <> " & promoid.Text
        End If
        If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
            showMessage("Code Sudah digunakan, gunakan code lain !!", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            UpdUser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)

            Dim OidTempDtl As Integer = GenerateID("QL_mstprogpointdtl", CompnyCode)
            Dim OidTempDtl2 As Integer = GenerateID("QL_mstprogpointdtl2", CompnyCode)

            If Session("oid") = Nothing Or Session("oid") = "" Then
                promoid.Text = GenerateID("QL_mstprogpoint", CompnyCode)

                sSql = "INSERT INTO [QL_mstprogpoint] ([cmpcode], [progpointmstoid], [progpointcode], [progpointdesc], [progpointtype], [progpointstartdate], [progpointfinishdate], [opsipiutanglunas], [opsipiutangontime], [progpointnote], [progpointres1], [progpointres2], [crtuser], [crttime],[upduser],[updtime], [progpointflag], branch_code) VALUES ('" & CompnyCode & "'," & promoid.Text & ",'" & Tchar(promeventcode.Text) & "','" & Tchar(promeventname.Text) & "','" & Tchar(promtype.SelectedValue) & "','" & CDate(toDate(promstartdate.Text)) & "','" & CDate(toDate(promfinishdate.Text)) & "', '" & IIf(cbPiutangLunas.Checked = True, "True", "False") & "', '" & IIf(cbPiutangOntime.Checked = True, "True", "False") & "', '" & Tchar(note.Text) & "', '', '', '" & Session("UserID") & "',current_timestamp, '" & Tchar(UpdUser.Text) & "', current_timestamp, '" & status.Text & "', '" & strcabang & "')"

            Else
                sSql = "UPDATE [QL_mstprogpoint] " & _
                        "SET [cmpcode]='" & CompnyCode & "', " & _
                        "[progpointdesc]='" & Tchar(promeventname.Text) & "', " & _
                        "[progpointstartdate]='" & CDate(toDate(promstartdate.Text)) & " " & promstarttime.Text & "', " & _
                        "[progpointfinishdate]='" & CDate(toDate(promfinishdate.Text)) & " " & promfinishtime.Text & "', " & _
                        "[progpointtype]='" & Tchar(promtype.SelectedValue) & "', " & _
                        "[progpointnote]='" & Tchar(note.Text) & "', " & _
                        "[opsipiutanglunas]='" & IIf(cbPiutangLunas.Checked = True, "True", "False") & "', " & _
                        "[opsipiutangontime]='" & IIf(cbPiutangOntime.Checked = True, "True", "False") & "', " & _
                        "[upduser]='" & Tchar(UpdUser.Text) & "', " & _
                        "[updtime]=current_timestamp, " & _
                        "[progpointflag]='" & status.Text & "', " & _
                        "[branch_code]='" & strcabang & "' " & _
                        "WHERE progpointmstoid=" & Session("oid")
            End If
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If Not (Session("oid") Is Nothing Or Session("oid") = "") Then
                'Data sudah ada
                sSql = "DELETE FROM QL_mstprogpointDtl WHERE cmpcode = '" & CompnyCode & "' AND progpointmstoid = " & Session("oid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "DELETE FROM QL_mstprogpointDtl2 WHERE cmpcode = '" & CompnyCode & "' AND progpointmstoid = " & Session("oid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                promoid.Text = Session("oid")

            Else
                'Data baru
                sSql = "Update QL_mstoid set lastoid=" & promoid.Text & " where tablename = 'QL_mstprogpoint' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If
            '=========================================
            insertingDtlPromo(promoid.Text, OidTempDtl)
            insertingDtlPromo2(promoid.Text, OidTempDtl2)
            '-----------------------------------------
            objTrans.Commit()
            conn.Close()
            ClearDtlInput()
            ClearMstInput()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR..!!!!!!!!")
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstProgPoint.aspx?awal=true")

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        ClearMstInput()
        ClearDtlInput()
        Response.Redirect("~\Master\mstProgPoint.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_mstprogpoint SET progpointflag='Inactive' WHERE progpointmstoid=" & Session("oid")
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING")
        Finally
            Response.Redirect("~\Master\mstProgPoint.aspx?awal=true")
            BtnDelete.Enabled = False
        End Try

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sPlus As String = ""
        sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If ddlStatus.SelectedValue <> "ALL" Then
            sPlus &= " AND progpointflag = '" & ddlStatus.SelectedValue & "'"
        End If
        BindData(sPlus)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterDDL.SelectedIndex = 0 : FilterText.Text = ""
        ddlStatus.SelectedIndex = 0 : ddlStatus.SelectedValue = "ALL"

        Dim sPlus As String = ""
        'sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        BindData(sPlus)
    End Sub

    Protected Sub promtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If promtype.SelectedValue.ToUpper.Trim = "BUY NX GET NY" Then
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf promtype.SelectedValue.ToUpper.Trim = "BUY X GET Y" Then
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType.Enabled = False
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf promtype.SelectedValue.ToUpper.Trim = "DISC % (ITEM ONLY)" Then
            ddlInsertType.CssClass = "inpText"
            ddlInsertType.Enabled = True
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf promtype.SelectedValue.ToUpper.Trim = "DISC QUANTITY" Then
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf promtype.SelectedValue.ToUpper.Trim = "BUY 1 GET 1" Then
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
        Else
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            ddlInsertType_SelectedIndexChanged(sender, e)
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddataMenu()
        gvMenu.Visible = True
        PanelUser.Visible = True
        BtnUser.Visible = True
        MpeUser.Show()
    End Sub

    Protected Sub btnListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        filterMenuItem.Text = ""
        binddataMenu()
        gvMenu.Visible = True
        PanelUser.Visible = True
        BtnUser.Visible = True
        MpeUser.Show()
    End Sub

    Protected Sub itemavailable_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateDetail()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
    End Sub

    Protected Sub GVMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVMst.PageIndexChanging
        GVMst.PageIndex = e.NewPageIndex
        Dim dtDtl As DataTable = Session("tbldata")
        GVMst.DataSource = dtDtl : GVMst.DataBind()
    End Sub

    Protected Sub GVMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
            e.Row.Cells(5).Text = Format(CDate(e.Row.Cells(5).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnSales_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelPerson.Visible = True
        ButtonPerson.Visible = True
        MPEPerson.Show()
        BindDataPersonPopup("")
    End Sub

    Protected Sub GVPerson_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelPerson.Visible = False
        ButtonPerson.Visible = False
        MPEPerson.Hide()
        Dim no As String = GetStrData("select substring('" & GVPerson.SelectedDataKey(1).ToString().Trim & "',1,5)")
        sSql = "SELECT ISNULL(MAX(CAST(substring(promeventcode,6,3) AS INTEGER))+ 1,1) AS IDNEW FROM ql_mstpromo  WHERE  cmpcode='" & CompnyCode & "' AND promeventcode LIKE '" & no & "%'"
        promeventcode.Text = GenNumberString(no, "", cKoneksi.ambilscalar(sSql), 3)
        promeventcode.Text = promeventcode.Text


        'promeventcode.Text = GVPerson.SelectedDataKey(1).ToString().Trim 'personname
        promeventcode.CssClass = "inpTextDisabled" : promeventcode.ReadOnly = True
        lblamp.Text = GVPerson.SelectedDataKey(2).ToString().Trim 'personoid
        CProc.DisposeGridView(sender)
    End Sub

    Protected Sub LinkButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelPerson.Visible = False
        ButtonPerson.Visible = False
        MPEPerson.Hide()
        CProc.DisposeGridView(GVPerson)
    End Sub

    Protected Sub BtnAddList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAddList.Click
        Try
            Dim sMsg As String = ""
            UpdateDetailData()

            If Session("tbldtlPoint") Is Nothing Then
                Dim dtlTable As DataTable = setTableDetailPoint()
                Session("tbldtlPoint") = dtlTable
            End If

            If Session("tblPoint") Is Nothing Then
                sMsg &= "- Tidak ada data yang ditambahkan.<BR>"
            Else
                Dim dtCek As DataTable = Session("tblPoint")
                If dtCek.Rows.Count < 1 Then
                    sMsg &= "- Tidak ada data yang ditambahkan.<BR>"
                Else

                    Dim dvCek As DataView = dtCek.DefaultView
                    If dvCek.Count < 1 Then
                        sMsg &= "- Maaf, Anda harus mengisi data katalog jika klik tombol add to list..!!<br />"
                    Else
                        Dim dtDtlCek As DataTable = Session("TblDtlPoint")
                        Dim dvDtlCek As DataView = dtDtlCek.DefaultView
                    End If
                End If
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName) : Exit Sub
            End If

            Dim dtCrd As DataTable = Session("tblPoint")
            Dim dvCrd As DataView = dtCrd.DefaultView
            dvCrd.RowFilter = "sumpoint > 0"

            Dim dtDtl As DataTable : dtDtl = Session("TblDtlPoint")
            For R1 As Integer = 0 To dvCrd.Count - 1
                Dim nuRow As DataRow = dtDtl.NewRow
                Dim dtView As DataView = dtDtl.DefaultView
                dtView.RowFilter = "itemoid=" & Integer.Parse(dvCrd(R1)("itemoid"))
                Dim dRowEdit() As DataRow

                If dtView.Count <= 0 Then
                    nuRow = dtDtl.NewRow
                    nuRow("seq") = dtDtl.Rows.Count + 1
                Else
                    dRowEdit = dtDtl.Select("itemoid=" & Integer.Parse(dvCrd(R1)("itemoid")))
                    nuRow = dRowEdit(0)
                    dRowEdit(0).BeginEdit()
                End If

                nuRow("itemoid") = dvCrd(R1)("itemoid").ToString
                nuRow("itemcode") = dvCrd(R1)("itemcode").ToString
                nuRow("itemdesc") = dvCrd(R1)("itemdesc").ToString
                nuRow("point01") = ToDouble(dvCrd(R1)("point01"))
                nuRow("point02") = ToDouble(dvCrd(R1)("point02"))
                nuRow("point03") = ToDouble(dvCrd(R1)("point03"))
                nuRow("point04") = ToDouble(dvCrd(R1)("point04"))
                nuRow("point05") = ToDouble(dvCrd(R1)("point05"))
                nuRow("point06") = ToDouble(dvCrd(R1)("point06"))
                nuRow("point07") = ToDouble(dvCrd(R1)("point07"))
                nuRow("point08") = ToDouble(dvCrd(R1)("point08"))
                nuRow("point09") = ToDouble(dvCrd(R1)("point09"))
                nuRow("point10") = ToDouble(dvCrd(R1)("point10"))
                nuRow("point11") = ToDouble(dvCrd(R1)("point11"))
                'nuRow("point12") = ToDouble(dvCrd(R1)("point12"))
                nuRow("point13") = ToDouble(dvCrd(R1)("point13"))
                nuRow("point14") = ToDouble(dvCrd(R1)("point14"))
                nuRow("point15") = ToDouble(dvCrd(R1)("point15"))
                nuRow("point16") = ToDouble(dvCrd(R1)("point16"))
                nuRow("point17") = ToDouble(dvCrd(R1)("point17"))

                If dtView.Count <= 0 Then
                    dtDtl.Rows.Add(nuRow)
                Else
                    dRowEdit(0).EndEdit()
                    dtDtl.Select(Nothing, Nothing)
                End If
                dtDtl.AcceptChanges()
                nuRow.EndEdit() : seq.Text = dtDtl.Rows.Count + 1
                dtView.RowFilter = ""
            Next
            dvCrd.RowFilter = "" : itemdesc1.Text = ""
            Session("TblDtlPoint") = dtDtl : ResetGenerate()
            gvPointDtl.DataSource = dtDtl : gvPointDtl.DataBind()
            btnClearDtl.Visible = True

        Catch ex As Exception
            showMessage(ex.ToString, CompnyName)
        End Try
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Dim itemcode As String = ""

        Dim countCabang As Integer = 0
        For C1 As Integer = 0 To cbCabang.Items.Count - 1
            If cbCabang.Items(C1).Selected = False Then
                countCabang += 1
            End If
        Next
        If cbCabang.Items.Count = countCabang Then
            showMessage("Pilih cabang terlebih dahulu.", CompnyName) : Exit Sub
        End If

        If cbCabang.Items.Count <> gvList.Columns.Count - 4 Then
            showMessage("Jumlah cabang tidak sama dengan tabel list, silahkan hubungi admin.", CompnyName)
            Exit Sub
        End If

        For C1 As Integer = 0 To cbCabang.Items.Count - 1
            If cbCabang.Items(C1).Value <> "All" And cbCabang.Items(C1).Selected = True Then
                If cbCabang.Items(C1).Text = gvList.Columns(C1 + 4).HeaderText Then
                    gvList.Columns(C1 + 4).Visible = True
                    gvPointDtl.Columns(C1 + 4).Visible = True
                End If
            Else
                gvList.Columns(C1 + 4).Visible = False
                gvPointDtl.Columns(C1 + 4).Visible = False
            End If
        Next

        Dim arCode() As String = itemdesc1.Text.Split(";")
        Dim sCodeIn As String = "" : Dim sQel As String = ""
        Dim adr As String = ""
        For C1 As Integer = 0 To arCode.Length - 1
            If arCode(C1) <> "" Then
                sCodeIn &= "'" & arCode(C1) & "',"
            End If
        Next

        If sCodeIn <> "" Then
            itemcode &= " where itemcode in (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
        End If
        sSql = "select 0 AS seq ,itemoid, itemdesc, itemcode, 0.00 AS point01, 0.00 AS point02, 0.00 AS point03, 0.00 AS point04, 0.00 AS point05, 0.00 AS point06, 0.00 AS point07, 0.00 AS point08, 0.00 AS point09, 0.00 AS point10, 0.00 AS point11/*, 0.00 AS point12*/, 0.00 AS point13, 0.00 AS point14, 0.00 point15, 0.00 AS point16, 0.00 AS point17, 0.00 AS sumpoint from ql_mstitem " & itemcode & ""
        Dim tbList As DataTable = cKoneksi.ambiltabel(sSql, "mstitem")
        For C1 As Integer = 0 To tbList.Rows.Count - 1
            tbList.Rows.Item(C1)("seq") = C1 + 1
        Next
        gvList.DataSource = tbList
        gvList.DataBind()
        Session("tblPoint") = tbList
        BtnAddList.Visible = (tbList.Rows.Count > 0)
        cbCabang.Enabled = False
        cbCheckAll.Enabled = False
        btnClear.Visible = True
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "Checkvalue=True"
        If dv.Count > 0 Then

            If itemdesc1.Text <> "" Then
                For C1 As Integer = 0 To dv.Count - 1
                    itemdesc1.Text &= ";" & dv(C1)("itemcode").ToString & ";"
                Next
            Else
                For C1 As Integer = 0 To dv.Count - 1
                    itemdesc1.Text &= dv(C1)("itemcode").ToString & ";"
                Next
            End If


            If itemdesc1.Text <> "" Then
                itemdesc1.Text = Left(itemdesc1.Text, itemdesc1.Text.Length - 1)
            End If
            dv.RowFilter = ""
            CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            showMessage("- Maaf, Silahkan pilih katalog, dengan beri centang pada kolom pilih..<BR>", 2)
        End If
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        UpdateDetailData()
        gvList.PageIndex = e.NewPageIndex
        Dim dtDtl As DataTable = Session("tblPoint")
        gvList.DataSource = dtDtl : gvList.DataBind()
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        For C1 As Integer = 0 To cbCabang.Items.Count - 1
            cbCabang.Items(C1).Selected = False
        Next
        cbCabang.Enabled = True
        cbCheckAll.Enabled = True
        itemdesc1.Text = ""
        ResetGenerate()
        btnClear.Visible = False
    End Sub

    Protected Sub btnClearDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDtl.Click
        Session("tbldtlPoint") = Nothing
        gvPointDtl.DataSource = Nothing
        gvPointDtl.DataBind()
        BtnAddList.Visible = False
        For C1 As Integer = 0 To cbCabang.Items.Count - 1
            cbCabang.Items(C1).Selected = False
        Next
        btnClearDtl.Visible = False : cbCabang.Enabled = True : cbCheckAll.Enabled = True
        btnGenerate.Visible = True : cbCheckAll.Checked = False
        promtype.Enabled = True : promtype.CssClass = "inpText"
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next

            If dd_stock.SelectedValue <> "ALL" Then
                sFilter &= " AND stockflag='" & dd_stock.SelectedValue & "'"
            End If

            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                'showMessage("Maaf, Data barang yang anda cari tidak ada..!!", CompnyName)
                gvItem.DataSource = Nothing
                gvItem.DataBind()
                mpeListMat.Show()
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        dd_stock.SelectedValue = "ALL"
        If UpdateCheckedMat() Then
            BindDataListItem()
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                showMessage("Data Barang tidak ada..!!", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindItem.Click
        filterMenuItem.Text = ""
        binddataMenu()
        gvMenu.Visible = True
        PanelUser.Visible = True
        BtnUser.Visible = True
        MpeUser.Show()
    End Sub

    Protected Sub point_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles point.TextChanged
        If point.Text = "" Then
            point.Text = 0
        End If
        point.Text = ToMaskEdit(point.Text, 4)
    End Sub

    Protected Sub btnAddListRwd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddListRwd.Click
        Dim sVali As String = ""
        If Session("tbldtlRwd") Is Nothing Then
            Dim dtlTable As DataTable = setTableDetailReward()
            Session("tbldtlRwd") = dtlTable
        End If
        If itemdesc.Text = "" Then
            showMessage("- Tidak ada data yang ditambahkan", "- WARNING") : Exit Sub
        End If
        If ddlTypeBarang.SelectedValue = "BARANG" Then
            If itemoid.Text = "" Then
                sVali &= "- Maaf, item " & itemdesc.Text & " belum di pilih..!!<br>"
            End If
        End If
        Dim objTable As DataTable
        objTable = Session("tbldtlRwd")
        Dim dv As DataView = objTable.DefaultView

        If I_u2.Text = "New Detail" Then
            If ddlTypeBarang.SelectedValue = "BARANG" Then
                dv.RowFilter = "itemoid= " & itemoid.Text & ""
            End If
        Else
            If ddlTypeBarang.SelectedValue = "BARANG" Then
                dv.RowFilter = "itemoid= " & itemoid.Text & " AND seq<>" & seq2.Text
            End If
        End If

        If ddlTypeBarang.SelectedValue = "BARANG" Then
            If dv.Count > 0 Then
                dv.RowFilter = ""
                If itemoid.Text <> "" Then
                    sVali &= "Maaf, " & itemdesc.Text & " Sudah ditambahkan, cek pada tabel..!!</ br>"
                End If
            End If
        End If

        If sVali <> "" Then
            showMessage(sVali, "- WARNING")
            Exit Sub
        End If

        dv.RowFilter = ""
        Dim objRow As DataRow
        If I_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("seq") = objTable.Rows.Count + 1
        Else
            objRow = objTable.Rows(seq2.Text - 1)
            objRow.BeginEdit()
        End If
        If ddlTypeBarang.SelectedValue = "BARANG" Then
            objRow("itemoid") = itemoid.Text
        Else
            objRow("itemoid") = 0
        End If
        objRow("itemdesc") = itemdesc.Text
        objRow("qty") = ToDouble(qty.Text)
        objRow("amount") = ToDouble(amount.Text)
        objRow("point") = ToDouble(point.Text)
        objRow("targetamount") = ToDouble(targetamount.Text)
        objRow("typebarang") = ddlTypeBarang.SelectedValue
        If I_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If

        ClearDtlInput()
        Session("tbldtlRwd") = objTable
        gvReward.Visible = True
        gvReward.DataSource = Nothing
        gvReward.DataSource = objTable
        gvReward.DataBind()
        seq2.Text = objTable.Rows.Count + 1
        gvReward.SelectedIndex = -1 : gvReward.Columns(10).Visible = True
        I_u2.Text = "New Detail" : btnClearRwd.Visible = False
        ddlTypeBarang.SelectedValue = "BARANG"
        ddlTypeBarang_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub gvReward_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReward.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
        End If
    End Sub

    Protected Sub gvReward_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvReward.RowDeleting
        Dim dtab As DataTable
        If Not Session("tbldtlRwd") Is Nothing Then
            dtab = Session("tbldtlRwd")
        Else
            showMessage("Maaf, Ada error detail list session !", 1)
            Exit Sub
        End If

        Dim df As DataView = dtab.DefaultView
        df.RowFilter = "seq=" & e.RowIndex + 1 & ""
        df.RowFilter = ""

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(e.RowIndex + 1) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        gvReward.DataSource = dtab
        gvReward.DataBind()
        Session("tbldtlRwd") = dtab
    End Sub

    Protected Sub gvReward_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReward.SelectedIndexChanged
        Try
            seq2.Text = gvReward.SelectedDataKey.Item("seq").ToString
            If Session("tbldtlRwd") Is Nothing = False Then
                I_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("tbldtlRwd")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "seq=" & seq2.Text
                itemoid.Text = dv.Item(0).Item("itemoid").ToString
                itemdesc.Text = dv.Item(0).Item("itemdesc").ToString & " "
                qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("qty").ToString), 4)
                amount.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("amount").ToString), 4)
                point.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("point").ToString), 4)
                targetamount.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("targetamount").ToString), 4)
                ddlTypeBarang.SelectedValue = dv.Item(0).Item("typebarang").ToString
                dv.RowFilter = ""
                btnClearRwd.Visible = True
                gvReward.Columns(10).Visible = False
                If ddlTypeBarang.SelectedValue <> "BARANG" Then
                    itemdesc.CssClass = "inpText" : imbFindItem.Visible = False
                    imbClearItem.Visible = False
                Else
                    itemdesc.CssClass = "inpTextDisabled" : imbFindItem.Visible = True
                    imbClearItem.Visible = True
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR !!")
        End Try
    End Sub

    Protected Sub btnClearRwd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearRwd.Click
        itemoid.Text = "" : itemdesc.Text = ""
        point.Text = "" : I_u2.Text = "New Detail"
        If Session("tbldtlRwd") Is Nothing = False Then
            Dim objTable As DataTable = Session("tbldtlRwd")
            seq.Text = objTable.Rows.Count + 1
        Else
            seq.Text = 1
        End If
        gvReward.SelectedIndex = -1
        gvReward.Columns(6).Visible = True
    End Sub

    Protected Sub imbClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearItem.Click
        itemoid.Text = "" : itemdesc.Text = ""
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        If qty.Text = "" Then
            qty.Text = 0
        End If
        qty.Text = ToMaskEdit(qty.Text, 4)
    End Sub

    Protected Sub amount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amount.TextChanged
        If amount.Text = "" Then
            amount.Text = 0
        End If
        amount.Text = ToMaskEdit(amount.Text, 4)
    End Sub

    Protected Sub targetamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles targetamount.TextChanged
        If targetamount.Text = "" Then
            targetamount.Text = 0
        End If
        targetamount.Text = ToMaskEdit(targetamount.Text, 4)
    End Sub

    Protected Sub ddlTypeBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTypeBarang.SelectedIndexChanged
        If ddlTypeBarang.SelectedValue <> "BARANG" Then
            itemdesc.CssClass = "inpText" : imbFindItem.Visible = False
            imbClearItem.Visible = False
        Else
            itemdesc.CssClass = "inpTextDisabled" : imbFindItem.Visible = True
            imbClearItem.Visible = True
        End If
    End Sub

    Protected Sub cbCheckAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCheckAll.CheckedChanged
        If cbCheckAll.Checked = True Then
            For Each listItem As ListItem In cbCabang.Items
                listItem.Selected = True
            Next
        Else
            For Each listItem As ListItem In cbCabang.Items
                listItem.Selected = False
            Next
        End If
    End Sub
#End Region

    Protected Sub imbPrintFromList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        sSql = "SELECT [cmpcode], pm.[progpointmstoid], [progpointcode], [progpointdesc], [progpointstartdate], [progpointfinishdate], CONVERT(time, progpointstartdate) AS [progpointstarttime], CONVERT(time, progpointfinishdate) AS [progpointfinsihtime], [progpointtype], [progpointnote], [crtuser], [upduser], [updtime], branch_code, opsipiutanglunas, opsipiutangontime, progpointflag, pd.* FROM [QL_mstprogpoint] pm INNER JOIN (Select distinct seq, progpointmstoid, pd.itemoid, i.itemdesc, i.itemcode, 0.00 AS point01, 0.00 AS point02, 0.00 AS point03, 0.00 AS point04, 0.00 AS point05, 0.00 AS point06, 0.00 AS point07, 0.00 AS point08, 0.00 AS point09, 0.00 AS point10, 0.00 AS point11/*, 0.00 AS point12*/, 0.00 AS point13, 0.00 AS point14, 0.00 AS point15, 0.00 AS point16, 0.00 AS point17, 0.0 qty, 0.0 amount, 0 targetamount, 0 point, 'POINT' typebarang, 'POINT' GroupNya From QL_mstprogpointdtl pd INNER JOIN QL_MSTITEM i ON i.itemoid = pd.itemoid UNION ALL Select seq, progpointmstoid, pd.itemoid, pd.itemdesc, Isnull(i.itemcode,'') itemcode, 0.00 AS point01, 0.00 AS point02, 0.00 AS point03, 0.00 AS point04, 0.00 AS point05, 0.00 AS point06, 0.00 AS point07, 0.00 AS point08, 0.00 AS point09, 0.00 AS point10, 0.00 AS point11/*, 0.00 AS point12*/, 0.00 AS point13, 0.00 AS point14, 0.00 AS point15, 0.00 AS point16, 0.00 AS point17, pd.qty, pd.amount, pd.targetamount, pd.point, pd.typebarang, 'REWARD' GroupNya From QL_mstprogpointdtl2 pd LEFT JOIN QL_MSTITEM i ON i.itemoid = pd.itemoid ) pd ON pd.progpointmstoid=pm.progpointmstoid Where pd.progpointmstoid=" & sender.ToolTip
        Dim ObjTable As DataTable = cKoneksi.ambiltabel(sSql, "DataDetail")
        Dim sValue() As String = ObjTable.Rows(0)("branch_code").ToString.Split(",")
        If sValue.Length > 0 Then
            If cbCabang.Items.Count > 0 Then
                For C1 As Integer = 0 To sValue.Length - 1
                    Dim li As ListItem = cbCabang.Items.FindByValue(sValue(C1).Trim)
                    Dim i As Integer = cbCabang.Items.IndexOf(li)
                    If i >= 0 Then
                        cbCabang.Items(cbCabang.Items.IndexOf(li)).Selected = True
                    End If
                Next
            End If
        End If
        For C1 As Integer = 0 To ObjTable.Rows.Count - 1
            For D1 As Integer = 0 To cbCabang.Items.Count - 1
                If cbCabang.Items(D1).Selected = True Then
                    Dim cb As String = cbCabang.Items(D1).Value & ""
                    Dim point As Double = GetStrData("Select isnull(point,0.00) From ql_mstprogpointdtl Where seq = " & ObjTable.Rows(C1).Item("seq") & " and branch_code = '" & cbCabang.Items(D1).Value & "'") & ""
                    ObjTable.Rows(C1).Item("point" & cbCabang.Items(D1).Value & "") = point
                    gvPointDtl.Columns(D1 + 4).Visible = True
                End If
            Next
        Next
        ObjTable.AcceptChanges() : ObjTable = ObjTable

        If Session("TabelDtl") Is Nothing Then
            CreateDtlTbl()
        End If

        Dim objRow As DataRow
        If ObjTable.Rows.Count > 0 Then
            For C1 As Integer = 0 To ObjTable.Rows.Count - 1
                objRow = ObjTable.NewRow()
                objRow("seq") = ObjTable.Rows(C1).Item("seq")
                objRow("progpointcode") = ObjTable.Rows(C1).Item("progpointcode")
                objRow("progpointdesc") = ObjTable.Rows(C1).Item("progpointdesc")
                objRow("progpointstartdate") = ObjTable.Rows(C1).Item("progpointstartdate")
                objRow("progpointfinishdate") = ObjTable.Rows(C1).Item("progpointfinishdate")
                objRow("progpointtype") = ObjTable.Rows(C1).Item("progpointtype")
                objRow("crtuser") = ObjTable.Rows(C1).Item("crtuser")
                objRow("upduser") = ObjTable.Rows(C1).Item("upduser")
                objRow("updtime") = ObjTable.Rows(C1).Item("updtime")
                objRow("progpointnote") = ObjTable.Rows(C1).Item("progpointnote")
                objRow("progpointflag") = ObjTable.Rows(C1).Item("progpointflag")
                objRow("itemoid") = ObjTable.Rows(C1).Item("itemoid")
                objRow("itemcode") = ObjTable.Rows(C1).Item("itemcode")
                objRow("itemdesc") = ObjTable.Rows(C1).Item("itemdesc")
                objRow("point01") = ObjTable.Rows(C1).Item("point01")
                objRow("point02") = ObjTable.Rows(C1).Item("point02")
                objRow("point03") = ObjTable.Rows(C1).Item("point03")
                objRow("point04") = ObjTable.Rows(C1).Item("point04")
                objRow("point05") = ObjTable.Rows(C1).Item("point05")
                objRow("point06") = ObjTable.Rows(C1).Item("point06")
                objRow("point07") = ObjTable.Rows(C1).Item("point07")
                objRow("point08") = ObjTable.Rows(C1).Item("point08")
                objRow("point09") = ObjTable.Rows(C1).Item("point09")
                objRow("point10") = ObjTable.Rows(C1).Item("point10")
                objRow("point11") = ObjTable.Rows(C1).Item("point11")
                'objRow("point12") = ObjTable.Rows(C1).Item("point12")
                objRow("point13") = ObjTable.Rows(C1).Item("point13")
                objRow("point14") = ObjTable.Rows(C1).Item("point14")
                objRow("point15") = ObjTable.Rows(C1).Item("point15")
                objRow("point16") = ObjTable.Rows(C1).Item("point16")
                objRow("point17") = ObjTable.Rows(C1).Item("point17")
                objRow("qty") = ObjTable.Rows(C1).Item("qty")
                objRow("amount") = ObjTable.Rows(C1).Item("amount")
                objRow("targetamount") = ObjTable.Rows(C1).Item("targetamount")
                objRow("point") = ObjTable.Rows(C1).Item("point")
                objRow("typebarang") = ObjTable.Rows(C1).Item("typebarang")
                objRow("GroupNya") = ObjTable.Rows(C1).Item("GroupNya")
                ObjTable.Rows.Add(objRow)
            Next
            ObjTable = ObjTable : Dim nFile As String = ""
            nFile = Server.MapPath(folderReport & "rptProgPoint.rpt")
            report.Load(nFile)
            report.SetDataSource(ObjTable)
            CProc.SetDBLogonReport(report)

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ProgramPointCustomer" & GetServerTime())
            Catch ex As Exception
                report.Close() : report.Dispose()
            End Try
        End If

    End Sub
End Class
