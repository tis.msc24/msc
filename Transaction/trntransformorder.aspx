<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trntransformorder.aspx.vb" Inherits="trntransformorder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <script type="text/javascript">
        function EnterEventSN(e) {
            if (e.keyCode == 13) {
                __doPostBack('<%=EnterSNTI.UniqueID%>', "");
                ModalPopupSN.Show();
            }

        }
    </script>
    <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0"
        class="tabelhias" width="100%">
        <tr>
            <th class="header" colspan="5" valign="top">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Transform Item"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"><HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image>
                            <strong><span style="FONT-SIZE: 9pt"> List Tranform Item :.</span></strong>&nbsp;</HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=3><asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w17" DefaultButton="imbFindBoM"><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small" align=left>Cabang</TD><TD style="FONT-SIZE: x-small" align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="ddlfCabang" runat="server" CssClass="inpText" __designer:wfdid="w18"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Filter </TD><TD style="FONT-SIZE: x-small" align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w19"> 
<asp:ListItem Value="a.transformno">Transform No.</asp:ListItem>
<asp:ListItem Value="a.transformnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w20"></asp:TextBox>&nbsp; <asp:ImageButton id="imbFindBoM" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w21" AlternateText="Find BoM"></asp:ImageButton>&nbsp; <asp:ImageButton id="imbAllBoM" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w22" AlternateText="View All BoM"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD colSpan=3><asp:GridView id="GVmst" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w23" EnableModelValidation="True" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowSorting="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="transformoid" DataNavigateUrlFormatString="trntransformorder.aspx?oid={0}" DataTextField="transformno" HeaderText="Transform No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="transformdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date" SortExpression="transdate">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformnote" HeaderText="Note" SortExpression="note" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="transformtype" HeaderText="Type" SortExpression="type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton ID="LinkButton1" OnClick="LinkButton1_Click" runat="server" ToolTip='<%# String.Format("{0},{1}", eval("transformno"), eval("transformtype")) %>' Text="Print"></asp:LinkButton>
                                                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="lblError" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVmst"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image>
                            <strong><span style="FONT-SIZE: 9pt"> Form Transform Item</span></strong> <strong><span
                                style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=3><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:Label id="Label20" runat="server" Font-Size="Small" Font-Bold="True" Text="Stock Item Bertambah" __designer:wfdid="w117" Font-Underline="True"></asp:Label> <asp:Label id="labelunit3" runat="server" __designer:wfdid="w115" Visible="False"></asp:Label> <asp:Label id="labelunit2" runat="server" __designer:wfdid="w114" Visible="False"></asp:Label><asp:Label id="labelunit1" runat="server" __designer:wfdid="w113" Visible="False"></asp:Label> <asp:Label id="labelsatuan1" runat="server" __designer:wfdid="w110" Visible="False"></asp:Label> <asp:Label id="labelsatuan2" runat="server" __designer:wfdid="w111" Visible="False"></asp:Label> <asp:Label id="labelsatuan3" runat="server" __designer:wfdid="w112" Visible="False"></asp:Label><asp:Label id="stockflag" runat="server" __designer:wfdid="w116" Visible="False"></asp:Label> <asp:Label id="FlagItem1" runat="server" __designer:wfdid="w116" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left>Transformasi&nbsp;No.</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:TextBox id="trnno" runat="server" Width="141px" CssClass="inpTextDisabled" __designer:wfdid="w118" ReadOnly="True" MaxLength="30"></asp:TextBox><asp:Label id="periodacctg" runat="server" __designer:wfdid="w107" Visible="False"></asp:Label><asp:Label id="i_u" runat="server" Width="49px" CssClass="Important" Text="new" __designer:wfdid="w21" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left><asp:Label id="labelkonversi2_3" runat="server" __designer:wfdid="w109" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 301px" class="Label" align=left><asp:Label id="labelkonversi1_2" runat="server" __designer:wfdid="w108" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left>Cabang</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:DropDownList id="ddlCabang" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w119" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left>Transformation Type</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 301px" class="Label" align=left><asp:DropDownList id="transtype" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w120" AutoPostBack="True" OnSelectedIndexChanged="transtype_SelectedIndexChanged"><asp:ListItem Value="Type">Choose Type</asp:ListItem>
<asp:ListItem>Create</asp:ListItem>
<asp:ListItem>Release</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>Location</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="trnloc" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w121" AutoPostBack="True"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 70px" class="Label" align=left>Date<asp:Label id="Label10" runat="server" CssClass="Important" Text="*" __designer:wfdid="w122"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 3px" class="Label" align=left>:</TD><TD style="WIDTH: 301px" class="Label" align=left><asp:TextBox id="trndate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w123" MaxLength="10" Enabled="False"></asp:TextBox> <asp:ImageButton id="ibtrndate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w124" Visible="False"></asp:ImageButton> <asp:Label id="Label9" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w125"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblWIP" runat="server" Text="Katalog" __designer:wfdid="w128"></asp:Label>&nbsp;<asp:Label id="Label12" runat="server" CssClass="Important" Text="*" __designer:wfdid="w129"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="trnitem" runat="server" Width="196px" CssClass="inpText" __designer:wfdid="w130"></asp:TextBox> <asp:ImageButton id="ibfinditem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w131" AlternateText="Find Item"></asp:ImageButton> <asp:ImageButton id="ibdelitem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w132" AlternateText="Clear Item"></asp:ImageButton></TD><TD class="Label" align=left>Quantity&nbsp;<asp:Label id="lblNeedWIP" runat="server" CssClass="Important" Text="*" __designer:wfdid="w135"></asp:Label></TD><TD class="Label" align=left>:</TD><TD style="WIDTH: 301px" class="Label" align=left><asp:TextBox id="itemqty" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w136" MaxLength="6">0</asp:TextBox>&nbsp;<asp:DropDownList id="itemunit" runat="server" Width="76px" CssClass="inpTextDisabled" __designer:wfdid="w137" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvItemSearch" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w139" Visible="False" EnableModelValidation="True" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" DataKeyNames="itemoid,satuan1,satuan2,satuan3,unit1,unit2,konversi1_2,konversi2_3,itemcode,stockflag,itemflag,itemdesc" EmptyDataText="No data in database." PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoAkhir" HeaderText="Saldo Akhir">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemflag" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="lblstatusitemdata" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="trnitemoid" runat="server" __designer:wfdid="w133" Visible="False"></asp:Label> <asp:Label id="ItemcodeHeHeader" runat="server" __designer:wfdid="w134" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Merk</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="merk" runat="server" Width="195px" CssClass="inpTextDisabled" __designer:wfdid="w140" Enabled="False"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Status</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 301px" class="Label" align=left><asp:TextBox id="txtStatus" runat="server" Width="101px" CssClass="inpTextDisabled" __designer:wfdid="w141" ReadOnly="True" MaxLength="10">IN PROCESS</asp:TextBox> <asp:Label id="StatusIsiSN" runat="server" __designer:wfdid="w142" Visible="False"></asp:Label> </TD></TR><TR><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 70px" class="Label" align=left>Note</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 3px" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:TextBox id="trnnote" runat="server" Width="272px" CssClass="inpText" __designer:wfdid="w143" MaxLength="250"></asp:TextBox>&nbsp; </TD><TD class="Label" align=left><asp:Label id="labelseqhead" runat="server" __designer:wfdid="w144" Visible="False"></asp:Label> </TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 3px" class="Label" align=left></TD><TD style="WIDTH: 301px" class="Label" align=left><asp:ImageButton style="MARGIN-RIGHT: 20px" id="imbAddToListHead" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w146" AlternateText="Add To List"></asp:ImageButton> <asp:ImageButton id="imbClearHead" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w147" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 100%"><asp:GridView id="GVHeader" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w149" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" DataKeyNames="itemoid,satuan,satuan1,satuan2,satuan3,unit1,unit2,unit3,konversi1_2,konversi2_3,locationoid,itemflag,stockflag">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Width="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemflag" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" Font-Size="Small" ForeColor="Red" Width="15px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="stockflag" HeaderText="stockflag" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Tranformation Header !"></asp:Label>
                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV><asp:Label id="labelref" runat="server" __designer:wfdid="w148" Visible="False"></asp:Label> <asp:Label id="i_u_head" runat="server" CssClass="Important" Text="new" __designer:wfdid="w145" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label17" runat="server" Font-Size="Small" Font-Bold="True" Text="Stock Item Berkurang" __designer:wfdid="w150" Font-Underline="True"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>Location</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:DropDownList id="dtlloc" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w151" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="itemdtloid" runat="server" __designer:wfdid="w157" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 3px" class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="ItemcodeFooter" runat="server" __designer:wfdid="w158" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="LbType" runat="server" Text="Katalog" __designer:wfdid="w152"></asp:Label> <asp:Label id="lbitemdetail" runat="server" CssClass="Important" Text="*" __designer:wfdid="w153"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="itemdetail" runat="server" Width="196px" CssClass="inpText" __designer:wfdid="w154"></asp:TextBox>&nbsp;<asp:ImageButton style="HEIGHT: 16px" id="ibfinditemdetail" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w155"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibdelitemdetail" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w156"></asp:ImageButton></TD><TD class="Label" align=left>Quantity <asp:Label id="Label13" runat="server" CssClass="Important" Text="*" __designer:wfdid="w159"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 3px" class="Label" align=left>:</TD><TD style="WIDTH: 301px" class="Label" align=left><asp:TextBox id="itemdtlqty" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w160">0</asp:TextBox>&nbsp;<asp:DropDownList id="unitdtl" runat="server" Width="76px" CssClass="inpTextDisabled" __designer:wfdid="w161" AutoPostBack="True" Enabled="False"></asp:DropDownList> (max. <asp:Label id="maxQty" runat="server" __designer:wfdid="w162">0.00</asp:Label>)<%--<ajaxToolkit:MaskedEditExtender ID="meedtlqty" runat="server" TargetControlID="itemdtlqty" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft" CultureTimePlaceholder="" CultureThousandsPlaceholder="" CultureDecimalPlaceholder="" CultureDatePlaceholder="" CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder="" Enabled="True"></ajaxToolkit:MaskedEditExtender>--%> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvItem2" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w163" Visible="False" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" DataKeyNames="itemoid,satuan1,satuan2,satuan3,unit1,unit2,konversi1_2,konversi2_3,stockflag,itemflag" EmptyDataText="No data in database." PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="sisa" DataFormatString="{0:#,##0.00}" HeaderText="Max Qty.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemflag" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="lblstatusitemdata" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="FlagItem2" runat="server" __designer:wfdid="w116" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Merk</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="merkdtl" runat="server" Width="195px" CssClass="inpTextDisabled" __designer:wfdid="w164" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Height="18px" CssClass="Important" Text="new" __designer:wfdid="w177" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD style="WIDTH: 301px" class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Detail Note</TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="272px" CssClass="inpText" __designer:wfdid="w165" MaxLength="250"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="labelseq" runat="server" __designer:wfdid="w178" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 3px" class="Label" align=left></TD><TD style="WIDTH: 301px" class="Label" align=left><asp:ImageButton style="MARGIN-RIGHT: 20px" id="imbAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w166" AlternateText="Add To List"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w167" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 100%"><asp:GridView id="GVDtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w179" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" DataKeyNames="itemoid,satuan,satuan1,satuan2,satuan3,unit1,unit2,unit3,konversi1_2,konversi2_3,locationoid,itemflag,stockflag">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Width="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemflag" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" Font-Size="Small" ForeColor="Red" Width="15px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="stockflag" HeaderText="stockflag" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Tranformation Detail !"></asp:Label>
                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV><asp:Label id="labelsatuan1dtl" runat="server" __designer:wfdid="w174" Visible="False"></asp:Label> <asp:Label id="labelsatuan2dtl" runat="server" __designer:wfdid="w175" Visible="False"></asp:Label> <asp:Label id="labelsatuan3dtl" runat="server" __designer:wfdid="w176" Visible="False"></asp:Label> <asp:Label id="labelkonversi1_2dtl" runat="server" __designer:wfdid="w168" Visible="False"></asp:Label> <asp:Label id="labelkonversi2_3dtl" runat="server" __designer:wfdid="w169" Visible="False"></asp:Label> <asp:Label id="stockflagdtl" runat="server" __designer:wfdid="w173" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><asp:Label id="labelunit1dtl" runat="server" __designer:wfdid="w170" Visible="False"></asp:Label> <asp:Label id="labelunit2dtl" runat="server" __designer:wfdid="w171" Visible="False"></asp:Label> <asp:Label id="labelunit3dtl" runat="server" __designer:wfdid="w172" Visible="False"></asp:Label> <asp:Label id="createtime" runat="server" __designer:wfdid="w197"></asp:Label></TD></TR><TR><TD colSpan=3><asp:Label id="Label18" runat="server" Text="Last updated On" __designer:wfdid="w180"></asp:Label> <asp:Label id="lblUpdTime" runat="server" Font-Bold="True" __designer:wfdid="w181"></asp:Label> <asp:Label id="Label19" runat="server" Text="by" __designer:wfdid="w182"></asp:Label> <asp:Label id="lblUpdUser" runat="server" Font-Bold="True" __designer:wfdid="w183"></asp:Label></TD></TR><TR><TD colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px; HEIGHT: 23px" id="imbSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w184"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w185"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w186" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w187" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w191" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w193"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="cetrndate" runat="server" __designer:wfdid="w194" TargetControlID="trndate" Format="dd/MM/yyyy" PopupButtonID="ibtrndate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteQty" runat="server" __designer:wfdid="w188" TargetControlID="itemdtlqty" ValidChars="0123456789."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="meetrndate" runat="server" __designer:wfdid="w195" TargetControlID="trndate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="TiHeaderqty" runat="server" __designer:wfdid="w196" TargetControlID="itemqty" ValidChars="0123456789."></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                            
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel ID="upMsgbox" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PanelMsgBox" runat="server" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK"
                            Visible="False" Width="496px">
                            <table border="0" cellpadding="1" cellspacing="1" style="width: 495px">
                                <tbody>
                                    <tr>
                                        <td align="left" colspan="2" style="height: 10px" valign="top">
                                            <asp:Panel ID="panelPesan" runat="server" BackColor="Yellow" Height="25px" Width="100%">
                                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Black"></asp:Label>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" style="height: 10px" valign="top"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="width: 46px" valign="top">
                                            <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                                Width="24px" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" style="height: 10px; text-align: center" valign="top">
                                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" style="text-align: center" valign="top">
                                            <asp:ImageButton ID="btnMsgBoxOK" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/ok.png"
                                                OnClick="btnMsgBoxOK_Click" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" BackgroundCssClass="modalBackground"
                            Drag="True" DropShadow="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption"
                            TargetControlID="beMsgBox">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Button ID="beMsgBox" runat="server" CausesValidation="False" Visible="False"
                            Width="130px" />
                    </ContentTemplate>
                </asp:UpdatePanel><br />
                &nbsp;
               <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
<asp:UpdatePanel id="UpdatePanelSN" runat="server"><ContentTemplate>
<asp:Panel id="pnlInvnSN" runat="server" Width="600px" CssClass="modalBox" BorderWidth="2px" Visible="False" BorderStyle="Solid"><TABLE style="WIDTH: 100%" __designer:mapid="195"><TBODY __designer:mapid="196"><TR __designer:mapid="197"><TD style="TEXT-ALIGN: center" align=left __designer:mapid="198"><asp:Label id="LabelDescSn" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Enter SN of Item"></asp:Label> </TD></TR><TR style="FONT-SIZE: 8pt" __designer:mapid="19a"><TD __designer:mapid="19b"><asp:Label id="NewSN" runat="server" ForeColor="Red" Text="New SN"></asp:Label> </TD></TR><TR style="FONT-SIZE: 8pt" __designer:mapid="19d"><TD style="TEXT-ALIGN: left" align=left __designer:mapid="19e">Item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <asp:TextBox id="TextItem" runat="server" Width="195px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp;<asp:Label id="KodeItem" runat="server" Visible="False"></asp:Label> &nbsp;<asp:Label id="SNseq" runat="server" Visible="False"></asp:Label> </TD></TR><TR style="FONT-SIZE: 8pt" __designer:mapid="1a2"><TD style="TEXT-ALIGN: left" align=left __designer:mapid="1a3">Qty Item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <asp:TextBox id="TextQty" runat="server" Width="69px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp; <asp:TextBox id="TextSatuan" runat="server" Width="69px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> </TD></TR><TR style="FONT-SIZE: 8pt" __designer:mapid="1a6"><TD style="TEXT-ALIGN: left" align=left __designer:mapid="1a7">Scan Serial Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <asp:TextBox id="TextSN" onkeypress="return EnterEventSN(event)" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> &nbsp; <asp:ImageButton style="HEIGHT: 23px" id="EnterSNTI" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> &nbsp;<asp:Button id="btnGenerateSN" runat="server" CssClass="btn green" Text="Generate SN"></asp:Button> </TD></TR></TBODY><CAPTION style="FONT-SIZE: 8pt" __designer:mapid="1ab"><DT style="TEXT-ALIGN: center" __designer:mapid="1ac"></DT></CAPTION><TBODY><TR style="FONT-SIZE: 8pt" __designer:mapid="1ae"><TD align=left __designer:mapid="1af"><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2" __designer:mapid="1b0"><DIV style="TEXT-ALIGN: center" id="Div2" __designer:mapid="1b1"><STRONG __designer:mapid="1b2"><asp:Label id="LabelPesanSn" runat="server" Font-Size="Larger" Font-Bold="True" ForeColor="Red"></asp:Label> </STRONG></DIV><DIV __designer:mapid="1b4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%" __designer:mapid="1b5"><asp:GridView id="GVSN" runat="server" Width="549px" CssClass="MyTabStyle" ForeColor="#333333" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemoid" HeaderText="Itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="id_SN" HeaderText="Serial Number">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="450px"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR style="FONT-SIZE: 8pt" __designer:mapid="1c0"><TD style="HEIGHT: 17px; TEXT-ALIGN: center" align=left __designer:mapid="1c1"><asp:LinkButton id="lkbCloseSN" runat="server" Font-Size="X-Small">[ Close ]</asp:LinkButton> &nbsp;&nbsp; <asp:LinkButton id="lkbPilihItem" runat="server" Font-Size="X-Small">[ OK ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupSN" runat="server" TargetControlID="btnHideSN" PopupDragHandleControlID="lblCaptInvtry" PopupControlID="UpdatePanelSN" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideSN" runat="server"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="TextSN"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="EnterSNTI"></asp:PostBackTrigger>
</Triggers>
                    </asp:UpdatePanel>
            </td>
        </tr>
    </table>

</asp:Content>

