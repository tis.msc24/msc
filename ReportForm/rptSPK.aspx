<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSPK.aspx.vb" Inherits="Report_SPK" title="PT. SIP - SPK Main" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%--
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit, Version=1.0.11119.20010, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<script language="javascript" type="text/javascript">
// <!CDATA[
// ]]>
</script>

    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
                <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0"
                    class="tabelhias" width="100%">
                    <tr>
                        <th class="header" colspan="3" valign="middle">
                            <%--<asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Sales Order"></asp:Label><asp:SqlDataSource ID="SDSDataview"
                        runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                        SelectCommand="SELECT od.orderoid, od.orderno, od.orderdate, od.ordernote, c.CUSTNAME, od.orderstatus FROM &#13;&#13;&#10;QL_trnordermst AS od INNER JOIN QL_MSTCUST AS c ON od.custoid = c.CUSTOID WHERE (od.cmpcode = &#13;&#13;&#10;@CMPCODE) AND (od.upduser LIKE @UPDUSER) AND (od.orderno LIKE @ORDERNO) AND (c.CUSTNAME LIKE &#13;&#13;&#10;@CUSTNAME) AND (od.orderdate BETWEEN @start AND @end ) AND (od.orderstatus LIKE @status) ORDER BY &#13;&#13;&#10;od.orderdate DESC, od.orderno DESC" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">--%>
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana"
                                Font-Size="21px" ForeColor="Maroon" Text=".: Laporan SPK"></asp:Label>&nbsp;
                        </th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
<asp:UpdatePanel id="UpdatePanelSearch" runat="server"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w11" DefaultButton="btnReport"><TABLE><TBODY><TR><TD style="HEIGHT: 21px" align=left>Cabang </TD><TD style="HEIGHT: 21px" align=center colSpan=1>:</TD><TD style="HEIGHT: 21px" align=left colSpan=1><asp:DropDownList id="DDLcabang" runat="server" CssClass="inpText" __designer:wfdid="w33"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Width="120px" Text="SPK Rencana Mulai" __designer:wfdid="w12"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD><TD align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:TextBox id="spkplanstart" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w13"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w14"></asp:ImageButton>&nbsp;<asp:Label id="Label8" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w15"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="SPK Rencana Akhir" __designer:wfdid="w16"></asp:Label>&nbsp; </TD><TD align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:TextBox id="spkplanend" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w17"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar3" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w18"></asp:ImageButton>&nbsp;<asp:Label id="Label9" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w19"></asp:Label></TD></TR><TR><TD align=right></TD><TD align=right></TD><TD style="WIDTH: 1031px" align=left colSpan=1><asp:DropDownList id="spkgroup" runat="server" Width="120px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w20" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="CONV">CONVERTING</asp:ListItem>
<asp:ListItem Value="FM">FILM MAKING</asp:ListItem>
</asp:DropDownList> <ajaxToolkit:MaskedEditExtender id="meeDate1" runat="server" __designer:wfdid="w21" CultureThousandsPlaceholder="" Mask="99/99/9999" CultureDecimalPlaceholder="" UserDateFormat="MonthDayYear" MaskType="Date" CultureCurrencySymbolPlaceholder="" Enabled="True" TargetControlID="spkplanstart" CultureAMPMPlaceholder="" CultureDateFormat="" CultureTimePlaceholder="" CultureDatePlaceholder=""></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w22" Enabled="True" TargetControlID="spkplanstart" PopupButtonID="btnCalendar2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate2" runat="server" __designer:wfdid="w23" CultureThousandsPlaceholder="" Mask="99/99/9999" CultureDecimalPlaceholder="" UserDateFormat="MonthDayYear" MaskType="Date" CultureCurrencySymbolPlaceholder="" Enabled="True" TargetControlID="spkplanend" CultureAMPMPlaceholder="" CultureDateFormat="" CultureTimePlaceholder="" CultureDatePlaceholder=""></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w24" Enabled="True" TargetControlID="spkplanend" PopupButtonID="btnCalendar3" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD><TD style="WIDTH: 1031px" align=left colSpan=1></TD><TD style="WIDTH: 1031px" align=left colSpan=1></TD></TR><TR><TD align=left><asp:Label id="Label4" runat="server" Text="Status           " __designer:wfdid="w25"></asp:Label>&nbsp; </TD><TD align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:DropDownList id="spkstatus" runat="server" Width="120px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w26"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>Approval</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnReport" onclick="btnReport_Click1" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w27"></asp:ImageButton> <asp:ImageButton id="ToPDF" onclick="ToPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w28" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnExcel" onclick="btnExcel_Click1" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w29" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w30" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton></TD><TD align=center colSpan=1></TD><TD align=center colSpan=1></TD></TR><TR><TD style="HEIGHT: 61px" align=right colSpan=3>&nbsp;<BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" Width="350px" __designer:wfdid="w32" AutoDataBind="true" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False"></CR:CrystalReportViewer> </TD><TD style="HEIGHT: 61px" align=right colSpan=1></TD><TD style="HEIGHT: 61px" align=right colSpan=1></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>
</td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
        <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" Text="header"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px" colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" ImageUrl="~/Images/warn.png"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="Validasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnErrOK" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/ok.png" OnClick="btnErrOK_click"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" PopupDragHandleControlID="Validasi" PopupControlID="Panelvalidasi" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

