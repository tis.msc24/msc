<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnCashBon.aspx.vb" Inherits="Accounting_trncashbon" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Text=".: Bon Sementara" CssClass="Title" Font-Size="Large"></asp:Label></th>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel3" runat="server">
                                <contenttemplate>
<DIV style="WIDTH: 100%; TEXT-ALIGN: left"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD style="WIDTH: 14%" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w423"></asp:Label></TD><TD style="WIDTH: 1%" align=left>:</TD><TD align=left><asp:DropDownList id="FilterDDL" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w424"><asp:ListItem Value="bsno">B.S No</asp:ListItem>
<asp:ListItem Value="cb.cashbankno">Cash/Bank No.</asp:ListItem>
<asp:ListItem Value="p.personname">PIC</asp:ListItem>
<asp:ListItem Value="cb.cashbanknote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w425"></asp:TextBox></TD></TR><TR><TD align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w426"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=1><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w427"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w428"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w429"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w430"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w431"></asp:ImageButton>&nbsp;<asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w432"></asp:Label></TD></TR><TR><TD align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w433"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=1><asp:DropDownList id="FilterDDLStatus" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w434">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=1></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w435" OnClick="btnSearch_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" onclick="btnAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w436"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w437" OnClick="btnPrint_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w438"></asp:ImageButton></TD><TD align=left></TD><TD align=left colSpan=1></TD></TR><TR><TD align=left colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w439"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w440"></asp:Image> Please wait ... 
</ProgressTemplate>
</asp:UpdateProgress>&nbsp;&nbsp;&nbsp;&nbsp;<ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w441" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w442" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w443" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w444" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender>&nbsp;&nbsp;&nbsp;&nbsp; <asp:LinkButton id="lkbReceiptInProcess" runat="server" __designer:wfdid="w445"></asp:LinkButton></TD></TR><TR><TD align=left colSpan=3>&nbsp;<asp:GridView id="gvTRN" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w446" AllowSorting="True" PageSize="8" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="bsoid" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="bsoid" DataNavigateUrlFormatString="~\Accounting\trnCashBon.aspx?oid={0}" DataTextField="bsno" HeaderText="BS No." SortExpression="bsno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="bsdate" HeaderText="BS Date" SortExpression="bsdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="BS Account" SortExpression="acctgdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="PIC" SortExpression="personname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bsamt" HeaderText="BS Amt">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bsstatus" HeaderText="Status" SortExpression="bsstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note" SortExpression="note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" __designer:wfdid="w2" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("bsoid") %>' Visible="False" __designer:wfdid="w3"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" CssClass="Important" Text="Click button Find or View All to view data" __designer:wfdid="w447"></asp:Label></TD></TR></TBODY></TABLE></DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span><span style="font-size: 9pt"><strong> List Memorial Journal :.</strong></span></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 954px"><TBODY><TR><TD colSpan=3 rowSpan=3><asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w49" ActiveViewIndex="0"><asp:View id="View1" runat="server" __designer:wfdid="w50"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w51"></asp:Label></TD><TD align=left></TD><TD align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w52" Visible="False"></asp:Label>&nbsp; <asp:Label id="cashbankoid" runat="server" __designer:wfdid="w53" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="bsmstoid" runat="server" __designer:wfdid="w54" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label13" runat="server" Text="Cabang" __designer:wfdid="w55"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="outlet" runat="server" Width="175px" CssClass="inpText" __designer:wfdid="w56" AutoPostBack="True" OnSelectedIndexChanged="outlet_SelectedIndexChanged"></asp:DropDownList></TD><TD align=left>B.S&nbsp;No.</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="BSNo" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w57" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left>Cash/Bank No.&nbsp;<asp:Label id="Label4" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w58"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="cashbankno" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w59" Enabled="False"></asp:TextBox></TD><TD align=left>Date</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="cashbankdate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w60" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="ibDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w61"></asp:ImageButton>&nbsp;<asp:Label id="Label10" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w62"></asp:Label></TD></TR><TR><TD align=left>B.S No Manual</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="bsnomanual" runat="server" CssClass="inpText" __designer:wfdid="w63"></asp:TextBox></TD><TD align=left>B.S Account</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="bsaccountoid" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w64"></asp:DropDownList></TD></TR><TR><TD align=left>Payment Type</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="cashbanktype" runat="server" Width="123px" CssClass="inpText" __designer:wfdid="w65" AutoPostBack="True" OnSelectedIndexChanged="cashbanktype_SelectedIndexChanged"><asp:ListItem Value="BKK">CASH</asp:ListItem>
<asp:ListItem Value="BBK">BANK</asp:ListItem>
</asp:DropDownList></TD><TD align=left>Payment Account</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="acctgoid" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w66" AutoPostBack="True" OnSelectedIndexChanged="acctgoid_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD align=left>Ref No</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="cashbankrefno" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w67" MaxLength="30"></asp:TextBox></TD><TD align=left><asp:Label id="lblDueDate" runat="server" Text="Due Date" __designer:wfdid="w68"></asp:Label>&nbsp;<asp:Label id="lblWarnDueDate" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w69" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="lblSeptDueDate" runat="server" Text=":" __designer:wfdid="w70"></asp:Label></TD><TD align=left><asp:TextBox id="cashbankduedate" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w71" ToolTip="MM/DD/YYYY"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDueDate" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w72"></asp:ImageButton>&nbsp;<asp:Label id="lblInfoDueDate" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w73"></asp:Label></TD></TR><TR><TD align=left>PIC</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="personoid" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w74">
            </asp:DropDownList></TD><TD align=left>B.S Amount</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="cashbankamt" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w75"></asp:TextBox></TD></TR><TR><TD align=left>Header Note</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="cashbanknote" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w76" MaxLength="100"></asp:TextBox></TD><TD align=left>Status</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="cashbankstatus" runat="server" Width="80px" CssClass="inpTextDisabled" __designer:wfdid="w77" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left colSpan=6>&nbsp;<ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w78" TargetControlID="cashbankdate" PopupButtonID="imbDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w79" TargetControlID="cashbankdate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceDueDate" runat="server" __designer:wfdid="w80" TargetControlID="cashbankduedate" PopupButtonID="imbDueDate" Format="MM/dd/yyyy">
            </ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDueDate" runat="server" __designer:wfdid="w81" TargetControlID="cashbankduedate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear">
                </ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w82" TargetControlID="cashbankamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD align=left></TD><TD align=left>&nbsp;</TD><TD align=left></TD><TD align=left></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left>&nbsp; </TD><TD align=left></TD><TD style="WIDTH: 75px" align=left></TD><TD align=left></TD><TD style="WIDTH: 205px" align=left></TD><TD align=left colSpan=2>&nbsp;</TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left>Created By<asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w209"></asp:Label>&nbsp;On&nbsp;<asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w210"></asp:Label></TD></TR><TR><TD align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w211"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w212"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" align=left></TD></TR><TR><TD style="HEIGHT: 5px" align=left><asp:ImageButton id="BtnSave" onclick="BtnSave_Click" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w85"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w86" OnClick="BtnCancel_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w87"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnPosting" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w88" OnClick="BtnPosting_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" __designer:wfdid="w89" AlternateText="Show COA" OnClick="btnShowCOA_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnUnpost" runat="server" ImageUrl="~/Images/unpost.png" ImageAlign="AbsMiddle" __designer:wfdid="w90" Visible="False" AlternateText="Un Post"></asp:ImageButton></TD></TR><TR><TD align=left></TD></TR><TR><TD style="HEIGHT: 10px" align=center><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w91"><ProgressTemplate>
<asp:Image id="ImageWait" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w92"></asp:Image> Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView></TD></TR><TR></TR><TR></TR></TBODY></TABLE><TABLE style="FONT-SIZE: x-small; WIDTH: 100%; COLOR: #585858" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 13px" align=left>&nbsp;</TD></TR><TR><TD align=left>&nbsp;&nbsp; </TD></TR><TR><TD align=left><TABLE width="100%"><TBODY><TR><TD align=left colSpan=2>&nbsp;&nbsp;&nbsp; </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="BtnSave"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image39" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            &nbsp;<span style="font-size: 9pt"><strong>Form Bon Sementara :.</strong><span style="font-size: 8pt">&nbsp;</span></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblCaption">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upCOAPosting" runat="server">
        <contenttemplate>
<asp:Panel id="pnlCOAPosting" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblCOAPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting Rate :"></asp:Label> <asp:DropDownList id="DDLRateType" runat="server" Width="125px" CssClass="inpText" Font-Size="Medium" Font-Bold="True" AutoPostBack="True">
                                <asp:ListItem Value="Default">Rate Default</asp:ListItem>
                                <asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
                                <asp:ListItem Value="USD">Rate To USD</asp:ListItem>
                            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvCOAPosting" runat="server" Width="99%" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" CellPadding="4" OnRowDataBound="gvCOAPosting_RowDataBound">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="acctgcode" HeaderText="Account No.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdebet" HeaderText="Debet">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgcredit" HeaderText="Credit">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseCOAPosting" runat="server" OnClick="lkbCloseCOAPosting_Click">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideCOAPosting" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeCOAPosting" runat="server" TargetControlID="btnHideCOAPosting" PopupDragHandleControlID="lblCOAPosting" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlCOAPosting">
            </ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

