Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions


Partial Class rptPO
#Region "Variable"
    Inherits System.Web.UI.Page
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnrequestitemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnrequestitemoid=" & cbOid
                                dtView2.RowFilter = "trnrequestitemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblDO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtTbl2 As DataTable = Session("TblDOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                dtView2.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblDO") = dtTbl
                Session("TblDOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedure"

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 

    Public Sub initGroup()
        sSql = "select genoid, gendesc from ql_mstgen Where gengroup='ITEMGROUP' ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("ALL GROUP", "ALL GROUP"))
        FilterDDLGrup.SelectedValue = "ALL GROUP"
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("Maaf, Format tanggal salah!", 2)
            Exit Sub
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Maaf, Period 2 harus lebih besar dari Period 1!", 2)
            Exit Sub
        End If

        Try
            Dim namaPDF As String = "" : Dim sWhere As String = "" : Dim dWhere As String = ""
            If CBclose.Checked = True Then
                sWhere = " Where pom.cmpcode='" & CompnyCode & "' AND CONVERT(CHAR(20), pom.closetime,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "'"
                If cbSR.Checked Then
                    sWhere &= " AND pom.flagSR = 'Y'"
                End If

                vReport = New ReportDocument

                If Type.SelectedValue = "Summary" Then
                    If tipe = "excel" Then
                        vReport.Load(Server.MapPath("~\Report\rptPOCloseSumExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath("~\Report\rptPOCloseSum.rpt"))
                    End If
                    namaPDF = "PO_Close_Sum_"
                Else
                    If itemname.Text.Trim <> "" Then
                        sWhere &= " And exists (select x.itemoid from QL_podtl x inner join ql_mstitem y on x.itemoid=y.itemoid Where y.itemdesc like '%" & TcharNoTrim(itemname.Text) & "%' And y.merk like '%" & TcharNoTrim(itemname.Text) & "%' and x.trnbelimstoid=pom.trnbelimstoid)"
                    End If

                    If dono.Text <> "" Then
                        Dim sDono() As String = Split(PersonOid.Text, ";")
                        sSql &= " AND ("
                        For c1 As Integer = 0 To sDono.Length - 1
                            sSql &= " personoid = " & Integer.Parse((sDono(c1)))
                            If c1 < sDono.Length - 1 Then
                                sSql &= " OR "
                            End If
                        Next
                        sSql &= ")" : sWhere &= sSql
                    End If

                    If tipe = "excel" Then
                        vReport.Load(Server.MapPath("~\Report\rptPOCloseDtlExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath("~\Report\rptPOCloseDtl.rpt"))
                    End If
                    namaPDF = "PO_Close_Dtl_"
                End If

                cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                vReport.SetParameterValue("periode", dateAwal.Text & "-" & dateAkhir.Text)

            Else 'CBclose.Checked

                sWhere = " Where convert(char(10),pom.trnbelipodate,121) between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "'"

                If dono.Text <> "" Then
                    Dim sDono() As String = Split(PersonOid.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sDono.Length - 1
                        sSql &= " i.personoid = " & Integer.Parse((sDono(c1)))
                        If c1 < sDono.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")" : sWhere &= sSql
                End If

                If cbSR.Checked Then
                    sWhere &= " AND pom.flagSR = 'Y'"
                End If

                sWhere &= IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", " ", " AND i.itemgroupoid=" & FilterDDLGrup.SelectedValue & "") 
                sWhere &= IIf(ddlstatusPO.SelectedValue = "ALL", " ", " and pom.trnbelistatus = '" & ddlstatusPO.SelectedValue & "'")

                If rbPO.Checked = False Then
                    dWhere &= " Where pom.cmpcode='" & CompnyCode & "'"
                End If

                If statusDelivery.SelectedValue <> "ALL" Then
                    If rbPO.Checked = False Then
                        dWhere &= " AND pom.trnsjbelistatus='" & statusDelivery.SelectedValue.ToLower & "' "
                    End If
                End If

                If ddlPODO.SelectedValue <> "ALL" Then
                    If rbPO.Checked = False Then
                        dWhere &= " AND pom.StatusPO='" & ddlPODO.SelectedValue & "'"
                    End If
                End If

                If SoNo.Text <> "" Then
                    Dim sSoNo() As String = Split(PomstOid.Text, ";")
                    sSql = " AND ("
                    For c1 As Integer = 0 To sSoNo.Length - 1
                        sSql &= " pom.trnbelimstoid = " & (sSoNo(c1))
                        If c1 < sSoNo.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                    sWhere &= sSql
                End If

                If ItemName.Text <> "" Then
                    Dim sItem() As String = Split(ItemOId.Text, ";")
                    sSql = " AND ("
                    For c1 As Integer = 0 To sItem.Length - 1
                        sSql &= " pod.itemoid = " & Integer.Parse((sItem(c1)))
                        If c1 < sItem.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                    sWhere &= sSql
                End If

                If custcode.Text <> "" Then
                    Dim sItem() As String = Split(trnsuppoid.Text, ";")
                    sSql = " AND ("
                    For c1 As Integer = 0 To sItem.Length - 1
                        sSql &= " pom.trnsuppoid = " & Integer.Parse((sItem(c1)))
                        If c1 < sItem.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                    sWhere &= sSql
                End If

                vReport = New ReportDocument
                If rbPO.Checked = True Then
                    If Type.SelectedValue = "Summary" Then
                        If tipe = "excel" Then
                            vReport.Load(Server.MapPath("~\Report\rptPOSumExl.rpt"))
                        Else
                            vReport.Load(Server.MapPath("~\Report\rptPOSum.rpt"))
                        End If
                        namaPDF = "POSUM_"
                    Else
                        If tipe = "excel" Then
                            vReport.Load(Server.MapPath("~\Report\rptPODetailExl.rpt"))
                        Else
                            vReport.Load(Server.MapPath("~\Report\rptPODetail.rpt"))
                        End If
                        namaPDF = "PO_DETAIL_"
                    End If
                Else
                    If tipe = "excel" Then
                        vReport.Load(Server.MapPath("~\Report\rptStatus_PODO_EXL.rpt"))
                    Else
                        vReport.Load(Server.MapPath("~\Report\rptStatus_PO_DO.rpt"))
                    End If
                    namaPDF = "Status_PO_DO_"
                End If

                cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

                If rbPO.Checked = False Then
                    vReport.SetParameterValue("dWhere", dWhere)
                End If

                vReport.SetParameterValue("sPeriod", GetDateToPeriodAcctg(CDate(toDate(dateAkhir.Text))))
                vReport.SetParameterValue("startperiod", dateAwal.Text)
                vReport.SetParameterValue("endperiod", dateAkhir.Text)
            End If

            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindListMat()
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                showMessage("Period 2 must be more than Period 1 !", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Please check Period value", 2)
            Exit Sub
        End Try

        sSql = "SELECT 'False' AS checkvalue, i.itemoid, i.itemcode, i.itemdesc, i.itemcode, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' End JenisNya, i.itemgroupoid, 'Buah' satuan3, i.merk, i.stockflag, i.personoid, i.itemflag FROM QL_mstitem i Where itemoid IN (Select itemoid from QL_podtl d INNER JOIN QL_pomst mst ON d.trnbelimstoid=mst.trnbelimstoid Where convert(char(10), mst.trnbelipodate,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "'"
        If SoNo.Text <> "" Then
            Dim sSono() As String = Split(SoNo.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sSono.Length - 1
                sSql &= " mst.trnbelipono LIKE '%" & Tchar(sSono(c1)) & "%'"
                If c1 < sSono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If
        sSql &= ") ORDER BY itemdesc"
        Session("TblMat") = ckon.ambiltabel(sSql, "QL_mstitem")
    End Sub 

    Private Sub BindListSO()
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                showMessage("Period 2 must be more than Period 1 !", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Please check Period value", 2)
            Exit Sub
        End Try

        sSql = "SELECT 'False' AS checkvalue, mst.trnbelimstoid trnrequestitemoid, Convert(Varchar(20),mst.trnbelipodate,103) trnrequestitemdate, mst.trnbelipono trnrequestitemno, mst.branch_code branchcode, cb.gendesc, mst.trnbelistatus trnrequestitemstatus, mst.trnbelinote trnrequestitemnote, mst.typepo typeSO FROM QL_pomst mst INNER JOIN QL_mstgen cb ON cb.gencode=mst.branch_code AND cb.gengroup='CABANG' Where convert(char(10), mst.trnbelipodate,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "' ORDER BY mst.trnbelimstoid"
        Session("TblSO") = ckon.ambiltabel(sSql, "QL_trnordermst")
    End Sub

    Private Sub BindListCust()
        Try
            sSql = "SELECT 'False' AS checkvalue, suppoid custoid, suppcode custcode, suppname custname, suppaddr custaddr FROM QL_mstsupp c WHERE cmpcode='" & CompnyCode & "' AND c.suppoid IN (Select o.trnsuppoid from QL_pomst o Where o.trnsuppoid=c.suppoid AND convert(char(10), o.trnbelipodate,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "'"

            If SoNo.Text <> "" Then
                Dim sSono() As String = Split(SoNo.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " o.trnbelipono LIKE '%" & Tchar(sSono(c1)) & "%'"
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If

            sSql &= ") ORDER BY custcode"
            Session("TblCust") = ckon.ambiltabel(sSql, "QL_mstcust")
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub BindListDO()
        Try
            sSql = "SELECT DISTINCT 'False' AS checkvalue, dom.PERSONOID doitemmstoid, dom.PERSONNAME doitemno, dom.PERSONBIRTHDATE doitemdate, CONVERT(VARCHAR(10), dom.PERSONBIRTHDATE, 101) AS dodate, USERID AS custname, dom.STATUS doitemmststatus, PERSONGENDER doitemmstnote, USERNAME doitemcustpono, STATUSPROF doitemcustdo FROM QL_MSTPERSON dom INNER JOIN QL_MSTPROF dod ON dom.PERSONOID=dod.personnoid INNER JOIN QL_mstitem som ON som.personoid=dom.PERSONOID "
            'If Session("UserID").ToString.ToUpper <> "ADMIN" Or Session("UserID").ToString.ToUpper <> "POPY" Then
            '    sSql &= " AND dod.userid='" & Session("UserID") & "'"
            'End If

            sSql &= " ORDER BY dom.PERSONNAME ASC"
            Session("TblDO") = ckon.ambiltabel(sSql, "QL_trndoitemmst")
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
       
    End Sub
#End Region

#Region "Event"  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptPO.aspx")
        End If

        Page.Title = CompnyName & " - Laporan PO"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            initGroup()
            sSql = CType(("select count(USERPROF) from QL_USERROLE ur join QL_ROLE r on ur.ROLEOID = r.ROLEOID where ROLENAME = 'WH Spv' and USERPROF = '" & Session("userid") & "'"), String)
            If (ToDouble(GetStrData(sSql)) > 0) Then
                rbPO.Visible = False
                rbPOLPB.Checked = True
            End If
            dateAwal.Text = Format(New Date(GetServerTime.Year, Now.Month, 1), "dd/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptPO.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("excel")
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        showPrint("pdf")
    End Sub

    Protected Sub rbPO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPO.CheckedChanged, rbPOLPB.CheckedChanged
        If rbPO.Checked = True Then
            tdTipeLap1.Visible = True : tdTipeLap2.Visible = True
            LblPOcLosed.Visible = True : CBclose.Visible = True
            Label2.Visible = True : Label5.Visible = False
            statusDelivery.Visible = False
            Label7.Visible = False : ddlPODO.Visible = False
        Else
            statusDelivery.Visible = True
            Label2.Visible = False : Label5.Visible = True
            LblPOcLosed.Visible = False : CBclose.Visible = False
            tdTipeLap1.Visible = True : tdTipeLap2.Visible = False
            Label7.Visible = True : ddlPODO.Visible = True
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("EmptyListDO") Is Nothing And Session("EmptyListDO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListDO") Then
                Session("EmptyListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
        If Not Session("WarningListDO") Is Nothing And Session("WarningListDO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListDO") Then
                Session("WarningListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
    End Sub

    Protected Sub CBclose_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CBclose.CheckedChanged
        If CBclose.Checked = True Then
            ddlstatusPO.SelectedValue = "CLOSED MANUAL"
            ddlstatusPO.Enabled = False
        Else
            ddlstatusPO.SelectedValue = "ALL"
            ddlstatusPO.Enabled = True
        End If
        showPrint("")
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showPrint("")
    End Sub

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing
            gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        SoNo.Text = "" : PomstOid.Text = ""
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If

        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = DDLFilterListSO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListSO.Text) & "%'"
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnrequestitemoid=" & dtTbl.Rows(C1)("trnrequestitemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If SoNo.Text <> "" Then
                            SoNo.Text &= ";" + vbCrLf + dtView(C1)("trnrequestitemno")
                            PomstOid.Text &= ";" & dtView(C1)("trnrequestitemoid")
                        Else
                            SoNo.Text &= dtView(C1)("trnrequestitemno")
                            PomstOid.Text &= dtView(C1)("trnrequestitemoid")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "- Maaf, Silahkan pilih data dulu..!!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "- Maaf, Silahkan pilih data dulu..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnrequestitemoid=" & dtTbl.Rows(C1)("trnrequestitemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Maaf, Silahkan pilih nomer transaksi..!!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "8"
            gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        ItemName.Text = "" : ItemOId.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        Dim sPlus As String = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"

        If stockFlag.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag='" & stockFlag.SelectedValue & "'"
        End If

        'sPlus &= " AND itemflag='" & ddlStatus1.SelectedValue & "'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSO")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnALLSelectItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnALLSelectItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Maaf, Anda belum pilih katalog..!!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedItem.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Mohon maaf, data tidak ditemukan..!! !"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If ItemName.Text <> "" Then
                            ItemName.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                            ItemOId.Text &= ";" & dtView(C1)("itemoid")
                        Else
                            ItemName.Text = dtView(C1)("itemcode")
                            ItemOId.Text = dtView(C1)("itemoid")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing
            gvListCust.DataSource = Nothing : gvListCust.DataBind()
            cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = "" : trnsuppoid.Text = ""
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                            trnsuppoid.Text &= ";" & dtView(C1)("custoid")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                            trnsuppoid.Text = dtView(C1)("custoid")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub
#End Region

    Protected Sub imbFindDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindDO.Click
        If IsValidPeriod() Then
            DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
            Session("TblDO") = Nothing : Session("TblDOView") = Nothing
            gvListDO.DataSource = Nothing : gvListDO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDO.Click
        Try
            If Session("TblDO") Is Nothing Then
                BindListDO()
                If Session("TblDO").Rows.Count <= 0 Then
                    Session("EmptyListDO") = "DO data can't be found!"
                    showMessage(Session("EmptyListDO"), 2)
                    Exit Sub
                End If
            End If

            Dim sPlus As String = ""

            If DDLFilterListDO.SelectedValue = "doitemmstoid" Then
                sPlus = DDLFilterListDO.SelectedValue & " = '" & TcharNoTrim(txtFilterListDO.Text) & "'"
            Else
                sPlus = DDLFilterListDO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListDO.Text) & "%'"
            End If
            If PicCb.Checked Then
                sPlus &= "AND doitemmststatus='" & DDLStatus.SelectedValue & "'"
            End If
            If UpdateCheckedDO() Then
                Dim dv As DataView = Session("TblDO").DefaultView
                dv.RowFilter = sPlus
                If dv.Count > 0 Then
                    Session("TblDOView") = dv.ToTable
                    gvListDO.DataSource = Session("TblDOView")
                    gvListDO.DataBind()
                    dv.RowFilter = ""
                    mpeListDO.Show()
                Else
                    dv.RowFilter = ""
                    Session("TblDOView") = Nothing
                    gvListDO.DataSource = Session("TblDOView")
                    gvListDO.DataBind()
                    Session("WarningListDO") = "DO data can't be found!"
                    showMessage(Session("WarningListDO"), 2)
                End If
            Else
                mpeListDO.Show()
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try

    End Sub

    Protected Sub btnViewAllListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListDO.Click
        DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = "" : PicCb.Checked = False
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "DO data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedDO() Then
            Dim dt As DataTable = Session("TblDO")
            Session("TblDOView") = dt
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub

    Protected Sub btnSelectAllDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedDO.Click
        If Session("TblDO") Is Nothing Then
            Session("WarningListDO") = "Selected PIC data can't be found!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
        If UpdateCheckedDO() Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
                Session("TblDOView") = dtView.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dtView.RowFilter = ""
                mpeListDO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "Selected PIC data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListDO.Click
        If Not Session("TblDO") Is Nothing Then
            If UpdateCheckedDO() Then
                Dim dtTbl As DataTable = Session("TblDO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If dono.Text <> "" Then
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= ";" + vbCrLf + dtView(C1)("doitemno")
                                PersonOid.Text &= ";" & dtView(C1)("doitemmstoid")
                            End If
                        Else
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= dtView(C1)("doitemno")
                                PersonOid.Text &= dtView(C1)("doitemmstoid").ToString
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
                Else
                    Session("WarningListDO") = "Please select DO to add to list!"
                    showMessage(Session("WarningListDO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListDO.Click
        cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
    End Sub

    Protected Sub gvListDO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDO.PageIndexChanging
        If UpdateCheckedDO2() Then
            gvListDO.PageIndex = e.NewPageIndex
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub

    Protected Sub imbEraseDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseDO.Click
        dono.Text = ""
        PersonOid.Text = ""
    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub
End Class
