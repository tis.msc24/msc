Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptNotaExpedisi
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub ResetReport()
        Session("showReport") = False
        CrystalReportViewer1.ReportSource = Nothing
    End Sub

    Private Sub ddlCabang()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(BranchDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(BranchDDL, sSql)
            Else
                FillDDL(BranchDDL, sSql)
                BranchDDL.Items.Add("ALL")
                BranchDDL.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(BranchDDL, sSql)
            BranchDDL.Items.Add("ALL")
            BranchDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showPrint(ByVal ekstensi As String)
        Try
            Dim dat1 As Date = CDate(toDate(date1.Text))
            Dim dat2 As Date = CDate(toDate(date2.Text))
        Catch ex As Exception
            labelmsg.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(date1.Text)) > CDate(toDate(date2.Text)) Then
            labelmsg.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        CrystalReportViewer1.Visible = True
        Try
            Dim d1 As Date = CDate(toDate(date1.Text)) : Dim d2 As Date = CDate(toDate(date2.Text))
            Dim sWhere As String = "Where cmpcode='" & CompnyCode & "' AND trnbiayaeksdate>='" & CDate(toDate(date1.Text)) & " 0:0:0' AND trnbiayaeksdate<='" & CDate(toDate(date2.Text)) & " 23:59:00'"

            If BranchDDL.SelectedValue <> "ALL" Then
                sWhere &= "And branch_code='" & BranchDDL.SelectedValue & "'"
            End If

            If TypeNotaDDL.SelectedValue <> "ALL" Then
                sWhere &= " And typenota='" & TypeNotaDDL.SelectedValue & "'"
            End If

            If OidNota.Text <> "" Then
                sWhere &= " And trnbiayaeksoid=" & OidNota.Text & ""
            End If

            If CustOid.Text <> "" Then
                sWhere &= " And custoid=" & CustOid.Text & ""
            End If

            If DDLStaturRetur.SelectedValue <> "ALL" Then
                If DDLStaturRetur.SelectedValue <> "POST" Then
                    sWhere &= " And approvalstatus='" & DDLStaturRetur.SelectedValue & "'"
                Else
                    sWhere &= " And approvalstatus=''"
                End If
            End If
            If TypeNotaDDL.SelectedValue <> "NON EXPEDISI" Then
                If CbOId.Text <> "" Then
                    sWhere &= " And cashbankoid=" & CustOid.Text & ""
                End If
            End If
            vReport = New ReportDocument
            If ekstensi.ToUpper = "EXCEL" Then
                vReport.Load(Server.MapPath("~\Report\rptExpExl.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptExp.rpt"))
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)
            'CrystalReportViewer1.DisplayGroupTree = False
            vReport.SetParameterValue("d1", d1)
            vReport.SetParameterValue("d2", d2)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            vReport.SetParameterValue("sWhere", sWhere)
            If ekstensi = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
            Else
                If ekstensi = "PDF" Then
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                    vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
                Else
                    CrystalReportViewer1.DisplayGroupTree = False
                    CrystalReportViewer1.ReportSource = vReport
                End If
            End If
        Catch ex As Exception
            labelmsg.Text = ex.ToString
            Exit Sub
        End Try
    End Sub

    Private Sub BindCashBank()
        Try
            Dim vWhere As String = ""
            If OidNota.Text <> "" Then
                vWhere = "AND bm.trnbiayaeksoid=" & OidNota.Text & ""
            End If
            sSql = "Select cb.branch_code,cb.cashbankoid,cb.cashbankno,cashbanktype,ac.acctgdesc,acctgcode From QL_trncashbankmst cb Inner Join QL_mstacctg ac ON ac.acctgoid=cb.cashbankacctgoid Where cb.cashbankoid IN (Select bm.cashbankoid from ql_trnbiayaeksmst bm Where cb.cashbankoid=bm.cashbankoid AND cb.branch_code=bm.branch_code " & vWhere & " AND bm.trnbiayaeksdate>='" & CDate(toDate(date1.Text)) & " 0:0:0' AND bm.trnbiayaeksdate<='" & CDate(toDate(date2.Text)) & " 23:59:59') AND cb.flagexpedisi='POST' And cb.cashbankno like '%" & Tchar(CbNo.Text) & "%'"
            If BranchDDL.SelectedValue <> "ALL" Then
                sSql &= "And cb.branch_code='" & BranchDDL.SelectedValue & "'"
            End If

            sSql &= " Order by cashbankno"
            Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_Cbno")
            gvCbNo.DataSource = dt : gvCbNo.DataBind()
            gvCbNo.Visible = True
        Catch ex As Exception
            labelmsg.Text = ex.ToString & "<br />" & sSql
            Exit Sub
        End Try
    End Sub

    Private Sub BindNotaNya()
        Try
            sSql = "Select bem.branch_code,bem.trnbiayaeksoid,trnbiayaeksno,cust.custname,bem.typenota,cb.gendesc CabangNya From ql_trnbiayaeksmst bem Inner Join QL_mstcust cust ON cust.custoid=bem.custoid AND cust.branch_code=bem.branch_code Inner Join QL_mstgen cb ON cb.gencode=bem.branch_code AND cb.gengroup='CABANG' Where trnbiayaeksno LIKE '%" & Tchar(NotaNya.Text) & "%' AND trnbiayaeksdate>='" & CDate(toDate(date1.Text)) & " 0:0:0' AND trnbiayaeksdate<='" & CDate(toDate(date2.Text)) & " 23:59:59'"
            If BranchDDL.SelectedValue <> "ALL" Then
                sSql &= "And bem.branch_code='" & BranchDDL.SelectedValue & "'"
            End If

            If TypeNotaDDL.SelectedValue <> "ALL" Then
                sSql &= "And bem.typenota='" & TypeNotaDDL.SelectedValue & "'"
            End If

            sSql &= " Order by trnbiayaeksno"
            Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_Nota")
            GvNota.DataSource = dt : GvNota.DataBind()
            GvNota.Visible = True
        Catch ex As Exception
            labelmsg.Text = ex.ToString & "<br />" & sSql
            Exit Sub
        End Try
    End Sub

    Private Sub BindCustomer()
        Try
            Dim vWhere As String = ""
            If OidNota.Text <> "" Then
                vWhere = "AND bm.trnbiayaeksoid=" & OidNota.Text & ""
            End If
            sSql = "Select cust.custoid,custcode,cust.custname,cust.custaddr From QL_mstcust cust Where cust.custoid IN (Select bm.custoid from ql_trnbiayaeksmst bm Where cust.custoid=bm.custoid AND cust.branch_code=bm.branch_code AND bm.trnbiayaeksdate>='" & CDate(toDate(date1.Text)) & " 0:0:0' AND bm.trnbiayaeksdate<='" & CDate(toDate(date2.Text)) & " 23:59:59' " & vWhere & ") AND (custcode LIKE '%" & Tchar(CustName.Text) & "%' OR custname LIKE '%" & Tchar(CustName.Text) & "%')"

            If BranchDDL.SelectedValue <> "ALL" Then
                sSql &= "And cust.branch_code='" & BranchDDL.SelectedValue & "'"
            End If

            sSql &= " Order by cust.custname"
            Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_Customer")
            GVCustomer.DataSource = dt : GVCustomer.DataBind()
            GVCustomer.Visible = True
        Catch ex As Exception
            labelmsg.Text = ex.ToString & "<br />" & sSql
            Exit Sub
        End Try
        
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptNotaExpedisi.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Laporan Nota Expedisi"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        If Not Page.IsPostBack Then 
            ddlCabang()
            TypeNotaDDL_SelectedIndexChanged(Nothing, Nothing)
            date1.Text = Format(GetServerTime(), "01/MM/yyyy")
            date2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub detailstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ResetReport()
    End Sub

    Protected Sub imbExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("EXCEL")
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("PDF")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptNotaExpedisi.aspx?awal=true")
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("")
    End Sub

    Protected Sub BtnViewRpt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnViewRpt.Click
        showPrint("")
    End Sub

    Protected Sub GvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvNota.PageIndexChanging
        GvNota.PageIndex = e.NewPageIndex
        BindNotaNya()
    End Sub

    Protected Sub GvNota_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvNota.SelectedIndexChanged
        NotaNya.Text = GvNota.SelectedDataKey.Item("trnbiayaeksno").ToString
        OidNota.Text = Integer.Parse(GvNota.SelectedDataKey.Item("trnbiayaeksoid"))
        GvNota.Visible = False
    End Sub

    Protected Sub GVCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVCustomer.PageIndexChanging
        GVCustomer.PageIndex = e.NewPageIndex
        BindCustomer()
    End Sub

    Protected Sub GVCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVCustomer.SelectedIndexChanged
        CustName.Text = GVCustomer.SelectedDataKey.Item("custname").ToString
        CustOid.Text = Integer.Parse(GVCustomer.SelectedDataKey.Item("custoid"))
        GVCustomer.Visible = False
    End Sub

    Protected Sub sCusTomer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sCusTomer.Click
        BindCustomer()
    End Sub

    Protected Sub eCusTomer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eCusTomer.Click
        CustName.Text = "" : CustOid.Text = ""
        GVCustomer.Visible = False
        ResetReport()
    End Sub

    Protected Sub sNotaNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sNotaNo.Click
        BindNotaNya()
    End Sub

    Protected Sub eNotanya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eNotanya.Click
        NotaNya.Text = "" : OidNota.Text = ""
        GvNota.Visible = False
        ResetReport()
    End Sub

    Protected Sub TypeNotaDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TypeNotaDDL.SelectedIndexChanged
        If TypeNotaDDL.SelectedValue = "NON EXPEDISI" Then
            noCB.Visible = False : eCbNo.Visible = False
            CbNo.Visible = False : sCbNo.Visible = False
            Label9.Visible = False
        Else
            noCB.Visible = True : eCbNo.Visible = True
            CbNo.Visible = True : sCbNo.Visible = True
            Label9.Visible = True
        End If
    End Sub

    Protected Sub sCbNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sCbNo.Click
        BindCashBank()
    End Sub

    Protected Sub gvCbNo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCbNo.PageIndexChanging
        gvCbNo.PageIndex = e.NewPageIndex
        BindCashBank()
    End Sub

    Protected Sub gvCbNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCbNo.SelectedIndexChanged
        CbNo.Text = gvCbNo.SelectedDataKey.Item("cashbankno").ToString
        CbOId.Text = Integer.Parse(gvCbNo.SelectedDataKey.Item("cashbankoid"))
        gvCbNo.Visible = False
    End Sub

    Protected Sub eCbNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eCbNo.Click
        CbNo.Text = "" : CbOId.Text = ""
        gvCbNo.Visible = False
    End Sub
#End Region
End Class
