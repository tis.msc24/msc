<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptTrialBalance.aspx.vb" Inherits="reportForm_rptTrialBalance" title="Untitled Page" %>

<%--<%@ Register Src="../QLMsgBox.ascx" TagName="QLMsgBox" TagPrefix="uc1" %>
--%>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias">
        <tr>
            <th align="left" class="header" style="width: 975px" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Text=".: Trial Balance Report" Font-Size="X-Large"></asp:Label></th>
        </tr>
         <tr align=center>
    <td align=center> 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE><TBODY><TR><TD vAlign=top colSpan=1><asp:Label id="Label2" runat="server" Text="Cabang" __designer:wfdid="w3"></asp:Label></TD><TD vAlign=top colSpan=1><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w4"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:DropDownList id="DDLcbg" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w5"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD vAlign=top colSpan=1><asp:Label id="Label12" runat="server" Text="Periode" __designer:wfdid="w6"></asp:Label></TD><TD vAlign=top colSpan=1><asp:Label id="Label19" runat="server" Text=":" __designer:wfdid="w7"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:TextBox id="tbPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w8"></asp:TextBox>&nbsp;<asp:ImageButton id="ibcal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w10"></asp:Label> <asp:TextBox id="tbPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w11"></asp:TextBox> <asp:ImageButton id="ibcal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w12"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w13"></asp:Label></TD></TR><TR><TD vAlign=bottom align=left colSpan=4><asp:ImageButton id="btnViewPrint" onclick="btnViewPrint_Click" runat="server" ImageUrl="~/Images/viewreport.png" __designer:wfdid="w14"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" onclick="btnExportToPdf_Click1" runat="server" ImageUrl="~/Images/topdf.png" __designer:wfdid="w15"></asp:ImageButton> <asp:ImageButton id="ExportToXls" onclick="ExportToXls_Click1" runat="server" ImageUrl="~/Images/toexcel.png" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibClear" onclick="ibClear_Click1" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w17"></asp:ImageButton><BR />&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD vAlign=top align=center colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w18"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w19"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibcal1" TargetControlID="tbPeriod1" __designer:wfdid="w20"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibcal2" TargetControlID="tbPeriod2" __designer:wfdid="w21"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="tbPeriod1" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w22"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" TargetControlID="tbPeriod2" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w23"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top colSpan=4><asp:UpdatePanel id="upPopUpMsg" runat="server" __designer:wfdid="w24"><ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w25" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w26"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w27"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w28"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" __designer:wfdid="w30" DropShadow="True" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w31" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" Width="350px" Height="50px" __designer:dtid="562949953421322" DisplayGroupTree="False" AutoDataBind="true" __designer:wfdid="w34"></CR:CrystalReportViewer> 
</ContentTemplate>
        <triggers>
<asp:PostBackTrigger ControlID="CrystalReportViewer1"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnViewPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ExportToXls"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    </td>
    </tr>
    </table>
   
   
</asp:Content>
