<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptAnalisaBiaya.aspx.vb" Inherits="ReportForm_RptAnalisaBiaya" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Analisa Biaya Report" CssClass="Title" ForeColor="Navy" Width="512px" Font-Size="X-Large"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" colspan="2" style="background-color: #ffffff" valign="center">
                <asp:UpdatePanel id="upReportForm" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" __designer:wfdid="w124" DefaultButton="btnViewReport"><TABLE><TBODY><TR><TD align=left runat="server">Cabang</TD><TD class="Label" align=center runat="server">:</TD><TD align=left colSpan=2 runat="server"><asp:DropDownList id="CbgDDL" runat="server" CssClass="inpText" __designer:wfdid="w125"></asp:DropDownList></TD></TR><TR><TD align=left runat="server">Type</TD><TD class="Label" align=center runat="server">:</TD><TD align=left colSpan=2 runat="server"><asp:DropDownList id="ddlType" runat="server" CssClass="inpText" __designer:wfdid="w126" AutoPostBack="True"><asp:ListItem>Analisa</asp:ListItem>
<asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD1" align=left runat="server" Visible="false"></TD><TD id="TD2" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD3" align=left colSpan=2 runat="server" Visible="false"><asp:TextBox id="txtStart" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w127"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w128"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w129"></asp:Label> <asp:TextBox id="txtFinish" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w130"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w131"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(dd/mm/yyyy)" __designer:wfdid="w132"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w133"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left colSpan=2><asp:DropDownList id="DDLMonth2" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w134">
            </asp:DropDownList>&nbsp;<asp:DropDownList id="DDLYear2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w135">
            </asp:DropDownList></TD></TR><TR><TD id="TD8" align=left runat="server" Visible="false">Status</TD><TD id="TD7" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD9" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="ddlstatus" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w136"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD6" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Width="96px" Text="Currency" __designer:wfdid="w137"></asp:Label></TD><TD id="TD4" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD5" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText" __designer:wfdid="w138"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" onclick="btnViewReport_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w139"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w140"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w141"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w142"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w143" Format="dd/MM/yyyy" PopupButtonID="imbStart" TargetControlID="txtStart"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceFin" runat="server" __designer:wfdid="w144" Format="dd/MM/yyyy" PopupButtonID="imbFinish" TargetControlID="txtFinish"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" __designer:wfdid="w145" TargetControlID="txtStart" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" __designer:wfdid="w146" TargetControlID="txtFinish" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 60px" vAlign=top align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w147" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w151"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvReportForm" runat="server" Width="350px" __designer:wfdid="w149" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer></asp:Panel> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w40" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w41"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w42"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w43"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w45" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w46" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

