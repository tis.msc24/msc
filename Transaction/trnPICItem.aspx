﻿ <%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPICItem.aspx.vb" Inherits="Transaction_trnPIC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">

    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Update PIC Item" CssClass="Title" ForeColor="Navy" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" valign="top" style="text-align: left;">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
<ajaxToolkit:TabPanel runat="server" ID="TabPanel2"><HeaderTemplate>
                            <asp:Image ID="Image221" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Update New PIC Item :.</span></strong>
                        
</HeaderTemplate>
<ContentTemplate>
                            <div style="WIDTH: 100%; TEXT-ALIGN: center">
                                <div style="width: 100%; text-align: left">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 100px" class="Label" align=left><asp:CheckBox id="CheckBox1" runat="server" Width="95px" Visible="False" AutoPostBack="True"></asp:CheckBox></TD><TD class="Label" align=left><asp:FileUpload id="FileUpload1" runat="server" Width="222px" CssClass="inpText" Visible="False"></asp:FileUpload><asp:Button id="BtnUplaodFile" runat="server" Text="Upload" Visible="False"></asp:Button> <asp:Label id="FileExelInitStokSN" runat="server" Visible="False"></asp:Label> <asp:Label id="Label2" runat="server" Text="Init SN" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" id="TD12" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label9" runat="server" Text="Warehouse"></asp:Label> </TD><TD id="TD11" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLLocation" runat="server" Width="300px" CssClass="inpText"></asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 100px" id="TD14" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label5" runat="server" Text="Group"></asp:Label> </TD><TD id="TD13" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLBusUnit" runat="server" Width="300px" CssClass="inpText"></asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 100px" class="Label" align=left><asp:ImageButton id="btnLookUpMat" runat="server" ImageUrl="~/Images/find.png"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Width="182px" Font-Size="8pt" Font-Bold="True" ForeColor="Red" Text="-- Click Find to List Item !!"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:GridView id="gvDtl" runat="server" Width="100%" ForeColor="#333333" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" DataKeyNames="seq">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowEditButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Sienna"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No." ReadOnly="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Code" ReadOnly="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="longdesc" HeaderText="Description" ReadOnly="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="PIC">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=2>Processed By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> &nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label> </TD></TR><TR><TD class="Label" align=left colSpan=2><asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:ImageButton style="FONT-WEIGHT: 700" id="btnPreview" runat="server" ImageAlign="AbsMiddle" Visible="False" AlternateText=">> Upload excel example <<"></asp:ImageButton> </TD></TR><TR><TD class="Label" align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>&nbsp; 
</contenttemplate>
                                                    <triggers>
<asp:AsyncPostBackTrigger ControlID="btnFindListMat"></asp:AsyncPostBackTrigger>
</triggers>
                                                </asp:UpdatePanel></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        
</ContentTemplate>
</ajaxToolkit:TabPanel>
</ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <%-- <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>--%><asp:UpdatePanel ID="UPListMat" runat="server">
            <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Item</asp:Label></TD></TR><TR><TD align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="matcode">Code</asp:ListItem>
<asp:ListItem Value="longdesc">Description</asp:ListItem>
<asp:ListItem Value="personname">PIC</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton style="HEIGHT: 23px" id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3>PIC : <asp:DropDownList id="DDLPIC" runat="server" Width="192px" CssClass="inpText"></asp:DropDownList> <asp:Label id="Label3" runat="server" ForeColor="Red" Text="* Update/Ganti PIC apabila masih belum sesuai!" Font-Overline="False" Font-Italic="True"></asp:Label></TD></TR><TR><TD align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" align=center>Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD style="HEIGHT: 100%" align=center colSpan=3><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 349px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 325px"><asp:GridView id="gvListMat" runat="server" Width="100%" Font-Bold="False" ForeColor="#333333" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbHdrLM" runat="server" AutoPostBack="true" OnCheckedChanged="cbHdrLM_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("matoid") %>' __designer:wfdid="w1"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="longdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="PIC"><ItemTemplate>
<asp:DropDownList id="personoid" runat="server" Width="192px" CssClass="inpText" ToolTip='<%# eval("personoid") %>' __designer:wfdid="w2"></asp:DropDownList> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="personoid" HeaderText="PICOID" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                <asp:Label ID="Label10" runat="server" Font-Bold="True" ForeColor="Red" Text="No Data Found !!"></asp:Label>
                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 18px" align=center colSpan=3><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllToList" runat="server" Visible="False">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
            <triggers>
<asp:AsyncPostBackTrigger ControlID="btnFindListMat"></asp:AsyncPostBackTrigger>
</triggers>
        </asp:UpdatePanel>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" TargetControlID="bePopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

