<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstSARole.aspx.vb" Inherits="Master_saRole" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <script type="text/javascript">
        function s()
        {/*
    try {
        var t = document.getElementById("<%=GVmst.ClientID%>");
        var t2 = t.cloneNode(true)
        for(i = t2.rows.length -1;i > 0;i--)
            t2.deleteRow(i)
        t.deleteRow(0)
        divTblData.appendChild(t2)
    }
    catch(errorInfo) {}
    
    }*/
    window.onload = s
    </script>

    <script type="text/jscript" language="javascript">
        function SelectAllCheckboxes(spanChk){

            // Added as ASPX uses SPAN for checkbox
            var oItem = spanChk.children;
            var theBox= (spanChk.type=="checkbox") ? 
                spanChk : spanChk.children.item[0];
            xState=theBox.checked;
            elm=theBox.form.elements;

            for(i=0;i<elm.length;i++)
                if(elm[i].type=="checkbox" && 
                         elm[i].id!=theBox.id)
                {
                    //elm[i].click();
                    if(elm[i].checked!=xState)
                        elm[i].click();
                    //elm[i].checked=xState;
                }
        }
    </script>
    <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0"
        width="100%" class="tabelhias">
        <tr style="font-size: 8pt; color: #000099">
            <th class="header" colspan="3" valign="middle">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Data Sales Activity Role"></asp:Label><asp:SqlDataSource ID="SDSData" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                        DeleteCommand="DELETE FROM [QL_SaUserRole] WHERE [CMPCODE] = @CMPCODE AND SAUSERID = @USERPROF"
                        
                        InsertCommand="INSERT INTO [QL_SaUserRole] ([CMPCODE], SAUSERID, PROFID, [USERPROF], [UPDUSER], [UPDTIME], [SPECIAL]) VALUES (@CMPCODE, @USERROLEOID, @ROLEOID, @USERPROF, @UPDUSER, @UPDTIME, @SPECIAL)"
                        
                        SelectCommand="SELECT distinct  [CMPCODE], SAUSERID, PROFID, [USERPROF], [UPDUSER], [UPDTIME],[SPECIAL] FROM [QL_SaUserRole] WHERE (([CMPCODE] = @CMPCODE) AND ([USERPROF] = @USERPROF))"
                        
                        UpdateCommand="UPDATE [QL_SaUserRole] SET [ROLEOID] = @ROLEOID, [USERPROF] = @USERPROF, [UPDUSER] = @UPDUSER, [UPDTIME] = @UPDTIME,[SPECIAL]=@SPECIAL WHERE [CMPCODE] = @CMPCODE AND [USERROLEOID] = @USERROLEOID" ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="JPT" Name="CMPCODE" Type="String" />
                            <asp:Parameter DefaultValue="" Name="USERPROF" Type="String" />
                        </SelectParameters>
                        <DeleteParameters>
                            <asp:Parameter Name="CMPCODE" Type="String" />
                            <asp:Parameter Name="USERPROF" />
                        </DeleteParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="ROLEOID" Type="Int32" />
                            <asp:Parameter Name="USERPROF" Type="String" />
                            <asp:Parameter Name="UPDUSER" Type="String" />
                            <asp:Parameter Name="UPDTIME" Type="String" />
                            <asp:Parameter Name="SPECIAL" />
                            <asp:Parameter Name="CMPCODE" Type="String" />
                            <asp:Parameter Name="USERROLEOID" Type="Int32" />
                        </UpdateParameters>
                        <InsertParameters>
                            <asp:Parameter Name="CMPCODE" Type="String" />
                            <asp:Parameter Name="USERROLEOID" Type="Int32" />
                            <asp:Parameter Name="ROLEOID" Type="Int32" />
                            <asp:Parameter Name="USERPROF" Type="String" />
                            <asp:Parameter Name="UPDUSER" Type="String" />
                            <asp:Parameter Name="UPDTIME" Type="String" />
                            <asp:Parameter Name="SPECIAL" />
                        </InsertParameters>
                    </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSOid" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                    SelectCommand="SELECT [cmpcode], [tablename], [lastoid], [tablegroup] FROM [QL_mstoid] WHERE (([tablename] = @tablename) AND ([cmpcode] = @cmpcode))"
                    UpdateCommand="UPDATE QL_mstoid SET lastoid = @lastoid WHERE (tablename = @tablename) AND (cmpcode = @cmpcode)" ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="QL_SaUserRole" Name="tablename" Type="String" />
                        <asp:Parameter DefaultValue="" Name="cmpcode" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="lastoid" />
                        <asp:Parameter Name="tablename" />
                        <asp:Parameter Name="cmpcode" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSUserProf" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                    SelectCommand="SELECT DISTINCT [CMPCODE], [USERID], [USERNAME], [PROFPASS], [APPROVALLIMIT], [STATUSPROF] FROM [QL_MSTPROF] WHERE ([STATUSPROF] = @STATUSPROF) AND ([CMPCODE] = @CMPCODE) AND USERID NOT IN (SELECT DISTINCT USERPROF FROM QL_SaUserRole)" ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Active" Name="STATUSPROF" Type="String" />
                        <asp:Parameter DefaultValue="JPT" Name="CMPCODE" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSRole" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                    SelectCommand="SELECT DISTINCT [CMPCODE], [ROLEOID], [ROLENAME], [DESCRIPTION] FROM [QL_ROLE] WHERE ([CMPCODE] = @CMPCODE)" ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="JPT" Name="CMPCODE" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSDataView" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                    SelectCommand="SELECT DISTINCT QL_SaUserRole.CMPCODE, QL_SaUserRole.USERPROF, QL_MSTPROF.USERNAME FROM QL_SaUserRole INNER JOIN QL_MSTPROF ON QL_SaUserRole.USERPROF = QL_MSTPROF.USERID WHERE (QL_SaUserRole.CMPCODE = @CMPCODE) AND (QL_MSTPROF.CMPCODE = @CMPCODE) AND (QL_SaUserRole.UPDUSER LIKE @upduser) AND (QL_SaUserRole.USERPROF LIKE @USERPROF) AND (QL_MSTPROF.USERNAME LIKE @USERLONGNAME)" ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="JPT" Name="CMPCODE" />
                        <asp:Parameter Name="upduser" />
                        <asp:Parameter Name="USERPROF" />
                        <asp:Parameter Name="USERLONGNAME" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px"></td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="350px" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <table>
                                    <tr>
                                        <td>
                                            <table align="center" border="0">
                                                <tr>
                                                    <td align="left" style="text-align: center" valign="middle" class="Label">Filter :</td>
                                                    <td style="text-align: left" valign="middle">
                                                        <asp:DropDownList ID="FilterDDL" runat="server" Width="75px" CssClass="inpText">
                                                            <asp:ListItem Value="Prof">User ID</asp:ListItem>
                                                            <asp:ListItem Value="Name">Full Name</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td style="text-align: left" valign="middle">
                                                        <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="150px" CssClass="inpText" Height="16px"></asp:TextBox></td>
                                                    <td style="text-align: left" valign="top">
                                                        <asp:ImageButton
                                                            ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" />
                                                    </td>
                                                    <td style="text-align: left" valign="top">
                                                        <asp:ImageButton
                                                            ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="height: 10px; text-align: center" valign="middle"></td>
                                                    <td style="height: 10px; text-align: left" valign="middle"></td>
                                                    <td style="height: 10px; text-align: left" valign="middle"></td>
                                                    <td style="height: 10px; text-align: left" valign="top"></td>
                                                    <td style="height: 10px; text-align: left" valign="top"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <fieldset id="field1" style="height: 275px; width: 99%; text-align: left; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                                <div id='divTblData'></div>
                                <div style="overflow-y: scroll; height: 275px; width: 100%;">
                                    <asp:GridView ID="GVmst" runat="server" CellPadding="4" AutoGenerateColumns="False" BackColor="White"
                                        BorderColor="#DEDFDE" BorderWidth="1px" Width="98%" BorderStyle="Solid" DataSourceID="SDSDataView" AllowSorting="True" AllowPaging="True">
                                        <FooterStyle BackColor="#F25407" />
                                        <SelectedRowStyle BackColor="#F25407" ForeColor="White" Font-Bold="True" />
                                        <PagerStyle ForeColor="Black" HorizontalAlign="Right" />
                                        <HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="User ID" SortExpression="USERPROF">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LBOid" runat="server" Text='<%# Eval("USERPROF", "{0}") %>' OnClick="LBOid_Click"></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="Navy" Width="20%" Font-Size="X-Small" HorizontalAlign="Left" />
                                                <ItemStyle Width="20%" HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="USERNAME" HeaderText="Full Name" SortExpression="USERNAME">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="80%" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" ForeColor="Navy" Width="80%" />
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle BackColor="#F7F7DE" />
                                        <EmptyDataTemplate>
                                            <asp:Label ID="lblmsg" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                </div>
                            </fieldset>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>:: List of Sales Activity Role</strong></span>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td colspan="3">
                                        <table id="Table3" align="center" width="475">
                                            <tr>
                                                <td style="width: 30%; text-align: left">
                                                    <span style="font-size: 10pt">User ID</span></td>
                                                <td class="Label" style="text-align: left; font-size: 10pt;">
                                                    <asp:DropDownList ID="userID" runat="server" CssClass="inpText"
                                                        DataTextField="ROLENAME" DataValueField="ROLEOID" Font-Size="X-Small" Width="173px" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblNama" runat="server" Visible="False"></asp:Label>
                                                    <asp:Label ID="lblID" runat="server" Visible="False"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="font-size: 8pt">
                                                <td style="width: 30%; text-align: left; height: 22px;">
                                                    <span style="font-size: x-small"><span style="font-size: 10pt">User Name</span></span></td>
                                                <td style="text-align: left; font-size: 10pt; height: 22px;">
                                                    <asp:DropDownList ID="userTargetoid" runat="server" CssClass="inpText"
                                                        DataTextField="ROLENAME" DataValueField="ROLEOID" Font-Size="X-Small" Width="173px">
                                                    </asp:DropDownList>
                                                    <asp:ImageButton ID="ibtn2" runat="server" Height="20px" ImageAlign="AbsMiddle" ImageUrl="~/Images/Add.png"
                                                       Width="20px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="TD1" runat="server" style="width: 30%; text-align: left" visible="False">
                                                    <span style="font-size: x-small"><span style="font-size: 10pt">Special Permissi</span><span
                                                        style="font-size: 8pt">on</span></span></td>
                                                <td id="TD2" runat="server" class="Label" style="text-align: left; font-size: 8pt;" visible="False">
                                                    <asp:CheckBox ID="Special" runat="server" Checked="True" Visible="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 30%; text-align: left">
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to delete this data??"
                                                        Enabled="True" TargetControlID="BtnDelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </td>
                                                <td style="text-align: left">
                                                    <ajaxToolkit:ListSearchExtender ID="ListSearchExtender2" runat="server" Enabled="True"
                                                        PromptCssClass="ListSearchExtenderPrompt" TargetControlID="userTargetoid">
                                                    </ajaxToolkit:ListSearchExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="vertical-align: top; text-align: left">
                                                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                                        <ContentTemplate>
<TABLE style="WIDTH: 300px"><TBODY><TR><TD colSpan=2><FIELDSET style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 175px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="field1"><DIV id="divTblData"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 175px"><asp:GridView style="BACKGROUND-COLOR: transparent" id="GVRole2" runat="server" Width="96%" BackColor="White" BorderWidth="1px" BorderColor="#DEDFDE" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w27">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:BoundField DataField="ROLENAME" HeaderText="Name">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Delete"><HeaderTemplate>
                                                                                                
</HeaderTemplate>
<ItemTemplate>
                                                                                                    <asp:CheckBox ID="cbDeleteRole" runat="server"></asp:CheckBox>
                                                                                                
</ItemTemplate>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#F7F7DE" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                                                                                            <asp:Label ID="lblNodataShipping" runat="server" CssClass="Important" Text="No data found  !!"></asp:Label>
                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#6B696B" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD></TD><TD align=right><asp:Label id="I_U2_2" runat="server" ForeColor="Red" Text="New Detail" Visible="False" __designer:wfdid="w28"></asp:Label><asp:ImageButton id="BtnRemoveRole2" onclick="BtnRemoveRole2_Click1" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                                                        <Triggers>
<asp:AsyncPostBackTrigger ControlID="ibtn2" EventName="Click"></asp:AsyncPostBackTrigger>
</Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="vertical-align: top; color: #585858; text-align: left">
                                                    <span style="font-size: x-small">Last Update On</span>
                                                    <asp:Label ID="UpdTime" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="#585858"
                                                        Text='<%# Eval("UPDTIME", "{0:G}") %>'></asp:Label>
                                                    <span style="font-size: x-small">by</span>
                                                    <asp:Label ID="UpdUser" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="#585858"
                                                        Text='<%# Eval("UPDUSER", "{0}") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:ImageButton ID="btnSave" runat="server" CommandName="update" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/Save.png" />
                                                    <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/Cancel.png" OnClick="btnCancel_Click2" />
                                                    <asp:ImageButton ID="BtnDelete" runat="server" CommandName="Delete" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/Delete.png" OnClick="BtnDelete_Click" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>:: Form Sales Activity Role</strong></span>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" align="left" colspan="2">
                            <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 5px" align="left"></td>
                        <td style="HEIGHT: 5px" align="left"></td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></td>
                        <td align="left">
                            <asp:Label ID="lblPopUpMsg" runat="server" CssClass="Important"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 5px" align="left"></td>
                        <td style="HEIGHT: 5px" align="left"></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" align="left" colspan="2">
                            <asp:ImageButton ID="imbPopUpOK" OnClick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

