﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnBiayaEks.aspx.vb" Inherits="Accounting_trnBiayaEks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Biaya Ekspedisi"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="1" style="width: 100%">
        <tr>
            <td align="left">
              <ajaxToolkit:TabContainer ID="TxtLunas" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel3" runat="server" Width="100%" __designer:wfdid="w26" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="Cabang" runat="server" Text="Cabang" __designer:wfdid="w27"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="fCabang" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w28"></asp:DropDownList></TD></TR><TR><TD align=left>Type Nota</TD><TD align=left colSpan=3><asp:DropDownList id="DDLTypeNota" runat="server" Width="120px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w29"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>NON EXPEDISI</asp:ListItem>
<asp:ListItem>EXPEDISI</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Periode</TD><TD align=left colSpan=3><asp:TextBox id="txtPeriode1" runat="server" Width="80px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w30"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="8pt" Text="to" __designer:wfdid="w9"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="80px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w33"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Filter</TD><TD align=left colSpan=3><asp:DropDownList id="ddlFilter" runat="server" Width="110px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w35"><asp:ListItem Value="trnbiayaeksno">No. Biaya</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="cashbankno">No. Bank</asp:ListItem>
<asp:ListItem Value="typenota">Type Nota</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w36"></asp:TextBox>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Status</TD><TD align=left colSpan=3><asp:DropDownList id="postinge" runat="server" Width="110px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w37"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="In Process">In Process</asp:ListItem>
<asp:ListItem Value="POST">POST</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><asp:GridView id="GVmstEkp" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w17" OnSelectedIndexChanged="GVmstEkp_SelectedIndexChanged" GridLines="None" DataKeyNames="branch_code,trnbiayaeksoid,trnbiayaeksno" CellPadding="4" AutoGenerateColumns="False" OnPageIndexChanging="GVmstEkp_PageIndexChanging" AllowPaging="True" PageSize="8" OnRowCommand="gridCommand" EnableModelValidation="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbiayaeksno" HeaderText="No. Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbiayaeksdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtnt" HeaderText="Amt. NT">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bayaridr" HeaderText="Bayar NT">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="returnt" HeaderText="Rtr. NT">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cnnt" HeaderText="CN NT">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="approvalstatus" HeaderText="Status Retur">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reasonretur" HeaderText="Reason Retur(Alasan)">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Action"><ItemTemplate>
<asp:Button id="BtnRetur" onclick="BtnRetur_Click" runat="server" CssClass="btn red" Font-Bold="True" Text="Retur Nota" __designer:wfdid="w51" Visible="<%# GetStatus() %>" CommandArgument='<%# Eval("branch_code") %>' ToolTip='<%# Eval("trnbiayaeksoid") %>'></asp:Button> <asp:Label id="LblLunas" runat="server" Font-Bold="True" Text="Sudah Dibayar" __designer:wfdid="w52" Visible="False"></asp:Label> <asp:Label id="Label5" runat="server" Font-Bold="True" Text="Sudah Diretur" __designer:wfdid="w53" Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Print"><ItemTemplate>
<asp:ImageButton id="BtnPrintList" onclick="BtnPrintList_Click" runat="server" ImageUrl="~/Images/print.gif" __designer:wfdid="w50" ToolTip='<%# Eval("trnbiayaeksoid") %>' CommandName='<%# Eval("branch_code") %>'></asp:ImageButton>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="bayaridr" HeaderText="bayaridr" Visible="False"></asp:BoundField>
<asp:BoundField DataField="SisaAmt" HeaderText="Amt. Sisa" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Data tidak ditemukan !!</asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w41" TargetControlID="txtPeriode2" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee4" runat="server" __designer:wfdid="w42" TargetControlID="txtPeriode1" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce4" runat="server" __designer:wfdid="w43" TargetControlID="txtperiode1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ce5" runat="server" __designer:wfdid="w44" TargetControlID="txtperiode2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE></asp:Panel> <asp:Label id="Label13" runat="server" Font-Bold="True" Text="Grand Total : Rp." __designer:wfdid="w45" Visible="False"></asp:Label> <asp:Label id="lblgrandtotal" runat="server" Font-Bold="True" __designer:wfdid="w46" Visible="False">0.00</asp:Label> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVmstEkp"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />&nbsp;<span style="font-size: 9pt"><strong> List Biaya Ekspedisi</strong></span><strong><span style="font-size: 9pt"> :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" /><strong><span style="font-size: 9pt"> Form Biaya Ekspedisi</span></strong><strong><span style="font-size: 9pt"> :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=4><DIV><asp:Label id="Label1" runat="server" Width="57px" Font-Size="Larger" Font-Bold="True" ForeColor="Black" Text="Information" __designer:wfdid="w68"></asp:Label> <asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Data" __designer:wfdid="w69" Visible="False"></asp:Label> <asp:Label id="cashbankoid" runat="server" Width="50px" __designer:wfdid="w70" Visible="False"></asp:Label> <asp:Label id="trnsuppoid" runat="server" Font-Size="X-Small" __designer:wfdid="w71" Visible="False"></asp:Label> <asp:Label id="CutofDate" runat="server" __designer:wfdid="w72" Visible="False"></asp:Label>&nbsp;<asp:Label id="BankDtlOid" runat="server" Font-Size="X-Small" __designer:wfdid="w73" Visible="False"></asp:Label> <asp:Label id="CabangDr" runat="server" Width="51px" __designer:wfdid="w74" Visible="False"></asp:Label></DIV></TD></TR><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left><asp:DropDownList style="HEIGHT: 17px" id="ddlcabang" runat="server" CssClass="inpText" __designer:wfdid="w75" OnSelectedIndexChanged="ddlcabang_SelectedIndexChanged" AutoPostBack="True" Enabled="true"></asp:DropDownList> </TD><TD style="FONT-SIZE: x-small" class="Label" align=left>Type Nota</TD><TD align=left><asp:DropDownList id="TypeNotaDDL" runat="server" CssClass="inpText" __designer:wfdid="w76" OnSelectedIndexChanged="TypeNotaDDL_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem>EXPEDISI</asp:ListItem>
<asp:ListItem Value="NON EXPEDISI">NON EXPEDISI</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left>No Biaya</TD><TD class="Label" align=left><asp:TextBox id="biayaeksno" runat="server" Width="140px" CssClass="inpTextDisabled" __designer:wfdid="w77" MaxLength="20" Enabled="False"></asp:TextBox><asp:Label id="expOid" runat="server" Font-Size="X-Small" __designer:wfdid="w78" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" class="Label" align=left>COA</TD><TD class="Label" align=left><asp:DropDownList id="cashbankacctgoid" runat="server" CssClass="inpText" __designer:wfdid="w79" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Tanggal&nbsp;<asp:Label id="Label25" runat="server" CssClass="Important" Text="*" __designer:wfdid="w80"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="PaymentDate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w81" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnPayDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w82" Visible="False" BorderColor="White"></asp:ImageButton> </TD><TD class="Label" align=left>Customer</TD><TD class="Label" align=left><asp:TextBox id="suppnames" runat="server" Width="140px" CssClass="inpTextDisabled" __designer:wfdid="w83" AutoPostBack="True" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton style="WIDTH: 16px" id="btnSearchSupp" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w84"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w85"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="LblNoBank" runat="server" Width="51px" Text="No. Bank" __designer:wfdid="w86"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="CashBankNo" runat="server" Width="140px" CssClass="inpTextDisabled" __designer:wfdid="w87" MaxLength="20" Enabled="False"></asp:TextBox> <asp:ImageButton id="BankBtnCari" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w88"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNoBank" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w89"></asp:ImageButton></TD><TD class="Label" align=left>Status</TD><TD class="Label" align=left><asp:TextBox id="txtStatus" runat="server" Width="140px" CssClass="inpTextDisabled" __designer:wfdid="w90" AutoPostBack="True" Enabled="False"></asp:TextBox> </TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left><asp:TextBox id="NoteExp" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w91" MaxLength="200" AutoPostBack="True"></asp:TextBox></TD><TD class="Label" align=left>Amount</TD><TD class="Label" align=left><asp:TextBox id="amtekspedisi" runat="server" Width="140px" CssClass="inpTextDisabled" __designer:wfdid="w92" MaxLength="100" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="I_U2" runat="server" Width="61px" Font-Size="X-Small" ForeColor="Red" Text="New Detail" __designer:wfdid="w93" Visible="False"></asp:Label></TD><TD class="Label" align=left>&nbsp;<asp:Label id="trnbiayaeksdtloid" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w94" Visible="False"></asp:Label><asp:Label id="Payseq" runat="server" Width="1px" __designer:wfdid="w95" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="lblpayduedate" runat="server" Width="88px" Text="Jatuh Tempo" __designer:wfdid="w96" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w97" Visible="False"></asp:TextBox> <asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w98" Visible="False" BorderColor="White"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="DtlLbl" runat="server" Width="57px" Font-Size="Larger" Font-Bold="True" ForeColor="Black" Text="Detail" __designer:wfdid="w99"></asp:Label></TD><TD class="Label" align=left><asp:Label id="trnbelimstoid" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w100" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="lblPOST" runat="server" Font-Size="X-Small" __designer:wfdid="w101" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="NotaLbl" runat="server" Width="51px" Text="No. Nota" __designer:wfdid="w102"></asp:Label>&nbsp;<asp:Label id="LblTandaDtl" runat="server" CssClass="Important" Text="*" __designer:wfdid="w103"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="trnbelino" runat="server" Width="123px" CssClass="inpTextDisabled" __designer:wfdid="w104" MaxLength="20" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchPurchasing" onclick="btnSearchPurchasing_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w105"></asp:ImageButton></TD><TD id="TD2" class="Label" align=left runat="server" visible="true"><asp:Label id="LblTotalNota" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w106" Visible="False">Amount</asp:Label></TD><TD id="TD1" class="Label" align=left runat="server" visible="true"><asp:TextBox id="amttrans" runat="server" Width="121px" CssClass="inpTextDisabled" __designer:wfdid="w107" Visible="False" MaxLength="100" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="LblNoteDtl" runat="server" Width="33px" Text="Note" __designer:wfdid="w108"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="noteDtl" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w109" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD><TD class="Label" align=right colSpan=2><asp:ImageButton id="ibtn" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w110"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w111" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD style="FONT-SIZE: x-small" class="Label" align=left colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtlEkp" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w112" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="payseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Overline="False" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualoid" HeaderText="trnbelimstoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label6" runat="server" Font-Size="X-Small" 
        ForeColor="Red" Text="Data detail belum ada..!!!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="Label21" runat="server" Font-Bold="True" Text="Grand Total :  " __designer:wfdid="w113" Visible="False"></asp:Label><asp:Label id="amtbelinettodtl4" runat="server" Font-Bold="True" __designer:wfdid="w114" Visible="False">0.0000</asp:Label><asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w115" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" class="Label" align=left colSpan=4>Last Updated By <asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w116"></asp:Label>&nbsp;On <asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w117"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" class="Label" align=left colSpan=4><asp:ImageButton style="HEIGHT: 23px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w118" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w119" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton style="HEIGHT: 23px" id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w120" AlternateText="Posting"></asp:ImageButton>&nbsp;<asp:ImageButton style="WIDTH: 60px" id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w121" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:wfdid="w122" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w123" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="FONT-SIZE: x-small" class="Label" align=center colSpan=4>&nbsp;<ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w124" TargetControlID="PaymentDate" Format="dd/MM/yyyy" PopupButtonID="btnPayDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w125" TargetControlID="payduedate" Format="dd/MM/yyyy" PopupButtonID="btnDueDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w126" CultureName="id-ID" Mask="99/99/9999" MaskType="Date" TargetControlID="PaymentDate"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEAmtExp" runat="server" __designer:wfdid="w127" TargetControlID="amtekspedisi" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEamttrans" runat="server" __designer:wfdid="w128" TargetControlID="amttrans" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w129" AssociatedUpdatePanelID="UpdatePanel11"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w130"></asp:Image><BR />Please Wait....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE><!--detail -->
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
        <tr>
            <td align="left">
                            <asp:UpdatePanel id="upPreview" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlPreview" runat="server" Width="800px" CssClass="modalBox" __designer:wfdid="w5" Visible="False"><TABLE style="BACKGROUND-COLOR: white" width="100%"><TBODY><TR><TD class="gvhdr" align=center colSpan=4><asp:Label id="lblCaptPreview" runat="server" Font-Size="Medium" Font-Bold="True" Text="Preview Data Nota" __designer:wfdid="w6" Font-Underline="True"></asp:Label></TD></TR><TR><TD align=left>Cabang</TD><TD align=left><asp:Label id="LblCabang" runat="server" __designer:wfdid="w7"></asp:Label></TD><TD align=left>Type Nota</TD><TD style="WIDTH: 227px" align=left><asp:Label id="LblTypeNota" runat="server" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD align=left>Nomer Biaya</TD><TD align=left><asp:Label id="LblNoBiaya" runat="server" __designer:wfdid="w9"></asp:Label></TD><TD align=left>COA</TD><TD style="WIDTH: 227px" align=left><asp:Label id="LblCOA" runat="server" __designer:wfdid="w10"></asp:Label></TD></TR><TR><TD align=left>Tanggal</TD><TD align=left><asp:Label id="LblTanggal" runat="server" __designer:wfdid="w11"></asp:Label></TD><TD align=left>Customer</TD><TD style="WIDTH: 227px" align=left><asp:Label id="LblCustName" runat="server" __designer:wfdid="w12"></asp:Label></TD></TR><TR><TD align=left>Amount</TD><TD align=left><asp:Label id="LblAmount" runat="server" __designer:wfdid="w13"></asp:Label></TD><TD align=left><asp:Label id="NoBankLbl" runat="server" __designer:wfdid="w14">No. Bank</asp:Label></TD><TD style="WIDTH: 227px" align=left><asp:Label id="LblNoCb" runat="server" __designer:wfdid="w15"></asp:Label> <asp:Label id="LblStatus" runat="server" __designer:wfdid="w9" Visible="False"></asp:Label></TD></TR><TR><TD align=left>Reason(Alasan)</TD><TD align=left colSpan=3><asp:TextBox id="ReasonRetur" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w4" MaxLength="300"></asp:TextBox></TD></TR><TR><TD align=left colSpan=4><asp:Label id="Label4" runat="server" Width="147px" Font-Size="Medium" Font-Bold="True" Text="Info Data Detail" __designer:wfdid="w6" Font-Underline="True"></asp:Label></TD></TR><TR><TD align=left colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 72%; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtlRet" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w17" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="payseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="DescNya" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Overline="False" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualoid" HeaderText="trnbelimstoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label6" runat="server" Font-Size="X-Small" 
        ForeColor="Red" Text="Data detail belum ada..!!!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=4><asp:Label id="Label3" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black" Text="Info Data Jurnal NT" __designer:wfdid="w18" Font-Underline="True"></asp:Label></TD></TR><TR><TD colSpan=4><asp:Panel id="pnlScrollPreview" runat="server" Width="100%" Height="151px" __designer:wfdid="w19" ScrollBars="Vertical"><asp:GridView id="gvPreview" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w20" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD align=center colSpan=4><asp:LinkButton id="lkbPostPreview" runat="server" CssClass="btn orange" Font-Bold="True" __designer:wfdid="w21">[ Send Approval]</asp:LinkButton> <asp:LinkButton id="lkbClosePreview" runat="server" CssClass="btn red" Font-Bold="True" __designer:wfdid="w22">[ Closed ]</asp:LinkButton></TD></TR><TR><TD align=left colSpan=4><asp:Label id="LblExpOid" runat="server" __designer:wfdid="w23" Visible="False"></asp:Label><asp:Label id="LblCustOid" runat="server" __designer:wfdid="w24" Visible="False"></asp:Label><asp:Label id="LblCoaOId" runat="server" __designer:wfdid="w25" Visible="False"></asp:Label><asp:Label id="LblCbOid" runat="server" __designer:wfdid="w26" Visible="False"></asp:Label><asp:Label id="LblCodeCabang" runat="server" __designer:wfdid="w27" Visible="False"></asp:Label><asp:Label id="LblCbDtlOid" runat="server" __designer:wfdid="w28" Visible="False"></asp:Label><asp:Label id="LblCabangAsal" runat="server" __designer:wfdid="w29" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="beHidePreview" runat="server" __designer:wfdid="w30" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpePreview" runat="server" __designer:wfdid="w31" TargetControlID="beHidePreview" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPreview" PopupDragHandleControlID="lblCaptPreview"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK" Visible="False"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label>
                            </asp:Panel> </TD></TR><TR><TD vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> <asp:Label id="LblValids" runat="server" __designer:wfdid="w27" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel4s" runat="server">
        <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Customer"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLSuppID" runat="server" CssClass="inpText"><asp:ListItem Value="custCODE">Code</asp:ListItem>
<asp:ListItem Selected="True" Value="custNAME">Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindSuppID" runat="server" Width="121px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibtnSuppID" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAlls" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView id="gvSupplier" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" AllowPaging="True" OnPageIndexChanging="gvSupplier_PageIndexChanging" GridLines="None" DataKeyNames="ID,Code,Name" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="Code" HeaderText="Cust. Code" SortExpression="Code">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Name" HeaderText="Cust. Name" SortExpression="bank">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ID" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                        <asp:Label ID="lblstatusdatasupp" runat="server" ForeColor="Red" Text="No Suppplier Data !" Visible="False"></asp:Label>
                                                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSupp" onclick="CloseSupp_Click" runat="server" CausesValidation="False" Font-Bold="False">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPE3" runat="server" TargetControlID="hiddenbtn2" PopupDragHandleControlID="lblSuppdata" PopupControlID="Panel1" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2" runat="server" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel4sX" runat="server">
        <contenttemplate>
<asp:Panel id="Panel1X" runat="server" Width="500px" Height="310px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdataX" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Bank"></asp:Label></TD></TR><TR><TD align=center>Filter :&nbsp; <asp:TextBox id="TxtCbNo" runat="server" Width="121px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="iBtBank" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="ViewAllBank" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=left><asp:GridView id="gvSupplierX" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" AllowPaging="True" OnPageIndexChanging="gvSupplierX_PageIndexChanging" GridLines="None" DataKeyNames="branch_code,Cabang,cashbankgloid,cashbankoid,acctgoid,cashbankglamtidr,acctgcode,acctgdesc,cashbankno,CabangDr" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Bank" SortExpression="cashbankno">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang" SortExpression="bank">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamtidr" HeaderText="Amount Biaya">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankgloid" HeaderText="cashbankgloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="cashbankoid" HeaderText="cashbankoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="CabangDr" HeaderText="CabangDr" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblstatusdatasupp" runat="server" ForeColor="Red" Text="Maaf, Data tidak ditemukan..!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSuppX" runat="server" CausesValidation="False" Font-Bold="False">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender3sX" runat="server" TargetControlID="hiddenbtn2sX" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel1x" PopupDragHandleControlID="lblSuppdata"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2sX" runat="server" Text="hiddenbtn2sX" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel7" runat="server">
        <contenttemplate>
<asp:Panel id="Panel2" runat="server" Width="900px" Height="300px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=4><asp:Label id="lblPurcdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Akun Piutang(Invoice)" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD align=center colSpan=4>Filter <asp:TextBox id="NoNota" runat="server" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindInv" onclick="imbFindInv_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w3" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAllInv" onclick="imbViewAllInv_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w4" AlternateText="View All"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w5" Visible="False"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=center colSpan=4><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 100%; BACKGROUND-COLOR: transparent; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 99%; HEIGHT: 180px; BACKGROUND-COLOR: beige"><asp:GridView id="gvPurchasing" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w6" OnSelectedIndexChanged="gvPurchasing_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelino,suppname,amttrans,acctgoid,trnbelidate" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualref" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="NoExpedisi" HeaderText="No. Expedisi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimstoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Tidak ada data piutang !!</asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET> </TD></TR><TR><TD vAlign=top align=center colSpan=4><asp:LinkButton id="ClosePurc" onclick="ClosePurc_Click" runat="server" CausesValidation="False" Font-Bold="True" __designer:wfdid="w7">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" TargetControlID="hiddenbtnpur" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel2" PopupDragHandleControlID="lblPurcdata"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtnpur" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
