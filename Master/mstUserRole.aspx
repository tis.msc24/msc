﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstUserRole.aspx.vb" Inherits="MasterUserRole" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: User Role" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <img id="Img2" align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of User Role :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w373" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 8%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w374"><asp:ListItem Value="ur.USERPROF">User ID</asp:ListItem>
<asp:ListItem Value="p.username">User Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w375"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w376"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w377"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w378" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvMst" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w379" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True" AllowPaging="True" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="profoid" DataNavigateUrlFormatString="~/Master/mstUserRole.aspx?oid={0}" DataTextField="profoid" HeaderText="User ID" SortExpression="profoid">
<FooterStyle ForeColor="White"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" BackColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="profname" HeaderText="User Name" SortExpression="profname">
<FooterStyle ForeColor="White"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" BackColor="Navy" Width="60%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="60%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalrole" HeaderText="Total Role" SortExpression="totalrole" Visible="False">
<FooterStyle ForeColor="White"></FooterStyle>

<HeaderStyle HorizontalAlign="Right" BackColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Navy" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" Text="There is no data !!" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w380"></asp:Label> </TD></TR></TBODY></TABLE></asp:Panel> &nbsp; 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 91px" class="Label" align=left><asp:Label id="I_U" runat="server" Width="59px" CssClass="Important" Text="New Data" __designer:wfdid="w27"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=right></TD><TD class="Label" align=left colSpan=2><asp:Label id="userroleoid" runat="server" __designer:wfdid="w28" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 91px" class="Label" align=left><asp:Label id="lblUserID" runat="server" Text="User ID" __designer:wfdid="w29"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddluserprof" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w30"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 91px" class="Label" align=left><asp:Label id="lblNmRole" runat="server" Width="65px" Text="Role Name" __designer:wfdid="w31"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddlroleoid" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w32"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 91px" id="TD3" class="Label" align=left><asp:Label id="lblSpAkses" runat="server" Width="88px" Text="Special Access" __designer:wfdid="w33"></asp:Label></TD><TD id="TD2" class="Label" align=right>:</TD><TD id="TD1" class="Label" align=left colSpan=2><asp:CheckBox id="cbspecial" runat="server" __designer:wfdid="w34" AutoPostBack="True"></asp:CheckBox><asp:Label id="lblspecial" runat="server" Text="No" __designer:wfdid="w35"></asp:Label>&nbsp; </TD></TR><TR><TD style="WIDTH: 91px" class="Label" align=left><asp:Label id="Label4" runat="server" Width="88px" Text="Kacab" __designer:wfdid="w33"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left colSpan=2><asp:CheckBox id="CbKacab" runat="server" Text="No" __designer:wfdid="w12" AutoPostBack="True" OnCheckedChanged="CbKacab_CheckedChanged"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left colSpan=4 rowSpan=3><asp:Label id="Label3" runat="server" Font-Bold="True" __designer:wfdid="w36">User Role Detail :</asp:Label></TD></TR><TR></TR><TR></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="imbTambah" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton><asp:ImageButton id="imbRemove" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="AbsMiddle" __designer:wfdid="w38" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 50%; HEIGHT: 100%"><asp:GridView id="gvDtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w14" BorderColor="Navy" AutoGenerateColumns="False" CellPadding="4" GridLines="None" OnRowDeleting="gvDtl_RowDeleting">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:BoundField DataField="rolename" HeaderText="Role Name">
<HeaderStyle HorizontalAlign="Left" BackColor="Navy" Font-Bold="True" ForeColor="White"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="special" HeaderText="Special Access">
<HeaderStyle HorizontalAlign="Center" BackColor="Navy" Font-Bold="True" ForeColor="White" Width="25%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cabang" HeaderText="Kacab">
<HeaderStyle HorizontalAlign="Center" BackColor="Navy" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbDelData" runat="server" __designer:wfdid="w11"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle BackColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="Navy" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmptyDetail" runat="server" CssClass="Important" Text="No User Role Detail !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:Label id="lblWarning" runat="server" CssClass="Important" __designer:wfdid="w40" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w41"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w42"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w43"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w44"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w45" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w46" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w47" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPrint2" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w48" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w49" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w50"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint2"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img id="Img1" align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form User Role :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

