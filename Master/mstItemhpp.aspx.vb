Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class mstItemhpp
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Private oRegex As Regex : Dim oMatches As MatchCollection
    Dim dv As DataView
    Dim cfunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim dtTemp As New DataTable
    Private rptSupp As New ReportDocument
    Private cProc As New ClassProcedure
#End Region

#Region "Function"
    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".jpg", ".jpeg", ".bmp", ".gif", ".png"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function

    Function getHargaBeli(ByVal itemOid As String) As String()
        Dim retVal(3) As String
        sSql = _
        "select m.trnbelidate 'tgl',m.trnbelimstoid, d.trnbelidtloid, m.trnbelino 'nonota',g.gendesc 'satasli', " & _
        "((d.amtbelinetto-( " & _
        "( " & _
        "(d.amtbelinetto/(select sum(amtbelinetto) from ql_trnbelidtl where trnbelimstoid=d.trnbelimstoid)*100) " & _
        "/100*(m.amtdischdr+m.amtdischdr2+m.amtdischdr3)) " & _
        "))+(m.trnamttax*( " & _
        "d.amtbelinetto/ " & _
        "	(select sum(amtbelinetto) from ql_trnbelidtl where trnbelimstoid=d.trnbelimstoid) " & _
        "*100)/100))/d.trnbelidtlqty 'harga', " & _
        "i.satuan1,i.satuan2,i.satuan3,i.konversi1_2,i.konversi2_3,d.trnbelidtlunitoid, " & _
        "g2.gendesc 'SatBesar',g3.gendesc 'SatSedang',g4.gendesc 'SatKecil',s.suppname,i.itemdesc   " & _
        "from ql_trnbelimst m  " & _
        "inner join ql_trnbelidtl d on d.trnbelimstoid=m.trnbelimstoid  " & _
        "inner join ql_mstgen g on d.trnbelidtlunitoid=g.genoid  " & _
        "inner join ql_mstitem i on d.itemoid=i.itemoid  " & _
        "inner join ql_mstgen g2 on i.satuan1=g2.genoid  " & _
        "inner join ql_mstgen g3 on i.satuan2=g3.genoid  " & _
        "inner join ql_mstgen g4 on i.satuan3=g4.genoid  " & _
        "inner join ql_mstsupp s on m.trnsuppoid=s.suppoid  " & _
        "where i.itemoid='" & itemOid & "' and m.trnbelidate between '" & DateTime.Now.AddYears(-1).ToShortDateString & "' and '" & DateTime.Now.ToShortDateString & "'" & _
        "order by m.trnbelidate, m.trnbelimstoid, d.trnbelidtloid "
        Dim dtTmp As DataTable = CreateDataTableFromSQL(sSql)

        If dtTmp.Rows.Count <= 0 Then
            ' belum ada pmebelian
            retVal(0) = "0"
            retVal(1) = "0"
            retVal(2) = "0"
        Else
            Dim targetRow As Integer = dtTmp.Rows.Count - 1

            ' ambil harga sat besar
            Dim hrgSatBesar As Double = 0
            If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga")
            ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi1_2")
            ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi2_3") * dtTmp.Rows(targetRow)("konversi1_2")
            End If

            Dim hrgSatSedang As Double = 0
            ' cek dulu apakah sat 1,2,3 sama semua
            If dtTmp.Rows(targetRow)("satuan1") = dtTmp.Rows(targetRow)("satuan2") Then
                hrgSatSedang = 0
            Else
                ' ambil harga sat sedang
                If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi1_2")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi2_3")
                End If
            End If

            Dim hrgSatKecil As Double = 0
            If dtTmp.Rows(targetRow)("satuan2") = dtTmp.Rows(targetRow)("satuan3") Then
                hrgSatKecil = 0
            Else
                ' ambil harga sat kecil
                If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi1_2") / dtTmp.Rows(targetRow)("konversi2_3")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi2_3")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga")
                End If
            End If

            retVal(0) = hrgSatBesar
            retVal(1) = hrgSatSedang
            retVal(2) = hrgSatKecil
        End If

        Return retVal
    End Function

    Function isHargaUpdated(ByVal itemOid As String) As String
        Dim retVal As String = "PNA"
        Dim hargaHitung(3) As String
        hargaHitung = getHargaBeli(itemOid)

        ' cek apakah sebelumnya ada pembelian
        If hargaHitung(0) = "0" And hargaHitung(1) = "0" And hargaHitung(2) = "0" Then
            retVal = "PNA"
            GoTo selesai
        End If

        Dim hargaSekarang(3) As String
        sSql = "select itemoid, itemdesc, itempriceunit1, itempriceunit2, itempriceunit3, " & _
        "satuan1, satuan2, satuan3 " & _
        "from ql_mstitem where itemoid='" & itemOid & "' "
        Dim dtTmp As DataTable = CreateDataTableFromSQL(sSql)

        Dim satuan1 As String = dtTmp.Rows(0)("satuan1").ToString()
        Dim satuan2 As String = dtTmp.Rows(0)("satuan2").ToString()
        Dim satuan3 As String = dtTmp.Rows(0)("satuan3").ToString()

        'If dtTmp.Rows.Count > 0 Then
        hargaSekarang(0) = dtTmp.Rows(0)("itempriceunit1")
        hargaSekarang(1) = dtTmp.Rows(0)("itempriceunit2")
        hargaSekarang(2) = dtTmp.Rows(0)("itempriceunit3")
        'Else
        'hargaSekarang(0) = "0"
        'hargaSekarang(1) = "0"
        'hargaSekarang(2) = "0"
        'End If

        'For i As Integer = 0 To 2
        Dim dHargaSkg1 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(0)) = False, hargaSekarang(0), "0"))
        Dim dHargaHtg1 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(0)) = False, hargaHitung(0), "0"))
        If dHargaSkg1 <= dHargaHtg1 Then
            retVal = "FALSE"
            GoTo selesai
        Else
            retVal = "TRUE"
        End If
        'Next i

        If satuan1 = satuan2 Then
            retVal = "TRUE"
        Else
            Dim dHargaSkg2 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(1)) = False, hargaSekarang(1), "0"))
            Dim dHargaHtg2 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(1)) = False, hargaHitung(1), "0"))
            If dHargaSkg2 <= dHargaHtg2 Then
                retVal = "FALSE"
                GoTo selesai
            Else
                retVal = "TRUE"
            End If
        End If


        If satuan2 = satuan3 Then
            retVal = "TRUE"
        Else
            Dim dHargaSkg3 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(2)) = False, hargaSekarang(2), "0"))
            Dim dHargaHtg3 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(2)) = False, hargaHitung(2), "0"))
            If dHargaSkg3 <= dHargaHtg3 Then
                retVal = "FALSE"
                GoTo selesai
            Else
                retVal = "TRUE"
            End If
        End If


selesai:
        Return retVal
    End Function

    Function getTanda(ByVal itemOid As String) As String
        Dim retVal As String = ""
        'If itemOid = "1043" Then
        '    Dim aaa As String = "lalalala"
        'End If
        Dim hasil As String = isHargaUpdated(itemOid)
        If hasil = "TRUE" Then
            retVal = "V"
        ElseIf hasil = "FALSE" Then
            retVal = "X"
        ElseIf hasil = "PNA" Then
            retVal = "O"
        End If

        Return retVal
    End Function

    'Function tandaHarga(ByVal itemOid As String) As String
    '    Dim tanda As String = getTanda(itemOid)
    '    Dim retVal As String = ""

    '    If tanda = "X" Then
    '        retVal = "<span style='color:#990000;font-weight:bolder;'>X</span>"
    '    ElseIf tanda = "V" Then
    '        retVal = "<span style='color:#009966;font-weight:bolder;'>V</span>"
    '    Else
    '        retVal = ""
    '    End If
    '    Return retVal
    'End Function

    Function tandaHarga(ByVal tanda As String) As String
        Dim retVal As String = ""
        If tanda = "X" Then
            retVal = "<span style='color:#990000;font-weight:bolder;'>X</span>"
        ElseIf tanda = "V" Then
            retVal = "<span style='color:#009966;font-weight:bolder;'>V</span>"
        Else
            retVal = "<span style='color:#ff6600;font-weight:bolder;'>O</span>"
        End If
        Return retVal
    End Function

    Sub copyGridView()
        '' copy column from gridview to datatable
        'Dim dtOlah As DataTable = Session("dtPublic")
        'Dim i As Integer = 0

        'For i = 0 To GVmstgen.Rows.Count - 1
        '    Dim row As GridViewRow = GVmstgen.Rows(i)
        '    If row.RowType = DataControlRowType.DataRow Then
        '        Dim lbl As System.Web.UI.WebControls.Label = row.FindControl("lblStatusHarga")
        '        dtOlah.Rows(i)("infoharga") = lbl.Text
        '    End If
        'Next i

        '' update dtPublic
        'Session("dtPublic") = dtOlah

        'Dim zz As String = "123"
    End Sub

    Function formatToIDR(ByVal money As String) As String
        'Dim ci As New System.Globalization.CultureInfo("id-ID")
        'Return money.ToString("c", ci)
        If money.IndexOf(".") > 0 Then
            Dim depan As String = money.Substring(0, money.IndexOf("."))
            depan = depan.Replace(",", ".")
            Dim belakang As String = money.Substring(money.IndexOf(".") + 1, money.Length - (depan.Length + 1))
            Return depan & "," & belakang
        Else
            Return money.Replace(",", ".")
        End If
    End Function
#End Region

#Region "Procedure"


    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function



    Public Function getFileName(ByVal backID As String) As String
        Dim fname As String = ""
        Dim ID As String = Trim(backID)
        fname = "item_" & ID & ".jpg"
        Return fname
    End Function

    Public Sub uploadFileGambar(ByVal proses As String, ByVal fname As String)
        Dim savePath As String = Server.MapPath("~/Images/")
        Const sbmpW = 80
        Const sbmpH = 50

        imgPerson.Visible = False
        If FileUploadInsert.HasFile Then
            If checkTypeFile(FileUploadInsert.FileName) Then
                Dim newWidth As Integer
                Dim newHeight As Integer
                newWidth = sbmpW
                newHeight = sbmpH
                savePath += fname
                Dim uploadBmp As Bitmap = Bitmap.FromStream(FileUploadInsert.PostedFile.InputStream)
                Dim newBmp As Bitmap = New Bitmap(newWidth, newHeight)
                Dim newGraphic As Graphics = Graphics.FromImage(newBmp)

                Try
                    newGraphic.Clear(Color.White)
                    newGraphic.DrawImage(uploadBmp, 0, 0, newWidth, newHeight)
                    newBmp.Save(savePath, Imaging.ImageFormat.Jpeg)
                    lblMenuPic.Text = "Upload File Berhasil"
                    lblMenuPic.Visible = True
                    imgPerson.ImageUrl = "~/Images/" & fname
                    imgPerson.Visible = True
                Catch ex As Exception
                End Try
            Else
                lblMenuPic.Text = "Tidak dapat diupload. Format file harus .jpg/.jpeg/.gif/.png/.bmp!"
                lblMenuPic.Visible = True
            End If
        Else
            lblMenuPic.Text = "Data Gambar belum dipilih"
            lblMenuPic.Visible = True
        End If
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        ' this function will load ALL DATA at first time
        ' so call this function wisely.
        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText")
        FilterDDL.SelectedIndex = Session("FilterDDL")
        orderby.SelectedIndex = Session("orderby")

        FilterText2.Text = Session("FilterText2")
        FilterDDL2.SelectedIndex = Session("FilterDDL2")

        FilterText3.Text = Session("FilterText3")
        FilterDDL3.SelectedIndex = Session("FilterDDL3")

        If Session("FilterDDL") Is Nothing = False Or Session("FilterDDL2") Is Nothing = False Or Session("FilterDDL3") Is Nothing = False Then
            If Session("FilterText").ToString.Trim <> "" Then
                If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                    sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
                Else
                    sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
                End If
            End If

            If Session("FilterText2").ToString.Trim <> "" Then
                If FilterDDL2.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL2.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL2.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                    sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '" & Tchar(FilterText2.Text) & "' "
                Else
                    sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '%" & Tchar(FilterText2.Text) & "%' "
                End If
            End If

            If Session("FilterText3").ToString.Trim <> "" Then
                If FilterDDL3.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL3.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL3.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                    sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '" & Tchar(FilterText3.Text) & "' "
                Else
                    sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '%" & Tchar(FilterText3.Text) & "%' "
                End If
            End If

        Else

            If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
            Else
                sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If

            If FilterDDL2.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL2.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL2.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '" & Tchar(FilterText2.Text) & "' "
            Else
                sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '%" & Tchar(FilterText2.Text) & "%' "
            End If

            If FilterDDL3.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL3.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL3.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '" & Tchar(FilterText3.Text) & "' "
            Else
                sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '%" & Tchar(FilterText3.Text) & "%' "
            End If

        End If

        If SelectTop.Text.Trim = "" Or SelectTop.Text <= 0 Then
            SelectTop.Text = 100
        End If


        sSql = "SELECT top " & SelectTop.Text & " 1 as nomor,	QL_mstitem.cmpcode, itemoid, itemcode, itemdesc,merk,	QL_mstgen.gendesc as itemgroupoid, itemsubgroupoid, itembarcode1, itembarcode2, itembarcode3, convert(numeric(18,2),round((itempriceunit1/konversi1_2)/konversi2_3,0)) itempriceunit1, convert(numeric(18,2),round(itempriceunit2/konversi2_3,0))  itempriceunit2, itempriceunit3, replace(g1.gendesc + '( '+convert(char(4),konversi1_2*konversi2_3)+')',' ','') as satuan1, replace(g2.gendesc + '( '+convert(char(4),konversi2_3)+')',' ','') as satuan2, g3.gendesc as satuan3,konversi1_2, konversi2_3,itemsafetystockunit1, ql_mstitem.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as personoid,'?' 'infoharga' ,pricelist bottompricegrosir ,(select isnull(SUM(saldoAkhir - qtyBooking ),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = QL_mstItem.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 = (select genoid from QL_mstgen where genother1  = 'GROSIR' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')  ) stock " & _
  " ,0 stockGUDANG35	" & _
" ,(select isnull(SUM(saldoAkhir - qtyBooking ),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = QL_mstItem.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 = (select genoid from QL_mstgen where genother1  = 'SUPPLIER' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')  ) stockSupp" & _
" FROM QL_mstitem 	inner join QL_mstgen on QL_mstitem.itemgroupoid =  QL_mstgen.genoid inner join QL_mstgen gsub on QL_mstitem.itemsubgroupoid =  gsub.genoid 	inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid  	inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1  	inner join ql_mstgen g2 on g2.genoid=QL_mstitem.satuan2  	inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3 	" & sWhere & IIf(sWhere <> "", " AND ", " WHERE ") & " QL_mstitem.cmpcode LIKE '%" & CompnyCode & "%' and itemflag = '" & ddlStatusView.SelectedValue & "' ORDER BY " & orderby.SelectedValue


        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        ' add value for infoharga columns
        Dim dr As DataRow
        For c1 As Integer = 0 To objDs.Tables("data").Rows.Count - 1
            dr = objDs.Tables("data").Rows(c1)
            dr.BeginEdit()
            dr(0) = c1 + 1
            'dr("infoharga") = getTanda(objDs.Tables("data").Rows(c1)("itemoid"))
            dr.EndEdit()
        Next
        'Dim dtz As DataTable = GVmstgen.DataSource
        'Dim xz As String = "aass"
        ' jika cari berdasarkan status info harga beli
        'If FilterDDL.SelectedValue = "infoharga" Then
        '    Dim dv As DataView = objDs.Tables("data").DefaultView
        '    dv.RowFilter = "infoharga='" & ddlStatusHarga.SelectedValue & "' "
        '    GVmstgen.DataSource = dv.ToTable()

        '    GVmstgen.DataBind()
        '    objDs.Tables("data").Clear()  ' di-clear supaya tidak berat
        '    Exit Sub
        'End If

        GVmstgen.DataSource = objDs.Tables("data")
        GVmstgen.DataBind()

        Session("dtPublic") = GVmstgen.DataSource
    End Sub

    Public Sub findAData()
        ' search data
        'Dim sWhere As String = ""
        'FilterText.Text = Session("FilterText")
        'FilterDDL.SelectedIndex = Session("FilterDDL")
        'orderby.SelectedIndex = Session("orderby")
        'If Session("FilterDDL") Is Nothing = False Then
        '    If Session("FilterText").ToString.Trim <> "" Then
        '        sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
        '    End If
        'Else
        '    sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
        'End If

        Dim dtTmp As DataTable = Session("dtPublic")
        Dim dv As DataView = dtTmp.DefaultView
        If FilterDDL.SelectedValue <> "infoharga" Then
            dv.RowFilter = FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%' "
        Else
            dv.RowFilter = "infoharga='" & ddlStatusHarga.SelectedValue & "' "
        End If

        Dim aa As Integer = 2
        GVmstgen.DataSource = dv.ToTable()
        GVmstgen.DataBind()
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As String)

        Try
            sSql = "SELECT mi.[cmpcode],mi.[itemoid],mi.[itemcode],mi.[itemname],mi.[itemdesc],mi.merk,mi.maxstock, " & _
                   "mi.[itemgroupoid],mi.[itemsubgroupoid],mi.[itembarcode1]," & _
                   "mi.[itembarcode2], mi.[itembarcode3], " & _
                    "mi.[lastpricebuyunit1], mi.[lastpricebuyunit2], mi.[lastpricebuyunit3], " & _
                   "mi.[itempriceunit1], mi.[itempriceunit2], mi.[itempriceunit3], " & _
                    "mi.[itempriceunit1retail], mi.[itempriceunit2retail], mi.[itempriceunit3retail], " & _
                     "mi.[bottompricegrosir], mi.[bottompriceretail], " & _
                   "mi.[satuan1], mi.[satuan2], mi.[satuan3],mi.payoid, " & _
                   "mi.[itemsafetystockunit1],mi.[createuser],mi.[upduser],mi.[updtime], mi.[itemflag], mi.[acctgoid] , konversi1_2, konversi2_3,mi.personoid,mi.itempictureloc, keterangan , editharga ,itemsafetystockunitgrosir,maxstockgrosir ,pricelist,discunit1,discunit2,discunit3,qty3_to2,qty3_to1 , persentobtmprice, hpp ,isnull((select top 1 isnull(suppname,'-')  from QL_mstsupp s inner join ql_trnbelimst m on m.trnsuppoid = s.suppoid inner join QL_trnbelidtl d on m.trnbelimstoid = d.trnbelimstoid and d.itemoid = mi.itemoid order by m.trnbelimstoid desc ),'-') Supplier   " & _
                   "FROM [QL_mstitem] mi where mi.cmpcode='" & CompnyCode & "' and mi.itemoid = '" & vGenoid & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Enabled = False
            While xreader.Read
                itemoid.Text = xreader("itemoid")
                itemcode.Text = xreader("itemcode")
                itemdesc.Text = xreader("itemdesc")
                merk.Text = xreader("merk")
                itemgroupoid.SelectedValue = xreader("itemgroupoid").ToString.Trim
                itemsubgroupoid.SelectedValue = xreader("itemsubgroupoid").ToString.Trim
                itembarcode1.Text = xreader("itembarcode1")
                itembarcode2.Text = xreader("itembarcode2")
                itembarcode3.Text = xreader("itembarcode3")
                itempriceunit1.Text = NewMaskEdit(xreader("itempriceunit1"))
                itempriceunit2.Text = NewMaskEdit(xreader("itempriceunit2"))
                itempriceunit3.Text = NewMaskEdit(xreader("itempriceunit3"))
                itempriceunit1retail.Text = NewMaskEdit(xreader("itempriceunit1retail"))
                itempriceunit2retail.Text = NewMaskEdit(xreader("itempriceunit2retail"))
                itempriceunit3retail.Text = NewMaskEdit(xreader("itempriceunit3retail"))
                bottompriceGrosir.Text = NewMaskEdit(xreader("bottompricegrosir"))
                bottompriceRetail.Text = NewMaskEdit(xreader("bottompriceretail"))
                qty3_to2.Text = NewMaskEdit(xreader("qty3_to2"))
                Qty3_to1.Text = NewMaskEdit(xreader("Qty3_to1"))
                satuan1.SelectedValue = xreader("satuan1").ToString.Trim
                satuan2.SelectedValue = xreader("satuan2").ToString.Trim
                satuan3.SelectedValue = xreader("satuan3").ToString.Trim
                PriceList.Text = NewMaskEdit(xreader("pricelist"))
                DiscUnit1.Text = NewMaskEdit(xreader("DiscUnit1"))
                DiscUnit2.Text = NewMaskEdit(xreader("DiscUnit2"))
                DiscUnit3.Text = NewMaskEdit(xreader("DiscUnit3"))
                itemsafetystock.Text = NewMaskEdit(xreader("itemsafetystockunit1"))
                Maxstock.Text = NewMaskEdit(xreader("maxstock"))
                konversi2_3.Text = NewMaskEdit(xreader("konversi2_3"))
                konversi1_2.Text = NewMaskEdit(xreader("konversi1_2"))
                persentobtmprice.Text = ToMaskEdit(xreader("persentobtmprice"), 3)
                hpp.Text = ToMaskEdit(xreader("hpp"), 3)
                spgOid.SelectedValue = xreader("personoid").ToString.Trim
                imgPerson.Visible = True
                imgPerson.ImageUrl = "~/Images/" & xreader("itempictureloc").ToString

                acctgoid.SelectedValue = xreader("acctgoid").ToString.Trim
                Upduser.Text = xreader("upduser")
                Updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy HH:mm:ss")
                ddlStatus.SelectedValue = xreader("itemflag").ToString.Trim
                keterangan.Text = xreader("keterangan").trim
                editprice.SelectedValue = xreader("editharga").trim
                payment.SelectedValue = xreader("payoid").ToString.Trim
                '  satuan1.Enabled = False
                ' satuan2.Enabled = False
                'satuan3.Enabled = False
                btnDelete.Enabled = True

                ' tampilkan harga beli (info harga beli)
                lblHrgBeliBsr.Text = NewMaskEdit(xreader("lastpricebuyunit1"))
                'lblHrgBeliBsr.Text = formatToIDR(NewMaskEdit(1504723.9253))
                lblHrgBeliSdg.Text = NewMaskEdit(xreader("lastpricebuyunit2"))
                lblHrgBeliKcl.Text = NewMaskEdit(xreader("lastpricebuyunit3"))

                itemsafetystockgrosir.Text = NewMaskEdit(xreader("itemsafetystockunitgrosir"))
                Maxstockgrosir.Text = NewMaskEdit(xreader("maxstockgrosir"))
                Me.TextBox1.Text = xreader("Supplier").ToString.Trim
                hargaPCSan()
                'formatToIDR(NewMaskEdit(Math.Round(CDbl(getHargaBeli(vGenoid)(0)), 2)))
                'formatToIDR(NewMaskEdit(Math.Round(CDbl(getHargaBeli(vGenoid)(1)), 2)))
                'formatToIDR(NewMaskEdit(Math.Round(CDbl(getHargaBeli(vGenoid)(2)), 2)))
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message & ex.ToString & sSql, "ERROR") : Exit Sub
        End Try

    End Sub

    Public Sub GenerateGenID()
        itemoid.Text = GenerateID("QL_mstitem", CompnyCode)
    End Sub

    Sub clearItem()
        itemoid.Text = ""
        itemcode.Text = ""
        itemdesc.Text = ""
        itemgroupoid.SelectedIndex = 0
        itemsubgroupoid.SelectedIndex = 0
        itembarcode1.Text = ""
        itembarcode2.Text = ""
        itembarcode3.Text = ""
        konversi2_3.Text = ""
        konversi1_2.Text = ""

        satuan1.SelectedIndex = 0
        satuan2.SelectedIndex = 0
        satuan3.SelectedIndex = 0
        acctgoid.SelectedIndex = 0
        itemsafetystock.Text = 0
        ddlStatus.SelectedIndex = 0
        Upduser.Text = Session("UserID")
        Updtime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")

    End Sub

    Sub initDDL()
        Dim msg As String = ""

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMGROUP' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        If cKon.ambilscalar(sSql) > 0 Then
            FillDDL(itemgroupoid, sSql)
        Else
            msg &= " - Isi Grup Barang di Data General terlebih dahulu !! " & "<BR>"
        End If

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"

        If cKon.ambilscalar(sSql) > 0 Then
            FillDDL(itemsubgroupoid, sSql)
        Else
            msg &= " - Isi Sub Grup Barang di Data General terlebih dahulu !! " & "<BR>"
        End If


        sSql = "select genoid ,gendesc from ql_mstgen where cmpcode = '" & CompnyCode & "' and gengroup = 'PAYTYPE' "
        FillDDL(payment, sSql)

        sSql = "select acctgoid, acctgcode  + '-' + acctgdesc from QL_mstacctg where cmpcode ='" & CompnyCode & "' "
        FillDDL(acctgoid, sSql)

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMUNIT' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(satuan1, sSql)
        FillDDL(satuan2, sSql)
        FillDDL(satuan3, sSql)

        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "'   and  personpost in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC = 'SALES PERSON')  and STATUS='AKTIF' "
        FillDDL(spgOid, sSql)

        If msg <> "" Then
            showMessage(msg, CompnyName & " - INFORMASI")
            btnSave.Visible = False
            Exit Sub
        End If

    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If


        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("mstitemhpp.aspx?page=" & GVmstgen.PageIndex)
        End If

        Page.Title = CompnyName & " - Data Item"
        Session("oid") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("onclick", "javascript:return confirm('Anda yakin akan HAPUS data ini ?');")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "Update"
            'TabContainer1.ActiveTabIndex = 1
        Else
            i_u.Text = "New"
        End If

        If Not IsPostBack Then
            GVmstgen.PageIndex = Session("page")
            
            'GVmstgen.AllowPaging = False
            BindData(CompnyCode)
            'copyGridView()
            'GVmstgen.AllowPaging = True

            initDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                lblupd.Text = "Last Update"
            Else
                GenerateGenID()
                btnDelete.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")
                TabContainer1.ActiveTabIndex = 0
                GVmstgen.PageIndex = ToDouble(Request.QueryString("page"))
                GVmstgen.DataBind()
                lblupd.Text = "Create"
            End If
        End If
    End Sub

    'BUat paging
    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
        Session("page") = GVmstgen.PageIndex

        'findAData()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        ' FilterDDL.SelectedIndex = 0
        Session("FilterText") = ""
        Session("FilterDDL") = 0

        Session("FilterText2") = ""
        Session("FilterDDL2") = 0

        Session("FilterText3") = ""
        Session("FilterDDL3") = 0

        Session("orderby") = orderby.SelectedIndex
        BindData(CompnyCode)
        'findAData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        Session("orderby") = orderby.SelectedIndex

        Session("FilterText2") = FilterText2.Text
        Session("FilterDDL2") = FilterDDL2.SelectedIndex
        'Session("orderby") = orderby.SelectedIndex

        Session("FilterText3") = FilterText3.Text
        Session("FilterDDL3") = FilterDDL3.SelectedIndex
        'Session("orderby") = orderby.SelectedIndex

        GVmstgen.PageIndex = 0
        BindData(CompnyCode)
        'findAData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errMessage As String = ""
        If itemcode.Text.Trim = "" Then errMessage &= "- Isi Kode Barang Dahulu ! <BR>"
        If itemdesc.Text.Trim = "" Then errMessage &= "- Isi Nama Barang Dahulu ! <BR>"
        If merk.Text.Trim = "" Then errMessage &= "- Isi Merk Dahulu ! <BR>"
        If itemsafetystock.Text.Trim = 0 Then errMessage &= "- Isi Safety Stok Retail Dahulu ! <BR>"
        If Maxstock.Text.Trim = 0 Then errMessage &= "- Isi Max Stok Retail Dahulu ! <BR>"
        If itemgroupoid.Items.Count <= 0 Then errMessage &= "- Pilih Grup Barang Dahulu ! <BR>"
        If itemsubgroupoid.Items.Count <= 0 Then errMessage &= "- Pilih Sub Grup Barang Dahulu ! <BR>"
        If ToDouble(konversi1_2.Text.Trim) = 0 Then errMessage &= "- Konversi 1-2 tidak boleh kosong / 0 ! <BR>"
        If ToDouble(konversi2_3.Text.Trim) = 0 Then errMessage &= "- Konversi 2-3 tidak boleh kosong / 0 ! <BR>"
        If ToDouble(itemsafetystock.Text.Trim) > ToDouble(Maxstock.Text.Trim) Then errMessage &= "- Safety Stock Retail harus lebih kecil dari Max Stock Retail! <BR>"
        If ToDouble(itemsafetystockgrosir.Text.Trim) > ToDouble(Maxstockgrosir.Text.Trim) Then errMessage &= "- Safety Stock Grosir harus lebih kecil dari Max Stock Grosir! <BR>"
        'If Left(itemdesc.Text, 1).ToUpper <> Left(itemcode.Text, 1).ToUpper Then errMessage &= "- Kode awal tidak sama dengan karakter awal dari nama barang ! <BR>"

        If satuan1.SelectedValue = satuan2.SelectedValue And ToDouble(konversi1_2.Text.Trim) <> 1 Then errMessage &= "- Satuan Qty & Satuan Partai Sama maka Konversi 1-2 Harus sama dengan 1 ! <BR>"
        If satuan2.SelectedValue = satuan3.SelectedValue And ToDouble(konversi2_3.Text.Trim) <> 1 Then errMessage &= "- Satuan Partai & Satuan Ecer Sama maka Konversi 2-3 Harus sama dengan 1 ! <BR>"

        If errMessage <> "" Then
            showMessage(errMessage, CompnyName & " - WARNING")
            Exit Sub
        End If

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_mstitem WHERE cmpcode='" & CompnyCode & "' and " & _
                                   " itemdesc = '" & Tchar(itemdesc.Text.Trim) & "' and merk = '" & Tchar(merk.Text.Trim) & "' "
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck1 &= " AND itemoid <> " & itemoid.Text
        End If
        If cKon.ambilscalar(sSqlCheck1) > 0 Then
            showMessage("Nama Barang dan Merk sudah dipakai ", CompnyName & " - WARNING")
            Exit Sub
        End If


        If ToDouble(bottompriceGrosir.Text) > ToDouble(itempriceunit3.Text) Then
            showMessage("Harga Barang3 Grosir harus lebih besar dari Harga Bawah Grosir ", CompnyName & " - WARNING")
            Exit Sub
        ElseIf ToDouble(bottompriceGrosir.Text) * ToDouble(konversi2_3.Text) > ToDouble(itempriceunit2.Text) Then
            showMessage("Harga Barang2 Grosir harus lebih besar dari Harga Bawah Grosir ", CompnyName & " - WARNING")
            Exit Sub
        ElseIf ToDouble(bottompriceGrosir.Text) * ToDouble(konversi2_3.Text) * ToDouble(konversi1_2.Text) > ToDouble(itempriceunit1.Text) Then
            showMessage("Harga Barang1 Grosir harus lebih besar dari Harga Bawah Grosir ", CompnyName & " - WARNING")
            Exit Sub
        End If

        'If ToDouble(bottompriceRetail.Text) > ToDouble(itempriceunit3retail.Text) Then
        '    showMessage("Harga Barang3 Retail harus lebih besar dari Harga Bawah Grosir ", CompnyName & " - WARNING")
        '    Exit Sub
        'ElseIf ToDouble(bottompriceRetail.Text) * ToDouble(konversi2_3.Text) > ToDouble(itempriceunit2retail.Text) Then
        '    showMessage("Harga Barang2 Retail harus lebih besar dari Harga Bawah Grosir ", CompnyName & " - WARNING")
        '    Exit Sub
        'ElseIf ToDouble(bottompriceRetail.Text) * ToDouble(konversi2_3.Text) * ToDouble(konversi1_2.Text) > ToDouble(itempriceunit1retail.Text) Then
        '    showMessage("Harga Barang1 Retail harus lebih besar dari Harga Bawah Grosir ", CompnyName & " - WARNING")
        '    Exit Sub
        'End If

        Session("ItemOid") = GenerateID("QL_mstitem", CompnyCode)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")

            If Session("oid") = Nothing Or Session("oid") = "" Then
                generateItemId(itemdesc.Text)

                sSql = "INSERT INTO " & DB_JPT_GROSIR & ".dbo.QL_MSTITEM ([cmpcode],[itemoid],[itemcode],[itemdesc], " & _
                       "[itemgroupoid],[itemsubgroupoid],[itembarcode1]," & _
                       "[itembarcode2], [itembarcode3], " & _
                       "[itempriceunit1], [itempriceunit2], [itempriceunit3], " & _
                       "[satuan1], [satuan2], [satuan3], " & _
                       "[itemsafetystockunit1],[itempictureloc]," & _
                       "[acctgoid],[createuser],[upduser],[updtime], [itemflag], konversi1_2, konversi2_3,personoid, keterangan, editharga,merk,maxstock,payoid,[itempriceunit1retail], [itempriceunit2retail], [itempriceunit3retail],bottompricegrosir,bottompriceretail,itemsafetystockunitgrosir,maxstockgrosir,pricelist,discunit1,discunit2,discunit3,qty3_to2,qty3_to1, persentobtmprice) " & _
                       "values ('" & CompnyCode & "'," & _
                       "" & Session("ItemOid") & "," & _
                       "'" & Tchar(itemcode.Text) & "', " & _
                       "'" & Tchar(itemdesc.Text) & "'," & _
                       "" & itemgroupoid.SelectedValue & ", " & _
                       "" & itemsubgroupoid.SelectedValue & "," & _
                       "'" & Tchar(itembarcode1.Text) & "'," & _
                       "'" & Tchar(itembarcode2.Text) & "'," & _
                       "'" & Tchar(itembarcode3.Text) & "'," & _
                       "" & ToDouble(itempriceunit1.Text) & "," & _
                       "" & ToDouble(itempriceunit2.Text) & "," & _
                       "" & ToDouble(itempriceunit3.Text) & "," & _
                       "'" & satuan1.Items(satuan1.SelectedIndex).Value & "'," & _
                       "'" & satuan2.Items(satuan2.SelectedIndex).Value & "'," & _
                       "'" & satuan3.Items(satuan3.SelectedIndex).Value & "'," & _
                       "" & ToDouble(itemsafetystock.Text) & ", " & _
                       "'item_" & Trim(itemoid.Text) & ".jpg'," & _
                       "" & acctgoid.SelectedValue & "," & _
                       "'" & Session("UserID") & "', " & _
                       "'" & Session("UserID") & "'," & _
                       "current_timestamp, " & _
                       "'" & ddlStatus.SelectedValue & "', " & ToDouble(konversi1_2.Text) & "," & _
                       "'" & ToDouble(konversi2_3.Text) & "','" & spgOid.SelectedValue & "', '" & Tchar(keterangan.Text) & "', '" & editprice.SelectedValue & "','" & Tchar(merk.Text) & "'," & ToDouble(Maxstock.Text) & "," & payment.SelectedValue & "," & _
                       "" & ToDouble(itempriceunit1.Text) & "," & _
                       "" & ToDouble(itempriceunit2.Text) & "," & _
                       "" & ToDouble(itempriceunit3.Text) & "," & _
                       "" & ToDouble(bottompriceGrosir.Text) & "," & ToDouble(bottompriceRetail.Text) & "," & ToDouble(itemsafetystockgrosir.Text) & "," & ToDouble(Maxstockgrosir.Text) & "," & ToDouble(PriceList.Text) & "," & ToDouble(DiscUnit1.Text) & "," & ToDouble(DiscUnit2.Text) & "," & ToDouble(DiscUnit3.Text) & "," & ToDouble(qty3_to2.Text) & "," & ToDouble(Qty3_to1.Text) & ", " & ToDouble(persentobtmprice.Text) & ") "

                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update " & DB_JPT_GROSIR & ".dbo.QL_mstoid set lastoid=" & Session("ItemOid") & " where " & _
                       "tablename='QL_mstitem' and cmpcode like '%" & CompnyCode & "%' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'insert juga ke retail
                sSql = "INSERT INTO " & DB_JPT_RITEL & ".dbo.QL_MSTITEM ([cmpcode],[itemoid],[itemcode],[itemdesc], " & _
                 "[itemgroupoid],[itemsubgroupoid],[itembarcode1]," & _
                 "[itembarcode2], [itembarcode3], " & _
                 "[itempriceunit1], [itempriceunit2], [itempriceunit3], " & _
                 "[satuan1], [satuan2], [satuan3], " & _
                 "[itemsafetystockunit1],[itempictureloc]," & _
                 "[acctgoid],[createuser],[upduser],[updtime], [itemflag], konversi1_2, konversi2_3,personoid, keterangan, editharga,merk,maxstock,payoid,[itempriceunit1retail], [itempriceunit2retail], [itempriceunit3retail],bottompricegrosir,bottompriceretail,itemsafetystockunitgrosir,maxstockgrosir) " & _
                 "values ('" & CompnyCode & "'," & _
                 "" & Session("ItemOid") & "," & _
                 "'" & Tchar(itemcode.Text) & "', " & _
                 "'" & Tchar(itemdesc.Text) & "'," & _
                 "" & itemgroupoid.SelectedValue & ", " & _
                 "" & itemsubgroupoid.SelectedValue & "," & _
                 "'" & Tchar(itembarcode1.Text) & "'," & _
                 "'" & Tchar(itembarcode2.Text) & "'," & _
                 "'" & Tchar(itembarcode3.Text) & "'," & _
                 "" & ToDouble(itempriceunit1.Text) & "," & _
                 "" & ToDouble(itempriceunit2.Text) & "," & _
                 "" & ToDouble(itempriceunit3.Text) & "," & _
                 "'" & satuan1.Items(satuan1.SelectedIndex).Value & "'," & _
                 "'" & satuan2.Items(satuan2.SelectedIndex).Value & "'," & _
                 "'" & satuan3.Items(satuan3.SelectedIndex).Value & "'," & _
                 "" & ToDouble(itemsafetystock.Text) & ", " & _
                 "'item_" & Trim(itemoid.Text) & ".jpg'," & _
                 "" & acctgoid.SelectedValue & "," & _
                 "'" & Session("UserID") & "', " & _
                 "'" & Session("UserID") & "'," & _
                 "current_timestamp, " & _
                 "'" & ddlStatus.SelectedValue & "', " & ToDouble(konversi1_2.Text) & "," & _
                 "'" & ToDouble(konversi2_3.Text) & "','" & spgOid.SelectedValue & "', '" & Tchar(keterangan.Text) & "', '" & editprice.SelectedValue & "','" & Tchar(merk.Text) & "'," & ToDouble(Maxstock.Text) & "," & payment.SelectedValue & "," & _
                       "" & ToDouble(itempriceunit1.Text) & "," & _
                       "" & ToDouble(itempriceunit2.Text) & "," & _
                       "" & ToDouble(itempriceunit3.Text) & "," & _
                       "" & ToDouble(bottompriceGrosir.Text) & "," & ToDouble(bottompriceRetail.Text) & "," & ToDouble(itemsafetystockgrosir.Text) & "," & ToDouble(Maxstockgrosir.Text) & ") "

                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update " & DB_JPT_RITEL & ".dbo.QL_mstoid set lastoid=" & Session("ItemOid") & " where " & _
                       "tablename='QL_mstitem' and cmpcode like '%" & CompnyCode & "%' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()



            Else
                sSql = "UPDATE " & DB_JPT_GROSIR & ".dbo.QL_MSTITEM SET [itemcode]='" & Tchar(itemcode.Text) & "'," & _
                       "[itemname]='', " & _
                       "[itemdesc]='" & Tchar(itemdesc.Text) & "'," & _
                       " merk='" & Tchar(merk.Text) & "', " & _
                       "[itemgroupoid]=" & itemgroupoid.SelectedValue & ", " & _
                       "[itemsubgroupoid]=" & itemsubgroupoid.SelectedValue & "," & _
                       "[itembarcode1]='" & Tchar(itembarcode1.Text) & "', " & _
                       "[itembarcode2]='" & Tchar(itembarcode2.Text) & "', " & _
                       "[itembarcode3]='" & Tchar(itembarcode3.Text) & "', " & _
                       "[itempriceunit1]=" & ToDouble(itempriceunit1.Text) & "," & _
                       "[itempriceunit2]=" & ToDouble(itempriceunit2.Text) & "," & _
                       "[itempriceunit3]=" & ToDouble(itempriceunit3.Text) & "," & _
                       "[itempriceunit1retail]=" & ToDouble(itempriceunit1.Text) & "," & _
                       "[itempriceunit2retail]=" & ToDouble(itempriceunit2.Text) & "," & _
                       "[itempriceunit3retail]=" & ToDouble(itempriceunit3.Text) & "," & _
                       "[bottompricegrosir]=" & ToDouble(bottompriceGrosir.Text) & "," & _
                       "[satuan1]=" & satuan1.SelectedValue & "," & _
                       "[satuan2]=" & satuan2.SelectedValue & "," & _
                       "[satuan3]=" & satuan3.SelectedValue & "," & _
                       "[pricelist]=" & ToDouble(PriceList.Text) & "," & _
                       "[discunit1]=" & ToDouble(DiscUnit1.Text) & "," & _
                       "[discunit2]=" & ToDouble(DiscUnit2.Text) & "," & _
                       "[discunit3]=" & ToDouble(DiscUnit3.Text) & "," & _
                       "[qty3_to2]=" & ToDouble(qty3_to2.Text) & "," & _
                       "[qty3_to1]=" & ToDouble(Qty3_to1.Text) & "," & _
                       "[itemsafetystockunit1]=" & ToDouble(itemsafetystock.Text) & ", " & _
                       "[maxstock]=" & ToDouble(Maxstock.Text) & ", " & _
                       "payoid=" & payment.SelectedValue & ", " & _
                       "[itempictureloc]='item_" & Trim(itemoid.Text) & ".jpg'," & _
                       "[acctgoid]='" & acctgoid.SelectedValue & "', " & _
                       "[upduser]='" & Session("UserID") & "',[updtime]=current_timestamp,  [itemflag]='" & ddlStatus.SelectedValue & "' , konversi1_2=" & ToDouble(konversi1_2.Text) & ", konversi2_3=" & ToDouble(konversi2_3.Text) & ", personoid = '" & spgOid.SelectedValue & "', keterangan='" & Tchar(keterangan.Text) & "' , editharga='" & editprice.SelectedValue & "' ,itemsafetystockunitgrosir = " & ToDouble(itemsafetystockgrosir.Text) & ",maxstockgrosir =" & ToDouble(Maxstockgrosir.Text) & " ,  persentobtmprice= " & ToDouble(persentobtmprice.Text) & "  WHERE cmpcode " & _
                       "like '%" & CompnyCode & "%' and itemoid=" & Session("oid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                ' "[itempriceunit1retail]=" & ToDouble(itempriceunit1retail.Text) & "," & _
                '"[itempriceunit2retail]=" & ToDouble(itempriceunit2retail.Text) & "," & _
                '"[itempriceunit3retail]=" & ToDouble(itempriceunit3retail.Text) & "," & _
                '"[bottompriceretail]=" & ToDouble(bottompriceRetail.Text) & "," & _

                'update juga ke retail
                sSql = "UPDATE " & DB_JPT_RITEL & ".dbo.QL_MSTITEM SET [itemcode]='" & Tchar(itemcode.Text) & "'," & _
                    "[itemname]='', " & _
                    "[itemdesc]='" & Tchar(itemdesc.Text) & "'," & _
                    " merk='" & Tchar(merk.Text) & "', " & _
                    "[itemgroupoid]=" & itemgroupoid.SelectedValue & ", " & _
                    "[itemsubgroupoid]=" & itemsubgroupoid.SelectedValue & "," & _
                    "[itembarcode1]='" & Tchar(itembarcode1.Text) & "', " & _
                    "[itembarcode2]='" & Tchar(itembarcode2.Text) & "', " & _
                    "[itembarcode3]='" & Tchar(itembarcode3.Text) & "', " & _
                    "[itempriceunit1]=" & ToDouble(itempriceunit1.Text) & "," & _
                    "[itempriceunit2]=" & ToDouble(itempriceunit2.Text) & "," & _
                    "[itempriceunit3]=" & ToDouble(itempriceunit3.Text) & "," & _
                    "[itempriceunit1retail]=" & ToDouble(itempriceunit1.Text) & "," & _
                    "[itempriceunit2retail]=" & ToDouble(itempriceunit2.Text) & "," & _
                    "[itempriceunit3retail]=" & ToDouble(itempriceunit3.Text) & "," & _
                    "[bottompricegrosir]=" & ToDouble(bottompriceGrosir.Text) & "," & _
                    "[satuan1]=" & satuan1.SelectedValue & "," & _
                    "[satuan2]=" & satuan2.SelectedValue & "," & _
                    "[satuan3]=" & satuan3.SelectedValue & "," & _
                    "[itemsafetystockunit1]=" & ToDouble(itemsafetystock.Text) & ", " & _
                    "[maxstock]=" & ToDouble(Maxstock.Text) & ", " & _
                    "payoid=" & payment.SelectedValue & ", " & _
                    "[itempictureloc]='item_" & Trim(itemoid.Text) & ".jpg'," & _
                    "[acctgoid]='" & acctgoid.SelectedValue & "', " & _
                    "[upduser]='" & Session("UserID") & "',[updtime]=current_timestamp,  konversi1_2=" & ToDouble(konversi1_2.Text) & ", konversi2_3=" & ToDouble(konversi2_3.Text) & ",  itemsafetystockunitgrosir = " & ToDouble(itemsafetystockgrosir.Text) & ",maxstockgrosir =" & ToDouble(Maxstockgrosir.Text) & "   WHERE cmpcode " & _
                    "like '%" & CompnyCode & "%' and itemoid=" & Session("oid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'editharga='" & editprice.SelectedValue & "' ,
                'personoid = '" & spgOid.SelectedValue & "', keterangan='" & Tchar(keterangan.Text) & "' ,
                '[itemflag]='" & ddlStatus.SelectedValue & "' , 
                ' "[itempriceunit1retail]=" & ToDouble(itempriceunit1retail.Text) & "," & _
                '"[itempriceunit2retail]=" & ToDouble(itempriceunit2retail.Text) & "," & _
                '"[itempriceunit3retail]=" & ToDouble(itempriceunit3retail.Text) & "," & _
                '"[bottompriceretail]=" & ToDouble(bottompriceRetail.Text) & "," & _

            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR") : Exit Sub
        End Try
        Session("oid") = Nothing
        Image1.ImageUrl = "~/images/information.png"
        'showMessage("Data telah tersimpan !", "INFORMASI")
        Response.Redirect("~\Master\mstitemhpp.aspx")
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If itemoid.Text = "" Then
            showMessage("Silahkan Pilih Data Barang !!", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim sColomnName() As String = {"refoid", "itemoid", "itemoid"}
        Dim sTable() As String = {"ql_crdmtr", "QL_podtl", "QL_trnorderdtl"}
        If CheckDataExists(Session("oid"), sColomnName, sTable) = True Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain(Retail) !", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from " & DB_JPT_RITEL & ".dbo.ql_crdmtr where refoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain(Retail) !", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from " & DB_JPT_RITEL & ".dbo.QL_podtl where itemoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain(Retail) !", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from " & DB_JPT_RITEL & ".dbo.QL_trnorderdtl where itemoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain(Retail) !", CompnyName & " - Warning")
            Exit Sub
        End If


        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "delete from " & DB_JPT_RITEL & ".dbo.QL_MSTITEM WHERE [cmpcode] = '" & CompnyCode & "' AND " & _
                   "[itemoid] = " & Session("oid") & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from " & DB_JPT_GROSIR & ".dbo.QL_MSTITEM WHERE [cmpcode] = '" & CompnyCode & "' AND " & _
                  "[itemoid] = " & Session("oid") & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR") : Exit Sub
        End Try

        Image1.ImageUrl = "~/images/information.png"
        'showMessage("Data telah terhapus !", "INFORMASI")
        Response.Redirect("~\Master\mstitemhpp.aspx?awal=true")
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Master\mstitemhpp.aspx?awal=true")
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()

        If Validasi.Text = "Data telah tersimpan !" Or Validasi.Text = "Data telah terhapus !" Then
            Me.Response.Redirect("~\Master\mstitemhpp.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim fname As String = getFileName(Trim(itemoid.Text))
        uploadFileGambar("insert", fname)
    End Sub

#End Region

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "SUMMARY"
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoItem") = ""
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)

    End Sub

    Protected Sub itempriceunit1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itempriceunit1.TextChanged
        itempriceunit1.Text = NewMaskEdit(ToDouble(itempriceunit1.Text))
        If itempriceunit1.Text = "" Then
            itempriceunit1.Text = "0"
        End If
        hargaPCSan()
    End Sub

    Protected Sub itemdesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemdesc.TextChanged
        itemdesc.Text = itemdesc.Text.Replace("'", "")
        If (Session("oid") <> Nothing And Session("oid") <> "") = False Then
            generateItemId(itemdesc.Text)
        End If
    End Sub

    Protected Sub generateItemId(ByVal namaBarang As String)
        Dim namaItem, jumlahNol, c, c2, kodeItem As String
        jumlahNol = ""
        Dim a, b, d As Integer
        namaItem = Left(itemdesc.Text, 1).ToUpper
        c = ToDouble(GetStrData("SELECT isnull(max(abs(replace(itemcode, '" & Tchar(namaItem) & "', ''))),0) FROM " & DB_JPT_GROSIR & ".dbo.ql_mstitem WHERE left(itemcode,1) = '" & Tchar(namaItem) & "' "))
        c2 = ToDouble(GetStrData("SELECT isnull(max(abs(replace(itemcode, '" & Tchar(namaItem) & "', ''))),0) FROM " & DB_JPT_RITEL & ".dbo.ql_mstitem WHERE left(itemcode,1) = '" & Tchar(namaItem) & "' "))

        If CInt(c) > CInt(c2) Then
            d = CInt(c) + 1
        Else
            d = CInt(c2) + 1
        End If

        kodeItem = GenNumberString(namaItem, "", d, 5)
        itemcode.Text = kodeItem
    End Sub

    Protected Sub itempriceunit2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itempriceunit2.Text = NewMaskEdit(ToDouble(itempriceunit2.Text))
        hargaPCSan()
    End Sub

    Protected Sub itempriceunit3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itempriceunit3.Text = NewMaskEdit(ToDouble(itempriceunit3.Text))
    End Sub

    Protected Sub konversi1_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        konversi1_2.Text = NewMaskEdit(ToDouble(konversi1_2.Text))
        'Qty3_to1.Text = NewMaskEdit(ToDouble(konversi1_2.Text) * ToDouble(konversi2_3.Text))
        hitunghargapricelist()
    End Sub

    Protected Sub itemsafetystock_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemsafetystock.Text = NewMaskEdit(ToDouble(itemsafetystock.Text))
    End Sub

    Protected Sub Maxstock_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Maxstock.TextChanged
        Maxstock.Text = NewMaskEdit(ToDouble(Maxstock.Text))
    End Sub


    Protected Sub konversi2_3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        konversi2_3.Text = NewMaskEdit(ToDouble(konversi2_3.Text))
        'qty3_to2.Text = NewMaskEdit(ToDouble(konversi2_3.Text))
        hitunghargapricelist()
    End Sub

    Protected Sub GVmstgen_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstgen.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = NewMaskEdit(ToDouble(e.Row.Cells(4).Text))
            e.Row.Cells(5).Text = NewMaskEdit(ToDouble(e.Row.Cells(5).Text))
            e.Row.Cells(7).Text = NewMaskEdit(ToDouble(e.Row.Cells(7).Text))
            e.Row.Cells(9).Text = NewMaskEdit(ToDouble(e.Row.Cells(9).Text))
            e.Row.Cells(11).Text = NewMaskEdit(ToDouble(e.Row.Cells(11).Text))

            e.Row.Cells(12).Text = NewMaskEdit(ToDouble(e.Row.Cells(12).Text))
            e.Row.Cells(13).Text = NewMaskEdit(ToDouble(e.Row.Cells(13).Text))
            

        End If
    End Sub

    Protected Sub btnprintlist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "DETAIL"
        orderNoForReport.Text = sender.ToolTip : orderIDForReport.Text = sender.CommandArgument()
        Session("NoItem") = sender.ToolTip
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintContract(printType.Text, orderIDForReport.Text, orderNoForReport.Text, ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintContract(printType.Text, orderIDForReport.Text, orderNoForReport.Text, ExportFormatType.Excel)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        orderIDForReport.Text = "" : orderNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        rptSupp.Close() : rptSupp.Dispose()
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)
        Dim sWhere As String = ""

        '"  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
        '        sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '%" & Tchar(FilterText2.Text) & "%' "
        '        sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '%" & Tchar(FilterText3.Text) & "%' "

        If FilterText.Text.Trim <> "" Then
            If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
            Else
                sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If
        End If

        If FilterText2.Text.Trim <> "" Then
            If FilterDDL2.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL2.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL2.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '" & Tchar(FilterText2.Text) & "' "
            Else
                sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '%" & Tchar(FilterText2.Text) & "%' "
            End If
        End If

        If FilterText3.Text.Trim <> "" Then
            If FilterDDL3.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL3.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL3.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '" & Tchar(FilterText3.Text) & "' "
            Else
                sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '%" & Tchar(FilterText3.Text) & "%' "
            End If
        End If

        Response.Clear()




        If printType = "SUMMARY" Then
            Response.AddHeader("content-disposition", "inline;filename=mstitem.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"

            sSql = "SELECT 	  itemoid, itemcode, itemdesc,merk, QL_mstgen.gendesc as itemgroup ,ggdtl.gendesc as itemsubgroup , itembarcode1, itembarcode2, itembarcode3, itempriceunit1, itempriceunit2, itempriceunit3, g1.gendesc as satuan1, g2.gendesc as satuan2, g3.gendesc as satuan3,konversi1_2,konversi2_3, itemsafetystockunit1 stockbawahRetail , maxstock stockatasRetail ,itemsafetystockunitgrosir stockbawahGrosir , maxstockgrosir maxstockGrosir , pricelist, hpp , persentobtmprice ,bottompricegrosir , discunit1 , discunit2 , discunit3 , qty3_to2 Min_JualPartai , qty3_to1 Min_JualQty ,gpay.gendesc Payment , QL_mstitem.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as SPG ,(select isnull(SUM(saldoAkhir - qtyBooking ),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = QL_mstItem.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 = (select genoid from QL_mstgen where genother1  = 'GROSIR' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')  ) stockGrosir " & _
  " ,(select isnull(SUM(saldoAkhir - qtyBooking ),0) from ql_jpt.dbo.QL_crdmtr where closingdate = '1/1/1900' and refoid = QL_mstItem.itemoid and mtrlocoid in (691,706,718,719,721,722) and mtrlocoid in (select genoid from ql_jpt.dbo.QL_mstgen where genother1 = (select genoid from ql_jpt.dbo.QL_mstgen where genother1  = 'RETAIL' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')  ) stockRetail	" & _
" ,(select isnull(SUM(saldoAkhir - qtyBooking ),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = QL_mstItem.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 = (select genoid from QL_mstgen where genother1  = 'SUPPLIER' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')  ) stockSupp" & _
" FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid =  QL_mstgen.genoid inner join QL_mstgen gsub on QL_mstitem.itemsubgroupoid =  gsub.genoid  inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1  inner join ql_mstgen g2 on g2.genoid=QL_mstitem.satuan2  inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3 inner join ql_mstgen ggdtl on ggdtl.genoid = ql_mstitem.itemsubgroupoid inner join QL_mstgen gpay on gpay.genoid = QL_MSTITEM.payoid " & sWhere & " ORDER BY itemdesc    "

        ElseIf printType = "DETAIL" Then

            Response.AddHeader("content-disposition", "inline;filename=mstitemdtl.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"

            sSql = "SELECT  itemoid, itemcode, itemdesc, QL_mstgen.gendesc as itemgroupoid, itemsubgroupoid, itembarcode1, itembarcode2, itembarcode3, itempriceunit1, itempriceunit2, itempriceunit3, g1.gendesc as satuan1, g2.gendesc as satuan2, g3.gendesc as satuan3,konversi1_2,konversi2_3, itemsafetystockunit1, QL_mstitem.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as SPG FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid =  QL_mstgen.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1  inner join ql_mstgen g2 on g2.genoid=QL_mstitem.satuan2  inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3  " & sWhere & "  and itemoid = " & oid

        End If


        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()


        Response.End()
        mpePrint.Show()

    End Sub

    Protected Sub orderby_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles orderby.SelectedIndexChanged
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        Session("orderby") = orderby.SelectedIndex

        Session("FilterText2") = FilterText2.Text
        Session("FilterDDL2") = FilterDDL2.SelectedIndex

        Session("FilterText3") = FilterText3.Text
        Session("FilterDDL3") = FilterDDL3.SelectedIndex

        BindData(CompnyCode)
        ''findAData()
    End Sub

    Protected Sub FilterDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If FilterDDL.SelectedValue = "infoharga" Then
        '    FilterText.Visible = False
        '    FilterText.Text = ""
        '    ddlStatusHarga.Visible = True
        '    ddlStatusHarga.SelectedIndex = 0
        'Else
        '    FilterText.Visible = True
        '    FilterText.Text = ""
        '    ddlStatusHarga.Visible = False
        '    ddlStatusHarga.SelectedIndex = 0
        'End If

    End Sub

    Protected Sub keterangan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles keterangan.TextChanged

    End Sub

    Protected Sub itempriceunit1retail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itempriceunit1retail.Text = NewMaskEdit(ToDouble(itempriceunit1retail.Text))
        If itempriceunit1retail.Text = "" Then
            itempriceunit1retail.Text = "0"
        End If
    End Sub

    Protected Sub itempriceunit2retail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itempriceunit2retail.Text = NewMaskEdit(ToDouble(itempriceunit2retail.Text))
        If itempriceunit2retail.Text = "" Then
            itempriceunit2retail.Text = "0"
        End If
    End Sub

    Protected Sub bottompriceGrosir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bottompriceGrosir.Text = NewMaskEdit(ToDouble(bottompriceGrosir.Text))
        If bottompriceGrosir.Text = "" Then
            bottompriceGrosir.Text = "0"
        End If
    End Sub

    Protected Sub bottompriceRetail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bottompriceRetail.Text = NewMaskEdit(ToDouble(bottompriceRetail.Text))
        If bottompriceRetail.Text = "" Then
            bottompriceRetail.Text = "0"
        End If
    End Sub

    Protected Sub itempriceunit3retail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itempriceunit3retail.Text = NewMaskEdit(ToDouble(itempriceunit3retail.Text))
        If itempriceunit3retail.Text = "" Then
            itempriceunit3retail.Text = "0"
        End If
    End Sub

    Protected Sub itemsafetystockgrosir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemsafetystockgrosir.TextChanged
        itemsafetystockgrosir.Text = NewMaskEdit(ToDouble(itemsafetystockgrosir.Text))
    End Sub

    Protected Sub Maxstockgrosir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Maxstockgrosir.TextChanged
        Maxstockgrosir.Text = NewMaskEdit(ToDouble(Maxstockgrosir.Text))
    End Sub

    Protected Sub itemgroupoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and (genother1='' or genother1='" & itemgroupoid.SelectedValue & "') and cmpcode='" & CompnyCode & "' ORDER BY gendesc"

        If cKon.ambilscalar(sSql) > 0 Then
            FillDDL(itemsubgroupoid, sSql)
        Else
            showMessage("Isi Sub Grup Barang di Data General terlebih dahulu !!", CompnyName & " - INFORMASI")
            btnSave.Visible = False
        End If
    End Sub


    Protected Sub PriceList_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PriceList.Text = NewMaskEdit(ToDouble(PriceList.Text))
        If PriceList.Text = "" Then
            PriceList.Text = "0"
        End If
        hitunghargapricelist()
    End Sub

    Protected Sub DiscUnit1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DiscUnit1.Text = NewMaskEdit(ToDouble(DiscUnit1.Text))
        If DiscUnit1.Text = "" Then
            DiscUnit1.Text = "0"
        End If
        hitunghargapricelist()
    End Sub

    Protected Sub DiscUnit2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DiscUnit2.Text = NewMaskEdit(ToDouble(DiscUnit2.Text))
        If DiscUnit2.Text = "" Then
            DiscUnit2.Text = "0"
        End If
        hitunghargapricelist()
    End Sub

    Protected Sub DiscUnit3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DiscUnit3.Text = NewMaskEdit(ToDouble(DiscUnit3.Text))
        If DiscUnit3.Text = "" Then
            DiscUnit3.Text = "0"
        End If
        hitunghargapricelist()
    End Sub

    Sub hitunghargapricelist()
        If ToDouble(PriceList.Text) <> 0 Then

            Dim hargaecer As Integer = 0
            Dim hargapartai As Integer = 0
            Dim hargaqty As Integer = 0

            '-----------------------------------------------------------------
            hargaecer = ToDouble(PriceList.Text) - (ToDouble(PriceList.Text) * ToDouble(DiscUnit3.Text) / 100)

            If hargaecer < 10000 Then
                If hargaecer Mod 100 >= 50 Then
                    hargaecer = hargaecer - (hargaecer Mod 100) + 100
                Else
                    hargaecer = hargaecer - (hargaecer Mod 100)
                End If
            ElseIf hargaecer < 100000 Then
                If hargaecer Mod 500 >= 250 Then
                    hargaecer = hargaecer - (hargaecer Mod 500) + 500
                Else
                    hargaecer = hargaecer - (hargaecer Mod 500)
                End If
            Else
                If hargaecer Mod 1000 >= 500 Then
                    hargaecer = hargaecer - (hargaecer Mod 1000) + 1000
                Else
                    hargaecer = hargaecer - hargaecer Mod 1000
                End If
            End If

            '-----------------------------------------------------------------
            hargapartai = (ToDouble(PriceList.Text) - (ToDouble(PriceList.Text) * ToDouble(DiscUnit2.Text) / 100))

            If hargapartai < 10000 Then
                If hargapartai Mod 100 >= 50 Then
                    hargapartai = hargapartai - (hargapartai Mod 100) + 100
                Else
                    hargapartai = hargapartai - (hargapartai Mod 100)
                End If

            ElseIf hargapartai < 100000 Then
                If hargapartai Mod 500 >= 250 Then
                    hargapartai = hargapartai - (hargapartai Mod 500) + 500
                Else
                    hargapartai = hargapartai - (hargapartai Mod 500)
                End If
            Else
                If hargapartai Mod 1000 >= 500 Then
                    hargapartai = hargapartai - (hargapartai Mod 1000) + 1000
                Else
                    hargapartai = hargapartai - hargapartai Mod 1000
                End If
            End If

            hargapartai = hargapartai * ToDouble(konversi2_3.Text)

            '--------------------------------------------------------------
            hargaqty = (ToDouble(PriceList.Text) - (ToDouble(PriceList.Text) * ToDouble(DiscUnit1.Text) / 100))


            If hargaqty < 10000 Then
                If hargaqty Mod 100 >= 50 Then
                    hargaqty = hargaqty - (hargaqty Mod 100) + 100
                Else
                    hargaqty = hargaqty - (hargaqty Mod 100)
                End If

            ElseIf hargaqty < 100000 Then
                If hargaqty Mod 500 >= 250 Then
                    hargaqty = hargaqty - (hargaqty Mod 500) + 500
                Else
                    hargaqty = hargaqty - (hargaqty Mod 500)
                End If
            Else
                If hargaqty Mod 1000 >= 500 Then
                    hargaqty = hargaqty - (hargaqty Mod 1000) + 1000
                Else
                    hargaqty = hargaqty - hargaqty Mod 1000
                End If
            End If

            hargaqty = hargaqty * ToDouble(konversi2_3.Text) * ToDouble(konversi1_2.Text)
            '--------------------------------------------------------------
            itempriceunit3.Text = NewMaskEdit(hargaecer)
            itempriceunit2.Text = NewMaskEdit(hargapartai)
            itempriceunit1.Text = NewMaskEdit(hargaqty)
        End If
        hargaPCSan()
    End Sub

    Sub hargaPCSan()
        lblhrgQtyPcs.Text = NewMaskEdit((ToDouble(itempriceunit1.Text) / ToDouble(konversi1_2.Text)) / ToDouble(konversi2_3.Text))
        lblPrtPcs.Text = NewMaskEdit((ToDouble(itempriceunit2.Text) / ToDouble(konversi2_3.Text)))
    End Sub


    Protected Sub qty3_to2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        qty3_to2.Text = NewMaskEdit(ToDouble(qty3_to2.Text))
        If qty3_to2.Text = "" Then
            qty3_to2.Text = "0"
        End If
    End Sub

    Protected Sub qty2_to1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Qty3_to1.Text = NewMaskEdit(ToDouble(Qty3_to1.Text))
        If Qty3_to1.Text = "" Then
            Qty3_to1.Text = "0"
        End If
    End Sub

    Protected Sub hpp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub persentobtmprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If persentobtmprice.Text = "" Then
            persentobtmprice.Text = "1"
        End If
        If ToDouble(persentobtmprice.Text) < 1 Then
            persentobtmprice.Text = "1"
        End If
        If hpp.Text = "" Then
            hpp.Text = 0
        End If
        persentobtmprice.Text = ToMaskEdit(persentobtmprice.Text, 3)
        bottompriceGrosir.Text = ToMaskEdit(ToDouble(persentobtmprice.Text) * ToDouble(hpp.Text), 3)
    End Sub
End Class