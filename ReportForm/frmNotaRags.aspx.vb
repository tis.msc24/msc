Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_NotaRags
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLCOA(ByRef oDDLObject As DropDownList, ByVal sVar() As String, ByVal sCabang As String) As Boolean
        'Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        'Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        'Dim xCmd As New SqlCommand("", conn)
        'Dim xreader As SqlDataReader
        FillDDLCOA = True
        oDDLObject.Items.Clear()

        For C1 As Integer = 0 To sVar.Length - 1
            Dim sSql As String = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar(C1) & "' "
            If sCabang <> "ALL" Then
                sSql &= " AND interfaceres1='" & sCabang & "'"
            End If

            Dim sCode As String = GetStrData(sSql)
            'If sCode = "" Then
            '    sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar(C1) & "' AND interfaceres1='All'"
            '    sCode = GetStrData(sSql)
            'End If
            If sCode <> "" Then
                sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' /*AND a.activeflag='ACTIVE'*/ AND ("
                Dim sSplitCode() As String = sCode.Split(",")
                For C2 As Integer = 0 To sSplitCode.Length - 1
                    sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C2)) & "%'"
                    If C2 < sSplitCode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                xCmd.CommandText = sSql
                xreader = xCmd.ExecuteReader
                While xreader.Read
                    oDDLObject.Items.Add(xreader.GetValue(1))
                    oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
                End While
                xreader.Close()
                conn.Close()
            End If
        Next

        If oDDLObject.Items.Count = 0 Then
            FillDDLCOA = False
        End If
        Return FillDDLCOA

    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "cashbankoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblDO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtTbl2 As DataTable = Session("TblDOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "cashbankoid=" & cbOid
                                dtView2.RowFilter = "cashbankoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblDO") = dtTbl
                Session("TblDOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedReg() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblReg") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblReg")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListReg.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListReg.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "registermstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblReg") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedReg2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblReg")
            Dim dtTbl2 As DataTable = Session("TblRegView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListReg.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListReg.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "registermstoid=" & cbOid
                                dtView2.RowFilter = "registermstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblReg") = dtTbl
                Session("TblRegView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListAR.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListAR.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListAR.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListAR.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 

    Private Sub BindListNo()
        sSql = "SELECT 'False' AS checkvalue, cashbankoid, cashbankno, acctgdesc, CONVERT(VARCHAR(10), cbm.createtime, 101) AS createdate, cashbanknote, cbm.cashbankacctgoid FROM QL_trncashbankmst cbm INNER JOIN QL_mstacctg a ON a.acctgoid=cbm.cashbankacctgoid WHERE cbm.cmpcode='" & CompnyCode & "' AND cashbankgroup = 'RAGS' AND cbm.cashbankoid>0"

        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " AND cbm.branch_code='" & DDLBusUnit.SelectedValue & "'"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            sSql &= " AND cashbankdate BETWEEN CAST('" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AS DATETIME) AND CAST('" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59' AS DATETIME)"
        End If

        If DDLPayType.SelectedIndex <> 0 Then
            sSql &= " AND cashbanktype='" & DDLPayType.SelectedValue & "'"
        End If

        If FilterTextCust.Text <> "" Then
            Dim scode() As String = Split(FilterTextCust.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " a.acctgcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        sSql &= " ORDER BY cashbankno"
        Session("TblDO") = cKon.ambiltabel(sSql, "QL_trncashbankmst")
        gvListDO.DataSource = Session("TblDO") : gvListDO.DataBind()
    End Sub 

    Private Sub BindListCust()
        sSql = "Select interfacevalue From QL_mstinterface Where cmpcode='" & CompnyCode & "'"
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "AND interfaceres1='" & DDLBusUnit.SelectedValue & "'"
        End If

        If DDLPayType.SelectedValue = "ALL" Then
            sSql &= " And interfacevar IN ('VAR_BANK','VAR_CASH')"
        ElseIf DDLPayType.SelectedValue = "BKM" Then
            sSql &= " And interfacevar IN ('VAR_CASH')"
        ElseIf DDLPayType.SelectedValue = "BBM" Then
            sSql &= " And interfacevar IN ('VAR_BANK')"
        End If

        Dim Vardt As DataTable = cKon.ambiltabel(sSql, "QL_mstinterface")
        Dim sSplit As String = "", sMek As String = ""
        For e As Integer = 0 To Vardt.Rows.Count - 1
            sSplit &= Vardt.Rows(e)("interfacevalue").ToString & ","
        Next
        sMek = sSplit

        sSql = "Select Top 100 'False' AS checkvalue, a.acctgoid custoid, a. acctgcode custcode, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS custname From ql_mstacctg a Where a.acctgcode IN (SELECT * FROM splitstring('" & sMek & "')) Order By acctgcode"
        Session("TblCust") = cKon.ambiltabel(sSql, "QL_mstcust")
        gvListCust.DataSource = Session("TblCust") : gvListCust.DataBind()
    End Sub 

    Private Sub BindListReg()
        sSql = "SELECT 'False' AS checkvalue, pr.trnragsmstoid registermstoid, trnragsno registerno, cashbankno Jabatan, pr.trnragsstatus [Status], jp.acctgdesc PERSONNIP FROM QL_trnragsmst pr Inner Join QL_trncashbankmst pf ON pf.cashbankoid=pr.cashbankmstoid AND pr.branch_code=pf.branch_code Inner Join QL_mstacctg jp ON jp.acctgoid=pf.cashbankacctgoid Where pr.cmpcode='" & CompnyCode & "' AND pf.cashbankgroup='RAGS'"
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "AND pf.branch_code='" & DDLBusUnit.SelectedValue & "'"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            sSql &= " AND cashbankdate BETWEEN CAST('" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AS DATETIME) AND CAST('" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59' AS DATETIME)"
        End If

        If FilterTextCust.Text <> "" Then
            Dim scode() As String = Split(FilterTextCust.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " jp.acctgcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterTextNo.Text <> "" Then
            Dim scode() As String = Split(FilterTextNo.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " jp.cashbankno = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        sSql &= " ORDER BY trnragsno DESC"
        Session("TblReg") = cKon.ambiltabel(sSql, "TblReg")
    End Sub

    Private Sub BindListMat() 
        sSql = "SELECT DISTINCT 'False' AS checkvalue, i.itemoid, i.itemdesc itemlongdesc, i.itemcode, 'BUAH' unit, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' End JenisNya, i.stockflag FROM QL_trnragsdtl dod INNER JOIN QL_trnragsmst dom ON dom.cmpcode=dod.cmpcode AND dom.trnragsmstoid=dod.trnragsmstoid AND dod.branch_code=dom.branch_code INNER JOIN QL_mstitem i ON i.itemoid=dod.itemoid INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=dom.cashbankmstoid AND cb.branch_code=dom.branch_code INNER JOIN QL_mstacctg a ON a.acctgoid=cb.cashbankacctgoid"
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " AND dom.branch_code='" & DDLBusUnit.SelectedValue & "'"
        End If

        If FilterTextCust.Text <> "" Then
            Dim scode() As String = Split(FilterTextCust.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " a.acctgcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterTextNo.Text <> "" Then
            Dim sSono() As String = Split(FilterTextNo.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sSono.Length - 1
                sSql &= " cb.cashbankno LIKE '%" & Tchar(sSono(c1)) & "%'"
                If c1 < sSono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If regno.Text <> "" Then
            Dim sDono() As String = Split(regno.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sDono.Length - 1
                sSql &= " dom.trnragsno LIKE '%" & Tchar(sDono(c1)) & "%'"
                If c1 < sDono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod1.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND cb.cashbankdate>='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND cb.cashbankdate<='" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub UpdateCheckedListAR(Optional ByVal sView As String = "")
        If Session("Tbl" & sView & "ListAR") IsNot Nothing Then
            For C1 As Integer = 0 To gvListAR.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListAR.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Session("Tbl" & sView & "ListAR").DefaultView.RowFilter = "armstoid=" & sOid
                            If cbcheck = True Then
                                If Session("Tbl" & sView & "ListAR").DefaultView.Count > 0 Then
                                    Session("Tbl" & sView & "ListAR").DefaultView(0)("checkvalue") = "True"
                                End If
                            Else
                                If Session("Tbl" & sView & "ListAR").DefaultView.Count > 0 Then
                                    Session("Tbl" & sView & "ListAR").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("Tbl" & sView & "ListAR").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String, Optional ByVal sRptType As String = "Pdf")
        ''If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
        ''    If Not IsValidPeriod() Then
        ''        Exit Sub
        ''    End If
        ''End If

        Try
            If CDate(toDate(FilterPeriod1.Text.Trim)) > CDate(toDate(FilterPeriod2.Text.Trim)) Then
                showMessage("Period 2 must be more than Period 1 !", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Please check Period value", 2)
            Exit Sub
        End Try

        Try
            Dim sRptName As String = "", sField As String = "", sJoin As String = ""
            If DDLType.SelectedValue = "SUMMARY" Then
                report.Load(Server.MapPath(folderReport & "rptRagsSum" & sRptType & ".rpt"))
                sRptName = "NotaRombengSummaryReport"
            Else
                report.Load(Server.MapPath(folderReport & "rptRagsDtl" & sRptType & ".rpt"))
                sRptName = "NotaRombengDetailReport"
            End If

            If DDLType.SelectedValue = "DETAIL" Then
                sField = ", i.itemoid, i.itemcode, i.itemdesc, rd.trnragsdtlprice, rd.trnragsdtlqty, rd.trnragsdtlnetto"
                sJoin = " INNER JOIN QL_trnragsdtl rd ON rg.trnragsmstoid=rd.trnragsmstoid INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid INNER JOIN QL_mstgen lo ON lo.genoid=rd.mtrlocoid AND lo.gengroup='LOCATION'"
            End If

            sSql = "Select rg.branch_code, bc.gendesc Cabang, rg.trnragsmstoid, rg.trnragsno, rg.trnragsdate, cb.cashbankno, a.acctgcode, a.acctgdesc, rg.trnragsamt, rg.trnragsnote, rg.trnragsstatus, rg.createuser" & sField & " From QL_trnragsmst rg INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=rg.cashbankmstoid AND cb.branch_code=rg.branch_code AND cb.cashbankgroup='RAGS' INNER JOIN QL_mstacctg a ON a.acctgoid=rg.acctgoid INNER JOIN QL_mstgen bc ON bc.gencode=rg.branch_code AND bc.gengroup='CABANG'" & sJoin & " WHERE cb.cmpcode='" & CompnyCode & "'"
            If DDLBusUnit.SelectedValue <> "ALL" Then
                sSql &= " AND rg.branch_code='" & DDLBusUnit.SelectedValue & "'"
            End If

            If DDLPayType.SelectedIndex <> 0 Then
                sSql &= " AND cashbanktype='" & DDLPayType.SelectedValue & "'"
            End If

            If FilterTextCust.Text <> "" Then
                Dim scode() As String = Split(FilterTextCust.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sSql &= " a.acctgcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If

            If FilterTextNo.Text <> "" Then
                Dim sSono() As String = Split(FilterTextNo.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " cb.cashbankno LIKE '%" & Tchar(sSono(c1)) & "%'"
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If

            If regno.Text <> "" Then
                Dim sDono() As String = Split(regno.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sDono.Length - 1
                    sSql &= " rg.trnragsno LIKE '%" & Tchar(sDono(c1)) & "%'"
                    If c1 < sDono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If

            If FilterPeriod1.Text <> "" And FilterPeriod1.Text <> "" Then
                sSql &= " AND cb.cashbankdate>='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND cb.cashbankdate<='" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
            End If

            If DDLType.SelectedValue = "DETAIL" Then
                If FilterTextAR.Text <> "" Then
                    Dim sItem() As String = Split(FilterTextAR.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sItem.Length - 1
                        sSql &= " i.itemcode LIKE '%" & Tchar(sItem(c1)) & "%'"
                        If c1 < sItem.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            End If
            sSql &= " ORDER BY rg.updtime"

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnrombeng")
            report.SetDataSource(dtTbl)
            report.SetParameterValue("sPeriod", FilterPeriod1.Text & " - " & FilterPeriod2.Text)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT USERID FROM QL_mstprof WHERE USERID='" & Session("UserID") & "'"))
            'report.PrintOptions.PaperSize = PaperSize.PaperA4
            'cProc.SetDBLogonForReport(report)
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False : Response.ClearContent() : Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
                report.Close() : report.Dispose()
            Else
                Response.Buffer = False : Response.ClearContent() : Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
                report.Close() : report.Dispose()
            End If
        Catch ex As Exception
            report.Close() : report.Dispose()
            showMessage(ex.ToString & sSql, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("~\ReportForm\frmNotaRags.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Nota Rombeng"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            sSql = "Select gencode,gendesc From QL_mstgen Where gengroup = 'CABANG'"
            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLBusUnit, sSql)
            ElseIf Session("UserLevel") = 2 Then
                If Session("branch_id") <> "10" Then
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(DDLBusUnit, sSql)
                Else
                    FillDDL(DDLBusUnit, sSql)
                    DDLBusUnit.Items.Add(New ListItem("ALL", "ALL"))
                    DDLBusUnit.SelectedValue = "ALL"
                End If
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                sSql &= "" : FillDDL(DDLBusUnit, sSql)
                DDLBusUnit.Items.Add(New ListItem("ALL", "ALL"))
                DDLBusUnit.SelectedValue = "ALL"
            End If 
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("EmptyListDO") Is Nothing And Session("EmptyListDO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListDO") Then
                Session("EmptyListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHideListNo, pnlListNo, mpeListNo, True)
            End If
        End If
        If Not Session("WarningListDO") Is Nothing And Session("WarningListno") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListDO") Then
                Session("WarningListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHideListNo, pnlListNo, mpeListNo, True) 
            End If
        End If
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        Dim bVal As Boolean = True
        If DDLType.SelectedValue = "SUMMARY" Then
            bVal = False
        End If
        lblARNo.Visible = bVal : lblSeptARNo.Visible = bVal
        FilterTextAR.Visible = bVal : btnSearchAR.Visible = bVal
        btnClearAR.Visible = bVal
    End Sub

    Protected Sub btnSearchNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchNo.Click 
        If IsValidPeriod() Then
            FilterDDLListNo.SelectedIndex = -1 : FilterTextListNo.Text = ""
            Session("TblDO") = Nothing : Session("TblDOView") = Nothing
            gvListDO.DataSource = Nothing : gvListDO.DataBind()
            cProc.SetModalPopUpExtender(btnHideListNo, pnlListNo, mpeListNo, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnClearNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearNo.Click
        FilterTextNo.Text = ""
    End Sub

    Protected Sub btnFindListNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListNo.Click
        If Session("TblDO") Is Nothing Then
            BindListNo()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "- Maaf, data not found..!!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListNo.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListNo.Text) & "%'"
        If UpdateCheckedDO() Then
            Dim dv As DataView = Session("TblDO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblDOView") = dv.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dv.RowFilter = ""
                mpeListNo.Show()
            Else
                dv.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "- Maaf, data not found..!!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListNo.Show()
        End If
    End Sub

    Protected Sub btnAllListNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListNo.Click
        FilterDDLListNo.SelectedIndex = -1 : FilterTextListNo.Text = ""
        If Session("TblDO") Is Nothing Then
            BindListNo()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "SI data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedDO() Then
            Dim dt As DataTable = Session("TblDO")
            Session("TblDOView") = dt
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListNo.Show() 
    End Sub 

    Protected Sub lbAddToListNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListNo.Click
        If Not Session("TblDO") Is Nothing Then
            If UpdateCheckedDO() Then
                Dim dtTbl As DataTable = Session("TblDO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If FilterTextNo.Text <> "" Then
                            If dtView(C1)("cashbankno") <> "" Then
                                FilterTextNo.Text &= ";" + vbCrLf + dtView(C1)("cashbankno")
                            End If
                        Else
                            If dtView(C1)("cashbankno") <> "" Then
                                FilterTextNo.Text &= dtView(C1)("cashbankno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListNo, pnlListNo, mpeListNo, False)

                Else
                    Session("WarningListDO") = "Please select SI to add to list!"
                    showMessage(Session("WarningListDO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListDO") = "Please show some SI data first!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If

    End Sub

    Protected Sub lbCloseListNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListNo.Click
        cProc.SetModalPopUpExtender(btnHideListNo, pnlListNo, mpeListNo, False)
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If Not IsValidPeriod() Then
                Exit Sub
            End If
        End If
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = ""
        Session("TblListCust") = Nothing : Session("TblViewListCust") = Nothing
        gvListCust.DataSource = Session("TblListCust") : gvListCust.DataBind()
        BindListCust()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        FilterTextCust.Text = ""
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListCust.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "COA data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "COA data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub lbAddToListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListCust.Click 
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If FilterTextCust.Text <> "" Then
                            FilterTextCust.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            FilterTextCust.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub btnSearchAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchAR.Click
        If IsValidPeriod() Then
            FilterDDLListAR.SelectedIndex = -1 : FilterTextListAR.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListAR.DataSource = Nothing : gvListAR.DataBind()
            'tbData.Text = "5"
            gvListAR.PageSize = 5
            cProc.SetModalPopUpExtender(btnHideListAR, pnlListAR, mpeListAR, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnClearAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearAR.Click
        FilterTextAR.Text = ""
    End Sub

    Protected Sub btnFindListAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListAR.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If

        Dim sPlus As String = FilterDDLListAR.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListAR.Text) & "%'"
        If JenisBarangDDL.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag ='" & JenisBarangDDL.SelectedValue & "'"
        End If

        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListAR.DataSource = Session("TblMatView")
                gvListAR.DataBind()
                dv.RowFilter = ""
                mpeListAR.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListAR.DataSource = Session("TblMatView")
                gvListAR.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListAR.Show()
        End If
    End Sub

    Protected Sub btnAllListAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListAR.Click
        FilterDDLListAR.SelectedIndex = -1 : FilterTextListAR.Text = "" 
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListAR.DataSource = Session("TblMatView")
            gvListAR.DataBind()
        End If
        mpeListAR.Show()
    End Sub 

    Protected Sub lbAddToListAR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListAR.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If FilterTextAR.Text <> "" Then
                            FilterTextAR.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            FilterTextAR.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListAR, pnlListAR, mpeListAR, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If

    End Sub

    Protected Sub lbCloseListAR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListAR.Click
        cProc.SetModalPopUpExtender(btnHideListAR, pnlListAR, mpeListAR, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel", "Xls")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmNotaRags.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
#End Region

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
         
    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub btnSelectAllDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "cashbankoid=" & dtTbl.Rows(C1)("cashbankoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListNo.Show()
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub 

    Protected Sub btnSelectNoneDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "cashbankoid=" & dtTbl.Rows(C1)("cashbankoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListNo.Show()
        Else
            Session("WarningListDO") = "Please show some SI data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedDO.Click
        If Session("TblDO") Is Nothing Then
            Session("WarningListDO") = "Selected SI data can't be found!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
        If UpdateCheckedDO() Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListNo.SelectedIndex = -1 : FilterTextListNo.Text = ""
                Session("TblDOView") = dtView.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dtView.RowFilter = ""
                mpeListNo.Show()
            Else
                dtView.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "Selected SI data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListNo.Show()
        End If
    End Sub

    Protected Sub gvListDO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDO.PageIndexChanging
        If UpdateCheckedDO2() Then
            gvListDO.PageIndex = e.NewPageIndex
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListNo.Show()
    End Sub

    Protected Sub imbFindReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindReg.Click
        If IsValidPeriod() Then
            DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
            Session("TblReg") = Nothing : Session("TblRegView") = Nothing : gvListReg.DataSource = Nothing : gvListReg.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        If Session("TblReg") Is Nothing Then
            BindListReg()
            If Session("TblReg").Rows.Count <= 0 Then
                Session("EmptyListReg") = "Sales data can't be found!"
                showMessage(Session("EmptyListReg"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListReg.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListReg.Text) & "%'"
        If UpdateCheckedReg() Then
            Dim dv As DataView = Session("TblReg").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblRegView") = dv.ToTable
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                dv.RowFilter = ""
                mpeListReg.Show()
            Else
                dv.RowFilter = ""
                Session("TblRegView") = Nothing
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                Session("WarningListReg") = "Sales data can't be found!"
                showMessage(Session("WarningListReg"), 2)
            End If
        Else
            mpeListReg.Show()
        End If
    End Sub

    Protected Sub btnViewAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListReg.Click
        DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
        If Session("TblReg") Is Nothing Then
            BindListReg()
            If Session("TblReg").Rows.Count <= 0 Then
                Session("EmptyListReg") = "Register data can't be found!"
                showMessage(Session("EmptyListReg"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedReg() Then
            Dim dt As DataTable = Session("TblReg")
            Session("TblRegView") = dt
            gvListReg.DataSource = Session("TblRegView")
            gvListReg.DataBind()
        End If
        mpeListReg.Show()
    End Sub

    Protected Sub btnSelectAllReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllReg.Click
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblRegView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblReg")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "registermstoid=" & dtTbl.Rows(C1)("registermstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblReg") = objTbl
                Session("TblRegView") = dtTbl
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
            End If
            mpeListReg.Show()
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneReg.Click
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblRegView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblReg")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "registermstoid=" & dtTbl.Rows(C1)("registermstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblReg") = objTbl
                Session("TblRegView") = dtTbl
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
            End If
            mpeListReg.Show()
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedReg.Click
        If Session("TblReg") Is Nothing Then
            Session("WarningListReg") = "Selected sales data can't be found!"
            showMessage(Session("WarningListReg"), 2)
            Exit Sub
        End If
        If UpdateCheckedReg() Then
            Dim dtTbl As DataTable = Session("TblReg")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
                Session("TblRegView") = dtView.ToTable
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                dtView.RowFilter = ""
                mpeListReg.Show()
            Else
                dtView.RowFilter = ""
                Session("TblRegView") = Nothing
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                Session("WarningListReg") = "Selected sales data can't be found!"
                showMessage(Session("WarningListReg"), 2)
            End If
        Else
            mpeListReg.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListReg.Click
        If Not Session("TblReg") Is Nothing Then
            If UpdateCheckedReg() Then
                Dim dtTbl As DataTable = Session("TblReg")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If regno.Text <> "" Then
                            If dtView(C1)("registerno") <> "" Then
                                regno.Text &= ";" + vbCrLf + dtView(C1)("registerno")
                            End If
                        Else
                            If dtView(C1)("registerno") <> "" Then
                                regno.Text &= dtView(C1)("registerno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, False)
                Else
                    Session("WarningListReg") = "Please select sales to add to list!"
                    showMessage(Session("WarningListReg"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListReg.Click
        cProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, False)
    End Sub

    Protected Sub imbEraseReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseReg.Click
        regno.Text = ""
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListAR.DataSource = Session("TblMatView")
                gvListAR.DataBind()
            End If
            mpeListAR.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListAR.DataSource = Session("TblMatView")
                gvListAR.DataBind()
            End If
            mpeListAR.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected Katalog data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListAR.SelectedIndex = -1 : FilterTextListAR.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListAR.DataSource = Session("TblMatView")
                gvListAR.DataBind()
                dtView.RowFilter = ""
                mpeListAR.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListAR.DataSource = Session("TblMatView")
                gvListAR.DataBind()
                Session("WarningListMat") = "Selected Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListAR.Show()
        End If
    End Sub

    Protected Sub gvListAR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListAR.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListAR.PageIndex = e.NewPageIndex
            gvListAR.DataSource = Session("TblMatView")
            gvListAR.DataBind()
        End If
        mpeListAR.Show()
    End Sub
End Class