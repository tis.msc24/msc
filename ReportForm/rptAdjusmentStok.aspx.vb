Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptAdjusmentStok
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

#Region "Procedure"
    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_branch, sSql)
            Else
                FillDDL(dd_branch, sSql)
                dd_branch.Items.Add(New ListItem("ALL", "ALL"))
                dd_branch.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dd_branch, sSql)
            dd_branch.Items.Add(New ListItem("ALL", "ALL"))
            dd_branch.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub GudangDDL()
       
        'If Session("branch_id") = "10" Then
        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' inner join QL_mstgen g on g.genoid = a.genother2 where a.cmpcode='" & CompnyCode & "'"
        If dd_branch.SelectedValue <> "ALL" Then
            sSql &= "and g.gencode = '" & dd_branch.SelectedValue & "'"
        End If
        sSql &= "ORDER BY a.gendesc"
        FillDDL(itemloc, sSql)
        itemloc.Items.Add("ALL")
        itemloc.SelectedValue = "ALL"
        'Else
        '    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' inner join QL_mstgen g on g.genoid = a.genother2 where a.cmpcode='" & CompnyCode & "' and g.gencode = '" & Session("branch_id") & "' ORDER BY a.gendesc"
        '    FillDDL(itemloc, sSql)
        'End If
    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub showPrintReport()
        lblkonfirmasi.Text = "" : Session("diprint") = "False"
        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Please fill period 1 value!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Please fill period 2 value!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If

        Try
            Dim swhere As String = ""
            vReport = New ReportDocument
            vReport.Load(Server.MapPath("~\Report\rptStockAdjustment.rpt"))
            'swhere = " where m.trndate between '" & CDate(toDate(dateAwal.Text)) & "' and '" & CDate(toDate(dateAkhir.Text)) & "' " & swhere
            swhere = " Where m.trndate>='" & toDate(dateAwal.Text) & "' And m.trndate<='" & toDate(dateAkhir.Text) & "'" & swhere
            swhere &= IIf(ddlSPG.SelectedValue <> "ALL", " And p.personoid=" & ddlSPG.SelectedValue & "", "") & IIf(dd_branch.SelectedValue <> "ALL", " and m.branch_code='" & dd_branch.SelectedValue & "'", "") & IIf(itemloc.SelectedValue <> "ALL", " and m.mtrlocoid=" & itemloc.SelectedValue & "", "") & "" & IIf(ToDouble(itemoid.Text) <> 0, " and m.refoid=" & itemoid.Text & "", "")
            swhere &= " And m.note like '%" & Tchar(noteAdj.Text) & "%' "

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("swhere", swhere)
            vReport.SetParameterValue("periode1", dateAwal.Text)
            vReport.SetParameterValue("periode2", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            crvMutasiStock.DisplayGroupTree = False
            crvMutasiStock.ReportSource = vReport
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Public Sub showPrint()
        lblkonfirmasi.Text = ""
        Session("diprint") = "False"

        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Please fill period 1 value!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Please fill period 2 value!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If

        Try
            Dim swhere As String = ""

            vReport = New ReportDocument
            vReport.Load(Server.MapPath("~\Report\rptStockAdjustment.rpt"))

            swhere = " Where m.trndate>='" & toDate(dateAwal.Text) & "' And m.trndate<='" & toDate(dateAkhir.Text) & "'" & swhere

            swhere &= IIf(ddlSPG.SelectedValue <> "ALL", " and p.personoid=" & ddlSPG.SelectedValue & "", "") & IIf(itemloc.SelectedValue <> "ALL", " and m.mtrlocoid=" & itemloc.SelectedValue & "", "") & "" & IIf(ToDouble(itemoid.Text) <> 0, " and m.refoid=" & itemoid.Text & "", "")

            swhere &= " and m.note like '%" & Tchar(noteAdj.Text) & "%' "

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("swhere", swhere)
            vReport.SetParameterValue("periode1", dateAwal.Text)
            vReport.SetParameterValue("periode2", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()

            vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "AS_" & Format(GetServerTime, "dd_MM_yy"))
            vReport.Close() : vReport.Dispose()
            Response.Redirect("~\ReportForm\rptAdjusmentStok.aspx?awal=true")
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Public Sub showPrintExcel()
        lblkonfirmasi.Text = ""
        Session("diprint") = "False"

        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Please fill period 1 value!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Please fill period 2 value!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If

        Try
            Dim sWhere As String = ""
            vReport = New ReportDocument
            vReport.Load(Server.MapPath("~\Report\rptStockAdjExl.rpt"))

            sWhere = " Where m.trndate>='" & toDate(dateAwal.Text) & "' And m.trndate<='" & toDate(dateAkhir.Text) & "'" & sWhere

            sWhere &= IIf(ddlSPG.SelectedValue <> "ALL", " And p.personoid=" & ddlSPG.SelectedValue & "", "") & IIf(itemloc.SelectedValue <> "ALL", " And m.mtrlocoid=" & itemloc.SelectedValue & "", "") & "" & IIf(ToDouble(itemoid.Text) <> 0, " And m.refoid=" & itemoid.Text & "", "")

            sWhere &= " And m.note like '%" & Tchar(noteAdj.Text) & "%' "

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("swhere", sWhere)
            vReport.SetParameterValue("periode1", dateAwal.Text)
            vReport.SetParameterValue("periode2", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()

            vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "AS_" & Format(GetServerTime, "dd_MM_yy"))
            vReport.Close() : vReport.Dispose()
            Response.Redirect("~\ReportForm\rptAdjusmentStok.aspx?awal=true")
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Session.Clear()  ' -->>  clear all session 
            Session("SpecialAccess") = xsetAcc
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Response.Redirect("~\ReportForm\rptAdjusmentStok.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Adjusment Stok "
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            'Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrintReport()
            End If
        Else
            fDDLBranch() : GudangDDL()

            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            sSql = "select personoid, personname from ql_mstperson where STATUS='Aktif' AND cmpcode = '" & CompnyCode & "' AND PERSONPOST = (SELECT genoid FROM QL_mstgen WHERE gengroup='JOBPOSITION' AND gendesc = 'SALES PERSON')"
            FillDDL(ddlSPG, sSql)
            ddlSPG.Items.Add("ALL")
            ddlSPG.SelectedValue = "ALL"

            'If Session("branch_id") = "10" Then
            '    sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang'"
            '    FillDDL(dd_branch, sSql)
            '    dd_branch.Items.Add(New ListItem("ALL", "ALL"))
            '    dd_branch.SelectedValue = "ALL"
            'Else
            '    sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang' and gencode = '" & Session("branch_id") & "'"
            '    FillDDL(dd_branch, sSql)
            'End If

            'sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
            'FillDDL(dd_branch, sSql)
            'dd_branch.Items.Add(New ListItem("ALL", "ALL"))
            'dd_branch.SelectedValue = "ALL"
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptAdjusmentStok.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        'Session("diprint") = "False"
        showPrintExcel()
    End Sub

    Protected Sub imbSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListItem()
    End Sub

    Protected Sub imbEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        itemname.Text = ""
        itemoid.Text = ""
        Merk.Text = ""
        gvItem.Visible = False
        gvItem.SelectedIndex = -1
    End Sub

    Private Sub bindDataListItem()
        Dim Loc As String = "" : Dim Branch As String = ""
        If dd_branch.SelectedValue <> "ALL" Then
            Branch = "AND branch_code LIKE '%" & dd_branch.SelectedValue & "%'"
        End If
        If itemloc.SelectedValue <> "ALL" Then
            Loc = "AND mtrwhoid LIKE '%" & itemloc.SelectedValue & "%'"
        End If

        sSql = "Select DISTINCT itemoid, itemcode, itemdesc,merk From ql_mstitem m Where itemoid IN (Select refoid FROM QL_trnstockadj adj Where adj.refoid=itemoid " & Branch & " " & Loc & ") AND (itemdesc like '%" & Tchar(itemname.Text) & "%' or itemcode like '%" & Tchar(itemname.Text) & "%') And Merk like '%" & Tchar(Merk.Text) & "%'"
        FillGV(gvItem, sSql, "ql_mstitem")
        gvItem.Visible = True
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemname.Text = gvItem.SelectedDataKey.Item("itemdesc")
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        Merk.Text = gvItem.SelectedDataKey.Item("merk")
        gvItem.Visible = False
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvItem.PageIndex = e.NewPageIndex
        bindDataListItem()
    End Sub

    Protected Sub ibpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint()
    End Sub

    Protected Sub btnreport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnreport.Click
        Session("diprint") = "True"
        showPrintReport()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub

    Protected Sub ibpdf_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showPrint()
    End Sub

    Function Get_Branch() As String
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim refcode As String = String.Empty

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT genoid FROM QL_mstgen where gengroup = 'cabang' and gencode = '" & dd_branch.SelectedValue & "'"
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            refcode = rdr("genoid")
        End While
        conn2.Close()
        Return refcode

    End Function

    Protected Sub dd_branch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dd_branch.SelectedIndexChanged
        GudangDDL()
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showPrintReport()
    End Sub
End Class
