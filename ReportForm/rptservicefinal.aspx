<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptservicefinal.aspx.vb" Inherits="ReportForm_rptservicefinal" title="Untitled Page" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Laporan Servis Final"></asp:Label>
            </th>
        </tr>
        <tr>
            <th align="center" style="background-color: white" valign="center">
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<TABLE><TBODY><TR><TD align=left>Cabang</TD><TD>:</TD><TD align=left><asp:DropDownList id="DDLcabang" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR>
    <tr>
        <td align="left">
            Type Service</td>
        <td>
            :</td>
        <td align="left">
            <asp:DropDownList id="DDLGaransi" runat="server" CssClass="inpText">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>Service</asp:ListItem>
                <asp:ListItem>Garansi</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <TR><TD align=left>Filter</TD><TD>:</TD><TD align=left><asp:DropDownList style="MARGIN-RIGHT: 10px" id="ddlfilter" runat="server" Width="128px" CssClass="inpText"><asp:ListItem Value="fm.trnfinalno">No. Final</asp:ListItem>
<asp:ListItem Value="cu.custname">Customer</asp:ListItem>
<asp:ListItem Enabled="False" Value="rqm.reqcode">No. Penerimaan(TTS)</asp:ListItem>
</asp:DropDownList><asp:TextBox id="tbfilter" runat="server" Width="184px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD align=left>Periode</TD><TD>:</TD><TD align=left><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="56px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton> to&nbsp;<asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="56px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;<SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN></TD></TR><TR style="COLOR: #000099"><TD align=left>Status</TD><TD>:</TD><TD align=left><asp:DropDownList id="ddlfilterstatus" runat="server" Width="105px" CssClass="inpText"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
<asp:ListItem Enabled="False">Invoiced</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" MaskType="Date" Mask="99/99/9999" TargetControlID="tbperiodstart"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" MaskType="Date" Mask="99/99/9999" TargetControlID="tbperiodend"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="tbperiodstart" PopupButtonID="ibperiodstart" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="tbperiodend" PopupButtonID="ibperiodend" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><asp:RadioButtonList id="cbstatus" runat="server" Width="117px" RepeatDirection="Horizontal" Visible="False"><asp:ListItem Selected="True">Status</asp:ListItem>
<asp:ListItem>Paid</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibreport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibtopdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibtoexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crv" runat="server" HasGotoPageButton="False" DisplayGroupTree="False" HasZoomFactorList="False" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer> 
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibtopdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibtoexcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel><asp:UpdatePanel id="uppopupmsg" runat="server"><contenttemplate>
<asp:Panel id="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpepopupmsg" runat="server" TargetControlID="bepopupmsg" Drag="True" PopupDragHandleControlID="lblcaption" BackgroundCssClass="modalBackground" PopupControlID="pnlpopupmsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bepopupmsg" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderColor="Transparent" BorderStyle="None"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
            </th>
        </tr>
    </table>
</asp:Content>

