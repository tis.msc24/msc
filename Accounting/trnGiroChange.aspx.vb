'Prgmr:Bhie Nduuutt | LastUpdt:01.04.2013
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports Koneksi
Imports ClassFunction
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Accounting_trnGiroChange
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public DefCounterCashBankNo As String = "4"
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim objCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dv As DataView
    Dim cKoneksi As New Koneksi
    Dim cProc As ClassProcedure
    Dim dtStaticItem As DataTable
    Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
    Dim objDs As New DataSet
    Private strSearch As String = ""
    Private reportAR As New ReportDocument
#End Region

#Region "Procedures"
    Private Sub ddlCabang()
        sSql = "select gencode, gendesc from ql_mstgen where gengroup='CABANG' ORDER BY gencode "
        FillDDL(CabangNya, sSql)
        If Session("branch_id") <> "10" Then
            CabangNya.SelectedValue = Session("branch_id")
            CabangNya.Enabled = False
        End If
    End Sub

    Private Sub ddlCashBank()
        Dim varCash As String = GetVarInterface("VAR_CASH", CabangNya.SelectedValue)
        Dim VAR_BANK_IDR As String = GetVarInterface("VAR_BANK", CabangNya.SelectedValue)
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='MSC' AND (/*interfacevar='VAR_CASH' OR*/ interfacevar='VAR_BANK') AND interfaceres1='" & CabangNya.SelectedValue & "'"
        Dim sCode As DataTable = cKoneksi.ambiltabel(sSql, "interfacevalue")
        'Dim sSplitVar() As String = sCode.Split(",")

        'Dim itemMasuk As DataTable = cKoneksi.ambiltabel(sSql, "interfacevalue")
        Dim IdCode As String = ""
         
        If sCode.Rows.Count <> 0 Then
            If Not sCode Is Nothing Then
                For it As Integer = 0 To sCode.Rows.Count - 1
                    IdCode &= sCode.Rows(it).Item("interfacevalue")
                    If it < sCode.Rows.Count - 1 Then
                        IdCode &= ","
                    End If
                Next
            End If
        End If

        If sCode.Rows.Count = 0 Then
            sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='MSC' AND (interfacevar='VAR_BANK' /*OR interfacevar='VAR_CASH'*/) AND interfaceres1='MSC'"
            sCode = cKoneksi.ambiltabel(sSql, "interfacevalue")
        End If

        If sCode.Rows.Count <> 0 Then
            sSql = "SELECT a.acctgoid, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='MSC' AND ("
            Dim sSplitCode() As String = IdCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillDDL(cash_bank_coa, sSql)
        End If
    End Sub

    Private Sub GenerateNo()
        Dim cabang As String = GetStrData("Select genother1 from ql_mstgen Where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")

        Dim sNo As String = "GG/" & cabang & "/" & Format(CDate(toDate(TanggalTukarGiro.Text)), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(NOCG,'" & sNo & "',''))),0)+1 AS IDNEW FROM QL_TukarGiro WHERE cmpcode='" & CompnyCode & "' AND NOCG LIKE '" & sNo & "%' AND branch_code='" & CabangNya.SelectedValue & "'"
        NOCG.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Protected Sub ClearInput()
        payduedatelama.Text = Format(CDate(GetServerTime()), "dd/MM/yyyy")
        payduedatebaru.Text = Format(CDate(GetServerTime()), "dd/MM/yyyy")
        TanggalTukarGiro.Text = Format(CDate(GetServerTime()), "dd/MM/yyyy")
        noGiroLama.Text = ""
        noGiroBaru.Text = ""
        note.Text = ""
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Protected Sub binddata()
        Dim st1, st2 As Boolean : Dim sMsg As String = ""
        Try
            Dim dt1 As Date = CDate(toDate(txtPeriode1.Text)) : st1 = True
        Catch ex As Exception
            sMsg &= "- Tanggal mulai salah!!<BR>" : st1 = False
        End Try
        Try
            Dim dt2 As Date = CDate(toDate(txtPeriode2.Text)) : st2 = True
        Catch ex As Exception
            sMsg &= "- Tanggal akhir salah!!<BR>" : st2 = False
        End Try
        If st1 And st2 Then
            If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
                sMsg &= "- Tanggal mulai harus <= tanggal akhir!!<BR>"
            End If
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        sSql = "select t.NoGiroBaru, t.NoGiroLama, t.TanggalTukarGiro, t.payduedateGiroLama, t.refname, t.refoid_cs, t.sebab, t.TanggalTukarGiro, t.TukarGiroNote, t.TukarGiroOid, t.updtime, t.upduser, cb.cashbankno, t.nocg, cb.cashbankcurrate, G.GENDESC currency , ISNULL(c.custname, s.suppname) CUSTNAME , substring(cb.cashbankno,1,3)jenis From QL_TukarGiro t inner join QL_trncashbankmst cb on cb.cashbankoid=t.cashbankoid and cb.cashbankacctgoid in (select ao.acctgoid from ql_mstacctg ao) and cb.cmpcode=t.cmpcode AND cb.cashbanktype='" & ddlTipe.SelectedValue & "' " & IIf(bankfilter.SelectedValue = "ALL", "", " and cb.bankoid=" & bankfilter.SelectedValue) & " inner join ql_mstgen g on g.genoid=cb.cashbankcurroid left join ql_mstcust c on c.custoid=t.refoid_cs and t.refname='QL_MSTCUST' left join ql_mstSUPP S on S.SUPPoid=t.refoid_cs and t.refname='QL_MSTSUPP' Where ISNULL(c.custname, s.suppname) like '%" & Tchar(searchNamaCustomer.Text) & "%' and t.sebab like '" & FilterStatus.SelectedValue & "' and t.nogirobaru like '%" & Tchar(searchNoGiroBaru.Text) & "%' and nogirolama like '%" & Tchar(searchNoGiroLama.Text) & "%' and TanggalTukarGiro Between '" & toDate(txtPeriode1.Text) & "' And '" & toDate(txtPeriode2.Text) & "' Order by nocg Desc"

        Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        Session("tbldata") = objTable
        GVmst.DataSource = objDs.Tables(0)
        GVmst.DataBind()
    End Sub

    Private Sub BinddataGiro()
        If ToDouble(refoid.Text) > 0 Then
            sSql = "select girodtloid, Trans_No, girono, m.gendesc Bank, bankoid, NoRekening, convert(char(10),d.GiroDueDate,103) GiroDueDate, amount from ql_giro g inner join QL_GIRODTL d on g.girooid=d.girooid inner join QL_mstgen m on m.genoid=d.bankoid where g.status='Post' and custoid=" & refoid.Text & " And d.status='Not Used' and girodtloid not in (select girodtloid from ql_trncashbankmst where girodtloid>0) and " & DDLSuppIDX.SelectedValue & " like '%" & Tchar(txtFindSuppIDX.Text) & "%' and g.branch_code='" & CabangNya.SelectedValue & "'"
            Dim tbldt As DataTable = CreateDataTableFromSQL(sSql)
            gvSupplierX.DataSource = tbldt
            gvSupplierX.DataBind()

            hiddenbtn2sX.Visible = True
            Panel1X.Visible = True
            ModalPopupExtender3sX.Show()
        End If
    End Sub

    Private Sub BindLastSearch(ByVal sSearchString As String)
        Dim mySqlDA As New SqlDataAdapter(sSearchString, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        Session("tbldata") = objTable
        GVmst.DataSource = objDs.Tables(0)
        GVmst.DataBind()

    End Sub

    Public Sub CheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("TukarGiroStatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmst.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub UncheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("TukarGiroStatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmst.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub bindDataGiro(ByVal noGiro As String, ByVal cmpcode As String)
        If jenis.SelectedValue = "BGM" Then 
            sSql = " select t.girodtloid,t.tgl, t.cashbankno,t.refno ,t.cashbanktype ,t.cashbankgroup ,sum(t.amount) amount ,t.custname ,t.custoid ,t.duedate ,t.bank ,t.cashbankoid ,t.cashbankacctgoid ,t.cashbankcurrate ,t.currency ,t.bankoid,t.branch_code from ( " & _
 " select distinct cb.girodtloid, convert(char(10),cb.cashbankdate,103)tgl, cb.cashbankno,cd.cashbankglnote, cd.refno,cb.cashbanktype,cb.cashbankgroup, (cd.cashbankglamt)amount, isnull(c.custname, '')custname,isnull(c.custoid, 0)custoid, convert(char(10),cd.duedate,103) duedate, isnull(g.gendesc,'') bank , cb.cashbankoid, cb.cashbankacctgoid , cb.cashbankcurrate, g2.currencycode currency,isnull(cb.bankoid,0) bankoid,cb.branch_code from ql_trncashbankmst cb inner join QL_cashbankgl cd on cb.cashbankoid=cd.cashbankoid and cb.cashbankstatus='POST' and cb.cmpcode=cd.cmpcode and cb.cashbanktype = 'BGM' inner join QL_trnpayar ar on ar.cmpcode=cb.cmpcode and cb.cashbankoid=ar.cashbankoid AND cb.branch_code=ar.branch_code inner join ql_mstcust c on c.custoid=ar.custoid and c.cmpcode=ar.cmpcode inner join ql_mstcurr g2 on g2.currencyoid=cb.cashbankcurroid left join ql_mstgen g on g.genoid= cb.bankoid Where " & giroFilter.SelectedValue & " like '%" & Tchar(tbGiro.Text) & "%' " & IIf(sebab.SelectedValue = "Giro Di Tolak", " and cb.status_giro<>'Giro Di Tolak' ", " ") & " And cb.status_giro NOT IN ('Ganti Cash/Bank','Ganti Giro') and cb.girodtloid not in (SELECT girodtloid FROM QL_GiroPaymentDtl gp where gp.paymentref_table like 'QL_CASHBANKMST%') AND cb.branch_code='" & CabangNya.SelectedValue & "') t " & _
 " group by t.girodtloid,t.tgl, t.cashbankno,t.refno ,t.cashbanktype ,t.cashbankgroup ,t.custname ,t.custoid ,t.duedate,t.bank ,t.cashbankoid ,t.cashbankacctgoid ,t.cashbankcurrate ,t.currency ,t.bankoid,t.branch_code " & _
            "UNION " & _
            "Select cb.girodtloid, convert(char(10),cb.cashbankdate,103)tgl, cb.cashbankno, ar.payrefno, cb.cashbanktype,cb.cashbankgroup, sum(ar.trndparamt)amount, isnull(c.custname, '')custname ,isnull(c.custoid, 0)custoid, convert(char(10),ar.payduedate,103) duedate, isnull(g.gendesc,'') bank, cb.cashbankoid, cb.cashbankacctgoid , cb.cashbankcurrate, g2.currencycode currency, isnull(cb.bankoid,0) bankoid,cb.branch_code from ql_trncashbankmst cb inner join QL_trndpar ar on ar.cmpcode=cb.cmpcode and cb.cashbankstatus='POST' and cb.cashbanktype = 'BGM' and cb.cashbankoid=ar.cashbankoid AND cb.branch_code=ar.branch_code inner join ql_mstcust c on c.custoid=ar.custoid  and c.cmpcode=ar.cmpcode and ar.trndparoid not in (SELECT paymentoid FROM QL_GiroPaymentDtl gp where gp.paymentref_table like 'QL_TRNDPAR%') inner join ql_mstcurr g2 on g2.currencyoid=cb.cashbankcurroid left join ql_mstgen g on g.genoid= cb.bankoid Where " & IIf(giroFilter.SelectedValue = "cd.refno", "ar.payrefno", giroFilter.SelectedValue) & " LIKE '%" & Tchar(tbGiro.Text) & "%' " & IIf(sebab.SelectedValue = "Giro Di Tolak", " and cb.status_giro<>'Giro Di Tolak' ", " ") & " And cb.status_giro NOT IN ('Ganti Cash/Bank','Ganti Giro') AND cb.branch_code='" & CabangNya.SelectedValue & "' group by cb.cashbankdate , cb.cashbankno, ar.payrefno, cb.cashbanktype,cb.cashbankgroup, c.custname ,ar.payduedate , g.gendesc, cb.cashbankoid, c.custoid, cb.cashbankacctgoid , cb.cashbankcurrate, g2.currencycode ,cb.bankoid , cb.girodtloid,cb.branch_code " & _
  " UNION " & _
  " select cb.girodtloid, convert(char(10),cb.cashbankdate,103)tgl, cb.cashbankno, cd.refno,cb.cashbanktype,cb.cashbankgroup,  sum(cd.cashbankglamt)amount,'' custname, 0 custoid, convert(char(10),cd.duedate,103) duedate, isnull(g.gendesc,'') bank, cb.cashbankoid, cb.cashbankacctgoid, cb.cashbankcurrate, g2.currencycode currency, isnull(cb.bankoid,0) bankoid,cb.branch_code from ql_trncashbankmst cb inner join QL_cashbankgl cd on cb.cashbankoid=cd.cashbankoid and cb.cashbankstatus='POST' and cb.cmpcode=cd.cmpcode and cb.cashbankgroup='IB GIRO IN' and cb.cashbanktype = 'BGM' and cb.cashbankoid not in (SELECT paymentoid FROM QL_GiroPaymentDtl gp where gp.paymentref_table like 'QL_CASHBANKMST%') AND cb.branch_code='" & CabangNya.SelectedValue & "' inner join ql_mstcurr g2 on g2.currencyoid=cb.cashbankcurroid left join ql_mstgen g on g.genoid= cb.bankoid Where " & giroFilter.SelectedValue & " like '%" & Tchar(tbGiro.Text) & "%' " & IIf(sebab.SelectedValue = "Giro Di Tolak", " And cb.status_giro<>'Giro Di Tolak' ", " ") & " And cb.status_giro NOT IN ('Ganti Cash/Bank','Ganti Giro') AND cb.branch_code='" & CabangNya.SelectedValue & "' And cb.cashbankoid in (select acctgoid from ql_mstacctg) group by cb.cashbankdate , cb.cashbankno, cd.refno,cb.cashbanktype,cb.cashbankgroup ,cd.duedate , g.gendesc, cb.cashbankoid, cb.cashbankacctgoid, cb.cashbankcurrate, g2.currencycode ,cb.bankoid , cb.girodtloid,cb.branch_code order by tgl,duedate , bank, refno"

        Else

            sSql = "Select convert(char(10),cb.cashbankdate,103)tgl, cb.cashbankno, cd.refno,  cb.cashbanktype,cb.cashbankgroup,sum(cd.cashbankglamt)amount, isnull(c.suppname, '')custname,isnull(c.suppoid, 0)custoid, convert(char(10),cd.duedate,103) duedate, isnull(g.gendesc,'') bank , cb.cashbankoid, cb.cashbankacctgoid , cb.cashbankcurrate, g2.gendesc currency, isnull(cb.bankoid,0)bankoid from ql_trncashbankmst cb inner join QL_cashbankgl cd on cb.cashbankoid=cd.cashbankoid and cb.cmpcode=cd.cmpcode and cb.cashbanktype = 'BGK' inner join QL_trnpayap ar on ar.cmpcode=cb.cmpcode and cb.cashbankoid=ar.cashbankoid inner join ql_mstsupp c on c.suppoid=ar.suppoid and c.cmpcode=ar.cmpcode inner join ql_mstgen g2 on g2.genoid=cb.cashbankcurroid left join ql_mstgen g on g.genoid= cb.bankoid Where " & giroFilter.SelectedValue & " like '%" & Tchar(tbGiro.Text) & "%' " & IIf(sebab.SelectedValue = "Giro Di Tolak", " and cb.status_giro<>'Giro Di Tolak' ", " ") & " and cb.status_giro<>'Ganti Cash/Bank' and cb.girodtloid not in (SELECT girodtloid FROM QL_GiroPaymentDtl gp where gp.paymentref_table like 'QL_CASHBANKMST%') group by cb.cashbankdate , cb.cashbankno, cd.refno, cb.cashbanktype,cb.cashbankgroup, c.custname,cd.duedate, g.gendesc , cb.cashbankoid, c.custoid , cb.cashbankacctgoid , cb.cashbankcurrate, g2.gendesc , cb.bankoid union select convert(char(10),cb.cashbankdate,103)tgl, cb.cashbankno, cd.refno, cb.cashbanktype,cb.cashbankgroup,sum(cd.cashbankglamt)amount, isnull(c.suppname, '')custname ,isnull(c.suppoid, 0)custoid, convert(char(10),cd.duedate,103) duedate, isnull(g.gendesc,'') bank, cb.cashbankoid, cb.cashbankacctgoid , cb.cashbankcurrate, g2.gendesc currency, isnull(cb.bankoid,0)bankoid from ql_trncashbankmst cb inner join QL_cashbankgl cd on cb.cashbankoid=cd.cashbankoid and cb.cmpcode=cd.cmpcode and cb.cashbanktype = 'BGK' inner join QL_trndpap ar on ar.cmpcode=cb.cmpcode and cb.cashbankoid=ar.cashbankoid inner join ql_mstsupp c on c.suppoid=ar.suppoid and c.cmpcode=ar.cmpcode and ar.trndpapoid not in (SELECT paymentoid FROM QL_GiroPaymentDtl gp Where gp.paymentref_table like 'QL_TRNDPAP%') inner join ql_mstgen g2 on g2.genoid=cb.cashbankcurroid left join ql_mstgen g on g.genoid= cb.bankoid Where " & giroFilter.SelectedValue & " like '%" & Tchar(tbGiro.Text) & "%' " & IIf(sebab.SelectedValue = "Giro Di Tolak", " and cb.status_giro<>'Giro Di Tolak' ", " ") & " And cb.status_giro NOT IN ('Ganti Cash/Bank','Ganti Giro'),'Ganti Giro') group by cb.cashbankdate , cb.cashbankno, cd.refno, cb.cashbanktype,cb.cashbankgroup, c.suppname ,cd.duedate , g.gendesc, cb.cashbankoid, c.custoid, cb.cashbankacctgoid , cb.cashbankcurrate, g2.gendesc, cb.bankoid union select convert(char(10),cb.cashbankdate,103)tgl, cb.cashbankno, cd.refno, cb.cashbanktype,cb.cashbankgroup, sum(cd.cashbankglamt)amount, '' custname, 0 custoid, convert(char(10),cd.duedate,103) duedate, isnull(g.gendesc,'') bank, cb.cashbankoid, cb.cashbankacctgoid, cb.cashbankcurrate, g2.gendesc currency, isnull(cb.bankoid,0)bankoid from ql_trncashbankmst cb inner join QL_cashbankgl cd on cb.cashbankoid=cd.cashbankoid and cb.cmpcode=cd.cmpcode and cb.cashbankgroup ='IB GIRO OUT' and cb.cashbanktype = 'BGK' and cb.cashbankoid not in (SELECT paymentoid FROM QL_GiroPaymentDtl gp where gp.paymentref_table like 'QL_CASHBANKMST%') inner join ql_mstgen g2 on g2.genoid=cb.cashbankcurroid left join ql_mstgen g on g.genoid= cb.bankoid where " & giroFilter.SelectedValue & " like '%" & Tchar(tbGiro.Text) & "%' " & IIf(sebab.SelectedValue = "Giro Di Tolak", " And cb.status_giro NOT IN ('Ganti Cash/Bank','Ganti Giro')", " ") & " and cb.status_giro<>'Ganti Cash/Bank' and cb.cashbankoid in (select acctgoid from ql_mstacctg) group by cb.cashbankdate , cb.cashbankno, cd.refno, cb.cashbanktype,cb.cashbankgroup ,cd.duedate , g.gendesc, cb.cashbankoid, cb.cashbankacctgoid, cb.cashbankcurrate, g2.gendesc , cb.bankoid order by tgl,duedate , bank, refno  "

        End If
        FillGV(gvGiro, sSql, "ql_payar")
    End Sub

    Private Sub PrintNota(ByVal oid As String, ByVal no As String)
        'untuk print
        If Left(no, 3) = "BKM" Then
            reportAR.Load(Server.MapPath("~/report/printBGKM.rpt"))
        ElseIf Left(no, 3) = "BBM" Then
            reportAR.Load(Server.MapPath("~/report/printBGBM.rpt"))
        End If
        reportAR.SetParameterValue("cmpcode", CompnyCode)
        reportAR.SetParameterValue("no", no)

        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, reportAR)

        reportAR.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        reportAR.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        reportAR.Close() : reportAR.Dispose()
    End Sub

    Private Sub FillTextBox(ByVal id As String)
        sSql = "select t.NoGiroBaru, t.NoGiroLama, t.TanggalTukarGiro, t.payduedateGiroLama,t.payduedateGiroBaru, t.refname, t.refoid_cs, t.sebab, t.TanggalTukarGiro, t.TukarGiroNote, t.TukarGiroOid, t.updtime, t.upduser, cb.cashbankno, TukarGiroNote,t.nocg , cb.cashbankcurrate, G.currencycode currency , coalesce(c.custname, s.suppname,'') custname, coalesce(c.custoid, s.suppoid,0)custoid, substring(cb.cashbankno,1,3)jenis, g2.gendesc bank, g3.gendesc bank_old, cb.bankoid, t.bankoid_old, CASHBANKACCTGOID_NEW, CASHBANKACCTGOID_old, cb.cashbankoid , t.girodtloid_new, t.girodtloid_old,t.branch_code from QL_TukarGiro t inner join QL_trncashbankmst cb on cb.cashbankoid=t.cashbankoid and cb.cmpcode=t.cmpcode inner join ql_mstgen g2 on g2.genoid=cb.bankoid inner join ql_mstcurr g on g.currencyoid=cb.cashbankcurroid left join ql_mstcust c on c.custoid=t.refoid_cs and t.refname='QL_MSTCUST' left join ql_mstSUPP S on S.SUPPoid=t.refoid_cs and t.refname='QL_MSTSUPP' and s.suppname like '%" & Tchar(searchNamaCustomer.Text) & "%' left join ql_mstgen g3 on g3.genoid=t.bankoid_old where TukarGiroOid=" & id
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objCmd.CommandText = sSql
        xreader = objCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                CabangNya.SelectedValue = Trim(xreader("branch_code"))
                TanggalTukarGiro.Text = Trim(Format(xreader("TanggalTukarGiro"), "dd/MM/yyyy"))
                noGiroLama.Text = Trim(xreader("NoGiroLama"))
                noGiroBaru.Text = Trim(xreader("NoGiroBaru"))
                cashbankoid.Text = xreader("cashbankoid")
                payduedatelama.Text = Trim(Format(xreader("payduedateGiroLama"), "dd/MM/yyyy"))
                payduedatebaru.Text = Trim(Format(xreader("payduedateGiroBaru"), "dd/MM/yyyy"))
                refoid.Text = Trim(xreader("refoid_cs"))
                custname.Text = Trim(xreader("custname"))

                reftype.Text = xreader("refname").ToString()
                note.Text = xreader("TukarGiroNote").ToString()
                Try
                    banklama.Text = xreader("bank_old").ToString()
                    bankoid_old.Text = xreader("bankoid_old")
                    bankbaru.SelectedValue = xreader("bankoid")
                Catch ex As Exception

                End Try
                NOCG.Text = xreader("nocg")
                currencyrate.Text = NewMaskEdit(xreader("cashbankcurrate"))
                currency.Text = xreader("currency")
                jenis.SelectedValue = xreader("jenis")
                sebab.SelectedValue = xreader("sebab")
                UpdUser.Text = xreader("upduser")
                noGiroLama.Text = xreader("NoGiroLama")
                noGiroBaru.Text = xreader("NoGiroBaru")
                nobukti.Text = xreader("cashbankno")
                girodtloid_old.Text = xreader("girodtloid_old")
                girodtloid.Text = xreader("girodtloid_new")
                noGiroBaru.Text = xreader("NoGiroBaru")
                cashbankacctgoid_old.Text = xreader("CASHBANKACCTGOID_old")
                UpdTime.Text = xreader("updtime")

                If sebab.SelectedValue = "Ganti Cash/Bank" Then
                    cash_bank_coa.SelectedValue = xreader("CASHBANKACCTGOID_NEW")
                End If
                lblPost.Text = "POST" : btnPosting.Visible = True
                btnSave.Visible = False : btnDelete.Visible = False
            End While
        End If
        xreader.Close() : conn.Close()

        sSql = "Select SUM(cashbankglamt) from ql_cashbankgl where cashbankoid=" & cashbankoid.Text & "  and cmpcode='" & CompnyCode & "'"
        amount.Text = NewMaskEdit(ToDouble(GetStrData(sSql)))
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim lokasioid As Int32 = Session("lokasioid")
            Dim lokasi As String = Session("lokasi")
            Dim lokasino As String = Session("lokasino")
            Dim sqlTemp As String = Session("SearchARR")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("lokasioid") = lokasioid
            Session("lokasi") = lokasi
            Session("lokasino") = lokasino
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("SearchARR") = sqlTemp
            Response.Redirect(Page.AppRelativeVirtualPath) '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        imbPrintNota.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to PRINT this data?');")
        Page.Title = CompnyName & " - Giro Change"

        i_u.Text = "New Data"
        If Not IsPostBack Then
            ClearInput()
            FillDDLGen("BANK NAME", bankbaru)
            FillDDLGen("BANK NAME", bankfilter)
            bankfilter.Items.Add("ALL")
            bankfilter.SelectedValue = "ALL"
            txtPeriode1.Text = Format(GetServerTime, "01/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            binddata() : ddlCabang() : ddlCashBank()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "Update Data"
                FillTextBox(Session("oid"))
                imbPrintNota.Visible = True
                TabContainer1.ActiveTabIndex = 1
            Else
                i_u.Text = "New Data"
                btnDelete.Visible = False
                ' binddata()
                UpdTime.Text = GetServerTime()
                UpdUser.Text = Session("UserID")
                TabContainer1.ActiveTabIndex = 0
                GenerateNo()
            End If

            If sebab.SelectedValue = "Giro Di Tolak" Then
                TD1.Visible = False : TD1.Visible = False : TD2.Visible = False
                TD3.Visible = False : TD4.Visible = False ': TD5.Visible = False
                'TD6.Visible = False : TD8.Visible = False : TD7.Visible = False
            ElseIf sebab.SelectedValue = "Ganti Giro" Then
                'TD8.Visible = False : TD7.Visible = False
                TD1.Visible = True : TD1.Visible = True : TD2.Visible = True
                TD3.Visible = False : TD4.Visible = False ': TD5.Visible = True
                'TD6.Visible = True
            Else
                TD1.Visible = False : TD1.Visible = False : TD2.Visible = False
                TD3.Visible = False : TD4.Visible = False ': TD5.Visible = False
                'TD6.Visible = False : TD8.Visible = True : TD7.Visible = True
            End If
        End If

        imbPrintNota.Visible = False

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            btnshowCOA.Visible = True
        Else
            btnshowCOA.Visible = False
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("SearchARR") = strSearch
        binddata()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtPeriode1.Text = Format(GetServerTime.AddDays(-2), "dd/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
        ddlTipe.SelectedIndex = 0
        FilterStatus.SelectedIndex = 0
        bankfilter.SelectedIndex = 0
        searchNoGiroLama.Text = ""
        searchNoGiroBaru.Text = ""
        searchNamaCustomer.Text = ""
        binddata()
    End Sub

    Protected Sub imbLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("SearchARR") Is Nothing = False Then
            BindLastSearch(Session("SearchARR").ToString)
        End If
    End Sub

    Protected Sub imbSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub imbSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub imbPostSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub ImageButton8_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub btnFindInvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblGiroData.Text = "List Of Giro Account (" & jenis.SelectedValue & ")"
        BinddataGiro(Tchar(tbGiro.Text), CompnyCode)
        ModalPopupExtenderGiro.Show()
    End Sub

    Protected Sub imbViewAllInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        tbGiro.Text = ""
        lblGiroData.Text = "List Of Giro Account (" & jenis.SelectedValue & ")"
        BinddataGiro("", CompnyCode)
        ModalPopupExtenderGiro.Show()
    End Sub

    Protected Sub CloseSales_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelGiro.Visible = False
        btnHiddenGiro.Visible = False
        ModalPopupExtenderGiro.Hide()
    End Sub

    Protected Sub gvGiro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        refoid.Text = gvGiro.SelectedDataKey("custoid")
        cashbankoid.Text = gvGiro.SelectedDataKey("cashbankoid")
        cashbankacctgoid_old.Text = gvGiro.SelectedDataKey("cashbankacctgoid")
        custname.Text = gvGiro.SelectedDataKey("custname")
        noGiroLama.Text = gvGiro.SelectedDataKey("refno")
        nobukti.Text = gvGiro.SelectedDataKey("cashbankno")
        payduedatelama.Text = gvGiro.SelectedDataKey("duedate")
        banklama.Text = gvGiro.SelectedDataKey("bank")
        amount.Text = NewMaskEdit(gvGiro.SelectedDataKey("amount"))
        cashbankgroup.Text = gvGiro.SelectedDataKey("cashbankgroup")
        currency.Text = gvGiro.SelectedDataKey("currency")
        currencyrate.Text = gvGiro.SelectedDataKey("cashbankcurrate")
        bankoid_old.Text = gvGiro.SelectedDataKey("bankoid")
        girodtloid_old.Text = gvGiro.SelectedDataKey("girodtloid")
        If jenis.SelectedValue = "BGM" Then
            reftype.Text = "QL_MSTCUST"
        Else
            reftype.Text = "QL_MSTSUPP"
        End If
        PanelGiro.Visible = False : btnHiddenGiro.Visible = False : ModalPopupExtenderGiro.Hide()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""

        Try
            Dim dt As Date = CDate(toDate(TanggalTukarGiro.Text))
            If CheckYearIsValid(toDate(TanggalTukarGiro.Text)) Then
                sMsg &= "Tahun tanggal tidak dapat kurang dari 2000 dan lebih dari 2072 <br />"
                Exit Sub
            End If
        Catch ex As Exception
            sMsg &= "- Tanggal transaksi saah!!<BR>"
        End Try

        Try
            Dim dt As Date = CDate(toDate(payduedatelama.Text))
        Catch ex As Exception
            sMsg &= "- Tanggal jatuh tempo giro ama salah !!<BR>"
        End Try
        If CheckYearIsValid(toDate(payduedatelama.Text)) Then
            sMsg &= "Tahun tanggal jatuh tempo tidak dapat kurang dari 2000 dan lebih dari 2072 ! <br />"
            Exit Sub
        End If

        Dim cashbankacctgoid_new As Integer = 0
        If jenis.SelectedValue = "BGM" Then
            Dim VAR_PIUTANG_GIRO_TOLAKAN As String = GetVarInterface("VAR_PIUTANG_GIRO_TOLAKAN", CabangNya.SelectedValue)
            sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_PIUTANG_GIRO_TOLAKAN & "%'"
        Else
            Dim VAR_HUTANG_GIRO_TOLAKAN As String = GetVarInterface("VAR_HUTANG_GIRO_TOLAKAN", CabangNya.SelectedValue)
            sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_HUTANG_GIRO_TOLAKAN & "%'  "
        End If

        If sebab.SelectedValue = "Giro Di Tolak" Then
            payduedatebaru.Text = payduedatelama.Text
            noGiroBaru.Text = noGiroLama.Text
            girodtloid.Text = girodtloid_old.Text
            cashbankacctgoid_new = ToDouble(GetStrData(sSql))
        ElseIf sebab.SelectedValue = "Ganti Giro" Then
            If ToDouble(cashbankacctgoid_old.Text) = ToDouble(GetStrData(sSql)) Then 'jika giro sblmnya di tolak
                If jenis.SelectedValue = "BGM" Then
                    Dim VAR_GIRO_PIUTANG As String = GetVarInterface("VAR_GIRO_PIUTANG", CabangNya.SelectedValue)
                    sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_GIRO_PIUTANG & "%'"
                Else
                    Dim VAR_GIRO_HUTANG As String = GetVarInterface("VAR_GIRO_HUTANG", CabangNya.SelectedValue)
                    sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_GIRO_HUTANG & "%'  "
                End If
                cashbankacctgoid_new = ToDouble(GetStrData(sSql))
            Else
                cashbankacctgoid_new = cashbankacctgoid_old.Text
            End If
        Else
            payduedatebaru.Text = TanggalTukarGiro.Text
            cashbankacctgoid_new = cash_bank_coa.SelectedValue
            girodtloid.Text = 0
        End If
        Try
            Dim dt As Date = CDate(toDate(payduedatebaru.Text))
        Catch ex As Exception
            sMsg &= "- Tanggal jatuh tempo salah!!<BR>"
        End Try

        If sebab.SelectedValue = "Ganti Giro" Then
            If girodtloid.Text = "" Then
                sMsg &= "- TOlong pilih no giro baru terlebih dahulu !!<BR>"
            End If
        Else
            If refoid.Text = "" Then
                sMsg &= "- TOlong pilih giro lama terlebih dahulu!!<BR>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        Dim vIDMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim vIDDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
        GenerateNo()

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                Dim sTukarGiro As Long = GenerateID("QL_TukarGiro", CompnyCode)

                If sebab.SelectedValue = "Giro Di Tolak" Then
                    sSql = "update QL_mstcust set statusAR='BLACK' where cmpcode='" & CompnyCode & "' and custoid=" & refoid.Text
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                ElseIf sebab.SelectedValue = "Ganti Giro" Then
                    sSql = "update QL_mstcust set statusAR='RED' where cmpcode='" & CompnyCode & "' and custoid=" & refoid.Text
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                sSql = "INSERT into QL_TukarGiro (cmpcode,TukarGiroOid,refname, cashbankoid, refoid_cs, NoGiroLama,NoGiroBaru,payduedateGiroLama,payduedateGiroBaru, TanggalTukarGiro, TukarGiroNote,upduser,updtime,sebab,CASHBANKACCTGOID_OLD, CASHBANKACCTGOID_NEW, NOCG , bankoid_old, girodtloid_old, girodtloid_new,branch_code) VALUES " & _
" ('" & CompnyCode & "'," & sTukarGiro & ",'" & reftype.Text & "', " & cashbankoid.Text & "," & refoid.Text & ", '" & Tchar(noGiroLama.Text) & "','" & Tchar(noGiroBaru.Text) & "','" & CDate(toDate(payduedatelama.Text)) & "','" & CDate(toDate(payduedatebaru.Text)) & "','" & CDate(toDate(TanggalTukarGiro.Text)) & "','" & Tchar(note.Text) & "','" & Session("UserID") & "',current_timestamp, '" & sebab.SelectedValue & "', " & cashbankacctgoid_old.Text & "," & cashbankacctgoid_new & ", '" & NOCG.Text & "', " & ToDouble(bankoid_old.Text) & ", " & ToDouble(girodtloid_old.Text) & ", " & ToDouble(girodtloid.Text) & ",'" & CabangNya.SelectedValue & "')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & sTukarGiro & " Where tablename like 'QL_TukarGiro%' and cmpcode like '%" & CompnyCode & "%' "
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                If jenis.SelectedValue = "BGM" Then
                    sSql = "UPDATE QL_trnpayar SET payrefno='" & Tchar(noGiroBaru.Text) & "', payduedate='" & CDate(toDate(payduedatebaru.Text)) & "', paybankoid=" & cashbankacctgoid_new & " WHERE cmpcode='" & CompnyCode & "' AND cashbankoid=" & cashbankoid.Text & " AND branch_code='" & CabangNya.SelectedValue & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    If cashbankgroup.Text = "DPAR" Then
                        sSql = "UPDATE QL_trndpar SET payrefno='" & Tchar(noGiroBaru.Text) & "', payduedate='" & CDate(toDate(payduedatebaru.Text)) & "', cashbankacctgoid =" & cashbankacctgoid_new & " WHERE cmpcode='" & CompnyCode & "' AND branch_code='" & CabangNya.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                Else 'BGK
                    sSql = "UPDATE QL_trnpayap SET payrefno='" & Tchar(noGiroBaru.Text) & "', payduedate='" & CDate(toDate(payduedatebaru.Text)) & "', paybankoid=" & cashbankacctgoid_new & " WHERE cmpcode='" & CompnyCode & "' AND cashbankoid=" & cashbankoid.Text
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    If cashbankgroup.Text = "DPAP" Then
                        sSql = "UPDATE QL_trndpaP SET payrefno='" & Tchar(noGiroBaru.Text) & "', payduedate='" & CDate(toDate(payduedatebaru.Text)) & "', cashbankacctgoid =" & cashbankacctgoid_new & " WHERE cmpcode='" & CompnyCode & "' AND cashbankoid=" & cashbankoid.Text
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                End If

                sSql = "UPDATE QL_cashbankgl SET refno='" & Tchar(noGiroBaru.Text) & "', duedate='" & CDate(toDate(payduedatebaru.Text)) & "' WHERE cmpcode='" & CompnyCode & "' AND cashbankoid=" & cashbankoid.Text
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_trncashbankmst set girodtloid=" & girodtloid.Text & ", cashbankacctgoid=" & cashbankacctgoid_new & " , status_giro='" & sebab.SelectedValue & "' " & IIf(sebab.SelectedValue = "Ganti Giro", ", bankoid=" & bankbaru.SelectedValue, " ") & " Where cashbankoid=" & cashbankoid.Text & " And cmpcode='" & CompnyCode & "' AND branch_code='" & CabangNya.SelectedValue & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                If cashbankacctgoid_new <> ToDouble(cashbankacctgoid_old.Text) Then
                    '//////INSERT INTO TRN GL MST//////
                    sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,branch_code)" & _
                        "VALUES ('" & CompnyCode & "'," & vIDMst & ",'" & CDate(toDate(TanggalTukarGiro.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(TanggalTukarGiro.Text))) & "','GANTI GIRO|No=" & NOCG.Text & "','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    If jenis.SelectedValue = "BGM" Then ' piutang giro tolakan
                        'insert data detail
                        'DEBET
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & vIDDtl & ",1," & vIDMst & "," & cashbankacctgoid_new & ",'D'," & ToDouble(amount.Text) * ToDouble(currencyrate.Text) & ",'" & NOCG.Text & "','Ganti Giro|No=" & NOCG.Text & "','" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        vIDDtl += 1

                        'kreditnya
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & vIDDtl & ",2," & vIDMst & "," & cashbankacctgoid_old.Text & ",'C'," & ToDouble(amount.Text) * ToDouble(currencyrate.Text) & ",'" & NOCG.Text & "','GANTI Giro|No=" & NOCG.Text & "','" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    Else 'hutang giro tolakan
                        'insert data detail DEBET
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & vIDDtl & ",1," & vIDMst & "," & cashbankacctgoid_old.Text & ",'D'," & ToDouble(amount.Text) * ToDouble(currencyrate.Text) & ",'" & NOCG.Text & "','GANTI Giro|No=" & NOCG.Text & "','" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        vIDDtl += 1

                        'kreditnya
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,upduser,updtime,branch_code) VALUES ('" & CompnyCode & "'," & vIDDtl & ",2," & vIDMst & "," & cashbankacctgoid_new & ",'C'," & ToDouble(amount.Text) * ToDouble(currencyrate.Text) & ",'" & NOCG.Text & "','GANTI Giro|No=" & NOCG.Text & "','" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    End If

                    sSql = "UPDATE QL_mstoid SET lastoid=" & vIDMst & " WHERE tablename='QL_trnglmst'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & vIDDtl & " WHERE tablename='QL_trngldtl'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
            End If

            '-- Cek Status giro sdh di input pada form giro in---
            '----------------------------------------------------
            sSql = "Select COUNT(trngirodtloid) From QL_trngiroindtl Where bankoid=" & bankoid_old.Text & " AND girores1=" & girodtloid_old.Text & ""
            objCmd.CommandText = sSql : Dim zCek As Integer = objCmd.ExecuteScalar
            If CInt(zCek) > 0 Then
                sSql = "Select trngirodtloid From QL_trngiroindtl Where bankoid=" & bankoid_old.Text & " AND girores1=" & girodtloid_old.Text & "" : objCmd.CommandText = sSql : zCek = objCmd.ExecuteScalar
                '--- Update kolom girores1 dan bankoid pada QL_trngiroindtl untuk integrasi pada manifest giro--- 
                sSql = "Update QL_trngiroindtl set bankoid=" & bankbaru.SelectedValue & ", girores1=" & girodtloid.Text & " Where trngirodtloid=" & CInt(zCek) & ""
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close() : lblPost.Text = ""
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
            objTrans.Dispose()
            Exit Sub
        End Try
        Session("tbldtl") = Nothing : Session("oid") = Nothing
        showMessage("Data telah diposting !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("trnGiroChange.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblPost.Text = "POST"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            Dim strSQL As String
            Dim objConn As New SqlClient.SqlConnection(ConnStr)
            Dim objTrans As SqlClient.SqlTransaction
            Dim objCmd As New SqlClient.SqlCommand
            objConn.Open()
            objTrans = objConn.BeginTransaction()
            objCmd.Connection = objConn
            objCmd.Transaction = objTrans

            Try
                strSQL = "DELETE FROM QL_TukarGiro WHERE cmpcode='" & CompnyCode & "' AND TukarGiroOid=" & Session("oid")
                objCmd.CommandText = strSQL
                objCmd.ExecuteNonQuery()

                objTrans.Commit() : objCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : objCmd.Connection.Close()
                showMessage(ex.Message, CompnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
            End Try
            Session("oid") = Nothing : Session("tbldtl") = Nothing
            showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            showMessage("Tidak dapat menghapus data karena tidak ada data yang di pilih", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End If
    End Sub

    Protected Sub imbPrintNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub imbPrintFormList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            PrintNota(sender.CommandArgument, sender.ToolTip)
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnGiroChange.aspx?awal=true")
    End Sub

    Protected Sub GVmst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    End Sub

    Protected Sub gvGiro_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            'e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub imbClearGiro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        noGiroLama.Text = ""
        reftype.Text = ""
        custname.Text = ""
        refoid.Text = ""
    End Sub

    Protected Sub GVmst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub sebab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If sebab.SelectedValue = "Giro Di Tolak" Then
            TD1.Visible = False : TD1.Visible = False : TD2.Visible = False
            TD3.Visible = False : TD4.Visible = False ': TD5.Visible = False
            'TD6.Visible = False : TD8.Visible = False : TD7.Visible = False
        ElseIf sebab.SelectedValue = "Ganti Giro" Then
            'TD8.Visible = False : TD7.Visible = False
            TD1.Visible = True : TD1.Visible = True : TD2.Visible = True
            TD3.Visible = False : TD4.Visible = False ': TD5.Visible = True
            'TD6.Visible = True
        Else
            TD1.Visible = False : TD1.Visible = False : TD2.Visible = False
            TD3.Visible = False : TD4.Visible = False ': TD5.Visible = False
            'TD6.Visible = False : TD8.Visible = True : TD7.Visible = True
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA(NOCG.Text, CabangNya.SelectedValue, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
            e.Row.Cells(3).Text = NewMaskEdit(e.Row.Cells(3).Text)
        End If
    End Sub

    Protected Sub jenis_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblGiroData.Text = "List Of Giro Account (" & jenis.SelectedValue & ")"
    End Sub

    Protected Sub gvGiro_RowDataBound1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGiro.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells.Item(5).Text = ToMaskEdit(e.Row.Cells.Item(5).Text, 2)
        End If
    End Sub

    Protected Sub ddlTipe_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub GVmst_RowDataBound1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells.Item(5).Text = "Giro Di Tolak" Or e.Row.Cells.Item(5).Text = "Ganti Cash/Bank" Then
                e.Row.Cells.Item(4).Text = ""
            End If
        End If
    End Sub

    Protected Sub gvSupplierX_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSupplierX.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = NewMaskEdit(e.Row.Cells(5).Text)
        End If
    End Sub

    Protected Sub gvsupplierz_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplierX.SelectedIndexChanged
        '   girodtloid,Trans_No,girono,Bank,bankoid,NoRekening,GiroDueDate
        girodtloid.Text = gvSupplierX.SelectedDataKey("girodtloid").ToString()
        noGiroBaru.Text = gvSupplierX.SelectedDataKey("girono") & " / " & gvSupplierX.SelectedDataKey("NoRekening")
        payduedatebaru.Text = gvSupplierX.SelectedDataKey("GiroDueDate")
        bankbaru.SelectedValue = gvSupplierX.SelectedDataKey("bankoid")
        amount.Text = NewMaskEdit(gvSupplierX.SelectedDataKey("amount"))
        hiddenbtn2sX.Visible = False
        Panel1X.Visible = False
        ModalPopupExtender3sX.Hide()
    End Sub

    Protected Sub ibtnSuppIDX_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BinddataGiro()
    End Sub

    Protected Sub imbViewAllsX_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFindSuppIDX.Text = ""
        BinddataGiro()
    End Sub

    Protected Sub CloseSuppX_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hiddenbtn2sX.Visible = False
        Panel1X.Visible = False
        ModalPopupExtender3sX.Hide()
    End Sub

    Protected Sub creditsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BinddataGiro()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnGiroChange.aspx?awal=true")
        End If
    End Sub

    Protected Sub CabangNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CabangNya.SelectedIndexChanged
        GenerateNo()
        ddlCashBank()
    End Sub

    Protected Sub imbGiro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbGiro.Click
        lblGiroData.Text = "List Of Giro Account (" & jenis.SelectedValue & ")"
        PanelGiro.Visible = True
        btnHiddenGiro.Visible = True
        ModalPopupExtenderGiro.Show()
        BinddataGiro("", CompnyCode)
    End Sub
#End Region

End Class
