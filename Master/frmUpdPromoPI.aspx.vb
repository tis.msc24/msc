'Prgmr : zipi ; Update : zipi on 15.03.13
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class UpdTglPromoPI
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Private cKon As New Koneksi
    Private cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub BindData(ByVal filterCompCode As String)
        Try 
            sSql = "Select bm.trnbelimstoid,bm.branch_code,bm.trnbelimstoid,Convert(Varchar(10),bm.trnbelidate,103) Belidate,bm.trnbelino,sp.suppname,statusupd,Convert(Varchar(10),bm.promodate,103) promodate from QL_trnbelimst bm Inner Join QL_mstsupp sp ON sp.suppoid=bm.trnsuppoid WHere statusupd='POST' And " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If chkPeriod.Checked Then
                If FilterPeriod1.Text = "" And FilterPeriod2.Text = "" Then
                    showMessage("Please fill periode!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                Else
                    sSql &= " AND bm.trnbelidate BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND '" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
                End If
            End If
            sSql &= " Order BY trnbelino"
            Session("QL_mstopenpo") = cKon.ambiltabel(sSql, "QL_mstopenpo")
            tbldata.DataSource = Session("QL_mstopenpo")
            tbldata.DataBind() : tbldata.SelectedIndex = -1
        Catch ex As Exception
            showMessage("Maaf, Program Error Silahkan hubungi staf programmer..!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub FillTextBox(ByVal OidOp As Integer)
        Try
            sSql = "Select bm.trnbelimstoid,bm.branch_code,bm.trnbelimstoid,Convert(Varchar(10),bm.trnbelidate,103) Belidate,Convert(Varchar(10),bm.promodate,103) promodate,bm.trnbelino,sp.suppname,statusupd,bm.updtime,bm.upduser from QL_trnbelimst bm Inner Join QL_mstsupp sp ON sp.suppoid=bm.trnsuppoid WHere bm.trnbelimstoid=" & OidOp & ""
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                iOid.Text = xreader("trnbelimstoid")
                Nopo.Text = Trim(xreader("trnbelino"))
                StatusNya.Text = Trim(xreader("statusupd"))
                TglPromo.Text = Trim(xreader("promodate"))
                Upduser.Text = Trim(xreader("upduser"))
                Updtime.Text = Format(CDate(Trim(xreader("updtime").ToString)), "MM/dd/yyyy HH:mm:ss")
                If StatusNya.Text = "POST" Then
                    BtnDel.Visible = False : BtnPost.Visible = False : btnsavemstr.Visible = False
                Else
                    BtnDel.Visible = True : BtnPost.Visible = True
                End If
            End While
            conn.Close()
        Catch ex As Exception
            conn.Close()
            showMessage("Maaf, Program Error Silahkan hubungi staff programmer..!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Public Sub binddataSupp(ByVal sqlPlus As String)
        sSql = "Select bm.trnbelimstoid,bm.branch_code,bm.trnbelimstoid,Convert(Varchar(10),bm.trnbelidate,103) Belidate,bm.trnbelino,sp.suppname from QL_trnbelimst bm Inner Join QL_mstsupp sp ON sp.suppoid=bm.trnsuppoid WHere trnbelimstoid>0 AND (bm.trnbelino LIKE '%" & Tchar(Nopo.Text) & "%' OR sp.suppname LIKE '%" & Tchar(Nopo.Text) & "%') ORDER BY bm.trnbelino desc"
        Session("QL_mstsupp") = cKon.ambiltabel(sSql, "QL_mstsupp")
        gvSupplier.DataSource = Session("QL_mstsupp")
        gvSupplier.DataBind() : gvSupplier.SelectedIndex = -1
    End Sub

    Private Sub BindBarangNya(ByVal BeliOid As Integer)
        sSql = "SELECT d.trnbelidtloid,d.trnbelimstoid,d.trnbelidtlseq,d.itemoid mtroid, d.trnbelidtlqty,d.trnbelidtlunitoid trnbelidtlunit,d.trnbelidtlprice, d.trnbelidtlqtydisc trnbelidtldisc,d.trnbelidtldisctype,d.trnbelidtlqtydisc2 trnbelidtldisc2,d.trnbelidtldisctype2,d.trnbelidtlflag,d.trnbelidtlnote, d.trnbelidtlstatus,d.amtdisc,d.amtdisc2,d.amtbelinetto, (select itemdesc FROM QL_mstitem where itemoid = d.itemoid ) as material, g.gendesc AS unit,sj.trnsjbelioid sjoid,sj.branch_code to_branch_code,sj.trnsjbelioid sjrefoid,sj.trnsjbelino sjno,d.unitseq ,d.itemloc ,d.trnbelidtloid_po ,amtdisc totamtdisc,d.amtEkspedisi biayaexpedisi,sj.trnsjbelinote FROM QL_trnbelidtl d INNER JOIN ql_trnsjbelimst sj ON sj.cmpcode=d.cmpcode AND sj.trnsjbelino=d.trnsjbelino INNER JOIN ql_mstgen g on g.cmpcode=d.cmpcode AND g.genoid=d.trnbelidtlunitoid WHERE d.trnbelimstoid=" & BeliOid & " Order by d.trnbelidtlseq"
        Dim dtl As DataTable = cKon.ambiltabel(sSql, "ql_trnbelidtl")
        GvPidtl.DataSource = dtl : GvPidtl.DataBind()
        Session("TblDtl1") = dtl : GvPidtl.Visible = True
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\master\frmUpdPromoPI.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Open Security PO "
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

        Session("oid") = Request.QueryString("oid")

        BtnPost.Attributes.Add("OnClick", "javascript:return confirm('Data akan diposting..!!');")

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime, "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
            BindData(CompnyCode)

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        Dim objTable As New DataTable : objTable = Session("TblDtl")
        'fillTot() ': ReAmount()
    End Sub

    Protected Sub TblData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles tbldata.PageIndexChanging
        tbldata.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If chkPeriod.Checked = True Then
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text))
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text))
                If dat1 > dat2 Then
                    showMessage("Start Date must be less than End Date!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            Catch ex As Exception
                showMessage("Invalid date format!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End Try
        End If

        Session("FilterText") = Tchar(FilterText.Text)
        Session("FilterDDL") = ddlFilter.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnsavemstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsavemstr.Click
        If iOid.Text.Trim = "" Then
            showMessage("Maaf, Pilih nomer PO dulu..!!", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
 
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "Update QL_trnbelimst Set promodate='" & CDate(toDate(TglPromo.Text)) & "', statusupd='POST' Where trnbelimstoid=" & Integer.Parse(iOid.Text) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : xCmd.Connection.Close()
            If Session("oid") = Nothing Or Session("oid") = "" Then
                '--- insert table master
                'Session("openoid") = GenerateID("QL_mstopenpo", CompnyCode)
                'sSql = "INSERT INTO [QL_mstopenpo]([cmpcode],[openoid],[pomstoid],[tocabang],[crtuser],[crttime],[upduser],[updtime],statusnya) VALUES " & _
                '"('MSC'," & Session("openoid") & "," & iOid.Text & ",'" & toCabang & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & StatusNya.Text & "')"
                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'sSql = "Update QL_mstoid set lastoid=" & Session("openoid") & " Where tablename = 'QL_mstopenpo' and cmpcode = '" & CompnyCode & "' "
                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                'sSql = "UPDATE [QL_MSC].[dbo].[QL_mstopenpo]" & _
                '" SET [pomstoid] = " & iOid.Text & ",[tocabang] = '" & toCabang & "',[upduser] = '" & Session("UserID") & "',[updtime] = CURRENT_TIMESTAMP,[statusnya] = '" & StatusNya.Text & "'" & _
                '" Where openoid=" & Session("oid") & ""
                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
            objTrans.Rollback() : StatusNya.Text = "In Process"
            showMessage("Maaf, Program Error Silahkan hubungi staf programmer..!!", CompnyName & "  - ERROR", 1, "modalMsgBox")
            xCmd.Connection.Close() : Exit Sub
        End Try
        Response.Redirect("~\master\frmUpdPromoPI.aspx?awal=true")
    End Sub

    Protected Sub btnCancelMstr1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelMstr.Click
        Session("oid") = Nothing
        Session("TblDtl") = Nothing
        Response.Redirect("~\master\frmUpdPromoPI.aspx")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldata.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            'e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ddlFilter.SelectedIndex = 0 : FilterText.Text = ""
        chkPeriod.Checked = False
        FilterPeriod1.Text = Format(GetServerTime, "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
        DDLStatus.SelectedIndex = 0
        Session("FilterText") = Nothing
        Session("FilterDDL") = Nothing
        BindData(CompnyCode)
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        gvSupplier.Visible = True
        binddataSupp("")
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        iOid.Text = gvSupplier.SelectedDataKey("trnbelimstoid")
        Nopo.Text = gvSupplier.SelectedDataKey("trnbelino").ToString().Trim
        TglPromo.Text = gvSupplier.SelectedDataKey("Belidate").ToString
        BindBarangNya(iOid.Text)
        cProc.DisposeGridView(sender)
        gvSupplier.Visible = False
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If lblMessage.Text = "Data telah diClose !" Then
            Response.Redirect("~\master\frmUpdPromoPI.aspx?awal=true")
        Else
            PanelMsgBox.Visible = False
            beMsgBox.Visible = False
            mpeMsgbox.Hide()
        End If
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        iOid.Text = "" : Nopo.Text = ""
        osPO.Text = "" : GvPidtl.Visible = False
    End Sub

    Protected Sub BtnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnPost.Click
        StatusNya.Text = "POST" : btnsavemstr_Click(sender, e)
    End Sub

    Protected Sub BtnDel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDel.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Then
                showMessage("Maaf, Apakah and sudah pernah input data sebelumnya?!", CompnyName & "  - ERROR", 1, "modalMsgBox")
            Else
                sSql = "Delete QL_mstopenpo Where openoid=" & Session("oid") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : StatusNya.Text = "In Process"
            showMessage("Maaf, Program Error Silahkan hubungi staf programmer..!!", CompnyName & "  - ERROR", 1, "modalMsgBox")
            xCmd.Connection.Close() : Exit Sub
        End Try
        Response.Redirect("~\master\frmUpdPromoPI.aspx?awal=true")
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        gvSupplier.Visible = True : binddataSupp("")
    End Sub
#End Region

End Class

