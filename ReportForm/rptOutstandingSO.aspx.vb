Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptOutstandingSO
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim CKon As New Koneksi
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim sSql As String = ""
#End Region

#Region "Procedure"
    Public Sub initGroup()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("ALL GROUP", "ALL GROUP"))
        FilterDDLGrup.SelectedValue = "ALL GROUP"
    End Sub
    Public Sub initSubGroup()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GROUP", "ALL SUB GROUP"))
        FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP"
    End Sub
    Sub showPrint(ByVal tipe As String)
        Try
            Dim sWhere As String = ""
            Dim namapdf As String = ""

            If ddltype.SelectedValue = "Summary" Then
                vReport.Load(Server.MapPath("~\Report\rptSOOutstandingSum.rpt"))
                namapdf = "SO_Outstanding_Sum_"
            Else
                vReport.Load(Server.MapPath("~\Report\rptSOOutstandingDtl.rpt"))
                namapdf = "SO_Outstanding_Dtl_"
            End If

            sWhere = " where pom.trnorderdate between '" & CDate(toDate(range1.Text)) & "' and '" & CDate(toDate(range2.Text)) & "'"
            If orderno.Text.Trim <> "" Then
                sWhere &= " and pom.orderno like '%" & Tchar(orderno.Text) & "%'"
            End If

            If custname.Text.Trim <> "" Then
                sWhere &= " and supp.custname like '%" & Tchar(custname.Text) & "%'"
            End If
            If itemdesc.Text.Trim <> "" Then
                sWhere &= "and pdt.itemoid = " & itemoid.Text & " "
            End If
            sWhere &= IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", "  ", "  and  mit.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
            sWhere &= IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", "  ", "  and  mit.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")
            If taxable.SelectedValue <> "ALL" Then
                If taxable.SelectedValue = "YES" Then
                    sWhere &= " and pom.trntaxpct>0"
                Else
                    sWhere &= " and pom.trntaxpct=0"
                End If
            End If


            CProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("periode", range1.Text & "-" & range2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            If tipe = "" Then
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namapdf & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namapdf & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If

        Catch ex As Exception
            vReport.Close()
            vReport.Dispose()
        End Try
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("rptOutstandingSO.aspx")
        End If
        Page.Title = CompnyCode & " - Laporan Outstanding SO"
        If IsPostBack Then
            If Session("showprint") = "True" Then
                showPrint("")
            End If
        Else
            initGroup()
            initSubGroup()
            range1.Text = Format(New Date(Now.Year, Now.Month, 1), "dd/MM/yyyy")
            range2.Text = Format(Now, "dd/MM/yyyy")
        End If
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub
    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowprint.Click
        message.Text = ""
        If range1.Text.Trim = "" Then
            message.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            message.Text = "Please fill period 2 value!"
            Exit Sub
        End If
        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            message.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            message.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            message.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        Session("showprint") = "True"
        showPrint("")
    End Sub
    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        message.Text = ""
        If range1.Text.Trim = "" Then
            message.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            message.Text = "Please fill period 2 value!"
            Exit Sub
        End If
        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            message.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            message.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            message.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        Session("showprint") = "False"
        showPrint("pdf")
    End Sub
    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        message.Text = ""
        If range1.Text.Trim = "" Then
            message.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            message.Text = "Please fill period 2 value!"
            Exit Sub
        End If
        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            message.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            message.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            message.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        Session("showprint") = "False"
        showPrint("excel")
    End Sub
    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("rptOutstandingSO.aspx?awal=true")
    End Sub
    Protected Sub searchso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles searchso.Click
        message.Text = ""
        If range1.Text.Trim = "" Then
            message.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            message.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            message.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            message.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            message.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If

        sSql = "select a.ordermstoid,a.orderno,a.trnorderdate,b.custoid,b.custname from ql_trnordermst a inner join ql_mstcust b on a.trncustoid=b.custoid where a.cmpcode='" & CompnyCode & "' and a.trnorderstatus in ('Approved') and a.trnordertype in ('grosir','projeck') and a.trnorderdate between '" & date1 & "' and '" & date2 & "' and a.orderno like '%" & Tchar(orderno.Text) & "%'"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVSo")
        GVSo.DataSource = dtab
        GVSo.DataBind()
        GVSo.Visible = True
        Session("GVSo") = dtab

        GVCustomer.Visible = False
        GVBarang.Visible = False
    End Sub
    Protected Sub eraseso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eraseso.Click
        orderno.Text = ""
        ordermstoid.Text = ""

        GVSo.DataSource = Nothing
        GVSo.DataBind()
        GVSo.Visible = False
        Session("GVSo") = Nothing
    End Sub
    Protected Sub GVSo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVSo.PageIndexChanging
        If Not Session("GVSo") Is Nothing Then
            GVSo.DataSource = Session("GVSo")
            GVSo.PageIndex = e.NewPageIndex
            GVSo.DataBind()
        End If
    End Sub
    Protected Sub GVSo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVSo.SelectedIndexChanged
        orderno.Text = GVSo.SelectedDataKey("orderno")
        ordermstoid.Text = GVSo.SelectedDataKey("ordermstoid")
        custname.Text = GVSo.SelectedDataKey("custname")
        custoid.Text = GVSo.SelectedDataKey("custoid")

        GVSo.DataSource = Nothing
        GVSo.DataBind()
        GVSo.Visible = False
        Session("GVSo") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub
    Protected Sub searchcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles searchcust.Click
        sSql = "select custoid,custcode,custname from QL_mstcust where custflag='Active' and cmpcode='" & CompnyCode & "' and (custname like '%" & Tchar(custname.Text) & "%' or custcode like '%" & Tchar(custname.Text) & "%')"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVCustomer")
        GVCustomer.DataSource = dtab
        GVCustomer.DataBind()
        GVCustomer.Visible = True
        Session("GVCustomer") = dtab

        GVSo.Visible = False
        GVBarang.Visible = False
    End Sub
    Protected Sub erasecust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles erasecust.Click
        custname.Text = ""
        custoid.Text = ""

        GVCustomer.DataSource = Nothing
        GVCustomer.DataBind()
        GVCustomer.Visible = False
        Session("GVCustomer") = Nothing
    End Sub
    Protected Sub GVCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVCustomer.PageIndexChanging
        If Not Session("GVCustomer") Is Nothing Then
            GVCustomer.DataSource = Session("GVCustomer")
            GVCustomer.PageIndex = e.NewPageIndex
            GVCustomer.DataBind()
        End If
    End Sub
    Protected Sub GVCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVCustomer.SelectedIndexChanged
        custname.Text = GVCustomer.SelectedDataKey("custname")
        custoid.Text = GVCustomer.SelectedDataKey("custoid")

        GVCustomer.DataSource = Nothing
        GVCustomer.DataBind()
        GVCustomer.Visible = False
        Session("GVCustomer") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub
    Protected Sub barangsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangsearch.Click
        sSql = "select itemoid,itemcode,itemdesc,Merk from QL_mstitem where itemflag='Aktif' and cmpcode='" & CompnyCode & "' and (itemcode like '%" & Tchar(itemdesc.Text) & "%' or itemdesc like '%" & Tchar(itemdesc.Text) & "%' or Merk like '%" & Tchar(itemdesc.Text) & "%')"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVBarang")
        GVBarang.DataSource = dtab
        GVBarang.DataBind()
        GVBarang.Visible = True
        Session("GVBarang") = dtab

        GVSo.Visible = False
        GVCustomer.Visible = False
    End Sub
    Protected Sub barangerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangerase.Click
        itemdesc.Text = ""
        itemoid.Text = ""
        merk.Text = ""

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
    End Sub
    Protected Sub GVBarang_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVBarang.PageIndexChanging
        If Not Session("GVBarang") Is Nothing Then
            GVBarang.DataSource = Session("GVBarang")
            GVBarang.PageIndex = e.NewPageIndex
            GVBarang.DataBind()
        End If
    End Sub
    Protected Sub GVBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVBarang.SelectedIndexChanged
        itemdesc.Text = GVBarang.SelectedDataKey("itemdesc")
        itemoid.Text = GVBarang.SelectedDataKey("itemoid")
        merk.Text = GVBarang.SelectedDataKey("Merk")

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub

#End Region
End Class
