Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptSSStatus
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim fCabang As String = ""
#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                dtView2.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblDO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtTbl2 As DataTable = Session("TblDOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                dtView2.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblDO") = dtTbl
                Session("TblDOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipment") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "shipmentitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblShipment") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtTbl2 As DataTable = Session("TblShipmentView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "shipmentitemmstoid=" & cbOid
                                dtView2.RowFilter = "shipmentitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblShipment") = dtTbl
                Session("TblShipmentView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("Maaf, Format tanggal salah..!!", 2)
            Exit Sub
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Maaf, periode 2 lebih besar dari periode 1..!!", 2)
            Exit Sub
        End If

        Try
            Dim namaPDF As String = "", StatusOs As String = "", sWhere As String = ""
            If ddlTypeTW.SelectedValue = "Service Supplier" Then
                sWhere &= " WHERE twm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
            Else
                sWhere &= " WHERE tcm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
            End If

            If dCabangNya.SelectedValue <> "ALL" Then
                sWhere &= "AND twm.FromBranch='" & dCabangNya.SelectedValue & "'"
            End If

            If sono.Text <> "" Then
                Dim scode() As String = Split(sono.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " twm.TrfwhserviceNo='" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If dono.Text <> "" Then
                Dim scode() As String = Split(dono.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " tcm.trftcserviceno='" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " sup.suppcode='" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If statusDelivery.SelectedValue <> "ALL" Then
                If statusDelivery.SelectedValue = "COMPLETE" Then
                    StatusOs &= " And upper(StatusOs)='COMPLETE'"
                Else
                    StatusOs &= " And upper(StatusOs) <> 'COMPLETE'"
                End If
            End If

            If DDLGaransi.SelectedValue <> "ALL" Then
                StatusOs &= " AND TypeGaransi='" & DDLGaransi.SelectedValue & "'"
            End If

            If shipmentno.Text <> "" Then
                Dim scode() As String = Split(shipmentno.Text, ";")
                StatusOs &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    StatusOs &= "ft.ReqTTS='" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        StatusOs &= " OR "
                    End If
                Next
                StatusOs &= ")"
            End If

            vReport = New ReportDocument
            If tipe = "excel" Then
                vReport.Load(Server.MapPath("~\Report\rptTWSstatusEXL.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptTWSstatus.rpt"))
            End If
            namaPDF = "Status_Servis_Supplier"

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("StatusOs", StatusOs)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            Session("diprint") = "True"
            If tipe = "" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub BindListSO()
        Try
            sSql = "Select 'False' AS checkvalue, twm.trfwhserviceoid soitemmstoid, br.gendesc Cabang, twm.TrfwhserviceNo soitemno, Convert(Char(20),twm.finalappovaldatetime, 103) sodate, twm.Trfwhservicestatus soitemmststatus, sup.suppname, sup.suppoid, twm.FromMtrLocOid Locoid, br.gendesc +' - '+loc.gendesc Gudang From ql_trfwhservicemst twm Inner Join QL_mstsupp sup ON sup.suppoid=twm.suppoid Inner Join QL_mstgen br ON br.gencode=twm.FromBranch AND br.gengroup='CABANG' Inner Join QL_mstgen loc ON loc.genoid=twm.FromMtrLocOid AND loc.gengroup='LOCATION' Left Join ql_trntcsuppintmst tcm ON tcm.Trfwhserviceoid=twm.Trfwhserviceoid Where Trfwhservicetype='EXTERNAL'"
            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= " AND twm.FromBranch='" & dCabangNya.SelectedValue & "'"
            End If

            If dateAwal.Text <> "" And dateAkhir.Text <> "" Then
                If IsValidPeriod() Then
                    If ddlTypeTW.SelectedValue = "Service Supplier" Then
                        sSql &= " AND twm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
                    Else
                        sSql &= " AND tcm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
                    End If
                Else
                    Exit Sub
                End If
            End If

            sSql &= " ORDER BY twm.updtime DESC, twm.TrfwhserviceNo DESC"
            Session("TblSO") = ckon.ambiltabel(sSql, "ql_trfwhservicemst")
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
        
    End Sub

    Private Sub BindListDO()
        sSql = "Select 'False' AS checkvalue, br.gendesc, tcm.trntcserviceoid doitemmstoid, trftcserviceno doitemno, sup.suppname custname, Convert(Char(20),tcm.updtime,103) dodate, tcm.trntcstatus doitemmststatus From QL_trntcsuppmst tcm Inner Join QL_mstsupp sup ON sup.suppoid=tcm.suppoid Inner Join QL_mstgen br ON br.gencode=tcm.branch_code AND br.gengroup='CABANG' Where tcm.cmpcode='" & CompnyCode & "'"

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND tcm.branch_code='" & dCabangNya.SelectedValue & "'"
        End If

        sSql &= " AND tcm.trfwhserviceoid IN (Select twm.Trfwhserviceoid From ql_trfwhservicemst twm Where twm.cmpcode='" & CompnyCode & "'"
        If dateAwal.Text <> "" And dateAkhir.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND twm.updtime>='" & CDate(toDate(dateAwal.Text)) & " 00:00:00' AND twm.updtime<='" & CDate(toDate(dateAkhir.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        If sono.Text <> "" Then
            Dim scode() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " twm.TrfwhserviceNo='" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        sSql &= " ) ORDER BY tcm.updtime DESC, tcm.trntcserviceoid DESC"
        Session("TblDO") = ckon.ambiltabel(sSql, "QL_trntcsuppmst")
    End Sub

    Private Sub BindListCust()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, gendesc, c.suppoid custoid, c.suppcode custcode, c.suppname custname,c.suppaddr custaddr FROM QL_mstsupp c INNER JOIN ql_trfwhservicemst twm ON twm.suppoid=c.suppoid AND twm.Trfwhservicetype='EXTERNAL' INNER JOIN QL_mstgen b ON b.gencode=twm.FromBranch AND b.gengroup='CABANG' LEFT JOIN QL_trntcsuppmst tcm ON tcm.trfwhserviceoid=twm.Trfwhserviceoid WHERE c.cmpcode='" & CompnyCode & "'"
        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND twm.FromBranch='" & dCabangNya.SelectedValue & "'"
        End If
 
        If dateAwal.Text <> "" And dateAkhir.Text <> "" Then
            If IsValidPeriod() Then
                If ddlTypeTW.SelectedValue = "Service Supplier" Then
                    sSql &= " AND twm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
                Else
                    sSql &= " AND tcm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
                End If
            Else
                Exit Sub
            End If
        End If

        If sono.Text <> "" Then
            Dim scode() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " twm.TrfwhserviceNo='" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If dono.Text <> "" Then
            Dim scode() As String = Split(dono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " tcm.trftcserviceno='" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        sSql &= " ORDER BY c.suppcode"
        Session("TblCust") = ckon.ambiltabel(sSql, "QL_mstsupp")
    End Sub

    Private Sub BindListShipment()
        sSql = "SELECT 'False' AS checkvalue, d.gendesc, rqm.reqoid shipmentitemmstoid,rqm.reqcode shipmentitemno, rqm.reqdate shipmentitemdate, CONVERT(VARCHAR(10), rqm.reqdate, 101) AS shipmentdate, c.custname AS custname, rqm.reqstatus shipmentitemmststatus,'' shipmentitemmstnote FROM QL_TRNREQUEST rqm INNER JOIN QL_mstcust c ON rqm.reqcustoid=c.custoid INNER JOIN QL_mstgen d ON d.gencode=rqm.branch_code AND d.gengroup='CABANG' Where reqoid IN ( Select twd.reqoid from ql_trfwhservicedtl twd INNER JOIN ql_trfwhservicemst twm ON twm.Trfwhserviceoid=twd.trfwhserviceoid LEFT JOIN QL_trntcsuppmst tcm ON tcm.trfwhserviceoid=twm.Trfwhserviceoid INNER JOIN QL_mstsupp c ON c.suppoid=twm.suppoid Where Trfwhservicetype='EXTERNAL'"

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND twm.FromBranch='" & dCabangNya.SelectedValue & "'"
        End If

        If dateAwal.Text <> "" And dateAkhir.Text <> "" Then
            If IsValidPeriod() Then
                If ddlTypeTW.SelectedValue = "Service Supplier" Then
                    sSql &= " AND twm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
                Else
                    sSql &= " AND tcm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & "' 23:59:59"
                End If
            Else
                Exit Sub
            End If
        End If

        If sono.Text <> "" Then
            Dim scode() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " twm.TrfwhserviceNo='" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If dono.Text <> "" Then
            Dim scode() As String = Split(dono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " tcm.trftcserviceno='" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.suppcode='" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        sSql &= ") Order BY reqcode"
        Session("TblShipment") = cKon.ambiltabel(sSql, "QL_trnshipmentitemmst")
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT 'False' AS checkvalue, i.itemoid, i.itemdesc itemlongdesc, i.itemcode, 'BUAH' unit,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' End JenisNya, i.stockflag FROM QL_mstitem i Where i.cmpcode='" & CompnyCode & "' AND i.itemoid IN (Select twd.itemoid from ql_trfwhservicedtl twd Inner Join ql_trfwhservicemst twm ON twd.trfwhserviceoid=twm.Trfwhserviceoid Inner Join QL_mstsupp c ON c.suppoid=twm.suppoid LEFT JOIN QL_trntcsuppmst tcm ON tcm.trfwhserviceoid=twm.Trfwhserviceoid Where Trfwhservicetype='EXTERNAL'"
        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND twm.FromBranch='" & dCabangNya.SelectedValue & "'"
        End If

        If dateAwal.Text <> "" And dateAkhir.Text <> "" Then
            If IsValidPeriod() Then
                If ddlTypeTW.SelectedValue = "Service Supplier" Then
                    sSql &= " AND twm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
                Else
                    sSql &= " AND tcm.updtime BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy") & "' AND '" & Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy") & " 23:59:59'"
                End If
            Else
                Exit Sub
            End If
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.suppcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If sono.Text <> "" Then
            Dim scode() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " twm.TrfwhserviceNo='" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If dono.Text <> "" Then
            Dim sDono() As String = Split(dono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sDono.Length - 1
                sSql &= " tcm.trftcserviceno LIKE '%" & Tchar(sDono(c1)) & "%'"
                If c1 < sDono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If
        sSql &= ") Order By i.itemcode"
        Session("TblMat") = ckon.ambiltabel(sSql, "QL_mstitem")
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptServiceSuppStatus.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Servis Supplier Status"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            fDDLBranch()
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub  

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptServiceSuppStatus.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showPrint("excel")
    End Sub   

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        showPrint("pdf")
    End Sub   

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showPrint("")
    End Sub   

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing
            gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)

        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If

        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If

        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If

        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If

        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If

        If Not Session("EmptyListDO") Is Nothing And Session("EmptyListDO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListDO") Then
                Session("EmptyListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If

        If Not Session("WarningListDO") Is Nothing And Session("WarningListDO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListDO") Then
                Session("WarningListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "No. Service Supplier data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        'soitemmstoid
        Dim sPlus As String = ""
        If DDLFilterListSO.SelectedValue = "soitemmstoid" Then
            sPlus = DDLFilterListSO.SelectedValue & "=" & TcharNoTrim(txtFilterListSO.Text) & ""
        Else
            sPlus = DDLFilterListSO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListSO.Text) & "%'"
        End If

        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "No. Service Supplier data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "No. Service Supplier data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some No. Service Supplier data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some No. Service Supplier data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Selected No. Service Supplier data can't be found!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Selected No. Service Supplier data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sono.Text <> "" Then
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= ";" + vbCrLf + dtView(C1)("soitemno")
                            End If
                        Else
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= dtView(C1)("soitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    dCabangNya.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "Please select No. Service Supplier to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "Please show some No. Service Supplier data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub imbFindDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindDO.Click
        If IsValidPeriod() Then
            DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
            Session("TblDO") = Nothing : Session("TblDOView") = Nothing
            gvListDO.DataSource = Nothing : gvListDO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        sono.Text = ""
    End Sub

    Protected Sub imbEraseDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseDO.Click
        dono.Text = ""
    End Sub

    Protected Sub btnFindListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDO.Click
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "No. Kembali data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = ""
        If DDLFilterListDO.SelectedValue = "doitemno" Then
            sPlus = DDLFilterListDO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListDO.Text) & "%'"
        Else
            sPlus = DDLFilterListDO.SelectedValue & " = '" & TcharNoTrim(txtFilterListDO.Text) & "'"
        End If
        If UpdateCheckedDO() Then
            Dim dv As DataView = Session("TblDO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblDOView") = dv.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dv.RowFilter = ""
                mpeListDO.Show()
            Else
                dv.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "No. Kembali data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub
 
    Protected Sub btnViewAllListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListDO.Click
        DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "No. Kembali data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedDO() Then
            Dim dt As DataTable = Session("TblDO")
            Session("TblDOView") = dt
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub

    Protected Sub btnSelectAllDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some SI data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedDO.Click
        If Session("TblDO") Is Nothing Then
            Session("WarningListDO") = "Selected SI data can't be found!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
        If UpdateCheckedDO() Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
                Session("TblDOView") = dtView.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dtView.RowFilter = ""
                mpeListDO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "Selected SI data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListDO.Click
        If Not Session("TblDO") Is Nothing Then
            If UpdateCheckedDO() Then
                Dim dtTbl As DataTable = Session("TblDO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If dono.Text <> "" Then
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= ";" + vbCrLf + dtView(C1)("doitemno")
                            End If
                        Else
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= dtView(C1)("doitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = "" : dCabangNya.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
                Else
                    Session("WarningListDO") = "Please select SI to add to list!"
                    showMessage(Session("WarningListDO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListDO") = "Please show some SI data first!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListDO.Click
        cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
    End Sub

    Protected Sub gvListDO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDO.PageIndexChanging
        If UpdateCheckedDO2() Then
            gvListDO.PageIndex = e.NewPageIndex
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing
            gvListCust.DataSource = Nothing : gvListCust.DataBind()
            cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = ""
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Supplier data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Supplier data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            Session("WarningListCust") = "Supplier data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub
 
    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Supplier data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If

        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Supplier data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Supplier to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Supplier data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub  

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub
 
    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind() 
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        itemcode.Text = ""
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If JenisBarangDDL.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag ='" & JenisBarangDDL.SelectedValue & "'"
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected Katalog data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If itemcode.Text <> "" Then
                            itemcode.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            itemcode.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub imbEraseShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseShipment.Click
        shipmentno.Text = ""
    End Sub 

    Protected Sub imbFindShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindShipment.Click
        If IsValidPeriod() Then
            DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
            Session("TblShipment") = Nothing : Session("TblShipmentView") = Nothing
            gvListShipment.DataSource = Nothing : gvListShipment.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListShipment.Click
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Shipment data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = ""
        If DDLFilterListShipment.SelectedValue = "shipmentitemmstoid" Then
            sPlus = DDLFilterListShipment.SelectedValue & " = '" & Tchar(txtFilterListShipment.Text) & "'"
        Else
            sPlus = DDLFilterListShipment.SelectedValue & " LIKE '%" & Tchar(txtFilterListShipment.Text) & "%'"
        End If

        If UpdateCheckedShipment() Then
            Dim dv As DataView = Session("TblShipment").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblShipmentView") = dv.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dv.RowFilter = ""
                mpeListShipment.Show()
            Else
                dv.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Shipment data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub btnViewAllListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListShipment.Click
        DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Shipment data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedShipment() Then
            Dim dt As DataTable = Session("TblShipment")
            Session("TblShipmentView") = dt
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub

    Protected Sub btnSelectAllShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllShipment.Click
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "shipmentitemmstoid=" & dtTbl.Rows(C1)("shipmentitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneShipment.Click
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "shipmentitemmstoid=" & dtTbl.Rows(C1)("shipmentitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub 

    Protected Sub btnViewCheckedShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedShipment.Click
        If Session("TblShipment") Is Nothing Then
            Session("WarningListShipment") = "Selected Shipment data can't be found!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
        If UpdateCheckedShipment() Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
                Session("TblShipmentView") = dtView.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dtView.RowFilter = ""
                mpeListShipment.Show()
            Else
                dtView.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Selected Shipment data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListShipment.Click
        If Not Session("TblShipment") Is Nothing Then
            If UpdateCheckedShipment() Then
                Dim dtTbl As DataTable = Session("TblShipment")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If shipmentno.Text <> "" Then
                            shipmentno.Text &= ";" + vbCrLf + dtView(C1)("shipmentitemno")
                        Else
                            shipmentno.Text = dtView(C1)("shipmentitemno")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
                Else
                    Session("WarningListShipment") = "Please select Shipment to add to list!"
                    showMessage(Session("WarningListShipment"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListShipment.Click
        cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
    End Sub
#End Region
End Class
