<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptProgPoint.aspx.vb" Inherits="rptPRogPoint" title="MSC - Laporan Program Point" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="width: 100%;">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="X-Large" ForeColor="Navy" Text=".: Rekap Program Point"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="width: 100%; background-color: transparent" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=center><TABLE><TBODY><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label3" runat="server" Text="Status" __designer:wfdid="w68"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="ddlStatus" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w5" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>DETAIL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server" Visible="true">Type</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="ddlType" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w6" AutoPostBack="True"><asp:ListItem>Customer</asp:ListItem>
<asp:ListItem>Sales</asp:ListItem>
<asp:ListItem>Cabang</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label2" runat="server" Text="Cabang" __designer:wfdid="w68"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="ddlCabang" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w3"></asp:DropDownList></TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Text="Periode" __designer:wfdid="w68"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w69"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w70"></asp:ImageButton>&nbsp;- <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w71"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w72"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w73">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w74" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w75" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w76" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w77" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD id="Td3" align=left runat="server" Visible="true">Program Point Name</TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="PromoNya" runat="server" Width="151px" CssClass="inpText" __designer:wfdid="w78"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" onclick="btnSearchNota_Click1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w79"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w80"></asp:ImageButton>&nbsp;<asp:Label id="Oid" runat="server" Visible="False" __designer:wfdid="w81"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" id="TD5" align=right Visible="true"></TD><TD id="TD6" align=left colSpan=3 Visible="true"><asp:GridView id="GVPromo" runat="server" Width="450px" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w82" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="progpointmstoid,progpointcode,progpointdesc" UseAccessibleHeader="False" CellPadding="4" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="progpointcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="progpointdesc" HeaderText="Program Point Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD2" align=left runat="server"><asp:Label id="Label5" runat="server" Text="Customer" __designer:wfdid="w68"></asp:Label></TD><TD id="TD1" align=left colSpan=3 runat="server"><asp:TextBox id="suppname" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w83"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupplier" onclick="btnSearchSupplier_click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w84"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseSupplier" onclick="btnEraseSupplier_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w85"></asp:ImageButton>&nbsp;<asp:Label id="suppoid" runat="server" Visible="False" __designer:wfdid="w86"></asp:Label></TD></TR><TR><TD id="TD7" align=right runat="server"></TD><TD id="TD8" align=left colSpan=3 runat="server"><asp:GridView id="gvSupp" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w87" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="custcode,custname,custoid" UseAccessibleHeader="False" CellPadding="4" GridLines="None" PageSize="8" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged1">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Kode Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD11" align=left runat="server" Visible="false"><asp:Label id="Label1" runat="server" Width="91px" Text="Nama Barang" __designer:wfdid="w68"></asp:Label></TD><TD id="TD12" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="ItemNya" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w123" MaxLength="100"></asp:TextBox>&nbsp;<asp:ImageButton id="sItemNya" onclick="sItemNya_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w124"></asp:ImageButton>&nbsp;<asp:ImageButton id="eItemNya" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w125"></asp:ImageButton> <asp:Button id="BtnViewList" runat="server" CssClass="btn green" Font-Bold="True" Text="View List" __designer:wfdid="w176"></asp:Button></TD></TR><TR><TD id="TD10" align=left runat="server" Visible="false"></TD><TD id="TD9" align=left colSpan=3 runat="server" Visible="false"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w101" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" __designer:wfdid="w155" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="statusitem" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=4><asp:Label id="labelEx" runat="server" Width="57px" __designer:wfdid="w88"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w89"></asp:ImageButton> <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w90"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w91"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w92"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w93" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w94"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD align=center colSpan=4><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w95"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" __designer:wfdid="w96" ShowAllPageIds="True" AutoDataBind="true" DisplayGroupTree="False" HasCrystalLogo="False" HasExportButton="False" HasPrintButton="False" HasRefreshButton="True" HasSearchButton="False" HasToggleGroupTreeButton="False"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

