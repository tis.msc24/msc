<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPObeli.aspx.vb" Inherits="rptPObeli" Title="MSC - Laporan Pembelian PO" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Lap. Pembelian AMP"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: white" valign="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE><TBODY><TR><TD id="tdPeriod1" vAlign=top align=left runat="server" visible="true"><asp:Label id="Periode" runat="server" Text="Tanggal"></asp:Label> </TD><TD id="Td1" vAlign=top align=left runat="server" visible="true">:</TD><TD id="tdperiod2" align=left colSpan=3 runat="server" visible="true">
    <asp:DropDownList ID="DDLMonth" runat="server" CssClass="inpText">
        <asp:ListItem Value="1">JANUARI</asp:ListItem>
        <asp:ListItem Value="2">FEBRUARI</asp:ListItem>
        <asp:ListItem Value="3">MARET</asp:ListItem>
        <asp:ListItem Value="4">APRIL</asp:ListItem>
        <asp:ListItem Value="5">MEI</asp:ListItem>
        <asp:ListItem Value="6">JUNI</asp:ListItem>
        <asp:ListItem Value="7">JULI</asp:ListItem>
        <asp:ListItem Value="8">AGUSTUS</asp:ListItem>
        <asp:ListItem Value="9">SEPTEMBER</asp:ListItem>
        <asp:ListItem Value="10">OKTOBER</asp:ListItem>
        <asp:ListItem Value="11">NOVEMBER</asp:ListItem>
        <asp:ListItem Value="12">DESEMBER</asp:ListItem>
    </asp:DropDownList>&nbsp;<asp:DropDownList ID="DDLYear" runat="server" CssClass="inpText">
    </asp:DropDownList></TD></TR><TR><TD id="Td2" vAlign=top align=left runat="server" visible="false">Cabang</TD><TD id="Td3" vAlign=top align=left runat="server" visible="false">:</TD><TD id="Td4" align=left colSpan=3 runat="server" visible="false"><asp:DropDownList id="dd_branch" runat="server" Width="185px" CssClass="inpText"></asp:DropDownList></TD></TR>
    <tr>
        <td runat="server" align="left" visible="true">
            <asp:Label id="Label3" runat="server" Width="91px" Text="Supplier"></asp:Label></td>
        <td runat="server" align="left" visible="true">
            :</td>
        <td runat="server" align="left" colspan="3" visible="true">
            <asp:TextBox id="CustName" runat="server" Width="250px" CssClass="inpText"></asp:TextBox>
            <asp:ImageButton id="sCust" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>
            <asp:ImageButton id="eCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton></td>
    </tr>
    <tr>
        <td runat="server" align="left" visible="true">
        </td>
        <td runat="server" align="left" visible="true">
        </td>
        <td runat="server" align="left" colspan="3" visible="true">
            <asp:GridView id="GvCust" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" UseAccessibleHeader="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="CbHdrOid" runat="server" AutoPostBack="true" Checked="<%# GetSessionCheckNota() %>" OnCheckedChanged="CbHdrOid_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="SelectOid" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("custoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Supp. code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Wrap="False"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" HeaderText="custoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Black"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>
        </td>
    </tr>
    <TR><TD align=left runat="server" visible="true"><asp:Label id="LblSi" runat="server" Width="91px" Text="No. PO"></asp:Label></TD><TD align=left runat="server" visible="true"><asp:Label id="Label1" runat="server" Width="2px" Text=":"></asp:Label></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="TxtNoTrans" runat="server" Width="250px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="sBtnNoTrans" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="eBtnNoTrans" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR><TR><TD align=left runat="server" visible="true"></TD><TD align=left runat="server" visible="true"></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:GridView id="GvNotrans" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" UseAccessibleHeader="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="CbHdrNo" runat="server" AutoPostBack="true" Checked="<%# GetSessionCheckNota() %>" OnCheckedChanged="CbHdrNo_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="CekSelect" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("JualOid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="notrans" HeaderText="No. PO">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnDate" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Black"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR>
    <tr>
        <td id="TD5" runat="server" align="left" visible="false">
            Item</td>
        <td id="TD7" runat="server" align="left" visible="false">
            :</td>
        <td id="TD6" runat="server" align="left" colspan="3" visible="false">
            <asp:TextBox ID="ItemDesc" runat="server" CssClass="inpText" Width="250px"></asp:TextBox>&nbsp;<asp:ImageButton
                ID="BtnItemSeacrh" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/search2.gif" />&nbsp;<asp:ImageButton
                    ID="BtnItemErase" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/erase.bmp" /></td>
    </tr>
    <TR><TD align=left runat="server" visible="true"></TD><TD align=left runat="server" visible="true"></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:GridView id="GvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" UseAccessibleHeader="False" CellPadding="4" GridLines="None">
    <PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast" />
    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
    <Columns>
        <asp:TemplateField>
            <HeaderTemplate>
                <asp:CheckBox ID="CbHdrCo" runat="server" AutoPostBack="true" Checked="<%# GetSessionCheckNota() %>"
                    OnCheckedChanged="CbHdrCo_CheckedChanged" />
            </HeaderTemplate>
            <ItemTemplate>
                <asp:CheckBox ID="SelectItem" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("itemoid") %>' />
            </ItemTemplate>
            <HeaderStyle CssClass="gvhdr" />
        </asp:TemplateField>
        <asp:BoundField DataField="itemcode" HeaderText="Item. Code">
            <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
        </asp:BoundField>
        <asp:BoundField DataField="itemdesc" HeaderText="Item. Desc">
            <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
        </asp:BoundField>
        <asp:BoundField DataField="statusitem" HeaderText="Jenis Item">
            <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
        </asp:BoundField>
    </Columns>
    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Red" HorizontalAlign="Right" />
    <EmptyDataTemplate>
        <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
    </EmptyDataTemplate>
    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Black" />
    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
    <AlternatingRowStyle BackColor="White" />
</asp:GridView>
</TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnreport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibpdf" onclick="ibpdf_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
<div id="progressBackgroundFilter" class="progressBackgroundFilter">
</div>
<div id="processMessage" class="processMessage">
<span style="font-weight: bold; font-size: 10pt; color: purple">
<asp:Image ID="Image4" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/loadingbar.gif" /><br />
Please Wait .....</span><br />
</div>
</ProgressTemplate>
</asp:UpdateProgress>
    <asp:Label ID="lblbln1" runat="server" Visible="False"></asp:Label><asp:Label ID="lblbln2" runat="server" Visible="False"></asp:Label><asp:Label ID="lblbln3" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="ibpdf"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

