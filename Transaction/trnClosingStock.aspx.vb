'Coded by VaL
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction

Partial Class transaction_trnClosingStock
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim ckoneksi As New Koneksi
    Dim sTemp As String = ""
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        xCmd.CommandTimeout = 360
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\Transaction\trnClosingStock.aspx") ' di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        If ckoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            'Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Closing Stock"
        CaptionLabel.Text = CompnyName & " - INFO-"
        CaptionLabel1.Text = CompnyName & " - INFO-"
        If Not IsPostBack Then
            Dim tempPeriod As String = GetStrData("select max(cast(replace(periodacctg,'-','') as int)) from ql_crdmtr Where closingdate<>'01/01/1900'")
            Session("LastClosingPeriod") = tempPeriod

            If (Session("LastClosingPeriod") = "") Then
                Session("LastClosingPeriod") = GetPeriodAcctg(GetServerTime)
                Session("ClosingPeriod") = GetPeriodAcctg(GetServerTime)
                lblInfo.Text = "It's first Closing Stock <br>Are you sure to closing period " & Session("LastClosingPeriod") & " ?"
            Else
                Dim nextPeriod As String = Session("LastClosingPeriod")

                Dim ss As String = nextPeriod.Substring(4)
                If nextPeriod.Substring(4) = "12" Then
                    nextPeriod = CInt(nextPeriod.Substring(0, 4)) + 1 & "01"
                Else
                    nextPeriod = CInt(nextPeriod.Substring(0, 4)) & Format((CInt(nextPeriod.Substring(4)) + 1), "00")
                End If

                Dim sLastDayToClose As Date = New Date(CInt(Left(nextPeriod, 4)), CInt(Right(nextPeriod, 2)), Date.DaysInMonth(CInt(Left(nextPeriod, 4)), CInt(Right(nextPeriod, 2))))
                If GetServerTime() < DateAdd(DateInterval.Day, 1, sLastDayToClose) Then
                    lblInfo1.Text = "Closing untuk periode " & nextPeriod & " hanya bisa dilakukan setelah tanggal " & Format(sLastDayToClose, "dd/MM/yyyy") & "</BR>(Server Hari ini = " & Format(GetServerTime(), "dd/MM/yyyy") & ")"
                    MainPanel2.Visible = True : btnHiddenInfo1.Visible = True
                    btnHiddenInfo1.Visible = True
                    MPEInfo1.Show()
                    Exit Sub
                Else
                    lblInfo.Text = "Closing terakhir adalah periode " & Session("LastClosingPeriod") & ", <br> Sekarang anda akan melakukan proses closing pada periode " & nextPeriod & ""
                    Session("ClosingPeriod") = nextPeriod
                    MainPanel1.Visible = True : btnHiddenInfo.Visible = True
                    MPEInfo.Show()
                End If
            End If
        End If

    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        MainPanel1.Visible = False
        btnHiddenInfo.Visible = False
        MPEInfo.Hide()

        gvTransaksi.DataSource = Nothing
        gvTransaksi.DataBind()
        sSql = "select gencode,gendesc,genother1 from ql_mstgen Where gengroup='cabang'"
        Dim cabang As DataTable = ckoneksi.ambiltabel(sSql, "Cabang")
        Dim sWhere As String = "" : Dim sWhere1 As String = ""
        Dim OidItem As String = ""

        'If Not Session("Cabang") Is Nothing Then
        For R1 As Integer = 0 To cabang.Rows.Count - 1
            OidItem &= "'" & cabang.Rows(R1)("gencode").ToString & "',"
        Next

        If OidItem <> "" Then
            sWhere &= " Where branch IN (" & Left(OidItem, OidItem.Length - 1) & ")"
        End If

        'End If
        sSql = "select * from ( " & _
        " Select upduser,trnsjbelino urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=branch_code) cabang,'Purchase DO' tipe,1 no,trnsjbelistatus Status,branch_code branch from QL_trnsjbelimst Where trnsjbelistatus in ('In Process','In Approval') and cast(YEAR(trnsjbelidate) as varchar)+'-'+cast(left(convert(char(10),trnsjbelidate ,101),2) as varchar) ='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' " & _
        " UNION " & _
        "Select upduser,Convert(CHAR(10),resfield1) urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=branch_code) cabang,'Stock Adjustment' tipe,1 no,stockadjstatus Status,branch_code branch from ql_trnstockadj Where stockadjstatus in ('In Process','In Approval') and periodacctg ='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "'" & _
        " UNION " & _
        " select upduser,trnsjjualno urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=branch_code) cabang,'Sales DO' tipe,2 no,trnsjjualstatus status,branch_code branch from ql_trnsjjualmst Where trnsjjualstatus in ('In Process','In Approval') and cast(YEAR(trnsjjualdate) as varchar) +'-'+ cast(left(convert(char(10),trnsjjualdate ,101),2) as varchar)='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' " & _
        " UNION " & _
        " select upduser,transferno urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=frommtrbranch) cabang,'Transfer Warehouse' tipe,4 no,status,frommtrbranch branch from QL_trntrfmtrmst Where status in ('In Process','In Approval') and cast(YEAR(trfmtrdate)as varchar) +'-'+ cast(left(convert(char(10),trfmtrdate,101),2) as varchar) ='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' " & _
        " /*UNION " & _
        " Select finalapprovaluser upduser,transferno urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=tomtrbranch) cabang,'TW ke cabang '+(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=tomtrbranch)+'' tipe,4 no,'Belum TC' status,tomtrbranch branch from QL_trntrfmtrmst Where status in ('Approved') and cast(YEAR(trfmtrdate)as varchar) +'-'+ cast(left(convert(char(10),trfmtrdate,101),2) as varchar) ='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' And transferno NOT IN (Select DISTINCT trnTrfToReturNo From ql_InTransfermst tc Where tc.trnTrfToReturNo=transferno)*/" & _
        " UNION " & _
        " select upduser,transformno urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=branch_code) cabang,'Transform Item' tipe,5 no,status, branch_code branch from QL_TransformItemMst Where status='In Process' and cast(YEAR(transformdate) as varchar) +'-'+ cast(left(convert(char(10),transformdate,101),2) as varchar)='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' " & _
        "UNION " & _
        " select upduser,trnbelireturno urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=branch_code) cabang,'Purchase Return' tipe,6 no,trnbelistatus status,branch_code branch from QL_trnbeliReturmst Where trnbelistatus in ('In Process','In Approval') and periodacctg='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' " & _
        " UNION " & _
        " select upduser,trnjualreturno urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=branch_code) cabang,'Sales Return' tipe,7 no,trnjualstatus status,branch_code branch from QL_trnjualreturmst Where trnjualstatus ='In Process' and periodacctg='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' " & _
        " UNION " & _
        " select upduser,intransferno urut,(Select gendesc From ql_mstgen g Where g.gengroup='CABANG' And g.gencode=tobranch) cabang,'Transfer Confirm' tipe,8 no,status,tobranch branch from ql_InTransfermst Where status in ('In Process','In Approval') And cast(YEAR(trntrfdate)as varchar) +'-'+ cast(left(convert(char(10),trntrfdate,101),2) as varchar) ='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' ) As dt " & sWhere & ""

        Session("TblTransaction") = ckoneksi.ambiltabel(sSql, "QL_trnsjbelimst")
        Dim dtBPM As DataTable
        dtBPM = ckoneksi.ambiltabel(sSql, "QL_trnsjbelimst")
        Session("TblTransaction") = dtBPM
        gvTransaksi.DataSource = dtBPM
        gvTransaksi.DataBind()

        'Looping yg ada transaksi
        Dim ada As Int16 = dtBPM.Rows.Count
        'Dim dvtransaksi As DataTable = Session("TblTransaction")
        'For c1 As Int16 = 0 To dvtransaksi.Rows.Count - 1
        '    If dvtransaksi.Rows(c1).Item("total") > 0 Then
        '        'gvTransaksi.Rows(c1).BackColor = Drawing.Color.SkyBlue
        '        ada = ada + 1
        '    End If
        'Next
        If ada > 0 Then
            Label2.Text = Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4)
            pnlWarning.Visible = True : pnlWarning.Visible = True
            btnHiddenWarning.Visible = True
            MPEWarning.Show()
            Exit Sub
        End If

        If CheckDataExists("select count(-1) from QL_crdmtr Where periodacctg = '" & Session("LastClosingPeriod") & "' And closingdate='01/01/1900'") And Session("LastClosingPeriod") <> GetPeriodAcctg(Format(GetServerTime(), "MM/dd/yyyy")) Then

            lblInfo1.Text = "Period " & Session("LastClosingPeriod") & " wasn't closing"
            MainPanel2.Visible = True : btnHiddenInfo1.Visible = True
            MPEInfo1.Show()
        Else
            If CheckDataExists("select count(-1) from QL_crdmtr Where periodacctg = '" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' and closingdate<>'01/01/1900'") Then

                lblInfo1.Text = "Data Sudah diclosing..!!"
                MainPanel2.Visible = True : btnHiddenInfo1.Visible = True
                MPEInfo1.Show()
            Else
                Dim newPeriod As String = Session("ClosingPeriod")
                If newPeriod.Substring(4, 2) = "12" Then
                    newPeriod = CInt(newPeriod.Substring(0, 4)) + 1 & "-01"
                Else
                    'Dim bulanS As String = Format((CInt(newPeriod.Substring(4, 2))), "00")
                    'Dim TahunSk As String = CInt(newPeriod.Substring(0, 4))
                    newPeriod = CInt(newPeriod.Substring(0, 4)) & "-" & Format((CInt(newPeriod.Substring(4, 2)) + 1), "00")
                End If

                If Session("ClosingPeriod") > Format(GetServerTime(), "yyyyMM") Then 'periode maksimal closing hanya pd bulan ini
                    lblInfo1.Text = "Maximal closing is " & CInt(Format(GetServerTime(), "yyyyMM"))
                    MainPanel2.Visible = True
                    btnHiddenInfo1.Visible = True
                    MPEInfo1.Show()
                    Exit Sub
                End If

                'Dim ters As String = Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4, 2) & "'  "

                If Session("isclosing") Is Nothing Then
                    Session("isclosing") = "yes"
                    Dim crdmtroid As Int64 = GenerateID("ql_crdmtr", cmpcode)
                    Dim conmatoid As Int64 = GenerateID("ql_conmtr", cmpcode)
                    Dim PeriodClose As String = Mid(GetPeriodAcctg(Format(GetServerTime(), "MM/dd/yyyy")), 6, 2)

                    If PeriodClose = "01" Then
                        PeriodClose = Mid(GetPeriodAcctg(Format(GetServerTime(), "MM/dd/yyyy")), 1, 4) - 1 & "-" & "12"
                    Else
                        PeriodClose = Mid(GetPeriodAcctg(Format(GetServerTime(), "MM/dd/yyyy")), 1, 4) & "-" & Format((CInt(PeriodClose)), "00")
                    End If
                    sSql = "SELECT cmpcode,0 crdmatoid,Current_timestamp lasttrans,'' lasttranstype,periodacctg,mtrlocoid,refoid,SUM(qtyIn) qtyin,SUM(qtyOut)qtyout,0 qtyadjin,0 qtyadjout,SUM(qtyIn)-SUM(qtyOut) saldoawal,0 qtyBooking,SUM(qtyIn)-SUM(qtyOut) saldoakhir,'' upduser,Current_timestamp updtime, 'QL_MSTITEM' refname,'' createuser,Current_timestamp createdate,Current_timestamp closingdate,''closeuser,branch_code,Isnull((Select HPP from QL_mstitem i Where itemoid=con.refoid),0.00) hpp,Isnull((Select personoid from QL_mstitem i Where itemoid=con.refoid),0) personoid,Isnull((SUM(qtyIn)-SUM(qtyOut))*(Select HPP from QL_mstitem i Where itemoid=con.refoid),0.00) Amount FROM QL_conmtr con Where periodacctg='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Session("ClosingPeriod").Substring(4) & "' and cmpcode='MSC' And branch_code = branch_code Group BY periodacctg,refoid,mtrlocoid,branch_code,cmpcode Order By branch_code,refoid"

                    Dim xAdap As New SqlDataAdapter(sSql, conn)
                    Dim oTable As New DataTable
                    xAdap.Fill(oTable)

                    'If oTable.Rows.Count > 0 Then
                    '    Exit Sub
                    'End If

                    Dim objTrans As SqlClient.SqlTransaction
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    objTrans = conn.BeginTransaction
                    xCmd.Transaction = objTrans

                    Try
                        sSql = "update ql_crdmtr set branch_code = branch_code,closingdate=current_timestamp , closeuser='" & Session("userid") & "' Where closingdate='1900-01-01 00:00:00' And periodacctg='" & Session("ClosingPeriod").Substring(0, 4) & "-" & Format((CInt(Session("ClosingPeriod").Substring(4, 2))), "00") & "' And cmpcode='" & cmpcode & "' and branch_code = branch_code "
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        For c1 As Integer = 0 To oTable.Rows.Count - 1
                            ''======================================
                            sSql = "INSERT INTO QL_crdmtr (cmpcode,crdmatoid,lasttrans,lasttranstype,periodacctg,mtrlocoid, branch_code,refoid,qtyin,qtyout,qtyadjin,qtyadjout,saldoawal,qtyBooking,saldoakhir,upduser,updtime,refname,createuser,createdate,closingdate,closeuser) VALUES" & _
                            " ('" & cmpcode & "'," & crdmtroid & ",current_timestamp,'CLOSING','" & newPeriod & "'," & oTable.Rows(c1).Item("mtrlocoid") & ",'" & oTable.Rows(c1).Item("branch_code") & "'," & oTable.Rows(c1).Item("refoid") & ",0,0,0,0," & oTable.Rows(c1).Item("saldoakhir") & ",0," & oTable.Rows(c1).Item("saldoakhir") & ",'" & Session("UserID") & "',Current_timestamp,'" & oTable.Rows(c1).Item("refname") & "','" & Session("userid") & "', current_timestamp,'01/01/1900','')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            sSql = "INSERT INTO QL_conmtr (cmpcode,conmtroid,type,trndate,periodacctg,FormAction,formoid ,refoid,refname,mtrlocoid, branch_code, reason,upduser,updtime,typeMin,personoid, qtyin, amount,hpp) VALUES " & _
                            "('" & cmpcode & "'," & conmatoid & ",'CLS',current_timestamp,'" & newPeriod & "','CLOSING'," & crdmtroid & "," & oTable.Rows(c1).Item("refoid") & ",'" & oTable.Rows(c1).Item("refname").trim & "'," & Integer.Parse(oTable.Rows(c1).Item("mtrlocoid")) & ",'" & oTable.Rows(c1).Item("branch_code") & "','CLOSING','" & Session("userid") & "',current_timestamp,1," & Integer.Parse(oTable.Rows(c1).Item("personoid")) & "," & ToDouble(oTable.Rows(c1).Item("saldoakhir")) & "," & ToDouble(oTable.Rows(c1).Item("Amount")) & "," & ToDouble(oTable.Rows(c1).Item("hpp")) & ") "

                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            crdmtroid += 1 : conmatoid += 1
                        Next

                        sSql = "update  QL_mstoid set lastoid=" & crdmtroid & " Where tablename ='QL_crdmtr' and cmpcode = '" & cmpcode & "' "
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "update  QL_mstoid set lastoid=" & conmatoid & " Where tablename ='QL_conmtr' And cmpcode = '" & cmpcode & "' "
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        objTrans.Commit() : lblInfo1.Text = "Closing Success"

                        conn.Close()
                    Catch ex As Exception
                        lblInfo1.Text = ex.ToString
                        objTrans.Rollback()
                        conn.Close()
                    End Try
                    MainPanel2.Visible = True : btnHiddenInfo1.Visible = True
                    MPEInfo1.Show()
                End If
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~\other\menu.aspx")
    End Sub

    Protected Sub OK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles OK.Click
        Response.Redirect("~\other\menu.aspx")
    End Sub
    
    Protected Sub ibClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClose.Click
        Response.Redirect("~\other\menu.aspx")
    End Sub

    Protected Sub gvTransaksi_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTransaksi.PageIndexChanging
        gvTransaksi.PageIndex = e.NewPageIndex
        gvTransaksi.DataSource = Session("TblTransaction")
        gvTransaksi.DataBind()
        MPEWarning.Show()
    End Sub
#End Region
End Class
