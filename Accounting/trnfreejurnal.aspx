<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnfreejurnal.aspx.vb" Inherits="Accounting_trnfreejurnal" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Text=".: Memorial Journal" CssClass="Title" Font-Size="Large"></asp:Label></th>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel3" runat="server">
                                <contenttemplate>
<DIV style="WIDTH: 100%; TEXT-ALIGN: left"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 67px" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Date" __designer:wfdid="w31" AutoPostBack="True" Checked="True" OnCheckedChanged="cbPeriode_CheckedChanged"></asp:CheckBox>&nbsp;&nbsp; </TD><TD style="WIDTH: 6px" align=left>:</TD><TD align=left><asp:TextBox id="periode1" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w32"></asp:TextBox>&nbsp;<asp:ImageButton id="btnperiode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton>&nbsp;to <asp:TextBox id="periode2" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w34"></asp:TextBox>&nbsp;<asp:ImageButton id="btnperiode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton>&nbsp;&nbsp; <asp:Label id="Label9" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w36"></asp:Label></TD></TR><TR><TD style="WIDTH: 67px" align=left><asp:Label id="Label12" runat="server" Text="Filter" __designer:wfdid="w37"></asp:Label></TD><TD style="WIDTH: 6px" align=left>:</TD><TD align=left><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w38" AutoPostBack="True"><asp:ListItem Value="(SELECT TOP 1 d.noref FROM QL_trngldtl d WHERE d.cmpcode=ms.cmpcode AND d.glmstoid=ms.glmstoid)">Ref No</asp:ListItem>
<asp:ListItem Value="ms.glnote">Note</asp:ListItem>
<asp:ListItem Value="ms.periodacctg">Period Acctg</asp:ListItem>
<asp:ListItem Value="ol.outletname">Outlet</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w39"></asp:TextBox></TD></TR><TR><TD align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w40" AutoPostBack="True"></asp:CheckBox></TD><TD style="WIDTH: 6px" align=left>:</TD><TD align=left colSpan=1><TABLE cellSpacing=0 border=0><TBODY><TR><TD style="HEIGHT: 23px" align=left><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" __designer:wfdid="w41" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="BtnSearch" onclick="BtnSearch_Click" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w42"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w43"></asp:ImageButton>&nbsp; </TD><TD style="HEIGHT: 23px" align=left></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="WIDTH: 67px" align=left><asp:Label id="Label010" runat="server" Width="87px" Text="Posting Option" __designer:wfdid="w44"></asp:Label></TD><TD style="WIDTH: 6px" align=left>:</TD><TD align=left colSpan=1><asp:ImageButton id="postedall" onclick="postedall_Click" runat="server" ImageUrl="~/Images/selectall.png" __designer:wfdid="w45"></asp:ImageButton>&nbsp;<asp:ImageButton id="postednotallx" onclick="postednotallx_Click" runat="server" ImageUrl="~/Images/selectnone.png" __designer:wfdid="w46"></asp:ImageButton>&nbsp;<asp:ImageButton id="postedx" onclick="postedx_Click" runat="server" ImageUrl="~/Images/postselect.png" __designer:wfdid="w47"></asp:ImageButton>&nbsp; <asp:Label id="poststatx" runat="server" Text="NORMAL" __designer:wfdid="w48" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w49"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:Image> Please wait ... 
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w51" TargetControlID="periode1" Format="dd/MM/yyyy" PopupButtonID="btnperiode1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w52" TargetControlID="periode2" Format="dd/MM/yyyy" PopupButtonID="btnperiode2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w53" Mask="99/99/9999" MaskType="Date" TargetControlID="periode1"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w54" Mask="99/99/9999" MaskType="Date" TargetControlID="periode2"></ajaxToolkit:MaskedEditExtender>&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD align=left colSpan=3><asp:GridView id="GvJurnal" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w55" OnSelectedIndexChanged="GvJurnal_SelectedIndexChanged1" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="glmstoid,cmpcode" GridLines="None">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Posting"><ItemTemplate>
<asp:CheckBox id="cbPosting" runat="server" Checked='<%# eval("selected") %>' Enabled="<%# GetEnabled() %>" ToolTip="<%# GetCheckedOid() %>"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="75px"></ItemStyle>
</asp:TemplateField>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="glmstoid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="noref" HeaderText="Ref No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outlet" HeaderText="Outlet">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gldate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="periodacctg" HeaderText="Periode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glflag" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="75px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" CssClass="gvfooter" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" CssClass="gvrow" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" CssClass="Important" Text="Click button Find or View All to view data" __designer:wfdid="w56"></asp:Label></TD></TR></TBODY></TABLE></DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span><span style="font-size: 9pt"><strong> List Memorial Journal :.</strong></span></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 954px"><TBODY><TR><TD colSpan=3 rowSpan=3><asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" __designer:wfdid="w127"><asp:View id="View1" runat="server" __designer:wfdid="w128"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w92"></asp:Label></TD><TD align=left></TD><TD align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w93" Visible="False"></asp:Label><asp:Label id="glmstoid" runat="server" __designer:wfdid="w94" Visible="False"></asp:Label> <asp:Label id="txtGLID" runat="server" __designer:wfdid="w94" Visible="False"></asp:Label> <asp:Label id="CutOffDateJurnal" runat="server" __designer:wfdid="w94" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label13" runat="server" Text="Cabang" __designer:wfdid="w129"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="outlet" runat="server" Width="175px" CssClass="inpText" __designer:wfdid="w130" OnSelectedIndexChanged="outlet_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD align=left>Date</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="glDate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w131" AutoPostBack="True" OnTextChanged="glDate_TextChanged"></asp:TextBox> <asp:ImageButton id="ibDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w132"></asp:ImageButton> <asp:Label id="Label10" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w133"></asp:Label> <asp:LinkButton id="lbkDetil" onclick="lbkDetil_Click" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w134" Visible="False">List</asp:LinkButton> <asp:Label id="Label18" runat="server" Font-Size="Small" ForeColor="#585858" Text="|" __designer:wfdid="w135" Visible="False"></asp:Label> <asp:Label id="tempoid" runat="server" Text="0" __designer:wfdid="w136" Visible="False"></asp:Label></TD></TR><TR><TD align=left>Ref. No <asp:Label id="Label4" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w137"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="noref" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w138" ReadOnly="True" MaxLength="20"></asp:TextBox></TD><TD align=left>Status</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="txtStatus" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w90" AutoPostBack="True" OnTextChanged="glDate_TextChanged" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left>Total Debet</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="txtDebet" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w89" AutoPostBack="True" OnTextChanged="glDate_TextChanged" Enabled="False"></asp:TextBox></TD><TD align=left>Total Credit</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="txtCredit" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w91" AutoPostBack="True" OnTextChanged="glDate_TextChanged" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left>Header Note</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="txtNote" runat="server" Width="296px" CssClass="inpText" __designer:wfdid="w88" AutoPostBack="True" OnTextChanged="glDate_TextChanged"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w141" InputDirection="RightToLeft" Mask="99/99/9999" MaskType="Date" TargetControlID="glDate"></ajaxToolkit:MaskedEditExtender> </TD><TD align=left></TD><TD align=left><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w142" TargetControlID="glDate" Format="dd/MM/yyyy" PopupButtonID="ibDate"></ajaxToolkit:CalendarExtender> </TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Font-Bold="True" Text="Memorial Journal Detail" __designer:wfdid="w143" Font-Underline="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w96"></asp:Label> <asp:Label id="gldtloid" runat="server" __designer:wfdid="w166" Visible="False"></asp:Label> <asp:Label id="glseq" runat="server" __designer:wfdid="w146" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label1" runat="server" Text="Account Group" __designer:wfdid="w99"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="acctggroup" runat="server" Width="305px" CssClass="inpText" __designer:wfdid="w102" OnSelectedIndexChanged="acctggroup_SelectedIndexChanged" AutoPostBack="True">
            </asp:DropDownList></TD><TD align=left><asp:Label id="Label99" runat="server" Text="Flag" __designer:wfdid="w105"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="gldbcr" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w107">
                <asp:ListItem Value="D">DEBIT</asp:ListItem>
                <asp:ListItem Value="C">CREDIT</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label11" runat="server" Text="Account" __designer:wfdid="w100" ToolTip="All Account Depend Acctg Group"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="acctgoid" runat="server" Width="305px" CssClass="inpText" __designer:wfdid="w103">
            </asp:DropDownList></TD><TD align=left><asp:Label id="Label5" runat="server" Text="Amount" __designer:wfdid="w106"></asp:Label> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w106"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="glamt" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w108"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="Detail Note" __designer:wfdid="w101"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="glnote" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w104" MaxLength="100"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD><TD align=left><ajaxToolkit:FilteredTextBoxExtender id="ftbAmount" runat="server" __designer:wfdid="w109" TargetControlID="glamt" ValidChars="1234567890,.">
                </ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left>&nbsp; </TD><TD align=left></TD><TD style="WIDTH: 75px" align=left></TD><TD align=left></TD><TD style="WIDTH: 205px" align=left></TD><TD align=left colSpan=2>&nbsp;</TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w112"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w113"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" align=left></TD></TR><TR><TD align=left><asp:GridView id="gvbaru" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w3" OnSelectedIndexChanged="gvbaru_SelectedIndexChanged" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="glseq" GridLines="None" PageSize="5" OnRowDataBound="gvbaru_RowDataBound">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Account No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glamtdb" HeaderText="Debit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glamtcr" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 10px" align=left></TD></TR><TR><TD align=left><asp:Label id="created" runat="server" Font-Bold="False" __designer:wfdid="w185"></asp:Label>&nbsp;; <asp:Label id="update" runat="server" Font-Bold="False" __designer:wfdid="w186"></asp:Label></TD></TR><TR><TD align=left></TD></TR><TR><TD align=left><asp:ImageButton id="SaveBtn" onclick="SaveBtn_Click" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w187"></asp:ImageButton>&nbsp;<asp:ImageButton id="CancelBtn" onclick="CancelBtn_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w188"></asp:ImageButton>&nbsp;<asp:ImageButton id="DelBtn" onclick="DelBtn_Click" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w189"></asp:ImageButton>&nbsp;<asp:ImageButton id="PostBtn" onclick="PostBtn_Click" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w190"></asp:ImageButton></TD></TR><TR><TD align=left></TD></TR><TR><TD align=center><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w191"><ProgressTemplate>
<asp:Image id="ImageWait" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:Image> Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView></TD></TR><TR></TR><TR></TR></TBODY></TABLE><TABLE style="FONT-SIZE: x-small; WIDTH: 100%; COLOR: #585858" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 13px" align=left>&nbsp;</TD></TR><TR><TD align=left>&nbsp;&nbsp; </TD></TR><TR><TD align=left><TABLE width="100%"><TBODY><TR><TD align=left colSpan=2>&nbsp;&nbsp;&nbsp; </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="SaveBtn"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image39" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            &nbsp;<span style="font-size: 9pt"><strong>Form Memorial Journal :.</strong><span style="font-size: 8pt">&nbsp;</span></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblCaption">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

