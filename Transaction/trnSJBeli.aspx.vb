﻿'==================================
'Last Update Programmer by Shutup_M
'==================================
Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports System.Drawing
Imports System.Drawing.Image
Imports System.Drawing.Bitmap

Partial Class Transaction_trnsjbeli
    Inherits System.Web.UI.Page

#Region "Variable"
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public toleranceSDO As Double = ConfigurationSettings.AppSettings("tolerance")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim commx As SqlCommand
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dtStaticItem As DataTable
    'Dim cFunction As New ClassFunction
    Dim mySqlConn As New SqlConnection(ConnStr)
    Dim mysql As String
    Dim reportSJ As New ReportDocument
    Dim cKoneksi As New Koneksi
    Dim cfunction As New ClassFunction
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String
    Dim objDataAdapter As New OleDbDataAdapter()
    Dim TBCekSN As New DataTable
    Dim TBCekItemcode As DataTable
    Dim dtsnExcelsama As New DataTable
    Dim objDataSet As New DataSet
    Private cKon As New Koneksi
    Dim KetSN As String
    Dim masuk As Integer
    'variable Barcode
    Private ActualWidth As Integer
    Private ActualHeight As Integer
    Private TipeBarcode As Integer
    Dim CodeBarcode As MW6.Barcode.BarcodeNet
    Dim appv As DataTable
    Dim LebarBarcode As Integer
    Dim TinggiBarcode As Integer
    Dim jp As Image

    Dim HeightBar As Double
    Dim WidhtBar As Double
    Dim HeightImage As Integer
    Dim widhtImage As Integer
    Dim textBd As String
    Dim cekkondimsg As String
    Dim kumpulanSN, TampungSnSama As String
    Dim jumlahSnSama As Integer = 0
    Dim has_sn As String

    Dim SaveLocation As String
    Dim ConnStrExe As String
    Dim connexc As OleDbConnection
    Dim adap As OleDbDataAdapter
    Dim commecel As OleDbCommand
#End Region

#Region "Function"
    Function copyimage(ByVal source As Bitmap, ByVal section As Rectangle) As Bitmap
        Dim bmp As New Bitmap(section.Width, section.Height)

        Dim g As Graphics = Graphics.FromImage(bmp)
        g.DrawImage(source, 0, 0, section, GraphicsUnit.Pixel)
        Return bmp
    End Function

    Private Function CheckQtyToPO() As Boolean ' Return False if Greater than PO
        Dim state As Boolean = True
        Dim dtCek As DataTable : dtCek = Session("TblDtl")
        If Not (dtCek Is Nothing) Then
            For C1 As Integer = 0 To dtCek.Rows.Count - 1
                Dim dQTY As Double = ToDouble(dtCek.Rows(C1)("referedqty").ToString) - ToDouble(dtCek.Rows(C1)("receivedqty").ToString)
                'Dim dRange As Double = (ToDouble(dtCek.Rows(C1)("tolerance").ToString) / 100) * dQTY
                Dim dReal As Double = 0
                If dtCek.Rows(C1)("trnsjbelidtlunitoid").ToString = dtCek.Rows(C1)("trnsjbelidtlunitoid").ToString Then
                    dReal = ToDouble(dtCek.Compute("SUM(trnsjbelidtlqty)", "matoid=" & dtCek.Rows(C1)("matoid").ToString).ToString)
                Else
                    dReal = ToDouble(dtCek.Compute("SUM(qty2)", "matoid=" & dtCek.Rows(C1)("matoid").ToString).ToString)
                End If
                'If dReal > (dQTY + dRange) Then
                If dReal > (dQTY) And dReal <= (dQTY + toleranceSDO) Then
                    state = False : Exit For
                End If
            Next
        End If
        Return state
    End Function

    Protected Function ExcelConnection(ByVal tablename As String) As OleDbCommand
        Dim sss As String = Server.MapPath("~/FileExcel/ExcelImport.xls")
        ' Connect to the Excel Spreadsheet
        'old
        Dim xConnStr As String = "Provider=Microsoft.JET.OLEDB.4.0;" & _
                    "Data Source='" & Server.MapPath("~/FileExcel/ExcelImport.xls") & "';" & _
                    "Extended Properties=Excel 8.0;Persist Security Info=False"
        ' ''new
        'Dim xConnStr As String = "Provider=Microsoft.ACE.OLEDB.12.0;" & _
        '        "Data Source=" & Server.MapPath("~/FileExcel/ExcelImport.xls") & ";" & _
        '        "Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;'"

        ' create your excel connection object using the connection string
        Dim objXConn As New OleDbConnection(xConnStr)
        objXConn.Open()

        ' use a SQL Select command to retrieve the data from the Excel Spreadsheet
        ' the "table name" is the name of the worksheet within the spreadsheet
        ' in this case, the worksheet name is "Members" and is coded as: [Members$]
        Dim objCommand As New OleDbCommand("SELECT * FROM [" & tablename & "$]", objXConn)

        objXConn.Close()
        Return objCommand

    End Function

    Private Function checkApprovaldata(ByVal sTableName As String, ByVal sEvent As String, ByVal sOid As String, _
        ByVal sRequestUser As String, ByVal sStatus As String) As DataTable
        Dim ssql As String = "SELECT CMPCODE,APPROVALOID,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME," & _
             "OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval WHERE CmpCode='" & CompnyCode & _
             "' and RequestUser LIKE '" & sRequestUser & "%' and STATUSREQUEST = '" & sStatus & "' " & _
             " AND TABLENAME='" & sTableName & "' and OID=" & sOid & " and EVENT='" & sEvent & "' and branch_code = '" & LblCabang.Text & "' ORDER BY REQUESTDATE DESC"
        Return cKoneksi.ambiltabel(ssql, "QL_APPROVAL")
    End Function

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("trnsjbelidtlseq", Type.GetType("System.Int32"))
        dt.Columns.Add("trnsjbelidtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnsjbelimstoid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnpodtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("matloc", Type.GetType("System.Int32"))
        dt.Columns.Add("matoid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnsjbelidtlqty", Type.GetType("System.Decimal"))
        dt.Columns.Add("trnsjbelidtlunitoid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnsjbelidtlnote", Type.GetType("System.String"))
        dt.Columns.Add("item", Type.GetType("System.String"))
        dt.Columns.Add("tolerance", Type.GetType("System.Decimal"))
        dt.Columns.Add("referedqty", Type.GetType("System.Decimal"))
        dt.Columns.Add("receivedqty", Type.GetType("System.Decimal"))
        dt.Columns.Add("setascomplete", Type.GetType("System.Boolean"))
        dt.Columns.Add("location", Type.GetType("System.String"))
        dt.Columns.Add("unit", Type.GetType("System.String"))
        dt.Columns.Add("podtlunit", Type.GetType("System.Int32"))
        dt.Columns.Add("pounit", Type.GetType("System.String"))
        dt.Columns.Add("qty2", Type.GetType("System.Decimal"))
        dt.Columns.Add("unit2", Type.GetType("System.Int32"))
        dt.Columns.Add("unit2desc", Type.GetType("System.String"))
        dt.Columns.Add("refname", Type.GetType("System.String"))
        'dt.Columns.Add("flagsn", Type.GetType("System.String"))
        dt.Columns.Add("SNList", Type.GetType("System.String"))
        dt.Columns.Add("KodeBR", Type.GetType("System.String"))
        'dt.Columns.Add("harga", Type.GetType("System.Decimal"))
        dt.Columns.Add("bonus", Type.GetType("System.String"))
        dt.Columns.Add("hargaekspedisi", Type.GetType("System.String"))
        dt.Columns.Add("ekspedisi", Type.GetType("System.String"))
        dt.Columns.Add("typeberat", Type.GetType("System.String"))
        dt.Columns.Add("berat", Type.GetType("System.String"))
        Return dt
    End Function

    Private Function checkOtherShipment(ByVal iRefoid As String, _
    ByVal iMatOid As Integer, ByVal cmpcode As String) As Boolean
        sSql = "SELECT COUNT(-1) FROM ql_trnsjbelidtl jbd INNER JOIN ql_trnsjbelimst jb ON jbd.trnsjbelioid=jb.trnsjbelioid  " & _
            "AND jb.cmpcode=jbd.cmpcode AND jb.trnsjbelistatus not in ('Approved','INVOICED') WHERE jbd.refoid=" & iMatOid & " AND jbd.cmpcode='" & cmpcode & "' " & _
            "AND jb.trnbelipono= '" & iRefoid & "' "
        If I_U.Text <> "New" Then
            sSql &= " AND jb.trnsjbelioid<>" & Session("oid")
        End If
        Return cKoneksi.ambilscalar(sSql) > 0
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND ISNULL(KACAB,'')='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKoneksi.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function formatNumber(ByVal num As Integer) As String
        Dim strNum As String = num.ToString()
        Dim retVal As String = ""
        For y As Integer = 0 To (4 - strNum.Length) - 1
            retVal &= "0"
        Next
        retVal &= strNum
        Return retVal
    End Function

    Public Function getFileName(ByVal backID As String) As String
        Dim fname As String = ""
        Dim ID As String = Trim(backID)
        fname = "Purchase_" & ID & ".xls"
        Return fname
    End Function

    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".xls"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function 
#End Region

#Region "Procedure" 
    Public Sub uploadFileGambar(ByVal proses As String, ByVal fname As String)
        Dim savePath As String = Server.MapPath("~/FileExcel/")

        If FileExel.HasFile Then
            If checkTypeFile(FileExel.FileName) Then
                savePath &= fname
                Try
                    FileExel.PostedFile.SaveAs(savePath)
                    Label19.Text = savePath
                    Session("LinkSaveFile") = Label19.Text
                Catch ex As Exception
                End Try
            Else
                showMessage("Tidak dapat diupload. Format file harus .xls!", CompnyName & " - WARNING", 2)
            End If
        Else
            'showMessage("Tidak Ada File  .xls! Yang Di Pilih", CompnyName & " - WARNING", 2)
        End If
    End Sub

    Private Sub fCabangInit()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLfCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLfCabang, sSql)
            Else
                FillDDL(DDLfCabang, sSql)
                DDLfCabang.Items.Add(New ListItem("ALL", "ALL"))
                DDLfCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLfCabang, sSql)
            DDLfCabang.Items.Add(New ListItem("ALL", "ALL"))
            DDLfCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitCabangNya()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_branch, sSql)
            Else
                FillDDL(dd_branch, sSql)
                'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
                'ddlFromcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dd_branch, sSql)
            'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
            'ddlFromcabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub CheckUnit()
        If trnsjbelidtlunit.Text = unit2.Text Then
            qty2.CssClass = "inpTextDisabled" : qty2.ReadOnly = True
        Else
            qty2.CssClass = "inpText" : qty2.ReadOnly = False
        End If
    End Sub

    Private Sub SetControlApproval(ByVal bState As Boolean)
        lblApprover.Visible = bState : ApprovalUserName.Visible = bState
        imbFindUserID.Visible = bState : imbClearUser.Visible = bState
        imbSendApproval.Visible = bState
        If Not bState Then
            approvaluser.Text = "" : ApprovalUserName.Text = ""
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintSJ(ByVal oid As String, ByVal no As String, ByVal sType As String)
        'untuk print
        Dim tipe As String = cKoneksi.ambilscalar("select trnsjbelino from ql_trnsjbelimst where trnsjbelioid=" & oid)
        reportSJ.Load(Server.MapPath("~/report/PrintOutPDO.rpt"))
        reportSJ.SetParameterValue("oid", oid)
        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, reportSJ)
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        If sType = "pdf" Or sType = "" Then
            reportSJ.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        ElseIf sType = "excel" Then
            reportSJ.ExportToHttpResponse(ExportFormatType.Excel, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub bindDataForc()
        sSql = "SELECT sf.forcoid,sf.forcno,sf.forcdate,sf.custoid,s.custname from " & _
            "ql_trnforcmst sf,ql_mstcust s where sf.custoid=s.custoid and " & _
            "sf.cmpcode=s.cmpcode and sf.forcstatus='Approved' and sf.forcoid in " & _
            "(select DISTINCT sfd.forcmstoid from QL_trnforcdtl sfd where sfd.forcdtlstatus IS NULL " & _
            "OR sfd.forcdtlstatus NOT IN ('Completed')) AND sf.forcno LIKE '%" & orderno.Text & "%'"
        ClassFunction.FillGV(gvListSF, sSql, "ql_trnforcmst")
    End Sub

    Private Sub CheckIfNeedApproval() '(ByVal sTabelDtl As DataTable)

        If posting.Text <> "POST" Then
            SetControlApproval(Not CheckQtyToPO())
        End If
    End Sub

    Private Sub LocDLL()

        Dim sCb As String = ""
        'If Session("UserID").ToString <> "ADMIN" Then
        '    sCb &= "AND a.genother2 IN (Select genoid From QL_mstgen Where gengroup='CABANG' AND gencode='" & Session("branch_id").ToString & "')"
        'End If
        sSql = "SELECT a.genoid,ISNULL((SELECT gendesc FROM QL_mstgen g WHERE g.genoid=a.genother1 AND g.gengroup = 'WAREHOUSE'),'GUDANG PERJALANAN') +' - '+a.gendesc AS gendesc From QL_mstgen a WHERE a.gengroup = 'Location' and a.genother6='UMUM' AND gencode <> 'EXPEDISI' AND a.genother2 IN (Select genoid From QL_mstgen Where gengroup='CABANG' AND gencode='" & dd_branch.SelectedValue & "')"
        FillDDL(matLoc, sSql)
        If matLoc.Items.Count = 0 Then
            showMessage("Please create/fill Data General in group LOCATION!", CompnyName & " - Warning", 2)
            btnSave.Visible = False : btnDelete.Visible = False : btnCancel.Visible = False
        End If
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category 1
        sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('itemgroup') and cmpcode like '%" & CompnyCode & "%'"
        If FillDDL(DDLCat01, sSql) Then
            InitDDLCat2()
        Else
            DDLCat02.Items.Clear()
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('ITEMSUBGROUP') and cmpcode like '%" & CompnyCode & "%' and genother1 = '" & DDLCat01.SelectedValue & "'"
        If FillDDL(DDLCat02, sSql) Then
            InitDDLCat3()
        Else
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('ITEMTYPE') and cmpcode like '%" & CompnyCode & "%' and genother2 = '" & DDLCat02.SelectedValue & "'"

        If FillDDL(DDLCat03, sSql) Then
            InitDDLCat4()
        Else
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat4()
        'Fill DDL Category 3
        sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('ITEMMERK') and cmpcode like '%" & CompnyCode & "%' and genother3 = '" & DDLCat03.SelectedValue & "'"
        FillDDL(DDLCat04, sSql)
    End Sub

    Private Sub FillTextBox(ByVal ToCabang As String, ByVal iID As Int64)
        btnSave.Visible = True : btnDelete.Visible = True : btnPrint.Visible = True : imbSendApproval.Visible = True
        sSql = "SELECT DISTINCT a.trnsjbelioid,a.trnsjbelino,P.trnbelipodate,a.trnsjbelidate,P.trnsuppoid,P.trnbelimstoid,a.trnsjbelinote,a.trnsjbelistatus,a.upduser,a.updtime,s.suppname,a.trnsjbelitype,a.trnbelipono no,a.finalapprovaluser,ISNULL(usr.username,'') AS username,a.finalapprovaldatetime, p.link_File, d.mtrlocoid,a.branch_code,a.frombranch,a.createtime FROM QL_trnsjbelimst a INNER JOIN QL_trnsjbelidtl d ON d.trnsjbelioid = a.trnsjbelioid and a.branch_code=d.tobranch INNER JOIN QL_pomst P ON A.trnbelipono = P.trnbelipono /*and p.tocabang=a.branch_code*/ inner join QL_mstsupp s on a.cmpcode=s.cmpcode AND P.trnsuppoid=s.suppoid LEFT OUTER JOIN QL_mstprof usr ON usr.cmpcode=a.cmpcode AND usr.userid=a.finalapprovaluser Where a.cmpcode ='" & CompnyCode & "' AND a.trnsjbelioid ='" & iID & "' AND a.branch_code='" & ToCabang & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                InitCabangNya()
                dd_branch.SelectedValue = xreader.Item("branch_code").ToString
                oid.Text = Trim(xreader.Item("trnsjbelioid"))
                trnsjbelino.Text = Trim(xreader.Item("trnsjbelino"))
                trnsjbelidate.Text = Format(CDate(xreader.Item("trnbelipodate").ToString), "dd/MM/yyyy")
                trnsjreceivedate.Text = Format(CDate(xreader.Item("trnsjbelidate").ToString), "dd/MM/yyyy")
                trnsjbelitype.SelectedValue = Trim(xreader.Item("trnsjbelitype"))
                trnsuppoid.Text = Trim(xreader.Item("trnsuppoid"))
                trnbelimstoid.Text = Trim(xreader.Item("trnbelimstoid"))
                orderno.Text = Trim(xreader.Item("no"))
                notes.Text = Trim(xreader.Item("trnsjbelinote"))
                trnsjbelistatus.Text = Trim(xreader.Item("trnsjbelistatus"))
                updUser.Text = Trim(xreader.Item("upduser"))
                updTime.Text = Trim(xreader.Item("updtime"))
                suppname.Text = Trim(xreader.Item("suppname"))
                posting.Text = Trim(xreader.Item("trnsjbelistatus"))
                approvaluser.Text = Trim(xreader.Item("finalapprovaluser").ToString)
                ApprovalUserName.Text = Trim(xreader.Item("username").ToString)
                Label19.Text = xreader.Item("link_File").ToString
                LocDLL()
                matLoc.SelectedValue = xreader.Item("mtrlocoid")
                Label15.Text = Trim(xreader.Item("branch_code")).ToString
                aCabang.Text = Trim(xreader.Item("frombranch")).ToString
                createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss tt")

                If posting.Text.ToUpper = "INVOICED" Or posting.Text = "Approved" Or posting.Text = "In Approval" Then
                    btnSave.Visible = False : btnDelete.Visible = False
                    imbSendApproval.Visible = False : CheckBoxSN.Enabled = False
                End If
            End While
        End If
        xreader.Close() : conn.Close()

        sSql = "select DISTINCT d.trnsjbelidtloid,d.trnsjbelioid trnsjbelimstoid,d.unitoid trnsjbelidtlunitoid,d.qty trnsjbelidtlqty,d.trnsjbelidtlseq,d.mtrlocoid matloc,d.refoid matoid,d.trnsjbelinotedtl trnsjbelidtlnote,d.trnbelidtloid trnpodtloid,d.refname, CASE WHEN ISnull((select has_sn from QL_mstitem where itemoid = pod.itemoid), '0') = '0' THEN 'N' ELSE 'Y' END as SNList, (select itemcode from QL_mstitem where itemoid = pod.itemoid) as KodeBR, (select itemdesc from QL_mstitem where itemoid = pod.itemoid) as item, pod.trnbelidtlqty AS referedqty, isnull((select SUM(qty) from ql_trnsjbelidtl where pod.trnbelidtloid=trnbelidtloid AND trnsjbelioid in (select trnsjbelioid from ql_trnsjbelimst where trnsjbelistatus in ('Approved','POST','INVOICED')) AND pd.branch_code=branch_code),0) as receivedqty,'setascomplete'='false', gw.gendesc +' - ' +g.gendesc AS location ,g1.gendesc AS unit,pod.trnbelidtlunit podtlunit, g3.gendesc AS unit2desc, d.biayaExpedisi ekspedisi ,isnull(d.hargaekspedisi,0.00) hargaekspedisi, isnull(d.typeberat,2) typeberat, isnull(d.berat,0.00) berat,pod.bonus,pod.trnbelidtlqty FROM QL_trnsjbelidtl d INNER JOIN QL_trnsjbelimst p ON p.cmpcode=d.cmpcode AND p.trnsjbelioid=d.trnsjbelioid INNER JOIN QL_trnsjbelidtl pd ON pd.cmpcode=p.cmpcode AND pd.trnsjbelioid=p.trnsjbelioid INNER JOIN QL_podtl pod ON pod.cmpcode=p.cmpcode AND pod.trnbelimstoid=(select trnbelimstoid from ql_pomst where trnbelipono= p.trnbelipono) and pod.trnbelidtloid = d.trnbelidtloid AND pod.itemoid=d.refoid INNER JOIN QL_mstgen g ON g.cmpcode=d.cmpcode AND g.genoid=d.mtrlocoid AND g.gengroup='LOCATION' and g.genother6='UMUM' INNER JOIN QL_mstgen gw ON gw.genoid = g.genother1 AND g.gengroup='LOCATION' INNER JOIN ql_mstgen g1 ON g1.cmpcode=d.cmpcode AND g1.genoid=d.unitoid INNER JOIN QL_mstgen g3 ON g3.cmpcode=pod.cmpcode AND g3.genoid=pod.trnbelidtlunit WHERE d.trnsjbelioid= " & iID & " AND d.cmpcode='" & CompnyCode & "' group by d.trnsjbelidtlseq,pod.trnbelidtloid , d.trnsjbelidtloid,d.trnsjbelioid,d.unitoid,d.qty, d.mtrlocoid,d.refoid, d.unitoid,d.trnsjbelinotedtl,d.trnbelidtloid, pod.trnbelidtlqty ,g.gendesc ,g1.gendesc ,pod.trnbelidtlunit,pod.itemoid,d.refname,g3.gendesc,G.GENOID ,gw.gendesc, d.biayaExpedisi,pd.qty,pod.bonus,pd.branch_code, d.hargaekspedisi, d.typeberat, d.berat "

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        tbldtl.DataSource = objDs.Tables("data")
        tbldtl.DataBind()
        Session("TblDtl") = objDs.Tables("data")
        Session("TblDtlSNItem") = Session("TblDtl")

        Dim ckitemSN As DataTable = Session("TblDtlSNItem")
        Dim dv As DataView = ckitemSN.DefaultView
        dv.RowFilter = "SNList= 'Y'"
        If dv.Count > 0 Then
            CheckBoxSN.Checked = True : FileExel.Visible = True
            btnuploadexel.Visible = True : Label9.Visible = True
        Else
            CheckBoxSN.Checked = False : FileExel.Visible = False
            btnuploadexel.Visible = False : Label9.Visible = False
        End If
        dv.RowFilter = ""
        trnsjbelidtlseq.Text = objDs.Tables("data").Rows.Count + 1
        I_u2.Text = "New Detail"

        If trnsjbelistatus.Text = "Revised" Or trnsjbelistatus.Text = "Rejected" Then
            btnSave.Visible = False : imbSendApproval.Visible = False
            btnDelete.Visible = False : imbSendApproval.Visible = False
        End If

        If ToDouble(Session("printapproval")) > 0 Then
            If posting.Text.ToUpper = "INVOICED" Or posting.Text = "Approved" Or posting.Text = "In Approval" Then 
                BtnToExcel.Visible = True
            Else
                BtnToExcel.Visible = False
            End If
        Else
            BtnToExcel.Visible = False
        End If
    End Sub

    Public Sub BindData(ByVal SQueryPlus As String)
        sSql = "SELECT a.branch_code, a.trnsjbelioid, a.trnsjbelitype, a.trnsjbelino, p.periodacctg, a.trnsjbelidate, p.trnbelipodate, p.trnsuppoid, p.trnbelimstoid, a.trnsjbelinote, isnull(p.Link_File, 'N') AS Link_File, a.trnsjbelistatus, a.upduser, a.updtime, s.suppname, p.trnbelipono FROM QL_trnsjbelimst a inner join QL_pomst p on p.trnbelipono = a.trnbelipono and a.cmpcode = p.cmpcode inner join QL_mstsupp s on p.trnsuppoid = s.suppoid And a.cmpcode = s.cmpcode WHERE a.cmpcode='" & CompnyCode & "'"
        If DDLfCabang.SelectedValue <> "ALL" Then
            sSql &= " AND a.branch_code='" & DDLfCabang.SelectedValue & "'"
        End If

        If chkPeriod.Checked = True Then
            sSql &= " AND trnsjbelidate BETWEEN '" & Format(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy").ToString & "' AND '" & Format(CDate(toDate(FilterPeriod2.Text)), "MM/dd/yyyy").ToString & "'"
        End If

        If chkStatus.Checked = True Then
            sSql &= " AND trnsjbelistatus LIKE '%" & DDLStatus.SelectedValue & "%'"
        End If

        sSql &= SQueryPlus & " AND a.cmpcode='" & CompnyCode & "' ORDER BY CASE a.trnsjbelistatus WHEN 'In Process' THEN 1 WHEN 'In Approval' THEN 2 ELSE 3 END ASC, trnsjbelino DESC"
        sqlTempSearch = sSql
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "Ql_trnsjbeli")
        gvTblData.DataSource = dtSupp : gvTblData.DataBind()
        Session("QL_mstcust") = dtSupp

        If ToDouble(Session("printapproval")) > 0 Then
            gvTblData.Columns(12).Visible = True
        Else
            gvTblData.Columns(12).Visible = False
        End If
        gvTblData.Visible = True : gvTblData.SelectedIndex = -1
    End Sub

    Private Sub binddataSalesOrder()
        Dim itemMasuk As DataTable = Session("TblDtl")
        Dim iditem As String = ""
        If Session("TblDtl") Is Nothing Then
            iditem = "''"
        Else
            If itemMasuk.Rows.Count = 0 Then
                iditem = "''"
            Else
                If Not itemMasuk Is Nothing Then
                    For it As Integer = 0 To itemMasuk.Rows.Count - 1
                        iditem &= itemMasuk.Rows(it).Item("matoid")
                        If it < itemMasuk.Rows.Count - 1 Then
                            iditem &= ","
                        End If
                    Next
                Else
                    iditem = "''"
                End If
            End If
        End If
        sSql = "select isnull((case pod.typeberat when 0 then i.beratbarang else i.beratvolume end),0.00) berat,pod.trnbelidtloid AS podtloid,(select itemcode from QL_mstitem where itemoid = pod.itemoid) as itemcodeb,(select itemdesc from QL_mstitem where itemoid = pod.itemoid) as matlongdesc,(select merk from QL_mstitem where itemoid = pod.itemoid) as merk,pod.trnbelidtlunit unit,pod.trnbelidtlqty qty,pod.itemoid matoid,'' matpictureloc,(SELECT g.gendesc FROM ql_mstgen g WHERE g.genoid=pod.trnbelidtlunit) AS satuan, isnull((select sum(qty) from ql_trnsjbelidtl sjd where pod.trnbelidtloid=sjd.trnbelidtloid and trnsjbelioid in  (select trnsjbelioid from ql_trnsjbelimst where upper(trnsjbelistatus) in ('POST','INVOICED','APPROVED'))),0) as trnsjbelidtlqty from ql_podtl pod,ql_pomst po where  pod.cmpcode=po.cmpcode AND pod.trnbelimstoid = po.trnbelimstoid and (pod.trnbelidtlstatus <> 'Completed' OR pod.trnbelidtlstatus IS NULL) and po.trnbelimstoid=" & trnbelimstoid.Text & " AND  pod.cmpcode='" & CompnyCode & "' and pod.itemoid not in(" & iditem & ") ORDER BY pod.trnbelidtlseq"
        FillGV(gvReference, sSql, "ql_podtl")
    End Sub

    Private Sub BindMaterial()
        Dim itemMasuk As DataTable = Session("TblDtl")
        Dim iditem As String = ""
        If Session("TblDtl") Is Nothing Then
            iditem = "''"
        Else
            If itemMasuk.Rows.Count = 0 Then
                iditem = "''"
            Else
                If Not itemMasuk Is Nothing Then
                    For it As Integer = 0 To itemMasuk.Rows.Count - 1
                        iditem &= itemMasuk.Rows(it).Item("matoid")
                        If it < itemMasuk.Rows.Count - 1 Then
                            iditem &= ","
                        End If
                    Next
                Else
                    iditem = "''"
                End If
            End If
        End If

        sSql = "select isnull((pod.berat),0.00) berat, 'true' enableberat ,pod.trnbelidtloid  podtloid,i.itemcode itemcodeb, i.itemdesc matlongdesc,i.merk, pod.trnbelidtlunit unit,pod.trnbelidtlqty - Isnull((SELECT (SUM(qty)) FROM ql_trnsjbelidtl bd Inner Join ql_trnsjbelimst bm On bm.trnsjbelioid=bd.trnsjbelioid AND bm.trnbelipono=po.trnbelipono AND refoid=pod.itemoid And pod.bonus=bd.bonus AND bm.trnsjbelistatus<>'REJECTED'),0.00) As qty, pod.itemoid matoid, '' matpictureloc,pod.bonus,(SELECT g.gendesc FROM ql_mstgen g WHERE g.genoid=pod.trnbelidtlunit) AS satuan,(SELECT g.gendesc FROM ql_mstgen g WHERE g.genoid=pod.trnbelidtlunit) AS satuan,Isnull((SELECT (SUM(qty)) FROM ql_trnsjbelidtl bd Inner Join ql_trnsjbelimst bm On bm.trnsjbelioid=bd.trnsjbelioid AND bm.trnbelipono=po.trnbelipono AND refoid=pod.itemoid And pod.bonus=(Case bd.bonus When '' Then 'No' End) AND bm.trnsjbelistatus<>'REJECTED'),0.00) as trnsjbelidtlqty, '' AS matno, 0.00 AS matqty/*, 0.00 AS Harga*/, 'False' AS checkvalue, 'True' AS enableqty, CASE i.has_SN WHEN 1 THEN 'Y' ELSE 'N' END has_SN, pod.ekspedisi, isnull(pod.hargaekspedisi,0.00) hargaekspedisi, 'true' enablehargaekspedisi, (case pod.typeberat when 0 then 'Berat Barang' when 1 then 'Berat Volume' when 3 then 'Global' else ' None' end) typeberat " & _
        " from ql_podtl pod INNER JOIN ql_pomst po ON pod.trnbelimstoid = po.trnbelimstoid INNER JOIN QL_mstItem i ON pod.itemoid = i.itemoid where (pod.trnbelidtlstatus <> 'Completed' OR pod.trnbelidtlstatus IS NULL) and po.trnbelimstoid=" & trnbelimstoid.Text & " and i.stockflag <> 'ASSET' AND pod.cmpcode='MSC' AND pod.itemoid not in(" & iditem & ") AND pod.trnbelidtlqty - Isnull((SELECT (SUM(qty)) FROM ql_trnsjbelidtl bd Inner Join ql_trnsjbelimst bm On bm.trnsjbelioid=bd.trnsjbelioid AND bm.trnbelipono=po.trnbelipono AND refoid=pod.itemoid And pod.bonus=bd.bonus AND bm.trnsjbelistatus<>'REJECTED'),0.00) <> 0 ORDER BY pod.trnbelidtlseq"

        FilterTextListMat.TextMode = TextBoxMode.SingleLine
        FilterTextListMat.Rows = 0
        FilterDDLListMat.Items(0).Enabled = False
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            'cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            'showMessage("Maaf, data yang anda maksud tidak ada..!!", CompnyName & " - INFORMATION", 3)
        End If
    End Sub

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    Dim bonus As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "podtloid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            If dv(0)("bonus") = "Yes" Then
                                dv(0)("checkvalue") = "True"
                                dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(6).Controls))
                                dv(0)("hargaekspedisi") = ToDouble(GetTextValue(row.Cells(7).Controls))
                            Else
                                dv(0)("checkvalue") = "True"
                                dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(6).Controls))
                                dv(0)("hargaekspedisi") = ToDouble(GetTextValue(row.Cells(7).Controls))
                            End If

                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If

        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(6).Controls))
                            dv(0)("hargaekspedisi") = ToDouble(GetTextValue(row.Cells(7).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub BindDataUser()
        sSql = "SELECT userid,username FROM ql_mstprof WHERE cmpcode='" & CompnyCode & "' AND statusprof='Active' and divisi in ('Approval') "
        FillGV(gvListUser, sSql, "QL_mstprof")
    End Sub

    Private Sub ClearDetail()
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            trnsjbelidtlseq.Text = objTable.Rows.Count + 1
        End If
        I_u2.Text = "New Detail" : matoid.Text = "" : matname.Text = ""
        refered.Text = "0.00" : acum.Text = "0.00" : trnbelidtlunit.Text = ""
        pounit.Text = "" : batchoid.Text = "" : trnsjbelidtlqty.Text = "0.00"
        satuan.Text = "" : trnsjbelidtlunit.Text = "" : qty2.Text = "0.00"
        Expedisiharga.Text = "0.00" : terkirim.Text = "0" : texthas_sn.Text = ""
        unit2desc.Text = "" : unit2.Text = "" : trnsjbelidtlnote.Text = ""
        tolerance.Text = "" : chkCompleted.Visible = False
        qty2.CssClass = "inpText" : qty2.ReadOnly = False
        chkCompleted.Checked = False : lblTolInfo.Visible = False
        cb1.Checked = False : cb2.Checked = False
        beratBarang.Text = "" : beratVolume.Text = ""
        costEkspedisi.Text = ""
        tbldtl.Columns(11).Visible = True
    End Sub

    Private Sub generateNo()
        trnsjbelino.Text = GenerateID("QL_trnsjbelimst", CompnyCode)
    End Sub

    Public Sub BindDataOrder(ByVal sPlus As String, ByVal view As String)
        Dim find As String = "", tOb As String = "", daterange As String = ""
        If sPlus <> "" Then
            find = " AND " & DDLFilterPO.SelectedValue & " LIKE '%" & Tchar(txtFilterPO.Text) & "%'"
        End If

        If view = "view" Then
            daterange = " and po.trnbelideliverydate BETWEEN '" & Format(GetServerTime(), "MM/01/yyyy") & "' AND '" & Format(GetServerTime(), "MM/dd/yyyy") & "' "
        End If

        If Session("branch_id") <> "10" Then
            tOb = " AND (ISNULL(po.tocabang,'') IN ('" & dd_branch.SelectedValue & "','') OR po.trnbelimstoid IN (Select pomstoid From QL_mstopenpo op Where op.pomstoid=po.trnbelimstoid and tocabang like '%" & dd_branch.SelectedValue & "%'))"
        End If
        sSql = "SELECT g.gendesc prefix, po.trnbelimstoid pomstoid, po.trnbelipono pono, case po.statusPOBackorder when 'T' then 'BO' else 'PO' end as Type, po.trnbelipodate podate, po.TRNBELIdeliverydate podeliv, po.trnsuppoid suppoid, s.suppname, po.branch_code, ISNULL((Select gendesc FROM QL_mstgen tc Where tc.gencode=po.tocabang AND tc.gengroup='CABANG'),'') dCabang, ISNULL(po.tocabang,'') toCabang ,SUM(d.trnbelidtlqty) QtyPO, ISNULL((select SUM(s.qty) From QL_trnsjbelidtl s Inner join ql_trnsjbelimst sj ON sj.trnsjbelioid=s.trnsjbelioid Where sj.trnsjbelistatus<>'REJECTED' AND sj.trnbelipono=po.trnbelipono AND sj.branch_code=s.branch_code),0) QtySJ From ql_pomst po Inner join ql_mstsupp s on po.trnsuppoid=s.suppoid and po.cmpcode=s.cmpcode Inner Join QL_podtl d ON d.trnbelimstoid=po.trnbelimstoid AND po.branch_code=d.branch_code LEFT JOIN QL_mstgen g on g.genoid = CASE po.PrefixOid When 0 Then 81 Else po.PrefixOid End and g.gengroup = 'prefixsupp' Where po.TRNBELItype = 'GROSIR' " & tOb & " /*" & daterange & "*/ " & find & " And po.trnbelistatus='Approved' Group by g.gendesc, po.trnbelimstoid, po.trnbelipono, po.statusPOBackorder, po.tocabang, po.trnbelipodate, po.TRNBELIdeliverydate, po.trnsuppoid, s.suppname, po.branch_code Having SUM(d.trnbelidtlqty)-ISNULL((select SUM(s.qty) From QL_trnsjbelidtl s Inner join ql_trnsjbelimst sj ON sj.trnsjbelioid=s.trnsjbelioid Where sj.trnsjbelistatus<>'REJECTED' AND sj.trnbelipono=po.trnbelipono AND sj.branch_code=s.branch_code),0)>0 ORDER BY po.trnbelipono desc"

        'sSql = "SELECT g.gendesc prefix, po.trnbelimstoid pomstoid, po.trnbelipono pono,case po.statusPOBackorder when 'T' then 'BO' else 'PO' end as Type ,po.trnbelipodate podate,po.TRNBELIdeliverydate podeliv,po.trnsuppoid suppoid,s.suppname,po.branch_code, ISNULL((Select gendesc FROM QL_mstgen tc Where tc.gencode=po.tocabang AND tc.gengroup='CABANG'),'') dCabang,ISNULL(po.tocabang,'') toCabang From ql_pomst po inner join ql_mstsupp s on po.trnsuppoid=s.suppoid and po.cmpcode=s.cmpcode and po.trnbelistatus='Approved' LEFT JOIN QL_mstgen g on g.genoid = CASE po.PrefixOid When 0 Then 81 Else po.PrefixOid End and g.gengroup = 'prefixsupp' Where po.trnbelimstoid in (Select dt.trnbelimstoid from (SELECT D.trnbelimstoid, D.trnbelidtlqty - ISNULL((select sum(s.qty) from QL_trnsjbelidtl s Inner join ql_trnsjbelimst sj ON sj.trnsjbelioid=s.trnsjbelioid where s.trnbelidtloid=d.trnbelidtloid AND sj.trnsjbelistatus<>'REJECTED'),0) QTY FROM QL_podtl D where d.trnbelidtlstatus <> 'Completed') DT WHERE DT.QTY>0) " & tOb & " /*" & daterange & "*/ " & find & " AND po.TRNBELItype = 'GROSIR'"
        'sSql &= " ORDER BY po.trnbelipono desc"
        FillGV(gvListPO, sSql, "ql_pomst")
    End Sub

    Private Sub clearRef()
        orderno.Text = "" : trnbelimstoid.Text = "" : trnsuppoid.Text = ""
        suppname.Text = "" : identifierno.Text = "" : trnsjbelitype.SelectedIndex = 0
        Session("TblDtl") = Nothing : tbldtl.DataSource = Nothing : tbldtl.DataBind()
    End Sub

    Private Sub isidtTable()
        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetail() : Session("TblDtl") = dtlTable
        End If

        Dim objTable As DataTable : objTable = Session("TblDtl")
        Dim dv As DataView = objTable.DefaultView
        'Cek apa sudah ada item yang sama dalam Tabel Detail

        'insert/update to list data
        Dim objRow As DataRow

        For C1 As Int16 = 0 To objDataSet.Tables("QL_trnsjbeli").Rows.Count - 1 'objTable.Rows.Count - 2
            If I_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("trnsjbelidtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(trnsjbelidtlseq.Text - 1)
                objRow.BeginEdit()
            End If 

            objRow("trnsjbelidtloid") = 0
            objRow("trnsjbelimstoid") = 0
            objRow("matloc") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("matloc")
            objRow("matoid") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("matoid") 'matoid.Text
            objRow("item") = cKoneksi.ambilscalar("select matlongdesc from ql_mstmat where matoid =" & objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("matoid")) 'objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("item").ToString.Trim 'matname.Text
            objRow("trnsjbelidtlqty") = ToDouble(objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("trnsjbelidtlqty").ToString.Trim) 'trnsjbelidtlqty.Text)
            objRow("trnsjbelidtlunitoid") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("trnsjbelidtlunitoid") 'trnsjbelidtlunit.Text
            objRow("trnsjbelidtlnote") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("trnsjbelidtlnote").ToString.Trim 'trnsjbelidtlnote.Text

            objRow("referedqty") = ToDouble(objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("referedqty").ToString.Trim) 'refered.Text)
            objRow("receivedqty") = ToDouble(objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("receivedqty").ToString.Trim) 'acum.Text)
            objRow("location") = cKoneksi.ambilscalar("select isnull(gendesc,'') from ql_mstgen where genoid=" & objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("matloc") & " and gengroup='LOCATION' ") 'objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("location").ToString.Trim 'matLoc.SelectedItem.Text
            objRow("unit") = cKoneksi.ambilscalar("select isnull(gendesc,'') from ql_mstgen where genoid=" & objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("trnsjbelidtlunitoid") & " and gengroup like '%unit%'") 'objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("unit").ToString.Trim 'satuan.Text
            objRow("podtlunit") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("podtlunit") 'trnbelidtlunit.Text
            objRow("unit") = cKoneksi.ambilscalar("select isnull(gendesc,'') from ql_mstgen where genoid=" & objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("trnsjbelidtlunitoid") & " and gengroup like '%unit%'") 'objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("unit").ToString.Trim 'pounit.Text
            objRow("batchoid") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("batchoid") 'batchoid.Text

            objRow("unit2desc") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("unit2desc").ToString.Trim 'unit2desc.Text
            objRow("trnpodtloid") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("trnpodtloid") 'IdDetail.Text
            objRow("noroll") = objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("noroll").ToString.Trim 'TxtNoroll.Text
            objRow("width") = ToDouble(objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("width").ToString.Trim) 'txtwidth.Text)
            objRow("lenght") = ToDouble(objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("length").ToString.Trim) 'txtLenght.Text)
            objRow("weight") = ToDouble(objDataSet.Tables("QL_trnsjbeli").Rows(C1).Item("weight").ToString.Trim) 'txtWeight.Text)

            If ToDouble(trnsjbelidtlqty.Text) <> (ToDouble(refered.Text) + ToDouble(acum.Text)) Then
                objRow("setascomplete") = False
            Else
                objRow("setascomplete") = True
            End If

            If I_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
        Next 
        'objTable.Rows.RemoveAt(objDataSet.Tables("QL_trnsjbeli").Rows.Count - 1)
        Session("TblDtl") = objTable
        tbldtl.Visible = True
        tbldtl.DataSource = Nothing
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
    End Sub

    Private Sub PrintSN()
        Try
            If gvTblData.SelectedDataKey.Item("Link_File") = "N" Then
                showMessage("Tidak Ada SN Untuk PO ", CompnyName & " - INFORMATION", 1)
                TabContainer1.ActiveTabIndex = 0
            Else
                Response.ContentType = "application/x-msexcel"
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(gvTblData.SelectedDataKey.Item("Link_File"))))
                Response.TransmitFile(gvTblData.SelectedDataKey.Item("Link_File"))
                CheckBoxSN.Checked = True
                Label19.Text = gvTblData.SelectedDataKey.Item("Link_File")
                Response.Flush()
            End If
        Catch ex As HttpException
            Response.Close()
            Response.End()
        End Try
    End Sub

    Private Sub BindLastSearch()
        Dim mySqlDA As New SqlDataAdapter(Session("SearchPDO"), conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        Session("tbldata") = objDs.Tables("data")
        gvTblData.DataSource = objDs.Tables("data")
        gvTblData.DataBind()
    End Sub
#End Region

#Region "events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sLastSearch As String = Session("SearchPDO")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchPDO") = sLastSearch
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\trnsjbeli.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Purchase Delivery Order"
        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")
        imbSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data to Approval?');")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        sSql = "Select COUNT(*) From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='PRINT EXCEL NOTA PURCHASE' and branch_code Like '%" & Session("branch_id") & "%' AND approvaluser='" & Session("UserID") & "'"
        Session("printapproval") = GetScalar(sSql)
        If Not IsPostBack Then
            fCabangInit()
            Session("ddlFilterIndex") = Nothing
            FilterPeriod1.Text = Format(GetServerTime, "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
            createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
            BindData("") : InitCabangNya() : LocDLL()
            matLoc_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                I_U.Text = "Update"
                FillTextBox(Session("branch_code"), Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                I_U.Text = "New" : generateNo()
                updUser.Text = Session("UserID")
                updTime.Text = GetServerTime()
                Session("ItemLine") = 1 : trnsjbelistatus.Text = "In Process"
                trnsjbelidtlseq.Text = Session("ItemLine")
                trnsjreceivedate.Text = Format(GetServerTime, "dd/MM/yyyy")
                trnsjbelidate.Text = Format(GetServerTime, "dd/MM/yyyy")
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        Else
            'TabContainer1.ActiveTabIndex = 1
        End If
        Dim objTable As New DataTable
        objTable = Session("TblDtl")
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        TabContainer1.ActiveTabIndex = 1
        MultiView1.SetActiveView(View2)
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        TabContainer1.ActiveTabIndex = 1
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub btnAddtolist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddtolist.Click
        ' Check Other Shipment
        Dim msg As String = "" : Dim JumKode As Integer : Dim DataExcel As String = ""
        JumKode = trnsjbelidtlqty.Text
        If masuk = 0 Then
            If ToDouble(JumKode) <= 0 Then
                msg &= "- Quantity must be greater than zero (0) !<br />"
            End If
            Dim dt As DataTable
            If Session("TblListMat") IsNot Nothing Then
                dt = Session("TblListMat")
            Else
                dt = Session("TblDtl")
            End If
            For j1 As Integer = 0 To dt.Rows.Count - 1
                If Session("TblListMat") IsNot Nothing Then
                    If dt.Rows(j1)("typeberat").ToString = "Global" Then
                        If (ToDouble(dt.Rows(j1)("matqty").ToString)) <> JumKode Then
                            Session("WarningListMat") = "Barang Tsb Tidak Dapat di Parsial Karena Menggunakan Ekspedisi Global"
                            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                            Exit Sub
                        End If
                    End If
                Else
                    If dt.Rows(j1)("typeberat").ToString = "3" Then
                        If (ToDouble(dt.Rows(j1)("referedqty").ToString)) <> JumKode Then
                            Session("WarningListMat") = "Barang Tsb Tidak Dapat di Parsial Karena Menggunakan Ekspedisi Global"
                            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                            Exit Sub
                        End If
                    End If
                End If
            Next


            If ToDouble(JumKode) > (ToDouble((ToDouble(refered.Text))) - ToDouble(acum.Text)) Then
                'sender.Text = ToMaskEdit(ToDouble(currentstock.Text), 1)
                msg &= "- Quantity Must be Less than Data PO Receive " & DataExcel & " !<br />"
            End If
            If msg <> "" Then
                showMessage(msg, CompnyName & " - WARNING", 1)
                Exit Sub
            End If
            If matoid.Text <> "" And I_u2.Text = "New Detail" Then
                If checkOtherShipment(orderno.Text, matoid.Text, CompnyCode) Then
                    showMessage("There is other Delivery that using same Item.<BR> Please DELETE or APPROVED that Delivery first to ensure<BR> you get up-to-date Delivery data!!", CompnyName & " - INFORMATION", 3)
                    Exit Sub
                End If
            End If
            Dim sMsg As String = ""
            If matoid.Text = "" Then : sMsg &= "- Please choose Material !!<BR>" : End If
            'If ToDouble(trnsjbelidtlqty.Text) > ToDouble(refered.Text) Then
            '    sMsg &= "- Qty can't more than Total Qty !<br />"
            'End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If Session("TblDtl") Is Nothing Then
                Dim dtlTable As DataTable = setTabelDetail()
                Session("TblDtl") = dtlTable
            End If

            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            'Cek apa sudah ada item yang sama dalam Tabel Detail
            If I_u2.Text = "New Detail" Then
                dv.RowFilter = "matoid=" & matoid.Text & " and Unit= '" & pounit.Text & "' and bonus='" & bonus.Text & "'"
                If dv.Count > 0 Then
                    dv.RowFilter = ""
                    showMessage("This Data has been added before, please check!", CompnyName & " - WARNING", 2)
                    objTable = Session("TblDtl")
                    tbldtl.DataSource = objTable
                    tbldtl.DataBind()
                    tbldtl.Visible = True
                    Exit Sub
                End If
                dv.RowFilter = ""
            Else
                ' edit dari table detail
                dv.RowFilter = "matoid=" & matoid.Text & " and unit = '" & pounit.Text & "'  AND trnsjbelidtlseq <> " & trnsjbelidtlseq.Text & " and bonus='" & bonus.Text & "' "

                If dv.Count > 0 Then
                    showMessage("This Data has been added before, please check!", CompnyName & " - WARNING", 2)
                    dv.RowFilter = "" : Exit Sub
                End If
                dv.RowFilter = ""
            End If

            'insert/update to list data
            Dim objRow As DataRow
            If I_u2.Text = "New Detail" Then

                objRow = objTable.NewRow()
                objRow("trnsjbelidtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(trnsjbelidtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("trnsjbelidtloid") = 0
            objRow("trnsjbelimstoid") = 0
            objRow("matloc") = matLoc.SelectedValue
            objRow("matoid") = matoid.Text
            objRow("item") = matname.Text
            objRow("refname") = "QL_MSTITEM"
            objRow("trnsjbelidtlqty") = ToDouble(JumKode)
            masuk = ToDouble(JumKode)
            objRow("trnsjbelidtlunitoid") = trnsjbelidtlunit.Text
            objRow("trnsjbelidtlnote") = trnsjbelidtlnote.Text
            objRow("referedqty") = ToDouble(refered.Text)
            objRow("receivedqty") = ToDouble(acum.Text)
            objRow("location") = matLoc.SelectedItem.Text
            objRow("unit") = satuan.Text
            objRow("podtlunit") = trnbelidtlunit.Text
            objRow("unit") = pounit.Text
            objRow("unit2desc") = unit2desc.Text
            objRow("trnpodtloid") = IdDetail.Text
            objRow("KodeBR") = LabelKode.Text
            objRow("ekspedisi") = ToDouble(Expedisiharga.Text)
            KetSN = texthas_sn.Text
            objRow("SNList") = KetSN
            If cb1.Checked = True Or cb2.Checked = True Then
                If cb2.Checked = True Then 'Berat Volume
                    If costEkspedisi.Text = 0 Then
                        objRow("ekspedisi") = ToDouble(costEkspedisi.Text)
                        objRow("berat") = ToDouble(beratVolume.Text)
                    Else
                        objRow("ekspedisi") = ToDouble(costEkspedisi.Text) * (ToDouble(beratVolume.Text) / 1000000) * ToDouble(trnsjbelidtlqty.Text)
                        objRow("berat") = ToDouble(beratVolume.Text)
                    End If
                Else
                    'Berat Barang
                    If costEkspedisi.Text = 0 Then
                        objRow("ekspedisi") = ToDouble(costEkspedisi.Text)
                        objRow("berat") = ToDouble(beratBarang.Text)
                    Else
                        objRow("ekspedisi") = ToDouble(beratBarang.Text) * ToDouble(trnsjbelidtlqty.Text) * ToDouble(costEkspedisi.Text)
                        objRow("berat") = ToDouble(beratBarang.Text)
                    End If
                End If
            Else
                'None / Global
                objRow("ekspedisi") = ToDouble(costEkspedisi.Text)
                objRow("berat") = 0
            End If
            objRow("hargaekspedisi") = ToDouble(costEkspedisi.Text)
            If cb1.Checked = True Then
                objRow("typeBerat") = 0
            ElseIf cb2.Checked = True Then
                objRow("typeBerat") = 1
            Else
                If costEkspedisi.Text <> 0.0 Then
                    objRow("typeBerat") = 3
                Else
                    objRow("typeBerat") = 2
                End If
            End If
            If ToDouble(JumKode) <> (ToDouble(refered.Text) + ToDouble(acum.Text)) Then
                objRow("setascomplete") = False
            Else
                objRow("setascomplete") = True
            End If

            If I_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            ClearDetail()
            Session("TblDtl") = objTable
            tbldtl.Visible = True : tbldtl.DataSource = Nothing
            tbldtl.DataSource = objTable
            tbldtl.DataBind()
            ' CheckIfNeedApproval() '(objTable)
            trnsjbelidtlseq.Text = objTable.Rows.Count + 1
            tbldtl.SelectedIndex = -1 : tbldtl.Columns(16).Visible = True
        End If
    End Sub

    Protected Sub Update_SN()
        Dim cmd As New SqlCommand
        Dim sql As String
        conn.Open()
        cmd.Connection = conn
        cmd.CommandType = CommandType.Text
        sql = "update ql_mstitemdtl set status_item ='In Approval' where itemoid in (select b.refoid from ql_trnsjbelimst a inner join ql_trnsjbelidtl b on a.trnsjbelioid = b.trnsjbelioid inner join ql_mstitemdtl c on b.refoid = c.itemoid where a.trnsjbelioid =@trnsjbelioid and a.trnsjbelistatus ='In Process')"
        cmd.CommandText = sql
        cmd.Parameters.AddWithValue("@trnsjbelioid", trnsjbelino.Text)
        cmd.ExecuteScalar()
        conn.Close()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim conmtroid As Int64 = 0 : Dim crdmtroid As Int64 = 0
        Dim sMsg As String = ""

        If trnbelimstoid.Text.Trim = "" Then
            sMsg &= "- Please select a Purchase Order!<BR>"
        End If
        Dim state1, state2 As Boolean
        Try
            Dim dTemp As Date = CDate(toDate(trnsjbelidate.Text))
            state1 = True
        Catch ex As Exception
            sMsg &= "- Invalid Order date !<BR>"
            state1 = False
        End Try

        Try
            Dim dTemp As Date = CDate(toDate(trnsjreceivedate.Text))
            periodacctg.Text = Format(CDate(toDate(trnsjreceivedate.Text)), "yyyyMM")
            state2 = True
        Catch ex As Exception
            sMsg &= "- Invalid received date !<BR>" : state2 = False
        End Try

        If state1 And state2 Then
            'If CDate(todate(trnsjbelidate.Text)) < CDate(todate(trnsjreceivedate.Text)) Then
            '    sMsg &= "- Purchase delivery Order date must be greater or same than Receive date!!<BR>"
            'End If
        End If
        If notes.Text.Trim = "" Then
            sMsg &= "- Input No. SJ Supplier !<BR>"
        End If
        'cek apa sudah ada item dari Detail SJ
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- No Item Data !!<BR>"
        Else
            Dim objTableCek As DataTable = Session("TblDtl")
            If objTableCek.Rows.Count = 0 Then
                sMsg &= "- No Item Data !!<BR>"
            End If

            For SecuritySN As Integer = 0 To objTableCek.Rows.Count - 1
                sSql = "SELECT has_sn FROM QL_mstItem WHERE itemoid = " & objTableCek.Rows(SecuritySN).Item("matoid") & ""
                Dim SNSecuritySN As Integer = ToDouble(GetStrData(sSql))
                If SNSecuritySN = 1 Then
                    If Label19.Text = "" Then
                        showMessage("<strong>ITEM " & objTableCek.Rows(SecuritySN).Item("item") & " menggunakan SN di mohon upload sn dengan kode item " & objTableCek.Rows(SecuritySN).Item("kodebr") & "</strong>", CompnyName & " - INFORMATION", 3)
                        Exit Sub
                    End If
                End If
            Next

            If posting.Text = "POST" Or posting.Text = "In Approval" Then
                'cek stok ke data yg dikiirm
                For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
                    sSql = "SELECT CAST((trnbelidtlqty*1) AS numeric(18,2)) - ISNULL((select sum(qty) from QL_trnsjbelidtl d inner join QL_trnsjbelimst s on s.trnsjbelioid=d.trnsjbelioid and trnsjbelistatus='Approved' and trnbelidtloid=" & objTableCek.Rows(c1).Item("trnpodtloid") & "),0) FROM QL_PODTL D WHERE D.trnbelidtloid=" & objTableCek.Rows(c1).Item("trnpodtloid")
                    Dim QTYSISADELIV As Decimal = ToDouble(GetStrData(sSql))
                    If objTableCek.Rows(c1).Item("trnsjbelidtlqty") > QTYSISADELIV Then
                        showMessage("Max QTY must be (" & ToMaskEdit(QTYSISADELIV, 4) & ") for item '" & objTableCek.Rows(c1).Item("item") & "'  !!", CompnyName & " - WARNING", 2)
                        posting.Text = "In Process"
                        Exit Sub
                    End If
                Next
            End If
        End If

        ' Check item quantity if above maximum
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            posting.Text = "In Process" : Exit Sub
        End If
        Dim snx As String = ""

        If Label19.Text <> "" Then
            SaveLocation = Label19.Text 'Server.MapPath("FileExcel") & "\" & fn
            ConnStrExe = "provider=Microsoft.jet.OLEDB.4.0; Data Source=" & SaveLocation & "; Extended Properties=Excel 8.0;"
            connexc = New OleDbConnection(ConnStrExe)
            'Cek Kode Item yang ada Di Excel Apakah Ada yang sama 
            Try
                If connexc.State = ConnectionState.Closed Then
                    connexc.Open()
                    sSql = "select Sn from [SN_Upload$] GROUP BY Sn HAVING COUNT(Sn) > 1 "
                    objDataAdapter = New OleDbDataAdapter(sSql, connexc)
                    objDataAdapter.Fill(dtsnExcelsama)
                    If dtsnExcelsama.Rows.Count > 0 Then
                        For n As Integer = 0 To dtsnExcelsama.Rows.Count - 1
                            snx &= dtsnExcelsama.Rows(n).Item("Sn")
                            If n < dtsnExcelsama.Rows.Count - 1 Then
                                snx &= ","
                                If (n / 8) = 0 Then
                                    snx &= "</br>"
                                End If
                            End If
                        Next
                        showMessage("<strong>SN yang di upload ada yang sama </strong> </br> " & snx, CompnyName & " - INFORMATION", 3)
                        connexc.Close()
                        Exit Sub
                    End If
                    connexc.Close()
                End If
            Catch ex As Exception
                showMessage("Format Excel Salah", CompnyName & " - ERROR", 1)
                connexc.Close()
                Exit Sub
            End Try

            'Cek Kode Item yang ada Di Excel Dengan Kode Item Yang Di pesan PO
            Try
                Dim dtitem As DataTable = Session("TblDtl")

                For j As Integer = 0 To dtitem.Rows.Count - 1
                    sSql = "SELECT has_sn FROM QL_mstItem WHERE itemoid = '" & dtitem.Rows(j).Item("matoid") & "'"
                    Dim has_sn_item As Integer = ToDouble(GetStrData(sSql))
                    If connexc.State = ConnectionState.Closed Then
                        connexc.Open()
                        If has_sn_item = 1 Then
                            sSql = "select count(itemcode) from [SN_Upload$] where itemcode = '" & dtitem.Rows(j).Item("kodebr") & "' "
                            commecel = New OleDbCommand(sSql, connexc)
                            Dim JumItemcode As Integer = commecel.ExecuteScalar()
                            If JumItemcode = 0 Then
                                showMessage("<strong>Kode Item Yang di Excel Tidak Sesuai Dengan Kode Item " & dtitem.Rows(j).Item("kodebr") & " pada PO yang di pesan  </strong>", CompnyName & " - INFORMATION", 3)
                                connexc.Close()
                                Exit Sub
                            End If
                        End If
                        connexc.Close()
                    End If
                Next
            Catch ex As Exception
                showMessage("Format Excel Salah", CompnyName & " - ERROR", 1)
                connexc.Close()
                Exit Sub
            End Try

            'Cek Jumlah Item yang di pesan yang menggunakan SN
            Try
                If connexc.State = ConnectionState.Closed Then
                    connexc.Open()
                    Dim dtItemEX As DataTable = Session("TblDtl")
                    For CekItemSN As Integer = 0 To dtItemEX.Rows.Count - 1
                        sSql = "SELECT has_sn FROM QL_mstItem WHERE itemoid = '" & dtItemEX.Rows(CekItemSN).Item("matoid") & "'"
                        Dim flagsn As Integer = ToDouble(GetStrData(sSql))
                        If flagsn = 1 Then
                            sSql = "select count(Sn) from [SN_Upload$] where itemcode = '" & dtItemEX.Rows(CekItemSN).Item("kodebr") & "' "
                            commecel = New OleDbCommand(sSql, connexc)
                            Dim JumItemPO As Integer = commecel.ExecuteScalar()
                            If JumItemPO <> dtItemEX.Rows(CekItemSN).Item("trnsjbelidtlqty") Then
                                showMessage("<strong>Jumlah PO yang di pesan " & CInt(dtItemEX.Rows(CekItemSN).Item("trnsjbelidtlqty")) & " tidak sesuai dengan jumlah " & JumItemPO & " SN pada : </strong> </br>item :" & dtItemEX.Rows(CekItemSN).Item("item") & "</br> Dengan Code :" & dtItemEX.Rows(CekItemSN).Item("kodebr"), CompnyName & " - INFORMATION", 3)
                                connexc.Close()
                                Exit Sub
                            End If
                        End If
                    Next
                    connexc.Close()
                End If
            Catch ex As Exception
                showMessage("Format Excel Salah", CompnyName & " - ERROR", 1)
                connexc.Close()
                Exit Sub
            End Try

            'Cek Data SN yang di table Dengan sn yang baru uplaod
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    If connexc.State = ConnectionState.Closed Then
                        connexc.Open()
                        sSql = "SELECT id_sn from QL_Mst_SN "
                        Dim ALLTBSN As DataTable = cKon.ambiltabel(sSql, "ALLTBSN")
                        For k As Integer = 0 To ALLTBSN.Rows.Count - 1
                            sSql = "select count(Sn) from [SN_Upload$] where itemcode = '" & ALLTBSN.Rows(k).Item("id_sn") & "' "
                            commecel = New OleDbCommand(sSql, connexc)
                            Dim sama As Integer = commecel.ExecuteScalar()
                            If sama = 1 Then
                                showMessage("<strong>SN " & ALLTBSN.Rows(k).Item("id_sn") & " yang di uplaod sudah ada yang menggunakan ", CompnyName & " - INFORMATION", 3)
                                connexc.Close()
                                Exit Sub
                            End If
                        Next
                        connexc.Close()
                    End If
                End If

            Catch ex As Exception
                showMessage("Format Excel Salah", CompnyName & " - ERROR", 1)
                connexc.Close()
                Exit Sub
            End Try
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trnsjbelimst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                showMessage("Maaf, Data sudah ter input, mohon klik tombol cancel, dan cek ulang datanya, terima kasih..!!!", CompnyName & " - WARNING", 3)
                trnsjbelistatus.Text = "In Process"
                Exit Sub
            End If
        End If


        'Inser File Excel Ke databese 
        ' checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(trnsjbelino.Text, "trnsjbelino", "ql_trnsjbelimst") Then
                generateNo()
            End If
        Else 'cek data sudah dihapus?
            If CheckDataExists(oid.Text, "trnsjbelioid", "ql_trnsjbelimst") = False Then
                showMessage("Can't Update this Data, Data has been delete !!", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
        End If

        'cek data sudah di send?
        If checkApproval("QL_trnsjbelimst", "In Approval", Session("oid"), "New", "FINAL", dd_branch.SelectedValue) > 0 Then
            showMessage("This Purchase Delivery Order has been send for Approval", CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        '====================================================================
        '---------Query Ambil data Kode Cabang Berdasarkan Location----------
        '====================================================================
        Dim sCabang As String = GetStrData("SELECT Case gencode When 'AWL' Then '00' Else g.gencode End Gencode,Case gencode When 'AWL' Then '00' Else g.gencode End+' - '+g.gendesc FROM QL_mstgen g WHERE g.genoid IN (SELECT a.genother2 From QL_mstgen a WHERE a.gengroup = 'Location' AND genoid=" & matLoc.SelectedValue & ")")
        '====================================================================

        If Session("oid") = Nothing Or Session("oid") = "" Then
            ' insert table master
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                oid.Text = GenerateID("QL_trnsjbelimst", CompnyCode)
                'Dim oidTemp As Int64 = GenerateID("QL_HistHPP", CompnyCode)
                sSql = "INSERT INTO QL_trnsjbelimst (cmpcode,trnsjbelioid,trnsjbelitype,trnsjbelidate,trnsjbelino,trnbelipono,trnsjbelinote,trnsjbelistatus,upduser,updtime,finalapprovaluser,finalapprovaldatetime, branch_code,frombranch,createtime) VALUES ('" & CompnyCode & "'," & oid.Text & ",'" & trnsjbelitype.SelectedValue & "','" & CDate(toDate(trnsjreceivedate.Text)) & "','" & trnsjbelino.Text & "','" & Tchar(orderno.Text) & "','" & Tchar(notes.Text) & "','" & posting.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & approvaluser.Text & "',CURRENT_TIMESTAMP, '" & sCabang & "','" & aCabang.Text & "','" & CDate(toDate(createtime.Text)) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & oid.Text & " where tablename ='QL_trnsjbelimst' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '========================Upload File Save =====================
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                Dim idItem As String = matoid.Text
                If CheckBoxSN.Checked = True Then

                    'Session("lokasiFile") = FileExel.PostedFile.FileName
                    Dim QueryKu As String

                    If Label19.Text <> "" Then
                        'Dim fn As String = System.IO.Path.GetFileName(Session("lokasiFile"))
                        If Not Session("TblDtl") Is Nothing Then
                            Dim objTable As DataTable
                            objTable = Session("TblDtl")
                            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                                If objTable.Rows(C1).Item("SNList") = "Y" Then
                                    Dim itemid As String = objTable.Rows(C1).Item("matoid")
                                    QueryKu = "update ql_mstitem set has_SN = " & 1 & " where cmpcode='" & CompnyCode & "' and itemoid = " & itemid
                                    xCmd.CommandText = QueryKu : xCmd.ExecuteNonQuery()
                                End If
                            Next
                        End If
                        QueryKu = "update QL_pomst set Link_File = '" & SaveLocation & "'  where cmpcode='" & CompnyCode & "' and trnbelipono= '" & orderno.Text & "'"
                        xCmd.CommandText = QueryKu : xCmd.ExecuteNonQuery()

                        SaveLocation = Label19.Text 'Server.MapPath("FileExcel") & "\" & fn
                        ConnStrExe = "provider=Microsoft.jet.OLEDB.4.0; Data Source=" & SaveLocation & "; Extended Properties=Excel 8.0;"
                        connexc = New OleDbConnection(ConnStrExe)

                        If connexc.State = ConnectionState.Closed Then
                            connexc.Open()
                            QueryKu = "select * from [SN_Upload$] where itemcode <> '' and sn <> ''"
                            adap = New OleDbDataAdapter(QueryKu, connexc)
                            Dim dtecel As New DataTable
                            adap.Fill(dtecel)
                            connexc.Close()
                            connexc.Dispose()
                            For i As Integer = 0 To dtecel.Rows.Count - 1
                                sSql = "SELECT itemoid FROM QL_mstItem WHERE itemcode = '" & dtecel.Rows(i).Item(0) & "'"
                                xCmd.CommandText = sSql
                                Dim TampungItemID As Integer = xCmd.ExecuteScalar

                                QueryKu = "insert into QL_mstitemDtl (cmpcode, itemoid, itemcode, sn, status_item,branch_code, locoid, createtime, createuser, status_in_out, last_trans_type) values ('" & CompnyCode & "', '" & TampungItemID & "','" & dtecel.Rows(i).Item(0) & "','" & dtecel.Rows(i).Item(1) & "', '" & posting.Text & "', '" & sCabang & "','" & matLoc.SelectedValue & "',CURRENT_TIMESTAMP,'" & Session("UserId") & "', 'IN','Beli')"
                                xCmd.CommandText = QueryKu : xCmd.ExecuteNonQuery()

                                QueryKu = "insert into Ql_mst_sn (id_sn, itemcode, trnbelimstoid, tglmasuk, status_in_out, status_approval, last_trans_type) values ('" & dtecel.Rows(i).Item(1) & "','" & dtecel.Rows(i).Item(0) & "'," & oid.Text & ",CURRENT_TIMESTAMP, 'IN', '" & posting.Text & "','Beli')"
                                xCmd.CommandText = QueryKu : xCmd.ExecuteNonQuery()
                            Next
                        End If
                    Else
                        'showMessage("Please select a file to upload.", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                End If
                '=========================================================================
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    trnbelidtloid.Text = GenerateID("QL_trnsjbelidtl", CompnyCode)

                    'INSERT TO QL_trnsjbelidtl
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        Dim cek As String = ""
                        'before insert into connmtr, set dahulu masuk mtrref mana?
                        Dim refnamepost As String = "QL_MSTITEM"
                        sSql = "Select bonus FROM QL_podtl Where trnbelidtloid=" & objTable.Rows(C1).Item("trnpodtloid") & " AND itemoid=" & objTable.Rows(C1).Item("matoid") & ""
                        xCmd.CommandText = sSql : Dim sBonus As String = xCmd.ExecuteScalar()

                        sSql = "INSERT INTO QL_trnsjbelidtl(cmpcode,trnsjbelidtloid,trnsjbelioid,trnbelidtloid,trnsjbelidtlseq,mtrlocoid,refoid,qty,unitoid,trnsjbelinotedtl, refname, branch_code,typeberat,berat,hargaekspedisi,biayaExpedisi,bonus,frombranch,tobranch) VALUES ('" & CompnyCode & "'," & (CInt(trnbelidtloid.Text) + C1) & "," & oid.Text & "," & objTable.Rows(C1).Item("trnpodtloid") & "," & C1 + 1 & "," & matLoc.SelectedValue & "," & objTable.Rows(C1).Item("matoid") & "," & objTable.Rows(C1).Item("trnsjbelidtlqty") & ",'" & Tchar(objTable.Rows(C1).Item("trnsjbelidtlunitoid")) & "','" & Tchar(objTable.Rows(C1).Item("trnsjbelidtlnote")) & "', '" & refnamepost & "','" & sCabang & "'," & objTable.Rows(C1).Item("typeberat") & "," & objTable.Rows(C1).Item("berat") & "," & objTable.Rows(C1).Item("hargaekspedisi") & "," & objTable.Rows(C1).Item("ekspedisi") & ",'" & sBonus & "','" & aCabang.Text & "','" & sCabang & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        '##### Last Update ####
                    Next
                    'update oid in ql_mstoid where tablename = 'QL_trnsjbelidtl'
                    sSql = "update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(trnbelidtloid.Text)) & " where tablename = 'QL_trnsjbelidtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                xCmd.Connection.Close()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1)
                posting.Text = "In Process"
                Exit Sub
            End Try
            '-----------------------------------------------------------------------------
        Else
            'update tabel    
            'Dim oidTemp As Int64 = GenerateID("QL_HistHPP", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction

            Dim dtTempWeb As DataTable = checkApprovaldata("ql_trnsjbelimst", "In Approval", oid.Text, "", "New")
            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            trnbelidtloid.Text = GenerateID("QL_trnsjbelidtl", CompnyCode)
            conmtroid = GenerateID("QL_conmtr", CompnyCode)
            crdmtroid = GenerateID("QL_crdmtr", CompnyCode)

            sSql = "SELECT id_sn FROM QL_Mst_SN WHERE last_trans_type = 'Beli' AND trnbelimstoid = '" & oid.Text & "'"
            Dim dtSNHapus As DataTable = cKon.ambiltabel(sSql, "dtSNHapus")
            'If posting.Text = "In Approval" Then
            If posting.Text = "In Approval" Then
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnsjbelimst' and branch_code Like '%" & dd_branch.SelectedValue & "%' order by approvallevel"

                Dim dtData2 As DataTable = cKon.ambiltabel2(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                    appv = Session("TblApproval")
                Else
                    showMessage("Maaf, User approval belum di setting, Silahkan hubungi admin..!!", CompnyName & " - WARNING", 2)
                    Exit Sub
                End If
            End If

            'End If

            Session("TblApproval") = appv
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try
                If posting.Text = "In Approval" Then
                    Dim objTable As DataTable
                    objTable = Session("TblApproval")
                    If Not Session("TblApproval") Is Nothing Then
                        For c1 As Integer = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus, branch_code) VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", '" & "LPB" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_trnsjbelimst', '" & Session("oid") & "','In Approval','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1', '" & objTable.Rows(c1).Item("approvalstatus") & "', '" & dd_branch.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + _
                             objTable.Rows.Count - 1 & " where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If
                ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
                sSql = "SELECT trnsjbelistatus FROM QL_trnsjbelimst WHERE trnsjbelioid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                Dim srest As String = xCmd.ExecuteScalar
                If srest Is Nothing Or srest = "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage("Data PDO tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", CompnyName & " - WARNING", 2)
                    trnsjbelistatus.Text = "In Process" : Exit Sub
                Else
                    If srest.ToLower = "in approval" Or srest.ToLower = "approved" Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("Status transaksi PDO sudah terupdate!<br />Tekan Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!", CompnyName & " - WARNING", 2)
                        trnsjbelistatus.Text = "In Process" : Exit Sub
                    End If

                End If

                sSql = "UPDATE QL_trnsjbelimst SET trnsjbelidate='" & CDate(toDate(trnsjreceivedate.Text)) & "',trnsjbelino='" & trnsjbelino.Text & "',trnbelipono= '" & Tchar(orderno.Text) & "' ,trnsjbelinote='" & Tchar(notes.Text) & "',trnsjbelistatus='" & posting.Text & "',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP,trnsjbelitype='" & trnsjbelitype.SelectedValue & "',finalapprovaluser='" & approvaluser.Text & "',finalapprovaldatetime='01/01/1900', branch_code='" & sCabang & "', frombranch='" & aCabang.Text & "' WHERE cmpcode='" & CompnyCode & "' and trnsjbelioid=" & oid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    'DELETE QL_trnsjbelidtl
                    sSql = "Delete from QL_trnsjbelidtl Where trnsjbelioid = " & oid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'insert tabel detail
                    Dim objTable As DataTable : objTable = Session("TblDtl")
                    'INSERT TO QL_trnsjjualdtl
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        Dim cek As String = ""
                        'before insert into connmtr, set dahulu masuk mtrref mana?
                        Dim refnamepost As String = "QL_MSTITEM"
                        sSql = "Select bonus FROM QL_podtl Where trnbelidtloid=" & objTable.Rows(C1).Item("trnpodtloid") & " AND itemoid=" & objTable.Rows(C1).Item("matoid") & ""
                        xCmd.CommandText = sSql : Dim sBonus As String = xCmd.ExecuteScalar()

                        sSql = "INSERT INTO QL_trnsjbelidtl(cmpcode,trnsjbelidtloid,trnsjbelioid,trnbelidtloid,trnsjbelidtlseq,mtrlocoid,refoid,qty,unitoid,trnsjbelinotedtl, refname, branch_code,typeberat,berat,hargaekspedisi,biayaExpedisi,bonus,frombranch,tobranch)" & _
                        " VALUES ('" & CompnyCode & "'," & (CInt(trnbelidtloid.Text) + C1) & "," & oid.Text & "," & objTable.Rows(C1).Item("trnpodtloid") & "," & C1 + 1 & "," & matLoc.SelectedValue & "," & objTable.Rows(C1).Item("matoid") & "," & objTable.Rows(C1).Item("trnsjbelidtlqty") & ",'" & Tchar(objTable.Rows(C1).Item("trnsjbelidtlunitoid")) & "','" & Tchar(objTable.Rows(C1).Item("trnsjbelidtlnote")) & "', '" & refnamepost & "','" & sCabang & "'," & objTable.Rows(C1).Item("typeberat") & "," & objTable.Rows(C1).Item("berat") & "," & objTable.Rows(C1).Item("hargaekspedisi") & "," & objTable.Rows(C1).Item("ekspedisi") & ",'" & sBonus & "','" & aCabang.Text & "','" & sCabang & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        'check update ref detail to complete when posting
                    Next

                    'update oid in ql_mstoid where tablename = 'QL_trnsjjualdtl'
                    sSql = "update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(trnbelidtloid.Text)) & " Where tablename = 'QL_trnsjbelidtl' and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '========================Upload File Edit =====================
                    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    Dim idItem As String = matoid.Text
                    'Session("lokasiFile") = FileExel.PostedFile.FileName
                    Dim QueryKu As String
                    If Label19.Text <> "" Then

                        If Not Session("TblDtl") Is Nothing Then
                            objTable = Session("TblDtl")
                            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                                If objTable.Rows(C1).Item("SNList") = "Y" Then
                                    Dim itemid As String = objTable.Rows(C1).Item("matoid")
                                    QueryKu = "update ql_mstitem set has_SN = " & 1 & " where cmpcode='" & CompnyCode & "' and itemoid = " & itemid
                                    xCmd.CommandText = QueryKu : xCmd.ExecuteNonQuery()
                                End If
                            Next
                        End If

                        If CheckBoxSN.Checked = True Then
                            QueryKu = "update QL_pomst set Link_File = '" & SaveLocation & "' Where cmpcode='" & CompnyCode & "' and trnbelipono= '" & orderno.Text & "'"
                            xCmd.CommandText = QueryKu : xCmd.ExecuteNonQuery()

                            SaveLocation = Label19.Text
                            'Server.MapPath("FileExcel") & "\" & fn
                            ConnStrExe = "provider=Microsoft.jet.OLEDB.4.0; Data Source=" & SaveLocation & "; Extended Properties=Excel 8.0;"
                            connexc = New OleDbConnection(ConnStrExe)

                            If connexc.State = ConnectionState.Closed Then
                                connexc.Open()
                                QueryKu = "select * from [SN_Upload$] where itemcode <> '' and sn <> '' "
                                adap = New OleDbDataAdapter(QueryKu, connexc)
                                Dim dtecel As New DataTable
                                adap.Fill(dtecel)
                                connexc.Close()

                                For dtsn As Integer = 0 To dtSNHapus.Rows.Count - 1
                                    QueryKu = "Delete from QL_mstitemDtl where sn = '" & dtSNHapus.Rows(dtsn).Item("id_sn") & "'"
                                    xCmd.CommandText = QueryKu : xCmd.ExecuteNonQuery()
                                    QueryKu = "Delete from QL_Mst_SN where id_sn = '" & dtSNHapus.Rows(dtsn).Item("id_sn") & "'"
                                    xCmd.CommandText = QueryKu : xCmd.ExecuteNonQuery()
                                Next

                                For i As Integer = 0 To dtecel.Rows.Count - 1
                                    sSql = "SELECT itemoid FROM QL_mstItem WHERE itemcode = '" & dtecel.Rows(i).Item(0) & "'"
                                    xCmd.CommandText = sSql
                                    Dim TampungItemID As Integer = xCmd.ExecuteScalar
                                    sSql = "insert into QL_mstitemDtl (cmpcode, itemoid, itemcode, sn, status_item,branch_code, locoid, createtime, createuser, status_in_out, last_trans_type) values ('" & CompnyCode & "', '" & TampungItemID & "','" & dtecel.Rows(i).Item(0) & "','" & dtecel.Rows(i).Item(1) & "', '" & posting.Text & "', '" & sCabang & "','" & matLoc.SelectedValue & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "', 'IN','Beli')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                    sSql = "insert into Ql_mst_sn  (id_sn, itemcode, trnbelimstoid, tglmasuk, status_in_out, status_approval, last_trans_type) values ('" & dtecel.Rows(i).Item(1) & "','" & dtecel.Rows(i).Item(0) & "'," & oid.Text & ",CURRENT_TIMESTAMP, 'IN', '" & posting.Text & "','Beli')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                Next
                            End If
                        End If
                    End If

                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                xCmd.Connection.Close()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1)
                posting.Text = "In Process"
                Exit Sub
            End Try
        End If
        'Session("TblDtl") = Nothing : Session("oid") = Nothing
        Response.Redirect("~\Transaction\trnsjbeli.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim objTable As DataTable
        If oid.Text.Trim = "" Then
            showMessage("Please fill General ID!", CompnyName & " - ERROR", 1)
            Exit Sub
        End If

        Dim dtTempWeb As DataTable = checkApprovaldata("ql_trnsjbelimst", "In Approval", oid.Text, "", "New")
        'cek data sudah di send?
        If checkApproval("QL_trnsjbelimst", "In Approval", Session("oid"), "New", "FINAL", Session("branch_id")) > 0 Then
            showMessage("This Purchase Delivery Order has been send for Approval", CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        sSql = "SELECT id_sn FROM QL_Mst_SN WHERE last_trans_type = 'Beli' AND trnbelimstoid = '" & oid.Text & "'"
        Dim dtSNHapus As DataTable = cKon.ambiltabel(sSql, "dtSNHapus")

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
            ' Update status Approval if exist to DELETED
            If dtTempWeb.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtTempWeb.Rows.Count - 1
                    sSql = "UPDATE ql_approval SET statusrequest='Deleted' WHERE cmpcode = '" & CompnyCode & _
                        "' AND approvaloid='" & dtTempWeb.Rows(C1)("approvaloid").ToString & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            For dtsn As Integer = 0 To dtSNHapus.Rows.Count - 1
                sSql = "Delete from QL_mstitemDtl where sn = '" & dtSNHapus.Rows(dtsn).Item("id_sn") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Delete from QL_Mst_SN where id_sn = '" & dtSNHapus.Rows(dtsn).Item("id_sn") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            sSql = "delete from QL_trnsjbelidtl where cmpcode='" & CompnyCode & "' and trnsjbelioid=" & oid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from  QL_trnsjbelimst  where cmpcode='" & CompnyCode & "' and trnsjbelioid=" & oid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If Not Session("TblDtl") Is Nothing Then
                objTable = Session("TblDtl")
                For C1 As Int16 = 0 To objTable.Rows.Count - 1

                    Dim itemid As String = objTable.Rows(C1).Item("matoid")
                    sSql = "SELECT count(-1) FROM QL_mstitemDtl WHERE itemoid = '" & itemid & "'"
                    xCmd.CommandText = sSql : Dim Item_po As Integer = xCmd.ExecuteScalar
                    If Item_po = 0 Then
                        sSql = "update ql_mstitem set has_SN = " & 0 & " where cmpcode='" & CompnyCode & "' and itemoid = " & itemid
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Next
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try

        Session("tbldtl") = Nothing : Session("oid") = ""
        Response.Redirect("~\Transaction\trnsjbeli.aspx?awal=true")
    End Sub

    Protected Sub btnsearhorder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearhorder.Click
        Dim view As String = "view"
        BindDataOrder("", view)
        PanelPO.Visible = True : btnHidepo.Visible = True
        mpepo.Show()
    End Sub

    Protected Sub lkbClosePO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtFilterPO.Text = ""
        cProc.SetModalPopUpExtender(btnHidepo, PanelPO, mpepo, False)
        ' PanelPO.Visible = False : btnHidepo.Visible = False : mpepo.Hide()
        PanelSF.Visible = False : btnHidesf.Visible = False : mpesf.Hide()
        cProc.DisposeGridView(gvListPO)
    End Sub

    Protected Sub gvReference_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReference.SelectedIndexChanged
        LabelKode.Text = gvReference.SelectedDataKey.Item("itemcodeb")
        matname.Text = gvReference.SelectedDataKey.Item("matlongdesc")
        matoid.Text = gvReference.SelectedDataKey.Item("matoid")
        refered.Text = ToMaskEdit(ToDouble(gvReference.SelectedDataKey.Item("qty")), 4)
        acum.Text = ToMaskEdit(ToDouble(gvReference.SelectedDataKey.Item("trnsjbelidtlqty")), 4)
        CheckUnit()
        trnsjbelidtlunit.Text = gvReference.SelectedDataKey.Item("unit")
        IdDetail.Text = gvReference.SelectedDataKey.Item("podtloid")
        pounit.Text = gvReference.SelectedDataKey("satuan").ToString
        trnbelidtlunit.Text = gvReference.SelectedDataKey.Item("unit")
        terkirim.Text = ToMaskEdit(ToDouble(refered.Text) - ToDouble(acum.Text), 1)
        cProc.SetModalPopUpExtender(btnHideRef, Panel2, mpeRef, False)
        cProc.DisposeGridView(gvReference)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
        btnAddtolist.Visible = True
        tbldtl.Columns(11).Visible = True
        tbldtl.SelectedIndex = -1
        GVSN.Visible = False
    End Sub

    Protected Sub btnfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfind.Click
        If chkPeriod.Checked Then
            Dim st1, st2 As Boolean : Dim sMsg As String = ""
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text)) : st1 = True
            Catch ex As Exception
                sMsg &= "- Invalid Start date!!<BR>" : st1 = False
            End Try
            Try
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text)) : st2 = True
            Catch ex As Exception
                sMsg &= "- Invalid End date!!<BR>" : st2 = False
            End Try
            If st1 And st2 Then
                If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                    sMsg &= "- End date can't be less than Start Date!!"
                End If
            End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim sPlus As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilterSJ.Text) & "%' "
        Session("SearchPDO") = sqlTempSearch
        BindData(sPlus)
    End Sub

    Protected Sub btnviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnviewall.Click
        Session("FilterText") = "" : Session("FilterDDL") = "0"
        ddlFilter.SelectedIndex = 0 : txtFilterSJ.Text = ""
        chkPeriod.Checked = False : chkStatus.Checked = False
        DDLStatus.SelectedIndex = 0
        FilterPeriod1.Text = Format(GetServerTime, "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
        BindData("")
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()
        If Session("cekkondimsg") = "y" Then
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        End If
        Session("cekkondimsg") = ""
    End Sub 

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindItem.Click
        If trnbelimstoid.Text = "" Then
            showMessage("Please choose Purchase Order first!!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1
        InitDDLCat1()
        FilterTextListMat.Text = "" : cbCat01.Checked = False
        cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        Session("TblListMat") = Nothing
        Session("TblListMatView") = Nothing
        gvListMat.DataSource = Session("TblListMat")
        gvListMat.DataBind() : BindMaterial()
        pnlListMat.Visible = True : btnHideListMat.Visible = True
        mpeListMat.Show()
    End Sub

    Protected Sub gvListSF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clearRef() : ClearDetail()
        orderno.Text = gvListSF.SelectedDataKey.Item(1)
        trnbelimstoid.Text = gvListSF.SelectedDataKey.Item(0)
        suppname.Text = gvListSF.SelectedDataKey.Item(3)
        trnsuppoid.Text = gvListSF.SelectedDataKey.Item(4)
        PanelSF.Visible = False : btnHidesf.Visible = False : mpesf.Hide()
    End Sub

    Protected Sub trnsjbelitype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

    Protected Sub gvListPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListPO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub gvListSF_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListSF.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub tbldtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbldtl.SelectedIndexChanged
        If Session("TblDtl") Is Nothing = False Then
            'SNList
            trnsjbelidtlseq.Text = tbldtl.SelectedDataKey(0).ToString().Trim
            I_u2.Text = "Update Detail"
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "trnsjbelidtlseq=" & trnsjbelidtlseq.Text

            'If dv.Item(0).Item("SNList").ToString <> "Y" Then
            '    btnAddtolist.Visible = True
            '    trnsjbelidtlqty.Enabled = True
            '    trnsjbelidtlqty.CssClass = "inpText"
            'Else
            '    btnAddtolist.Visible = True
            '    trnsjbelidtlqty.Enabled = False
            '    trnsjbelidtlqty.CssClass = "inpTextDisabled"
            'End If

            'insert/update to list data
            matoid.Text = dv.Item(0).Item("matoid").ToString
            matname.Text = dv.Item(0).Item("item").ToString
            LabelKode.Text = dv.Item(0).Item("KodeBR").ToString
            refered.Text = ToMaskEdit(dv.Item(0).Item("referedqty").ToString, 4)
            bonus.Text = dv.Item(0).Item("bonus").ToString
            acum.Text = ToMaskEdit(dv.Item(0).Item("receivedqty").ToString, 4)
            'trnbelidtlunit.Text = dv.Item(0).Item("trnbelidtlunit").ToString
            pounit.Text = dv.Item(0).Item("unit").ToString
            'batchoid.Text = dv.Item(0).Item("batchoid").ToString
            'suppbatch.Text = dv.Item(0).Item("suppbatch").ToString
            matLoc.SelectedValue = dv.Item(0).Item("matloc").ToString
            trnsjbelidtlqty.Text = ToMaskEdit(dv.Item(0).Item("trnsjbelidtlqty").ToString, 4)
            trnsjbelidtlunit.Text = dv.Item(0).Item("trnsjbelidtlunitoid").ToString
            satuan.Text = dv.Item(0).Item("unit").ToString
            trnbelidtlunit.Text = dv.Item(0).Item("podtlunit").ToString
            'qty2.Text = ToMaskEdit(dv.Item(0).Item("qty2").ToString, 4)
            'unit2.Text = dv.Item(0).Item("unit2").ToString
            unit2desc.Text = dv.Item(0).Item("unit2desc").ToString
            CheckUnit()
            trnsjbelidtlnote.Text = dv.Item(0).Item("trnsjbelidtlnote").ToString
            'tolerance.Text = ToMaskEdit(dv.Item(0).Item("tolerance").ToString, 4)
            terkirim.Text = ToMaskEdit(ToDouble(refered.Text) - ToDouble(acum.Text), 1)
            IdDetail.Text = dv.Item(0).Item("trnpodtloid").ToString
            If IsDBNull(dv.Item(0).Item("hargaekspedisi")) Then
                Expedisiharga.Text = 0
            Else
                Expedisiharga.Text = ToMaskEdit(dv.Item(0).Item("hargaekspedisi").ToString, 4)
            End If
            If dv.Item(0).Item("typeBerat") = 2 Or dv.Item(0).Item("typeBerat") = 3 Then
                beratBarang.Text = dv.Item(0).Item("berat")
                beratVolume.Text = dv.Item(0).Item("berat")
            Else
                If dv.Item(0).Item("typeBerat") = 0 Then
                    cb1.Checked = True : beratBarang.Text = dv.Item(0).Item("berat")
                    beratVolume.Enabled = False : beratVolume.CssClass = "inpTextDisabled"
                Else
                    cb2.Checked = True : beratVolume.Text = dv.Item(0).Item("berat")
                    beratBarang.Enabled = False : beratBarang.CssClass = "inpTextDisabled"
                End If
            End If
            costEkspedisi.Text = dv.Item(0).Item("hargaekspedisi")
            Dim dQTY As Double = ToDouble(refered.Text) - ToDouble(acum.Text)
            'Dim dRange As Double = (ToDouble(tolerance.Text) / 100) * dQTY
            Dim dReal As Double = ToDouble(trnsjbelidtlqty.Text)
            If dReal < dQTY And dReal >= dQTY Then
                chkCompleted.Visible = True : lblTolInfo.Visible = True
                chkCompleted.Checked = dv.Item(0).Item("setascomplete")
            Else
                chkCompleted.Visible = False : lblTolInfo.Visible = False
                chkCompleted.Checked = False
            End If
            texthas_sn.Text = tbldtl.Rows(tbldtl.SelectedIndex).Cells(12).Text
            dv.RowFilter = ""
            tbldtl.Columns(16).Visible = False
            'End If

            'sSql = "select id,id_sn from ql_Trans_sn where id='" & tbldtl.SelectedDataKey("trnsjbelidtlseq") & "'"
            'Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "table")

            If Label19.Text <> "" Then
                Dim SaveLocation As String = Label19.Text
                Dim ConnStrExe As String = "provider=Microsoft.jet.OLEDB.4.0; Data Source=" & SaveLocation & "; Extended Properties=Excel 8.0;"
                Dim connexc As New OleDbConnection(ConnStrExe)

                Dim AdapExcel As OleDbDataAdapter
                Dim dtexcel As New DataTable

                If connexc.State = ConnectionState.Closed Then
                    connexc.Open()

                    sSql = "select itemcode, sn from [SN_Upload$] where itemcode = '" & tbldtl.SelectedDataKey("KodeBR").ToString & "'"
                    AdapExcel = New OleDbDataAdapter(sSql, connexc)
                    AdapExcel.Fill(dtexcel)
                    connexc.Close()
                    GVSN.DataSource = dtexcel
                    GVSN.DataBind()
                End If
            End If

        End If
    End Sub

    Protected Sub trnsjbelidtlqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnsjbelidtlqty.TextChanged
        If matoid.Text = "" Then Exit Sub
        Dim dQTY As Double = ToDouble(refered.Text) - ToDouble(acum.Text)
        'Dim dRange As Double = (ToDouble(tolerance.Text) / 100) * dQTY
        Dim dReal As Double = ToDouble(trnsjbelidtlqty.Text)
        If dReal < dQTY And dReal >= dQTY Then
            chkCompleted.Visible = True : lblTolInfo.Visible = True
        Else
            chkCompleted.Visible = False : lblTolInfo.Visible = False : chkCompleted.Checked = False
        End If
        If trnsjbelidtlunit.Text = unit2.Text Then
            qty2.Text = ToMaskEdit(ToDouble(trnsjbelidtlqty.Text), 4)
        End If
        If ToDouble(trnsjbelidtlqty.Text) > (ToDouble((ToDouble(refered.Text))) - ToDouble(acum.Text)) Then
            'sender.Text = ToMaskEdit(ToDouble(currentstock.Text), 1)
            showMessage("Quantity Must be Less than Data PO Receive  !!", CompnyName & " - WARNING", 2)
        End If
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideRef, Panel2, mpeRef, False)
        cProc.DisposeGridView(gvReference)
    End Sub

    Protected Sub gvReference_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReference.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub tbldtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 4)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 4)
        End If
    End Sub

    Protected Sub tbldtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl.RowDeleting
        Dim iIndex As Int16 = e.RowIndex
        Dim objTable As DataTable = Session("TblDtl")
        objTable.Rows.RemoveAt(iIndex)

        'resequence sjDtlsequence 
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("trnsjbelidtlseq") = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        tbldtl.Visible = True

        tbldtl.DataSource = objTable
        tbldtl.DataBind()
        ClearDetail()

        Dim tbTampungSn As New DataTable
        tbTampungSn.Clear()
        GVSN.DataSource = tbTampungSn
        GVSN.DataBind()

        'Dim country As String = tbldtl.Rows(e.RowIndex).Cells(8).Text
        'If country = "Y" Then
        '    showMessage("Data Menggunakan SN Tidak Bisa Di Hapus", CompnyCode, 4)
        '    e.Cancel = True
        'Else

        'End If
        'CheckIfNeedApproval()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        clearRef() : ClearDetail()
    End Sub

    Protected Sub imbClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearItem.Click
        ClearDetail()
        tbldtl.SelectedIndex = -1
    End Sub

    Protected Sub gvTblData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTblData.PageIndexChanging
        gvTblData.PageIndex = e.NewPageIndex
        Dim sPlus As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilterSJ.Text) & "%' "
        Session("SearchPDO") = sqlTempSearch
        BindData(sPlus)
    End Sub

    Protected Sub gvTblData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
            e.Row.Cells(5).Text = Format(CDate(e.Row.Cells(5).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("TblDtl") = Nothing : Session("oid") = Nothing
        Response.Redirect("~\Transaction\trnsjbeli.aspx?awal=true")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Try
            PrintSJ(Session("oid"), trnsjbelino.Text, "pdf")
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            PrintSJ(sender.CommandArgument, sender.ToolTip, "pdf")
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnSJJual.aspx?awal=true")
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("SearchPDO") Is Nothing = False Then
            BindLastSearch()
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reportSJ.Close() : reportSJ.Dispose()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ' Search User ID
        BindDataUser()
        pnlListUser.Visible = True : btnHideUser.Visible = True : mpeUser.Show()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ' Clear User ID
        approvaluser.Text = "" : ApprovalUserName.Text = ""
    End Sub

    Protected Sub lkbCloseUser_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlListUser.Visible = False : btnHideUser.Visible = False : mpeUser.Hide()
        cProc.DisposeGridView(gvListUser)
    End Sub

    Protected Sub gvListUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        approvaluser.Text = gvListUser.SelectedDataKey("userid").ToString
        ApprovalUserName.Text = gvListUser.SelectedDataKey("username").ToString
        pnlListUser.Visible = False : btnHideUser.Visible = False : mpeUser.Hide()
        cProc.DisposeGridView(sender)
    End Sub

    Protected Sub imbSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSendApproval.Click
        If CheckDateIsClosedMtr(CDate(toDate(trnsjreceivedate.Text)), CompnyCode) Then
            showMessage("This Periode was closed !", CompnyName & " - Warning", 2)
            posting.Text = "IN PROCESS"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(CDate(toDate(trnsjreceivedate.Text)), CompnyCode) = False Then
            showMessage("This is not active periode !", CompnyName & " - Warning", 2)
            posting.Text = "IN PROCESS"
            Exit Sub
        End If

        posting.Text = "In Approval"
        'Update_SN()
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If orderno.Text = "" Then
            showMessage("Please Chose Purchase Order first.", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        If budgetfileloc.HasFile Then
            Try
                ' alter path for your project
                budgetfileloc.SaveAs(Server.MapPath("~/FileExcel/ExcelImport.xls"))

                lblMenuPic.Text = "Upload File Name: " & _
                     budgetfileloc.PostedFile.FileName & "<br>" & _
                     "Type: " & _
                     budgetfileloc.PostedFile.ContentType & _
                     " File Size: " & _
                     budgetfileloc.PostedFile.ContentLength & " kb<br>"
                If Right(budgetfileloc.PostedFile.FileName.ToString, 4) <> ".xls" Then
                    lblMenuPic.Text = ""
                    showMessage("Uploaded file must be in Excel (.xls) format !!!", CompnyName & " - WARNING", 2)
                    Exit Sub
                End If
                ' retrieve the Select command for the Spreadsheet
                objDataAdapter.SelectCommand = ExcelConnection("Sheet1")

                ' Populate the DataSet with the spreadsheet worksheet data
                objDataAdapter.Fill(objDataSet, "QL_trnsjbelidtl")

                Session("dataImport") = objDataSet

                File.Delete(Server.MapPath("~/FileExcel/ExcelImport.xls"))

                isidtTable()
                lblMenuPic.Text = "Upload file Success "
                MultiView1.ActiveViewIndex = 1
            Catch ex As Exception
                lblMenuPic.Text = "Error : " & ex.Message.ToString
            End Try
        Else
            lblMenuPic.Text = "Please insert file first "
        End If

    End Sub

    Protected Sub imbFindPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPO.Click
        Dim sPlus As String = "ada"
        BindDataOrder(sPlus, "")
        PanelPO.Visible = True
        btnHidepo.Visible = True
        mpepo.Show()
    End Sub

    Protected Sub imbAlPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFilterPO.Text = ""
        DDLFilterPO.SelectedIndex = 0
        BindDataOrder("", "") : PanelPO.Visible = True
        btnHidepo.Visible = True : mpepo.Show()
    End Sub

    Protected Sub gvListPO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListPO.PageIndex = e.NewPageIndex
        Dim sPlus As String = "ada"
        BindDataOrder(sPlus, "") : PanelPO.Visible = True
        btnHidepo.Visible = True : mpepo.Show()
    End Sub

    Protected Sub CheckBoxSN_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSN.CheckedChanged

        If Page.IsPostBack Then
            TabContainer1.ActiveTabIndex = 1
            If CheckBoxSN.Checked = True Then
                FileExel.Visible = True
                btnuploadexel.Visible = True
                Label19.Visible = True
                btnPreview.Visible = True
            Else
                FileExel.Visible = False
                btnuploadexel.Visible = False
                Label19.Visible = False
                btnPreview.Visible = False
                Label19.Text = ""
            End If
        End If
    End Sub 

    Protected Sub btnuploadexel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuploadexel.Click
        Dim fname As String = getFileName(Trim(trnsjbelino.Text))
        uploadFileGambar("insert", fname)
        'If Label19.Text <> "" Then
        '    Loadupload.Text = "Success Upload"
        'Else
        '    Loadupload.Text = ""
        'End If
    End Sub 

    Protected Sub gvTblData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTblData.SelectedIndexChanged
        Response.Redirect("trnsjbeli.aspx?branch_code=" & gvTblData.SelectedDataKey("branch_code").ToString & "&oid=" & gvTblData.SelectedDataKey("trnsjbelioid") & "")
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnPreview.Click
        Try
            Dim path As String = Server.MapPath("~/report/FileExel_SN.xls") 'get file object as FileInfo
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(path) '-- if the file exists on the server
            Response.AddHeader("Content-Disposition", "attachment; filename=FileExel_SN.xls")
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End() 'if file does not exist
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DDLCat01.SelectedIndexChanged
        InitDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DDLCat02.SelectedIndexChanged
        InitDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DDLCat03.SelectedIndexChanged
        InitDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = ""
                If FilterDDLListMat.SelectedIndex = 0 Then
                    Dim sText() As String = FilterTextListMat.Text.Split(";")
                    For C1 As Integer = 0 To sText.Length - 1
                        If sText(C1) <> "" Then
                            sFilter &= FilterDDLListMat.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
                        End If
                    Next
                    If sFilter <> "" Then
                        sFilter = "(" & Left(sFilter, sFilter.Length - 4) & ")"
                    Else
                        sFilter = "1=1"
                    End If
                Else
                    sFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
                End If

                If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
                    sFilter &= " AND itemcodeb LIKE '" & DDLCat04.SelectedValue & "%'"
                Else
                    If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                        sFilter &= " AND itemcodeb LIKE '" & DDLCat03.SelectedValue & "%'"
                    Else
                        If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                            sFilter &= " AND itemcodeb LIKE '" & DDLCat02.SelectedValue & "%'"
                        Else
                            If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                                sFilter &= " AND itemcodeb LIKE '" & DDLCat01.SelectedValue & "%'"
                            End If
                        End If
                    End If
                End If
                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbCloseListMat.Click
        btnHideListMat.Visible = False
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1
                FilterTextListMat.Text = ""
                InitDDLCat1()
                cbCat01.Checked = False
                cbCat02.Checked = False
                cbCat03.Checked = False
                cbCat04.Checked = False
                Session("TblListMatView") = Session("TblListMat")
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    Dim sErr As String = ""
                    For C1 As Integer = 0 To dv.Count - 1
                        If ToDouble(dv(C1)("matqty").ToString) <= 0 Then
                            sErr = "Maaf, Anda belum input qty received..!!"
                            Exit For

                        End If
                        If ToDouble(dv(C1)("hargaekspedisi").ToString) <= -1 Then
                            sErr = "Maaf, Harga tidak bisa kurang dari Nol!"
                            Exit For
                        End If

                    Next
                    If sErr <> "" Then
                        Session("WarningListMat") = sErr
                        Session("cekkondimsg") = "y"
                        showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                        Exit Sub
                    End If

                    If Session("TblDtl") Is Nothing Then
                        Session("TblDtl") = setTabelDetail()
                    End If

                    Dim objTbl As DataTable = Session("TblDtl")
                    Dim iSeq As Integer = objTbl.Rows.Count + 1
                    Dim objView As DataView = objTbl.DefaultView

                    For j As Integer = 0 To dv.Count - 1
                        If (ToDouble(dv(j)("matqty").ToString)) > ToDouble(dv(j)("qty").ToString) Then
                            Session("WarningListMat") = "Qty Receive tidak boleh lebih besar dari Qty PO"
                            Session("cekkondimsg") = "y"
                            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                            Exit Sub
                        End If
                    Next

                    For j1 As Integer = 0 To dv.Count - 1
                        If dv(j1)("typeberat").ToString = "Global" Then
                            If (ToDouble(dv(j1)("matqty").ToString)) <> ToDouble(dv(j1)("qty").ToString) Then
                                Session("WarningListMat") = "Barang Tsb Tidak Dapat di Parsial Karena Menggunakan Ekspedisi Global"
                                Session("cekkondimsg") = "y"
                                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                                Exit Sub
                            End If
                        End If
                    Next

                    For C1 As Integer = 0 To dv.Count - 1
                        objView.RowFilter = " matoid=" & dv(C1)("matoid") & " AND bonus ='" & dv(C1)("bonus") & "'"
                        If objView.Count > 0 Then
                            objView(0)("matqty") = ToDouble(dv(C1)("matqty"))
                        Else
                            Dim rv As DataRowView = objView.AddNew()
                            rv.BeginEdit()
                            rv("trnsjbelidtlseq") = iSeq
                            rv("trnsjbelidtloid") = 0
                            rv("trnsjbelidtloid") = 0
                            rv("matloc") = matLoc.SelectedValue
                            rv("matoid") = dv(C1)("matoid").ToString
                            rv("item") = dv(C1)("matlongdesc").ToString
                            rv("refname") = "QL_MSTITEM"
                            rv("trnsjbelidtlunitoid") = dv(C1)("unit").ToString
                            rv("trnsjbelidtlnote") = ""
                            rv("referedqty") = ToMaskEdit(dv(C1)("qty").ToString, 4)
                            rv("receivedqty") = ToMaskEdit(dv(C1)("trnsjbelidtlqty").ToString, 4)
                            rv("location") = matLoc.SelectedItem.Text
                            rv("unit") = 0
                            rv("podtlunit") = dv(C1)("unit").ToString
                            rv("unit") = dv(C1)("satuan").ToString
                            rv("unit2desc") = ""
                            rv("trnpodtloid") = dv(C1)("podtloid").ToString
                            rv("KodeBR") = dv(C1)("itemcodeb").ToString
                            rv("matoid") = dv(C1)("matoid").ToString
                            rv("SNList") = dv(C1)("has_sn").ToString
                            rv("trnsjbelidtlqty") = ToDouble(dv(C1)("matqty").ToString)
                            'If ToDouble(dv(C1)("harga")) = 0.0 Then
                            '    rv("harga") = ToDouble(0.0)
                            'Else
                            '    rv("harga") = 0
                            'End If
                            rv("bonus") = dv(C1)("bonus").ToString

                            If dv(C1)("typeberat").ToString = "Berat Volume" Then
                                If dv(C1)("hargaekspedisi").ToString = 0 Then
                                    rv("ekspedisi") = dv(C1)("hargaekspedisi").ToString
                                    rv("berat") = dv(C1)("berat").ToString
                                Else
                                    rv("ekspedisi") = dv(C1)("hargaekspedisi").ToString * (dv(C1)("berat").ToString / 1000000) * dv(C1)("matqty").ToString
                                    rv("berat") = dv(C1)("berat").ToString
                                End If
                            ElseIf dv(C1)("typeberat").ToString = "Berat Barang" Then
                                If dv(C1)("hargaekspedisi").ToString = 0 Then
                                    rv("berat") = dv(C1)("berat").ToString
                                    rv("ekspedisi") = dv(C1)("hargaekspedisi").ToString
                                Else
                                    rv("ekspedisi") = dv(C1)("berat").ToString * dv(C1)("matqty").ToString * dv(C1)("hargaekspedisi").ToString
                                    rv("berat") = dv(C1)("berat").ToString

                                End If
                            Else
                                rv("berat") = dv(C1)("berat").ToString
                                rv("ekspedisi") = dv(C1)("ekspedisi").ToString

                            End If
                            rv("hargaekspedisi") = dv(C1)("hargaekspedisi").ToString
                            If dv(C1)("typeberat").ToString = "Berat Barang" Then
                                rv("typeberat") = 0
                            ElseIf dv(C1)("typeberat").ToString = "Berat Volume" Then
                                rv("typeberat") = 1
                            Else
                                If dv(C1)("ekspedisi").ToString <> 0 Then
                                    rv("typeberat") = 3
                                Else
                                    rv("typeberat") = 2
                                End If
                            End If
                            rv.EndEdit()
                            iSeq += 1
                        End If
                        objView.RowFilter = ""
                    Next
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    objTbl.AcceptChanges()
                    Session("TblDtl") = objTbl
                    tbldtl.DataSource = Session("TblDtl")
                    tbldtl.DataBind()

                Else
                    Session("WarningListMat") = "Maaf, Silahkan klik add to list dulu..!!"
                    Session("cekkondimsg") = "y"
                    showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)

                End If
            End If
        End If

    End Sub

    Protected Sub ButtonA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonA.Click, upListMat.DataBinding
        Dim objTbl As DataTable = Session("TblDtl")
        objTbl.AcceptChanges()
        Session("TblDtl") = objTbl
        tbldtl.DataSource = Session("TblDtl")
        tbldtl.DataBind()
    End Sub

    Protected Sub matLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles matLoc.SelectedIndexChanged
        LblCabang.Text = GetStrData("SELECT Case gencode When 'AWL' Then '00' Else g.gencode End Gencode,Case gencode When 'AWL' Then '00' Else g.gencode End+' - '+g.gendesc FROM QL_mstgen g WHERE g.genoid IN (SELECT a.genother2 From QL_mstgen a WHERE a.gengroup = 'Location' AND genoid=" & matLoc.SelectedValue & ")")
    End Sub

    Protected Sub cb1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb1.CheckedChanged
        If cb1.Checked = True Then
            beratBarang.Enabled = True : beratBarang.CssClass = "inpText"
            beratVolume.Enabled = False : beratVolume.CssClass = "inpTextDisabled"
            beratVolume.Text = ""
            cb2.Checked = False
        ElseIf cb2.Checked = True Then
            beratVolume.Enabled = True : beratVolume.CssClass = "inpText"
            beratBarang.Enabled = False : beratBarang.CssClass = "inpTextDisabled"
            cb1.Checked = False
            beratBarang.Text = ""
        Else
        End If
    End Sub

    Protected Sub cb2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If cb2.Checked = True Then
            beratVolume.Enabled = True : beratVolume.CssClass = "inpText"
            beratBarang.Enabled = False : beratBarang.CssClass = "inpTextDisabled"
            cb1.Checked = False
            beratBarang.Text = ""
        Else
            beratBarang.Enabled = True : beratBarang.CssClass = "inpText"
            beratVolume.Enabled = False : beratVolume.CssClass = "inpTextDisabled"

            beratVolume.Text = ""
        End If
    End Sub

    Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPO.SelectedIndexChanged
        clearRef() : ClearDetail()
        'trnsjbelitype.SelectedValue = gvListPO.SelectedDataKey("Type")
        'isengin checker ahhh :D
        'SO BO belum cz nunggu Sales module
        If trnsjbelitype.SelectedValue = "BO" Then
            '---- hasil iseng ditutup
            orderno.Text = gvListPO.SelectedDataKey.Item("pono")
            trnbelimstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid")
            suppname.Text = gvListPO.SelectedDataKey.Item("suppname")
            trnsuppoid.Text = gvListPO.SelectedDataKey.Item("suppoid")
            trnsjbelidate.Text = Format(CDate(gvListPO.SelectedDataKey.Item("podate")), "dd/MM/yyyy")
            aCabang.Text = gvListPO.SelectedDataKey.Item("branch_code")
        Else
            orderno.Text = gvListPO.SelectedDataKey.Item("pono")
            trnbelimstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid")
            suppname.Text = gvListPO.SelectedDataKey.Item("suppname")
            trnsuppoid.Text = gvListPO.SelectedDataKey.Item("suppoid")
            trnsjbelidate.Text = Format(CDate(gvListPO.SelectedDataKey.Item("podate")), "dd/MM/yyyy")
            aCabang.Text = gvListPO.SelectedDataKey.Item("branch_code")
        End If
        cProc.SetModalPopUpExtender(btnHidepo, PanelPO, mpepo, False)
        cProc.DisposeGridView(gvListPO) : generateNo()
        trnsjbelidtlqty.Visible = True : Labelqty.Visible = True
        Label11.Visible = False : Label13.Visible = True
        terkirim.Visible = False
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub dd_branch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dd_branch.SelectedIndexChanged
        LocDLL()
    End Sub

    Protected Sub DDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLStatus.SelectedIndexChanged
        chkStatus.Checked = True
    End Sub

    Protected Sub FilterPeriod1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterPeriod1.TextChanged
        chkPeriod.Checked = True
    End Sub

    Protected Sub BtnPrintExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            PrintSJ(sender.CommandArgument, sender.ToolTip, "excel")
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnSJJual.aspx?awal=true")
    End Sub
#End Region

    Protected Sub BtnToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnToExcel.Click
        Try
            PrintSJ(Session("oid"), trnsjbelino.Text, "excel")
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
    End Sub
End Class