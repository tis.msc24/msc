﻿Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunctionAccounting
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class TrnPaySrv
    Inherits System.Web.UI.Page

#Region "Variables"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = 4
    Public DefCounterCashBankNo As String = "4"
    Public DefaultFormatCounter As Int16 = 4
    Dim conn As New SqlConnection(ConnStr)
    Dim conn2 As New SqlConnection(ConnStr)
    Dim objCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dv As DataView
    Dim dtStaticItem As DataTable
    Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
    Dim objDs As New DataSet
    Dim cKoneksi As New Koneksi
    Dim dp, DPsc As Decimal
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String = ""
    Private cBulan() As Char = {"A", "B", "C", "", "D", "E", "F", "G", "H", "I", "J", "K", "L"}
    Dim dRate As Double
#End Region

#Region "Procedures"

    Sub Fill_payflag()
        lbldpno.Visible = False
        trndpapoid.Visible = False : lbldpbalance.Visible = False
        dpbalance.Visible = False : Session("payflag") = payflag.SelectedValue
        If payflag.SelectedValue = "CASH" Then
            lblPayType.Text = "Cash" : lblpayrefno.Visible = False : payrefno.Visible = False
            lblpayduedate.Visible = False : payduedate.Visible = False : btnDueDate.Visible = False
            lblnotice.Visible = False : lblBankName.Visible = False : ddlBankName.Visible = False
            LblBank.Visible = False : Label4.Visible = False
        ElseIf payflag.SelectedValue = "BANK" Then
            lblpayrefno.Text = "No. Rekening"
            lblBankName.Visible = False : ddlBankName.Visible = False
            lblPayType.Text = "Bank" : lblpayrefno.Visible = True : payrefno.Visible = True
            lblpayduedate.Visible = True : payduedate.Visible = True : btnDueDate.Visible = True
            lblnotice.Visible = True : LblBank.Visible = False : Label4.Visible = False
        ElseIf payflag.SelectedValue = "GIRO" Then
            lblpayrefno.Text = "No. Giro"
            lblPayType.Text = "Giro" : lblpayrefno.Visible = True
            payrefno.Visible = True : lblpayduedate.Visible = True
            payduedate.Visible = True : btnDueDate.Visible = True
            lblnotice.Visible = True : lblBankName.Visible = False ' True ' Buat apa sih ??
            ddlBankName.Visible = False ' True ' Buat apa sih ??
            creditsearch.Visible = True : CREDITCLEAR.Visible = True
            payrefno.Enabled = False : payrefno.CssClass = "inpTextDisabled"
            LblBank.Visible = True : Label4.Visible = True
        ElseIf payflag.SelectedValue = "CREDIT CARD" Then
            lblPayType.Text = "Credit Card" : lblpayrefno.Visible = True : payrefno.Visible = True
            lblpayduedate.Visible = True : payduedate.Visible = True : btnDueDate.Visible = True
            lblnotice.Visible = True : lblBankName.Visible = False ' True ' Buat apa sih ??
            ddlBankName.Visible = False ' True ' Buat apa sih ??
            LblBank.Visible = False : Label4.Visible = False
        ElseIf payflag.SelectedValue = "DP" Then
            lblPayType.Text = "Down Payment" : lblpayrefno.Visible = False : payrefno.Visible = False
            lblpayduedate.Visible = False : payduedate.Visible = False : btnDueDate.Visible = False
            lblnotice.Visible = False : lblBankName.Visible = False : ddlBankName.Visible = False
            lbldpno.Visible = True : trndpapoid.Visible = True : lbldpbalance.Visible = True
            dpbalance.Visible = True : LblBank.Visible = False : Label4.Visible = False
            If trnsuppoid.Text.Trim = "" Then
                'showMessage("Pilih Supplier dahulu !!!", CompnyName & " - INFORMATION", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
        initDDLcashbank(payflag.SelectedValue)
        GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
        If payflag.SelectedValue = "GIRO" Then
            creditsearch.Visible = True : CREDITCLEAR.Visible = True
            If Session("oid") = "" Then
                payrefno.Text = "" : payrefno.Enabled = False : payrefno.CssClass = "inpTextDisabled"
            Else
                creditsearch.Visible = False : CREDITCLEAR.Visible = False
            End If
            AmountGiro.Visible = True
        Else
            AmountGiro.Visible = False
            creditsearch.Visible = False : CREDITCLEAR.Visible = False
            code.Visible = False : code.Text = "" : payrefno.Text = ""
            payrefno.Enabled = True : payrefno.CssClass = "inpText"
        End If

        If Page.IsPostBack Then
            If payflag.SelectedValue = "DP" Then
                InitDPAR(trnsuppoid.Text)
                If trndpapoid.Items.Count > 0 Then
                    FillDPBalance(trndpapoid.SelectedValue)
                    FillDPAccount(trndpapoid.SelectedValue)
                Else
                    FillDPBalance(0)
                End If
            Else
                CurrencyOid.SelectedValue = "1"
                CurrencyOid.Enabled = True
            End If
        End If
        If lblPOST.Text = "POST" Then
            cashbankacctgoid.SelectedValue = Session("cashbankoid")
        End If
    End Sub

    Sub filterGVS(ByVal code As String, ByVal name As String, ByVal cmpcode As String, ByVal flag As String, ByVal branch_code As String)
        sSql = "SELECT custoid AS ID,custcode AS Code, custname AS Name,0 ReqOid FROM QL_mstcust AS c WHERE cmpcode = '" & cmpcode & "' AND (" & DDLSuppID.SelectedValue & " LIKE '%" & Tchar(txtFindSuppID.Text.Trim) & "%') AND custoid IN (select custoid from QL_TRNINVOICE where amtnetto > 0 and SSTATUS = 'POST' and BRANCH_CODE = '" & DDLCabang.SelectedValue & "') ORDER BY custCODE"
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "QL_MSTcust")
        Session("MSTcust") = objTable
        gvSupplier.DataSource = objTable
        gvSupplier.DataBind()
    End Sub

    Private Sub CalculateTotalPayment()
        amtpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 4)
        If amtpayment.Text = 0 Then
            amtpayment.Text = 1
        End If
        If ToDouble(amtpayment.Text) > ToDouble(APAmt.Text) Then
            chkOther.Enabled = False
            chkOther.Checked = True : DDLOtherType.SelectedValue = "+"
            SetOtherAccount(True)
            DDLOtherType_SelectedIndexChanged(Nothing, Nothing)
            otheramt.Text = ToMaskEdit(ToDouble(amtpayment.Text) - ToDouble(amttrans.Text), 4)
            amtdtlselisih.Text = otheramt.Text
        ElseIf ToDouble(amtpayment.Text) < ToDouble(APAmt.Text) Then
            chkOther.Enabled = False
            chkOther.Checked = False : DDLOtherType.SelectedValue = "-"
            DDLOtherType_SelectedIndexChanged(Nothing, Nothing)
            SetOtherAccount(True)
            otheramt.Text = ToMaskEdit(ToDouble(amttrans.Text) - ToDouble(amtpayment.Text), 4)
            amtdtlselisih.Text = otheramt.Text
        Else
            chkOther.Checked = False
            chkOther.Enabled = False
            SetOtherAccount(False)
            otheramt.Text = ToMaskEdit(0, 4)
            amtdtlselisih.Text = otheramt.Text
        End If
        totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 4)
        CalculateSelisih()
    End Sub

    Private Sub CalculateSelisih()
        ' Calculate total Detail Selisih
        If Not Session("DtlSelisih") Is Nothing Then
            Dim objCek As DataTable = Session("DtlSelisih")
            Dim dTotalSelisih As Double = Math.Abs(ToDouble(objCek.Compute("SUM(amtdtlselisih)", "").ToString))
            If dTotalSelisih < 0 Then dTotalSelisih *= -1
            otheramt.Text = ToMaskEdit(dTotalSelisih, 4)
            If ToDouble(amtpayment.Text) < ToDouble(APAmt.Text) Then
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text) + dTotalSelisih, 4)
            Else
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 4)
            End If
        End If
        If ToDouble(amtpayment.Text) <> ToDouble(amttrans.Text) Then
            chkOther.Checked = True
        End If
    End Sub

    Public Sub InitAllDDL()
        FillDDL(CurrencyOid, "select currencyoid,currencycode from QL_mstcurr where cmpcode='" & cmpcode & "' order by currencyoid")
        FillCurrencyRate(CurrencyOid.SelectedItem.Value)
    End Sub

    Private Sub Cabangddl()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLCabang, sSql)
            Else
                FillDDL(DDLCabang, sSql)
                'DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
                'DDLCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLCabang, sSql)
            'DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
            'DDLCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FilterCbg, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FilterCbg, sSql)
            Else
                FillDDL(FilterCbg, sSql)
                FilterCbg.Items.Add(New ListItem("ALL", "ALL"))
                FilterCbg.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(FilterCbg, sSql)
            FilterCbg.Items.Add(New ListItem("ALL", "ALL"))
            FilterCbg.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub BankDdl()
        FillDDL(dd_bankgiro, "Select bankoid,(Select gb.gendesc FROM QL_mstgen gb Where gb.genoid=gd.bankoid AND gengroup ='BANK NAME') Bank From ql_girodtl gd")
    End Sub

    Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 D.curratestoIDRbeli from QL_mstcurrhist D INNER JOIN ql_mstcurr m on M.CMPCODE=D.cmpcode AND m.currencyoid=d.curroid and m.cmpcode='" & cmpcode & "' and currencyoid=" & iOid & " order by d.currdate desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyRate.Text = ToMaskEdit(objCmd.ExecuteScalar, 4)
        conn.Close()
    End Sub

    Public Function InvoiceRate(ByVal curroid As Integer) As Double
        Dim curRate As Double
        If Session("paymentOid") > 0 Then
            sSql = "select isnull((case when c.currencyoid=1 then rate2usdvalue else rate2idrvalue end),0) rateValue from ql_trnjualmst a INNER JOIN ql_trnordermst b on a.orderno = b.orderno left join ql_mstrate2 c on b.rate2oid = c.rate2oid Where a.trnjualmstoid=" & Session("paymentOid") & ""
            objCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            curRate = objCmd.ExecuteScalar
            Return curRate
            conn.Close()
        Else
            curRate = DailyRate()
            Return curRate
        End If
    End Function

    Public Function DailyRate() As Single
        Dim curRate As Single
        sSql = "select top 1 rate2idrvalue from ql_mstrate2 where currencyoid=2 and rate2month = MONTH(GETDATE()) and rate2year = year(getdate()) order by rate2oid desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curRate = objCmd.ExecuteScalar
        Return curRate
        conn.Close()

    End Function

    Function WeeklyRate() As Single
        Dim curRate As Single
        sSql = "select top 1 rateidrvalue from ql_mstrate where currencyoid=2 and  convert(varchar,getdate(),101) between  convert(varchar,ratedate,101) and convert(varchar,ratetodate,101) order by rateoid desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curRate = ToMaskEdit(objCmd.ExecuteScalar, 4)
        Return curRate
        conn.Close()

    End Function

    Function get_dpOid(ByVal trndparno As String, ByVal custoid As Integer) As Integer

        Dim dpoid As Integer

        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "select trdparoid from QL_trndpar where trndparno = '" & trndparno & "' and branch_code='" & Session("branch_id") & "' and custoid=" & custoid & ""
        cmd.CommandType = CommandType.Text

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            dpoid = rdr("trdparoid")
        End While
        Return dpoid
        conn2.Close()

    End Function

    Private Sub CekPaymentCurrencyToInvoice()
        If CurrencyOid.SelectedValue = invCurrOid.Text Then
            If ToDouble(invCurrRate.Text) <> ToDouble(currencyRate.Text) Then
                Session("tbldtl") = Nothing : GVDtlPayAP.DataSource = Nothing : GVDtlPayAP.DataBind()
                currencyRate.Text = ToMaskEdit(ToDouble(invCurrRate.Text), 4)
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub UncheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("cashbankstatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub gridCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If (e.CommandName = "ConfirmPrint") Then
            Response.Redirect("~/reportform/ql_printBK.aspx?paymentoid=" & Trim(e.CommandArgument.ToString()) & "&printtype=8")
        End If
    End Sub

    Private Sub InitOtherAcctg(ByVal sType As String)
        If sType = "+" Then
            FillDDLAcctg(otherAcctgoid, "VAR_PAYAR_DIFFERENCE_+1", DDLCabang.SelectedValue)
            If otherAcctgoid.Items.Count = 0 Then
                showMessage("Silahkan seting VAR_PAYAR_DIFFERENCE_+1 di master interface !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        ElseIf sType = "-" Then
            FillDDLAcctg(otherAcctgoid, "VAR_PAYAR_DIFFERENCE_-1", DDLCabang.SelectedValue)
            If otherAcctgoid.Items.Count = 0 Then
                showMessage("Silahkan seting VAR_PAYAR_DIFFERENCE_-1 di master interface !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
    End Sub

    Public Sub initDDLcashbank(ByVal cashbank As String)
        If lblPOST.Text = "POST" Then
            btnSave.Visible = False
        Else
            btnSave.Visible = True
        End If
        Dim sVar As String = ""
        If Trim(cashbank) = "CASH" Then
            sVar = "VAR_CASH"
        ElseIf Trim(cashbank) = "BANK" Then
            sVar = "VAR_BANK"
        ElseIf Trim(cashbank) = "GIRO" Then
            sVar = "VAR_GIRO_PIUTANG"
        ElseIf Trim(cashbank) = "DP" Then
            sVar = "VAR_DPAR"
        End If
        FillDDLAcctg(cashbankacctgoid, sVar, DDLCabang.SelectedValue)
        If cashbankacctgoid.Items.Count < 1 Then
            showMessage("Isi/Buat account " & sVar & " di master accounting!! " & sVar & " !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
    End Sub

    Public Sub binddata(ByVal fTgl As String)
        Dim st1, st2 As Boolean
        Dim sMsg As String = "" : Dim sPost As String = ""
        Dim sCabang As String = ""
        If IsDate(toDate(txtPeriode1.Text)) Then : st1 = True
        Else : st1 = False : sMsg &= "- Format Periode  salah!!<BR>" : End If
        If IsDate(toDate(txtPeriode2.Text)) Then : st2 = True
        Else : st2 = False : sMsg &= "- Format Periode  salah!!<BR>" : End If
        If st1 And st2 Then
            If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
                sMsg &= "- Last periode must be >= first periode  !!<BR>"
            End If
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If
        If postinge.SelectedValue.Trim.ToUpper <> "ALL" Then
            sPost = "AND upper(cashbankstatus) = '" & postinge.SelectedValue.Trim.ToUpper & "'"
        End If
        If FilterCbg.SelectedValue <> "ALL" Then
            sCabang = "AND cb.branch_code ='" & FilterCbg.SelectedValue & "'"
        End If
        Dim mySqlConn As New SqlConnection(ConnStr)
        sSql = "SELECT DISTINCT cb.cashbankoid,cashbankno,case cashbanktype when 'BKM' then 'CASH' when 'BBM' then 'BANK' when 'BCM' then 'CREDIT CARD' when 'BDM' then 'DP' WHEN 'BGM' then 'GIRO' else 'LAIN-LAIN' end payflag, cashbankdate as date,(SELECT FLOOR(SUM(payamt)) FROM QL_trnpayar p WHERE p.cashbankoid=cb.cashbankoid) AS payamt,(SELECT FLOOR(SUM(payamt)) FROM QL_trnpayar p WHERE p.cashbankoid=cb.cashbankoid) * cb.cashbankcurrate AS payamtrp,cashbankstatus,cashbanknote, cr.currencycode currencycode,cb.cashbankcurrate,'' payrefno,'' bank,cb.pic,c.custname suppliername FROM QL_trncashbankmst cb INNER JOIN QL_mstcurr cr ON cr.cmpcode=cb.cmpcode AND cr.currencyoid=cb.cashbankcurroid INNER JOIN ql_mstcust c ON c.custoid=cb.pic WHERE cb.cmpcode='" & cmpcode & "' AND cashbankgroup='SERVICE' " & fTgl & " AND " & ddlFilter.SelectedValue & " LIKE '%" & FilterText.Text & "%' " & sCabang & "  ORDER BY cashbankoid desc"

        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "ql_trncashbankmst")
        Session("tbldata") = objTable
        GVmstPAYAP.DataSource = objTable
        GVmstPAYAP.DataBind()
        calcTotalInGrid()
    End Sub

    Protected Sub calcTotalInGrid()
        Dim objTable As DataTable : objTable = Session("tbldata")
        Dim gtotal As Double = 0
        For C1 As Integer = 0 To objTable.Rows.Count - 1
            gtotal += ToDouble(objTable.Rows(C1)("payamtrp").ToString)
        Next
        lblgrandtotal.Text = ToMaskEdit(gtotal, 4)
    End Sub

    Protected Sub bindDataPurchasing(ByVal cmpcode As String, ByVal noNotaBeli As String)
        sSql = "SELECT * FROM (" & _
 "Select i.BRANCH_CODE,ISNULL((select reqcode from QL_TRNREQUEST r where r.reqoid = i.MSTREQOID),'') AS reqcode,i.soid trnbelimstoid,(SELECT TOP 1 ar.ACCTGOID FROM ql_conar ar Where i.cmpcode=ar.cmpcode AND ar.reftype='QL_trninvoice' AND i.soid=ar.refoid) AS acctgoid ,i.invoiceno trnbelino,CONVERT(varchar, i.SENDTIME, 103) AS trnbelidate,ISNULL((select reqoid from QL_TRNREQUEST r where r.reqoid = i.MSTREQOID),0) AS reqoid,ISNULL((select reqcustoid from QL_TRNREQUEST r where r.reqoid = i.MSTREQOID),c.custoid) AS reqcustoid,c.custcode,c.custname suppname, i.amtnetto AS amttrans,isnull((SELECT abs(sum(kredit)) kredit FROM ( SELECT ISNULL(bm.invoiceno,'') AS notrans ,ISNULL(ap.amtbayaridr,0) AS kredit FROM QL_conar ap INNER JOIN QL_TRNINVOICE bm ON bm.cmpcode=ap.cmpcode AND bm.SOID=ap.refoid INNER JOIN QL_trnpayar pap ON pap.cmpcode=ap.cmpcode AND pap.paymentoid=ap.payrefoid AND ap.trnartype = 'PAYARSVC' WHERE ap.custoid=i.custoid AND ap.trnarstatus='POST' AND (pap.payflag <> 'OTHER' /*OR (pap.payflag='OTHER' AND pap.payamt < 0)*/) AND ap.refoid=i.SOID  )as dt),0) amtpaid,1 AS currencyoid,CONVERT(varchar, i.SENDTIME, 103) AS payduedate, 'IDR' AS currencyrate, 'IDR' AS currencycode, 'IDR' AS currencydesc, 0.00 amtretur,0.00 trntaxpct,(select reqperson from QL_TRNREQUEST r where r.reqoid = i.MSTREQOID) AS reqperson From QL_TRNINVOICE i INNER JOIN ql_mstcust c On i.custoid=c.custoid AND i.cmpcode=c.cmpcode INNER JOIN ql_conar con On i.SOID = con.refoid AND con.reftype='QL_trninvoice' INNER JOIN ql_mstacctg a On a.acctgoid=con.acctgoid  Where i.amtnetto > 0 AND SSTATUS IN ('POST') AND i.cmpcode='" & cmpcode & "' AND i.custoid= " & trnsuppoid.Text & " and i.branch_code = '" & DDLCabang.SelectedValue & "' ) inv where amttrans - amtpaid > 0"
        FillGV(gvPurchasing, sSql, "QL_TRNINVOICE")
        cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, True)
    End Sub

    Protected Sub ClearDtlAP(ByVal bState As Boolean)
        txtPaymentNo.Text = "" : trnbelino.Text = "" : trnbelimstoid.Text = ""
        suppname.Text = "" : amttrans.Text = "0.00" : amtpaid.Text = "0.00"
        APAmt.Text = "0.00" : APAcc.Text = "" : acctgoid.Text = ""
        amtpayment.Text = "0.00" : totalpayment.Text = "0.00"
        trnTaxPct.Text = "0.00" : TaxAmount.Text = "0.00"
        lblTaxPct.Visible = False : trnTaxPct.Visible = False
        lblAmtTax.Visible = False : TaxAmount.Visible = False
        invCurrOid.Text = "" : invCurrRate.Text = ""
        invCurrCode.Text = "" : invCurrDesc.Text = ""
        reqoid.Text = "" : sParts.Text = "" : sJasa.Text = ""
        amtretur.Text = "0.00" : chkOther.Checked = False : chkOther.Enabled = True
        otherAcctgoid.Enabled = True : otherAcctgoid.CssClass = "inpTextDisabled"
        otheramt.Text = "" : lkbAddDtlSlisih.Visible = False : lkbClearDtlSlisih.Visible = False
        If bState Then
            ' Reset Tabel Detail Selisih
            Session("DtlSelisih") = Nothing
            gvDtlSelisih.DataSource = Nothing
            gvDtlSelisih.DataBind()
            ClearDtlSelisih()
            txtNote.Text = ""
            'GVDtlPayAP.Columns(10).Visible = True
            GVDtlPayAP.SelectedIndex = -1
            I_U2.Text = "New Detail"
        End If
    End Sub

    Public Sub CheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("cashbankstatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Public Sub FillTextBox(ByVal vpayid As String)
        Try
            sSql = "SELECT DISTINCT cb.branch_code,cb.cashbankoid,cashbankno,case cashbanktype when 'BKM' then 'CASH' when 'BBM' then 'BANK' when 'BCM' then 'CREDIT CARD' when 'BDM' then 'DP' WHEN 'BGM' then 'GIRO' else 'LAIN-LAIN' end payflag, cashbankdate,(SELECT FLOOR(SUM(cashbankglamtidr)) FROM QL_cashbankgl p WHERE p.cashbankoid=cb.cashbankoid) AS payamt,(SELECT FLOOR(SUM(cashbankglamtidr)) FROM QL_cashbankgl p WHERE p.cashbankoid=cb.cashbankoid) * cb.cashbankcurrate AS payamtrp,cashbankstatus,cashbanknote, cr.currencycode currencycode,cb.cashbankcurrate, cuk.custname suppname,'' payrefno,cb.pic,cb.cashbankacctgoid,cashbankcurroid,cashbanknote,giroref,cb.upduser,cb.updtime FROM QL_trncashbankmst cb INNER JOIN QL_mstcurr cr ON cr.cmpcode=cb.cmpcode AND cr.currencyoid=cb.cashbankcurroid INNER JOIN ql_mstcust cuk ON cuk.custoid=cb.pic WHERE cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid='" & vpayid & "'"

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objCmd.CommandText = sSql : xreader = objCmd.ExecuteReader
            'Dim dprefoid As Integer
            Dim q As String = String.Empty
            If xreader.HasRows Then
                While xreader.Read
                    DDLCabang.SelectedValue = xreader("branch_code").ToString
                    cashbankoid.Text = xreader("cashbankoid").ToString
                    cashbankno.Text = Trim(xreader("cashbankno")).ToString
                    defcbno.Text = Trim(xreader("cashbankno")).ToString
                    payflag.SelectedValue = Trim(xreader("payflag")).ToString
                    initDDLcashbank(payflag.SelectedValue)
                    cashbankacctgoid.SelectedValue = Trim(xreader("cashbankacctgoid"))
                    HiddenField2.Value = Trim(xreader.GetValue(4))
                    suppnames.Text = Trim(xreader("suppname").ToString)
                    trnsuppoid.Text = Trim(xreader("pic").ToString)
                    If Session("currsess") = False Then
                        CurrencyOid.SelectedValue = xreader("cashbankcurroid").ToString
                    Else
                        CurrencyOid.SelectedValue = CurrencyOid.SelectedValue
                    End If
                    currencyRate.Text = ToMaskEdit(ToDouble(xreader("cashbankcurrate").ToString), 4)
                    cashbanknote.Text = Trim(xreader("cashbanknote"))
                    updUser.Text = xreader("upduser")
                    updTime.Text = xreader("updtime")
                    PaymentDate.Text = Format(CDate(Trim(xreader("cashbankdate").ToString)), "dd/MM/yyyy")
                    
                    lblPOST.Text = Trim(xreader("cashbankstatus"))
                    Session("cashbankoid") = xreader("cashbankacctgoid")
                    Session("giroOid") = xreader("giroref").ToString
                End While
            End If
            conn.Close()
        Catch ex As Exception
            showMessage(ex.ToString & "<BR><BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
        'Fill_payflag()
        If payflag.SelectedValue = "GIRO" Then
            'Dim vamt As Double
            If Session("giroOid") = "" Then
                Session("giroOid") = 0
            End If
            sSql = "Select amount from ql_girodtl where girodtloid = " & Session("giroOid") & ""
            AmountGiro.Text = ToMaskEdit(ToDouble(GetStrData(sSql)), 4)
            AmountGiro.Visible = True
            sSql = "Select bankoid from ql_girodtl Where girodtloid = " & Session("giroOid") & ""
            dd_bankgiro.SelectedValue = GetStrData(sSql)
            dd_bankgiro.Visible = True : dd_bankgiro.Enabled = False
        End If 
        If lblPOST.Text.ToUpper = "POST" Then
            cashbankacctgoid.SelectedValue = Session("cashbankoid")
            btnPosting2.Visible = False : btnDelete.Visible = False
            btnSave.Visible = False
            GVDtlPayAP.Columns(0).Visible = False
        Else
            btnPosting2.Visible = True : btnDelete.Visible = True
            btnSave.Visible = True : GVDtlPayAP.Columns(0).Visible = True
        End If
        defcbno.Text = cashbankno.Text
      
        sSql = "SELECT 1 AS payseq, cb.cashbankoid, bm.SOID AS payrefoid, cb.branch_code, pap.paymentoid, pap.payduedate, pap.payrefno, pap.trndparoid trndpapoid, bm.invoiceno AS trnbelino, pap.payacctgoid, a.acctgcode+' - '+a.acctgdesc AS acctgdesc,bm.amtnetto AS amttrans, (select isnull(SUM(p2.payamtidr),0) From ql_trnpayar p2 INNER JOIN ql_conar cp on cp.payrefoid=p2.paymentoid and cp.reftype='QL_trnpayar' where p2.cmpcode=cb.cmpcode and p2.cashbankoid<>cb.cashbankoid and p2.paystatus='POST' and p2.payrefoid=pap.payrefoid AND p2.custoid=pap.custoid and p2.branch_code=pap.branch_code and p2.payreftype='QL_trninvoice' and rtrim(p2.payflag)='') AS amtpaid, 0.00 AS amtretur, pap.payamt, pap.paynote, bm.SOID AS trnjualmstoid, pap.payflag AS flagdtl, pap.payreftype, s.custname AS suppname,(select r.reqcode from QL_TRNREQUEST r where r.reqoid = bm.MSTREQOID) AS reqcode,(select r.reqperson from QL_TRNREQUEST r where r.reqoid = bm.MSTREQOID) AS reqperson, (select r.reqoid from QL_TRNREQUEST r where r.reqoid = bm.MSTREQOID) AS reqoid, pap.payres1 FROM ql_trnpayar pap INNER JOIN ql_conar ap ON pap.cmpcode=ap.cmpcode AND pap.paymentoid=ap.payrefoid and pap.branch_code=ap.branch_code and ap.custoid = pap.custoid and ap.trnartype = 'PAYARSVC' INNER JOIN QL_mstacctg a ON a.acctgoid = pap.payacctgoid INNER JOIN QL_TRNINVOICE bm ON bm.cmpcode=pap.cmpcode AND ap.refoid=bm.SOID and pap.branch_code=bm.branch_code INNER JOIN ql_trncashbankmst cb ON cb.cmpcode=pap.cmpcode AND cb.cashbankoid=pap.cashbankoid INNER JOIN ql_mstcust s ON bm.cmpcode=s.cmpcode AND bm.custoid=s.custoid WHERE cb.cashbankoid='" & vpayid & "' AND cb.cmpcode='" & cmpcode & "' AND cb.branch_code='" & DDLCabang.SelectedValue & "'"

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet : mySqlDA.Fill(objDs, "dataCost")

        Dim dv As DataView = objDs.Tables("dataCost").DefaultView
        Session("dtlTable") = objDs.Tables("dataCost")
        conn.Close()
        Dim dtlTable As DataTable
        dtlTable = Session("dtlTable")
        For C2 As Integer = 0 To dtlTable.Rows.Count - 1
            Dim ed As DataRow = dtlTable.Rows(C2)
            ed.BeginEdit()
            ed("payseq") += C2
            ed.EndEdit()
        Next
        If dtlTable.Rows.Count > 0 Then
            Session("paymentOid") = dtlTable.Rows(0).Item("trnjualmstoid").ToString
            Payseq.Text = dtlTable.Rows.Count + 1
            Session("ItemLinePayment") = Payseq.Text
            GVDtlPayAP.Visible = True
            Session("tbldtl") = dtlTable
            GVDtlPayAP.DataSource = dtlTable
            GVDtlPayAP.DataBind()
            calcTotalInGridDtl()
        Else
            showMessage("Tidak dapat membuka data detail pembayaran!!!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
    End Sub

    Protected Sub initddlDP(ByVal suppoid As String)
        FillDDL(ddlDPNo, "SELECT trndparoid, trndparno FROM QL_trndpar WHERE cmpcode='" & cmpcode & "' AND custoid=" & suppoid & " AND trndparflag='OPEN'")
    End Sub

    Private Sub BindCust()
        sSql = "SELECT custoid AS ID,custcode AS Code, custname AS Name,0 ReqOid FROM QL_mstcust AS c WHERE cmpcode = '" & cmpcode & "' AND (" & DDLSuppID.SelectedValue & " LIKE '%" & Tchar(txtFindSuppID.Text.Trim) & "%') AND custoid IN (select i.custoid from QL_TRNINVOICE i LEFT JOIN QL_conar c ON c.refoid = i.SOID and c.branch_code = i.BRANCH_CODE and trnartype = 'PAYARSVC' where amtnetto > 0 and SSTATUS = 'POST' and i.BRANCH_CODE = '" & DDLCabang.SelectedValue & "' group by i.custoid, amtnetto having i.amtnetto - ISNULL(SUM(amtbayaridr),0) > 0) ORDER BY custCODE"
        Dim xTableItem2 As DataTable = cKoneksi.ambiltabel(sSql, "QL_MSTcust")
        gvSupplier.DataSource = xTableItem2
        gvSupplier.DataBind()
        gvSupplier.SelectedIndex = -1
        gvSupplier.Visible = True 
    End Sub

#End Region

#Region "Functions"
    Private Function SetTabelDetailSlisih() As DataTable
        Dim nuDT As New DataTable
        nuDT.Columns.Add("selisihseq", Type.GetType("System.Int32"))
        nuDT.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("acctgdesc", Type.GetType("System.String"))
        nuDT.Columns.Add("amtdtlselisih", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("dtlnoteselisih", Type.GetType("System.String"))
        Return nuDT
    End Function

    Private Function SetTabelDetail() As DataTable
        Dim nuDT As New DataTable
        nuDT.Columns.Add("paymentoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("cashbankoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payreftype", Type.GetType("System.String"))
        nuDT.Columns.Add("payrefoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payacctgoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("paynote", Type.GetType("System.String"))
        nuDT.Columns.Add("payamt", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("trnbelino", Type.GetType("System.String"))
        nuDT.Columns.Add("suppname", Type.GetType("System.String"))
        nuDT.Columns.Add("amttrans", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("amtpaid", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("amtretur", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("acctgdesc", Type.GetType("System.String"))
        nuDT.Columns.Add("trntaxpct", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("payseq", Type.GetType("System.Int32")) 
        nuDT.Columns.Add("payduedate", Type.GetType("System.String"))
        nuDT.Columns.Add("payrefno", Type.GetType("System.String"))
        nuDT.Columns.Add("trndpapoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("DPAmt", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("flagdtl", Type.GetType("System.String"))
        nuDT.Columns.Add("reqcode", Type.GetType("System.String"))
        nuDT.Columns.Add("reqperson", Type.GetType("System.Int32"))
        nuDT.Columns.Add("sParts", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("sJasa", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("reqoid", Type.GetType("System.String"))
        nuDT.Columns.Add("payres1", Type.GetType("System.String"))
        Return nuDT
    End Function

    Private Function GetVarCoa(ByVal sVar As String, ByVal Branch As String) As String
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & sVar & "' and interfaceres1='" & Branch & "'", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Try
            GetVarCoa = xCmd.ExecuteScalar
        Catch ex As Exception
            GetVarCoa = "Kode COA tidak ditemukan !!"
        End Try
        conn.Close()
        Return GetVarCoa
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchAP")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchAP") = sqlSearch
            Response.Redirect(Page.AppRelativeVirtualPath) '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' /*AND BRANCH_CODE='" & Session("branch_id") & "'*/")
        Page.Title = CompnyName & " - Payment Service"
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk HAPUS data ini ?');")
        btnPosting2.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk POST data ini ?');")
        I_U.Text = "New Data"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        If Not IsPostBack Then
            initDDLcashbank(payflag.SelectedValue)
            InitAllDDL() : BankDdl() : Cabangddl() : fDDLBranch()
            payflag.SelectedIndex = 0

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                I_U.Text = "Update Data"
                PaymentDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 1
                trndpapoid.Enabled = False : payflag.CssClass = "inpTextDisabled"
                btnSearchSupp.Visible = False : PaymentDate.Enabled = False
                PaymentDate.CssClass = "inpTextDisabled" : btnPayDate.Visible = False
                Fill_payflag()
                FillTextBox(Session("oid"))
                If lblPOST.Text = "POST" Then
                    btnPosting2.Visible = False : btnSave.Visible = False : btnDelete.Visible = False
                    ibtn.Visible = False : payflag.Enabled = False
                Else
                    payflag.Enabled = True : payflag.CssClass = "inpText" : btnPosting2.Visible = True
                    btnSave.Visible = True : btnDelete.Visible = True : ibtn.Visible = True
                End If
            Else
                I_U.Text = "New Data" : lblPOST.Text = "In Process"
                Session("mstoid") = GenerateID("QL_trnpayar", cmpcode)
                txtPaymentNo.Text = Session("mstoid")
                Session("mstoid2") = GenerateID("QL_trncashbankmst", cmpcode)
                cashbankoid.Text = Session("mstoid2")
                btnDelete.Visible = False : btnPrint.Visible = False : lblPOST.Text = ""
                Payseq.Text = 1 : Session("ItemLinePayment") = Payseq.Text
                PaymentDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                updUser.Text = Session("UserID") : updTime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0 : btnPosting2.Visible = False
                Fill_payflag() : payflag_SelectedIndexChanged(Nothing, Nothing)
                btnSearchSupp.Visible = True : trnbelimstoid.Visible = True
            End If
            txtPeriode1.Text = Format(GetServerTime(), "01/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            binddata("")

            Dim objTable As DataTable : objTable = Session("tbldtl")
            GVDtlPayAP.DataSource = objTable : GVDtlPayAP.DataBind()
            calcTotalInGridDtl()
            Dim objTable2 As DataTable : objTable2 = Session("DtlSelisih")
            gvDtlSelisih.DataSource = objTable2 : gvDtlSelisih.DataBind()
        End If

        If lblPOST.Text = "POST" Then
            btnshowCOA.Visible = True : btnPrint.Visible = True
        Else
            btnPrint.Visible = False : btnshowCOA.Visible = False
        End If
        GVmstPAYAP.Columns(8).Visible = True
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal no As String, ByVal type As String)
        'untuk print
        If type = "BBM" Then
            report.Load(Server.MapPath(folderReport & "printBBM.rpt"))
        ElseIf type = "BKM" Then
            report.Load(Server.MapPath(folderReport & "printBKM.rpt"))
        ElseIf type = "BGM" Then
            report.Load(Server.MapPath(folderReport & "printBGM.rpt"))
        ElseIf type = "BDM" Then
            report.Load(Server.MapPath(folderReport & "printBDM.rpt"))
        End If

        report.SetParameterValue("cmpcode", cmpcode)
        report.SetParameterValue("no", no)
        report.SetParameterValue("companyname", "MULTI SARANA COMPUTER")

        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)
        'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no)
        report.Close() : report.Dispose() : Session("no") = Nothing
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payflag.SelectedIndexChanged
        Fill_payflag()
    End Sub

    Protected Sub btnSearchPurchasing_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPurchasing.Click
        If trnsuppoid.Text.Trim = "" Then
            showMessage("Pilih Customer dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        bindDataPurchasing(cmpcode, txtInputNotaBeli.Text)
    End Sub

    Protected Sub gvPurchasing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        chkOther.Checked = False 
        trnbelino.Text = gvPurchasing.SelectedDataKey(1).ToString()
        suppname.Text = gvPurchasing.SelectedDataKey("suppname").ToString()
        acctgoid.Text = gvPurchasing.SelectedDataKey("acctgoid").ToString()
        amttrans.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amttrans").ToString), 4)
        ReqCode.Text = gvPurchasing.SelectedDataKey("reqcode").ToString()
        reqperson.Text = gvPurchasing.SelectedDataKey("reqperson").ToString()
        trnbelimstoid.Text = gvPurchasing.SelectedDataKey("trnbelimstoid").ToString()
        Dim iconaroid As Int64 = ToDouble(GetStrData("select top 1 conaroid from QL_conar where refoid=" & trnbelimstoid.Text & " and reftype='QL_trnjualmst' order by conaroid asc"))
        If ToDouble(acctgoid.Text) = 0 Then
            'insert to conar & get acctgoid piutang from mst interface
            Dim Var_AR_x As String = GetVarCoa("VAR_AR", DDLCabang.SelectedValue)
            acctgoid.Text = GetStrData("select acctgoid from ql_mstacctg where acctgcode ='" & Var_AR_x & "'")
            APAcc.Text = cKoneksi.ambilscalar("SELECT '('+acctgcode+') '+acctgdesc FROM ql_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgoid='" & acctgoid.Text & "'")
        End If
        APAcc.Text = cKoneksi.ambilscalar("SELECT acctgcode+'-'+acctgdesc FROM ql_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgoid='" & acctgoid.Text & "'")
        invCurrOid.Text = gvPurchasing.SelectedDataKey("currencyoid").ToString()
        invCurrRate.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("currencyrate").ToString()), 4)
        invCurrCode.Text = gvPurchasing.SelectedDataKey("currencycode").ToString()
        invCurrDesc.Text = gvPurchasing.SelectedDataKey("currencycode").ToString() & "-" & gvPurchasing.SelectedDataKey("currencydesc").ToString()
        'CekPaymentCurrencyToInvoice()

        amttrans.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amttrans").ToString), 4)
        amtpaid.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amtpaid").ToString), 4)
        amtretur.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amtretur").ToString), 4)
        APAmt.Text = ToMaskEdit(ToDouble(amttrans.Text) - (ToDouble(amtpaid.Text) + ToDouble(amtretur.Text)), 4)
        amtpayment.Text = ToMaskEdit(ToDouble(APAmt.Text), 4)
        totalpayment.Text = ToMaskEdit(ToDouble(APAmt.Text), 4)
        sParts.Text = ToDouble(gvPurchasing.SelectedDataKey("sParts"))
        sJasa.Text = ToDouble(gvPurchasing.SelectedDataKey("sJasa"))
        reqoid.Text = gvPurchasing.SelectedDataKey("reqoid")
        'initddlDP(cKoneksi.ambilscalar("SELECT trncustoid FROM QL_trnjualmst WHERE cmpcode='" & cmpcode & "' AND trnjualmstoid=" & trnbelimstoid.Text))
        CalculateTotalPayment()
        cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, False)
        cProc.DisposeGridView(gvPurchasing)
    End Sub

    Protected Sub calcTotalInGridDtl()
        Dim gtotal As Double = 0
        If Not (Session("tbldtl") Is Nothing) Then
            Dim objTable As DataTable : objTable = Session("tbldtl")
            gtotal = ToDouble(objTable.Compute("SUM(payamt)", "").ToString)
        End If
        amtbelinettodtl.Text = ToMaskEdit(gtotal, 4)
        amtbelinettodtl4.Text = ToMaskEdit(gtotal, 4)
        NetPayment.Text = ToMaskEdit(ToDouble(amtbelinettodtl.Text) - ToDouble(TotalCost.Text), 4)
    End Sub

    Protected Sub ClosePurc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hiddenbtnpur.Visible = False : Panel2.Visible = False : ModalPopupExtender2.Hide()
        gvPurchasing.DataSource = Nothing : gvPurchasing.DataBind()
    End Sub

    Protected Sub GVDtlPayAP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlPayAP.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 4)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 4)
        End If
    End Sub

    Protected Sub GVDtlPayAP_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlPayAP.RowDeleting
        If lblPOST.Text <> "POST" Then
            Dim idx As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("tbldtl")

            ' CEK apakah induk transaksi (utang) ato bukan
            Dim dvTemp As DataView = objTable.DefaultView
            dvTemp.RowFilter = "payseq=" & idx + 1
            ' Get Id Induk 
            Dim idInvoice As Integer = objTable.Rows(e.RowIndex).Item("payrefoid")
            Dim payreftype As String = objTable.Rows(e.RowIndex).Item("payreftype")
            If dvTemp.Count > 0 Then
                If dvTemp(0)("flagdtl") = "OTHER" Then
                    showMessage("Pilih induk transaksi !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If
            dvTemp.RowFilter = ""

            ' DELETE data bila ada
            For C1 As Integer = objTable.Rows.Count - 1 To 0 Step -1
                If objTable.Rows(C1).Item("payrefoid") = idInvoice And objTable.Rows(C1).Item("payreftype").ToString.ToUpper = payreftype.ToUpper Then
                    objTable.Rows.RemoveAt(C1)
                End If
            Next

            'resequence Detial 
            For C2 As Int16 = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C2)
                dr.BeginEdit() : dr("payseq") = C2 + 1 : dr.EndEdit()
            Next

            Session("tbldtl") = objTable
            GVDtlPayAP.DataSource = Session("tbldtl")
            GVDtlPayAP.DataBind()
            Payseq.Text = objTable.Rows.Count + 1
            Session("ItemLinePayment") = Payseq.Text
            calcTotalInGridDtl()
        Else
            e.Cancel = True 'Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Cannot be Deleted!!"))
        End If
    End Sub

    Protected Sub GVDtlPayAP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtlPayAP.SelectedIndexChanged
        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dvTemp As DataView = objTable.DefaultView
        Payseq.Text = GVDtlPayAP.SelectedDataKey.Item("payseq").ToString
        dvTemp.RowFilter = "payseq=" & Payseq.Text
        trnbelimstoid.Text = dvTemp(0)("payrefoid").ToString
        If dvTemp.Count > 0 Then
            If dvTemp(0)("flagdtl") = "OTHER" Then
                showMessage("Pilih induk transaksi", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                dvTemp.RowFilter = ""
                Exit Sub
            End If
        End If
        dvTemp.RowFilter = ""
        ' Get Table Detail Selisih
        gvDtlSelisih.DataSource = Nothing
        gvDtlSelisih.DataBind()
        otheramt.Text = "0.00"
        dvTemp.RowFilter = "payrefoid=" & trnbelimstoid.Text & " AND flagdtl='OTHER'"
        Dim dTotSelisih As Double = 0
        If dvTemp.Count > 0 Then
            Dim tbSelisih As DataTable = SetTabelDetailSlisih()
            For C4 As Integer = 0 To dvTemp.Count - 1
                Dim nuRow As DataRow : nuRow = tbSelisih.NewRow
                nuRow("selisihseq") = C4 + 1
                nuRow("acctgoid") = dvTemp(C4)("payacctgoid").ToString
                nuRow("acctgdesc") = dvTemp(C4)("acctgdesc").ToString
                nuRow("amtdtlselisih") = Math.Abs(ToDouble(dvTemp(C4)("payamt").ToString))
                nuRow("dtlnoteselisih") = dvTemp(C4)("paynote").ToString
                dTotSelisih += ToDouble(dvTemp(C4)("payamt").ToString)
                tbSelisih.Rows.Add(nuRow)
            Next

            chkOther.Checked = True
            ' Cek kurang bayar atau lebih bayar
            If dTotSelisih < 0 Then ' Kurang bayar
                DDLOtherType.SelectedValue = "-"
                chkOther.Enabled = True
                SetOtherAccount(chkOther.Checked)
                InitOtherAcctg(DDLOtherType.SelectedValue)
            Else ' Lebih bayar
                DDLOtherType.SelectedValue = "+"
                chkOther.Enabled = False
                'chkOther_CheckedChanged(Nothing, Nothing)
                SetOtherAccount(chkOther.Checked)
                InitOtherAcctg(DDLOtherType.SelectedValue)
            End If
            otheramt.Text = ToMaskEdit(Math.Abs(ToDouble(dTotSelisih)), 4)
            Session("DtlSelisih") = tbSelisih
            gvDtlSelisih.DataSource = tbSelisih
            gvDtlSelisih.DataBind()
        End If
        dvTemp.RowFilter = ""
        ' Detail Payap
        dvTemp.RowFilter = "payseq=" & Payseq.Text
        trnbelimstoid.Text = dvTemp(0)("payrefoid").ToString
        trnbelino.Text = dvTemp(0)("trnbelino").ToString
        reqperson.Text = dvTemp(0)("reqperson").ToString
        'suppname.Text = dvTemp(0)("suppname").ToString
        acctgoid.Text = dvTemp(0)("payacctgoid").ToString
        APAcc.Text = dvTemp(0)("acctgdesc").ToString
        reqoid.Text = dvTemp(0)("reqoid").ToString
        amttrans.Text = ToMaskEdit(ToDouble(dvTemp(0)("amttrans").ToString), 4)
        amtpaid.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtpaid").ToString), 4)
        amtretur.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtretur").ToString), 4)
        APAmt.Text = ToMaskEdit(ToDouble(amttrans.Text) - (ToDouble(amtpaid.Text) + ToDouble(amtretur.Text)), 4)
        'sParts.Text = ToMaskEdit(ToDouble(dvTemp(0)("sParts").ToString), 4)
        If chkOther.Checked Then
            If DDLOtherType.SelectedValue = "+" Then ' Lebih Bayar
                totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) + Math.Abs(dTotSelisih), 4)
                amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) + Math.Abs(dTotSelisih), 4)
            Else ' Kurang Bayar
                totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 4)
                amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) - Math.Abs(dTotSelisih), 4)
            End If
        Else ' Tanpa Selisih
            totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 4)
            amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 4)
        End If

        If amtpayment.Text = APAmt.Text Then
            chkOther.Enabled = False
        End If

        dvTemp.RowFilter = ""
        GVDtlPayAP.Columns(8).Visible = False
        I_U2.Text = "Update Data"
        CalculateTotalPayment()
        If dTotSelisih <> 0 Then ' lebih bayar
            chkOther.Checked = True
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        GVmstPAYAP.PageIndex = 0
        Dim fTgl As String = ""
        If cbPeriode.Checked = True Then
            fTgl &= "AND cb.cashbankdate>='" & CDate(toDate(txtPeriode1.Text)) & "' and cb.cashbankdate<='" & CDate(toDate(txtPeriode2.Text)) & "'"
        End If
        binddata(fTgl)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        txtPeriode1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
        postinge.SelectedIndex = 0 : ddlFilter.SelectedIndex = 0
        FilterText.Text = "" : FilterCbg.SelectedValue = "ALL"
        cbPeriode.Checked = False
        binddata("")
    End Sub

    Protected Sub cashbankacctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
    End Sub

    Protected Sub btnClearPurchase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP(True)
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvPurchasing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPurchasing.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 4)
        End If
    End Sub

    Protected Sub currencyRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ToDouble(currencyRate.Text) <= 0 Then
            FillCurrencyRate(CurrencyOid.SelectedValue)
        End If
        ClearDtlAP(True) : Session("tbldtl") = Nothing
        GVDtlPayAP.DataSource = Nothing : GVDtlPayAP.DataBind()
    End Sub

    Protected Sub LBPost_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnPosting2_Click(sender, e)
    End Sub

    Protected Sub ibtnacctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub CBTax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initDDLcashbank(payflag.SelectedValue)
    End Sub

    Protected Sub GVmstPAYAP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstPAYAP.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            If e.Row.Cells(7).Text.Trim = "&nbsp;" Then
                e.Row.Cells(7).Text = "In Process"
            End If

            Dim HyperLink1 As HyperLink = CType(e.Row.Cells(0).FindControl("HyperLink1"), HyperLink)
            Dim objGrid As GridView = CType(e.Row.Cells(5).FindControl("gvsubmst"), GridView)
            sSql = "select DISTINCT bm.invoiceno FROM ql_trnpayar pap INNER JOIN ql_conar c ON c.cmpcode=pap.cmpcode AND c.payrefoid=pap.paymentoid  and c.branch_code=pap.branch_code and c.custoid = pap.custoid and c.trnartype = 'PAYARSVC' INNER JOIN QL_TRNINVOICE bm ON bm.cmpcode=pap.cmpcode AND c.refoid=bm.SOID  and pap.branch_code=bm.branch_code INNER JOIN ql_trncashbankmst cb ON cb.cmpcode=pap.cmpcode AND cb.cashbankoid=pap.cashbankoid INNER JOIN ql_mstcust s ON bm.cmpcode=s.cmpcode AND bm.custoid=s.custoid where pap.cashbankoid in (select cashbankoid from QL_trncashbankmst where cashbankno='" & HyperLink1.Text & "')"
            Dim ods As New SqlDataAdapter(sSql, conn2)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)
            objGrid.DataSource = objTablee.DefaultView
            objGrid.DataBind()
        End If
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
        If lblState.Text = "INV" Then
            lblState.Text = "" : ModalPopupExtender2.Show()
        End If
    End Sub

    Protected Sub imbFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataPurchasing(cmpcode, txtInputNotaBeli.Text)
    End Sub

    Protected Sub imbViewAllInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewAllInv.Click
        bindDataPurchasing(cmpcode, "")
        txtInputNotaBeli.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP(True) : ClearDtlSelisih()
        Payseq.Text = Session("ItemLinePayment")
    End Sub

    Protected Sub lkbInfo1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lkbInfo2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lkbDetail2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing : Session("tbldtl") = Nothing
        Response.Redirect("~\Accounting\trnPaySrv.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim strSQL As String
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            strSQL = "Delete From QL_conar Where cmpcode='" & cmpcode & "' AND payrefoid IN (SELECT paymentoid FROM QL_trnpayar WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text & ")"

            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            strSQL = "Delete from QL_trnpayar where cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()


            strSQL = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' and cashbankoid = " & Trim(cashbankoid.Text)
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            strSQL = "DELETE FROM ql_cashbankgl WHERE cmpcode='" & cmpcode & "' and cashbankoid = " & Trim(cashbankoid.Text)
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            Dim dRate2 As Double
            dRate2 = InvoiceRate(Session("invCurrOid"))
            Dim dparacumamt, dparacumamtidr, dparacumamtusd As Double
            If CurrencyOid.SelectedValue = 1 Then
                dparacumamt = ToDouble(amtbelinettodtl.Text)
                dparacumamtidr = ToDouble(amtbelinettodtl.Text)
                If Session("invCurrOid") = 1 Then
                    dparacumamtusd = ToDouble(amtbelinettodtl.Text) * dRate2
                Else

                    dparacumamtusd = ToDouble(amtbelinettodtl.Text) / dRate2
                End If
            Else
                dparacumamt = ToDouble(amtbelinettodtl.Text)
                dparacumamtusd = ToDouble(amtbelinettodtl.Text)
                If Session("invCurrOid") = 1 Then
                    dparacumamtidr = ToDouble(amtbelinettodtl.Text) / dRate2
                Else
                    dparacumamtidr = ToDouble(amtbelinettodtl.Text) * dRate2
                End If
            End If
            ' UPDATE QL_trndpap bila pake DP status CLose
            If payflag.SelectedValue = "DP" Then
                sSql = "UPDATE QL_trndpar SET trndparflag=CASE WHEN  (ISNULL(trndparacumamt,0) - " & ToDouble(amtbelinettodtl.Text) & ")>=ISNULL(trndparamt,0) THEN 'CLOSE' ELSE 'OPEN' END WHERE cmpcode='" & cmpcode & "' AND trndparoid='" & trndpapoid.SelectedValue & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
        Session("oid") = Nothing : Session("tbldtl") = Nothing
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub

    Protected Sub btnPosting2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting2.Click
        lblPOST.Text = "POST" : btnSave_Click(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Try
            Dim sMsg As String = "" 
            dRate = InvoiceRate(Session("invCurrOid"))
            'CEK PERIODE AKTIF BULANAN
            sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctgHJ(CDate(toDate(PaymentDate.Text))) < GetStrData(sSql) Then
                showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            '=========================================================================== 
            If cashbankacctgoid.SelectedValue = "" Then
                sMsg &= "- Please Fill COA Cash/Bank/Giro !!<BR>"
            End If
            Dim st2 As Boolean
            If ToDouble(trnsuppoid.Text) = 0 Then
                sMsg &= "- Silahkan pilih customer!!<BR>"
            End If
            If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
                payduedate.Text = PaymentDate.Text
            ElseIf payduedate.Text.Trim = "" Then
                sMsg &= "- Silahkan isi Tanggal jatuh tempo bayar !!<BR>"
            End If

            If payflag.SelectedValue = "DP" Then
                If trndpapoid.Items.Count < 1 Then
                    sMsg &= "- Tidak ada DP di customer ini !!<BR>"
                Else
                    If ToDouble(amtbelinettodtl.Text) > ToDouble(dpbalance.Text) Then
                        sMsg &= "- Total bayar harus <= total DP !!<BR>"
                    End If
                End If
            End If
            Try
                Dim dt As Date = CDate(toDate(PaymentDate.Text)) : st2 = True
            Catch ex As Exception
                sMsg &= "- Tanggal bayar salah !!<BR>" : st2 = False
            End Try
             
            If ddlDPNo.Items.Count > 0 Then
                Dim tempDP As Double = ToDouble(cKoneksi.ambilscalar("SELECT trndparamt FROM QL_trndpar WHERE trndparoid=" & ddlDPNo.SelectedValue()))
                If ToDouble(DPAmt.Text) > tempDP Then
                    sMsg &= "- Jumlah DP tidak boleh > actual DP !!<BR>"
                End If
            End If

            ' cek apakah Payment Detail sudah ada atau belum
            If Session("tbldtl") Is Nothing Then
                sMsg &= "- Tidak ada detail bayar !!<BR>"
                GVDtlPayAP.DataSource = Session("tbldtl")
                GVDtlPayAP.DataBind()
            Else
                Dim objTableCek As DataTable : objTableCek = Session("tbldtl")
                If objTableCek.Rows.Count <= 0 Then
                    sMsg &= "- Tidak ada detail bayar !!<BR>"
                    GVDtlPayAP.DataSource = Session("tbldtl")
                    GVDtlPayAP.DataBind()
                Else
                    'cek tgl ivoice pd detail apakah ada yg kurang dari tgl bayar
                    'If dRate = 0 Then
                    '    showMessage("Rate Invoice tidak ditemukan", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                    '    Exit Sub
                    'End If

                    For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
                        If objTableCek.Rows(c1).Item("flagdtl") = "" Then
                            sSql = "select trnjualdate from ql_trnjualmst where cmpcode='" & cmpcode & "' and trnjualmstoid=" & objTableCek.Rows(c1).Item("payrefoid")
                            If (conn.State = ConnectionState.Closed) Then
                                conn.Open()
                            End If
                            objCmd.CommandText = sSql
                            Dim tgle As Date = objCmd.ExecuteScalar
                            conn.Close()

                            If tgle > CDate(toDate(PaymentDate.Text)) Then
                                sMsg &= "- Maaf, tanggal nota jual tidak boleh > tanggal pembayaran (inv.date=" & objTableCek.Rows(c1).Item("trnbelino") & " dan Tanggal bayar= " & Format(tgle, "dd/MM/yyyy") & ")!!<BR>"
                                Exit For
                            End If
                        End If

                        ''cek data synchronisasi dengan payment
                        'If CurrencyOid.SelectedValue = 1 Then
                        '    sSql = "select accumpaymentidr from ql_trnjualmst where cmpcode='" & cmpcode & "' and trnjualmstoid=" & objTableCek.Rows(c1).Item("payrefoid")
                        '    If ToDouble(GetStrData(sSql)) < 0 Then
                        '        sMsg &= "- Nota beli (" & objTableCek.Rows(c1).Item("trnbelino") & ") tidak valid !!, Silahkan refresh atau pilih ulang untuk nota jual ini !! <BR>"
                        '        Exit For
                        '    End If
                        'Else
                        '    sSql = "select accumpaymentusd from ql_trnjualmst where cmpcode='" & cmpcode & "' and trnjualmstoid=" & objTableCek.Rows(c1).Item("payrefoid")
                        '    If ToDouble(GetStrData(sSql)) < 0 Then
                        '        sMsg &= "- Nota beli (" & objTableCek.Rows(c1).Item("trnbelino") & ") tidak valid !!, Silahkan refresh atau pilih ulang untuk nota jual ini !! <BR>"
                        '        Exit For
                        '    End If
                        'End If

                        If Session("oid") = Nothing Or Session("oid") = "" Then
                            'Cek apakah ada invoice yg sudah dibuatkan payment tp blm diposting
                            If GetStrData("select count(-1) from QL_trnpayar where payrefoid=" & objTableCek.Rows(c1).Item("payrefoid") & " and upper(payreftype)='QL_TRNJUALMST' and upper(paystatus)<>'POST' ") > 0 Then
                                sMsg &= "- Ada transaksi pembayaran/Retur lain untuk nota jual ini (" & objTableCek.Rows(c1).Item("trnbelino") & ") <BR> Silahkan tekan batal dan cek di transaksi pembayaran yang lain kemudian tekan posting untuk nota (" & objTableCek.Rows(c1).Item("trnbelino") & ") terlebih dahulu !! <BR>"
                                Exit For
                            End If
                        Else
                            'Cek apakah ada invoice yg sudah dibuatkan payment tp blm diposting
                            If GetStrData("select count(-1) from QL_trnpayar where payrefoid=" & objTableCek.Rows(c1).Item("payrefoid") & " and upper(payreftype)='QL_TRNJUALMST' and cashbankoid <> " & Session("oid") & " and upper(paystatus)<>'POST' ") > 0 Then
                                sMsg &= "- Ada transaksi pembayaran/Retur lain untuk nota jual ini (" & objTableCek.Rows(c1).Item("trnbelino") & ") <BR> Silahkan tekan batal dan cek di transaksi pembayaran yang lain kemudian tekan posting untuk nota  (" & objTableCek.Rows(c1).Item("trnbelino") & ") terlebih dahulu !! <BR>"
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If

            If defcbno.Text.Trim = "" Then
                sMsg &= " - Isi no pembayaran piutang dahulu !!<BR>"
            End If

            If cashbanknote.Text.Trim.Length > 200 Then
                sMsg &= "- Maksimal Note Header adalah 200 karakter !!<BR>"
            End If
            If sMsg <> "" Then
                lblPOST.Text = "In Process"
                showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End If
        Catch ex As Exception
            lblPOST.Text = "In Process"
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try

       'insert to conar & get acctgoid piutang from mst interface
        If Session("oid") = Nothing Or Session("oid") = "" Then
            GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
        End If
        Session("vJurnalDtl") = GenerateID("ql_trngldtl", cmpcode)
        Session("vJurnalMst") = GenerateID("ql_trnglmst", cmpcode)
        Dim sCashBank As Integer
        If Session("oid") = Nothing Or Session("oid") = "" Then
            sCashBank = GenerateID("QL_trncashbankmst", cmpcode)
        Else
            sCashBank = cashbankoid.Text
        End If

        Session("vDtlSeq") = 1 : currencyRate.Text = "1.00"
        Dim itrnbelimstoid As Int32 = 0 : Dim sHpp As Double
        '=====================
        Dim TblObj As DataTable = Session("tbldtl")
        Dim RowObj() As DataRow
        Dim dtlV As DataView = TblObj.DefaultView
        'dtlV.RowFilter = "payamt > 0.00"
        'RowObj = TblObj.Select(Nothing, Nothing, DataViewRowState.CurrentRows) 
        'Dim nDelok As Integer = TblObj.Rows.Count
        'For C1 As Int16 = 0 To TblObj.Rows.Count - 1
        '    If lblPOST.Text.ToUpper = "POST" Then
        '        Dim oid As String = trnbelimstoid.Text
        '        sSql = "Select payseq,inv.BRANCH_CODE,inv.soid trnbelimstoid,STOTALPRICE,potongan,itemoid,Qty,Price,ITSERVTARIF,PARTUNIT,hpp,Note,dtlOid,mtrloc,isnull(hpp*Qty,0.00) sHpp FROM QL_TRNINVOICE inv INNER JOIN (Select BRANCH_CODE,it.SITEMOID dtlOid,SOID,ITSERVOID itemoid,0 Qty,ITSERVTARIF Price,ITSERVTARIF,0 PARTUNIT,ITSERVTARIF hpp,it.SPARTNOTE Note,SEQ payseq,0 mtrloc FROM QL_trnInvoiceitem it UNION ALL Select BRANCH_CODE,ip.SPARTOID dtlOid,SOID,SPARTMOID itemoid,SPARTQTY Qty,SPARTPRICE Price,ISNULL(SPARTQTY * SPARTPRICE,0.0) ITSERVTARIF,sp.satuan1 PARTUNIT,hpp,ip.SPARTNOTE Note,SPARTSEQ payseq,ip.mtrlocoid mtrloc FROm QL_trnInvoicepart ip INNER JOIN ql_mstitem sp ON SPARTMOID=sp.itemoid) As it ON inv.SOID=it.SOID AND inv.BRANCH_CODE=it.BRANCH_CODE AND it.SOID=" & TblObj.Rows(C1)("payrefoid").ToString & ""
        '        Dim objTbl As DataTable = cKoneksi.ambiltabel(sSql, "QL_TRNINVOICE")
        '        Session("JualDtl") = objTbl
        '    End If
        'Next
        '--- Interface untuk set COA ---'
        '===============================
        'Dim Var_Jasa As String = GetVarCoa("VAR_JASA", DDLCabang.SelectedValue)
        'If Var_Jasa = "0" Then
        '    showMessage("Maaf, Interface VAR_JASA Untuk cabang " & DDLCabang.SelectedItem.Text & " belum disetting silahkan hubungi admin", CompnyName & " - ERROR", 2, "modalMsgBox")
        '    Exit Sub
        'End If
        'Dim Var_Spart As String = GetVarCoa("VAR_SPART", DDLCabang.SelectedValue)
        'If Var_Spart = "0" Then
        '    showMessage("Maaf, Interface VAR_SPART Untuk cabang " & DDLCabang.SelectedItem.Text & " belum disetting silahkan hubungi admin", CompnyName & " - ERROR", 2, "modalMsgBox")
        '    Exit Sub
        'End If
        'Dim Var_HPP As String = GetVarCoa("VAR_HPP", "MSC")

        'Dim var_gudang As String = GetVarCoa("VAR_GUDANG", DDLCabang.SelectedValue)
        'If var_gudang = "0" Then
        '    showMessage("Maaf, Interface VAR_GUDANG Untuk cabang " & DDLCabang.SelectedItem.Text & " belum disetting silahkan hubungi admin", CompnyName & " - ERROR", 2, "modalMsgBox")
        '    Exit Sub
        'End If

        'Dim OidCoaJasa As String = GetStrData("select acctgoid from ql_mstacctg where acctgcode ='" & Var_Jasa & "'")

        'Dim OidCoaSpart As String = GetStrData("select acctgoid from ql_mstacctg where acctgcode ='" & Var_Spart & "'")

        'Dim OidCoaHpp As String = GetStrData("select acctgoid from ql_mstacctg where acctgcode ='" & Var_HPP & "'")

        'Dim OidCoaGudang As String = GetStrData("select acctgoid from ql_mstacctg where acctgcode ='" & var_gudang & "'")

        'If OidCoaJasa = "?" Or OidCoaJasa = "" Then
        '    showMessage("Maaf, kode COA " & Var_Jasa & " tidak ditemukan, silahkan cek di master COA", CompnyName & " - ERROR", 2, "modalMsgBox") : Exit Sub
        'ElseIf OidCoaSpart = "?" Or OidCoaSpart = "" Then
        '    showMessage("Maaf, kode COA " & Var_Spart & " tidak ditemukan, silahkan cek di master COA", CompnyName & " - ERROR", 2, "modalMsgBox") : Exit Sub
        'ElseIf OidCoaHpp = "?" Or OidCoaHpp = "" Then
        '    showMessage("Maaf, kode COA " & Var_HPP & " tidak ditemukan, silahkan cek di master COA", CompnyName & " - ERROR", 2, "modalMsgBox") : Exit Sub
        'ElseIf OidCoaGudang = "?" Or OidCoaGudang = "" Then
        '    showMessage("Maaf, kode COA " & var_gudang & " tidak ditemukan, silahkan cek di master COA", CompnyName & " - ERROR", 2, "modalMsgBox") : Exit Sub
        'End If
        '=====================
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                Dim sCBType As String = ""
                Dim iCurID As Integer = 0 : Dim sCBCode As String = ""

                If payflag.SelectedValue = "CASH" Then
                    sCBType = "BKM"
                ElseIf payflag.SelectedValue = "BANK" Then
                    sCBType = "BBM"
                ElseIf payflag.SelectedValue = "GIRO" Then
                    sCBType = "BGM"
                ElseIf payflag.SelectedValue = "CREDIT CARD" Then
                    sCBType = "BCM"
                ElseIf payflag.SelectedValue = "DP" Then
                    sCBType = "BDM"
                    payrefno.Text = trndpapoid.SelectedItem.Text
                Else
                    sCBType = "BLM"
                End If

                Dim cbnote As String = ""
                If payflag.SelectedValue = "GIRO" Then
                    cbnote = "GIRO =>" & dd_bankgiro.SelectedItem.Text & " " & cashbanknote.Text
                Else
                    cbnote = cashbanknote.Text
                End If
                If girooid.Text = "" Then
                    girooid.Text = 0
                End If
                sSql = "INSERT into QL_trncashbankmst(cmpcode, cashbankoid, cashbankno, cashbankstatus, cashbanktype, cashbankgroup, cashbankacctgoid, cashbankdate, cashbanknote, createuser, createtime, upduser, updtime, cashbankcurroid, cashbankcurrate, pic, pic_refname, branch_code, giroref, cashbankrefno, bankoid, girodtloid) " & _
                   " VALUES ('" & cmpcode & "'," & sCashBank & ",'" & Tchar(defcbno.Text) & "','" & lblPOST.Text & "','" & sCBType & "','SERVICE'," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & Tchar(cbnote) & "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp," & CurrencyOid.SelectedValue & "," & ToDouble(currencyRate.Text) & ", " & trnsuppoid.Text & ", 'QL_MSTCUST','" & DDLCabang.SelectedValue & "', 0,'" & Tchar(payrefno.Text.Trim) & "', 0, 0)"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                'Update lastoid dari QL_mstoid table QL_trncashbankmst
                sSql = "Update QL_mstoid set lastoid=" & sCashBank & " Where tablename like 'QL_trncashbank%' and cmpcode like '%" & cmpcode & "%'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                Dim dtTable As DataTable = Session("tbldtl")
                Dim dvRow As DataView = dtTable.DefaultView
                Dim objTable As DataTable : Dim objRow() As DataRow
                objTable = Session("tbldtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                Dim AmtNya As Double
                Dim sTemp As Integer = GenerateID("QL_trnpayar", cmpcode)
                Session("conaroid") = GenerateID("ql_conar", cmpcode)
                For t1 As Integer = 0 To dvRow.Count - 1
                    If dvRow(t1)("payres1").ToString = "KURANG BAYAR" Then
                        AmtNya = ToDouble(dtTable.Compute("SUM(payamt)", "payrefoid =" & Integer.Parse(dvRow(t1)("payrefoid")) & ""))
                    Else
                    AmtNya = ToDouble(dvRow(t1)("payamt").ToString)
                    End If

                    sSql = "INSERT into QL_trnpayar(cmpcode, paymentoid, cashbankoid, custoid, payreftype, payrefoid, payacctgoid, payrefno, paybankoid, payduedate, paynote, payamt, payamtidr, payamtusd, paystatus, upduser, updtime, trndparoid, DPAmt, DPamtidr, DPAmtusd, payflag, branch_code, payres1)" & _
                    " VALUES ('" & cmpcode & "'," & sTemp & "," & sCashBank & "," & trnsuppoid.Text & ",'QL_trninvoice'," & objRow(t1)("payrefoid").ToString & "," & ToDouble(objRow(t1)("payacctgoid").ToString) & ",'" & Tchar(payrefno.Text.Trim) & "'," & cashbankacctgoid.SelectedValue() & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(objRow(t1)("paynote").ToString) & "'," & ToDouble(objRow(t1)("payamt")) & "," & ToDouble(objRow(t1)("payamt")) & "," & ToDouble(objRow(t1)("payamt")) & ",'" & lblPOST.Text & "','" & Session("UserID") & "',current_timestamp,0,0,0,0,'" & objRow(t1)("flagdtl").ToString & "','" & DDLCabang.SelectedValue & "','" & objRow(t1)("payres1").ToString & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()


                    sSql = "INSERT into QL_conar (cmpcode, conaroid, branch_code, reftype, refoid, payrefoid, custoid, amttrans, amtbayar, amtbayaridr, amtbayarusd, paymentdate, periodacctg, upduser, updtime, payrefno, acctgoid, paymentacctgoid, trnarstatus, trnarnote, trnartype) " & _
                   "VALUES ('" & cmpcode & "'," & Session("conaroid") & ",'" & DDLCabang.SelectedValue & "','QL_trnpayar'," & dvRow(t1)("payrefoid") & "," & sTemp & "," & trnsuppoid.Text & ",0," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & "," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & "," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(defcbno.Text) & "'," & cashbankacctgoid.SelectedValue & "," & dvRow(t1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(defcbno.Text) & "','PAYARSVC')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Session("conaroid") += 1
                    sTemp += 1
                Next
                ' Update lastoid conar
                sSql = "UPDATE ql_mstoid SET lastoid=" & Session("conaroid") - 1 & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_conar'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                'Update lastoid dari QL_mstoid table QL_trnpayar
                sSql = "UPDATE QL_mstoid SET lastoid=" & sTemp - 1 & " WHERE tablename='QL_trnpayar'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                If lblPOST.Text = "POST" Then
                    sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,branch_code)" & _
                  "VALUES ('" & cmpcode & "'," & Session("vJurnalMst") & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & _
                  "','A/R Payment|No=" & Tchar(defcbno.Text) & "','" & lblPOST.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',current_timestamp,'" & DDLCabang.SelectedValue & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    '//////INSERT INTO TRN GL DTL
                    Dim dvDtl As DataView = objTable.DefaultView
                    Dim glamt, glamtidr, glamtusd As Double
                    ' PIUTANG
                    dvDtl.RowFilter = "flagdtl<>'OTHER'"
                    For c1 As Integer = 0 To dvDtl.Count - 1

                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                            "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & DDLCabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & ",'" & Tchar(defcbno.Text) & "','Payment Svc | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & sCashBank & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""

                    ' LEBIH BAYAR
                    dvDtl.RowFilter = "flagdtl='OTHER' AND payamt>0"
                    For c1 As Integer = 0 To dvDtl.Count - 1

                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                            "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & DDLCabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & ",'" & Tchar(defcbno.Text) & "','Payment Svc (Lebih Bayar) | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & sCashBank & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""
                    Dim cglamt, cglamtidr, cglamtusd As Double
                    If CurrencyOid.SelectedValue = 1 Then
                        cglamt = ToDouble(amtbelinettodtl.Text)
                        cglamtidr = ToDouble(amtbelinettodtl.Text)
                        'If Session("invCurrOid") = 1 Then
                        '    cglamtusd = ToDouble(amtbelinettodtl.Text) * dRate2
                        'Else
                        '    cglamtusd = ToDouble(amtbelinettodtl.Text) / dRate2
                        'End If
                    Else
                        cglamt = ToDouble(amtbelinettodtl.Text)
                        cglamtusd = ToDouble(amtbelinettodtl.Text)
                        'If Session("invCurrOid") = 1 Then
                        '    cglamtidr = ToDouble(amtbelinettodtl.Text) / dRate2
                        'Else
                        '    cglamtidr = ToDouble(amtbelinettodtl.Text) * dRate2
                        'End If
                    End If
                    ' CASH/BANK
                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime, glflag) " & _
                        "VALUES ('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & DDLCabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & cashbankacctgoid.SelectedValue & ",'D'," & cglamtidr & "," & cglamtidr & "," & cglamtidr & ",'" & Tchar(defcbno.Text) & "','Payment Svc | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Dim kcglamt, kcglamtidr, kcglamtusd As Double

                    ' KURANG BAYAR
                    dvDtl.RowFilter = "flagdtl='OTHER' AND payamt < 0"
                    For c1 As Integer = 0 To dvDtl.Count - 1
                        If CurrencyOid.SelectedValue = 1 Then
                            kcglamt = dvDtl(c1)("payamt") * -1
                            kcglamtidr = dvDtl(c1)("payamt") * -1
                            'If Session("invCurrOid") = 1 Then
                            '    kcglamtusd = (dvDtl(c1)("payamt") * -1) * dRate2
                            'Else
                            '    kcglamtusd = (dvDtl(c1)("payamt") * -1) / dRate2
                            'End If
                        Else
                            kcglamt = dvDtl(c1)("payamt") * -1
                            kcglamtusd = dvDtl(c1)("payamt") * -1
                            'If Session("invCurrOid") = 1 Then
                            '    kcglamtidr = dvDtl(c1)("payamt") / dRate2 * -1
                            'Else
                            '    kcglamtidr = dvDtl(c1)("payamt") * dRate2 * -1
                            'End If
                        End If
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                            "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & DDLCabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'D'," & kcglamtidr & "," & kcglamtidr & "," & kcglamtidr & ",'" & Tchar(defcbno.Text) & "','Payment Svc (Kurang Bayar) | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & sCashBank & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""

                    ' Update lastoid GLMST
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalMst") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trnglmst'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    ' Update lastoid GLDTL
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalDtl") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trngldtl'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
            Else
                '================================Update=================================
                '=======================================================================
                Dim bUpdPayType As Boolean = False
                Dim sCBType As String = "" : Dim sCBCode As String = ""
                Dim sTemp As Integer = GenerateID("QL_trnpayar", cmpcode)
                Session("conaroid") = GenerateID("ql_conar", cmpcode)
                If payflag.SelectedValue <> HiddenField1.Value Or cashbankacctgoid.SelectedValue <> HiddenField2.Value Then ' jika ada perubahan pada payment type
                    Dim iCurID As Integer = 0
                    If payflag.SelectedValue = "CASH" Then
                        sCBType = "BKM"
                    ElseIf payflag.SelectedValue = "BANK" Then
                        sCBType = "BBM"
                    ElseIf payflag.SelectedValue = "GIRO" Then
                        sCBType = "BGM"
                    ElseIf payflag.SelectedValue = "DP" Then
                        sCBType = "BDM"
                    Else
                        sCBType = "BLM"
                    End If
                    bUpdPayType = True
                Else
                    sSql = "SELECT cashbanktype FROM ql_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & Trim(cashbankoid.Text)
                    objCmd.CommandText = sSql : sCBType = objCmd.ExecuteScalar
                End If

                ' Karena No Bisa Manual, no bs diupdate terus
                bUpdPayType = True
                sSql = "UPDATE QL_trncashbankmst SET cashbankacctgoid=" & cashbankacctgoid.SelectedValue & ", cashbankdate='" & CDate(toDate(PaymentDate.Text)) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', upduser = '" & Session("UserID") & "',updtime=current_timestamp" & ",cashbankcurroid='" & CurrencyOid.SelectedValue & "',cashbankcurrate=" & ToDouble(currencyRate.Text) & ",cashbankstatus='" & lblPOST.Text & "', PIC=" & trnsuppoid.Text & ", pic_refname='QL_MSTCUST',cashbankrefno='" & Tchar(payrefno.Text.Trim) & "' "
                If bUpdPayType Then
                    sSql &= ",cashbankno='" & Tchar(defcbno.Text) & "', cashbanktype='" & sCBType & "'"
                End If
                sSql &= " WHERE cmpcode = '" & cmpcode & "' and branch_code='" & DDLCabang.SelectedValue & "' AND cashbankoid = " & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                sSql = "Delete from QL_conar where cmpcode='" & cmpcode & "' and branch_code = '" & DDLCabang.SelectedValue & "' AND payrefoid IN (SELECT paymentoid FROM QL_trnpayar WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text & ")"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                sSql = "Delete from QL_trnpayar where cmpcode='" & cmpcode & "' and branch_code = '" & DDLCabang.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                Dim dtTable As DataTable = Session("tbldtl")
                Dim dvRow As DataView = dtTable.DefaultView
                Dim objTable As DataTable : Dim objRow() As DataRow
                objTable = Session("tbldtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                Dim AmtNya As Double
                'dvRow.RowFilter = "flagdtl <> 'OTHER'"
                For t1 As Integer = 0 To dvRow.Count - 1
                    If dvRow(t1)("payres1").ToString = "KURANG BAYAR" Then
                        AmtNya = ToDouble(dtTable.Compute("SUM(payamt)", "payrefoid =" & dvRow(t1)("payrefoid") & ""))
                    Else
                        AmtNya = ToDouble(dvRow(t1)("payamt"))
                    End If

                    sSql = "INSERT into QL_trnpayar(cmpcode,paymentoid,cashbankoid,custoid,payreftype, payrefoid,payacctgoid,payrefno,paybankoid,payduedate,paynote,payamt,payamtidr,payamtusd, paystatus,upduser,updtime,trndparoid,DPAmt,DPamtidr,DPAmtusd,payflag,branch_code,payres1)" & _
                    " VALUES ('" & cmpcode & "'," & sTemp & "," & cashbankoid.Text & "," & trnsuppoid.Text & ",'QL_trninvoice'," & objRow(t1)("payrefoid").ToString & "," & ToDouble(objRow(t1)("payacctgoid").ToString) & ",'" & Tchar(payrefno.Text.Trim) & "'," & cashbankacctgoid.SelectedValue() & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(objRow(t1)("paynote").ToString) & "'," & ToDouble(objRow(t1)("payamt")) & "," & ToDouble(objRow(t1)("payamt")) & "," & ToDouble(objRow(t1)("payamt")) & ",'" & lblPOST.Text & "','" & Session("UserID") & "',current_timestamp,0,0,0,0,'" & objRow(t1)("flagdtl").ToString & "','" & DDLCabang.SelectedValue & "','" & IIf(objRow(t1)("flagdtl").ToString = "OTHER", objRow(t1)("payres1").ToString, "") & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()


                    sSql = "INSERT into QL_conar (cmpcode,conaroid,branch_code,reftype,refoid,payrefoid,custoid,amttrans,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnarstatus,trnarnote,trnartype) " & _
                   "VALUES ('" & cmpcode & "'," & Session("conaroid") & ",'" & DDLCabang.SelectedValue & "','QL_trnpayar'," & dvRow(t1)("payrefoid") & "," & sTemp & "," & trnsuppoid.Text & ",0," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & "," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & "," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(defcbno.Text) & "'," & cashbankacctgoid.SelectedValue & "," & dvRow(t1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(defcbno.Text) & "','PAYARSVC')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Session("conaroid") += 1
                    sTemp += 1
                Next
                ' Update lastoid conar
                sSql = "UPDATE ql_mstoid SET lastoid=" & Session("conaroid") - 1 & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_conar'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                'Update lastoid dari QL_mstoid table QL_trnpayar
                sSql = "UPDATE QL_mstoid SET lastoid=" & sTemp - 1 & " WHERE tablename='QL_trnpayar'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                If lblPOST.Text = "POST" Then
                    sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,branch_code)" & _
                  "VALUES ('" & cmpcode & "'," & Session("vJurnalMst") & ",'" & CDate(toDate(PaymentDate.Text)) & "','" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & _
                  "','Payment Svc|No=" & Tchar(defcbno.Text) & "','" & lblPOST.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',current_timestamp,'" & DDLCabang.SelectedValue & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    '//////INSERT INTO TRN GL DTL
                    Dim dvDtl As DataView = objTable.DefaultView
                    Dim glamt, glamtidr, glamtusd As Double
                    ' PIUTANG
                    dvDtl.RowFilter = "flagdtl<>'OTHER'"
                    For c1 As Integer = 0 To dvDtl.Count - 1

                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                            "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & DDLCabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & ",'" & Tchar(defcbno.Text) & "','Payment Svc | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & sCashBank & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""

                    ' LEBIH BAYAR
                    dvDtl.RowFilter = "flagdtl='OTHER' AND payamt>0"
                    For c1 As Integer = 0 To dvDtl.Count - 1

                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                            "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & DDLCabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & ",'" & Tchar(defcbno.Text) & "','Payment Svc (Lebih Bayar) | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & sCashBank & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""
                    Dim cglamt, cglamtidr, cglamtusd As Double
                    If CurrencyOid.SelectedValue = 1 Then
                        cglamt = ToDouble(amtbelinettodtl.Text)
                        cglamtidr = ToDouble(amtbelinettodtl.Text)
                        'If Session("invCurrOid") = 1 Then
                        '    cglamtusd = ToDouble(amtbelinettodtl.Text) * dRate2
                        'Else
                        '    cglamtusd = ToDouble(amtbelinettodtl.Text) / dRate2
                        'End If
                    Else
                        cglamt = ToDouble(amtbelinettodtl.Text)
                        cglamtusd = ToDouble(amtbelinettodtl.Text)
                        'If Session("invCurrOid") = 1 Then
                        '    cglamtidr = ToDouble(amtbelinettodtl.Text) / dRate2
                        'Else
                        '    cglamtidr = ToDouble(amtbelinettodtl.Text) * dRate2
                        'End If
                    End If
                    ' CASH/BANK
                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime, glflag) " & _
                        "VALUES ('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & DDLCabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & cashbankacctgoid.SelectedValue & ",'D'," & cglamtidr & "," & cglamtidr & "," & cglamtidr & ",'" & Tchar(defcbno.Text) & "','Payment Svc | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & "','','" & sCashBank & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Dim kcglamt, kcglamtidr, kcglamtusd As Double

                    ' KURANG BAYAR
                    dvDtl.RowFilter = "flagdtl='OTHER' AND payamt < 0"
                    For c1 As Integer = 0 To dvDtl.Count - 1
                        If CurrencyOid.SelectedValue = 1 Then
                            kcglamt = dvDtl(c1)("payamt") * -1
                            kcglamtidr = dvDtl(c1)("payamt") * -1
                            'If Session("invCurrOid") = 1 Then
                            '    kcglamtusd = (dvDtl(c1)("payamt") * -1) * dRate2
                            'Else
                            '    kcglamtusd = (dvDtl(c1)("payamt") * -1) / dRate2
                            'End If
                        Else
                            kcglamt = dvDtl(c1)("payamt") * -1
                            kcglamtusd = dvDtl(c1)("payamt") * -1
                            'If Session("invCurrOid") = 1 Then
                            '    kcglamtidr = dvDtl(c1)("payamt") / dRate2 * -1
                            'Else
                            '    kcglamtidr = dvDtl(c1)("payamt") * dRate2 * -1
                            'End If
                        End If
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                            "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & DDLCabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'D'," & kcglamtidr & "," & kcglamtidr & "," & kcglamtidr & ",'" & Tchar(defcbno.Text) & "','Payment Svc (Kurang Bayar) | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & sCashBank & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                    Next
                    dvDtl.RowFilter = ""
                    ' Update lastoid GLMST
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalMst") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trnglmst'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    ' Update lastoid GLDTL
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalDtl") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trngldtl'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
                dvRow.RowFilter = ""
            End If

            'Dim dRate As Double : dRate = InvoiceRate(Session("invCurrOid"))
            If Not Session("tbldtl") Is Nothing Then
                Dim BeliOid As Integer = 0 : Dim AmtByr As Double = 0.0
                Dim objTable As DataTable = Session("tbldtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow() As DataRow
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    BeliOid = objTable.Rows(C1)("payrefoid")
                Next

                dv.RowFilter = "payrefoid='" & BeliOid & "' AND payamt > 0.00"
                For C1 As Int16 = 0 To dv.Count - 1
                    'cek apakah supplier ada atau tidak
                    Dim vPayam, vPayamidr, vPayamusd As Double
                    vPayam = ToDouble(dv(C1)("payamt").ToString)
                    vPayamidr = ToDouble(dv(C1)("payamt").ToString)
                    vPayamusd = ToDouble(dv(C1)("payamt").ToString)
                Next
            End If

            '=================
            'Insert Cashbankgl
            '================= 
            Dim iCbdtloid As Int64 = GenerateID("QL_CASHBANKGL", cmpcode)
            'If lblPOST.Text = "POST" Then
            Dim objtablexx As DataTable : Dim objrowxx() As DataRow
            objtablexx = Session("tbldtl")
            objrowxx = objtablexx.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim Cashbankglamt, Cashbankglamtidr, Cashbankglamtusd As Double
            sSql = "Delete QL_cashbankgl Where cashbankoid=" & sCashBank & " AND branch_code='" & DDLCabang.SelectedValue & "'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            For C1 As Int16 = 0 To objrowxx.Length - 1
                'INSERT TO CASHBANK GL DULU
                If CurrencyOid.SelectedValue = 1 Then
                    Cashbankglamt = ToDouble(objrowxx(C1)("payamt").ToString)
                    Cashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString)
                    If Session("invCurrOid") = 1 Then
                        Cashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString) * 1
                    Else
                        Cashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString) / 1
                    End If
                Else
                    Cashbankglamt = ToDouble(objrowxx(C1)("payamt").ToString)
                    Cashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString)
                    If Session("invCurrOid") = 1 Then
                        Cashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString) / 1
                    Else
                        Cashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString) * 1
                    End If
                End If

                sSql = "INSERT INTO QL_cashbankgl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,cashbankglstatus,cashbankglres1,duedate,refno,createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                    "('" & cmpcode & "'," & iCbdtloid & "," & sCashBank & "," & ToDouble(objrowxx(C1)("payacctgoid").ToString) & "," & Cashbankglamt & "," & Cashbankglamtidr & "," & Cashbankglamtusd & ",'A/R Payment|No=" & Tchar(objrowxx(C1)("TRNBELINO").ToString) & "','" & lblPOST.Text & "','" & objrowxx(C1)("payrefoid").ToString & "','" & CDate(toDate(PaymentDate.Text)) & "','" & Tchar(objrowxx(C1)("TRNBELINO").ToString) & "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & DDLCabang.SelectedValue & "')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                iCbdtloid += 1
            Next
            sSql = "update QL_mstoid set lastoid=" & iCbdtloid & " where tablename like 'QL_cashbankGL' and cmpcode like '%" & cmpcode & "%'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            sSql = "update g set trnid = (select trnid from QL_trncashbankmst where cashbankoid = g.cashbankoid and branch_code = g.branch_code) from QL_cashbankgl g inner join QL_trncashbankmst cb on cb.branch_code=g.branch_code AND cb.cashbankoid=g.cashbankoid AND g.trnid=0"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            'End If

            objTrans.Commit() : objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            lblPOST.Text = ""
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox") : objTrans.Dispose()
            Exit Sub
        End Try
            Session("tbldtl") = Nothing : Session("oid") = Nothing
            If lblPOST.Text = "POST" Then
                showMessage("Data telah diposting !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
            Else
                showMessage("Data telah disimpan !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
            End If
    End Sub

    Protected Sub amtpayment_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amtpayment.TextChanged
        CalculateTotalPayment()
    End Sub

    Protected Sub SqlDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs)

    End Sub

    Protected Sub CurrencyOid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CurrencyOid.SelectedIndexChanged
        FillCurrencyRate(CurrencyOid.SelectedValue)
        If Session("oid") = "" Or Session("oid") <= 0 Then
            ClearDtlAP(True) : Session("tbldtl") = Nothing
            GVDtlPayAP.DataSource = Nothing
            If payflag.SelectedValue = "GIRO" Then
                payrefno.Text = ""
                AmountGiro.Text = 0
            End If
        Else
            Session("currsess") = True

            FillTextBox(Session("oid"))
        End If
        If lblPOST.Text = "POST" Then
            btnSave.Visible = False
        End If

    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.SetActiveView(View2)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & Tchar(cashbankno.Text) & "'"
        PrintReport(cashbankno.Text, cKoneksi.ambilscalar(sSql))
    End Sub

    Protected Sub GVmstPAYAP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmstPAYAP.SelectedIndexChanged
        sSql = "SELECT count(-1) FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & GVmstPAYAP.SelectedDataKey.Item("cashbankno").ToString & "' and cashbankstatus in ('POST')"
        If cKoneksi.ambilscalar(sSql) <= 0 Then
            showMessage("Data harus di posting terlebih dahulu sebelum di cetak !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & GVmstPAYAP.SelectedDataKey.Item("cashbankno").ToString & "'"
        PrintReport(GVmstPAYAP.SelectedDataKey.Item("cashbankno").ToString, cKoneksi.ambilscalar(sSql))
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sQuery As String = Session("SearchAP")
        If sQuery <> "" Then
            Dim objTable As DataTable = cKoneksi.ambiltabel(Session("SearchAP"), "ql_trncashbankmst")
            Session("tbldata") = objTable
            GVmstPAYAP.DataSource = objTable
            GVmstPAYAP.DataBind()
            calcTotalInGrid()
        End If
    End Sub

    Protected Sub btnClearSupp_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub DP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblDPAmount.Visible = cbDP.Checked
        ddlDPNo.Visible = cbDP.Checked
        DPAmt.Visible = cbDP.Checked
        If cbDP.Checked Then
            initddlDP(cKoneksi.ambilscalar("SELECT trncustoid FROM QL_trnjualmst WHERE cmpcode='" & cmpcode & "' AND trnbelimstoid=" & trnbelimstoid.Text))
            If ddlDPNo.Items.Count > 0 Then
                DPAmt.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT trndparamt FROM QL_trndpar WHERE trndparoid=" & ddlDPNo.SelectedValue())), 4)
            Else
                DPAmt.Text = ToMaskEdit(0, 4)
            End If

        End If
    End Sub

    Protected Sub ddlDPNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDPNo.SelectedIndexChanged
        DPAmt.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT trndparamt FROM QL_trndpar WHERE trndparoid=" & ddlDPNo.SelectedValue())), 4)
    End Sub

    ' UPDATE MULTI ACCOUNT on DEBET/CREDIT ==> SELEISH PEMBAYARAN

    Protected Sub chkOther_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SetOtherAccount(chkOther.Checked)
        InitOtherAcctg(DDLOtherType.SelectedValue)

        If DDLOtherType.SelectedValue = "-" Then
            If chkOther.Checked Then
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text) + ToDouble(otheramt.Text), 4)
            Else
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 4)
            End If
        End If
        'If DDLOtherType.SelectedValue = "-" Then
        '    If chkOther.Checked Then
        '        otheramt.ReadOnly = False : otheramt.CssClass = "inpText"
        '    Else
        '        otheramt.ReadOnly = True : otheramt.CssClass = "inpTextDisabled"
        '    End If
        'End If
    End Sub

    Protected Sub DDLOtherType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitOtherAcctg(DDLOtherType.SelectedValue)
    End Sub

    Private Sub SetOtherAccount(ByVal bState As Boolean)
        otherAcctgoid.Enabled = bState
        lkbAddDtlSlisih.Visible = bState
        lkbClearDtlSlisih.Visible = bState
        If bState Then
            otherAcctgoid.CssClass = "inpText"
        Else
            otherAcctgoid.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub lkbAddDtlSlisih_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sMsg As String = ""
        If otherAcctgoid.Items.Count < 1 Then
            sMsg &= "- tidak ada COA untuk beda bayar !!<BR>"
        End If
        If ToDouble(amtdtlselisih.Text) <= 0 Then
            sMsg &= "- Total beda bayar harus > 0 !!<BR>"
        End If
        If dtlnoteselisih.Text.Trim.Length > 200 Then
            sMsg &= "- Maksimal Note is 200 Karakter !!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If Session("DtlSelisih") Is Nothing Then
            Dim nu As DataTable = SetTabelDetailSlisih()
            Session("DtlSelisih") = nu
        End If

        Dim objTable As DataTable : objTable = Session("DtlSelisih")
        Dim oRow As DataRow
        If stateDtlSls.Text = "New Selisih" Then
            oRow = objTable.NewRow
            oRow("selisihseq") = objTable.Rows.Count + 1
        Else
            oRow = objTable.Rows(gvDtlSelisih.SelectedIndex)
            oRow.BeginEdit()
        End If

        oRow("acctgoid") = otherAcctgoid.SelectedValue
        oRow("acctgdesc") = otherAcctgoid.SelectedItem.Text
        oRow("amtdtlselisih") = ToDouble(amtdtlselisih.Text)
        oRow("dtlnoteselisih") = dtlnoteselisih.Text

        If stateDtlSls.Text = "New Selisih" Then
            objTable.Rows.Add(oRow)
        Else
            oRow.EndEdit()
        End If
        Session("DtlSelisih") = objTable
        gvDtlSelisih.DataSource = Session("DtlSelisih")
        gvDtlSelisih.DataBind()
        ClearDtlSelisih()
        CalculateSelisih()
    End Sub

    Protected Sub lkbClearDtlSlisih_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDtlSelisih()
    End Sub

    Private Sub ClearDtlSelisih()
        If otherAcctgoid.Items.Count > 1 Then
            otherAcctgoid.SelectedIndex = 0
        End If
        amtdtlselisih.Text = ""
        gvDtlSelisih.Columns(5).Visible = True
        gvDtlSelisih.SelectedIndex = -1
        dtlnoteselisih.Text = ""
        stateDtlSls.Text = "New Selisih"
    End Sub

    Protected Sub gvDtlSelisih_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtlSelisih.SelectedIndexChanged
        Dim objTable As DataTable : objTable = Session("DtlSelisih")
        Dim dvTemp As DataView = objTable.DefaultView
        dvTemp.RowFilter = "selisihseq=" & gvDtlSelisih.SelectedDataKey("selisihseq").ToString
        otherAcctgoid.SelectedValue = dvTemp(0)("acctgoid").ToString
        amtdtlselisih.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtdtlselisih").ToString), 4)
        dtlnoteselisih.Text = dvTemp(0)("dtlnoteselisih").ToString
        dvTemp.RowFilter = ""
        gvDtlSelisih.Columns(5).Visible = False
        stateDtlSls.Text = "Update"
    End Sub

    Protected Sub gvDtlSelisih_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtlSelisih.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub gvDtlSelisih_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtlSelisih.RowDeleting
        Dim idx As Integer = e.RowIndex
        Dim objTable As DataTable
        objTable = Session("DtlSelisih")
        objTable.Rows.RemoveAt(idx)

        'resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            objTable.Rows(C2)("selisihseq") = C2 + 1
        Next

        Session("DtlSelisih") = objTable
        gvDtlSelisih.DataSource = Session("DtlSelisih")
        gvDtlSelisih.DataBind()
        CalculateSelisih()
    End Sub

    Private Sub GenerateDefaultNo(ByVal sFlag As String, ByVal iAcctgOid As Integer, ByVal dDate As Date)
        ' BKK/10FKBE0001
        Dim sCBType As String
        Select Case sFlag
            Case "CASH" : sCBType = "BKM"
            Case "BANK" : sCBType = "BBM"
            Case "GIRO" : sCBType = "BGM"
            Case "CREDIT CARD" : sCBType = "BCM"
            Case "DP" : sCBType = "BDM"
            Case Else : sCBType = "BLM"
        End Select
        Dim cabang As String = GetStrData("Select genother1 From ql_mstgen where gencode='" & DDLCabang.SelectedValue & "' AND gengroup='CABANG'")
        Dim sNo As String = sCBType & "/" & cabang & "/" & Format(dDate, "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1  FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%' and branch_code='" & DDLCabang.SelectedValue & "'"
        defcbno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
    End Sub

    Protected Sub PaymentDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(toDate(PaymentDate.Text)) Then
            GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
        Else
            showMessage("Format tanggal salah !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            PaymentDate.Text = GetServerTime() : Exit Sub
        End If
    End Sub

    Protected Sub amtdtlselisih_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amtdtlselisih.TextChanged, DPAmt.TextChanged
        sender.text = ToMaskEdit(ToDouble(sender.text), 4)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        BindCust()
        hiddenbtn2.Visible = True : Panel1.Visible = True
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        trnsuppoid.Text = "" : suppname.Text = ""
        ClearDtlAP()
        Session("tbldtl") = Nothing : GVDtlPayAP.DataSource = Nothing
        GVDtlPayAP.DataBind() : calcTotalInGridDtl()
        'payflag.SelectedIndex = 0 : payflag_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub ClearDtlAP()
        Session("tbldtl") = Nothing
        GVDtlPayAP.DataSource = Nothing
        GVDtlPayAP.DataBind()
        Session("DtlSelisih") = Nothing
        gvDtlSelisih.DataSource = Nothing
        gvDtlSelisih.DataBind()

        ClearDtlAP(True)
    End Sub

    Protected Sub ibtnSuppID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnSuppID.Click
        ModalPopupExtender1.Show()
        filterGVS(txtFindSuppID.Text, "", cmpcode, "MANUFACTURE", DDLCabang.SelectedValue)
    End Sub

    Protected Sub imbViewAlls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewAlls.Click
        txtFindSuppID.Text = "" : BindCust()
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub CloseSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CloseSupp.Click
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, False)
        cProc.DisposeGridView(gvSupplier)
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        'ClearDtlAP()
        Session("tbldtl") = Nothing
        trnsuppoid.Text = gvSupplier.SelectedDataKey.Item("ID")
        suppnames.Text = gvSupplier.SelectedDataKey("Name").ToString
        'ReqOid.Text = gvSupplier.SelectedDataKey("ReqOid").ToString
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, False)
        cProc.DisposeGridView(gvSupplier)
    End Sub

    Protected Sub trndpapoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndpapoid.SelectedIndexChanged
        If trndpapoid.Items.Count > 0 Then
            FillDPBalance(trndpapoid.SelectedValue)
            FillDPAccount(trndpapoid.SelectedValue)
        Else
            FillDPBalance(0)
        End If
    End Sub

    Private Sub FillDPAccount(ByVal iDPOid As Integer)
        sSql = "SELECT trndpaRacctgoid acctgoid FROM QL_trndpaR WHERE cmpcode='" & cmpcode & "' AND trndpaRoid='" & iDPOid & "'"
        cashbankacctgoid.SelectedValue = CStr(cKoneksi.ambilscalar(sSql))

        sSql = "SELECT currencyoid FROM QL_trndpaR WHERE cmpcode='" & cmpcode & "' AND trndpaRoid='" & iDPOid & "'"
        dp_currency.Text = CStr(cKoneksi.ambilscalar(sSql))
        CurrencyOid.Enabled = True
        If dp_currency.Text = 2 Then
            CurrencyOid.SelectedValue = 2
            CurrencyOid.Enabled = False

        End If
    End Sub

    Private Sub FillDPBalance(ByVal iDPOid As Integer)
        sSql = "SELECT ISNULL(dp.trndpaRamt-dp.trndpaRacumamt,0) "
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= "+ISNULL((SELECT SUM(pap.payamt) FROM QL_trnpayaR pap " & _
                "WHERE pap.paybankoid=dp.trndpaRoid AND pap.cashbankoid='" & Session("oid") & "'),0) "
        End If
        sSql &= "FROM QL_trndpaR dp WHERE cmpcode='" & cmpcode & "' AND trndpaRoid='" & iDPOid & "'"
        dpbalance.Text = ToMaskEdit(ToDouble(CStr(cKoneksi.ambilscalar(sSql))), 4)

        payrefno.Text = RTrim(GetStrData("select trndpaRno from ql_trndpaR where trndpaRoid=" & iDPOid))
    End Sub

    Private Sub InitDPAR(ByVal iSuppOid As Integer)
        sSql = "SELECT dp.trndpaRoid,dp.trndpaRno/*+' ('+CONVERT(VARCHAR,dp.trndpaRdate,101)+')'*/ AS no FROM QL_trndpaR dp WHERE cmpcode='" & cmpcode & "' AND dp.trndpaRstatus='POST' AND dp.CUSToid=" & iSuppOid & " AND (ISNULL(dp.trndpaRamt-dp.trndpaRacumamt,0)"
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= "+ISNULL((SELECT SUM(par.payamt) FROM QL_trnpayaR par " & _
                "WHERE par.paybankoid=dp.trndpaRoid AND par.cashbankoid='" & Session("oid") & "'),0)"
        End If
        sSql &= ")>0 ORDER BY dp.trndpaRdate,dp.trndpaRno "
        FillDDL(trndpapoid, sSql)
    End Sub

    Protected Sub CREDITCLEAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CREDITCLEAR.Click
        payrefno.Text = ""
        code.Text = ""
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Private Sub ShowCOA(ByVal noRef As String, ByVal cmpcode As String, ByVal Obj As GridView)
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim ssql As String = "select a.acctgcode acctgcode, a.acctgdesc, case d.gldbcr when  'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit', glnote from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid where noref ='" & noRef & "' and d.cmpcode='" & cmpcode & "' order by glseq"
        Dim xadap As New SqlDataAdapter(ssql, conn)
        Dim otable As New DataTable
        xadap.Fill(otable)
        If otable.Rows.Count = 0 Then
            ssql = "select a.acctgcode acctgcode , a.acctgdesc, case d.gldbcr when  'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit' , glnote from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid  where glnote like '%" & noRef & "' and d.cmpcode='" & cmpcode & "' order by glseq"
            otable.Rows.Clear()
            Dim xadap2 As New SqlDataAdapter(ssql, conn)
            xadap2.Fill(otable)
        End If
        Obj.DataSource = otable
        Obj.DataBind()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        ShowCOA(cashbankno.Text, cmpcode, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = NewMaskEdit(e.Row.Cells(3).Text)
            e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
        End If
    End Sub

    Protected Sub GVmstPAYAP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVmstPAYAP.PageIndex = e.NewPageIndex
        binddata("")
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False
        mpeMsgbox.Hide()
        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnPaySrv.aspx?awal=true")
        End If
    End Sub

    Protected Sub ibtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtn.Click
        Dim sMsg As String = ""
        If trnbelimstoid.Text = "" Then
            sMsg &= "- Pilih Nota Jual !!<BR>"
        End If
        If ToDouble(amtpayment.Text) <= 0 Then
            sMsg &= "- Total Payment must be >0 !!<BR>"
        End If
        If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
            payduedate.Text = PaymentDate.Text
        End If
        If payflag.SelectedValue = "GIRO" And ToDouble(amtpayment.Text) > ToDouble(AmountGiro.Text) Then
            sMsg &= "Nilai pembayaran tidak boleh lebih besar dari nilai giro !"
        End If

        If chkOther.Checked Then
            If Session("DtlSelisih") Is Nothing Then
                sMsg &= "- Maaf, jika ada selisih amount bayar Detail Selisih Bayar harus di isi !!<BR>"
            Else
                Dim objCek As DataTable = Session("DtlSelisih")
                If objCek.Rows.Count < 1 Then
                    sMsg &= "- Maaf, jika ada selisih amount bayar Detail Selisih Bayar !!harus di isi !!<BR>"
                Else
                    Dim dTotalSelisih As Double = ToMaskEdit(ToDouble(objCek.Compute("SUM(amtdtlselisih)", "").ToString), 4)
                    If dTotalSelisih > ToDouble(ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 4)) Then
                        sMsg &= "- Maksimum total detail di selisih bayar =" & ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 4) & " !!<BR>"
                    End If
                    If DDLOtherType.SelectedValue = "+" Then
                        If dTotalSelisih <> ToDouble(ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 4)) Then
                            sMsg &= "- Total detail di selisih bayar harus = " & ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 4) & " !!<BR>"
                        End If
                    End If
                End If
            End If
        End If
        If txtNote.Text.Trim.Length > 200 Then
            sMsg &= "- Maksimal Note detail adalah 200 karakter !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If
        'GET ACCTGOID DARI ALL DP
        Dim VAR_DPAR As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='VAR_DPAR'")
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_DPAR & "%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
        Session("acctgoid_dp") = cKoneksi.ambiltabel(sSql, "acctgoid_dp")
        If (Session("oid") = Nothing Or Session("oid") = "") And Session("tbldtl") Is Nothing Then
            Dim dtlTable As DataTable = SetTabelDetail()
            Session("tbldtl") = dtlTable
        End If

        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        If I_U2.Text = "New Detail" Then
            dv.RowFilter = "payrefoid='" & Trim(trnbelimstoid.Text) & "' AND flagdtl<>'OTHER'"
        Else
            dv.RowFilter = "payrefoid='" & Trim(trnbelimstoid.Text) & "' AND flagdtl<>'OTHER' AND payseq<>" & Payseq.Text
        End If
        If dv.Count > 0 Then
            showMessage("Data sudah ditambahkan sebelumnya !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            dv.RowFilter = ""
            GVDtlPayAP.DataSource = Session("tbldtl")
            GVDtlPayAP.DataBind()
            Exit Sub
        End If
        dv.RowFilter = ""

        ' DELETE data bila ada
        For C1 As Integer = objTable.Rows.Count - 1 To 0 Step -1
            If objTable.Rows(C1).Item("payrefoid") = trnbelimstoid.Text Then
                objTable.Rows.RemoveAt(C1)
            End If
        Next
        Dim hh As String = reqperson.Text
        Dim oRow As DataRow

        oRow = objTable.NewRow
        oRow("payseq") = objTable.Rows.Count + 1
        oRow("paymentoid") = 0
        oRow("cashbankoid") = 0
        oRow("reqperson") = reqperson.Text
        oRow("payreftype") = "ql_trnjualmst"
        oRow("payrefoid") = trnbelimstoid.Text
        oRow("payacctgoid") = acctgoid.Text
        'oRow("paynote") = IIf(txtNote.Text = "", trnbelino.Text & " ~ ", "") & "" & txtNote.Text
        If txtNote.Text = "" Then
            If trnbelino.Text <> txtNote.Text Then
                oRow("paynote") = trnbelino.Text & " ~ " & txtNote.Text
            Else
                oRow("paynote") = txtNote.Text
            End If
        Else
            oRow("paynote") = trnbelino.Text & " ~ " & txtNote.Text
        End If
        If chkOther.Checked Then
            If DDLOtherType.SelectedValue = "+" Then
                oRow("payamt") = ToDouble(APAmt.Text)
                oRow("payres1") = "LEBIH BAYAR"
            ElseIf DDLOtherType.SelectedValue = "-" Then
                oRow("payamt") = ToDouble(totalpayment.Text)
                oRow("payres1") = "KURANG BAYAR"
            End If
        Else
            oRow("payamt") = ToDouble(amtpayment.Text)
            oRow("payres1") = ""
        End If
        oRow("trnbelino") = trnbelino.Text
        oRow("suppname") = SuppName.Text
        oRow("amttrans") = ToDouble(amttrans.Text)
        oRow("amtpaid") = ToDouble(amtpaid.Text)
        oRow("amtretur") = ToDouble(amtretur.Text)
        oRow("acctgdesc") = APAcc.Text
        'oRow("payamt") = ToDouble(amtpayment.Text)
        oRow("reqcode") = ReqCode.Text
        oRow("reqoid") = reqoid.Text
        If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
            payduedate.Text = Format(GetServerTime, "dd/MM/yyyy")
        End If
        oRow("payduedate") = "01/01/1900" ''toDate(payduedate.Text)
        oRow("payrefno") = payrefno.Text
        oRow("flagdtl") = ""
        oRow("trndpapoid") = 0
        'oRow("sParts") = ToDouble(sParts.Text)
        'oRow("sJasa") = ToDouble(sJasa.Text)
        'If payflag.SelectedValue = "DP" Then
        '    oRow("DPAmt") = ToDouble(dpbalance.Text)
        'Else
        '    oRow("DPAmt") = 0
        'End If
        objTable.Rows.Add(oRow)

        ' Bila ada selisih
        If chkOther.Checked Then
            Dim tbSls As DataTable = Session("DtlSelisih")
            Dim objTableCek As DataTable = Session("DtlSelisih")
            'If objTableCek.Rows.Count <= 0 Then
            '    sMsg &= "- Tidak ada detail bayar !!<BR>"
            '    gvDtlSelisih.DataSource = Session("tbldtl")
            '    gvDtlSelisih.DataBind()
            'End If

            Dim dTotalSelisih As Double = ToDouble(tbSls.Compute("SUM(amtdtlselisih)", "").ToString)
            For C2 As Integer = 0 To tbSls.Rows.Count - 1
                Dim PlusRow As DataRow
                PlusRow = objTable.NewRow
                PlusRow("payseq") = objTable.Rows.Count + 1
                PlusRow("paymentoid") = 0
                PlusRow("cashbankoid") = 0
                PlusRow("payreftype") = "ql_trnjualmst"
                PlusRow("payrefoid") = trnbelimstoid.Text
                PlusRow("payacctgoid") = tbSls.Rows(C2)("acctgoid").ToString
                PlusRow("paynote") = trnbelino.Text & " ~ " & tbSls.Rows(C2)("dtlnoteselisih").ToString
                If DDLOtherType.SelectedValue = "+" Then
                    PlusRow("payamt") = ToDouble(tbSls.Rows(C2)("amtdtlselisih").ToString)
                    'PlusRow("payamt") = ToDouble(amtpayment.Text) - dTotalSelisih
                    PlusRow("payres1") = "LEBIH BAYAR"
                ElseIf DDLOtherType.SelectedValue = "-" Then
                    'PlusRow("payamt") = ToDouble(amtpayment.Text) + dTotalSelisih
                    PlusRow("payamt") = ToDouble(tbSls.Rows(C2)("amtdtlselisih").ToString) * -1
                    PlusRow("payres1") = "KURANG BAYAR"
                Else
                    'PlusRow("payamt") = ToDouble(amtpayment.Text) + dTotalSelisih
                    PlusRow("payamt") = ToDouble(tbSls.Rows(C2)("amtdtlselisih").ToString) * -1
                    PlusRow("payres1") = ""
                End If
                PlusRow("trnbelino") = trnbelino.Text
                PlusRow("suppname") = SuppName.Text
                PlusRow("amttrans") = ToDouble(amttrans.Text)
                PlusRow("amtpaid") = ToDouble(amtpaid.Text)
                PlusRow("amtretur") = ToDouble(amtretur.Text)
                PlusRow("acctgdesc") = tbSls.Rows(C2)("acctgdesc").ToString

                If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
                    payduedate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                End If
                PlusRow("payduedate") = toDate(payduedate.Text)
                'PlusRow("payrefno") = payrefno.Text
                PlusRow("flagdtl") = "OTHER"
                PlusRow("trndpapoid") = 0
                Dim otableDP As DataTable = Session("acctgoid_dp")
                If otableDP.Rows.Count > 0 Then
                    For c1 As Int16 = 0 To otableDP.Rows.Count - 1
                        If otableDP.Rows(c1).Item("acctgoid") = tbSls.Rows(C2)("acctgoid").ToString Then
                            PlusRow("trndpapoid") = 1
                        End If
                    Next
                End If
                'PlusRow("DPAmt") = 0
                objTable.Rows.Add(PlusRow)
            Next
        End If

        'resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            objTable.Rows(C2)("payseq") = C2 + 1
        Next
        Session("paymentOid") = trnbelimstoid.Text
        'showMessage(Session("paymentOid") & "=>" & Session("invCurrOid"), CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        Session("tbldtl") = objTable
        'GVDtlPayAP.Columns(8).Visible = True
        GVDtlPayAP.DataSource = Session("tbldtl")
        GVDtlPayAP.DataBind()
        ClearDtlAP(True)
        calcTotalInGridDtl()
        GVDtlPayAP.SelectedIndex = -1
    End Sub

    'Sub BinddataGiro()
    '    Dim rate As Double = InvoiceRate(CurrencyOid.SelectedValue)
    '    If ToDouble(trnsuppoid.Text) > 0 Then
    '        If CurrencyOid.SelectedValue = 2 Then
    '            sSql = "SELECT girodtloid, Trans_No, girono, m.gendesc Bank, bankoid, NoRekening, convert(char(10),d.GiroDueDate,103) GiroDueDate, amount / " & rate & " as amount from ql_giro g INNER JOIN QL_GIRODTL d on g.girooid=d.girooid INNER JOIN QL_mstgen m on m.genoid=d.bankoid AND m.gengroup='BANK NAME' where g.status='Post' and custoid=" & trnsuppoid.Text & "  and  d.status='Not Used' and girodtloid not in (SELECT girodtloid  from ql_trncashbankmst where girodtloid>0)  and  " & DDLSuppIDX.SelectedValue & " like '%" & Tchar(txtFindSuppIDX.Text) & "%'"
    '        Else
    '            sSql = "SELECT girodtloid, Trans_No, girono, m.gendesc Bank, bankoid, NoRekening, convert(char(10),d.GiroDueDate,103) GiroDueDate, amount from ql_giro g INNER JOIN QL_GIRODTL d on g.girooid=d.girooid INNER JOIN QL_mstgen m on m.genoid=d.bankoid AND m.gengroup='BANK NAME' where g.status='Post' and custoid=" & trnsuppoid.Text & " and d.status='Not Used' and girodtloid not in (SELECT girodtloid from ql_trncashbankmst where girodtloid>0) and " & DDLSuppIDX.SelectedValue & " like '%" & Tchar(txtFindSuppIDX.Text) & "%' "
    '        End If

    '        Dim tbldt As DataTable = CreateDataTableFromSQL(sSql)
    '        gvSupplierX.DataSource = tbldt
    '        gvSupplierX.DataBind()

    '        hiddenbtn2sX.Visible = True : Panel1X.Visible = True
    '        ModalPopupExtender3sX.Show()
    '    End If
    'End Sub

    'Protected Sub creditsearch_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles creditsearch.Click
    '    BinddataGiro()
    'End Sub

    'Protected Sub gvSupplierX_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvSupplierX.SelectedIndexChanged
    '    girooid.Text = gvSupplierX.SelectedDataKey("girodtloid").ToString()
    '    payrefno.Text = gvSupplierX.SelectedDataKey("girono") & " / " & gvSupplierX.SelectedDataKey("NoRekening")
    '    payduedate.Text = gvSupplierX.SelectedDataKey("GiroDueDate")
    '    dd_bankgiro.SelectedValue = gvSupplierX.SelectedDataKey("bankoid")
    '    AmountGiro.Text = NewMaskEdit(gvSupplierX.SelectedDataKey("amount"))
    '    hiddenbtn2sX.Visible = False : dd_bankgiro.Enabled = False
    '    dd_bankgiro.Visible = True : Panel1X.Visible = False
    '    ModalPopupExtender3sX.Hide()
    'End Sub

    'Protected Sub ibtnSuppIDX_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ibtnSuppIDX.Click
    '    BinddataGiro()
    'End Sub

    'Protected Sub imbViewAllsX_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbViewAllsX.Click
    '    BinddataGiro()
    'End Sub

    'Protected Sub CloseSuppX_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseSuppX.Click
    '    hiddenbtn2sX.Visible = False
    '    Panel1X.Visible = False
    '    ModalPopupExtender3sX.Hide()
    'End Sub

    'Protected Sub gvSupplierX_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSupplierX.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
    '    End If
    'End Sub

    Protected Sub DDLCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initDDLcashbank("CASH")
        If cashbankacctgoid.Items.Count < 1 Then
            showMessage("Isi/Buat account CASH di master accounting!! CASH Cabang " & DDLCabang.SelectedItem.Text & " !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
    End Sub
#End Region
End Class
