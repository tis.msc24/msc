Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptPOClose
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim CKon As New Koneksi
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim sSql As String = ""
#End Region

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub DdlPic()
        Dim upd As String = ""
        If checkPagePermission("Transaction/ft_trnpo.aspx", Session("SpecialAccess")) = False Then
            sSql = "SELECT personoid, personname FROM QL_mstperson prsn INNER JOIN QL_MSTPROF prof ON prof.USERNAME = prsn.PERSONNAME WHERE prof.cmpcode = '" & CompnyCode & "' and prsn.PERSONSTATUS in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC like '%AMP%') AND prof.userid='" & Session("UserID") & "'"
            FillDDL(pic, sSql)
        Else
            sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "' And PERSONSTATUS in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC like '%AMP%')"
            FillDDL(pic, sSql)
            pic.Items.Add(New ListItem("ALL", "ALL"))
            pic.SelectedValue = "ALL"
        End If
    End Sub

    Sub showPrint(ByVal tipe As String)
        Try
            Dim sWhere As String = "" : Dim namapdf As String = ""
            'Dim date1 As Date = range1.Text
            'Dim date2 As Date = range2.Text
            sWhere = " Where pom.cmpcode='" & CompnyCode & "' AND CONVERT(CHAR(20),pom.closetime,121) Between '" & Format(CDate(toDate(range1.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(range2.Text)), "yyyy-MM-dd") & "'"
            If pono.Text.Trim <> "" Then
                sWhere &= " And pom.trnbelipono like '%" & Tchar(pono.Text) & "%'"
            End If
            If suppname.Text.Trim <> "" Then
                sWhere &= " And suppname like '%" & Tchar(suppname.Text) & "%'"
            End If
            If ddltype.SelectedValue = "Summary" Then
                vReport.Load(Server.MapPath("~\Report\rptPOCloseSum.rpt"))
                namapdf = "PO_Close_Sum_"
            Else
                If barang.Text.Trim <> "" Then
                    sWhere &= " And exists (select x.itemoid from QL_podtl x inner join ql_mstitem y on x.itemoid=y.itemoid Where y.itemdesc like '%" & Tchar(barang.Text) & "%' and y.merk like '%" & Tchar(barang.Text) & "%' and x.trnbelimstoid=pom.trnbelimstoid)"
                End If
                If pic.SelectedValue <> "ALL" Then
                    sWhere &= "And personoid=" & pic.SelectedValue
                End If
                vReport.Load(Server.MapPath("~\Report\rptPOCloseDtl.rpt"))
                namapdf = "PO_Close_Dtl_"
            End If

            CProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("periode", range1.Text & "-" & range2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "" Then
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namapdf & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namapdf & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If

        Catch ex As Exception
            vReport.Close()
            vReport.Dispose()
            showMessage(ex.Message, CompnyName, 1)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptPOClose.aspx")
        End If

        Page.Title = CompnyCode & " - Laporan PO Close"
        DdlPic()
        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrint("")
            End If
        Else
            range1.Text = Format(New Date(Now.Year, Now.Month, 1), "dd/MM/yyyy")
            range2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        vReport.Close()
        vReport.Dispose()
        Label2.Text = ""
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        PanelErrMsg.Visible = False
        btnExtender.Visible = False
        MPEErrMsg.Hide()
    End Sub

    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowprint.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        Session("diprint") = "True"
        showPrint("")
    End Sub

    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        Session("diprint") = "False"
        showPrint("pdf")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        Session("diprint") = "False"
        showPrint("excel")
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("rptPOClose.aspx?awal=true")
    End Sub

    Protected Sub searchpo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles searchpo.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        sSql = "SELECT a.trnbelimstoid,a.trnbelipono,a.trnbelipodate,b.suppoid,b.suppname,a.trnbelistatus FROM ql_pomst a INNER JOIN ql_mstsupp b ON a.trnsuppoid=b.suppoid WHERE a.cmpcode='" & CompnyCode & "' And a.trnbelitype='GROSIR' AND a.trnbelipodate Between '" & date1 & "' And '" & date2 & "' And a.trnbelipono LIKE '%" & Tchar(pono.Text) & "%' AND b.suppname LIKE '%" & Tchar(suppname.Text) & "%' AND a.trnbelires1 = 'POCLOSE'"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "ql_trnpomst")
        GVPo.DataSource = dtab
        GVPo.DataBind()
        GVPo.Visible = True
        Session("GVPo") = dtab
        GVSupplier.Visible = False
        GVBarang.Visible = False

    End Sub

    Protected Sub erasepo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles erasepo.Click
        pono.Text = ""
        pooid.Text = ""

        GVPo.DataSource = Nothing
        GVPo.DataBind()
        GVPo.Visible = False
        Session("GVPo") = Nothing

    End Sub

    Protected Sub GVPo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVPo.PageIndexChanging
        If Not Session("GVPo") Is Nothing Then
            GVPo.DataSource = Session("GVPo")
            GVPo.PageIndex = e.NewPageIndex
            GVPo.DataBind()
        End If
    End Sub

    Protected Sub GVPo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVPo.SelectedIndexChanged
        pono.Text = GVPo.SelectedDataKey("trnbelipono")
        pooid.Text = GVPo.SelectedDataKey("trnbelimstoid")
        suppname.Text = GVPo.SelectedDataKey("suppname")
        suppoid.Text = GVPo.SelectedDataKey("suppoid")

        GVPo.DataSource = Nothing
        GVPo.DataBind()
        GVPo.Visible = False
        Session("GVPo") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
    End Sub

    Protected Sub searchsupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles searchsupp.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        sSql = "SELECT s.suppoid,s.suppcode,s.suppname FROM QL_mstsupp s WHERE (s.suppname LIKE '%" & Tchar(suppname.Text) & "%' OR s.suppcode LIKE '%" & Tchar(suppname.Text) & "%') AND s.suppflag='Active' AND s.suppoid IN (SELECT po.trnsuppoid FROM QL_pomst po WHERE s.suppoid=po.trnsuppoid AND po.branch_code = '" & Session("branch_id") & "' AND po.trnbelipodate BETWEEN '" & date1 & "' AND '" & date2 & "' AND po.cmpcode='" & CompnyCode & "' AND po.trnbelires1= 'POCLOSE')"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVSupplier")
        GVSupplier.DataSource = dtab
        GVSupplier.DataBind()
        GVSupplier.Visible = True
        Session("GVSupplier") = dtab

        GVPo.Visible = False
        GVBarang.Visible = False
    End Sub

    Protected Sub erasesupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles erasesupp.Click
        suppname.Text = ""
        suppoid.Text = ""

        GVSupplier.DataSource = Nothing
        GVSupplier.DataBind()
        GVSupplier.Visible = False
        Session("GVSupplier") = Nothing

    End Sub

    Protected Sub GVSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVSupplier.PageIndexChanging
        If Not Session("GVSupplier") Is Nothing Then
            GVSupplier.DataSource = Session("GVSupplier")
            GVSupplier.PageIndex = e.NewPageIndex
            GVSupplier.DataBind()
        End If
    End Sub

    Protected Sub GVSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVSupplier.SelectedIndexChanged
        suppname.Text = GVSupplier.SelectedDataKey("suppname")
        suppoid.Text = GVSupplier.SelectedDataKey("suppoid")

        GVSupplier.DataSource = Nothing
        GVSupplier.DataBind()
        GVSupplier.Visible = False
        Session("GVSupplier") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
    End Sub

    Protected Sub barangsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangsearch.Click
        sSql = "select itemoid,itemcode,itemdesc,Merk from QL_mstitem where itemflag='Aktif' and cmpcode='" & CompnyCode & "' and (itemcode like '%" & Tchar(barang.Text) & "%' or itemdesc like '%" & Tchar(barang.Text) & "%' or Merk like '%" & Tchar(barang.Text) & "%')"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVBarang")
        GVBarang.DataSource = dtab
        GVBarang.DataBind()
        GVBarang.Visible = True
        Session("GVBarang") = dtab

        GVPo.Visible = False
        GVSupplier.Visible = False
    End Sub

    Protected Sub barangerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangerase.Click
        barang.Text = ""
        barangoid.Text = ""
        merk.Text = ""

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing

    End Sub

    Protected Sub GVBarang_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVBarang.PageIndexChanging
        If Not Session("GVBarang") Is Nothing Then
            GVBarang.DataSource = Session("GVBarang")
            GVBarang.PageIndex = e.NewPageIndex
            GVBarang.DataBind()
        End If
    End Sub

    Protected Sub GVBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVBarang.SelectedIndexChanged
        barang.Text = GVBarang.SelectedDataKey("itemdesc")
        barangoid.Text = GVBarang.SelectedDataKey("itemoid")
        merk.Text = GVBarang.SelectedDataKey("Merk")

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
    End Sub

    Protected Sub ddltype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddltype.SelectedValue = "Summary" Then
            sPic.Visible = False : Label4.Visible = False : pic.Visible = False
        Else
            sPic.Visible = True : Label4.Visible = True : pic.Visible = True
        End If
    End Sub
End Class
