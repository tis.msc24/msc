Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnWh
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
    Public ItemcodeToSN As String
    Dim NoTWID As String
    Dim PrintDoc1 As New System.Drawing.Printing.PrintDocument
#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function


    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If itemoid.Text = "" Then
            sError &= "- Maaf, Pilihlah katalog dulu..!!<BR>"
        End If

        If dtlseq.Text = "" Then
            sError &= "- Maaf, Dimohon untuk isi qty..!!<BR>"
        Else
            If ToDouble(dtlseq.Text) <= 0 Then
                sError &= "- Maaf, Dimohon untuk isi qty diatas nol..!!<BR>"
            End If
        End If
        Dim sErr As String = ""
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("ReqQty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("dtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("ReqQty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("dtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView(0)("ReqQty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                        dtView(0)("dtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedure"
    Dim sMsg As String = ""
    Dim dtab As DataTable
    Dim drow As DataRow
    Dim drowedit() As DataRow

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trntwdtl")
        dtlTable.Columns.Add("dtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("ReqQty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("dtlnote", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        'ibitemsearch.Visible = bVal
    End Sub

    Private Sub ClearDetail()
        dtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
            dtlseq.Text = objTable.Rows.Count + 1
        Else
            EnableHeader(True)
        End If
        I_u2.Text = "new"
        itemdesc.Text = "" : itemoid.Text = ""
        itemCode.Text = "" : notedtl.Text = "" : dtlseq.Text = 1
        qty.Text = "0.00" : GVItemDetail.SelectedIndex = -1 
    End Sub

    Private Sub FillTextbox()
        Try
            Dim oidasal, oidtujuan As Integer
            sSql = "SELECT trntwmstoid,branch_code,trntwmstno,CONVERT(VARCHAR(20),trntwmstdate,103) trntwmstdate,frommtrlocoid,tomtrlocoid,trntwmststatus,crtuser,crtdate,updtime,upduser,trntwmstnote FROM QL_trntwmst WHERE trntwmstoid = " & Integer.Parse(Session("oid")) & ""
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    DDLCabang.SelectedValue = xreader("branch_code")
                    DDLCabang_SelectedIndexChanged(Nothing, Nothing)
                    transferno.Text = xreader("trntwmstno")
                    transferdate.Text = Format(xreader("trntwmstdate"), "dd/MM/yyyy")
                    note.Text = xreader("trntwmstnote")
                    oidasal = xreader("frommtrlocoid")
                    oidtujuan = xreader("tomtrlocoid")
                    trnstatus.Text = xreader("trntwmststatus").ToString
                    upduser.Text = xreader("upduser")
                    updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
                End While
            End If
            xreader.Close()
            GdAsal.SelectedValue = oidasal
            sSql = "Select a.genoid, a.gendesc from QL_mstgen a Where a.gengroup='LOCATION' AND a.genother2 IN (Select genoid from QL_mstgen br Where br.genoid=a.genother2 AND br.gencode='" & DDLCabang.SelectedValue & "' AND gengroup='CABANG') AND a.genoid<>'" & oidasal & "' AND a.genother6 IN ('UMUM','ECOMMERCE')"
            FillDDL(GdTujuan, sSql)
            GdTujuan.SelectedValue = oidtujuan
            If trnstatus.Text = "POST" Then
                btnPosting.Visible = False : btnDelete.Visible = False
                btnSave.Visible = False : btnDelete.Visible = False
            Else
                btnPosting.Visible = True : btnDelete.Visible = True
                btnSave.Visible = True
            End If

            sSql = "SELECT td.trntwdtloid,trntwdtlseq dtlseq,i.itemoid,i.itemcode,i.itemdesc,td.trntwdtlqty ReqQty,td.trntwdtlnote dtlnote FROM QL_trntwdtl td INNER  JOIN QL_mstitem i ON i.itemoid=td.itemoid WHERE td.trntwmstoid=" & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            Dim dtab As DataTable = ckon.ambiltabel(sSql, "QL_twdtl")
            Session("TblDtl") = dtab
            GVItemDetail.DataSource = dtab
            GVItemDetail.DataBind()
        Catch ex As Exception
            xreader.Close()
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub BindData()
        sSql = "SELECT branch_code,cb.gendesc,trntwmstoid,trntwmstno,CONVERT(VARCHAR(20),trntwmstdate,103) trntwmstdate,trntwmststatus,ga.gendesc asal,gt.gendesc tujuan,trntwmstnote FROM QL_trntwmst tw INNER JOIN QL_mstgen cb ON cb.gencode=tw.branch_code AND cb.gengroup='CABANG' INNER JOIN QL_mstgen ga ON ga.genoid=tw.frommtrlocoid AND ga.gengroup='LOCATION' INNER JOIN QL_mstgen gt ON gt.genoid=tw.tomtrlocoid AND gt.gengroup='LOCATION' WHERE tw.cmpcode='" & cmpcode & "' AND " & DDLFilter.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If fCbangDDL.SelectedValue <> "ALL" Then
            sSql &= " AND branch_code='" & fCbangDDL.SelectedValue & "'"
        End If
        If ddlstatus.SelectedValue <> "ALL" Then
            sSql &= " AND trntwmststatus='" & ddlstatus.SelectedValue & "'"
        End If

        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND trntwmstdate>='" & FilterPeriod1.Text & " 00:00:00' AND trntwmstdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        sSql &= " ORDER BY trntwmstno DESC"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "QL_trntwmst")
        Session("trntwmst") = dtab
        gvMaster.DataSource = dtab
        gvMaster.DataBind() 
    End Sub

    Private Sub BindItem()
        sSql = "SELECT 'False' AS checkvalue,itemoid,itemcode,itemdesc,SUM(con.qtyIn) QtyIn,SUM(qtyOut) QtyOut,ISNULL(SUM(con.qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00) SaldoAkhir,0.00 ReqQty,statusitem,'' dtlnote," & dtlseq.Text & " dtlseq FROM QL_conmtr con INNER JOIN QL_mstitem i ON itemoid=con.refoid WHERE con.mtrlocoid=" & GdAsal.SelectedValue & " AND periodacctg='" & GetServerTime.ToString("yyyy-MM") & "' AND con.branch_code='" & DDLCabang.SelectedValue & "' itemflag = 'Aktif' GROUP BY itemoid,itemcode,itemdesc,con.branch_code,refoid,mtrlocoid,statusitem ORDER BY itemcode"
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
        Session("ItemPilih") = objTable
        gvListMat.DataSource = objTable
        gvListMat.DataBind()
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDLCabang()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("LevelUser") = 4 Or Session("LevelUser") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLCabang, sSql)
        ElseIf Session("LevelUser") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLCabang, sSql)
            Else
                FillDDL(DDLCabang, sSql)
            End If
        ElseIf Session("LevelUser") = 1 Or Session("LevelUser") = 3 Then
            FillDDL(DDLCabang, sSql)
        End If
    End Sub

    Private Sub fInitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCbangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCbangDDL, sSql)
            Else
                FillDDL(fCbangDDL, sSql)
                fCbangDDL.Items.Add(New ListItem("ALL", "ALL"))
                fCbangDDL.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fCbangDDL, sSql)
            fCbangDDL.Items.Add(New ListItem("ALL", "ALL"))
            fCbangDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitGudang(ByVal genother6 As String)
        sSql = "Select a.genoid, a.gendesc From QL_mstgen a Where a.gengroup='LOCATION' AND a.genother2 IN (Select genoid from QL_mstgen br Where br.genoid=a.genother2 AND br.gencode='" & DDLCabang.SelectedValue & "' AND gengroup='CABANG') " & genother6 & ""
        FillDDL(GdAsal, sSql)
    End Sub

    Private Sub InitTujuan(ByVal genother6 As String)
        sSql = "Select a.genoid, a.gendesc from QL_mstgen a Where a.gengroup='LOCATION' AND a.genother2 IN (Select genoid from QL_mstgen br Where br.genoid=a.genother2 AND br.gencode='" & DDLCabang.SelectedValue & "' AND gengroup='CABANG') AND a.genoid<>'" & GdAsal.SelectedValue & "' AND a.genother6 IN ('UMUM', 'ECOMMERCE')"
        FillDDL(GdTujuan, sSql)
    End Sub

    Private Sub BindMatData()
        sSql = "SELECT * FROM (SELECT 'False' AS checkvalue,itemoid,itemcode,itemdesc,SUM(con.qtyIn) QtyIn,SUM(qtyOut) QtyOut,ISNULL(SUM(con.qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00) SaldoAkhir,ISNULL(SUM(con.qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00) ReqQty,statusitem,'' dtlnote," & dtlseq.Text & " dtlseq,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya FROM QL_conmtr con INNER JOIN QL_mstitem i ON itemoid=con.refoid WHERE con.mtrlocoid=" & GdAsal.SelectedValue & " AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND con.branch_code='" & DDLCabang.SelectedValue & "' GROUP BY itemoid,itemcode,itemdesc,con.branch_code,refoid,mtrlocoid,statusitem,i.stockflag) i ORDER BY itemcode"
        Session("TblMat") = ckon.ambiltabel(sSql, "QL_mstitem")
        gvListMat.Visible = True
    End Sub

    Private Sub GeneratedNo()
        Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & DDLCabang.SelectedValue & "' AND gengroup='CABANG'")
        Dim sNo As String = "TWE/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(trntwmstno,'" & sNo & "',''))),0)+1 FROM [QL_trntwmst] WHERE trntwmstno LIKE '" & sNo & "%' and branch_code='" & DDLCabang.SelectedValue & "'"
        transferno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub 
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\Login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            'simpan session k variabel spy tidak hilang
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            'di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
            Response.Redirect("~\Transaction\trnWh.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')") 
        Page.Title = CompnyName & " - TW E-COMMERCE"

        Session("LevelUser") = GetStrData("SELECT userlevel FROM QL_mstprof WHERE BRANCH_CODE='" & Session("branch_id") & "' And USERID='" & Session("UserID") & "'")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        If Not Page.IsPostBack Then
            fInitCabang() : BindData()
            transferno.Text = GenerateID("QL_trntwmst", cmpcode)
            transferdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitDDLCabang()
            DDLCabang_SelectedIndexChanged(Nothing, Nothing)
            InitTujuan("AND a.genother6 IN ('UMUM','ECOMMERCE')")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
                FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                TabContainer1.ActiveTabIndex = 0

                Trntwmstoid.Text = GenerateID("QL_trntwmst", cmpcode)
                'Dim trnmtrdtloid As Int32 = GenerateID("ql_trntcsuppintdtl", cmpcode)
                Dim conmtroid As Int32 = GenerateID("QL_conmtr", cmpcode)
                qty.Text = "0.00"
                GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                trnstatus.Text = "In Process"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterText.Text = ""
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbPeriode.Checked = False : ddlstatus.SelectedValue = "ALL"
        BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If

        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("UserInput") Is Nothing And Session("UserInput") <> "" Then
            Response.Redirect("~\other\Menu.aspx?page=true")
        End If
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "8"
        gvListMat.PageSize = CInt(tbData.Text)
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnWh.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        Dim qtyval As Double = 0.0
        If qty.Text.Trim <> "" Then
            If Double.TryParse(qty.Text.Trim, qtyval) Then
                qty.Text = ToMaskEdit(qtyval, 3)
            Else
                qty.Text = ToMaskEdit(0.0, 3)
            End If
        Else
            qty.Text = ToMaskEdit(0.0, 3)
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If I_u2.Text = "new" Then
                dv.RowFilter = "itemoid=" & itemoid.Text & ""
                If dv.Count > 0 Then
                    sMsg &= "- Maaf, Katalog sudah di input..!!<br />"
                    dv.RowFilter = ""
                End If
            Else
                dv.RowFilter = "itemoid=" & itemoid.Text & " AND dtlseq <>" & dtlseq.Text & ""
                If dv.Count > 0 Then
                    sMsg &= "- Maaf, Katalog sudah di input..!!<br />"
                    dv.RowFilter = ""
                End If
            End If

            If ToDouble(qty.Text) > ToDouble(labelmaxqty.Text) Then
                sMsg &= "- Maaf, Quantity " & itemdesc.Text & " yang anda isi " & ToMaskEdit(qty.Text, 3) & ", melebihi stok akhir " & ToMaskEdit(labelmaxqty.Text, 3) & ""
            End If

            If sMsg <> "" Then
                showMessage(sMsg, 2)
                Exit Sub
            End If

            dv.RowFilter = ""
            Dim objRow As DataRow
            If I_u2.Text = "new" Then
                objRow = objTable.NewRow()
                objRow("dtlseq") = objTable.Rows.Count + 1
            Else
                Dim selrow As DataRow() = objTable.Select("dtlseq=" & dtlseq.Text)
                objRow = selrow(0)
                objRow.BeginEdit() 
            End If

            objRow = objTable.NewRow() 
            objRow("itemoid") = itemoid.Text
            objRow("itemcode") = itemCode.Text
            objRow("itemdesc") = itemdesc.Text
            objRow("ReqQty") = ToDouble(qty.Text)
            objRow("dtlnote") = notedtl.Text
            If I_u2.Text = "new" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            GVItemDetail.DataSource = objTable
            GVItemDetail.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If
        itemdesc.Text = "" : qty.Text = ToMaskEdit(0, 3)
        notedtl.Text = ""
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable = Session("itemdetail")
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()

        '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@

        If Not Session("TblSN") Is Nothing Then
            'Dim country As String = GVItemDetail.Rows(e.RowIndex).Cells(5).Text
            Dim objTableSN As DataTable = Session("TblSN")
            Dim dvSN As DataView = objTableSN.DefaultView
            Dim itemid As Integer = Integer.Parse(gvr.Cells(2).Text)
            dvSN.RowFilter = "itemoid = " & itemid
            If Not dvSN.Count = 0 Then
                For kdsn As Integer = 0 To dvSN.Count - 1
                    objTableSN.Rows.RemoveAt(0)
                Next
            End If
            objTableSN.AcceptChanges()
        End If
        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab 
    End Sub

    Protected Sub GVItemDetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVItemDetail.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Protected Sub GVItemDetail_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles GVItemDetail.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("dtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        GVItemDetail.DataSource = objTable
        GVItemDetail.DataBind()
        ClearDetail()
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        Try
            dtlseq.Text = GVItemDetail.SelectedDataKey.Item("dtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                I_u2.Text = "Update"
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "dtlseq=" & dtlseq.Text
                qty.Text = ToMaskEdit(dv.Item(0).Item("ReqQty"), 3)
                itemoid.Text = dv.Item(0).Item("itemoid").ToString
                itemCode.Text = dv.Item(0).Item("itemcode").ToString
                itemdesc.Text = dv.Item(0).Item("itemdesc").ToString
                notedtl.Text = dv.Item(0).Item("dtlnote").ToString
                dv.RowFilter = ""
                labelmaxqty.Text = ToMaskEdit(GetScalar("SELECT SUM(con.qtyIn) QtyIn,SUM(qtyOut) QtyOut,ISNULL(SUM(con.qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00) SaldoAkhir FROM QL_conmtr con WHERE con.mtrlocoid=" & GdAsal.SelectedValue & " AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND con.branch_code='" & DDLCabang.SelectedValue & "' AND con.refoid=" & itemoid.Text & " GROUP BY con.branch_code,refoid,mtrlocoid"), 3)
            End If
            ibitemsearch.Visible = True 
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click 

        If GVItemDetail.Rows.Count <= 0 Then
            sMsg &= "- Maaf, Detail transfer tidak boleh kosong..!!<br />"
        End If

        If i_u.Text = "new" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trntwmst WHERE crtdate='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "- Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!<BR />"
            End If
        Else
            sSql = "SELECT [trntwmststatus] FROM QL_trntwmst WHERE trntwmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'" 
            Dim srest As String = GetScalar(sSql)
            If srest.ToLower Is Nothing Or srest.ToLower = "" Then
                sMsg &= " - Maaf, Data transfer service tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain..!!<br >"
            Else
                If srest.ToLower = "post" Then
                    sMsg &= "- Maaf, Data transfer service tidak dapat diposting !<br />Periksa bila data telah diposting oleh user lain..!!<br />"
                End If
            End If
        End If

        If sMsg.Trim <> "" Then
            showMessage(sMsg, 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If i_u.Text = "new" Then
            Trntwmstoid.Text = GenerateID("QL_trntwmst", cmpcode)
            transferno.Text = GenerateID("QL_trntwmst", cmpcode)
        Else
            Trntwmstoid.Text = Integer.Parse(Session("oid"))
        End If

        If trnstatus.Text = "POST" Then
            GeneratedNo()
        Else

        End If
        Dim trntwdtloid As Int32 = GenerateID("QL_trntwdtl", cmpcode)
        Dim conmtroid As Int32 = GenerateID("QL_conmtr", cmpcode)

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & GetDateToPeriodAcctg(GetServerTime()) & "' AND closingdate = '1/1/1900'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = 0 Then
                objTrans.Rollback() : conn.Close()
                showMessage("Tanggal ini tidak dalam periode Open Stock !", 2)
                trnstatus.Text = "In Process"
                Exit Sub
            End If

            'Cek saldoakhir
            If Not Session("TblDtl") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("TblDtl")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = " SELECT ISNULL(SUM(qtyin)-SUM(qtyOut),0.00) FROM QL_conmtr con Inner Join ql_mstitem i ON i.itemoid=con.refoid WHERE con.periodacctg = '" & GetDateToPeriodAcctg(GetServerTime()) & "' AND con.branch_code = '" & DDLCabang.SelectedValue & "' AND con.mtrlocoid = " & GdAsal.SelectedValue & " AND refoid =" & dtab.Rows(j).Item("itemoid") & " AND con.cmpcode = '" & cmpcode & "' Group BY mtrlocoid,branch_code,refoid,periodacctg,i.stockflag"
                    xCmd.CommandText = sSql : saldoakhire = xCmd.ExecuteScalar
                    If saldoakhire = Nothing Or saldoakhire = 0 Then
                        sMsg &= "- Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "', karena saldo akhir satuan std hanya tersedia 0.00..!!<br />"
                        trnstatus.Text = "In Process"
                        Exit Sub
                    End If
                Next
                If sMsg.Trim <> "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage(sMsg, 2)
                    trnstatus.Text = "In Process"
                    Exit Sub
                End If

            End If
 
            'Insert new record
            If i_u.Text = "new" Then
                sSql = "INSERT INTO [QL_trntwmst]([cmpcode],[trntwmstoid],[branch_code],[trntwmstno],[trntwmstdate],[frommtrlocoid],[tomtrlocoid],[trntwmstnoref],[trntwmstnote],[trntwmststatus],[crtuser],[crtdate],[upduser],[updtime])" & _
                " VALUES ('" & cmpcode & "'," & Integer.Parse(Trntwmstoid.Text) & ",'" & DDLCabang.SelectedValue & "','" & transferno.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))," & GdAsal.SelectedValue & "," & GdTujuan.SelectedValue & ",'','" & Tchar(note.Text) & "','" & trnstatus.Text & "','" & Tchar(Session("UserID")) & "',Current_timestamp,'" & Tchar(Session("UserID")) & "',Current_timestamp)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_trntrfmtrmst
                sSql = "UPDATE QL_mstoid SET lastoid = " & Trntwmstoid.Text & " WHERE tablename = 'QL_trntwmst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                'Update master record
                sSql = " UPDATE [QL_trntwmst] SET [branch_code] = '" & DDLCabang.SelectedValue & "',[trntwmstno] ='" & transferno.Text & "',[trntwmstdate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),[frommtrlocoid] =" & GdAsal.SelectedValue & ",[tomtrlocoid] =" & GdTujuan.SelectedValue & ",[trntwmstnote] = '" & Tchar(note.Text) & "',[trntwmststatus]='" & trnstatus.Text & "' ,[upduser] = '" & Tchar(Session("UserID")) & "' ,[updtime]=Current_timestamp WHERE Trntwmstoid = " & Integer.Parse(Trntwmstoid.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM QL_trntwdtl WHERE Trntwmstoid = " & Integer.Parse(Session("oid")) & " "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'Insert to detail
            If Not Session("TblDtl") Is Nothing Then
                Dim obj As DataTable = Session("TblDtl") 
                For i As Integer = 0 To obj.Rows.Count - 1 
                    sSql = "INSERT INTO [QL_trntwdtl]([cmpcode], [trntwdtloid], [trntwmstoid], [trntwdtlseq], [itemoid],[trntwdtlqty], [trntwdtlunitoid], [trntwdtlnote], [upduser], [updtime]) VALUES ('" & cmpcode.ToLower & "'," & trntwdtloid + i & "," & Integer.Parse(Trntwmstoid.Text) & "," & obj.Rows(i)("dtlseq") & "," & obj.Rows(i)("itemoid") & "," & ToDouble(obj.Rows(i)("ReqQty")) & ",945,'" & Tchar(obj.Rows(i)("dtlnote")) & "','" & Session("UserID") & "',Current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If trnstatus.Text = "POST" Then
                        '--------------------------
                        sSql = "Select hpp from ql_mstitem Where itemoid=" & Integer.Parse(obj.Rows(i).Item("itemoid")) & "" : xCmd.CommandText = sSql
                        Dim lasthpp As Double = xCmd.ExecuteScalar
                        'Update crdmtr for item out
                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note,HPP,Branch_Code,conrefoid) VALUES " & _
                        "('" & cmpcode & "', " & conmtroid & ", 'TWE', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & transferno.Text & "', " & Integer.Parse(trntwdtloid) + i & ", 'QL_trntwdtl', " & Integer.Parse(obj.Rows(i).Item("itemoid")) & ", 'QL_MSTITEM', 945, " & GdAsal.SelectedValue & ", 0, " & obj.Rows(i).Item("ReqQty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(obj.Rows(i).Item("dtlnote")) & "', " & ToDouble(lasthpp) & ", '" & DDLCabang.SelectedValue & "', " & Integer.Parse(0) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1

                        '-- INSERT QTY IN --
                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,Branch_Code,conrefoid) VALUES " & _
                       "('" & cmpcode & "', " & conmtroid & ", 'TCI', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & transferno.Text & "', " & Integer.Parse(trntwdtloid + i) & ", 'QL_trntwdtl', " & Integer.Parse(obj.Rows(i).Item("itemoid")) & ", 'QL_MSTITEM', 945, " & GdTujuan.SelectedValue & ", " & obj.Rows(i).Item("ReqQty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(obj.Rows(i).Item("dtlnote")) & "', " & ToDouble(lasthpp) & ", '" & DDLCabang.SelectedValue & "', " & Integer.Parse(0) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                        '--------------------------- 
                    End If
                Next

                'Update lastoid QL_trntrfmtrdtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & trntwdtloid + obj.Rows.Count - 1 & "  WHERE tablename = 'QL_trntwdtl' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, 1)
            trnstatus.Text = "In Process"
            Exit Sub
        End Try
        Response.Redirect("trnWh.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "DELETE FROM QL_trntwmst WHERE trntwmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trntwdtl WHERE trntwmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Response.Redirect("trnWh.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        trnstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")
            Dim sWhere As String = " " & Integer.Parse(labeloid.Text) & " "
            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "TWGprintout.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("IDTW", sWhere)
            'report.PrintOptions.PaperSize = PaperSize.PaperStatement
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            sSql = "SELECT transferno from QL_trntrfmtrmst WHERE trfmtrmstoid = '" & sWhere & "'"
            Dim NOTW As String = ckon.ambilscalar(sSql)
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TW_" & NOTW & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close()
            report.Dispose()
            Response.Redirect("trnWh.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        Try
            If Not Session("trntwmst") Is Nothing Then
                Dim dtab As DataTable = Session("trntwmst")
                gvMaster.DataSource = dtab
                gvMaster.PageIndex = e.NewPageIndex
                gvMaster.DataBind()
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub
#End Region

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
 
        If FilterDDLListMat.SelectedIndex <> 0 Then
            sSql = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        Else
            If FilterTextListMat.Text <> "" Then
                Dim sText() As String = FilterTextListMat.Text.Split(";")
                For C1 As Integer = 0 To sText.Length - 1
                    If sText(C1) <> "" Then
                        sSql &= FilterDDLListMat.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
                    End If
                Next
                If sSql <> "" Then
                    sSql = Left(sSql, sSql.Length - 4)
                Else
                    sSql = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
                End If
            Else
                sSql = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            End If
        End If

        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sSql
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub 

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False" 
                    objView(0)("dtlnote") = ""
                    dtTbl.Rows(C1)("checkvalue") = "False" 
                    dtTbl.Rows(C1)("dtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = "checkvalue='True' AND ReqQty<=0.00"
                    If dtView.Count > 0 Then
                        Session("WarningListMat") = "- Maaf, Quantity masih nol, guys..!!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""

                    dtView.RowFilter = "checkvalue='True' AND ReqQty>SaldoAkhir"
                    If dtView.Count > 0 Then
                        Session("WarningListMat") = "- Maaf, Quantity melebihi jumlah stok akhir, guys..!!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True'"

                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If

                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "itemoid=" & dtView(C1)("itemoid") & ""
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("ReqQty") = dtView(C1)("ReqQty")
                            dv(0)("dtlnote") = dtView(C1)("dtlnote")
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("dtlseq") = counter
                            objRow("itemoid") = dtView(C1)("itemoid")
                            objRow("itemcode") = dtView(C1)("itemcode").ToString
                            objRow("itemdesc") = dtView(C1)("itemdesc").ToString
                            objRow("ReqQty") = ToDouble(dtView(C1)("ReqQty"))
                            objRow("dtlnote") = dtView(C1)("dtlnote")
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    GVItemDetail.DataSource = objTable
                    GVItemDetail.DataBind()
                    CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If 
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub DDLCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCabang.SelectedIndexChanged
        Dim QlFis As String = GetStrData("SELECT g.genother3 FROM QL_mstgen g INNER JOIN QL_mstgen cb ON cb.genoid=CAST(g.genother2 AS int) WHERE g.gengroup='LOCATION' AND cb.gengroup='CABANG' AND g.genother6='ECOMMERCE' AND cb.gencode='" & DDLCabang.SelectedValue & "'")
        Dim sCode As String = ""
        Dim Code() As String = QlFis.Split(",")
        For C1 As Integer = 0 To Code.Length - 1
            If Code(C1) <> "" Then
                sCode &= "'" & Code(C1).Trim & "',"
            End If
        Next
        sSql = "SELECT COUNT(personnoid) FROM QL_MSTPROF WHERE personnoid IN (" & Left(sCode, sCode.Length - 1) & ") AND USERID='" & Session("UserID") & "'"
        If GetScalar(sSql) > 0 Then
            sSql = " AND a.genother6 IN ('UMUM','ECOMMERCE')"
        Else
            sSql = " AND a.genother6 IN ('UMUM')"
        End If
        InitGudang(sSql) : InitTujuan(sSql)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLListMat.SelectedIndexChanged
        If FilterDDLListMat.SelectedIndex <> 0 Then
            FilterTextListMat.TextMode = TextBoxMode.SingleLine
        Else
            FilterTextListMat.TextMode = TextBoxMode.MultiLine
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3) 
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(0.0, 3)
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(CType(myControl, System.Web.UI.WebControls.TextBox).Text, 3)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub GdTujuan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdTujuan.SelectedIndexChanged
     
    End Sub

    Protected Sub GdAsal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdAsal.SelectedIndexChanged
        Dim QlFis As String = GetStrData("SELECT g.genother3 FROM QL_mstgen g INNER JOIN QL_mstgen cb ON cb.genoid=CAST(g.genother2 AS int) WHERE g.gengroup='LOCATION' AND cb.gengroup='CABANG' AND g.genother6='ECOMMERCE' AND cb.gencode='" & DDLCabang.SelectedValue & "'")
        Dim sCode As String = ""
        Dim Code() As String = QlFis.Split(",")
        For C1 As Integer = 0 To Code.Length - 1
            If Code(C1) <> "" Then
                sCode &= "'" & Code(C1).Trim & "',"
            End If
        Next
        sSql = "SELECT COUNT(personnoid) FROM QL_MSTPROF WHERE personnoid IN (" & Left(sCode, sCode.Length - 1) & ") AND USERID='" & Session("UserID") & "'"
        If GetScalar(sSql) > 0 Then
            sSql = " AND a.genother6 IN ('UMUM','ECOMMERCE')"
        Else
            sSql = " AND a.genother6 IN ('UMUM')"
        End If
        InitTujuan(sSql)
    End Sub
End Class
