<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptFastMoving.aspx.vb" Inherits="rptFastMoving" title="JPT - Adjusment Stok" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="width: 975px">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Fast Moving"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: transparent" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD vAlign=top align=left runat="server" Visible="true"><asp:Label id="Label2" runat="server" Width="62px" Text="Cabang" __designer:wfdid="w1"></asp:Label></TD><TD vAlign=top align=left runat="server" Visible="true">:</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="FilterDDLDiv" runat="server" Width="168px" CssClass="inpTextDisabled" __designer:wfdid="w2" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD vAlign=top align=left runat="server" Visible="true"><asp:Label id="Label1" runat="server" Width="61px" Text="Period" __designer:wfdid="w3"></asp:Label></TD><TD vAlign=top align=left runat="server" Visible="true">:</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w4" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w6"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w7" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton> <asp:Label id="Label4" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w9"></asp:Label></TD></TR><TR><TD vAlign=top align=left runat="server" Visible="true"><asp:Label id="Label3" runat="server" Width="96px" Text="Item / Barang " __designer:wfdid="w10"></asp:Label></TD><TD vAlign=top align=left runat="server" Visible="true">:</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="ItemName" runat="server" Width="255px" CssClass="inpTextDisabled" __designer:dtid="562949953421371" __designer:wfdid="w6" Enabled="False" Rows="2" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w12"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" onclick="btnEraseItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w13"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w20" Format="dd/MM/yyyy" PopupButtonID="CalPeriod1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w22" Format="dd/MM/yyyy" PopupButtonID="CalPeriod2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w21" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w23" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <asp:Label id="WarningListMat" runat="server" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnViewReport" onclick="btnViewReport_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w24"></asp:ImageButton> <asp:ImageButton id="ibpdf" onclick="ibpdf_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 138px" align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w28" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w40"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><asp:Label id="lblbln3" runat="server" Visible="False" __designer:wfdid="w2"></asp:Label><asp:Label id="lblbln1" runat="server" Visible="False" __designer:wfdid="w3"></asp:Label><asp:Label id="lblbln2" runat="server" Visible="False" __designer:wfdid="w4"></asp:Label><asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w5"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" __designer:wfdid="w41" ShowAllPageIds="True" AutoDataBind="true" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibpdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=4><asp:Label id="lblListMat" runat="server" Width="190px" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Katalog</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4>Filter : &nbsp;&nbsp; <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Kode Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=center colSpan=4>Jenis Barang : &nbsp; <asp:DropDownList id="dd_stock" runat="server" CssClass="inpText" Font-Size="Small"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:Button id="BtnSelectALL" runat="server" CssClass="btn red" Text="Select All"></asp:Button>&nbsp;<asp:Button id="BtnSelectNone" runat="server" CssClass="btn green" Text="Select None"></asp:Button></TD></TR><TR><TD class="Label" align=center colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 325px"><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" CellPadding="4" UseAccessibleHeader="False" PageSize="100" DataKeyNames="itemcode,itemdesc,itemoid,satuan3" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=4><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" PopupControlID="pnlListMat" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
                </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

