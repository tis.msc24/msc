Imports System.Data
Imports System.Data.SqlClient

Imports System.Windows.Forms
Imports ClassFunction

Partial Class Transaction_CheckTeknisi
    Inherits System.Web.UI.Page
#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String = ""    
    Dim cKoneksi As New Koneksi
    Dim CProc As New ClassProcedure

    '------------------------
    Dim IDGVJob As String = ""
    Dim sTitle As String = CompnyName & " - WARNING"
    Dim iCount As Integer = 0
    Dim rowDel As Integer = 0
    Dim hitung As Integer
#End Region

#Region "Procedure"

    Sub initTypeJob()
        Dim kCbg As String = ""
        If Session("UserID").ToString.ToUpper <> "ADMIN" Then
            kCbg &= "And g2.gencode='" & Session("branch_id").ToString & "'"
        End If
        sSql = "Select gencode ,g2.gendesc from ql_mstgen g2 where cmpcode = 'MSC' and gengroup = 'CABANG' " & kCbg & " ORDER BY g2.gencode"
        FillDDL(DdlCabang, sSql)

        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='SERVICETYPE' ORDER BY GENDESC DESC"
        FillDDL(ddlTypeJob, sSql)
    End Sub

    Private Sub InitLoc()
        sSql = "SELECT a.genoid,ISNULL((SELECT gendesc FROM QL_mstgen g WHERE g.genoid=a.genother1 AND g.gengroup = 'WAREHOUSE'),'GUDANG PERJALANAN') +' - '+a.gendesc AS gendesc,* From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.gencode <> 'EXPEDISI' AND c.gencode='" & DdlCabang.SelectedValue & "'"
        FillDDL(matLoc, sSql)
    End Sub

    Private Function checkFilter()
        'Dim sWhere As String = ""
        checkFilter = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If cbPeriod.Checked Then
            Try
                txtPeriod1.Enabled = True
                txtPeriod2.Enabled = True
                Dim date1 As Date = CDate(toDate(txtPeriod1.Text))
                Dim date2 As Date = CDate(toDate(txtPeriod2.Text))
                If txtPeriod1.Text = "" Then
                    showMessage("Silahkan isi Period 1 !", CompnyName & " - Warning", 2)
                End If
                If txtPeriod2.Text = "" Then
                    showMessage("Silahkan isi Period 2 !", CompnyName & " - Warning", 2)
                End If

                If date1 < CDate("01/01/1900") Then
                    showMessage("Invalid Start Date !", CompnyName & " - Warning", 2) : Exit Function
                End If
                If date2 < CDate("01/01/1900") Then
                    showMessage("Invalid End Date !", CompnyName & " - Warning", 2) : Exit Function
                End If
                If date1 > date2 Then
                    showMessage("Period 1 harus lebih kecil dari Period 2 !", CompnyName & " - Warning", 2)
                    txtPeriod1.Text = "" : txtPeriod2.Text = ""
                    Exit Function
                End If
                checkFilter &= " AND CONVERT(CHAR(20), QLR.CREATETIME, 101) BETWEEN '" & CDate(toDate(txtPeriod1.Text)) & "' AND '" & CDate(toDate(txtPeriod2.Text)) & "'"
            Catch ex As Exception
                showMessage("Format tanggal tidak sesuai", CompnyName & " - Warning", 2)
            End Try
        End If
        If cbStatus.Checked Then
            checkFilter &= " AND QLR.REQSTATUS LIKE '%" & ddlStatus.SelectedValue & "%'"
        End If
        If (txtFilter.Text = "" Or cbPeriod.Checked = False) And cbStatus.Checked = False Then
            checkFilter &= " AND QLR.REQSTATUS IN ('IN','Receive', 'Check')"
        End If
        Return checkFilter
    End Function

    Private Function IDRow()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT lastoid from ql_mstoid where tablename =  'QL_TRNMECHCHECK' "
        xCmd.CommandText = sSql
        IDRow = xCmd.ExecuteScalar()
        Return IDRow
    End Function

    Private Function IDRowSpart()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid where tablename = 'QL_TRNMECHPARTDTL'"
        xCmd.CommandText = sSql
        IDRowSpart = xCmd.ExecuteScalar()
        Return IDRowSpart
    End Function

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Sub ClearDetail()
        lbID.Text = "1"
        If Session("GVDetKerusakan") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("GVDetKerusakan")
            lbID.Text = objTable.Rows.Count + 1
        End If
        txtI_U.Text = "New"
    End Sub

    Sub ClearDetailPart()
        lbIDPart.Text = "1"
        If Session("GVDetSparepart") Is Nothing = False Then
            Dim objTableSpart As DataTable
            objTableSpart = Session("GVDetSparepart")
            lbIDPart.Text = objTableSpart.Rows.Count + 1
        End If
        txtI_U.Text = "New"
    End Sub

    Sub BindData(ByVal sWhere As String)
        If Session("UserID").ToString.ToLower <> "admin" Then
            sWhere &= " And usr.userid = '" & Session("UserID") & "'"
        End If
        sSql = "SELECT QLR.REQOID, QLR.BARCODE, QLR.REQCODE, QLC.CUSTNAME, QLM.GENDESC, QLR.REQITEMNAME, QLR.REQSTATUS ,prs.PERSONNAME FROM QL_TRNREQUEST QLR INNER JOIN QL_mstcust QLC ON QLR.REQCUSTOID=QLC.CUSTOID INNER JOIN QL_MSTGEN QLM ON QLR.REQITEMBRAND=QLM.GENOID inner join QL_MSTPERSON prs on QLR.REQPERSON = prs.PERSONOID inner join QL_MSTPROF usr on prs.PERSONNAME = usr.USERNAME WHERE QLR.cmpcode='MSC'" & sWhere & " ORDER BY QLR.REQOID DESC"
        Dim xTableItem0 As DataTable = cKoneksi.ambiltabel(sSql, "ChkTeknisi")
        GVListTeknisi.DataSource = xTableItem0
        GVListTeknisi.DataBind() : GVListTeknisi.SelectedIndex = -1
    End Sub

    Sub BindDataInfo(ByVal sWhere As String)
        sSql = "SELECT DISTINCT QLR.REQOID, QLR.REQCODE,qlr.barcode,CONVERT(CHAR(10), QLR.CREATETIME, 103) date, QLC.CUSTNAME,isnull(p.personname,'-') personname, QLR.REQITEMNAME, QLM.GENDESC as merk, QLR.REQSTATUS,QLR.itemoid FROM QL_TRNREQUEST QLR INNER JOIN QL_mstcust QLC ON QLR.REQCUSTOID=QLC.CUSTOID INNER JOIN QL_TRNREQUESTDTL QLD ON QLD.REQMSTOID=QLR.REQOID left join QL_MSTPERSON p on QLR.REQPERSON = p.PERSONOID INNER JOIN QL_MSTGEN QLM ON QLR.REQITEMBRAND=QLM.GENOID WHERE QLR.CMPCODE='" & CompnyCode & "'" & sWhere & " AND QLR.REQSTATUS IN ('POST','In') ORDER BY QLR.REQOID"
        Dim xTableItem1 As DataTable = cKoneksi.ambiltabel(sSql, "Info")
        GVListInfo.DataSource = xTableItem1
        GVListInfo.DataBind()
        GVListInfo.SelectedIndex = -1
    End Sub

    Sub BindDataCust(ByVal idPage As Integer)
        'Dim no As Integer = 0        
        sSql = "SELECT ROW_NUMBER() OVER(ORDER BY REQDTLOID) Row, REQDTLOID, REQDTLJOB FROM QL_TRNREQUESTDTL WHERE REQMSTOID=" & idPage & ""
        Dim xTableItem2 As DataTable = cKoneksi.ambiltabel(sSql, "Cust")
        GVCust.DataSource = xTableItem2
        GVCust.DataBind()
        GVCust.SelectedIndex = -1
    End Sub

    Sub BindDataJob(ByVal sEmpServ As String)
        sSql = "SELECT ITSERVOID, ITSERVDESC, ITSERVTARIF FROM QL_MSTITEMSERV WHERE ITSERVDESC LIKE '%" & Tchar(txtJob.Text) & "%' " & sEmpServ & ""
        Dim xTableItem3 As DataTable = cKoneksi.ambiltabel(sSql, "Job")
        GVJob.DataSource = xTableItem3
        GVJob.DataBind() : GVJob.SelectedIndex = -1
        MPEJob.Show()
    End Sub

    Sub BindDataSpart()
        sSql = "Select DISTINCT crd.branch_code,a.itemoid PARTOID,a.itemcode,a.itemdesc PARTDESCSHORT,a.HPP,isnull((select gendesc from QL_mstgen where genoid = a.itemgroupoid ),'') itemgroupcode,a.merk,a.updtime,Case When b.branch_code ='01' Then a.pricelist else ISNULL(a.pricelist + b.biayaExpedisi,0.00) end priceList,crd.saldoAkhir,periodacctg,a.satuan1,a.itemsafetystockunit1 From QL_crdmtr crd Inner Join ql_mstitem a ON a.itemoid=crd.refoid LEFT JOIN QL_mstItem_branch b ON a.itemcode=b.itemcode Where crd.mtrlocoid=" & matLoc.SelectedValue & " AND crd.periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(txtTglTerima.Text))) & "' AND (a.itemcode LIKE '%" & Tchar(txtNamaSpart.Text) & "%' OR a.itemdesc LIKE '%" & Tchar(txtNamaSpart.Text) & "%') ORDER BY itemcode ASC"
        Dim xTableItem4 As DataTable = cKoneksi.ambiltabel(sSql, "Sparepart")
        GVSpart.DataSource = xTableItem4
        GVSpart.DataBind()
        GVSpart.SelectedIndex = -1
    End Sub

    Sub BindDataIn(ByVal iId As Integer)
        'Dim no As Integer = 0
        sSql = "SELECT ROW_NUMBER() OVER(ORDER BY REQDTLOID) Row, REQDTLOID, REQDTLJOB FROM QL_TRNREQUESTDTL WHERE REQMSTOID=" & iId & ""
        Dim xTableItem5 As DataTable = cKoneksi.ambiltabel(sSql, "Cust")
        GVCust.DataSource = xTableItem5
        GVCust.DataBind()
        GVCust.SelectedIndex = -1

        If Session("GVDetKerusakan") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetail()
            Session("GVDetKerusakan") = dtlTable
        End If
    End Sub

    'Sub BindDetSpart(ByVal sID As Integer)
    '    'sSql = "SELECT QLC.MCHECKOID AS IDCheck, QLD.MPARTOID, QLM.GENDESC, QLS.ITSERVDESC, QLP.PARTDESCSHORT, QLD.MPARTQTY FROM QL_TRNMECHPARTDTL QLD INNER JOIN QL_TRNMECHCHECK QLC ON QLC.MCHECKOID=QLD.MCHECKOID INNER JOIN QL_MSTITEMSERV QLS ON QLC.ITSERVOID=QLS.ITSERVOID INNER JOIN QL_MSTGEN QLM ON QLS.ITSERVTYPEOID=QLM.GENOID INNER JOIN QL_MSTSPAREPART QLP ON QLD.MSTPARTOID=QLP.PARTOID WHERE QLC.MCHECKOID=" & sID & ""
    '    sSql = "SELECT QLC.MCHECKOID AS IDCheck, QLD.MPARTOID, QLM.GENDESC, QLS.ITSERVDESC, QLP.PARTDESCSHORT, QLD.MPARTQTY FROM QL_TRNMECHPARTDTL QLD INNER JOIN QL_TRNMECHCHECK QLC ON QLC.MCHECKOID=QLD.MCHECKOID INNER JOIN QL_MSTITEMSERV QLS ON QLC.ITSERVOID=QLS.ITSERVOID INNER JOIN QL_MSTGEN QLM ON QLS.ITSERVTYPEOID=QLM.GENOID INNER JOIN QL_MSTSPAREPART QLP ON QLD.MSTPARTOID=QLP.PARTOID WHERE QLC.MCHECKOID IN (SELECT MCHECKOID FROM QL_TRNMECHPARTDTL WHERE MCHECKOID IN (SELECT MCHECKOID FROM QL_TRNMECHCHECK WHERE MSTREQOID =" & sID & ")) ORDER BY QLC.MCHECKOID"
    '    Dim xTableItem As DataTable = cKoneksi.ambiltabel(sSql, "DetailSparepart")
    '    GVDetSparepart.DataSource = xTableItem
    '    GVDetSparepart.DataBind()
    '    GVDetSparepart.SelectedIndex = -1
    'End Sub

    Sub ClearInfo()
        txtNo.Text = ""
        barcode.Text = ""
        txtTglTerima.Text = ""
        txtWaktuTerima.Text = ""
        txtNamaCust.Text = ""
        txtJenisBrg.Text = ""
        txtMerk.Text = ""
        txtNamaBrg.Text = ""
        txtStatus.Text = ""
        GVCust.Visible = False


        barcode.CssClass = "inpText"
        barcode.Enabled = True
    End Sub

    Sub FillTextInfo(ByVal id As Integer)
        sSql = "SELECT QLR.REQOID, QLR.REQCODE,qlr.barcode, CONVERT(CHAR(20), QLR.CREATETIME, 103) as tanggal, CONVERT(CHAR(20), QLR.CREATETIME, 108) as waktu, QLC.CUSTNAME, QLM.GENDESC as jenis,QLR.REQITEMTYPE, QLM2.GENDESC as merk, QLR.REQITEMNAME, QLR.REQSTATUS,isnull(t.PERSONNAME,'') teknisi,QLR.itemoid FROM QL_TRNREQUEST QLR INNER JOIN QL_mstcust QLC ON QLR.REQCUSTOID=QLC.CUSTOID left join ql_mstperson t on QLR.reqperson = t.personoid INNER JOIN QL_TRNREQUESTDTL QLD ON QLD.REQMSTOID=QLR.REQOID INNER JOIN QL_MSTGEN QLM ON QLR.REQITEMTYPE=QLM.GENOID INNER JOIN QL_MSTGEN QLM2 ON QLR.REQITEMBRAND=QLM2.GENOID WHERE QLR.cmpcode='" & CompnyCode & "' AND QLR.REQOID='" & id & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                txtOid.Text = xReader("REQOID")
                ItemOid.Text = xReader("itemoid")
                txtNo.Text = xReader("REQCODE").ToString.Trim
                barcode.Text = xReader("barcode").ToString.Trim
                txtTglTerima.Text = xReader("tanggal").ToString.Trim
                txtWaktuTerima.Text = xReader("waktu").ToString.Trim
                txtNamaCust.Text = xReader("CUSTNAME").ToString.Trim
                REQITEMTYPE.Text = xReader("REQITEMTYPE").ToString.Trim
                txtJenisBrg.Text = xReader("jenis").ToString.Trim
                txtMerk.Text = xReader("merk").ToString.Trim
                txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                teknisi.Text = xReader("teknisi").ToString.Trim
                txtStatus.Text = "Receive" 'xReader("REQSTATUS").ToString.Trim
                BindDataIn(xReader("REQOID"))
            End While
        End If
    End Sub

    Sub FillTextJob(ByVal iCode As Integer)
        sSql = "SELECT ITSERVOID, ITSERVDESC, ITSERVTARIF FROM QL_MSTITEMSERV WHERE ITSERVOID=" & iCode & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                IDJob.Text = xReader("ITSERVOID").ToString.Trim
                txtJob.Text = xReader("ITSERVDESC").ToString.Trim
                txtAmount.Text = xReader("ITSERVTARIF").ToString.Trim
            End While
        End If
    End Sub

    Sub FillTextSpart(ByVal iID As Integer)
        sSql = "Select DISTINCT crd.branch_code,a.itemoid PARTOID,a.itemcode,a.itemdesc PARTDESCSHORT,a.HPP,isnull((select gendesc from QL_mstgen where genoid = a.itemgroupoid ),'') itemgroupcode,a.merk,a.updtime,Case When b.branch_code ='01' Then a.pricelist else ISNULL(a.pricelist + b.biayaExpedisi,0.00) end MPARTPRICE,crd.saldoAkhir,periodacctg,a.satuan1,a.itemsafetystockunit1 From QL_crdmtr crd Inner Join ql_mstitem a ON a.itemoid=crd.refoid LEFT JOIN QL_mstItem_branch b ON a.itemcode=b.itemcode Where crd.mtrlocoid=" & matLoc.SelectedValue & " AND crd.periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(txtTglTerima.Text))) & "' AND a.itemoid=" & iID & "ORDER BY itemcode ASC"

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                idSparePart.Text = xReader("PARTOID").ToString.Trim
                txtNamaSpart.Text = xReader("PARTDESCSHORT").ToString.Trim
                merkspr.Text = xReader("merk").ToString.Trim
                txtHarga.Text = xReader("MPARTPRICE").ToString.Trim
                txtJumlah.Text = 0
            End While
        End If
    End Sub

    Sub FillTextBox(ByVal idPage As Integer)
        sSql = "SELECT DISTINCT REQ.branch_code,REQ.REQOID, REQ.REQCODE,req.barcode, CONVERT(CHAR(20), REQ.CREATETIME, 103) as tanggal, CONVERT(CHAR(20), REQ.CREATETIME, 108) as waktu, CUST.custname, QLM.GENDESC AS Jenis,req.REQITEMTYPE, QLM2.GENDESC AS Merk, REQ.REQITEMNAME, REQ.REQSTATUS, REQ.UPDUSER, REQ.UPDTIME ,isnull(t.PERSONNAME,'') teknisi,REQ.ITEMOID FROM QL_TRNREQUEST REQ INNER JOIN QL_MSTCUST CUST ON REQ.REQCUSTOID=CUST.custoid INNER JOIN QL_MSTGEN QLM ON REQ.REQITEMTYPE=QLM.GENOID left join ql_mstperson t on req.reqperson = t.personoid INNER JOIN QL_MSTGEN QLM2 ON REQ.REQITEMBRAND=QLM2.GENOID WHERE REQ.CMPCODE='" & CompnyCode & "' AND REQ.REQOID=" & idPage & " ORDER BY REQ.REQOID"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        If xReader.HasRows Then
            While xReader.Read
                DdlCabang.SelectedValue = xReader("branch_code").ToString.Trim
                txtOid.Text = xReader("REQOID")
                txtNo.Text = xReader("REQCODE").ToString.Trim
                barcode.Text = xReader("barcode").ToString.Trim
                txtTglTerima.Text = xReader("tanggal").ToString.Trim
                txtWaktuTerima.Text = xReader("waktu").ToString.Trim
                txtNamaCust.Text = xReader("custname").ToString.Trim
                REQITEMTYPE.Text = xReader("REQITEMTYPE").ToString.Trim
                txtJenisBrg.Text = xReader("Jenis").ToString.Trim
                txtMerk.Text = xReader("Merk").ToString.Trim
                txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                txtStatus.Text = xReader("REQSTATUS").ToString.Trim
                updUser.Text = xReader("UPDUSER").ToString.Trim
                updTime.Text = xReader("UPDTIME").ToString.Trim
                teknisi.Text = xReader("teknisi").ToString.Trim
                ItemOid.Text = xReader("ITEMOID").ToString.Trim
            End While
        End If
        xReader.Close()
        conn.Close()

        sSql = "SELECT QLC.MCHECKOID, QLC.ITSERVOID, QLM.GENOID, QLM.GENDESC, QLS.ITSERVDESC, QLS.ITSERVTARIF, QLC.NOTEDTL FROM QL_TRNMECHCHECK QLC INNER JOIN QL_TRNREQUEST QLR ON QLC.MSTREQOID=QLR.REQOID INNER JOIN QL_MSTITEMSERV QLS ON QLC.ITSERVOID=QLS.ITSERVOID INNER JOIN QL_MSTGEN QLM ON QLS.ITSERVTYPEOID=QLM.GENOID WHERE QLR.REQCODE='" & txtNo.Text & "' ORDER BY QLC.MCHECKOID"
        Dim mysqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mysqlDA.Fill(objDs, "data")
        GVDetKerusakan.DataSource = objDs.Tables("data")
        GVDetKerusakan.DataBind()
        Session("GVDetKerusakan") = objDs.Tables("data")

        sSql = "SELECT QLC.MCHECKOID AS IDCheck, QLD.MPARTOID, QLD.MSTPARTOID, QLM.GENDESC, QLS.ITSERVDESC, QLP.itemdesc PARTDESCSHORT, QLP.merk,QLD.MPARTQTY,qld.mtrloc,qld.MPARTPRICE,Isnull((Select saldoAkhir From QL_crdmtr crd Where crd.mtrlocoid=QLD.mtrloc And crd.branch_code=QLD.branch_code And crd.refoid=QLP.itemoid AND crd.periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'),0.00) saldoakhir FROM QL_TRNMECHPARTDTL QLD INNER JOIN QL_TRNMECHCHECK QLC ON QLC.MCHECKOID=QLD.MRCHECKOID INNER JOIN QL_MSTITEMSERV QLS ON QLC.ITSERVOID=QLS.ITSERVOID INNER JOIN QL_MSTGEN QLM ON QLS.ITSERVTYPEOID=QLM.GENOID INNER JOIN QL_MSTitem QLP ON QLD.MSTPARTOID=QLP.itemoid WHERE (MRCHECKOID IN (SELECT MCHECKOID FROM QL_TRNMECHCHECK WHERE MSTREQOID =" & Convert.ToInt32(txtOid.Text) & ")) ORDER BY QLC.MCHECKOID"
        Dim mysqlA As New SqlDataAdapter(sSql, conn)
        Dim objDsPart As New DataSet
        mysqlA.Fill(objDsPart, "Part")
        GVDetSparepart.DataSource = objDsPart.Tables("Part")
        GVDetSparepart.DataBind()
        Session("GVDetSparepart") = objDsPart.Tables("Part")
    End Sub
#End Region

#Region "Function"
    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("MCHECKOID", Type.GetType("System.Int32"))
        dt.Columns.Add("ITSERVOID", Type.GetType("System.Int32"))
        dt.Columns.Add("GENOID", Type.GetType("System.Int32"))
        dt.Columns.Add("GENDESC", Type.GetType("System.String"))
        dt.Columns.Add("ITSERVDESC", Type.GetType("System.String"))
        dt.Columns.Add("ITSERVTARIF", Type.GetType("System.String"))
        dt.Columns.Add("NOTEDTL", Type.GetType("System.String"))
        Return dt
    End Function

    Private Function setTabelSpart() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("IDCheck", Type.GetType("System.Int32"))
        dt.Columns.Add("MPARTOID", Type.GetType("System.Int32"))
        dt.Columns.Add("MSTPARTOID", Type.GetType("System.Int32"))
        dt.Columns.Add("GENDESC", Type.GetType("System.String"))
        dt.Columns.Add("ITSERVDESC", Type.GetType("System.String"))
        dt.Columns.Add("PARTDESCSHORT", Type.GetType("System.String"))
        dt.Columns.Add("MERK", Type.GetType("System.String"))
        dt.Columns.Add("MPARTQTY", Type.GetType("System.Int32"))
        dt.Columns.Add("MPARTPRICE", Type.GetType("System.Decimal"))
        dt.Columns.Add("mtrloc", Type.GetType("System.Decimal"))
        dt.Columns.Add("saldoakhir", Type.GetType("System.Decimal"))
        Return dt
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            Response.Redirect("~\Transaction\CheckTeknisi.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Check Teknisi"
        Me.btnPosting.Attributes.Add("onclick", "return confirm('Data yang sudah diposting tidak bisa diupdate lagi, \n Anda yakin ingin melanjutkan ?');")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Apakah Anda yakin menghapus data ini ?');")
        Session("idPage") = Request.QueryString("idPage")

        If Not IsPostBack Then
            initTypeJob() : InitLoc()
            txtI_U.Text = "New" : lblStatusPart.Text = "New"
            Dim sWhere = " AND QLR.REQSTATUS NOT IN ('Post','In Process')"
            BindData(sWhere)
            If ddlTypeJob.Items.Count = 0 Then
                showMessage("Silahkan buat Master type service", CompnyName & " - WARNING", 2)
            End If

            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                lblLast.Visible = False
                updTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                updUser.Text = Session("UserID")
                TabContainer1.ActiveTabIndex = 0
                txtPeriod1.Text = Format(GetServerTime(), "dd/MM/yyyy")
                txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            Else
                'txtI_U.Text = "Update"
                txtI_U.Text = "New" : lblCreate.Visible = False
                btnSearch.Enabled = False : btnErase.Enabled = False
                'lblStatusPart.Text = "Update"
                lblStatusPart.Text = "New"
                FillTextBox(Session("idPage"))
                BindDataCust(Session("idPage"))
                TabContainer1.ActiveTabIndex = 1 : btnAddToListSPart.Visible = True
                btnClear.Visible = True : btnBack.Visible = True
                barcode.CssClass = "inpTextDisabled" : barcode.Enabled = False
                If txtStatus.Text = "Receive" Or txtStatus.Text.ToUpper = "IN" Then
                    btnPosting.Visible = True : btnDelete.Visible = True
                Else
                    btnPosting.Visible = False : btnSave.Visible = False
                    btnAddToList.Visible = False : btnClearKerusakan.Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub lbDetKerusakan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDetKerusakan.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub lbDetSparepart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDetSparepart.Click
        MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub lbInformasi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInformasi.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbDetKerusakan2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDetKerusakan2.Click
        MultiView1.ActiveViewIndex = 1
        txtType.Text = ""
        txtJobSpart.Text = ""
        txtNamaSpart.Text = ""
        txtJumlah.Text = ""
        merkspr.Text = ""
    End Sub

    Protected Sub lbDetSparepart2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDetSparepart2.Click
        MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub lbInformasi2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInformasi2.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        If cbPeriod.Checked = True Then
            Dim sMsg As String = ""
            If txtPeriod1.Text = "" Then
                sMsg &= "- Silahkan isi Period 1 !<br>"
            End If
            If txtPeriod2.Text = "" Then
                sMsg &= "- Silahkan isi Period 2 !<br>"
            End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2)
                Exit Sub
            End If
        End If
        BindData(checkFilter())
    End Sub

    Protected Sub btnFindInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindInfo.Click
        PanelInfo.Visible = True
        btnHiddenInfo.Visible = True
        MPEInfo.Show()

        If cbPeriodInfo.Checked = True Then
            Dim sMsg As String = ""
            If txtPeriod1Info.Text = "" Then
                sMsg &= "- Silahkan isi Period 1 !<br>"
            End If
            If txtPeriod2Info.Text = "" Then
                sMsg &= "- Silahkan isi Period 2 !<br>"
            End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If

        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilterInfo.SelectedValue & " LIKE '%" & Tchar(txtFilterInfo.Text) & "%' and REQPERSON = (select personoid from QL_MSTPERSON a inner join QL_MSTPROF b on a.PERSONNAME = b.USERNAME where b.USERID ='" & Session("UserID") & "')"

        If cbPeriodInfo.Checked Then
            Try
                txtPeriod1Info.Enabled = True
                txtPeriod2Info.Enabled = True
                Dim date1Info As Date = CDate(toDate(txtPeriod1Info.Text))
                Dim date2Info As Date = CDate(toDate(txtPeriod2Info.Text))

                If date1Info < CDate("01/01/1900") Then
                    showMessage("Invalid Start Date !", CompnyName & " - Warning", 2) : Exit Sub
                End If
                If date2Info < CDate("01/01/1900") Then
                    showMessage("Invalid End Date !", CompnyName & " - Warning", 2) : Exit Sub
                End If
                If date1Info > date2Info Then
                    showMessage("Period 1 harus lebih kecil dari Period 2 !", CompnyName & " - Warning", 2) : Exit Sub
                    txtPeriod1Info.Text = "" : txtPeriod2Info.Text = ""
                    Exit Sub
                End If
                sWhere &= " AND CONVERT(CHAR(20), QLR.CREATETIME, 101)  BETWEEN '" & CDate(toDate(txtPeriod1Info.Text)) & "' AND '" & CDate(toDate(txtPeriod2Info.Text)) & "'"
            Catch ex As Exception
                showMessage("Format tanggal tidak sesuai", CompnyName & " - Warning", 2) : Exit Sub
            End Try
        End If
        BindDataInfo(sWhere)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        txtFilter.Text = "" : ddlFilter.SelectedIndex = 0
        cbPeriod.Checked = False : txtPeriod1.Text = ""
        txtPeriod2.Text = "" : cbStatus.Checked = False
        ddlStatus.SelectedIndex = 0
        Dim sWhere As String = " AND QLR.REQSTATUS NOT IN ('Post','In Process')"
        BindData(sWhere)
    End Sub

    Protected Sub btnViewAllInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllInfo.Click
        PanelInfo.Visible = True
        btnHiddenInfo.Visible = True
        MPEInfo.Show()
        txtFilterInfo.Text = ""
        ddlFilterInfo.SelectedIndex = 0
        cbPeriodInfo.Checked = False
        txtPeriod1Info.Text = ""
        txtPeriod2Info.Text = ""
        BindDataInfo(" and REQPERSON = (select personoid from QL_MSTPERSON a inner join QL_MSTPROF b on a.PERSONNAME = b.USERID where b.USERID =  '" & Session("UserID") & "') ")
    End Sub

    Protected Sub GVListTeknisi_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVListTeknisi.PageIndexChanging
        GVListTeknisi.PageIndex = e.NewPageIndex
        BindData(checkFilter())
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        PanelInfo.Visible = True
        btnHiddenInfo.Visible = True
        MPEInfo.Show()
        Dim sWhere As String = " AND REQPERSON = (select personoid from QL_MSTPERSON a inner join QL_MSTPROF b on a.PERSONNAME = b.USERNAME where b.USERID ='" & Session("UserID") & "')  "
        BindDataInfo(sWhere)
    End Sub

    Protected Sub GVListInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVListInfo.SelectedIndexChanged
        GVCust.Visible = True : Dim iID As Integer
        btnHiddenInfo.Visible = False : PanelInfo.Visible = False
        MPEInfo.Hide()
        iID = GVListInfo.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextInfo(iID)
        Session("index") = Nothing
        Session("GVDetKerusakan") = Nothing : Session("GVDetSparepart") = Nothing
        GVDetKerusakan.Visible = False : GVDetSparepart.Visible = False
        barcode.CssClass = "inpTextDisabled" : barcode.Enabled = False
    End Sub

    Protected Sub GVJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVJob.SelectedIndexChanged
        'Dim IDGVJob As String = ""
        btnHiddenJob.Visible = False
        PanelJob.Visible = False
        MPEJob.Hide()
        IDGVJob = GVJob.SelectedDataKey(0).ToString.Trim
        'MsgBox(IDGVJob)
        CProc.DisposeGridView(sender)
        FillTextJob(IDGVJob)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        TabContainer1.ActiveTabIndex = 0
        Response.Redirect("~\Transaction\CheckTeknisi.aspx?page=true")
    End Sub

    Protected Sub lbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbClose.Click
        btnHiddenInfo.Visible = False
        PanelInfo.Visible = False
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErase.Click
        ClearInfo()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Dim msg As String = ""
        If GVDetKerusakan.Rows.Count = 0 Then
            txtI_U.Text = "New"
        End If

        If txtNo.Text = "" Then
            MultiView1.ActiveViewIndex = 0
            showMessage("Pilih No. Tanda Terima terlebih dahulu", CompnyName & " - Warning", 2)
            Exit Sub
        End If

        If ddlTypeJob.Items.Count = 0 Then
            showMessage("Silahkan buat Master type service", CompnyName & " - WARNING", 2)
            Exit Sub

        End If

        If IDJob.Text = "" Or txtAmount.Text = "" Then
            msg &= "- Silahkan pilih Job<br>"
        ElseIf txtJob.Text = "" Then
            msg &= "- Job harus diisi<br>"
        End If

        If txtNoteDtl.Text <> "" Then
            If Tchar(txtNoteDtl.Text).Length > txtNoteDtl.MaxLength Then
                msg &= "- Keterangan tidak boleh lebih dari 100 karakter<br>"
            End If
        End If


        If msg <> "" Then
            showMessage(msg, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        If Session("GVDetKerusakan") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetail()
            Session("GVDetKerusakan") = dtlTable
        End If

        Dim objTable As DataTable
        objTable = Session("GVDetKerusakan")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = ""

        '=================================================================================
        If txtI_U.Text = "New" Then
            For iCGVE As Integer = 0 To objTable.Rows.Count - 1
                If Convert.ToInt32(IDJob.Text) = objTable.Rows(iCGVE).Item("ITSERVOID") And ddlTypeJob.SelectedValue = objTable.Rows(iCGVE).Item("GENOID") Then
                    showMessage("Data yang Anda tambahkan sudah ada", CompnyName & " - WARNING", 2)
                    ddlTypeJob.SelectedIndex = 0
                    txtJob.Text = "" : txtAmount.Text = "" : txtNoteDtl.Text = ""
                    Exit Sub
                End If
            Next
        ElseIf txtI_U.Text = "Update" Then
            For iCGVE2 As Integer = 0 To objTable.Rows.Count - 1
                If lbID.Text <> objTable.Rows(iCGVE2).Item("MCHECKOID") Then
                    If Convert.ToInt32(IDJob.Text) = objTable.Rows(iCGVE2).Item("ITSERVOID") And ddlTypeJob.SelectedValue = objTable.Rows(iCGVE2).Item("GENOID") Then
                        showMessage("Data yang Anda tambahkan sudah ada", CompnyName & " - WARNING", 2)
                        ddlTypeJob.SelectedIndex = 0
                        txtJob.Text = ""
                        txtAmount.Text = ""
                        txtNoteDtl.Text = ""
                        Exit Sub
                    End If
                End If
            Next
        End If
        '=================================================================================
        'Insert / Update Data
        Dim objRow As DataRow
        If txtI_U.Text = "New" Then
            objRow = objTable.NewRow()
            If IDRow().ToString = "" Then
                For idDetKerusakan As Integer = 0 To objTable.Rows.Count
                    hitung += 1
                    objRow("MCHECKOID") = hitung
                Next
            End If

            If IDRow().ToString <> "" Then
                For idDetKerusakan1 As Integer = 0 To objTable.Rows.Count
                    hitung += 1
                    Dim idDK As Integer = IDRow()
                    objRow("MCHECKOID") = idDK + hitung
                Next
            End If
            Session("MCHECKOID") += 1
        Else
            objRow = objTable.Rows(Session("index"))
            objRow.BeginEdit()
        End If
        objRow("ITSERVOID") = Convert.ToInt32(IDJob.Text)
        objRow("GENOID") = ddlTypeJob.SelectedValue
        objRow("GENDESC") = ddlTypeJob.SelectedItem.Text
        objRow("ITSERVDESC") = txtJob.Text
        objRow("ITSERVTARIF") = txtAmount.Text
        objRow("NOTEDTL") = txtNoteDtl.Text

        If txtI_U.Text = "New" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If

        ClearDetail()
        Session("GVDetKerusakan") = objTable
        GVDetKerusakan.Visible = True
        GVDetKerusakan.DataSource = Nothing
        GVDetKerusakan.DataSource = objTable
        GVDetKerusakan.DataBind()

        lbID.Text = objTable.Rows.Count + 1
        GVDetKerusakan.SelectedIndex = -1

        ddlTypeJob.SelectedIndex = 0
        txtJob.Text = "" : txtAmount.Text = ""
        txtNoteDtl.Text = "" : txtI_U.Text = "New"

        btnAddToList.Visible = False
        btnClearKerusakan.Visible = False

    End Sub

    Protected Sub btnAddToListSPart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToListSPart.Click
        Dim sMsg As String = ""

        If GVDetSparepart.Rows.Count = 0 Then
            lblStatusPart.Text = "New"
        End If

        If txtType.Text = "" Then
            sMsg &= "- Type Service harus diisi<br>"
        End If

        If txtJobSpart.Text = "" Then
            sMsg &= "- Job Service harus diisi<br>"
        End If

        If txtNamaSpart.Text = "" Then
            sMsg &= "- Nama Sparepart harus diisi<br>"
        End If

        If txtJumlah.Text = "" Then
            sMsg &= "- Jumlah Sparepart harus diisi<br>"
        ElseIf txtJumlah.Text < 1 Then
            sMsg &= "- Jumlah Sparepart minimal 1<br>"
        End If
        If txtJumlah.Text >= saldoakhir.Text Then
            sMsg &= "- Jumlah Sparepart tidak boleh melebihi jumlah stock !<br>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        If Session("GVDetSparepart") Is Nothing Then
            Dim dtlTable As DataTable = setTabelSpart()
            Session("GVDetSparepart") = dtlTable
        End If

        Dim objTable As DataTable
        Dim iCountSpart As Integer = 0
        objTable = Session("GVDetSparepart")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = ""

        '=================================================================================
        If lblStatusPart.Text = "New" Then
            For iCheck As Integer = 0 To objTable.Rows.Count - 1
                If Convert.ToInt32(lbIDPart.Text) = objTable.Rows(iCheck).Item("IDCheck") And Convert.ToInt32(idSparePart.Text) = objTable.Rows(iCheck).Item("MSTPARTOID") Then
                    showMessage("Data yang Anda tambahkan sudah ada", CompnyName & " - WARNING", 2)
                    txtNamaSpart.Text = ""
                    txtJumlah.Text = "" : merkspr.Text = ""
                    Exit Sub
                End If
            Next
        ElseIf lblStatusPart.Text = "Update" Then
            For iCheck2 As Integer = 0 To objTable.Rows.Count - 1
                If lbIDDtl.Text <> objTable.Rows(iCheck2).Item("MPARTOID") Then
                    If Convert.ToInt32(lbIDPart.Text) = objTable.Rows(iCheck2).Item("IDCheck") And Convert.ToInt32(idSparePart.Text) = objTable.Rows(iCheck2).Item("MSTPARTOID") Then
                        showMessage("Data yang Anda tambahkan sudah ada", CompnyName & " - WARNING", 2)
                        txtNamaSpart.Text = ""
                        txtJumlah.Text = "" : merkspr.Text = ""
                        Exit Sub
                    End If
                End If

            Next
        End If
        '=================================================================================
        'Insert / Update Data
        Dim objRow As DataRow
        If lblStatusPart.Text = "New" Then
            objRow = objTable.NewRow()
            For idDetSpart As Integer = 0 To objTable.Rows.Count
                iCountSpart += 1
                Dim idDS As Integer = IDRowSpart()
                objRow("MPARTOID") = idDS + iCountSpart
            Next
            objRow("IDCheck") = Convert.ToInt32(lbIDPart.Text)
        Else
            objRow = objTable.Rows(Session("indexPart"))
            objRow.BeginEdit()
        End If
        Dim Jumlah As Integer = Convert.ToInt32(txtJumlah.Text)
        objRow("MSTPARTOID") = Convert.ToInt32(idSparePart.Text)
        objRow("GENDESC") = txtType.Text
        objRow("ITSERVDESC") = txtJobSpart.Text
        objRow("PARTDESCSHORT") = txtNamaSpart.Text
        objRow("MPARTQTY") = Jumlah
        objRow("MERK") = merkspr.Text
        objRow("MPARTPRICE") = txtHarga.Text
        objRow("mtrloc") = matLoc.SelectedValue
        objRow("saldoakhir") = saldoakhir.Text

        If lblStatusPart.Text = "New" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If
        Session("GVDetSparepart") = objTable
        GVDetSparepart.Visible = True
        GVDetSparepart.DataSource = Nothing
        GVDetSparepart.DataSource = objTable
        GVDetSparepart.DataBind()
        GVDetSparepart.SelectedIndex = -1
        txtNamaSpart.Text = "" : txtJumlah.Text = ""
        merkspr.Text = "" : lblStatusPart.Text = "New"
        Label77.Visible = False : ss.Visible = False : saldoakhir.Text = ""
    End Sub

    Protected Sub GVDetKerusakan_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDetKerusakan.RowDeleting
        Dim iIndex As Int16 = e.RowIndex
        Dim objTable As DataTable = Session("GVDetKerusakan")
        Dim objTableSpart As DataTable = Session("GVDetSparepart")
        Dim iChkId As Integer = objTable.Rows(iIndex).Item("MCHECKOID")
        Dim iCatchRow As Integer = 0
        '=================================================================================================        
        For iChkRows As Integer = 0 To GVDetSparepart.Rows.Count - 1
            If iChkRows > GVDetSparepart.Rows.Count - 1 Then
                If objTableSpart.Rows(0).Item("IDCheck") = iChkId Then
                    objTableSpart.Rows.RemoveAt(0)
                ElseIf objTableSpart.Rows.Count > 1 And objTableSpart.Rows(objTableSpart.Rows.Count - 1).Item("IDCheck") = iChkId Then
                    objTableSpart.Rows.RemoveAt(objTableSpart.Rows.Count - 1)
                End If
                Session("GVDetSparepart") = objTableSpart
                GVDetSparepart.Visible = True
                GVDetSparepart.DataSource = objTableSpart
                GVDetSparepart.DataBind()
                'ClearDetailPart()
                Exit For

            Else
                If objTableSpart.Rows(iChkRows).Item("IDCheck") = iChkId Then
                    objTableSpart.Rows.RemoveAt(iChkRows)

                    Session("GVDetSparepart") = objTableSpart
                    GVDetSparepart.Visible = True
                    GVDetSparepart.DataSource = objTableSpart
                    GVDetSparepart.DataBind()
                    'ClearDetailPart()
                End If

            End If
        Next
        '=================================================================================================        
        objTable.Rows.RemoveAt(iIndex)
        Session("GVDetKerusakan") = objTable
        GVDetKerusakan.Visible = True
        GVDetKerusakan.DataSource = objTable
        GVDetKerusakan.DataBind()
        GVDetKerusakan.SelectedIndex = -1

        '-------------------------------------------------------------------------------
        If lbID.Text <> "" Then
            If lbID.Text = iChkId Then
                ddlTypeJob.SelectedIndex = 0
                txtJob.Text = ""
                txtAmount.Text = ""
                txtNoteDtl.Text = ""
                txtI_U.Text = "New"
                lbID.Text = ""
                IDJob.Text = ""
            End If
        End If
        '-------------------------------------------------------------------------------
        'ClearDetail()

        btnAddToList.Visible = True
        btnClearKerusakan.Visible = True

    End Sub

    Protected Sub GVDetKerusakan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDetKerusakan.SelectedIndexChanged
        Try
            Session("index") = GVDetKerusakan.SelectedIndex
            If Session("GVDetKerusakan") Is Nothing = False Then
                lbID.Text = GVDetKerusakan.SelectedDataKey(0).ToString().Trim
                txtI_U.Text = "Update"
                Dim objTable As DataTable
                objTable = Session("GVDetKerusakan")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "MCHECKOID='" & GVDetKerusakan.SelectedDataKey(0).ToString().Trim & "'"

                'Insert / Update List data
                IDJob.Text = dv.Item(0).Item("ITSERVOID").ToString
                ddlTypeJob.SelectedValue = dv.Item(0).Item("GENOID")
                txtJob.Text = dv.Item(0).Item("ITSERVDESC").ToString
                txtAmount.Text = dv.Item(0).Item("ITSERVTARIF").ToString
                txtNoteDtl.Text = dv.Item(0).Item("NOTEDTL").ToString
                dv.RowFilter = ""
                'GVDetKerusakan.Columns(5).Visible = False


                btnAddToList.Visible = True
                btnClearKerusakan.Visible = True
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Error", 1)
        End Try
    End Sub

    Protected Sub GVDetSparepart_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDetSparepart.RowDeleting
        Dim iIndex2 As Int16 = e.RowIndex
        Dim objTableSpart As DataTable = Session("GVDetSparepart")
        Dim idSpart As Integer = objTableSpart.Rows(iIndex2).Item("MPARTOID")
        objTableSpart.Rows.RemoveAt(iIndex2)

        Session("GVDetSparepart") = objTableSpart
        GVDetSparepart.Visible = True
        GVDetSparepart.DataSource = objTableSpart
        GVDetSparepart.DataBind()
        GVDetSparepart.SelectedIndex = -1

        '--------------------------------------------------------------
        If lbIDDtl.Text <> "" Then
            If lbIDDtl.Text = idSpart Then
                txtType.Text = ""
                txtJobSpart.Text = ""
                txtNamaSpart.Text = ""
                merkspr.Text = ""
                txtJumlah.Text = ""
                lblStatusPart.Text = "New"
                lbIDDtl.Text = ""
                lbIDPart.Text = ""
                idSparePart.Text = ""
                Label77.Visible = False
                ss.Visible = False
                saldoakhir.Text = ""
            End If
        End If
        '--------------------------------------------------------------
        'ClearDetailPart()
    End Sub

    Protected Sub GVDetSparepart_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDetSparepart.SelectedIndexChanged
        Try
            Session("indexPart") = GVDetSparepart.SelectedIndex
            If Session("GVDetSparepart") Is Nothing = False Then
                lbIDPart.Text = GVDetSparepart.SelectedDataKey(1).ToString().Trim
                lblStatusPart.Text = "Update"
                Dim objTable As DataTable
                objTable = Session("GVDetSparepart")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "MPARTOID=" & GVDetSparepart.SelectedDataKey(0).ToString().Trim & ""

                'Insert / Update List data
                Dim Jumlah As Integer = Convert.ToInt32(dv.Item(0).Item("MPARTQTY"))
                lbIDDtl.Text = dv.Item(0).Item("MPARTOID").ToString
                idSparePart.Text = dv.Item(0).Item("MSTPARTOID").ToString
                txtType.Text = dv.Item(0).Item("GENDESC").ToString
                txtJobSpart.Text = dv.Item(0).Item("ITSERVDESC").ToString
                txtNamaSpart.Text = dv.Item(0).Item("PARTDESCSHORT").ToString
                merkspr.Text = dv.Item(0).Item("merk").ToString
                txtJumlah.Text = Jumlah
                matLoc.SelectedValue = dv.Item(0).Item("mtrloc").ToString
                txtHarga.Text = dv.Item(0).Item("MPARTPRICE").ToString
                saldoakhir.Text = dv.Item(0).Item("saldoakhir").ToString
                Label77.Visible = True : ss.Visible = True
                dv.RowFilter = ""                
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Error", 1)
        End Try
    End Sub

    Protected Sub lbAddSparepart_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 2
        btnAddToListSPart.Visible = True
        btnClear.Visible = True : btnBack.Visible = True

        Dim xCom() As String = sender.CommandArgument.ToString.Split("?")
        Dim objTable As DataTable
        objTable = Session("GVDetKerusakan")
        Dim dv As DataView = objTable.DefaultView
        lbIDPart.Text = xCom(0)
        txtType.Text = xCom(1)
        txtJobSpart.Text = xCom(2)
        InitLoc()
        If Not Session("idPage") Is Nothing Or Session("idPage") <> "" Then
            'BindDetSpart(Session("idPage"))
        End If
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        If PanelInfo.Visible = True And btnHiddenInfo.Visible = True Then
            PanelInfo.Visible = True
            btnHiddenInfo.Visible = True
            MPEInfo.Show()
        End If
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub

    Protected Sub lbCloseJob_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseJob.Click
        btnHiddenJob.Visible = False
        PanelJob.Visible = False
    End Sub

    Protected Sub btnSearchJob_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchJob.Click
        If REQITEMTYPE.Text = "" Then
            showMessage("Tolong pilih No. Tanda Terima dahulu !", CompnyName & " - Warning", 2)
            txtJob.Text = ""
            txtAmount.Text = ""
            Exit Sub
        End If

        PanelJob.Visible = True
        btnHiddenJob.Visible = True
        MPEJob.Show()
        Dim sWhere As String = ""
        If ddlTypeJob.Items.Count = 0 Then
            sWhere = " and typejob = " & REQITEMTYPE.Text & " "
        Else
            sWhere = " AND ITSERVTYPEOID=" & ddlTypeJob.SelectedValue & " and typejob = " & REQITEMTYPE.Text & " "
        End If
        BindDataJob(sWhere)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
        MultiView1.ActiveViewIndex = 1
        txtType.Text = ""
        txtJobSpart.Text = ""
    End Sub

    Protected Sub btnSearchSpart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSpart.Click
        PanelSpart.Visible = True
        btnHiddenSpart.Visible = True
        MPESpart.Show()
        Dim sWhere As String = ""
        BindDataSpart()
    End Sub

    Protected Sub GVSpart_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVSpart.SelectedIndexChanged
        Dim sCode As String = ""
        btnHiddenSpart.Visible = False
        PanelSpart.Visible = False
        MPESpart.Hide()
        sCode = GVSpart.SelectedDataKey(0).ToString.Trim
        saldoakhir.Text = GVSpart.SelectedDataKey("saldoakhir").ToString.Trim
        saldoakhir.Visible = True
        ss.Visible = True : Label77.Visible = True
        CProc.DisposeGridView(sender)
        FillTextSpart(sCode)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Dim sMsg As String = ""
        If ddlTypeJob.Items.Count = 0 Then
            sMsg &= "- Silahkan buat Master type service<br>"
        End If

        If txtNo.Text = "" Then
            MultiView1.ActiveViewIndex = 0
            sMsg &= "- No. tanda terima harus diisi<br>"
        End If

        If GVDetKerusakan.Rows.Count = 0 Then
            If txtNo.Text = "" Then
                MultiView1.ActiveViewIndex = 0
                sMsg &= "- Detail Kerusakan harus diisi<br>"
            Else
                MultiView1.ActiveViewIndex = 1
                sMsg &= "- Detail Kerusakan harus diisi<br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " -WARNING", 2) : Exit Sub
        End If

        Try
            'Insert
            If Session("idPage") = Nothing Or Session("idPage") = "" Then
                If txtStatus.Text = "Check" Then
                    sSql = "UPDATE QL_TRNREQUEST SET REQSTATUS='Check' WHERE REQOID=" & Convert.ToInt32(txtOid.Text) & " AND branch_code='" & DdlCabang.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_TRNREQUEST SET REQSTATUS='Receive' WHERE REQOID='" & Convert.ToInt32(txtOid.Text) & "' AND branch_code='" & DdlCabang.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("GVDetKerusakan") Is Nothing Then
                    Dim objTable As DataTable
                    Dim oid As Integer = GenerateID("QL_TRNMECHCHECK", CompnyCode)
                    objTable = Session("GVDetKerusakan")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_TRNMECHCHECK (CMPCODE, MCHECKOID, MSTREQOID, ITSERVOID, CREATEUSER, CREATETIME, NOTEDTL,BRANCH_CODE,itemoid) " & _
                                "VALUES ('" & CompnyCode & "', " & oid & ", '" & Convert.ToInt32(txtOid.Text) & "', " & objTable.Rows(C1).Item("ITSERVOID") & ", '" & Session("UserID") & "', current_timestamp, '" & Tchar(objTable.Rows(C1).Item("NOTEDTL")) & "','" & DdlCabang.SelectedValue & "'," & ItemOid.Text & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        oid += 1
                    Next
                    sSql = "UPDATE QL_MSTOID SET LASTOID=(SELECT MAX(MCHECKOID) FROM QL_TRNMECHCHECK) WHERE TABLENAME='QL_TRNMECHCHECK' AND CMPCODE='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("GVDetSparepart") Is Nothing Then
                    Dim objTableSpart As DataTable
                    Dim oidSpart As Integer = GenerateID("QL_TRNMECHPARTDTL", CompnyCode)
                    objTableSpart = Session("GVDetSparepart")
                    For C1 As Int16 = 0 To objTableSpart.Rows.Count - 1
                        sSql = "INSERT INTO QL_TRNMECHPARTDTL (CMPCODE, MPARTOID, MRCHECKOID, MSTPARTOID, MPARTQTY, CREATEUSER,BRANCH_CODE,MPARTPRICE,mtrloc) " & _
                                "VALUES ('" & CompnyCode & "', " & oidSpart & ", " & objTableSpart.Rows(C1).Item("IDCheck") & ", '" & objTableSpart.Rows(C1).Item("MSTPARTOID") & "', " & objTableSpart.Rows(C1).Item("MPARTQTY") & ", '" & Session("UserID") & "','" & DdlCabang.SelectedValue & "','" & ToDouble(objTableSpart.Rows(C1).Item("MPARTPRICE")) & "','" & objTableSpart.Rows(C1).Item("mtrloc") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        oidSpart += 1
                    Next
                    sSql = "UPDATE QL_MSTOID SET LASTOID=(SELECT MAX(MPARTOID) FROM QL_TRNMECHPARTDTL) WHERE TABLENAME='QL_TRNMECHPARTDTL' AND CMPCODE='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            '--------------------------------------------------------------------------
            'Update
            If Not Session("idPage") Is Nothing Or Session("idPage") <> "" Then
                If txtStatus.Text = "Check" Then
                    sSql = "UPDATE QL_TRNREQUEST SET REQSTATUS='Check' WHERE REQOID=" & Session("idPage") & " AND branch_code='" & DdlCabang.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("GVDetSparepart") Is Nothing Then
                    Dim objTableSpart As DataTable
                    Dim oidSpart As Integer = GenerateID("QL_TRNMECHPARTDTL", CompnyCode)
                    objTableSpart = Session("GVDetSparepart")

                    If objTableSpart.Rows.Count <> 0 Then
                        sSql = "DELETE FROM QL_TRNMECHPARTDTL WHERE MRCHECKOID IN (SELECT MCHECKOID FROM QL_TRNMECHCHECK WHERE MSTREQOID=" & Session("idPage") & ") And BRANCH_CODE='" & DdlCabang.SelectedValue & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                If Not Session("GVDetKerusakan") Is Nothing Then
                    Dim objTableSpart As DataTable
                    Dim oidSpart As Integer = GenerateID("QL_TRNMECHPARTDTL", CompnyCode)
                    objTableSpart = Session("GVDetSparepart")
                    '=====================================================
                    Dim oid As Integer = GenerateID("QL_TRNMECHCHECK", CompnyCode)
                    Dim objTable As DataTable
                    objTable = Session("GVDetKerusakan")
                    If objTable.Rows.Count <> 0 Then
                        sSql = "DELETE FROM QL_TRNMECHCHECK WHERE MSTREQOID=" & Session("idPage") & " AND BRANCH_CODE='" & DdlCabang.SelectedValue & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_TRNMECHCHECK (CMPCODE, MCHECKOID, MSTREQOID, ITSERVOID, CREATEUSER, CREATETIME, NOTEDTL,BRANCH_CODE,itemoid) " & _
                                "VALUES ('" & CompnyCode & "', " & oid & ", '" & Convert.ToInt32(txtOid.Text) & "', " & objTable.Rows(C1).Item("ITSERVOID") & ", '" & Session("UserID") & "', current_timestamp, '" & Tchar(objTable.Rows(C1).Item("NOTEDTL")) & "','" & DdlCabang.SelectedValue & "'," & ItemOid.Text & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        '=============================================================

                        If Not Session("GVDetSparepart") Is Nothing Then
                            For C2 As Int16 = 0 To objTableSpart.Rows.Count - 1
                                If objTable.Rows(C1).Item("MCHECKOID") = objTableSpart.Rows(C2).Item("IDCheck") Then
                                    sSql = "INSERT INTO QL_TRNMECHPARTDTL (CMPCODE, MPARTOID, MRCHECKOID, MSTPARTOID, MPARTQTY, CREATEUSER,BRANCH_CODE,MPARTPRICE,mtrloc) " & _
                                           "VALUES ('" & CompnyCode & "', " & oidSpart & ", " & oid & ", '" & objTableSpart.Rows(C2).Item("MSTPARTOID") & "', " & objTableSpart.Rows(C2).Item("MPARTQTY") & ", '" & Session("UserID") & "','" & DdlCabang.SelectedValue & "','" & ToDouble(objTableSpart.Rows(C1).Item("MPARTPRICE")) & "','" & objTableSpart.Rows(C1).Item("mtrloc") & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    oidSpart += 1
                                End If
                            Next
                        End If
                        '===============================================================
                        oid += 1
                    Next
                End If

                If Not Session("GVDetSparepart") Is Nothing Then
                    'Dim objTableSpart As DataTable
                    'Dim oidSpart As Integer = GenerateID("QL_TRNMECHPARTDTL", CompnyCode)
                    'objTableSpart = Session("GVDetSparepart")

                    'If objTableSpart.Rows.Count <> 0 Then
                    '    sSql = "DELETE FROM QL_TRNMECHPARTDTL WHERE MCHECKOID IN (SELECT MCHECKOID FROM QL_TRNMECHPARTDTL WHERE MCHECKOID IN (SELECT MCHECKOID FROM QL_TRNMECHCHECK WHERE MSTREQOID =" & Session("idPage") & "))"
                    '    xCmd.CommandText = sSql
                    '    xCmd.ExecuteNonQuery()
                    'End If
                    sSql = "UPDATE QL_MSTOID SET LASTOID=(SELECT MAX(MPARTOID) FROM QL_TRNMECHPARTDTL) WHERE TABLENAME='QL_TRNMECHPARTDTL' AND CMPCODE='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                sSql = "UPDATE QL_MSTOID SET LASTOID=(SELECT MAX(MCHECKOID) FROM QL_TRNMECHCHECK) WHERE TABLENAME='QL_TRNMECHCHECK' AND CMPCODE='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - Error", 1) : Exit Sub
        End Try
        Response.Redirect("~\Transaction\CheckTeknisi.aspx?page=true")
    End Sub

    Protected Sub btnEraseJob_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseJob.Click
        txtJob.Text = "" : txtAmount.Text = ""
    End Sub

    Protected Sub btnEraseSpart_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseSpart.Click
        txtNamaSpart.Text = "" : txtHarga.Text = ""
        txtJumlah.Text = "" : merkspr.Text = ""
        Label77.Text = "" : ss.Visible = False : saldoakhir.Text = False
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        txtStatus.Text = "Check"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If Not Session("idPage") Is Nothing Or Session("idPage") <> "" Then
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            If Not Session("GVDetKerusakan") Is Nothing Then
                Dim objTable As DataTable
                objTable = Session("GVDetKerusakan")
                If objTable.Rows.Count <> 0 Then
                    sSql = "DELETE FROM QL_TRNMECHCHECK WHERE MSTREQOID=" & Session("idPage") & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            If Not Session("GVDetSparepart") Is Nothing Then
                Dim objTablePart As DataTable
                objTablePart = Session("GVDetKerusakan")
                If objTablePart.Rows.Count <> 0 Then
                    sSql = "DELETE FROM QL_TRNMECHPARTDTL Where MRCHECKOID IN (Select MCHECKOID FROM QL_TRNMECHCHECK ck Where ck.MSTREQOID=" & Session("idPage") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            sSql = "UPDATE QL_TRNREQUEST SET REQSTATUS='In' WHERE REQOID=" & Session("idPage") & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            showMessage("Data berhasil dihapus", CompnyName & " - Information", 3)
        End If

        Response.Redirect("~\Transaction\CheckTeknisi.aspx")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        GVDetSparepart.SelectedIndex = -1 : txtNamaSpart.Text = ""
        txtJumlah.Text = "" : merkspr.Text = ""
        lblStatusPart.Text = "New"
        Label77.Text = "" : ss.Text = "" : saldoakhir.Text = ""
    End Sub

    Protected Sub btnClearKerusakan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearKerusakan.Click
        GVDetKerusakan.SelectedIndex = -1
        ddlTypeJob.SelectedIndex = 0
        txtJob.Text = ""
        txtAmount.Text = ""
        txtNoteDtl.Text = ""
        If txtI_U.Text <> "New" Then
            btnAddToList.Visible = False
            btnClearKerusakan.Visible = False
        Else
            btnAddToList.Visible = True
            btnClearKerusakan.Visible = True
        End If

        txtI_U.Text = "New"
    End Sub

    Protected Sub lbCloseSpart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseSpart.Click
        btnHiddenSpart.Visible = False
        PanelSpart.Visible = False
    End Sub

    Protected Sub ddlTypeJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTypeJob.SelectedIndexChanged
        txtJob.Text = ""
        txtAmount.Text = ""
    End Sub

    Protected Sub barcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles barcode.TextChanged
        If Not barcode.Text = "" Then
            sSql = "SELECT QLR.REQOID, QLR.REQCODE,qlr.barcode, CONVERT(CHAR(20), QLR.CREATETIME, 103) as tanggal, CONVERT(CHAR(20), QLR.CREATETIME, 108) as waktu, QLC.CUSTNAME,QLR.REQITEMTYPE, QLM.GENDESC as jenis, QLM2.GENDESC as merk, QLR.REQITEMNAME, QLR.REQSTATUS ,isnull(t.PERSONNAME,'') teknisi FROM QL_TRNREQUEST QLR INNER JOIN QL_mstcust QLC ON QLR.REQCUSTOID=QLC.CUSTOID INNER JOIN QL_TRNREQUESTDTL QLD ON QLD.REQMSTOID=QLR.REQOID INNER JOIN QL_MSTGEN QLM ON QLR.REQITEMTYPE=QLM.GENOID left join ql_mstperson t on QLR.reqperson = t.personoid INNER JOIN QL_MSTGEN QLM2 ON QLR.REQITEMBRAND=QLM2.GENOID WHERE QLR.cmpcode='" & CompnyCode & "' AND QLR.barcode='" & Tchar(barcode.Text) & "' AND QLR.REQSTATUS='In' and REQPERSON = (select personoid from QL_MSTPERSON a inner join QL_MSTPROF b on a.PERSONNIP = b.USERID where b.USERID =  '" & Session("UserID") & "') "
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xReader = xCmd.ExecuteReader
            If xReader.HasRows Then
                While xReader.Read
                    txtOid.Text = xReader("REQOID")
                    txtNo.Text = xReader("REQCODE").ToString.Trim
                    barcode.Text = xReader("barcode").ToString.Trim
                    txtTglTerima.Text = xReader("tanggal").ToString.Trim
                    txtWaktuTerima.Text = xReader("waktu").ToString.Trim
                    txtNamaCust.Text = xReader("CUSTNAME").ToString.Trim
                    txtJenisBrg.Text = xReader("jenis").ToString.Trim
                    REQITEMTYPE.Text = xReader("REQITEMTYPE").ToString.Trim
                    txtMerk.Text = xReader("merk").ToString.Trim
                    txtNamaBrg.Text = xReader("REQITEMNAME").ToString.Trim
                    teknisi.Text = xReader("teknisi").ToString.Trim
                    txtStatus.Text = "Receive" 'xReader("REQSTATUS").ToString.Trim
                    BindDataIn(xReader("REQOID"))
                End While
            Else
                showMessage("Tidak ada No. Barcode yang ditemukan", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        Session("index") = Nothing
        Session("GVDetKerusakan") = Nothing
        Session("GVDetSparepart") = Nothing
        GVDetKerusakan.Visible = False
        GVDetSparepart.Visible = False
        conn.Close()

        barcode.CssClass = "inpTextDisabled"
        barcode.Enabled = False
    End Sub

    Protected Sub txtJob_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJob.TextChanged        
    End Sub

    Protected Sub txtNamaSpart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNamaSpart.TextChanged
    End Sub

    Protected Sub GVDetKerusakan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble((e.Row.Cells(6).Text)), 3)
        End If
    End Sub
#End Region
End Class
