<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trndpar_payother.aspx.vb" Inherits="trndpar_payother" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: DPAR - Return"></asp:Label>
            </th>
        </tr>
        <tr>
            <th align="left" style="background-color: transparent" valign="center">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            .<span style="font-size: 9pt"><strong>: List Down Payment AR - Pay Other</strong></span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch" __designer:wfdid="w6"><TABLE width=750><TBODY><TR><TD style="FONT-SIZE: small; WIDTH: 64px" class="Label" align=left>Cabang</TD><TD align=left colSpan=4><asp:DropDownList id="fddlCabang" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w30"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small; WIDTH: 64px" class="Label" align=left>Filter&nbsp; </TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" Width="111px" CssClass="inpText" __designer:wfdid="w31"><asp:ListItem Value="trndparno">No. DP</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterTextsupp" runat="server" CssClass="inpText" __designer:wfdid="w32"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: small; WIDTH: 64px" class="Label" align=left>Periode </TD><TD align=left colSpan=4><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w34"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton>&nbsp;-&nbsp;<asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w36"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton> <asp:ImageButton id="btnAll" onclick="btnAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton></TD></TR><TR><TD style="FONT-SIZE: small; HEIGHT: 25px" align=left colSpan=5><asp:GridView id="gvMst" runat="server" Width="950px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w24" OnPageIndexChanging="gvMst_PageIndexChanging" AllowPaging="True" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" DataKeyNames="cashbankoid" OnSelectedIndexChanged="gvMst_SelectedIndexChanged" PageSize="8">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trndparoid" DataNavigateUrlFormatString="trndpar_payother.aspx?oid={0}" DataTextField="trndparno" HeaderText="No. DP">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trndpardate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trndparamt" HeaderText="Pay Amt/Return">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndparstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpno" HeaderText="DP Ref.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="Branch">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trndparnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" Text="No data found !!" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:SqlDataSource id="SDSList" runat="server" __designer:dtid="281474976710661" ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" __designer:wfdid="w44">
                    </asp:SqlDataSource><ajaxToolkit:CalendarExtender id="CEdateAwal" runat="server" __designer:wfdid="w45" TargetControlID="dateAwal" PopupButtonID="imageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CEdateAkhir" runat="server" __designer:wfdid="w46" TargetControlID="dateAkhir" PopupButtonID="imageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w47" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w48" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMst"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="HEIGHT: 10px" class="Label" align=left>Cabang</TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" __designer:wfdid="w64" OnSelectedIndexChanged="CabangNya_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList> <asp:Label id="trndparoid" runat="server" __designer:wfdid="w63" Visible="False"></asp:Label></TD><TD style="WIDTH: 156px; HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="currencyoid" runat="server" Width="64px" CssClass="inpText" __designer:wfdid="w93" OnSelectedIndexChanged="currencyoid_SelectedIndexChanged" AutoPostBack="True" Visible="False"></asp:DropDownList></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:Label id="cashbankoid" runat="server" __designer:wfdid="w62" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Width="89px" Text="No. DP Retur" __designer:wfdid="w124"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="trndparno" runat="server" Width="126px" CssClass="inpTextDisabled" __designer:wfdid="w66" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 156px" class="Label" align=left>Tanggal</TD><TD class="Label" align=left><asp:TextBox id="trndpardate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w67" AutoPostBack="True" Enabled="False"></asp:TextBox> <asp:ImageButton id="imbDPARDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w68" Visible="False"></asp:ImageButton><asp:Label id="Label2" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w69"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Customer" runat="server" Text="Customer" __designer:wfdid="w131"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="custname" runat="server" Width="188px" CssClass="inpTextDisabled" __designer:wfdid="w70" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbSearchCust" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w128"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w1"></asp:ImageButton><asp:Label id="custoid" runat="server" __designer:wfdid="w2" Visible="False"></asp:Label></TD><TD style="WIDTH: 156px" class="Label" align=left>N0. DP</TD><TD class="Label" align=left><asp:DropDownList id="dpno" runat="server" Width="219px" CssClass="inpText" __designer:wfdid="w73" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Width="95px" Text="Payment Type" __designer:wfdid="w116"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="payreftype" runat="server" Width="102px" CssClass="inpText" __designer:wfdid="w77" OnSelectedIndexChanged="payreftype_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem>NON CASH</asp:ListItem>
<asp:ListItem Selected="True">CASH</asp:ListItem>
<asp:ListItem>GIRO</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 156px" class="Label" align=left>COA DP</TD><TD class="Label" align=left><asp:DropDownList id="trndparacctgoid" runat="server" Width="247px" CssClass="inpText" __designer:wfdid="w76"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblDueDate" runat="server" Font-Size="X-Small" Text="Due Date" __designer:wfdid="w81" Visible="False"></asp:Label> <asp:Label id="lblNeedDue" runat="server" CssClass="Important" Text="*" __designer:wfdid="w82" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w132" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w133" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="lblDate" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w134" Visible="False"></asp:Label></TD><TD style="WIDTH: 156px" class="Label" align=left><asp:Label id="Label6" runat="server" Width="88px" Font-Size="X-Small" Text="COA Payment" __designer:wfdid="w86"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="cashbankacctgoid" runat="server" Width="286px" CssClass="inpText" __designer:wfdid="w80"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Width="63px" Text="DP Amt" __designer:wfdid="w122"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="trndparamt" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w75" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 156px" class="Label" align=left><asp:Label id="lblRefNo" runat="server" Font-Size="X-Small" Text="Ref. No" __designer:wfdid="w86"></asp:Label></TD><TD class="Label" align=left colSpan=1><asp:TextBox id="payrefno" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w87" MaxLength="20"></asp:TextBox>&nbsp;<asp:TextBox id="code" runat="server" Width="99px" CssClass="inpTextDisabled" __designer:wfdid="w88" Visible="False" Enabled="False" MaxLength="30"></asp:TextBox>&nbsp;<asp:ImageButton id="creditsearch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w89" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="CREDITCLEAR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w90" Visible="False"></asp:ImageButton>&nbsp;</TD></TR><TR><TD class="Label" align=left>Pay Amt</TD><TD class="Label" align=left><asp:TextBox id="PayAmt" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w78" AutoPostBack="True" Enabled="False" MaxLength="14"></asp:TextBox></TD><TD style="WIDTH: 156px" class="Label" align=left>Status</TD><TD class="Label" align=left colSpan=1><asp:TextBox id="trndparstatus" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w96" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Note <asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w94" Visible="False"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="trndparnote" runat="server" Width="307px" Height="33px" CssClass="inpText" Font-Size="Medium" __designer:wfdid="w95" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=4>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w118"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w119"></asp:Label> <asp:Label id="currentpayreftype" runat="server" __designer:wfdid="w120" Visible="False"></asp:Label> <asp:Label id="currentcbacctgoid" runat="server" __designer:wfdid="w121" Visible="False"></asp:Label> <asp:Label id="currencyrate" runat="server" __designer:wfdid="w127" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=4><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w108" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w109" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w110" AlternateText="Posting"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w111" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w112" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=center colSpan=4><ajaxToolkit:FilteredTextBoxExtender id="FTEPAYAMT" runat="server" __designer:wfdid="w117" TargetControlID="PAYAMT" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w125" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div5" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w126"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            .<span style="font-size: 9pt"><strong>: Form Down Payment AR - Pay Other</strong></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left">
                </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKValidasi_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upCust" runat="server">
        <contenttemplate>
<asp:Panel id="pnlCust" runat="server" Width="500px" CssClass="modalBox" Visible="False" BorderStyle="Solid"><TABLE id="Table2" onclick="return TABLE1_onclick()" width="100%"><TR><TD style="TEXT-ALIGN: center" vAlign=middle colSpan=5 rowSpan=1><asp:Label id="lblCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Customer"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=middle colSpan=5 rowSpan=2>Filter&nbsp; : <asp:DropDownList id="suppFilter1" runat="server" Width="67px" CssClass="inpText"><asp:ListItem Value="custname">Nama</asp:ListItem>
<asp:ListItem Value="custcode">Kode</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="filtertext" runat="server" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbsearchFilter" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD></TR><TR></TR><TR><TD colSpan=5><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: beige"><asp:GridView id="gvCust" runat="server" Width="98%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None" DataKeyNames="custoid,custname" OnSelectedIndexChanged="gvCust_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="350px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=5>&nbsp; <asp:LinkButton id="lkbCloseCust" onclick="lkbCloseCust_Click" runat="server">[ Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <asp:Button id="btnHideCust" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpeCust" runat="server" TargetControlID="btnHideCust" Drag="True" PopupDragHandleControlID="lblCust" BackgroundCssClass="modalBackground" PopupControlID="pnlCust"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel6" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Perkiraan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

