Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptNotaHR
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Public Sub initddl()

        sSql = "select distinct periodacctg,periodacctg from ql_pomst p order by p.periodacctg desc"
        FillDDL(period, sSql)

        sSql = "select A.genoid, G.GENDESC  + ' - ' + A.gendesc, A.genother1  from QL_mstgen A INNER JOIN QL_MSTGEN G ON G.GENOID = A.genother1  AND G.gengroup = 'WAREHOUSE' and G.genother1 IN ('GROSIR','SUPPLIER') where A.cmpcode='" & CompnyCode & "'  and A.gengroup='LOCATION'  ORDER BY A.GENDESC DESC"
        FillDDL(ddlLocation, sSql)

        ddlLocation.Items.Add(New ListItem("ALL LOCATION", "ALL LOCATION"))
        ddlLocation.SelectedValue = "ALL LOCATION"
        '---------------------------------------------------------------------------------------
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("ALL GRUP", "ALL GRUP"))
        FilterDDLGrup.SelectedValue = "ALL GRUP"
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GRUP", "ALL SUB GRUP"))
        FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP"

    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        lblkonfirmasi.Text = "" 'Session("diprint") = "False"
        Dim personoid As Integer = GetStrData("Select pr.PERSONOID from QL_MSTPERSON pr INNER JOIN QL_MSTPROF pf ON pf.USERNAME=pr.PERSONNAME Where pf.USERID='" & Session("UserID") & "'")

        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Try
            Dim sWhere As String = "" : Dim namaPDF As String = ""

            'If Session("UserID").ToString.ToUpper <> "ADMIN" Then
            '    sWhere &= " AND mst.trnbelimstoid IN (Select trnbelimstoid FROM QL_trnbelidtl dt INNER JOIN ql_mstitem i ON i.itemoid=dt.itemoid AND i.personoid =" & personoid & ")"
            'End If

            '            bm.trnbelimstoid NOT IN (select trnbelimstoid from QL_trnnotahrdtl nd INNER JOIN QL_trnnotahrmst nm ON nm.trnnotahroid = nd.trnnotahroid) and bdx.trnbelidtldisctype2 = 'VCR' and bdx.amtdisc2 > 0
            'order by s.suppname, bm.trnbelidate

            vReport = New ReportDocument
            If Type.SelectedValue = "Summary" Then
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptPurchSumExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptPurchSum.rpt"))
                End If

                sWhere &= " Where mst.trnbelidate between '" & toDate(dateAwal.Text) & "' And '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and mst.trnbelimstoid = '" & oid.Text & "' ") & IIf(ToDouble(suppoid.Text) > 0, " And mst.trnsuppoid =" & suppoid.Text, " ")
                namaPDF = "Nota_Beli_(Sum)_"
            ElseIf type.SelectedValue = "Detail HR" Then

                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptNotaHRDetailXls.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptNotaHRDetail.rpt"))
                End If

                sWhere &= " Where hrm.trnnotahrstatus = 'Post' AND hrm.trnnotahrdate between '" & toDate(dateAwal.Text) & "' And '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and hrm.trnnotahroid = " & oid.Text & "") & IIf(ToDouble(suppoid.Text) > 0, " And hrm.suppoid=" & suppoid.Text, " ") & IIf(txtSjNo.Text <> "", " AND hrd.trnbelimstoid = " & trnbelimstoid.Text & "", "")
                namaPDF = "Nota_Piutang_Gantung_(Dtl)_"
            ElseIf type.SelectedValue = "Detail PI" Then
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptNotaHRPIDetailXls.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptNotaHRPIDetail.rpt"))
                End If

                sWhere &= " Where hrm.trnnotahrstatus = 'Post' AND hrm.trnnotahrdate between '" & toDate(dateAwal.Text) & "' And '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and hrm.trnnotahroid = " & oid.Text & "") & IIf(ToDouble(suppoid.Text) > 0, " And hrm.suppoid=" & suppoid.Text, " ") & IIf(txtSjNo.Text <> "", " AND hrd.trnbelimstoid = " & trnbelimstoid.Text & "", "")
                namaPDF = "Nota_Piutang_Gantung_(Dtl)_"
            Else
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptNotaHROSXls.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptNotaHROS.rpt"))
                End If

                sWhere &= " Where bm.trnbelidate between '" & toDate(dateAwal.Text) & "' And '" & toDate(dateAkhir.Text) & "' And bm.trnbelimstoid NOT IN (select trnbelimstoid from QL_trnnotahrdtl nd INNER JOIN QL_trnnotahrmst nm ON nm.trnnotahroid = nd.trnnotahroid) and bdx.trnbelidtldisctype2 = 'VCR' and bdx.amtdisc2 > 0 " & IIf(ToDouble(suppoid.Text) > 0, " And bm.trnsuppoid=" & suppoid.Text, " ") & IIf(txtSjNo.Text <> "", " AND bm.trnbelimstoid = " & trnbelimstoid.Text & "", "") & ""
                'order by s.suppname, bm.trnbelidate", "")
                namaPDF = "Nota_Piutang_Gantung_(OS)_"
            End If
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            If tipe = "" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If

            'Response.Redirect("~\ReportForm\rptnotahr.aspx?awal=true")
        Catch ex As Exception

            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Public Sub showPrintExcel(ByVal name As String)
        'Dim sWhere As String = ""
        'lblkonfirmasi.Text = ""
        ''Session("diprint") = "False"

        'If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
        '    lblkonfirmasi.Text = "Tanggal Terakhir tidak boleh lebih kecil dari Tanggal Awal!!"
        '    Exit Sub
        'End If
        'Response.Clear()

        'Dim sSql As String = ""

        'If type.SelectedValue = "Summary" Then
        '    sWhere = " where mst.trnbelidate between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and mst.trnbelimstoid = '" & oid.Text & "' ") & IIf(ToDouble(suppoid.Text) > 0, "  and  mst.trnsuppoid =" & suppoid.Text, "  ")

        '    sSql = "SELECT mst.periodacctg as [Periode], mst.trnbelidate as Tanggal, " & _
        '    "mst.trnbelino as [Nomor Pembelian], mst.trnbelipono as [No PO], gen4.gendesc as [Tipe Bayar], mst.trnbelitype as [Tipe Beli], " & _
        '    "mst.amtbeli as [Brutto],  (mst.amtdischdr + mst.amtdiscdtl) as Diskon, mst.amtbelinetto as [Netto]," & _
        '    "supp.suppname as [Nama Supplier]" & _
        '    "FROM  QL_trnbelimst mst " & _
        '    "INNER JOIN QL_mstsupp supp ON supp.suppoid = mst.trnsuppoid " & _
        '    " INNER JOIN QL_mstgen gen4 ON gen4.genoid = mst.trnpaytype " & _
        '    "" & sWhere & "order by suppname, trnbelidate"
        'Else
        '    sWhere = " where mst.trnbelidate between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and mst.trnbelimstoid = '" & oid.Text & "' ") & IIf(ToDouble(suppoid.Text) > 0, "  and  mst.trnsuppoid=" & suppoid.Text, "  ") & IIf(ToDouble(itemoid.Text) > 0, "  and  i.itemoid=" & itemoid.Text, "  ") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and beli.itemloc=" & ddlLocation.SelectedValue & " ")
        'End If

        'sSql = "select supp.suppname as [Nama Supplier],  mst1.trnbelino, beli.trnbelidtlseq as [No Urut], g1.gendesc as gudang , " & _
        '"i.itemcode as [Kode Barang],i.itemdesc as [nama Barang], g2.gendesc as satuan, beli.trnbelidtlprice as Harga, " & _
        '"beli.trnbelidtlqty as Qty, beli.trnbelidtldisctype as [Tipe Diskon], beli.trnbelidtlqtydisc as [Diskon], " & _
        '"beli.amtbelinetto as [Jumlah beli(net)], beli.trnbelidtlnote as [Catatan]" & _
        '"from ql_trnbelidtl beli " & _
        '"inner join ql_trnbelimst mst1 on mst1.trnbelimstoid = beli.trnbelimstoid " & _
        '"inner join ql_mstsupp supp on mst1.trnsuppoid = supp.suppoid " & _
        '"inner join ql_mstgen g1 on beli.itemloc = g1.genoid and beli.cmpcode = g1.cmpcode " & _
        '"inner join ql_mstitem i on beli.itemoid = i.itemoid " & _
        '"inner join ql_mstgen g2 on beli.trnbelidtlunitoid = g2.genoid " & _
        '"" & sWhere & "order by beli.trnbelidtlseq"

        'Response.AddHeader("content-disposition", "inline;filename=stock.xls")
        'Response.Charset = ""
        ''set the response mime type for excel
        'Response.ContentType = "application/vnd.ms-excel"

        'If conn.State = ConnectionState.Open Then
        '    conn.Close()
        'End If
        'Dim ds As New DataSet
        'Dim da As New SqlDataAdapter
        'Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        'da.SelectCommand = xcmd
        'da.Fill(ds)
        'Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        'dt = ds.Tables(0)
        'Response.Write(ConvertDtToTDF(dt))
        'conn.Close()

        'Response.End()

        lblkonfirmasi.Text = ""
        Session("diprint") = "False"

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Try
            Dim swhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and mtrlocoid=" & ddlLocation.SelectedValue & " ")
            Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, "  and  i.itemoid=" & itemoid.Text, "  ")
            Dim swheregrup As String = IIf(FilterDDLGrup.SelectedValue = "ALL GRUP", "  ", "  and  i.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
            Dim swheresubgrup As String = IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP", "  ", "  and  i.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")
            Dim swhere As String = ""
            Dim namaPDF As String = ""
            vReport = New ReportDocument
            If Type.SelectedValue = "Summary" Then
                vReport.Load(Server.MapPath("~\Report\rptPurchSum.rpt"))
                swhere = " where mst.trnbelidate between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and mst.trnbelimst1oid = '" & oid.Text & "' ") & IIf(ToDouble(suppoid.Text) > 0, "  and  mst.trnsuppoid =" & suppoid.Text, "  ")
                namaPDF = "Nota_Beli_(Sum)_"
            Else
                vReport.Load(Server.MapPath("~\Report\rptPurchDetail.rpt"))
                swhere = " where mst.trnbelidate between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and mst.trnbelimst1oid = '" & oid.Text & "' ") & IIf(ToDouble(suppoid.Text) > 0, "  and  mst.trnsuppoid=" & suppoid.Text, "  ") & IIf(ToDouble(itemoid.Text) > 0, "  and  i.itemoid=" & itemoid.Text, "  ") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and beli.itemloc=" & ddlLocation.SelectedValue & " ")
                namaPDF = "Nota_Beli_(Dtl)_"
            End If

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'crvMutasiStock.DisplayGroupTree = False
            'crvMutasiStock.ReportSource = vReport
            vReport.SetParameterValue("sWhere", swhere)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)

            Session("diprint") = "True"

            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
            vReport.Close() : vReport.Dispose()
            Response.Redirect("~\ReportForm\rptnotahr.aspx?awal=true")
        Catch ex As Exception

            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Public Sub BindDataListItem()
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        sSql = "SELECT i.itemcode, i.itemdesc, i.itempriceunit1,i.itempriceunit2,i.itempriceunit3,i.itemoid, g.gendesc satuan1,g2.gendesc satuan2, g3.gendesc satuan3,kONversi1_2, kONversi2_3,i.merk FROM ql_mstitem i INNER JOIN ql_mstgen g ON g.genoid=satuan1 and itemflag='AKTIF' INNER JOIN ql_mstgen g2 ON g2.genoid=satuan2 INNER JOIN ql_mstgen g3 ON g3.genoid=satuan3 WHERE (itemdesc like '%" & Tchar(itemname.Text.Trim) & "%' OR i.itemcode like '%" & Tchar(itemname.Text.Trim) & "%' OR i.merk like '%" & Tchar(itemname.Text.Trim) & "%') AND i.itemoid IN (SELECT d.itemoid FROM QL_trnbelidtl d INNER JOIN QL_trnbelimst m ON m.trnbelimstoid=d.trnbelimstoid AND d.cmpcode=m.cmpcode AND d.itemoid=i.itemoid WHERE m.trnbelino LIKE '%" & Tchar(nota.Text.Trim) & "%' AND m.trnbelidate BETWEEN '" & CDate(toDate(dateAwal.Text.Trim)) & "' AND '" & CDate(toDate(dateAkhir.Text.Trim)) & "')"
        FillGV(gvItem, sSql, "ql_mstitem")
        gvItem.Visible = True
    End Sub

    Private Sub BindSj()
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        Dim sWhere As String = ""
        If nota.Text <> "" Then
            sWhere &= "AND hrd.trnnotahroid = " & oid.Text & ""
        End If
        sSql = " Select DISTINCT bm.trnbelimstoid,bm.trnbelino,CONVERT(varchar(20),bm.trnbelidate,103) trnbelidate, s.suppname from QL_trnbelidtl bd Inner Join QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid INNER JOIN QL_trnnotahrdtl hrd ON hrd.trnbelimstoid = bm.trnbelimstoid INNER JOIN QL_mstsupp s ON s.suppoid = bm.trnsuppoid Where bm.trnbelino LIKE '%" & Tchar(txtSjNo.Text.Trim) & "%' " & sWhere & " Order By bm.trnbelino"
        FillGV(GvSj, sSql, "ql_mstitem")
        GvSj.Visible = True
    End Sub

    Public Sub bindDataListSupplier()
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        If type.SelectedValue = "Voucher OS" Then
            sSql = "SELECT DISTINCT suppoid, suppcode, suppname, supptype FROM ql_mstsupp s INNER JOIN QL_trnbelimst bm ON bm.trnsuppoid = s.suppoid INNER JOIN QL_trnbelidtl bd ON bd.trnbelimstoid = bm.trnbelimstoid and bd.branch_code = bm.branch_code where bd.amtdisc2 > 0 and bd.trnbelidtldisctype2 = 'VCR' GROUP BY bm.trnbelimstoid, s.suppoid, s.suppname, s.suppcode, s.supptype HAVING SUM(amtdisc2) - (SELECT SUM(hrd.trnnotahrdtlamt) FROM QL_trnnotahrdtl hrd INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = hrd.trnnotahroid where hrd.trnbelimstoid = bm.trnbelimstoid) > 0 and suppname like '%" & Tchar(suppname.Text.Trim) & "%' or suppcode like '%" & Tchar(suppname.Text.Trim) & "%'  ORDER BY suppname"
        Else
            sSql = "SELECT DISTINCT suppoid, suppcode, suppname, supptype FROM ql_mstsupp s WHERE (suppname like '%" & Tchar(suppname.Text.Trim) & "%' or suppcode like '%" & Tchar(suppname.Text.Trim) & "%') AND s.suppoid IN (SELECT b.suppoid FROM QL_trnnotahrmst b WHERE b.trnnotahroid > 0 and trnnotahrdate between '" & CDate(toDate(dateAwal.Text)) & "' and '" & CDate(toDate(dateAkhir.Text)) & "') ORDER BY suppname"
        End If
        FillGV(gvSupp, sSql, "ql_mstsupp")
        gvSupp.Visible = True
    End Sub

    Public Sub bindDataListPO()
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Dim sWhere As String = ""
        If suppname.Text <> "" Then
            sWhere &= " AND suppoid = " & suppoid.Text & ""
        End If

        sSql = "select trnnotahroid, trnnotahrno, trnnotahrdate from ql_trnnotahrmst where trnnotahrdate between '" & CDate(toDate(dateAwal.Text)) & "' and '" & CDate(toDate(dateAkhir.Text)) & "' and trnnotahrno like '%" & Tchar(nota.Text) & "%' " & sWhere & ""
        FillGV(GVNota, sSql, "ql_trnnotahrmst")
        GVNota.Visible = True
    End Sub
#End Region

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        merk.Text = gvItem.SelectedDataKey.Item("merk")
        gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        merk.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblkonfirmasi.Text = ""
        BindDataListItem()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            If Session("branch_id") = "" Then
                Response.Redirect("~\Other\login.aspx")
            End If
            Response.Redirect("~\ReportForm\rptnotahr.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Nota Pembelian "
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not IsPostBack Then
            fDDLBranch() : initddl()
            dateAwal.Text = Format(New Date(GetServerTime().Year, GetServerTime().Month, 1), "dd/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        'Session("diprint") = "True"
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptnotahr.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Session("diprint") = "False"
        showPrint("excel")
        'showPrintExcel("ListofStock")
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListSupplier()
    End Sub

    Protected Sub btnEraseSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppname.Text = ""
        suppoid.Text = ""
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
        suppname.Text = gvSupp.SelectedDataKey.Item("suppname")
        suppoid.Text = gvSupp.SelectedDataKey.Item("suppoid")
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupp.PageIndexChanging
        gvSupp.PageIndex = e.NewPageIndex
        bindDataListSupplier()
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        GVNota.PageIndex = e.NewPageIndex
        bindDataListPO()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nota.Text = ""
        oid.Text = ""
        GVNota.Visible = False
    End Sub

    Protected Sub btntopdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntopdf.Click
        'Session("diprint") = "False"
        showPrint("pdf")
    End Sub

    Protected Sub type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles type.SelectedIndexChanged
        If type.SelectedValue = "Detail" Then
            Td1.Visible = True : Td2.Visible = True
        Else
            Td1.Visible = False : Td2.Visible = False
        End If
    End Sub

    Protected Sub crISjno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles crISjno.Click
        BindSj()
    End Sub

    Protected Sub GvSj_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvSj.SelectedIndexChanged
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
        txtSjNo.Text = GvSj.SelectedDataKey.Item("trnbelino")
        trnbelimstoid.Text = GvSj.SelectedDataKey.Item("trnbelimstoid")
        GvSj.Visible = False
    End Sub

    Protected Sub GvSj_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvSj.PageIndex = e.NewPageIndex
        BindSj()
    End Sub

    Protected Sub DelSjNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtSjNo.Text = "" : GvSj.Visible = False
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("")
    End Sub

    Protected Sub btnSearchNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchNota.Click
        bindDataListPO()
    End Sub

    Protected Sub GVNota_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVNota.SelectedIndexChanged
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
        nota.Text = GVNota.SelectedDataKey.Item("trnnotahrno")
        oid.Text = GVNota.SelectedDataKey.Item("trnnotahroid")
        GVNota.Visible = False
    End Sub

End Class
