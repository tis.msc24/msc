<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnInvoice.aspx.vb" Inherits="Transaction_Invoice" title="MSC - Invoice Service" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="100%">
        <tr>
            <td align="left" colspan="3">
    <table id="Table3" align="center" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th class="header" colspan="5" valign="top">
                <asp:Label ID="Label81" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Invoice Servis"></asp:Label></th>
        </tr>
    </table>
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <strong style="font-weight: bold; font-size: 12px">
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                List Invoice Servis</strong> <strong>:.</strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD>Cabang</TD><TD style="WIDTH: 6px">:</TD><TD colSpan=3><asp:DropDownList id="fCabang" runat="server" Width="130px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD><asp:Label id="Label1" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 6px"><asp:Label id="Label2" runat="server" Text=":"></asp:Label></TD><TD colSpan=3><asp:DropDownList id="ddlFilter" runat="server" Width="130px" CssClass="inpText"><asp:ListItem Value="si.invoiceno">No Invoice</asp:ListItem>
<asp:ListItem Value="co.custname">Customer</asp:ListItem>
<asp:ListItem Value="rqm.reqcode">No. TTS</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="155px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px"><asp:CheckBox id="cbPeriod" runat="server" Text="Periode" Checked="True"></asp:CheckBox></TD><TD style="WIDTH: 6px"><asp:Label id="Label3" runat="server" Text=":"></asp:Label></TD><TD colSpan=3><asp:TextBox id="txtPeriod1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnCal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label>&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnCal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px"><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD style="WIDTH: 6px"><asp:Label id="Label4" runat="server" Text=":"></asp:Label></TD><TD colSpan=3><asp:DropDownList id="ddlStatus" runat="server" Width="130px" CssClass="inpText"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">In Process</asp:ListItem>
<asp:ListItem Value="POST">Post</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Middle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Middle"></asp:ImageButton></TD></TR><TR><TD colSpan=5><asp:GridView id="GVListInvoice" runat="server" Width="100%" ForeColor="#333333" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="Data Not Found">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="White" BorderColor="White" ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="SOID" DataNavigateUrlFormatString="~\transaction\trnInvoice.aspx?idPage={0}" DataTextField="invoiceno" HeaderText="No. Invoice">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="reqcode" HeaderText="No. TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SSTARTTIME" HeaderText="Tgl. Invoice">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="biayaservis" HeaderText="Biaya Servis">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttotalspart" HeaderText="Total S.Part">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="potongan" HeaderText="Amt Potongan" Visible="False">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtnetto" HeaderText="Amt. Netto">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SSTATUS" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="lbprint" runat="server" OnClick="printInvoice">Print</asp:LinkButton>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="reqcustoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="CECal1" runat="server" TargetControlID="txtPeriod1" PopupButtonID="btnCal1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" TargetControlID="txtPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CECal2" runat="server" TargetControlID="txtPeriod2" PopupButtonID="btnCal2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <asp:SqlDataSource id="SDSGVList" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT DISTINCT QLA.SOID, QLR.REQOID, QJA.PARTDESCSHORT SPARTM, QLR.REQBARCODE, QLR.REQCODE, QLJ.ITSERVDESC, QLZ.GENDESC ITSERVTYPE, QLAA.SPARTNOTE, QLAA.SPARTQTY, QLAA.SPARTPRICE FROM QL_TRNREQUEST QLR INNER JOIN QL_TRNMECHCHECK QLG ON QLG.MSTREQOID = QLR.REQOID INNER JOIN QL_MSTITEMSERV QLJ ON QLG.ITSERVOID = QLJ.ITSERVOID INNER JOIN QL_MSTGEN QLZ ON QLZ.GENOID = QLJ.ITSERVTYPEOID INNER JOIN QL_TRNSERVICES QLA ON QLA.SOID = QLG.ITSERVOID  INNER JOIN QL_TRNSERVICESPART QLAA ON QLAA.SOID = QLA.SOID INNER JOIN QL_MSTSPAREPART QJA ON QJA.PARTOID = QLAA.SPARTMOID WHERE (QLA.CMPCODE = @CMPCODE) AND (QLA.SOID = @SOID)&#13;&#10;"><SelectParameters>
<asp:Parameter Name="CMPCODE"></asp:Parameter>
<asp:Parameter Name="SOID"></asp:Parameter>
</SelectParameters>
</asp:SqlDataSource></TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="GVListInvoice"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <strong>
                                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                Form Invoice Servis</strong>&nbsp;<strong>:.</strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=6><asp:Label id="lblInformasi" runat="server" Font-Size="Small" Font-Bold="True" Text="Informasi" __designer:wfdid="w92" Font-Underline="True"></asp:Label><asp:Label id="PlanOid" runat="server" Visible="False" __designer:wfdid="w93"></asp:Label><asp:Label id="txtPlanOid" runat="server" Visible="False" __designer:wfdid="w94"></asp:Label><asp:Label id="txti_u" runat="server" Font-Bold="True" ForeColor="Red" Text="New" Visible="False" __designer:wfdid="w95"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="Label77" runat="server" Width="115px" Text="Cabang" __designer:wfdid="w96"></asp:Label></TD><TD>:</TD><TD><asp:DropDownList id="DdlCabang" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w97" AutoPostBack="True"></asp:DropDownList></TD><TD><asp:Label id="lblcustoid" runat="server" Visible="False" __designer:wfdid="w98"></asp:Label></TD><TD></TD><TD><asp:Label id="mtrwhoid" runat="server" Visible="False" __designer:wfdid="w99"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="Label70" runat="server" Width="115px" Text="No. Invoice Servis" __designer:wfdid="w100"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="noinvo" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w101" Enabled="False"></asp:TextBox></TD><TD><asp:Label id="Label14" runat="server" Text="Tanggal" __designer:wfdid="w102"></asp:Label></TD><TD><asp:Label id="Label33" runat="server" Text=":" __designer:wfdid="w103"></asp:Label></TD><TD><asp:TextBox id="txtTglTarget" runat="server" Width="70px" CssClass="inpTextDisabled" __designer:wfdid="w104" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w105"></asp:ImageButton>&nbsp;<asp:Label id="Label22" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" Visible="False" __designer:wfdid="w106"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="Label13" runat="server" Text="No. Tanda Terima" __designer:wfdid="w107"></asp:Label></TD><TD><asp:Label id="Label23" runat="server" Text=":" __designer:wfdid="w108"></asp:Label></TD><TD><asp:TextBox id="txtNo" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w109"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w110"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnErase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w111"></asp:ImageButton>&nbsp;</TD><TD><asp:Label id="Label66" runat="server" Width="72px" Text="Biaya Jasa" __designer:wfdid="w112"></asp:Label></TD><TD><asp:Label id="Label68" runat="server" Text=":" __designer:wfdid="w113"></asp:Label></TD><TD><asp:TextBox id="txtHargaJob" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w114" Enabled="False" ReadOnly="True"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="ReqOid" runat="server" Visible="False" __designer:wfdid="w115"></asp:Label></TD><TD></TD><TD colSpan=4><asp:GridView id="GVListInfo" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" __designer:wfdid="w116" DataKeyNames="reqoid,reqcode,custoid,custname,phone1,custaddr,mtrwhoid">
<RowStyle BackColor="#FFFBD6" ForeColor="Red"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="REQCODE" HeaderText="No. TT">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="DescCbInp" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtrwhoid" HeaderText="mtrwhoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
&nbsp;<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No Data Found !"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="Label16" runat="server" Text="Customer" __designer:wfdid="w117"></asp:Label></TD><TD><asp:Label id="Label24" runat="server" Text=":" __designer:wfdid="w118"></asp:Label></TD><TD><asp:TextBox id="txtNamaCust" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w119" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD><asp:Label id="Label67" runat="server" Width="128px" Text="Amt. Sparepart Total" __designer:wfdid="w120"></asp:Label></TD><TD><asp:Label id="Label39" runat="server" Text=":" __designer:wfdid="w121"></asp:Label></TD><TD><asp:TextBox id="txtHargaSPart" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w122" Enabled="False" ReadOnly="True" MaxLength="18">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="Label11" runat="server" Text="Alamat" __designer:wfdid="w123"></asp:Label></TD><TD><asp:Label id="Label26" runat="server" Text=":" __designer:wfdid="w124"></asp:Label></TD><TD><asp:TextBox id="txtAlamatCust" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w125" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD><asp:Label id="Label71" runat="server" Width="68px" Text="Amt. Netto" __designer:wfdid="w126"></asp:Label></TD><TD><asp:Label id="Label69" runat="server" Text=":" __designer:wfdid="w127"></asp:Label></TD><TD><asp:TextBox id="txtHargaServis" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w128" Enabled="False" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="Label36" runat="server" Text="Nomer Hp" __designer:wfdid="w129"></asp:Label></TD><TD><asp:Label id="Label35" runat="server" Text=":" __designer:wfdid="w130"></asp:Label></TD><TD><asp:TextBox id="txtHPCust" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w131" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD id="TD2" Visible="false"><asp:Label id="Label32" runat="server" Text="Status" __designer:wfdid="w132"></asp:Label></TD><TD id="TD1" Visible="false"><asp:Label id="Label37" runat="server" Text=":" __designer:wfdid="w133"></asp:Label></TD><TD id="TD3" Visible="false"><asp:TextBox id="txtStatus" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w134" Enabled="False">IN PROCESS</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="Label73" runat="server" Width="64px" Text="Amt. Netto" Visible="False" __designer:wfdid="w135"></asp:Label></TD><TD><asp:Label id="Label72" runat="server" Text=":" Visible="False" __designer:wfdid="w136"></asp:Label></TD><TD><asp:TextBox id="txtJumlah" runat="server" Width="130px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w137" Enabled="False" ReadOnly="True">0.0000</asp:TextBox></TD><TD id="TD6" Visible="false"><asp:Label id="Label38" runat="server" Width="56px" Text="Potongan" Visible="False" __designer:wfdid="w138"></asp:Label></TD><TD id="TD4" runat="server" Visible="false"><asp:Label id="Label74" runat="server" Text=":" Visible="False" __designer:wfdid="w139"></asp:Label></TD><TD id="TD5" runat="server" Visible="false"><asp:TextBox id="AmtDisc" runat="server" Width="130px" CssClass="inpText" Visible="False" __designer:wfdid="w140" AutoPostBack="True">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD><asp:Label id="Label8" runat="server" Width="132px" Font-Size="Small" Font-Bold="True" Text="Detail Informasi" __designer:wfdid="w141" Font-Underline="True" Font-Overline="False"></asp:Label></TD><TD></TD><TD><asp:Label id="SeqBarang" runat="server" Visible="False" __designer:wfdid="w142"></asp:Label><asp:Label id="I_Text2" runat="server" Font-Bold="True" ForeColor="Red" Text="New Detail" Visible="False" __designer:wfdid="w143" Font-Underline="True"></asp:Label></TD><TD></TD><TD></TD><TD></TD></TR><TR style="FONT-SIZE: 8pt"><TD>No. Final</TD><TD>:</TD><TD><asp:TextBox id="FinalNo" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w144" AutoPostBack="True" Enabled="False" ReadOnly="True"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnSfinal" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w145"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w146"></asp:ImageButton>&nbsp;<asp:Label id="FinalOid" runat="server" __designer:wfdid="w147"></asp:Label></TD><TD><asp:Label id="createtime" runat="server" Font-Bold="True" Visible="False" __designer:wfdid="w148"></asp:Label></TD><TD></TD><TD><asp:TextBox id="TglTgl" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w149"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="GvInvDtl" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" __designer:wfdid="w150">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FINOID" HeaderText="FinalOid" Visible="False">
<HeaderStyle HorizontalAlign="Left" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnfinalno" HeaderText="No. Final">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="BiayaService" HeaderText="Biaya Servis">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TotalNett" HeaderText="Amt. Netto">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtljob" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typetts" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CbgInput" HeaderText="CbgInput" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cabangasal" HeaderText="cabangasal" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Action"><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="ItemoidTTS" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=6>Created&nbsp;By <asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w151"></asp:Label>&nbsp;&nbsp;On&nbsp;<asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w152"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w153"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w154"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnReady" onclick="btnReady_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w155"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnBatal" onclick="btnBatal_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w156"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnprint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w157"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w158" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w159"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CEtglTarget" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalendar" TargetControlID="txtTglTarget" __designer:wfdid="w160"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MEPtglTarget" runat="server" TargetControlID="txtTglTarget" MaskType="Date" Mask="99/99/9999" __designer:wfdid="w161"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FteJumlah" runat="server" TargetControlID="txtJumlah" __designer:wfdid="w162" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEHrgJob" runat="server" TargetControlID="txtHargaJob" __designer:wfdid="w163" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteAmtDisc" runat="server" TargetControlID="AmtDisc" __designer:wfdid="w164" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEHrgSpart" runat="server" TargetControlID="txtHargaSPart" __designer:wfdid="w165" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEHrgSerVis" runat="server" TargetControlID="txtHargaServis" __designer:wfdid="w166" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="btnprint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="UpdatePanel8" runat="server">
                                <contenttemplate>
<asp:Panel id="panelItem" runat="server" Width="80%" CssClass="modalBox" Visible="False" __designer:wfdid="w168" DefaultButton="BtnFinals"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptItem" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar No. Final" __designer:wfdid="w169"></asp:Label></TD></TR><TR><TD align=center>Filter : <asp:TextBox id="FilterFinal" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w170"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnFinals" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top" __designer:wfdid="w171"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnViewFinal" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top" __designer:wfdid="w172"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView id="GVfinal" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" Visible="False" __designer:wfdid="w173" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("FINOID") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnfinalno" HeaderText="No. Final">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="BiayaService" HeaderText="Biaya Servis">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Totalspart" HeaderText="Total Spart">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="True" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typetts" HeaderText="Type Final">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FINOID" HeaderText="FinOid" Visible="False">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="locoidtitipan" HeaderText="locoidtitipan" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label91" runat="server" 
                                                                        Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center><asp:LinkButton id="AddToListFn" runat="server" __designer:wfdid="w174">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="btnCloseFn" runat="server" __designer:wfdid="w175">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"><asp:Label id="sValidasi" runat="server" Visible="False" __designer:wfdid="w176"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" TargetControlID="btnHideItem" BackgroundCssClass="modalBackground" PopupControlID="panelItem" PopupDragHandleControlID="lblCaptItem" __designer:wfdid="w177"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideItem" runat="server" Visible="False" __designer:wfdid="w178"></asp:Button><BR />
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="WARNING" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="WIDTH: 29px"><asp:ImageButton id="ImageButton3" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD colSpan=2><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD></TD><TD colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD style="WIDTH: 29px"></TD><TD align=center colSpan=2><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" PopupDragHandleControlID="WARNING" PopupControlID="PanelErrMsg" BackgroundCssClass="modalBackground" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

