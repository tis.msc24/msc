Imports System.Data
Imports System.Data.SqlClient
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure

Partial Class frmCekPI
    Inherits System.Web.UI.Page
    Dim sSql As String = ""
    Dim CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cmd As New SqlCommand("", conn)
    Dim cKon As New Koneksi
    Dim cFungsi As New ClassFunction
    Dim cProc As New ClassProcedure

    Private Sub CabDDl()
        sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangDDL, sSql)
            Else
                FillDDL(CabangDDL, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(CabangDDL, sSql)
            CabangDDL.Items.Add(New ListItem("SEMUA BRANCH", "ALL"))
            CabangDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub showMessage(ByVal isi As String, ByVal judul As String)
        isiPesan.Text = isi
        captionPesan.Text = judul
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        '' jangan lupa cek hak akses
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSDO")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSDO") = sqlSearch
            Response.Redirect("~/Transaction/frmCekPI.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Cek Hutang Pernota"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        If Not Page.IsPostBack Then
            CabDDl()
            tglAwal2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Private Sub DataBindTrn()
        Try
            If ordermstoid.Text <> "" Then
                sSql = "Select TOP 50 * FROM (" & _
    " Select con.branch_code, gc.gendesc, jm.trnbelimstoid trnOid, jm.trnbelino notrans, Convert(VarChar(20),jm.trnbelidate,103) trnDate, con.amttransidr trnAmtnya, con.amtbayaridr amtBayarNya, trnbelidate datee, '0' orot From QL_trnbelimst jm INNER JOIN QL_conap con ON jm.trnbelimstoid=con.refoid AND con.reftype='QL_trnbelimst' INNER JOIN ql_mstgen gc ON gc.gencode=jm.branch_code AND gc.gengroup='CABANG' " & _
    "UNION ALL Select py.branch_code, gc.gendesc, con.refoid trnOid, cb.cashbankno notrans, Convert(VarChar(20),cb.cashbankdate,103) trnDate, con.amttransidr trnAmtnya, con.amtbayaridr amtBayarNya, cashbankdate datee, '1' orot From ql_trnpayap py INNER JOIN ql_trncashbankmst cb ON cb.cashbankoid=py.cashbankoid AND cb.branch_code=py.branch_code INNER JOIN QL_conap con ON con.payrefoid=py.paymentoid AND con.branch_code=py.branch_code AND con.reftype='ql_trnpayap' INNER JOIN ql_mstgen gc ON gc.gencode=con.branch_code AND gc.gengroup='CABANG' AND con.trnapstatus='POST' " & _
    "UNION ALL Select con.branch_code, gc.gendesc, con.refoid trnOid, (CASE con.payrefno WHEN '' THEN SUBSTRING(trnapnote, 8, (SELECT CHARINDEX('for', trnapnote,4))-(SELECT CHARINDEX('.', trnapnote,4))-2) ELSE con.payrefno END) notrans, Case con.trnapdate When '1/1/1900' Then Convert(VarChar(20),con.paymentdate,103) Else Convert(VarChar(20),con.trnapdate,103) End trnDate, con.amttransidr trnAmtnya, con.amtbayaridr amtBayarNya, trnapdate datee, '1' orot From QL_conap con INNER JOIN QL_mstgen gc ON gencode=con.branch_code AND gengroup='CABANG' AND con.reftype<>'QL_trnbelimst' AND con.trnaptype<>'PAYAP' AND trnaptype<>'APK' " & _
    "UNION ALL Select con.branch_code, gc.gendesc, jm.trnbelimstoid trnOid, jm.trnbelireturno +' - ('+'DP)' notrans, Convert(VarChar(20),jm.trnbelidate,103) trnDate, 0.00 trnAmtnya, con.amtreturdp amtBayarNya, jm.trnbelidate datee, '2' orot From QL_trnbeliReturmst jm INNER JOIN QL_trnbelimst con ON jm.trnbelimstoid=con.trnbelimstoid AND jm.typeret='JADI DP' INNER JOIN ql_mstgen gc ON gc.gencode=jm.branch_code AND gc.gengroup='CABANG' " & _
    " ) tr Where trnoid=" & ordermstoid.Text & " Order by 9 asc,8 asc"
                Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
                Session("TblListMat") = dt
                gvListNya.DataSource = Session("TblListMat")
                gvListNya.DataBind() : gvListNya.Visible = True
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            Else
                showMessage("Maaf, Pilih Nomer nota (SI)", CompnyName)
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName)
            Exit Sub
        End Try
    End Sub

    Private Sub bindData()
        Try 
            sSql = "Select *, (AmtNetto-(amtRetur-AmtDP))-(amtBayar) AmtSisa From ( Select m.branch_code, m.trnbelimstoid orderoid, m.trnbelino orderno, m.trnsuppoid custoid, c.suppname custname, m.trnbelidate deliverydate, m.trnbelistatus orderstatus, m.periodacctg, ISNULL((Select SUM(amtjualdisc) From ql_trnjualdtl dt Where m.trnbelimstoid=dt.trnjualmstoid),0.0) AmtPot, ISNULL((Select SUM(dt.trndpapamtidr)-SUM(dt.trndpapacumamt) From QL_trndpap dt Where m.trnsuppoid=dt.suppoid),0.0) AmtDP, ISNULL((Select ABS(SUM(amtbayar)) from QL_conap con Where con.refoid=m.trnbelimstoid AND con.suppoid=m.trnsuppoid AND trnaptype='PAYAP' AND con.trnapstatus='POST'),0.00) + ISNULL((Select ABS(SUM(amtbayar)) from QL_conap con Where con.refoid=m.trnbelimstoid AND con.suppoid=m.trnsuppoid AND trnaptype='DNAP' AND con.trnapstatus='POST'),0.0) amtBayar, ISNULL((Select sum(rd.trnbelireturdtlqty*rd.trnbelireturdtlprice)-SUM(rd.amtdisc) From QL_trnbeliReturdtl rd INNER JOIN QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid Where rm.trnbelistatus IN ('APPROVED','POST') AND rm.trnsuppoid=m.trnsuppoid AND m.trnbelino=rm.trnbelino AND rm.typeret='NORMAL'),0.00) amtRetur, m.amtbelinettoidr + ISNULL((Select ABS(SUM(amtbayar)) from QL_conap con Where con.refoid=m.trnbelimstoid AND con.suppoid=m.trnsuppoid AND trnaptype='CNAP' AND con.trnapstatus='POST'),0.0) AmtNetto, 0.00 DiscUmum, ISNULL((Select SUM(dt.amtdisc2idr) From QL_trnbelidtl dt Where m.trnbelimstoid=dt.trnbelimstoid),0.00) DiscIn, ISNULL((Select SUM(om.trnbelidtlqty*trnbelidtlprice) from QL_trnbelidtl om Where om.trnbelimstoid=m.trnbelimstoid),0.00) AmtJualNya, '' NotaManual, m.typepo FROM QL_trnbelimst m inner join QL_mstsupp c on m.trnsuppoid=c.suppoid Where UPPER(m.cmpcode)='" & CompnyCode & "' and (m.trnbelino like '%" & TcharNoTrim(txtorderNo.Text) & "%' OR c.suppname LIKE '%" & TcharNoTrim(txtorderNo.Text) & "%')) tg Where orderstatus='POST' Order By orderoid Desc"
            FillGV(gvBPM2, sSql, "QL_Trnbeli")
            gvBPM2.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName)
            Exit Sub
        End Try
       
    End Sub

    Private Sub bindItem()
        Try
            sSql = "Select a.itemoid, item.itemcode, item.itemdesc, a.trnbelidtlqty qty, b.trnbelimstoid trnjualmstoid, (select gendesc From QL_mstgen Where gengroup='itemunit' and genoid = a.trnbelidtlunitoid) unit, a.trnbelidtlprice PriceItem, a.amtbelinettoidr amtjualnetto From QL_trnbelidtl a inner join QL_trnbelimst b ON a.trnbelimstoid=b.trnbelimstoid AND a.branch_code=b.branch_code inner join QL_mstitem item on item.itemoid = a.itemoid Where b.trnbelino='" & Tchar(txtorderNo.Text.Trim) & "' and a.branch_code='" & CabangDDL.SelectedValue & "'"
            FillGV(gv_item, sSql, "ql_trnordermstdtl")
            gv_item.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim x As Integer = 123
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, False)
    End Sub

    Protected Sub imbSearchBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchBPM.Click
        bindData()
    End Sub

    Protected Sub imbEraseBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseBPM.Click
        txtorderNo.Text = "" : ordermstoid.Text = ""
        custname.Text = ""
        gvBPM2.Visible = False : gv_item.Visible = False
        gv_item.SelectedIndex = -1
        gvBPM2.SelectedIndex = -1
    End Sub

    Protected Sub gvBPM2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBPM2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy") 
        End If
    End Sub

    Protected Sub gvBPM2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBPM2.SelectedIndexChanged
        Try
            ordermstoid.Text = gvBPM2.SelectedDataKey.Item("orderoid").ToString
            txtorderNo.Text = gvBPM2.SelectedDataKey.Item("orderno").ToString
            custname.Text = gvBPM2.SelectedDataKey.Item("custname").ToString
            tglAwal2.Text = Format(gvBPM2.SelectedDataKey.Item("deliverydate"), "dd/MM/yyyy")
            TotNota.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("AmtNetto")), 3)
            AmtBayar.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("amtBayar")), 3)
            DiscUmum.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("DiscUmum")), 3)
            DiscInt.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("DiscIn")), 3)
            AmtDisc.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("AmtPot")), 3)
            AmtDP.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("AmtDP")), 3)
            AmtBruto.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("AmtJualNya")), 3)
            AmtRet.Text = ToMaskEdit(ToDouble(gvBPM2.SelectedDataKey.Item("amtRetur")), 3)
            AmtSisaNett.Text = ToMaskEdit(gvBPM2.SelectedDataKey.Item("AmtSisa"), 3)
            CabangDDL.SelectedValue = gvBPM2.SelectedDataKey.Item("branch_code").ToString
            gvBPM2.Visible = False : gvBPM2.SelectedIndex = -1
            btnCloseBPM.Visible = True : btnInfoTrn.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName)
            Exit Sub
        End Try
        
    End Sub

    Protected Sub gvBPM2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBPM2.PageIndexChanging
        gvBPM2.PageIndex = e.NewPageIndex
        bindData()
    End Sub

    Protected Sub btnCloseBPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseBPM.Click
        bindItem()
    End Sub

    Protected Sub gv_item_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gv_item.PageIndexChanging
        gv_item.PageIndex = e.NewPageIndex
        bindItem()
    End Sub

    Protected Sub gv_item_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_item.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub btnInfoTrn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInfoTrn.Click
        DataBindTrn()
        mpeListMat.Show()
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListNya_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub gvListNya_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListNya.PageIndexChanging
        gvListNya.PageIndex = e.NewPageIndex
        DataBindTrn() : mpeListMat.Show()
    End Sub
End Class
