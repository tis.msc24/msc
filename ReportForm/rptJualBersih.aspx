<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptJualBersih.aspx.vb" Inherits="rptJualBersih" Title="MSC - Laporan Penjualan Bersih" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Laporan Penjualan Bersih"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: white" valign="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE><TBODY><TR><TD id="tdPeriod1" align=left runat="server" visible="true"><asp:Label id="Tanggal" runat="server" Text="Tanggal"></asp:Label> </TD><TD id="Td1" align=left runat="server" visible="true">:</TD><TD id="tdperiod2" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> to&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label2" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)"></asp:Label> </TD></TR><TR><TD id="Td2" align=left runat="server" visible="true">Cabang</TD><TD id="Td3" align=left runat="server" visible="true">:</TD><TD id="Td4" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="dd_branch" runat="server" Width="185px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=left runat="server" visible="true">Type Laporan</TD><TD align=left runat="server" visible="true">:</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="TypeDDL" runat="server" Width="185px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="PERITEM">PENJUALAN BERSIH PER ITEM</asp:ListItem>
<asp:ListItem Value="PERNOTA">PENJUALAN BERSIH PER NOTA</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server" visible="true"><asp:Label id="Label15" runat="server" Width="65px" Text="Type SI" __designer:wfdid="w44"></asp:Label></TD><TD align=left runat="server" visible="true">:</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:CheckBox id="cbCheckAll" runat="server" BackColor="#FFFFC0" Text="CHECK ALL" AutoPostBack="True" __designer:wfdid="w5" BorderColor="Black"></asp:CheckBox><asp:CheckBoxList id="cbType" runat="server" ForeColor="White" BackColor="#FFFFC0" AutoPostBack="True" __designer:wfdid="w6" OnSelectedIndexChanged="cbType_SelectedIndexChanged" CellPadding="5" BorderColor="Black" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><asp:DropDownList id="DDLPenjType" runat="server" Width="152px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w43" Visible="False"></asp:DropDownList></TD></TR><TR><TD id="Td5" align=left runat="server" visible="true"><asp:DropDownList id="ddlPIC" runat="server" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="jb.createuser">SALES</asp:ListItem>
<asp:ListItem Value="jb.Amp">PIC</asp:ListItem>
</asp:DropDownList></TD><TD id="Td6" align=left runat="server" visible="true">:</TD><TD id="Td7" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="TxtPinjamNo" runat="server" Width="185px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="sPerson" onclick="sPerson_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="eRasePerson" onclick="eRasePerson_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:Label id="PersonOid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="Td8" align=left runat="server" visible="true"></TD><TD id="Td9" align=left runat="server" visible="true"></TD><TD id="Td10" align=left colSpan=3 runat="server" visible="true"><asp:GridView id="PersonGv" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" OnSelectedIndexChanged="PersonGv_SelectedIndexChanged" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="personoid,personname" UseAccessibleHeader="False" CellPadding="4" OnPageIndexChanging="PersonGv_PageIndexChanging" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="Red"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Maroon" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="personnip" HeaderText="NIP">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Nama Sales">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="PERSONCRTADDRESS" HeaderText="Alamat Sales">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left runat="server" visible="true"><asp:Label id="Label3" runat="server" Width="91px" Text="Customer" __designer:wfdid="w4"></asp:Label></TD><TD align=left runat="server" visible="true">:</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="CustName" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w1"></asp:TextBox>&nbsp;<asp:ImageButton id="sCust" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w2"></asp:ImageButton>&nbsp;<asp:ImageButton id="eCust" onclick="imbEraseItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w3"></asp:ImageButton></TD></TR><TR><TD align=left runat="server" visible="true"></TD><TD align=left runat="server" visible="true"></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:GridView id="GvCust" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w6" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" UseAccessibleHeader="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="CbHdrOid" runat="server" AutoPostBack="true" __designer:wfdid="w13" Checked="<%# GetSessionCheckNota() %>" OnCheckedChanged="CbHdrOid_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="SelectOid" runat="server" __designer:wfdid="w12" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("custoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Cust. code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" HeaderText="custoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!" __designer:wfdid="w10"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Black"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left runat="server" visible="true"><asp:Label id="LblSi" runat="server" Width="91px" Text="No. Transaksi"></asp:Label></TD><TD align=left runat="server" visible="true"><asp:Label id="Label1" runat="server" Width="2px" Text=":"></asp:Label></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="TxtNoTrans" runat="server" Width="250px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="sBtnNoTrans" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="eBtnNoTrans" onclick="imbEraseItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR><TR><TD align=left runat="server" visible="true"></TD><TD align=left runat="server" visible="true"></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:GridView id="GvNotrans" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" UseAccessibleHeader="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="CbHdrNo" runat="server" AutoPostBack="true" __designer:wfdid="w16" Checked="<%# GetSessionCheckNota() %>" OnCheckedChanged="CbHdrNo_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="CekSelect" runat="server" __designer:wfdid="w15" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("JualOid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="notrans" HeaderText="No. Transaksi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnDate" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Black"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Type Barang</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddlTypeBarang" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>UNGGULAN</asp:ListItem>
<asp:ListItem>NON UNGGULAN</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD vAlign=top align=left><asp:Label id="LblBarang" runat="server" Width="43px" Text="Katalog"></asp:Label></TD><TD align=left><asp:Label id="Lbl2" runat="server" Width="2px" Text=":"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="itemname" runat="server" Width="250px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchItem" onclick="imbSearchItem_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseItem" onclick="imbEraseItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD vAlign=top align=right></TD><TD vAlign=top align=right></TD><TD align=left colSpan=3><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" OnSelectedIndexChanged="gvItem_SelectedIndexChanged" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemcode,itemoid,itemdesc,merk" UseAccessibleHeader="False" CellPadding="4" OnPageIndexChanging="gvItem_PageIndexChanging" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbHdrLM" runat="server" AutoPostBack="true" OnCheckedChanged="cbHdrLM_CheckedChanged" Checked='<%# GetSessionCheckItem() %>'></asp:CheckBox>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" ToolTip='<%# Eval("itemoid") %>' Checked='<%# Eval("selected") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Item">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Item">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Maroon" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Black"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
    <PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast" />
</asp:GridView></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnreport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibpdf" onclick="ibpdf_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
<div id="progressBackgroundFilter" class="progressBackgroundFilter">
</div>
<div id="processMessage" class="processMessage">
<span style="font-weight: bold; font-size: 10pt; color: purple">
<asp:Image ID="Image4" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/loadingbar.gif" /><br />
Please Wait .....</span><br />
</div>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w3" TargetControlID="FilterPeriod1" Enabled="True" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureThousandsPlaceholder="" CultureDecimalPlaceholder="" CultureTimePlaceholder="" CultureDatePlaceholder="" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w4" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CEtxtPeriod2" runat="server" __designer:wfdid="w2" TargetControlID="txtPeriod2" Enabled="True" PopupButtonID="ibPeriod2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CEFilterPeriod1" runat="server" __designer:wfdid="w1" TargetControlID="FilterPeriod1" Enabled="True" PopupButtonID="ibPeriod1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="ibpdf"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

