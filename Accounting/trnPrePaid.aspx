<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPrePaid.aspx.vb" Inherits="Accounting_trnPrePaid" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server"> 
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    Text=".: Pre-Paid Transaksi"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%"><ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="TabPanel1"><ContentTemplate>
<asp:UpdatePanel id="UpdatePanel2" runat="server"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><asp:MultiView id="MultiView2" runat="server" ActiveViewIndex="0"><asp:View id="View3" runat="server"><asp:Panel id="Panel2" runat="server" DefaultButton="btnSearch"><TABLE><TBODY><TR><TD align=left colSpan=5><asp:Label id="lblPosInformation" runat="server" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label> <asp:Label id="Label36" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="|" __designer:wfdid="w464"></asp:Label> <asp:LinkButton id="lbkPostMoreInfo" onclick="lbkPostMoreInfo_Click" runat="server" CssClass="submenu">Monthly Posting</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 115px" align=left>Cabang</TD><TD align=left colSpan=4><asp:DropDownList id="dCabangNya" runat="server" CssClass="inpText" __designer:wfdid="w1"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 115px" align=left><asp:CheckBox id="cbPeriode" runat="server" Width="74px" Text="Tanggal"></asp:CheckBox></TD><TD align=left colSpan=4><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label160" runat="server" Text="to"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 115px" align=left><asp:CheckBox id="cbDesc" runat="server" Text="Prepaid Desc"></asp:CheckBox></TD><TD align=left colSpan=4><asp:TextBox id="txtFilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 115px" align=left><asp:CheckBox id="cbBlmPosting" runat="server" Text="Not Posted Yet"></asp:CheckBox></TD><TD align=left colSpan=4><asp:Button id="btnSearch" runat="server" Width="75px" CssClass="orange" Font-Bold="True" ForeColor="White" Text="FIND"></asp:Button> <asp:Button id="btnList" runat="server" Width="75px" CssClass="gray" Font-Bold="True" ForeColor="White" Text="VIEW ALL"></asp:Button></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="GVPrePaidmst" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cmpcode,prepaidmstoid" GridLines="None" PageSize="8" OnSelectedIndexChanged="GVPrePaidmst_SelectedIndexChanged" OnPageIndexChanging="GVPrePaidmst_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="prepaidmstoid" HeaderText="prepaidmstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outletname" HeaderText="Outlet">
<HeaderStyle CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmststartdate" HeaderText="StartDate">
<HeaderStyle CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Width="200px"></HeaderStyle>

<ItemStyle Wrap="True" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstfinishdate" HeaderText="FinishDate">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstamount" HeaderText="Prepaid Amt">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmsttotalamount" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmststatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label38" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data tidak ditemukan !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvalternate"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" CssClass="Important" Text="Click button Find or View All to view data"></asp:Label></TD></TR><TR><TD align=left><ajaxToolkit:CalendarExtender id="ce2" runat="server" TargetControlID="txtPeriode1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1">
                            </ajaxToolkit:CalendarExtender> </TD><TD align=left><ajaxToolkit:CalendarExtender id="ce3" runat="server" TargetControlID="txtPeriode2" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender> </TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" TargetControlID="txtPeriode1" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" TargetControlID="txtPeriode2" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE></asp:Panel></asp:View><asp:View id="View4" runat="server"><asp:UpdatePanel id="UpdatePanelPostingDtl" runat="server"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=left colSpan=7><asp:LinkButton id="lbkPostInfo" onclick="lbkPostInfo_Click" runat="server" CssClass="submenu" __designer:wfdid="w4">Information</asp:LinkButton>&nbsp;<asp:Label id="Label37" runat="server" Font-Size="Small" ForeColor="Black" Text="|" __designer:wfdid="w5"></asp:Label> <asp:Label id="Label26" runat="server" Font-Bold="True" ForeColor="Black" Text="Monthly Posting" __designer:wfdid="w6"></asp:Label></TD></TR><TR><TD align=left colSpan=1>Cabang</TD><TD align=left colSpan=6><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" __designer:wfdid="w2"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=1><asp:Label id="Label90" runat="server" Font-Bold="False" Text="Bulan"></asp:Label></TD><TD align=left colSpan=6><asp:DropDownList id="ddlMonth" runat="server" CssClass="inpText"><asp:ListItem Text="January" Value="1"></asp:ListItem>
<asp:ListItem Text="February" Value="2"></asp:ListItem>
<asp:ListItem Text="March" Value="3"></asp:ListItem>
<asp:ListItem Text="April" Value="4"></asp:ListItem>
<asp:ListItem Text="May" Value="5"></asp:ListItem>
<asp:ListItem Text="June" Value="6"></asp:ListItem>
<asp:ListItem Text="July" Value="7"></asp:ListItem>
<asp:ListItem Text="August" Value="8"></asp:ListItem>
<asp:ListItem Text="September" Value="9"></asp:ListItem>
<asp:ListItem Text="October" Value="10"></asp:ListItem>
<asp:ListItem Text="November" Value="11"></asp:ListItem>
<asp:ListItem Text="December" Value="12"></asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD align=left colSpan=1><asp:Label id="Label89" runat="server" Font-Bold="False" Text="Tahun"></asp:Label></TD><TD align=left colSpan=6><asp:DropDownList id="ddlYear" runat="server" CssClass="inpText">
                                </asp:DropDownList> <asp:Button id="FindBtn" onclick=" Findbtn_Click" runat="server" Width="75px" CssClass="red" Font-Bold="True" ForeColor="White" Text="FIND"></asp:Button> <asp:Button id="btnBatal" runat="server" Width="75px" CssClass="gray" Font-Bold="True" ForeColor="White" Text="CANCEL"></asp:Button> <asp:Button id="btnPostingDtl" runat="server" Width="75px" CssClass="orange" Font-Bold="True" ForeColor="White" Text="POSTING"></asp:Button></TD></TR><TR><TD align=left colSpan=7><asp:GridView id="GVDtlMonth" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w3" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="branch_code,prepaiddtloid,prepaidmstoid,prepaiddtldate,prepaidmstcode,prepaidmstacctgoid,prepaidmstpayacctgoid,prepaiddtlamount,prepaiddtlaccumamt,prepaidmsttotalamount" GridLines="None" OnRowDataBound="GVDtlMonth_RowDataBound">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="prepaiddtloid" HeaderText="ID" Visible="False">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtldate" HeaderText="Tanggal">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaidmstdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlaccumamt" HeaderText="Pay Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlaccum" HeaderText="Book Value">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" CssClass="gvfooter" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label39" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE>
</ContentTemplate>
</asp:UpdatePanel></asp:View> </asp:MultiView></asp:Panel> 
</ContentTemplate>
    <triggers>
<asp:PostBackTrigger ControlID="GVPrePaidmst"></asp:PostBackTrigger>
</triggers>
</asp:UpdatePanel> 
</ContentTemplate>
<HeaderTemplate>
<STRONG><SPAN style="FONT-SIZE: 9pt"><asp:Image id="Image2" runat="server" ImageUrl="~/Images/../Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image>
    List Prepaid Transaksi &nbsp;:.</SPAN></STRONG>&nbsp; 
</HeaderTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="TabPanel2"><ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD><asp:Label id="Outlet" runat="server" Text="Cabang" __designer:wfdid="w391"></asp:Label></TD><TD><asp:DropDownList id="DDLCabang" runat="server" Width="180px" CssClass="inpText" __designer:wfdid="w392" AutoPostBack="True"></asp:DropDownList></TD><TD>No Expense</TD><TD><asp:TextBox id="cashbankno" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w1"></asp:TextBox> <asp:ImageButton id="imbFindExp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton> <asp:ImageButton id="imbClearExp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton></TD><TD><asp:Label id="cashbankgloid" runat="server" __designer:wfdid="w5" Visible="False"></asp:Label></TD><TD></TD></TR><TR><TD></TD><TD></TD><TD colSpan=4><asp:GridView id="gvExpense" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w446" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankoid,cashbankno,cashbankdate,cashbankgloid,acctgoid,cashbankglnote,cashbankglamt,branch_code" GridLines="None" PageSize="5">
<RowStyle HorizontalAlign="Right" BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankno" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" DataFormatString="{0:#,##0.00}" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label11" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No Detail Data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD><asp:Label id="Label27" runat="server" Width="92px" Text="Pre Paid Code" __designer:wfdid="w393"></asp:Label> </TD><TD><asp:TextBox id="prepaidmstcode" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w394" MaxLength="20" ReadOnly="True"></asp:TextBox></TD><TD><asp:Label id="Label19" runat="server" Text="Deskripsi " __designer:wfdid="w396"></asp:Label> <asp:Label id="Label2x" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w398"></asp:Label></TD><TD><asp:TextBox id="PrepaidDesc" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w397" MaxLength="145"></asp:TextBox></TD><TD></TD><TD></TD></TR><TR><TD><asp:Label id="Label6" runat="server" Width="81px" Text="Prepaid Date" __designer:wfdid="w399"></asp:Label></TD><TD><asp:TextBox id="PrepaidDate" runat="server" Width="61px" CssClass="inpTextDisabled" __designer:wfdid="w400" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w401" Visible="False"></asp:ImageButton> <asp:Label id="Label14" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w402" Visible="False"></asp:Label></TD><TD><asp:Label id="Label25" runat="server" Width="77px" Text="Tanggal Awal" __designer:wfdid="w403"></asp:Label></TD><TD><asp:TextBox id="StartDate" runat="server" Width="49px" CssClass="inpText" __designer:wfdid="w404" AutoPostBack="True" OnTextChanged="StartDate_TextChanged"></asp:TextBox> <asp:ImageButton id="BtnStartDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w405"></asp:ImageButton>&nbsp;<asp:Label id="Label20" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w406"></asp:Label></TD><TD><asp:Label id="lbldate" runat="server" Width="86px" Text="Tanggal Akhir" __designer:wfdid="w407"></asp:Label></TD><TD><asp:TextBox id="FinishDate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w408" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD><asp:Label id="Label7" runat="server" Width="73px" Text="Prepaid Amt" __designer:wfdid="w411"></asp:Label></TD><TD><asp:TextBox id="PrePaidAmt" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w412" AutoPostBack="True" ReadOnly="True"></asp:TextBox> </TD><TD><asp:Label id="Label8" runat="server" Width="78px" Text="Length Period" __designer:wfdid="w416" Visible="False"></asp:Label></TD><TD><asp:TextBox id="PrepaidLenght" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w417" AutoPostBack="True" Visible="False" MaxLength="5">1</asp:TextBox> <asp:Label id="Monthly" runat="server" Font-Size="X-Small" ForeColor="Red" Text="(Month)" __designer:wfdid="w418" Visible="False"></asp:Label> <asp:Label id="Label33" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w419" Visible="False"></asp:Label></TD><TD><asp:Label id="Label24" runat="server" Width="127px" Text="Prepaid Accum Amt" __designer:wfdid="w414"></asp:Label></TD><TD><asp:TextBox id="accumAmt" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w415" OnTextChanged="accumAmt_TextChanged" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 23px"><asp:Label id="Label17" runat="server" Width="115px" Text="Expense Account" __designer:wfdid="w423"></asp:Label></TD><TD style="HEIGHT: 23px" colSpan=3><asp:DropDownList id="prepaidmstacctgoid" runat="server" Width="350px" CssClass="inpTextDisabled" __designer:wfdid="w424" Enabled="False"></asp:DropDownList></TD><TD style="HEIGHT: 23px"><asp:Label id="Label10" runat="server" Text="Book Value" __designer:wfdid="w420"></asp:Label></TD><TD style="HEIGHT: 23px"><asp:TextBox id="PrepaidAccum" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w421" AutoPostBack="True" MaxLength="15" Enabled="False" OnTextChanged="PrepaidAccum_TextChanged"></asp:TextBox> <asp:Label id="Label43" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w422"></asp:Label></TD></TR><TR><TD><asp:Label id="Label23" runat="server" Width="97px" Text="Accum Account" __designer:wfdid="w427"></asp:Label></TD><TD colSpan=3><asp:DropDownList id="ddLPayAcctg" runat="server" Width="350px" CssClass="inpText" __designer:wfdid="w428"></asp:DropDownList></TD><TD><asp:Label id="Label4" runat="server" Text="Angsuran" __designer:wfdid="w425"></asp:Label> <asp:Label id="Label15" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w413"></asp:Label></TD><TD><asp:TextBox id="hasilbagi" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w426" AutoPostBack="True" MaxLength="5" OnTextChanged="hasilbagi_TextChanged"></asp:TextBox></TD></TR><TR><TD><asp:Label id="Label9" runat="server" Width="81px" Text="Prepaid Note" __designer:wfdid="w409"></asp:Label></TD><TD colSpan=3><asp:TextBox id="PaidNote" runat="server" Width="390px" CssClass="inpText" __designer:wfdid="w410" MaxLength="90"></asp:TextBox></TD><TD><asp:Label id="Label42" runat="server" Width="83px" Text="Prepaid Value" __designer:wfdid="w429"></asp:Label></TD><TD><asp:TextBox id="PrepaidmstValue" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w430" AutoPostBack="True" MaxLength="15" OnTextChanged="PrepaidmstValue_TextChanged" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD><asp:Label id="Label3" runat="server" Text="Status" __designer:wfdid="w436"></asp:Label></TD><TD><asp:TextBox id="StatusMst" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w437" AutoPostBack="True" MaxLength="15" OnTextChanged="PrepaidmstValue_TextChanged" ReadOnly="True"></asp:TextBox></TD><TD><asp:Label id="offdate" runat="server" Text="CutOffDate" __designer:wfdid="w441" Visible="False"></asp:Label></TD><TD><asp:TextBox id="CutoffDatePaid" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w442" Visible="False" MaxLength="20" ReadOnly="True"></asp:TextBox></TD><TD colSpan=2><asp:Button id="btnGenerate" onclick="btnGenerate_Click" runat="server" Width="136px" CssClass="green" Font-Bold="True" ForeColor="White" Text="AUTO GENERATE" __designer:wfdid="w189"></asp:Button></TD></TR><TR><TD><asp:Label id="payacctgoid" runat="server" Text="payacctgoid" __designer:wfdid="w435" Visible="False"></asp:Label></TD><TD colSpan=3><asp:Label id="Label12" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="* Set Depreciation (month)  -1 if have no Depreciation !!" __designer:wfdid="w466" Visible="False"></asp:Label> <asp:Label id="lblPOST" runat="server" Text="lblPOST" __designer:wfdid="w439" Visible="False"></asp:Label></TD><TD><asp:Label id="Label29" runat="server" Text="Buku Value" __designer:wfdid="w443" Visible="False"></asp:Label></TD><TD><asp:TextBox id="fixPresentValue" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w444" Visible="False">0</asp:TextBox> <asp:Label id="Label22" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w445" Visible="False"></asp:Label></TD></TR><TR><TD><asp:Label id="Label16" runat="server" Width="90px" Text="Tipe Period" __designer:wfdid="w431" Visible="False"></asp:Label></TD><TD colSpan=5><asp:DropDownList id="DDLPeriodType" runat="server" CssClass="inpText" __designer:wfdid="w472" AutoPostBack="True" Visible="False" OnSelectedIndexChanged="DDLPeriod_SelectedIndexChanged"><asp:ListItem>MONTHLY</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="PrePaiddtlAccum" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w473" Visible="False" MaxLength="20" ReadOnly="True"></asp:TextBox> <asp:Label id="PrePaidmstoid" runat="server" Text="PrePaidmsoid" __designer:wfdid="w434" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=6><asp:GridView id="GVprePaiddtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w446" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="prepaiddtlaccumamt" GridLines="None" PageSize="8" OnSelectedIndexChanged="GVprePaiddtl_SelectedIndexChanged" OnPageIndexChanging="GVprePaiddtl_PageIndexChanging">
<RowStyle HorizontalAlign="Right" BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="prepaiddtldate" DataFormatString="{0:MMMM d, yyyy}" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlaccumamt" DataFormatString="{0:#,##0.00}" HeaderText="Pay Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlamount" DataFormatString="{0:#,##0.00}" HeaderText="Prepaid Dtl Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlaccum" DataFormatString="{0:#,##0.00}" HeaderText="Prepaid Accum">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="prepaiddtlstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label11" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No Detail Data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=6><asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w447"></asp:Label> <asp:Label id="UpdUser" runat="server" Font-Bold="True" __designer:wfdid="w448"></asp:Label> </TD></TR><TR><TD colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w468"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w469"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnPosting" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w471"></asp:ImageButton> <asp:ImageButton id="BtnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w470"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w453" TargetControlID="PrepaidDate" Format="dd/MM/yyyy" PopupButtonID="btnDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w454" TargetControlID="PrepaidDate" CultureName="en-US" Mask="99/99/9999" MaskType="Date" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="DateStart" runat="server" __designer:wfdid="w455" TargetControlID="StartDate" Format="dd/MM/yyyy" PopupButtonID="BtnStartDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:FilteredTextBoxExtender id="prepaid" runat="server" __designer:wfdid="w456" TargetControlID="PrePaidAmt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEhasilbagi" runat="server" __designer:wfdid="w457" TargetControlID="hasilbagi" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ValuePrepaid" runat="server" __designer:wfdid="w458" TargetControlID="PrepaidmstValue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="AccumPrepaid" runat="server" __designer:wfdid="w459" TargetControlID="PrepaidAccum" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:MaskedEditExtender id="MEEFinishDate" runat="server" __designer:wfdid="w460" TargetControlID="FinishDate" CultureName="en-US" Mask="99/99/9999" MaskType="Date" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MEEStartDate" runat="server" __designer:wfdid="w461" TargetControlID="StartDate" CultureName="en-US" Mask="99/99/9999" MaskType="Date" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender> <asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w462" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w463"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnSave"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel78" runat="server">
        <contenttemplate>
<asp:Panel id="panelMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE style="WIDTH: 99%; HEIGHT: 109%"><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeValidasi" runat="server" TargetControlID="btnValidasi" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnValidasi" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
                        
</ContentTemplate>
<HeaderTemplate>
                            <span style="font-size: 9pt">
                             <strong><span>
                                 <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/../Images/corner.gif" />
                                 Form Prepaid Transaksi:.</span></strong></span>&nbsp;
                            
                        
</HeaderTemplate>
</ajaxToolkit:TabPanel>
</ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>