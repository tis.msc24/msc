Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptTItem
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim Report As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim CProc As New ClassProcedure
    Dim CKon As New Koneksi
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sSql As String = ""
    Public folderReport As String = "~/Report/"
#End Region

#Region "Prcedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyCode
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindTransNo()
        sSql = "Select transformoid,transformno,Convert(VarChar(20),transformdate,103) AS Transdate,transformtype from QL_TransformItemMst Where cmpcode='MSC' AND transformno LIKE '%" & Tchar(txtTransNo.Text.Trim) & "%'"
        TransItemNo.DataSource = Nothing : TransItemNo.DataBind()
        Dim noTrans As DataTable = CKon.ambiltabel(sSql, "QL_TransformItemMst")
        TransItemNo.DataSource = noTrans : TransItemNo.DataBind()
        Session("QL_TransNo") = noTrans
        TransItemNo.Visible = True
    End Sub

    Private Sub BindListMat()
        sSql = "Select DISTINCT i.itemoid,itemcode matcode,itemdesc matlongdesc From ql_mstitem i INNER JOIN QL_TransformItemDtlmst dmst ON i.itemoid=dmst.itemoid Where (itemcode LIKE '%" & Tchar(barang.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(barang.Text.Trim) & "%') AND dmst.transformoid LIKE '%" & lblTransOid.Text & "%' " & _
        " UNION " & _
        " Select DISTINCT i.itemoid,itemcode matcode,itemdesc matlongdesc From ql_mstitem i INNER JOIN QL_TransformItemDtl ddt ON i.itemoid=ddt.itemoid Where (itemcode LIKE '%" & Tchar(barang.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(barang.Text.Trim) & "%') AND ddt.transformoid LIKE '%" & lblTransOid.Text & "%'"
        gvMat.DataSource = Nothing : gvMat.DataBind()
        Dim MstItem As DataTable = CKon.ambiltabel(sSql, "ql_mstitem")
        gvMat.DataSource = MstItem : gvMat.DataBind()
        Session("TblMat") = MstItem
    End Sub 'OK

    Private Sub DDLUnit()
        sSql = "SELECT a.gencode, a.gencode +' - '+ a.gendesc FROM QL_mstgen a WHERE a.gengroup = 'cabang' AND a.cmpcode='" & CompnyCode & "' ORDER BY a.gencode"
        FillDDL(DDLbranch, sSql)
        DDLbranch.Items.Add("ALL")
        DDLbranch.Items(DDLbranch.Items.Count - 1).Value = "ALL"
        DDLbranch.SelectedValue = "ALL"
    End Sub

    Sub DDLFromWh()
        sSql = "Select genoid,gendesc from QL_mstgen Where genoid IN (Select DISTINCT dt.locoid from QL_TransformItemDtlMst dt INNER JOIN QL_TransformItemMst ms On ms.transformoid=dt.transformoid And ms.transformno LIKE '%%') AND gengroup='LOCATION'"
        FillDDL(ddllocawal, sSql)
        ddllocawal.Items.Add("ALL")
        ddllocawal.SelectedValue = "ALL"
    End Sub

    Sub DDLToWh(ByVal WhOId As String)
        sSql = "Select genoid,gendesc from QL_mstgen Where genoid IN (Select DISTINCT dt.locoid from QL_TransformItemDtl dt INNER JOIN QL_TransformItemMst ms On ms.transformoid=dt.transformoid And ms.transformno LIKE '%%') AND gengroup='LOCATION'"
        'If ddllocawal.SelectedItem.Text <> "ALL" Then
        FillDDL(ddllocakhir, sSql)
        'Else
        ddllocakhir.Items.Add("ALL")
        ddllocakhir.SelectedValue = "ALL"
        'End If
    End Sub

    Sub showPrint(ByVal sType As String)
        Try
            Session("diprint") = "False"
            Dim PlusLoc As String = "" : Dim MinLoc As String = ""
            Dim rptName As String = "" : Dim sWhere As String = ""
            sWhere &= " Where a.transformtype='" & TypeTransDll.SelectedValue & "' AND itemcode LIKE '%" & Tchar(barang.Text.Trim) & "%' AND a.transformno LIKE '%" & Tchar(txtTransNo.Text.Trim) & "%'"
            sWhere &= " And a.transformdate between '" & CDate(toDate(range1.Text)) & "' and '" & CDate(toDate(range2.Text)) & "'"

            If DDLbranch.SelectedItem.Text <> "ALL" Then
                sWhere &= " AND branch_code='" & DDLbranch.SelectedValue & "'"
            End If

            If ddllocawal.SelectedItem.Text.ToUpper <> "ALL" Then
                PlusLoc &= " AND b.locoid=" & ddllocawal.SelectedValue & ""
            End If

            If ddllocakhir.SelectedItem.Text.ToUpper <> "ALL" Then
                MinLoc &= " AND b.locoid=" & ddllocakhir.SelectedValue & ""
            End If

            Report = New ReportDocument
            If sType = "xls" Then
                Report.Load(Server.MapPath("~\Report\rptCrtTransform.rpt"))
                rptName = "Transform_Item_Price_Report"
            Else
                Report.Load(Server.MapPath("~\Report\rptCrtTransform.rpt"))
                rptName = "Transform_Item_Price_Report"
            End If
            Report.SetParameterValue("sWhere", sWhere)
            Report.SetParameterValue("MinLoc", MinLoc)
            Report.SetParameterValue("PlusLoc", PlusLoc)
            Report.SetParameterValue("TypeTrans", TypeTransDll.SelectedItem.Text)
            Report.SetParameterValue("sPeriode", range1.Text & " - " & range2.Text)
            Report.SetParameterValue("reportName", Report.FileName.Substring(Report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            CProc.SetDBLogonForReport(Report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'CProc.SetDBLogonForReport(Report)
            Session("diprint") = "True"
            If sType = "crv" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = Report
            ElseIf sType = "xls" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Transform_Item_Price_Report" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close() : Report.Dispose()
                Response.Redirect("~\ReportForm\rptTranformItem.aspx")
            ElseIf sType = "pdf" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Transform_Item_Price_Report" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close()
                Report.Dispose()
                Response.Redirect("~\ReportForm\rptTranformItem.aspx")
            Else
            End If

        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()

            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("SpecialAccess") = xsetAcc
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\ReportForm\rptTranformItem.aspx")
        End If
        'If checkPagePermission("~\ReportForm\rptTranformItem.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If
        Page.Title = CompnyName & " - Transformation Item Report"
        Label2.Text = ""

        If Not Page.IsPostBack Then
            DDLFromWh() : DDLToWh("") : DDLUnit()
            range1.Text = Format(New Date(GetServerTime.Year, GetServerTime.Month, 1), "dd/MM/yyyy")
            range2.Text = Format(GetServerTime, "dd/MM/yyyy")
        Else
            If Session("diprint") = "True" Then
                showPrint("crv")
            End If
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not Report Is Nothing Then
                If Report.IsLoaded Then
                    Report.Dispose()
                    Report.Close()
                End If
            End If
        Catch ex As Exception
            Report.Dispose()
            Report.Close()
        End Try
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowprint.Click

        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period!"
            Exit Sub
        End If
        Session("diprint") = "True"
        showPrint("crv")
    End Sub

    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period!"
            Exit Sub
        End If
        showPrint("pdf")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period!"
            Exit Sub
        End If
        showPrint("xls")
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("rptTranformItem.aspx?awal=true")
    End Sub

    Protected Sub barangsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangsearch.Click
        BindListMat()
        gvMat.Visible = True
    End Sub

    Protected Sub barangerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangerase.Click
        barang.Text = "" : barangoid.Text = "" : merk.Text = "" : gvMat.Visible = False
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("")
    End Sub

    Protected Sub DDLbranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLbranch.SelectedIndexChanged
        If Page.IsPostBack Then
            If Session("branch_id") = "01" Then
                If DDLbranch.SelectedValue = "All" Then
                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 "

                    FillDDL(ddllocawal, sSql)
                    ddllocawal.Items.Add("All")
                    ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                    ddllocawal.SelectedValue = "All"

                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 "
                    FillDDL(ddllocakhir, sSql)
                    ddllocakhir.Items.Add("All")
                    ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                    ddllocakhir.SelectedValue = "All"
                Else
                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"

                    FillDDL(ddllocawal, sSql)
                    ddllocawal.Items.Add("All")
                    ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                    ddllocawal.SelectedValue = "All"

                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"
                    FillDDL(ddllocakhir, sSql)
                    ddllocakhir.Items.Add("All")
                    ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                    ddllocakhir.SelectedValue = "All"
                End If
            Else
                sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"

                FillDDL(ddllocawal, sSql)
                ddllocawal.Items.Add("All")
                ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                ddllocawal.SelectedValue = "All"

                sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"
                FillDDL(ddllocakhir, sSql)
                ddllocakhir.Items.Add("All")
                ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                ddllocakhir.SelectedValue = "All"
            End If
        End If
    End Sub

    Protected Sub ddllocawal_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddllocawal.SelectedIndexChanged
        If ddllocawal.SelectedItem.Text <> "ALL" Then
            DDLToWh("WHERE tr.transformfromwhoid = " & ddllocawal.SelectedValue & "")
        Else
            DDLToWh("")
        End If
    End Sub

    Protected Sub gvMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        barang.Text = gvMat.SelectedDataKey.Item("matcode").ToString
        barangoid.Text = gvMat.SelectedDataKey.Item("itemoid")
        gvMat.Visible = False
    End Sub

    Protected Sub SearchTransNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindTransNo()
    End Sub

    Protected Sub EraseTransNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        TransItemNo.Visible = False
        txtTransNo.Text = "" : lblTransOid.Text = ""
    End Sub

    Protected Sub TransItemNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtTransNo.Text = TransItemNo.SelectedDataKey.Item("transformno").ToString
        lblTransOid.Text = TransItemNo.SelectedDataKey.Item("transformoid").ToString
        TransItemNo.Visible = False
    End Sub

    Protected Sub TransItemNo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        TransItemNo.PageIndex = e.NewPageIndex
        BindTransNo()
    End Sub

    Protected Sub gvMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvMat.PageIndex = e.NewPageIndex
        BindListMat()
    End Sub
#End Region
End Class
