Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Master_Customer
    Inherits System.Web.UI.Page

#Region "Variable"
    Private Regex As Regex
    Dim oMatches As MatchCollection
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim dv As DataView
    Dim cfunction As New ClassFunction
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cKoneksi As New Koneksi
    Dim dtTemp As New DataTable
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String
    Dim sFax1 As String
    Dim sFax2 As String
    Dim sValue As String
    Private cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal message As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        validasi.Text = message
        lblCaption.Text = caption
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEError.Show()
    End Sub

    Private Sub fillFaxField(ByVal faxString As String, ByVal interCode As TextBox, ByVal localCode As TextBox, ByVal faxCode As TextBox)
        Dim sTemp() As String = faxString.Split(".")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                interCode.Text = sTemp(0)
            ElseIf i = 1 Then
                localCode.Text = sTemp(1)
            ElseIf i = 2 Then
                faxCode.Text = sTemp(2)
            End If
        Next
    End Sub

    Private Sub fillPhoneField(ByVal phoneString As String, ByVal interCode As TextBox, ByVal localCode As TextBox, ByVal phoneCode As TextBox)
        Dim sTemp() As String = phoneString.Split(".")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                interCode.Text = sTemp(0)
            ElseIf i = 1 Then
                localCode.Text = sTemp(1)
            ElseIf i = 2 Then
                phoneCode.Text = sTemp(2)
            End If
        Next
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fddlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fddlCabang, sSql)
            Else
                FillDDL(fddlCabang, sSql)
                fddlCabang.Items.Add(New ListItem("ALL", "ALL"))
                fddlCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fddlCabang, sSql)
            fddlCabang.Items.Add(New ListItem("ALL", "ALL"))
            fddlCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub initProvince(ByVal sCountry As String)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup IN ('PROVINCE') AND (genother1='" & sCountry & "') AND cmpcode like '%" & CompnyCode & "%'   ORDER BY gendesc"
        FillDDL(custprovoid, sSql)

        sSql = "SELECT genoid FROM QL_mstgen WHERE gengroup IN ('PROVINCE') AND (genother1='" & sCountry & "') AND cmpcode like '%" & CompnyCode & "%' and gendesc like 'BALI'  ORDER BY gendesc"
        custprovoid.SelectedValue = sSql
    End Sub

    Private Sub initCity(ByVal sProv As String, ByVal sCountry As String)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup IN ('CITY') AND (genother2='" & sProv & "' ) AND (genother1='" & sCountry & "') AND cmpcode LIKE '%" & CompnyCode & "%' ORDER BY gendesc"
        FillDDL(custcityoid, sSql)
    End Sub

    Private Sub initCountry()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('COUNTRY') AND cmpcode LIKE '%" & CompnyCode & "%' ORDER BY gendesc"
        FillDDL(custcountryoid, sSql)
    End Sub

    Private Sub initPrefix()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('PREFIXCOMPANY') AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(precustname, sSql)
    End Sub

    Private Sub initConTitle()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('PREFIXPERSON') AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(ddlconttitle, sSql)
        FillDDL(ddlconttitle2, sSql)
        FillDDL(ddlconttitle3, sSql)
    End Sub

    Private Sub initCurr()
        sSql = "SELECT currencyoid, currencycode FROM QL_mstcurr WHERE cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(custdefaultcurroid, sSql)
        FillDDL(custcreditlimitcurroid, sSql)
    End Sub

    Private Sub checkCityList()
        Try
            If custcityoid.Items.Count = 0 Then
                'cpcityphonecode.Text = "" 
                Exit Sub
            Else
                Try
                    initCityPhoneCode(custcityoid.SelectedValue)
                Catch ex As Exception
                    showMessage(ex.ToString, CompnyName & " - Error", 1)
                End Try
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Error", 1)
        End Try
    End Sub

    Private Sub initCityPhoneCode(ByVal ctoid As Char)
        'City Phone Code
        sSql = "SELECT ISNULL(genother3, '') FROM QL_mstgen WHERE (gengroup IN ('CITY')) AND genoid ='" & ctoid & "' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        'cpcityphonecode.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Private Sub initCountryPhoneCode(ByVal ctoid As Int32)
        'City Phone Code
        sSql = "SELECT ISNULL(genother3, ''), genoid, gengroup FROM QL_mstgen WHERE (gengroup IN ('COUNTRY')) AND genoid ='" & ctoid & "' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        cpcountryphonecode.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Private Sub Outlate()
        sSql = "SELECT gencode from QL_mstgen where gengroup = 'CABANG' and genoid = '" & branchID.SelectedValue & "'"
        KodeOutlate.Text = cKoneksi.ambilscalar(sSql)
    End Sub

    Private Sub ddlCabang()
        sSql = "SELECT gencode, gendesc from QL_mstgen where gengroup = 'CABANG' AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(branchID, sSql)
    End Sub

    Private Sub initTOP()
        'top
        sSql = "select genoid, gendesc from QL_mstgen where gengroup like 'PAYTYPE' AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(fddlTOP, sSql)
    End Sub

    Private Sub initDDL()
        initPrefix() : initConTitle() : initCurr()

        Dim swhere As String = ""
        swhere = " and gendesc like 'INDONESIA%'  "
        sSql = "SELECT genoid, gendesc gengroup FROM QL_mstgen WHERE gengroup IN ('COUNTRY') AND cmpcode LIKE '%" & CompnyCode & "%' " & swhere & " ORDER BY gendesc"
        FillDDL(custcountryoid, sSql)
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "'   and  personstatus in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC like '%SALES%') "
        FillDDL(spgOid, sSql)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE (gengroup IN ('PAYTYPE')) AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(custpaytermdefaultoid, sSql)

        'top
        sSql = "select genoid, gendesc from QL_mstgen where gengroup like 'PAYTYPE' AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(dd_timeofpayment, sSql)

        initCountryPhoneCode(custcountryoid.SelectedValue)
        checkCityList()

        sSql = "SELECT gencode from QL_mstgen where gengroup = 'CABANG' and genoid = '" & branchID.SelectedValue & "'"
        KodeOutlate.Text = cKoneksi.ambilscalar(sSql)
    End Sub

    Private Sub InitCoa()
        FillDDLAcctg(araccount, "VAR_AR", branchID.SelectedValue)
        FillDDLAcctg(returaccount, "VAR_RETJUAL", branchID.SelectedValue)
    End Sub

    Private Sub initCustGroup()
        If ddlcustgroup1.SelectedValue <> "ALL" Then
            sSql = "select custgroupoid,custgroupname from QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND custgroupflag='ACTIVE' ORDER BY custgroupname"
            FillDDL(ddlcustgroup1, sSql)
        End If
        ddlcustgroup1.Items.Add("ALL")
        ddlcustgroup1.SelectedValue = "ALL"
    End Sub

    Public Sub generateCustID()
        sSql = "SELECT (lastoid+1) FROM QL_mstoid WHERE tablename LIKE '%QL_mstcust%' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        custoid.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Public Sub clearItems()
        custcode.Text = "" : custname.Text = ""
        custaddr.Text = "" : custpostcode.Text = ""
        phone1.Text = "" : phone2.Text = "" : phone3.Text = ""
        custfax1.Text = "" : CustFax2.Text = ""
        custemail.Text = "" : custwebsite.Text = ""
        notes.Text = "" : custnpwp.Text = ""
        UpdUser.Text = Session("UserId")
        UpdTime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")
        precustname.SelectedIndex = 0
        custcityoid.SelectedIndex = 0
        custprovoid.SelectedIndex = 0
        custcountryoid.SelectedIndex = 0
        custflag.SelectedIndex = 0
        custpaytermdefaultoid.SelectedIndex = 0
        custdefaultcurroid.SelectedIndex = 0
        custcreditlimitcurroid.SelectedIndex = 0
        custcreditlimit.Text = ""
    End Sub

    Private Sub bindCust(ByVal sWhere As String)

        sSql = "SELECT custoid, custcode, custname, custaddr, b.gendesc AS city,cbg.gendesc cabang, ISNULL(cust.phone1, 0) AS phone, cust.custflag,custgroup, notes,cust.custcreditlimitrupiah creditlimit,cust.custcreditlimitusagerupiah creditusage /*,case when custcreditlimitusagerupiah>custcreditlimitrupiah then (custcreditlimitusagerupiah-custcreditlimitrupiah) * -1 else 0 end creditover*/,cust.pin_bb, cust.custcreditlimitrupiahtempo AS credittempo, (select g.gendesc from ql_mstgen g where g.genoid = cust.timeofpayment and g.gengroup = 'PAYTYPE') AS termin,(SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM ( select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate,CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=cust.custoid AND j.trnjualstatus='POST' AND branch_code=cust.branch_code) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)) creditover " & _
               "FROM QL_MSTCUST cust INNER JOIN QL_mstgen b ON b.genoid = cust.custcityoid " & _
               "LEFT JOIN QL_mstgen cbg on cbg.gencode = cust.branch_code and cbg.gengroup = 'cabang' WHERE cust.cmpcode LIKE '" & CompnyCode & "' and custgroup in ('GROSIR','PROJECT','','TOKO') " & sWhere & " ORDER BY cust.custname "
        Dim xTableItem1 As DataTable = cKoneksi.ambiltabel(sSql, "customer")
        GVmstcust.Visible = True
        GVmstcust.DataSource = xTableItem1
        GVmstcust.DataBind()
        GVmstcust.SelectedIndex = -1
        If cbfOverDue.Checked = True Then
            GVmstcust.Columns(14).Visible = True
        Else
            GVmstcust.Columns(14).Visible = False
        End If
    End Sub

    Private Sub fillTextBox(ByVal idPage As Integer)
        sSql = "SELECT c.createtime,custoid,branch_code ,custcode,timeofpayment ,custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3,custfax1, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, custcreditlimitrupiah, custcreditlimitusagerupiah, notes, createuser, upduser, updtime, prefixcmp, prefixcp1, prefixcp2, prefixcp3, custgroup, custbank, custpassportname, custpassportno, custbirthdate,salesoid,custacctgoid,coa_retur,pin_bb,custgroupoid, isnull(custcreditlimitrupiahawal,0) custcreditlimitrupiahawal, isnull(custcreditlimitrupiahtempo,0) custcreditlimitrupiahtempo, ISNULL((select TOP 1 custcreditlimittempo from ql_mstcustdtl cd where cd.custoid = c.custoid and cd.custdtlflag = 'Tempo' order by custdtloid DESC),c.custcreditlimitrupiahtempo) AS custcreditlimittempo, ISNULL((select TOP 1 custcreditlimitawal from ql_mstcustdtl cd where cd.custoid = c.custoid and cd.custdtlflag = 'Awal' order by custdtloid DESC),c.custcreditlimitrupiahawal) AS custcreditlimitawal, ISNULL((select TOP 1 custoverduestatus from ql_mstcustdtl cd where cd.custoid = c.custoid and cd.custdtlflag = 'Overdue' order by custdtloid DESC),c.custoverduestatus) AS custoverduestatusdiff, ISNULL((select TOP 1 custdtloid from ql_mstcustdtl cd where cd.custoid = c.custoid Order by custdtloid DESC),0) custdtloid, ISNULL((select TOP 1 custdtlstatus custdtlstatus from ql_mstcustdtl cd where cd.custoid = c.custoid  and custdtlflag = 'Tempo' Order by custdtloid DESC),'') AS custdtlstatus, ISNULL((select TOP 1 custdtlstatus custdtlstatus from ql_mstcustdtl cd where cd.custoid = c.custoid and custdtlflag = 'Over Due' Order by custdtloid DESC),'') AS custdtlstatusoverdue, isnull(custoverduestatus,'') AS custoverduestatus,(SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM ( select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate,CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=c.custoid AND j.trnjualstatus='POST' AND branch_code=c.branch_code) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)) custcreditlimitusagaeoverdue FROM QL_mstcust c WHERE cmpcode='" & CompnyCode & "' AND custoid=" & idPage & ""

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        Dim supname As String = "" : Dim contname As String = ""
        If xReader.HasRows Then
            While xReader.Read
                ddlCabang()
                branchID.SelectedValue = xReader("branch_code").ToString
                custoid.Text = xReader("custoid")
                custdtloid.Text = xReader("custdtloid")
                custdtlstatus.Text = xReader("custdtlstatus").ToString.Trim
                custdtlstatusoverdue.Text = xReader("custdtlstatusoverdue").ToString.Trim
                dd_timeofpayment.SelectedValue = xReader("timeofpayment")
                Session("creditlimit") = ToMaskEdit(ToDouble(xReader("custcreditlimitrupiah").ToString), 3)
                custcode.Text = xReader("custcode").ToString.Trim
                custflag.Text = xReader("custflag").ToString.Trim
                custaddr.Text = xReader("custaddr").ToString.Trim
                custcountryoid.SelectedValue = xReader("custcountryoid")
                custpostcode.Text = xReader("custpostcode").ToString.Trim
                phone1.Text = xReader("phone1").ToString
                phone2.Text = xReader("phone2").ToString
                phone3.Text = xReader("phone3").ToString
                custfax1.Text = xReader("custfax1").ToString
                lastsalesdate.Text = GetStrData("select top 1 trnjualdate from QL_trnjualmst where trnjualstatus = 'POST' and trncustoid='" & custoid.Text & "' order by trnjualdate desc")
                If lastsalesdate.Text = "?" Then
                    lastsalesdate.Text = "-"
                End If
                txtage.Text = GetStrData("select top 1 DATEDIFF(d, trnjualdate , GETDATE()) [umur sales]  from QL_trnjualmst where trnjualstatus = 'POST' and trncustoid='" & custoid.Text & "' order by trnjualdate desc")
                If txtage.Text = "?" Then
                    txtage.Text = "-"
                End If
                custemail.Text = xReader("custemail").ToString.Trim
                custwebsite.Text = xReader("custwebsite").ToString.Trim
                custnpwp.Text = xReader("custnpwp").ToString.Trim
                contactperson1.Text = xReader("contactperson1").ToString.Trim
                contactperson2.Text = xReader("contactperson2").ToString.Trim
                contactperson3.Text = xReader("contactperson3").ToString.Trim
                phonecontactperson1.Text = xReader("phonecontactperson1").ToString.Trim
                phonecontactperson2.Text = xReader("phonecontactperson2").ToString.Trim
                phonecontactperson3.Text = xReader("phonecontactperson3").ToString.Trim
                notes.Text = Trim(xReader("notes")).ToString
                UpdUser.Text = xReader("upduser").ToString.Trim
                UpdTime.Text = Format(xReader("updtime"), "dd/MM/yyyy HH:mm:ss")
                TglGabung.Text = Format(xReader("createtime"), "dd/MM/yyyy")
                initPrefix()
                precustname.SelectedValue = xReader("PREFIXCMP").ToString
                custname.Text = xReader("custname").ToString '.Trim.Replace("," & precustname.SelectedItem.Text, "")
                ddlcustgroup1.SelectedValue = xReader("custgroup")
                InitCoa()
                If xReader("custgroupoid").ToString.Trim = 0 Then
                    ddlcustgroup1.SelectedValue = "ALL"
                Else
                    ddlcustgroup1.SelectedValue = xReader("custgroupoid").ToString.Trim
                End If
                custprovoid.Items.Clear()
                initProvince(custcountryoid.SelectedValue)
                custprovoid.SelectedValue = xReader("custprovoid")
                custcityoid.Items.Clear()
                initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)

                custcityoid.SelectedValue = xReader("custcityoid")
                custcreditlimitrupiah.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitrupiah").ToString), 3)
                custcreditlimitusagerupiah.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitusagerupiah").ToString), 3)
                custcreditlimitusageoverdue.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitusagaeoverdue").ToString), 3)
                custcreditlimitdiff.Text = ToMaskEdit(ToDouble(xReader("custcreditlimittempo").ToString), 3)
                custcreditlimitawaldiff.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitawal").ToString), 3)
                ddlconttitle.SelectedValue = xReader("prefixcp1").ToString
                ddlconttitle2.SelectedValue = xReader("prefixcp2").ToString
                ddlconttitle3.SelectedValue = xReader("prefixcp3").ToString
                branchID.SelectedValue = xReader("branch_code").ToString
                namapaspor.Text = xReader("custpassportname").ToString
                nopaspor.Text = xReader("custpassportno").ToString
                bankrekening.Text = xReader("custbank").ToString
                spgOid.SelectedValue = xReader("salesoid").ToString

                If xReader("custbirthdate") <> "1/1/1900" Then
                    tgllahir.Text = Format(xReader("custbirthdate"), "dd/MM/yyyy")
                End If

                If dd_timeofpayment.SelectedValue = 34 Then
                    custcreditlimitrupiah.Enabled = False
                    custcreditlimitrupiah.CssClass = "inpTextDisabled"
                End If
                InitCoa()
                araccount.SelectedValue = xReader("custacctgoid").ToString
                returaccount.SelectedValue = xReader("coa_retur").ToString
                pin_bb.Text = xReader("pin_bb").ToString
                custcreditlimitrupiahawal.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitrupiahawal").ToString), 3)
                custcreditlimitrupiahtempo.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitrupiahtempo").ToString), 3)
                custcreditlimitrupiahsisa.Text = ToMaskEdit(ToDouble(custcreditlimitrupiah.Text), 3) - ToMaskEdit(ToDouble(custcreditlimitusagerupiah.Text), 3)
                custcreditlimitrupiahsisa.Text = ToMaskEdit(ToDouble(custcreditlimitrupiahsisa.Text), 3)
                cbOverdue.Checked = IIf(xReader("custoverduestatus").ToString = "Y", True, False)
                cbOverduediff.Checked = IIf(xReader("custoverduestatusdiff").ToString = "Y", True, False)
                dd_timeofpayment_SelectedIndexChanged(Nothing, Nothing)
            End While
        End If
        conn.Close()
        sSql = "SELECT /*CONVERT(varchar, c.createtime,103)*/ c.createtime, c.custcreditlimit, c.custcreditlimitawal, c.custcreditlimittempo, c.custdtlstatus AS status, c.custdtlflag, c.updtime from ql_mstcustdtl c WHERE c.custoid=" & custoid.Text & " Order by c.createtime DESC"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet

        mySqlDA.Fill(objDs, "ql_mstcustdtl")
        gvCustdtl.Visible = True

        Dim dv As DataView = objDs.Tables("ql_mstcustdtl").DefaultView
        gvCustdtl.DataSource = objDs.Tables("ql_mstcustdtl")
        gvCustdtl.DataBind()
        Session("custdtl") = objDs.Tables("ql_mstcustdtl")
        conn.Close()
        custname.Enabled = False : custname.CssClass = "inpTextDisabled"
        branchID.Enabled = False : branchID.CssClass = "inpTextDisabled"
        sSql = "SELECT CASE WHEN ISNULL(SUM(late),0.0) = 0 THEN 0 WHEN ISNULL(COUNT(totalnota),0.0) = 0 THEN 0 ELSE ISNULL(SUM(late),0.0) / ISNULL(COUNT(totalnota),0.0) END FROM (SELECT case when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) = 0 AND (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC) < payduedate then 0 when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) <> 0 then (case when DATEDIFF(d,payduedate, CURRENT_TIMESTAMP) < 0 then 0 else DATEDIFF(d,payduedate, CURRENT_TIMESTAMP) end) when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) = 0 AND (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC) > payduedate then DATEDIFF(d,payduedate, (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC)) else 0 end as late, trnjualmstoid totalnota FROM QL_conar con INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid=con.refoid and jm.branch_code = con.branch_code AND jm.trncustoid=con.custoid AND jm.branch_code=con.branch_code WHERE reftype ='ql_trnjualmst' AND con.trnartype IN ('AR','piutang') and jm.trncustoid = " & idPage & " ) gg"
        averagelate.Text = GetStrData(sSql)
        If averagelate.Text = "?" Then
            averagelate.Text = 0.0
        End If
    End Sub

    Private Sub CreditLimit(ByVal idPage As Integer)
        sSql = "SELECT timeofpayment FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid=" & idPage & ""

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        Dim supname As String = ""
        Dim contname As String = ""
        If xReader.HasRows Then
            While xReader.Read
                dd_timeofpayment.SelectedValue = xReader("timeofpayment")
            End While
        End If

    End Sub

    Private Sub DataBindTrn()
        sSql = "SELECT trnjualno, CONVERT(varchar,trnjualdate,103) AS trnjualdate, CONVERT(varchar,payduedate,103) AS payduedate,ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) trnamtjualnetto FROM ( select j.trnjualmstoid, j.trnjualno,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate, CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=" & custoid.Text & " AND j.trnjualstatus='POST' ) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr) group by trnjualno,trnjualdate, payduedate"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstmat")
        Session("TblListMat") = dt
        gvListNya.DataSource = Session("TblListMat")
        gvListNya.DataBind() : gvListNya.Visible = True
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub
#End Region

#Region "Function"
    Private Function generateCustCode(ByVal custname As String) As String
        Dim retVal As String = ""
        Dim custPrefix As String = custname.Substring(0, 1) ' 1 karakter nama CUSTOMER
        sSql = "select custcode from QL_mstcust where custcode like '" & custPrefix & "%' order by custcode desc "
        Dim x As Object = cKoneksi.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = UCase(custPrefix) & "00001"
        Else
            If x = "" Then
                ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
                retVal = UCase(custPrefix) & "00001"
            Else
                ' kode supplier seperti yg diminta ada, tinggal generate angka
                Dim angka As Integer = CInt(x.ToString.Substring(1, 5))
                angka += 1
                retVal = UCase(custPrefix) & tambahNol(angka)
            End If
        End If
        Return retVal
    End Function


    Function tambahNol(ByVal iAngka As Integer)
        sValue = "0000"
        If iAngka >= 10 Then : sValue = "000" : End If
        If iAngka >= 100 Then : sValue = "00" : End If
        If iAngka >= 1000 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 10000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sMsg As String = ""
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            sMsg &= "- Session login anda habis !<br>"
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Session("consignee") = Nothing
            Dim userId As String = Session("UserID")
            Dim xSetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xSetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xSetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xSetRole
            Session("tblSA") = Nothing
            Session("tblCP") = Nothing
            Session("tblBA") = Nothing
            TabContainer1.ActiveTabIndex = 0
            Response.Redirect("Master_Customer_Create.aspx")
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        Page.Title = CompnyName & " - Data Customer Pusat"
        Session("idPage") = Request.QueryString("idPage")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Apakah Anda yakin ingin menghapus data ini ?');")

        If Not IsPostBack Then
            ddlCabang() : initDDL() : InitDDLBranch()
            initCustGroup() : bindCust("") : initTOP()
            ' Update CL Tempo menjadi NOL ketika melebihi 24 jam Konversi ke detik
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "Update ql_mstcust set custcreditlimitrupiahtempo = 0,custcreditlimit=custcreditlimitrupiahawal,custcreditlimitrupiah=custcreditlimitrupiahawal, updtime = CURRENT_TIMESTAMP where custoid in (select custoid from ql_mstcustdtl where custdtlstatus = 'Open' and custdtlflag = 'Tempo' and DATEDIFF(S, createtime, CURRENT_TIMESTAMP) > 86400)"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            sSql = "Update ql_mstcust set custoverduestatus = 'N', updtime = CURRENT_TIMESTAMP where custoid in (select custoid from ql_mstcustdtl where custdtlstatus = 'Enabled' and custdtlflag = 'Over Due' and DATEDIFF(S, createtime, CURRENT_TIMESTAMP) > 86400 /*(select (genother1*3600) from ql_mstgen where gengroup = 'CREDITLIMITTYPE' and gendesc = 'Over Due')*/)"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            sSql = "Update ql_mstcustdtl set custdtlstatus = 'Expired', updtime = CURRENT_TIMESTAMP where custdtloid in (select custdtloid from ql_mstcustdtl where custdtlstatus = 'Open' and custdtlflag = 'Tempo' and DATEDIFF(S, createtime, CURRENT_TIMESTAMP) > 86400)"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            sSql = "Update ql_mstcustdtl set custdtlstatus = 'Expired', updtime = CURRENT_TIMESTAMP where custdtloid in (select custdtloid from ql_mstcustdtl where custdtlstatus = 'Enabled' and custdtlflag = 'Over Due' and DATEDIFF(S, createtime, CURRENT_TIMESTAMP) > 86400 /*(select (genother1*3600) from ql_mstgen where gengroup = 'CREDITLIMITTYPE' and gendesc = 'Over Due')*/)"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'sSql = "select (select MAX(custdtloid) from QL_mstcustdtl) AS custdtloid, custoid, custcreditlimitrupiahtempo, custcreditlimitrupiah, custcreditlimit,custcreditlimitrupiahawal, ISNULL((select TOP 1 custoverduestatus from QL_mstcustdtl cd where cd.custoid = c.custoid and cd.custdtlflag = 'Over Due' order by custdtloid), 'N') AS custoverduestatus from QL_mstcust c where custoid NOT IN (select trncustoid from QL_trnjualmst where trnjualdate >= DATEADD(D,-60,CURRENT_TIMESTAMP) and trnjualdate <= CURRENT_TIMESTAMP and trnjualmstoid > 0 group by trncustoid) and (custcreditlimitrupiahawal + custcreditlimitrupiahtempo) = custcreditlimitrupiah and (custcreditlimitrupiahawal + custcreditlimitrupiahtempo) = custcreditlimit and custcreditlimitrupiah > 0 and custcreditlimit > 0"
            'Session("custdtl") = cKoneksi.ambiltabel(sSql, "ql_mstcust")
            'Dim objTable As DataTable : objTable = Session("custdtl")
            'Dim custoiddtl As Integer = GenerateID("QL_mstcustdtl", CompnyCode)
            'For C1 As Int16 = 0 To objTable.Rows.Count - 1
            '    sSql = "Update ql_mstcust set custcreditlimitrupiah = 0, custcreditlimit = 0 where custoid = " & objTable.Rows(C1)("custoid") & ""
            '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '    sSql = "INSERT INTO QL_mstcustdtl (custdtloid, custoid, custcreditlimit, custcreditlimittempo, custcreditlimitawal, createuser, createtime, custdtlstatus, custdtlflag, custoverduestatus) VALUES (" & custoiddtl & ", " & objTable.Rows(C1)("custoid") & ", 0, " & ToDouble(objTable.Rows(C1)("custcreditlimitrupiahtempo")) & ", " & ToDouble(objTable.Rows(C1)("custcreditlimitrupiahawal")) & ", 'Auto CL', current_timestamp, 'Closed', 'Credit Limit', '" & objTable.Rows(C1)("custoverduestatus").ToString & "')"
            '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    custoiddtl += 1
            'Next
            'sSql = "update QL_mstoid set lastoid=" & custoiddtl - 1 & " where tablename like 'QL_mstcustdtl' and cmpcode = '" & CompnyCode & "' "
            'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            conn.Close()
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                initProvince(custcountryoid.SelectedValue)
                InitCoa()
                initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
                initCountryPhoneCode(custcountryoid.SelectedValue)
                initCityPhoneCode(custcityoid.SelectedValue)
                I_U.Text = "New"
                TglGabung.Text = Format(GetServerTime(), "dd/MM/yyyy")
                UpdTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                UpdUser.Text = Session("UserID")
                lblLast.Visible = False : TabContainer1.ActiveTabIndex = 0
                btnDelete.Visible = False
                ddlcustgroup1_SelectedIndexChanged(Nothing, Nothing)
                btnInfoNota.Visible = False
            Else
                initCountry()
                I_U.Text = "UPDATE" : lblCreate.Visible = False
                With SDSData
                    .SelectParameters("custoid").DefaultValue = Session.Item("idPage")
                    .SelectParameters("cmpcode").DefaultValue = CompnyCode
                End With
                fillTextBox(Session("idPage"))
                TabContainer1.ActiveTabIndex = 1
                btnDelete.Visible = True
                btnInfoNota.Visible = True 
            End If
        End If
    End Sub

    Protected Sub GVmstcust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstcust.PageIndexChanging
        GVmstcust.PageIndex = e.NewPageIndex
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If cbStatus.Checked = True Then
            sWhere &= " AND cust.custflag='" & DDLStatus.SelectedValue & "'"
        End If
        If fddlCabang.SelectedValue <> "ALL" Then
            sWhere &= " AND cust.branch_code = '" & fddlCabang.SelectedValue & "'"
        End If
        If cbCreditTempo.Checked = True Then
            sWhere &= " AND cust.custcreditlimitrupiahtempo " & ddlCreditTempo.SelectedValue & ""
        End If
        If cbfOverDue.Checked = True Then
            sWhere &= " AND cust.custoverduestatus ='" & ddlOverDue.SelectedValue & "'"
        End If
        If chkCreatedByBranch.Checked Then
            sWhere &= " AND cust.createuser NOT IN ('','admin','adminql') "
        End If
        If cbTop.Checked Then
            sWhere &= " AND cust.timeofpayment = " & fddlTOP.SelectedValue & " "
        End If
        bindCust(sWhere)
    End Sub

    Protected Sub custcountryoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcountryoid.SelectedIndexChanged
        initProvince(custcountryoid.SelectedValue)
        initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
        initCountryPhoneCode(custcountryoid.SelectedValue)
        If custcityoid.Items.Count <> 0 Then
            initCityPhoneCode(custcityoid.SelectedValue)
        Else
            'cpcityphonecode.Text = ""
        End If
    End Sub

    Protected Sub custprovoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custprovoid.SelectedIndexChanged
        initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
        If custcityoid.Items.Count <> 0 Then
            initCityPhoneCode(custcityoid.SelectedValue)
        Else
            'cpcityphonecode.Text = ""
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        Dim sWhere As String = ""
        Dim sTgl As String = Format(GetServerTime, "MM/dd/yyyy")
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If fddlCabang.SelectedValue <> "ALL" Then
            sWhere &= " AND cust.branch_code = '" & fddlCabang.SelectedValue & "'"
        End If
        If cbCreditTempo.Checked = True Then
            sWhere &= " AND cust.custcreditlimitrupiahtempo " & ddlCreditTempo.SelectedValue & ""
        End If
        If cbStatus.Checked = True Then
            sWhere &= " AND cust.custflag='" & DDLStatus.SelectedValue & "'"
        End If
        If cbCreditAwal.Checked = True Then
            sWhere &= " AND cust.custoid in (SELECT custoid FROM QL_mstcustdtl where custdtlflag = 'Awal' and createtime >= '" & sTgl & " 00:00:00' and createtime <= '" & sTgl & " 23:59:59')"
        End If
        If cbfOverDue.Checked = True Then
            sWhere &= " AND cust.custoverduestatus ='" & ddlOverDue.SelectedValue & "'"
        End If
        If chkCreatedByBranch.Checked Then
            sWhere &= " AND cust.createuser NOT IN ('','admin','adminql') "
        End If
        If cbTop.Checked Then
            sWhere &= " AND cust.timeofpayment = " & fddlTOP.SelectedValue & " "
        End If
        bindCust(sWhere)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        txtFilter.Text = "" : cbStatus.Checked = False
        fddlCabang.SelectedValue = "ALL" : cbCreditTempo.Checked = False
        ddlFilter.SelectedIndex = 0 : DDLStatus.SelectedIndex = 0
        cbfOverDue.Checked = False : cbTop.Checked = False
        bindCust("")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click 
        I_U.Text = "NEW"
        btnDelete.Visible = False : TabContainer1.ActiveTabIndex = 0
        Response.Redirect("~\master\Master_Customer_Create.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If custcode.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Kode Customer !<br>"
        End If

        If precustname.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan pilih Prefix Customer !<br>"
        End If

        If custname.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Nama Customer !<br>"
        End If

        If custname.Text.Length > 100 Then
            sMsg &= "Nama Customer Max. 100 karakter !<br>"
        End If
        'If custaddr.Text = "" Then
        '    sMsg &= "- Silahkan isi Alamat Customer !<br>"
        'End If
        If ddlconttitle.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 1 !<br>"
        End If

        If ddlconttitle2.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 2 !<br>"
        End If

        If ddlconttitle3.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 3 !<br>"
        End If

        If contactperson1.Text = "" Then
            sMsg &= "Silahkan isi Nama Pemilik"
        End If

        If custcountryoid.SelectedValue = "" Then
            sMsg &= "- Silahkan pilih nama Negara !<br>"
        End If

        If custprovoid.Items.Count = 0 Then
            sMsg &= "- Silahkan buat Kota dan Provinsi untuk Negara " & custcountryoid.SelectedItem.Text & "!<br>"
        End If

        If custprovoid.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan pilih nama Provinsi !<br>"
        End If

        If custcityoid.SelectedValue = "" Then
            sMsg &= "- Silahkan pilih nama Kota !<br>"
        End If

        If custcityoid.Items.Count = 0 Then
            sMsg &= "- Silahkan buat Kota untuk Provinsi " & custprovoid.SelectedItem.Text & " !<br>"
        End If

        If phone1.Text = "" Then
            sMsg &= "- Silahkan isi Telepon Rumah !<br>"
        End If

        If phone2.Text = "" Then
            sMsg &= "- Silahkan isi Telepon Kantor !<br>"
        End If

        If phone3.Text = "" Then
            sMsg &= "- Silahkan isi Telepon HP / WA !<br>"
        End If
        If custemail.Text <> "" Then
            oMatches = System.Text.RegularExpressions.Regex.Matches(custemail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                sMsg &= "- Alamat email tidak valid, Ex : mail@sample.com !!<BR>"
            End If
        End If
        If spgOid.SelectedValue = "" Then
            sMsg &= "Sales Person tidak boleh kosong !!<BR>"
        End If

        If custwebsite.Text <> "" Then
            oMatches = System.Text.RegularExpressions.Regex.Matches(custwebsite.Text, "\w\w\w\.([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?")
            If oMatches.Count <= 0 Then
                sMsg &= "- Alamat website tidak valid, Ex : www.sample.com !!<BR>"
            End If
        End If

        If notes.Text.Trim.Length > 500 Then
            sMsg &= "- Catatan tidak boleh lebih dari 500 karakter !<br>"
        End If

        Dim datebirth As New Date
        If tgllahir.Text.Trim <> "" Then
            If Date.TryParseExact(tgllahir.Text, "dd/MM/yyyy", Nothing, Nothing, datebirth) = False Then
                sMsg &= "- Tanggal lahir customer tidak valid !<br>"
            End If
        End If

        If ((ToDouble(custcreditlimitusagerupiah.Text)) > ToDouble(custcreditlimitrupiah.Text)) And dd_timeofpayment.SelectedValue <> 34 Then
            sMsg &= "Credit Limit should be greather than credit usage "
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        ' Cek Nama - Alamat dan Kota yang sudah ada
        Dim sQuery As String = "select count(-1) from ql_mstcust where cmpcode='" & CompnyCode & "' " & _
            "AND custname= '" & Tchar(custname.Text) & "' and custaddr='" & Tchar(custaddr.Text) & "' " & _
            "and custcityoid=" & custcityoid.SelectedValue & " AND branch_code='" & branchID.SelectedValue & "' "
        If I_U.Text.ToLower <> "new" Then
            sQuery &= " and custoid <> " & Session("idPage")
        End If

        Dim iCount As Integer = ToDouble(GetStrData(sQuery))
        If iCount > 0 Then
            showMessage("Data Customer ini sudah ada :<BR>Cabang =" & branchID.SelectedItem.Text & "<BR>Nama = " & custname.Text & "<BR>Alamat =" & custaddr.Text & "<BR>Kota =" & custcityoid.SelectedItem.Text, CompnyName & "- Warning", 2)
            Exit Sub
        End If

        Dim tittlename As String = ""
        If precustname.SelectedItem.Text.ToLower = "other" Then
            tittlename = ""
        Else
            tittlename = "," & precustname.SelectedItem.Text
        End If

        Dim CustGroup As String = ""
        If ddlcustgroup1.SelectedValue = "ALL" Then
            CustGroup = 0
        Else
            CustGroup = ddlcustgroup1.SelectedValue
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
             
            custdtloid.Text = GenerateID("QL_mstcustdtl", CompnyCode)

            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                'New Insert
                custoid.Text = GenerateID("QL_mstcust", CompnyCode)
                sSql = "INSERT INTO QL_mstcust (cmpcode, custoid,branch_code,timeofpayment, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, statusAR, custpaytermdefaultoid, custcurrdefaultoid, custcreditlimitcurroid, custcreditlimit, custcreditlimitusage, custcreditlimitrupiah, custcreditlimitusagerupiah, custacctgoid, notes, resfield1, resfield2, createuser, upduser, updtime, cdphonecountry, cdphonecity, prefixcmp, prefixcp1, prefixcp2, prefixcp3, CustGroup, custbank, custpassportname, custpassportno, custbirthdate,salesoid,coa_retur,pin_bb,custgroupoid, custcreditlimitrupiahawal, custcreditlimitrupiahtempo, custoverduestatus,createtime) " & _
                "VALUES ('" & CompnyCode & "', " & Tchar(custoid.Text) & ",'" & branchID.SelectedValue & "' ," & dd_timeofpayment.SelectedValue & ",'" & Tchar(custcode.Text) & "', '" & Tchar(custname.Text) & "', '" & custflag.SelectedValue & "', '" & Tchar(custaddr.Text) & "', " & custcityoid.SelectedValue & ", " & custprovoid.SelectedValue & ", " & custcountryoid.SelectedValue & ", '" & custpostcode.Text & "', '" & phone1.Text & "', '" & phone2.Text & "', '" & phone3.Text & "', '" & custfax1.Text & "', '', '" & Tchar(custemail.Text) & "', '" & Tchar(custwebsite.Text) & "', '" & Tchar(custnpwp.Text) & "', '" & Tchar(contactperson1.Text) & "', '" & Tchar(contactperson2.Text) & "', '" & Tchar(contactperson3.Text) & "', '" & phonecontactperson1.Text & "', '" & phonecontactperson2.Text & "', '" & phonecontactperson3.Text & "', '" & statusar.SelectedValue & "', 0, 0, 0, " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitusage.Text) & ", " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitusage.Text) & ", " & araccount.SelectedValue & ", '" & Tchar(notes.Text) & "', '', '', '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp, '', '', " & precustname.SelectedValue & ", " & ddlconttitle.SelectedValue & ", " & ddlconttitle2.SelectedValue & ", " & ddlconttitle3.SelectedValue & ", '" & ddlcustgroup.SelectedValue.ToString & "', '" & Tchar(bankrekening.Text) & "', '" & Tchar(namapaspor.Text) & "', '" & Tchar(nopaspor.Text) & "', '" & datebirth & "'," & spgOid.SelectedValue & "," & returaccount.SelectedValue & ",'" & pin_bb.Text & "','" & CustGroup & "', " & ToDouble(custcreditlimitrupiahawal.Text) & ", " & ToDouble(custcreditlimitrupiahtempo.Text) & ", " & IIf(cbOverdue.Checked = True, "Y", "N") & ",'" & CDate(toDate(TglGabung.Text)) & "')"
            Else
                sSql = "UPDATE QL_mstcust set branch_code='" & branchID.SelectedValue & "',timeofpayment=" & dd_timeofpayment.SelectedValue & ",custcode = '" & Tchar(custcode.Text) & "', custname = '" & Tchar(custname.Text) & "', custflag = '" & custflag.SelectedValue & "', custaddr = '" & Tchar(custaddr.Text) & "', custcityoid = " & custcityoid.SelectedValue & ", custprovoid = " & custprovoid.SelectedValue & ", custcountryoid = " & custcountryoid.SelectedValue & ", custpostcode = '" & custpostcode.Text & "', phone1 = '" & phone1.Text & "', phone2 = '" & phone2.Text & "', phone3 = '" & phone3.Text & "',custfax1 = '" & custfax1.Text & "', custemail = '" & Tchar(custemail.Text) & "', custwebsite = '" & Tchar(custwebsite.Text) & "', custnpwp = '" & Tchar(custnpwp.Text) & "', contactperson1 = '" & Tchar(contactperson1.Text) & "', contactperson2 = '" & Tchar(contactperson2.Text) & "', contactperson3 = '" & Tchar(contactperson3.Text) & "', phonecontactperson1 = '" & phonecontactperson1.Text & "', phonecontactperson2 = '" & phonecontactperson2.Text & "', phonecontactperson3 = '" & phonecontactperson3.Text & "', custcreditlimit = " & ToDouble(custcreditlimitrupiah.Text) & ", custcreditlimitusage = " & ToDouble(custcreditlimitusagerupiah.Text) & ", custcreditlimitrupiah = " & ToDouble(custcreditlimitrupiah.Text) & ", custcreditlimitusagerupiah = " & ToDouble(custcreditlimitusagerupiah.Text) & ", notes = '" & Tchar(notes.Text) & "', upduser = '" & Session("UserID") & "', updtime = current_timestamp, prefixcmp = " & precustname.SelectedValue & ", prefixcp1 = " & ddlconttitle.SelectedValue & ", prefixcp2 = " & ddlconttitle2.SelectedValue & ", prefixcp3 = " & ddlconttitle3.SelectedValue & ", CustGroup = '" & ddlcustgroup.SelectedValue.ToString & "', custbank = '" & Tchar(bankrekening.Text) & "', custpassportname = '" & Tchar(namapaspor.Text) & "', custpassportno = '" & Tchar(nopaspor.Text) & "', custbirthdate = '" & datebirth & "',salesoid = " & spgOid.SelectedValue & ",custacctgoid=" & araccount.SelectedValue & ",coa_retur=" & returaccount.SelectedValue & " ,pin_bb='" & pin_bb.Text & "',custgroupoid='" & CustGroup & "', custcreditlimitrupiahawal = " & ToDouble(custcreditlimitrupiahawal.Text) & ", custcreditlimitrupiahtempo = " & ToDouble(custcreditlimitrupiahtempo.Text) & ", custoverduestatus = '" & IIf(cbOverdue.Checked = True, "Y", "N") & "',createtime='" & CDate(toDate(TglGabung.Text)) & "' WHERE cmpcode='" & CompnyCode & "' AND custoid=" & Session("idPage") & ""
            End If
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                sSql = "INSERT INTO QL_mstcustdtl (custdtloid, custoid, custcreditlimit, custcreditlimittempo, custcreditlimitawal, createuser, createtime, custdtlstatus, custoverduestatus) VALUES (" & custdtloid.Text & ", " & custoid.Text & ", " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitrupiahtempo.Text) & ", " & ToDouble(custcreditlimitrupiahawal.Text) & ", '" & Session("UserID") & "', current_timestamp, 'Open', '" & IIf(cbOverdue.Checked = True, "Y", "N") & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                custdtloid.Text += 1
            Else
                'OVER DUE
                If cbOverdue.Checked <> cbOverduediff.Checked Then
                    If custdtlstatusoverdue.Text <> "Expired" Then
                        sSql = "UPDATE QL_mstcustdtl set custdtlstatus = 'False', custoverduestatus = 'N' where custoid = " & custoid.Text & " and custdtlflag = 'Over Due' and custdtlstatus = 'Enabled'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                    If custdtlstatusoverdue.Text <> "Enabled" Then
                        sSql = "INSERT INTO QL_mstcustdtl (custdtloid, custoid, custcreditlimit, custcreditlimittempo, custcreditlimitawal, createuser, createtime, custdtlstatus, custdtlflag, custoverduestatus) VALUES (" & custdtloid.Text & ", " & custoid.Text & ", " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitrupiahtempo.Text) & ", " & ToDouble(custcreditlimitrupiahawal.Text) & ", '" & Session("UserID") & "', current_timestamp, 'Enabled', 'Over Due', '" & IIf(cbOverdue.Checked = True, "Y", "N") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        custdtloid.Text += 1
                    End If
                End If
                'CRD LIMIT AWAL
                If ToDouble(custcreditlimitawaldiff.Text) <> ToDouble(custcreditlimitrupiahawal.Text) Then
                    sSql = "INSERT INTO QL_mstcustdtl (custdtloid, custoid, custcreditlimit, custcreditlimittempo, custcreditlimitawal, createuser, createtime, custdtlstatus, custdtlflag, custoverduestatus) VALUES (" & custdtloid.Text & ", " & custoid.Text & ", " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitrupiahtempo.Text) & ", " & ToDouble(custcreditlimitrupiahawal.Text) & ", '" & Session("UserID") & "', current_timestamp, 'Closed', 'Awal','" & IIf(cbOverdue.Checked = True, "Y", "N") & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    custdtloid.Text += 1
                End If
                'CRD LIMIT TEMPO
                If ToDouble(custcreditlimitdiff.Text) <> ToDouble(custcreditlimitrupiahtempo.Text) Then
                    If custdtlstatus.Text <> "Expired" Then
                        sSql = "UPDATE QL_mstcustdtl set custdtlstatus = 'Ignore' where custoid = " & custoid.Text & " and custdtlflag = 'Tempo' and custdtlstatus = 'Open'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                    sSql = "INSERT INTO QL_mstcustdtl (custdtloid, custoid, custcreditlimit, custcreditlimittempo, custcreditlimitawal, createuser, createtime, custdtlstatus, custdtlflag, custoverduestatus) VALUES (" & custdtloid.Text & ", " & custoid.Text & ", " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitrupiahtempo.Text) & ", " & ToDouble(custcreditlimitrupiahawal.Text) & ", '" & Session("UserID") & "', current_timestamp, 'Open','Tempo','" & IIf(cbOverdue.Checked = True, "Y", "N") & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    custdtloid.Text += 1
                End If
            End If

            Dim groupoid As Integer
            sSql = "SELECT ISNULL(custgroupoid, 0) FROM ql_mstcust WHERE custoid = " & custoid.Text & " AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : groupoid = xCmd.ExecuteScalar()
            If groupoid <> 0 Then
                sSql = "update ql_mstcustgroup set custgroupcreditlimitrupiah = " & ToDouble(custcreditlimitrupiah.Text) & " where custgroupoid = " & groupoid & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If Session("idPage") = Nothing Or Session("idPage") = "" Then
                sSql = "update QL_mstoid set lastoid=" & custoid.Text & " where tablename like 'QL_mstcust' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
 
            sSql = "update QL_mstoid set lastoid=" & custdtloid.Text - 1 & " where tablename like 'QL_mstcustdtl' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    End If
            'End If

            If Not Session("idPage") Is Nothing And Session("idPage") <> "" Then
                sSql = "DELETE FROM QL_mstcustcons WHERE custoid = " & Session("idPage") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close() 
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - Error", 1)
            Exit Sub
        End Try
        Session("idPage") = Nothing : Session("consignee") = Nothing
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data tersimpan"))
        Response.Redirect("~\master\Master_Customer_Create.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim sColomnName() As String = {"REQCUSTOID", "trncustoid", "trncustoid"}
        Dim sTable() As String = {"QL_TRNREQUEST", "QL_trnordermst", "QL_trnjualmst"}
        If CheckDataExists(custoid.Text, sColomnName, sTable) = True Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain !", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstcust WHERE cmpcode = '" & CompnyCode & "' AND custoid=" & Session("idPage") & " "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE QL_mstcustdtl WHERE custoid=" & Session("idPage") & " "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE QL_mstcustcons WHERE cmpcode = '" & CompnyCode & "' AND custoid=" & Session("idPage") & " "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close() 
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, CompnyName & " - Error", 1)
            Exit Sub
        End Try
        Response.Redirect("~\master\Master_Customer_Create.aspx?awal=true")
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        btnExtender.Visible = False
        PanelErrMsg.Visible = False 
    End Sub

    Protected Sub custcityoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcityoid.SelectedIndexChanged
        initCityPhoneCode(custcityoid.SelectedValue)
    End Sub

    Protected Sub custname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custname.TextChanged
        'If Session("idPage") <> "" And Session("idPage") <> Nothing Then
        'Else
        'HANYA NEW YANG GENERATE CODE
        If custname.Text.Trim <> "" Then
            If custname.Text.Length < 1 Then
                showMessage("Nama Customer Minimal 1 Character", CompnyName & " - WARNING", 2)
                custname.Text = "" : custcode.Text = ""
                Exit Sub
            Else
                custcode.Text = KodeOutlate.Text & generateCustCode(Tchar(custname.Text))
            End If
        Else
            custcode.Text = ""
        End If
        'End If
    End Sub

    Protected Sub DropBrunch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles branchID.TextChanged
        Outlate()
        If custname.Text <> "" Then
            If Session("idPage") <> "" And Session("idPage") <> Nothing Then
            Else
                'HANYA NEW YANG GENERATE CODE
                If custname.Text.Trim <> "" Then
                    If custname.Text.Length < 1 Then
                        showMessage("Nama Customer Minimal 1 Character", CompnyName & " - WARNING", 2)
                        custname.Text = "" : custcode.Text = ""
                        Exit Sub
                    Else
                        custcode.Text = KodeOutlate.Text & generateCustCode(Tchar(custname.Text))
                    End If
                Else
                    custcode.Text = ""
                End If
            End If
        End If
    End Sub   

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrintPDF.Click
        lblError.Text = ""
        Try
            PrintReport("", "Customer_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        'untuk print
        rptReport.Load(Server.MapPath("~/report/rpt_mstCustomer.rpt"))
        'rptReport.SetParameterValue("cmpcode", CompnyCode)
        Dim sWhere As String = "" : Dim sFilter As String = ""

        sWhere = " where QL_mstcust.cmpcode LIKE '%" & CompnyCode & "%' and custflag = '" & DDLStatus.SelectedValue & "' "

        If txtFilter.Text.Trim <> "" Then
            If ddlFilter.SelectedItem.Text.ToUpper = "Code" Or ddlFilter.SelectedItem.Text.ToUpper = "Nama" Or ddlFilter.SelectedItem.Text.ToUpper = "Group" Then
                sWhere &= "  and  upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '" & Tchar(txtFilter.Text) & "' "
            Else
                sWhere &= "  and  upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '%" & Tchar(txtFilter.Text) & "%' "
            End If 
        End If
        rptReport.SetParameterValue("sWhere", sWhere)
        cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        rptReport.ExportToHttpResponse(formatReport, Response, True, sFileName)
        'Response.Redirect(Page.AppRelativeVirtualPath.ToString)
    End Sub

    Private Sub PrintContract(ByVal sType As String)
        Dim sWhere As String = "" : Dim periode As String = ""
        Try
            If sType = "PDF" Then
                rptReport.Load(Server.MapPath(folderReport & "rptCust.rpt"))
            Else
                rptReport.Load(Server.MapPath(folderReport & "rptCust1.rpt"))
            End If
            If fddlCabang.SelectedValue <> "ALL" Then
                sWhere = " AND cust.branch_code = '" & fddlCabang.SelectedValue & "'"
            End If
            If cbCreditTempo.Checked = True Then
                sWhere &= " AND cust.custcreditlimitrupiahtempo " & ddlCreditTempo.SelectedValue & ""
            End If
            If cbStatus.Checked = True Then
                sWhere &= " AND cust.custflag='" & DDLStatus.SelectedValue & "'"
            End If
            If cbfOverDue.Checked = True Then
                sWhere &= " AND cust.custoverduestatus ='" & ddlOverDue.SelectedValue & "'"
            End If
            If chkCreatedByBranch.Checked Then
                sWhere &= " AND cust.createuser NOT IN ('','admin','adminql') "
            End If
            If cbTop.Checked Then
                sWhere &= " AND cust.timeofpayment = " & fddlTOP.SelectedValue & " "
            End If
            sWhere = " WHERE cust.cmpcode='" & CompnyCode & "' AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%' " & sWhere & ""
            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, rptReport)
            rptReport.SetParameterValue("sWhere", sWhere)
            rptReport.SetParameterValue("periode", periode)
            'cProc.SetDBLogonForReport(rptReport)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                rptReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Master_Customer_" & Format(GetServerTime(), "dd_MM_yy"))
                rptReport.Close() : rptReport.Dispose()
            End If
            rptReport.Close() : rptReport.Dispose()
        Catch ex As Exception
            rptReport.Close()
            rptReport.Dispose()
            showMessage(ex.Message, 1, "")
        End Try
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            'ShowReport("Excel")
            PrintContract("EXCEL")
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "CUSTOMER"
        imbPrintPDF.Visible = True
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoCust") = "" : lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        orderIDForReport.Text = "" : orderNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub dd_timeofpayment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dd_timeofpayment.SelectedIndexChanged
        If dd_timeofpayment.SelectedValue = 34 Then
            custcreditlimitrupiah.Enabled = False
            custcreditlimitrupiah.CssClass = "inpTextDisabled" 
        End If
    End Sub

    Protected Sub imbprintdtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("Custoid") = sender.tooltip
    End Sub

    Protected Sub imbcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "FORMAT" : imbPrintPDF.Visible = True
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoCust") = "" : lblError.Text = ""
        imbPrintPDF.Visible = False
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub GVmstcust_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstcust.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If ToDouble(e.Row.Cells(7).Text) > ToDouble(e.Row.Cells(6).Text) Then
                For C1 As Integer = 0 To GVmstcust.Columns.Count - 2
                    e.Row.Cells(C1).ForeColor = System.Drawing.Color.Red
                Next
            End If
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
        End If
    End Sub

    Protected Sub custcreditlimitrupiah_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcreditlimitrupiah.TextChanged
        custcreditlimitrupiah.Text = ToMaskEdit(ToDouble(custcreditlimitrupiah.Text), 3)
        custcreditlimitrupiahawal.Text = ToMaskEdit(ToDouble(custcreditlimitrupiah.Text), 3)
        custcreditlimitrupiahsisa.Text = ToMaskEdit(ToDouble(custcreditlimitrupiah.Text), 3)

    End Sub

    Protected Sub custcreditlimitusagerupiah_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcreditlimitusagerupiah.TextChanged
        custcreditlimitusagerupiah.Text = ToMaskEdit(ToDouble(custcreditlimitusagerupiah.Text), 3)
    End Sub
#End Region

    Protected Sub ddlcustgroup1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlcustgroup1.SelectedValue <> "ALL" Then
            sSql = "select custgroupcreditlimitrupiah,custgroupcreditlimitusagerupiah from QL_mstcustgroup where custgroupoid = " & ddlcustgroup1.SelectedValue & " "
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xReader = xCmd.ExecuteReader
            Dim supname As String = "" : Dim contname As String = ""
            If xReader.HasRows Then
                While xReader.Read
                    custcreditlimitrupiah.Text = xReader("custgroupcreditlimitrupiah")
                    custcreditlimitusagerupiah.Text = xReader("custgroupcreditlimitusagerupiah")
                End While
            End If
	    conn.Close()
        Else
            custcreditlimitrupiah.Text = "" : custcreditlimitusagerupiah.Text = ""
        End If
    End Sub

    Protected Sub branchID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitCoa()
    End Sub

    Protected Sub custcreditlimitrupiahtempo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcreditlimitrupiahtempo.TextChanged
        custcreditlimitrupiah.Text = ToDouble(custcreditlimitrupiahawal.Text) + ToDouble(custcreditlimitrupiahtempo.Text)
        custcreditlimitrupiahsisa.Text = ToDouble(custcreditlimitrupiah.Text) - ToDouble(custcreditlimitusagerupiah.Text)
        custcreditlimitrupiah.Text = ToMaskEdit(ToDouble(custcreditlimitrupiah.Text), 3)
        custcreditlimitrupiahtempo.Text = ToMaskEdit(ToDouble(custcreditlimitrupiahtempo.Text), 3)
        custcreditlimitrupiahsisa.Text = ToMaskEdit(ToDouble(custcreditlimitrupiahsisa.Text), 3)
    End Sub

    Protected Sub custcreditlimitrupiahawal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custcreditlimitrupiah.Text = ToDouble(custcreditlimitrupiahawal.Text) + ToDouble(custcreditlimitrupiahtempo.Text)
        custcreditlimitrupiahsisa.Text = ToDouble(custcreditlimitrupiah.Text) - ToDouble(custcreditlimitusagerupiah.Text)
        custcreditlimitrupiah.Text = ToMaskEdit(ToDouble(custcreditlimitrupiah.Text), 3)
        custcreditlimitrupiahawal.Text = ToMaskEdit(ToDouble(custcreditlimitrupiahawal.Text), 3)
        custcreditlimitrupiahtempo.Text = ToMaskEdit(ToDouble(custcreditlimitrupiahtempo.Text), 3)
        custcreditlimitrupiahsisa.Text = ToMaskEdit(ToDouble(custcreditlimitrupiahsisa.Text), 3)
    End Sub

    Protected Sub gvCustdtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = ToMaskEdit(e.Row.Cells(1).Text, 3)
            e.Row.Cells(2).Text = ToMaskEdit(e.Row.Cells(2).Text, 3)
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
        End If
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim updcustoid As Integer = sender.tooltip
        sSql = "Update ql_mstcust set custoverduestatus = 'N' where custoid = " & updcustoid & ""
        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

        sSql = "Update ql_mstcustdtl set custdtlstatus = 'Closed' where custdtloid in (select custdtloid from ql_mstcustdtl where custdtlstatus = 'Enabled' and custdtlflag = 'Over Due' and custoid = " & updcustoid & ")"
        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
        conn.Close()
        showMessage("Data Berhasil Di Process", CompnyName & "- Warning", 3)
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If fddlCabang.SelectedValue <> "ALL" Then
            sWhere &= " AND cust.branch_code = '" & fddlCabang.SelectedValue & "'"
        End If
        If cbCreditTempo.Checked = True Then
            sWhere &= " AND cust.custcreditlimitrupiahtempo " & ddlCreditTempo.SelectedValue & ""
        End If
        If cbStatus.Checked = True Then
            sWhere &= " AND cust.custflag='" & DDLStatus.SelectedValue & "'"
        End If
        If cbfOverDue.Checked = True Then
            sWhere &= " AND cust.custoverduestatus ='" & ddlOverDue.SelectedValue & "'"
        End If
        bindCust(sWhere)
    End Sub

    Protected Sub btnInfoNota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInfoNota.Click
        DataBindTrn()
        mpeListMat.Show()
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListNya_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub gvListNya_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListNya.PageIndex = e.NewPageIndex
        DataBindTrn() : mpeListMat.Show()
    End Sub
End Class