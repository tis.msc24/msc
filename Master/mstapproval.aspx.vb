'Coded by zipi'
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_mstapproval
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim folderReport As String = "~/Report/"
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub InitCBL()
        sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' and gengroup='CABANG' ORDER BY gencode ASC"
        cbCabang.Items.Clear()
        'cbCabang.Items.Add("All")
        'cbCabang.Items(cbCabang.Items.Count - 1).Value = "All"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            cbCabang.Items.Add(xreader.GetValue(1).ToString)
            cbCabang.Items(cbCabang.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
        Session("LastAllCheck") = False
    End Sub

    Public Sub GenerateApprovalID()
        approvalsetoid.Text = GenerateID("QL_approvalstructure", CompnyCode)
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText_mstapproval")
        FilterDDL.SelectedIndex = Session("FilterDDL_mstapproval")
        Dim sSort As String = ""
        If cbSorting.Checked Then
            sSort &= "order by " & DDLSorting.SelectedValue & " " & DDLSorting2.SelectedValue & ""
        End If
        sWhere = " AND upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%' " & sSort & ""
        'disimpan ke dalam session untuk keperluan print
        Session("sWhere_mstapproval") = sWhere

        sSql = "SELECT approvalsetoid,gendesc tablename,USERNAME  approvaluser, approvalstatus FROM QL_approvalstructure inner join QL_mstgen on gencode  = tablename inner join QL_MSTPROF on USERID = approvaluser  WHERE upper(QL_approvalstructure.cmpcode)='" & sCompCode.ToUpper & "' and approvaluser in (select userid from QL_mstprof where cmpcode='" & CompnyCode & "' and statusProf = 'ACTIVE' ) " & sWhere & ""

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstapproval.DataSource = objDs.Tables("data")
        GVmstapproval.DataBind()
    End Sub

    Public Sub FillTextBox(ByVal vApprovalsetoid As String)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "SELECT approvalsetoid, tablename, approvaltype, approvallevel, approvaluser, approvalstatus,branch_code,upduser, updtime FROM QL_approvalstructure where cmpcode='" & CompnyCode & "' and approvalsetoid=" & vApprovalsetoid
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            approvalsetoid.Text = Trim(xreader("approvalsetoid").ToString)
            tablename.SelectedValue = Trim(xreader("tablename").ToString)
            approvaltype.SelectedValue = Trim(xreader("approvaltype").ToString)
            approvaltype_SelectedIndexChanged(Nothing, Nothing)
            approvallevel.SelectedValue = Trim(xreader("approvallevel").ToString)
            approvaluser.SelectedValue = Trim(xreader("approvaluser").ToString)
            approvalstatus.Text = Trim(xreader("approvalstatus").ToString) 
            Upduser.Text = Trim(xreader("upduser").ToString)
            Updtime.Text = CDate(Trim(xreader("updtime").ToString)).ToString("dd/MM/yyyy HH:mm:ss tt")
            Dim sValue() As String = xreader("branch_code").ToString.Split(",")
            If sValue.Length > 0 Then
                If cbCabang.Items.Count > 0 Then
                    For C1 As Integer = 0 To sValue.Length - 1
                        Dim li As ListItem = cbCabang.Items.FindByValue(sValue(C1).Trim)
                        Dim i As Integer = cbCabang.Items.IndexOf(li)
                        If i >= 0 Then
                            cbCabang.Items(cbCabang.Items.IndexOf(li)).Selected = True
                        End If
                    Next
                End If
            End If

        End While
        xreader.Close()
        conn.Close()
    End Sub

    Public Sub InitAllDDL()
        'sSql = "select tablename, tablename from QL_mstoid where tablegroup='MASTER'"
        'FillDDL(tablename, sSql)
        'Update:30.06.2010; ubah sql untuk tablename 
        sSql = "Select gencode,gendesc from QL_mstgen where gengroup='TABLENAME' ORDER BY gendesc"
        FillDDL(tablename, sSql)
        sSql = "select userid, username from QL_mstprof where cmpcode='" & CompnyCode & "' and statusProf = 'ACTIVE' AND DIVISI = 'Approval' ORDER BY username"
        FillDDL(approvaluser, sSql)
        approvalstatus.Text = approvaltype.SelectedItem.Text & approvallevel.SelectedItem.Text
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            'Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Remove("oid_mstapproval")
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstapproval.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid_mstapproval") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")
        Page.Title = CompnyName & " - Approval Structure"

        If Not Page.IsPostBack Then
            BindData(CompnyCode)
            InitAllDDL() : InitCBL()
            If Session("oid_mstapproval") IsNot Nothing And Session("oid_mstapproval") <> "" Then
                btnDelete.Visible = True
                PanelUpdate.Visible = True
                FillTextBox(Session("oid_mstapproval"))
                TabContainer1.ActiveTabIndex = 1
                approvaltype.Enabled = False
                approvallevel.Enabled = False
            Else
                GenerateApprovalID()
                btnDelete.Visible = False
                PanelUpdate.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0
            End If
        End If

        'FilterGV("", "", "", Session("USERID"))
        ''Ambil max level yg udah diinputkan
        'sSql = "select count(-1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' " & _
        '       "and tablename='" & tablename.SelectedValue & "'"
        'If cKon.ambilscalar(sSql) > 0 Then
        '    sSql = "select min(isnull(approvallevel,0)) from QL_approvalstructure where " & _
        '      "cmpcode='" & CompnyCode & "' AND tablename ='" & tablename.SelectedValue & "' "
        '    If cKon.ambilscalar(sSql) > 1 Then
        '        approvallevel.SelectedValue = cKon.ambilscalar(sSql) - 1
        '    Else
        '        approvallevel.SelectedIndex = 0
        '    End If
        'Else
        '    approvallevel.SelectedIndex = 0
        'End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText_mstapproval") = FilterText.Text
        Session("FilterDDL_mstapproval") = FilterDDL.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session.Remove("FilterText_mstapproval")
        Session.Remove("FilterDDL_mstapproval")
        If cbSorting.Checked Then
            cbSorting.Checked = False
        End If
        BindData(CompnyCode)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Master\mstapproval.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errMessage As String = "" : Dim sSqlCheck As String = ""
        Dim strcabang As String = ""

        For C1 As Integer = 0 To cbCabang.Items.Count - 1
            If cbCabang.Items(C1).Value <> "All" And cbCabang.Items(C1).Selected = True Then
                strcabang &= cbCabang.Items(C1).Value & ", "
            End If
        Next

        If Session("oid_mstapproval") Is Nothing Or Session("oid_mstapproval") = "" Then
            sSql = "Select COUNT(*) from QL_approvalstructure Where approvaluser='" & approvaluser.SelectedValue & "' And tablename='" & tablename.SelectedValue & "'"
            Dim sMisV As Double = GetScalar(sSql)
            If ToDouble(sMisV) > 0 Then
                errMessage &= "Maaf, User " & approvaluser.SelectedItem.Text & " dengan table name " & tablename.SelectedItem.Text & " sudah ada..!!<BR>"
            End If
        End If

        'cek apakah approval sudah ada untuk table ini
        sSqlCheck = "Select count(-1) from QL_approvalstructure Where cmpcode='" & CompnyCode & "' and tablename='" & tablename.Text & "'"

        If Session("oid_mstapproval") IsNot Nothing And Session("oid_mstapproval") <> "" Then
            sSqlCheck &= " AND approvalsetoid <> " & approvalsetoid.Text
        End If

        If cKon.ambilscalar(sSqlCheck) = 0 Then
            If approvaltype.SelectedValue <> "FINAL" Then
                errMessage &= "Please select approval type 'FINAL' for first approval setting<BR>"
            End If
        End If

        If approvaltype.SelectedValue <> "FINAL" Then
            If approvallevel.SelectedValue = "7" Then
                errMessage &= "Approval level '7' just for Final !!<BR>"
            End If
        End If

        'Ambil max level yg udah diinputkan
        sSqlCheck = "select isnull(min(approvallevel),0) from QL_approvalstructure where " & _
               "cmpcode='" & CompnyCode & "' AND tablename ='" & tablename.SelectedValue & "' "
        If Session("oid_mstapproval") IsNot Nothing And Session("oid_mstapproval") <> "" Then
            sSqlCheck &= " AND approvalsetoid <> " & approvalsetoid.Text
        End If
        'untuk level table yang sama, apabila level terkecilnya > 1
        If cKon.ambilscalar(sSqlCheck) > 1 Then
            If cKon.ambilscalar(sSqlCheck) - 1 > approvallevel.SelectedValue Then
                errMessage &= "Approval level must " & cKon.ambilscalar(sSqlCheck) - 1 & "<BR>"
            End If
        End If

        'cek approval user yang kembar
        sSqlCheck = "SELECT COUNT(-1) FROM QL_approvalstructure WHERE " & _
        " cmpcode='" & CompnyCode & "' AND " & _
        " tablename='" & tablename.SelectedValue & "' AND " & _
        " approvaluser='" & approvaluser.SelectedValue & "' and branch_code = '" & Session("branch_id") & "'"
        If Session("oid_mstapproval") IsNot Nothing And Session("oid_mstapproval") <> "" Then
            sSqlCheck &= " AND approvalsetoid <> " & approvalsetoid.Text
        End If
        If cKon.ambilscalar(sSqlCheck) > 0 Then
            errMessage &= "User ID has been added for this approval<BR>"
        End If

        If errMessage <> "" Then
            showMessage(errMessage, CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

            'mode insert
            If Session("oid_mstapproval") Is Nothing Or Session("oid_mstapproval") = "" Then
                GenerateApprovalID()
                sSql = "INSERT into QL_approvalstructure(cmpcode,approvalsetoid,tablename,approvaltype,approvallevel,approvaluser,approvalstatus,createuser,upduser,updtime,branch_code) " & _
                "VALUES (" & _
                "'" & CompnyCode & "', " & _
                " " & approvalsetoid.Text & ", " & _
                "'" & tablename.Text & "', " & _
                "'" & approvaltype.Text & "', " & _
                " " & approvallevel.Text & ", " & _
                "'" & approvaluser.SelectedValue & "', " & _
                "'" & approvalstatus.Text & "', " & _
                "'" & Upduser.Text & "', " & _
                "'" & Upduser.Text & "', " & _
                "current_timestamp, '" & strcabang & "')"
            Else 'mode update
                sSql = "UPDATE QL_approvalstructure SET " & _
                "tablename='" & tablename.Text & "', " & _
                "approvaltype='" & approvaltype.Text & "', " & _
                "approvallevel=" & approvallevel.Text & ", " & _
                "approvaluser='" & approvaluser.SelectedValue & "', " & _
                "approvalstatus='" & approvalstatus.Text & "', " & _
                "upduser='" & Upduser.Text & "', " & _
                "updtime=current_timestamp, branch_code = '" & strcabang & "' " & _
                " WHERE cmpcode='" & CompnyCode & "' and approvalsetoid=" & approvalsetoid.Text
            End If
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'jika insert data baru, update mstoid
            If Session("oid_mstapproval") Is Nothing Or Session("oid_mstapproval") = "" Then
                sSql = "update QL_mstoid set lastoid=" & approvalsetoid.Text & " where tablename='QL_approvalstructure' and cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING")
            Exit Sub
        End Try
        Session.Remove("oid_mstapproval")
        Response.Redirect("~\Master\mstapproval.aspx?awal=true")
    End Sub

  Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
    CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, False)
    End Sub

  Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
    If approvalsetoid.Text = "" Then
      Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please fill Approval ID"))
      Exit Sub
    End If

        'Update:30.06.2010; 
        If approvaltype.SelectedValue = "FINAL" Then
            'cek apakah jika menghapus approval ini typex FINAL, dan tidak ada yg tipe FINAL lagi
            sSql = "SELECT COUNT(-1) FROM ql_approvalstructure " & _
                   "WHERE cmpcode = '" & CompnyCode & "'" & _
                   "AND tablename ='" & tablename.SelectedValue & "' " & _
                   "AND approvaltype = '" & approvaltype.SelectedValue & "' "
            If cKon.ambilscalar(sSql) = 1 Then
                showMessage("can't delete this data because this approval Type 'FINAL' only one data !!", CompnyName & " - WARNING")
                Exit Sub
            End If
        Else
            'cek apakah jika menghapus approval ini typex PROCESS, lalu cek type bawahnya
            sSql = "SELECT COUNT(-1) FROM ql_approvalstructure " & _
                   "WHERE cmpcode = '" & CompnyCode & "'" & _
                   "AND tablename ='" & tablename.SelectedValue & "' " & _
                   "AND approvaltype = '" & approvaltype.SelectedValue & "' " & _
                    "AND approvallevel = " & approvallevel.SelectedValue & " "
            If cKon.ambilscalar(sSql) = 1 Then
                'showMessage("can't delete this data because this approval Type 'PROCESS' only one data !!", CompnyName & " - WARNING")
                'Exit Sub
                'Else
                sSql = "SELECT COUNT(-1) FROM ql_approvalstructure " & _
                 "WHERE cmpcode = '" & CompnyCode & "'" & _
                 "AND tablename ='" & tablename.SelectedValue & "' " & _
                 "AND approvaltype = '" & approvaltype.SelectedValue & "' " & _
                  "AND approvallevel = " & approvallevel.SelectedValue & " -1 "
                If cKon.ambilscalar(sSql) > 0 Then
                    showMessage("can't delete this data because this approval Type 'PROCESS' only one data and Have level " & approvallevel.SelectedValue & " above !!", CompnyName & " - WARNING")
                    Exit Sub
                End If
            End If
        End If
       
        If DeleteData("QL_approvalstructure", "approvalsetoid", approvalsetoid.Text, CompnyCode) = True Then
            GenerateApprovalID()
            Session("oid_mstapproval") = ""
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data has been deleted!"))
        Else
            Exit Sub
        End If
        Response.Redirect("~\Master\mstapproval.aspx?awal=true")
    End Sub

  Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
    Try
      report.Load(Server.MapPath(folderReport & "rptMstApproval.rpt"))

            report.SetParameterValue("cmpcode", CompnyCode)
      report.SetParameterValue("sWhere", Session("sWhere_mstapproval"))
      'report.SetParameterValue("sWhere", sWhere)

      CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
          System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

      Response.Buffer = False

      Response.ClearContent()
      Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Approval_" & Format(GetServerTime(), "dd_MM_yy"))
      report.Close() : report.Dispose()
      Response.Redirect("~\Master\mstapproval.aspx?awal=true")
    Catch ex As Exception
            showMessage(ex.Message, CompnyName & " - WARNING")
    End Try
    report.Close() : report.Dispose()
    End Sub

    Protected Sub approvaltype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles approvaltype.SelectedIndexChanged

        If approvaltype.SelectedValue = "FINAL" Then
            approvallevel.Enabled = False
            approvallevel.CssClass = "inpTextDisabled"
            approvallevel.SelectedIndex = 0
        Else
            approvallevel.Enabled = True
            approvallevel.CssClass = "inpText"
        End If
        approvalstatus.Text = approvaltype.Text & approvallevel.Text
    End Sub

    Protected Sub approvallevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles approvallevel.SelectedIndexChanged
        approvalstatus.Text = approvaltype.Text & approvallevel.Text
    End Sub
#End Region

End Class
