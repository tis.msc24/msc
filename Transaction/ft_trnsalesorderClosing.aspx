<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ft_trnsalesorderClosing.aspx.vb" Inherits="SO_Close"
    Title="PT. MULTI SARANA COMPUTER - SO Closing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">


    <table class="tabelhias" width="100%">
        <tr>
            <th class="header">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: SO Closing"></asp:Label></th>
        </tr>
    </table>

    <div style="text-align: left;">
        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Font-Size="9pt">
            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updPanel1" runat="server">
                        <ContentTemplate>
<TABLE id="Table2" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD style="WIDTH: 137px">Cabang</TD><TD><asp:DropDownList id="cBangFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w21" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 137px">Filter</TD><TD><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w22" AutoPostBack="True" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged">
                                                <asp:ListItem Value="orderno">SO No</asp:ListItem>
                                            </asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w23" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 137px"><asp:CheckBox id="CbPeriode" runat="server" Text="Periode" __designer:wfdid="w24"></asp:CheckBox></TD><TD><asp:TextBox id="tglAwal" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w25"></asp:TextBox>&nbsp;<asp:ImageButton id="btnTglAwal" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> to <asp:TextBox id="tglAkhir" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w27"></asp:TextBox><asp:ImageButton id="btnTglAkhir" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w29"></asp:Label></TD></TR><TR><TD style="WIDTH: 137px"></TD><TD><asp:ImageButton id="ImgFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w30"></asp:ImageButton> <asp:ImageButton id="ImgAllFind" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton></TD></TR><TR><TD colSpan=2><asp:GridView id="gvClosedBPM" runat="server" Width="96%" Height="88px" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w32" GridLines="None" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="8" OnPageIndexChanging="gvClosedBPM_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="orderno" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderdateclosed" HeaderText="Date Closed">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderuserclosed" HeaderText="User Closed">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ordernoteclosed" HeaderText="Note Closed">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE><ajaxToolkit:CalendarExtender id="ceAwal" runat="server" __designer:wfdid="w33" TargetControlID="tglAwal" PopupButtonID="btnTglAwal" Format="dd/MM/yyyy">
                                            </ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ceAkhir" runat="server" __designer:wfdid="w34" TargetControlID="tglAkhir" PopupButtonID="btnTglAkhir" Format="dd/MM/yyyy">
                                            </ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w35" TargetControlID="tglAwal" UserDateFormat="DayMonthYear" CultureName="id-ID" MaskType="Date" Mask="99/99/9999">
                                            </ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w36" TargetControlID="tglAkhir" UserDateFormat="DayMonthYear" CultureName="id-ID" MaskType="Date" Mask="99/99/9999">
                                            </ajaxToolkit:MaskedEditExtender> 
</ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
                <HeaderTemplate>
                    <img align="absMiddle" alt="" src="../Images/corner.gif" />
                    <strong><span style="font-size: 9pt"> List of Closed SO :. </span></strong>
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD runat="server" visible="true"><asp:Label id="Label4" runat="server" Text="Periode" __designer:wfdid="w34" Visible="False"></asp:Label></TD><TD colSpan=4 runat="server" visible="true"><asp:TextBox id="tglAwal2" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w35" Visible="False"></asp:TextBox> <asp:ImageButton id="btnTglAwal2" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w36" Visible="False"></asp:ImageButton> <asp:TextBox id="tglAkhir2" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w37" Visible="False"></asp:TextBox> <asp:ImageButton id="btnTglAkhir2" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w38" Visible="False"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w39" Visible="False"></asp:Label> <asp:TextBox id="txtFilter2" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w40" MaxLength="30" Visible="False" Wrap="False"></asp:TextBox></TD></TR><TR><TD id="TD1" runat="server" visible="true">Cabang</TD><TD id="TD2" colSpan=2 runat="server" visible="true"><asp:DropDownList id="fCabang" runat="server" CssClass="inpText" __designer:wfdid="w41" AutoPostBack="True"></asp:DropDownList></TD><TD colSpan=1 runat="server" visible="true"><asp:Label id="ordermstoid" runat="server" __designer:wfdid="w42" Visible="False"></asp:Label></TD><TD colSpan=1 runat="server" visible="true"><asp:Label id="Label6" runat="server" __designer:wfdid="w43" Visible="False"></asp:Label></TD></TR><TR><TD>SO&nbsp;No.</TD><TD colSpan=2><asp:TextBox id="txtorderNo" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w44"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchBPM" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w45"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseBPM" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w46"></asp:ImageButton>&nbsp; </TD><TD colSpan=1>Customer</TD><TD colSpan=1><asp:TextBox id="custname" runat="server" Width="220px" CssClass="inpTextDisabled" __designer:wfdid="w47" Enabled="False"></asp:TextBox> </TD></TR><TR><TD></TD><TD colSpan=4><asp:GridView id="gvBPM2" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w48" PageSize="8" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." GridLines="None" DataKeyNames="orderoid,orderno,custoid,custname,branch_code" Font-Underline="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="SO No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="deliverydate" HeaderText="Delivery Date">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD><asp:Label id="Label3" runat="server" Width="54px" Text="Note" __designer:wfdid="w49"></asp:Label></TD><TD colSpan=2><asp:TextBox id="txtorderNote" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w50" MaxLength="200"></asp:TextBox>&nbsp; <asp:Label id="custoid" runat="server" __designer:wfdid="w51" Visible="False"></asp:Label></TD><TD colSpan=1><asp:Label id="Label7" runat="server" Width="106px" Text="Sisa Credit Limit" __designer:wfdid="w52" Visible="False"></asp:Label></TD><TD colSpan=1><asp:TextBox id="netto" runat="server" Width="131px" CssClass="inpTextDisabled" __designer:wfdid="w53" Visible="false" Enabled="False"></asp:TextBox></TD></TR><TR><TD colSpan=5><asp:GridView id="gv_item" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w54" PageSize="8" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." GridLines="None" DataKeyNames="itemcode,qty,unit" Font-Underline="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="CEtglAwal2" runat="server" __designer:wfdid="w55" Format="dd/MM/yyyy" PopupButtonID="btnTglAwal2" TargetControlID="tglAwal2" Enabled="True"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CEtglAkhir2" runat="server" __designer:wfdid="w56" Format="dd/MM/yyyy" PopupButtonID="btnTglAkhir2" TargetControlID="tglAkhir2" Enabled="True"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD colSpan=5><asp:Button id="btnCloseBPM" onclick="btnCloseBPM_Click" runat="server" CssClass="btn green" Text="Close SO" __designer:wfdid="w57"></asp:Button> <asp:Button id="btncancel" runat="server" CssClass="btn gray" Text="Cancel" __designer:wfdid="w58"></asp:Button></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w59" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w60"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
                <HeaderTemplate>
                    <strong><span style="font-size: 9pt">
                        <img align="absMiddle" alt="" src="../Images/corner.gif" />
                        Form Closing SO :.</span></strong>
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer><br />
        <br />
        <asp:UpdatePanel ID="updPanel2" runat="server">
            <ContentTemplate>
                <asp:Panel ID="PanelValidasi" runat="server" CssClass="modalMsgBox" __designer:wfdid="w120" Visible="False">
                    <table>
                        <tbody>
                            <tr>
                                <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                    <asp:Label ID="captionPesan" runat="server" Font-Size="Small" Font-Bold="True" Text="header" __designer:wfdid="w121"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="HEIGHT: 3px" colspan="2"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/warn.png" __designer:wfdid="w122"></asp:Image></td>
                                <td style="TEXT-ALIGN: left" class="Label">
                                    <asp:Label ID="isiPesan" runat="server" ForeColor="Red" __designer:wfdid="w123"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="HEIGHT: 3px; TEXT-ALIGN: center" colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="TEXT-ALIGN: center" colspan="2">
                                    <asp:Button ID="btnErrOK" OnClick="btnErrOK_Click" runat="server" CssClass="btn red" Text=" OK " __designer:wfdid="w124" UseSubmitBehavior="False"></asp:Button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
                &nbsp;
                <ajaxToolkit:ModalPopupExtender ID="mpeMsg" runat="server" TargetControlID="PanelValidasi" __designer:wfdid="w125" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panelvalidasi" PopupDragHandleControlID="Validasi"></ajaxToolkit:ModalPopupExtender>
                <asp:Button ID="btnExtenderValidasi" runat="server" __designer:wfdid="w126" Visible="False"></asp:Button>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

