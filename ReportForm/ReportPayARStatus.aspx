﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ReportPayARStatus.aspx.vb" Inherits="ReportForm_ReportPayARStatus" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: A/R PAYMENT STATUS REPORT"></asp:Label></th>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center" colspan="3">


<asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" Height="100%" __designer:wfdid="w7" DefaultButton="btnSearchSupplier"><TABLE><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD align=left colSpan=2><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w155"></asp:Label></TD><TD align=left colSpan=1>:</TD><TD align=left>&nbsp;<asp:TextBox id="txtStart" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w156"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w157"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w158" Visible="False"></asp:Label> <asp:TextBox id="txtFinish" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w159"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w160"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(MM/DD/yyyy)" __designer:wfdid="w161"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:Label id="Label2" runat="server" Text="Customer" __designer:wfdid="w162"></asp:Label></TD><TD class="Label" align=left colSpan=1>:</TD><TD align=left><asp:RadioButtonList id="rbSupplier" runat="server" __designer:wfdid="w163" RepeatDirection="Horizontal" AutoPostBack="True">
                                                                                                    <asp:ListItem Selected="True">ALL</asp:ListItem>
                                                                                                    <asp:ListItem>SELECT</asp:ListItem>
                                                                                                </asp:RadioButtonList></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:Label id="custoid" runat="server" __designer:wfdid="w167" Visible="False"></asp:Label></TD><TD class="Label" align=right colSpan=1></TD><TD align=left><asp:TextBox id="FilterTextSupplier" runat="server" Width="175px" CssClass="inpText" __designer:wfdid="w164" Visible="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupplier" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w165" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w166" Visible="False"></asp:ImageButton> </TD></TR><TR id="tr_supp" runat="server" visible="false"><TD class="Label" align=center colSpan=2></TD><TD class="Label" align=center colSpan=1></TD><TD vAlign=top align=left><asp:GridView id="gvSupplier" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w168" Visible="False" DataKeyNames="custoid,custcode,custname,custaddr" EnableModelValidation="True" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><EditItemTemplate>
                                                                                                                                                        <asp:CheckBox ID="CheckBox1" runat="server" __designer:wfdid="w3"></asp:CheckBox>
                                                                                                                                                    
</EditItemTemplate>
<ItemTemplate>
                                                                                                                                                        <asp:CheckBox ID="chkSelect" runat="server" ToolTip='<%# Eval("custoid")%>' Checked='<%# Eval("selected") %>' __designer:wfdid="w2"></asp:CheckBox>
                                                                                                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                                                                                <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label>
                                                                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR id="tr_caricoa" runat="server"><TD class="Label" align=left colSpan=2>Account:</TD><TD class="Label" align=left colSpan=1></TD><TD align=left><asp:RadioButtonList id="rblAccount" runat="server" __designer:wfdid="w169" RepeatDirection="Horizontal" AutoPostBack="True">
                                                                                                <asp:ListItem Selected="True">ALL</asp:ListItem>
                                                                                                <asp:ListItem>SELECT</asp:ListItem>
                                                                                            </asp:RadioButtonList></TD></TR><TR runat="server"><TD class="Label" align=right colSpan=2></TD><TD class="Label" align=right colSpan=1></TD><TD align=left><asp:TextBox id="acctgdesc" runat="server" Width="168px" CssClass="inpText" __designer:wfdid="w170" Visible="False"></asp:TextBox> <asp:ImageButton id="btnsearchacctg" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w171" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w172" Visible="False"></asp:ImageButton></TD></TR><TR id="tr_coa" runat="server" visible="false"><TD class="Label" align=right colSpan=2></TD><TD class="Label" align=right colSpan=1></TD><TD align=left><asp:GridView id="gvAccounting" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w173" Visible="False" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer "></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Acctgoid" Visible="False"><ItemTemplate>
                                                                                                <asp:Label ID="lblacctgoid" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
                                                                                                <asp:CheckBox ID="cbAccount" runat="server" Checked='<%# Eval("selected") %>' />
                                                                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="code" HeaderText="Code" SortExpression="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" Font-Size="Small" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                        <asp:Label ID="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found."></asp:Label>
                                                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate "></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=2><asp:Label id="Label5" runat="server" Text="No.Bukti" __designer:wfdid="w174"></asp:Label></TD><TD align=left colSpan=1>:</TD><TD align=left><asp:TextBox id="Txtnobukti" runat="server" Width="227px" CssClass="inpText" __designer:wfdid="w175"></asp:TextBox></TD></TR><TR><TD align=left colSpan=2>Status Payment</TD><TD align=left colSpan=1>:</TD><TD align=left><asp:DropDownList id="ddstatus" runat="server" CssClass="inpText" __designer:wfdid="w176">
                                                                                    <asp:ListItem>All Status</asp:ListItem>
                                                                                    <asp:ListItem>In Process</asp:ListItem>
                                                                                    <asp:ListItem>Post</asp:ListItem>
                                                                                </asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w177"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w178"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w179"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w180"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w181" TargetControlID="txtStart" Format="dd/MM/yyyy" PopupButtonID="imbStart"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" __designer:wfdid="w182" TargetControlID="txtStart" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceFin" runat="server" __designer:wfdid="w183" TargetControlID="txtFinish" Format="dd/MM/yyyy" PopupButtonID="imbFinish"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" __designer:wfdid="w184" TargetControlID="txtFinish" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w185" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w186"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></TD></TR><TR><TD colSpan=3><CR:CrystalReportViewer id="crvReportForm" runat="server" __designer:wfdid="w57" HasViewList="False" HasToggleGroupTreeButton="False" HasPrintButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="True"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel></td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel></td>
        </tr>
    </table>


</asp:Content>
