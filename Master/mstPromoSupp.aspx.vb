Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports Microsoft.Office.Interop

Partial Class Master_PromoSupp
    Inherits System.Web.UI.Page
    'TargetQty

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public BranchCode As String = ConfigurationSettings.AppSettings("BranchCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Private statusReport As New ReportDocument
    Private myDiskFileDestinationOptions As DiskFileDestinationOptions
    Private myExportOptions As ExportOptions
    Dim CProc As New ClassProcedure
    Public folderReport As String = "~/report/"
    Dim report As New ReportDocument
    Dim oRegex As Regex
    Dim oMatches As MatchCollection
#End Region

#Region "Functions" 
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Function SetDtlSupp() As DataTable
        If Session("SetDtlSupp") Is Nothing Then
            Dim dtlSupp As New DataTable
            dtlSupp.Columns.Add("seq", Type.GetType("System.Int32"))
            dtlSupp.Columns.Add("suppoid", Type.GetType("System.String"))
            dtlSupp.Columns.Add("suppcode", Type.GetType("System.String"))
            dtlSupp.Columns.Add("suppname", Type.GetType("System.String"))
            dtlSupp.Columns.Add("suppaddr", Type.GetType("System.String"))
            Return dtlSupp
        End If
    End Function

    Private Function SetTblReward() As DataTable
        If Session("TblReward") Is Nothing Then
            Dim TblReward As New DataTable
            TblReward.Columns.Add("seq", Type.GetType("System.String")) '0
            TblReward.Columns.Add("RewardOid", Type.GetType("System.Int32")) '1
            TblReward.Columns.Add("itemoid", Type.GetType("System.Int32")) '2
            TblReward.Columns.Add("itemcode", Type.GetType("System.String")) '3
            TblReward.Columns.Add("itemdesc", Type.GetType("System.String")) '4
            TblReward.Columns.Add("TargetPoint", Type.GetType("System.Decimal")) '5
            TblReward.Columns.Add("TargetAmt", Type.GetType("System.Decimal")) '6
            TblReward.Columns.Add("rQty", Type.GetType("System.Decimal")) '7
            TblReward.Columns.Add("rAmount", Type.GetType("System.Decimal")) '8
            TblReward.Columns.Add("TypeNya", Type.GetType("System.String")) '9
            TblReward.Columns.Add("targetqty", Type.GetType("System.Decimal")) '10
            TblReward.Columns.Add("persenreward", Type.GetType("System.Decimal")) '11
            TblReward.Columns.Add("persenreward2", Type.GetType("System.Decimal")) '12
            Return TblReward
        End If
    End Function

    Private Function setTblDtl() As DataTable
        If Session("TbldtlItem") Is Nothing Then
            Dim tbldtl As New DataTable
            tbldtl.Columns.Add("seq", Type.GetType("System.String")) '0
            tbldtl.Columns.Add("promodtloid", Type.GetType("System.Int32")) '1
            tbldtl.Columns.Add("itemoid", Type.GetType("System.Int32")) '2
            tbldtl.Columns.Add("itemcode", Type.GetType("System.String")) '3
            tbldtl.Columns.Add("itemdesc", Type.GetType("System.String")) '4
            tbldtl.Columns.Add("Point", Type.GetType("System.Decimal")) '5
            tbldtl.Columns.Add("TargetQty", Type.GetType("System.Decimal")) '6
            tbldtl.Columns.Add("priceitem", Type.GetType("System.Decimal")) '7
            tbldtl.Columns.Add("aspend", Type.GetType("System.Decimal")) '8
            Return tbldtl
        End If
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function
#End Region

#Region "Procedure"
    Private Sub EnabledField()
        If TypeDDL.SelectedValue = "POINT" Then 'inpTextDisabled
            TargetPoint.Enabled = True : TargetPoint.CssClass = "inpText"
            TargetAmt.Enabled = False : TargetAmt.CssClass = "inpTextDisabled"
            TargetQty.Enabled = False : TargetQty.CssClass = "inpTextDisabled"
            PersenReward.Enabled = False : PersenReward.CssClass = "inpTextDisabled"
            PersenReward2.Enabled = False : PersenReward2.CssClass = "inpTextDisabled"
        ElseIf TypeDDL.SelectedValue = "AMOUNT" Then
            TargetPoint.Enabled = False : TargetPoint.CssClass = "inpTextDisabled"
            TargetAmt.Enabled = True : TargetAmt.CssClass = "inpText"
            TargetQty.Enabled = False : TargetQty.CssClass = "inpTextDisabled"
            PersenReward.Enabled = True : PersenReward.CssClass = "inpText"
            PersenReward2.Enabled = False : PersenReward2.CssClass = "inpTextDisabled"
        ElseIf TypeDDL.SelectedValue = "QTYBARANG" Then
            TargetPoint.Enabled = False : TargetPoint.CssClass = "inpTextDisabled"
            TargetAmt.Enabled = False : TargetAmt.CssClass = "inpTextDisabled"
            TargetQty.Enabled = False : TargetQty.CssClass = "inpTextDisabled"
            PersenReward.Enabled = True : PersenReward.CssClass = "inpText"
            PersenReward2.Enabled = False : PersenReward2.CssClass = "inpTextDisabled"
        ElseIf TypeDDL.SelectedValue = "QTYTOTAL" Then
            TargetPoint.Enabled = False : TargetPoint.CssClass = "inpTextDisabled"
            TargetAmt.Enabled = False : TargetAmt.CssClass = "inpTextDisabled"
            TargetQty.Enabled = True : TargetQty.CssClass = "inpText"
            PersenReward.Enabled = True : PersenReward.CssClass = "inpText"
            PersenReward2.Enabled = False : PersenReward2.CssClass = "inpTextDisabled"
        ElseIf TypeDDL.SelectedValue = "ASPEND" Then
            TargetPoint.Enabled = False : TargetPoint.CssClass = "inpTextDisabled"
            TargetAmt.Enabled = True : TargetAmt.CssClass = "inpText"
            TargetQty.Enabled = False : TargetQty.CssClass = "inpTextDisabled"
            PersenReward.Enabled = True : PersenReward.CssClass = "inpText"
            PersenReward2.Enabled = True : PersenReward2.CssClass = "inpText"
        End If

        If TypeDDL.SelectedValue = "POINT" Then
            GVItemList.Columns(4).Visible = True
            GVItemList.Columns(5).Visible = False
            GVItemList.Columns(7).Visible = False
        ElseIf TypeDDL.SelectedValue = "AMOUNT" Then
            GVItemList.Columns(4).Visible = False
            GVItemList.Columns(5).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYBARANG" Then
            GVItemList.Columns(4).Visible = False
            GVItemList.Columns(5).Visible = True
            GVItemList.Columns(7).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYTOTAL" Then
            GVItemList.Columns(4).Visible = False
            GVItemList.Columns(5).Visible = False
            GVItemList.Columns(7).Visible = False
        ElseIf TypeDDL.SelectedValue = "ASPEND" Then
            GVItemList.Columns(4).Visible = False
            GVItemList.Columns(5).Visible = False
            GVItemList.Columns(7).Visible = True
        End If

        If TypeDDL.SelectedValue = "POINT" Then
            gvPromoDtl.Columns(4).Visible = True
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeDDL.SelectedValue = "AMOUNT" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYBARANG" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = True
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYTOTAL" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeDDL.SelectedValue = "ASPEND" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = True
        End If

        If TypeDDL.SelectedValue = "POINT" Then
            GvItemReward.Columns(4).Visible = True
            GvItemReward.Columns(5).Visible = False
            GvItemReward.Columns(6).Visible = True
            GvItemReward.Columns(7).Visible = False
            GvItemReward.Columns(8).Visible = False
            GvItemReward.Columns(9).Visible = False
        ElseIf TypeDDL.SelectedValue = "AMOUNT" Then
            GvItemReward.Columns(4).Visible = False
            GvItemReward.Columns(5).Visible = True
            GvItemReward.Columns(6).Visible = True
            GvItemReward.Columns(7).Visible = False
            GvItemReward.Columns(8).Visible = True
            GvItemReward.Columns(9).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYBARANG" Then
            GvItemReward.Columns(4).Visible = False
            GvItemReward.Columns(5).Visible = False
            GvItemReward.Columns(6).Visible = True
            GvItemReward.Columns(7).Visible = False
            GvItemReward.Columns(8).Visible = True
            GvItemReward.Columns(9).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYTOTAL" Then
            GvItemReward.Columns(4).Visible = False
            GvItemReward.Columns(5).Visible = False
            GvItemReward.Columns(6).Visible = True
            GvItemReward.Columns(7).Visible = True
            GvItemReward.Columns(8).Visible = True
            GvItemReward.Columns(9).Visible = False
        ElseIf TypeDDL.SelectedValue = "ASPEND" Then
            GvItemReward.Columns(4).Visible = False
            GvItemReward.Columns(5).Visible = True
            GvItemReward.Columns(6).Visible = True
            GvItemReward.Columns(7).Visible = False
            GvItemReward.Columns(8).Visible = True
            GvItemReward.Columns(9).Visible = True
        End If

        If TypeDDL.SelectedValue = "POINT" Then
            gvPromoDtl.Columns(4).Visible = True
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeDDL.SelectedValue = "AMOUNT" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYBARANG" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = True
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYTOTAL" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeDDL.SelectedValue = "ASPEND" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = True
        End If

        If TypeDDL.SelectedValue = "POINT" Then
            GvRewardDtl.Columns(4).Visible = True
            GvRewardDtl.Columns(5).Visible = False
            GvRewardDtl.Columns(6).Visible = True
            GvRewardDtl.Columns(7).Visible = True
            GvRewardDtl.Columns(8).Visible = False
            GvRewardDtl.Columns(9).Visible = False
            GvRewardDtl.Columns(10).Visible = False
        ElseIf TypeDDL.SelectedValue = "AMOUNT" Then
            GvRewardDtl.Columns(4).Visible = False
            GvRewardDtl.Columns(5).Visible = True
            GvRewardDtl.Columns(6).Visible = false
            GvRewardDtl.Columns(7).Visible = False
            GvRewardDtl.Columns(8).Visible = False
            GvRewardDtl.Columns(9).Visible = True
            GvRewardDtl.Columns(10).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYBARANG" Then
            GvRewardDtl.Columns(4).Visible = False
            GvRewardDtl.Columns(5).Visible = False
            GvRewardDtl.Columns(6).Visible = True
            GvRewardDtl.Columns(7).Visible = True
            GvRewardDtl.Columns(8).Visible = False
            GvRewardDtl.Columns(9).Visible = True
            GvRewardDtl.Columns(10).Visible = False
        ElseIf TypeDDL.SelectedValue = "QTYTOTAL" Then
            GvRewardDtl.Columns(4).Visible = False
            GvRewardDtl.Columns(5).Visible = False
            GvRewardDtl.Columns(6).Visible = True
            GvRewardDtl.Columns(7).Visible = True
            GvRewardDtl.Columns(8).Visible = True
            GvRewardDtl.Columns(9).Visible = True
            GvRewardDtl.Columns(10).Visible = False
        ElseIf TypeDDL.SelectedValue = "ASPEND" Then
            GvRewardDtl.Columns(4).Visible = False
            GvRewardDtl.Columns(5).Visible = True
            GvRewardDtl.Columns(6).Visible = True
            GvRewardDtl.Columns(7).Visible = False
            GvRewardDtl.Columns(8).Visible = True
            GvRewardDtl.Columns(9).Visible = True
            GvRewardDtl.Columns(10).Visible = True
        End If
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    'TargetQty
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("Point") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("TargetQty") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("aspend") = ToDouble(GetTextValue(row.Cells(7).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If

        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("Point") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("TargetQty") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("aspend") = ToDouble(GetTextValue(row.Cells(7).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                            dv(0)("Point") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("TargetQty") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("aspend") = ToDouble(GetTextValue(row.Cells(7).Controls))
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub UpdateCheckedItem()
        If Session("TblListItem") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListItem")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GvItemReward.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GvItemReward.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("TargetPoint") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("TargetAmt") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("rQTy") = ToDouble(GetTextValue(row.Cells(6).Controls))
                            dv(0)("TargetQTy") = ToDouble(GetTextValue(row.Cells(7).Controls))
                            dv(0)("PersenReward") = ToDouble(GetTextValue(row.Cells(8).Controls))
                            dv(0)("PersenReward2") = ToDouble(GetTextValue(row.Cells(9).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListItem") = dt
        End If

        If Not Session("TblListItemView") Is Nothing Then
            Dim dt As DataTable = Session("TblListItemView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GvItemReward.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GvItemReward.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("TargetPoint") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("TargetAmt") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("rQTy") = ToDouble(GetTextValue(row.Cells(6).Controls))
                            dv(0)("TargetQTy") = ToDouble(GetTextValue(row.Cells(7).Controls))
                            dv(0)("PersenReward") = ToDouble(GetTextValue(row.Cells(8).Controls))
                            dv(0)("PersenReward2") = ToDouble(GetTextValue(row.Cells(9).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                            dv(0)("TargetPoint") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("TargetAmt") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("rQTy") = ToDouble(GetTextValue(row.Cells(6).Controls))
                            dv(0)("TargetQTy") = ToDouble(GetTextValue(row.Cells(7).Controls))
                            dv(0)("PersenReward") = ToDouble(GetTextValue(row.Cells(8).Controls))
                            dv(0)("PersenReward2") = ToDouble(GetTextValue(row.Cells(9).Controls))
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListItemView") = dt
        End If
    End Sub

    Private Sub GeneratedCode()
        'Dim cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & Session("branch_id") & "' AND gengroup='CABANG'")

        'Dim sNo As String = "PS/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        'sSql = "SELECT isnull(max(abs(replace(promocode,'" & sNo & "',''))),0)+1 FROM QL_mstpromosupp WHERE promocode LIKE '" & sNo & "%'"
        'CodePro.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub ClearDtlInput()
        ItemNya.Text = "" : ItemOidNya.Text = 0
        Session("TblReward") = Nothing : Session("TbldtlItem") = Nothing
    End Sub

    Private Sub ClearMstInput()
        SuppOid.Text = "" : SuppName.Text = ""
        promeventname.Text = "" : promstartdate.Text = ""
        promfinishdate.Text = "" : note.Text = ""
    End Sub

    Private Sub CheckedSupp()
        If Session("TblSupp") IsNot Nothing Then
            Dim dt As DataTable = Session("TblSupp")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvSupplier.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvSupplier.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "suppoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblSupp") = dt
        End If

        If Not Session("TblSuppView") Is Nothing Then
            Dim dt As DataTable = Session("TblSuppView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvSupplier.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvSupplier.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "suppoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblSuppView") = dt
        End If
    End Sub

    Public Sub DataSupp(ByVal fSupp As String)
        sSql = "Select 'False' AS checkvalue,suppoid,oldsuppcode,suppaddr,suppcode,suppname,paymenttermoid,prefixsupp From QL_mstsupp Where supptype = 'GROSIR' and cmpcode='" & CompnyCode & "' " & fSupp & " AND suppflag='Active' ORDER BY suppcode ASC"
        Dim dtS As DataTable = cKoneksi.ambiltabel(sSql, "QL_MSTSUPP")
        If dtS.Rows.Count > 0 Then
            Session("TblSupp") = dtS
            Session("TblSuppView") = Session("TblSupp")
            gvSupplier.DataSource = Session("TblSuppView")
            gvSupplier.DataBind() : gvSupplier.SelectedIndex = -1
            gvSupplier.Visible = True
        End If
    End Sub

    Private Sub BindData(ByVal sSqlNya As String)
        sSql = "Select * from (SELECT promooid [promoid], promocode [promeventcode], promoname [promeventname], convert(varchar(10),promodate1,103) promstartdate, convert(varchar(10),promodate2,103) promfinishdate, getdate() [promstarttime],GETDATE() [promfinishtime], '' [promtype], ps.suppoid, ps.statuspromo, Case typenya When 'POINT' then 'TARGET POINT' When 'AMOUNT' Then 'TARGET AMOUNT' When 'QTYBARANG' Then 'TARGET QTY PER BARANG' When 'QTYTOTAL' Then 'TARGET QTY TOTAL'  When 'ASPEND' then 'TARGET ASPEND' Else '' End typenya FROM QL_mstpromosupp ps Where ps.cmpcode='" & CompnyCode & "' " & sSqlNya & ") sp "
        If ddlStatus.SelectedValue <> "ALL" Then
            sSql &= " Where statuspromo='" & ddlStatus.SelectedValue & "'"
        End If
        sSql &= " ORDER BY [promoid] DESC"
        FillGV(GVMst, sSql, "QL_MstPromoSupp")
    End Sub

    Private Sub BindItemReward(ByVal tPoint As Double, ByVal tAmt As Double, ByVal rQty As Double, ByVal TargetQty As Double, ByVal PersenReward As Double, ByVal PersenReward2 As Double, ByVal iFilter As String)
        sSql = "Select 'False' AS checkvalue,i.itemoid,i.itemcode,i.itemdesc,Case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End TypeBarang,i.pricelist," & ToMaskEdit(tPoint, 3) & " TargetPoint," & ToMaskEdit(tAmt, 3) & " TargetAmt," & ToMaskEdit(rQty, 3) & " rQty," & ToMaskEdit(TargetQty, 3) & " TargetQty," & ToMaskEdit(PersenReward, 3) & " PersenReward," & ToMaskEdit(PersenReward2, 3) & " PersenReward2 From ql_mstitem i Where itemflag='AKTIF' and (itemcode like '%" & iFilter & "%' or itemdesc like '%" & iFilter & "%')"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstitem")
        If dt.Rows.Count > 0 Then
            Session("TblListItem") = dt
            Session("TblListItemView") = Session("TblListItem")
            GvItemReward.DataSource = Session("TblListItemView")
            GvItemReward.DataBind() : GVItemList.SelectedIndex = -1
            GvItemReward.Visible = True
        End If
    End Sub

    Private Sub BindItemNya(ByVal nPoint As Double, ByVal QtyTarget As Double, ByVal Aspend As Double, ByVal iFilter As String)
        EnabledField()
        sSql = "Select 'False' AS checkvalue,i.itemoid,i.itemcode,i.itemdesc,Case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End TypeBarang,Isnull(it.priceitem,0.00) priceitem," & ToMaskEdit(nPoint, 3) & " Point," & ToMaskEdit(QtyTarget, 3) & " targetqty," & ToMaskEdit(Aspend, 3) & " Aspend From ql_mstitem i left Join (SELECT bd.itemoid,Isnull(SUM(bd.trnbelidtlpriceidr*bd.trnbelidtlqty),00)/Isnull(SUM(bd.trnbelidtlqty),0.00) PriceItem FROM QL_trnbelidtl bd Inner Join QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid Where bm.trnbelidate Between '" & CDate(toDate(promstartdate.Text)) & "' AND '" & CDate(toDate(promfinishdate.Text)) & "' Group by bd.itemoid) it ON it.itemoid=i.itemoid Where itemflag='AKTIF' And (itemcode like '%" & Tchar(iFilter) & "%' or itemdesc like '%" & Tchar(iFilter) & "%')"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.SelectedIndex = -1
            GVItemList.Visible = True
        End If
    End Sub

    Private Sub FillTextBox(ByVal vSearchKey As String)
        Dim personid As String = "" : Dim OidSupp As String = ""
        sSql = "SELECT promooid [promoid], promocode [promeventcode], promoname [promeventname], convert(varchar(10),promodate1,103) promstartdate, convert(varchar(10),promodate2,103) promfinishdate, getdate() [promstarttime], GETDATE() [promfinishtime], ps.suppoid, '' suppname, crtuser, crttime, ps.upduser, ps.updtime, notemst, typenya, ps.statuspromo FROM QL_mstpromosupp ps Where promooid=" & vSearchKey & ""

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xreader = xCmd.ExecuteReader
        BtnDelete.Enabled = True

        While xreader.Read
            CodePro.Text = Trim(xreader("promeventcode")).ToString
            SuppName.Text = Trim(xreader("suppname"))
            promeventname.Text = Trim(xreader("promeventname")).ToString
            promstartdate.Text = xreader("promstartdate")
            promfinishdate.Text = xreader("promfinishdate")
            note.Text = Trim(xreader("notemst")).ToString
            sTatus.Text = xreader("statuspromo")
            UpdUser.Text = Trim(xreader("upduser"))
            Updtime.Text = Format(CDate(Trim(xreader("updtime").ToString)), "MM/dd/yyyy HH:mm:ss")
            createtime.Text = Format(xreader("crttime"), "dd/MM/yyyy HH:mm:ss tt")
            TypeDDL.Enabled = False
            TypeDDL.SelectedValue = Trim(xreader("typenya").ToString)
            OidSupp = Trim(xreader("suppoid")) 
        End While

        conn.Close() : xreader.Close()
        sSql = "Select spd.seq, spd.suppoid, sup.suppaddr, sup.suppcode, sup.suppname From QL_mstsuppdtl spd INNER JOIN QL_mstsupp sup ON sup.suppoid=spd.suppoid Where spd.promooid=" & vSearchKey & " ORDER BY suppcode ASC"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet

        mySqlDA.Fill(objDs, "SetDtlSupp")
        GVdtlSupp.Visible = True

        Dim dv As DataView = objDs.Tables("SetDtlSupp").DefaultView
        GVdtlSupp.DataSource = objDs.Tables("SetDtlSupp")
        GVdtlSupp.DataBind()
        Session("SetDtlSupp") = objDs.Tables("SetDtlSupp")
        conn.Close()

        sSql = "Select branch_code, promodtloid, promooid, i.itemoid, i.itemcode, i.itemdesc, pd.point, pd.seq, pd.targetqty, pd.priceitem, pd.aspend from QL_mstpromosuppdtl pd Inner Join QL_mstitem i ON i.itemoid=pd.itemoid Where pd.promooid=" & vSearchKey & " Order by pd.seq"
        Dim dtv As DataTable = cKoneksi.ambiltabel(sSql, "ql_promodtlsupp")
        Session("TbldtlItem") = dtv : gvPromoDtl.Visible = True
        gvPromoDtl.DataSource = dtv : gvPromoDtl.DataBind()

        sSql = "Select rw.branch_code, rw.rewarddtloid RewardOid, promooid, isnull(i.itemoid,0) itemoid, ISNULL(i.itemcode,'') itemcode, Isnull(i.itemdesc,rw.itemdesc) itemdesc, seq,rw.targetpoint, rw.targetamt, rw.qtyreward rQty, amtreward rAmount, typereward TypeNya, targetqty, PersenReward, PersenReward2 From ql_mstrewarddtl rw Left Join QL_mstitem i ON i.itemoid=rw.itemoid Where rw.promooid=" & vSearchKey & " Order by seq"
        Dim dtr As DataTable = cKoneksi.ambiltabel2(sSql, "ql_mstrewarddtl")
        Session("TblReward") = dtr : GvRewardDtl.Visible = True
        GvRewardDtl.DataSource = dtr : GvRewardDtl.DataBind()

        If sTatus.Text = "Approved" Then
            btnSave.Visible = False : BtnDelete.Visible = False
            imbApproval.Visible = False : BtnSaveAs.Visible = True
        ElseIf sTatus.Text = "In Approval" Then
            btnSave.Visible = False : BtnDelete.Visible = False
            imbApproval.Visible = False
        ElseIf sTatus.Text = "CLOSED" Then
            btnSave.Visible = False : BtnDelete.Visible = False
            imbApproval.Visible = False : BtnSaveAs.Visible = False 
        Else
            btnSave.Visible = True : BtnDelete.Visible = True
            imbApproval.Visible = True : BtnSaveAs.Visible = False
        End If
        EnabledField() : GVItemList.Visible = False
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 15
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xoutletoid As String = Session("outletoid")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstPromoSupp.aspx")
            '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Master Promo"
        Session("oid") = Request.QueryString("oid")
        Me.BtnDelete.Attributes.Add("onclick", "return confirm('Are u sure want to delete ??');")
        'TypeDDL_SelectedIndexChanged(Nothing, Nothing)
        If Not IsPostBack Then
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                PromOid.Text = GenerateID("QL_mstpromosupp", CompnyCode)
                CodePro.Text = GenerateID("QL_mstpromosupp", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
                UpdUser.Text = Session("UserID")
                Updtime.Text = GetServerTime()
                BtnDelete.Enabled = False
                BindData("")
                BtnDelete.Visible = False
                promstartdate.Text = Format(GetServerTime(), "01/MM/yyy")
                promfinishdate.Text = Format(GetServerTime(), "dd/MM/yyy")
                TypeDDL_SelectedIndexChanged(Nothing, Nothing)
            Else
                FillTextBox(Session("oid"))
                EnabledField()
                TabContainer1.ActiveTabIndex = 1
            End If
        End If
    End Sub

    Protected Sub gvPromoDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPromoDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
        End If
    End Sub

    Protected Sub gvPromoDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("TbldtlItem")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))

        Session("TbldtlItem") = objTable
        gvPromoDtl.DataSource = Session("TbldtlItem")
        gvPromoDtl.DataBind()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        ClearMstInput() : ClearDtlInput()
        Response.Redirect("~\Master\mstPromoSupp.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstpromosupp WHERE promooid=" & Integer.Parse(Session("oid"))
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM [QL_mstpromosuppdtl] WHERE cmpcode = '" & CompnyCode & "' AND promooid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM [QL_mstrewarddtl] WHERE cmpcode = '" & CompnyCode & "' AND promooid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING")
        Finally
            Response.Redirect("~\Master\mstPromoSupp.aspx?awal=true")
            BtnDelete.Enabled = False
        End Try

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sPlus As String = ""
        sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        BindData(sPlus)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterDDL.SelectedIndex = 0 : FilterText.Text = ""
        ddlStatus.SelectedIndex = 0
        BindData("")
    End Sub

    Protected Sub btnListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub GVMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVMst.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(3).Text = Format(e.Row.Cells(3).Text, "dd/MM/yyyy")
        '    e.Row.Cells(4).Text = Format(e.Row.Cells(4).Text, "dd/MM/yyyy")
        'End If
    End Sub

    Protected Sub BtnAddList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAddList.Click

        If Not Session("TblListMatView") Is Nothing Then
            UpdateCheckedMat()
            Dim dt As DataTable = Session("TblListMatView")
            Dim dtv As DataView = dt.DefaultView
            dtv.RowFilter = "checkvalue='True'"

            '--Create Tbl detail--
            '=====================
            Dim dtab As DataTable
            If Session("TbldtlItem") Is Nothing Then
                dtab = setTblDtl() : Session("TbldtlItem") = dtab
                seq.Text = 0 + 1
            Else
                dtab = Session("TbldtlItem")
            End If

            Dim dRow As DataRow
            For C1 As Integer = 0 To dtv.Count - 1
                Dim dtView As DataView = dtab.DefaultView
                dtView.RowFilter = "itemoid=" & Integer.Parse(dtv(C1)("itemoid")) & ""
                Dim dRowEdit() As DataRow
                If dtView.Count <= 0 Then
                    dRow = dtab.NewRow
                    dRow("seq") = dtab.Rows.Count + 1
                Else
                    dRowEdit = dtab.Select("itemoid=" & Integer.Parse(dtv(C1)("itemoid")) & "")
                    dRow = dRowEdit(0)
                    dRowEdit(0).BeginEdit()
                End If
                Dim ad As Double = ToDouble(dtv(C1)("Point"))
                dRow("promodtloid") = 0
                dRow("itemoid") = Integer.Parse(dtv(C1)("itemoid"))
                dRow("itemcode") = Tchar(dtv(C1)("itemcode").ToString)
                dRow("itemdesc") = Tchar(dtv(C1)("itemdesc").ToString)
                dRow("Point") = ToDouble(dtv(C1)("Point"))
                dRow("TargetQty") = ToDouble(dtv(C1)("TargetQty"))
                dRow("priceitem") = ToDouble(dtv(C1)("priceitem"))
                dRow("aspend") = ToDouble(dtv(C1)("aspend"))

                If dtView.Count <= 0 Then
                    dtab.Rows.Add(dRow) : dtab.AcceptChanges()
                Else
                    dRowEdit(0).EndEdit()
                    dtab.Select(Nothing, Nothing)
                End If
                dtab.AcceptChanges()
                dRow.EndEdit() : seq.Text = dtab.Rows.Count + 1
                dtView.RowFilter = ""
            Next

            Session("TbldtlItem") = dtab
            gvPromoDtl.Visible = True
            gvPromoDtl.DataSource = Nothing
            gvPromoDtl.DataSource = dtab
            gvPromoDtl.DataBind()

            gvPromoDtl.SelectedIndex = -1
            I_u2.Text = "New Detail"
            GVItemList.Visible = False : ItemNya.Text = ""
            dtv.RowFilter = "" : ImgAddReward.Visible = True
            Session("TblListMatView") = Nothing
            TypeDDL.Enabled = False : TypeDDL.CssClass = "inpTextDisabled"
            EnabledField()
        Else
            showMessage("Maaf, Anda belum memilih katalog sama sekali..!!", "MSC - WARNING")
        End If
    End Sub

    Protected Sub AddListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AddListSupp.Click
        If Not Session("TblSuppView") Is Nothing Then
            CheckedSupp()
            Dim dt As DataTable = Session("TblSuppView")
            Dim dtv As DataView = dt.DefaultView
            dtv.RowFilter = "checkvalue='True'"

            '--Create Tbl detail--
            '=====================
            Dim dSupp As DataTable
            If Session("SetDtlSupp") Is Nothing Then
                dSupp = SetDtlSupp() : Session("SetDtlSupp") = dSupp
                SuppSeq.Text = 0 + 1
            Else
                dSupp = Session("SetDtlSupp")
            End If

            Dim dRow As DataRow
            For C1 As Integer = 0 To dtv.Count - 1
                Dim dtView As DataView = dSupp.DefaultView
                dtView.RowFilter = "suppoid=" & Integer.Parse(dtv(C1)("suppoid")) & ""
                Dim dRowEdit() As DataRow
                If dtView.Count <= 0 Then
                    dRow = dSupp.NewRow
                    dRow("seq") = dSupp.Rows.Count + 1
                Else
                    dRowEdit = dSupp.Select("suppoid=" & Integer.Parse(dtv(C1)("suppoid")) & "")
                    dRow = dRowEdit(0)
                    dRowEdit(0).BeginEdit()
                End If

                dRow("suppoid") = Tchar(dtv(C1)("suppoid").ToString)
                dRow("suppcode") = Tchar(dtv(C1)("suppcode").ToString)
                dRow("suppname") = Tchar(dtv(C1)("suppname").ToString)
                dRow("suppaddr") = Tchar(dtv(C1)("suppaddr").ToString)
                If dtView.Count <= 0 Then
                    dSupp.Rows.Add(dRow) : dSupp.AcceptChanges()
                Else
                    dRowEdit(0).EndEdit()
                    dSupp.Select(Nothing, Nothing)
                End If
                dSupp.AcceptChanges()
                dRow.EndEdit() : SuppSeq.Text = dSupp.Rows.Count + 1
                dtView.RowFilter = ""
            Next

            Session("SetDtlSupp") = dSupp : GVdtlSupp.Visible = True
            GVdtlSupp.DataSource = Nothing : GVdtlSupp.DataSource = dSupp
            GVdtlSupp.DataBind() : GVdtlSupp.SelectedIndex = -1
        Else

        End If
        gvSupplier.Visible = False
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If gvSupplier.Visible = True Then
            CheckedSupp()
            Dim dBar As DataTable = Session("TblSuppView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "suppcode like '%" & Tchar(SuppName.Text.Trim) & "%' OR suppname LIKE '%" & Tchar(SuppName.Text.Trim) & "%'"
            If MisV.Count > 0 Then
                Session("DataSupp") = MisV.ToTable
                gvSupplier.DataSource = Session("DataSupp")
                gvSupplier.DataBind() : MisV.RowFilter = ""
            End If
        Else
            DataSupp("AND (suppcode like '%" & Tchar(SuppName.Text.Trim) & "%' OR suppname LIKE '%" & Tchar(SuppName.Text.Trim) & "%')")
        End If
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        CheckedSupp()
        gvSupplier.PageIndex = e.NewPageIndex
        gvSupplier.DataSource = Session("TblSuppView")
        gvSupplier.DataBind() 
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        gvSupplier.Visible = False
        SuppName.Text = "" : SuppOid.Text = ""
    End Sub

    Protected Sub sItemNya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sItemNya.Click
        EnabledField()
        If GVItemList.Visible = True Then
            UpdateCheckedMat()
            Dim dBar As DataTable = Session("TblListMatView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "(itemcode like '%" & Tchar(ItemNya.Text.Trim) & "%' or itemdesc like '%" & Tchar(ItemNya.Text.Trim) & "%')"
            If MisV.Count > 0 Then
                Session("DataItem") = MisV.ToTable
                GVItemList.DataSource = Session("DataItem")
                GVItemList.DataBind() : MisV.RowFilter = ""
            End If
        Else
            BindItemNya(0.0, 0.0, 0.0, ItemNya.Text)
        End If
    End Sub

    Protected Sub TypeDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TypeDDL.SelectedIndexChanged
        ItemOidNya.Text = "0" : ItemNya.Text = ""
        GVItemList.Visible = False : GvItemReward.Visible = False
        EnabledField()
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        UpdateCheckedMat()
        GVItemList.PageIndex = e.NewPageIndex
        Dim dBar As DataTable = Session("TblListMatView")
        Dim MisV As DataView = dBar.DefaultView
        MisV.RowFilter = "(itemcode like '%" & Tchar(ItemNya.Text.Trim) & "%' or itemdesc like '%" & Tchar(ItemNya.Text.Trim) & "%')"
        If MisV.Count > 0 Then
            Session("DataItem") = MisV.ToTable
            GVItemList.DataSource = Session("DataItem")
            GVItemList.DataBind() : MisV.RowFilter = ""
        End If
    End Sub

    Protected Sub sRewardBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sRewardBtn.Click
        EnabledField()
        If GvItemReward.Visible = True Then
            UpdateCheckedItem()
            Dim dBar As DataTable = Session("TblListItemView")
            Dim xVi As DataView = dBar.DefaultView
            xVi.RowFilter = "(itemcode like '%" & Tchar(ItemReward.Text.Trim) & "%' or itemdesc like '%" & Tchar(ItemReward.Text.Trim) & "%')"
            If xVi.Count > 0 Then
                Session("DataView") = xVi.ToTable
                GvItemReward.DataSource = Session("DataView")
                GvItemReward.DataBind() : xVi.RowFilter = ""
            End If
        Else
            BindItemReward(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Tchar(ItemReward.Text))
        End If
    End Sub

    Protected Sub GvItemReward_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        UpdateCheckedItem()
        GvItemReward.PageIndex = e.NewPageIndex
        Dim dBar As DataTable = Session("TblListItemView")
        Dim MisV As DataView = dBar.DefaultView
        MisV.RowFilter = "(itemcode like '%" & Tchar(ItemNya.Text.Trim) & "%' or itemdesc like '%" & Tchar(ItemNya.Text.Trim) & "%')"
        If MisV.Count > 0 Then
            Session("TblListItemView") = MisV.ToTable
            GvItemReward.DataSource = Session("TblListItemView")
            GvItemReward.DataBind() : MisV.RowFilter = ""
        End If 
    End Sub

    Protected Sub eRewardBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eRewardBtn.Click
        GvItemReward.Visible = False
        Session("TblListItemView") = Nothing
    End Sub

    Protected Sub ImgAddReward_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAddReward.Click

        If DDLTypeReward.SelectedValue = "BARANG" Then
            If Not Session("TblListItemView") Is Nothing Then
                UpdateCheckedItem()
                Dim dt As DataTable = Session("TblListItemView")
                Dim dtv As DataView = dt.DefaultView
                dtv.RowFilter = "checkvalue='True'"
                If dtv.Count <= 0 Then
                    sValidasi.Text &= "- Maaf, Anda belum memilih item reward sama sekali..!!<br >"
                End If

                '----Create detail----
                '=====================
                Dim dtb As DataTable
                If Session("TblReward") Is Nothing Then
                    dtb = SetTblReward() : Session("TblReward") = dtb
                    rSeq.Text = 0 + 1
                Else
                    dtb = Session("TblReward")
                End If

                If sValidasi.Text <> "" Then
                    showMessage(sValidasi.Text, "MSC - WARNING")
                    Exit Sub
                End If

                Dim dRow As DataRow
                For C1 As Integer = 0 To dtv.Count - 1
                    Dim dtView As DataView = dtb.DefaultView
                    dtView.RowFilter = "itemoid=" & Integer.Parse(dtv(C1)("itemoid")) & ""
                    Dim dRowEdit() As DataRow

                    If dtView.Count <= 0 Then
                        dRow = dtb.NewRow
                        dRow("seq") = dtb.Rows.Count + 1
                    Else
                        dRowEdit = dtb.Select("itemoid=" & Integer.Parse(dtv(C1)("itemoid")) & "")
                        dRow = dRowEdit(0)
                        dRowEdit(0).BeginEdit()
                    End If

                    dRow("RewardOid") = 0
                    dRow("itemoid") = Integer.Parse(dtv(C1)("itemoid"))
                    dRow("itemcode") = Tchar(dtv(C1)("itemcode").ToString)
                    dRow("itemdesc") = Tchar(dtv(C1)("itemdesc").ToString)
                    dRow("TargetPoint") = ToDouble(dtv(C1)("TargetPoint"))
                    dRow("TargetAmt") = ToDouble(dtv(C1)("TargetAmt"))
                    dRow("rQty") = ToDouble(dtv(C1)("rQty"))
                    dRow("rAmount") = ToDouble(0)
                    dRow("targetqty") = ToDouble(dtv(C1)("targetqty"))
                    dRow("persenreward") = ToDouble(dtv(C1)("persenreward"))
                    dRow("persenreward2") = ToDouble(dtv(C1)("persenreward2"))
                    dRow("TypeNya") = DDLTypeReward.SelectedValue

                    If dtView.Count <= 0 Then
                        dtb.Rows.Add(dRow)
                    Else
                        dRowEdit(0).EndEdit()
                        dtb.Select(Nothing, Nothing)
                    End If

                    dtb.AcceptChanges()
                    dRow.EndEdit() : rSeq.Text = dtb.Rows.Count + 1
                    dtView.RowFilter = ""
                Next

                Session("TblReward") = dtb
                GvRewardDtl.Visible = True
                GvRewardDtl.DataSource = Nothing
                GvRewardDtl.DataSource = dtb
                GvRewardDtl.DataBind()
                GvRewardDtl.SelectedIndex = -1
                I_u2r.Text = "New Detail"
                dtv.RowFilter = "" : GvItemReward.Visible = False
                ItemReward.Text = ""
                TargetAmt.Text = ToMaskEdit(0, 3) : rAmount.Text = ToMaskEdit(0, 3)
                TargetPoint.Text = ToMaskEdit(0, 3) : rQty.Text = ToMaskEdit(0, 3)
                TypeDDL.Enabled = False : TypeDDL.CssClass = "inpTextDisabled"
            Else
                showMessage("- Maaf, Anda belum memilih item reward sama sekali..!!", "MSC - WARNING")
            End If
        Else
            If ItemReward.Text.Trim = "" Then
                sValidasi.Text &= "- Maaf, Isi item reward..!!"
            End If

            If sValidasi.Text <> "" Then
                showMessage(sValidasi.Text, "MSC - WARNING")
                Exit Sub
            End If

            Dim dtb As DataTable
            If Session("TblReward") Is Nothing Then
                dtb = SetTblReward() : Session("TblReward") = dtb
                rSeq.Text = 0 + 1
            Else
                dtb = Session("TblReward")
            End If

            Dim dRow As DataRow
            Dim dtView As DataView = dtb.DefaultView
            dtView.RowFilter = "itemdesc='" & Tchar(ItemReward.Text) & "'"
            Dim dRowEdit() As DataRow
            If dtView.Count <= 0 Then
                dRow = dtb.NewRow
                dRow("seq") = dtb.Rows.Count + 1
            Else
                dRowEdit = dtb.Select("seq=" & Integer.Parse(rSeq.Text) & "")
                dRow = dRowEdit(0)
                dRowEdit(0).BeginEdit()
            End If

            dRow("RewardOid") = 0
            dRow("itemoid") = -9999
            dRow("itemcode") = Tchar(ItemReward.Text)
            dRow("itemdesc") = Tchar(ItemReward.Text)
            dRow("TargetPoint") = ToDouble(TargetPoint.Text)
            dRow("TargetAmt") = ToDouble(TargetAmt.Text)
            dRow("rQty") = ToDouble(rQty.Text)
            dRow("rAmount") = ToDouble(rAmount.Text)
            dRow("targetqty") = ToDouble(TargetQty.Text)
            dRow("persenreward") = ToDouble(PersenReward.Text)
            dRow("persenreward2") = ToDouble(PersenReward2.Text)
            dRow("TypeNya") = DDLTypeReward.SelectedValue

            If dtView.Count <= 0 Then
                dtb.Rows.Add(dRow)
            Else
                dRowEdit(0).EndEdit()
                dtb.Select(Nothing, Nothing)
            End If
            dtb.AcceptChanges()
            dRow.EndEdit() : rSeq.Text = dtb.Rows.Count + 1
            dtView.RowFilter = ""

            Session("TblReward") = dtb
            GvRewardDtl.Visible = True : GvRewardDtl.DataSource = Nothing
            GvRewardDtl.DataSource = dtb : GvRewardDtl.DataBind()
            I_u2r.Text = "New Detail"
            dtView.RowFilter = "" : ItemReward.Text = ""
            TargetAmt.Text = ToMaskEdit(0, 3) : rAmount.Text = ToMaskEdit(0, 3)
            TargetPoint.Text = ToMaskEdit(0, 3) : rQty.Text = ToMaskEdit(0, 3)
            TargetQty.Text = ToMaskEdit(0, 3) : PersenReward.Text = ToMaskEdit(0, 3)
            PersenReward2.Text = ToMaskEdit(0, 3)
            TypeDDL.Enabled = False : TypeDDL.CssClass = "inpTextDisabled"
        End If
        EnabledField()
    End Sub

    Protected Sub GvRewardDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvRewardDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
        End If
    End Sub

    Protected Sub BtnOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnOk.Click
        sValidasi.Text = ""
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
    End Sub

    Protected Sub GvRewardDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GvRewardDtl.RowDeleting
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("TblReward")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))

        Session("TblReward") = objTable
        GvRewardDtl.DataSource = Session("TblReward")
        GvRewardDtl.DataBind()
    End Sub
 
    Protected Sub GvRewardDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvRewardDtl.SelectedIndexChanged
        ImgAddReward.Visible = True : I_u2.Text = "Edit Detail"
        rSeq.Text = Integer.Parse(GvRewardDtl.SelectedDataKey("seq"))
        ItemReward.Text = GvRewardDtl.SelectedDataKey("itemdesc").ToString
        ItemRewardOid.Text = Integer.Parse(GvRewardDtl.SelectedDataKey("itemoid"))
        rAmount.Text = ToMaskEdit(GvRewardDtl.SelectedDataKey("rAmount"), 3)
        rQty.Text = ToMaskEdit(GvRewardDtl.SelectedDataKey("rQty"), 3)
        DDLTypeReward.SelectedValue = GvRewardDtl.SelectedDataKey("TypeNya")
        TargetPoint.Text = ToMaskEdit(GvRewardDtl.SelectedDataKey("targetpoint"), 3)
        TargetAmt.Text = ToMaskEdit(GvRewardDtl.SelectedDataKey("targetamt"), 3)
        TargetQty.Text = ToMaskEdit(GvRewardDtl.SelectedDataKey("targetqty"), 3)
        PersenReward.Text = ToMaskEdit(GvRewardDtl.SelectedDataKey("persenreward"), 3)
        PersenReward2.Text = ToMaskEdit(GvRewardDtl.SelectedDataKey("persenreward2"), 3)
        EnabledField()
        GvItemReward.Visible = True
        If GvRewardDtl.SelectedDataKey("TypeNya") = "BARANG" Then
            BindItemReward(GvRewardDtl.SelectedDataKey("TargetPoint"), GvRewardDtl.SelectedDataKey("TargetAmt"), GvRewardDtl.SelectedDataKey("rQty"), GvRewardDtl.SelectedDataKey("targetqty"), GvRewardDtl.SelectedDataKey("persenreward"), GvRewardDtl.SelectedDataKey("persenreward2"), Tchar(ItemReward.Text))
        Else
            sRewardBtn.Visible = False : eRewardBtn.Visible = False
            GvItemReward.Visible = False
        End If 
    End Sub

    Protected Sub gvPromoDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ItemNya.Text = gvPromoDtl.SelectedDataKey("itemdesc").ToString
        seq.Text = gvPromoDtl.SelectedDataKey("seq")
        BindItemNya(gvPromoDtl.SelectedDataKey("Point"), gvPromoDtl.SelectedDataKey("TargetQty"), gvPromoDtl.SelectedDataKey("aspend"), gvPromoDtl.SelectedDataKey("itemdesc"))
    End Sub
 
    Protected Sub eItemNya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eItemNya.Click
        ItemNya.Text = "" : GVItemList.Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        sValidasi.Text = ""
        Dim s1, s2 As Boolean : Dim sWhere As String = ""
        s1 = False : s2 = False : Dim OidSupp As String = ""

        If promeventname.Text.Trim = "" Then
            sValidasi.Text &= "- Maaf, Silahkan isi ""Promo Name""! <br/>"
        End If

        If promstartdate.Text.Trim = "" Then
            sValidasi.Text &= "- Maaf, pilih isi ""Tanggal periode1""! <br/>"
        Else

            Try
                Dim tgle As Date = CDate(toDate(promstartdate.Text))
                tgle = CDate(toDate(promstartdate.Text))
                s1 = True
            Catch ex As Exception
                sValidasi.Text &= "- Incorrect format date ""dd/MM/yyyy"" !" & "<br>"
                s1 = False
            End Try
        End If

        If promfinishdate.Text.Trim = "" Then
            sValidasi.Text &= "- Maaf, pilih isi ""Tanggal periode2""! <br/>"
        Else
            Try
                Dim tgle As Date = CDate(toDate(promfinishdate.Text))
                tgle = CDate(toDate(promfinishdate.Text))
                s2 = True
            Catch ex As Exception
                sValidasi.Text &= "- Maaf, Incorrect format date ""dd/MM/yyyy"" !" & "<br>"
                s2 = False
            End Try
        End If

        If s1 And s2 Then
            If CDate(toDate(promstartdate.Text)) > CDate(toDate(promfinishdate.Text)) Then
                sValidasi.Text &= "- Finish Date must >= Start Date ! <br/>"
            End If
        End If

        If (Session("TbldtlItem")) Is Nothing Then
            sValidasi.Text &= "- Maaf, Data detail belum ada !!<br/>"
        Else
            Dim objTableCek As DataTable = (Session("TbldtlItem"))
            If objTableCek.Rows.Count = 0 Then
                sValidasi.Text &= "- Maaf, Data detail belum ada !!<br/>"
            End If
        End If

        If checkApproval("QL_mstpromosupp", "In Approval", Session("oid"), "New", "FINAL", Session("branch_id")) > 0 Then
            sValidasi.Text &= "Maaf, Data Promo Ini sudah send Approval..!!<br>"
            status.Text = "In Approval"
        End If

        '----------------------------------
        If sTatus.Text = "In Approval" Then
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstpromosupp' And branch_code LIKE '%" & Session("branch_id") & "%' order by approvallevel"
            Dim dtData2 As DataTable = cKoneksi.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                sValidasi.Text &= "- Maaf, Tidak ada User untuk Approved Master Promo, Silahkan hubungi admin..!!<br>"
            End If
        ElseIf sTatus.Text = "Rejected" Then
            sTatus.Text = "In Process"
        End If

        If Session("SetDtlSupp") Is Nothing Then
            'Dim dBar As DataTable = Session("SetDtlSupp")
            'If dBar.Rows.Count > 0 Then
            '    For L1 As Int32 = 0 To dBar.Rows.Count - 1
            '        sWhere &= dBar.Rows(L1)("suppoid") & ","
            '    Next
            '    OidSupp &= Left(sWhere, sWhere.Length - 1)
            'Else
            '    sValidasi.Text = "- Maaf, Silahkan pilih Supplier..!!<br />"
            'End If
            'Else
            sValidasi.Text = "- Maaf, Silahkan pilih Supplier..!!<br />"
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_mstpromosupp WHERE crttime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sValidasi.Text &= "- Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
            End If
        End If

        If sValidasi.Text <> "" Then
            showMessage(sValidasi.Text, CompnyName & " - WARNING")
            Exit Sub
        End If

        UpdUser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
        Dim OidTemp As Integer = 0
        Dim OidTempDtl As Integer = GenerateID("QL_mstpromosuppdtl", CompnyCode)
        Dim OidSuppDtl As Integer = GenerateID("QL_mstsuppdtl", CompnyCode)
        Dim OidRewDtl As Integer = GenerateID("QL_mstrewarddtl", CompnyCode)
        Session("AppOid") = GenerateID("QL_Approval", CompnyCode)

        If Session("oid") = Nothing Or Session("oid") = "" Then
            OidTemp = GenerateID("QL_mstpromosupp", CompnyCode)
        Else
            OidTemp = Session("oid")
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If Session("oid") = Nothing Or Session("oid") = "" Then
                PromOid.Text = GenerateID("QL_mstpromosupp", CompnyCode)
                sSql = "INSERT INTO QL_mstpromosupp ([cmpcode],[branch_code],[promooid],[promocode],[promoname],[suppoid],[promodate1],[promodate2],[statuspromo],[notemst],[crtuser],[crttime],[upduser],[updtime],[typenya])" & _
               " VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & Integer.Parse(OidTemp) & ",'" & CodePro.Text & "','" & Tchar(promeventname.Text.Trim) & "','" & OidSupp & "','" & CDate(toDate(promstartdate.Text)) & "','" & CDate(toDate(promfinishdate.Text)) & "','" & sTatus.Text & "','" & Tchar(note.Text) & "','" & Session("UserID") & "','" & CDate(toDate(createtime.Text)) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & TypeDDL.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(OidTemp) & " WHERE tablename = 'QL_mstpromosupp' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                sSql = "UPDATE [QL_mstpromosupp] SET promoname='" & Tchar(promeventname.Text.Trim) & "', [promodate1]='" & CDate(toDate(promstartdate.Text)) & "', [promodate2]='" & CDate(toDate(promfinishdate.Text)) & "', [statuspromo]='" & Tchar(sTatus.Text) & "', [notemst]='" & Tchar(note.Text) & "', [upduser]='" & Session("UserID") & "', [updtime]=current_timestamp, [typenya]='" & TypeDDL.SelectedValue & "' WHERE promooid=" & Integer.Parse(Session("oid"))
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "DELETE FROM [QL_mstpromosuppdtl] WHERE cmpcode = '" & CompnyCode & "' AND promooid = " & Integer.Parse(OidTemp) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_mstsuppdtl WHERE cmpcode = '" & CompnyCode & "' AND promooid = " & Integer.Parse(OidTemp) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Dim dBar As DataTable = Session("SetDtlSupp")
            If dBar.Rows.Count > 0 Then
                For L1 As Int32 = 0 To dBar.Rows.Count - 1
                    sSql = "INSERT INTO QL_mstsuppdtl ([promosuppdtloid],[promooid],[suppoid],[updtime],[seq],[cmpcode]) VALUES (" & OidSuppDtl + L1 & "," & Integer.Parse(OidTemp) & "," & Integer.Parse(dBar.Rows(L1)("suppoid")) & ",current_timestamp," & Integer.Parse(dBar.Rows(L1)("seq")) & ",'MSC')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
                sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(OidSuppDtl) + dBar.Rows.Count - 1 & " Where tablename = 'QL_mstsuppdtl' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            '=========================================

            Dim objTable As DataTable = Session("TbldtlItem")
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT INTO QL_mstpromosuppdtl ([cmpcode],[branch_code],[promodtloid],[promooid],[seq],[statusdtl],[suppoid],[itemoid],[targetqty],[priceitem],point,aspend) " & _
                "VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & OidTempDtl + c1 & "," & Integer.Parse(OidTemp) & "," & Integer.Parse(objTable.Rows(c1).Item("seq")) & ",'" & sTatus.Text & "'," & 0 & "," & Integer.Parse(objTable.Rows(c1).Item("itemoid")) & "," & ToDouble(objTable.Rows(c1).Item("targetqty")) & "," & ToDouble(objTable.Rows(c1).Item("priceitem")) & "," & ToDouble(objTable.Rows(c1).Item("Point")) & "," & ToDouble(objTable.Rows(c1).Item("aspend")) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            sSql = "DELETE FROM [QL_mstrewarddtl] WHERE cmpcode = '" & CompnyCode & "' AND promooid = " & Integer.Parse(OidTemp) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            If Session("TblReward") IsNot Nothing Then
                Dim tbr As DataTable = Session("TblReward")
                For c1 As Int16 = 0 To tbr.Rows.Count - 1
                    sSql = "INSERT INTO [QL_mstrewarddtl] ([cmpcode],[branch_code],[rewarddtloid],[promooid],[suppoid],[itemoid],[statusdtl],[seq],[targetpoint],[targetamt],[upduser],[updtime],[qtyreward],[amtreward],[typereward],itemdesc,targetqty,persenreward,persenreward2)" & _
                    " VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & Integer.Parse(OidRewDtl) + c1 & "," & Integer.Parse(OidTemp) & "," & 0 & "," & Integer.Parse(tbr.Rows(c1).Item("itemoid")) & ",'" & sTatus.Text & "'," & Integer.Parse(tbr.Rows(c1).Item("seq")) & "," & ToDouble(tbr.Rows(c1).Item("targetpoint")) & "," & ToDouble(tbr.Rows(c1).Item("targetamt")) & ",'" & Session("UserID") & "',current_timestamp," & ToDouble(tbr.Rows(c1).Item("rQty")) & "," & ToDouble(tbr.Rows(c1).Item("rAmount")) & ",'" & tbr.Rows(c1).Item("TypeNya") & "','" & Tchar(tbr.Rows(c1).Item("itemdesc")) & "'," & ToDouble(tbr.Rows(c1).Item("targetqty")) & "," & ToDouble(tbr.Rows(c1).Item("persenreward")) & "," & ToDouble(tbr.Rows(c1).Item("persenreward2")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
                sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(OidRewDtl) + tbr.Rows.Count - 1 & " Where tablename = 'QL_mstrewarddtl' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(OidTempDtl) + objTable.Rows.Count - 1 & " Where tablename = 'QL_mstpromosuppdtl' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '-----------------------------------------------

            If sTatus.Text = "In Approval" Then
                If Session("oid") <> Nothing Or Session("oid") <> "" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim tbApp As DataTable : tbApp = Session("TblApproval")
                        For c1 As Int16 = 0 To tbApp.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus,branch_code) VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", '" & "PRO" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_mstpromosupp', '" & Session("oid") & "','In Approval','0', '" & tbApp.Rows(c1).Item("approvaluser") & "','1/1/1900','" & tbApp.Rows(c1).Item("approvaltype") & "','1', '" & tbApp.Rows(c1).Item("approvalstatus") & "','" & Session("branch_id") & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + tbApp.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Else
                    sTatus.Text = "In Procces"
                    showMessage("Maaf, Silahkan Simpan data promo dulu..!!", CompnyName & " - WARNING")
                    Exit Sub
                End If
            End If

            objTrans.Commit() : conn.Close()
            ClearDtlInput() : ClearMstInput()
        Catch ex As Exception
            sTatus.Text = "IN PROCESS"
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br/ >" & sSql, CompnyName & " - WARNING")
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstPromoSupp.aspx?awal=true")
    End Sub

    Protected Sub imbApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbApproval.Click
        status.Text = "In Approval" : btnSave_Click(sender, e)
    End Sub

    Protected Sub DDLTypeReward_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLTypeReward.SelectedIndexChanged
        If DDLTypeReward.SelectedValue <> "BARANG" Then
            sRewardBtn.Visible = False : eRewardBtn.Visible = False
            GvItemReward.Visible = False
        Else
            sRewardBtn.Visible = True : eRewardBtn.Visible = True
        End If
    End Sub

    Protected Sub BtnSaveAs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSaveAs.Click
        createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
        sTatus.Text = "IN PROCESS" : sValidasi.Text = ""
        Dim s1, s2 As Boolean : s1 = False : s2 = False

        If promeventname.Text.Trim = "" Then
            sValidasi.Text &= "Maaf, Silahkan isi ""Promo Name""! <br/>"
        End If

        If promstartdate.Text.Trim = "" Then
            sValidasi.Text &= "Maaf, pilih isi ""Tanggal periode1""! <br/>"
        Else

            Try
                Dim tgle As Date = CDate(toDate(promstartdate.Text))
                tgle = CDate(toDate(promstartdate.Text))
                s1 = True
            Catch ex As Exception
                sValidasi.Text &= "Incorrect format date ""dd/MM/yyyy"" !" & "<br>"
                s1 = False
            End Try
        End If

        If promfinishdate.Text.Trim = "" Then
            sValidasi.Text &= "Maaf, pilih isi ""Tanggal periode2""! <br/>"
        Else
            Try
                Dim tgle As Date = CDate(toDate(promfinishdate.Text))
                tgle = CDate(toDate(promfinishdate.Text))
                s2 = True
            Catch ex As Exception
                sValidasi.Text &= "Incorrect format date ""dd/MM/yyyy"" !" & "<br>"
                s2 = False
            End Try
        End If

        If s1 And s2 Then
            If CDate(toDate(promstartdate.Text)) > CDate(toDate(promfinishdate.Text)) Then
                sValidasi.Text &= "Finish Date must >= Start Date ! <br/>"
            End If
        End If

        If (Session("TbldtlItem")) Is Nothing Then
            sValidasi.Text &= "- Maaf, Data detail belum ada !!<br/>"
        Else
            Dim objTableCek As DataTable = (Session("TbldtlItem"))
            If objTableCek.Rows.Count = 0 Then
                sValidasi.Text &= "- No- Maaf, Data detail belum ada !!<br/>"
            End If
        End If

        '-------------------------------------------------------------------------------------     
        If Session("oid") <> Nothing Or Session("oid") <> "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_mstpromosupp WHERE crttime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sValidasi.Text &= "Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
            End If
        End If

        If sValidasi.Text <> "" Then
            showMessage(sValidasi.Text, CompnyName & " - WARNING")
            Exit Sub
        End If

        UpdUser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
        Dim OidTemp As Integer = GenerateID("QL_mstpromosupp", CompnyCode)
        Dim OidTempDtl As Integer = GenerateID("QL_mstpromosuppdtl", CompnyCode)
        Dim OidRewDtl As Integer = GenerateID("QL_mstrewarddtl", CompnyCode)
        Dim OidSuppDtl As Integer = GenerateID("QL_mstsuppdtl", CompnyCode)

        If Session("oid") <> Nothing Or Session("oid") <> "" Then
            OidTemp = GenerateID("QL_mstpromosupp", CompnyCode)
        End If
 
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If Session("oid") <> Nothing Or Session("oid") <> "" Then
                sSql = "INSERT INTO QL_mstpromosupp ([cmpcode],[branch_code],[promooid],[promocode],[promoname],[suppoid],[promodate1],[promodate2],[statuspromo],[notemst],[crtuser],[crttime],[upduser],[updtime],[typenya])" & _
              " VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & Integer.Parse(OidTemp) & ",'" & OidTemp & "','" & Tchar(promeventname.Text.Trim) & "','0','" & CDate(toDate(promstartdate.Text)) & "','" & CDate(toDate(promfinishdate.Text)) & "','" & sTatus.Text & "','" & Tchar(note.Text) & "','" & Session("UserID") & "','" & CDate(toDate(createtime.Text)) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & TypeDDL.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(OidTemp) & " WHERE tablename = 'QL_mstpromosupp' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Else
                sSql = "UPDATE [QL_mstpromosupp] SET [statuspromo]='CLOSED', [notemst]='" & Tchar(note.Text) & "' WHERE promooid=" & Integer.Parse(Session("oid"))
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "DELETE FROM QL_mstsuppdtl WHERE cmpcode = '" & CompnyCode & "' AND promooid = " & Integer.Parse(OidTemp) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Dim dBar As DataTable = Session("SetDtlSupp")
            If dBar.Rows.Count > 0 Then
                For L1 As Int32 = 0 To dBar.Rows.Count - 1
                    sSql = "INSERT INTO QL_mstsuppdtl ([promosuppdtloid],[promooid],[suppoid],[updtime],[seq],[cmpcode]) VALUES (" & OidSuppDtl + L1 & "," & Integer.Parse(OidTemp) & "," & Integer.Parse(dBar.Rows(L1)("suppoid")) & ",current_timestamp," & Integer.Parse(dBar.Rows(L1)("seq")) & ",'MSC')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
                sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(OidSuppDtl) + dBar.Rows.Count - 1 & " Where tablename = 'QL_mstsuppdtl' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            '=========================================
            sSql = "DELETE FROM [QL_mstpromosuppdtl] WHERE cmpcode = '" & CompnyCode & "' AND promooid = " & Integer.Parse(OidTemp) & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Dim objTable As DataTable = Session("TbldtlItem")
            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "INSERT INTO QL_mstpromosuppdtl ([cmpcode],[branch_code],[promodtloid],[promooid],[seq],[statusdtl],[suppoid],[itemoid],[targetqty],[priceitem],point,aspend) " & _
                 "VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & OidTempDtl + c1 & "," & Integer.Parse(OidTemp) & "," & Integer.Parse(objTable.Rows(c1).Item("seq")) & ",'" & sTatus.Text & "'," & 0 & "," & Integer.Parse(objTable.Rows(c1).Item("itemoid")) & "," & ToDouble(objTable.Rows(c1).Item("targetqty")) & "," & ToDouble(objTable.Rows(c1).Item("priceitem")) & "," & ToDouble(objTable.Rows(c1).Item("Point")) & "," & ToDouble(objTable.Rows(c1).Item("aspend")) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            sSql = "DELETE FROM [QL_mstrewarddtl] WHERE cmpcode = '" & CompnyCode & "' AND promooid = " & Integer.Parse(OidTemp) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            If Session("TblReward") IsNot Nothing Then
                Dim tbr As DataTable = Session("TblReward")
                For c1 As Int16 = 0 To tbr.Rows.Count - 1
                    sSql = "INSERT INTO [QL_mstrewarddtl] ([cmpcode],[branch_code],[rewarddtloid],[promooid],[suppoid],[itemoid],[statusdtl],[seq],[targetpoint],[targetamt],[upduser],[updtime],[qtyreward],[amtreward],[typereward],itemdesc,targetqty,persenreward,PersenReward2)" & _
                 " VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & Integer.Parse(OidRewDtl) + c1 & "," & Integer.Parse(OidTemp) & "," & 0 & "," & Integer.Parse(tbr.Rows(c1).Item("itemoid")) & ",'" & sTatus.Text & "'," & Integer.Parse(tbr.Rows(c1).Item("seq")) & "," & ToDouble(tbr.Rows(c1).Item("targetpoint")) & "," & ToDouble(tbr.Rows(c1).Item("targetamt")) & ",'" & Session("UserID") & "',current_timestamp," & ToDouble(tbr.Rows(c1).Item("rQty")) & "," & ToDouble(tbr.Rows(c1).Item("rAmount")) & ",'" & tbr.Rows(c1).Item("TypeNya") & "','" & Tchar(tbr.Rows(c1).Item("itemdesc")) & "'," & ToDouble(tbr.Rows(c1).Item("targetqty")) & "," & ToDouble(tbr.Rows(c1).Item("persenreward")) & "," & ToDouble(tbr.Rows(c1).Item("persenreward2")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
                sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(OidRewDtl) + tbr.Rows.Count - 1 & " Where tablename = 'QL_mstrewarddtl' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(OidTempDtl) + objTable.Rows.Count - 1 & " Where tablename = 'QL_mstpromosuppdtl' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '-----------------------------------------------           

            objTrans.Commit() : conn.Close()
            ClearDtlInput() : ClearMstInput()
        Catch ex As Exception
            sTatus.Text = "IN PROCESS"
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br/ >" & sSql, CompnyName & " - WARNING")
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstPromoSupp.aspx?awal=true")
    End Sub
 
    Protected Sub GVdtlSupp_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVdtlSupp.RowDeleting
        Dim objTable As DataTable : Dim objRow() As DataRow
        objTable = Session("SetDtlSupp")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        Session("SetDtlSupp") = objTable
        GVdtlSupp.DataSource = Session("SetDtlSupp")
        GVdtlSupp.DataBind()
    End Sub
#End Region

    Protected Sub GVItemList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub BtnSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblListMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblListMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListMat") = objTbl
                Session("TblListMatView") = dtTbl
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind()
                objView.RowFilter = ""
            End If
        End If
    End Sub 

    Protected Sub BtnAllReward_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblListItemView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblListItemView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListItem")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListItem") = objTbl
                Session("TblListItemView") = dtTbl
                GvItemReward.DataSource = Session("TblListItemView")
                GvItemReward.DataBind()
                objView.RowFilter = ""
            End If
        End If
    End Sub

    Protected Sub imbExportXLS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbExportXLS.Click
        Try  
            report.Load(Server.MapPath("~/report/rptsuppPromoXls.rpt"))
            Dim sWhere As String = "Where " & FilterDDL.SelectedValue & " LIKE '%" & TcharNoTrim(FilterText.Text) & "%'"
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sPeriod", "")

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "FP_" & Format(GetServerTime(), "yyyyMMdd"))
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            report.Close()
            report.Dispose()
        End Try
    End Sub
End Class