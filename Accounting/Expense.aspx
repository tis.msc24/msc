<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="Expense.aspx.vb" Inherits="Accounting_Expense" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <script type="text/javascript"> 
    </script>

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Expense/Biaya"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel2" runat="server" __designer:wfdid="w277" DefaultButton="btnSearch"><TABLE style="WIDTH: 700px"><TBODY><TR><TD align=left><asp:CheckBox id="cbPeriode" runat="server" Font-Size="X-Small" Text="Periode" __designer:wfdid="w301" Checked="True"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w278"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w279"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="X-Small" Text="to" __designer:wfdid="w280"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w281"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w282"></asp:ImageButton>&nbsp;&nbsp;</TD></TR><TR><TD align=left>Cabang</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="dd_cabang" runat="server" CssClass="inpText" __designer:wfdid="w284"></asp:DropDownList> <asp:DropDownList id="picfilter" runat="server" Width="233px" CssClass="inpText" Visible="False" __designer:wfdid="w285">
                                                            <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                                        </asp:DropDownList></TD></TR><TR><TD align=left>Nomer</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="no" runat="server" Width="143px" CssClass="inpText" __designer:wfdid="w286"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="statuse" runat="server" Width="98px" CssClass="inpText" __designer:wfdid="w287"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w288"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w289"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewLast" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w290"></asp:ImageButton></TD></TR><TR><TD id="TD8" align=left colSpan=4 Visible="false"><asp:ImageButton id="btnCheck" onclick="btnCheck_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w291"></asp:ImageButton> <asp:ImageButton id="btnUncheck" onclick="btnUncheck_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w292"></asp:ImageButton> <asp:ImageButton id="BtnPostSelected" onclick="BtnPostSelected_Click" runat="server" ImageUrl="~/Images/postselect.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w293"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4 Visible="false"><SPAN style="COLOR: #ff0033"><STRONG>Pemberitahuan : * Data transaksi dengan status in procces akan otomatis closed jika melewati tanggal sekarang, mohon untuk segera di posting</STRONG></SPAN></TD></TR><TR><TD align=left colSpan=4><asp:GridView id="gvmstcost" runat="server" Width="950px" ForeColor="#333333" __designer:wfdid="w294" PageSize="8" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankno,cashbankoid,cashbankstatus" AllowPaging="True" GridLines="None">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
<asp:CheckBox id="cbPosting" runat="server" __designer:wfdid="w221" Checked='<%# eval("selected") %>' ToolTip='<%# eval("cashbankoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:TemplateField>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Transaksi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="ImgPrint" onclick="ImgPrint_Click" runat="server" ImageUrl="~/Images/print.gif" __designer:wfdid="w218" ToolTip='<%# Eval("cashbankno") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="cashbankoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                            <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label>
                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w295" Enabled="True" TargetControlID="txtperiode2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w296" TargetControlID="txtPeriode1" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w297" TargetControlID="txtPeriode2" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w298" Enabled="True" TargetControlID="txtperiode1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE></asp:Panel><asp:Label id="Label13" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Expense : Rp. " Visible="False" __designer:wfdid="w299"></asp:Label> <asp:Label id="lblgrandtotal" runat="server" Font-Size="Small" Font-Bold="True" Visible="False" __designer:wfdid="w300"></asp:Label> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="gvmstcost"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="FONT-SIZE: 9pt">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image>
                                Daftar Biaya/Expense</span></strong>&nbsp; <strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="lblIO" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="" Visible="False" __designer:wfdid="w101"></asp:Label></TD><TD style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #000099" align=left>&nbsp;&nbsp;&nbsp; </TD><TD align=left></TD><TD align=left>&nbsp;</TD></TR><TR><TD align=left><asp:Label id="Label6" runat="server" Font-Size="X-Small" Text="Cabang" __designer:wfdid="w102"></asp:Label></TD><TD align=left><asp:DropDownList id="deptoid" runat="server" Width="150px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w103" AutoPostBack="True" OnSelectedIndexChanged="deptoid_SelectedIndexChanged"></asp:DropDownList> <asp:Label id="CutofDate" runat="server" Visible="False" __designer:wfdid="w104"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label24" runat="server" Font-Size="X-Small" Text="No. Bukti" __designer:wfdid="w105"></asp:Label></TD><TD align=left><asp:TextBox id="CashBankNo" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w106" Enabled="False"></asp:TextBox> <asp:TextBox id="cashbankoid" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w107" MaxLength="10" ReadOnly="True"></asp:TextBox></TD><TD align=left><asp:Label id="lblBankname" runat="server" Font-Size="X-Small" Text="Bank" __designer:wfdid="w108"></asp:Label></TD><TD align=left><asp:DropDownList id="bank" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w109" AutoPostBack="True" OnSelectedIndexChanged="currate_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label9" runat="server" Font-Size="X-Small" Text="Pembayaran" __designer:wfdid="w110"></asp:Label></TD><TD align=left><asp:DropDownList id="payflag" runat="server" Width="105px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w111" AutoPostBack="True" OnSelectedIndexChanged="payflag_SelectedIndexChanged"><asp:ListItem Text="CASH" Value="CASH"></asp:ListItem>
<asp:ListItem Text="NON CASH" Value="NON CASH"></asp:ListItem>
<asp:ListItem Enabled="False">GIRO</asp:ListItem>
</asp:DropDownList></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label17" runat="server" Width="67px" Font-Size="X-Small" Text="No Cheque" Visible="False" __designer:wfdid="w112"></asp:Label> <asp:Label id="Label8" runat="server" Width="1px" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w113"></asp:Label></TD><TD align=left><asp:TextBox id="payrefno" runat="server" Width="230px" CssClass="inpText" Visible="False" __designer:wfdid="w114" MaxLength="30"></asp:TextBox></TD><TD align=left>&nbsp;</TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label16" runat="server" Width="101px" Font-Size="X-Small" Text="Cash/Akun Bank" __designer:wfdid="w115"></asp:Label></TD><TD align=left><asp:TextBox id="txtCoa" runat="server" Width="405px" CssClass="inpText " __designer:wfdid="w116"></asp:TextBox> <asp:ImageButton id="btnCariCoa" onclick="BtnCariCoa_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" Height="16px" __designer:wfdid="w117"></asp:ImageButton> <asp:ImageButton id="ImageButton2" onclick="btnErase_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" Height="16px" __designer:wfdid="w118"></asp:ImageButton> <asp:Label id="lblCoa" runat="server" Text="0" Visible="False" __designer:wfdid="w119"></asp:Label></TD><TD align=left><asp:Label id="Label10" runat="server" Width="81px" Font-Size="X-Small" Text="Jatuh Tempo" Visible="False" __designer:wfdid="w120"></asp:Label></TD><TD align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" Visible="False" __designer:wfdid="w121"></asp:TextBox> <asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" Visible="False" __designer:wfdid="w122" BorderColor="White"></asp:ImageButton></TD></TR><TR><TD align=left></TD><TD align=left><asp:GridView id="GvCoa" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w123" OnSelectedIndexChanged="GvCoa_SelectedIndexChanged" OnPageIndexChanging="GvCoa_PageIndexChanging" PageSize="5" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD><TD align=left><asp:DropDownList id="cashbankacctgoid" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w124"></asp:DropDownList></TD><TD align=left><ajaxToolkit:FilteredTextBoxExtender id="amtdtlext" runat="server" __designer:wfdid="w125" ValidChars="1234567890,." TargetControlID="cashbankglamt"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="excashbankglamt" runat="server" __designer:wfdid="w126" ValidChars="1234567890.," TargetControlID="cashbankglamt"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:CalendarExtender id="ce7" runat="server" __designer:wfdid="w127" TargetControlID="cashbankdate" PopupButtonID="imbCBDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left><asp:Label id="Label12" runat="server" Font-Size="X-Small" Text="Tanggal" __designer:wfdid="w128"></asp:Label> <asp:Label id="Label22" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w129"></asp:Label></TD><TD align=left><asp:TextBox id="cashbankdate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w130" AutoPostBack="True" Enabled="False" OnTextChanged="cashbankdate_TextChanged"></asp:TextBox> <asp:ImageButton id="imbCBDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w131"></asp:ImageButton> <asp:Label id="Label23" runat="server" ForeColor="Red" __designer:wfdid="w132">(dd/MM/yyyy)</asp:Label></TD><TD align=left><asp:Label id="Label20" runat="server" Font-Size="X-Small" Text="Mata Uang" Visible="False" __designer:wfdid="w133"></asp:Label></TD><TD align=left><asp:DropDownList id="currate" runat="server" Width="98px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w134" AutoPostBack="True" OnSelectedIndexChanged="currate_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD></TR><TR><TD align=left>PIC <asp:Label id="Label25" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w135"></asp:Label></TD><TD align=left><asp:TextBox id="pic" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w136" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchPerson" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w137"></asp:ImageButton> <asp:ImageButton id="BtnErasePIC" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" Height="16px" __designer:wfdid="w138"></asp:ImageButton> <asp:Label id="picoid" runat="server" Width="1px" Visible="False" __designer:wfdid="w139">0</asp:Label></TD><TD align=left><asp:Label id="Label26" runat="server" Font-Size="X-Small" Text="Rate" Visible="False" __designer:wfdid="w140"></asp:Label></TD><TD align=left><asp:TextBox id="currencyrate" runat="server" Width="100px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w141" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD align=left></TD><TD align=left rowSpan=1><asp:GridView id="gvPerson" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w142" PageSize="5" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="personoid,personnip,personname" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="personname" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="350px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD align=left></TD><TD align=left>&nbsp;<ajaxToolkit:MaskedEditExtender id="meedate" runat="server" __designer:wfdid="w143" TargetControlID="cashbankdate" InputDirection="RightToLeft" ClearMaskOnLostFocus="true" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><asp:HiddenField id="HiddenField2" runat="server" __designer:wfdid="w144"></asp:HiddenField><asp:HiddenField id="HiddenField1" runat="server" __designer:wfdid="w145"></asp:HiddenField><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w146" TargetControlID="payduedate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w147" TargetControlID="payduedate" PopupButtonID="btnDueDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left><asp:Label id="Label4" runat="server" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w148"></asp:Label></TD><TD align=left><asp:TextBox id="cashbanknote" runat="server" Width="467px" CssClass="inpText" __designer:wfdid="w149" MaxLength="95"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD id="TD4" align=left visible="false"><asp:Label id="Label11" runat="server" Width="77px" Font-Size="X-Small" Font-Bold="True" Text="Detail :" __designer:wfdid="w150" Font-Underline="True"></asp:Label></TD><TD id="TD3" align=left runat="server" visible="false"><asp:Label id="lblAcctgOid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w151"></asp:Label> <asp:Label id="cashbankglseq" runat="server" Text="cashbankglseq" Visible="False" __designer:wfdid="w152"></asp:Label> <asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False" __designer:wfdid="w153"></asp:Label> <asp:Label id="AddToList" runat="server" Font-Size="X-Small" Text="Add To List" Visible="False" __designer:wfdid="w154"></asp:Label> <asp:Label id="lblPOST" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w155"></asp:Label></TD><TD id="TD6" align=left runat="server" visible="false"><asp:Label id="Label7" runat="server" Font-Size="X-Small" Text="Cost ID" Visible="False" __designer:wfdid="w156" Font-Italic="False"></asp:Label></TD><TD id="TD5" align=left runat="server" visible="false"><asp:TextBox id="cashbankgloid" runat="server" Width="50px" CssClass="inpText" Visible="False" __designer:wfdid="w157" MaxLength="10" ReadOnly="True" Wrap="False"></asp:TextBox> <asp:LinkButton id="LinkButton2" runat="server" CssClass="submenu" Font-Size="Small" Visible="False" __designer:wfdid="w158">List</asp:LinkButton></TD></TR><TR><TD id="Td14" align=left visible="false"><asp:Label id="Label15" runat="server" Width="75px" Font-Size="X-Small" Text="COA Expense" __designer:wfdid="w159"></asp:Label> <asp:Label id="Label19" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w160"></asp:Label></TD><TD id="Td15" align=left visible="false"><asp:TextBox id="acctgoid" runat="server" Width="330px" CssClass="inpTextDisabled" __designer:wfdid="w161" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="BtnCari" onclick="ImageButton1_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w162"></asp:ImageButton></TD><TD id="Td16" align=left visible="false"></TD><TD id="Td17" align=left visible="false"></TD></TR><TR><TD id="Td18" align=left visible="false"><asp:Label id="Label14" runat="server" Font-Size="X-Small" Text="Amount" __designer:wfdid="w163"></asp:Label> <asp:Label id="Label21" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w164"></asp:Label></TD><TD id="Td19" align=left visible="false"><asp:TextBox id="cashbankglamt" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w165" AutoPostBack="True" MaxLength="20">0</asp:TextBox> <asp:Label id="lblCurr" runat="server" Font-Bold="True" Text="IDR" __designer:wfdid="w166"></asp:Label></TD><TD id="Td20" align=right visible="false"></TD><TD id="Td21" align=left visible="false"></TD></TR><TR><TD align=left visible="false"><asp:Label id="lblItemDesc" runat="server" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w167"></asp:Label></TD><TD align=left visible="false"><asp:TextBox id="cashbankglnote" runat="server" Width="452px" CssClass="inpText" __designer:wfdid="w168" MaxLength="150"></asp:TextBox></TD><TD align=right visible="false"><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w169"></asp:ImageButton></TD><TD align=left visible="false"><asp:ImageButton id="btnClear" onclick="btnClear_Click1" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w170"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 150px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div5"><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtlCost" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w171" OnSelectedIndexChanged="GVDtlCost_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankglnote,cashbankglamt,acctgOid,acctgdesc,acctgcode,seq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField HeaderText="ID" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="AcctgDesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="400px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="500px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail biaya" __designer:wfdid="w111"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></DIV></FIELDSET></TD></TR><TR><TD align=left colSpan=4><asp:Label id="lblTotExpense" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Expense : " __designer:wfdid="w172"></asp:Label> <asp:Label id="amtcost" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w173"></asp:Label></TD></TR><TR><TD align=left colSpan=4><SPAN style="FONT-SIZE: 10pt; COLOR: #696969">Last update On </SPAN><asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w174"></asp:Label> By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w175"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton style="HEIGHT: 23px" id="btnsave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:wfdid="w176"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" CausesValidation="False" __designer:wfdid="w177"></asp:ImageButton> <asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w178"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:wfdid="w179"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" Visible="False" __designer:wfdid="w180"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" runat="server" Width="66px" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="24px" Visible="False" __designer:wfdid="w181"></asp:ImageButton> <asp:Label id="createtime" runat="server" Font-Bold="True" Visible="False" __designer:wfdid="w182"></asp:Label></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w183" AssociatedUpdatePanelID="UpdatePanel5" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w184"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w185"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" Visible="False" __designer:wfdid="w186" BorderStyle="Solid"><TABLE id="Table2" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD vAlign=middle align=center colSpan=5 rowSpan=1><asp:Label id="Label1" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Akun" __designer:wfdid="w187"></asp:Label></TD></TR><TR><TD vAlign=middle colSpan=5><asp:Label id="lbldesc" runat="server" Font-Size="X-Small" Font-Bold="False" Text="Filter : " __designer:wfdid="w188"></asp:Label>&nbsp;<asp:DropDownList id="ddlFindAcctg" runat="server" CssClass="inpText" __designer:wfdid="w189"><asp:ListItem Value="acctgcode">Code</asp:ListItem>
<asp:ListItem Value="acctgdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFind" runat="server" CssClass="inpText" __designer:wfdid="w190"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind" onclick="btnFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w191"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" onclick="btnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w192"></asp:ImageButton> </TD></TR><TR><TD colSpan=5><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV id="Div1"><TABLE class="gvhdr" width="100%"><TBODY><TR><TD style="WIDTH: 64px"></TD><TD style="WIDTH: 91px">Kode</TD><TD>Deskripsi</TD></TR></TBODY></TABLE></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 95%"><asp:GridView id="GvAccCost" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w193" OnSelectedIndexChanged="GvAccCost_SelectedIndexChanged" PageSize="5" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc" GridLines="None" ShowHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="350px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!"></asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp;</DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 18px; TEXT-ALIGN: center" colSpan=5><asp:LinkButton id="LinkButton4" onclick="LinkButton4_Click" runat="server" __designer:wfdid="w194">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHidden" runat="server" Visible="False" __designer:wfdid="w195"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w196" TargetControlID="btnHidden" PopupDragHandleControlID="Label1" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="Panel1"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>

                        </ContentTemplate>
                        <HeaderTemplate>
                            <span style="font-size: 9pt">
                                <strong><span>
                                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                    Form Expense/Biaya</span></strong></span> <strong><span style="font-size: 9pt">:.</span></strong>


                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Nama COA">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
