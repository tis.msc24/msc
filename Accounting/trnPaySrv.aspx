﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPaySrv.aspx.vb" Inherits="TrnPaySrv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text="Payment Invoice Service"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel3" runat="server" Width="100%" __designer:wfdid="w32" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 90px" align=left>Cabang</TD><TD align=left colSpan=3><asp:DropDownList id="FilterCbg" runat="server" CssClass="inpText" __designer:wfdid="w45"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 90px" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode" __designer:wfdid="w1"></asp:CheckBox></TD><TD align=left colSpan=3><asp:TextBox id="txtPeriode1" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w33"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="X-Small" Text="to" __designer:wfdid="w35"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w36"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton><ajaxToolkit:CalendarExtender id="ce5" runat="server" __designer:wfdid="w38" TargetControlID="txtperiode2" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 90px" align=left>Status</TD><TD align=left colSpan=3><asp:DropDownList id="postinge" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w39">
<asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value=" ">In Process</asp:ListItem>
<asp:ListItem Value="POST">POST</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w40" Visible="False"></asp:ImageButton><asp:DropDownList id="ddlpaytype" runat="server" Width="55px" CssClass="inpText" __designer:wfdid="w41" Visible="False">
<asp:ListItem Text="CASH" Value="K"></asp:ListItem>
<asp:ListItem Value="G">GIRO</asp:ListItem>
<asp:ListItem Text="NONCASH" Value="BB"></asp:ListItem>
<asp:ListItem Value="D">DP</asp:ListItem>
<asp:ListItem Value="C">CREDIT CARD</asp:ListItem>
</asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 90px" align=left>Filter</TD><TD align=left colSpan=3><asp:DropDownList id="ddlFilter" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w42"><asp:ListItem Value="cashbankno">No. Bank</asp:ListItem>
<asp:ListItem Value="c.custname">Customer</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w43"></asp:TextBox>&nbsp;<ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w44" TargetControlID="txtPeriode2" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 90px" align=left></TD><TD align=left colSpan=3><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w46"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton>&nbsp; <ajaxToolkit:CalendarExtender id="ce4" runat="server" __designer:wfdid="w48" TargetControlID="txtperiode1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee4" runat="server" __designer:wfdid="w49" TargetControlID="txtPeriode1" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><asp:GridView id="GVmstPAYAP" runat="server" Width="950px" ForeColor="#333333" __designer:wfdid="w50" OnSelectedIndexChanged="GVmstPAYAP_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankno" GridLines="None" OnRowCommand="gridCommand" OnPageIndexChanging="GVmstPAYAP_PageIndexChanging" EnableModelValidation="True" AllowPaging="True" PageSize="8">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="No Pembayaran" SortExpression="cashbankno"><ItemTemplate>
<asp:HyperLink id="HyperLink1" runat="server" Text='<%# Eval("cashbankno") %>' __designer:wfdid="w2" NavigateUrl='<%# Eval("cashbankoid", "trnPaySrv.aspx?oid={0}") %>'></asp:HyperLink> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" ForeColor="Navy"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppliername" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="date" HeaderText="Tanggal Bayar">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payrefno" HeaderText="Pay Ref No" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Total Bayar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="No.Nota"><EditItemTemplate>
<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                                    
</EditItemTemplate>
<ItemTemplate>
<asp:GridView id="gvsubmst" runat="server" __designer:wfdid="w3" GridLines="None" BorderStyle="None" ShowHeader="False"></asp:GridView> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="cashbanknote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Data tidak ditemukan !!</asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> <asp:Label id="Label13" runat="server" Font-Bold="True" Text="Grand Total : Rp." __designer:wfdid="w51" Visible="False"></asp:Label> <asp:Label id="lblgrandtotal" runat="server" Font-Bold="True" __designer:wfdid="w52" Visible="False">0.00</asp:Label> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVmstPAYAP"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span style="font-size: 9pt"><strong> List Payment Invoice Service</strong></span><strong><span style="font-size: 9pt"> :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Payment Invoice Service</span></strong>&nbsp;<strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
<!--detail --><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 112px" align=left><asp:Label id="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text="Informasi :" Font-Underline="True" __designer:wfdid="w52"></asp:Label> </TD><TD align=left><asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Data" __designer:wfdid="w3" Visible="False"></asp:Label> </TD><TD align=left><asp:Label id="tello" runat="server" __designer:wfdid="w7" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD style="WIDTH: 112px" align=left>Cabang</TD><TD align=left><asp:DropDownList style="HEIGHT: 17px" id="DDLCabang" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w56" AutoPostBack="True" OnSelectedIndexChanged="DDLCabang_SelectedIndexChanged"></asp:DropDownList></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD style="WIDTH: 112px" align=left>Tanggal <asp:Label id="Label25" runat="server" CssClass="Important" Text="*" __designer:wfdid="w11"></asp:Label></TD><TD align=left><asp:TextBox id="PaymentDate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w9" OnTextChanged="PaymentDate_TextChanged"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPayDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w10" Visible="False" BorderColor="White"></asp:ImageButton></TD><TD align=left>Customer</TD><TD align=left><asp:TextBox id="suppnames" runat="server" Width="250px" CssClass="inpTextDisabled" __designer:wfdid="w13" AutoPostBack="True" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w14"></asp:ImageButton></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left><asp:Label id="trnsuppoid" runat="server" Font-Size="X-Small" __designer:wfdid="w51" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 112px" align=left>Pay. Type</TD><TD align=left><asp:DropDownList style="HEIGHT: 17px" id="payflag" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w12" AutoPostBack="True" Enabled="False"><asp:ListItem Value="CASH">CASH</asp:ListItem>
<asp:ListItem>BANK</asp:ListItem>
<asp:ListItem>GIRO</asp:ListItem>
<asp:ListItem>DP</asp:ListItem>
<asp:ListItem Enabled="False">CREDIT CARD</asp:ListItem>
</asp:DropDownList></TD><TD align=left>Coa <asp:Label id="lblPayType" runat="server" Text="Cash" __designer:wfdid="w16"></asp:Label></TD><TD align=left><asp:DropDownList id="cashbankacctgoid" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w15" AutoPostBack="True" OnSelectedIndexChanged="cashbankacctgoid_SelectedIndexChanged"></asp:DropDownList></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left><asp:Label id="CutofDate" runat="server" __designer:wfdid="w8" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 112px" align=left>No. Bank</TD><TD align=left><asp:TextBox id="defcbno" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w17" Enabled="False" MaxLength="20"></asp:TextBox></TD><TD align=left>Note</TD><TD align=left><asp:TextBox id="cashbanknote" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w50" AutoPostBack="True"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left><asp:Label id="lblPOST" runat="server" Font-Size="X-Small" __designer:wfdid="w4" Visible="False">In Process</asp:Label></TD></TR><TR><TD style="WIDTH: 112px" align=left></TD><TD align=left><asp:Label id="cashbankoid" runat="server" Width="50px" __designer:wfdid="w2" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left><asp:Label id="SuppName" runat="server" __designer:wfdid="w5" Visible="False"></asp:Label> <asp:Label id="cashbankno" runat="server" __designer:wfdid="w5" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD style="WIDTH: 112px" align=left><asp:Label id="Label5" runat="server" Width="60px" Font-Bold="True" ForeColor="Red" Text="Detail :" Font-Underline="True" __designer:wfdid="w206" Font-Overline="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left><asp:TextBox id="ReqCode" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w1" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 112px" id="TD28" align=left runat="server" Visible="false"><asp:Label id="I_U2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" __designer:wfdid="w207" Visible="False"></asp:Label></TD><TD id="TD21" align=left runat="server" Visible="false">&nbsp;<asp:Label id="lbltello" runat="server" __designer:wfdid="w209" Visible="False"></asp:Label> </TD><TD id="TD30" align=left runat="server" Visible="false"><asp:CheckBox id="CBTax" runat="server" Width="70px" Font-Size="X-Small" __designer:wfdid="w210" Visible="False" AutoPostBack="True" OnCheckedChanged="CBTax_CheckedChanged"></asp:CheckBox></TD><TD id="TD29" align=left runat="server" Visible="false"><asp:Label id="Payseq" runat="server" __designer:wfdid="w211" Visible="False"></asp:Label> <asp:Label id="invCurrOid" runat="server" Font-Bold="False" __designer:wfdid="w212" Visible="False"></asp:Label> <asp:Label id="invCurrCode" runat="server" __designer:wfdid="w213" Visible="False"></asp:Label> <asp:Label id="trnbelimstoid" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w59"></asp:Label></TD><TD align=left runat="server" Visible="false"></TD><TD align=left runat="server" Visible="false"></TD><TD id="TD27" align=left runat="server" Visible="false"></TD><TD id="TD22" align=left runat="server" Visible="false">&nbsp;</TD></TR><TR><TD style="WIDTH: 112px" align=left>Nota No <asp:Label id="Label26" runat="server" CssClass="Important" Text="*" __designer:wfdid="w214"></asp:Label></TD><TD align=left><asp:TextBox id="trnbelino" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w215" Enabled="False" MaxLength="20"></asp:TextBox> <asp:ImageButton id="btnSearchPurchasing" onclick="btnSearchPurchasing_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w216"></asp:ImageButton> </TD><TD align=left>A/R Account</TD><TD align=left><asp:TextBox id="APAcc" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w217" Enabled="False"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD><TD align=left><asp:Label id="acctgoid" runat="server" Font-Bold="False" __designer:wfdid="w218" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="invCurrRate" runat="server" Width="115px" CssClass="inpTextDisabled" __designer:wfdid="w221" Visible="False" Enabled="False">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 112px" align=left>Total Nota</TD><TD align=left><asp:TextBox id="amttrans" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w222" Enabled="False" MaxLength="100">0</asp:TextBox></TD><TD align=left>Pembayaran<asp:Label id="Label8" runat="server" CssClass="Important" Text="*" __designer:wfdid="w84"></asp:Label></TD><TD align=left><asp:TextBox id="amtpayment" runat="server" Width="121px" CssClass="inpText" __designer:wfdid="w227" AutoPostBack="True" OnTextChanged="amtpayment_TextChanged" MaxLength="10">0</asp:TextBox></TD><TD align=left></TD><TD align=left></TD><TD align=left><asp:Label id="Label3" runat="server" Width="68px" Text="Total Retur" __designer:wfdid="w224" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="amtretur" runat="server" Width="115px" CssClass="inpTextDisabled" __designer:wfdid="w225" Visible="False" Enabled="False" MaxLength="100">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 112px" align=left>Total Bayar</TD><TD align=left><asp:TextBox id="amtpaid" runat="server" Width="121px" CssClass="inpTextDisabled" __designer:wfdid="w223" Enabled="False" MaxLength="100">0</asp:TextBox></TD><TD align=left></TD><TD align=left>&nbsp;<asp:TextBox id="APAmt" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w226" Visible="False" Enabled="False" MaxLength="30">0</asp:TextBox></TD><TD align=left></TD><TD align=left></TD><TD id="TD15" align=left runat="server" Visible="true">Sub Total Bayar</TD><TD id="TD16" align=left runat="server" Visible="true"><asp:TextBox id="totalpayment" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w228" Enabled="False" MaxLength="30">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 112px" id="TD48" align=left runat="server" Visible="false"><asp:Label id="dp_currency" runat="server" __designer:wfdid="w52" Visible="False"></asp:Label></TD><TD id="TD49" align=left colSpan=7 runat="server" Visible="false"><asp:TextBox id="txtNote" runat="server" Width="300px" Height="10px" CssClass="inpText" __designer:wfdid="w229" Visible="False" MaxLength="200" TextMode="MultiLine"></asp:TextBox> <asp:TextBox id="invCurrDesc" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w220" Visible="False" Enabled="False"></asp:TextBox> <asp:Label id="reqperson" runat="server" __designer:wfdid="w2" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 112px" id="TD42" align=left runat="server" Visible="true"><asp:CheckBox id="chkOther" runat="server" Width="137px" Text="Selisih Pembayaran" __designer:wfdid="w230" AutoPostBack="True" Enabled="False" OnCheckedChanged="chkOther_CheckedChanged"></asp:CheckBox></TD><TD id="TD43" align=left runat="server" Visible="true"><asp:DropDownList id="DDLOtherType" runat="server" Width="155px" CssClass="inpTextDisabled" __designer:wfdid="w231" AutoPostBack="True" OnSelectedIndexChanged="DDLOtherType_SelectedIndexChanged" Enabled="False"><asp:ListItem Value="+">Kelebihan Bayar</asp:ListItem>
<asp:ListItem Value="-">Kurang Bayar</asp:ListItem>
</asp:DropDownList></TD><TD id="TD2" align=left runat="server" visible="true">Selisih</TD><TD id="TD1" align=left runat="server" visible="true"><asp:TextBox id="otheramt" runat="server" Width="129px" CssClass="inpTextDisabled" __designer:wfdid="w232" AutoPostBack="True"></asp:TextBox> <asp:DropDownList id="TaxAccount" runat="server" CssClass="inpText" __designer:wfdid="w247" Visible="False" OnSelectedIndexChanged="cashbankacctgoid_SelectedIndexChanged"></asp:DropDownList></TD><TD align=left runat="server" visible="true"></TD><TD align=left runat="server" visible="true"></TD><TD id="TD40" align=left runat="server" Visible="false"><asp:Label id="lblTaxAcc" runat="server" Text="Akun Tax" __designer:wfdid="w235" Visible="False"></asp:Label></TD><TD id="TD45" align=left runat="server" Visible="false"><asp:TextBox id="trnTaxPct" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w236" Visible="False" Enabled="False" MaxLength="20">0.00</asp:TextBox>&nbsp;<asp:TextBox id="txtPaymentNo" runat="server" Width="25px" CssClass="inpText" __designer:wfdid="w237" Visible="False" MaxLength="10" ReadOnly="True"></asp:TextBox><asp:Label id="lblTaxPct" runat="server" Text="Tax (%)" __designer:wfdid="w238" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 112px" id="TD38" align=left runat="server" Visible="false"><asp:CheckBox id="cbDP" runat="server" Text="DP" __designer:wfdid="w239" Visible="False" AutoPostBack="True" OnCheckedChanged="DP_CheckedChanged"></asp:CheckBox></TD><TD id="TD47" align=left runat="server" Visible="false"><asp:DropDownList id="ddlDPNo" runat="server" Width="155px" CssClass="inpText" __designer:wfdid="w240" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="ddlDPNo_SelectedIndexChanged"></asp:DropDownList></TD><TD id="TD44" align=left runat="server" Visible="false"><asp:Label id="lblDPAmount" runat="server" Text="Total DP" __designer:wfdid="w241" Visible="False"></asp:Label></TD><TD id="TD41" align=left runat="server" Visible="false"><asp:TextBox id="DPAmt" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w242" Visible="False" AutoPostBack="True">0.0000</asp:TextBox></TD><TD align=left runat="server" Visible="false"></TD><TD align=left runat="server" Visible="false"></TD><TD id="TD39" align=left runat="server" Visible="false"><asp:Label id="lblAmtTax" runat="server" Text="Total Tax" __designer:wfdid="w234" Visible="False"></asp:Label></TD><TD id="TD46" align=left runat="server" Visible="false"><asp:TextBox id="TaxAmount" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w233" Visible="False" MaxLength="30" ReadOnly="True">0.0000</asp:TextBox></TD></TR><TR><TD style="WIDTH: 112px" align=left></TD><TD align=left><asp:Label id="sParts" runat="server" __designer:wfdid="w3" Visible="False"></asp:Label> <asp:Label id="sJasa" runat="server" __designer:wfdid="w3" Visible="False"></asp:Label> <asp:Label id="reqoid" runat="server" __designer:wfdid="w5" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left colSpan=2></TD></TR><TR><TD id="TD37" align=left colSpan=8 runat="server" Visible="true"><asp:Label id="Label14" runat="server" Font-Bold="True" ForeColor="Red" Text="Detail Selisih Bayar :" Font-Underline="True" __designer:wfdid="w243"></asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="ftx_payamt" runat="server" __designer:wfdid="w244" ValidChars="1234567890.," TargetControlID="amtpayment"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbdpPayment" runat="server" __designer:wfdid="w245" ValidChars="1234567890.," TargetControlID="DPAmt"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteDtlselisih" runat="server" __designer:wfdid="w246" ValidChars="1234567890.," TargetControlID="amtdtlselisih"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w248" TargetControlID="payduedate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="WIDTH: 112px" id="TD34" align=left runat="server" Visible="true">Selisih COA</TD><TD id="TD33" align=left colSpan=5 runat="server" Visible="true"><asp:DropDownList id="otherAcctgoid" runat="server" Width="408px" CssClass="inpTextDisabled" __designer:wfdid="w2" Enabled="False"></asp:DropDownList>&nbsp;</TD><TD align=left colSpan=1 runat="server" Visible="true">Total Selisih<asp:Label id="Label18" runat="server" CssClass="Important" Text="*" __designer:wfdid="w9"></asp:Label></TD><TD align=left colSpan=1 runat="server" Visible="true"><asp:TextBox id="amtdtlselisih" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w3" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 112px" id="TD31" align=left runat="server" Visible="true"></TD><TD id="TD32" align=right colSpan=7 runat="server" Visible="true">&nbsp;<asp:LinkButton id="lkbAddDtlSlisih" onclick="lkbAddDtlSlisih_Click" runat="server" Font-Bold="True" __designer:wfdid="w4" Visible="False">[Tambah Detail Selisih]</asp:LinkButton> <asp:LinkButton id="lkbClearDtlSlisih" onclick="lkbClearDtlSlisih_Click" runat="server" Font-Bold="True" __designer:wfdid="w5" Visible="False">[Batal]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 112px" id="TD36" align=left runat="server" Visible="false"><asp:Label id="stateDtlSls" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Selisih" __designer:wfdid="w252" Visible="False"></asp:Label></TD><TD id="TD35" align=left colSpan=7 runat="server" Visible="false"><asp:TextBox id="dtlnoteselisih" runat="server" Width="400px" Height="10px" CssClass="inpText" __designer:wfdid="w6" Visible="False" MaxLength="200" TextMode="MultiLine"></asp:TextBox>&nbsp;</TD></TR><TR><TD id="Td50" align=left colSpan=8 runat="server" Visible="true">&nbsp;<FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 150px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvDtlSelisih" runat="server" Width="99%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w2" OnSelectedIndexChanged="gvDtlSelisih_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="selisihseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="selisihseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Selisih COA">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtlselisih" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dtlnoteselisih" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl=" " DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data selisih bayar tidak ditemukan !!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></DIV></FIELDSET></TD></TR></TBODY></TABLE><TABLE id="TABLE1" width="100%" runat="server"><TBODY><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=right><asp:ImageButton id="ibtn" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton><asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w11" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 178px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div3"></DIV>&nbsp;<DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 159px"><asp:GridView id="GVDtlPayAP" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w12" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="payseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="COA" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Pembayaran">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Total Bayar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paynote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Overline="False" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="reqcode" HeaderText="OrderNo" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqperson" HeaderText="reqperson" Visible="False"></asp:BoundField>
<asp:BoundField DataField="sParts" HeaderText="Sparts" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sJasa" HeaderText="sJasa" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="reqoid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="payres1" HeaderText="payres1" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data detail tidak ditemukan !!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="Label21" runat="server" Font-Bold="True" Text="Grand Total :  " __designer:wfdid="w9"></asp:Label> <asp:Label id="amtbelinettodtl4" runat="server" Font-Bold="True" __designer:wfdid="w10">0.0000</asp:Label></FIELDSET></TD></TR><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left>Last Updated By <asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w1"></asp:Label>&nbsp;On <asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w2"></asp:Label> </TD></TR><TR><TD align=left><TABLE width="100%"><TBODY><TR><TD id="TD3" class="Label" align=left runat="server" Visible="false">Cuurency</TD><TD id="TD4" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="CurrencyOid" runat="server" Width="92px" CssClass="inpTextDisabled" __designer:wfdid="w19" AutoPostBack="True" OnSelectedIndexChanged="CurrencyOid_SelectedIndexChanged"></asp:DropDownList></TD><TD id="TD13" class="Label" align=left runat="server" Visible="false"><asp:Label id="LblBank" runat="server" Text="Bank" __designer:wfdid="w20" Visible="False"></asp:Label></TD><TD id="TD10" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="dd_bankgiro" runat="server" CssClass="inpText" __designer:wfdid="w21" Visible="False"></asp:DropDownList> <asp:Label id="girooid" runat="server" __designer:wfdid="w22"></asp:Label></TD></TR><TR><TD id="TD11" class="Label" align=left runat="server" Visible="false">Total</TD><TD id="TD12" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="amtbelinettodtl" runat="server" Width="111px" CssClass="inpTextDisabled" __designer:wfdid="w23" Enabled="False">0</asp:TextBox> <asp:TextBox id="TotalCost" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w38" Visible="False" ReadOnly="True">0.0000</asp:TextBox></TD><TD id="TD14" class="Label" align=left runat="server" Visible="false"><asp:Label id="lblpayrefno" runat="server" Width="77px" Text="Giro Number" __designer:wfdid="w24" Visible="False"></asp:Label></TD><TD id="TD9" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="payrefno" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w25" Visible="False" MaxLength="15"></asp:TextBox></TD></TR><TR><TD id="Td5" class="Label" align=left runat="server" Visible="false"><asp:Label id="lblpayduedate" runat="server" Text="Jatuh Tempo" __designer:wfdid="w26" Visible="False"></asp:Label></TD><TD id="TD6" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w27" Visible="true"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w28" Visible="False" BorderColor="White"></asp:ImageButton> <asp:TextBox id="NetPayment" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w42" Visible="False" ReadOnly="True">0.0000</asp:TextBox></TD><TD id="Td7" class="Label" align=left runat="server" Visible="false"></TD><TD id="Td8" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="code" runat="server" Width="99px" CssClass="inpTextDisabled" __designer:wfdid="w29" Visible="False" Enabled="False" MaxLength="30"></asp:TextBox>&nbsp;<asp:ImageButton style="WIDTH: 16px" id="creditsearch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w30" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="CREDITCLEAR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w31" Visible="False"></asp:ImageButton> </TD><DIV><asp:ImageButton style="HEIGHT: 23px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w3" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w4" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton style="HEIGHT: 23px" id="btnPosting2" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w5" AlternateText="Posting"></asp:ImageButton>&nbsp;<asp:ImageButton style="WIDTH: 60px" id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w6" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:wfdid="w7"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w8" Visible="False"></asp:ImageButton></DIV></TR><TR><TD id="Td17" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label4" runat="server" Text="Total Giro" __designer:wfdid="w32" Visible="False"></asp:Label></TD><TD id="Td18" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="AmountGiro" runat="server" Width="140px" CssClass="inpTextDisabled" __designer:wfdid="w33" Visible="False" Enabled="False"></asp:TextBox> <asp:Label id="lblnotice" runat="server" Font-Size="X-Small" __designer:wfdid="w44"></asp:Label></TD><TD id="Td19" class="Label" align=left runat="server" Visible="false"><asp:Label id="lbldpbalance" runat="server" Text="DP Balance" __designer:wfdid="w34" Visible="False"></asp:Label></TD><TD id="Td20" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="dpbalance" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w35" Visible="False" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD id="Td23" class="Label" align=left runat="server" Visible="false"><asp:Label id="lblBankName" runat="server" Text="Bank Name" __designer:wfdid="w45" Visible="False"></asp:Label></TD><TD id="Td24" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="ddlBankName" runat="server" Width="151px" CssClass="inpText" __designer:wfdid="w46" Visible="False"></asp:DropDownList> <asp:TextBox id="currencyRate" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w43" Visible="False" AutoPostBack="True" OnTextChanged="currencyRate_TextChanged"></asp:TextBox></TD><TD id="Td25" class="Label" align=left runat="server" Visible="false"><asp:Label id="lbldpno" runat="server" Text="DP No" __designer:wfdid="w36" Visible="False"></asp:Label></TD><TD id="Td26" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="trndpapoid" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w37" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="trndpapoid_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" class="Label" align=left colSpan=4><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w39" TargetControlID="PaymentDate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w40" TargetControlID="PaymentDate" Format="dd/MM/yyyy" PopupButtonID="btnPayDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w41" TargetControlID="payduedate" Format="dd/MM/yyyy" PopupButtonID="btnDueDate"></ajaxToolkit:CalendarExtender><asp:HiddenField id="HiddenField2" runat="server" __designer:wfdid="w47"></asp:HiddenField> <asp:HiddenField id="HiddenField1" runat="server" __designer:wfdid="w48"></asp:HiddenField> </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD vAlign=top align=center><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w270"><ContentTemplate>
<ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w271" TargetControlID="hiddenbtn2" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel1" PopupDragHandleControlID="lblSuppdata"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2" runat="server" __designer:wfdid="w272" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD><TD vAlign=top align=left></TD></TR><TR><TD vAlign=top align=left><asp:UpdatePanel id="UpdatePanel7" runat="server" __designer:wfdid="w273"><ContentTemplate>
<asp:Panel id="Panel2" runat="server" Width="850px" CssClass="modalBox" __designer:wfdid="w274" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblPurcdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Invoice" __designer:wfdid="w275"></asp:Label></TD></TR><TR><TD align=center colSpan=3>Nota&nbsp;&nbsp;: <asp:TextBox id="txtInputNotaBeli" runat="server" CssClass="inpText" __designer:wfdid="w276"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton id="imbFindInv" onclick="imbFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w277" AlternateText="Find"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbViewAllInv" onclick="imbViewAllInv_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w278" AlternateText="View All"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" onclick="btnClearSupp_Click1" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w279" Visible="False"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=center colSpan=3><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 87%"><asp:GridView id="gvPurchasing" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w2" OnSelectedIndexChanged="gvPurchasing_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelino,suppname,trnbelidate,amttrans,amtpaid,acctgoid,currencyoid,currencyrate,currencycode,currencydesc,trntaxpct,amtretur,payduedate,reqoid,reqperson,reqcode" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="No. Invoice">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualnote" HeaderText="Note" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Total Bayar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtretur" HeaderText="Total Retur" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimstoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trntaxpct" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="nodata" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:wfdid="w15">Tidak ada data piutang !!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET> </TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="ClosePurc" onclick="ClosePurc_Click" runat="server" CausesValidation="False" Font-Bold="True" __designer:wfdid="w281">[ Close ]</asp:LinkButton></TD></TR><TR><TD vAlign=top align=center colSpan=3></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" __designer:wfdid="w282" TargetControlID="hiddenbtnpur" PopupDragHandleControlID="lblPurcdata" PopupControlID="Panel2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtnpur" runat="server" __designer:wfdid="w283" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel4s" runat="server" __designer:wfdid="w284"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w285" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Customer" __designer:wfdid="w286"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLSuppID" runat="server" CssClass="inpText" __designer:wfdid="w287"><asp:ListItem Value="custCODE">Code</asp:ListItem>
<asp:ListItem Selected="True" Value="CustName">Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindSuppID" runat="server" Width="121px" CssClass="inpText" __designer:wfdid="w288"></asp:TextBox> <asp:ImageButton id="ibtnSuppID" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w289" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAlls" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w290" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div5"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvSupplier" runat="server" Width="98%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w291" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID,Code,Name" GridLines="None" AllowSorting="True" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Blue"></HeaderStyle>

<ItemStyle ForeColor="Blue"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="ID" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Code" HeaderText="Cust. Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Name" HeaderText="Cust. Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="lblstatusdatasupp" runat="server" ForeColor="Red" Text="No Suppplier Data !" Visible="False"></asp:Label>
                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSupp" onclick="CloseSupp_Click" runat="server" CausesValidation="False" Font-Bold="False" __designer:wfdid="w292">[ Close ]</asp:LinkButton>&nbsp; </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender3s" runat="server" __designer:wfdid="w294" TargetControlID="hiddenbtn2s" PopupDragHandleControlID="lblSuppdata" PopupControlID="Panel1" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2s" runat="server" __designer:wfdid="w295" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel>&nbsp; </TD><TD align=left></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
