<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="InvoiceReportForm.aspx.vb" Inherits="ReportForm_InvoiceReportForm" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="100%">
        <tr>
            <td align="left" colspan="3" style="background-color: silver; height: 31px;">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Navy" Text=".: Laporan Invoice Service"></asp:Label></td>
        </tr>
        <tr>
            <td align="center" colspan="3" style="background-color: white">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 452px"><TBODY><TR><TD align=left><asp:Label id="Label8" runat="server" Text="Cabang"></asp:Label></TD><TD><asp:Label id="Label9" runat="server" Text=":"></asp:Label></TD><TD align=left colSpan=2><asp:DropDownList id="DDLcabang" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR>
    <tr>
        <td align="left">
            <asp:Label ID="Label10" runat="server" Text="Type Service" Width="79px"></asp:Label></td>
        <td>
            <asp:Label ID="Label11" runat="server" Text=":"></asp:Label></td>
        <td align="left" colspan="2">
            <asp:DropDownList id="DDLGaransi" runat="server" CssClass="inpText">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>Service</asp:ListItem>
                <asp:ListItem>Garansi</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <TR><TD align=left><asp:Label id="Label2" runat="server" Text="Period"></asp:Label></TD><TD><asp:Label id="Label4" runat="server" Text=":"></asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="txtPeriod1" runat="server" Width="65px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label>&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="65px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label6" runat="server" Font-Bold="True" ForeColor="Red" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label1" runat="server" Width="40px" Text="Filter"></asp:Label></TD><TD><asp:Label id="Label3" runat="server" Text=":"></asp:Label></TD><TD align=left colSpan=2><asp:DropDownList id="DDLFilter" runat="server" Width="121px" CssClass="inpText"><asp:ListItem Value="si.invoiceno">No. Invoice</asp:ListItem>
<asp:ListItem Value="rqm.reqcode">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="co.custname">Customer</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="169px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="BtnSearch" runat="server" Width="21px" ImageUrl="~/Images/search.gif" Height="21px"></asp:ImageButton> <asp:ImageButton id="BtnErase" runat="server" ImageUrl="~/Images/erase.bmp" Height="21px" Width="21px"></asp:ImageButton></TD></TR><TR><TD align=left></TD><TD></TD><TD align=left colSpan=2><asp:GridView id="GVListInvoice" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="reqcustoid,mstreqoid,soid" EmptyDataText="Data Not Found" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="White" BorderColor="White" ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="CbHdrOid" runat="server" Checked="<%# GetSessionCheckNota() %>" OnCheckedChanged="CbHdrOid_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="SelectOid" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("soid") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="invoiceno" HeaderText="No. Invoice">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqcode" HeaderText="No. TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SSTARTTIME" HeaderText="Tgl. Invoice">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SSTATUS" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqcustoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="soid" HeaderText="invoiceoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="MSTREQOID" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Status</TD><TD>:</TD><TD align=left colSpan=2><asp:DropDownList id="DDLStatus" runat="server" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnview" onclick="btnview_Click" runat="server" ImageUrl="~/Images/viewreport.png"></asp:ImageButton> <asp:ImageButton id="btnETPdf" runat="server" ImageUrl="~/Images/topdf.png" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnETExcel" runat="server" ImageUrl="~/Images/toexcel.png"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/clear.png"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><ajaxToolkit:CalendarExtender id="CPeriod1" runat="server" PopupButtonID="btnCalendar1" Format="dd/MM/yyyy" TargetControlID="txtPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CEPeriod2" runat="server" PopupButtonID="btnCalendar2" Format="dd/MM/yyyy" TargetControlID="txtPeriod2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" TargetControlID="txtPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" TargetControlID="txtPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress>&nbsp; </TD></TR></TBODY></TABLE>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvinvo" runat="server" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer>&nbsp;&nbsp; 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnETPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnETExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnview"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel2" runat="server"><contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" Width="100%" CssClass="modalMsgBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-COLOR: red" colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelErrMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel></td>
        </tr>
    </table>
    <br />
</asp:Content>

