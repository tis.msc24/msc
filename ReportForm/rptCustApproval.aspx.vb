Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptCustApproval
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"

    Public Function GetSessionCheckItem() As Boolean
        If Session("CheckItem") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetSessionCheckNota() As Boolean
        If Session("CheckNota") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - PERINGATAN"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Protected Sub CbHdrNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("NoTrans") Is Nothing Then
            Dim dtab As DataTable = Session("NoTrans")
            If dtab.Rows.Count > 0 Then
                For i As Integer = 0 To dtab.Rows.Count - 1
                    dtab.Rows(i)("selected") = sender.Checked
                Next
                dtab.AcceptChanges()
            End If
            GvNotrans.DataSource = dtab
            GvNotrans.DataBind()
            Session("NoTrans") = dtab
            If Session("CheckNota") <> True Then
                Session("CheckNota") = True
            Else
                Session("CheckNota") = False
            End If
        End If
    End Sub

    Private Sub CheckedGVNotrans()
        If Not Session("NoTrans") Is Nothing Then
            Dim dtab As DataTable = Session("NoTrans")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To GvNotrans.Rows.Count - 1
                    cb = GvNotrans.Rows(i).FindControl("CekSelect")
                    dView.RowFilter = "JualOid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("NoTrans") = dtab
        End If
    End Sub

    Private Sub CheckedGVCust()
        If Not Session("Cust") Is Nothing Then
            Dim dts As DataTable = Session("Cust")

            If dts.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dts.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To GvCust.Rows.Count - 1
                    cb = GvCust.Rows(i).FindControl("SelectOid")
                    dView.RowFilter = "custoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dts.AcceptChanges()
            End If
            Session("Cust") = dts
        End If
    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub showReport(ByVal tipe As String)
        Dim dDate1 As Date = CDate(toDate(txtPeriod1.Text))
        Dim ddate2 As Date = CDate(toDate(txtPeriod2.Text))
        Dim sWhere As String = "WHERE cp.custapprovaldate Between '" & dDate1 & " 00:00' And '" & ddate2 & " 23:59'"

        If txtPeriod1.Text.Trim <> "" And txtPeriod2.Text.Trim <> "" Then
            If dDate1 < CDate("01/01/1900") Then
                showMessage("Maaf, periode 1 belum dipilih..!!", 2)
            End If
            If ddate2 < CDate("01/01/1900") Then
                showMessage("Maaf, periode 2 belum dipilih..!!", 2)
            End If
            If dDate1 > ddate2 Then
                showMessage("Maaf, Period 2 " & txtPeriod2.Text & " harus lebih besar dari Period 1 " & txtPeriod2.Text & "..!!", 2)
            End If
        Else
            showMessage("Maaf, periode belum dipilih..!!", 2)
            Exit Sub
        End If

        '------ Filter No Transaksi ------
        '---------------------------------
        Dim dtNo As DataTable = Session("NoTrans")
        Dim OidJual As String = ""
        If GvNotrans.Visible = True Then
            If Not (Session("NoTrans") Is Nothing) Then
                If dtNo.Rows.Count > 0 Then
                    Dim dvNo As DataView = dtNo.DefaultView
                    dvNo.RowFilter = "selected='1'"
                    If dvNo.Count < 1 Then
                        showMessage("- Maaf, Filter nomer transaksi belum anda pilih, Silahkan Klik icon luv kemudian pilih no transaksi..<BR>", 2)
                        Exit Sub
                    End If
                    dvNo.RowFilter = ""
                Else
                    showMessage("- Maaf, Filter nomer transaksi belum anda pilih, Silahkan Klik icon luv kemudian pilih no transaksi..<BR>", 2)
                    Exit Sub
                End If
            End If
        End If

        If Not Session("NoTrans") Is Nothing Then
            If GvNotrans.Visible = True Then
                Dim nDt As DataView = dtNo.DefaultView
                nDt.RowFilter = "selected='1'"

                For R1 As Integer = 0 To nDt.Count - 1
                    OidJual &= nDt(R1)("JualOid").ToString & ","
                Next

                If OidJual <> "" Then
                    sWhere &= " AND cp.custapprovaloid IN (" & Left(OidJual, OidJual.Length - 1) & ")"
                End If
            End If
        End If

        Dim dtc As DataTable = Session("Cust")
        Dim OidCust As String = ""
        If GvCust.Visible = True Then
            If Not (Session("Cust") Is Nothing) Then
                If dtc.Rows.Count > 0 Then
                    Dim dvs As DataView = dtc.DefaultView
                    dvs.RowFilter = "selected='1'"
                    If dvs.Count < 1 Then
                        showMessage("- Maaf, Filter customer belum anda pilih, Silahkan Klik icon luv kemudian pilih customer..<BR>", 2)
                        Exit Sub
                    End If
                    dvs.RowFilter = ""
                Else
                    showMessage("- Maaf, Filter customer belum anda pilih, Silahkan Klik icon luv kemudian pilih customer..<BR>", 2)
                    Exit Sub
                End If
            End If
        End If

        If Not Session("Cust") Is Nothing Then
            If GvCust.Visible = True Then
                Dim cDt As DataView = dtc.DefaultView
                cDt.RowFilter = "selected='1'"

                For R1 As Integer = 0 To cDt.Count - 1
                    OidCust &= cDt(R1)("custoid").ToString & ","
                Next

                If OidCust <> "" Then
                    sWhere &= " AND cu.custoid IN (" & Left(OidCust, OidCust.Length - 1) & ")"
                End If
            End If
        End If

        If custapprovaltype.SelectedValue <> "ALL" Then
            sWhere &= " AND custapprovaltype='" & custapprovaltype.SelectedValue & "'"
        End If

        If StatusNya.SelectedValue <> "ALL" Then
            sWhere &= " AND custapprovalstatus='" & StatusNya.SelectedValue & "'"
        End If

        Try
            Dim namaFile As String = ""
            If dd_branch.SelectedValue <> "ALL" Then
                sWhere &= " AND cu.branch_code='" & dd_branch.SelectedValue & "'"
            End If

            vReport = New ReportDocument : namaFile = "CustApproval_Doc"
            If tipe = "EXCEL" Then
                vReport.Load(Server.MapPath("~\Report\rptCustApprovalExl.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptCustApproval.rpt"))
            End If

            vReport.SetParameterValue("sWhere", sWhere) 
            vReport.SetParameterValue("startperiode", txtPeriod1.Text)
            vReport.SetParameterValue("endperiode", txtPeriod2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            Session("diprint") = "True"
            If tipe = "View" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "PDF" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaFile & Format(GetServerTime, "yyMMdd"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "EXCEL" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaFile & Format(GetServerTime, "yyMMdd"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindNoTrans()
        CheckedGVCust()
        Dim dDate1 As Date = CDate(toDate(txtPeriod1.Text))
        Dim ddate2 As Date = CDate(toDate(txtPeriod2.Text))
        Try

            If dDate1 < CDate("01/01/1900") Then
                showMessage("Maaf, periode 1 belum dipilih..!!", 2)
                Exit Sub
            End If

            If ddate2 < CDate("01/01/1900") Then
                showMessage("Maaf, periode 2 belum dipilih..!!", 2)
                Exit Sub
            End If

            If dDate1 > ddate2 Then
                showMessage("Maaf, Period 2 " & txtPeriod2.Text & " harus lebih besar dari Period 1 " & txtPeriod1.Text & "..!!", 2)
                Exit Sub
            End If

            sSql = "Select DISTINCT 0 Selected,* From (Select jm.cmpcode,jm.custapprovaloid JualOid,jm.custapprovalno notrans,jm.custoid trncustoid,(Select c.custname from QL_mstcust c Where c.custoid=jm.custoid AND c.branch_code=jm.branch_code) trncustname,gc.gendesc,jm.custapprovaldate trnDate,'PIUTANG' TengerNya,0 spgoid,jm.branch_code From QL_mstcustapproval jm INNER JOIN ql_mstgen gc ON gc.gencode=jm.branch_code AND gc.gengroup='CABANG') Sd Where cmpcode='" & CompnyCode & "' AND trnDate Between '" & dDate1 & " 00:00' AND '" & ddate2 & " 23:59' AND (notrans LIKE '%" & TcharNoTrim(TxtNoTrans.Text) & "%' OR trncustname LIKE '%" & TcharNoTrim(TxtNoTrans.Text) & "%')"
            If dd_branch.SelectedValue <> "ALL" Then
                sSql &= " AND branch_code='" & dd_branch.SelectedValue & "'"
            End If
            Dim dtc As DataTable = Session("Cust")
            Dim OidCust As String = ""
            If GvCust.Visible = True Then
                If Not (Session("Cust") Is Nothing) Then
                    If dtc.Rows.Count > 0 Then
                        Dim dvs As DataView = dtc.DefaultView
                        dvs.RowFilter = "selected='1'"
                        If dvs.Count < 1 Then
                            showMessage("- Maaf, Filter customer belum anda pilih, Silahkan Klik icon luv kemudian pilih customer..<BR>", 2)
                            Exit Sub
                        End If
                        dvs.RowFilter = ""
                    Else
                        showMessage("- Maaf, Filter customer belum anda pilih, Silahkan Klik icon luv kemudian pilih customer..<BR>", 2)
                        Exit Sub
                    End If
                End If
            End If

            If Not Session("Cust") Is Nothing Then
                If GvCust.Visible = True Then
                    Dim cDt As DataView = dtc.DefaultView
                    cDt.RowFilter = "selected='1'"

                    For R1 As Integer = 0 To cDt.Count - 1
                        OidCust &= cDt(R1)("custoid").ToString & ","
                    Next

                    If OidCust <> "" Then
                        sSql &= " AND trncustoid IN (" & Left(OidCust, OidCust.Length - 1) & ")"
                    End If
                End If
            End If

            sSql &= "Order By JualOid,trnDate Asc"

            Dim dtab As DataTable = ckon.ambiltabel(sSql, "QL_NoTrans")
            GvNotrans.DataSource = dtab : GvNotrans.DataBind()
            Session("NoTrans") = dtab : GvNotrans.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try

    End Sub

    Private Sub BindCust()
        Dim dDate1 As Date = CDate(toDate(txtPeriod1.Text))
        Dim ddate2 As Date = CDate(toDate(txtPeriod2.Text))

        Try
            If dDate1 < CDate("01/01/1900") Then
                showMessage("Maaf, periode 1 belum dipilih..!!", 2)
                Exit Sub
            End If

            If ddate2 < CDate("01/01/1900") Then
                showMessage("Maaf, periode 2 belum dipilih..!!", 2)
                Exit Sub
            End If

            If dDate1 > ddate2 Then
                showMessage("Maaf, Period 2 " & txtPeriod2.Text & " harus lebih besar dari Period 1 " & txtPeriod1.Text & "..!!", 2)
                Exit Sub
            End If

            sSql = "Select Distinct 0 Selected,jm.custoid,cu.custcode,custname,custaddr,(Select gendesc from ql_mstgen cg Where cg.gencode=cu.branch_code AND cg.gengroup='CABANG') Cabang From QL_mstcust cu Inner Join QL_mstcustapproval jm ON jm.custoid=cu.custoid AND jm.branch_code=cu.branch_code Where cu.cmpcode='" & CompnyCode & "' AND cu.custname LIKE '%" & TcharNoTrim(CustName.Text) & "%' AND jm.custapprovaldate Between '" & dDate1 & " 00:00' AND '" & ddate2 & " 23:59'"
            If dd_branch.SelectedValue <> "ALL" Then
                sSql &= " AND cu.branch_code='" & dd_branch.SelectedValue & "'"
            End If

            Dim dtc As DataTable = ckon.ambiltabel(sSql, "QL_NoTrans")
            GvCust.DataSource = dtc : GvCust.DataBind()
            Session("Cust") = dtc : GvCust.Visible = True

        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Session.Clear()  ' -->>  clear all session 
            Session("SpecialAccess") = xsetAcc
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Response.Redirect("~\ReportForm\rptCustApproval.aspx")
        End If

        Page.Title = CompnyName & " - Laporan History Price Jual"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            txtPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")

            sSql = "Select gencode,gendesc From QL_mstgen Where gengroup = 'CABANG'"
            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_branch, sSql)
            ElseIf Session("UserLevel") = 2 Then
                If Session("branch_id") <> "10" Then
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(dd_branch, sSql)
                Else
                    FillDDL(dd_branch, sSql)
                    dd_branch.Items.Add(New ListItem("ALL", "ALL"))
                    dd_branch.SelectedValue = "ALL"
                End If
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                sSql &= "" : FillDDL(dd_branch, sSql)
                dd_branch.Items.Add(New ListItem("ALL", "ALL"))
                dd_branch.SelectedValue = "ALL"
            End If
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptCustApproval.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        showReport("EXCEL")
    End Sub  

    Protected Sub ibpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckedGVNotrans() : showReport("PDF")
    End Sub

    Protected Sub btnreport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnreport.Click
        CheckedGVCust() : CheckedGVNotrans()
        showReport("View")
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        CheckedGVNotrans() : showReport("View")
    End Sub

    Protected Sub eBtnNoTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eBtnNoTrans.Click
        GvNotrans.Visible = False : TxtNoTrans.Text = ""
    End Sub

    Protected Sub GvNotrans_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvNotrans.PageIndexChanging
        CheckedGVNotrans()
        GvNotrans.PageIndex = e.NewPageIndex
        Dim NoTrn As DataTable = Session("NoTrans")
        GvNotrans.DataSource = NoTrn
        GvNotrans.DataBind()
    End Sub

    Protected Sub sBtnNoTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sBtnNoTrans.Click
        CheckedGVNotrans() : BindNoTrans()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub 

    Protected Sub GvNotrans_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvNotrans.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            Dim cb As System.Web.UI.WebControls.CheckBox
            cb = e.Row.FindControl("CbHdrNo")
            cb.Checked = GetSessionCheckNota()
        End If
    End Sub

    Protected Sub GvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvCust.PageIndexChanging
        CheckedGVCust()
        GvCust.PageIndex = e.NewPageIndex
        Dim OidCust As DataTable = Session("Cust")
        GvCust.DataSource = OidCust
        GvCust.DataBind()
    End Sub

    Protected Sub CbHdrOid_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("Cust") Is Nothing Then
            Dim dts As DataTable = Session("Cust")
            If dts.Rows.Count > 0 Then
                For i As Integer = 0 To dts.Rows.Count - 1
                    dts.Rows(i)("selected") = sender.Checked
                Next
                dts.AcceptChanges()
            End If

            GvCust.DataSource = dts : GvCust.DataBind()

            Session("Cust") = dts
            If Session("CheckCust") <> True Then
                Session("CheckCust") = True
            Else
                Session("CheckCust") = False
            End If
        End If
    End Sub

    Protected Sub sCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sCust.Click
        CheckedGVCust() : BindCust()
    End Sub

    Protected Sub eCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eCust.Click
        CustName.Text = "" : GvCust.Visible = False
        GvCust.SelectedIndex = -1
    End Sub
#End Region
End Class
