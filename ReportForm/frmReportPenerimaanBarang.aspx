<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmReportPenerimaanBarang.aspx.vb" Inherits="ReportForm_frmReportPenerimaanBarang" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 984px; height: 40px">
        <tr>
            <td align="left" style="height: 45px; background-color: silver">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="Maroon"
                    Text=":: Laporan Penerimaan Service"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE style="WIDTH: 712px; HEIGHT: 72px"><TBODY><TR><TD style="HEIGHT: 22px" align=left>Filter</TD><TD style="HEIGHT: 22px">:</TD><TD style="HEIGHT: 22px" align=left><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w114"><asp:ListItem Value="r.reqcode">No. Tanda Masuk</asp:ListItem>
<asp:ListItem Value="r.barcode">Barcode</asp:ListItem>
<asp:ListItem Value="c.custname">Nama Customer</asp:ListItem>
<asp:ListItem Value="c.phone1">Telepon1</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="176px" CssClass="inpText" __designer:wfdid="w115"></asp:TextBox></TD></TR><TR><TD align=left>Periode</TD><TD>:</TD><TD align=left><asp:TextBox id="txtTgl1" runat="server" Width="72px" CssClass="inpText" __designer:wfdid="w116"></asp:TextBox>&nbsp;<asp:ImageButton id="periode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w117"></asp:ImageButton> to <asp:TextBox id="txtTgl2" runat="server" Width="72px" CssClass="inpText" __designer:wfdid="w118"></asp:TextBox>&nbsp;<asp:ImageButton id="periode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" CssClass="inpText" __designer:wfdid="w119"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w120"></asp:Label></TD></TR><TR><TD><asp:RadioButtonList id="cbstatus" runat="server" __designer:wfdid="w1" RepeatDirection="Horizontal"><asp:ListItem Selected="True">Status</asp:ListItem>
<asp:ListItem>Paid</asp:ListItem>
</asp:RadioButtonList></TD><TD>:</TD><TD><asp:DropDownList id="StatusDDL" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w122"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In</asp:ListItem>
<asp:ListItem>Out</asp:ListItem>
<asp:ListItem>Receive</asp:ListItem>
<asp:ListItem>Check</asp:ListItem>
<asp:ListItem>CheckOut</asp:ListItem>
<asp:ListItem>Send</asp:ListItem>
<asp:ListItem>Ready</asp:ListItem>
<asp:ListItem>Close</asp:ListItem>
<asp:ListItem>Start</asp:ListItem>
<asp:ListItem>Finish</asp:ListItem>
<asp:ListItem>Final</asp:ListItem>
<asp:ListItem>Invoiced</asp:ListItem>
<asp:ListItem>SPK</asp:ListItem>
<asp:ListItem>SPKClose</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD colSpan=3></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnView" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w123"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w124" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w125" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w126"></asp:ImageButton></TD></TR><TR><TD colSpan=3></TD></TR><TR><TD style="HEIGHT: 59px" colSpan=3><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><CR:CrystalReportViewer id="CRVPenerimaanBarang" runat="server" __designer:wfdid="w127" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer></TD></TR><TR><TD colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w128" Format="dd/MM/yyyy" TargetControlID="txtTgl2" PopupButtonID="periode2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w129" Format="dd/MM/yyyy" TargetControlID="txtTgl1" PopupButtonID="periode1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w130" TargetControlID="txtTgl1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w131" TargetControlID="txtTgl2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnView"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel></td>
        </tr>
    </table>
    <br />
    &nbsp;&nbsp;
    <br />
    <asp:UpdatePanel id="UpdatePanel2" runat="server">
        <contenttemplate>
<asp:Panel id="panelMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE style="WIDTH: 99%; HEIGHT: 109%"><TBODY><TR><TD style="HEIGHT: 18px; BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsg" runat="server" __designer:wfdid="w24" TargetControlID="btnMsg" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" DropShadow="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>&nbsp;&nbsp; <asp:Button id="btnMsg" runat="server" __designer:wfdid="w25" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

