<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptTTS.aspx.vb" Inherits="ReportForm_rptSalesRetur"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" style="width: 976px; height: 168px">
        <tr>
            <td colspan="3" rowspan="3" style="width: 100%; vertical-align: top; text-align: center;">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                                Text=".: Laporan Tanda Terima Sementara" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                </table>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" colSpan=3></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Periode :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" colSpan=3><asp:TextBox id="range1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w1"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Nomor TTS :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:TextBox id="ttsno" runat="server" CssClass="inpText" __designer:wfdid="w14"></asp:TextBox>&nbsp;<asp:ImageButton id="ttssearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w15"></asp:ImageButton>&nbsp;<asp:ImageButton id="ttserase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:Label id="ttsoid" runat="server" __designer:wfdid="w17" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 0px; POSITION: relative; TOP: 2px; BACKGROUND-COLOR: transparent" id="GVTts" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w18" Visible="False" DataKeyNames="TTSNo,TTSDate,trnsjjualno,custname,trnjualno,custoid" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" PageSize="14">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="TTSNo" HeaderText="Nomor TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TTSDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="Nomor SO">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualno" HeaderText="Nomor Sales DO">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Moccasin" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Nomor&nbsp;Order Penjualan&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:TextBox id="orderno" runat="server" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="ordersearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:ImageButton id="ordererase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:Label id="orderoid" runat="server" __designer:wfdid="w6" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 0px; POSITION: relative; TOP: 2px; BACKGROUND-COLOR: transparent" id="GVOrder" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w7" Visible="False" DataKeyNames="orderno,trnorderdate,custname" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" PageSize="14">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="Nomor SO">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal SO">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Moccasin" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Nomor&nbsp;Sales DO&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:TextBox id="dono" runat="server" CssClass="inpText" __designer:wfdid="w8"></asp:TextBox>&nbsp;<asp:ImageButton id="dosearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;<asp:ImageButton id="doerase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton>&nbsp;<asp:Label id="dooid" runat="server" __designer:wfdid="w6" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 0px; POSITION: relative; TOP: 2px; BACKGROUND-COLOR: transparent" id="GVDo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w7" Visible="False" DataKeyNames="trnsjjualno,trnsjjualdate,custname" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" PageSize="14">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjjualno" HeaderText="Nomor Sales DO">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal Sales DO">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Moccasin" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Barang :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:TextBox id="barang" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w19"></asp:TextBox>&nbsp;<asp:ImageButton id="barangsearch" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w20"></asp:ImageButton>&nbsp;<asp:ImageButton id="barangerase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w21"></asp:ImageButton>&nbsp;<asp:Label id="barangoid" runat="server" __designer:wfdid="w22" Visible="False"></asp:Label> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVBarang" runat="server" Width="100%" __designer:wfdid="w23" Visible="False" DataKeyNames="itemoid,itemcode,itemdesc,Merk" AutoGenerateColumns="False" CellPadding="4" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label150" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Moccasin" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Merk&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:TextBox id="merk" runat="server" Width="169px" CssClass="inpTextDisabled" __designer:wfdid="w24" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Customer&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:TextBox id="custname" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w25"></asp:TextBox>&nbsp;<asp:ImageButton id="custsearch" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="17px" __designer:wfdid="w26"></asp:ImageButton>&nbsp;<asp:ImageButton id="custerase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton>&nbsp;<asp:Label id="custoid" runat="server" __designer:wfdid="w28" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVCustomer" runat="server" Width="100%" __designer:wfdid="w29" Visible="False" DataKeyNames="custoid,custcode,custname" AutoGenerateColumns="False" CellPadding="4" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Kode Customer">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama Customer">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Moccasin" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center" align=center colSpan=4><BR /><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnshowprint" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton> <BR /><BR /><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w40"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait ....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w41"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD colSpan=4><ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w201" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'." TargetControlID="range1" PopupButtonID="ImageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w202" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'." TargetControlID="range2" PopupButtonID="ImageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w203" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." TargetControlID="range1" Mask="99/99/9999" ErrorTooltipEnabled="True" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w204" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." TargetControlID="range2" Mask="99/99/9999" ErrorTooltipEnabled="True" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w42"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=4><TABLE style="MARGIN: auto"><TBODY><TR><TD style="TEXT-ALIGN: left"><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w270" DisplayGroupTree="False" HasZoomFactorList="False" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE> </TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnshowprint"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    <tr>
        <td colspan="3" rowspan="1" style="vertical-align: top; width: 100%">
            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width=250><TBODY><TR><TD style="WIDTH: 294px; HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=250><TBODY><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" __designer:wfdid="w9"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelErrMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
            </asp:UpdatePanel></td>
    </tr>
    </table>
</asp:Content>

