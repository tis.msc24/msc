<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSalesItem.aspx.vb" Inherits="ReportForm_rptSalesItem"%>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Text=".: Penjualan Bersih Item Per Sales" Font-Size="Large"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD id="TD2" align=left runat="server" Visible="false"><asp:Label id="Label2" runat="server" Width="100px" Text="Tipe Transaksi"></asp:Label></TD><TD id="TD1" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="type" runat="server" Width="78px" CssClass="inpText" AutoPostBack="True"><asp:ListItem>SI</asp:ListItem>
<asp:ListItem Value="%">ALL</asp:ListItem>
<asp:ListItem>POS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server" Visible="true">Type</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:RadioButtonList id="rblType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rblType_SelectedIndexChanged" RepeatDirection="Horizontal"><asp:ListItem Selected="True" Value="sales">Per Sales</asp:ListItem>
<asp:ListItem Value="pic">Per PIC</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;- <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/MM/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="dateAwal" PopupButtonID="imageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="dateAkhir" PopupButtonID="imageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD id="Td4" align=left runat="server" Visible="true">Cabang</TD><TD id="Td5" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="DDLcabang" runat="server" Width="156px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="DDLcabang_SelectedIndexChanged"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td6" align=left runat="server" Visible="true"><asp:Label id="sPic" runat="server" Text="Sales" __designer:wfdid="w1"></asp:Label></TD><TD id="Td7" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="pic" runat="server" Width="156px" CssClass="inpText" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td8" align=left runat="server" Visible="true">Item</TD><TD id="Td9" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="custname" runat="server" Width="151px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="oid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="Td10" align=right runat="server" visible="true"></TD><TD id="Td11" align=left colSpan=3 runat="server" visible="true"><asp:GridView id="GVCust" runat="server" Width="100%" ForeColor="#333333" Visible="False" CellPadding="4" DataKeyNames="itemoid,itemdesc" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField>
<ItemTemplate>
<asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("itemoid") %>' />
</ItemTemplate>
<HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
<ItemStyle HorizontalAlign="Left" />
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD id="Td12" align=left runat="server" visible="true"><asp:Label id="Label3" runat="server" Text="Product"></asp:Label></TD><TD id="Td13" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="ddlproduct" runat="server" Width="150px" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>UNGGULAN</asp:ListItem>
<asp:ListItem>NON UNGGULAN</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td14" align=left runat="server" visible="true"><asp:Label id="Label1" runat="server" Text="Status"></asp:Label></TD><TD id="Td15" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td17" align=left colSpan=4 runat="server" visible="true"><asp:ImageButton id="btnView" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnXls" onclick="btnXls_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnclear" onclick="btnclear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="Td16" align=center colSpan=4 runat="server" visible="true"><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD align=center colSpan=4 runat="server" visible="true"><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w1" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="imbProgress" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvReportForm" runat="server" Width="350px" Height="50px" DisplayGroupTree="False" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnXLS"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPDF"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
<asp:UpdatePanel ID="upPopUpMsg" runat="server">
<ContentTemplate>
<asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
<table>
<tbody>
<tr>
<td colspan="2" style="background-color: #cc0000; text-align: left">
<asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
</tr>
<tr>
<td colspan="2" style="height: 10px">
</td>
</tr>
<tr>
<td>
<asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
Width="24px" /></td>
<td class="Label" style="text-align: left">
<asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
</tr>
<tr>
<td colspan="2" style="height: 10px; text-align: center">
</td>
</tr>
<tr>
<td colspan="2" style="text-align: center">
&nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
</tr>
</tbody>
</table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
TargetControlID="bePopUpMsg">
</ajaxToolkit:ModalPopupExtender>
<asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
</ContentTemplate>
</asp:UpdatePanel>
</th>
        </tr>
    </table>
    &nbsp;
</asp:Content>

