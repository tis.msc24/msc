﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnSJ.aspx.vb" Inherits="Transaction_trnSJ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
  

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%" onclick="return tbRight_onclick()">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" oreColor="Maroon" Text=".: SJ GLOBAL"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">



                        
<ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
<asp:Panel style="MARGIN-TOP: 4px" id="pnlFind" runat="server" __designer:wfdid="w241" DefaultButton="btnfind"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Filter :</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w242">
                                                            <asp:ListItem Value="sjglobalno">No</asp:ListItem>
                                                            <asp:ListItem Value="note">Note</asp:ListItem>
                                                        </asp:DropDownList>&nbsp;<asp:TextBox id="txtFindSJ" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w243"></asp:TextBox>&nbsp;&nbsp;</TD></TR><TR><TD class="Label" align=left>Cabang :</TD><TD align=left colSpan=4><asp:DropDownList id="ddlcabang1" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w244"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Periode :</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w245"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w246"></asp:ImageButton>&nbsp;<asp:Label id="Label16" runat="server" Text="to" __designer:wfdid="w247"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w248"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w249"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w250"></asp:Label> <asp:CheckBox id="chkUpdateVSTrans" runat="server" Text="Tanggal Transaksi ≠ Tanggal Posting" __designer:wfdid="w251" AutoPostBack="False"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w252" Checked="True"></asp:CheckBox></TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w253"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton style="HEIGHT: 23px" id="btnfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w254"></asp:ImageButton><asp:ImageButton id="btnviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w255"></asp:ImageButton>&nbsp;</TD></TR><TR><TD align=left colSpan=5><asp:GridView id="gvTblData" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w234" DataKeyNames="sjglobalmstoid,branch_code" PageSize="8" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="sjglobalmstoid" DataNavigateUrlFormatString="~\transaction\trnSJ.aspx?oid={0}" DataTextField="sjglobalno" HeaderText="No" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:HyperLinkField>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sjglobalno" HeaderText="No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="tglSj" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl SJ">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="kepada" HeaderText="Kepada">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="toAdderes" HeaderText="Kirim Ke">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="statusSjglobal" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note" HtmlEncode="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" __designer:wfdid="w12" ToolTip='<%# Eval("sjglobalmstoid")%>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" Text='<%# Eval("sjglobalmstoid")%>' __designer:wfdid="w11"></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                            <asp:Label ID="Label3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" BorderStyle="None" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w257" TargetControlID="FilterPeriod2" PopupButtonID="imbPeriod2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w258" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w259" TargetControlID="FilterPeriod1" PopupButtonID="imbPeriod1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w260" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                        </ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="gvTblData"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

                        
</ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> List of Travel Document</span></strong>&nbsp;<strong><span
                                style="font-size: 9pt">:.</span></strong>
</HeaderTemplate>



                    
</ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">



                        
<ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="Label4" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Header" __designer:wfdid="w58"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="oid" runat="server" Font-Size="X-Small" __designer:wfdid="w59" Visible="False"></asp:Label> <asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New" __designer:wfdid="w60" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left></TD><TD style="FONT-SIZE: 8pt" align=left>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD class="Label" align=left>Tanggal&nbsp; </TD><TD style="WIDTH: 294px" align=left><asp:TextBox id="trnBerlakudate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w61" Enabled="False"></asp:TextBox> <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w62" Visible="False"></asp:ImageButton> <asp:Label id="Label11" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w63"></asp:Label></TD><TD class="Label" align=left>Kepada</TD><TD align=left><asp:TextBox id="Pengirim" runat="server" Width="200px" Height="16px" CssClass="inpText" __designer:wfdid="w64" MaxLength="300"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>No. SJ </TD><TD style="WIDTH: 294px" align=left><asp:TextBox id="trnsjjualno" runat="server" Width="187px" CssClass="inpTextDisabled" __designer:wfdid="w65" Enabled="False" MaxLength="20" ReadOnly="True"></asp:TextBox> </TD><TD class="Label" align=left>Alamat <asp:Label id="Label68" runat="server" CssClass="Important" Text="*" __designer:wfdid="w66"></asp:Label> </TD><TD align=left><asp:TextBox id="Tujaun" runat="server" Width="200px" Height="20px" CssClass="inpText" __designer:wfdid="w67" MaxLength="400" TextMode="MultiLine"></asp:TextBox> </TD></TR><TR><TD class="Label" vAlign=top align=left>Kami <asp:Label id="Label67" runat="server" CssClass="Important" Text="*" __designer:wfdid="w68"></asp:Label> </TD><TD style="WIDTH: 294px" vAlign=top align=left><asp:DropDownList id="DDLPengirim" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w69">
                                                            </asp:DropDownList> </TD><TD class="Label" vAlign=top align=left>Kota</TD><TD align=left><asp:DropDownList id="DDLKota" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w70">
                                                            </asp:DropDownList> </TD></TR><TR><TD class="Label" vAlign=top align=left>No Kendaraan</TD><TD style="WIDTH: 294px" vAlign=top align=left><asp:TextBox id="nopolisi" runat="server" Width="187px" CssClass="inpText" __designer:wfdid="w71" MaxLength="10"></asp:TextBox> </TD><TD class="Label" vAlign=top align=left>Keterangan</TD><TD align=left><asp:TextBox id="NoteMst" runat="server" Width="200px" Height="38px" CssClass="inpText" __designer:wfdid="w72" MaxLength="500" TextMode="MultiLine"></asp:TextBox> </TD></TR><TR><TD class="Label" align=left><asp:LinkButton id="LinkButton10" runat="server" Width="83px" CssClass="submenu" Font-Size="Small" Font-Bold="True" ForeColor="Black" __designer:wfdid="w73">Detail</asp:LinkButton> </TD><TD style="WIDTH: 294px" align=left>&nbsp;<asp:TextBox id="posting" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w74" Visible="False" ReadOnly="True">In Process</asp:TextBox> <asp:Label id="seqglobal" runat="server" Font-Size="X-Small" __designer:wfdid="w75" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="I_u2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" __designer:wfdid="w76" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="SJdate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w77" Visible="False" Enabled="False"></asp:TextBox> <asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w78" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left>Nama Barang <asp:Label id="Label6" runat="server" CssClass="Important" Text="*" __designer:wfdid="w79"></asp:Label> </TD><TD style="WIDTH: 294px" align=left><asp:TextBox id="itemname" runat="server" Width="219px" Height="16px" CssClass="inpText" __designer:wfdid="w80" MaxLength="400"></asp:TextBox> <asp:ImageButton id="imbFindItem" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w81" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbClearItem" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w82" Visible="False"></asp:ImageButton> </TD><TD align=left>Quantity <asp:Label id="Label9" runat="server" CssClass="Important" Text="*" __designer:wfdid="w83"></asp:Label> </TD><TD align=left><asp:TextBox id="QtySJ" runat="server" Width="54px" CssClass="inpText" __designer:wfdid="w84" AutoPostBack="True" MaxLength="4"></asp:TextBox> <asp:DropDownList id="DDLUnit" runat="server" CssClass="inpText" __designer:wfdid="w85">
                                                            </asp:DropDownList> &nbsp; </TD></TR><TR><TD align=left>&nbsp;</TD><TD align=left colSpan=3><asp:GridView id="gvReference" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w86" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" GridLines="None" PageSize="8" Visible="False" DataKeyNames="itemoid,itemcode,itemdesc,Merk">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="10px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label66" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" vAlign=top align=left>Collie</TD><TD style="WIDTH: 294px" vAlign=top align=left><asp:TextBox id="TextCollie" runat="server" CssClass="inpText" __designer:wfdid="w87"></asp:TextBox> </TD><TD vAlign=top align=left>Note</TD><TD align=left><asp:TextBox id="txtLenght" runat="server" Width="221px" Height="31px" CssClass="inpText" __designer:wfdid="w88" AutoPostBack="True" MaxLength="300" TextMode="MultiLine"></asp:TextBox> </TD></TR><TR><TD class="Label" align=left>&nbsp;</TD><TD style="WIDTH: 294px" align=left><asp:ImageButton id="btnAddtolist" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w89"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w90"></asp:ImageButton> </TD><TD align=left><ajaxToolkit:FilteredTextBoxExtender id="ftCollie" runat="server" __designer:wfdid="w91" TargetControlID="TextCollie" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender></TD><TD align=left><ajaxToolkit:FilteredTextBoxExtender id="SjQty" runat="server" __designer:wfdid="w92" TargetControlID="QtySJ" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="tbldtl" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w93" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seqglobal" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="item" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Collie" HeaderText="Collie">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notedtl" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton9" runat="server" __designer:wfdid="w2" CommandArgument='<%# Eval("item")%>' CommandName='<%# Eval("qty")%>' ToolTip='<%# Eval("seqglobal")%>'>Detail</asp:LinkButton>
                                                                        
</ItemTemplate>
</asp:TemplateField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="5%"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=4><asp:Label id="lbltext" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Create" __designer:wfdid="w94"></asp:Label> &nbsp;By <asp:Label id="createuser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="createuser" __designer:wfdid="w95"></asp:Label> &nbsp;On <asp:Label id="createtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="createtime" __designer:wfdid="w96"></asp:Label> <BR /><asp:Label id="lbltext0" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Last update" __designer:wfdid="w97"></asp:Label> &nbsp; By <asp:Label id="updUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Upduser" __designer:wfdid="w98"></asp:Label> &nbsp;On <asp:Label id="updTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="Updtime" __designer:wfdid="w99"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w100"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w101"></asp:ImageButton> <asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w102"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w103"></asp:ImageButton> <asp:ImageButton id="imbSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" __designer:wfdid="w104" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrintSJ" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w105"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w106" AssociatedUpdatePanelID="UpdatePanel2" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w107"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:MaskedEditExtender id="meesend" runat="server" __designer:wfdid="w108" TargetControlID="trnBerlakudate" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cesend" runat="server" __designer:wfdid="w109" TargetControlID="trnBerlakudate" Format="dd/MM/yyyy" PopupButtonID="ImageButton1"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnPrintSJ"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

                        
</ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Travel Document</span></strong>&nbsp;<strong><span
                                style="font-size: 9pt">:.</span></strong>
</HeaderTemplate>



                    
</ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;<asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Visible="False" CssClass="modalMsgBox" DefaultButton="btnMsgBoxOK">
                <table>
                    <tr>
                        <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" CssClass="Important"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Visible="False" CausesValidation="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

