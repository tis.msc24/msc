Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class MstRole
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If rolename.Text = "" Then
            sError &= "- Please fill ROLE NAME field!<BR>"
        End If
        If roledesc.Text = "" Then
            sError &= "- Please fill DESCRIPTION field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function UpdateChecked() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblRole") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblRole")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvRole.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvRole.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "genoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblRole") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindRoleForm()
        sSql = "Select 'False' Checkvalue,genoid,genother2 FormType,gendesc FormName,genother1 FormAddress from QL_mstgen Where gengroup='FORMNAME' AND (genother2 LIKE '%" & Tchar(TxtFormType.Text) & "%' Or gendesc Like '%" & Tchar(TxtFormType.Text) & "%' Or genother1 LIke '%" & Tchar(TxtFormType.Text) & "%')"
        Dim sNone As String = ""
        If Session("TabelDtlRole") IsNot Nothing Then
            Dim dt As DataTable = Session("TabelDtlRole")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                sNone &= "'" & dt.Rows(C1)("formname").ToString & "',"
            Next
        End If
        If sNone <> "" Then
            sSql &= " AND gendesc NOT IN (" & Left(sNone, sNone.Length - 1) & ")"
        End If
        sSql &= " ORDER BY genother2,gendesc"

        Dim dtForm As DataTable = cKon.ambiltabel(sSql, "QL_mstRole")
        Session("TblRole") = dtForm
        Session("TblRoleView") = Session("TblRole")
        gvRole.DataSource = Session("TblRoleView")
        gvRole.DataBind() : gvRole.Visible = True
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT roleoid, rolename, [DESCRIPTION] AS roledesc,[UPDUSER], [UPDTIME] FROM QL_ROLE WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%' ORDER BY rolename"
        'If checkPagePermission("~\Master\mstRole.aspx", Session("SpecialAccess")) = False Then
        '    sSql &= " AND createuser='" & Session("UserID") & "'"
        'End If
        sSql &= " "
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstrole")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            ' Fill Data Master
            sSql = "SELECT roleoid, rolename,[DESCRIPTION] AS roledesc,upduser AS createuser,UPDTIME AS createtime, upduser, updtime FROM QL_role WHERE cmpcode='" & CompnyCode & "' AND roleoid=" & Session("oid")
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Enabled = False
            While xreader.Read
                roleoid.Text = xreader("roleoid").ToString
                rolename.Text = xreader("rolename").ToString
                roledesc.Text = xreader("roledesc").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            ' Fill Data Detail
            sSql = "SELECT formtype, formname, formaddress,upduser AS createuser, upduser, updtime FROM QL_roledtl WHERE cmpcode='" & CompnyCode & "' AND roleoid=" & Session("oid") & " ORDER BY formtype, formname"
            Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_mstroledtl")
            Session("TabelDtlRole") = objTable
            gvDtl.DataSource = Session("TabelDtlRole")
            gvDtl.DataBind()
            imbRemove.Visible = (objTable.Rows.Count > 0)

            ' Fill Data User Role
            If Session("TblDtlUserRole") Is Nothing Then
                CreateTblDetailUser()
            End If
            sSql = "SELECT DISTINCT ur.cmpcode, ur.USERPROF AS profoid, p.USERNAME AS profname, ur.special, ur.upduser createuser, ur.UPDTIME as createtime, ur.upduser, ur.updtime FROM QL_userrole ur INNER JOIN QL_mstprof p ON ur.cmpcode=p.cmpcode AND ur.USERPROF=p.USERID WHERE ur.roleoid=" & roleoid.Text & ""
            Dim objTableUser As DataTable = cKon.ambiltabel2(sSql, "QL_mstuserrole")
            Session("TblDtlUserRole") = objTableUser
            gvUserRole.DataSource = Session("TblDtlUserRole")
            gvUserRole.DataBind()
            Dim objTbl As DataTable = Session("TabelDtlRole")
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            xreader.Close()
            conn.Close()
            btnDelete.Enabled = True
            TD1.Visible = True
            'TD2.Visible = True
            TD3.Visible = True
        End Try
    End Sub

    Private Sub FillDDLFormName(ByVal sFilter As String)
        sSql = "SELECT gendesc As Value, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='FORMNAME' AND genother2='" & Tchar(sFilter) & "'"
        Dim sNone As String = ""
        If Session("TabelDtlRole") IsNot Nothing Then
            Dim dt As DataTable = Session("TabelDtlRole")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                sNone &= "'" & dt.Rows(C1)("formname").ToString & "',"
            Next
        End If
        If sNone <> "" Then
            sSql &= " AND gendesc NOT IN (" & Left(sNone, sNone.Length - 1) & ")"
        End If
        sSql &= " ORDER BY gendesc"
        FillDDL(listformname, sSql)
    End Sub

    Private Sub FillDDLFormType()
        sSql = "SELECT DISTINCT genother2 As Value, genother2 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='FORMNAME' ORDER BY genother2"
        FillDDL(listformtype, sSql)
    End Sub

    Private Sub FillDDLFormModule()
        sSql = "SELECT DISTINCT genother3 As Value, genother3 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='FORMNAME' ORDER BY genother3"
        FillDDL(listformmodule, sSql)
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TblDtlRole")
        dtlTable.Columns.Add("formtype", Type.GetType("System.String"))
        dtlTable.Columns.Add("formname", Type.GetType("System.String"))
        dtlTable.Columns.Add("formaddress", Type.GetType("System.String"))
        dtlTable.Columns.Add("createuser", Type.GetType("System.String"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Session("TabelDtlRole") = dtlTable
    End Sub

    Private Sub CreateTblDetailUser()
        Dim dtlTable As DataTable = New DataTable("TblDtlUserRole")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("profoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("profname", Type.GetType("System.String"))
        dtlTable.Columns.Add("special", Type.GetType("System.String"))
        dtlTable.Columns.Add("createuser", Type.GetType("System.String"))
        dtlTable.Columns.Add("createtime", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Session("TblDtlUserRole") = dtlTable
    End Sub

    Private Sub PrintReport(ByVal printType As String, ByVal formatReport As ExportFormatType, ByVal sOid As String)
        Try
            Dim sWhere As String
            sWhere = " WHERE r.cmpcode='" & CompnyCode & "'"

            If sOid = "" Then
                sWhere &= " AND r." & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If checkPagePermission("~\Master\mstRole.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND r.upduser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND r.roleoid=" & sOid
            End If

            Response.Clear()
            Response.AddHeader("content-disposition", "inline;filename=MasterRole.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"

            sSql = "SELECT r.roleoid [ID Role], rolename [Role Name],description [Role desc], formtype [Type Form], formname [Deskripsi Form], formaddress [Alamat URL] FROM QL_role r INNER JOIN QL_roledtl rd ON rd.cmpcode=r.cmpcode AND rd.roleoid=r.roleoid " & sWhere & ""

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
            da.SelectCommand = xcmd
            da.Fill(ds)
            Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
            dt = ds.Tables(0)
            Response.Write(ConvertDtToTDF(dt))
            conn.Close()
            Response.End()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\mstRole.aspx?awal=true")
    End Sub

    Private Sub RemoveList(ByVal checkCol As Integer)
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("TabelDtlRole")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = objRow.Length() - 1 To 0 Step -1
                If cKon.getCheckBoxValue(i, checkCol, gvDtl) = True Then
                    objTable.Rows.Remove(objRow(i))
                End If
            Next
            Session("TabelDtlRole") = objTable
            gvDtl.DataSource = Session("TabelDtlRole")
            gvDtl.DataBind()
        End If
        imbRemove.Visible = (objTable.Rows.Count > 0)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"

            Response.Redirect("mstrole.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Role"
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            FillDDLFormModule()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                btnDelete.Visible = False
                btnPrint2.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
            listformmodule.SelectedIndex = -1
            FillDDLFormType()
            listformtype.SelectedIndex = -1
            FillDDLFormName(listformtype.SelectedValue)
            listformname.SelectedIndex = -1
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("", ExportFormatType.Excel, "")
    End Sub

    Protected Sub btnPrint2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint2.Click
        'PrintReport(roleoid.Text)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                roleoid.Text = GenerateID("QL_ROLE", CompnyCode)
            End If
            Dim iOid As Int64 = GenerateID("QL_ROLEDTL", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_ROLE (cmpcode, roleoid, rolename, [DESCRIPTION],  upduser, updtime) VALUES ('" & CompnyCode & "', " & roleoid.Text & ", '" & Tchar(rolename.Text.Trim) & "', '" & Tchar(roledesc.Text.Trim) & "', '" & Session("UserID") & "', current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & roleoid.Text & " where tablename='QL_ROLE' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_role SET rolename='" & Tchar(rolename.Text.Trim) & "', DESCRIPTION='" & Tchar(roledesc.Text.Trim) & "', upduser='" & Session("UserID") & "', updtime=current_timestamp WHERE cmpcode='" & CompnyCode & "' AND roleoid=" & roleoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "DELETE QL_roledtl WHERE cmpcode='" & CompnyCode & "' AND roleoid=" & roleoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                If Not Session("TabelDtlRole") Is Nothing Then
                    Dim objTable As DataTable = Session("TabelDtlRole")
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_roledtl (cmpcode, roledtloid, roleoid, formtype, formname, formaddress, upduser, updtime) VALUES ('" & CompnyCode & "', " & iOid & ", " & roleoid.Text & ", '" & Tchar(objTable.Rows(C1)("formtype").ToString.ToUpper) & "', '" & Tchar(objTable.Rows(C1)("formname").ToString.ToUpper) & "', '" & Tchar(objTable.Rows(C1)("formaddress").ToString.ToUpper) & "', '" & Session("UserID") & "', current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iOid += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_ROLEDTL'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                Dim iOidUser As Int64 = GenerateID("QL_userrole", CompnyCode)
                If Not Session("oid") Is Nothing And Session("oid") <> "" Then
                    sSql = "DELETE QL_userrole WHERE roleoid='" & roleoid.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtlUserRole") Is Nothing Then
                    Dim objTableUser As DataTable = Session("TblDtlUserRole")
                    For C1 As Integer = 0 To objTableUser.Rows.Count - 1
                        sSql = "INSERT INTO QL_USERROLE (cmpcode, userroleoid, roleoid, userprof, special, upduser, updtime) VALUES ('" & objTableUser.Rows(C1)("cmpcode").ToString & "', " & iOidUser & ", " & roleoid.Text & ", '" & objTableUser.Rows(C1)("profoid").ToString & "', '" & Tchar(objTableUser.Rows(C1)("special").ToString) & "', '" & objTableUser.Rows(C1)("upduser").ToString & "', '" & objTableUser.Rows(C1)("updtime").ToString & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iOidUser += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iOidUser - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_USERROLE'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstrole.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstrole.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If roleoid.Text = "" Then
            showMessage("Please select role data first!", 1)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_userrole WHERE roleoid='" & roleoid.Text & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        If DeleteData("QL_role", "roleoid", roleoid.Text, CompnyCode) = True Then
            DeleteData("QL_roledtl", "roleoid", roleoid.Text, CompnyCode)
            Response.Redirect("~\Master\mstrole.aspx?awal=true")
        End If
    End Sub

    Protected Sub imbTambah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbTambah.Click
        UpdateChecked()
        If Session("TblRole") IsNot Nothing Then
            Dim dt As DataTable = Session("TblRole")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                Dim sErr As String = ""

                If Session("TabelDtlRole") Is Nothing Then
                    CreateTblDetail()
                End If

                Dim objTbl As DataTable = Session("TabelDtlRole")
                Dim objRow As DataRow
                Dim objView As DataView = objTbl.DefaultView

                For j As Integer = 0 To dv.Count - 1
                    objView.RowFilter = "formname='" & dv(j)("FormName").ToString & "'"
                    If objView.Count > 0 Then
                        sErr = "Maaf, Data " & dv(j)("FormName").ToString & " sudah ditambahkan, Silahkan pilih yang lain..!!<br>"
                        objView.RowFilter = ""
                        Exit For
                    End If
                Next

                If sErr <> "" Then
                    showMessage(sErr, 2)
                    Exit Sub
                End If

                For C1 As Integer = 0 To dv.Count - 1
                    objView.RowFilter = ""
                    objRow = objTbl.NewRow
                    objRow("formtype") = dv(C1)("FormType").ToString
                    objRow("formname") = dv(C1)("FormName").ToString
                    objRow("formaddress") = dv(C1)("FormAddress").ToString
                    objRow("createuser") = Session("UserID")
                    objRow("upduser") = Session("UserID")
                    objRow("updtime") = GetServerTime()
                    objTbl.Rows.Add(objRow)
                Next

                Session("TabelDtlRole") = objTbl
                gvDtl.DataSource = Session("TabelDtlRole")
                gvDtl.DataBind()
                BindRoleForm()
            End If
        End If
        'lblListFormError.Text = ""
        'listformname_SelectedIndexChanged(Nothing, Nothing)
        'cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
    End Sub

    Protected Sub imbRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRemove.Click
        RemoveList(3) : BindRoleForm()
    End Sub

    Protected Sub LBCancelRole_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBCancelRole.Click
        lblListFormError.Text = ""
        cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, False)
    End Sub

    Protected Sub LBAddRole_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBAddRole.Click
        If listformurl.Text = "" Then
            lblListFormError.Text = "Please fill Form URL field"
            cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
            Exit Sub
        End If
        If Session("TabelDtlRole") Is Nothing Then
            CreateTblDetail()
        End If
        Dim objTbl As DataTable = Session("TabelDtlRole")
        Dim objRow As DataRow
        Dim objView As DataView = objTbl.DefaultView

        objView.RowFilter = "formname='" & listformname.SelectedValue & "'"
        If objView.Count > 0 Then
            lblListFormError.Text = "This data has been added before"
            objView.RowFilter = ""
            'cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
            Exit Sub
        End If

        objView.RowFilter = ""
        objRow = objTbl.NewRow
        objRow("formtype") = listformtype.SelectedValue
        objRow("formname") = listformname.SelectedValue
        objRow("formaddress") = listformurl.Text
        objRow("createuser") = Session("UserID")
        objRow("upduser") = Session("UserID")
        objRow("updtime") = GetServerTime()
        objTbl.Rows.Add(objRow)
        Session("TabelDtlRole") = objTbl
        gvDtl.DataSource = Session("TabelDtlRole")
        gvDtl.DataBind()
        imbRemove.Visible = (objTbl.Rows.Count > 0)
        listformmodule.SelectedIndex = -1
        listformtype.SelectedIndex = -1
        listformtype_SelectedIndexChanged(Nothing, Nothing)
        lblListFormError.Text = ""
        'cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
    End Sub

    Protected Sub listformname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listformname.SelectedIndexChanged
        lblListFormError.Text = ""
        sSql = "SELECT genother1 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='FORMNAME' AND gendesc='" & listformname.SelectedValue & "' AND genother2='" & listformtype.SelectedValue & "' AND genother3='" & listformmodule.SelectedValue & "'"
        If GetStrData(sSql) <> "" Then
            listformurl.Text = GetStrData(sSql)
        Else
            listformurl.Text = ""
        End If
        'cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
    End Sub

    Protected Sub listformtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listformtype.SelectedIndexChanged, listformmodule.SelectedIndexChanged
        FillDDLFormName(listformtype.SelectedValue)
        listformname_SelectedIndexChanged(Nothing, Nothing)
        lblListFormError.Text = ""
        'cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub gvUserRole_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvUserRole.RowDeleting
        Dim objTable As DataTable = Session("TblDtlUserRole")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        Session("TblDtlUserRole") = objTable
        gvUserRole.DataSource = objTable
        gvUserRole.DataBind()
    End Sub
#End Region
 
    Protected Sub btnSearchForm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchForm.Click
        Panel2.Visible = True : BindRoleForm()
    End Sub

    Protected Sub gvRole_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRole.PageIndexChanging
        gvRole.PageIndex = e.NewPageIndex
        If UpdateChecked() Then
            Dim dtTbl As DataTable = Session("TblRole")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            'Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            'For C1 As Integer = 0 To sSplit.Length - 1
            '    sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
            '    'If ddlPIC.SelectedValue <> "NONE" Then
            '    '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
            '    'End If
            '    If C1 < sSplit.Length - 1 Then
            '        sFilter &= " AND "
            '    End If
            'Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblRoleView") = dtView.ToTable
                gvRole.DataSource = Session("TblRoleView")
                gvRole.DataBind() : dtView.RowFilter = ""
                ' mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                showMessage("Maaf, Data tidak ada tidak ditemukan..!!", 2)
            End If
        Else
            'mpeListMat.Show()
        End If
    End Sub 

    Protected Sub btnEraseFrom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseFrom.Click
        gvRole.Visible = False : Panel2.Visible = False
        TxtFormType.Text = ""
    End Sub
End Class
