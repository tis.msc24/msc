<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="TrnClosedTTS.aspx.vb" Inherits="TTS_Close"
    Title="PT. MULTI SARANA COMPUTER - Closed Penerimaan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">


    <table class="tabelhias" width="100%">
        <tr>
            <th class="header">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Close Penerimaan Service"></asp:Label></th>
        </tr>
    </table>

    <div style="text-align: left;">
        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Font-Size="9pt">
            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updPanel1" runat="server">
                        <ContentTemplate>
<TABLE id="Table2" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD style="WIDTH: 137px">Cabang</TD><TD><asp:DropDownList id="cBangFilter" runat="server" Width="80px" CssClass="inpText" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 137px">Filter</TD><TD><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged">
                                                <asp:ListItem Value="reqcode">No. TTS</asp:ListItem>
    <asp:ListItem Value="c.custname">Customer</asp:ListItem>
                                            </asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="136px" CssClass="inpText" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 137px"><asp:CheckBox id="CbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD><asp:TextBox id="tglAwal" runat="server" Width="78px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnTglAwal" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> to <asp:TextBox id="tglAkhir" runat="server" Width="78px" CssClass="inpText"></asp:TextBox><asp:ImageButton id="btnTglAkhir" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD style="WIDTH: 137px"></TD><TD><asp:ImageButton id="ImgFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ImgAllFind" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD colSpan=2><asp:GridView id="gvClosedBPM" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="8" Font-Underline="False">
                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                <Columns>
                                                    <asp:BoundField DataField="cabang" HeaderText="Cabang">
                                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="reqcode" HeaderText="No. TTS">
                                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="custname" HeaderText="Customer">
                                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="reqdate" HeaderText="Tanggal">
                                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="reqstatus" HeaderText="Status">
                                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Red" HorizontalAlign="Right" />
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                                </EmptyDataTemplate>
                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView>
                                            </TD></TR></TBODY></TABLE><ajaxToolkit:CalendarExtender id="ceAwal" runat="server" TargetControlID="tglAwal" PopupButtonID="btnTglAwal" Format="dd/MM/yyyy">
                                            </ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ceAkhir" runat="server" TargetControlID="tglAkhir" PopupButtonID="btnTglAkhir" Format="dd/MM/yyyy">
                                            </ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="tglAwal" UserDateFormat="DayMonthYear" CultureName="id-ID" MaskType="Date" Mask="99/99/9999">
                                            </ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" TargetControlID="tglAkhir" UserDateFormat="DayMonthYear" CultureName="id-ID" MaskType="Date" Mask="99/99/9999">
                                            </ajaxToolkit:MaskedEditExtender> 
</ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
                <HeaderTemplate>
                    <img align="absMiddle" alt="" src="../Images/corner.gif" />
                    <strong><span style="font-size: 9pt"> List of Closed Penerimaan :. </span></strong>
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD id="TD1" runat="server" visible="true">Cabang</TD><TD id="TD2" colSpan=2 runat="server" visible="true"><asp:DropDownList id="fCabang" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList>
    <asp:Label id="ordermstoid" runat="server" Visible="False"></asp:Label></TD><TD colSpan=1 runat="server" visible="true">Customer</TD><TD colSpan=1 runat="server" visible="true" id="Td3">
    <asp:TextBox id="custname" runat="server" Width="220px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> </TD></TR><TR><TD>No. TTS</TD><TD colSpan=2><asp:TextBox id="txtorderNo" runat="server" Width="120px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchBPM" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseBPM" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp; </TD><TD colSpan=1><asp:Label id="Label3" runat="server" Width="1px" Text="Keterangan"></asp:Label></TD><TD colSpan=1><asp:TextBox id="txtorderNote" runat="server" Width="136px" CssClass="inpText" MaxLength="200"></asp:TextBox><asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD>
    <asp:Label ID="lblStatus" runat="server" Visible="False" Width="67px">In Approval</asp:Label></TD><TD colSpan=4><asp:GridView id="gvBPM2" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="8" Font-Underline="False" DataKeyNames="reqmstoid,reqcode,custoid,custname,branch_code">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
    <ItemStyle HorizontalAlign="Left" Wrap="False" />
</asp:BoundField>
<asp:BoundField DataField="reqcode" HeaderText="No. TTS">
<HeaderStyle CssClass="gvhdr" HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
<asp:BoundField DataField="reqdate" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD colSpan=5><asp:GridView id="gv_item" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="8" Font-Underline="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
    <asp:BoundField DataField="snno" HeaderText="No. SN">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="reqqty" HeaderText="Quantity">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
    <asp:BoundField DataField="typegaransi" HeaderText="Type garansi">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
    <asp:BoundField DataField="NoSI" HeaderText="No. SI">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
    <asp:BoundField DataField="reqdtljob" HeaderText="Kerusakan">
        <HeaderStyle CssClass="gvhdr" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
    <asp:BoundField DataField="kelengkapan" HeaderText="Kelengkapan">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD colSpan=5><asp:Button id="btnCloseBPM" onclick="btnCloseBPM_Click" runat="server" CssClass="btn orange" Text="Send Approval"></asp:Button> <asp:Button id="btncancel" runat="server" CssClass="btn gray" Text="Cancel"></asp:Button></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
                <HeaderTemplate>
                    <strong><span style="font-size: 9pt">
                        <img align="absMiddle" alt="" src="../Images/corner.gif" />
                        Form Closed Penerimaan :.</span></strong>
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer><br />
        <br />
        <asp:UpdatePanel ID="updPanel2" runat="server">
            <ContentTemplate>
                <asp:Panel ID="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False">
                    <table>
                        <tbody>
                            <tr>
                                <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                    <asp:Label ID="captionPesan" runat="server" Font-Size="Small" Font-Bold="True" Text="header"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="HEIGHT: 3px" colspan="2"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/warn.png"></asp:Image></td>
                                <td style="TEXT-ALIGN: left" class="Label">
                                    <asp:Label ID="isiPesan" runat="server" ForeColor="Red"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="HEIGHT: 3px; TEXT-ALIGN: center" colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="TEXT-ALIGN: center" colspan="2">
                                    <asp:Button ID="btnErrOK" OnClick="btnErrOK_Click" runat="server" CssClass="btn red" Text=" OK " UseSubmitBehavior="False"></asp:Button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
                &nbsp;
                <ajaxToolkit:ModalPopupExtender ID="mpeMsg" runat="server" TargetControlID="PanelValidasi" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panelvalidasi" PopupDragHandleControlID="Validasi"></ajaxToolkit:ModalPopupExtender>
                <asp:Button ID="btnExtenderValidasi" runat="server" Visible="False"></asp:Button>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

