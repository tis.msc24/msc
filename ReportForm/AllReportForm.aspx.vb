Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_AllReportForm
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Dim CProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim folderReport As String = "~/report/"
    Dim RptFrm As String = "~\ReportForm\"
    Dim param As String = ""
    Dim param2 As String = ""
    Dim fileReport As String = ""
    Dim formReport As String = ""      
#End Region

#Region "Procedure"
    Sub clearItems()
        DDLFilter.Visible = True
        DDLPending.Visible = False
        ddlSpk.Visible = False
        ddlSJ.Visible = False
        DDLReport.SelectedIndex = 0
        DDLFilter.SelectedIndex = 0
        DDLStatus.SelectedIndex = 0
        txtFilter.Text = ""
        txtPeriod1.Text = Format(Date.Now, "01/" & "MM/yyyy")
        txtPeriod2.Text = Format(Date.Now, "dd/MM/yyyy")
        cbStatus.Checked = False
        cbStatus.Visible = True
        Label10.Visible = True
        DDLOper.Visible = False
        DDLStatus.Visible = True
        CRVAllReport.ReportSource = Nothing
        initStatus()
        sumddl.Visible = False
    End Sub

    Sub initStatus()
        'sSql = "SELECT DISTINCT REQSTATUS, REQSTATUS FROM QL_TRNREQUEST"
        'FillDDL(DDLStatus, sSql)
        DDLStatus.Items.Clear()
        Dim Arr1 As String() = {"In Process", "In", "Out", "Receive", "Check", "CheckOut", "Send", "Ready", "Close", "Start", "Finish", "Final", "Invoiced", "SPK", "SPKClose", "Paid"}
        For i As Integer = 0 To Arr1.Length - 1
            DDLStatus.Items.Add(Arr1(i))
            DDLStatus.Items(DDLStatus.Items.Count - 1).Value = Arr1(i)
        Next
        checkfiltered()
    End Sub

    Sub checkfiltered()

        If DDLReport.SelectedValue = "Penerimaan Barang" Then
            DDLFilter.Items(0).Value = "r.reqcode"
            DDLFilter.Items(1).Value = "c.custname"
            DDLFilter.Items(2).Value = "r.barcode"
            DDLFilter.Items(3).Value = "c.phone1"
        End If
        If DDLReport.SelectedValue = "Check Teknisi" Then
            DDLFilter.Items(0).Value = "qlr.reqcode"
            DDLFilter.Items(1).Value = "qlc.custname"
            DDLFilter.Items(2).Value = "qlr.barcode"
            DDLFilter.Items(3).Value = "qlc.phone1"
        End If
        If DDLReport.SelectedValue = "Rencana Service" Then
            DDLFilter.Items(0).Value = "qlr.reqcode"
            DDLFilter.Items(1).Value = "qlc.custname"
            DDLFilter.Items(2).Value = "qlr.barcode"
            DDLFilter.Items(3).Value = "qlc.phone1"
        End If
        If DDLReport.SelectedValue = "Pengerjaan" Then
            DDLFilter.Items(0).Value = "qla.reqcode"
            DDLFilter.Items(1).Value = "cust.custname"
            DDLFilter.Items(2).Value = "qla.barcode"
            DDLFilter.Items(3).Value = "cust.phone1"
        End If
        If DDLReport.SelectedValue = "Service Final" Then
            DDLFilter.Items(0).Value = "b.reqcode"
            DDLFilter.Items(1).Value = "c.custname"
            DDLFilter.Items(2).Value = "b.barcode"
            DDLFilter.Items(3).Value = "c.phone1"
        End If
        If DDLReport.SelectedValue = "SPK" Then
            DDLFilter.Items(0).Value = "id.reqcode"
            DDLFilter.Items(1).Value = "c.custname"
            DDLFilter.Items(2).Value = "id.barcode"
            DDLFilter.Items(3).Value = "c.phone1"
        End If
        If DDLReport.SelectedValue = "Surat Jalan" Then
            DDLFilter.Items(0).Value = "qlr.reqcode"
            DDLFilter.Items(1).Value = "qlc.custname"
            DDLFilter.Items(2).Value = "c.barcode"
            DDLFilter.Items(3).Value = "qlc.phone1"
        End If
        If DDLReport.SelectedValue = "Invoice Service" Then
            DDLFilter.Items(0).Value = "qlr.reqcode"
            DDLFilter.Items(1).Value = "qlc.custname"
            DDLFilter.Items(2).Value = "qlr.barcode"
            DDLFilter.Items(3).Value = "qlc.phone1"
        End If
        If DDLReport.SelectedValue = "Oper Teknisi" Then
            DDLFilter.Items(0).Value = "reqcode"
            DDLFilter.Items(1).Value = "custname"
            DDLFilter.Items(2).Value = "barcode"
            DDLFilter.Items(3).Value = "phone1"
        End If
        If DDLReport.SelectedValue = "Invoice Service Batal" Then
            DDLFilter.Items(0).Value = "reqcode"
            DDLFilter.Items(1).Value = "custname"
            DDLFilter.Items(2).Value = "barcode"
            DDLFilter.Items(3).Value = "phone1"
        End If
        If DDLReport.SelectedValue = "Barang Service Selesai" Then
            DDLFilter.Items(0).Value = "reqcode"
            DDLFilter.Items(1).Value = "custname"
            DDLFilter.Items(2).Value = "barcode"
            DDLFilter.Items(3).Value = "phone1"
        End If
    End Sub

    Sub checkFilter()
        Dim Arr1 As String() = {"In Process", "In", "Out", "Receive", "Check", "CheckOut", "Send", "Ready", "Close", "Start", "Finish", "Final", "Invoiced", "SPK", "SPKClose", "Paid"}
        Dim Arr2 As String() = {"Receive", "Check", "CheckOut", "Send", "Ready", "Close", "Start", "Finish", "Final", "Invoiced", "Paid"}
        Dim Arr3 As String() = {"Check", "CheckOut", "Send", "Ready", "Close", "Start", "Finish", "Final", "Invoiced", "Paid"}
        Dim Arr4 As String() = {"CheckOut", "Send", "Close", "Start", "Finish", "Final", "Invoiced", "Paid"}
        Dim Arr5 As String() = {"Finish", "Final", "Invoiced", "Paid"}
        Dim Arr6 As String() = {"Final", "Invoiced", "Paid"}
        Dim Arr7 As String() = {"Final", "Close"}
        checkfiltered()

        If DDLReport.SelectedValue = "Pending" Then
            DDLFilter.Visible = False
            DDLPending.Visible = True
            cbStatus.Visible = False
            cbStatus.Checked = False
            DDLStatus.Visible = False
            DDLOper.Visible = False
            ddlSpk.Visible = False
            ddlSJ.Visible = False
            Label10.Visible = False
            sumddl.Visible = False


        ElseIf DDLReport.SelectedValue = "Invoice Service Batal" Then
            DDLFilter.Visible = True
            DDLPending.Visible = False
            cbStatus.Checked = False
            cbStatus.Visible = False
            Label10.Visible = False
            DDLStatus.Visible = False
            DDLOper.Visible = False
            ddlSpk.Visible = False
            ddlSJ.Visible = False
            sumddl.Visible = False

        ElseIf DDLReport.SelectedValue = "Teknisi Invoice" Then
            DDLFilter.Visible = False
            DDLPending.Visible = True
            cbStatus.Checked = False
            cbStatus.Visible = False
            Label10.Visible = False
            DDLStatus.Visible = False
            DDLOper.Visible = False
            ddlSpk.Visible = False
            ddlSJ.Visible = False
            sumddl.Visible = True

        Else
            If DDLReport.SelectedValue = "Oper Teknisi" Then
                DDLOper.Visible = True
                DDLStatus.Visible = False
                ddlSpk.Visible = False
                ddlSJ.Visible = False
            ElseIf DDLReport.SelectedValue = "SPK" Then
                DDLOper.Visible = False
                DDLStatus.Visible = False
                ddlSpk.Visible = True
                ddlSJ.Visible = False
            ElseIf DDLReport.SelectedValue = "Surat Jalan" Then
                DDLOper.Visible = False
                DDLStatus.Visible = False
                ddlSpk.Visible = False
                ddlSJ.Visible = True
            Else
                DDLStatus.Items.Clear()
                If DDLReport.SelectedValue = "Penerimaan Barang" Then
                    For i As Integer = 0 To Arr1.Length - 1
                        DDLStatus.Items.Add(Arr1(i))
                        DDLStatus.Items(DDLStatus.Items.Count - 1).Value = Arr1(i)
                    Next
                ElseIf DDLReport.SelectedValue = "Check Teknisi" Then
                    For i As Integer = 0 To Arr2.Length - 1
                        DDLStatus.Items.Add(Arr2(i))
                        DDLStatus.Items(DDLStatus.Items.Count - 1).Value = Arr2(i)
                    Next
                ElseIf DDLReport.SelectedValue = "Rencana Service" Then
                    For i As Integer = 0 To Arr3.Length - 1
                        DDLStatus.Items.Add(Arr3(i))
                        DDLStatus.Items(DDLStatus.Items.Count - 1).Value = Arr3(i)
                    Next
                ElseIf DDLReport.SelectedValue = "Pengerjaan" Then
                    For i As Integer = 0 To Arr4.Length - 1
                        DDLStatus.Items.Add(Arr4(i))
                        DDLStatus.Items(DDLStatus.Items.Count - 1).Value = Arr4(i)
                    Next
                ElseIf DDLReport.SelectedValue = "Service Final" Then
                    For i As Integer = 0 To Arr5.Length - 1
                        DDLStatus.Items.Add(Arr5(i))
                        DDLStatus.Items(DDLStatus.Items.Count - 1).Value = Arr5(i)
                    Next
                ElseIf DDLReport.SelectedValue = "Invoice Service" Then
                    For i As Integer = 0 To Arr6.Length - 1
                        DDLStatus.Items.Add(Arr6(i))
                        DDLStatus.Items(DDLStatus.Items.Count - 1).Value = Arr6(i)
                    Next
                ElseIf DDLReport.SelectedValue = "Barang Service Selesai" Then
                    For i As Integer = 0 To Arr7.Length - 1
                        DDLStatus.Items.Add(Arr7(i))
                        DDLStatus.Items(DDLStatus.Items.Count - 1).Value = Arr7(i)
                    Next
                End If
                DDLStatus.Visible = True
                DDLOper.Visible = False
                ddlSpk.Visible = False
                ddlSJ.Visible = False
            End If
            DDLFilter.Visible = True
            DDLPending.Visible = False
            cbStatus.Visible = True
            Label10.Visible = True
            sumddl.Visible = False

        End If
        Session("ShowReport") = False
    End Sub

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Private Sub showValidation()
        PanelPass.Visible = True
        btnPass.Visible = True
        MPEPass.Show()
    End Sub

    Private Function checkValidation(ByVal sType As String)
        If txtPass.Text = "" Then
            showMessage("Password harus diisi", CompnyName & " - WARNING", 2)
            lblError.Text = "showMsg"
            Return False
            Exit Function
        End If
        If txtPass.Text <> "" Then
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT LOWER(profpass) FROM QL_mstprof WHERE userid = 'ADMIN'"
            xCmd.CommandText = sSql
            Dim pass As String = xCmd.ExecuteScalar

            conn.Close()

            If txtPass.Text.ToLower = pass Then
                showPrint(sType)
            Else
                txtPass.Text = ""
                showMessage("Password yang Anda inputkan salah", CompnyName & " - WARNING", 2)
                lblError.Text = "showMsg"
                Return False
                Exit Function
            End If
        End If
    End Function

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Sub showPrint(ByVal tipe As String)
        Try
            txtPass.Text = ""
            btnPass.Visible = False
            PanelPass.Visible = False
            'checkFilter()
            If txtFilter.Text <> "" Then
                If DDLPending.Visible = True Then
                    If DDLReport.SelectedValue = "Teknisi Invoice" Then
                        param &= " where PERSONNAME like '%" & Tchar(txtFilter.Text) & "%'"
                    Else
                        param2 &= " and PERSONNAME like '%" & Tchar(txtFilter.Text) & "%'"
                    End If

                Else
                    If DDLReport.SelectedValue = "Invoice Service Batal" Or DDLReport.SelectedValue = "Barang Service Selesai" Then
                        param &= " and " & DDLFilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%'"
                    Else
                        param &= " where " & DDLFilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%'"
                    End If

                End If
            End If
            
            If txtPeriod1.Text <> "" And txtPeriod2.Text <> "" Then
                Try
                    Dim dDate1 As Date = CDate(toDate(txtPeriod1.Text))
                    Dim dDate2 As Date = CDate(toDate(txtPeriod2.Text))
                    If dDate1 < CDate("01/01/1900") Then
                        showMessage("Tanggal awal tidak valid", CompnyName & " - WARNING", 2) : Exit Sub
                    End If
                    If dDate2 < CDate("01/01/1900") Then
                        showMessage("Tanggal akhir tidak valid", CompnyName & " - WARNING", 2) : Exit Sub
                    End If
                    If dDate1 > dDate2 Then
                        showMessage("Period 1 harus lebih kecil dari period 2", CompnyName & " - WARNING", 2) : Exit Sub
                    End If
                Catch ex As Exception
                    showMessage("Format tanggal tidak sesuai", CompnyName & " - WARNING", 2) : Exit Sub
                End Try
                If txtFilter.Text = "" Then
                    If DDLReport.SelectedValue = "Invoice Service Batal" Or DDLReport.SelectedValue = "Barang Service Selesai" Then
                        param &= " and "
                    Else
                        If DDLReport.SelectedValue = "Teknisi Invoice" Then
                            param &= ""
                        Else
                            param &= " where "
                        End If

                    End If

                Else
                    If DDLReport.SelectedValue = "Teknisi Invoice" Then
                        param &= " "
                    Else
                        param &= " and "
                    End If

                End If

                If checkFilterPeriod(DDLReport.SelectedValue) = "SPK" Then
                    param &= "CONVERT(DATE, sm.spkplanstart, 101) >= '" & CDate(toDate(txtPeriod1.Text)) & "' AND CONVERT(DATE, sm.spkplanend, 101) <= '" & CDate(toDate(txtPeriod2.Text)) & "'"
                ElseIf DDLPending.Visible = True Then
                    If DDLReport.SelectedValue = "Teknisi Invoice" Then
                        param2 = " and  CONVERT(DATE, " & checkFilterPeriod(DDLReport.SelectedValue) & ", 101) between '" & CDate(toDate(txtPeriod1.Text)) & "' and '" & CDate(toDate(txtPeriod2.Text)) & "'"
                    Else
                        param = " and REQPERSON=PERSONOID and CONVERT(DATE, " & checkFilterPeriod(DDLReport.SelectedValue) & ", 101) between '" & CDate(toDate(txtPeriod1.Text)) & "' and '" & CDate(toDate(txtPeriod2.Text)) & "'"
                    End If
                Else
                    param &= "CONVERT(DATE, " & checkFilterPeriod(DDLReport.SelectedValue) & ", 101) between '" & CDate(toDate(txtPeriod1.Text)) & "' and '" & CDate(toDate(txtPeriod2.Text)) & "'"
                End If

            End If
            If cbStatus.Checked = True Then
                If txtFilter.Text = "" And (txtPeriod1.Text = "" And txtPeriod2.Text = "") Then
                    param &= " where "
                Else
                    param &= " and "
                End If
                If DDLReport.SelectedValue = "Oper Teknisi" Then
                    param &= "" & checkStatus(DDLReport.SelectedValue) & "='" & DDLOper.SelectedValue & "'"
                ElseIf DDLReport.SelectedValue = "SPK" Then
                    param &= "" & checkStatus(DDLReport.SelectedValue) & "='" & ddlSpk.SelectedValue & "'"
                ElseIf DDLReport.SelectedValue = "Surat Jalan" Then
                    param &= "" & checkStatus(DDLReport.SelectedValue) & "='" & ddlSJ.SelectedValue & "'"
                Else
                    param &= "" & checkStatus(DDLReport.SelectedValue) & "='" & DDLStatus.SelectedValue & "'"
                End If

            End If
            report.Load(Server.MapPath(folderReport & checkFileReport(DDLReport.SelectedValue)))

            Dim crConninfo As New ConnectionInfo

            With crConninfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConninfo, report)
            If DDLReport.SelectedValue = "Pending" Then
                If param2 <> "" Then
                    report.SetParameterValue("sWhere2", param2)
                Else
                    param2 = ""
                    report.SetParameterValue("sWhere2", param2)
                End If
            End If
            If DDLReport.SelectedValue = "Teknisi Invoice" Then
                report.SetParameterValue("dWhere", param2)
            End If

            report.SetParameterValue("sWhere", param)
            report.SetParameterValue("dPeriode", txtPeriod1.Text & " - " & txtPeriod2.Text)
            report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            If tipe = "PRINT" Then
                CRVAllReport.DisplayGroupTree = False
                CRVAllReport.SeparatePages = False
                CRVAllReport.ReportSource = report
            ElseIf tipe = "EXCEL" Then
                'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Left(DDLReport.SelectedValue, 3) & "_" & Format(Date.Now, "dd/MM/yyyy"))
                report.Close() : report.Dispose()
                Response.Redirect("~\ReportForm\AllReportForm.aspx?page=true")
            Else
                'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Left(DDLReport.SelectedValue, 3) & "_" & Format(Date.Now, "dd/MM/yyyy"))
                report.Close() : report.Dispose()
                Response.Redirect("~\ReportForm\AllReportForm.aspx?page=true")
            End If
            'Session("ShowReport") = True
            PanelPass.Visible = False
            btnPass.Visible = False
            MPEPass.Hide()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        param = ""
        param2 = ""
    End Sub
#End Region

#Region "Function"
    Private Function checkFileReport(ByVal flr As String)
        Select Case flr
            Case "Penerimaan Barang" : Return "ReportPenerimaanBarang.rpt"
            Case "Check Teknisi" : Return "CheckTeknisiReport.rpt"
            Case "Rencana Service" : Return "ServicePlanReport.rpt"
            Case "Pengerjaan" : Return "rptPengerjaan.rpt"
            Case "Service Final" : Return "rptservicefinalreport.rpt"
            Case "SPK" : Return "rptSPK.rpt"
            Case "Surat Jalan" : Return "rptsuratjalanreport.rpt"
            Case "Invoice Service" : Return "InvoiceServiceReport.rpt"
            Case "Pending" : Return "rptPending.rpt"
            Case "Oper Teknisi" : Return "rptOper.rpt"
            Case "Invoice Service Batal" : Return "rptInvoiceBatal.rpt"
            Case "Barang Service Selesai" : Return "rptbarangservice.rpt"
            Case "Teknisi Invoice"
                If sumddl.SelectedValue = "Summary" Then
                    Return "rptInvoicePengerjaan.rpt"
                Else
                    Return "rptInvoicePengerjaandtl.rpt"
                End If


        End Select
    End Function

    Private Function checkFormReport(ByVal fmr As String)
        Select Case fmr
            Case "Penerimaan Barang" : Return RptFrm & "frmReportPenerimaanBarang.aspx?awal=true"
            Case "Check Teknisi" : Return RptFrm & "CheckTeknisiReportForm.aspx?page=true"
            Case "Rencana Service" : Return RptFrm & "ServicePlanReportForm.aspx?page=true"
            Case "Pengerjaan" : Return RptFrm & "rptPengerjaan.aspx?awal=true"
            Case "Service Final" : Return RptFrm & "rptservicefinal.aspx?awal=true"
            Case "SPK" : Return RptFrm & "rptSPK.aspx?awal=true"
            Case "Surat Jalan" : Return RptFrm & "rptsuratjalan.aspx?awal=true"
            Case "Invoice Service" : Return RptFrm & "InvoiceReportForm.aspx?page=true"
            Case "Pending" : Return RptFrm & "rptPending.aspx?page=true"
        End Select
    End Function

    Private Function checkFilterPeriod(ByVal cfp As String)
        Select Case cfp
            Case "Penerimaan Barang" : Return "r.createtime"
            Case "Check Teknisi" : Return "QLR.CREATETIME"
            Case "Rencana Service" : Return "QLR.CREATETIME"
            Case "Pengerjaan" : Return "qla.CREATETIME"
            Case "Service Final" : Return "b.CREATETIME"
            Case "SPK" : Return "SPK"
            Case "Surat Jalan" : Return "a.sjldate"
            Case "Invoice Service" : Return "QLS.SSTARTTIME"
            Case "Pending" : Return "REQDATE"
            Case "Oper Teknisi" : Return "o.operdate"
            Case "Invoice Service Batal" : Return "r.dateclosed"
            Case "Barang Service Selesai" : Return "r.CREATETIME"
            Case "Teknisi Invoice" : Return "A.SSTARTTIME"

        End Select
    End Function

    Private Function checkStatus(ByVal cs As String)
        Select Case cs
            Case "Penerimaan Barang" : Return "r.REQSTATUS"
            Case "Check Teknisi" : Return "QLR.REQSTATUS"
            Case "Rencana Service" : Return "QLR.REQSTATUS"
            Case "Pengerjaan" : Return "QLA.REQSTATUS"
            Case "Service Final" : Return "b.REQSTATUS"
            Case "SPK" : Return "sm.spkstatus"
            Case "Surat Jalan" : Return "a.sjlstatus"
            Case "Invoice Service" : Return "QLR.REQSTATUS"
            Case "Pending" : Return "REQSTATUS"
            Case "Oper Teknisi" : Return "o.operstatus"
            Case "Barang Service Selesai" : Return "r.REQSTATUS"
        End Select
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Response.Redirect("AllReportForm.aspx")
        End If
        Session("idPage") = Request.QueryString("idPage")
        Page.Title = CompnyName & " - Semua Laporan"

        If Not Page.IsPostBack Then
            initStatus()
            Session("ShowReport") = Nothing
            txtPeriod1.Text = Format(Date.Now, "01/" & "MM/yyyy")
            txtPeriod2.Text = Format(Date.Now, "dd/MM/yyyy")
        End If
        'If Session("ShowReport") = True Then
        '    btnPass.Visible = False
        '    PanelPass.Visible = False
        '    showPrint("PRINT")
        'End If
        If lblError.Text = "showMsg" Then
            showValidation()
            lblError.Text = ""
        Else
            btnPass.Visible = False
            PanelPass.Visible = False
        End If
    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnview.Click
        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        showPrint("PRINT")
        'Session("ShowReport") = True
        param = ""
        param2 = ""        
    End Sub

    Protected Sub btnETPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnETPdf.Click
        Session("ShowReport") = False

        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        showValidation()
        lblType.Text = "PDF"
    End Sub

    Protected Sub btnETExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnETExcel.Click
        Session("ShowReport") = False

        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        showValidation()
        lblType.Text = "EXCEL"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        clearItems()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click        
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub

    Protected Sub DDLReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLReport.SelectedIndexChanged        
        checkFilter()        
        Session("ShowReport") = False
        txtPeriod1.Text = Format(Date.Now, "01/" & "MM/yyyy")
        txtPeriod2.Text = Format(Date.Now, "dd/MM/yyyy")
        cbStatus.Checked = False
        DDLStatus.SelectedIndex = 0
    End Sub

    Protected Sub btnOKPass_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOKPass.Click        
        PanelPass.Visible = False
        btnPass.Visible = False
        MPEPass.Hide()
        checkValidation(lblType.Text)
    End Sub

    Protected Sub btnCancelPass_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelPass.Click
        btnPass.Visible = False
        PanelPass.Visible = False
        lblError.Text = ""
        lblType.Text = ""
    End Sub
#End Region

    Protected Sub Page_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
End Class
