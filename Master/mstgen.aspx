<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstgen.aspx.vb" Inherits="Mstgen" Title="PT. MULTI SARANA KOMPUTER - Master General" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">

    <%--<script type="text/javascript">
    function s()
    {
    try {
        var t = document.getElementById("<%=GVmstgen.ClientID%>");
        var t2 = t.cloneNode(true)
        for(i = t2.rows.length -1;i > 0;i--)
        t2.deleteRow(i)
        t.deleteRow(0)
        divTblData.appendChild(t2)
        }
    catch(errorInfo) {}
    }
    window.onload = s
  </script>--%>

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Set COA Persediaan"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div style="width: 100%; text-align: left">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" class="Label">Filter </td>
                                            <td align="left" class="Label">
                                                :</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="gm.gendesc">Description</asp:ListItem>
                                                    <asp:ListItem Value="gm.genCode">Code</asp:ListItem>
                                                    <asp:ListItem Value="gm.gengroup" Enabled="False">General Group</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText"></asp:TextBox>
                                                <asp:ImageButton
                                                    ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" />
                                                <asp:ImageButton
                                                    ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="Label" id="TD4" runat="server" visible="False">
                                                <asp:CheckBox ID="chkGroup" runat="server" Text="Group" /></td>
                                            <td align="left" class="Label" id="TD3" runat="server" visible="False">
                                                :</td>
                                            <td align="left" colspan="4" id="TD5" runat="server" visible="False">
                                                <asp:DropDownList ID="FilterGroup" runat="server" CssClass="inpText" Width="155px">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="6">




                                <fieldset id="Fieldset1">
                                    <div id='divTblData'>
                                        <asp:GridView ID="GVmstgen" runat="server" EmptyDataText="No data in database." AutoGenerateColumns="False" Width="100%" CellPadding="4" AllowPaging="True" EnableModelValidation="True" ForeColor="#333333" GridLines="None">
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="genoid" DataNavigateUrlFormatString="mstgen.aspx?oid={0}"
                                                    DataTextField="genCode" HeaderText="Code">
                                                    <ItemStyle Width="200px" Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Width="200px" Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="gendesc" HeaderText="Description" SortExpression="gendesc">
                                                    <ItemStyle Width="250px" Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle ForeColor="Black" Width="550px" Font-Size="X-Small" CssClass="gvhdr" HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="gudang" HeaderText="Gudang" SortExpression="gengroup">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" ForeColor="Black" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AkunCoa" HeaderText="Akun COA">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Left" Width="250px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Cabang" HeaderText="Cabang">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                </asp:BoundField>
                                            </Columns>
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Right" Font-Bold="True" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <EmptyDataTemplate>



                                                <asp:Label ID="lblmsg" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                &nbsp;</div>
                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> List Of General</span></strong> <strong><span
                                style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="HEIGHT: 8px" class="Label" align=left><asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" __designer:wfdid="w23" Visible="False"></asp:Label></TD><TD style="HEIGHT: 8px" class="Label" align=left></TD><TD style="HEIGHT: 8px" class="Label" align=left><asp:Label id="GenOid" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" __designer:wfdid="w24" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Code <asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w25"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="gencode" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w26" size="20" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" vAlign=top align=left>Description <asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w27"></asp:Label><BR /></TD><TD class="Label" vAlign=top align=left>:</TD><TD class="Label" vAlign=top align=left><asp:TextBox id="gendesc" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w28" size="20" MaxLength="30" AutoPostBack="True"></asp:TextBox> <asp:Label id="Label4" runat="server" CssClass="Important" Text="* 70 characters" __designer:wfdid="w29"></asp:Label></TD></TR><TR><TD class="Label" align=left>Group</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="gengroup" runat="server" Width="205px" CssClass="inpTextDisabled" Font-Size="X-Small" __designer:wfdid="w30" AutoPostBack="True" Enabled="False"></asp:DropDownList>&nbsp; </TD></TR><TR><TD class="Label" align=left>Type COA</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" __designer:wfdid="w31" AutoPostBack="True" OnSelectedIndexChanged="DDLType_SelectedIndexChanged"><asp:ListItem Value="T">TRANSACTION</asp:ListItem>
<asp:ListItem Value="I">INVENTORY</asp:ListItem>
<asp:ListItem Value="V">MERCHANDISE</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="Cabang" __designer:wfdid="w36"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="genother3" runat="server" CssClass="inpText" __designer:wfdid="w37" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Width="41px" Text="Gudang" __designer:wfdid="w32"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="genother1" runat="server" CssClass="inpText" __designer:wfdid="w33" AutoPostBack="True"></asp:DropDownList> </TD></TR><TR><TD class="Label" align=left><SPAN style="FONT-SIZE: 7pt"></SPAN><asp:Label id="Label6" runat="server" Width="33px" Text="COA" __designer:wfdid="w34"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="genother2" runat="server" CssClass="inpText" __designer:wfdid="w35" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3>Last Update On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w38"></asp:Label> By <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w39"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton> <asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w41"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w42" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w45" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt">LOADING....</SPAN><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w46"></asp:Image></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form General :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w1" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" align="left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w2"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w3"></asp:Image></td>
                            <td align="left">
                                <asp:Label ID="lblPopUpMsg" runat="server" CssClass="Important" __designer:wfdid="w4"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:ImageButton ID="imbPopUpOK" OnClick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" __designer:wfdid="w6" DropShadow="True" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w7" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
