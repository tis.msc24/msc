Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports ClassProcedure
Imports Koneksi

Partial Class Master_mstsparepart
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub initunitddl()
        sSql = "SELECT genoid, gendesc FROM QL_MSTGEN WHERE gengroup = 'Unit' AND cmpcode = '" & CompnyCode & "' order by gendesc asc"
        FillDDL(ddlunit, sSql)

        sSql = "SELECT genoid, gendesc FROM QL_MSTGEN WHERE gengroup = 'SPARECAT' AND cmpcode = '" & CompnyCode & "' order by gendesc DESC"
        FillDDL(Category, sSql)
    End Sub

    Private Sub clearform()
        lblcurrentuser.Text = Session("UserID")
        If tbsdesc.Text.Trim.Length > 0 Then
            tbsdesc.Text = ""
        End If
        If tbldesc.Text.Trim.Length > 0 Then
            tbldesc.Text = ""
        End If
        If tbsafestock.Text.Trim.Length > 0 Then
            tbsafestock.Text = ""
        End If
        ddlunit.SelectedIndex = 0
        If tbnote.Text.Trim.Length > 0 Then
            tbnote.Text = ""
        End If
        lblcreate.Text = "Create On <span style='font-weight: bold;'>" & Date.Now.ToString("dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & Session("UserID") & "</span>"
    End Sub

    Private Sub bindalldata()
        sSql = "SELECT partoid, partcode, partdescshort,MERK FROM ql_mstsparepart WHERE cmpcode = '" & CompnyCode & "' ORDER BY partoid desc"
        Session("DataSpart") = Nothing
        Session("DataSpart") = cKon.ambiltabel(sSql, "sparepart")
        gvdata.DataSource = Session("DataSpart")
        gvdata.DataBind()
    End Sub

    Private Sub fillform()
        lblcurrentuser.Text = Session("UserID")
        Try
            Dim oid As Integer = 0
            If Integer.TryParse(Session("oid"), oid) Then
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                sSql = "SELECT partoid, partcode, partdescshort, partdesclong, partunit, partsafestockqty, note,merk, PARTCAT, createuser, createtime, upduser, updtime FROM ql_mstsparepart WHERE partoid = " & oid & " AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xreader = xCmd.ExecuteReader
                If xreader.HasRows Then
                    While xreader.Read
                        tbcode.Text = xreader("partcode").ToString
                        tbsdesc.Text = xreader("partdescshort").ToString
                        tbldesc.Text = xreader("partdesclong").ToString
                        tbsafestock.Text = xreader("partsafestockqty").ToString
                        ddlunit.SelectedValue = xreader("partunit")
                        Category.SelectedValue = xreader("PARTCAT")
                        Merk.Text = xreader("merk")
                        tbnote.Text = xreader("note").ToString
                        If Not IsDBNull(xreader("upduser")) Or xreader("upduser").ToString <> "" Then
                            lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("upduser").ToString & "</span>"
                        Else
                            lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("createtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("createuser").ToString & "</span>"
                        End If
                    End While
                Else
                    xreader.Close()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End If
            Else
                Response.Redirect("~\Master\mstsparepart.aspx?awal=true")
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        Finally
            If Not xreader.IsClosed Then
                xreader.Close()
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub finddata(ByVal filter As String, ByVal val As String)
        sSql = "SELECT partoid, partcode, partdescshort, merk FROM ql_mstsparepart"
        If filter = "code" Then
            sSql &= " WHERE partcode LIKE '%" & val & "%'"
        ElseIf filter = "description" Then
            sSql &= " WHERE partdescshort LIKE '%" & val & "%'"
        ElseIf filter = "merk" Then
            sSql &= " where merk like '%" & val & "%'"
        End If
        sSql &= " AND cmpcode = '" & CompnyCode & "' ORDER BY partoid"
        Session("DataSpart") = Nothing
        Session("DataSpart") = cKon.ambiltabel(sSql, "sparepart")
        gvdata.DataSource = Session("DataSpart")
        gvdata.DataBind()
    End Sub
#End Region

#Region "Function"
    Private Function setpartcode() As String
        Dim code As String = ""

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Try
            Dim oid As Integer = GenerateID("QL_MSTSPAREPART", CompnyCode)
            Dim soid As String = oid.ToString("000000")
            sSql = "SELECT gencode FROM QL_MSTGEN WHERE gengroup ='SPARECAT' AND cmpcode LIKE '%" & CompnyCode & "%'"
            xCmd.CommandText = sSql
            Dim scode As String = xCmd.ExecuteScalar
            conn.Close()
            code &= scode : code &= soid
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return code
    End Function

    Private Function saverecord() As String
        Dim result As String = ""

        If tbsdesc.Text.Trim = "" Then
            result = "Deskripsi pendek harus diisi!"
        Else
            Dim code As String = Tchar(tbcode.Text.Trim)
            Dim sdesc As String = Tchar(tbsdesc.Text.Trim)
            Dim ldesc As String = Tchar(tbldesc.Text.Trim)
            Dim unit As Integer = ddlunit.SelectedValue
            Dim safestock As Decimal = 0.0
            Dim tstock As Decimal = 0.0
            If Decimal.TryParse(tbsafestock.Text.Trim, tstock) Then
                safestock = tstock
            End If
            Dim note As String = Tchar(tbnote.Text.Trim)

            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans
            Try
                sSql = "SELECT COUNT(partoid) FROM ql_mstsparepart WHERE LOWER(LTRIM(RTRIM(partdescshort))) = '" & sdesc.ToLower & "' AND LOWER(LTRIM(RTRIM(Merk))) = '" & Merk.Text.ToLower & "' "
                xCmd.CommandText = sSql
                Dim res As Integer = xCmd.ExecuteScalar
                If res > 0 Then
                    result = "Sparepart tidak bisa disimpan karena telah tercatat sebelumnya!"
                    otrans.Rollback()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    Return result
                End If

                Dim oid As Integer = GenerateID("QL_MSTSPAREPART", CompnyCode)

                sSql = "INSERT INTO QL_MSTSPAREPART (CMPCODE, PARTOID, PARTCODE, PARTDESCSHORT, PARTDESCLONG, PARTUNIT, PARTSTOCKQTY, PARTSAFESTOCKQTY, NOTE, CREATEUSER, CREATETIME, UPDUSER, UPDTIME,merk,PARTCAT) VALUES ('" & CompnyCode & "', " & oid & ", '" & code & "', '" & sdesc & "', '" & ldesc & "', " & unit & ", 0, " & safestock & ", '" & note & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "','" & Tchar(Merk.Text) & "'," & Category.SelectedValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_MSTOID SET lastoid=" & oid & " WHERE tablename LIKE 'QL_MSTSPAREPART' AND cmpcode LIKE '%" & CompnyCode & "%'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                otrans.Commit()
                conn.Close()
            Catch ex As Exception
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                result = ex.ToString
                showMessage(ex.ToString, 2)
            End Try
        End If

        Return result
    End Function

    Private Function updaterecord(ByVal oid As Integer) As String
        Dim result As String = ""

        If tbsdesc.Text.Trim = "" Then
            result = "Deskripsi pendek harus diisi!"
        Else
            Dim code As String = Tchar(tbcode.Text.Trim)
            Dim sdesc As String = Tchar(tbsdesc.Text.Trim)
            Dim ldesc As String = Tchar(tbldesc.Text.Trim)
            Dim unit As Integer = ddlunit.SelectedValue
            Dim safestock As Decimal = 0.0
            Dim tstock As Decimal = 0.0
            If Decimal.TryParse(tbsafestock.Text.Trim, tstock) Then
                safestock = tstock
            End If
            Dim note As String = Tchar(tbnote.Text.Trim)

            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans
            Try
                sSql = "SELECT COUNT(partoid) FROM ql_mstsparepart WHERE LOWER(LTRIM(RTRIM(partdescshort))) = '" & sdesc.ToLower & "' AND LOWER(LTRIM(RTRIM(Merk))) = '" & Merk.Text.ToLower & "' AND partoid <> " & oid & ""
                xCmd.CommandText = sSql
                Dim res As Integer = xCmd.ExecuteScalar
                If res > 0 Then
                    result = "Nama sparepart tidak bisa disimpan karena telah tercatat sebelumnya!"
                    otrans.Rollback()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    Return result
                End If

                sSql = "UPDATE QL_MSTSPAREPART SET CMPCODE = '" & CompnyCode & "', PARTDESCSHORT = '" & sdesc & "', PARTDESCLONG = '" & ldesc & "', PARTUNIT = " & unit & ", PARTSAFESTOCKQTY = " & safestock & ", NOTE = '" & note & "', UPDUSER = '" & lblcurrentuser.Text & "', UPDTIME = '" & DateTime.Now & "',merk='" & Tchar(Merk.Text) & "' , PARTCAT = " & Category.SelectedValue & " WHERE PARTOID = " & oid & ""
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                otrans.Commit()
                conn.Close()
            Catch ex As Exception
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                result = ex.ToString
            End Try
        End If

        Return result
    End Function

    Private Function checkspartexist(ByVal oid As Integer) As Boolean
        Dim result As Boolean = False

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT COUNT(mstpartoid) FROM ql_trnmechpartdtl WHERE mstpartoid = " & oid
            xCmd.CommandText = sSql
            Dim res As Integer = xCmd.ExecuteScalar
            If res > 0 Then
                result = True
            Else
                sSql = "SELECT COUNT(spartmoid) FROM ql_trnservicespart WHERE spartmoid = " & oid
                xCmd.CommandText = sSql
                res = xCmd.ExecuteScalar
                If res > 0 Then
                    result = True
                Else
                    sSql = "SELECT COUNT(partoid) FROM ql_trninvoicedtl WHERE partoid = " & oid
                    xCmd.CommandText = sSql
                    res = xCmd.ExecuteScalar
                    If res > 0 Then
                        result = True
                    End If
                End If
            End If

            conn.Close()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return result
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        Session.Timeout = 60
		If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
		
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userID
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\Master\mstsparepart.aspx")
            TabContainer1.ActiveTabIndex = 0
        End If

        Page.Title = CompnyName & " - Sparepart"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            lbltitle.Text = "Tambah Data Sparepart"
        Else
            lbltitle.Text = "Ubah Data Sparepart"
        End If
        ibdelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah Anda ingin menghapus data ini?');")

        If Not Page.IsPostBack Then

            bindalldata()
            initunitddl()
            If Session("oid") = "" Or Session("oid") Is Nothing Then
                TabContainer1.ActiveTabIndex = 0
                tbcode.Text = setpartcode()
                clearform()
            Else
                fillform()
                TabContainer1.ActiveTabIndex = 1
                ibdelete.Visible = True
            End If
        End If
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        finddata(ddlfilter.SelectedValue, Tchar(tbfilter.Text.Trim))
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        bindalldata()
        ddlfilter.SelectedIndex = 0
        tbfilter.Text = ""
    End Sub

    Protected Sub ibsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsave.Click
        Dim result As String = ""
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            result = saverecord()
            If result = "" Then
                Response.Redirect("~\Master\mstsparepart.aspx?awal=true")
            Else
                showMessage(result, 2)
            End If
        Else
            result = updaterecord(Session("oid"))
            If result = "" Then
                Response.Redirect("~\Master\mstsparepart.aspx?awal=true")
            Else
                showMessage(result, 2)
            End If
        End If

    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("~\Master\mstsparepart.aspx?awal=true")
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub gvdata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvdata.PageIndexChanging
        gvdata.PageIndex = e.NewPageIndex
        'If tbfilter.Text.Trim <> "" Then
        '    finddata(ddlfilter.SelectedValue, Tchar(tbfilter.Text.Trim))
        'Else
        '    bindalldata()
        'End If
        gvdata.DataSource = Session("DataSpart")
        gvdata.DataBind()
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click
        Dim oid As Integer = 0
        If Integer.TryParse(Session("oid"), oid) Then
            If checkspartexist(oid) = False Then
                If DeleteData("ql_mstsparepart", "partoid", oid, CompnyCode) Then
                    Response.Redirect("~\Master\mstsparepart.aspx?awal=true")
                Else
                    showMessage("Error deleting data!", 2)
                End If
            Else
                showMessage("Data telah digunakan di dalam transaksi, data tidak bisa dihapus!", 2)
            End If
        Else
            Exit Sub
        End If
    End Sub
#End Region

    Protected Sub gvdata_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvdata.SelectedIndexChanged

    End Sub
End Class
