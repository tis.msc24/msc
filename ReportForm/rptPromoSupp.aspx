<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPromoSupp.aspx.vb" Inherits="RptPromoSupp" title="MSC - Rekap Promo Supplier" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="width: 100%;">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="X-Large" ForeColor="Navy" Text=".: Rekap Program Promo"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="width: 100%; background-color: transparent" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=center><TABLE><TBODY><TR><TD id="tdPeriod1" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Text="Periode" __designer:wfdid="w1"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton>&nbsp;- <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w6">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w7" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w8" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w9" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w10" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Type" runat="server" Text="Type" __designer:wfdid="w11"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="TypeDDL" runat="server" CssClass="inpText" __designer:wfdid="w12"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>DETAIL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label2" runat="server" Text="Type Promo" __designer:wfdid="w13"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="DDLPromoType" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w14" AutoPostBack="True"><asp:ListItem Value="POINT">TARGET POINT</asp:ListItem>
<asp:ListItem Value="ASPEND">TARGET ASPEND</asp:ListItem>
<asp:ListItem Value="AMOUNT">TARGET AMOUNT</asp:ListItem>
<asp:ListItem Value="QTYTOTAL">TARGET QTY TOTAL</asp:ListItem>
<asp:ListItem Value="QTYBARANG">TARGET QTY PER BARANG</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td3" align=left runat="server" Visible="true">Promo</TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="PromoNya" runat="server" Width="222px" CssClass="inpText" __designer:wfdid="w15"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w17"></asp:ImageButton>&nbsp;<asp:Label id="Oid" runat="server" Visible="False" __designer:wfdid="w18"></asp:Label>&nbsp;<asp:Button id="BtnViewListProm" runat="server" Width="134px" CssClass="btn green" Font-Bold="True" Text="View List Promo" __designer:wfdid="w19"></asp:Button></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right runat="server" Visible="true"></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="GVPromo" runat="server" Width="450px" Font-Size="X-Small" ForeColor="#333333" Visible="False" __designer:wfdid="w20" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="promooid,promocode,promoname" UseAccessibleHeader="False" CellPadding="4" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbProm" runat="server" __designer:wfdid="w1" AutoPostBack="True" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("promooid") %>' OnCheckedChanged="cbProm_CheckedChanged"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="promocode" HeaderText="Kode Promo">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="promoname" HeaderText="Nama Promo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="statuspromo" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="promooid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Supplier</TD><TD align=left colSpan=3><asp:TextBox id="suppname" runat="server" Width="222px" CssClass="inpText" __designer:wfdid="w21"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupplier" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w22"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseSupplier" onclick="btnEraseSupplier_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w23"></asp:ImageButton>&nbsp;<asp:Label id="suppoid" runat="server" Visible="False" __designer:wfdid="w24"></asp:Label>&nbsp;<asp:Button id="BtnViewListSupp" runat="server" Width="134px" CssClass="btn green" Font-Bold="True" Text="View List Supplier" __designer:wfdid="w25"></asp:Button></TD></TR><TR><TD align=right></TD><TD align=left colSpan=3><asp:GridView id="gvSupp" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" Visible="False" __designer:wfdid="w26" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="suppcode,suppoid,suppname,supptype" UseAccessibleHeader="False" CellPadding="4" GridLines="None" PageSize="8" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged1">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSupp" runat="server" __designer:wfdid="w2" AutoPostBack="True" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("suppoid") %>' OnCheckedChanged="cbSupp_CheckedChanged"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="supptype" HeaderText="Tipe" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="promooid" HeaderText="promooid" Visible="False"></asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="Label1" runat="server" Width="91px" Text="Nama Barang" __designer:wfdid="w27"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="ItemNya" runat="server" Width="222px" CssClass="inpText" __designer:wfdid="w28" MaxLength="100"></asp:TextBox>&nbsp;<asp:ImageButton id="sItemNya" onclick="sItemNya_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w29"></asp:ImageButton>&nbsp;<asp:ImageButton id="eItemNya" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w30"></asp:ImageButton> <asp:Button id="BtnViewList" runat="server" Width="134px" CssClass="btn green" Font-Bold="True" Text="View List Katalog" __designer:wfdid="w31"></asp:Button></TD></TR><TR><TD align=left></TD><TD align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w32" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" __designer:wfdid="w1" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TypeBarang" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="promooid" HeaderText="promooid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=4><asp:Label id="labelEx" runat="server" Width="57px" __designer:wfdid="w33"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton> <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w38" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w39"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD align=center colSpan=4><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w40"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" __designer:wfdid="w41" ShowAllPageIds="True" AutoDataBind="true" DisplayGroupTree="False" HasCrystalLogo="False" HasExportButton="False" HasPrintButton="False" HasRefreshButton="True" HasSearchButton="False" HasToggleGroupTreeButton="False"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

