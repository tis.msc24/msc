<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnFABalance.aspx.vb" Inherits="Accounting_trnFAB" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Fixed Asset Balance"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" align="Left" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView2" runat="server" ActiveViewIndex="0" __designer:wfdid="w365"><asp:View id="View3" runat="server" __designer:wfdid="w366"><asp:Panel id="Panel1" runat="server" __designer:wfdid="w367" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 66px" align=left><asp:Label id="Label11" runat="server" Text="Periode " __designer:wfdid="w368"></asp:Label></TD><TD align=left><asp:Label id="Label18" runat="server" Text=":" __designer:wfdid="w368"></asp:Label></TD><TD align=left><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w369"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w370"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w371"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w372"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w373"></asp:ImageButton>&nbsp;</TD></TR><TR><TD style="WIDTH: 66px" align=left><asp:Label id="Label19" runat="server" Text="Filter " __designer:wfdid="w374"></asp:Label></TD><TD align=left><asp:Label id="Label20" runat="server" Text=":" __designer:wfdid="w368"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLfilter" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w375" AutoPostBack="True">
            <asp:ListItem Value="gendesc">Group</asp:ListItem>
            <asp:ListItem Value="fixcode">Kode</asp:ListItem>
            <asp:ListItem Value="fixflag">Status</asp:ListItem>
            <asp:ListItem Value="f.fixdesc">Description</asp:ListItem>
        </asp:DropDownList> &nbsp; <asp:TextBox id="txtFilter" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w376"></asp:TextBox> &nbsp;<asp:ImageButton id="BtnSearch" onclick="BtnSearch_Click" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w377"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnViewAll" onclick="BtnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w378"></asp:ImageButton> </TD></TR><TR><TD align=left colSpan=3><asp:GridView id="GVFixedAsset" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w379" OnPageIndexChanging="GVFixedAsset_PageIndexChanging" GridLines="None" DataKeyNames="cmpcode,fixoid" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="8" OnRowDataBound="GVFixedAsset_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="fixoid" DataNavigateUrlFormatString="~/Accounting/trnFABalance.aspx?oid={0}" DataTextField="fixcode" HeaderText="Fixed Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="gendesc" HeaderText="Group">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdate" HeaderText="Fixed Date" SortExpression="fixdate">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="#0000C0"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixfirstvalue" HeaderText="First Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixpresentvalue" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixflag" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                    <asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label>
                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" CssClass="Important" Text="Click button Find or View All to view data" __designer:wfdid="w380"></asp:Label><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w381" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1" TargetControlID="txtPeriode1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w382" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2" TargetControlID="txtPeriode2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" __designer:wfdid="w383" TargetControlID="txtPeriode1" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" __designer:wfdid="w384" TargetControlID="txtPeriode2" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel></asp:View>&nbsp; </asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">&nbsp;<asp:Image ID="Image3" runat="server"
                                ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                List of Fixed Asset Balance :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 100%" vAlign=top colSpan=3 rowSpan=3><asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" __designer:wfdid="w269"><asp:View id="View1" runat="server" __designer:wfdid="w270"><DIV style="HEIGHT: 100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:Label id="lblNo" runat="server" Text="Draft No." __designer:wfdid="w275" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:Label id="fixmstoid" runat="server" __designer:wfdid="w276" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD id="Td10" align=left>Cabang</TD><TD style="WIDTH: 206px" id="Td11" align=left><asp:DropDownList id="DDLoutlet" runat="server" CssClass="inpText" __designer:wfdid="w1" AutoPostBack="True" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged"></asp:DropDownList></TD><TD id="Td12" align=left><asp:Label id="Label27" runat="server" Text="Asset Code" __designer:wfdid="w272"></asp:Label></TD><TD style="WIDTH: 228px" id="Td13" align=left><asp:TextBox id="FixCode" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w273" MaxLength="20"></asp:TextBox> <asp:Label id="Label1" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w274"></asp:Label></TD><TD id="Td14" align=left><asp:Label id="Label3" runat="server" Font-Bold="True" ForeColor="Black" Text="Informasi" __designer:wfdid="w277" Visible="False"></asp:Label> <asp:Label id="Label30" runat="server" Font-Size="Small" Font-Bold="False" ForeColor="#585858" Text="|" __designer:wfdid="w278" Visible="False"></asp:Label></TD><TD id="Td15" align=left><asp:LinkButton id="lbkDetil" runat="server" CssClass="submenu" __designer:wfdid="w279" Visible="False">More Informasi </asp:LinkButton></TD></TR><TR><TD align=left><asp:Label id="Label5" runat="server" Text="Asset Date" __designer:wfdid="w280"></asp:Label></TD><TD style="WIDTH: 206px" align=left><asp:TextBox id="fixDate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w281" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w282"></asp:ImageButton> <asp:Label id="Label14" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w209"></asp:Label></TD><TD align=left><asp:Label id="Label6" runat="server" Width="106px" Text="Description Asset" __designer:wfdid="w284"></asp:Label></TD><TD style="WIDTH: 228px" align=left><asp:TextBox id="fixdesc" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w285" MaxLength="200"></asp:TextBox> <asp:Label id="Label4" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w286"></asp:Label>&nbsp;<asp:ImageButton id="btnLookUp" onclick="btnLookUp_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w243"></asp:ImageButton> <asp:ImageButton id="btnHapus" onclick="btnHapus_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w244"></asp:ImageButton> <asp:Label id="lblItemOid" runat="server" ForeColor="DarkBlue" __designer:wfdid="w245" Visible="False"></asp:Label>&nbsp;<asp:Label id="ItemCode" runat="server" ForeColor="DarkBlue" __designer:wfdid="w245" Visible="False"></asp:Label> </TD><TD align=left><asp:Label id="Label28" runat="server" Text="Type asset" __designer:wfdid="w287"></asp:Label></TD><TD align=left><asp:DropDownList id="fixgroup" runat="server" Width="154px" CssClass="inpText" __designer:wfdid="w288" AutoPostBack="True" OnSelectedIndexChanged="fixgroup_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD align=left></TD><TD style="WIDTH: 206px" align=left></TD><TD align=left></TD><TD align=left colSpan=3><asp:GridView id="GVmstitem" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w137" OnSelectedIndexChanged="GVmstitem_SelectedIndexChanged" OnPageIndexChanging="GVmstitem_PageIndexChanging" GridLines="None" DataKeyNames="itemoid,itemcode,itemdesc" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="5">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code" SortExpression="itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="DarkGoldenrod"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="Label15" runat="server" Text="Currency" __designer:wfdid="w289"></asp:Label></TD><TD style="WIDTH: 206px" align=left><asp:DropDownList id="CurroidDDL" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w290" Enabled="False"></asp:DropDownList></TD><TD align=left><asp:Label id="Label7" runat="server" Width="95px" Text="Harga Perolehan" __designer:wfdid="w291"></asp:Label> </TD><TD style="WIDTH: 228px" align=left><asp:TextBox id="fixfirstvalue" runat="server" CssClass="inpText" __designer:wfdid="w292" AutoPostBack="True" MaxLength="12">0</asp:TextBox> <asp:Label id="Label2x" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w293"></asp:Label></TD><TD align=left><asp:Label id="Label29" runat="server" Text="Nilai Buku" __designer:wfdid="w294"></asp:Label>&nbsp; </TD><TD align=left><asp:TextBox id="fixPresentValue" runat="server" Width="145px" CssClass="inpText" __designer:wfdid="w295" AutoPostBack="True" MaxLength="12">0</asp:TextBox><asp:Label id="Label40" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w296"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" ForeColor="DarkBlue" Text="Accum. Dep. Value" __designer:wfdid="w297"></asp:Label></TD><TD style="WIDTH: 206px" align=left><asp:TextBox id="accumDV" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w298" Enabled="False" OnTextChanged="accumDV_TextChanged" ReadOnly="True">0</asp:TextBox></TD><TD align=left><asp:Label id="Label8" runat="server" Text="Depreciation" __designer:wfdid="w299"></asp:Label> </TD><TD style="WIDTH: 228px" align=left><asp:TextBox id="fixdepmonth" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w300" AutoPostBack="True" MaxLength="2">0</asp:TextBox> <asp:Label id="Label9" runat="server" Font-Size="X-Small" ForeColor="Red" Text="(month)" __designer:wfdid="w301"></asp:Label> <asp:Label id="Label33" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w302"></asp:Label></TD><TD align=left><asp:Label id="Label10" runat="server" Width="76px" Text="Dep. Value" __designer:wfdid="w303"></asp:Label></TD><TD align=left><asp:TextBox id="fixdepval" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w304" Enabled="False" ReadOnly="True">0</asp:TextBox> </TD></TR><TR><TD id="TD9" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label42" runat="server" Width="128px" ForeColor="DarkBlue" Text="Aset Value Terakhir" __designer:wfdid="w305"></asp:Label></TD><TD style="WIDTH: 206px" id="TD8" align=left runat="server" Visible="true"><asp:TextBox id="fixLastAsset" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w306" AutoPostBack="True" OnTextChanged="fixLastAsset_TextChanged">0</asp:TextBox> <asp:Label id="Label16" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w307"></asp:Label></TD><TD id="Td1" align=left colSpan=4 runat="server"><asp:Label id="Label13" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w308"></asp:Label> <asp:Label id="Label12" runat="server" Font-Bold="True" ForeColor="Red" Text="Set Depreciation (month)  -1, If have no Depreciation !!" __designer:wfdid="w309"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label17" runat="server" Text="Akun Asset" __designer:wfdid="w310"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLassets" runat="server" Width="350px" CssClass="inpText" __designer:wfdid="w311"></asp:DropDownList></TD><TD align=left><asp:Label id="lblPOST" runat="server" Text="lblPOST" __designer:wfdid="w312" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="CutofDate" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w313" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label23" runat="server" Text="Accum. Dep" __designer:wfdid="w314"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLaccum" runat="server" Width="350px" CssClass="inpText" __designer:wfdid="w315"></asp:DropDownList></TD><TD align=left><asp:Label id="payacctgoid" runat="server" Text="payacctgoid" __designer:wfdid="w316" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="AccumVal" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w317" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label22" runat="server" Text="Accum. Dep. Expense" __designer:wfdid="w318"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLadExpense" runat="server" Width="350px" CssClass="inpText" __designer:wfdid="w319"></asp:DropDownList></TD><TD align=left></TD><TD align=left><asp:TextBox id="fixlocation" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w320" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w321" Format="dd/MM/yyyy" PopupButtonID="btnDate" TargetControlID="fixDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w8" TargetControlID="fixDate" CultureName="en-US" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="True" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"></ajaxToolkit:MaskedEditExtender></TD><TD align=left colSpan=3><asp:ImageButton id="GerenatedBtn" onclick="GerenatedBtn_Click" runat="server" ImageUrl="~/Images/generate.png" __designer:wfdid="w322"></asp:ImageButton> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w323" TargetControlID="fixLastAsset" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w324" TargetControlID="fixfirstvalue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" __designer:wfdid="w325" TargetControlID="fixPresentValue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender4" runat="server" __designer:wfdid="w326" TargetControlID="fixdepmonth" ValidChars="-1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD align=left></TD><TD align=left>&nbsp; <asp:TextBox id="fixperson" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w327" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="GVFixedAssetdtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w328" OnSelectedIndexChanged="GVFixedAssetdtl_SelectedIndexChanged" OnPageIndexChanging="GVFixedAssetdtl_PageIndexChanging" GridLines="None" DataKeyNames="fixperiod" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="8" OnRowDataBound="GVFixedAssetdtl_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField>
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="fixperiod" HeaderText="Periode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepvalue" DataFormatString="{0:#,##0.00}" HeaderText="Depreciation">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperiodvalue" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepaccum" DataFormatString="{0:#,##0.00}" HeaderText="Accumulation">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixflag" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=4>Created By&nbsp;<asp:Label id="lblUser" runat="server" Font-Bold="True" __designer:wfdid="w329">User</asp:Label>&nbsp; On <asp:Label id="lblTime" runat="server" Font-Bold="True" __designer:wfdid="w330">Time</asp:Label>,&nbsp; Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w331"></asp:Label>&nbsp; By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w332"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="ButtonSave" onclick="ButtonSave_Click" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w334"></asp:ImageButton> <asp:ImageButton id="ButtonCancel" onclick="ButtonCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w335"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnDelete" onclick="BtnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w336"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w337"></asp:ImageButton></TD><TD align=left></TD><TD align=left><asp:TextBox id="fixother" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w338" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w339"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w340"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD><TD align=left></TD><TD align=left></TD></TR></TBODY></TABLE></DIV></asp:View> </asp:MultiView> </TD></TR><TR></TR><TR></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            &nbsp;<strong><span style="font-size: 9pt">Form Fixed Asset Balance :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblCaption"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>