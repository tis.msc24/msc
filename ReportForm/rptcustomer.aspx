<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptcustomer.aspx.vb" Inherits="ReportForm_rptDeadStock" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" style="width: 976px; height: 168px">
        <tr>
            <td colspan="3" rowspan="3" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                                Text="Report Customer" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                </table>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 560px; HEIGHT: 72px"><TBODY><TR><TD style="FONT-SIZE: x-small; WIDTH: 117px; TEXT-ALIGN: right" align=right colSpan=4>Period&nbsp;</TD><TD style="FONT-SIZE: x-small; TEXT-ALIGN: left" align=right colSpan=1>: </TD><TD style="FONT-SIZE: x-small; TEXT-ALIGN: left" align=right colSpan=1><asp:DropDownList id="period" runat="server" Width="168px" CssClass="inpText" __designer:wfdid="w13" AutoPostBack="True"><asp:ListItem Value="0">0 Bulan</asp:ListItem>
<asp:ListItem Value="1">1 Bulan</asp:ListItem>
<asp:ListItem Value="2">2 Bulan</asp:ListItem>
<asp:ListItem Value="3">3 Bulan</asp:ListItem>
<asp:ListItem Value="6">6 Bulan</asp:ListItem>
<asp:ListItem Value="9">9 Bulan</asp:ListItem>
<asp:ListItem Value="12">12 Bulan</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 117px; TEXT-ALIGN: right" align=right colSpan=4>Customer </TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; TEXT-ALIGN: left" align=right colSpan=1>: </TD><TD style="FONT-SIZE: x-small; TEXT-ALIGN: left" align=right colSpan=1><asp:TextBox id="custname" runat="server" Width="162px" CssClass="inpText" __designer:wfdid="w14"></asp:TextBox>&nbsp;<asp:ImageButton id="imbcustlist" onclick="imbcustlist_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w15"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbcustdel" onclick="imbcustdel_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:Label id="custoid" runat="server" __designer:wfdid="w17" Visible="False"></asp:Label>&nbsp; <asp:Label id="custoidD" runat="server" __designer:wfdid="w18" Visible="False"></asp:Label> </TD></TR><TR><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; WIDTH: 117px; TEXT-ALIGN: right" align=right colSpan=4></TD><TD style="FONT-SIZE: x-small; VERTICAL-ALIGN: top; TEXT-ALIGN: left" align=right colSpan=1></TD><TD style="FONT-SIZE: x-small; TEXT-ALIGN: left" align=right colSpan=1><asp:GridView id="gvCust" runat="server" Width="432px" ForeColor="#333333" __designer:wfdid="w92" Visible="False" PageSize="8" CellPadding="4" GridLines="None" OnSelectedIndexChanged="gvCust_SelectedIndexChanged" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="custoid,custname" OnPageIndexChanging="gvCust_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Cust. Code">
<ControlStyle BorderStyle="Solid"></ControlStyle>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Cust. Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Addres">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
  <asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
  
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 38px; TEXT-ALIGN: center" align=center colSpan=6><asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w7"></asp:Label><BR /><asp:ImageButton id="btnViewReport" onclick="btnViewReport_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton>&nbsp;<asp:ImageButton id="BTNpRINT" onclick="BTNpRINT_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton> <asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton> <asp:ImageButton id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w11"></asp:ImageButton></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w868" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" OnNavigate="CrystalReportViewer1_Navigate"></CR:CrystalReportViewer> 
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="BTNpRINT"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>

