Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Public Class ClassToken
    Dim Encoder As New System.Text.ASCIIEncoding()

    Public Function SHA1Hash(ByVal InputStr As String, ByVal InputCode As String) As String
        Dim SHA1Hasher As New SHA1CryptoServiceProvider()
        If InputCode = "RC" Then
            Return ToHexString(SHA1Hasher.ComputeHash(Encoder.GetBytes(InputStr)))
        ElseIf InputCode = "PC" Then
            Return ToNumbString(SHA1Hasher.ComputeHash(Encoder.GetBytes(InputStr)))
        End If
    End Function

    Private Function ToHexString(ByVal ByteArray As Byte()) As String
        Dim i As Integer
        For i = LBound(ByteArray) To UBound(ByteArray)
            ToHexString &= Hex(ByteArray(i))
        Next
        'Return Convert.ToBase64String(ByteArray)
    End Function

    Private Function ToNumbString(ByVal ByteArray As Byte()) As String
        Dim i As Integer
        For i = LBound(ByteArray) To UBound(ByteArray)
            ToNumbString &= Int(ByteArray(i))
        Next
    End Function
End Class
