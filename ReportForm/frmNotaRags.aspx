<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmNotaRags.aspx.vb" Inherits="ReportForm_NotaRags" title="" EnableEventValidation="false" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Laporan Nota Rombeng" CssClass="Title" ForeColor="Navy" Font-Size="X-Large"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" colspan="2" style="background-color: transparent; width: 100%;" valign="center">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" __designer:wfdid="w2" DefaultButton="btnViewReport"><TABLE><TBODY><TR id="BusinessUnitA" runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Cabang" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" Width="115px" CssClass="inpText" __designer:wfdid="w4" AutoPostBack="True"></asp:DropDownList></TD></TR><TR id="Tr1" runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Type" __designer:wfdid="w5"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLType" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w6" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>DETAIL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Tanggal</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w8" ToolTip="dd/MM/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton>&nbsp;<asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w14"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w15" ToolTip="dd/MM/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w17"></asp:Label></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label18" runat="server" Text="Pay. Type" __designer:wfdid="w16"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLPayType" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w17" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="BKM">CASH</asp:ListItem>
<asp:ListItem Value="BBM">TRANSFER</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label5" runat="server" Text="Akun COA" __designer:wfdid="w27"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left><asp:TextBox id="FilterTextCust" runat="server" Width="150px" Height="25px" CssClass="inpText" __designer:wfdid="w28" Rows="4" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search.gif" Height="25px" __designer:wfdid="w29"></asp:ImageButton> <asp:ImageButton id="btnClearCust" runat="server" ImageUrl="~/Images/erase.bmp" Height="25px" __designer:wfdid="w30"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="No. Cash/Bank" __designer:wfdid="w23"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" vAlign=top align=left><asp:TextBox id="FilterTextNo" runat="server" Width="150px" Height="25px" CssClass="inpText" __designer:wfdid="w24" Rows="4" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="btnSearchNo" runat="server" ImageUrl="~/Images/search.gif" Height="25px" __designer:wfdid="w25"></asp:ImageButton> <asp:ImageButton id="btnClearNo" runat="server" ImageUrl="~/Images/erase.bmp" Height="25px" __designer:wfdid="w26"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="No. Nota" __designer:wfdid="w23"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" vAlign=top align=left><asp:TextBox id="regno" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w9" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindReg" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w10"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseReg" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w11"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="lblARNo" runat="server" Text="Katalog" __designer:wfdid="w88" Visible="False"></asp:Label></TD><TD class="Label" align=center rowSpan=1><asp:Label id="lblSeptARNo" runat="server" Text=":" __designer:wfdid="w89" Visible="False"></asp:Label></TD><TD class="Label" vAlign=top align=left><asp:TextBox id="FilterTextAR" runat="server" Width="150px" Height="25px" CssClass="inpText" __designer:wfdid="w34" Rows="4" TextMode="MultiLine" Visible="False"></asp:TextBox> <asp:ImageButton id="btnSearchAR" runat="server" ImageUrl="~/Images/search.gif" Height="25px" __designer:wfdid="w35" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearAR" runat="server" ImageUrl="~/Images/erase.bmp" Height="25px" __designer:wfdid="w36" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w37" TargetControlID="FilterPeriod1" PopupButtonID="CalPeriod1" Format="MM/dd/yyyy">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w38" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999" CultureName="en-US"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w39" TargetControlID="FilterPeriod2" PopupButtonID="CalPeriod2" Format="MM/dd/yyyy">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w40" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999" CultureName="en-US"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w41"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w42"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w43"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><DIV><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w45" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV style="WIDTH: 80%" id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w46"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>&nbsp;</TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" __designer:dtid="281474976710682" __designer:wfdid="w47" HasDrillUpButton="False" HasToggleGroupTreeButton="False" HasExportButton="False" HasPrintButton="False" HasCrystalLogo="False" HasViewList="False" AutoDataBind="True"></CR:CrystalReportViewer> 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListNo" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListNo" runat="server" Width="700px" CssClass="modalBox" __designer:wfdid="w48" DefaultButton="btnFindListNo" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lbListNo" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Cash/Bank No." __designer:wfdid="w49"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListNo" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w50"><asp:ListItem Value="cashbankno">Cash/Bank No.</asp:ListItem>
<asp:ListItem Value="acctgdesc">Account</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListNo" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w51"></asp:TextBox> <asp:ImageButton id="btnFindListNo" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w52"></asp:ImageButton> <asp:ImageButton id="btnAllListNo" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w53"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllDO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneDO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedDO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListDO" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w54" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbListNo" runat="server" __designer:wfdid="w8" ToolTip='<%# eval("cashbankoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="cashbankno" HeaderText="Cash/Bank No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="createdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListNo" runat="server" __designer:wfdid="w55">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListNo" runat="server" __designer:wfdid="w56">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListNo" runat="server" __designer:wfdid="w57" TargetControlID="btnHideListNo" Drag="True" PopupDragHandleControlID="lbListNo" PopupControlID="pnlListNo" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListNo" runat="server" __designer:wfdid="w58" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListCust" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" __designer:wfdid="w59" DefaultButton="btnFindListCust" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lbListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of COA" __designer:wfdid="w60"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListCust" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w61"><asp:ListItem Value="custcode">Code</asp:ListItem>
<asp:ListItem Value="custname">Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListCust" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w62"></asp:TextBox> <asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w63"></asp:ImageButton> <asp:ImageButton id="btnAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w64"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w65" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbListCust" runat="server" ToolTip='<%# eval("custoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListCust" runat="server" __designer:wfdid="w66">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListCust" runat="server" __designer:wfdid="w67">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" __designer:wfdid="w68" TargetControlID="btnHideListCust" Drag="True" PopupDragHandleControlID="lbListCust" PopupControlID="pnlListCust" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" __designer:wfdid="w69" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListReg" runat="server"><contenttemplate>
<asp:Panel id="PanelListReg" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListReg" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Nota Rombeng"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListReg" runat="server" Width="100%" DefaultButton="btnFindListReg"><asp:Label id="Label245" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListReg" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="registerno">No. Nota</asp:ListItem>
<asp:ListItem Value="cashbankno">No. Cash/Bank</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListReg" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListReg" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListReg" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllReg" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneReg" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedReg" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListReg" runat="server" Width="98%" ForeColor="Black" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                    <asp:CheckBox ID="cbLMPO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("registermstoid") %>' />
                                                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="PERSONNIP" HeaderText="No.Nota">
<HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="registerno" HeaderText="No. Cash/Bank">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PERSONNIP" HeaderText="Jabatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="STATUS" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToListListReg" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListReg" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListReg" runat="server" TargetControlID="btnHiddenListReg" PopupDragHandleControlID="lblListReg" PopupControlID="PanelListReg" BackgroundCssClass="modalBackground">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListReg" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListAR" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListAR" runat="server" Width="650px" CssClass="modalBox" DefaultButton="btnFindListAR" __designer:wfdid="w70" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lbListAR" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of katalog" __designer:wfdid="w71"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListAR" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w72"><asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListAR" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w73"></asp:TextBox> <asp:ImageButton id="btnFindListAR" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w74"></asp:ImageButton> <asp:ImageButton id="btnAllListAR" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w75"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3>Jenis Barang : <asp:DropDownList id="JenisBarangDDL" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w4"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListAR" runat="server" Width="96%" ForeColor="#333333" __designer:wfdid="w8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" DataKeyNames="itemcode">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                        <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                &nbsp;
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListAR" runat="server" __designer:wfdid="w77">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListAR" runat="server" __designer:wfdid="w78">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListAR" runat="server" __designer:wfdid="w79" TargetControlID="btnHideListAR" BackgroundCssClass="modalBackground" PopupControlID="pnlListAR" PopupDragHandleControlID="lbListAR" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListAR" runat="server" __designer:wfdid="w80" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w81" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w82"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w83"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w84"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w85"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w86" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w87" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

