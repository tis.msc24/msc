<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="CheckTeknisi.aspx.vb" Inherits="Transaction_CheckTeknisi" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="100%">
        <tr>
           <td align="left" colspan="3" style="background-color: silver" class="header">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="Navy" Text=".: Data Check Teknisi" Font-Names="Verdana"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <strong>
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />List Of Teknisi</strong><strong>:.</strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 90px"><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w28"></asp:Label></TD><TD style="WIDTH: 6px"><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w29"></asp:Label></TD><TD colSpan=3><asp:DropDownList id="ddlFilter" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w30"><asp:ListItem Value="QLR.BARCODE">Barcode</asp:ListItem>
<asp:ListItem Value="QLR.REQCODE">Code</asp:ListItem>
<asp:ListItem Value="QLR.REQITEMNAME">Nama Barang</asp:ListItem>
<asp:ListItem Value="QLM.GENDESC">Merk</asp:ListItem>
<asp:ListItem Value="PERSONNAME">Teknisi</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="155px" CssClass="inpText" __designer:wfdid="w31" MaxLength="100"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind" tabIndex=1 runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w32"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w33"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 90px" id="TD1" runat="server" Visible="false"><asp:CheckBox id="cbPeriod" runat="server" Text="Period" __designer:wfdid="w34"></asp:CheckBox></TD><TD style="WIDTH: 6px" id="TD3" runat="server" Visible="false"><asp:Label id="Label3" runat="server" Text=":" __designer:wfdid="w35"></asp:Label></TD><TD id="TD2" colSpan=3 runat="server" Visible="false"><asp:TextBox id="txtPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w36"></asp:TextBox> <asp:ImageButton id="btnCal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton>&nbsp; <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w38"></asp:Label>&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w39"></asp:TextBox> <asp:ImageButton id="btnCal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton>&nbsp; <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w41"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w42"></asp:CheckBox></TD><TD style="WIDTH: 6px"><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w43"></asp:Label></TD><TD colSpan=3><asp:DropDownList id="ddlStatus" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w44"><asp:ListItem>Check</asp:ListItem>
<asp:ListItem>Receive</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 6px" colSpan=5><asp:GridView style="max-width: 940px" id="GVListTeknisi" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w20" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="White" BorderColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="REQOID" DataNavigateUrlFormatString="~\Transaction\CheckTeknisi.aspx?idPage={0}" DataTextField="BARCODE" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="110px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="110px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="REQCODE" HeaderText="No. Penerimaan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Customer" Visible="False">
<ControlStyle BackColor="Red"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="160px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Font-Overline="False" Width="160px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMNAME" HeaderText="Nama Barang" SortExpression="REQITEMNAME">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="180px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="180px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GENDESC" HeaderText="Merk" SortExpression="GENDESC">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="180px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="180px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PERSONNAME" HeaderText="Teknisi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQSTATUS" HeaderText="Status" SortExpression="REQSTATUS">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label73" runat="server" ForeColor="Red" Text="Data Not Found"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=5><ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" __designer:wfdid="w46" TargetControlID="txtPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CECal1" runat="server" __designer:wfdid="w47" TargetControlID="txtPeriod1" Format="dd/MM/yyyy" PopupButtonID="btnCal1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" __designer:wfdid="w48" TargetControlID="txtPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CECal2" runat="server" __designer:wfdid="w49" TargetControlID="txtPeriod2" Format="dd/MM/yyyy" PopupButtonID="btnCal2"></ajaxToolkit:CalendarExtender> <asp:SqlDataSource id="SDSGVList" runat="server" __designer:wfdid="w50" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT c.CMPCODE, c.REQOID, c.BARCODE, g1.gendesc, g2.gendesc AS Expr1, c.REQITEMNAME, c.REQSERVICECAT, c.REQFLAG, c.REQSTATUS FROM QL_TRNREQUEST AS c INNER JOIN QL_mstgen AS g1 ON c.REQITEMTYPE = g1.genoid INNER JOIN QL_mstgen AS g2 ON c.REQITEMBRAND = g2.genoid WHERE (c.CMPCODE = @CMPCODE) AND (c.REQOID = @REQOID)"><SelectParameters>
<asp:Parameter Name="CMPCODE"></asp:Parameter>
<asp:Parameter Name="REQOID"></asp:Parameter>
</SelectParameters>
</asp:SqlDataSource></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <strong>
                                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                Form Teknisi</strong><strong>:. </strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" __designer:wfdid="w172"><asp:View id="View1" runat="server" __designer:wfdid="w173"><TABLE width="100%"><TBODY><TR><TD colSpan=3><asp:Label id="lblInformasi" runat="server" Font-Size="Small" Font-Bold="True" Text="Informasi" __designer:wfdid="w174"></asp:Label> <asp:Label id="Label9" runat="server" Font-Size="Small" Text="|" __designer:wfdid="w175"></asp:Label> <asp:LinkButton id="lbDetKerusakan" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w176">Detail Kerusakan</asp:LinkButton> <asp:Label id="Label11" runat="server" Font-Size="Small" Text="|" __designer:wfdid="w177"></asp:Label> <asp:LinkButton id="lbDetSparepart" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w178">Detail Sparepart</asp:LinkButton></TD><TD style="WIDTH: 33px"></TD><TD style="WIDTH: 2px"></TD><TD style="WIDTH: 345px"></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label76" runat="server" Text="Cabang" __designer:wfdid="w179"></asp:Label></TD><TD style="WIDTH: 7px"><asp:Label id="Label78" runat="server" Text=":" __designer:wfdid="w179"></asp:Label></TD><TD style="WIDTH: 369px"><asp:DropDownList id="DdlCabang" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w1" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 33px"></TD><TD style="WIDTH: 2px"></TD><TD style="WIDTH: 345px"></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label74" runat="server" Text="Barcode" __designer:wfdid="w179"></asp:Label></TD><TD style="WIDTH: 7px"><asp:Label id="Label75" runat="server" Text=":" __designer:wfdid="w180"></asp:Label></TD><TD style="WIDTH: 369px"><asp:TextBox id="barcode" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w181" AutoPostBack="True"></asp:TextBox>&nbsp;</TD><TD style="WIDTH: 33px">Teknisi</TD><TD style="WIDTH: 2px">:</TD><TD style="WIDTH: 345px"><asp:TextBox id="teknisi" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w182" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label13" runat="server" Text="No. Tanda Terima" __designer:wfdid="w183"></asp:Label> <asp:Label id="Label64" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w184"></asp:Label></TD><TD style="WIDTH: 7px"><asp:Label id="Label23" runat="server" Text=":" __designer:wfdid="w185"></asp:Label></TD><TD style="WIDTH: 369px"><asp:TextBox id="txtNo" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w186" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w187"></asp:ImageButton> <asp:ImageButton id="btnErase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w188"></asp:ImageButton></TD><TD style="WIDTH: 33px"><asp:Label id="Label32" runat="server" Text="Status" __designer:wfdid="w189"></asp:Label></TD><TD style="WIDTH: 2px"><asp:Label id="Label33" runat="server" Text=":" __designer:wfdid="w190"></asp:Label></TD><TD style="WIDTH: 345px"><asp:TextBox id="txtStatus" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w191" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label14" runat="server" Text="Tanggal Terima" __designer:wfdid="w192"></asp:Label></TD><TD style="WIDTH: 7px"><asp:Label id="Label24" runat="server" Text=":" __designer:wfdid="w193"></asp:Label></TD><TD style="WIDTH: 369px"><asp:TextBox id="txtTglTerima" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w194" Enabled="False"></asp:TextBox>&nbsp; <asp:Label id="Label22" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w195"></asp:Label></TD><TD style="WIDTH: 33px"></TD><TD style="WIDTH: 2px"></TD><TD style="WIDTH: 345px">&nbsp;</TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label15" runat="server" Text="Waktu Terima" __designer:wfdid="w196"></asp:Label></TD><TD style="WIDTH: 7px"><asp:Label id="Label25" runat="server" Text=":" __designer:wfdid="w197"></asp:Label></TD><TD style="WIDTH: 369px"><asp:TextBox id="txtWaktuTerima" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w198" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 33px"></TD><TD style="WIDTH: 2px"></TD><TD style="WIDTH: 345px"><asp:TextBox id="txtOid" runat="server" Width="18px" CssClass="inpText" __designer:wfdid="w199" Visible="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label17" runat="server" Text="Jenis Barang" __designer:wfdid="w200"></asp:Label></TD><TD style="WIDTH: 7px"><asp:Label id="Label27" runat="server" Text=":" __designer:wfdid="w201"></asp:Label></TD><TD style="WIDTH: 369px"><asp:TextBox id="txtJenisBrg" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w202" Enabled="False"></asp:TextBox> <asp:Label id="REQITEMTYPE" runat="server" __designer:wfdid="w203" Visible="False"></asp:Label></TD><TD style="WIDTH: 33px"><asp:Label id="Label16" runat="server" Width="64px" Text="Customer" __designer:wfdid="w204" Visible="False"></asp:Label></TD><TD style="WIDTH: 2px"><asp:Label id="Label26" runat="server" Text=":" __designer:wfdid="w205" Visible="False"></asp:Label></TD><TD style="WIDTH: 345px"><asp:TextBox id="txtNamaCust" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w206" Enabled="False" Visible="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label18" runat="server" Text="Merk" __designer:wfdid="w207"></asp:Label></TD><TD style="WIDTH: 7px"><asp:Label id="Label28" runat="server" Text=":" __designer:wfdid="w208"></asp:Label></TD><TD style="WIDTH: 369px"><asp:TextBox id="txtMerk" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w209" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="ItemOid" runat="server" __designer:wfdid="w203" Visible="False"></asp:Label></TD><TD style="WIDTH: 33px"></TD><TD style="WIDTH: 2px"></TD><TD style="WIDTH: 345px"></TD></TR><TR><TD style="WIDTH: 155px"><asp:Label id="Label19" runat="server" Text="Nama Barang" __designer:wfdid="w210"></asp:Label></TD><TD style="WIDTH: 7px"><asp:Label id="Label29" runat="server" Text=":" __designer:wfdid="w211"></asp:Label></TD><TD style="WIDTH: 369px"><asp:TextBox id="txtNamaBrg" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w212" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 33px"></TD><TD style="WIDTH: 2px"></TD><TD style="WIDTH: 345px"></TD></TR><TR><TD align=left colSpan=6><asp:Label id="Label21" runat="server" Text="Detail Kerusakan dari Keterangan Customer" __designer:wfdid="w213"></asp:Label></TD></TR><TR><TD align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%"><asp:GridView id="GVCust" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w214" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="Row" HeaderText="No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQDTLOID" HeaderText="ID" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQDTLJOB" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label60" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server" __designer:wfdid="w215"><TABLE width="100%"><TBODY><TR><TD style="HEIGHT: 14px" colSpan=3><asp:LinkButton id="lbInformasi" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w216">Informasi</asp:LinkButton> <asp:Label id="Label35" runat="server" Font-Size="Small" Text="|" __designer:wfdid="w217"></asp:Label> <asp:Label id="Label36" runat="server" Font-Size="Small" Font-Bold="True" Text="Detail Kerusakan" __designer:wfdid="w218"></asp:Label> <asp:Label id="Label37" runat="server" Font-Size="Small" Text="|" __designer:wfdid="w219"></asp:Label> <asp:LinkButton id="lbDetSparepart2" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w220">Detail Sparepart</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 110px; HEIGHT: 14px"><asp:Label id="Label39" runat="server" Text="Type" __designer:wfdid="w221"></asp:Label> </TD><TD style="WIDTH: 2px; HEIGHT: 14px"><asp:Label id="Label20" runat="server" Text=":" __designer:wfdid="w222"></asp:Label></TD><TD style="WIDTH: 844px"><asp:DropDownList id="ddlTypeJob" runat="server" Width="98px" CssClass="inpText" __designer:wfdid="w223" AutoPostBack="True"></asp:DropDownList> <asp:TextBox id="txtI_U" runat="server" Width="48px" CssClass="inpText" __designer:wfdid="w224" Visible="False"></asp:TextBox> <asp:Label id="lbID" runat="server" __designer:wfdid="w225" Visible="False"></asp:Label> <asp:Label id="IDJob" runat="server" __designer:wfdid="w226" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 110px"><asp:Label id="Label40" runat="server" Text="Job" __designer:wfdid="w227"></asp:Label> <asp:Label id="Label63" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w228"></asp:Label></TD><TD style="WIDTH: 2px"><asp:Label id="Label30" runat="server" Text=":" __designer:wfdid="w229"></asp:Label></TD><TD style="WIDTH: 844px"><asp:TextBox id="txtJob" runat="server" Width="294px" CssClass="inpText" __designer:wfdid="w230" OnTextChanged="txtJob_TextChanged" MaxLength="100"></asp:TextBox> <asp:ImageButton id="btnSearchJob" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w231"></asp:ImageButton> <asp:ImageButton id="btnEraseJob" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w232"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 110px"><asp:Label id="Label41" runat="server" Text="Amount(Rp)" __designer:wfdid="w233"></asp:Label> </TD><TD style="WIDTH: 2px"><asp:Label id="Label31" runat="server" Text=":" __designer:wfdid="w234"></asp:Label></TD><TD style="WIDTH: 844px"><asp:TextBox id="txtAmount" runat="server" Width="131px" CssClass="inpTextDisabled" __designer:wfdid="w235" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 110px">Keterangan</TD><TD style="WIDTH: 2px"><asp:Label id="Label61" runat="server" Width="2px" Text=":" __designer:wfdid="w236"></asp:Label></TD><TD style="WIDTH: 844px"><asp:TextBox id="txtNoteDtl" runat="server" Width="334px" Height="20px" CssClass="inpText" Font-Size="Medium" __designer:wfdid="w237" MaxLength="100" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label62" runat="server" ForeColor="Red" Text="Maks. 100 Karakter" __designer:wfdid="w238"></asp:Label>//</TD></TR><TR><TD colSpan=3><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" __designer:wfdid="w239"></asp:ImageButton> <asp:ImageButton id="btnClearKerusakan" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w240"></asp:ImageButton></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 170px"><asp:GridView id="GVDetKerusakan" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w241" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="MCHECKOID,GENDESC" OnRowDataBound="GVDetKerusakan_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="MCHECKOID" HeaderText="ID" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVOID" HeaderText="IDJob" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GENOID" HeaderText="IDType" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GENDESC" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVDESC" HeaderText="Job">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVTARIF" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="NOTEDTL" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbAddSparepart" onclick="lbAddSparepart_Click" runat="server" CommandArgument='<%# Eval("MCHECKOID")  &  "?" &  Eval("GENDESC")  &  "?"  & Eval("ITSERVDESC") %>'>Add Sparepart</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" BorderColor="Blue" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="5%"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label38" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail data"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View3" runat="server" __designer:wfdid="w242"><TABLE width=950><TBODY><TR><TD colSpan=3><asp:LinkButton id="lbInformasi2" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w243">Informasi</asp:LinkButton> <asp:Label id="Label43" runat="server" Font-Size="Small" Text="|" __designer:wfdid="w244"></asp:Label>&nbsp;<asp:LinkButton id="lbDetKerusakan2" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w245">Detail Kerusakan</asp:LinkButton>&nbsp;<asp:Label id="Label45" runat="server" Font-Size="Small" Text="|" __designer:wfdid="w246"></asp:Label>&nbsp;<asp:Label id="Label46" runat="server" Font-Size="Small" Font-Bold="True" Text="Detail Sparepart" __designer:wfdid="w247"></asp:Label></TD></TR><TR><TD colSpan=3></TD></TR></TBODY></TABLE><TABLE width=950><TBODY><TR><TD style="WIDTH: 131px"><asp:Label id="Label48" runat="server" Text="Type" __designer:wfdid="w248"></asp:Label> <asp:Label id="Label65" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w249"></asp:Label></TD><TD style="WIDTH: 10px"><asp:Label id="Label54" runat="server" Text=":" __designer:wfdid="w250"></asp:Label></TD><TD><asp:TextBox id="txtType" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w251" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="lblStatusPart" runat="server" __designer:wfdid="w252" Visible="False"></asp:Label>&nbsp;<asp:Label id="lbIDDtl" runat="server" __designer:wfdid="w253" Visible="False"></asp:Label> <asp:Label id="lbIDPart" runat="server" __designer:wfdid="w254" Visible="False"></asp:Label>&nbsp; <asp:Label id="idSparePart" runat="server" __designer:wfdid="w255" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 131px"><asp:Label id="Label49" runat="server" Text="Job" __designer:wfdid="w256"></asp:Label> <asp:Label id="Label66" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w257"></asp:Label></TD><TD style="WIDTH: 10px"><asp:Label id="Label55" runat="server" Text=":" __designer:wfdid="w258"></asp:Label></TD><TD><asp:TextBox id="txtJobSpart" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w259" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 131px">Gudang</TD><TD style="WIDTH: 10px">:</TD><TD><asp:DropDownList id="matLoc" runat="server" Height="20px" CssClass="inpText" __designer:dtid="2814749767106643" __designer:wfdid="w260" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 131px"><asp:Label id="Label50" runat="server" Text="Sparepart" __designer:wfdid="w261"></asp:Label> <asp:Label id="Label67" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w262"></asp:Label></TD><TD style="WIDTH: 10px"><asp:Label id="Label56" runat="server" Text=":" __designer:wfdid="w263"></asp:Label></TD><TD><asp:TextBox id="txtNamaSpart" runat="server" Width="272px" CssClass="inpText" __designer:wfdid="w264" MaxLength="100"></asp:TextBox> <asp:ImageButton id="btnSearchSpart" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w265"></asp:ImageButton> <asp:ImageButton id="btnEraseSpart" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w266"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 131px">Merk</TD><TD style="WIDTH: 10px">:</TD><TD><asp:TextBox id="merkspr" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w267" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 131px"><asp:Label id="Label51" runat="server" Text="Jumlah" __designer:wfdid="w268"></asp:Label> <asp:Label id="Label68" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w269"></asp:Label></TD><TD style="WIDTH: 10px"><asp:Label id="Label57" runat="server" Text=":" __designer:wfdid="w270"></asp:Label></TD><TD><asp:TextBox id="txtJumlah" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w271" MaxLength="4"></asp:TextBox>&nbsp;<asp:Label id="Label77" runat="server" Text="Stock" __designer:wfdid="w1" Visible="False"></asp:Label>&nbsp;<asp:Label id="ss" runat="server" Text=":" __designer:wfdid="w268" Visible="False"></asp:Label> <asp:Label id="saldoakhir" runat="server" Text="0" __designer:wfdid="w268" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 131px"><asp:Label id="Label52" runat="server" Text="Harga" __designer:wfdid="w272" Visible="False"></asp:Label></TD><TD style="WIDTH: 10px"><asp:Label id="Label58" runat="server" Text=":" __designer:wfdid="w273" Visible="False"></asp:Label></TD><TD><asp:TextBox id="txtHarga" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w274" Enabled="False" Visible="False"></asp:TextBox></TD></TR><TR><TD colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w275" ValidChars="0987654321" FilterType="Numbers" TargetControlID="txtJumlah"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD colSpan=3><asp:ImageButton id="btnAddToListSPart" runat="server" ImageUrl="~/Images/addtolist.png" __designer:wfdid="w276"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w277"></asp:ImageButton> <asp:ImageButton id="btnBack" runat="server" ImageUrl="~/Images/Back.png" __designer:wfdid="w278"></asp:ImageButton></TD></TR><TR><TD colSpan=3><DIV style="WIDTH: 100%; HEIGHT: 100px"><asp:GridView id="GVDetSparepart" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w279" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="MPARTOID,IDCheck">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="MPARTOID" HeaderText="ID" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="IDCheck" HeaderText="IDCheck" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="MSTPARTOID" HeaderText="IDPart" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GENDESC" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVDESC" HeaderText="Job">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PARTDESCSHORT" HeaderText="Nama Sparepart">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="MPARTQTY" HeaderText="Jumlah">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" BorderColor="Blue" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="mtrloc" HeaderText="LocOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="MPARTPRICE" HeaderText="MPARTPRICE" Visible="False"></asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="saldoakhir" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label59" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data" __designer:wfdid="w5"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD colSpan=3><asp:Label id="lblLast" runat="server" Text="Last Update On" __designer:wfdid="w280"></asp:Label><asp:Label id="lblCreate" runat="server" Text="Created On" __designer:wfdid="w281"></asp:Label>&nbsp;<asp:Label id="updTime" runat="server" Font-Bold="True" __designer:wfdid="w282"></asp:Label>&nbsp;By <asp:Label id="updUser" runat="server" Font-Bold="True" __designer:wfdid="w283"></asp:Label> </TD></TR><TR><TD colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w284"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w285"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w286"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w287" Visible="False"></asp:ImageButton> </TD></TR></TBODY></TABLE></asp:View></asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                            <asp:UpdatePanel id="UpdatePanel4" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelInfo" runat="server" CssClass="modalBox" __designer:wfdid="w289" Visible="False" DefaultButton="btnFindInfo"><TABLE width=800><TBODY><TR><TD style="TEXT-ALIGN: center" colSpan=6><asp:Label id="lblInfo" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Penerimaan Barang" __designer:wfdid="w290"></asp:Label></TD></TR><TR><TD style="WIDTH: 95px"><asp:Label id="Label8" runat="server" Text="Filter" __designer:wfdid="w291"></asp:Label></TD><TD style="WIDTH: 2px"><asp:Label id="Label10" runat="server" Text=":" __designer:wfdid="w292"></asp:Label></TD><TD colSpan=4><asp:DropDownList id="ddlFilterInfo" runat="server" Width="117px" CssClass="inpText" __designer:wfdid="w293"><asp:ListItem Value="QLR.REQCODE">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="Barcode">Barcode</asp:ListItem>
<asp:ListItem Value="QLR.REQITEMNAME">Nama Barang</asp:ListItem>
<asp:ListItem Value="QLM.GENDESC">Merk</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterInfo" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w294"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindInfo" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w295"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllInfo" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w296"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 95px; HEIGHT: 15px"><asp:CheckBox id="cbPeriodInfo" runat="server" Text="Period" __designer:wfdid="w297"></asp:CheckBox></TD><TD style="WIDTH: 2px; HEIGHT: 15px"><asp:Label id="Label12" runat="server" Text=":" __designer:wfdid="w298"></asp:Label></TD><TD colSpan=4><asp:TextBox id="txtPeriod1Info" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w299"></asp:TextBox> <asp:ImageButton id="btnCal1Info" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w300"></asp:ImageButton> <asp:Label id="Label34" runat="server" Text="to" __designer:wfdid="w301"></asp:Label><asp:TextBox id="txtPeriod2Info" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w302"></asp:TextBox> <asp:ImageButton id="btnCal2Info" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" CssClass="inpText" __designer:wfdid="w303"></asp:ImageButton> <asp:Label id="Label72" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w304"></asp:Label></TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 801px; HEIGHT: 178px"><asp:GridView id="GVListInfo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w305" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="REQOID" EmptyDataText="Data Not Found">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="REQOID" HeaderText="ID" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="REQCODE" HeaderText="No. Tanda Terima">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Barcode" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Customer" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQITEMNAME" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="REQSTATUS" HeaderText="Status" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="date" HeaderText="Tanggal Terima">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Teknisi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label69" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD colSpan=6><ajaxToolkit:MaskedEditExtender id="MPECal1Info" runat="server" __designer:wfdid="w306" TargetControlID="txtPeriod1Info" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MPECal2Info" runat="server" __designer:wfdid="w307" TargetControlID="txtPeriod2Info" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w308" TargetControlID="txtPeriod1Info" Format="dd/MM/yyyy" PopupButtonID="btnCal1Info"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w309" TargetControlID="txtPeriod2Info" Format="dd/MM/yyyy" PopupButtonID="btnCal2Info"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=center colSpan=6><asp:LinkButton id="lbClose" runat="server" __designer:wfdid="w310">[Close]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenInfo" runat="server" __designer:wfdid="w311" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="MPEInfo" runat="server" __designer:wfdid="w312" TargetControlID="btnHiddenInfo" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelInfo" PopupDragHandleControlID="lblInfo"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <br />
                            &nbsp;<asp:UpdatePanel id="UpdatePanel5" runat="server"><contenttemplate>
<asp:Panel id="PanelJob" runat="server" CssClass="modalBox" __designer:wfdid="w314" Visible="False" DefaultButton="btnFindJob"><TABLE width=700><TBODY><TR><TD style="TEXT-ALIGN: center" colSpan=6><asp:Label id="lblJob" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Job" __designer:wfdid="w315"></asp:Label></TD></TR><TR><TD style="WIDTH: 51px"><asp:Label id="Label42" runat="server" Text="Filter" __designer:wfdid="w316" Visible="False"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label44" runat="server" Text=":" __designer:wfdid="w317" Visible="False"></asp:Label></TD><TD colSpan=4><asp:DropDownList id="ddlFilterJob" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w318" Visible="False"></asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterJob" runat="server" Width="187px" CssClass="inpText" __designer:wfdid="w319" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindJob" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w320" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllJob" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w321" Visible="False"></asp:ImageButton></TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 698px; HEIGHT: 139px"><asp:GridView id="GVJob" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w322" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="ITSERVOID">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="20%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="ITSERVOID" HeaderText="ID" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVDESC" HeaderText="Job">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="50%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ITSERVTARIF" HeaderText="Tarif">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="30%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label70" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=6><asp:LinkButton id="lbCloseJob" runat="server" __designer:wfdid="w323">[Close]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenJob" runat="server" __designer:wfdid="w324" Visible="False"></asp:Button><BR /><ajaxToolkit:ModalPopupExtender id="MPEJob" runat="server" __designer:wfdid="w325" TargetControlID="btnHiddenJob" PopupDragHandleControlID="lblJob" PopupControlID="PanelJob" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <br />
                            <asp:UpdatePanel id="UpdatePanel6" runat="server"><contenttemplate>
<asp:Panel id="PanelSpart" runat="server" CssClass="modalBox" __designer:wfdid="w327" Visible="False" DefaultButton="btnFindSpart"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=6><asp:Label id="lbSpart" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Sparepart" __designer:wfdid="w328"></asp:Label></TD></TR><TR><TD style="WIDTH: 61px"><asp:Label id="Label47" runat="server" Text="Filter" __designer:wfdid="w329" Visible="False"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label53" runat="server" Text=":" __designer:wfdid="w330" Visible="False"></asp:Label></TD><TD colSpan=4><asp:DropDownList id="ddlFilterSpart" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w331" Visible="False"></asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterSpart" runat="server" Width="166px" CssClass="inpText" __designer:wfdid="w332" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindSpart" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w333" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllSpart" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w334" Visible="False"></asp:ImageButton></TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 136px"><asp:GridView id="GVSpart" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w335" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="PARTOID,priceList,saldoakhir">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="20%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="PARTOID" HeaderText="ID" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="PARTDESCSHORT" HeaderText="Sparepart">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="80%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Stok">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label71" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Detail Data" __designer:wfdid="w31"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=6><asp:LinkButton id="lbCloseSpart" runat="server" __designer:wfdid="w336">[Close]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenSpart" runat="server" __designer:wfdid="w337" Visible="False"></asp:Button><BR /><ajaxToolkit:ModalPopupExtender id="MPESpart" runat="server" __designer:wfdid="w338" TargetControlID="btnHiddenSpart" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelSpart" PopupDragHandleControlID="lblSpart"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width=350><TBODY><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="WIDTH: 29px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 29px"></TD><TD><asp:Label id="lblState" runat="server"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 29px"></TD><TD align=center><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" PopupDragHandleControlID="lblCaption" PopupControlID="PanelErrMsg" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

