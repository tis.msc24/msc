<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmRealisasi_Pembelian.aspx.vb" Inherits="ReportForm_frmRealisasi_Pembelian" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
 <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" style="text-align: left; height: 23px;" valign="center">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Report Realisasi Pembelian :."></asp:Label></th>
        </tr>
<tr>
<th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD align=left>Report Type</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlType" runat="server" Width="130px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem Value="Detail">Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:DropDownList id="ddlOption1" runat="server" Width="96px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="pomst.trnbelipodate">PO Date</asp:ListItem>
<asp:ListItem Value="sjmst.trnsjbelidate">LPB Date</asp:ListItem>
<asp:ListItem Value="retmst.trnbelidate">Return Date</asp:ListItem>
</asp:DropDownList></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="podate1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibPOdate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label2" runat="server" Text="to"></asp:Label> <asp:TextBox id="podate2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibPODate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibPOdate1" TargetControlID="podate1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender4" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibPODate2" TargetControlID="podate2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="podate1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="podate2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left>Supplier</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="suppName" runat="server" Width="187px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="suppoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=right></TD><TD align=left></TD><TD align=left><asp:GridView id="gvSupplier" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="suppoid,suppcode,suppname" AllowPaging="True" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Supplier Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="250px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD8" align=left runat="server" Visible="false">Taxable</TD><TD id="TD7" align=left runat="server" Visible="false">:</TD><TD id="TD9" align=left runat="server" Visible="false"><asp:DropDownList id="ddlTax" runat="server" Width="130px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="Yes">Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Group</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="FilterDDLGrup" runat="server" Width="250px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=left>Sub Group</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="250px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=left>Barang</TD><TD align=left>:</TD><TD align=left colSpan=1><asp:TextBox id="material" runat="server" Width="187px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibSearchMat" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="ibClearMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="matoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=right></TD><TD align=left></TD><TD id="tdMaterial" align=left colSpan=1><asp:GridView id="gvItem" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3,merk" AllowPaging="True" GridLines="None" PageSize="8" UseAccessibleHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                        <asp:Label ID="Label8" runat="server"  ForeColor="Red" Text="No data in database!!"></asp:Label>
                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>PO No.</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="pono" runat="server" Width="187px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchPO" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelPO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="pomstoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=right></TD><TD align=left></TD><TD align=left><asp:GridView id="gvPO" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnbelimstoid,pono,podate" AllowPaging="True" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="PO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podate" HeaderText="PO Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>LPB No.</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="lpbNo" runat="server" Width="187px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchLPB" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelLPB" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="lpboid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=right></TD><TD align=left></TD><TD align=left><asp:GridView id="gvLPB" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnsjbelioid,trnsjbelino,trnsjbelidate" AllowPaging="True" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjbelino" HeaderText="LPB No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelidate" HeaderText="LPB Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD1" align=left runat="server" Visible="false">Purchase Return No</TD><TD id="TD2" align=left runat="server" Visible="false">:</TD><TD id="TD3" align=left runat="server" Visible="false"><asp:TextBox id="purchaseReturn" runat="server" Width="187px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchPurchaseReturn" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelPurchaseReturn" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="purchaseReturnOid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="Td4" align=right runat="server" Visible="false"></TD><TD id="Td5" align=left runat="server" Visible="false"></TD><TD id="Td6" align=left runat="server" Visible="false"><asp:GridView id="gvPurchaseReturn" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="trnretbelimstoid,trnretbelino,trnretbelidate" AllowPaging="True" GridLines="None" PageSize="8" RowHeaderColumn="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnretbelino" HeaderText="Retur No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnretbelidate" HeaderText="Retur Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TDmerk1" align=left runat="server" Visible="false">Merk</TD><TD id="TDmerk2" align=left runat="server" Visible="false">:</TD><TD id="TDmerk3" align=left runat="server" Visible="false"><asp:TextBox id="merk" runat="server" Width="168px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD align=left>PO Status</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="postatus" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ToPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbExport" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w1" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w2"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress>&nbsp;<asp:UpdatePanel id="upMsgbox" runat="server"><ContentTemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" DropShadow="True" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <CR:CrystalReportViewer id="crv_pembelian" runat="server" AutoDataBind="true"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</th>
</tr>
    </table>
    <br />
</asp:Content>