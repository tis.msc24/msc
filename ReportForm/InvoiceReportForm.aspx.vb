Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_InvoiceReportForm
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim folderReport As String = "~/report/"
    Dim Param As String = ""
#End Region

#Region "Procedure"
    Public Function GetSessionCheckNota() As Boolean
        If Session("CheckNota") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub BindData()
        Try
            sSql = "Select 0 selected,si.SOID,si.BRANCH_CODE,si.invoiceno,co.custoid,si.MSTREQOID,co.custname,rqm.reqcode,si.SSTATUS,CONVERT(VARCHAR(20),SSTARTTIME,103) SSTARTTIME,rqm.reqcustoid From QL_TRNINVOICE si Inner Join QL_mstcust co ON co.custoid=si.custoid Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=si.MSTREQOID AND rqm.reqcustoid=co.custoid Where si.CMPCODE='" & CompnyCode & "' AND " & DDLFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%' AND CONVERT(date, si.SSTARTTIME, 101) Between '" & CDate(toDate(txtPeriod1.Text)) & "' AND '" & CDate(toDate(txtPeriod2.Text)) & "'"
            If DDLcabang.SelectedValue <> "ALL" Then
                sSql &= " AND si.BRANCH_CODE='" & DDLcabang.SelectedValue & "'"
            End If

            sSql &= " Order BY si.SSTARTTIME Desc"

            Dim dt As DataTable = cKon.ambiltabel(sSql, "ChkInvoice")
            GVListInvoice.DataSource = dt : Session("ChkInvoice") = dt
            GVListInvoice.DataBind() : GVListInvoice.SelectedIndex = -1
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & "ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub CheckedGVCust()
        If Not Session("ChkInvoice") Is Nothing Then
            Dim dts As DataTable = Session("ChkInvoice")

            If dts.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dts.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To GVListInvoice.Rows.Count - 1
                    cb = GVListInvoice.Rows(i).FindControl("SelectOid")
                    dView.RowFilter = "SOID = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dts.AcceptChanges()
            End If
            Session("ChkInvoice") = dts
        End If
    End Sub

    Private Sub showPrint(ByVal tipe As String)
        Try
            Dim sWhere As String = "Where inv.CMPCODE='" & CompnyCode & "'"

            If DDLcabang.SelectedValue <> "ALL" Then
                sWhere &= " AND inv.BRANCH_CODE='" & DDLcabang.SelectedValue & "'"
            End If

            If txtPeriod1.Text <> "" And txtPeriod2.Text <> "" Then
                Try
                    Dim dDate1 As Date = CDate(toDate(txtPeriod1.Text))
                    Dim dDate2 As Date = CDate(toDate(txtPeriod2.Text))
                    If dDate1 < CDate("01/01/1900") Then
                        showMessage("Tanggal awal tidak valid", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    If dDate2 < CDate("01/01/1900") Then
                        showMessage("Tanggal akhir tidak valid", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    If dDate1 > dDate2 Then
                        showMessage("Period 1 harus lebih kecil dari period 2", CompnyName & " - WARNING", 2) : Exit Sub
                    End If
                Catch ex As Exception
                    showMessage("Format tangal tidak sesuai", CompnyName & " - WARNING", 2)
                    Exit Sub
                End Try

                sWhere &= " AND CONVERT(date, inv.SSTARTTIME, 101) Between '" & CDate(toDate(txtPeriod1.Text)) & "' AND '" & CDate(toDate(txtPeriod2.Text)) & "'"
            End If

            Dim dtc As DataTable = Session("ChkInvoice")
            Dim OidCust As String = ""
            If GVListInvoice.Visible = True Then
                If Not (Session("ChkInvoice") Is Nothing) Then
                    If dtc.Rows.Count > 0 Then
                        Dim dvs As DataView = dtc.DefaultView
                        dvs.RowFilter = "selected='1'"
                        If dvs.Count < 1 Then
                            showMessage("- Maaf, Filter customer belum anda pilih, Silahkan Klik icon luv kemudian pilih customer..<BR>", CompnyName, 2)
                            Exit Sub
                        End If
                        dvs.RowFilter = ""
                    Else
                        showMessage("- Maaf, Filter customer belum anda pilih, Silahkan Klik icon luv kemudian pilih customer..<BR>", CompnyName, 2)
                        Exit Sub
                    End If
                End If
            End If

            If Not Session("ChkInvoice") Is Nothing Then
                If GVListInvoice.Visible = True Then
                    Dim cDt As DataView = dtc.DefaultView
                    cDt.RowFilter = "selected='1'"

                    For R1 As Integer = 0 To cDt.Count - 1
                        OidCust &= cDt(R1)("SOID").ToString & ","
                    Next

                    If OidCust <> "" Then
                        sWhere &= " AND inv.SOID IN (" & Left(OidCust, OidCust.Length - 1) & ")"
                    End If
                End If
            End If

            If DDLGaransi.SelectedValue <> "ALL" Then
                sWhere &= " AND rqd.typegaransi='" & DDLGaransi.SelectedValue & "'"
            End If

            If DDLStatus.SelectedValue <> "ALL" Then
                sWhere &= " AND inv.SSTATUS='" & DDLStatus.SelectedValue & "'"
            End If

            If tipe = "PRINT" Then
                report.Load(Server.MapPath(folderReport & "RptInvService.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "RptInvServiceExl.rpt"))
            End If

            Dim crConninfo As New ConnectionInfo
            With crConninfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With

            SetDBLogonForReport(crConninfo, report)
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sCabang", DDLcabang.SelectedItem.Text)
            report.SetParameterValue("dPeriode", txtPeriod1.Text & " - " & txtPeriod2.Text)
            report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            If tipe = "PRINT" Then
                crvinvo.DisplayGroupTree = False
                crvinvo.ReportSource = report
            ElseIf tipe = "EXCEL" Then
                report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "_" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose()
            Else
                report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "_" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close() : report.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & "ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub Cabangddl()
        sSql = "Select gencode,gendesc From QL_mstgen Where gengroup = 'CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLcabang, sSql)
            Else
                FillDDL(DDLcabang, sSql)
                DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
                DDLcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(DDLcabang, sSql)
            DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
            DDLcabang.SelectedValue = "ALL"
        End If
    End Sub
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("page") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("InvoiceReportForm.aspx")
        End If

        Session("idPage") = Request.QueryString("idPage")
        Page.Title = CompnyName & " - Laporan Invoice Service"

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            Cabangddl()
            txtPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnETPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnETPdf.Click
        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        CheckedGVCust() : showPrint("PDF")
    End Sub

    Protected Sub btnETExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnETExcel.Click
        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2) : Exit Sub
        End If
        CheckedGVCust() : showPrint("EXCEL")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        DDLFilter.SelectedIndex = 0
        txtFilter.Text = "" : DDLStatus.SelectedIndex = 0
        txtPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        crvinvo.ReportSource = Nothing
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub 

    Protected Sub crvinvo_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvinvo.Navigate
        showPrint("PRINT")
    End Sub

    Protected Sub Page_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSearch.Click
        CheckedGVCust() : BindData()
    End Sub

    Protected Sub BtnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnErase.Click
        txtFilter.Text = "" : GVListInvoice.Visible = False
    End Sub

    Protected Sub CbHdrOid_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("ChkInvoice") Is Nothing Then
            Dim dts As DataTable = Session("ChkInvoice")
            If dts.Rows.Count > 0 Then
                For i As Integer = 0 To dts.Rows.Count - 1
                    dts.Rows(i)("selected") = sender.Checked
                Next
                dts.AcceptChanges()
            End If

            GVListInvoice.DataSource = dts : GVListInvoice.DataBind()

            Session("ChkInvoice") = dts 
        End If
    End Sub
#End Region

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnview.Click
        If txtPeriod1.Text <> "" And txtPeriod2.Text = "" Then
            showMessage("Silahkan isi period 2 !", CompnyName & "- WARNING", 2)
            Exit Sub
        End If

        If txtPeriod2.Text <> "" And txtPeriod1.Text = "" Then
            showMessage("Silahkan isi period 1 !", CompnyName & "- WARNING", 2)
            Exit Sub
        End If
        CheckedGVCust() : showPrint("PRINT")
    End Sub
End Class
