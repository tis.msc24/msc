<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptDimensiItem.aspx.vb" Inherits="ReportForm_rptSalesItem"%>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Text=".: Laporan Dimensi Barang" Font-Size="Large"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=left runat="server" visible="true">Katalog Barang</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="ItemName" runat="server" Width="255px" CssClass="inpText" Enabled="False" Rows="2" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="oid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="Td14" align=left runat="server" visible="true"><asp:Label id="Label1" runat="server" Text="Status"></asp:Label></TD><TD id="Td15" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText"><asp:ListItem>Aktif</asp:ListItem>
<asp:ListItem Value="InAktif">In Aktif</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td17" align=left colSpan=4 runat="server" visible="true"><asp:ImageButton id="btnView" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnXls" onclick="btnXls_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnclear" onclick="btnclear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="Td16" align=center colSpan=4 runat="server" visible="true"><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD align=center colSpan=4 runat="server" visible="true"><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w2"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvReportForm" runat="server" Width="350px" Height="50px" DisplayGroupTree="False" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnXLS"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPDF"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
<asp:UpdatePanel ID="upListMat" runat="server">
<ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Katalog</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText">
<asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Kode Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" UseAccessibleHeader="False" GridLines="None" DataKeyNames="itemcode,itemdesc,itemoid,satuan3" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" __designer:wfdid="w1">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" __designer:wfdid="w2" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="SaddleBrown" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="upPopUpMsg" runat="server">
<ContentTemplate>
<asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
<table>
<tbody>
<tr>
<td colspan="2" style="background-color: #cc0000; text-align: left">
<asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
</tr>
<tr>
<td colspan="2" style="height: 10px">
</td>
</tr>
<tr>
<td>
<asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
Width="24px" /></td>
<td class="Label" style="text-align: left">
<asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
</tr>
<tr>
<td colspan="2" style="height: 10px; text-align: center">
</td>
</tr>
<tr>
<td colspan="2" style="text-align: center">
&nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
</tr>
</tbody>
</table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
TargetControlID="bePopUpMsg">
</ajaxToolkit:ModalPopupExtender>
<asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
</ContentTemplate>
</asp:UpdatePanel>
</th>
        </tr>
<tr>
<th align="center" style="background-color: #ffffff" valign="center">
</th>
</tr>
    </table>
</asp:Content>

