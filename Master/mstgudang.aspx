<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstgudang.aspx.vb" Inherits="mstgudang" Title="PT. MULTI SARANA COMPUTER - Master Gudang" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: MASTER GUDANG"></asp:Label><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                        SelectCommand="SELECT [genoid], [gencode], [gendesc], [gengroup], [genother1], [genother2] FROM [QL_mstgen] WHERE ([gengroup] = @gengroup)" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="PROVINCE" Name="gengroup" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div style="width: 100%; text-align: left">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" class="Label">Filter :</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="l.gendesc">Description</asp:ListItem>
                                                    <asp:ListItem Value="l.genCode">Code</asp:ListItem>
                                                    <asp:ListItem Value="c.gendesc">Cabang</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText"></asp:TextBox>
                                                <asp:ImageButton
                                                    ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" />
                                                <asp:ImageButton
                                                    ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="height: 10px" colspan="5">
                                                &nbsp;<fieldset id="field1" style="height: 290px; width: 98%; text-align: left; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                                    <div id='divTblData'>
                                        <asp:GridView ID="GVmstgen" runat="server" EmptyDataText="No data in database." AutoGenerateColumns="False" Width="100%" CellPadding="4" AllowPaging="True" EnableModelValidation="True" ForeColor="#333333" GridLines="None" PageSize="8">
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="genoid" DataNavigateUrlFormatString="mstgudang.aspx?oid={0}"
                                                    DataTextField="genCode" HeaderText="Code">
                                                    <ItemStyle Width="200px" Font-Size="X-Small" />
                                                    <HeaderStyle Width="200px" Font-Size="X-Small" CssClass="gvhdr" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="gendesc" HeaderText="Description" SortExpression="gendesc">
                                                    <ItemStyle Width="550px" Font-Size="X-Small" />
                                                    <HeaderStyle Width="550px" Font-Size="X-Small" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cabang" HeaderText="Cabang" SortExpression="Cabang">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" CssClass="gvhdr" Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" Font-Bold="True" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <EmptyDataTemplate>



                                                <asp:Label ID="lblmsg" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        </div>
                                </fieldset>
                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                &nbsp;
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: List Of Gudang</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" Visible="False" __designer:wfdid="w156"></asp:Label> </TD><TD class="Label" align=left><asp:Label id="GenOid" runat="server" Font-Size="8pt" Visible="False" __designer:wfdid="w157"></asp:Label><asp:Label id="genother4" runat="server" Font-Size="8pt" Visible="False" __designer:wfdid="w158"></asp:Label></TD></TR><TR><TD style="WIDTH: 133px" align=left>Code <asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w159"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="gencode" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w160" MaxLength="30" size="20"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD vAlign=top align=left>Description <asp:Label id="Label3" runat="server" CssClass="Important" Font-Size="8pt" Text="*" __designer:wfdid="w161"></asp:Label></TD><TD vAlign=top align=left><asp:TextBox id="gendesc" runat="server" Width="200px" Height="50px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w162" MaxLength="70" size="20" TextMode="MultiLine" AutoPostBack="True"></asp:TextBox> <asp:Label id="Label4" runat="server" CssClass="Important" Font-Size="8pt" Text="* 70 characters" __designer:wfdid="w163"></asp:Label> </TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="CbAssetType" runat="server" Font-Size="8pt" Text="Cabang" __designer:wfdid="w164"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="sCabang" runat="server" Width="205px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w165" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="Label7" runat="server" Font-Size="8pt" Text="Warehouse" __designer:wfdid="w166"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLWarehouse" runat="server" Width="205px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w167" AutoPostBack="True"></asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><SPAN style="FONT-SIZE: 7pt"><asp:Label id="Label5" runat="server" Font-Size="8pt" Text="Type Gudang" __designer:wfdid="w168"></asp:Label></SPAN></TD><TD class="Label" align=left><asp:DropDownList id="DdlType" runat="server" Width="205px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w169"><asp:ListItem Value="UMUM">GUDANG UMUM</asp:ListItem>
<asp:ListItem Value="TITIPAN">GUDANG TITIPAN</asp:ListItem>
<asp:ListItem Value="RUSAK">GUDANG BARANG RUSAK</asp:ListItem>
<asp:ListItem Value="KONSINYASI">GUDANG KONSINYASI</asp:ListItem>
<asp:ListItem Value="ROMBENG">GUDANG ROMBENG</asp:ListItem>
<asp:ListItem Value="ECOMMERCE">GUDANG E-COMMERCE</asp:ListItem>
<asp:ListItem Value="KANVAS">GUDANG KANVAS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left></TD><TD class="Label" align=left><asp:LinkButton id="BtnGetUser" runat="server" CssClass="green" Font-Bold="True" __designer:wfdid="w3">Get User</asp:LinkButton></TD></TR><TR><TD class="Label" align=left colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 700px; HEIGHT: 155px; BACKGROUND-COLOR: beige" id="sCroll"><asp:GridView id="GvUserNya" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w1" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="nomer" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="USERID" HeaderText="User ID">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="USERNAME" HeaderText="User Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Font-Size="" ForeColor="Red" Text="No Detail Data!!"></asp:Label>
                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR><TD align=left colSpan=2>Last Update On <asp:Label id="Updtime" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w170"></asp:Label> By <asp:Label id="Upduser" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w171"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w172"></asp:ImageButton> <asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w173"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w174"></asp:ImageButton> <asp:Label id="genother3" runat="server" Font-Size="8pt" Visible="False" __designer:wfdid="w177"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w175" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt">LOADING....</SPAN><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w176"></asp:Image></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: Form Gudang</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" align="left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></td>
                            <td align="left">
                                <asp:Label ID="lblPopUpMsg" runat="server" CssClass="Important"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:ImageButton ID="imbPopUpOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" DropShadow="True" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar User" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD vAlign=top align=center colSpan=3>Filter : &nbsp;&nbsp;<asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" AutoPostBack="True"><asp:ListItem Value="USERID">User ID</asp:ListItem>
<asp:ListItem Value="USERNAME">User Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText" Font-Size="8pt"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnViewALL" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="BtnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" align=center>Load Data...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 894px; HEIGHT: 13%; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("personnoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="USERID" HeaderText="User ID">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="USERNAME" HeaderText="User Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Jabatan" HeaderText="Jabatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="BtnAddToList" runat="server" CssClass="green" Font-Bold="True" Font-Underline="False"> Add To List </asp:LinkButton> - <asp:LinkButton id="CloseList" runat="server" CssClass="red" Font-Bold="True" Font-Underline="False"> Cancel & Close </asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" Drag="True" PopupDragHandleControlID="lblTitleListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat" TargetControlID="btnHideListMat">
                                                </ajaxToolkit:ModalPopupExtender> <asp:HiddenField id="hfStatus" runat="server"></asp:HiddenField> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>
