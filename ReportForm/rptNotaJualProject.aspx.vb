Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptNotaJual
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

#End Region

#Region "Procedures"
    Public Sub initddl()

        sSql = "select distinct periodacctg,periodacctg from ql_pomst order by periodacctg desc"
        FillDDL(period, sSql)

        sSql = "select genoid, gendesc from ql_mstgen where gengroup='WAREHOUSE'  ORDER BY gendesc"
        FillDDL(ddlLocation, sSql)

        ddlLocation.Items.Add(New ListItem("ALL LOCATION", "ALL LOCATION"))
        ddlLocation.SelectedValue = "ALL LOCATION"
        '---------------------------------------------------------------------------------------
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("ALL GRUP", "ALL GRUP"))
        FilterDDLGrup.SelectedValue = "ALL GRUP"
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GRUP", "ALL SUB GRUP"))
        FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP"
    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        lblkonfirmasi.Text = ""
        Session("diprint") = "False"

        Try

            Dim swhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and mtrlocoid=" & ddlLocation.SelectedValue & " ")
            Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, "  and  i.itemoid=" & itemoid.Text, "  ")
            Dim swheregrup As String = IIf(FilterDDLGrup.SelectedValue = "ALL GRUP", "  ", "  and  i.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
            Dim swheresubgrup As String = IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP", "  ", "  and  i.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")
            Dim swhere As String = ""
            Dim namaPDF As String = ""

            vReport = New ReportDocument
            If type.SelectedValue = "rptnotajualSum.rpt" Then
                vReport.Load(Server.MapPath("~\Report\rptnotajualSum.rpt"))
                swhere = " where mst.trnjualtype='PROJECK' and mst.trnjualnote like '%" & Tchar(note.Text) & "%' and mst.trnjualdate between '" & CDate(toDate(dateAwal.Text)) & "' and '" & CDate(toDate(dateAkhir.Text)) & "'" & IIf(oid.Text.Trim = "", " ", " and mst.trnjualmstoid = '" & oid.Text & "' ") & IIf(ToDouble(custoid.Text) > 0, " and mst.trncustoid =" & custoid.Text, "  ") & IIf(status.SelectedValue = "All", " ", " and mst.trnjualstatus = '" & status.SelectedValue & "' ")
                namaPDF = "SI_(Sum)_"
            Else
                vReport.Load(Server.MapPath("~\Report\rptnotajualDetail.rpt"))
                swhere = " where mst1.trnjualtype='PROJECK' and mst1.trnjualnote like '%" & Tchar(note.Text) & "%' and mst1.trnjualdate between '" & CDate(toDate(dateAwal.Text)) & "' and '" & CDate(toDate(dateAkhir.Text)) & "'" & IIf(oid.Text.Trim = "", " ", " and mst1.trnjualmstoid = '" & oid.Text & "' ") & IIf(ToDouble(custoid.Text) > 0, "  and mst1.trncustoid=" & custoid.Text, "  ") & IIf(ToDouble(itemoid.Text) > 0, "  and  i.itemoid=" & itemoid.Text, "  ") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and jual.itemloc=" & ddlLocation.SelectedValue & " ") & IIf(status.SelectedValue = "All", " ", " and mst1.trnjualstatus = '" & status.SelectedValue & "' ")
                namaPDF = "SI_(Dtl)_"
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            setDBLogonForReport(crConnInfo, vReport)

            'crvMutasiStock.DisplayGroupTree = False
            'crvMutasiStock.ReportSource = vReport
            vReport.SetParameterValue("sWhere", swhere)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            vReport.SetParameterValue("titledef", "Project ")
            Session("diprint") = "True"
 

            If tipe = "view" Then
                CrystalReportViewer1.DisplayGroupTree = False
                'CRVPenerimaanBarang.SeparatePages = False
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "excel" Then
                'vReport.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

                Response.Buffer = False

                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptNotaJualProject.aspx?awal=true")
            ElseIf tipe = "pdf" Then
                'vReport.PrintOptions.PaperSize = PaperSize.PaperLegal
                'vReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape

                Response.Buffer = False

                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))

                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptNotaJualProject.aspx?awal=true")

            End If
        Catch ex As Exception

            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Public Sub BindDataListItem()
        sSql = "select ql_mstitem.itemcode, ql_mstitem.itemdesc, " & _
        "ql_mstitem.itempriceunit1,ql_mstitem.itempriceunit2," & _
        "ql_mstitem.itempriceunit3,ql_mstitem.itemoid, g.gendesc satuan1," & _
        "g2.gendesc satuan2, g3.gendesc satuan3,konversi1_2, " & _
        "konversi2_3 from ql_mstitem inner join ql_mstgen g on g.genoid=satuan1 " & _
        "and itemflag='AKTIF'    inner join ql_mstgen g2 on g2.genoid=satuan2  " & _
        "inner join ql_mstgen g3 on g3.genoid=satuan3  " & _
        "where itemdesc like '%" & Tchar(itemname.Text) & "%' or ql_mstitem.itemcode like '%" & Tchar(itemname.Text) & "%'"
        FillGV(gvItem, sSql, "ql_mstitem")
        gvItem.Visible = True
    End Sub

    Public Sub bindDataListCustomer()
        sSql = "select custoid, custcode, custname, custaddr  from ql_mstcust where custgroup='PROJECK' and (custname like '%" & Tchar(custname.Text) & "%' or custcode like '%" & Tchar(custname.Text) & "%')"
        FillGV(gvCust, sSql, "ql_mstcust")
        gvCust.Visible = True
    End Sub

    Public Sub bindDataListSO()
        sSql = "select trnjualmstoid, trnjualno, trncustname, trnjualstatus, trncustoid , convert(char(10),trnjualdate,103)trnjualdate   from ql_trnjualmst where trnjualtype='PROJECK' and trnjualno like '%" & Tchar(nota.Text) & "%' and trnjualdate between '" & CDate(toDate(dateAwal.Text)) & "' and '" & CDate(toDate(dateAkhir.Text)) & "' order by trnjualdate desc"
        FillGV(GVNota, sSql, "ql_trnjualmst")
        GVNota.Visible = True
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") : Dim userName As String = Session("UserName")  ' simpan session k variabel spy tidak hilang
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear() : Session("UserName") = userName   '  clear all session
            Session("Role") = xsetRole
            'Session.Clear() : Session("UserName") = userName   '  clear all session
            Session("UserID") = userId 'insert lagi sesion yg disimpan dan create session lagi
            Session("Access") = access
            Response.Redirect("~\ReportForm\rptNotaJualProject.aspx") ' di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Laporan Nota Penjualan Project"
        'If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If

        If type.SelectedValue = "rptnotajualSum.rpt" Then
            itemname.Enabled = False
            itemname.CssClass = "inpTextDisabled"
            btnSearchItem.Enabled = False
            btnEraseItem.Enabled = False
            ddlLocation.Enabled = False
            ddlLocation.CssClass = "inpTextDisabled"
        Else
            itemname.Enabled = True
            itemname.CssClass = "inpText"
            btnSearchItem.Enabled = True
            btnEraseItem.Enabled = True
            ddlLocation.Enabled = True
            ddlLocation.CssClass = "inpText"

        End If
        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrint("view")
            End If
        Else
            initddl()
            dateAwal.Text = Format(Now, "01/MM/yyyy")
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                lblkonfirmasi.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = "Please check Period value"
            Exit Sub
        End Try
        showPrint("view")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptNotaJualProject.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                lblkonfirmasi.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = "Please check Period value"
            Exit Sub
        End Try
        showPrint("excel")
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
    End Sub

    Protected Sub btnSearchItem_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        lblkonfirmasi.Text = ""
        'If itemname.Text.Trim = "" Then
        '    lblkonfirmasi.Text = "Please Fill Filter Item, minimal 1 character !!"
        '    Exit Sub
        'End If
        'gvItem.Visible = True
        BindDataListItem()
    End Sub

    Protected Sub btnSearchCustomer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblkonfirmasi.Text = ""
        'If custname.Text.Trim = "" Then
        '    lblkonfirmasi.Text = "Please Fill Filter Custname, minimal 1 character !!"
        '    Exit Sub
        'End If
        'gvCust.Visible = True
        bindDataListCustomer()
    End Sub

    Protected Sub btnSearchNota_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                lblkonfirmasi.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = "Please check Period value"
            Exit Sub
        End Try
        bindDataListSO()
    End Sub

    Protected Sub btnEraseCustomer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custname.Text = ""
        custoid.Text = ""
    End Sub

    Protected Sub gvCust_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        custname.Text = gvCust.SelectedDataKey.Item("custname")
        custoid.Text = gvCust.SelectedDataKey.Item("custoid")
        gvCust.Visible = False
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
    End Sub

    Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCust.PageIndexChanging
        gvCust.PageIndex = e.NewPageIndex
        bindDataListCustomer()
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        GVNota.PageIndex = e.NewPageIndex
        bindDataListSO()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nota.Text = ""
        oid.Text = ""
    End Sub

    Protected Sub GVNota_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        nota.Text = GVNota.SelectedDataKey.Item("trnjualno")
        oid.Text = GVNota.SelectedDataKey.Item("trnjualmstoid")
        custoid.Text = GVNota.SelectedDataKey.Item("trncustoid")
        custname.Text = GVNota.SelectedDataKey.Item("trncustname")
        GVNota.Visible = False
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
    End Sub

    Protected Sub BTNpRINT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                lblkonfirmasi.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = "Please check Period value"
            Exit Sub
        End Try
        showPrint("pdf")
    End Sub

#End Region
End Class
