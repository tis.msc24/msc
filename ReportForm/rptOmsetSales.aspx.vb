Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptOmsetSales
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub showReport(ByVal tipe As String)

        lblkonfirmasi.Text = "" : Session("diprint") = "False"
        Dim dDate1 As String = Format(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy")
        Dim ddate2 As String = Format(CDate(toDate(txtPeriod2.Text)), "MM/dd/yyyy")
        Dim sWhere As String = "Where cmpcode ='MSC' "
        If FilterPeriod1.Text.Trim <> "" And txtPeriod2.Text.Trim <> "" Then
            Try
                'dDate1 = Format(FilterPeriod1.Text, "MM/dd/yyyy")
                'ddate2 = toDate(txtPeriod2.Text)
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Please fill period 1 value!"
                    Exit Sub
                End If
                If ddate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Please fill period 2 value!"
                    Exit Sub
                End If
                If dDate1 > ddate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
                    Exit Sub
                End If
                sWhere &= "AND trnjualdate between '" & dDate1 & "' and '" & ddate2 & "'"
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!"
                Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!"
            Exit Sub
        End If

        Try
            Dim namaPDF As String = ""
            If dd_branch.SelectedValue <> "ALL" Then
                sWhere &= "AND branch_code='" & dd_branch.SelectedValue & "'"
            End If

            If PersonOid.Text <> "" Then
                sWhere &= " AND personoid=" & PersonOid.Text.Trim & ""
            End If

            If itemoid.Text <> "" Then
                sWhere &= " AND itemoid=" & itemoid.Text.Trim & ""
            End If

            vReport = New ReportDocument
            vReport.Load(Server.MapPath("~\Report\rptOmsetSales.rpt"))
            namaPDF = "Lap_Omset_Sales"
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("startperiode", FilterPeriod1.Text)
            vReport.SetParameterValue("endperiode", txtPeriod2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            If tipe = "View" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "PDF" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "EXCEL" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Private Sub BindPerson()
        Dim sCabang As String = ""
        If dd_branch.SelectedValue <> "ALL" Then
            sCabang = "AND branch_code='" & dd_branch.SelectedValue & "'"
        End If
        sSql = "select PERSONOID, PERSONNIP, PERSONNAME, PERSONCRTADDRESS from QL_MSTPERSON where PERSONSTATUS = 987 and (personname LIKE '%" & Tchar(TxtPinjamNo.Text.Trim) & "%' OR personnip LIKE '%" & Tchar(TxtPinjamNo.Text.Trim) & "%') " & sCabang & ""
        FillGV(PersonGv, sSql, "ql_mstperson")
        PersonGv.Visible = True
    End Sub

    Private Sub BindItem()
        sSql = "Select DISTINCT itemoid, itemcode, itemdesc,merk From ql_mstitem m Where (itemdesc LIKE '%" & Tchar(itemname.Text) & "%' OR itemcode LIKE '%" & Tchar(itemname.Text) & "%') And m.itemoid IN (select jd.itemoid from QL_trnjualdtl jd inner join QL_trnjualmst jm on jd.trnjualmstoid = jm.trnjualmstoid and jd.branch_code = jm.branch_code and jm.spgoid LIKE '%" & Tchar(PersonOid.Text.Trim) & "%') "
        FillGV(gvItem, sSql, "ql_mstitem")
        gvItem.Visible = True
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Session.Clear()  ' -->>  clear all session 
            Session("SpecialAccess") = xsetAcc
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Response.Redirect("~\ReportForm\rptOmsetSales.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Omset Sales"

        If IsPostBack Then
            If Session("diprint") = "True" Then
                showReport("View")
            End If
        Else
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            sSql = "Select gencode,gendesc From QL_mstgen Where gengroup = 'CABANG'"
            FillDDL(dd_branch, sSql)
            dd_branch.Items.Add(New ListItem("ALL", "ALL"))
            dd_branch.SelectedValue = "ALL"
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptOmsetSales.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        Session("diprint") = "False"
        showReport("EXCEL")
    End Sub

    Protected Sub imbSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindItem()
    End Sub

    Protected Sub imbEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        itemname.Text = "" : itemoid.Text = ""
        gvItem.Visible = False : gvItem.SelectedIndex = -1
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemname.Text = gvItem.SelectedDataKey.Item("itemdesc")
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvItem.PageIndex = e.NewPageIndex
        BindItem()
    End Sub

    Protected Sub ibpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showReport("PDF")
    End Sub

    Protected Sub btnreport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnreport.Click
        Session("diprint") = "True"
        showReport("View")
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub

    Protected Sub sPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindPerson()
    End Sub

    Protected Sub PersonGv_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        PersonGv.PageIndex = e.NewPageIndex
        BindPerson()
    End Sub

    Protected Sub PersonGv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PersonOid.Text = PersonGv.SelectedDataKey.Item("personoid")
        TxtPinjamNo.Text = PersonGv.SelectedDataKey.Item("personname")
        PersonGv.Visible = False
    End Sub

    Protected Sub eRasePerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PersonOid.Text = "" : TxtPinjamNo.Text = ""
        PersonGv.Visible = False
    End Sub

#End Region
End Class
