<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptNotaJual.aspx.vb" Inherits="rptNotaJual" title="Toko Ali - Laporan Nota Penjualan" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="top">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Sales Invoice/Nota Jual"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD id="tdPeriod1" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode "></asp:Label></TD><TD id="Td1" align=right runat="server" Visible="true">:</TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/MM/yyyy)</asp:Label></TD></TR><TR><TD id="Td2" align=left runat="server" Visible="true"><asp:Label id="Label2" runat="server" Text="Type"></asp:Label></TD><TD id="Td3" align=right runat="server" Visible="true">:</TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="type" runat="server" Width="152px" CssClass="inpText" OnSelectedIndexChanged="type_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Value="SUMMARY">Summary</asp:ListItem>
<asp:ListItem Value="DETAIL">Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td5" align=left runat="server" Visible="true"><asp:Label id="Label15" runat="server" Width="57px" Text="Type SI"></asp:Label></TD><TD id="Td6" align=right runat="server" Visible="true">:</TD><TD id="Td7" align=left colSpan=3 runat="server" Visible="true"><asp:CheckBox id="cbCheckAll" runat="server" BackColor="#FFFFC0" Text="CHECK ALL" AutoPostBack="True" BorderColor="Black" __designer:wfdid="w3"></asp:CheckBox><asp:CheckBoxList id="cbType" runat="server" ForeColor="White" BackColor="#FFFFC0" OnSelectedIndexChanged="cbType_SelectedIndexChanged" AutoPostBack="True" BorderColor="Black" __designer:wfdid="w4" RepeatDirection="Horizontal" RepeatColumns="4" CellPadding="5"></asp:CheckBoxList><asp:DropDownList id="DDLPenjType" runat="server" CssClass="inpText" Visible="False" OnSelectedIndexChanged="type_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD id="Td8" align=left runat="server" Visible="true"><asp:Label id="Label12" runat="server" Text="Cabang"></asp:Label></TD><TD id="Td9" align=right runat="server" Visible="true"><asp:Label id="Label13" runat="server" Text=":"></asp:Label></TD><TD id="Td10" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="DDLcabang" runat="server" Width="152px" CssClass="inpText" OnSelectedIndexChanged="DDLcabang_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td11" align=left runat="server" Visible="true"><asp:Label id="lblprd1" runat="server" Text="Product" Visible="False"></asp:Label></TD><TD id="Td12" align=right runat="server" Visible="true"><asp:Label id="lblprd2" runat="server" Text=":" Visible="False"></asp:Label></TD><TD id="Td13" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="ddlproduct" runat="server" Width="152px" CssClass="inpText" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>UNGGULAN</asp:ListItem>
<asp:ListItem>NON UNGGULAN</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD vAlign=top align=left runat="server" Visible="true"><asp:Label id="Label16" runat="server" Width="76px" Text="No. Nota SI" __designer:wfdid="w4"></asp:Label></TD><TD vAlign=top align=right runat="server" Visible="true">:</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="SoNo" runat="server" Width="240px" Height="35px" CssClass="inpTextDisabled" __designer:wfdid="w1" TextMode="MultiLine" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w2"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w3"></asp:ImageButton></TD></TR><TR><TD vAlign=middle align=left runat="server" Visible="true"><asp:Label id="Label5" runat="server" Text="Customer" __designer:wfdid="w1"></asp:Label></TD><TD vAlign=top align=right runat="server" Visible="true">:</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="custcode" runat="server" Width="240px" Height="35px" CssClass="inpTextDisabled" __designer:wfdid="w33" TextMode="MultiLine" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w34"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w35"></asp:ImageButton></TD></TR><TR><TD vAlign=middle align=left runat="server"><asp:Label id="Label14" runat="server" Text="Sales"></asp:Label></TD><TD vAlign=top align=right runat="server">:</TD><TD vAlign=top align=left colSpan=3 runat="server"><asp:TextBox id="regno" runat="server" Width="240px" Height="35px" CssClass="inpTextDisabled" __designer:wfdid="w19" TextMode="MultiLine" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindReg" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w20"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseReg" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w21"></asp:ImageButton></TD></TR><TR><TD id="Td27" align=left runat="server" Visible="false"><asp:Label id="Label10" runat="server" Text="Status "></asp:Label></TD><TD id="Td28" align=right runat="server" Visible="false">:</TD><TD id="Td29" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="status" runat="server" Width="152px" CssClass="inpText" Visible="False"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Posting</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Width="50px" Text="Katalog"></asp:Label></TD><TD align=right><asp:Label id="Label11" runat="server" Width="1px" Text=":"></asp:Label></TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="ItemName" runat="server" Width="300px" CssClass="inpTextDisabled" TextMode="MultiLine" Enabled="False" Rows="3"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click1" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;</TD></TR><TR><TD align=left><asp:Label id="LbLpIc" runat="server" Width="3px" Text="PIC" Visible="False"></asp:Label></TD><TD align=right><asp:Label id="pIcLbL" runat="server" Width="2px" Text=":" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="PicTxt" runat="server" Width="150px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="pIcBtn" onclick="pIcBtn_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="17px" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="PicDel" onclick="PicDel_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="OidPic" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD><TD align=right></TD><TD align=left colSpan=3><asp:GridView id="gvPic" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" Visible="False" OnSelectedIndexChanged="gvPic_SelectedIndexChanged" CellPadding="4" OnPageIndexChanging="gvPic_PageIndexChanging" UseAccessibleHeader="False" PageSize="5" DataKeyNames="PERSONOID,PERSONNIP,PERSONNAME" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#C00000" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="PERSONNIP" HeaderText="NIP">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="PERSONNAME" HeaderText="PIC Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibpdf" onclick="BTNpRINT_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:Label id="labelEx" runat="server" Width="20px"></asp:Label></TD></TR><TR><TD align=center colSpan=5><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w1" PopupButtonID="imageButton1" TargetControlID="dateAwal" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w2" PopupButtonID="imageButton2" TargetControlID="dateAkhir" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w3" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w4" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender>&nbsp; </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" Width="350px" Height="50px" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibpdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upListSO" runat="server"><contenttemplate>
<asp:Panel id="PanelListSO" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Large" Font-Bold="True" Text="List Sales Invoice"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="trnrequestitemno">No. Nota SI</asp:ListItem>
<asp:ListItem Value="trnrequestitemnote">Keterangan</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" ForeColor="#333333" CellPadding="4" PageSize="5" DataKeyNames="trnrequestitemoid,trnrequestitemno,trnrequestitemdate,trnrequestitemstatus,trnrequestitemnote" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                <asp:CheckBox ID="cbLMSO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("trnrequestitemoid") %>' />
                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnrequestitemoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemno" HeaderText="No. Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                        &nbsp;
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupControlID="PanelListSO" PopupDragHandleControlID="lblListSO" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListCust" runat="server"><contenttemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="DDLFilterListCust" runat="server" Width="100px" CssClass="inpText">
                                                            <asp:ListItem Value="custname">Customer</asp:ListItem>
                                                            <asp:ListItem Value="Custcode">Kode</asp:ListItem>
                                                        </asp:DropDownList> <asp:TextBox id="txtFilterListCust" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="98%" ForeColor="#333333" CellPadding="4" PageSize="8" DataKeyNames="custoid,custcode,custname" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="cbLMCust" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="custcode" HeaderText="Kode">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Center"
                                                                    VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="custname" HeaderText="Customer">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="gendesc" HeaderText="Cabang">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="custaddr" HeaderText="Address">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                            HorizontalAlign="Right" />
                                                        <EmptyDataTemplate>
                                                        </EmptyDataTemplate>
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListCust" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" PopupControlID="pnlListCust" PopupDragHandleControlID="lblListCust" BackgroundCssClass="modalBackground" Drag="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListReg" runat="server"><contenttemplate>
<asp:Panel id="PanelListReg" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListReg" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Sales "></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListReg" runat="server" Width="100%" DefaultButton="btnFindListReg"><asp:Label id="Label245" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListReg" runat="server" Width="100px" CssClass="inpText">
                                                            <asp:ListItem Value="registerno">Nama</asp:ListItem>
                                                            <asp:ListItem Value="PERSONNIP">NIP</asp:ListItem>
                                                        </asp:DropDownList> <asp:TextBox id="txtFilterListReg" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListReg" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListReg" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllReg" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneReg" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedReg" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListReg" runat="server" Width="98%" ForeColor="Black" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" PageSize="8" CellPadding="4">
                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="cbLMPO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("registermstoid") %>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="PERSONNIP" HeaderText="NIP">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Left"
                                                                    VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="registerno" HeaderText="Nama Sales">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="PERSONNIP" HeaderText="Jabatan">
                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="STATUS" HeaderText="Status">
                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                            HorizontalAlign="Right" />
                                                        <EmptyDataTemplate>
                                                        </EmptyDataTemplate>
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToListListReg" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListReg" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListReg" runat="server" TargetControlID="btnHiddenListReg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListReg" PopupControlID="PanelListReg">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListReg" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="800px" CssClass="modalBox" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List of katalog</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="Kode/Deskripsi">Kode/Deskripsi</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat" onclick="btnFindListMat_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" onclick="btnAllListMat_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 350px"><asp:GridView id="gvItem" runat="server" Width="97%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemcode,itemdesc,itemoid,satuan3" UseAccessibleHeader="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
    <HeaderTemplate>
        <asp:CheckBox ID="chkAllItem" runat="server" AutoPostBack="True" OnCheckedChanged="chkAllItem_CheckedChanged" />
    </HeaderTemplate>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="SaddleBrown" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
    <PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast" />
</asp:GridView></DIV></TD></TR><TR><TD style="HEIGHT: 18px" align=center colSpan=3><asp:LinkButton id="lbAddToListMat" onclick="lbAddToListMat_Click" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" onclick="lbCloseListMat_Click" runat="server">[ Cancel & Close ]</asp:LinkButton> <asp:Label id="LBLBOXMSG" runat="server" ForeColor="#FF3300" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" PopupControlID="pnlListMat" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

