<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPengerjaan.aspx.vb" Inherits="ReportForm_rptPengerjaan" title="MSC - Report Pengerjaan" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                            <asp:Label ID="Label249" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                                ForeColor="Navy" Text=".: Laporan Pengerjaan Servis"></asp:Label></th>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD align=left>Cabang</TD><TD>:</TD><TD style="TEXT-ALIGN: left" align=left><asp:DropDownList id="DDLcabang" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w284"></asp:DropDownList></TD></TR><TR><TD align=left>Filter</TD><TD>:</TD><TD style="TEXT-ALIGN: left" align=left><asp:DropDownList id="ddlFilter" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w263"><asp:ListItem Value="QLA.BARCODE">Barcode</asp:ListItem>
<asp:ListItem Value="QLA.REQCODE">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="QLA.REQITEMNAME">Nama Barang</asp:ListItem>
<asp:ListItem Value="QLA2.GENDESC">Merk Barang</asp:ListItem>
<asp:ListItem Value="QLA3.GENDESC">Jenis Barang</asp:ListItem>
<asp:ListItem Value="t2.PERSONNAME">Teknisi Akhir</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="141px" CssClass="inpText" __designer:wfdid="w264"></asp:TextBox></TD></TR><TR><TD align=left>Periode</TD><TD>:</TD><TD style="TEXT-ALIGN: left" align=left><asp:TextBox id="period1" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w265"></asp:TextBox>&nbsp;<asp:ImageButton id="ibtgl1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w266"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w267"></asp:Label>&nbsp;<asp:TextBox id="period2" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w268"></asp:TextBox>&nbsp;<asp:ImageButton id="ibtgl2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w269"></asp:ImageButton>&nbsp; <asp:Label id="Label2" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w270"></asp:Label>&nbsp;<asp:Label id="custoid" runat="server" __designer:wfdid="w271"></asp:Label></TD></TR><TR><TD id="TD1" runat="server" Visible="true"><asp:RadioButtonList id="cbStatus" runat="server" Width="115px" __designer:wfdid="w272" RepeatDirection="Horizontal"><asp:ListItem Selected="True">Status</asp:ListItem>
<asp:ListItem>Paid</asp:ListItem>
</asp:RadioButtonList></TD><TD id="TD3" runat="server" Visible="true">:</TD><TD style="TEXT-ALIGN: left" id="TD2" runat="server" Visible="true"><asp:DropDownList id="ddlStatus" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w273"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>Start</asp:ListItem>
<asp:ListItem>Finish</asp:ListItem>
<asp:ListItem>Final</asp:ListItem>
<asp:ListItem>Invoiced</asp:ListItem>
<asp:ListItem Value="CheckOut">Failed</asp:ListItem>
<asp:ListItem>Send</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="ImageButton1" onclick="ImageButton1_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w274"></asp:ImageButton> <asp:ImageButton id="ToPDF" onclick="ToPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w275" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbExport" onclick="imbExport_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w276" Visible="False"></asp:ImageButton> <asp:ImageButton id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w277"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w278" TargetControlID="period1" PopupButtonID="ibtgl1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w279" TargetControlID="period1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w280" TargetControlID="period2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w281" TargetControlID="period2" PopupButtonID="ibtgl2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w282"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle" __designer:wfdid="w283"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR></TR><TR></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" Width="350px" __designer:wfdid="w241" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="CrystalReportViewer1"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="UpdatePanelValidasi" runat="server"><contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" align=left colSpan=2><asp:Label id="WARNING" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD vAlign=middle align=left><asp:Label id="lblValidasi" runat="server" Font-Size="X-Small" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=center colSpan=2>&nbsp;<asp:ImageButton id="btnErrOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" DropShadow="True" PopupDragHandleControlID="Labelvalidasi" PopupControlID="PanelValidasi" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

