<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="PenerimaanBarang.aspx.vb" Inherits="Master_PenerimaanBarang" title="MSC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table class="header" width="100%">
       <tr>
         <td align="left" style="height: 15px; background-color: silver; width: 337px;">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="Navy" Text=".: Penerimaan Service"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel3" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 168px; HEIGHT: 22px">Cabang</TD><TD style="HEIGHT: 22px">:</TD><TD style="WIDTH: 1264px; HEIGHT: 22px"><asp:DropDownList id="fCbangDDL" runat="server" CssClass="inpText" __designer:wfdid="w24"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 168px; HEIGHT: 22px">Type TTS</TD><TD style="HEIGHT: 22px">:</TD><TD style="WIDTH: 1264px; HEIGHT: 22px"><asp:DropDownList id="DDLTypeTTS" runat="server" Width="90px" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="N">Service</asp:ListItem>
<asp:ListItem Value="M">Mutasi</asp:ListItem>
<asp:ListItem Value="Y">Saldo Awal</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 168px; HEIGHT: 22px">Filter&nbsp; </TD><TD style="HEIGHT: 22px">:</TD><TD style="WIDTH: 1264px; HEIGHT: 22px"><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w25"><asp:ListItem Value="Reqcode">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="custname">Nama Customer</asp:ListItem>
<asp:ListItem Enabled="False" Value="cust.phone1">Telepon</asp:ListItem>
<asp:ListItem Enabled="False" Value="Barcode">Barcode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" __designer:wfdid="w26"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnView" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w29" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 168px"><asp:CheckBox id="checkPeriod" runat="server" Text="Periode" __designer:wfdid="w30"></asp:CheckBox> </TD><TD>:</TD><TD style="WIDTH: 1264px"><asp:TextBox id="FilterPeriod1" runat="server" Width="65px" CssClass="inpText" __designer:wfdid="w31"></asp:TextBox>&nbsp;<asp:ImageButton id="ibPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w32"></asp:ImageButton> to&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="65px" CssClass="inpText" __designer:wfdid="w33"></asp:TextBox>&nbsp;<asp:ImageButton id="ibPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton>&nbsp;<asp:Label id="Label2" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w35"></asp:Label></TD></TR><TR><TD style="WIDTH: 168px"><asp:CheckBox id="checkStatus" runat="server" Text="Status" __designer:wfdid="w36"></asp:CheckBox> </TD><TD>:</TD><TD style="WIDTH: 1264px"><asp:DropDownList id="StatusDDL" runat="server" CssClass="inpText" __designer:wfdid="w37"><asp:ListItem Value="All">ALL</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD colSpan=3><asp:GridView style="max-width: 940px" id="gvPenerimaan" runat="server" Width="100%" Height="72px" ForeColor="#333333" __designer:wfdid="w38" AutoGenerateColumns="False" PageSize="8" DataKeyNames="REQOID" CellPadding="4" GridLines="None" AllowPaging="True" AllowSorting="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="reqoid" DataNavigateUrlFormatString="~/Transaction/PenerimaanBarang.aspx?oid={0}" DataTextField="reqcode" HeaderText="No. Penerimaan" SortExpression="reqcode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="70px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="barcode" HeaderText="Barcode" SortExpression="barcode" Visible="False">
<FooterStyle BackColor="DodgerBlue" BorderColor="DodgerBlue"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="phone1" HeaderText="Telepon" SortExpression="phone1">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tanggal" HeaderText="Tanggal" SortExpression="tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqstatus" HeaderText="Status" SortExpression="reqstatus">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TypeTTS" HeaderText="Type TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbPrint" onclick="lbPrint_Click" runat="server" __designer:wfdid="w43" ToolTip='<%# Eval("REQOID") %>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Width="65px"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label10" runat="server" ForeColor="Red" Text="Data Not Found" __designer:wfdid="w56"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:dtid="281474976710684" __designer:wfdid="w39" Enabled="True" TargetControlID="FilterPeriod1" PopupButtonID="ibPeriod1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:dtid="281474976710685" __designer:wfdid="w40" Enabled="True" TargetControlID="txtPeriod2" PopupButtonID="ibPeriod2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:dtid="281474976710687" __designer:wfdid="w41" Enabled="True" TargetControlID="FilterPeriod1" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureThousandsPlaceholder="" CultureDecimalPlaceholder="" CultureTimePlaceholder="" CultureDatePlaceholder="" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w42" TargetControlID="txtPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="gvPenerimaan"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <strong><span style="font-size: 9pt"> List Of Penerimaan Service</span></strong>
                <strong><span style="font-size: 9pt">:.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<DIV><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 171px">Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DdlCabang" runat="server" Width="144px" CssClass="inpText" OnSelectedIndexChanged="DdlCabang_SelectedIndexChanged" AutoPostBack="True" __designer:wfdid="w109"></asp:DropDownList> <asp:Label id="lblBarcode" runat="server" __designer:wfdid="w110" Visible="False"></asp:Label></TD><TD style="WIDTH: 74px">Type TTS</TD><TD>:</TD><TD><asp:DropDownList id="saldoawal" runat="server" Width="90px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w111"><asp:ListItem Value="N">Service</asp:ListItem>
<asp:ListItem Enabled="False" Value="M">Mutasi</asp:ListItem>
<asp:ListItem Enabled="False" Value="Y">Saldo Awal</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 171px">Location</TD><TD>:</TD><TD><asp:DropDownList id="DDLLocation" runat="server" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w112"></asp:DropDownList></TD><TD style="WIDTH: 74px">Status</TD><TD>:</TD><TD><asp:TextBox id="txtStatus" runat="server" Width="136px" CssClass="inpTextDisabled" __designer:wfdid="w113" ReadOnly="True" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 171px">No. Tanda Terima</TD><TD style="HEIGHT: 21px">:</TD><TD style="HEIGHT: 21px"><asp:TextBox id="txtNoTanda" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w114" ReadOnly="True" MaxLength="15"></asp:TextBox></TD><TD style="WIDTH: 74px; HEIGHT: 21px" id="Td7" Visible="false">C. Person 1</TD><TD style="HEIGHT: 21px" id="Td8" Visible="false">:</TD><TD style="HEIGHT: 21px" id="Td9" Visible="false"><asp:TextBox id="cperson1" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w115" Enabled="False" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 171px">Tanggal Masuk</TD><TD>:</TD><TD><asp:TextBox id="txtTglTerima" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w116" ReadOnly="True"></asp:TextBox> <asp:Label id="Label7" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w117"></asp:Label></TD><TD style="WIDTH: 74px; COLOR: #000099">Telepon 2</TD><TD>:</TD><TD><asp:TextBox id="telp2" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w118" Enabled="False" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 171px">Waktu Masuk</TD><TD>:</TD><TD><asp:TextBox id="txtWaktuTerima" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w119" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 74px"><asp:Label id="Label9" runat="server" Text="Reqdate" __designer:wfdid="w120" Visible="False"></asp:Label></TD><TD></TD><TD><asp:TextBox id="txtreqdate" runat="server" Width="136px" CssClass="inpTextDisabled" __designer:wfdid="w121" Visible="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 171px">Customer <asp:Label id="Label3" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w122"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="txtNamaCust" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w123" Enabled="False" MaxLength="25"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w124"></asp:ImageButton> <asp:ImageButton id="btnErase" onclick="btnErase_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w125"></asp:ImageButton> <asp:ImageButton id="ibTambahCust" runat="server" ImageUrl="~/Images/Add.png" ImageAlign="AbsMiddle" __designer:wfdid="w126" Visible="False"></asp:ImageButton></TD><TD style="WIDTH: 74px"><asp:Label id="i_u" runat="server" ForeColor="Red" __designer:wfdid="w127" Visible="False"></asp:Label></TD><TD></TD><TD><asp:Label id="lblCust" runat="server" __designer:wfdid="w128" Visible="False"></asp:Label><asp:Label id="lbloid" runat="server" __designer:wfdid="w129" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 171px">Alamat</TD><TD>:</TD><TD><asp:TextBox id="txtAlamat" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w130" Enabled="False" MaxLength="100"></asp:TextBox></TD><TD style="WIDTH: 74px"><asp:ImageButton id="btnBarcode" runat="server" ImageUrl="~/Images/generatebarcode1.png" ImageAlign="AbsMiddle" __designer:wfdid="w131" Visible="False"></asp:ImageButton></TD><TD></TD><TD><asp:TextBox id="txtBarcode" runat="server" Width="136px" CssClass="inpTextDisabled" __designer:wfdid="w132" Visible="False" Enabled="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 171px">HP Customer <asp:Label id="Label5" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w133"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="txtHP" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w134" ReadOnly="True" MaxLength="15"></asp:TextBox></TD><TD style="WIDTH: 74px"></TD><TD></TD><TD><asp:DropDownList id="MtrLocDDL" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w135" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 171px" id="TD2" runat="server" Visible="false">Type&nbsp;Barang</TD><TD id="TD4" runat="server" Visible="false">:</TD><TD id="TD6" runat="server" Visible="false"><asp:DropDownList id="JenisDDL" runat="server" Width="144px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w136"></asp:DropDownList></TD><TD style="WIDTH: 74px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 171px" id="TD3" runat="server" Visible="false">Merk Barang</TD><TD id="TD1" runat="server" Visible="false">:</TD><TD id="TD5" runat="server" Visible="false"><asp:DropDownList id="MerkDDL" runat="server" Width="144px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w137"></asp:DropDownList>&nbsp;<asp:ImageButton id="ibTambahMerk" runat="server" ImageUrl="~/Images/Add.png" ImageAlign="AbsMiddle" __designer:wfdid="w138" Visible="False"></asp:ImageButton></TD><TD style="WIDTH: 74px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 171px"><asp:Label id="Label20" runat="server" Width="136px" Font-Size="Small" Font-Bold="True" ForeColor="Red" Text="Detail Kerusakan :" __designer:wfdid="w139" Font-Underline="True"></asp:Label> </TD><TD></TD><TD colSpan=4></TD></TR><TR><TD style="WIDTH: 171px"><asp:Label id="TxtLoc" runat="server" Text="Gudang" __designer:wfdid="w140" Visible="False"></asp:Label></TD><TD><asp:Label id="Label21" runat="server" Text=":" __designer:wfdid="w141" Visible="False"></asp:Label></TD><TD colSpan=4></TD></TR><TR><TD>Type Service</TD><TD style="HEIGHT: 21px">:</TD><TD colSpan=4><asp:RadioButtonList id="rblgaransi" runat="server" __designer:wfdid="w142" RepeatDirection="Horizontal"><asp:ListItem>Servis</asp:ListItem>
<asp:ListItem Selected="True">Garansi</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD>No SI.</TD><TD style="HEIGHT: 21px"></TD><TD colSpan=4><asp:TextBox id="trnjualno" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w143" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSeacrhSI" onclick="imbSeacrhSI_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w144"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearSI" onclick="imbClearSI_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w145"></asp:ImageButton>&nbsp;<asp:Label id="trnjualoid" runat="server" __designer:wfdid="w146" Visible="False"></asp:Label></TD></TR><TR><TD>Katalog&nbsp;<asp:Label id="Label6" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w147"></asp:Label></TD><TD style="HEIGHT: 21px">:</TD><TD colSpan=4><asp:TextBox id="txtNamaBarang" runat="server" Width="288px" CssClass="inpText" __designer:wfdid="w148" Enabled="False" MaxLength="70"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w149"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearItem" onclick="imbClearItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w150"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" __designer:wfdid="w151" Visible="False"></asp:Label></TD></TR><TR><TD>Qty</TD><TD style="HEIGHT: 21px">:</TD><TD colSpan=4><asp:TextBox id="txtQty" runat="server" Width="136px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w152">0.00</asp:TextBox></TD></TR><TR><TD>Kelengkapan</TD><TD style="HEIGHT: 21px">:</TD><TD colSpan=4><asp:TextBox id="txtKelengkapan" runat="server" Width="288px" Height="30px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w153" MaxLength="150" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:Label id="Label19" runat="server" Width="128px" ForeColor="Red" Text="maks. 150 karakter" __designer:wfdid="w154"></asp:Label></TD></TR><TR><TD>No SN</TD><TD style="HEIGHT: 21px">:</TD><TD colSpan=4><asp:TextBox id="snno" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w155" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 171px" rowSpan=2>Kerusakan</TD><TD rowSpan=2>:</TD><TD colSpan=4 rowSpan=2><asp:TextBox id="txtDetailKerusakan" runat="server" Width="288px" Height="30px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w156" MaxLength="150" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:Label id="Label8" runat="server" Width="128px" ForeColor="Red" Text="maks. 150 karakter" __designer:wfdid="w157"></asp:Label>&nbsp;</TD></TR><TR></TR><TR><TD colSpan=6><asp:Label id="dtlseq" runat="server" __designer:wfdid="w158" Visible="False"></asp:Label>&nbsp;<asp:Label id="dtlrow" runat="server" __designer:wfdid="w159" Visible="False"></asp:Label>&nbsp;<asp:Label id="lblNo" runat="server" __designer:wfdid="w160" Visible="False"></asp:Label>&nbsp;<asp:Label id="lblUpdNo" runat="server" __designer:wfdid="w161" Visible="False"></asp:Label>&nbsp;<asp:Label id="createtime" runat="server" __designer:wfdid="w162" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w163"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w164"></asp:ImageButton> <asp:Label id="iuAdd" runat="server" __designer:wfdid="w165" Visible="False">new</asp:Label>&nbsp;</TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 184px; BACKGROUND-COLOR: beige"><asp:GridView id="gvNoKerusakan" runat="server" Width="99%" Height="40px" ForeColor="#333333" __designer:wfdid="w166" OnRowDataBound="gvNoKerusakan_RowDataBound" AutoGenerateColumns="False" PageSize="2" DataKeyNames="sequence" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" BorderColor="Blue" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sequence" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" BorderColor="Blue"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="SI No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="snno" HeaderText="SN No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kelengkapan" HeaderText="Kelengkapan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="garansi" HeaderText="Garansi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kerusakan" HeaderText="Kerusakan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="Medium" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" BorderColor="Blue" Font-Bold="True" Font-Size="Medium" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualoid" HeaderText="trnjualoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label23" runat="server" ForeColor="Red" Text="Data Not Found" __designer:wfdid="w26"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD colSpan=6><asp:Label id="lblUpd" runat="server" __designer:dtid="562949953421338" __designer:wfdid="w167"></asp:Label> On <asp:Label id="UpdTime" runat="server" Font-Bold="True" __designer:dtid="562949953421339" __designer:wfdid="w168"></asp:Label> By&nbsp;<asp:Label id="UpdUser" runat="server" Font-Bold="True" __designer:dtid="562949953421340" __designer:wfdid="w169"></asp:Label> <asp:DropDownList id="ddlprinter" runat="server" Width="66px" CssClass="inpText" __designer:dtid="562949953421353" __designer:wfdid="w170" Visible="False"><asp:ListItem __designer:dtid="562949953421354">PDF</asp:ListItem>
<asp:ListItem __designer:dtid="562949953421355">Auto</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:dtid="562949953421345" __designer:wfdid="w171"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnPost" runat="server" ImageUrl="~/Images/posting.png" __designer:dtid="562949953421346" __designer:wfdid="w172" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:dtid="562949953421347" __designer:wfdid="w173"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:dtid="562949953421348" __designer:wfdid="w174"></asp:ImageButton> <asp:ImageButton id="printmanualpenerimaan" runat="server" ImageUrl="~/Images/print.png" __designer:dtid="562949953421352" __designer:wfdid="w175" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="BtnSendApp" runat="server" ImageUrl="~/Images/sendapproval.png" __designer:dtid="562949953421346" __designer:wfdid="w176" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w177" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w178"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" __designer:wfdid="w179" ValidChars="1234567890,." TargetControlID="txtQty"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE></DIV>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="printmanualpenerimaan"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel4" runat="server">
        <contenttemplate>
<asp:Panel id="panelCust" runat="server" CssClass="modalBox" __designer:wfdid="w181" Visible="False" DefaultButton="imbFindCust"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptCust" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Customer" __designer:wfdid="w182"></asp:Label></TD></TR><TR><TD align=center>Filter&nbsp;: <asp:DropDownList id="DDLFilterCust" runat="server" CssClass="inpText" __designer:wfdid="w183"><asp:ListItem Value="c.custname">Nama</asp:ListItem>
<asp:ListItem Value="c.custaddr">Alamat</asp:ListItem>
<asp:ListItem Value="c.phone1">No. Telp</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterCust" runat="server" CssClass="inpText" __designer:wfdid="w184"></asp:TextBox> <asp:ImageButton id="imbFindCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w185"></asp:ImageButton> <asp:ImageButton id="imbViewCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w186"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView id="gvListCust" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w187" AllowPaging="True" GridLines="None" CellPadding="4" DataKeyNames="custoid" PageSize="5" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="CUSTCODE" HeaderText="No" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="phone1" HeaderText="No.Telp">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="NmCabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
No data Pelanggan
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center><asp:LinkButton id="lkbCloseCust" runat="server" __designer:wfdid="w188">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeCust" runat="server" __designer:wfdid="w189" TargetControlID="btnHideCust" BackgroundCssClass="modalBackground" PopupControlID="panelCust" PopupDragHandleControlID="lblCaptCust"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideCust" runat="server" __designer:wfdid="w190" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel5" runat="server">
        <contenttemplate>
<asp:Panel id="panelSI" runat="server" CssClass="modalBox" __designer:wfdid="w192" Visible="False" DefaultButton="imbFindSI"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblSI" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="List Sales Invoice" __designer:wfdid="w193"></asp:Label></TD></TR><TR><TD align=center>Filter&nbsp;: <asp:DropDownList id="DDLFilterSI" runat="server" CssClass="inpText" __designer:wfdid="w194"><asp:ListItem Value="trnjualno">SI No,</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterSI" runat="server" CssClass="inpText" __designer:wfdid="w195"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSI" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w196"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbViewSI" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w197"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView id="gvListSI" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w198" AllowPaging="True" GridLines="None" CellPadding="4" DataKeyNames="trnjualmstoid,trnjualno,trnjualdate" PageSize="5" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="No. Nota SI">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" BorderColor="Black"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" ForeColor="Red" Text="Data Not Found" __designer:wfdid="w2"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center><asp:LinkButton id="lkbCloseSI" runat="server" __designer:wfdid="w199">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeSI" runat="server" __designer:wfdid="w200" TargetControlID="btnHideSI" BackgroundCssClass="modalBackground" PopupControlID="panelSI" PopupDragHandleControlID="lblSI"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideSI" runat="server" __designer:wfdid="w201" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel8" runat="server">
        <contenttemplate>
<asp:Panel id="panelItem" runat="server" Width="70%" CssClass="modalBox" __designer:wfdid="w203" Visible="False" DefaultButton="imbFindItem"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptItem" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Barang" __designer:wfdid="w204"></asp:Label></TD></TR><TR><TD align=center>Filter&nbsp;: <asp:DropDownList id="DDLFilterItem" runat="server" CssClass="inpText" __designer:wfdid="w205"><asp:ListItem Value="itemdesc">Deskripsi</asp:ListItem>
<asp:ListItem>Itemcode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterItem" runat="server" Width="242px" CssClass="inpText" __designer:wfdid="w206"></asp:TextBox> <asp:ImageButton id="imbFindItem" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w207"></asp:ImageButton> <asp:ImageButton id="imbViewItem" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w208"></asp:ImageButton></TD></TR><TR><TD align=center><asp:GridView id="gvListItem" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w209" AllowPaging="True" GridLines="None" CellPadding="4" DataKeyNames="itemoid" PageSize="5" AutoGenerateColumns="False" OnPageIndexChanging="gvListItem_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deksripsi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle BorderColor="Black"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockflag" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="StokAkhir" HeaderText="Stock Akhir">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
No data Pelanggan
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center><asp:LinkButton id="lkbCloseItem" runat="server" __designer:wfdid="w210">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" __designer:wfdid="w211" TargetControlID="btnHideItem" BackgroundCssClass="modalBackground" PopupControlID="panelItem" PopupDragHandleControlID="lblCaptItem"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideItem" runat="server" __designer:wfdid="w212" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <strong><span style="font-size: 9pt"> <span>
                Form Penerimaan Service</span></span></strong> <strong><span style="font-size: 9pt">
                    :.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel2" runat="server">
        <contenttemplate>
<asp:Panel id="panelMsg" runat="server" Width="100%" CssClass="modalMsgBox" Visible="False"><TABLE style="WIDTH: 100%; HEIGHT: 109%"><TBODY><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeValidasi" runat="server" TargetControlID="btnValidasi" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnValidasi" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

