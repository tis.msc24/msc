Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnWhTransfer
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region
    
#Region "Procedure"
    Private Sub FillTextbox()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim tolocationoid As Integer = 0

        sSql = "SELECT transferno, trfmtrdate, trfmtrnote, noref, status, FromMtrlocoid, ToMtrlocOid, updtime, upduser FROM QL_matUsageMst WHERE cmpcode = '" & cmpcode & "' AND trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                transferno.Text = xreader("transferno")
                transferdate.Text = Format(xreader("trfmtrdate"), "dd/MM/yyyy")
                fromlocation.SelectedValue = xreader("FromMtrlocoid")
                note.Text = xreader("trfmtrnote")
                noref.Text = xreader("noref")
                tolocationoid = xreader("ToMtrlocOid")
                trnstatus.Text = xreader("status").ToString.Trim
                upduser.Text = xreader("upduser")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
            End While
        Else
            showMessage("Missing transfer data >.<", 2)
        End If
        xreader.Close()

        sSql = "SELECT a.seq, a.refoid AS itemoid, b.itemdesc, b.merk, a.qty, a.unitoid AS satuan, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3, c.gendesc AS unit, d.gendesc AS unit1, e.gendesc AS unit2, f.gendesc AS unit3, a.trfdtlnote AS note FROM QL_matUsageDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.unitoid = c.genoid INNER JOIN QL_mstgen d ON b.cmpcode = d.cmpcode AND b.satuan1 = d.genoid INNER JOIN QL_mstgen e ON b.cmpcode = e.cmpcode AND b.satuan2 = e.genoid INNER JOIN QL_mstgen f ON b.cmpcode = f.cmpcode AND b.satuan2 = f.genoid WHERE a.cmpcode = '" & cmpcode & "' AND a.trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "detailtw")
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        If trnstatus.Text = "POST" Or trnstatus.Text = "In Approval" Or trnstatus.Text = "Approved" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            BtnCancel.Visible = True
            btnPosting.Visible = False
            btnApproval.Visible = False
        Else
            btnSave.Visible = True
            btnDelete.Visible = True
            BtnCancel.Visible = True
            btnPosting.Visible = True
            btnApproval.Visible = False
        End If

        conn.Close()

        'locasi kedua dibuat Nol
        FillDDL(tolocation, "SELECT 0,'Material Usage'")

        tolocation.SelectedValue = tolocationoid
    End Sub

    Private Sub BindData(ByVal state As String)
        Dim date1, date2 As New Date

        If state = 0 Then

            date1 = New Date(Date.Now.Year, Date.Now.Month, 1)
            date2 = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)

            sSql = "SELECT trfmtrmstoid, transferno, trfmtrdate, upduser, status FROM QL_matUsageMst WHERE cmpcode = '" & cmpcode & "' AND CONVERT(DATE, trfmtrdate, 101) BETWEEN '" & date1 & "' AND '" & date2 & "' and typetransfer = 'MS' ORDER BY transferno"

        Else

            date1 = Date.ParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing)
            date2 = Date.ParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing)

            Dim swhere As String = ""
            Dim swhere1 As String = ""

            If group.SelectedValue <> "ALL" Then
                swhere &= " AND b.itemgroupoid = " & group.SelectedValue & ""
            End If

            If subgroup.SelectedValue <> "ALL" Then
                swhere &= " AND b.itemsubgroupoid = " & subgroup.SelectedValue & ""
            End If

            Dim searchitemoid As Integer = 0
            If itemoid.Text <> "" Then
                If Integer.TryParse(itemoid.Text, searchitemoid) = False Then
                    showMessage("Item tidak valid !", 2)
                    Exit Sub
                Else
                    swhere &= " AND a.refoid = " & Integer.Parse(itemoid.Text) & ""
                End If
            End If

            If FilterText.Text.Trim <> "" Then
                swhere1 &= " AND c.transferno LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
            End If

            sSql = "SELECT DISTINCT c.trfmtrmstoid, c.transferno, c.trfmtrdate, c.upduser, c.status FROM QL_matUsageDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM'" & swhere & " INNER JOIN QL_matUsageMst c ON a.cmpcode = c.cmpcode AND a.trfmtrmstoid = c.trfmtrmstoid" & swhere1 & " WHERE CONVERT(DATE, c.trfmtrdate, 101) BETWEEN '" & date1 & "' AND '" & date2 & "' and c.typetransfer = 'MS'  ORDER BY c.transferno"

        End If

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "tw")
        gvMaster.DataSource = dtab
        gvMaster.DataBind()
        Session("tw") = dtab
    End Sub

    Private Sub BindItem()
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        sSql = "SELECT a.itemoid, a.itemcode, a.itemdesc, a.merk, (e.saldoakhir-e.qtybooking) as sisa, a.satuan1, b.gendesc AS unit1, a.satuan2, c.gendesc AS unit2, a.satuan3, d.gendesc AS unit3, a.konversi1_2, a.konversi2_3 FROM QL_mstItem a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.satuan1 = b.genoid INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.satuan2 = c.genoid INNER JOIN QL_mstgen d ON a.cmpcode = d.cmpcode AND a.satuan3 = d.genoid INNER JOIN QL_crdmtr e ON a.cmpcode = e.cmpcode AND a.itemoid = e.refoid AND e.refname = 'QL_MSTITEM' AND e.mtrlocoid = " & fromlocation.SelectedValue & " AND e.periodacctg  = '" & GetDateToPeriodAcctg(transdate).Trim & "' WHERE a.itemflag = 'Aktif' AND (a.itemdesc LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.itemcode LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.merk LIKE '%" & Tchar(item.Text.Trim) & "%') AND a.cmpcode = '" & cmpcode & "' and (e.saldoakhir-e.qtybooking) > 0"

        Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
        GVItemList.DataSource = objTable
        GVItemList.DataBind()
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Sub InitAllDDL()
        FillDDLGen("ITEMGROUP", group)
        group.Items.Add("ALL")
        group.SelectedValue = "ALL"
        FillDDLGen("ITEMSUBGROUP", subgroup)
        subgroup.Items.Add("ALL")
        subgroup.SelectedValue = "ALL"

        'FillDDLGen("LOCATION", fromlocation)
        FillDDL(fromlocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "'  ORDER BY a.gendesc")
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If
        fromlocation.SelectedIndex = 0

        'locasi kedua dibuat Nol
        FillDDL(tolocation, "SELECT 0,'Material Usage'")
        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If
        tolocation.SelectedIndex = 0
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            'simpan session k variabel spy tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")

            'clear all session 
            Session.Clear()

            'insert lagi session yg disimpan dan create session 
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            'di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
            Response.Redirect("~\Transaction\trnMatUsage_Service.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")
        btnApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data to approval?')")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Material Usage"

        If Not Page.IsPostBack Then
            BindData(0)
            transferno.Text = GenerateID("QL_matUsageMst", cmpcode)
            transferdate.Text = Format(Date.Now, "dd/MM/yyyy")
            InitAllDDL()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(date1, "dd/MM/yyyy")
                tgl2.Text = Format(date2, "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0

                qty.Text = "0.00"
                GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                trnstatus.Text = "In Process"
                labelseq.Text = "1"
                labelkonversi1_2.Text = "1"
                labelkonversi2_3.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        BindData(0)

        FilterText.Text = ""
        group.SelectedValue = "ALL"
        subgroup.SelectedValue = "ALL"
        Filteritem.Text = ""

        Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
        Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
        tgl1.Text = Format(date1, "dd/MM/yyyy")
        tgl2.Text = Format(date2, "dd/MM/yyyy")

        itemoid.Text = ""
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Tanggal period harus diisi !", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Tanggal period 1 tidak valid !", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Tanggal period 2 tidak valid !", 2)
            Exit Sub
        End If

        If date1 > date2 Then
            showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
            Exit Sub
        End If

        BindData(1)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Try
            BindItem()
            GVItemList.Visible = True
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = ""
        labelitemoid.Text = ""
        qty.Text = "0.00"
        labelmaxqty.Text = "0.00"
        merk.Text = ""
        notedtl.Text = ""

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        Try
            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            sSql = "SELECT a.itemoid, a.itemcode, a.itemdesc, a.merk, (e.saldoakhir-e.qtybooking) as sisa, a.satuan1, b.gendesc AS unit1, a.satuan2, c.gendesc AS unit2, a.satuan3, d.gendesc AS unit3, a.konversi1_2, a.konversi2_3 FROM QL_mstItem a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.satuan1 = b.genoid INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.satuan2 = c.genoid INNER JOIN QL_mstgen d ON a.cmpcode = d.cmpcode AND a.satuan3 = d.genoid INNER JOIN QL_crdmtr e ON a.cmpcode = e.cmpcode AND a.itemoid = e.refoid AND e.refname = 'QL_MSTITEM' AND e.mtrlocoid = " & fromlocation.SelectedValue & " AND e.periodacctg  = '" & GetDateToPeriodAcctg(transdate).Trim & "' WHERE a.itemflag = 'Aktif' AND (a.itemdesc like '%" & Tchar(item.Text.Trim) & "%' OR a.itemcode like '%" & Tchar(item.Text.Trim) & "%') AND a.cmpcode = '" & cmpcode & "'"

            Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
            GVItemList.DataSource = objTable
            GVItemList.PageIndex = e.NewPageIndex
            GVItemList.DataBind()
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        item.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(2).Text
        labelitemoid.Text = GVItemList.SelectedDataKey("itemoid")
        merk.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(3).Text
        qty.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text
        labelmaxqty.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text

        unit.Items.Clear()
        unit.Items.Add(GVItemList.Rows(GVItemList.SelectedIndex).Cells(5).Text)
        unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan3")
        If GVItemList.SelectedDataKey("satuan2") <> GVItemList.SelectedDataKey("satuan3") Then
            unit.Items.Add(GVItemList.SelectedDataKey("unit2"))
            unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan2")
        End If
        If GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan3") And GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan2") Then
            unit.Items.Add(GVItemList.SelectedDataKey("unit1"))
            unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan1")
        End If
        unit.SelectedValue = GVItemList.SelectedDataKey("satuan3")
        labelsatuan1.Text = GVItemList.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemList.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemList.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemList.SelectedDataKey("unit1").ToString
        labelunit2.Text = GVItemList.SelectedDataKey("unit2").ToString
        labelunit3.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(5).Text

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelkonversi1_2.Text = GVItemList.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemList.SelectedDataKey("konversi2_3")

        GVItemList.Visible = False
        GVItemList.DataSource = Nothing
        GVItemList.DataBind()
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnMatUsage_Service.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        Dim qtyval As Double = 0.0
        If qty.Text.Trim <> "" Then
            If Double.TryParse(qty.Text.Trim, qtyval) Then
                qty.Text = Format(qtyval, "#,##0.00")
            Else
                qty.Text = Format(0.0, "#,##0.00")
            End If
        Else
            qty.Text = Format(0.0, "#,##0.00")
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            showMessage("Item tidak boleh kosong!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) = 0.0 Then
            showMessage("Qty harus lebih dari 0!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) > Double.Parse(labelmaxqty.Text.Trim) Then
            showMessage("Qty tidak boleh lebih dari maksimum qty!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable

        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("unit1", Type.GetType("System.String"))
                dtab.Columns.Add("unit2", Type.GetType("System.String"))
                dtab.Columns.Add("unit3", Type.GetType("System.String"))
                dtab.Columns.Add("note", Type.GetType("System.String"))
                Session("itemdetail") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetail")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "' AND seq <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list !", 2)
                Exit Sub
            End If
        End If

        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow

            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = unit.SelectedValue
            drow("unit") = unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim

            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()

            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = unit.SelectedValue
            drow("unit") = unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim

            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
        merk.Text = ""
        qty.Text = "0.00"
        labelmaxqty.Text = "0.00"
        unit.Items.Clear()
        notedtl.Text = ""
        labelitemoid.Text = ""
        item.Text = ""
        I_u2.Text = "new"

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new"
        labelitemoid.Text = ""
        qty.Text = "0.00"
        item.Text = ""
        labelmaxqty.Text = "0.00"
        unit.Items.Clear()
        notedtl.Text = ""
        merk.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        item.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(2).Text
        qty.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(4).Text

        notedtl.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(6).Text.Replace("&nbsp;", "")
        merk.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(3).Text

        unit.Items.Clear()
        unit.Items.Add(GVItemDetail.SelectedDataKey("unit3"))
        unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan3")
        If GVItemDetail.SelectedDataKey("satuan2") <> GVItemDetail.SelectedDataKey("satuan3") Then
            unit.Items.Add(GVItemDetail.SelectedDataKey("unit2"))
            unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan2")
        End If
        If GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan3") And GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan2") Then
            unit.Items.Add(GVItemDetail.SelectedDataKey("unit1"))
            unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan1")
        End If
        unit.SelectedValue = GVItemDetail.SelectedDataKey("satuan")

        labelsatuan1.Text = GVItemDetail.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemDetail.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemDetail.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemDetail.SelectedDataKey("unit1")
        labelunit2.Text = GVItemDetail.SelectedDataKey("unit2")
        labelunit3.Text = GVItemDetail.SelectedDataKey("unit3")

        labelkonversi1_2.Text = GVItemDetail.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemDetail.SelectedDataKey("konversi2_3")

        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text

        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        conn.Open()
        sSql = "SELECT (saldoakhir-qtybooking) from QL_crdmtr WHERE refname = 'QL_MSTITEM' AND refoid = " & Integer.Parse(labelitemoid.Text) & " AND periodacctg = '" & GetDateToPeriodAcctg(Date.Now).Trim & "' AND cmpcode = '" & cmpcode & "'"
        xCmd.CommandText = sSql
        Dim maxqty As Double = xCmd.ExecuteScalar
        If Not maxqty = Nothing Then
            If GVItemDetail.SelectedDataKey("satuan") = GVItemDetail.SelectedDataKey("satuan1") Then
                maxqty = maxqty / GVItemDetail.SelectedDataKey("konversi1_2")
            End If
            labelmaxqty.Text = Format(maxqty, "#,##0.00")
        Else
            labelmaxqty.Text = "0.00"
        End If
        conn.Close()

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        GVItemDetail.Columns(7).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""

        If fromlocation.Items.Count <= 0 Then
            errmsg &= "Lokasi asal transfer tidak boleh kosong !"
        End If
        If tolocation.Items.Count <= 0 Then
            errmsg &= "Lokasi tujuan transfer tidak boleh kosong !"
        End If
        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "Detail transfer tidak boleh kosong !"
        End If

        If errmsg.Trim <> "" Then
            showMessage(errmsg, 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            Dim period As String = GetDateToPeriodAcctg(transdate).Trim

            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = 0 Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Tanggal ini tidak dalam periode Open Stock !", 2)
                trnstatus.Text = "In Process"
                Exit Sub
            End If

            'Cek saldoakhir
            If Not Session("itemdetail") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("itemdetail")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "SELECT (saldoAkhir-qtybooking) FROM QL_crdmtr WHERE periodacctg = '" & period & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND refoid = " & dtab.Rows(j).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql
                    saldoakhire = xCmd.ExecuteScalar
                    If saldoakhire = Nothing Or saldoakhire = 0 Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Material Usage tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir satuan std hanya tersedia 0.00 !", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub
                    Else
                        If dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan3") Then
                            If saldoakhire - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Material Usage tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                trnstatus.Text = "In Process"
                                Exit Sub
                            End If
                        ElseIf dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan2") Then
                            If (saldoakhire / dtab.Rows(j).Item("konversi2_3")) - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Material Usage tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                trnstatus.Text = "In Process"
                                Exit Sub
                            End If
                        ElseIf dtab.Rows(j).Item("satuan") = dtab.Rows(j).Item("satuan1") Then
                            If (saldoakhire / dtab.Rows(j).Item("konversi2_3") / dtab.Rows(j).Item("konversi1_2")) - dtab.Rows(j).Item("qty") < 0 Then
                                objTrans.Rollback()
                                conn.Close()
                                showMessage("Material Usage tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & Format(saldoakhire, "#,##0.00") & " " & dtab.Rows(j).Item("unit") & " !", 2)
                                trnstatus.Text = "In Process"
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If

            'Generate crdmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

            'Generate conmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim crdmtroid As Int32 = xCmd.ExecuteScalar + 1
            Dim tempoid As Int32 = crdmtroid

            Dim trfmtrmstoid As Integer = 0
            If i_u.Text = "new" Then
                'Generate transfer master ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_matUsageMst' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                trfmtrmstoid = xCmd.ExecuteScalar + 1
            End If

            'Generate transfer number Posting
            If trnstatus.Text = "POST" Then
                Dim trnno As String = "MS/" & Format(transdate, "yy") & "/" & Format(transdate, "MM") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transferno, 4) AS INT)), 0) FROM QL_matUsageMst WHERE cmpcode = '" & cmpcode & "' AND transferno LIKE '" & trnno & "%'"
                xCmd.CommandText = sSql
                trnno = trnno & Format(xCmd.ExecuteScalar + 1, "0000")
                transferno.Text = trnno
            Else
                If i_u.Text = "new" Then
                    transferno.Text = trfmtrmstoid.ToString
                End If
            End If

            'Generate transfer detail ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_matUsageDtl' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim trfmtrdtloid As Int32 = xCmd.ExecuteScalar + 1

            ''Jika pakai In approval
            'If trnstatus.Text = "In Approval" Then
            '    sSql = "select lastoid from ql_mstoid where tablename = 'ql_approval' and cmpcode = '" & cmpcode & "'"
            '    xCmd.CommandText = sSql
            '    Dim approvaloid As Integer = xCmd.ExecuteScalar

            '    sSql = "select tablename, approvaltype, approvallevel, approvaluser, approvalstatus from QL_approvalstructure where cmpcode = '" & cmpcode & "' and tablename = 'QL_matUsageMst' order by approvallevel"

            '    Dim dtab2 As DataTable = ckon.ambiltabel(sSql, "QL_approvalstructure")
            '    If dtab2.Rows.Count > 0 Then
            '        For i As Integer = 0 To dtab2.Rows.Count - 1
            '            approvaloid = approvaloid + 1
            '            sSql = "insert into ql_approval(cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus) VALUES ('" & cmpcode & "', " & approvaloid & ", '" & "TW" & Session("oid") & "_" & approvaloid & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'QL_matUsageMst', '" & Session("oid") & "', 'In Approval', '0', '" & dtab2.Rows(i).Item("approvaluser") & "', '1/1/1900', '" & dtab2.Rows(i).Item("approvaltype") & "', '1', '" & dtab2.Rows(i).Item("approvalstatus") & "')"
            '            xCmd.CommandText = sSql
            '            xCmd.ExecuteNonQuery()
            '        Next
            '        sSql = "update ql_mstoid set lastoid = " & approvaloid & " where tablename = 'ql_approval' and cmpcode = '" & cmpcode & "'"
            '        xCmd.CommandText = sSql
            '        xCmd.ExecuteNonQuery()
            '    Else
            '        objTrans.Rollback()
            '        If conn.State = ConnectionState.Open Then
            '            conn.Close()
            '        End If
            '        showMessage("No approval hierarki for transfer warehouse, please contact admin first!", 2)
            '        trnstatus.Text = "In Process"
            '        Exit Sub
            '    End If
            'End If



            Dim iglmst As Int64 = GenerateID("QL_trnglmst", cmpcode)
            Dim igldtl As Int64 = GenerateID("QL_trngldtl", cmpcode)
            Dim iGlSeq As Int16 = 1



            Dim COA_adjust_stock_minus As Integer = 0
            Dim varadjustmin As String = GetVarInterface("VAR_MATERIAL_USAGE_SERV", cmpcode)
            If varadjustmin Is Nothing Or varadjustmin = "" Then
                showMessage("Interface untuk Akun VAR_MATERIAL_USAGE_SERV tidak ditemukan!", 2)
                objTrans.Rollback()
                conn.Close()
                Exit Sub
            Else
                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & cmpcode & "' AND ACCTGCODE = '" & varadjustmin & "'"
                xCmd.CommandText = sSql
                COA_adjust_stock_minus = xCmd.ExecuteScalar
                If COA_adjust_stock_minus = 0 Or COA_adjust_stock_minus = Nothing Then
                    showMessage("Akun COA untuk VAR_ADJUST_STOCK- tidak ditemukan!", 2)
                    objTrans.Rollback()
                    conn.Close()
                    Exit Sub
                End If
            End If

            Dim COA_gudang As Integer = 0
            Dim vargudang As String = GetVarInterface("VAR_GUDANG", cmpcode)
            If vargudang Is Nothing Or vargudang = "" Then
                showMessage("Interface untuk Akun VAR_GUDANG tidak ditemukan!", 2)
                objTrans.Rollback()
                conn.Close()
                Exit Sub
            Else
                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & cmpcode & "' AND ACCTGCODE = '" & vargudang & "'"
                xCmd.CommandText = sSql
                COA_gudang = xCmd.ExecuteScalar
                If COA_gudang = 0 Or COA_gudang = Nothing Then
                    showMessage("Akun COA untuk VAR_GUDANG tidak ditemukan!", 2)
                    objTrans.Rollback()
                    conn.Close()
                    Exit Sub
                End If
            End If

            If trnstatus.Text = "POST" Then
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime) VALUES ('" & cmpcode & "', " & iglmst & ", CURRENT_TIMESTAMP, '" & period & "', 'MS', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & iglmst & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If



            'Insert new record
            If i_u.Text = "new" Then

                transferno.Text = trfmtrmstoid.ToString

                'Insert to master
                sSql = "INSERT INTO QL_matUsageMst (cmpcode, trfmtrmstoid, transferno, trfmtrdate, trfmtrnote, noref, status, frommtrlocoid, tomtrlocoid, updtime, upduser, typeTransfer) VALUES ('" & cmpcode & "', " & trfmtrmstoid & ", '" & transferno.Text & "', '" & transdate & "', '" & Tchar(note.Text.Trim) & "', '" & Tchar(noref.Text.Trim) & "', '" & trnstatus.Text & "', " & fromlocation.SelectedValue & ", " & tolocation.SelectedValue & ", CURRENT_TIMESTAMP, '" & upduser.Text & "', 'MS')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Update lastoid QL_matUsageMst
                sSql = "UPDATE QL_mstoid SET lastoid = " & trfmtrmstoid & " WHERE tablename = 'QL_matUsageMst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            Else

                sSql = "SELECT status FROM QL_matUsageMst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                Dim srest As String = xCmd.ExecuteScalar
                If srest Is Nothing Or srest = "" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer warehouse tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                    Exit Sub
                Else
                    If srest = "In Approval" Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Data transfer warehouse tidak dapat diubah !<br />Periksa bila data telah dalam status 'In Approval'!", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub
                    ElseIf srest = "Approved" Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Data transfer warehouse tidak dapat diubah !<br />Periksa bila data telah dalam status 'Approved'", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub
                    End If
                End If

                'Update master record
                sSql = "UPDATE QL_matUsageMst SET transferno = '" & transferno.Text & "', trfmtrnote = '" & Tchar(note.Text.Trim) & "', frommtrlocoid = " & fromlocation.SelectedValue & ", tomtrlocoid = " & tolocation.SelectedValue & ", noref = '" & Tchar(noref.Text.Trim) & "', status = '" & trnstatus.Text & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "' WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM QL_matUsageDtl WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0
                Dim qty_to As Double = 0.0

                For i As Integer = 0 To objTable.Rows.Count - 1

                    If i_u.Text = "edit" Then
                        trfmtrmstoid = Integer.Parse(Session("oid"))
                    End If

                    If objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan1") Then
                        unitseq = 1
                        qty_to = objTable.Rows(i).Item("qty") * objTable.Rows(i).Item("konversi1_2") * objTable.Rows(i).Item("konversi2_3")
                    ElseIf objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan2") Then
                        unitseq = 2
                        qty_to = objTable.Rows(i).Item("qty") * objTable.Rows(i).Item("konversi2_3")
                    ElseIf objTable.Rows(i).Item("satuan") = objTable.Rows(i).Item("satuan3") Then
                        unitseq = 3
                        qty_to = objTable.Rows(i).Item("qty")
                    End If

                    sSql = "INSERT INTO QL_matUsageDtl (cmpcode, trfmtrdtloid, trfmtrmstoid, seq, refoid, refname, unitseq, qty, unitoid, trfdtlnote, unitseq_to, qty_to, unitoid_to) VALUES ('" & cmpcode & "', " & trfmtrdtloid & ", " & trfmtrmstoid & ", " & i + 1 & ", " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & unitseq & ", " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan") & ", '" & Tchar(objTable.Rows(i).Item("note")) & "', 3, " & qty_to & ", " & objTable.Rows(i).Item("satuan3") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    If trnstatus.Text = "POST" Then

                        'Update crdmtr for item out
                        sSql = "UPDATE QL_crdmtr SET qtyout = qtyout + " & qty_to & ", saldoakhir = saldoakhir - " & qty_to & ", LastTransType = 'MS', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & cmpcode & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND refoid = " & objTable.Rows(i).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & cmpcode & "', " & conmtroid & ", 'MS', '" & transdate & "', '" & period & "', '" & transferno.Text & "', " & trfmtrdtloid & ", 'QL_matUsageDtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & fromlocation.SelectedValue & ", 0, " & objTable.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', 0)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        conmtroid += 1


                        'update stock inventory(-)( biaya(D)-persediaan(C))

                        '--------------------------
                        Dim lasthpp As Double = 0
                        sSql = "select hpp  from ql_mstitem where itemoid=" & objTable.Rows(i).Item("itemoid") & " "
                        xCmd.CommandText = sSql
                        lasthpp = xCmd.ExecuteScalar
                        '---------------------------

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & cmpcode & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_adjust_stock_minus & ", 'D', " & objTable.Rows(i).Item("qty") * ToDouble(lasthpp) & ", '" & transferno.Text & "', 'MATERIAL USAGE SERV', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        igldtl += 1
                        iGlSeq += 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & cmpcode & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_gudang & ", 'C', " & objTable.Rows(i).Item("qty") * ToDouble(lasthpp) & ", '" & transferno.Text & "', 'MATERIAL USAGE SERV', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        igldtl += 1
                        iGlSeq += 1



                    End If

                    trfmtrdtloid += 1

                Next

                'Update lastoid QL_matUsageDtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & trfmtrdtloid - 1 & "  WHERE tablename = 'QL_matUsageDtl' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Update lastoid QL_crdmtr
                If tempoid <> crdmtroid Then
                    sSql = "UPDATE QL_mstoid SET lastoid =" & crdmtroid - 1 & "  WHERE tablename = 'QL_crdmtr' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid = " & igldtl - 1 & " where tablename = 'QL_trngldtl' and cmpcode = '" & cmpcode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()


            End If

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            trnstatus.Text = "In Process"
            Exit Sub
        End Try

        Response.Redirect("trnMatUsage_Service.aspx?awal=true")
    End Sub

    Protected Sub unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unit.SelectedIndexChanged
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        conn.Open()

        sSql = "SELECT (a.saldoakhir-a.qtybooking) saldoakhir, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3 from QL_crdmtr a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' WHERE a.refoid = " & Integer.Parse(labelitemoid.Text) & " AND a.periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "' AND a.mtrlocoid = " & fromlocation.SelectedValue & " AND a.cmpcode = '" & cmpcode & "'"
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        Dim maxqty As Double = 0.0
        If xreader.HasRows Then
            While xreader.Read
                If unit.SelectedValue = xreader("satuan2") Then
                    maxqty = xreader("saldoakhir") / xreader("konversi2_3")
                ElseIf unit.SelectedValue = xreader("satuan1") Then
                    maxqty = xreader("saldoakhir") / xreader("konversi1_2") / xreader("konversi2_3")
                End If
            End While
        End If
        labelmaxqty.Text = Format(maxqty, "#,##0.00")

        conn.Close()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT status FROM QL_matUsageMst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transfer warehouse tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                Exit Sub
            Else
                If srest = "Approved" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer warehouse tidak dapat dihapus !<br />Periksa bila data telah berstatus 'Approved'!", 2)
                    Exit Sub
                ElseIf srest = "In Approval" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer warehouse tidak dapat dihapus !<br />Data sedang berada dalam status 'In Approval'!", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM QL_matUsageMst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_matUsageDtl WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from QL_approval where tablename = 'QL_matUsageMst' and oid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnMatUsage_Service.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        trnstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub fromlocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromlocation.SelectedIndexChanged

        'locasi kedua dibuat Nol
        FillDDL(tolocation, "SELECT 0,'Material Usage'")

        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        I_u2.Text = "new"
        labelitemoid.Text = ""
        qty.Text = "0.00"
        item.Text = ""
        labelmaxqty.Text = "0.00"
        Unit.Items.Clear()
        notedtl.Text = ""
        merk.Text = ""
        labelsatuan1.Text = ""
        labelsatuan2.Text = ""
        labelsatuan3.Text = ""
        labelunit1.Text = ""
        labelunit2.Text = ""
        labelunit3.Text = ""
        labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1"
        labelseq.Text = 1

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        sSql = "SELECT itemoid, itemcode, itemdesc FROM QL_mstitem WHERE cmpcode = '" & cmpcode & "' AND (itemcode LIKE '%" & Tchar(Filteritem.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(Filteritem.Text.Trim) & "%') AND itemflag = 'Aktif'"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "mstitem")
        GVItemSearch.DataSource = dtab
        GVItemSearch.DataBind()

        GVItemSearch.Visible = True
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        Filteritem.Text = ""
        itemoid.Text = ""

        GVItemSearch.DataSource = Nothing
        GVItemSearch.DataBind()

        GVItemSearch.Visible = False
    End Sub

    Protected Sub GVItemSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemSearch.PageIndexChanging
        sSql = "SELECT itemoid, itemcode, itemdesc FROM QL_mstitem WHERE cmpcode = '" & cmpcode & "' AND (itemcode LIKE '%" & Tchar(Filteritem.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(Filteritem.Text.Trim) & "%') AND itemflag = 'Aktif'"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "mstitem")
        GVItemSearch.DataSource = dtab
        GVItemSearch.PageIndex = e.NewPageIndex
        GVItemSearch.DataBind()
    End Sub

    Protected Sub GVItemSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemSearch.SelectedIndexChanged
        Filteritem.Text = GVItemSearch.Rows(GVItemSearch.SelectedIndex).Cells(2).Text
        itemoid.Text = GVItemSearch.SelectedDataKey("itemoid")

        GVItemSearch.DataSource = Nothing
        GVItemSearch.DataBind()

        GVItemSearch.Visible = False
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'showMessage("Print out not available yet !", 2)
            'Exit Sub

            Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")

            Dim sWhere As String = " " & Integer.Parse(labeloid.Text) & " "

            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "MSprintout.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", sWhere)
            'report.PrintOptions.PaperSize = PaperSize.PaperA5

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MS_" & labeloid.Text & "")

            report.Close() : report.Dispose()

            Response.Redirect("trnMatUsage_Service.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        If Not Session("tw") Is Nothing Then
            Dim dtab As DataTable = Session("tw")
            gvMaster.DataSource = dtab
            gvMaster.PageIndex = e.NewPageIndex
            gvMaster.DataBind()
        Else
            showMessage("Missing something !", 2)
            Exit Sub
        End If
    End Sub
#End Region

    Protected Sub btnApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnApproval.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        trnstatus.Text = "In Approval"
        btnSave_Click(Nothing, Nothing)
    End Sub
End Class
