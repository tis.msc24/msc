<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnoperteknisi.aspx.vb" Inherits="Transaction_trnoperteknisi" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tableutama" width="100%" class="header">
        <tr>
            <td>
                <table id="Table3" align="center" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th class="header" colspan="5" valign="top">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="navy" Text=".: Oper Teknisi" Width="217px">
                </asp:Label></th>
                    </tr>
                </table>
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Font-Bold="True">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 78px">Filter</TD><TD style="WIDTH: 1px; HEIGHT: 22px">:</TD><TD style="HEIGHT: 22px"><asp:DropDownList id="DDLFilter" runat="server" CssClass="inpText" __designer:wfdid="w432"><asp:ListItem Value="barcode">Barcode</asp:ListItem>
<asp:ListItem Value="reqcode">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="personname">Teknisi</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="TxtFilter" runat="server" CssClass="inpText" __designer:wfdid="w433"></asp:TextBox> <asp:ImageButton id="btnFind" onclick="btnFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w434"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnView" onclick="btnView_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w435"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 78px"><asp:CheckBox id="cbPeriode" runat="server" Text="Periode" __designer:wfdid="w436"></asp:CheckBox></TD><TD style="WIDTH: 1px">:</TD><TD><asp:TextBox id="periode1" runat="server" CssClass="inpText" __designer:wfdid="w437"></asp:TextBox>&nbsp;<asp:ImageButton id="ibPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w438"></asp:ImageButton> to <asp:TextBox id="periode2" runat="server" CssClass="inpText" __designer:wfdid="w439"></asp:TextBox>&nbsp;<asp:ImageButton id="ibPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w440"></asp:ImageButton> <asp:Label id="Label2" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w441"></asp:Label></TD></TR><TR><TD style="WIDTH: 78px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w442"></asp:CheckBox></TD><TD style="WIDTH: 1px">:</TD><TD><asp:DropDownList id="StatusDDL" runat="server" CssClass="inpText" __designer:wfdid="w443"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>Process</asp:ListItem>
<asp:ListItem>Switch</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD colSpan=3><asp:GridView id="gvOper" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w444" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="OPEROID" GridLines="None" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="operoid" DataNavigateUrlFormatString="~/Transaction/trnoperteknisi.aspx?oid={0}" DataTextField="barcode" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="reqcode" HeaderText="No. Tanda Terima">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="operdate" HeaderText="Tanggal Oper">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dari" HeaderText="Dari Teknisi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ke" HeaderText="Ke Teknisi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="operstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label6" runat="server" ForeColor="Red" Text="Data Not Found" __designer:wfdid="w6"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:dtid="281474976710684" __designer:wfdid="w445" Enabled="True" PopupButtonID="ibPeriode1" Format="dd/MM/yyyy" TargetControlID="periode1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:dtid="281474976710685" __designer:wfdid="w446" Enabled="True" PopupButtonID="ibPeriode2" Format="dd/MM/yyyy" TargetControlID="periode2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:dtid="281474976710687" __designer:wfdid="w447" Enabled="True" TargetControlID="periode1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureThousandsPlaceholder="" CultureDecimalPlaceholder="" CultureTimePlaceholder="" CultureDatePlaceholder=""></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w448" TargetControlID="periode2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <span><strong>List Oper Teknisi :</strong><span style="font-size: 10pt"><strong>.</strong></span></span>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <HeaderTemplate>
                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                <strong><span style="font-size: 10pt">
                Form Oper Teknisi</span></strong> <strong><span style="font-size: 10pt">:.</span></strong>
            </HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 122px"><asp:Label id="i_u" runat="server" ForeColor="Red" __designer:wfdid="w45" Visible="False"></asp:Label>&nbsp;</TD><TD></TD><TD colSpan=3><asp:Label id="lbloid" runat="server" __designer:wfdid="w46" Visible="False"></asp:Label></TD><TD style="WIDTH: 100px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 122px">Barcode <asp:Label id="Label8" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w47"></asp:Label></TD><TD>:</TD><TD colSpan=3><asp:TextBox id="barcode" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w48" MaxLength="15" OnTextChanged="barcode_TextChanged"></asp:TextBox><asp:Label id="reqoid" runat="server" __designer:wfdid="w49" Visible="False"></asp:Label>&nbsp;<asp:Label id="planoid" runat="server" __designer:wfdid="w50" Visible="False"></asp:Label></TD><TD style="WIDTH: 100px">Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DDLcabang" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w51"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 122px">No. Tanda Terima</TD><TD>:</TD><TD colSpan=3><asp:TextBox id="notanda" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w52" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibCarino" onclick="ibCarino_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w53"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibClearno" onclick="ibClearno_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w54"></asp:ImageButton></TD><TD style="WIDTH: 100px">Oper Type</TD><TD>:</TD><TD><asp:RadioButtonList id="operflag" runat="server" __designer:wfdid="w55" AutoPostBack="True" OnSelectedIndexChanged="operflag_SelectedIndexChanged" RepeatDirection="Horizontal"><asp:ListItem Selected="True" Value="In">Internal</asp:ListItem>
<asp:ListItem Value="Out">External</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD style="WIDTH: 122px">Customer</TD><TD>:</TD><TD colSpan=3><asp:TextBox id="cust" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w56" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 100px">Tanggal Oper <asp:Label id="Label10" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w57"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="tgloper" runat="server" CssClass="inpText" __designer:wfdid="w58" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibTglOper" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w59" Visible="False" Enabled="False"></asp:ImageButton> <asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w60"></asp:Label></TD></TR><TR><TD style="WIDTH: 122px">Dari Teknisi</TD><TD>:</TD><TD colSpan=3><asp:TextBox id="drteknisi" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w61" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="dari" runat="server" __designer:wfdid="w62" Visible="False"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Label id="lblketeknisi" runat="server" Width="73px" __designer:wfdid="w63">Ke Teknisi</asp:Label></TD><TD><asp:Label id="lblpetik2" runat="server" __designer:wfdid="w64">:</asp:Label></TD><TD><asp:TextBox id="keteknisi" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w65" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibCarike" onclick="ibCarike_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w66"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibClearke" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w67"></asp:ImageButton>&nbsp;<asp:Label id="ke" runat="server" __designer:wfdid="w68" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 122px">Alasan<asp:Label id="Label9" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w69"></asp:Label></TD><TD>:</TD><TD colSpan=6><asp:TextBox id="alasan" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w70" MaxLength="150" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label4" runat="server" Width="123px" ForeColor="Red" Text="maks. 150 karakter" __designer:wfdid="w71"></asp:Label></TD></TR><TR><TD style="WIDTH: 122px">Status</TD><TD>:</TD><TD colSpan=6><asp:TextBox id="status" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w72" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="lblstatus" runat="server" __designer:wfdid="w73" Visible="False"></asp:Label>&nbsp;</TD></TR><TR><TD colSpan=8><asp:Label id="lblUpd" runat="server" __designer:wfdid="w74"></asp:Label> by <asp:Label id="UpdUser" runat="server" __designer:wfdid="w75"></asp:Label> On <asp:Label id="UpdTime" runat="server" __designer:wfdid="w76"></asp:Label></TD></TR><TR><TD colSpan=8><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w77"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w78"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w79"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDel" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w80"></asp:ImageButton>&nbsp;<asp:Label id="post" runat="server" __designer:wfdid="w81" Visible="False"></asp:Label> <ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w82" PopupButtonID="ibTglOper" Format="dd/MM/yyyy" TargetControlID="tgloper"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w83" TargetControlID="tgloper" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
                <asp:UpdatePanel id="UpdatePanel4" runat="server">
                    <contenttemplate>
<asp:Panel id="panelno" runat="server" CssClass="modalBox" __designer:wfdid="w209" Visible="False" DefaultButton="ibFindno"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblDaftar" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Penerimaan Barang" __designer:wfdid="w210"></asp:Label></TD></TR><TR><TD>Filter : <asp:DropDownList id="DDLno" runat="server" CssClass="inpText" __designer:wfdid="w211"><asp:ListItem Value="reqcode">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="personname">Teknisi</asp:ListItem>
<asp:ListItem Value="sstatus">Status</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterNo" runat="server" CssClass="inpText" __designer:wfdid="w212"></asp:TextBox>&nbsp;<asp:ImageButton id="ibFindno" onclick="ibFindno_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w213"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibViewno" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w214"></asp:ImageButton></TD></TR><TR><TD><DIV style="OVERFLOW-Y: scroll; WIDTH: 600px; HEIGHT: 100px"><asp:GridView id="gvNo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w326" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="REQOID" GridLines="None" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="reqoid" HeaderText="Reqoid" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reqcode" HeaderText="No. Tanda Terima">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="160px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="160px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="barcode" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Teknisi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" ForeColor="Red" Text="Data Not Found" __designer:wfdid="w66"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center><asp:LinkButton id="lkbCloseno" onclick="lkbCloseno_Click" runat="server" __designer:wfdid="w216">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeNo" runat="server" __designer:wfdid="w217" TargetControlID="btnHideNo" DropShadow="True" BackgroundCssClass="modalBackground" PopupControlID="panelno" PopupDragHandleControlID="lblDaftar"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideNo" runat="server" __designer:wfdid="w218" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="UpdatePanel5" runat="server">
        <contenttemplate>
<asp:Panel id="panelke" runat="server" CssClass="modalBox" Visible="False" DefaultButton="ibFindOper"><TABLE style="WIDTH: 384px; HEIGHT: 56px"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblOper" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Oper Teknisi"></asp:Label></TD></TR><TR><TD colSpan=3>Filter : <asp:DropDownList id="DDLOper" runat="server" CssClass="inpText"><asp:ListItem Value="personnip">NIP</asp:ListItem>
<asp:ListItem Value="personname">Nama</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtOper" runat="server" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibFindOper" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w1"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibViewOper" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll"><asp:GridView id="gvKe" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w3" AutoGenerateColumns="False" DataKeyNames="personoid" GridLines="None" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="personnip" HeaderText="NIP">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Nama">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pengerjaan" HeaderText="Pengerjaan">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="Data Not Found" __designer:wfdid="w65"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseke" onclick="lkbCloseno_Click" runat="server" __designer:wfdid="w531">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeke" runat="server" TargetControlID="btnHideKe" BackgroundCssClass="modalBackground" PopupControlID="panelke" PopupDragHandleControlID="lblOper"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideKe" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
                <asp:UpdatePanel id="UpdatePanel3" runat="server">
                    <contenttemplate>
<asp:Panel id="panelMsg" runat="server" Width="264px" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w220"><TABLE style="WIDTH: 99%; HEIGHT: 109%"><TBODY><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w221"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle" __designer:wfdid="w222"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True" __designer:wfdid="w223"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server" __designer:wfdid="w224"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w225" OnClick="btnErrorOK_Click"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeValidasi" runat="server" __designer:wfdid="w226" DropShadow="True" BackgroundCssClass="modalBackground" PopupControlID="panelMsg" PopupDragHandleControlID="lblCaption" TargetControlID="btnValidasi"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnValidasi" runat="server" Visible="False" __designer:wfdid="w227"></asp:Button><BR />
</contenttemplate>
                </asp:UpdatePanel>
</asp:Content>

