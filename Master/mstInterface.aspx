<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstInterface.aspx.vb" Inherits="Master_Interface1" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Interface" CssClass="Title" Font-Size="X-Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="../Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image> <STRONG><SPAN style="FONT-SIZE: 9pt">List of Interface :.</SPAN></STRONG> 
</HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel2" runat="server"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w145" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w146"><asp:ListItem Value="interfacevar">Variable</asp:ListItem>
<asp:ListItem Value="interfacevalue">Selected COA No.</asp:ListItem>
<asp:ListItem Value="interfacenote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w147"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Branch</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDlBranch" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w148"></asp:DropDownList></TD></TR><TR><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:CheckBox id="cbType" runat="server" Text="Type" __designer:wfdid="w149"></asp:CheckBox></TD><TD id="TD1" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD3" class="Label" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="FilterDDLType" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w150">
                <asp:ListItem>Single COA</asp:ListItem>
                <asp:ListItem>Multi COA</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w151"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w152">
    <asp:ListItem>ACTIVE</asp:ListItem>
    <asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 27px" class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w153"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w154"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w155" DataKeyNames="interfaceoid" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="interfaceoid" DataNavigateUrlFormatString="~/Master/mstInterface.aspx?oid={0}" DataTextField="interfacevar" HeaderText="Variable" SortExpression="interfacevar">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="interfacevalue" HeaderText="COA No." SortExpression="interfacevalue">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="interfacenote" HeaderText="Note" SortExpression="interfacenote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="interfaceres2" HeaderText="Type" SortExpression="interfaceres2" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="Save As" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="There is no data have been saved before !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w156"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress21" runat="server" __designer:wfdid="w157" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w158"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
    <triggers>
<asp:PostBackTrigger ControlID="gvMst"></asp:PostBackTrigger>
</triggers>
</asp:UpdatePanel> 
</ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="I_U" runat="server" Width="93px" CssClass="Important" Text="New Data" __designer:wfdid="w27" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="interfaceoid" runat="server" __designer:wfdid="w28" Visible="False"></asp:Label><asp:Label id="interfaceres2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Red" __designer:wfdid="w29" Visible="False"></asp:Label><asp:Label id="interfacevalue" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Red" __designer:wfdid="w29" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="Akun COA" __designer:wfdid="w17"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Button id="BtnBindCOA" runat="server" CssClass="btn green" Text="Get Data COA" __designer:wfdid="w21"></asp:Button></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Variable" __designer:wfdid="w32"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="interfacevar" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w33" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w34"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w35">
        <asp:ListItem Value="ACTIVE">Active</asp:ListItem>
        <asp:ListItem Value="INACTIVE">Inactive</asp:ListItem>
    </asp:DropDownList><asp:DropDownList id="DDLBranch" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w16" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="Note" __designer:wfdid="w36"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="interfacenote" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w37" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" vAlign=top align=left colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 700px; HEIGHT: 155px; BACKGROUND-COLOR: beige" id="sCroll"><asp:GridView id="gvDataCoa" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w1" GridLines="None" CellPadding="4" AutoGenerateColumns="False" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="nomer" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Coa Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Font-Size="" ForeColor="Red" Text="No Detail Data!!"></asp:Label>
                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD id="TD6" class="Label" vAlign=top align=left runat="server" Visible="false"><asp:Label id="Label8" runat="server" Text="COA" __designer:wfdid="w38"></asp:Label></TD><TD id="TD5" class="Label" vAlign=top align=center runat="server" Visible="false">:</TD><TD id="TD4" class="Label" vAlign=top align=left runat="server" Visible="false"><asp:CheckBoxList id="cbldata" runat="server" Font-Bold="False" __designer:wfdid="w39" AutoPostBack="True" Font-Underline="False" RepeatColumns="2"></asp:CheckBoxList></TD></TR><TR><TD class="Label" align=left colSpan=3>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w40"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w41"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w42"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w43"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w44" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnSaveAs" runat="server" ImageUrl="~/Images/saveas.png" ImageAlign="AbsMiddle" __designer:wfdid="w45" AlternateText="Save As"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w46" AlternateText="Cancel"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w47" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w48"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV><asp:UpdatePanel id="upListCust" runat="server" __designer:dtid="562949953421324" __designer:wfdid="w1"><ContentTemplate __designer:dtid="562949953421325">
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" __designer:wfdid="w2" Visible="False" DefaultButton="BtnFindListCOA"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lbListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of COA" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListCust" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w4"><asp:ListItem Value="acctgcode">Code</asp:ListItem>
<asp:ListItem Value="acctgdesc">Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListCust" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w5"></asp:TextBox> <asp:ImageButton id="BtnFindListCOA" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton> <asp:ImageButton id="BtnAllListCOA" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="BtnSelectAllCOA" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnSelectNoneCOA" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedCOA" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCOA" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w11" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbListCust" runat="server" __designer:wfdid="w22" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("acctgoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="LbAddToListCOA" runat="server" CssClass="green" Font-Bold="True" ForeColor="White" __designer:wfdid="w12">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="LbCloseListCOA" runat="server" CssClass="red" Font-Bold="True" ForeColor="White" __designer:wfdid="w13">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" __designer:wfdid="w14" TargetControlID="btnHideListCust" Drag="True" PopupDragHandleControlID="lbListCust" BackgroundCssClass="modalBackground" PopupControlID="pnlListCust"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" __designer:wfdid="w15" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
                        <HeaderTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="../Images/corner.gif" ImageAlign="AbsMiddle"></asp:Image> <STRONG><SPAN style="FONT-SIZE: 9pt">Form Interface :.</SPAN></STRONG> 
</HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

