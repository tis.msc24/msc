<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
CodeFile="creditnote.aspx.vb" Inherits="creditnote" Title="Credit Note" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
 <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center" style="height: 34px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Credit Note" CssClass="Title" Font-Size="X-Large"></asp:Label></th>
                    </tr>
                </table>
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1X">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<DIV><asp:Panel id="Panel1s" runat="server" DefaultButton="btnSearch" __designer:wfdid="w123"><TABLE width="100%"><TBODY><TR><TD align=left>Cabang</TD><TD align=left colSpan=1>:</TD><TD align=left colSpan=4><asp:DropDownList id="DDLfiltercabang" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w131"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Filter </TD><TD align=left colSpan=1>:</TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w124"><asp:ListItem Value="no">CN No.</asp:ListItem>
<asp:ListItem Value="name">Customer/Supplier Name</asp:ListItem>
<asp:ListItem Value="note">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="235px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w125" MaxLength="30"></asp:TextBox></TD></TR><TR><TD align=left>Period </TD><TD align=left colSpan=1>:</TD><TD align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="75px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w126"></asp:TextBox>&nbsp;<asp:ImageButton id="sTgl1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w127"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="75px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w128"></asp:TextBox>&nbsp;<asp:ImageButton id="sTgl2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w129"></asp:ImageButton>&nbsp;<asp:Label id="Label9" runat="server" CssClass="Important" Font-Size="8pt" Text="(dd/MM/yyyy)" __designer:wfdid="w130"></asp:Label></TD></TR><TR><TD align=left>Type </TD><TD align=left colSpan=1>:</TD><TD align=left colSpan=4><asp:DropDownList id="tipene" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w132"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="AP">A/P</asp:ListItem>
<asp:ListItem Value="AR">A/R</asp:ListItem>
<asp:ListItem>NT</asp:ListItem>
<asp:ListItem>SRV</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Status</TD><TD align=left colSpan=1>:</TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w133"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>IN APPROVAL</asp:ListItem>
<asp:ListItem Value="APPROVED">APPROVED</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD align=left></TD><TD align=left colSpan=1></TD><TD align=left colSpan=4><asp:ImageButton id="btnsearch" onclick="btnsearch_Click" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w134"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnlist" onclick="btnlist_Click" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w135"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrintExcel" onclick="btnPrintExcel_Click" runat="server" ImageUrl="~/Images/printexport.gif" __designer:wfdid="w136"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 300px; BACKGROUND-COLOR: beige; TEXT-ALIGN: left"><asp:GridView id="GVmstgen" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w137" PageSize="8" GridLines="None" DataKeyNames="oid,cmpcode" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="GVmstgen_SelectedIndexChanged">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="no" HeaderText="CN. No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tgl" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Customer/Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invoice" HeaderText="No. Invoice">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invoiceAmt" HeaderText="Invoice Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="CN Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cnstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cmpcode" HeaderText="Code" Visible="False">
<ControlStyle BackColor="White" BorderColor="White" BorderStyle="None"></ControlStyle>

<FooterStyle BackColor="White" BorderColor="White" BorderStyle="None" ForeColor="White"></FooterStyle>

<HeaderStyle BackColor="White" BorderColor="White" BorderStyle="None" ForeColor="White"></HeaderStyle>

<ItemStyle BackColor="White" BorderColor="White" BorderStyle="None" ForeColor="White"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" CssClass="Important" Text="Data not found !!" __designer:wfdid="w104"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></DIV><asp:Label id="lblViewInfo" runat="server" Width="243px" Font-Bold="False" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w138"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> </DIV>
</ContentTemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrintExcel"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" height="16" />
                            <strong><span>List of Credit Note :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE><TBODY><TR><TD id="TD4" align=left runat="server"></TD><TD style="WIDTH: 8px" id="TD8" align=left runat="server"></TD><TD style="WIDTH: 318px" id="TD5" align=left runat="server"><asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" __designer:wfdid="w192" Visible="False"></asp:Label> <asp:DropDownList id="DDLOutlet" runat="server" Width="200px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w193" Visible="False" OnSelectedIndexChanged="DDLOutlet_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD id="TD6" align=left runat="server"><asp:Label id="CutofDate" runat="server" __designer:wfdid="w194" Visible="False"></asp:Label></TD><TD id="TD9" align=left runat="server"></TD><TD id="TD7" align=left runat="server"><asp:Label id="invoicedate" runat="server" __designer:wfdid="w195" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="lblDraftno" runat="server" Text="Draft No." __designer:wfdid="w196"></asp:Label></TD><TD style="WIDTH: 8px" align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:Label id="draft" runat="server" __designer:wfdid="w197"></asp:Label> <asp:TextBox id="cnno" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w198" MaxLength="15" Visible="False" size="20" Enabled="False"></asp:TextBox> <asp:Label id="refoid" runat="server" __designer:wfdid="w199" Visible="False"></asp:Label> <asp:Label id="cnoid" runat="server" __designer:wfdid="w200" Visible="False"></asp:Label> </TD><TD align=left><asp:Label id="Label7" runat="server" Text="Cabang" __designer:wfdid="w201"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="DDLcabang" runat="server" Width="176px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w202" OnSelectedIndexChanged="DDLcabang_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList> <asp:Label id="cabang" runat="server" __designer:wfdid="w203" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="lblAPARNo" runat="server" Width="85px" Text="Type" __designer:wfdid="w204"></asp:Label></TD><TD style="WIDTH: 8px" align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:DropDownList id="reftype" runat="server" Width="50px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w205" AutoPostBack="True"><asp:ListItem Value="AR">A/R</asp:ListItem>
<asp:ListItem Value="AP">A/P</asp:ListItem>
<asp:ListItem>NT</asp:ListItem>
<asp:ListItem>SRV</asp:ListItem>
</asp:DropDownList> <asp:Label id="cust_supp_oid" runat="server" __designer:wfdid="w206" Visible="False"></asp:Label></TD><TD style="WHITE-SPACE: nowrap" align=left>Date <asp:Label id="Label6" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w207"></asp:Label></TD><TD style="WHITE-SPACE: nowrap" align=left>:</TD><TD align=left><asp:TextBox id="tgl" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w208" MaxLength="10" AutoPostBack="True" Enabled="False" OnTextChanged="tgl_TextChanged"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w209" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Font-Size="8pt" Text="(dd/MM/yyyy)" __designer:wfdid="w210"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="lblsupp_cust" runat="server" Width="65px" Text="Supplier" __designer:wfdid="w211"></asp:Label> <asp:Label id="Label3" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w212"></asp:Label></TD><TD style="WIDTH: 8px" align=left>:</TD><TD align=left><asp:TextBox id="nama" runat="server" Width="231px" CssClass="inpText" __designer:wfdid="w213"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch_SC" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w214"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnclearSC" onclick="btnclearSC_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w215"></asp:ImageButton></TD><TD align=left>Currency</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="curroid" runat="server" Width="80px" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w216" Enabled="False"></asp:DropDownList> <asp:Label id="aparrateoid" runat="server" __designer:wfdid="w217" Visible="False"></asp:Label> <asp:Label id="aparrate2oid" runat="server" __designer:wfdid="w218" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD style="WIDTH: 8px" align=left></TD><TD align=left><asp:GridView id="gvSupplier" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w219" Visible="False" PageSize="8" GridLines="None" DataKeyNames="oid,name" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="40px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="40px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="code" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name">
<HeaderStyle CssClass="gvhdr" Width="300px"></HeaderStyle>

<ItemStyle Width="300px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No data found!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left>Invoice &nbsp;<asp:Label id="Label2" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w220"></asp:Label>&nbsp;</TD><TD style="WIDTH: 8px" align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="invno" runat="server" Width="197px" CssClass="inpText" __designer:wfdid="w221" MaxLength="30"></asp:TextBox> <asp:ImageButton id="btnSearchinv" onclick="btnSearchPurchasing_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w222"></asp:ImageButton> <asp:ImageButton id="btnClearinv" onclick="btnClearinv_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w223"></asp:ImageButton></TD><TD align=left>Amount <asp:Label id="Label10" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w224"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="amount" runat="server" Width="116px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w225" MaxLength="10" AutoPostBack="True" size="20" OnTextChanged="amount_TextChanged"></asp:TextBox>&nbsp;&nbsp;<asp:Label id="lblmaxpayment" runat="server" __designer:wfdid="w226">&lt;=</asp:Label> <asp:Label id="maxpayment" runat="server" __designer:wfdid="w227"></asp:Label></TD></TR><TR><TD align=left></TD><TD style="WIDTH: 8px" align=left></TD><TD align=left colSpan=4><asp:GridView id="gvPurchasing" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w228" PageSize="8" GridLines="None" DataKeyNames="refoid,invno,trndate,amttrans,amtpaid,acctgoid,payduedate,cmpcode,amtbalance,apaccount,currencyoid" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="gvPurchasing_SelectedIndexChanged" OnPageIndexChanging="gvPurchasing_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="invno" HeaderText="No. Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Jth Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Nota Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Total bayar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbalance" HeaderText="Balance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="nodata" runat="server" ForeColor="Red">Data Not Found!!</asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Debet COA</TD><TD style="WIDTH: 8px" align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:DropDownList id="coadebet" runat="server" Width="349px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w229"></asp:DropDownList></TD><TD align=left>Credit COA</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="coacredit" runat="server" Width="349px" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w230" Enabled="False"></asp:DropDownList></TD></TR><TR><TD align=left>Note</TD><TD style="WIDTH: 8px" align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="note" runat="server" Width="345px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w231" MaxLength="200" size="20" TextMode="MultiLine"></asp:TextBox></TD><TD vAlign=middle align=left><asp:Label id="Label8" runat="server" Text="Status" __designer:wfdid="w232"></asp:Label></TD><TD vAlign=middle align=left>:</TD><TD vAlign=middle align=left><asp:Label id="LblPosting" runat="server" CssClass="Important" Font-Size="8pt" Text="IN PROCESS" __designer:wfdid="w233"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left colSpan=6><asp:Label id="create" runat="server" Font-Bold="True" __designer:wfdid="w234"></asp:Label> <asp:Label id="update" runat="server" Font-Bold="True" __designer:wfdid="w235"></asp:Label></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="btnsave" onclick="btnsave_Click" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w236"></asp:ImageButton> <asp:ImageButton id="btncancel" onclick="btncancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w237"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w238" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnpost" onclick="btnpost_Click" runat="server" ImageUrl="~/Images/sendapproval.png" __designer:wfdid="w239"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" __designer:wfdid="w240"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w241" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w242"></asp:Image> Please Wait ..... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" height="16" /> <strong><span>
                                Form of Credit Note :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" __designer:wfdid="w48" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting" __designer:wfdid="w49"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" __designer:wfdid="w50" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Perkiraan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Bold="True" __designer:wfdid="w51">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" __designer:wfdid="w52" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting2" Drag="True" TargetControlID="btnHidePosting2">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" __designer:wfdid="w53" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
