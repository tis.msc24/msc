Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_DailyCashReport
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLAcctgWithAll(ByRef oDDLObject As DropDownList, ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sNoneValue As String = "", Optional ByVal sNoneText As String = "") As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillDDLAcctgWithAll = True
        oDDLObject.Items.Clear()

        Dim sCode As String = ""
        Dim sSql As String = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1<>'MSC' " & IIf(sCmpCode <> "%", " AND interfaceres1='" & sCmpCode & "'", "")
        Dim dtCode As DataTable = GetDataTable(sSql, "QL_mstinterface")
        If dtCode.Rows.Count < 1 Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceres1='MSC' AND interfacevar='" & sVar & "'"
            sCode = GetStrData(sSql)
        Else
            For C0 As Integer = 0 To dtCode.Rows.Count - 1
                sCode += (dtCode.Rows(C0)("interfacevalue").ToString & ",")
            Next
            sCode = Left(sCode, sCode.Length - 1)
        End If

        If sCode <> "?" Then
            sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
            If sNoneText <> "" And sNoneValue <> "" Then
                FillDDLWithAdditionalText(oDDLObject, sSql, sNoneText, sNoneValue)
            Else
                FillDDL(oDDLObject, sSql)
            End If
        End If
        If oDDLObject.Items.Count = 0 Then
            FillDDLAcctgWithAll = False
        End If
        Return FillDDLAcctgWithAll
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvAcctg.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvAcctg.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "acctgoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyCode
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        crvReportForm.ReportSource = Nothing
        Try
            If DDLType.SelectedValue = "Summary" Then
				If FilterCurrency.SelectedValue.ToUpper <> "VALAS" Then
					If sType = "Print Excel" Then
                        report.Load(Server.MapPath(folderReport & "rptDailyCash_xls.rpt"))
					Else
						report.Load(Server.MapPath(folderReport & "rptDailyCash.rpt"))
					End If
                End If

                Dim sPeriod As String = "" : Dim sAnd As String = ""
                Dim acctgoid As String = ""
                Dim sActgoid As String = ""
                Dim arCode() As String = AcctgName.Text.Split(";")
                Dim sCodeIn As String = "" : Dim sQel As String = ""
                Dim adr As String = ""
                For C1 As Integer = 0 To arCode.Length - 1
                    If arCode(C1) <> "" Then
                        sCodeIn &= "'" & arCode(C1) & "',"
                    End If
                Next

                If sCodeIn <> "" Then
                    acctgoid &= " AND oid IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                    sActgoid &= " Where a.acctgoid IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                Else
                    showMessage("Silahkan pilih account terlebih dahulu!", 2)
                    Exit Sub
                End If
                Dim sWhere As String = "WHERE CompCode='" & CompnyCode & "' AND branch_code LIKE '" & FilterDDLDiv.SelectedValue & "'"

                'Dim iAcctgOid As Integer = CInt(FilterDDLAccount.SelectedValue)
                If IsValidPeriod() Then
                    sPeriod = Format(CDate(toDate(FilterPeriod1.Text)), "dd MMMM yyyy") & " s/d " & Format(CDate(toDate(FilterPeriod2.Text)), "dd MMMM yyyy")
                    sWhere &= " AND Tanggal BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & "' AND '" & CDate(toDate(FilterPeriod2.Text)) & "'"
                    sAnd &= " where cb.branch_code LIKE '" & FilterDDLDiv.SelectedValue & "' " & acctgoid & " and cb.Tanggal <=DATEADD(DAY,-1,'" & CDate(toDate(FilterPeriod1.Text)) & "') AND oid = a.acctgoid"
                Else
                    Exit Sub
                End If
                Dim sWhereBranch As String = sActgoid
                report.SetParameterValue("sBranch", FilterDDLDiv.SelectedValue)
                report.SetParameterValue("sTgl", CDate(toDate(FilterPeriod1.Text)))
                report.SetParameterValue("sCompnyName", FilterDDLDiv.SelectedItem.Text)
                report.SetParameterValue("sPeriod", sPeriod)
                report.SetParameterValue("sAcctgoid", sActgoid)
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("sWhereBranch", sWhereBranch)
                'report.SetParameterValue("iAcctgOid", iAcctgOid)
                report.SetParameterValue("sAnd", sAnd)
                report.SetParameterValue("sCurrency", FilterCurrency.SelectedValue)
                report.SetParameterValue("cmpcode", "MSC")
                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                If FilterCurrency.SelectedValue.ToUpper = "VALAS" Then
                    report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                Else
                    report.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                End If
            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptDailyCashDtl_xls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptDailyCashDtl.rpt"))
                End If
                Dim sAnd As String = ""
                Dim sPeriod As String = ""
                Dim acctgoid As String = ""
                Dim sActgoid As String = ""
                Dim arCode() As String = AcctgName.Text.Split(";")
                Dim sCodeIn As String = "" : Dim sQel As String = ""
                Dim adr As String = ""
                For C1 As Integer = 0 To arCode.Length - 1
                    If arCode(C1) <> "" Then
                        sCodeIn &= "'" & arCode(C1) & "',"
                    End If
                Next

                If sCodeIn <> "" Then
                    acctgoid &= " AND cb.cashbankacctgoid IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                    sActgoid &= " Where a.acctgoid IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"

                Else
                    showMessage("Silahkan pilih account terlebih dahulu!", 2)
                    Exit Sub
                End If
                Dim sWhere As String = " WHERE cb.cmpcode='" & CompnyCode & "' and cb.branch_code like '" & FilterDDLDiv.SelectedValue & "' " & acctgoid & " "

                If IsValidPeriod() Then
                    sPeriod = Format(CDate(toDate(FilterPeriod1.Text)), "dd MMMM yyyy") & " s/d " & Format(CDate(toDate(FilterPeriod2.Text)), "dd MMMM yyyy")
                    sWhere &= " AND cb.Tanggal BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & "' AND '" & CDate(toDate(FilterPeriod2.Text)) & "'"
                    sAnd &= " where cb.branch_code like '" & FilterDDLDiv.SelectedValue & "' and cb.Tanggal <=DATEADD(DAY,-1,'" & CDate(toDate(FilterPeriod1.Text)) & "') " & acctgoid & " and cb.cashbankacctgoid = a.acctgoid "
                Else
                    Exit Sub
                End If
                Dim sTgl As String = CDate(toDate(FilterPeriod1.Text))
                Dim sWhereBranch As String = sActgoid
                report.SetParameterValue("sBranch", FilterDDLDiv.SelectedValue)
                report.SetParameterValue("sTgl", CDate(toDate(FilterPeriod1.Text)))
                report.SetParameterValue("sPeriod", sPeriod)
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("sWhereBranch", sWhereBranch)
                report.SetParameterValue("sAnd", sAnd)
                'report.SetParameterValue("iAcctgOid", ToInteger(FilterDDLAccount.SelectedValue))
                report.SetParameterValue("cmpcode", CompnyCode)
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("PrintUserName", GetStrData("SELECT username profname FROM QL_mstprof WHERE userid='" & Session("UserID") & "'"))
                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                report.PrintOptions.PaperSize = PaperSize.PaperA4
            End If

            If sType = "View" Then
                Session("ViewReport") = "True"
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DAILYCASHREPORT_" & Format(CDate(toDate(FilterPeriod1.Text)), "ddMMMyy").ToUpper & "_" & Format(CDate(toDate(FilterPeriod2.Text)), "ddMMMyy").ToUpper)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DAILYCASHREPORT_" & Format(CDate(toDate(FilterPeriod1.Text)), "ddMMMyy").ToUpper & "_" & Format(CDate(toDate(FilterPeriod2.Text)), "ddMMMyy").ToUpper)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FilterDDLDiv, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FilterDDLDiv, sSql)
            Else
                FillDDL(FilterDDLDiv, sSql)
                FilterDDLDiv.Items.Add(New ListItem("ALL", "%"))
                FilterDDLDiv.SelectedValue = "%"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(FilterDDLDiv, sSql)
            FilterDDLDiv.Items.Add(New ListItem("ALL", "%"))
            FilterDDLDiv.SelectedValue = "%"
        End If
    End Sub

    Public Sub BindDataListAcctg(ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sNoneValue As String = "")
        Try
            Dim cBrn As String = ""
            If FilterDDLDiv.SelectedValue.ToUpper <> "ALL" Then
                cBrn = "AND gl.branch_code='" & FilterDDLDiv.SelectedValue & "'"
            End If
            sSql = "SELECT DISTINCT 'False' AS Checkvalue, a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.acctgflag='A' AND a.acctgoid IN (SELECT gl.acctgoid FROM QL_trngldtl gl WHERE a.acctgoid=gl.acctgoid AND a.cmpcode=gl.cmpcode " & cBrn & ")"
            Dim sCode As String = ""
            sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1<>'MSC' " & IIf(sCmpCode <> "%", " AND interfaceres1='" & sCmpCode & "'", "")
            Dim dtCode As DataTable = GetDataTable(sSql, "QL_mstinterface")
            If dtCode.Rows.Count < 1 Then
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceres1='MSC' AND interfacevar='" & sVar & "'"
                sCode = GetStrData(sSql)
            Else
                For C0 As Integer = 0 To dtCode.Rows.Count - 1
                    sCode += (dtCode.Rows(C0)("interfacevalue").ToString & ",")
                Next
                sCode = Left(sCode, sCode.Length - 1)
            End If

            If sCode <> "?" Then
                sSql = "SELECT DISTINCT 'False' AS Checkvalue, a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND ("
                Dim sSplitCode() As String = sCode.Split(",")
                For C1 As Integer = 0 To sSplitCode.Length - 1
                    sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                    If C1 < sSplitCode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgcode"
            End If
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
            Session("TblMat") = dt
            Session("TblMatView") = Session("TblMat")
            gvAcctg.DataSource = Session("TblMatView")
            gvAcctg.DataBind() : gvAcctg.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("branch_id") = branchId
            Session("branch") = branch
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\rptCash.aspx")
        End If
        If checkPagePermission("~\ReportForm\rptCash.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        Page.Title = CompnyName & " - Daily Cash Report"
        If Not Page.IsPostBack Then
            InitDDLBranch()
            'FilterDDLDiv_SelectedIndexChanged(Nothing, Nothing)

            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        Else
            If Session("ViewReport") = "True" Then
                ShowReport("View")
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Session("ViewReport") = "False"
        Response.Redirect("~\ReportForm\rptCash.aspx?awal=true")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        If DDLType.SelectedValue = "Summary" Then
            lbCurr.Visible = True : lbSeptCurr.Visible = True : FilterCurrency.Visible = True
        Else
            lbCurr.Visible = False : lbSeptCurr.Visible = False : FilterCurrency.Visible = False
        End If
    End Sub

#End Region

    Protected Sub btnSearchAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchAcctg.Click
        FilterDDLListAcctg.SelectedIndex = 0
        FilterTextListAcctg.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvAcctg.DataSource = Nothing : gvAcctg.DataBind()
        BindDataListAcctg("VAR_CASH", IIf(FilterDDLDiv.SelectedValue = "ALL", "MSC", FilterDDLDiv.SelectedValue))
    End Sub

    Protected Sub btnEraseAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseAcctg.Click
        acctgoid.Text = ""
        AcctgName.Text = ""
    End Sub

    Protected Sub btnFindListAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListAcctg.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListAcctg.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListAcctg.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next

            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvAcctg.DataSource = Session("TblMatView")
                gvAcctg.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Data Account tidak ditemukan!"
                showMessage("Maaf, Data Account yang anda cari tidak ditemukan!", 2)
                mpeListMat.Show()
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListAcctg.Click
        FilterDDLListAcctg.SelectedIndex = 0
        FilterTextListAcctg.Text = ""
        If UpdateCheckedMat() Then
            BindDataListAcctg("VAR_CASH", IIf(FilterDDLDiv.SelectedValue = "ALL", "MSC", FilterDDLDiv.SelectedValue))
            Session("TblMatView") = Session("TblMat")
            gvAcctg.DataSource = Session("TblMatView")
            gvAcctg.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "acctgoid=" & dtTbl.Rows(C1)("acctgoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                Session("TblMat") = objView.ToTable
                Session("TblMatView") = dtTbl
                gvAcctg.DataSource = Session("TblMatView")
                gvAcctg.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Silahkan tampilkan data account terlebih dahulu!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "acctgoid=" & dtTbl.Rows(C1)("acctgoid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                Session("TblMat") = objView.ToTable
                Session("TblMatView") = dtTbl
                gvAcctg.DataSource = Session("TblMatView")
                gvAcctg.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Silahkan tampilkan data account terlebih dahulu!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub gvAcctg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAcctg.PageIndexChanging
        gvAcctg.PageIndex = e.NewPageIndex
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListAcctg.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListAcctg.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvAcctg.DataSource = Session("TblMatView")
                gvAcctg.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                showMessage("Data Account tidak ditemukan!", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbAddToListAcctg_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "Checkvalue=True"
        If dv.Count > 0 Then

            If AcctgName.Text <> "" Then
                For C1 As Integer = 0 To dv.Count - 1
                    AcctgName.Text &= ";" & dv(C1)("acctgoid").ToString & ";"
                Next
            Else
                For C1 As Integer = 0 To dv.Count - 1
                    AcctgName.Text &= dv(C1)("acctgoid").ToString & ";"
                Next
            End If


            If AcctgName.Text <> "" Then
                AcctgName.Text = Left(AcctgName.Text, AcctgName.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            showMessage("- Maaf, Silahkan pilih Account, dengan beri centang pada kolom pilih!<BR>", 2)
        End If
    End Sub

    Protected Sub lbCloseListAcctg_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub
End Class
