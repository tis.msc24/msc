<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="SPK_Close.aspx.vb" Inherits="SPK_Close"
title="MSC - SPK Closing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">


<table class="tabelhias" width="100%">
<tr>
<th class="header">
    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
        ForeColor="Maroon" Text=".: Closing Penerimaan  SPK "></asp:Label></th>
</tr>
</table>

<div style="text-align:left;">
    <br />
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Font-Bold="True"
        Font-Size="9pt">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
<asp:UpdatePanel ID="updPanel1" runat="server">
    <contenttemplate>
<TABLE id="TABLE1" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD colSpan=2><ajaxToolkit:CalendarExtender id="ceAwal" runat="server" __designer:wfdid="w17" Format="dd/MM/yyyy" PopupButtonID="btnTglAwal" TargetControlID="tglAwal">
                </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceAkhir" runat="server" __designer:wfdid="w18" Format="dd/MM/yyyy" PopupButtonID="btnTglAkhir" TargetControlID="tglAkhir">
                </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w19" TargetControlID="tglAwal" Mask="99/99/9999" MaskType="Date" CultureName="id-ID" UserDateFormat="DayMonthYear">
                </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w20" TargetControlID="tglAkhir" Mask="99/99/9999" MaskType="Date" CultureName="id-ID" UserDateFormat="DayMonthYear">
                </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="WIDTH: 137px">Filter</TD><TD><asp:DropDownList id="ddlFilter" runat="server" Width="102px" CssClass="inpText" __designer:wfdid="w21" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Selected="True">Barcode</asp:ListItem>
<asp:ListItem Value="reqcode">Penerimaan No</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="136px" CssClass="inpText" MaxLength="30" __designer:wfdid="w22"></asp:TextBox>&nbsp;&nbsp;<asp:Button id="btnFind" onclick="btnFind_Click" runat="server" Width="51px" CssClass="btn orange" Text="Find" __designer:wfdid="w23" UseSubmitBehavior="False"></asp:Button>&nbsp;<asp:Button id="btnViewAll" onclick="btnViewAll_Click" runat="server" CssClass="btn gray" Text="View All" __designer:wfdid="w24" UseSubmitBehavior="False"></asp:Button></TD></TR><TR><TD style="WIDTH: 137px">Periode</TD><TD><asp:TextBox id="tglAwal" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w25"></asp:TextBox>&nbsp;<asp:ImageButton id="btnTglAwal" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> to&nbsp; <asp:TextBox id="tglAkhir" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w27"></asp:TextBox>&nbsp; <asp:ImageButton id="btnTglAkhir" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w29"></asp:Label></TD></TR><TR><TD style="WIDTH: 137px"></TD><TD vAlign=bottom height=30>&nbsp; </TD></TR><TR><TD colSpan=2 height=20></TD></TR><TR><TD colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvClosedBPM" runat="server" Width="90%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w15" OnSelectedIndexChanged="gvClosedBPM_SelectedIndexChanged" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." GridLines="None" OnPageIndexChanging="gvClosedBPM_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="spmno" HeaderText="No Penerimaan.">
<HeaderStyle CssClass="gvhdr" Width="130px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spmdateclosed" HeaderText="Tanggal Closed">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="machine" HeaderText="Barcode">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spmuserclosed" HeaderText="User Closed">
<HeaderStyle CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spmnoteclosed" HeaderText="Note Closed">
<HeaderStyle CssClass="gvhdr" Width="170px"></HeaderStyle>

<ItemStyle Width="170px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spmstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle Width="60px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:GridView id="gvMR2" runat="server" Width="150px" __designer:wfdid="w108" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="spkno" HeaderText="SPK">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                            <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR><TD style="HEIGHT: 15px" colSpan=2></TD></TR></TBODY></TABLE>
</contenttemplate>
</asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <img align="absMiddle" alt="" src="../Images/corner.gif" />
                <strong><span style="font-size: 9pt">
                :: List of Closed Penerimaan SPK&nbsp;</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 2px; HEIGHT: 15px" id="TD1" runat="server" visible="False">Filter</TD><TD style="HEIGHT: 15px" id="TD2" colSpan=2 runat="server" visible="False"><asp:TextBox id="txtFilter2" runat="server" Width="136px" CssClass="inpText" MaxLength="30" __designer:wfdid="w54"></asp:TextBox> </TD></TR><TR><TD style="WIDTH: 2px; HEIGHT: 15px"><asp:Label id="Label4" runat="server" Text="Periode" __designer:wfdid="w55" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" colSpan=2><asp:TextBox id="tglAwal2" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w56" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnTglAwal2" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w57" Visible="False"></asp:ImageButton>&nbsp;&nbsp; <asp:TextBox id="tglAkhir2" runat="server" Width="78px" CssClass="inpText" __designer:wfdid="w58" Visible="False"></asp:TextBox>&nbsp; <asp:ImageButton id="btnTglAkhir2" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w59" Visible="False"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w60" Visible="False"></asp:Label> <BR /><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w61" Enabled="True" Format="dd/MM/yyyy" PopupButtonID="btnTglAwal2" TargetControlID="tglAwal2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w62" Enabled="True" Format="dd/MM/yyyy" PopupButtonID="btnTglAkhir2" TargetControlID="tglAkhir2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="WIDTH: 2px; HEIGHT: 15px">Penerimaan&nbsp;No.</TD><TD style="HEIGHT: 15px" colSpan=2><asp:TextBox id="txtBPMNo" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w63"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchBPM" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w64"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseBPM" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w65"></asp:ImageButton> <asp:Label id="bpmoid" runat="server" __designer:wfdid="w66" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 2px; HEIGHT: 15px"><asp:Label id="Label3" runat="server" Text="Note" __designer:wfdid="w67"></asp:Label>&nbsp;<SPAN style="COLOR: #ff0033">*</SPAN></TD><TD style="HEIGHT: 15px" colSpan=2><asp:TextBox id="spmnoteclosed" runat="server" Width="271px" CssClass="inpText" MaxLength="130" __designer:wfdid="w68"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 2px; HEIGHT: 15px">Closing Type</TD><TD style="HEIGHT: 15px" colSpan=2><asp:RadioButtonList id="rbtmutation" runat="server" Width="201px" Height="19px" __designer:wfdid="w69" RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged"><asp:ListItem Selected="True">Closed</asp:ListItem>
<asp:ListItem Value="Internal">Mutasi Internal</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD style="WIDTH: 2px"></TD><TD colSpan=2><asp:GridView id="gvBPM2" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w70" DataKeyNames="spmoid,spmno" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="spmno" HeaderText="Penerimaan No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="spmdate" HeaderText="Tanggal Penerimaan">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="machine" HeaderText="Barcode">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="spmstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:GridView id="gvMR" runat="server" Width="350px" __designer:wfdid="w296" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="spkno" HeaderText="SPK">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkstatus" HeaderText="Status">
<HeaderStyle Width="110px"></HeaderStyle>

<ItemStyle Width="110px"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Width="250px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 2px"></TD><TD colSpan=2><asp:Button id="btnCloseBPM" runat="server" CssClass="btn green" Text="Close Penerimaan" __designer:wfdid="w71"></asp:Button>&nbsp; <asp:Button id="btncancel" runat="server" CssClass="btn gray" Text="Cancel" __designer:wfdid="w72"></asp:Button></TD></TR></TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <strong><span style="font-size: 9pt">
                <img align="absMiddle" alt="" src="../Images/corner.gif" />
                :: Form Closing Penerimaan SPK </span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer><br />
    <br />
    <asp:UpdatePanel id="updPanel2" runat="server">
        <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" __designer:wfdid="w120" Visible="False"><TABLE><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="captionPesan" runat="server" Font-Size="Small" Font-Bold="True" Text="header" __designer:wfdid="w121"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px" colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" ImageUrl="~/Images/warn.png" __designer:wfdid="w122"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="isiPesan" runat="server" ForeColor="Red" __designer:wfdid="w123"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:Button id="btnErrOK" onclick="btnErrOK_Click" runat="server" CssClass="btn red" Text=" OK " __designer:wfdid="w124" UseSubmitBehavior="False"></asp:Button> </TD></TR></TBODY></TABLE></asp:Panel>&nbsp; <ajaxToolkit:ModalPopupExtender id="mpeMsg" runat="server" TargetControlID="PanelValidasi" __designer:wfdid="w125" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panelvalidasi" PopupDragHandleControlID="Validasi"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtenderValidasi" runat="server" __designer:wfdid="w126" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>

