Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptSalesRetur
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim CKon As New Koneksi
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim sSql As String = ""
#End Region

#Region "Function"
    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                dtView2.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblDO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtTbl2 As DataTable = Session("TblDOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                dtView2.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblDO") = dtTbl
                Session("TblDOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedReg() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblReg") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblReg")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListReg.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListReg.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "registermstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblReg") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedReg2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblReg")
            Dim dtTbl2 As DataTable = Session("TblRegView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListReg.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListReg.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "registermstoid=" & cbOid
                                dtView2.RowFilter = "registermstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblReg") = dtTbl
                Session("TblRegView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = CKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(CDate(toDate(range1.Text)), "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(CDate(toDate(range2.Text)), "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(toDate(range1.Text)) > CDate(toDate(range2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedure"

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub initDLL()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLcabang, sSql)
            Else
                FillDDL(DDLcabang, sSql)
                DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
                DDLcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLcabang, sSql)
            DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
            DDLcabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub showPrint(ByVal tipe As String)
        Try
            Dim sWhere As String = ""

            If ddltype.SelectedValue = "Konsinyasi" Then
                If DDLPeriode.SelectedValue = "dom.trnjualdate" Then
                    sWhere = "Where j.trnjualdate Between '" & CDate(toDate(range1.Text)) & " 00:00' And '" & CDate(toDate(range2.Text)) & " 23:59'"
                Else
                    sWhere = "Where a.trnjualdate Between '" & CDate(toDate(range1.Text)) & " 00:00' And '" & CDate(toDate(range2.Text)) & " 23:59'"
                End If 
            Else
                If DDLPeriode.SelectedValue = "dom.trnjualdate" Then
                    sWhere = "Where j.trnjualdate Between '" & CDate(toDate(range1.Text)) & " 00:00' And '" & CDate(toDate(range2.Text)) & " 23:59'"
                Else
                    sWhere = "Where a.trnjualdate Between '" & CDate(toDate(range1.Text)) & " 00:00' And '" & CDate(toDate(range2.Text)) & " 23:59'"
                End If
            End If

            sWhere &= IIf(DDLcabang.SelectedValue = "ALL", " ", " And a.branch_code='" & DDLcabang.SelectedValue & "'")

            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sSql &= " cust.custcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
                sWhere &= sSql
            End If

            If sono.Text <> "" Then
                Dim sSono() As String = Split(sono.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " a.trnjualreturno LIKE '%" & Tchar(sSono(c1)) & "%'"
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
                sWhere &= sSql
            End If

            If dono.Text <> "" Then
                Dim sDono() As String = Split(dono.Text, ";")
                If ddltype.SelectedValue = "Konsinyasi" Then
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sDono.Length - 1
                        sSql &= " j.trnjualno LIKE '%" & Tchar(sDono(c1)) & "%'"
                        If c1 < sDono.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                ElseIf ddltype.SelectedValue = "DtlKonsi" Then
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sDono.Length - 1
                        sSql &= " j.trnjualno LIKE '%" & Tchar(sDono(c1)) & "%'"
                        If c1 < sDono.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                Else
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sDono.Length - 1
                        sSql &= " a.refnotaretur LIKE '%" & Tchar(sDono(c1)) & "%'"
                        If c1 < sDono.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
                sWhere &= sSql
            End If

            If regno.Text <> "" Then
                Dim sregno() As String = Split(regno.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sregno.Length - 1
                    sSql &= " p.PERSONNAME LIKE '%" & Tchar(sregno(c1)) & "%'"
                    If c1 < sregno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
                sWhere &= sSql
            End If

            If ddltype.SelectedValue = "Konsinyasi" Then
                If DDLAmtRetur.SelectedValue <> "ALL" Then
                    sWhere &= " AND j.amtreturidr " & DDLAmtRetur.SelectedValue & ""
                End If
            ElseIf ddltype.SelectedValue = "DtlKonsi" Then
                If DDLAmtRetur.SelectedValue <> "ALL" Then
                    sWhere &= " AND j.amtreturidr " & DDLAmtRetur.SelectedValue & ""
                End If
            End If

            If ddltype.SelectedValue <> "Summary" Then
                If itemcode.Text <> "" Then
                    Dim sMatcode() As String = Split(itemcode.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sMatcode.Length - 1
                        sSql &= " d.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                        If c1 < sMatcode.Length - 1 Then
                            sSql &= " OR"
                        End If
                    Next
                    sSql &= ")"
                End If
                sWhere &= sSql
            End If

            If ddltype.SelectedValue = "Summary" Then
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptSalesReturSumEXL.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptSalesReturSum.rpt"))
                End If
            ElseIf ddltype.SelectedValue = "Detail" Then
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptSalesReturDtlEXL.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptSalesReturDtl.rpt"))
                End If
            ElseIf ddltype.SelectedValue = "Konsinyasi" Then
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptSalesReturKonsiExlSum.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptSalesReturKonsiSum.rpt"))
                End If
            Else
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptSalesReturKonsiEXL.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptSalesReturKonsi.rpt"))
                End If
            End If

            CProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("periode", range1.Text & "-" & range2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            If tipe = "" Then
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ReturJual_Sum_" & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ReturJual_Dtl_" & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If

        Catch ex As Exception
            vReport.Close()
            vReport.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub BindListCust()
        sSql = "SELECT 'False' AS checkvalue, gendesc, custoid, custcode, custname, custaddr FROM QL_mstcust c INNER JOIN QL_mstgen b ON b.gencode=c.branch_code AND b.gengroup='CABANG' WHERE c.cmpcode='" & CompnyCode & "'"
        If DDLcabang.SelectedValue <> "ALL" Then
            sSql &= " AND c.branch_code='" & DDLcabang.SelectedValue & "'"
        End If
        sSql &= " AND c.custoid IN (Select db_accessadmin=sj.trncustoid from QL_trnjualreturmst sj Where sj.branch_code=c.branch_code "
        If range1.Text <> "" And range2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND sj.trnjualdate>='" & CDate(toDate(range1.Text)) & " 00:00:00' AND sj.trnjualdate<='" & CDate(toDate(range2.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= ") ORDER BY custcode"
        Session("TblCust") = cKon.ambiltabel(sSql, "QL_mstCust")
    End Sub

    Private Sub BindListSO()
        Try
            sSql = "SELECT DISTINCT 'False' AS checkvalue, som.trnjualreturmstoid soitemmstoid, som.trnjualreturno soitemno, som.trnjualdate soitemdate, CONVERT(VARCHAR(10), som.trnjualdate, 101) AS sodate, som.trnjualstatus soitemmststatus, som.trnjualnote soitemmstnote, cb.gendesc cabang, dom.trnjualno FROM QL_trnjualreturmst som INNER JOIN QL_mstcust c ON c.custoid=som.trncustoid AND c.branch_code=som.branch_code Inner Join QL_mstgen cb ON cb.gencode=som.branch_code AND cb.gengroup='CABANG' INNER JOIN QL_trnjualmst dom ON dom.trnjualmstoid=som.trnjualmstoid AND dom.branch_code=som.branch_code Where som.trnjualstatus='Approved'"
            If DDLcabang.SelectedValue <> "ALL" Then
                sSql &= " AND som.branch_code ='" & DDLcabang.SelectedValue & "'"
            End If

            If ddltype.SelectedValue = "Konsinyasi" Then
                sSql &= " AND som.orderno IN (Select orderno from QL_trnordermst om Where typeSO='" & ddltype.SelectedValue & "' AND om.branch_code=som.branch_code)"
            End If

            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If

            If range1.Text <> "" And range2.Text <> "" Then
                If IsValidPeriod() Then
                  sSql &= " AND " & DDLPeriode.SelectedValue & ">='" & CDate(toDate(range1.Text)) & " 00:00:00' AND dom.trnjualdate<='" & CDate(toDate(range2.Text)) & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
            sSql &= " ORDER BY som.trnjualdate DESC, som.trnjualreturmstoid DESC"
            Session("TblSO") = CKon.ambiltabel(sSql, "QL_soitemmst")
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub BindListDO()
        Dim Join As String = ""
        If ddltype.SelectedValue = "Konsinyasi" Then
            Join = "LEFT"
        Else
            Join = "INNER"
        End If
        sSql = "SELECT DISTINCT 'False' AS checkvalue, dom.trnjualmstoid doitemmstoid, dom.trnjualno doitemno, dom.trnjualdate doitemdate, CONVERT(VARCHAR(10), dom.trnjualdate, 101) AS dodate, c.custname AS custname, dom.trnjualstatus doitemmststatus, UPPER((Select om.trnordernote From QL_trnordermst om Where om.ordermstoid=dom.ordermstoid AND dom.branch_code=om.branch_code)) doitemmstnote, dom.trnjualdate, '' doitemcustpono, '' doitemcustdo FROM ql_trnjualmst dom LEFT JOIN QL_trnjualreturmst som ON som.trnjualmstoid=dom.trnjualmstoid AND som.branch_code=dom.branch_code INNER JOIN QL_mstcust c ON dom.trncustoid=c.custoid AND dom.branch_code=c.branch_code Where dom.cmpcode='" & CompnyCode & "'"
        If DDLcabang.SelectedValue <> "ALL" Then
            sSql &= " AND dom.branch_code='" & DDLcabang.SelectedValue & "'"
        End If

        If ddltype.SelectedValue = "Konsinyasi" Then
            sSql &= " AND dom.ordermstoid IN (Select om.ordermstoid from QL_trnordermst om Where om.typeSO='" & ddltype.SelectedValue & "' AND om.branch_code=dom.branch_code)"
        Else
            sSql &= " AND som.trnjualstatus='Approved'"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If sono.Text <> "" Then
            Dim sSono() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sSono.Length - 1
                sSql &= " som.trnjualreturno LIKE '%" & Tchar(sSono(c1)) & "%'"
                If c1 < sSono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If range1.Text <> "" And range2.Text <> "" Then
            sSql &= " AND " & DDLPeriode.SelectedValue & ">='" & CDate(toDate(range1.Text)) & " 00:00:00' AND dom.trnjualdate<='" & CDate(toDate(range2.Text)) & " 23:59:59'" 
        End If

        sSql &= " ORDER BY dom.trnjualdate DESC, dom.trnjualmstoid DESC"
        Session("TblDO") = CKon.ambiltabel(sSql, "ql_trnjualmst")
    End Sub

    Private Sub BindListMat()
        Dim Join As String = ""
        If ddltype.SelectedValue = "Konsinyasi" Then
            Join = "LEFT"
        Else
            Join = "INNER"
        End If

        sSql = "SELECT DISTINCT 'False' AS checkvalue, i.itemoid, i.itemdesc itemlongdesc, i.itemcode, 'BUAH' unit,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' End JenisNya, i.stockflag FROM QL_trnjualreturdtl dod INNER JOIN QL_trnjualreturmst som ON som.cmpcode=dod.cmpcode AND som.trnjualreturmstoid=dod.trnjualreturmstoid AND dod.branch_code=som.branch_code INNER JOIN QL_mstitem i ON i.itemoid=dod.itemoid INNER JOIN QL_mstcust c ON som.trncustoid=c.custoid AND som.branch_code=c.branch_code INNER JOIN ql_trnjualmst dom ON dom.trnjualmstoid=som.trnjualmstoid AND dom.branch_code=som.branch_code Where som.trnjualstatus='Approved'"
        If DDLcabang.SelectedValue <> "ALL" Then
            sSql &= " AND som.branch_code='" & DDLcabang.SelectedValue & "'"
        End If

        If ddltype.SelectedValue = "Konsinyasi" Then
            sSql &= " AND som.orderno IN (Select om.orderno from QL_trnordermst om Where om.typeSO='" & ddltype.SelectedValue & "' AND om.branch_code=dom.branch_code)"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If sono.Text <> "" Then
            Dim sSono() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sSono.Length - 1
                sSql &= " som.trnjualreturno LIKE '%" & Tchar(sSono(c1)) & "%'"
                If c1 < sSono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If dono.Text <> "" Then
            Dim sDono() As String = Split(dono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sDono.Length - 1
                sSql &= " dom.trnjualno LIKE '%" & Tchar(sDono(c1)) & "%'"
                If c1 < sDono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If range1.Text <> "" And range2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & DDLPeriode.SelectedValue & ">='" & CDate(toDate(range1.Text)) & " 00:00:00' AND " & DDLPeriode.SelectedValue & "<='" & CDate(toDate(range2.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        Session("TblMat") = CKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub BindListReg()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, personoid registermstoid, personname registerno, gendesc Jabatan, pr.STATUS, pr.PERSONNIP FROM QL_mstperson pr Inner Join QL_MSTPROF pf ON pf.personnoid=pr.PERSONOID Inner Join QL_mstgen jp ON jp.genoid=pr.personstatus AND jp.gengroup='JOBPOSITION' Inner Join QL_trnjualmst jm ON jm.spgoid=pr.PERSONOID AND pf.BRANCH_CODE=jm.branch_code INNER JOIN QL_trnjualreturmst jr ON jr.trnjualmstoid=jm.trnjualmstoid AND jr.branch_code=jm.branch_code Inner Join QL_mstcust cuk ON cuk.custoid=jr.trncustoid AND jr.branch_code=cuk.branch_code WHERE pr.CMPCODE = '" & CompnyCode & "' AND (gendesc like '%sales%' or gendesc like '%MARKETING CABANG%' or gendesc like '%SALES PERSON%' or gendesc like '%TELEMARKETING%' OR gendesc like '%KEPALA CABANG%' OR FLAGSALES='YES' OR gendesc LIKE '%ADMINISTRASI PUSAT%')"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= "AND pf.USERID='" & Session("UserID") & "'"
        End If

        If ddltype.SelectedValue = "Konsinyasi" Then
            sSql &= " AND jm.ordermstoid IN (Select om.ordermstoid from QL_trnordermst om Where om.typeSO='" & ddltype.SelectedValue & "' AND om.branch_code=jm.branch_code)"
        End If

        If DDLcabang.SelectedValue <> "ALL" Then
            sSql &= " AND pf.BRANCH_CODE='" & DDLcabang.SelectedValue & "'"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " cuk.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If ddltype.SelectedValue = "Detail" Then
            If sono.Text <> "" Then
                Dim sSono() As String = Split(sono.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " jr.trnjualreturno LIKE '%" & Tchar(sSono(c1)) & "%'"
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If

        If dono.Text <> "" Then
            Dim sDono() As String = Split(dono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sDono.Length - 1
                sSql &= " jm.trnjualno LIKE '%" & Tchar(sDono(c1)) & "%'"
                If c1 < sDono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        'If range1.Text <> "" And range2.Text <> "" Then
        '    If IsValidPeriod() Then
        '        sSql &= " AND jr.trnjualdate>='" & CDate(toDate(range1.Text)) & " 00:00:00' AND jr.trnjualdate<='" & CDate(toDate(range2.Text)) & " 23:59:59'"
        '    Else
        '        Exit Sub
        '    End If
        'End If

        sSql &= " ORDER BY personname DESC"
        Session("TblReg") = CKon.ambiltabel(sSql, "QL_Sales")
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("rptSalesRetur.aspx")
        End If

        Page.Title = CompnyCode & " - Laporan Retur Penjualan"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If kacabCek("ReportForm/rptSalesRetur.aspx", Session("UserID")) = True Or Session("UserID") = "POPY" Then
            ibexcel.Visible = True : btnpdf.Visible = True
        Else
            If Session("branch_id") <> "10" Then
                ibexcel.Visible = False : btnpdf.Visible = False
                Label5.Visible = True : Label6.Visible = True : DDLcabang.Visible = True
            Else
                ibexcel.Visible = True : btnpdf.Visible = True
                Label5.Visible = True : Label6.Visible = True : DDLcabang.Visible = True
            End If
        End If

        If Not IsPostBack Then
            range1.Text = Format(New Date(GetServerTime.Year, GetServerTime.Month, 1), "01/MM/yyyy")
            range2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            initDLL()
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowprint.Click
        Dim date1, date2 As New Date, sMsg As String = ""
        If range1.Text.Trim = "" Then
            sMsg &= "- Please fill period 1 value..!!<br>"
        End If

        If range2.Text.Trim = "" Then
            sMsg &= "- Please fill period 2 value..!!<br>"
        End If

        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            sMsg &= "- Period 1 is invalid..!!<br>"
        End If

        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            sMsg &= "- Period 2 is invalid..!!<br>"
        End If

        If date2 < date1 Then
            sMsg &= "- Period 2 must be more than Period 1 !!<br>"
            Exit Sub
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        showPrint("")
    End Sub

    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        If range1.Text.Trim = "" Then
            showMessage("Please fill period 1 value!", 2)
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            showMessage("Please fill period 2 value!", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Period 1 is invalid.", 2)
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Period 2 is invalid.", 2)
            Exit Sub
        End If
        If date2 < date1 Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Exit Sub
        End If 
        showPrint("pdf")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        If range1.Text.Trim = "" Then
            showMessage("Please fill period 1 value!", 2)
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            showMessage("Please fill period 2 value!", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Period 1 is invalid.", 2)
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Period 2 is invalid.", 2)
            Exit Sub
        End If
        If date2 < date1 Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Exit Sub
        End If 
        showPrint("excel")
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("rptSalesRetur.aspx?awal=true")
    End Sub     

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        If range1.Text.Trim = "" Then
            showMessage("Please fill period 1 value!", 2)
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            showMessage("Please fill period 2 value!", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Period 1 is invalid.", 2)
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Period 2 is invalid.", 2)
            Exit Sub
        End If
        If date2 < date1 Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Exit Sub
        End If 
        Session("showprint") = "True"
        showPrint("")
    End Sub 

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing
            gvListSO.DataSource = Nothing : gvListSO.DataBind()
            CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SR data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "SR data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SR data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SR data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SR data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub 

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Selected SR data can't be found!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Selected SR data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sono.Text <> "" Then
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= ";" + vbCrLf + dtView(C1)("soitemno")
                            End If
                        Else
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= dtView(C1)("soitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    DDLcabang.Enabled = False
                    CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "Please select SO to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        sono.Text = ""
    End Sub

    Protected Sub imbFindDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindDO.Click
        If IsValidPeriod() Then
            DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
            Session("TblDO") = Nothing : Session("TblDOView") = Nothing
            gvListDO.DataSource = Nothing : gvListDO.DataBind()
            CProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseDO.Click
        dono.Text = ""
    End Sub

    Protected Sub btnFindListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDO.Click
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "SI data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = ""
        If DDLFilterListDO.SelectedValue = "doitemno" Then
            sPlus = DDLFilterListDO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListDO.Text) & "%'"
        Else
            sPlus = DDLFilterListDO.SelectedValue & " = '" & TcharNoTrim(txtFilterListDO.Text) & "'"
        End If
        If UpdateCheckedDO() Then
            Dim dv As DataView = Session("TblDO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblDOView") = dv.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dv.RowFilter = ""
                mpeListDO.Show()
            Else
                dv.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "SI data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListDO.Click
        DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "SI data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedDO() Then
            Dim dt As DataTable = Session("TblDO")
            Session("TblDOView") = dt
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub

    Protected Sub lkbAddToListListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListDO.Click
        If Not Session("TblDO") Is Nothing Then
            If UpdateCheckedDO() Then
                Dim dtTbl As DataTable = Session("TblDO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If dono.Text <> "" Then 
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= ";" + vbCrLf + dtView(C1)("doitemno")
                            End If 
                        Else
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= dtView(C1)("doitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = "" : DDLcabang.Enabled = False
                    CProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
                Else
                    Session("WarningListDO") = "Please select SI to add to list!"
                    showMessage(Session("WarningListDO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListDO") = "Please show some SI data first!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListDO.Click
        CProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
    End Sub

    Protected Sub btnSelectAllDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some SI data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedDO.Click
        If Session("TblDO") Is Nothing Then
            Session("WarningListDO") = "Selected SI data can't be found!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
        If UpdateCheckedDO() Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
                Session("TblDOView") = dtView.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dtView.RowFilter = ""
                mpeListDO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "Selected SI data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind()
            tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        itemcode.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If JenisBarangDDL.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag ='" & JenisBarangDDL.SelectedValue & "'"
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected Katalog data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If itemcode.Text <> "" Then
                            itemcode.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            itemcode.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub gvListDO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDO.PageIndexChanging
        If UpdateCheckedDO2() Then
            gvListDO.PageIndex = e.NewPageIndex
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                CProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                CProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                CProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("EmptyListDO") Is Nothing And Session("EmptyListDO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListDO") Then
                Session("EmptyListDO") = Nothing
                CProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
        If Not Session("WarningListDO") Is Nothing And Session("WarningListDO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListDO") Then
                Session("WarningListDO") = Nothing
                CProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
        If Not Session("EmptyListReg") Is Nothing And Session("EmptyListReg") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListReg") Then
                Session("EmptyListReg") = Nothing
                CProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, True)
            End If
        End If
        If Not Session("WarningListReg") Is Nothing And Session("WarningListReg") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListReg") Then
                Session("WarningListReg") = Nothing
                CProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, True)
            End If
        End If
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    CProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        CProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing : gvListCust.DataSource = Nothing : gvListCust.DataBind()
            CProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = ""
    End Sub

    Protected Sub ddltype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltype.SelectedIndexChanged
        If ddltype.SelectedValue = "Detail" Then
            Label2.Visible = True : Label3.Visible = True
            itemcode.Visible = True : imbFindMat.Visible = True
            imbEraseMat.Visible = True : itemcode.Text = ""
            Label8.Visible = False : Label9.Visible = False
            DDLAmtRetur.Visible = False
            Label4.Visible = True : Label10.Visible = True
            sono.Visible = True : imbFindSO.Visible = True
            imbEraseSO.Visible = True : DDLPeriode.Enabled = True
            DDLPeriode.SelectedValue = "som.trnjualdate"
        ElseIf ddltype.SelectedValue = "Konsinyasi" Then
            Label2.Visible = True : Label3.Visible = True
            itemcode.Visible = True : imbFindMat.Visible = True
            imbEraseMat.Visible = True : itemcode.Text = ""
            Label8.Visible = True : Label9.Visible = True
            DDLAmtRetur.Visible = True : DDLPeriode.SelectedValue = "dom.trnjualdate"

            Label4.Visible = False : Label10.Visible = False
            sono.Visible = False : imbFindSO.Visible = False
            imbEraseSO.Visible = False : DDLPeriode.Enabled = False
        ElseIf ddltype.SelectedValue = "DtlKonsi" Then
            Label2.Visible = True : Label3.Visible = True
            itemcode.Visible = True : imbFindMat.Visible = True
            imbEraseMat.Visible = True : itemcode.Text = ""
            Label8.Visible = True : Label9.Visible = True
            DDLAmtRetur.Visible = True
            DDLPeriode.SelectedValue = "som.trnjualdate"
            Label4.Visible = True : Label10.Visible = True
            sono.Visible = True : imbFindSO.Visible = True
            imbEraseSO.Visible = True : DDLPeriode.Enabled = True
        Else
            Label2.Visible = False : Label3.Visible = False
            itemcode.Visible = False : imbFindMat.Visible = False
            imbEraseMat.Visible = False : itemcode.Text = ""
            Label8.Visible = False : Label9.Visible = False
            DDLAmtRetur.Visible = False
            DDLPeriode.SelectedValue = "som.trnjualdate"
            Label4.Visible = True : Label10.Visible = True
            sono.Visible = True : imbFindSO.Visible = True
            imbEraseSO.Visible = True : DDLPeriode.Enabled = True
        End If
    End Sub

    Protected Sub imbFindReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindReg.Click
        If IsValidPeriod() Then
            DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
            Session("TblReg") = Nothing : Session("TblRegView") = Nothing : gvListReg.DataSource = Nothing : gvListReg.DataBind()
            CProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseReg.Click
        regno.Text = ""
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        If Session("TblReg") Is Nothing Then
            BindListReg()
            If Session("TblReg").Rows.Count <= 0 Then
                Session("EmptyListReg") = "Sales data can't be found!"
                showMessage(Session("EmptyListReg"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListReg.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListReg.Text) & "%'"
        If UpdateCheckedReg() Then
            Dim dv As DataView = Session("TblReg").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblRegView") = dv.ToTable
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                dv.RowFilter = ""
                mpeListReg.Show()
            Else
                dv.RowFilter = ""
                Session("TblRegView") = Nothing
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                Session("WarningListReg") = "Sales data can't be found!"
                showMessage(Session("WarningListReg"), 2)
            End If
        Else
            mpeListReg.Show()
        End If
    End Sub

    Protected Sub btnViewAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListReg.Click
        DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
        If Session("TblReg") Is Nothing Then
            BindListReg()
            If Session("TblReg").Rows.Count <= 0 Then
                Session("EmptyListReg") = "Register data can't be found!"
                showMessage(Session("EmptyListReg"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedReg() Then
            Dim dt As DataTable = Session("TblReg")
            Session("TblRegView") = dt
            gvListReg.DataSource = Session("TblRegView")
            gvListReg.DataBind()
        End If
        mpeListReg.Show()
    End Sub

    Protected Sub btnSelectAllReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllReg.Click
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblRegView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblReg")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "registermstoid=" & dtTbl.Rows(C1)("registermstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblReg") = objTbl
                Session("TblRegView") = dtTbl
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
            End If
            mpeListReg.Show()
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneReg.Click
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblRegView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblReg")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "registermstoid=" & dtTbl.Rows(C1)("registermstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblReg") = objTbl
                Session("TblRegView") = dtTbl
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
            End If
            mpeListReg.Show()
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedReg.Click
        If Session("TblReg") Is Nothing Then
            Session("WarningListReg") = "Selected sales data can't be found!"
            showMessage(Session("WarningListReg"), 2)
            Exit Sub
        End If
        If UpdateCheckedReg() Then
            Dim dtTbl As DataTable = Session("TblReg")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
                Session("TblRegView") = dtView.ToTable
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                dtView.RowFilter = ""
                mpeListReg.Show()
            Else
                dtView.RowFilter = ""
                Session("TblRegView") = Nothing
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                Session("WarningListReg") = "Selected sales data can't be found!"
                showMessage(Session("WarningListReg"), 2)
            End If
        Else
            mpeListReg.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListReg.Click
        If Not Session("TblReg") Is Nothing Then
            If UpdateCheckedReg() Then
                Dim dtTbl As DataTable = Session("TblReg")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If regno.Text <> "" Then
                            If dtView(C1)("registerno") <> "" Then
                                regno.Text &= ";" + vbCrLf + dtView(C1)("registerno")
                            End If
                        Else
                            If dtView(C1)("registerno") <> "" Then
                                regno.Text &= dtView(C1)("registerno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = "" 
                    CProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, False)
                Else
                    Session("WarningListReg") = "Please select sales to add to list!"
                    showMessage(Session("WarningListReg"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListReg.Click
        CProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, False)
    End Sub
#End Region
End Class
