﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="menu.aspx.vb" Inherits="menu" Title="" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">

    <table id="tableutama" align="center" border="0" cellpadding="0" cellspacing="0"
        width="979" style="height: 375px">
        <tr>
            <td valign="top">
                <br />
                <br />
            </td>
            <td valign="top">
                <table id="Table1" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="right" class="header" style="width: 50%;" colspan="" rowspan="">
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; &nbsp;Home :.&nbsp;
                        </th>
                        <th align="right" class="header" colspan="1" rowspan="1" style="text-align: right">
                            <img align="absMiddle" alt="" src="../Images/corner.gif" /><asp:LinkButton ID="LinkButton5" runat="server" Font-Bold="True" PostBackUrl="~/Other/WaitingAction.aspx" Font-Names="Arial" Width="157px">Waiting for My Approval :.</asp:LinkButton></th>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left" valign="top">
                            <ajaxToolkit:TabContainer Width="100%" ActiveTabIndex="1" Height="375px" ID="TabContainer1" runat="server" ScrollBars="Both" Font-Bold="True">
                            <ajaxToolkit:TabPanel ID="TabPanel7" runat="server" HeaderText="TabPanel7">
                                    <ContentTemplate>
                                        <asp:UpdatePanel id="upNotification" runat="server">
                                            <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: beige; TEXT-ALIGN: left"><asp:GridView id="dtApproval" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w19" CellPadding="4" GridLines="None" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="type" HeaderText="Tipe">
<HeaderStyle CssClass="gvhdr" Font-Overline="False" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jml" HeaderText="Jml">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl='<%# Eval("address") %>'>View</asp:LinkButton>

                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Approval data !!"></asp:Label>

                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET></TD></TR><TR><TD id="TD1" class="Label" align=left runat="server"></TD></TR><TR><TD style="HEIGHT: 5px" id="TDNotifyDOinproc" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" __designer:wfdid="w20" Text="Berikut ini transaksi yang belum diposting :"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:GridView id="GVListPost" runat="server" Width="50%" __designer:wfdid="w21" GridLines="None" AutoGenerateColumns="False" DataKeyNames="appformvalue" ShowHeader="False"><Columns>
<asp:BoundField DataField="apptype">
<ItemStyle HorizontalAlign="Left" Height="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appsept">
<ItemStyle HorizontalAlign="Center" Width="2%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appcount">
<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="[ View ]" ShowSelectButton="True">
<ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
</asp:CommandField>
</Columns>
</asp:GridView> </TD></TR></TBODY></TABLE></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left></TD></TR><TR><TD id="TDNotifyPricelist" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" __designer:wfdid="w22" Text="Berikut ini data customer yang harus di approved :"></asp:Label></TD></TR><TR><TD class="Label" align=left>Total&nbsp;Data&nbsp; : <asp:Label id="DtTotal" runat="server" __designer:wfdid="w23"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvMst" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w24" CellPadding="4" GridLines="None" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No data in database." AllowSorting="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="custapprovaloid" DataNavigateUrlFormatString="~/Master/MstPengajuan.aspx?oid={0}" DataTextField="custapprovalno" HeaderText="No. Pengajuan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custapprovaldate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcode" HeaderText="Cust. Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custapprovaltype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="NewData" HeaderText="Informasi">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custapprovalnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custapprovalstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data not found !!" __designer:wfdid="w4"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE><BR /><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" __designer:wfdid="w25" Text="Berikut ini master barang dengan pricelist nol :"></asp:Label></TD></TR><TR><TD class="Label" align=left>Total Barang&nbsp; : <asp:Label id="totalBarang" runat="server" __designer:wfdid="w26"></asp:Label>&nbsp;<asp:ImageButton id="imbPrintPLExcel" onclick="imbPrintPLExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvListPricelist" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w28" CellPadding="4" GridLines="None" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Height="15px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Height="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="stockflag" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="[ View ]" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></TD></TR><TR><TD id="TD2" class="Label" align=left runat="server"></TD></TR><TR><TD id="TDNotifyPromo" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" __designer:wfdid="w29" Text="Berikut ini promo yang periodenya telah berakhir :"></asp:Label></TD></TR><TR><TD class="Label" align=left>&nbsp;<asp:GridView id="gvListPromo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w30" CellPadding="4" GridLines="None" AutoGenerateColumns="False" DataKeyNames="promooid,type" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="promooid" HeaderText="promooid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Height="15px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Height="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="promocode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="promoname" HeaderText="Nama Promo">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="type" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="[ View ]" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></TD></TR><TR><TD class="Label" align=left runat="server"></TD></TR><TR><TD id="TDNotifyPiutangDueDate" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" __designer:wfdid="w31" Text="Berikut ini piutang yang sudah jatuh tempo :"></asp:Label></TD></TR><TR><TD class="Label" align=left>&nbsp;<asp:GridView id="gvListPiutangDueDate" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w32" CellPadding="4" GridLines="None" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnjualno" HeaderText="No. Jual">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnamtjualnetto" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="[ View ]" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></TD></TR><TR><TD class="Label" align=left runat="server"></TD></TR><TR><TD id="TDNotifyPOoutstanding" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" __designer:wfdid="w33" Text="Berikut ini PO yang masih Outstanding berdasarkan ETD dan 3 Hari sebelum jatuh tempo :"></asp:Label></TD></TR><TR><TD class="Label" align=left>&nbsp;<asp:GridView id="gvListPOoutstanding" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w34" CellPadding="4" GridLines="None" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnbelipono" HeaderText="PO No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipodate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="etd" HeaderText="ETD">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbelinettoidr" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="[ View ]" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE> <asp:LinkButton id="aLinkButton" runat="server" __designer:wfdid="w36">Update Lasoid Conmtr</asp:LinkButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                                            <triggers>
<asp:PostBackTrigger ControlID="imbPrintPLExcel"></asp:PostBackTrigger>
</triggers>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        <img align="absMiddle" alt="" src="../Images/corner.gif" />
                                        <strong><span style="font-size: 9pt">.: Notification</span></strong>
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"><HeaderTemplate>
                                        <img align="absMiddle" alt="" src="../Images/corner.gif" />
                                        <strong><span style="font-size: 9pt">.: <span style="font-size: 8pt; font-family: Arial;">Master</span></span></strong>
                                    
</HeaderTemplate>
<ContentTemplate>
<TABLE style="WIDTH: 100%" cellSpacing=1 cellPadding=1><TBODY><TR><TD align=center><asp:Label id="mnuProfile" runat="server" Font-Bold="False"></asp:Label> </TD><TD vAlign=middle align=center><asp:Label id="mnuRole" runat="server" Font-Bold="False"></asp:Label></TD><TD vAlign=middle align=center><asp:Label id="mnuUserRole" runat="server" Font-Bold="False"></asp:Label> </TD><TD vAlign=middle align=center><asp:Label id="mnuGeneral" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuDimensi" runat="server" Font-Bold="False"></asp:Label></TD>
    <td align="center">
        <asp:Label id="RateStandard" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center">
        <asp:Label id="mnuCurrency" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center">
        <asp:Label id="mnuKatalog" runat="server" Font-Bold="False"></asp:Label></td>
</TR><TR><TD align=center>
    <asp:Label ID="mstPrice" runat="server" Font-Bold="False"></asp:Label></TD><TD vAlign=middle align=center>
    <asp:Label id="expedisiBranch" runat="server"></asp:Label></TD><TD vAlign=middle align=center><asp:Label id="mnuItem" runat="server" Font-Bold="False"></asp:Label></TD><TD vAlign=middle align=center>
    <asp:Label id="MstSCOA" runat="server" Font-Bold="False"></asp:Label></TD><TD vAlign=middle align=center>
        <asp:Label ID="mnuBottomPrice" runat="server"></asp:Label></TD>
    <td align="center" valign="middle">
        <asp:Label id="mstDimensi" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center" valign="middle">
        <asp:Label id="MstExpedisi" runat="server"></asp:Label></td>
    <td align="center" valign="middle">
        <asp:Label id="mnuMstUserID" runat="server" Font-Bold="False"></asp:Label></td>
</TR><TR><TD vAlign=middle align=center>
    <asp:Label id="UploadInit" runat="server" Font-Bold="False"></asp:Label></TD><TD vAlign=middle align=center> 
            <asp:Label ID="MnuMstCabang" runat="server"></asp:Label></TD><TD align=center> 
                <asp:Label ID="MnuStsKatalog" runat="server"></asp:Label></TD><TD vAlign=middle align=center>
                    &nbsp;<asp:Label id="MnuAdjHpp" runat="server"></asp:Label></TD><TD vAlign=middle align=center>
                        <asp:Label ID="MnuPriceCabang" runat="server"></asp:Label></TD>
    <td align="center" valign="middle">
        &nbsp;<asp:Label ID="MnuJabatan" runat="server"></asp:Label></td>
    <td align="center" valign="middle">
        <asp:Label ID="MnuGudang" runat="server"></asp:Label>&nbsp;</td>
    <td align="center" valign="middle">
                    <asp:Label ID="mnuUploadAdj" runat="server" Visible="False"></asp:Label></td>
</TR><TR><TD vAlign=middle align=center>
    &nbsp;&nbsp;
</TD><TD vAlign=middle align=center></TD><TD vAlign=middle align=center>
    </TD><TD vAlign=middle align=center>&nbsp;&nbsp;
    </TD><TD vAlign=middle align=center>
    &nbsp;&nbsp; </TD>
    <td align="center" valign="middle">
    </td>
    <td align="center" valign="middle">
    </td>
    <td align="center" valign="middle">
    </td>
</TR></TBODY></TABLE>
</ContentTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel2"><HeaderTemplate>
                                        <img align="absMiddle" alt="" src="../Images/corner.gif" />
                                        <strong><span style="font-size: 9pt">.: <span style="font-size: 8pt; font-family: Arial;">Service</span></span></strong>
                                    
</HeaderTemplate>
<ContentTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" valign="middle" colspan="5">
                                                    <strong><span style="text-decoration: underline">.: MASTER</span></strong></td>
                                                <td align="left" colspan="1" valign="middle">
                                                </td>
                                                <td align="left" colspan="1" valign="middle">
                                                </td>
                                                <td align="left" colspan="1" valign="middle">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 144px; height: 16px; vertical-align: middle; text-align: center;" valign="middle">
                                                    </td>
                                                <td align="center" style="height: 16px; vertical-align: middle; text-align: center;" valign="middle">
                                                    </td>
                                                <td align="center" style="width: 144px; height: 16px; vertical-align: middle; text-align: center;" valign="middle">
                                                    </td>
                                                <td align="center" style="width: 144px; height: 16px; vertical-align: middle; text-align: center;" valign="middle">
                                                    </td>
                                                <td align="center" style="width: 144px; height: 16px; vertical-align: middle; text-align: center;" valign="middle">
                                                    </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="5" style="vertical-align: middle; height: 16px; text-align: left"
                                                    valign="middle">
                                                    <strong><span style="text-decoration: underline">.: TRANSACTION</span></strong></td>
                                                <td align="left" colspan="1" style="vertical-align: middle; height: 16px; text-align: left"
                                                    valign="middle">
                                                </td>
                                                <td align="left" colspan="1" style="vertical-align: middle; height: 16px; text-align: left"
                                                    valign="middle">
                                                </td>
                                                <td align="left" colspan="1" style="vertical-align: middle; height: 16px; text-align: left"
                                                    valign="middle">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    <asp:Label ID="mnpPR" runat="server"></asp:Label>
                                                </td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                    <asp:Label ID="mnuTrfService" runat="server"></asp:Label>
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    <asp:Label ID="TcConfirmInt" runat="server"></asp:Label>
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    <asp:Label ID="MnuSvcSupplier" runat="server"></asp:Label>
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    <asp:Label ID="MnuKembaliSupp" runat="server"></asp:Label>
                                                </td>
                                                <td align="center"
                                                    valign="middle">
                                                    <asp:Label ID="MnuSuppInt" runat="server"></asp:Label></td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    <asp:Label ID="mnuFinal" runat="server"></asp:Label></td>
                                                <td align="center"
                                                    valign="middle">
                                                    <asp:Label ID="MnuFinInt" runat="server" Width="81px"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="center"
                                                    valign="middle">
                                                    &nbsp;<asp:Label ID="mnuPReturn" runat="server" Width="85px"></asp:Label></td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;<asp:Label ID="MnuCekTTS" runat="server"></asp:Label></td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;</td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;</td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="5" valign="middle">
                                                    <strong><span style="text-decoration: underline">.: REPORT</span></strong></td>
                                                <td align="left" colspan="1" valign="middle">
                                                </td>
                                                <td align="left" colspan="1" valign="middle">
                                                </td>
                                                <td align="left" colspan="1" valign="middle">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 144px; height: 16px; vertical-align: middle; text-align: center;" valign="middle">
                                                    <asp:Label ID="mnuPOStatus" runat="server" Width="119px"></asp:Label></td>
                                                <td align="center" valign="middle" style="vertical-align: middle; height: 16px; text-align: center">
                                                    <asp:Label ID="MnuSvcSupp" runat="server" Width="116px"></asp:Label></td>
                                                <td align="center" valign="middle" style="vertical-align: middle; width: 144px; height: 16px; text-align: center">
                                                    <asp:Label ID="mnuRptTwInt" runat="server" Width="135px"></asp:Label></td>
                                                <td align="center" valign="middle" style="vertical-align: middle; width: 144px; height: 16px; text-align: center">
                                                                <asp:Label ID="mnustatusdeliv" runat="server" Width="110px"></asp:Label>
                                                </td>
                                                <td align="center" style="width: 144px; height: 16px; vertical-align: middle; text-align: center;" valign="middle">
                                                    <asp:Label ID="mnuSOStatus2" runat="server" Width="116px"></asp:Label>
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;<asp:Label ID="MnuSuppIntRpt" runat="server" Width="119px"></asp:Label></td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    <asp:Label ID="RptSFI" runat="server" Width="119px"></asp:Label></td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;
                                                </td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;</td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    </td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                    &nbsp;</td>
                                                <td align="center" style="vertical-align: middle; width: 144px; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                                <td align="center" style="vertical-align: middle; height: 16px; text-align: center"
                                                    valign="middle">
                                                </td>
                                            </tr>
                                        </table>
                                    
</ContentTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel3"><HeaderTemplate>
                                        <img align="absMiddle" alt="" src="../Images/corner.gif" style="font-family: Arial" /><span
                                            style="font-family: Arial"> <strong><span style="font-size: 9pt">.:<span style="font-size: 8pt"> Marketing</span>
                                        </span></strong></span>
                                    
</HeaderTemplate>
<ContentTemplate>
<TABLE><TBODY><TR><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left" align=center colSpan=5><STRONG><SPAN style="TEXT-DECORATION: underline">.: MASTER</SPAN></STRONG></TD>
    <td align="center" colspan="1" style="vertical-align: middle; text-align: left">
    </td>
    <td align="center" colspan="1" style="vertical-align: middle; text-align: left">
    </td>
    <td align="center" colspan="1" style="vertical-align: middle; text-align: left">
    </td>
</TR><TR><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center><asp:Label id="mnuCustomer" runat="server" Font-Bold="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center><asp:Label id="mnuBOM" runat="server" Font-Bold="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center><asp:Label id="mnuRoute" runat="server" Font-Bold="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center>&nbsp;<asp:Label id="CustGroup" runat="server" Font-Bold="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center>
    <asp:Label ID="MnuProgPoint" runat="server" Font-Bold="False"></asp:Label></TD>
    <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
        <asp:Label ID="MnuCustApp" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
        <asp:Label ID="PriceBottom" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
    </td>
</TR><TR><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center></TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center></TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center></TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center></TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center></TD>
        <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
    </TR><TR><TD style="VERTICAL-ALIGN: middle; HEIGHT: 16px; TEXT-ALIGN: left" align=center colSpan=5><STRONG><SPAN style="TEXT-DECORATION: underline">.: TRANSACTION</SPAN></STRONG></TD>
        <td align="center" colspan="1" style="vertical-align: middle; height: 16px; text-align: left">
        </td>
        <td align="center" colspan="1" style="vertical-align: middle; height: 16px; text-align: left">
        </td>
        <td align="center" colspan="1" style="vertical-align: middle; height: 16px; text-align: left">
        </td>
    </TR><TR><TD align=center><asp:Label id="mnuSO" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuSDO" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuSI" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="mnuSReturn" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="mnuSOclos" runat="server" Font-Bold="False"></asp:Label>&nbsp;</TD>
        <td align="center">
            <asp:Label id="mnuSOCanceled" runat="server"></asp:Label></td>
        <td align="center">
            <asp:Label id="mnuSuratJalan" runat="server" Font-Bold="False"></asp:Label></td>
        <td align="center">
            <asp:Label id="NotaExpMnu" runat="server" Font-Bold="False"></asp:Label></td>
    </TR><TR><TD align=center>
        &nbsp;<asp:Label id="mnuSIRecord" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="PerNotaMnu" runat="server" Font-Bold="False"></asp:Label>&nbsp;</TD><TD align=center>
        &nbsp;<asp:Label id="mnuWIPRes" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
        &nbsp;<asp:Label ID="MnuRags" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
        &nbsp;</TD>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
    </TR><TR><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center> </TD><TD align=center> </TD><TD align=center></TD>
        <td align="center">
            </td>
        <td align="center">
            </td>
        <td align="center">
        </td>
    </TR><TR><TD align=left colSpan=5 style="height: 15px"><STRONG><SPAN style="TEXT-DECORATION: underline">.: REPORT</SPAN></STRONG></TD>
        <td align="left" colspan="1" style="height: 15px">
        </td>
        <td align="left" colspan="1" style="height: 15px">
        </td>
        <td align="left" colspan="1" style="height: 15px">
        </td>
    </TR><TR><TD align=center><asp:Label id="mnuSOStatus" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuRPTDO" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuOutsSO" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="rptSOClose" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuRptSalesRetur" runat="server" Font-Bold="False"></asp:Label> </TD>
        <td align="center">
            <asp:Label id="mnuJualBersih" runat="server" Font-Bold="False"></asp:Label></td>
        <td align="center">
            <asp:Label id="mnurptcust" runat="server"></asp:Label></td>
        <td align="center">
    <asp:Label ID="RptSuratJalan" runat="server" Font-Bold="False"></asp:Label></td>
    </TR><TR><TD align=center>
        <asp:Label id="RptNotaExp" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label ID="MnuLapProgPoint" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
            <asp:Label ID="MnuHistPriceSO" runat="server" Font-Bold="False"></asp:Label>
        </TD><TD align=center>
            <asp:Label ID="MnuPromCust" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
                <asp:Label ID="MnuCustApproval" runat="server" Font-Bold="False" Width="138px"></asp:Label></TD>
        <td align="center">
            <asp:Label ID="MnuInfoPrintOutsSI" runat="server" Font-Bold="False" Width="138px"></asp:Label></td>
        <td align="center">
            <asp:Label ID="MnuNotaRags" runat="server" Font-Bold="False" Width="138px"></asp:Label></td>
        <td align="center">
            <asp:Label ID="MnuStatusSIKonsi" runat="server" Font-Bold="False"></asp:Label></td>
    </TR>
    <tr>
        <td align="center">
            &nbsp;<asp:Label ID="RptJualTriWulan" runat="server" Font-Bold="False"></asp:Label>
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
    </tr>
</TBODY></TABLE>
</ContentTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel4"><HeaderTemplate>
                                        <img align="absMiddle" alt="" src="../Images/corner.gif" style="font-family: Arial" /><span
                                            style="font-family: Arial"> <span style="font-size: 9pt">.<span>: <span>Purchasing</span></span></span>
                                        </span>
                                    
</HeaderTemplate>
<ContentTemplate>
<TABLE><TBODY><TR><TD align=left colSpan=5><STRONG><SPAN style="TEXT-DECORATION: underline">.: MASTER</SPAN></STRONG></TD>
    <td align="left" colspan="1">
    </td>
    <td align="left" colspan="1">
    </td>
    <td align="left" colspan="1">
    </td>
</TR><TR><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=left><asp:Label id="mnuPrintBarcode" runat="server" Font-Bold="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=left><asp:Label id="mnuSupplier" runat="server" Font-Bold="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=left><asp:Label id="genbarcmat" runat="server" Font-Bold="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=left>
    <asp:Label ID="mstTargetItem" runat="server" Font-Bold="False"></asp:Label>&nbsp;</TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=left>
        <asp:Label ID="MnuProgSupp" runat="server" Font-Bold="False"></asp:Label></TD>
    <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
            <asp:Label ID="MnuOpenPO" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        <asp:Label ID="mnuUpdatePI" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
    </td>
</TR>
    <tr>
        <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
        <td align="left" style="vertical-align: middle; width: 144px; text-align: center">
        </td>
    </tr>
    <TR><TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left" align=left colSpan=5>&nbsp;<STRONG><SPAN style="TEXT-DECORATION: underline">.:&nbsp; TRANSACTION</SPAN></STRONG></TD>
                <td align="left" colspan="1" style="vertical-align: middle; text-align: left">
                </td>
                <td align="left" colspan="1" style="vertical-align: middle; text-align: left">
                </td>
                <td align="left" colspan="1" style="vertical-align: middle; text-align: left">
                </td>
            </TR><TR><TD align=center><asp:Label id="mnuBPM" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center> <asp:Label id="mnuPDO" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="mnuPI" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuBPBJ" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center> <asp:Label id="mnuSPKClose" runat="server" Font-Bold="False"></asp:Label></TD>
                <td align="center">
                    <asp:Label id="MnuCekHutang" runat="server" Font-Bold="False"></asp:Label></td>
                <td align="center">
                <asp:Label ID="MnuForeCastPO" runat="server"></asp:Label></td>
                <td align="center">
                </td>
            </TR><TR><TD align=center> </TD><TD align=center> &nbsp;</TD><TD align=center> &nbsp;</TD><TD align=center></TD><TD align=center></TD>
                    <td align="center">
                        </td>
                    <td align="center">
                    </td>
                    <td align="center">
                    </td>
                </TR><TR><TD align=left colSpan=5><STRONG><SPAN style="TEXT-DECORATION: underline">.: REPORT</SPAN></STRONG></TD>
                    <td align="left" colspan="1">
                    </td>
                    <td align="left" colspan="1">
                    </td>
                    <td align="left" colspan="1">
                    </td>
                </TR><TR><TD align=center>&nbsp;<asp:Label id="rptPO" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="rptPDO" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="mnuRptPI" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="mnuRptPRetur" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="rptPOClose" runat="server" Font-Bold="False"></asp:Label></TD>
                    <td align="center">
                        <asp:Label id="MnuBeliBersih" runat="server" Font-Bold="False"></asp:Label></td>
                    <td align="center">
            <asp:Label ID="rptPOStatus" runat="server" Font-Bold="False"></asp:Label></td>
                    <td align="center">
                        <asp:Label id="rptVoucher" runat="server"></asp:Label></td>
                </TR><TR><TD align=center>
                <asp:Label id="MnuLapProgSupp" runat="server" Font-Bold="False"></asp:Label>
            </TD><TD align=center>
                &nbsp;<asp:Label ID="MnuPOAMP" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
                    <asp:Label ID="MnurptPS" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
                    &nbsp;</TD><TD align=center>
                    &nbsp;</TD>
                <td align="center">
                </td>
                <td align="center">
                </td>
                <td align="center">
                </td>
            </TR></TBODY></TABLE>
</ContentTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel6" ID="TabPanel6"><HeaderTemplate>
                                        <img align="absMiddle" alt="" src="../Images/corner.gif" style="font-family: Arial" /><span
                                            style="font-family: Arial"> <span style="font-size: 9pt">.<span>: <span>Inventory</span></span></span>
                                        </span>
                                    
</HeaderTemplate>
<ContentTemplate>
<TABLE><TBODY><TR><TD align=left colSpan=5><STRONG><SPAN style="TEXT-DECORATION: underline">.: TRANSACTION</SPAN></STRONG></TD>
    <td align="left" colspan="1">
    </td>
    <td align="left" colspan="1">
    </td>
    <td align="left" colspan="1">
    </td>
</TR><TR><TD align=center><asp:Label id="mnuInitStockAwal" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center>&nbsp;<asp:Label id="mnuSDOTRD" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center>&nbsp;<asp:Label id="mnuTrfWh" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuTransform" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="mnuClosingStock" runat="server" Font-Bold="False"></asp:Label> </TD>
    <td align="center">
        <asp:Label id="mnuTrfConf" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center">
        <asp:Label id="mnuMatusageUmum" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center">
        <asp:Label id="mnuPinjamBarang" runat="server" Font-Bold="False"></asp:Label></td>
</TR><TR><TD align=center>
    <asp:Label id="MnuKembalibarang" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center> &nbsp;<asp:Label ID="MnuPb" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
        <asp:Label ID="MnuTWecommerce" runat="server" Font-Bold="False"></asp:Label>&nbsp; </TD><TD align=center>&nbsp; </TD><TD align=center>&nbsp;</TD>
    <td align="center">
    </td>
    <td align="center">
    </td>
    <td align="center">
    </td>
</TR>
    <tr>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
        <td align="center">
        </td>
    </tr>
    <TR><TD align=left colSpan=5><STRONG><SPAN style="TEXT-DECORATION: underline">.: REPORT</SPAN></STRONG></TD>
    <td align="left" colspan="1">
    </td>
    <td align="left" colspan="1">
    </td>
    <td align="left" colspan="1">
    </td>
</TR><TR><TD align=center>&nbsp;<asp:Label id="mnuStockReport" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuRptAS" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="mnuRptTW" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center><asp:Label id="mnuRptTransform" runat="server" Font-Bold="False"></asp:Label> </TD><TD align=center><asp:Label id="mnuMatUsage" runat="server" Font-Bold="False"></asp:Label> </TD>
    <td align="center">
        <asp:Label id="mnutwsts" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center">
        <asp:Label id="MnuPinjam" runat="server" Font-Bold="False"></asp:Label></td>
    <td align="center">
        <asp:Label id="mnuMatStock" runat="server" Font-Bold="False"></asp:Label></td>
</TR><TR><TD align=center>
    <asp:Label ID="mnuDimensiItem" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
    <asp:Label ID="rptIOWH" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
        <asp:Label ID="mnuRptDeadStock" runat="server" Font-Bold="False"></asp:Label>
    </TD><TD align=center>
        <asp:Label ID="MnuRptPB" runat="server" Font-Bold="False"></asp:Label></TD><TD align=center>
            <asp:Label ID="MnuTWEcom" runat="server"></asp:Label>
                &nbsp;&nbsp;</TD>
    <td align="center">
    </td>
    <td align="center">
    </td>
    <td align="center">
    </td>
</TR><TR><TD align=center>
                &nbsp;</TD><TD align=center>
                &nbsp;</TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center>&nbsp; </TD><TD align=center>&nbsp;</TD><TD style="VERTICAL-ALIGN: middle; WIDTH: 144px; TEXT-ALIGN: center" align=center>&nbsp; </TD>
                <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
                </td>
                <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
                </td>
                <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
                </td>
            </TR></TBODY></TABLE>
</ContentTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel5" ID="TabPanel5"><HeaderTemplate>
                                        <img align="absMiddle" alt="" src="../Images/corner.gif" style="font-family: Arial" /><span
                                            style="font-family: Arial"> <strong><span style="font-size: 9pt">.: <span style="font-size: 8pt">Accounting</span></span></strong>
                                        </span>
                                    
</HeaderTemplate>
<ContentTemplate>
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td align="left" colspan="4" style="height: 15px"><strong><span style="text-decoration: underline">.: MASTER</span></strong></td>
                                                    <td align="left" colspan="1" style="height: 15px">
                                                    </td>
                                                    <td align="left" colspan="1" style="height: 15px">
                                                    </td>
                                                    <td align="left" colspan="1" style="height: 15px">
                                                    </td>
                                                    <td align="left" colspan="1" style="height: 15px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="InitFA" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuInterface" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuInitCOA" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="MNUCLOSINGACCTG" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuCOA" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="Label1" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuMstFaktur" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuAssignFaktur" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="4">
                                                        <strong><span style="text-decoration: underline">.: TRANSACTION</span></strong></td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="mnuInitAP" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuInitAR" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="InitDPAp" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="InitDPAr" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuAPPymnt" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnu_AR_Payment" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnu_DPAP" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnu_DPAR" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="MNUCREDITNOTE" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="MNUDEBITNOTE" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="mnureceipt" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuCBExpense" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuCBMut" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="MnuGIRO" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="MnuGIRO_IN" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="MnuGIRO_OUT" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="PurchaseFA" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="TrnFA" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="InitVoucher" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="RealVoucher" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="PayInvServis" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="MnuFreeJurnal" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnu_AP_Koreksi" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnu_AR_Koreksi" runat="server" Font-Bold="False"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="mnuDPAPRetur" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuDPARRetur" runat="server" Font-Bold="False"></asp:Label>
                                                        &nbsp;</td>
                                                    <td align="center">
                                                        <asp:Label ID="MnuPrepaid" runat="server" Width="91px"></asp:Label></td>
                                                    <td align="center">
                                                        &nbsp;<asp:Label ID="MnuPrepaidDispose" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuNotaHR" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuPaymentHR" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuGiroFlag" runat="server"></asp:Label>&nbsp;</td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuHRReturn" runat="server"></asp:Label>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        &nbsp;<asp:Label ID="mnuCashBon" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                    </td>
                                                    <td align="center">
                                                    </td>
                                                    <td align="center">
                                                    </td>
                                                    <td align="center">
                                                    </td>
                                                    <td align="center">
                                                    </td>
                                                    <td align="center">
                                                    </td>
                                                    <td align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="4">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="4"><strong><span style="text-decoration: underline">.: REPORT</span></strong></td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                    <td align="left" colspan="1">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="rptDPAR" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
                                                        <asp:Label ID="rptDPAP" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuGenJrnl" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                        <asp:Label ID="mnuGenLed" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                        <asp:Label ID="mnuCashRpt" runat="server"></asp:Label></td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                        <asp:Label ID="mnuBankRpt" runat="server"></asp:Label></td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                        <asp:Label ID="mnukartuhutang" runat="server"></asp:Label></td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                        <asp:Label ID="rptPiutang" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        &nbsp;<asp:Label ID="mnuManGiroInStatus" runat="server"></asp:Label>
</td>
                                                    <td align="center" style="vertical-align: middle; width: 144px; text-align: center">
&nbsp;<asp:Label ID="mnuManGiroOutStatus" runat="server"></asp:Label>
</td>
                                                    <td align="center">
                                                        <asp:Label ID="mnugiroin" runat="server"></asp:Label>
</td>
                                                    <td align="center">
                                                        <asp:Label ID="mnugiroout" runat="server"></asp:Label></td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                        <asp:Label ID="rptTB" runat="server"></asp:Label></td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                        <asp:Label ID="mnuRealisasiVoucher" runat="server"></asp:Label></td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                    <asp:Label ID="mnuWIPStock" runat="server" Font-Bold="False"></asp:Label></td>
                                                    <td align="center" style="vertical-align: middle; text-align: center">
                                                    <asp:Label ID="mnuPackList" runat="server" Font-Bold="False"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
<asp:Label ID="mnuPiutangGantung" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        &nbsp;<asp:Label ID="MnuMatUsagePrice" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuBiaya" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuNotaPiutangGantung" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuNrc" runat="server"></asp:Label></td>
                                                    <td align="center">
                                                        <asp:Label ID="mnuDNCN" runat="server"></asp:Label>&nbsp;</td>
                                                    <td align="center">
                                                        &nbsp;</td>
                                                    <td align="center">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    
</ContentTemplate>
</ajaxToolkit:TabPanel>
                                
</ajaxToolkit:TabContainer>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
