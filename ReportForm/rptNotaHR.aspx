<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptNotaHR.aspx.vb" Inherits="rptNotaHR" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Nota Piutang Gantung"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server" RenderMode="Inline">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=center><TABLE width="100%"><TBODY><TR><TD id="TD10" align=left runat="server" Visible="false"><asp:Label style="WHITE-SPACE: nowrap" id="Label1" runat="server" Text="Cabang" __designer:wfdid="w1"></asp:Label></TD><TD id="TD8" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="dCabangNya" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w49"></asp:DropDownList></TD></TR><TR><TD id="TD11" align=left runat="server" Visible="false"><asp:Label style="WHITE-SPACE: nowrap" id="Label5" runat="server" Text="Tipe Nota" __designer:wfdid="w1"></asp:Label></TD><TD id="TD9" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="TypeNota" runat="server" CssClass="inpText" __designer:wfdid="w50"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>Normal</asp:ListItem>
<asp:ListItem>Selisih</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD18" align=left runat="server" Visible="true"><asp:Label style="WHITE-SPACE: nowrap" id="Label2" runat="server" Text="Tipe Laporan" __designer:wfdid="w1"></asp:Label></TD><TD id="TD3" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="type" runat="server" Width="112px" CssClass="inpText" __designer:wfdid="w2" AutoPostBack="True"><asp:ListItem Value="Detail HR">Detail By Nota HR</asp:ListItem>
<asp:ListItem Value="Detail PI">Detail By No. PI</asp:ListItem>
<asp:ListItem>Voucher OS</asp:ListItem>
<asp:ListItem Enabled="False">Summary</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode" __designer:wfdid="w3"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="55px" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;- <asp:TextBox id="dateAkhir" runat="server" Width="55px" CssClass="inpText" __designer:wfdid="w6"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w8">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w9" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w10" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w11" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w12" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left runat="server" Visible="true">Supplier</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="suppname" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupplier" onclick="btnSearchSupplier_click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w3"></asp:ImageButton> <asp:ImageButton id="btnEraseSupplier" onclick="btnEraseSupplier_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w4"></asp:ImageButton> <asp:Label id="suppoid" runat="server" Visible="False" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD align=left runat="server" Visible="true"></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="gvSupp" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w22" PageSize="8" GridLines="None" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged1" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="suppcode,suppoid,suppname,supptype" UseAccessibleHeader="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#804040" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supplier">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="supptype" HeaderText="Tipe" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="Td1" align=left runat="server" Visible="true">No Nota HR</TD><TD id="Td2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="nota" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w13"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w14"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w15"></asp:ImageButton>&nbsp;<asp:Label id="oid" runat="server" Visible="False" __designer:wfdid="w16"></asp:Label></TD></TR><TR><TD align=left runat="server" Visible="true"></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="GVNota" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w17" PageSize="8" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="trnnotahroid,trnnotahrno" UseAccessibleHeader="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#804000" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnnotahrno" HeaderText="No Nota">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnotahrdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal Nota">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="Td4" align=left runat="server" Visible="true"><asp:Label id="Label3" runat="server" Text="No. PI" __designer:wfdid="w23"></asp:Label></TD><TD id="Td5" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="txtSjNo" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w24"></asp:TextBox>&nbsp;<asp:ImageButton id="crISjno" onclick="crISjno_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w25"></asp:ImageButton> <asp:ImageButton id="DelSjNo" onclick="DelSjNo_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w26"></asp:ImageButton> <asp:Label id="trnbelimstoid" runat="server" Visible="False" __designer:wfdid="w6"></asp:Label></TD></TR><TR><TD id="Td6" align=left runat="server" Visible="true"></TD><TD id="Td7" align=left colSpan=3 runat="server" Visible="true"><asp:GridView id="GvSj" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w27" PageSize="8" GridLines="None" OnSelectedIndexChanged="GvSj_SelectedIndexChanged" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="trnbelino,trnbelidate,suppname,trnbelimstoid" UseAccessibleHeader="False" CellPadding="4" OnPageIndexChanging="GvSj_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#804040" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="No. PI">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp. Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR id="trlokasi" runat="server" visible="false"><TD id="TD14" align=left runat="server" Visible="false"><asp:Label id="labellokasi" runat="server" Text="Lokasi" __designer:wfdid="w28"></asp:Label></TD><TD id="TD13" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="ddlLocation" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w29" AutoPostBack="True"><asp:ListItem>ALL LOCATION</asp:ListItem>
</asp:DropDownList></TD></TR><TR id="tritem" runat="server" visible="false"><TD id="TD12" align=left runat="server" Visible="false"><asp:Label style="WHITE-SPACE: nowrap" id="labelitem" runat="server" Text="Item / Barang" __designer:wfdid="w30"></asp:Label></TD><TD id="TD17" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="itemname" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w31"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w32"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w33"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w34"></asp:Label><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w35" PageSize="8" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3,merk" UseAccessibleHeader="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Size="X-Small" ForeColor="#804040" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Satuan" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR id="trmerk" runat="server" visible="false"><TD id="TD15" align=left runat="server" Visible="false">Merk</TD><TD id="TD16" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="merk" runat="server" Width="238px" CssClass="inpTextDisabled" __designer:wfdid="w36" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=right></TD><TD align=left colSpan=3><asp:DropDownList id="period" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w37"></asp:DropDownList><asp:DropDownList id="FilterDDLGrup" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w38"></asp:DropDownList><asp:DropDownList id="FilterDDLSubGrup" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w39"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton> <asp:ImageButton id="btntopdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w41"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w42"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w43"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w44" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w45"></asp:Image><BR __designer:dtid="844424930132047" /><SPAN style="FONT-SIZE: 12pt">Sedang proses....</SPAN><BR /></SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="labelEx" runat="server" Width="98px" __designer:wfdid="w46"></asp:Label> <asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w47"></asp:Label></TD></TR></TBODY></TABLE>&nbsp;&nbsp; </TD></TR></TBODY></TABLE>&nbsp; <CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w48" AutoDataBind="true"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btntopdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

