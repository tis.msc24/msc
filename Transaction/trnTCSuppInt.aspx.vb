Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnTCSuppInt
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Function"
    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        Return ToDouble(GetStrData(sSql))
    End Function
#End Region

#Region "Procedure"

    Private Sub fCabang()
        If DDLfCabang.SelectedValue = "ToBranch" Then
            sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(drCabang, sSql)
            ElseIf Session("UserLevel") = 2 Then
                If Session("branch_id") <> "10" Then
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(drCabang, sSql)
                Else
                    FillDDL(drCabang, sSql)
                    drCabang.Items.Add(New ListItem("ALL", "ALL"))
                    drCabang.SelectedValue = "ALL"
                End If
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                FillDDL(drCabang, sSql)
                drCabang.Items.Add(New ListItem("ALL", "ALL"))
                drCabang.SelectedValue = "ALL"
            End If
        Else
            sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang'"
            FillDDL(drCabang, sSql)
            drCabang.Items.Add(New ListItem("ALL", "ALL"))
            drCabang.SelectedValue = "ALL"
        End If

    End Sub

    Private Sub TujuanCabDDL()
        sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ToBranch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ToBranch, sSql)
            Else
                FillDDL(ToBranch, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ToBranch, sSql)
        End If
    End Sub

    Private Sub IniDDLTolocoid()
        sSql = "Select a.genoid, a.gendesc from QL_mstgen a Where a.gengroup='LOCATION' and a.genother6 = 'RUSAK' AND a.genother2 IN (Select genoid from QL_mstgen br Where br.genoid=a.genother2 AND br.gencode='" & ToBranch.SelectedValue & "' AND gengroup='CABANG') "
        FillDDL(ToloCation, sSql)
    End Sub

    Private Sub DDLuNIt()
        sSql = "Select genoid,gendesc from QL_mstgen Where gengroup='ITEMUNIT' AND genoid=945"
        FillDDL(unit, sSql)
    End Sub

    Private Sub FillTextbox()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim transdate As New Date
        Dim fromlocationoid As Integer = 0 : Dim tolocationoid As Integer = 0

        sSql = "SELECT TOP 1 a.trntcserviceoid, a.trntcserviceno InTransferNo, a.trntcservicedate trntrfdate, tw.TrfwhserviceNo trnTrfToReturNo, a.tomtrlocoid, a.trntcservicenote Trntrfnote, a.trntcstatus status, a.updtime, a.upduser, a.FromBranch, a.ToBranch, tw.FromMtrLocOid FromMtrloc, a.createtime, tw.Trfwhserviceoid FROM ql_trntcsuppintmst a INNER jOIN ql_trfwhservicemst tw ON tw.Trfwhserviceoid=a.Trfwhserviceoid AND tw.Trfwhservicetype='INTERNSUPP' WHERE a.cmpcode = '" & cmpcode & "' AND a.trntcserviceoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                ToBranch.SelectedValue = xreader("tobranch")
                transferno.Text = xreader("InTransferNo")
                transdate = xreader("trntrfdate")
                transferdate.Text = Format(xreader("trntrfdate"), "dd/MM/yyyy")
                noref.Text = xreader("trnTrfToReturNo")
                fromlocation.Text = xreader("FromMtrloc")
                IniDDLTolocoid()
                ToloCation.SelectedValue = xreader("tomtrlocoid")
                note.Text = xreader("Trntrfnote")
                trnstatus.Text = xreader("status").ToString.Trim
                upduser.Text = xreader("upduser")
                trfmtrmstoid.Text = xreader("Trfwhserviceoid")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                AsalCabang.Text = xreader("FromBranch")
            End While
        Else
            showMessage("Error...!!", 2)
        End If
        xreader.Close()
        sSql = "Select a.genoid, a.gendesc from QL_mstgen a Where a.gengroup='LOCATION' and a.genother6 = 'RUSAK' AND a.genother2 IN (Select genoid from QL_mstgen br Where br.genoid=a.genother2 AND br.gencode='" & ToBranch.SelectedValue & "' AND gengroup='CABANG') "
        FillDDL(ToloCation, sSql)

        sSql = "SELECT a.trfwhserviceoid, ROW_NUMBER() OVER(ORDER BY a.trntcservicedtloid ) seq, a.refoid AS itemoid, b.itemdesc, b.merk, a.trntcservicedtlqty qty, a.unitoid AS satuan, b.satuan1, c.gendesc AS unit, d.gendesc AS unit1, a.trntcservicedtlnote AS note, 0 sisaretur, (select trfwhserviceqty from ql_trfwhservicedtl trd Where trd.itemoid = b.itemoid and trd.trfwhserviceoid = a.trfwhserviceoid AND a.trfwhservicedtloid=trd.trfwhservicedtloid) trfqty, a.trfwhservicedtloid, a.ReqDtlOid, a.ReqOid, (Select snno from QL_TRNREQUESTDTL rd Where rd.reqdtloid=a.reqdtloid) snno FROM ql_trntcsuppintdtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.unitoid = c.genoid INNER JOIN QL_mstgen d ON b.cmpcode = d.cmpcode AND b.satuan1 = d.genoid AND d.gengroup='ITEMUNIT' WHERE a.cmpcode = '" & cmpcode & "' AND a.trntcserviceoid =" & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "detailts")
        GVItemDetail.DataSource = dtab : GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        If trnstatus.Text = "In Process" Then
            btnSave.Visible = True : btnDelete.Visible = True
            BtnCancel.Visible = True : btnPosting.Visible = True
        ElseIf trnstatus.Text = "Post" Then
            btnSave.Visible = False : btnDelete.Visible = False
            BtnCancel.Visible = True : btnPosting.Visible = False
        End If
        conn.Close()
    End Sub

    Private Sub BindData()
        Dim sWhere As String = "" : Dim sWhere1 As String = ""

        sSql = "SELECT trntcserviceoid InTransferoid,trntcserviceno InTransferNo,trntcservicedate trntrfdate, upduser,trntcstatus status,(SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=c.ToBranch AND tb.cmpcode=c.cmpcode AND gengroup='CABANG') ToBranch,(SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=c.FromBranch AND tb.cmpcode=c.cmpcode AND gengroup='CABANG') FromBranch FROM ql_trntcsuppintmst c WHERE c.cmpcode = '" & cmpcode & "' AND c.trntcserviceno LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"

        If ddlStatus.SelectedValue.ToUpper <> "ALL" Then
            sSql &= " AND status='" & ddlStatus.SelectedValue.ToUpper & "'"
        End If

        If CbTanggal.Checked = True Then
            sSql &= " AND CONVERT(varchar(10), c.trntcservicedate, 103) BETWEEN '" & tgl1.Text & "' AND '" & tgl2.Text & "'"
        End If

        If drCabang.SelectedValue <> "ALL" Then
            If DDLfCabang.SelectedValue = "ToBranch" Then
                sSql &= " AND c.ToBranch='" & drCabang.SelectedValue & "'"
            Else
                sSql &= " AND c.FromBranch='" & drCabang.SelectedValue & "'"
            End If
        End If
        sSql &= " ORDER BY trntcserviceoid Desc"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "ts")
        gvMaster.DataSource = dtab
        gvMaster.DataBind()
        Session("ts") = dtab
    End Sub

    Private Sub bindDataPO()
        Try
            sSql = "Select * From (Select trm.Trfwhserviceoid, trm.TrfwhserviceNo trnTrfToReturNo, trm.Trfwhservicedate AS trntrfdate, trm.Trfwhservicenote trfmtrnote, trm.ToMtrlocOid, trm.fromBranch fromMtrBranch, trm.toBranch toMtrBranch, sum(trfwhserviceqty) totalqty, ISNULL(SUM(totaluse), 0.00) totaluse, trm.FromMtrlocoid From ql_trfwhservicemst trm inner join ql_trfwhservicedtl twd on twd.trfwhserviceoid=trm.Trfwhserviceoid Left Join (Select trfwhservicedtloid, trfwhserviceoid, reqoid, reqdtloid, SUM(trntcservicedtlqty) totaluse from ql_trntcsuppintdtl tcd Group BY trfwhservicedtloid, trfwhserviceoid, reqoid, reqdtloid) tcd ON twd.trfwhservicedtloid=tcd.trfwhservicedtloid AND tcd.reqoid=twd.reqoid AND tcd.reqdtloid=twd.reqdtloid AND twd.trfwhserviceoid=trm.Trfwhserviceoid Where trm.cmpcode = '" & cmpcode & "' And trm.Trfwhservicestatus = 'Approved' AND trm.Trfwhservicetype='INTERNSUPP' And trm.toBranch = '" & ToBranch.SelectedValue & "' AND isnull(trm.trfwhserviceres1,'') = '' And trm.TrfwhserviceNo like '%" & Tchar(noref.Text) & "%' Group BY trm.Trfwhserviceoid, trm.TrfwhserviceNo, trm.Trfwhservicedate, trm.Trfwhservicenote, trm.ToMtrlocOid, trm.fromBranch, trm.toBranch, trm.FromMtrlocoid ) Tc Where totalqty-totaluse>0.00"
            Dim dtab As DataTable = ckon.ambiltabel(sSql, "polist")
            GVpo.DataSource = dtab : GVpo.DataBind()
            Session("polistwhsupplier") = dtab
        Catch ex As Exception
            showMessage(ex.ToString & "<br/> " & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindItem()
        Try
            sSql = "Select * from (Select trd.trfwhserviceoid, trd.trfwhservicedtloid, i.itemoid, i.itemcode, i.itemdesc, i.merk, 0.00 as sisa, 945 unitoid, 'BUAH' as unit, trm.trfwhserviceno trnTrfToReturNo, trd.trfwhserviceqty-ISNULL(QtyTc,0.00) sisaretur, trd.reqoid, trd.reqdtloid, (Select snno From QL_TRNREQUESTDTL rd Where rd.reqdtloid=trd.reqdtloid) snno From ql_mstitem i Inner Join ql_trfwhservicedtl trd on i.itemoid = trd.itemoid inner join ql_trfwhservicemst trm On trd.trfwhserviceoid = trm.trfwhserviceoid Left Join (Select ms.FromBranch, ms.trfwhserviceoid, m.trntcserviceoid, m.trfwhservicedtloid, m.refoid, m.reqdtloid, Isnull(sum(trntcservicedtlqty), 0.0) QtyTc From ql_trntcservicedtl m Inner Join ql_trntcsuppintmst ms ON ms.trntcserviceoid=m.trntcserviceoid Group BY m.trntcserviceoid, ms.trfwhserviceoid, m.trfwhservicedtloid, m.refoid, m.reqdtloid, ms.FromBranch) m ON m.refoid=trd.itemoid AND trm.Trfwhserviceoid=m.trfwhserviceoid AND m.reqdtloid=trd.reqdtloid AND m.trfwhservicedtloid=trd.trfwhservicedtloid AND m.FromBranch=trm.FromBranch Where i.cmpcode = '" & cmpcode & "' And trm.trfwhserviceno like '%" & Tchar(noref.Text) & "%' AND isnull(trd.trfwhservicedtlres1,'') = '' And (i.itemdesc LIKE '%" & Tchar(item.Text.Trim) & "%' OR i.merk LIKE '%" & Tchar(item.Text.Trim) & "%') AND trfwhservicedtlres1<>'Complete') cok Where sisaretur>0"
            Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
            GVItemList.DataSource = objTable
            GVItemList.DataBind() : Session("itemlistwhretur") = objTable
        Catch ex As Exception
            showMessage(ex.ToString & "<br /> " & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\trnTCSuppInt.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Transfer Confirm Service"

        If Not Page.IsPostBack Then
            createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
            fCabang() : BindData()
            transferno.Text = GenerateID("ql_trntcsuppintmst", cmpcode)
            transferdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            TujuanCabDDL() : IniDDLTolocoid() : DDLuNIt()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
                tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0

                qty.Text = "0.00" : GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                labelseq.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        CbTanggal.Checked = False
        tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
        tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        'drCabang.SelectedValue = "ALL"
        'ddlStatus.SelectedValue = "ALL"
        FilterText.Text = ""
        BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Tanggal period harus diisi !", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Tanggal period 1 tidak valid !", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Tanggal period 2 tidak valid !", 2)
            Exit Sub
        End If

        If date1 > date2 Then
            showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
            Exit Sub
        End If
        BindData()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        If noref.Text.Trim = "" Then
            showMessage("Pilih nomor (TW) terlebih dahulu!", 2)
            Exit Sub
        End If

        If noref.Text <> pooid.Text Then
            showMessage("Pilih nomor (TW) terlebih dahulu!", 2)
            Exit Sub
        End If
        BindItem() : GVItemList.Visible = True
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = "" : labelitemoid.Text = "" : qty.Text = "0.00"
        labelmaxqty.Text = "0.00" : merk.Text = "" : notedtl.Text = ""

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        GVItemList.PageIndex = e.NewPageIndex
        BindItem()
        GVItemList.Visible = True
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        item.Text = GVItemList.SelectedDataKey("itemdesc").ToString
        labelitemoid.Text = GVItemList.SelectedDataKey("itemoid")
        merk.Text = GVItemList.SelectedDataKey("merk").ToString
        labelmaxqty.Text = ToMaskEdit(GVItemList.SelectedDataKey("sisaretur"), 3)
        qty.Text = ToMaskEdit(GVItemList.SelectedDataKey("sisaretur"), 3)
        labelsatuan.Text = GVItemList.SelectedDataKey("unitoid")
        trfmtrmstoid.Text = GVItemList.SelectedDataKey("trfwhserviceoid")
        trfwhservicedtloid.Text = GVItemList.SelectedDataKey("trfwhservicedtloid")
        ReqOid.Text = GVItemList.SelectedDataKey("reqoid")
        ReqDtlOid.Text = GVItemList.SelectedDataKey("reqdtloid")
        unit.SelectedValue = GVItemList.SelectedDataKey("unitoid")
        snno.Text = GVItemList.SelectedDataKey("snno")

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        GVItemList.Visible = False
        GVItemList.DataSource = Nothing
        GVItemList.DataBind()
        unit_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnTCSuppInt.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        qty.Text = ToMaskEdit(ToDouble(qty.Text), 4)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Dim sMsg As String = ""
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            sMsg &= "- Maaf, Tolong pilih katalognya dulu..!!<br />"
        End If

        If Double.Parse(qty.Text.Trim) = 0.0 Then
            sMsg &= "- Maaf, tolong isi Qty dulu..!!<br />"
        End If

        If Double.Parse(qty.Text.Trim) > Double.Parse(labelmaxqty.Text.Trim) Then
            sMsg &= "- Maaf, Tolong Qty tidak boleh lebih dari Nilai Max Quantity..!!<br />"
        End If

        If sMsg <> "" Then
            trnstatus.Text = "In Process"
            showMessage(sMsg, 2)
            Exit Sub
        End If

        Dim dtab As DataTable
        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("trfqty", Type.GetType("System.Double"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("note", Type.GetType("System.String"))
                dtab.Columns.Add("sisaretur", Type.GetType("System.Double"))
                dtab.Columns.Add("trfwhserviceoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("trfwhservicedtloid", Type.GetType("System.Int32"))
                dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
                dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("snno", Type.GetType("System.String"))
                Session("itemdetail") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetail")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "' AND seq <> " & Integer.Parse(labelseq.Text) & " AND trfwhservicedtloid=" & Integer.Parse(trfwhservicedtloid.Text) & " And reqdtloid=" & Integer.Parse(ReqDtlOid.Text) & "")
            If drowc.Length > 0 Then
                sMsg &= "- Maaf, Item ini sudah ditambahkan di dalam list..!!<br />"
            End If
        End If

        If sMsg <> "" Then
            trnstatus.Text = "In Process"
            showMessage(sMsg, 2)
            Exit Sub
        End If

        Dim drow As DataRow : Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow
            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("trfqty") = Double.Parse(labelmaxqty.Text)
            drow("satuan") = unit.SelectedValue
            drow("unit") = unit.SelectedItem.Text
            drow("note") = notedtl.Text.Trim
            drow("sisaretur") = Double.Parse(labelmaxqty.Text)
            drow("trfwhserviceoid") = trfmtrmstoid.Text
            drow("trfwhservicedtloid") = trfwhservicedtloid.Text
            drow("ReqOid") = ReqOid.Text
            drow("ReqDtlOid") = ReqDtlOid.Text
            drow("snno") = snno.Text
            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("trfqty") = Double.Parse(labelmaxqty.Text)
            drow("satuan") = unit.SelectedValue
            drow("unit") = unit.SelectedItem.Text
            drow("note") = notedtl.Text.Trim
            drow("sisaretur") = Double.Parse(labelmaxqty.Text)
            drow("trfwhserviceoid") = trfmtrmstoid.Text
            drow("trfwhservicedtloid") = trfwhservicedtloid.Text
            drow("ReqOid") = ReqOid.Text
            drow("ReqDtlOid") = ReqDtlOid.Text
            drow("snno") = snno.Text
            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
        merk.Text = "" : qty.Text = "0.00" : labelmaxqty.Text = "0.00"
        unit.SelectedIndex = -1 : I_u2.Text = "new"
        notedtl.Text = "" : labelitemoid.Text = "" : item.Text = ""
        trfwhservicedtloid.Text = ""
        ReqOid.Text = "" : ReqDtlOid.Text = ""
        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new" : labelitemoid.Text = "" : qty.Text = "0.00"
        item.Text = "" : labelmaxqty.Text = "0.00"
        'Unit.Items.Clear()
        notedtl.Text = "" : merk.Text = "" : trfmtrmstoid.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)
        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If
        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()
        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        item.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(2).Text
        qty.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(4).Text

        notedtl.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(6).Text.Replace("&nbsp;", "")
        merk.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(3).Text
        unit.SelectedValue = GVItemDetail.SelectedDataKey("satuan")
        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text
        labelmaxqty.Text = ToMaskEdit(GVItemDetail.SelectedDataKey("sisaretur"), 3)
        trfmtrmstoid.Text = Integer.Parse(GVItemDetail.SelectedDataKey("trfwhserviceoid"))
        trfwhservicedtloid.Text = (GVItemDetail.SelectedDataKey("trfwhservicedtloid"))
        ReqDtlOid.Text = Integer.Parse(GVItemDetail.SelectedDataKey("ReqDtlOid"))
        ReqOid.Text = Integer.Parse(GVItemDetail.SelectedDataKey("ReqOid"))
        snno.Text = GVItemDetail.SelectedDataKey("snno").ToString
        Dim maxqty As Double = ToDouble(labelmaxqty.Text)
        labelmaxqty.Text = ToMaskEdit(GVItemDetail.SelectedDataKey("trfqty"), 3)

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        GVItemDetail.Columns(7).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""
        Dim period As String = GetDateToPeriodAcctg(GetServerTime())

        If fromlocation.Text = 0 Then
            errmsg &= "- Maaf, Lokasi asal transfer tidak boleh kosong..!! <br />"
        End If
        If ToloCation.Items.Count <= 0 Then
            errmsg &= "- Maaf, Lokasi tujuan transfer tidak boleh kosong..!! <br />"
        End If

        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "- Maaf, Detail transfer tidak boleh kosong..!! <br />"
        End If

        If Session("itemdetail") Is Nothing Then
            errmsg &= "- Maaf, ada kemungkinan anda klik tombol save dua kali,, cek data yang anda input jika data terinput double segera info ke admin..!!<br />"
        End If

        'Cek period is open stock
        sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
        If GetScalar(sSql) = 0 Then
            errmsg &= "Tanggal ini tidak dalam periode Open Stock..!!<br />"
        End If

        'Cek saldoakhir
        If Not Session("itemdetail") Is Nothing Then
            Dim saldoakhire As Double = 0.0
            Dim dtab As DataTable = Session("itemdetail")
            For j As Integer = 0 To dtab.Rows.Count - 1
                sSql = "Select ISNULL(SUM(qtyin)-SUM(qtyOut),0.00) From QL_conmtr Where mtrlocoid = -9 AND branch_code='" & ToBranch.SelectedValue & "' AND conrefoid=" & dtab.Rows(j).Item("ReqDtlOid") & ""
                saldoakhire = GetScalar(sSql)
                If saldoakhire = Nothing Or saldoakhire = 0 Then
                    errmsg &= "- Maaf, Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena qty " & ToMaskEdit(saldoakhire, 3) & "..!!<br />"
                End If
            Next
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM ql_trntcsuppintmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                errmsg &= "- Maaf, Data sudah tersimpan, silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!<BR />"
            End If
        Else
            sSql = "SELECT trntcstatus FROM ql_trntcsuppintmst WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = GetScalar(sSql)
            If srest Is Nothing Or srest = "" Then
                errmsg &= " - Maaf, Data transfer service tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain..!!<br >"
            Else
                If srest = "post" Then
                    errmsg &= "- Maaf, Data transfer service tidak dapat diposting !<br />Periksa bila data telah diposting oleh user lain..!!<br />"
                End If
            End If
        End If

        If errmsg.Trim <> "" Then
            trnstatus.Text = "In Process"
            showMessage(errmsg, 2)
            Exit Sub
        End If

        Dim trnmtrmstoid As Int32 = GenerateID("ql_trntcsuppintmst", cmpcode)
        Dim trnmtrdtloid As Int32 = GenerateID("ql_trntcsuppintdtl", cmpcode)
        Dim conmtroid As Int32 = GenerateID("QL_conmtr", cmpcode)
        Dim glsequence As Integer = 1

        '--- Open Koneksi ke database ---
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            'Generate transfer number
            If trnstatus.Text = "Post" Then
                Dim iCurID As Integer = 0
                sSql = "select genother1 From ql_mstgen Where gencode='" & ToBranch.Text & "' And gengroup='CABANG'"
                Dim cabang As String = GetScalar(sSql)
                Dim trnno As String = "TCI/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trntcserviceno, 4) AS INT)), 0) FROM ql_trntcsuppintmst WHERE cmpcode = '" & cmpcode & "' AND trntcserviceno LIKE '" & trnno & "%'"

                xCmd.CommandText = sSql : iCurID = xCmd.ExecuteScalar + 1
                trnno = GenNumberString(trnno, "", iCurID, 4)
                transferno.Text = trnno
            Else
                If i_u.Text = "new" Then
                    transferno.Text = trnmtrmstoid.ToString
                End If
            End If

            If i_u.Text = "new" Then
                'Insert to master
                sSql = "INSERT INTO [ql_trntcsuppintmst]([cmpcode],[trntcserviceoid],[trntcserviceno],[Trfwhserviceoid],[trntcservicedate],[personoid],[FromBranch],[ToBranch],[frommtrlocoid],[tomtrlocoid],[trntcservicenote],[trntcstatus],[createtime],[createuser],[updtime],[upduser]) VALUES " & _
                "('" & cmpcode & "'," & trnmtrmstoid & ",'" & trnmtrmstoid & "','" & Integer.Parse(trfmtrmstoid.Text) & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),0,'" & AsalCabang.Text & "','" & ToBranch.SelectedValue & "',-9," & Integer.Parse(ToloCation.SelectedValue) & ",'" & Tchar(note.Text.Trim) & "','" & trnstatus.Text & "',Current_timestamp,'" & Session("UserID") & "',Current_timestamp,'" & Session("UserID") & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_trntrfmtrmst
                sSql = "UPDATE QL_mstoid SET lastoid = " & trnmtrmstoid & " WHERE tablename = 'ql_trntcsuppintmst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                'Update master record
                sSql = "UPDATE [ql_trntcsuppintmst] SET [trntcserviceno] ='" & transferno.Text & "', [trntcservicedate] =(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), [FromBranch] ='" & AsalCabang.Text & "',[ToBranch] ='" & ToBranch.SelectedValue & "',[tomtrlocoid] =" & Integer.Parse(ToloCation.SelectedValue) & ",[trntcservicenote] ='" & Tchar(note.Text.Trim) & "',[trntcstatus] = '" & trnstatus.Text & "', [updtime] =Current_timestamp,[upduser] ='" & Session("UserID") & "' WHERE [trntcserviceoid]=" & Integer.Parse(Session("oid")) & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM [ql_trntcsuppintdtl] WHERE [trntcserviceoid] = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0 : Dim qty_to As Double = 0.0
                Dim amthpp As Double = 0.0 : Dim COA_gudang As Integer = 0
                Dim vargudang As String = "" : Dim iMatAccount As String = ""

                For i As Integer = 0 To objTable.Rows.Count - 1
                    If i_u.Text = "edit" Then
                        trnmtrmstoid = Integer.Parse(Session("oid"))
                    End If
                    qty_to = objTable.Rows(i).Item("qty")

                    sSql = "INSERT INTO [ql_trntcsuppintdtl] ([cmpcode], [trntcservicedtloid], [trntcserviceoid], [trfwhserviceoid], [trntcservicedtlseq], [refname], [refoid], [frommtrlocoid], [tomtrlocoid],[unitseq], [trntcservicedtlqty], [unitoid], [trntcservicedtlnote], [trfwhservicedtloid], [reqoid], [reqdtloid]) VALUES " & _
                    "('" & cmpcode & "', " & trnmtrdtloid & ", '" & Integer.Parse(trnmtrmstoid) & "', " & trfmtrmstoid.Text & ", " & objTable.Rows(i).Item("seq") & ", '', " & objTable.Rows(i).Item("itemoid") & ", -9, " & Integer.Parse(ToloCation.SelectedValue) & ", " & unitseq & ", " & ToDouble(objTable.Rows(i).Item("qty")) & ", 945, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & Integer.Parse(objTable.Rows(i).Item("trfwhservicedtloid")) & ", " & Integer.Parse(objTable.Rows(i).Item("reqoid")) & ", " & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()  

                    If trnstatus.Text = "Post" Then

                        sSql = "UPDATE ql_trfwhservicedtl SET trfwhservicedtlres1='' WHERE cmpcode='" & cmpcode & "' AND trfwhservicedtloid IN (SELECT trfwhservicedtloid FROM ql_trntcservicedtl WHERE cmpcode='" & cmpcode & "' AND trntcserviceoid=" & Integer.Parse(Session("oid")) & ") AND trfwhserviceoid IN (SELECT trfwhserviceoid FROM ql_trntcservicemst WHERE cmpcode='" & cmpcode & "' AND trntcserviceoid=" & Integer.Parse(Session("oid")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "UPDATE ql_trfwhservicemst SET trfwhserviceres1='' WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid IN (SELECT trfwhserviceoid FROM ql_trntcservicemst WHERE cmpcode='" & cmpcode & "' AND trntcserviceoid=" & Integer.Parse(Session("oid")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        If ToDouble(objTable.Rows(i).Item("qty").ToString) >= ToDouble(objTable.Rows(i).Item("trfqty").ToString) Then
                            sSql = "UPDATE ql_trfwhservicedtl SET trfwhservicedtlres1 ='Complete' WHERE cmpcode='" & cmpcode & "' AND trfwhservicedtloid=" & Integer.Parse(objTable.Rows(i).Item("trfwhservicedtloid")) & " AND Trfwhserviceoid=" & trfmtrmstoid.Text & ""
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            sSql = "UPDATE ql_trfwhservicemst SET trfwhserviceres1='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid=" & Integer.Parse(objTable.Rows(i).Item("trfwhserviceoid")) & " AND (SELECT COUNT(*) FROM ql_trfwhservicedtl WHERE cmpcode='" & cmpcode & "' AND trfwhservicedtlres1='' AND trfwhserviceoid=" & Integer.Parse(objTable.Rows(i).Item("trfwhserviceoid")) & " AND trfwhservicedtloid <>" & Integer.Parse(objTable.Rows(i).Item("trfwhservicedtloid")) & ")=0"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If

                        '--------------------------
                        sSql = "Select hpp from ql_mstitem Where itemoid=" & Integer.Parse(objTable.Rows(i).Item("itemoid")) & "" : xCmd.CommandText = sSql
                        Dim lasthpp As Double = xCmd.ExecuteScalar
                        '---------------------------
                        amthpp = qty_to * lasthpp

                        'Update crdmtr for item out
                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note,HPP,Branch_Code,conrefoid) VALUES " & _
                        "('" & cmpcode & "', " & conmtroid & ", 'TCI',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & transferno.Text & "', " & Integer.Parse(trnmtrdtloid) & ", 'ql_trntcsuppintdtl', " & Integer.Parse(objTable.Rows(i).Item("itemoid")) & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & -9 & ", 0, " & objTable.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & ToDouble(lasthpp) & ",'" & ToBranch.SelectedValue & "'," & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1

                        'Update crdmtr for item in

                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,Branch_Code,conrefoid) VALUES " & _
                        "('" & cmpcode & "', " & conmtroid & ", 'TCI', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & transferno.Text & "', " & Integer.Parse(trnmtrdtloid) & ", 'ql_trntcsuppintdtl', " & Integer.Parse(objTable.Rows(i).Item("itemoid")) & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & ToloCation.SelectedValue & ", " & objTable.Rows(i).Item("qty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & ToDouble(lasthpp) & ",'" & ToBranch.SelectedValue & "'," & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                    End If
                    trnmtrdtloid += 1
                Next
                'Update lastoid ql_InTransferDtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & trnmtrdtloid & " WHERE tablename = 'ql_trntcsuppintdtl' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ''Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & " WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            trnstatus.Text = "In Process"
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & " " & sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("trnTCSuppInt.aspx?awal=true")
    End Sub

    Protected Sub unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unit.SelectedIndexChanged
        Dim maxqty As Double = Double.Parse(labelmaxqty.Text)
        labelmaxqty.Text = ToMaskEdit(maxqty, 4)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            Dim objTable As DataTable = Session("itemdetail")
            For i As Integer = 0 To objTable.Rows.Count - 1
                sSql = "UPDATE ql_trfwhservicedtl SET trfwhservicedtlres1 ='' WHERE cmpcode='" & cmpcode & "' AND trfwhservicedtloid=" & Integer.Parse(objTable.Rows(i).Item("trfwhservicedtloid")) & " AND trfwhserviceoid=" & trfmtrmstoid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            sSql = "UPDATE ql_trfwhservicemst SET trfwhserviceres1='' WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid=" & trfmtrmstoid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "SELECT trntcstatus FROM ql_trntcsuppintmst WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transfer gudang retur tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                Exit Sub
            Else
                If srest = "post" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer gudang retur tidak dapat dihapus !<br />Periksa bila data telah diposting oleh user lain", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM ql_trntcsuppintmst WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trntcsuppintdtl WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString & "" & sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("trnTCSuppInt.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If
        trnstatus.Text = "Post" : btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim tcOid As Integer = sender.ToolTip
            Dim sWhere As String = " Where tcm.trntcserviceoid = " & Integer.Parse(tcOid) & ""
            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "PrintOutTWI.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", Integer.Parse(tcOid))

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TCInternalPrint_" & tcOid & "")
            report.Close() : report.Dispose()
            Response.Redirect("trnTCSuppInt.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        gvMaster.PageIndex = e.NewPageIndex
        BindData()
    End Sub

    Protected Sub ibposearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposearch.Click
        bindDataPO() : GVpo.Visible = True
    End Sub

    Protected Sub ibpoerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoerase.Click
        noref.Text = "" : pooid.Text = ""

        If GVpo.Visible = True Then
            GVpo.Visible = False
            GVpo.DataSource = Nothing
            GVpo.DataBind()
            Session("polistwhsupplier") = Nothing
        End If

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub GVpo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVpo.PageIndexChanging
        If Not Session("polistwhsupplier") Is Nothing Then
            GVpo.DataSource = Session("polistwhsupplier")
            GVpo.PageIndex = e.NewPageIndex
            GVpo.DataBind()
        End If
    End Sub

    Protected Sub GVpo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVpo.SelectedIndexChanged
        noref.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        pooid.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        IniDDLTolocoid()
        ToloCation.SelectedValue = GVpo.SelectedDataKey("ToMtrlocOid")
        ToBranch.SelectedValue = GVpo.SelectedDataKey("toMtrBranch")
        AsalCabang.Text = GVpo.SelectedDataKey("fromMtrBranch")
        trfmtrmstoid.Text = GVpo.SelectedDataKey("Trfwhserviceoid")
        fromlocation.Text = "-9"
        GVpo.Visible = False : GVpo.DataSource = Nothing
        GVpo.DataBind()
        Session("polistwhsupplier") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub gvMaster_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvMaster.SelectedIndexChanging
        Response.Redirect("trnTcService.aspx?branch_code=" & gvMaster.SelectedDataKey("branch_code").ToString & "&oid=" & gvMaster.SelectedDataKey("trntcserviceoid") & "")
    End Sub

    Protected Sub DDLfCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLfCabang.SelectedIndexChanged
        fCabang()
    End Sub

    Protected Sub ToBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToBranch.SelectedIndexChanged
        IniDDLTolocoid()
        GVpo.Visible = False
        pooid.Text = "" : noref.Text = ""
    End Sub
#End Region
End Class
