<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstdimensi.aspx.vb" Inherits="mstdimensi" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
  
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="height: 28px">
                <asp:Label ID="HDRBARANG" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Navy" Text=".: Data Barang (Dimensi)"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">

                <contenttemplate></contenttemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<ajaxToolkit:TabContainer id="TabContainer1" runat="server" Width="100%" ActiveTabIndex="1"><cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"><HeaderTemplate>
                            <img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Daftar Barang (Dimensi) :.</span></strong>

                        
</HeaderTemplate>
<ContentTemplate>
<DIV style="WIDTH: 100%; TEXT-ALIGN: left">
    <asp:UpdatePanel id="UPanelListMaterial" runat="server" __designer:wfdid="w419"><ContentTemplate>
            <TABLE width="100%">
                <TBODY>
                    <TR>
                        <TD align=left class="Label">
                            <asp:Label ID="Label8" runat="server" __designer:wfdid="w420" 
                CssClass="normalFont" Text="Filter 1 :"></asp:Label>
                        </TD>
                        <TD align=left>
                            <asp:DropDownList ID="FilterDDL" runat="server" 
                __designer:wfdid="w421" CssClass="inpText" Width="100px">
                                <asp:ListItem Selected="True" Value="itemdesc">Nama Barang</asp:ListItem>
                                <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="FilterText" runat="server" 
                __designer:wfdid="w423" CssClass="inpText" MaxLength="30" Width="150px"></asp:TextBox>
                            &#160;<asp:ImageButton ID="btnSearch" runat="server" 
                __designer:wfdid="w424" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png">
                            </asp:ImageButton>
                            <asp:ImageButton ID="btnList" runat="server" 
                __designer:wfdid="w425" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrint" runat="server" 
                __designer:wfdid="w426" ImageAlign="AbsMiddle" ImageUrl="~/Images/print.png" 
                onclick="imbPrint_Click">
                            </asp:ImageButton>
                        </TD>
                    </TR>
                    <tr>
                        <td align="left" class="Label">
                            Status :</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlStatusView" runat="server" __designer:wfdid="w434" 
                                AutoPostBack="True" CssClass="inpText" Width="99px">
                                <asp:ListItem Value="Aktif">Tidak Aktif</asp:ListItem>
                                <asp:ListItem Value="Tidak Aktif">Aktif</asp:ListItem>
                            </asp:DropDownList>
                            View Top
                            <asp:TextBox ID="SelectTop" runat="server" __designer:wfdid="w435" 
                                CssClass="inpText" MaxLength="30" Width="30px">100</asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="SelectTop_FilteredTextBoxExtender" 
                                runat="server" __designer:wfdid="w436" Enabled="True" 
                                TargetControlID="SelectTop" ValidChars=",.0123456789">
                            </cc1:FilteredTextBoxExtender>
                            &#160;</td>
                    </tr>
                    <TR>
                        <td align="left" class="Label" colspan="2">
                            <asp:Panel ID="Panel1" runat="server" 
            __designer:wfdid="w439" DefaultButton="btnSearch">
                                <asp:GridView ID="GVmstgen" runat="server" __designer:wfdid="w111" 
                                    AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                                    CssClass="normalFont" DataKeyNames="itemoid" EnableModelValidation="True" 
                                    ForeColor="#333333" GridLines="None" Width="100%">
                                    <PagerSettings PageButtonCount="15" />
                                    <RowStyle BackColor="#FFFBD6" BorderColor="Cyan" ForeColor="#333333" />
                                    <Columns>
                                        <asp:HyperLinkField DataNavigateUrlFields="itemoid" 
                                            DataNavigateUrlFormatString="mstDimensi.aspx?oid={0}" DataTextField="itemcode" 
                                            HeaderText="Kode Barang" SortExpression="itemcode">
                                            <controlstyle font-size="Small" />
                                            <HeaderStyle CssClass="gvhdr" Font-Size="Small" ForeColor="Black" 
                                            HorizontalAlign="Left" Width="15%" Wrap="False" />
                                            <ItemStyle Font-Size="Small" HorizontalAlign="Left" Width="15%" />
                                        </asp:HyperLinkField>
                                        <asp:BoundField DataField="itemdesc" HeaderText="Nama Barang" 
                                            SortExpression="itemdesc">
                                            <HeaderStyle CssClass="gvhdr" Font-Size="Small" ForeColor="Black" 
                                            HorizontalAlign="Left" Width="65%" Wrap="False" />
                                            <ItemStyle Font-Size="Small" HorizontalAlign="Left" Width="65%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="JenisBarang" HeaderText="Jenis Barang">
                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left"/>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="dimensi_p" HeaderText="Panjang">
                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="dimensi_l" HeaderText="Lebar">
                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="dimensi_t" HeaderText="Tinggi">
                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="beratvolume" HeaderText="Berat Volume">
                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" Wrap="False" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="beratbarang" HeaderText="Berat Barang">
                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" Wrap="False" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnprintlist" runat="server" 
                                                    CommandArgument='<%#eval("itemoid") %>' ImageAlign="AbsMiddle" 
                                                    ImageUrl="~/Images/print.gif" OnClick="btnprintlist_Click" 
                                                    ToolTip='<%#eval("itemcode") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" Width="25px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333" 
                                        HorizontalAlign="Right" />
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label5" runat="server" CssClass="Important" 
                                            Text="Data Tidak Ditemukan !!!"></asp:Label>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </TR>
                    <TR>
                        <TD align=left colSpan=2 class="Label">
                            &#160;<asp:UpdateProgress ID="UpdateProgress1" runat="server" 
            __designer:wfdid="w437">
                                <progresstemplate>
                                    <br />
                                    <span class="normalFont">Please wait...</span>
                                    <br />
                                </progresstemplate>
                            </asp:UpdateProgress>
                        </TD>
                    </TR>
                </TBODY>
            </TABLE>
            <DIV style="DISPLAY: block; TEXT-ALIGN: center">
                &nbsp;</DIV>
        
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

    &nbsp; &nbsp;&nbsp;
</DIV>
</ContentTemplate>
</cc1:TabPanel>
&nbsp; &nbsp; &nbsp;&nbsp; (Dimensi)"></asp:Label></TH></TR></TBODY></TABLE><table>
</table><cc1:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel2"><HeaderTemplate>
                            <img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Barang (Dimensi) :.</span></strong>
                        </HeaderTemplate>
<ContentTemplate>
<TABLE><TBODY><!-- Grouping Barang-->
        <TR>
            <td align="left" colspan="4">

                &nbsp;
            </td>
        
            <td align="left" class="Label" style="WIDTH: 122px">
                <asp:Label ID="itemoid" runat="server" __designer:wfdid="w248" 
        CssClass="normalFont" Font-Size="Small" Visible="False" Width="4px"></asp:Label>





            </td>
            <td align="left">
                &nbsp;</td>
            <td align="left" style="WIDTH: 118px; COLOR: #000099">
                &nbsp;</td>
            <td align="left" style="COLOR: #000099">
                &nbsp;</td>
        </TR>
        <!-- end grouping barang -->
        <!-- Dimensi -->
        <tr>
            <td align="left" class="Label" style="WIDTH: 122px">
                <asp:Label ID="Label73" runat="server" __designer:wfdid="w253" 
                    CssClass="normalFont" Font-Size="Small" Text="Kode Item" Width="91px"></asp:Label>





            </td>
            <td align="left">
                <asp:TextBox ID="itemcode" runat="server" __designer:wfdid="w252" 
                    CssClass="inpTextDisabled" Enabled="False" MaxLength="20" size="20" 
                    Width="152px"></asp:TextBox>





            </td>
            <td align="left" style="WIDTH: 118px; COLOR: #000099">



                <asp:Label ID="UserName" runat="server" __designer:wfdid="w250" Visible="False"></asp:Label>





                <asp:Label ID="i_u" runat="server" __designer:wfdid="w249" Font-Names="Verdana" 
                    Font-Size="8pt" ForeColor="Red" Text="New" Visible="False"></asp:Label>





                </td>
            <td align="left" style="COLOR: #000099">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" class="Label" style="WIDTH: 122px">
                <asp:Label ID="Label64" runat="server" __designer:wfdid="w253" 
                    CssClass="normalFont" Font-Size="Small" Text="Nama Barang" Width="91px"></asp:Label>





            </td>
            <td align="left">
                <asp:TextBox ID="itemdesc" runat="server" __designer:wfdid="w254" 
                    CssClass="inpText" MaxLength="250" size="20" 
                    Width="208px" AutoPostBack="True" Font-Size="Small" Height="16px" 
                    style="TEXT-TRANSFORM: uppercase"></asp:TextBox>





                &nbsp;<asp:ImageButton ID="btnSearchItem" runat="server" __designer:wfdid="w820" 
                    Height="16px" ImageAlign="AbsMiddle" ImageUrl="~/Images/search.gif" 
                    Width="16px"></asp:ImageButton>





                &nbsp;<asp:ImageButton ID="btnDelItem" runat="server" __designer:wfdid="w821" 
                    ImageAlign="AbsMiddle" ImageUrl="~/Images/erase.bmp"></asp:ImageButton>





            </td>
            <td align="left" style="WIDTH: 118px; COLOR: #000099">
                &nbsp;</td>
            <td align="left" style="COLOR: #000099">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" class="Label" style="WIDTH: 122px">
                &nbsp;</td>
            <td align="left" ID="gv" colspan="3">
                <asp:GridView ID="gvItem" runat="server" __designer:wfdid="w756" 
                    AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                    DataKeyNames="itemcode,itemdesc,itemoid,satuan1,konversi1_2,merk,dimensi_p,dimensi_l,dimensi_t,beratbarang,beratvolume,stockflag,statusitem,itemflag,unitoid,keterangan,itemtype" 
                    EnableModelValidation="True" ForeColor="#333333" GridLines="None" PageSize="8" 
                    UseAccessibleHeader="False" Width="100%">
<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle ForeColor="Red" HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemtype" HeaderText="itemtype" Visible="False">
<HeaderStyle CssClass="gvhdr" />
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
                        <asp:Label ID="Label59" runat="server" ForeColor="Red" 
        Text="No data in database!!"></asp:Label>
                    
</EmptyDataTemplate>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<PagerStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333" HorizontalAlign="Right"></PagerStyle>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>
</asp:GridView>





            </td>
        </tr>
        <TR>
            <TD style="WIDTH: 122px" class="Label" align=left>
                <asp:Label ID="Label61" runat="server" __designer:wfdid="w256" 
        CssClass="normalFont" Font-Size="Small" Text="Grup Barang" Width="87px"></asp:Label>





            </TD>
            <TD align=left>
                <asp:DropDownList ID="itemgroupoid" runat="server" 
        __designer:wfdid="w257" AutoPostBack="True" CssClass="inpText" Enabled="False" 
        Font-Size="Small" OnSelectedIndexChanged="itemgroupoid_SelectedIndexChanged" 
        Width="207px"></asp:DropDownList>





            </TD>
            <td align="left" style="WIDTH: 118px">
                &nbsp;</td>
            <td align="left" valign="top">
                &nbsp;</td>
        </TR>
        <tr>
            <td align="left" class="Label" style="WIDTH: 122px">
                <asp:Label ID="Label60" runat="server" __designer:wfdid="w258" 
                    CssClass="normalFont" Font-Size="Small" Text="Tipe Barang" Width="109px"></asp:Label>





            </td>
            <td align="left">
                <asp:DropDownList ID="dd_type" runat="server" __designer:wfdid="w259" 
                    CssClass="inpText" Enabled="False" Font-Size="Small" Width="112px"></asp:DropDownList>





            </td>
            <td align="left" style="WIDTH: 118px">
                &nbsp;&nbsp;
            </td>
            <td align="left" valign="top">
            </td>
        </tr>
        <!-- end dimensi -->
        <TR>
            <td align="left" style="WIDTH: 122px">
                <asp:Label ID="Label55" runat="server" __designer:wfdid="w260" 
                    CssClass="normalFont" Font-Size="Small" Text="Merk" Width="109px"></asp:Label>





            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="dd_merk" runat="server" __designer:wfdid="w261" 
                    CssClass="inpText" Enabled="False" Font-Size="Small" Width="112px"></asp:DropDownList>





            </td>
            <td align="left" style="WIDTH: 118px">
                &nbsp;</td>
            <td align="left" valign="top">
            </td>
        </TR>
        <tr>
            <td align="left" style="WIDTH: 122px">
                <asp:Label ID="Label74" runat="server" __designer:wfdid="w262" 
                    CssClass="normalFont" Font-Size="Small" Text="Satuan" Width="109px"></asp:Label>





            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="satuan2" runat="server" __designer:wfdid="w263" 
                    CssClass="inpText" Font-Size="Small" Width="112px"></asp:DropDownList>





            </td>
            <td align="left" style="WIDTH: 118px">
                &nbsp;</td>
            <td align="left" valign="top">
                &nbsp;</td>
        </tr>
        <TR>
            <TD align=left style="WIDTH: 122px" class="Label">
                <asp:Label ID="Label68" runat="server" __designer:wfdid="w266" 
        Text="Flag Item" Width="56px"></asp:Label>





            </TD>
            <TD align=left>
                <asp:DropDownList ID="dd_stock" runat="server" 
        __designer:wfdid="w267" CssClass="inpText" Enabled="False" Font-Size="Small" 
        Width="186px"><asp:ListItem Value="T">Transaction</asp:ListItem>
<asp:ListItem Value="I">Inventory</asp:ListItem>
<asp:ListItem Value="V">Voucher</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList>





            </TD>
            <TD style="WIDTH: 118px; color: #000099;" align=left>
                &nbsp;<span style="COLOR: #ff0000"></span></TD>
            <TD align=left style="COLOR: #000099">
                &nbsp;</TD>
        </TR>
        <TR>
            <td align="left" style="WIDTH: 122px">
                <asp:Label ID="Label66" runat="server" __designer:wfdid="w268" 
                    CssClass="normalFont" Font-Size="Small" Text="Jenis Produk" Width="89px"></asp:Label>





            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="DDLStatusItem" runat="server" __designer:wfdid="w269" 
                    CssClass="inpText" Enabled="False" Font-Size="Small" Width="112px"><asp:ListItem>UNGGULAN</asp:ListItem>
<asp:ListItem>NON UNGGULAN</asp:ListItem>
</asp:DropDownList>





                <asp:Label ID="flagitem" runat="server" __designer:wfdid="w270" Visible="False"></asp:Label>





            </td>
            <td align="left" class="Label" style="WIDTH: 118px" valign="top">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
        </TR>
        <TR>
            <td align="left" style="WIDTH: 122px" vAlign="top">
                <asp:Label ID="Label67" runat="server" __designer:wfdid="w271" 
                    CssClass="normalFont" Font-Size="Small" Text="Status"></asp:Label>





            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlStatus" runat="server" __designer:wfdid="w272" 
                    CssClass="inpText" Enabled="False" Font-Size="Small">
                    <asp:ListItem Value="Aktif">Aktif</asp:ListItem>
<asp:ListItem Value="Tidak Aktif">Tidak Aktif</asp:ListItem>
</asp:DropDownList>





            </td>
            <TD align=left class="Label" style="WIDTH: 118px" valign="top">
                &nbsp;</TD>
            <TD align=left>
                &nbsp;</TD>
        </TR>
        <!-- end grouping barang -->
        <TR>
            <TD vAlign=top align=left style="WIDTH: 122px">
                <asp:Label ID="Label4" runat="server" __designer:wfdid="w273" 
        CssClass="normalFont" Font-Size="Small" Text="Keterangan"></asp:Label>





            </TD>
            <TD align=left colspan="3">
                <asp:TextBox ID="keterangan" runat="server" __designer:wfdid="w274" 
        CssClass="inpText" Font-Size="Small" MaxLength="100" Width="500px"></asp:TextBox>





            </TD>
        </TR>
        <TR>
            <TD id="TD23" align=left colSpan=2 runat="server">
                <asp:Label style="FONT-WEIGHT: 700" id="Label41" runat="server" Width="208px" CssClass="normalFont" Font-Size="Small" Text=".: Dimensi Expedisi Barang" __designer:wfdid="w275"></asp:Label>





            </TD>





            <TD style="WIDTH: 118px" id="TD6" align=left runat="server">
                &nbsp;</TD>





            <TD id="TD1" runat="server">
                &nbsp;</TD>





        </TR>
        <!-- Dimensi -->
        <TR>
            <td id="TD9" runat="server" align="left" class="Label" 
        style="WIDTH: 122px">
                <asp:Label ID="Label40" runat="server" __designer:wfdid="w278" 
        CssClass="normalFont" Font-Size="Small" Text="Panjang" Width="56px"></asp:Label>





                <span style="COLOR: #ff0000">*</span></td>





            <td id="TD4" runat="server" align="left">
                <span style="COLOR: #ff0000">
                <asp:TextBox ID="txt_panjang" runat="server" 
        __designer:wfdid="w279" AutoPostBack="True" CssClass="inpText" Width="100px">1.00</asp:TextBox>





                </span>
            </td>





            <td id="TD21" runat="server" align="left" style="WIDTH: 118px">
                <asp:Label ID="Label62" runat="server" __designer:wfdid="w280" 
        CssClass="normalFont" Font-Size="Small" Text="Lebar" Width="40px"></asp:Label>





                <span style="COLOR: #ff0000">*</span></td>





            <td id="Td12" runat="server" align="left">
                <asp:TextBox ID="txt_lebar" runat="server" __designer:wfdid="w281" 
        AutoPostBack="True" CssClass="inpText" Width="100px">1.00</asp:TextBox>





                <cc1:FilteredTextBoxExtender ID="txt_lebar_FilteredTextBoxExtender" 
        runat="server" __designer:wfdid="w285" Enabled="True" 
        TargetControlID="txt_lebar" ValidChars="0123456789.\ "></cc1:FilteredTextBoxExtender>





            </td>





        </TR>
        <TR>
            <TD style="WIDTH: 122px" align=left>
                <asp:Label ID="Label71" runat="server" __designer:wfdid="w282" 
        CssClass="normalFont" Font-Size="Small" Text="Tinggi" Width="48px"></asp:Label>





                <span style="COLOR: #ff0000">*</span></TD>
            <TD align=left>
                <asp:TextBox ID="txt_tinggi" runat="server" __designer:wfdid="w283" 
        AutoPostBack="True" CssClass="inpText" Width="100px">1.00</asp:TextBox>





                <cc1:FilteredTextBoxExtender 
        ID="txt_tinggi_FilteredTextBoxExtender" runat="server" __designer:wfdid="w284" 
        Enabled="True" TargetControlID="txt_tinggi" ValidChars="0123456789.\ "></cc1:FilteredTextBoxExtender>





            </TD>
            <TD style="WIDTH: 118px" align=left>
                <asp:Label ID="Label72" runat="server" __designer:wfdid="w278" 
        CssClass="normalFont" Font-Size="Small" Text="Berat Volume" Width="104px"></asp:Label>





            </TD>
            <TD align="left" valign="top">
                <asp:TextBox ID="beratVolume" runat="server" 
        __designer:wfdid="w288" CssClass="inpTextDisabled" Font-Size="Small" 
        MaxLength="25" size="20" Width="100px">0.00</asp:TextBox>





            </TD>
        </TR>
        <tr>
            <td align="left" style="WIDTH: 122px">
                <asp:Label ID="Label76" runat="server" __designer:wfdid="w289" 
                    CssClass="normalFont" Font-Size="Small" Text="Berat Barang" Width="89px"></asp:Label>





            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="beratBarang" runat="server" __designer:wfdid="w290" 
                    CssClass="inpText" Font-Size="Small" MaxLength="25" size="20" Width="151px">0.00</asp:TextBox>





                <cc1:FilteredTextBoxExtender ID="beratBarang_FilteredTextBoxExtender" 
                    runat="server" __designer:wfdid="w287" Enabled="True" 
                    TargetControlID="beratBarang" ValidChars="0123456789,."></cc1:FilteredTextBoxExtender>





            </td>
            <td align="left" class="Label" style="width: 118px;" valign="top">
                &nbsp;</td>
            <td align="left" style="VERTICAL-ALIGN: top">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <asp:Label ID="lblupd" runat="server" __designer:wfdid="w291" Font-Bold="True" 
                    Font-Size="X-Small" ForeColor="#585858"></asp:Label>





                by &nbsp;<asp:Label ID="Upduser" runat="server" __designer:wfdid="w292" 
                    Font-Bold="True" Font-Size="X-Small" ForeColor="#585858"></asp:Label>





                on &nbsp;<asp:Label ID="Updtime" runat="server" __designer:wfdid="w293" 
                    Font-Bold="True" Font-Size="X-Small" ForeColor="#585858"></asp:Label>





                &nbsp;&nbsp;
            
            </td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <asp:ImageButton ID="btnSave" runat="server" __designer:wfdid="w294" 
                    ImageAlign="AbsMiddle" ImageUrl="~/Images/Save.png"></asp:ImageButton>





                <asp:ImageButton ID="BtnCancel" runat="server" __designer:wfdid="w295" 
                    ImageAlign="AbsMiddle" ImageUrl="~/Images/Cancel.png"></asp:ImageButton>





                <asp:ImageButton ID="btnDelete" runat="server" __designer:wfdid="w296" 
                    ImageAlign="AbsMiddle" ImageUrl="~/Images/Delete.png"></asp:ImageButton>





                </td>
        </tr>
        <TR>
            <TD align=left style="WIDTH: 122px">
                &nbsp;</TD>
            <TD align=left valign="top">
                &nbsp;</TD>
            <TD style="width: 118px;" align=left class="Label" valign="top">
                &nbsp;</TD>
            <TD align="left" style="VERTICAL-ALIGN: top">
                &nbsp;</TD>
        </TR>
        <TR>
            <td align="left" colspan="4">
                &nbsp;</td>
        </TR>
    </TBODY></TABLE>
</ContentTemplate>
</cc1:TabPanel>
</ajaxToolkit:TabContainer> 
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" Font-Size="X-Small" ForeColor="Red"></asp:Label><BR /></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" Drag="True" PopupDragHandleControlID="label15" BackgroundCssClass="modalBackground" PopupControlID="Panelvalidasi" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdPanelPrint" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPrint" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid">
                <table style="WIDTH: 100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:Label ID="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Barang"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:Label ID="printType" runat="server" CssClass="Important"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px; TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:Label ID="orderIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label ID="orderNoForReport" runat="server" CssClass="Important"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:ImageButton ID="imbPrintPDF" OnClick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton ID="imbPrintExcel" OnClick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton ID="imbCancelPrint" OnClick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:Label ID="lblError" runat="server" CssClass="Important"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 20px" align="left"></td>
                            <td style="HEIGHT: 20px" align="left"></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePrint" runat="server" TargetControlID="btnHidePrint" Drag="True" PopupDragHandleControlID="lblPrint" BackgroundCssClass="modalBackground" PopupControlID="pnlPrint"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnHidePrint" runat="server" Visible="False"></asp:Button>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
