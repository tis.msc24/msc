<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="AllReportForm.aspx.vb" Inherits="ReportForm_AllReportForm" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="970">
        <tr>
            <td align="left" class="header" colspan="3" style="background-color: silver">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Maroon" Text=":: Semua Laporan"></asp:Label></td>
        </tr>
    </table>
    <table width="970">
        <tr>
            <td colspan="3" style="height: 15px">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width=500><TBODY><TR><TD style="WIDTH: 67px; HEIGHT: 21px"><asp:Label id="Label8" runat="server" Text="Laporan" __designer:wfdid="w16"></asp:Label></TD><TD style="WIDTH: 6px; HEIGHT: 21px"><asp:Label id="Label9" runat="server" Width="3px" Text=":" __designer:wfdid="w17"></asp:Label></TD><TD style="WIDTH: 123px; HEIGHT: 21px"><asp:DropDownList id="DDLReport" runat="server" Width="122px" CssClass="inpText" __designer:wfdid="w18" AutoPostBack="True"><asp:ListItem>Penerimaan Barang</asp:ListItem>
<asp:ListItem>Check Teknisi</asp:ListItem>
<asp:ListItem>Rencana Service</asp:ListItem>
<asp:ListItem>Pengerjaan</asp:ListItem>
<asp:ListItem>Service Final</asp:ListItem>
<asp:ListItem>SPK</asp:ListItem>
<asp:ListItem>Surat Jalan</asp:ListItem>
<asp:ListItem>Invoice Service</asp:ListItem>
<asp:ListItem>Pending</asp:ListItem>
<asp:ListItem>Oper Teknisi</asp:ListItem>
<asp:ListItem>Barang Service Selesai</asp:ListItem>
<asp:ListItem>Invoice Service Batal</asp:ListItem>
<asp:ListItem>Teknisi Invoice</asp:ListItem>
</asp:DropDownList></TD><TD style="HEIGHT: 21px"><asp:DropDownList id="sumddl" runat="server" Width="122px" CssClass="inpText" __designer:wfdid="w1" Visible="False"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Label id="lblError" runat="server" __designer:wfdid="w2" Visible="False"></asp:Label><asp:Label id="lblType" runat="server" __designer:wfdid="w3" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 67px; HEIGHT: 21px"><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w19"></asp:Label></TD><TD style="WIDTH: 6px; HEIGHT: 21px"><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w20"></asp:Label></TD><TD style="WIDTH: 123px; HEIGHT: 21px"><asp:DropDownList id="DDLFilter" runat="server" Width="122px" CssClass="inpText" __designer:wfdid="w21"><asp:ListItem>No. Tanda Terima</asp:ListItem>
<asp:ListItem>Nama Customer</asp:ListItem>
<asp:ListItem>Barcode</asp:ListItem>
<asp:ListItem>Telepon 1</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="DDLPending" runat="server" Width="122px" CssClass="inpText" __designer:wfdid="w22" Visible="False"><asp:ListItem>Nama Teknisi</asp:ListItem>
</asp:DropDownList></TD><TD style="HEIGHT: 21px"><asp:TextBox id="txtFilter" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w23"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 67px; HEIGHT: 10px"><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w24"></asp:Label></TD><TD style="WIDTH: 6px; HEIGHT: 10px"><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w25"></asp:Label></TD><TD style="WIDTH: 123px; HEIGHT: 10px"><asp:TextBox id="txtPeriod1" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w26"></asp:TextBox> <asp:ImageButton id="btnCalendar1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton></TD><TD style="HEIGHT: 10px"><asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w28"></asp:Label>&nbsp; <asp:TextBox id="txtPeriod2" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w29"></asp:TextBox> <asp:ImageButton id="btnCalendar2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w30"></asp:ImageButton> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w31"></asp:Label></TD></TR><TR><TD style="WIDTH: 67px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w32"></asp:CheckBox></TD><TD style="WIDTH: 6px"><asp:Label id="Label10" runat="server" Text=":" __designer:wfdid="w33"></asp:Label></TD><TD style="WIDTH: 123px"><asp:DropDownList id="DDLStatus" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w34"></asp:DropDownList> <asp:DropDownList id="DDLOper" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w1" Visible="False"><asp:ListItem>Process</asp:ListItem>
<asp:ListItem>Switch</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="ddlSpk" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w1" Visible="False"><asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="ddlSJ" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w1" Visible="False"><asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Send</asp:ListItem>
</asp:DropDownList></TD><TD></TD></TR><TR><TD style="WIDTH: 67px"></TD><TD style="WIDTH: 6px"></TD><TD style="WIDTH: 123px"></TD><TD></TD></TR></TBODY></TABLE><TABLE width=500><TBODY><TR><TD style="HEIGHT: 15px" colSpan=3><asp:ImageButton id="btnview" onclick="btnview_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnETPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton> <asp:ImageButton id="btnETExcel" onclick="btnETExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton> </TD></TR><TR><TD colSpan=3></TD></TR><TR><TD style="HEIGHT: 15px" colSpan=3><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><CR:CrystalReportViewer id="CRVAllReport" runat="server" __designer:wfdid="w39" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasPageNavigationButtons="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR><TR><TD style="HEIGHT: 15px" colSpan=3><ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" __designer:wfdid="w40" TargetControlID="txtPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" __designer:wfdid="w41" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CEPeriod1" runat="server" __designer:wfdid="w42" TargetControlID="txtPeriod1" Format="dd/MM/yyyy" PopupButtonID="btnCalendar1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CEPeriod2" runat="server" __designer:wfdid="w43" TargetControlID="txtPeriod2" Format="dd/MM/yyyy" PopupButtonID="btnCalendar2"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnETExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnETPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnview"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel2" runat="server">
        <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False" DefaultButton="btnErrOK"><TABLE><TBODY><TR><TD style="HEIGHT: 15px; BACKGROUND-COLOR: red" colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelErrMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="PanelPass" runat="server" CssClass="modalBox" __designer:wfdid="w3" Visible="False" DefaultButton="btnOKPass"><TABLE width=300><TBODY><TR><TD align=center colSpan=3><asp:Label id="Label17" runat="server" Font-Size="Medium" Font-Bold="True" Text="Masukkan Password" __designer:wfdid="w4"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=300><TBODY><TR><TD style="WIDTH: 80px">&nbsp;</TD><TD style="WIDTH: 9px"></TD><TD></TD></TR><TR><TD style="WIDTH: 80px"><asp:Label id="Label13" runat="server" Text="Password" __designer:wfdid="w5"></asp:Label> <asp:Label id="Label16" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w6"></asp:Label></TD><TD style="WIDTH: 9px"><asp:Label id="Label14" runat="server" Text=":" __designer:wfdid="w7"></asp:Label></TD><TD><asp:TextBox id="txtPass" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w8" TextMode="Password"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 80px"></TD><TD style="WIDTH: 9px"></TD><TD></TD></TR></TBODY></TABLE><TABLE width=300><TBODY><TR><TD></TD><TD></TD><TD></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnOKPass" runat="server" ImageUrl="~/Images/ok.png" __designer:wfdid="w9"></asp:ImageButton> <asp:ImageButton id="btnCancelPass" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w10"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnPass" runat="server" __designer:wfdid="w11" Visible="False"></asp:Button><BR /><ajaxToolkit:ModalPopupExtender id="MPEPass" runat="server" __designer:wfdid="w12" TargetControlID="btnPass" BackgroundCssClass="modalBackground" PopupDragHandleControlID="Label17" PopupControlID="PanelPass" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnOKPass"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</asp:Content>

